﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"

RelationServiceProtocol CDealRelationSrvPkg::m_relationProtocol;


void CDealRelationSrvPkg::Init()
{
	//todo wcj 未注册的消息
	Register( R_G_ReportGateIdResult, &OnReportGateIdResult );


	Register( R_G_CreateTeamResult, &OnCreateTeamResult );
	Register( R_G_AddTeamMemberResult, &OnAddTeamMemberResult );	//???结果的反馈
	Register( R_G_TeamCreatedNotify,  &OnCreateTeamNotify );			//创建组队
	Register( R_G_AddTeamMemberNotify, &OnAddTeamMemberNotify );		//增加组员
	Register( R_G_RemoveTeamMemberNotify, &OnRemoveTeamMemberNotify );	     //移除组员
	Register( R_G_TeamCaptainModifiedNotify, &OnModifyTeamCaptainNotify );   //改变队长
	Register( R_G_TeamBroadcastNotify, &OnTeamBroadcastNotify );   //组队聊天
	Register( R_G_TeamDestroyedNotify, &OnTeamDestroyedNotify );	//组队解散
	Register( R_G_ModifyExpSharedModeNotify, &OnModifyExpSharedModeNotify );	//组队经验更改
	Register( R_G_ModifyGoodSharedModeNotify, &OnModifyGoodSharedModeNotify );	//组队物品更改
	Register( R_G_GROUP_MSG, &OnRecvFriendGroupInfo );//好友组信息
	Register( R_G_FRIENDS_MSG,&OnRecvFriendInfo );//好友信息
	Register( R_G_ADDFRIEND_MSG,&OnRecvAddFriendResult );//加好友的消息RelationSrv的反馈
	Register( R_G_DELFRIEND_MSG,&OnRecvDeleteFriendResult );//删除好友的消息RelationSrv的反馈
	Register( R_G_UPDATAFRIEND_MSG,&OnRecvUpdateFriendInfo );//接到RelationSrv传回的好友更新消息
	Register( R_G_ADD_GROUP, &OnRecvAddFriendGroup );//接到创建好友组的返回
	Register( R_G_FRIEND_ALTER_GROUP_ID,&OnRecvMoveFrindToOtherGroup );//改变好友所属组的消息
	Register( R_G_DEL_GROUP, &OnRecvDeleteFriendGroup );//接到了删除好友组的消息
	Register( R_G_SENDGROUPMSG, &OnRecvFriendGroupChat );//接到好友组了消息
	Register( R_G_SENDMSG, &OnRecvFriendSingleChat );//接到好友之间的聊天
	Register( R_G_ALTERGROUPNAME ,&OnRecvChangeFriendGroupName );//改变组名的消息
	Register( R_G_BLACKLIST, &OnRecvBlackList );
	Register( R_G_ADDTOBLACKLIST, &OnRecvAddPlayerToBlackList );
	Register( R_G_DELFROMBLACKLIST,&OnRecvDelPlayerFromBlackList);
	Register( R_G_ENEMY_LIST, &OnRecvFoeList );
	Register( R_G_LOCK_ENEMY,&OnRecvLockFoeResult );
	Register( R_G_DEL_ENEMY ,&OnRecvDelFoeResult );
	Register( R_G_ADD_ENEMY, &OnRecvAddFoeResult );
	Register( R_G_UPDATE_ENEMY, &OnRGUpdateEnemy );
	Register( R_G_QueryTeamResult, &OnQueryTeamResult );//切换地图查询队伍信息的反馈 
	Register( R_G_ModifyTeamRollItemLevelNotify, &OnModifyTeamRollItemLevelNotify );
	Register( R_G_NOTIFY_R_GROUP_MSG_SUM,&OnRecvChangeFriendGroupMsgCount );
	Register( R_G_CreateChatGroupResult,&OnCreateChatGroupResult );//创建群聊结果
	Register( R_G_DestroyChatGroupResult,&OnDestroyChatGroupResult );//销毁群聊结果
	Register( R_G_ChatGroupDestroyedNotify,&OnDestrocyChatGroupNotify );//销毁群聊的通知
	Register( R_G_AddChatGroupMemberResult,&OnAddChatGroupMemberResult );//加入群聊成员结果
	Register( R_G_ChatGroupMemberAddedNotify,&OnAddChatGroupMemberNotify );//加入群聊成员的通知
	Register( R_G_RemoveChatGroupMemberResult,&OnRemoveChatGroupMemberResult );//群聊成员离开结果
	Register( R_G_ChatGroupMemberRemovedNotify,&OnRemoveChatGroupMemberNotify );//群聊成员离开的通知
	Register( R_G_ChatGroupSpeekResult,&OnChatGroupSpeakResult );//群聊中聊天结果
	Register( R_G_ChatGroupSpeekNotify,&OnChatGroupSpeakNotify );//群聊中聊天的通知
	Register( R_G_QueryTeamMembersCountResult, &OnTeamMemberCountRst ); //查询队伍中人数多少的

	Register( R_G_CreateUnionResult, &OnCreateUnionRst ); //创建工会结果
	Register( R_G_DestroyUnionResult, &OnDestroyUnionRst );//解散工会结果
	Register( R_G_UnionDestroyedNotify, &OnDestroyUnionNtf ); //解散工会通知
	Register( R_G_QueryUnionBasicResult, &OnQueryUnionBasicInfo ); //请求工会基本信息
	Register( R_G_QueryUnionMemberListResult, &OnQueryUnionMemberInfo ); //请求工会成员信息
	Register( R_G_AddUnionMemberConfirm, &OnAddUnionMember ); //增加工会成员请求转发
	Register( R_G_AddUnionMemberResult, &OnAddUnionMemberRst ); //增加工会成员请求结果
	Register( R_G_UnionMemberAddedNotify, &OnAddUnionMemberNtf ); //增加工会成员通知
	Register( R_G_RemoveUnionMemberResult, &OnRemoveChatGroupMemberResult );//离开工会结果
	Register( R_G_UnionMemberRemovedNotify, &OnRemoveUnionMemberNtf ); //离开工会通知
	//Register( R_G_UNION_POS_RIGHTS_NOTIFY, &OnUninPosRightsNtf ); //工会职位对应权限修改通知
	//Register( R_G_UNION_MODIFY_POS_RIGHTS_RESULT, &OnModifyUnionPosRightRst ); //工会职位对应权限修改结果
	//Register( R_G_UNION_ADD_POS_RESULT, &OnAddUnionPosRst ); //工会职位增加结果
	//Register( R_G_UNION_REMOVE_POS_RESULT, &OnRemoveUnionPosRst ); //工会职位减少结果
	Register( R_G_UNION_TRANSFORM_CAPTION_RESULT, &OnTransformUnionCaptionRst ); //转移公会会长职务结果
	Register( R_G_UNION_TRANSFORM_CAPTION_NOTIFY, &OnTransformUninCaptionNtf ); //转移公会会长职务通知
	Register( R_G_UNION_MODIFY_MEMBER_TITLE_RESULT, &OnModifyUnionMemberTitleRst ); //修改工会成员称号结果
	Register( R_G_UNION_MEMBER_TITLE_NOTIFY, &OnUnionMemberTitileNtf ); //修改工会成员称号通知
	Register( R_G_UNION_ADVANCE_MEMBER_POS_RESULT, &OnAdvanceUnionMemberPosRst ); //提升工会成员职位结果
	Register( R_G_UNION_ADVANCE_MEMBER_POS_NOTIFY, &OnAdvanceUnionMemerPosNtf ); //提升工会成员职位通知
	Register( R_G_UNION_REDUCE_MEMBER_POS_RESULT, &OnReduceUnionMemberPosRst ); //降低工会成员职位结果
	Register( R_G_UNION_REDUCE_MEMBER_POS_NOTIFY, &OnReduceUnionMemberPosNtf ); //降低工会成员职位通知
	Register( R_G_UNION_CHANNEL_SPEEK_RESULT, &OnUnionChannelSpeekRst ); //工会频道聊天结果
	Register( R_G_UNION_CHANNEL_SPEEK_NOTIFY, &OnUnionChannelSpeekNtf ); //工会频道聊天通知
	Register( R_G_UNION_UPDATE_POS_CFG_RESULT, &OnUpdateUnionPosCfgRst ); //更新工会职务管理结果
	Register( R_G_UNION_UPDATE_POS_CFG_NOTIFY, &OnUpdateUnionPosCfgNtf ); //更新工会职务管理通知
	Register( R_G_UNION_FORBID_SPEEK_RESULT, &OnForbidUnionSpeekRst ); //工会成员禁言结果
	Register( R_G_UNION_FORBID_SPEEK_NOTIFY, &OnForbidUnionSpeekNtf ); //工会成员禁言通知
	Register( R_G_UNION_SKILLS_LIST_NOTIFY, &OnUnionSkills ); // 工会技能
	Register( R_G_POST_UNION_TASK_LIST_RESULT, &OnIssueUnionTaskRst ); // 发布工会任务结果
	Register( R_G_UNION_TASK_LIST_NOTIFY, &OnIssueUnionTaskNtf ); //发布工会任务通知
	//Register( R_G_UNION_TASK_NOTIFY, &OnDisplayUnionTasks ); //显示已发布任务
	Register( R_G_STUDY_UNION_SKILL_RESULT, &OnLearnUnionSkillRst ); //学习技能结果
	//Register( R_G_STUDY_UNION_SKILL_NOTIFY, &OnLearnUnionSkillNtf ); //学习技能通知
	Register( R_G_UNION_ACTIVE_POINT_NOTIFY, &OnChanageUnionActiveNtf ); //改变工会活跃度通知
	Register( R_G_ADVANCE_UNION_LEVEL_RESULT, &OnAdvanceUnionLevelRst ); //提升等级结果
	Register( R_G_UNION_LEVEL_NOTIFY, &OnUnionLevelNtf ); //等级通知
	Register( R_G_POST_UNION_BULLETIN_RESULT, &OnPostUnionBulletinRst ); //工会公告结果
	Register( R_G_UNION_BULLETIN_NOTIFY, &OnUnionBulletinNtf ); //工会公告通知
	Register( R_G_UNION_MEMBER_UPLINE_NOTIFY, &OnUnionMemberOnLineNtf ); //工会成员上线通知
	Register( R_G_UNION_MEMBER_OFFLINE_NOTIFY, &OnUnionMemberOffLineNtf ); //工会成员下线通知
	Register( R_G_QUERY_UNION_LOG_RESULT, &OnQueryUnionLogRst ); //工会日至结果
	Register( R_G_UNION_EMAIL_NOTIFY, &OnUnionMailNtf ); //工会邮件通知
	Register( R_G_UNION_OWNER_CHECK_RESULT, &OnUnionOwnerCheckRst ); //是否公会会长
	Register( R_G_UNION_REMOVE_MEMBER_RESULT, &OnRemoveUnionMemberRst ); //ROMOVE工会成员
	Register( R_G_UNION_MODIFY_PRESTIGE_RESULT, &OnModifyUnionPrestigeRst );//威望修改结果
	Register( R_G_UNION_PRESTIGE_NOTIFY, &OnModifyUnionPrestigeNtf );//威望通知
	Register( R_G_UNION_MODIFY_BADGE_RESULT, &OnModifyUnionBadgeRst);//徽章修改结果
	Register( R_G_UNION_BADGE_NOTIFY, &OnModifyUnionBadgeNtf );//徽章通知
	Register( R_G_UNION_MULTI_NOTIFY, &OnUnionMultiNtf );//工会活跃度或威望n倍
	Register( R_G_QUERY_PLAYER_RELATION_RESULT, &OnGMQueryRelationRst );//查询关系信息
	Register( R_G_QUERY_PLAYER_INFO_RESULT, &OnQueryPlayerInfoResult );//查询关系信息
	Register( R_G_QUERY_UNION_RANK_RESULT, &OnRecvQueryUnionRstByPage );//查询工会排行的返回结果
	Register( R_G_SEARCH_UNION_RANK_RESULT,&OnRecvQueryUnionRstByUnionName );//依据工会名查询工会排行的结果
	Register( R_G_QUERY_RACEMASTER_RESULT,&OnRecvQueryRaceMasterRst );//返回族长的查询结果
	Register( R_G_PLAYER_UPLINE_TEAMID_NOTIFY,&OnRecvPlayerUpLineTeamIDNotify );//玩家上线组队信息返回
	Register( R_G_TeamLogNotify, &OnTeamLogNtf );//组队日至通知

	Register( R_G_ShowTweet, &OnFriendTweetNtf );//直接通知好友动态
	Register( R_G_QueryTwttesResult, &OnQueryFriendTweetRst );//好友动态的查询结果
	Register( R_G_QueryFriendsListResult, &OnQueryFriendListRst );//好友的好友列表
	Register( R_G_NotifyAddExp, &OnFriendNonTeamGainNtf );//非组队好友收益
	Register( R_G_QueryExpNeededLvlUpRequest, &OnQueryExpOfLevelUpReq );//升级需要的经验信息
	Register( R_G_NotifyTeamGain, &OnFriendTeamGainNtf );//组队好友收益
	Register( R_G_FriendDegreeGainEmail, &OnFriendNonTeamGainMailNtf);//好友非组队邮件通知

	Register( R_G_QUERY_NICKUNION_RST, &OnQueryNickUnionRst );//查询昵称战盟结果

	return ;
}


bool CDealRelationSrvPkg::OnReportGateIdResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnReportGateIdResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( CreateTeamResult, pRelationMsg, pPkg, wPkgLen )

		D_DEBUG("收到创建结果消息%d\n", pRelationMsg->returnCode);

	return true;
	TRY_END;
	return false;

}
//////////////////////////////////////////////////////////////////////////////////////////////

bool CDealRelationSrvPkg::OnCreateTeamResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnCreateTeamResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( ReportGateSvrIdResult, pRelationMsg, pPkg, wPkgLen )


		return true;
	TRY_END;
	return false;

}


bool CDealRelationSrvPkg::OnCreateTeamNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnCreateTeamNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( TeamCreateedNotify, pRelationMsg, pPkg, wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR(" 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnCreateTeamNotify,找不到通知的玩家\n");
		return false;
	}

	D_DEBUG("收到RelationSrv创建组队消息\n");
	PlayerID tmpID = pNotifyPlayer->GetPlayerID();
	if( (tmpID.dwPID == pRelationMsg->team.captainPlayerId) && (tmpID.wGID == pRelationMsg->team.captainGateId) )
	{
		pNotifyPlayer->SetTeamInfo( true, pRelationMsg->team.teamInd );
		pNotifyPlayer->SetTeamNum( 2 );		//建立成功只有2个人
		D_DEBUG("设置%s为队长\n", pNotifyPlayer->GetRoleName() );
	}else{
		//接受到这个消息的玩家是本次新加入的玩家
		//teamcopyinfo
		/************************************************************************/
		/*玩家为单人，他加入别人组队，先向centresrv发送离开副本组队的GELeaveSTeamCenterNoti,在向centresrv发送副本组队解散的GESTeamDestoryCenterNoti消息*/
		/***********************************************************************/
		pNotifyPlayer->NotiPlayerEndSingleTeamCopyFlag();

		pNotifyPlayer->SetTeamInfo( false, pRelationMsg->team.teamInd );
		pNotifyPlayer->SetTeamNum( 2 );		//建立成功只有2个人

		PlayerID invitePlayerID;
		invitePlayerID.dwPID = pRelationMsg->team.captainPlayerId;
		invitePlayerID.wGID = pRelationMsg->team.captainGateId;
		pNotifyPlayer->RemoveTeamInvitePlayer( invitePlayerID );
	}

	GMTeamDetialInfo mapTeam;
	ConvertRTeamInfoToMapTeamInfo( &pRelationMsg->team, &mapTeam );
	mapTeam.lNoticePlayerID = pNotifyPlayer->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMTeamDetialInfo, pNotifyPlayer, mapTeam );	//通知mapsrv组队建立

	GCTeamInfo teamInfo;
	ConvertRTeamInfoToCliTeamInfo( &pRelationMsg->team, &teamInfo );
	D_INFO("CreateTeam的组员信息 teamSize:%d teamID:%d\n", teamInfo.teamSize, teamInfo.ulTeamID );

	NewSendPlayerPkg( GCTeamInfo, pNotifyPlayer, teamInfo );

	return true;
	TRY_END;
	return false;
}


bool CDealRelationSrvPkg::OnAddTeamMemberResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddTeamMemberResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( AddTeamMemberResult, pRelationMsg, pPkg, wPkgLen );


	D_DEBUG("收到RELATION增加人员的结果 结果:%d\n", pRelationMsg->returnCode );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->palyerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAddTeamMemberResult 找不到通知的玩家\n");
		return false;
	}

	if( pRelationMsg->returnCode != R_G_RESULT_SUCCEED )
	{
		GCInvitePlayerJoinTeam errMsg;
		errMsg.nErrorCode = PLAYER_NOT_FIND;
		SafeStrCpy( errMsg.playerName, pRelationMsg->member.memberName.memberName );
		errMsg.nameSize = (UINT)strlen( errMsg.playerName ) + 1;

		NewSendPlayerPkg( GCInvitePlayerJoinTeam, pNotifyPlayer, errMsg );
	}


	return true;
	TRY_END;
	return false;
}


bool CDealRelationSrvPkg::OnAddTeamMemberNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddTeamMemberNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( AddTeamMemberNotify, pRelationMsg, pPkg, wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnAddTeamMemberNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAddTeamMemberNotify,找不到通知的玩家\n");
		return false;
	}

	unsigned int teamNum = pNotifyPlayer->GetTeamNum();


	//如果玩家之前没有在组队
	if( !pNotifyPlayer->IsInTeam() )
	{
		//接受到这个消息的玩家是本次新加入的玩家
		//teamcopyinfo
		/************************************************************************/
		/*玩家为单人，他加入别人组队，先向centresrv发送离开副本组队的GELeaveSTeamCenterNoti,在向centresrv发送副本组队解散的GESTeamDestoryCenterNoti消息*/
		/************************************************************************/
		if ( pNotifyPlayer->GetPlayerID().dwPID == pRelationMsg->playerIdAdded && 
			pNotifyPlayer->GetPlayerID().wGID == pRelationMsg->gateIdAdded )
		{
			pNotifyPlayer->NotiPlayerEndSingleTeamCopyFlag();
		}
		pNotifyPlayer->SetTeamInfo( false, pRelationMsg->teamInd );
	}

	//增加组员也要发送整个组队的消息-->mapsrv
	GMTeamDetialInfo mapTeam;
	ConvertRTeamInfoToMapTeamInfo( &pRelationMsg->team, &mapTeam );
	mapTeam.lNoticePlayerID = pNotifyPlayer->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMTeamDetialInfo, pNotifyPlayer, mapTeam );//通知mapsrv组队建立

	//通知组队成员的信息-->client
	SafeStruct( GCTeamInfo, teamInfo );
	ConvertRTeamInfoToCliTeamInfo( &pRelationMsg->team, &teamInfo );
	NewSendPlayerPkg( GCTeamInfo, pNotifyPlayer, teamInfo );

	if( teamNum == 0 )
	{
		pNotifyPlayer->SetTeamNum( 2 );
	}else{
		pNotifyPlayer->SetTeamNum( teamNum + 1 );
	}

	D_DEBUG("收到组队成员增加的消息,队伍编号:%d\n", teamInfo.ulTeamID );
	return true;
	TRY_END;
	return false;
}


bool CDealRelationSrvPkg::OnTeamDestroyedNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnTeamDestroyedNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( TeamDestroyedNotify, pRelationMsg, pPkg, wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnTeamDestroyedNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnTeamDestroyedNotify,找不到通知的玩家\n");
		return false;
	}

	if( !pNotifyPlayer->IsInTeam() )
	{
		D_WARNING("不在组队,RelationSrv却发来消息\n");
		return false;
	}

	//teamcopyinfo
	/************************************************************************/
	/* 玩家在队伍中，且不是队长，离开组队，此时向centersrv发送玩家离开副本组队信息GELeaveSTeamCenterNoti*/
	/************************************************************************/
	if ( !pNotifyPlayer->IsTeamCaptain() )
	{
		GELeaveSTeamCenterNoti srvmsg;
		StructMemSet( srvmsg, 0, sizeof(srvmsg) );
		srvmsg.leavePlayerID = pNotifyPlayer->GetPlayerID();
		srvmsg.steamID = pNotifyPlayer->GetPlayerCopyFlag();
		SafeStrCpy( srvmsg.szNickName, pNotifyPlayer->GetRoleName() );
		SendMsgToCenterSrv( GELeaveSTeamCenterNoti,srvmsg );

		D_DEBUG("玩家%s接到解散组队消息,玩家为非队长,向center发送组队GELeaveSTeamCenterNoti消息,teamID号:%d\n", pNotifyPlayer->GetRoleName(), srvmsg.steamID );
	}

	//mapsrv的发送
	SafeStruct( GMDisBandTeam, mapSrvMsg );
	mapSrvMsg.TeamID = ConvertRSrvTeamIndToTeamID( pNotifyPlayer->GetTeamId() );
	SendMsgToMapSrvByPlayer( GMDisBandTeam, pNotifyPlayer, mapSrvMsg );

	SafeStruct( GCDestoryTeam, climsg );
	climsg.teamID = ConvertRSrvTeamIndToTeamID( pNotifyPlayer->GetTeamId() );
	climsg.nErrorCode = INVITE_SUCCESS;

	NewSendPlayerPkg( GCDestoryTeam, pNotifyPlayer, climsg );

	D_DEBUG("收到解除组队消息 teamID:%d\n", climsg.teamID );
	pNotifyPlayer->DestoryTeam();

	return true;
	TRY_END;
	return false;

}


bool CDealRelationSrvPkg::OnRemoveTeamMemberNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRemoveTeamMemberNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RemoveTeamMemberNotify, pRelationMsg, pPkg, wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRemoveTeamMemberNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRemoveTeamMemberNotify,找不到通知的玩家\n");
		return false;
	}

	if( !pNotifyPlayer->IsInTeam() )
	{
		D_ERROR("发送的玩家不是组队玩家111\n");
		return false;
	}



	//teamcopyinfo
	/************************************************************************/
	/* 玩家在队伍中，非单人组队状态，且不是队长，离开组队，向centersrv发送离开副本组队信息GELeaveSTeamCenterNoti*/
	/************************************************************************/
	//if ( !pNotifyPlayer->IsTeamCaptain() )//如果玩家不是队长
	{
		if ( pNotifyPlayer->GetPlayerID().dwPID  == pRelationMsg->removedMember.playerId 
			&& pNotifyPlayer->GetPlayerID().wGID == pRelationMsg->removedMember.gateId  )//通知的玩家是要离开组队的人
		{
			GELeaveSTeamCenterNoti srvmsg;
			StructMemSet( srvmsg, 0, sizeof(srvmsg) );
			srvmsg.leavePlayerID = pNotifyPlayer->GetPlayerID();
			srvmsg.steamID = pNotifyPlayer->GetPlayerCopyFlag();
			SafeStrCpy( srvmsg.szNickName, pNotifyPlayer->GetRoleName() );
			SendMsgToCenterSrv( GELeaveSTeamCenterNoti,srvmsg );

			D_DEBUG("玩家%s离开组队,玩家为非队长,向center发送组队GELeaveSTeamCenterNoti消息,teamID号:%d\n", pNotifyPlayer->GetRoleName(), srvmsg.steamID );
		}
	}

	D_DEBUG("%s收到RelationSrv的REMOVE消息 被踢出的玩家GID:%d PID:%d\n", pNotifyPlayer->GetRoleName(), pRelationMsg->removedMember.gateId, pRelationMsg->removedMember.playerId );

	//发给MAPSRV
	GMKickTeamPlayer mapSrvMsg;
	mapSrvMsg.playerID.dwPID = pRelationMsg->removedMember.playerId;
	mapSrvMsg.playerID.wGID = pRelationMsg->removedMember.gateId;
	mapSrvMsg.TeamID = ConvertRSrvTeamIndToTeamID( pNotifyPlayer->GetTeamId() );
	SendMsgToMapSrvByPlayer( GMKickTeamPlayer, pNotifyPlayer, mapSrvMsg );


	PlayerID tmpID = pNotifyPlayer->GetPlayerID();
	if( (tmpID.dwPID == pRelationMsg->removedMember.playerId) && (tmpID.wGID == pRelationMsg->removedMember.gateId) )
	{
		pNotifyPlayer->LeaveTeam();
	}else
	{
		unsigned int teamNum = pNotifyPlayer->GetTeamNum();
		pNotifyPlayer->SetTeamNum( teamNum - 1 );
	}

	if( pRelationMsg->removeMode == 0)
	{
		SafeStruct( GCReqQuitTeam, climsg );
		climsg.quitMemberID.dwPID = pRelationMsg->removedMember.playerId;
		climsg.quitMemberID.wGID = pRelationMsg->removedMember.gateId;
		climsg.nErrorCode = INVITE_SUCCESS;
		NewSendPlayerPkg( GCReqQuitTeam, pNotifyPlayer, climsg );
	}else
	{
		SafeStruct( GCBanTeamMember, climsg );
		climsg.banPlayerID.dwPID = pRelationMsg->removedMember.playerId;
		climsg.banPlayerID.wGID = pRelationMsg->removedMember.gateId;
		climsg.teamID = ConvertRSrvTeamIndToTeamID( pRelationMsg->teamInd );
		climsg.nErrorCode = INVITE_SUCCESS;  //暂时???
		NewSendPlayerPkg( GCBanTeamMember, pNotifyPlayer, climsg );
	}

	//处理队伍人数

	return true;
	TRY_END;
	return false;

}


bool CDealRelationSrvPkg::OnModifyTeamCaptainNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnModifyTeamCaptainNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( ModifyTeamCaptainNotify, pRelationMsg, pPkg, wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyTeamCaptainNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnModifyTeamCaptainNotify,找不到通知的玩家\n");
		return false;
	}


	if( !pNotifyPlayer->IsInTeam() )
	{
		D_ERROR("发送的玩家根本没组队\n");
		return false;
	}

	PlayerID notifyID = pNotifyPlayer->GetPlayerID();
	if( (notifyID.dwPID == pRelationMsg->captainPlayerId ) && ( notifyID.wGID == pRelationMsg->captainGateId) )
	{
		pNotifyPlayer->EnableTeamCaptain();
		D_DEBUG("%s被任命为队长\n", pNotifyPlayer->GetRoleName() );
	}else
	{
		pNotifyPlayer->DisableTeamCaptain();
	}

	GMChangeTeamLeader srvmsg;
	srvmsg.TeamID = ConvertRSrvTeamIndToTeamID( pRelationMsg->teamInd );
	srvmsg.playerID.dwPID = pRelationMsg->captainPlayerId;
	srvmsg.playerID.wGID = pRelationMsg->captainGateId;
	SendMsgToMapSrvByPlayer( GMChangeTeamLeader, pNotifyPlayer, srvmsg );


	SafeStruct( GCChangeLeader, climsg );
	climsg.leaderID.dwPID = pRelationMsg->captainPlayerId;
	climsg.leaderID.wGID = pRelationMsg->captainGateId;
	climsg.nErrorCode = INVITE_SUCCESS;

	NewSendPlayerPkg( GCChangeLeader, pNotifyPlayer, climsg );

	return true;
	TRY_END;
	return false;
}


bool CDealRelationSrvPkg::OnTeamBroadcastNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnTeamBroadcastNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( TeamBroadcastNotify, pRelationMsg, pPkg, wPkgLen );

	if( pRelationMsg->toGateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnTeamBroadcastNotify       发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->toPlayerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnTeamBroadcastNotify,找不到通知的玩家\n");
		return false;
	}

	SafeStruct( GCChat, climsg );
	climsg.sourcePlayerID.dwPID = pRelationMsg->fromPlayerId;
	climsg.sourcePlayerID.wGID = pRelationMsg->fromGateId;
	climsg.chatType = CT_TEAM_CHAT;
	climsg.extInfo = 0;//无额外信息；
	SafeStrCpy( climsg.strChat, pRelationMsg->broadcastData.data );
	climsg.chatSize = (UINT)strlen( climsg.strChat );

	NewSendPlayerPkg( GCChat, pNotifyPlayer, climsg );

	return true;
	TRY_END;
	return false;

}


bool CDealRelationSrvPkg::OnModifyExpSharedModeNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnModifyExpSharedModeNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( ModifyExpSharedModeNotify, pRelationMsg, pPkg, wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyExpSharedModeNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnModifyExpSharedModeNotify,找不到通知的玩家\n");
		return false;
	}

	SafeStruct( GMChangeTeamExpreMode, mapSrvMsg );
	mapSrvMsg.expreShareMode = (EXPERIENCE_SHAREMODE)pRelationMsg->mode;
	mapSrvMsg.TeamID = ConvertRSrvTeamIndToTeamID( pNotifyPlayer->GetTeamId() );
	SendMsgToMapSrvByPlayer( GMChangeTeamExpreMode, pNotifyPlayer, mapSrvMsg );

	SafeStruct( GCChangeTeamExpMode, climsg );
	climsg.nErrorCode = INVITE_SUCCESS;
	climsg.ucExpMode = (TYPE_UI8)pRelationMsg->mode;

	NewSendPlayerPkg( GCChangeTeamExpMode, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeTeamExpMode, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;

}


bool CDealRelationSrvPkg::OnModifyGoodSharedModeNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnModifyGoodSharedModeNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( ModifyGoodSharedModeNotify, pRelationMsg, pPkg, wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyGoodSharedModeNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnModifyGoodSharedModeNotify,找不到通知的玩家\n");
		return false;
	}

	SafeStruct( GMChangeTeamItemMode, mapSrvMsg );
	mapSrvMsg.TeamID = ConvertRSrvTeamIndToTeamID( pNotifyPlayer->GetTeamId() );
	mapSrvMsg.itemShareMode = (ITEM_SHAREMODE)pRelationMsg->mode;
	SendMsgToMapSrvByPlayer( GMChangeTeamItemMode, pNotifyPlayer, mapSrvMsg );

	SafeStruct( GCChangeTeamItemMode, climsg );
	climsg.nErrorCode = INVITE_SUCCESS;
	climsg.ucItemMode = (TYPE_UI8)pRelationMsg->mode;

	NewSendPlayerPkg( GCChangeTeamItemMode, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeTeamItemMode, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealRelationSrvPkg::OnQueryTeamResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnQueryTeamResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( QueryTeamResult, pRelationMsg, pPkg, wPkgLen );  //查询回来就返回给切换地图的人，只有切换地图时候才去查询

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnQueryTeamResult,找不到通知的玩家\n");
		return false;
	}

	GMTeamDetialInfo mapTeam;
	ConvertRTeamInfoToMapTeamInfo( &pRelationMsg->team, &mapTeam );
	mapTeam.lNoticePlayerID = pNotifyPlayer->GetPlayerID();
	SendMsgToMapSrvByPlayer( GMTeamDetialInfo, pNotifyPlayer, mapTeam );	//通知mapsrv组队建立

	return true;
	TRY_END;
	return false;

}

//接到好友组信息
bool CDealRelationSrvPkg::OnRecvFriendGroupInfo(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvFriendGroupInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGFriendGroups, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->player.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvFriendGroupInfo 发来的gateID 不对\n");
		return false;
	}

	if ( pRelationMsg->friendGroupsLen > 10 )
	{
		D_ERROR(" CDealRelationSrvPkg::OnRecvFriendGroupInfo assert 接到friendGroupLen大于10\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->player.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvFriendGroupInfo,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("角色%d接到了传来的好友组信息\n",pRelationMsg->player.PPlayer  );
#endif

	//将好友组的信息转发给客户端
	GCFriendGroups groupMsg;
	StructMemSet( groupMsg,0x0, sizeof(groupMsg) );
	groupMsg.groupMsgCount = pRelationMsg->Remanent;
	groupMsg.friendGroupsSize = pRelationMsg->friendGroupsLen ;
	for ( unsigned int i = 0 ; i< groupMsg.friendGroupsSize; i++ )
	{
		if ( ( i >= ARRAY_SIZE(pRelationMsg->friendGroups) )
			|| ( i >= ARRAY_SIZE(groupMsg.friendGroups) )
			)
		{
			D_ERROR( "OnRecvFriendGroupInfo, i(%d), pRelationMsg->friendGroups或groupMsg.friendGroups越界\n", i );
			return false;
		}
		RelationServiceNS::FriendGroupInfo& rlFriendInfo = pRelationMsg->friendGroups[i];
		MUX_PROTO::FriendGroupInfo& gcfriendInfo = groupMsg.friendGroups[i];
		gcfriendInfo.groupID = rlFriendInfo.GroupID;
		gcfriendInfo.groupNameSize = rlFriendInfo.GroupNameLen;
		SafeStrCpy( gcfriendInfo.groupName, rlFriendInfo.GroupName );
	}

	NewSendPlayerPkg( GCFriendGroups, pNotifyPlayer, groupMsg );

	//好友仇人数量还要更新到mapSrv

	GMNotifyRelationNumber mapMsg;
	mapMsg.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
	mapMsg.type = GMNotifyRelationNumber::Relation_Friend;
	mapMsg.mode = GMNotifyRelationNumber::Number_Set;
	mapMsg.relationNum = pRelationMsg->validSize;
	SendMsgToMapSrvByPlayer( GMNotifyRelationNumber, pNotifyPlayer, mapMsg );


	//MsgToPut* pNewMsg = CreatePlayerPkg( GCFriendGroups, pNotifyPlayer, groupMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

#ifdef _DEBUG
	D_DEBUG("将组信息发送给客户端%d完毕\n",pRelationMsg->player.PPlayer );
#endif

	return true;
	TRY_END;
	return false;

}

//接到好友信息
bool CDealRelationSrvPkg::OnRecvFriendInfo(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvFriendInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGFriendInfos, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->player.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvFriendInfo 发来的gateID 不对\n");
		return false;
	}

	if (  pRelationMsg->friendinfosLen > 5 )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvFriendInfo assert len > 5\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->player.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvFriendInfo,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("角色%d接到传来的好友信息\n",pRelationMsg->player.PPlayer );
#endif

	GCPlayerFriends friendMsg;
	StructMemSet( friendMsg, 0x0, sizeof(GCPlayerFriends) );
	//好友长度
	friendMsg.pageID                      = pRelationMsg->validSize;
	int len = friendMsg.playerFriendsSize = pRelationMsg->friendinfosLen;

	for ( int i = 0 ; i< len; i++ )
	{
		if ( ( i >= ARRAY_SIZE(friendMsg.playerFriends) )
			|| ( i >= ARRAY_SIZE(pRelationMsg->friendinfos) )
			)
		{
			D_ERROR( "OnRecvFriendInfo, i(%d), friendMsg.playerFriends或pRelationMsg->friendinfos越界\n", i );
			return false;
		}
		MUX_PROTO::FriendInfo& fInfo = friendMsg.playerFriends[i];
		RelationServiceNS::FriendInfo& rInfo =  pRelationMsg->friendinfos[i];
		ConvertRFriendInfoToCliFriendInfo(fInfo,rInfo);
	}

	NewSendPlayerPkg( GCPlayerFriends, pNotifyPlayer, friendMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerFriends, pNotifyPlayer, friendMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

#ifdef _DEBUG
	D_DEBUG("发送组中好友信息给客户端:%d\n",pRelationMsg->player.PPlayer );
#endif

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvAddFriendResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvAddFriendResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGAddFriend, pRelationMsg,pPkg,wPkgLen );

#ifdef _DEBUG
	D_DEBUG("从RelationSrv接到了加好友的返回结果\n");
#endif

	//如果添加好友成功
	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		if( pRelationMsg->addFriend.Suc.playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
		{
			D_ERROR("CDealRelationSrvPkg::OnRecvAddFriendResult 发来的gateID 不对\n");
			return false;
		}

		CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->addFriend.Suc.playerID.PPlayer );
		if( NULL == pNotifyPlayer )
		{
			D_ERROR("OnRecvAddFriendResult,找不到通知的玩家\n");
			return false;
		}

		GCAddFriendResponse addFriendResp;
		StructMemSet( addFriendResp, 0x0,sizeof(GCAddFriendResponse) );
		addFriendResp.nErrcode = GCAddFriendResponse::ADD_FRIEND_OK;
		addFriendResp.isAgree  = true;
		MUX_PROTO::FriendInfo& fInfo = addFriendResp.friendInfo;
		RelationServiceNS::FriendInfo& rInfo =  pRelationMsg->addFriend.Suc.friendinfo;
		ConvertRFriendInfoToCliFriendInfo( fInfo, rInfo );
		NewSendPlayerPkg( GCAddFriendResponse, pNotifyPlayer, addFriendResp );

		GMNotifyRelationNumber addMsg;
		addMsg.mode = GMNotifyRelationNumber::Number_Add;
		addMsg.type = GMNotifyRelationNumber::Relation_Friend;
		addMsg.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
		addMsg.friendsex = ( pRelationMsg->addFriend.Suc.friendinfo.Flag == 0x2 )?1:2;
		addMsg.relationNum = 1;
		SendMsgToMapSrvByPlayer( GMNotifyRelationNumber, pNotifyPlayer, addMsg );

#ifdef _DEBUG
		D_DEBUG("发送加好友成功的消息给客户端\n");
#endif

		//记录日至
		CLogManager::DoRoleRelationChanage(pNotifyPlayer, pRelationMsg->addFriend.Suc.friendinfo.uiID, (unsigned char)LOG_SVC_NS::RT_FRIEND, (unsigned char)LOG_SVC_NS::RCT_ADD );

		return true;

	}//如果加好友失败
	else if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED  )
	{
		if( pRelationMsg->addFriend.Error.playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
		{
			D_ERROR("CDealRelationSrvPkg::OnRecvAddFriendResult 发来的gateID 不对\n");
			return false;
		}

		CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->addFriend.Error.playerID.PPlayer );
		if( NULL == pNotifyPlayer )
		{
			D_ERROR("OnRecvAddFriendResult,找不到通知的玩家\n");
			return false;
		}

		GCAddFriendResponse addFriendResp;
		StructMemSet( addFriendResp, 0x0,sizeof(GCAddFriendResponse) );
		addFriendResp.nErrcode = pRelationMsg->addFriend.Error.ErrorCode;
		NewSendPlayerPkg( GCAddFriendResponse, pNotifyPlayer, addFriendResp );

#ifdef _DEBUG
		D_DEBUG("发送加好友失败的消息给客户端\n");
#endif

	}

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvDeleteFriendResult(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvDeleteFriendResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGDelFriend, pRelationMsg,pPkg,wPkgLen );

#ifdef _DEBUG
	D_DEBUG("接到了RelaionSrv返回的删除好友的消息\n");
#endif


	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvDeleteFriendResult 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvDeleteFriendResult,找不到通知的玩家\n");
		return false;
	}

	//如果删除好友成功
	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		GCDelFriendResponse delFriendResp;
		StructMemSet(delFriendResp, 0x0,sizeof(delFriendResp) );
		delFriendResp.nErrcode = GCDelFriendResponse::DEL_FRIEND_OK;
		delFriendResp.friendID = pRelationMsg->delfriend.Suc.playerIDByDell.uiID;

		NewSendPlayerPkg( GCDelFriendResponse, pNotifyPlayer, delFriendResp );

#ifdef _DEBUG
		D_DEBUG("发送删除好友成功的消息给客户端\n");
#endif

		GMNotifyRelationNumber delMsg;
		delMsg.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
		delMsg.mode = GMNotifyRelationNumber::Number_Sub;
		delMsg.type = GMNotifyRelationNumber::Relation_Friend;
		delMsg.relationNum = 1;
		SendMsgToMapSrvByPlayer( GMNotifyRelationNumber, pNotifyPlayer, delMsg );

		//记录日至
		CLogManager::DoRoleRelationChanage(pNotifyPlayer, pRelationMsg->delfriend.Suc.playerIDByDell.uiID, (unsigned char)LOG_SVC_NS::RT_FRIEND, (unsigned char)LOG_SVC_NS::RCT_DEL );
	}
	//如果删除好友失败
	else if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED  )
	{
		GCDelFriendResponse delFriendResp;
		StructMemSet(delFriendResp, 0x0,sizeof(delFriendResp) );
		delFriendResp.nErrcode  = GCDelFriendResponse::DEL_FRIEND_FAILED;
		NewSendPlayerPkg( GCDelFriendResponse, pNotifyPlayer, delFriendResp );

#ifdef _DEBUG
		D_DEBUG("发送删除好友失败的消息给客户端\n");
#endif	
	}

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvUpdateFriendInfo(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvUpdateFriendInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGFriendInfoUpdate, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->player.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvUpdateFriendInfo 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->player.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvUpdateFriendInfo,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("接到了RelationSrv更新玩家信息的消息\n");
#endif

	GCFriendInfoUpdate friendInfoUpdate;
	StructMemSet( friendInfoUpdate, 0x0,sizeof(GCFriendInfoUpdate) );
	MUX_PROTO::FriendInfo& fInfo = friendInfoUpdate.friendInfo;
	fInfo.friendID =  pRelationMsg->uiID;
	fInfo.isOnline =  pRelationMsg->Online;
	fInfo.mapID    =  pRelationMsg->MapID;
	fInfo.playerClass = pRelationMsg->playerClass;
	fInfo.playerLevel = pRelationMsg->playerLevel;
	fInfo.FereNameLen = pRelationMsg->FereNameLen;
	SafeStrCpy( fInfo.FereName, pRelationMsg->FereName );
	fInfo.GuildNameLen = pRelationMsg->GuildNameLen;
	SafeStrCpy( fInfo.GuildName, pRelationMsg->GuildName );
	fInfo.degree = pRelationMsg->degree;
	fInfo.tweetsFlag = pRelationMsg->tweetsFlag;

	NewSendPlayerPkg( GCFriendInfoUpdate, pNotifyPlayer, friendInfoUpdate );

#ifdef _DEBUG
	D_DEBUG("发送好友信息更新的消息给客户端\n");
#endif

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvFriendGroupChat( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvFriendGroupChat,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGSendGroupMsg, pRelationMsg,pPkg,wPkgLen );

	if ( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvFriendGroupChat 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvFriendGroupChat,找不到通知的玩家\n");
		return false;
	}

	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		GCFriendChat gcChat;
		gcChat.chatContentSize = pRelationMsg->result.Suc.MsgLen;
		SafeStrCpy( gcChat.chatContent, pRelationMsg->result.Suc.Msg );
		gcChat.chaterNameSize = pRelationMsg->result.Suc.NameLen;
		SafeStrCpy( gcChat.chaterName, pRelationMsg->result.Suc.Name );
		gcChat.friendChatType = GCFriendChat::FRIEND_CHAT_GROUP;
		gcChat.Remanent = pRelationMsg->Remanent;

		NewSendPlayerPkg( GCFriendChat, pNotifyPlayer, gcChat );

#ifdef _DEBUG
		D_DEBUG("发送组聊天的消息给客户端\n");
#endif

	}
	else if( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED )
	{

	}

	return true;
	TRY_END;
	return false;

}


bool CDealRelationSrvPkg::OnRecvFriendSingleChat(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvFriendSingleChat,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGSendMsg, pRelationMsg,pPkg,wPkgLen  );

	if ( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvFriendSingleChat 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvFriendSingleChat,找不到通知的玩家\n");
		return false;
	}

	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		GCFriendChat gcChat;
		gcChat.chatContentSize = pRelationMsg->result.Suc.MsgLen;
		SafeStrCpy( gcChat.chatContent, pRelationMsg->result.Suc.Msg );
		gcChat.chaterNameSize = pRelationMsg->result.Suc.NameLen;
		SafeStrCpy( gcChat.chaterName, pRelationMsg->result.Suc.Name );
		gcChat.friendChatType = GCFriendChat::FRIEND_CHAT_SINGLE;
		gcChat.Remanent = 0; //单人聊天

		NewSendPlayerPkg( GCFriendChat, pNotifyPlayer, gcChat );

#ifdef _DEBUG
		D_DEBUG("发送好友双人聊天的消息给客户端\n");
#endif

	}
	else if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED )
	{

	}

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvChangeFriendGroupName(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvChangeFriendGroupName,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(RGAlterGroupName, pRelationMsg,pPkg,wPkgLen  );

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvChangeFriendGroupName 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvChangeFriendGroupName,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("接到了RelationSrv修改组名的消息\n");
#endif


	GCChgFriendGroupNameResponse changeFriendGroupName;
	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		changeFriendGroupName.nErrcode  = GCChgFriendGroupNameResponse::CHG_FRIENDGROUPNAME_OK;
		changeFriendGroupName.updatedFriendGroup.groupID = pRelationMsg->result.Suc.GroupID;
		changeFriendGroupName.updatedFriendGroup.groupNameSize = pRelationMsg->result.Suc.newGroupNameLen;
		SafeStrCpy( changeFriendGroupName.updatedFriendGroup.groupName, pRelationMsg->result.Suc.newGroupName );
	}
	else if( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED )
	{
		StructMemSet( changeFriendGroupName, 0x0, sizeof(GCChgFriendGroupNameResponse) );
		changeFriendGroupName.nErrcode = GCChgFriendGroupNameResponse::CHG_FRIENDGROUPNAME_FAILED;
	}

	NewSendPlayerPkg( GCChgFriendGroupNameResponse, pNotifyPlayer, changeFriendGroupName );

#ifdef _DEBUG
	D_DEBUG("发送修改组名的消息给客户端\n");
#endif

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvAddPlayerToBlackList(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvAddPlayerToBlackList,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(RGAddToBlacklist, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvAddPlayerToBlackList 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvAddPlayerToBlackList,找不到通知的玩家\n");
		return false;
	}

	GCAddBlacklistResponse addBlackListResp;
	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		addBlackListResp.nErrcode = GCAddBlacklistResponse::ADD_BLACKLIST_OK;
		addBlackListResp.blacklistItemInfo.nickNameSize = pRelationMsg->result.Suc.NameLen;
		SafeStrCpy( addBlackListResp.blacklistItemInfo.nickName,pRelationMsg->result.Suc.Name );

		//记录日至
		CLogManager::DoRoleRelationChanage(pNotifyPlayer, 0, (unsigned char)LOG_SVC_NS::RT_BLACK, (unsigned char)LOG_SVC_NS::RCT_ADD );
	}
	else if( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED )
	{
		StructMemSet( addBlackListResp, 0x0, sizeof(GCAddBlacklistResponse) );
		if(  pRelationMsg->result.Error.err != RelationServiceNS::R_G_RESULT_FRIENT_EXIST )
		{
			addBlackListResp.nErrcode = GCAddBlacklistResponse::ADD_BLACKLIST_FAILED;
		}
		else
			addBlackListResp.nErrcode = GCAddBlacklistResponse::ADD_BLACKLIST_FRIRENDFAILED;
	}

	NewSendPlayerPkg( GCAddBlacklistResponse, pNotifyPlayer, addBlackListResp );

#ifdef _DEBUG
	D_DEBUG("发送加黑名单成功的消息给客户端\n");
#endif

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvDelPlayerFromBlackList(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvDelPlayerFromBlackList,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(RGDelFromBlackList, pRelationMsg,pPkg,wPkgLen);

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvDelPlayerFromBlackList 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvDelPlayerFromBlackList,找不到通知的玩家\n");
		return false;
	}

	GCDelBlacklistResponse delBlackListResp;
	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		delBlackListResp.nErrcode        = GCDelBlacklistResponse::DEL_BLACKLIST_OK;
		delBlackListResp.delBlackNameLen = pRelationMsg->result.Suc.NameLen;
		SafeStrCpy( delBlackListResp.delBlackName , pRelationMsg->result.Suc.Name );

		//记录日至
		CLogManager::DoRoleRelationChanage(pNotifyPlayer, 0, (unsigned char)LOG_SVC_NS::RT_BLACK, (unsigned char)LOG_SVC_NS::RCT_DEL );
	} else if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED ) {
		StructMemSet(delBlackListResp,0x0,sizeof(GCDelBlacklistResponse) );
		delBlackListResp.nErrcode = GCDelBlacklistResponse::DEL_BLACKLIST_FAILED;
	}

	NewSendPlayerPkg( GCDelBlacklistResponse, pNotifyPlayer, delBlackListResp );

#ifdef _DEBUG
	D_DEBUG("删除黑名单的消息给客户端\n");
#endif

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvBlackList(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvBlackList,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(RGBlacklist, pRelationMsg,pPkg,wPkgLen  );

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvBlackList 发来的gateID 不对\n");
		return false;
	}

	if ( pRelationMsg->BlacklistLen > 10 )
	{
		D_ERROR("OnRecvBlackList 黑名单长度>10\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvBlackList,找不到通知的玩家\n");
		return false;
	}

	GCPlayerBlacklist blackListMsg;
	for ( int  i = 0 ; i< pRelationMsg->BlacklistLen; i++ )
	{
		if ( i >= ARRAY_SIZE(pRelationMsg->Blacklist) )
		{
			D_ERROR( "OnRecvBlackList, i(%d) >= ARRAY_SIZE(pRelationMsg->Blacklist)\n", i );
			return false;
		}
		RelationServiceNS::BlacklistItem& blackList = pRelationMsg->Blacklist[i];
		blackListMsg.playerBlacklist.nickNameSize = blackList.NameLen;
		SafeStrCpy( blackListMsg.playerBlacklist.nickName,blackList.Name );		

		NewSendPlayerPkg( GCPlayerBlacklist, pNotifyPlayer, blackListMsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerBlacklist, pNotifyPlayer, blackListMsg );
		//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
	}	

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvAddFriendGroup( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvAddFriendGroup,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGAddGroup, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvAddFriendGroup 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvAddFriendGroup,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("接到了RelationSrv返回的创建好友组的消息\n");
#endif

	GCCreateFriendGroupResponse createGroupRespMsg;
	//如果创建好友组成功
	if( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		createGroupRespMsg.nErrcode = GCCreateFriendGroupResponse::CREATE_FRIENDGROUP_OK;
		createGroupRespMsg.newFriendGroup.groupID = pRelationMsg->result.Suc.GroupID;
		createGroupRespMsg.newFriendGroup.groupNameSize = pRelationMsg->result.Suc.GroupNameLen;
		SafeStrCpy( createGroupRespMsg.newFriendGroup.groupName, pRelationMsg->result.Suc.GroupName );
	}
	//如果创建失败
	else if( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED )
	{
		StructMemSet( createGroupRespMsg, 0x0, sizeof(GCCreateFriendGroupResponse) );
		createGroupRespMsg.nErrcode = GCCreateFriendGroupResponse::CREATE_FRIENDGROUP_FAILED;
	}

	NewSendPlayerPkg( GCCreateFriendGroupResponse, pNotifyPlayer, createGroupRespMsg );

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvChangeFriendGroupMsgCount(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvChangeFriendGroupMsgCount,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGNotifyRGroupMsgNum, pRelationMsg,pPkg,wPkgLen );

#ifdef _DEBUG
	D_DEBUG("收到了更新好友组消息的个数信息\n");
#endif

	if( pRelationMsg->player.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR(" CDealRelationSrvPkg::OnRecvChangeFriendGroupMsgCount 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->player.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvChangeFriendGroupMsgCount,找不到通知的玩家\n");
		return false;
	}

	GCChangeGroupMsgNum changeMsg;
	changeMsg.MsgNum = pRelationMsg->Remanent;
	NewSendPlayerPkg( GCChangeGroupMsgNum, pNotifyPlayer, changeMsg );

	return true;
	TRY_END;

	return false;
}

bool CDealRelationSrvPkg::OnRecvMoveFrindToOtherGroup(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvMoveFrindToOtherGroup,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGFriendAlterGroupID, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvMoveFrindToOtherGroup 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvMoveFrindToOtherGroup,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("接到了改变好友组的消息\n");
#endif

	GCMoveFriendGroupResponse moveFriendResp;
	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		moveFriendResp.friendID = pRelationMsg->result.Suc.FriendID;
		moveFriendResp.newGroupID = pRelationMsg->result.Suc.newGroupID;
		moveFriendResp.orgGroupID = pRelationMsg->result.Suc.preGroupID;
		moveFriendResp.nErrcode =  GCMoveFriendGroupResponse::MOVE_FRIENDGROUP_OK;
#ifdef _DEBUG
		D_DEBUG("发送改变好友组队所属的消息给客户端 Friend:%d  OldID:%d NewID:%d \n",moveFriendResp.friendID,moveFriendResp.orgGroupID, moveFriendResp.newGroupID  );
#endif	
	}
	else if( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED )
	{
		StructMemSet( moveFriendResp, 0x0, sizeof(GCMoveFriendGroupResponse) );
		moveFriendResp.nErrcode = GCMoveFriendGroupResponse::MOVE_FRIENDGROUPNAME_FAILED;
	}

	NewSendPlayerPkg( GCMoveFriendGroupResponse, pNotifyPlayer, moveFriendResp );

#ifdef _DEBUG
	D_DEBUG("发送移动好友组的消息给客户端\n");
#endif

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvDeleteFriendGroup(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvDeleteFriendGroup,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGDelGroup, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvDeleteFriendGroup 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvDeleteFriendGroup,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("接到了RelationSrv返回的删除好友组的消息\n");
#endif

	GCDelFriendGroupResponse delFriendResp;
	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED  )
	{
		delFriendResp.nErrcode = GCDelFriendGroupResponse::DEL_FRIENDGROUP_OK;
		delFriendResp.groupID  = pRelationMsg->result.Suc.GroupID;
	} else if( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED ) {
		StructMemSet( delFriendResp,0x0, sizeof(GCDelFriendGroupResponse) );
		delFriendResp.nErrcode = GCDelFriendGroupResponse::DEL_FRIENDGROUP_FAILED;
	}

	NewSendPlayerPkg( GCDelFriendGroupResponse, pNotifyPlayer, delFriendResp );

#ifdef _DEBUG
	D_DEBUG("发送删除好友组的消息给客户端\n");
#endif

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvFoeList(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvFoeList,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGEnemylist,pRelationMsg,pPkg,wPkgLen  );

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvFoeList 发来的gateID 不对\n");
		return false;
	}

	if ( pRelationMsg->EnemylistLen > 5 )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvFoeList assert foeList.playerFoeListSize 大于5\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvFoeList,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("接到RelationSrv玩家的仇人消息\n");
#endif

	GMNotifyRelationNumber mapMsg;
	mapMsg.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
	mapMsg.mode = GMNotifyRelationNumber::Number_Set;
	mapMsg.type = GMNotifyRelationNumber::Relation_Enemy;
	mapMsg.relationNum = pRelationMsg->validSize;
	SendMsgToMapSrvByPlayer( GMNotifyRelationNumber, pNotifyPlayer, mapMsg );

	GCFoeList foeList;
	StructMemSet( foeList, 0x0, sizeof(GCFoeList) );
	foeList.playerFoeListSize = pRelationMsg->EnemylistLen;
	if (  pRelationMsg->EnemylistLen > ARRAY_SIZE(foeList.playerFoeList) )//检测两边协议长度是否相等
	{
		D_ERROR("GCFoeList与RGEnemylist长度不匹配\n");
		return false;
	} 

	for ( unsigned int i = 0 ; i< foeList.playerFoeListSize; i++  )
	{
		if ( ( i >= ARRAY_SIZE(foeList.playerFoeList) )
			|| ( i >= ARRAY_SIZE(pRelationMsg->Enemylist) )
			)
		{
			D_ERROR( "CDealRelationSrvPkg::OnRecvFoeList, i(%d)， foeList.playerFoeList或pRelationMsg->Enemylist越界\n", i );
			return false;
		}
		MUX_PROTO::FoeInfo& foeInfo = foeList.playerFoeList[i];
		RelationServiceNS::EnemylistItem& enemyInfo = pRelationMsg->Enemylist[i];
		ConvertRFoeInfoToCliFoeInfo( foeInfo,enemyInfo );
	}
	NewSendPlayerPkg( GCFoeList, pNotifyPlayer, foeList );
#ifdef _DEBUG
	D_DEBUG("将仇人信息发送给客户端\n");
#endif

	return true;
	TRY_END;
	return false;

}


bool CDealRelationSrvPkg::OnRGUpdateEnemy( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{	
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRGUpdateEnemy,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGUpdateEnemy, pRelationMsg, pPkg, wPkgLen);

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRGUpdateEnemy 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRGUpdateEnemy,找不到通知的玩家\n");
		return false;
	}

	SafeStruct( GCFoeInfoUpdate, climsg );
	climsg.updateFoe.foeID = pRelationMsg->Enemy.uiID;
	SafeStrCpy( climsg.updateFoe.nickName, pRelationMsg->Enemy.Name ); 
	climsg.updateFoe.nickNameSize = pRelationMsg->Enemy.NameLen;
	SafeStrCpy( climsg.updateFoe.GuildName,pRelationMsg->Enemy.GuildName );
	climsg.updateFoe.GuildNameLen = pRelationMsg->Enemy.GuildNameLen;
	SafeStrCpy( climsg.updateFoe.FereName, pRelationMsg->Enemy.FereName );
	climsg.updateFoe.FereNameLen = pRelationMsg->Enemy.FereNameLen;
	climsg.updateFoe.isOnline    = pRelationMsg->Enemy.Online;
	climsg.updateFoe.playerID.dwPID = pRelationMsg->Enemy.player.PPlayer;
	climsg.updateFoe.playerID.wGID  = pRelationMsg->Enemy.player.GateSrvID;
	climsg.updateFoe.mapID = pRelationMsg->Enemy.MapID;
	climsg.updateFoe.playerRace = pRelationMsg->Enemy.playerRace;
	climsg.updateFoe.playerIcon = pRelationMsg->Enemy.playerIcon;
	climsg.updateFoe.playerLevel = pRelationMsg->Enemy.playerLevel;
	climsg.updateFoe.playerClass = pRelationMsg->Enemy.playerClass;
	climsg.updateFoe.isLocked    =( pRelationMsg->Enemy.IsLock == 0x01 ) ? true : false;
	climsg.updateFoe.playerSex   =( pRelationMsg->Enemy.Sex    == 0x01 ) ? true : false;

	NewSendPlayerPkg( GCFoeInfoUpdate, pNotifyPlayer, climsg );

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvLockFoeResult(CRelationSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvLockFoeResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(RGLockEnemy,pRelationMsg,pPkg,wPkgLen);

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvLockFoeResult 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvLockFoeResult,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("接到RelationSrv玩家锁定仇人消息\n");
#endif

	GCLockFoeResponse gcLockFoeResp;
	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		gcLockFoeResp.nErrcode  = GCLockFoeResponse::LOCK_FOE_OK;
		gcLockFoeResp.foeItemID = pRelationMsg->result.Suc.uiID;
		gcLockFoeResp.bLock     = ( pRelationMsg->IsLock == 0x01 ) ? true : false;
	}
	else if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED )
	{
		gcLockFoeResp.nErrcode = GCLockFoeResponse::LOCK_FOE_FAILED;
	}

	NewSendPlayerPkg( GCLockFoeResponse, pNotifyPlayer, gcLockFoeResp );

#ifdef _DEBUG
	D_DEBUG("发送锁定仇人的消息给客户端\n");
#endif

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvDelFoeResult(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvDelFoeResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGDelEnemy, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvDelFoeResult 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvDelFoeResult,找不到通知的玩家\n");
		return false;
	}

#ifdef _DEBUG
	D_DEBUG("接到了删除仇人的消息\n");
#endif

	GCDelFoeResponse gcDelFoeResp;
	if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
		gcDelFoeResp.nErrcode  = GCDelFoeResponse::DEL_FOE_OK;
		gcDelFoeResp.foeItemID = pRelationMsg->result.Suc.uiID;

		GMNotifyRelationNumber delMsg;
		delMsg.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
		delMsg.mode = GMNotifyRelationNumber::Number_Sub;
		delMsg.type = GMNotifyRelationNumber::Relation_Enemy;
		delMsg.relationNum = 1;
		SendMsgToMapSrvByPlayer( GMNotifyRelationNumber, pNotifyPlayer, delMsg );

		//记录日至
		CLogManager::DoRoleRelationChanage(pNotifyPlayer, pRelationMsg->result.Suc.uiID, (unsigned char)LOG_SVC_NS::RT_ENEMY, (unsigned char)LOG_SVC_NS::RCT_DEL );
	}
	else if ( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_FAILED )
	{
		gcDelFoeResp.nErrcode = GCDelFoeResponse::DEL_FOE_FAILED;
	}

	NewSendPlayerPkg( GCDelFoeResponse, pNotifyPlayer, gcDelFoeResp );

#ifdef _DEBUG
	D_DEBUG("发送删除仇人的消息给客户端\n");
#endif

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRecvAddFoeResult(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvAddFoeResult,pOwner空\n" );
		return false;
	}

	DealRelationPkgPre( RGAddEnemy, pRelationMsg,pPkg,wPkgLen  );

	if( pRelationMsg->playerID.GateSrvID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvAddFoeResult 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvAddFoeResult,找不到通知的玩家\n");
		return false;
	}

	if( pRelationMsg->ErrCode == RelationServiceNS::R_G_RESULT_SUCCEED )
	{
#ifdef _DEBUG
		D_DEBUG("接到了增加仇人的消息,成功\n");
#endif

		GCNewFoe gcNewFoe;
		StructMemSet( gcNewFoe, 0x0, sizeof(gcNewFoe) );
		gcNewFoe.orgFoeItemID = pRelationMsg->result.Suc.Replace_uiID;
		gcNewFoe.newFoe.foeID = pRelationMsg->result.Suc.uiID;
		gcNewFoe.newFoe.mapID = pRelationMsg->result.Suc.MapID;
		gcNewFoe.newFoe.playerClass = pRelationMsg->result.Suc.playerClass;
		gcNewFoe.newFoe.playerRace  = pRelationMsg->result.Suc.playerRace;
		gcNewFoe.newFoe.playerLevel = pRelationMsg->result.Suc.playerLevel;
		gcNewFoe.newFoe.playerIcon  = pRelationMsg->result.Suc.playerIcon;
		gcNewFoe.newFoe.playerID.dwPID = pRelationMsg->result.Suc.player.PPlayer;
		gcNewFoe.newFoe.playerID.wGID = pRelationMsg->result.Suc.player.GateSrvID;
		gcNewFoe.newFoe.FereNameLen   = pRelationMsg->result.Suc.FereNameLen;
		SafeStrCpy( gcNewFoe.newFoe.FereName,  pRelationMsg->result.Suc.FereName );
		gcNewFoe.newFoe.GuildNameLen  = pRelationMsg->result.Suc.GuildNameLen;
		SafeStrCpy( gcNewFoe.newFoe.GuildName, pRelationMsg->result.Suc.GuildName );
		gcNewFoe.newFoe.nickNameSize  = pRelationMsg->result.Suc.NameLen;
		SafeStrCpy( gcNewFoe.newFoe.nickName,  pRelationMsg->result.Suc.Name );
		gcNewFoe.newFoe.playerSex = ( pRelationMsg->result.Suc.Sex & 0x01 )? true:false; 

		NewSendPlayerPkg( GCNewFoe, pNotifyPlayer, gcNewFoe );

		//通知mapsrv仇人的变化
		GMNotifyRelationNumber mapMsg;
		mapMsg.mode = GMNotifyRelationNumber::Number_Add;
		mapMsg.type = GMNotifyRelationNumber::Relation_Enemy;
		mapMsg.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
		mapMsg.relationNum = 1;
		SendMsgToMapSrvByPlayer( GMNotifyRelationNumber, pNotifyPlayer, mapMsg );

		//记录日至
		CLogManager::DoRoleRelationChanage(pNotifyPlayer, pRelationMsg->result.Suc.uiID, (unsigned char)LOG_SVC_NS::RT_ENEMY, (unsigned char)LOG_SVC_NS::RCT_ADD );

#ifdef _DEBUG
		D_DEBUG("转发增加仇人消息给客户端\n");
#endif

	}
	else
	{
		D_DEBUG("接到了增加仇人的消息,失败 错误号:%d\n",pRelationMsg->ErrCode );
	}
	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnModifyTeamRollItemLevelNotify( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnModifyTeamRollItemLevelNotify,pOwner空\n" );
		return false;
	}

	DealRelationPkgPre( ModifyTeamRollItemLevelNotify, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->toGateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyTeamRollItemLevelNotify 发来要寻找的玩家的gateId错\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->toPlayerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyTeamRollItemLevelNotify 改变物品ROLL等级找不到通知的玩家\n");
		return false;
	}

	GMChangeTeamRollLevel srvmsg;
	srvmsg.TeamID = ConvertRSrvTeamIndToTeamID( pRelationMsg->teamInd );
	srvmsg.rollItemLevel = pRelationMsg->level;
	SendMsgToMapSrvByPlayer( GMChangeTeamRollLevel, pNotifyPlayer, srvmsg );

	GCChangeTeamRollLevel climsg;
	climsg.nErrorCode = GCChangeTeamRollLevel::SUCCESS;
	climsg.ucRollLevel = pRelationMsg->level;

	NewSendPlayerPkg( GCChangeTeamRollLevel, pNotifyPlayer, climsg );

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnCreateChatGroupResult(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnCreateChatGroupResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( CreateChatGroupResult, pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->group.owner.gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnCreateChatGroupResult 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->group.owner.playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnCreateChatGroupResult,找不到通知的玩家\n");
		return false;
	}

	GCCreateChatGroupResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.ret = pRelationMsg->returnCode;
	resp.chatGroupID = pRelationMsg->group.groupId;

	D_DEBUG("群组编号%d\n", resp.chatGroupID);

	NewSendPlayerPkg( GCCreateChatGroupResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCCreateChatGroupResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnDestroyChatGroupResult(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnDestroyChatGroupResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( DestroyChatGroupResult, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnDestroyChatGroupResult 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnDestroyChatGroupResult,找不到通知的玩家\n");
		return false;
	}

	GCDestroyChatGroupResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.ret = pRelationMsg->returnCode;
	resp.chatGroupID = pRelationMsg->groupId;

	NewSendPlayerPkg( GCDestroyChatGroupResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCDestroyChatGroupResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnDestrocyChatGroupNotify(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnDestrocyChatGroupNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( ChatGroupDestroyedNotify, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnDestrocyChatGroupNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnDestrocyChatGroupNotify,找不到通知的玩家\n");
		return false;
	}

	GCDestroyChatGroupNotify notify;
	StructMemSet(notify, 0x00, sizeof(notify));

	notify.chatGroupID = pRelationMsg ->groupId;

	NewSendPlayerPkg( GCDestroyChatGroupNotify, pNotifyPlayer, notify );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCDestroyChatGroupNotify, pNotifyPlayer, notify );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnAddChatGroupMemberResult(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddChatGroupMemberResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( AddChatGroupMemberResult, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnAddChatGroupMemberResult 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAddChatGroupMemberResult,找不到通知的玩家\n");
		return false;
	}

	GCAddChatGroupMemberResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.ret = pRelationMsg->returnCode;
	resp.chatGroupID = pRelationMsg->group.groupId;
	if((pRelationMsg->group.members.membersCount > 0) && 
		(pRelationMsg->group.members.membersCount <= (sizeof(resp.chatGroupMembers)/sizeof(resp.chatGroupMembers[0]))))//成员列表
	{
		resp.chatGroupMemberCount = pRelationMsg->group.members.membersCount;

		for(UINT i = 0; i < resp.chatGroupMemberCount; i++)
		{
			if ( ( i >= ARRAY_SIZE(resp.chatGroupMembers) )
				|| ( i >= ARRAY_SIZE(pRelationMsg->group.members.members) )
				)
			{
				D_ERROR( "OnAddChatGroupMemberResult, i(%d), resp.chatGroupMembers或pRelationMsg->group.members.members越界\n", i );
				return false;
			}
			StructMemCpy( resp.chatGroupMembers[i].memberName, pRelationMsg->group.members.members[i].memberInd, sizeof(resp.chatGroupMembers[i].memberName) );
			if (sizeof(resp.chatGroupMembers[i].memberName) > pRelationMsg->group.members.members[i].memberIndLen)
			{
				resp.chatGroupMembers[i].memberNameSize = pRelationMsg->group.members.members[i].memberIndLen;
			} else {
				resp.chatGroupMembers[i].memberNameSize = sizeof(resp.chatGroupMembers[i].memberName);
			}
		}
	}

	NewSendPlayerPkg( GCAddChatGroupMemberResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddChatGroupMemberResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnAddChatGroupMemberNotify(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddChatGroupMemberNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( ChatGroupMemberAddedNotify, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnAddChatGroupMemberNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAddChatGroupMemberNotify,找不到通知的玩家\n");
		return false;
	}

	GCAddChatGroupMemberNotify notify;
	StructMemSet(notify, 0x00, sizeof(notify));

	notify.chatGroupID = 0;
	StructMemCpy( notify.memberName, pRelationMsg->memberAdded.memberId.memberInd, sizeof(notify.memberName) );
	if ( sizeof(notify.memberName) > pRelationMsg->memberAdded.memberId.memberIndLen )
	{
		notify.memberNameSize  = pRelationMsg->memberAdded.memberId.memberIndLen;
	} else {
		notify.memberNameSize = sizeof(notify.memberName);
	}

	NewSendPlayerPkg( GCAddChatGroupMemberNotify, pNotifyPlayer, notify );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddChatGroupMemberNotify, pNotifyPlayer, notify );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRemoveChatGroupMemberResult(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRemoveChatGroupMemberResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RemoveChatGroupMemberResult, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRemoveChatGroupMemberResult 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRemoveChatGroupMemberResult,找不到通知的玩家\n");
		return false;
	}

	GCRemoveChatGroupMemberResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.ret = pRelationMsg->returnCode;
	resp.chatGroupID = pRelationMsg->gateId;

	NewSendPlayerPkg( GCRemoveChatGroupMemberResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCRemoveChatGroupMemberResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnRemoveChatGroupMemberNotify(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRemoveChatGroupMemberNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( ChatGroupMemberRemovedNotify, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRemoveChatGroupMemberNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRemoveChatGroupMemberNotify,找不到通知的玩家\n");
		return false;
	}

	GCRemoveChatGroupNotify notify;
	StructMemSet(notify, 0x00, sizeof(notify));

	notify.chatGroupID = pRelationMsg->groupId;
	StructMemCpy( notify.memberName, pRelationMsg->memberRemoved.memberId.memberInd,sizeof(notify.memberName) );
	if ( sizeof(notify.memberName) > pRelationMsg->memberRemoved.memberId.memberIndLen )
	{
		notify.memberNameSize = pRelationMsg->memberRemoved.memberId.memberIndLen;
	} else {
		notify.memberNameSize = sizeof(notify.memberName);
	}

	NewSendPlayerPkg( GCRemoveChatGroupNotify, pNotifyPlayer, notify );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCRemoveChatGroupNotify, pNotifyPlayer, notify );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnChatGroupSpeakResult(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnChatGroupSpeakResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( ChatGroupSpeekResult, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnChatGroupSpeakResult 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnChatGroupSpeakResult,找不到通知的玩家\n");
		return false;
	}

	GCChatGroupSpeakResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.ret = pRelationMsg->returnCode;
	resp.chatGroupID = 0;

	NewSendPlayerPkg( GCChatGroupSpeakResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChatGroupSpeakResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;

}

bool CDealRelationSrvPkg::OnChatGroupSpeakNotify(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnChatGroupSpeakNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( ChatGroupSpeekNotify, pRelationMsg,pPkg,wPkgLen );

	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnChatGroupSpeakNotify 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnChatGroupSpeakNotify,找不到通知的玩家\n");
		return false;
	}

	GCChatGroupSpeakNotify notify;
	StructMemSet(notify, 0x00, sizeof(notify));

	notify.chatGroupID = pRelationMsg->groupId;
	StructMemCpy( notify.chatData, pRelationMsg->data.data,sizeof(notify.chatData) );
	if (sizeof(notify.chatData) > pRelationMsg->data.dataLen)
	{
		notify.chatDataSize = pRelationMsg->data.dataLen;
	} else {
		notify.chatDataSize = sizeof(notify.chatData);
	}

	StructMemCpy( notify.speakerName, pRelationMsg->talker.memberInd, sizeof(notify.speakerName) );
	if ( sizeof(notify.speakerName) > pRelationMsg->talker.memberIndLen )
	{
		notify.speakerNameSize = pRelationMsg->talker.memberIndLen;
	} else {
		notify.speakerNameSize = sizeof(notify.speakerName);
	}

	NewSendPlayerPkg( GCChatGroupSpeakNotify, pNotifyPlayer, notify );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChatGroupSpeakNotify, pNotifyPlayer, notify );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}


bool CDealRelationSrvPkg::OnTeamMemberCountRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnTeamMemberCountRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( QueryTeamMembersCountResult, pRelationMsg, pPkg,wPkgLen );

	return true;
	TRY_END;
	return false;
}

void CDealRelationSrvPkg::ConvertRTeamInfoToCliTeamInfo( const Team* pSrvTeam,  GCTeamInfo* pCliTeam )
{
	TRY_BEGIN;

	if ( (NULL==pSrvTeam ) || (NULL==pCliTeam) )
	{
		D_WARNING( "ConvertRTeamInfoToCliTeamInfo，输入指针空\n" );
		return;
	}

	StructMemSet( *pCliTeam, 0, sizeof( GCTeamInfo ) );
	pCliTeam->leaderPlayerID.dwPID = pSrvTeam->captainPlayerId;
	pCliTeam->leaderPlayerID.wGID = pSrvTeam->captainGateId;
	pCliTeam->teamSize = pSrvTeam->teamMembers.membersSize;  //队伍的大小
	pCliTeam->ulTeamID = ConvertRSrvTeamIndToTeamID( pSrvTeam->teamInd );				//队伍的ID
	pCliTeam->ucExpMode = (TYPE_UI8)pSrvTeam->expSharedMode;
	pCliTeam->ucItemMode = (TYPE_UI8)pSrvTeam->goodSharedMode;
	pCliTeam->ucRollItemMode = (TYPE_UI8)pSrvTeam->rollItemLevel;

	for(unsigned int i=0; i<pSrvTeam->teamMembers.membersSize; ++i)
	{
		if ( ( i >= ARRAY_SIZE(pCliTeam->membersInfo) )
			|| ( i >= ARRAY_SIZE(pSrvTeam->teamMembers.members) )
			)
		{
			D_ERROR( "ConvertRTeamInfoToCliTeamInfo, i(%d), pCliTeam->membersInfo或pSrvTeam->teamMembers.members越界\n", i );
			return;
		}
		pCliTeam->membersInfo[i].portraitID = pSrvTeam->teamMembers.members[i].avatarId;    //头像
		SafeStrCpy( pCliTeam->membersInfo[i].playerName,  pSrvTeam->teamMembers.members[i].memberName.memberName );  //姓名
		pCliTeam->membersInfo[i].nameSize = (UINT)strlen(  pCliTeam->membersInfo[i].playerName ) + 1;  //名字长度
		pCliTeam->membersInfo[i].ucClass = pSrvTeam->teamMembers.members[i].memberJob;	//职业
		pCliTeam->membersInfo[i].playerID.dwPID = pSrvTeam->teamMembers.members[i].playerId;	//玩家的PLAYER
		pCliTeam->membersInfo[i].playerID.wGID = pSrvTeam->teamMembers.members[i].gateId;		//gate的ID
		pCliTeam->membersInfo[i].shFlag = ( pSrvTeam->teamMembers.members[i].memberSex == SEX_MALE ) ? SEX_MALE : SEX_FEMALE;		//性别
		pCliTeam->membersInfo[i].ucRace = (TYPE_UI8)pSrvTeam->teamMembers.members[i].memberLevel;	//等级
	}	

	return;
	TRY_END;
	return;
}

void CDealRelationSrvPkg::ConvertRTeamInfoToMapTeamInfo(const Team* pSrvTeam,  GMTeamDetialInfo* pMapTeam)
{
	TRY_BEGIN;

	if ( (NULL==pSrvTeam ) || (NULL==pMapTeam) )
	{
		D_WARNING( "ConvertRTeamInfoToMapTeamInfo，输入指针空\n" );
		return;
	}

	StructMemSet( *pMapTeam, 0, sizeof( GMTeamDetialInfo ) );
	pMapTeam->ArrSize = pSrvTeam->teamMembers.membersSize;
	pMapTeam->expShareMode = (EXPERIENCE_SHAREMODE)pSrvTeam->expSharedMode;
	pMapTeam->itemShareMode = (ITEM_SHAREMODE)pSrvTeam->goodSharedMode;
	pMapTeam->rollItemMode = (ROLLITEM_MODE)pSrvTeam->rollItemLevel;
	pMapTeam->TeamID = ConvertRSrvTeamIndToTeamID( pSrvTeam->teamInd );
	for(unsigned int i=0; i<pSrvTeam->teamMembers.membersSize; ++i)
	{
		if ( ( i >= ARRAY_SIZE(pMapTeam->playerArr) )
			|| ( i >= ARRAY_SIZE(pSrvTeam->teamMembers.members) )
			|| ( i >= ARRAY_SIZE(pMapTeam->memberSeqArr) )
			)
		{
			D_ERROR( "CDealRelationSrvPkg::ConvertRTeamInfoToMapTeamInfo, i(%d), pMapTeam->playerArr或pSrvTeam->teamMembers.members或pMapTeam->memberSeqArr越界\n", i );
			return;
		}
		pMapTeam->playerArr[i].dwPID = pSrvTeam->teamMembers.members[i].playerId;	//玩家的PLAYER
		pMapTeam->playerArr[i].wGID = pSrvTeam->teamMembers.members[i].gateId;		
		pMapTeam->memberSeqArr[i] = pSrvTeam->teamMembers.members[i].memberSeq;
	}

	return;
	TRY_END;
	return;
}


TYPE_ID CDealRelationSrvPkg::ConvertRSrvTeamIndToTeamID( const TeamInd& teamInd )
{
	TYPE_ID teamID;
	StructMemSet( teamID, 0, sizeof(teamID) );

	TRY_BEGIN;

	char tmpBuff[MAX_TEAM_IND_LEN];
	SafeStrCpy( tmpBuff, teamInd.teamInd );
	tmpBuff[teamInd.teamIndLen] = 0;
	teamID = atoi( tmpBuff );

	return teamID;
	TRY_END;
	return teamID;
}

TeamInd CDealRelationSrvPkg::ConvertTeamIDToRSrvTeamInd( const TYPE_ID teamID )
{
	TeamInd teamInd;
	StructMemSet( teamInd, 0, sizeof(teamInd) );

	TRY_BEGIN;

	stringstream strBuff;
	strBuff << teamID;
	SafeStrCpy( teamInd.teamInd, strBuff.str().c_str() );
	teamInd.teamIndLen = (USHORT)strlen( teamInd.teamInd );

	return teamInd;
	TRY_END;
	return teamInd;
}


void CDealRelationSrvPkg::ConvertRFriendInfoToCliFriendInfo(MUX_PROTO::FriendInfo& fInfo, RelationServiceNS::FriendInfo& srvInfo )
{
	TRY_BEGIN;

	RelationServiceNS::FriendInfo& rInfo =  srvInfo;
	fInfo.friendID = rInfo.uiID;
	fInfo.groupID  = rInfo.GroupID;
	fInfo.mapID    = rInfo.MapID;
	fInfo.playerClass = rInfo.playerClass;
	fInfo.playerLevel = rInfo.playerLevel;
	fInfo.playerRace  = rInfo.playerRace;
	fInfo.playerIcon  = rInfo.playerIcon;
	fInfo.isHateMe  = rInfo.Flag  & 0x01 ? true:false;
	fInfo.playerSex = (rInfo.Flag & 0x02)? SEX_MALE :SEX_FEMALE;
	fInfo.isOnline  = rInfo.EnterModel;
	fInfo.GuildNameLen = rInfo.GuildNameLen;
	fInfo.nickNameSize = rInfo.NickNameLen;
	fInfo.FereNameLen  = rInfo.FereNameLen;
	SafeStrCpy( fInfo.GuildName,  rInfo.GuildName );
	SafeStrCpy( fInfo.nickName,   rInfo.NickName  );
	SafeStrCpy( fInfo.FereName,   rInfo.FereName  );
	fInfo.degree = rInfo.degree;
	fInfo.tweetsFlag = rInfo.tweetsFlag;

	TRY_END;
}

void CDealRelationSrvPkg::ConvertRFoeInfoToCliFoeInfo(MUX_PROTO::FoeInfo& foeInfo ,RelationServiceNS::EnemylistItem& enemyInfo )
{
	TRY_BEGIN;

	foeInfo.foeID    = enemyInfo.uiID;
	foeInfo.mapID    = enemyInfo.MapID;
	foeInfo.isOnline = enemyInfo.Online;
	foeInfo.nickNameSize = enemyInfo.NameLen;
	SafeStrCpy( foeInfo.nickName, enemyInfo.Name );
	foeInfo.GuildNameLen = enemyInfo.GuildNameLen;
	SafeStrCpy( foeInfo.GuildName, enemyInfo.GuildName );
	foeInfo.FereNameLen  = enemyInfo.FereNameLen;
	SafeStrCpy( foeInfo.FereName, enemyInfo.FereName );
	foeInfo.playerClass = enemyInfo.playerClass;
	foeInfo.playerLevel = enemyInfo.playerLevel;
	foeInfo.playerRace  = enemyInfo.playerRace;
	foeInfo.playerIcon  = enemyInfo.playerIcon;
	foeInfo.playerSex   =  ( enemyInfo.Sex & 0x01 )?SEX_MALE:SEX_FEMALE;
	foeInfo.isLocked    =  ( enemyInfo.IsLock == 0x01 ) ? true : false;
	foeInfo.playerID.wGID  = enemyInfo.player.GateSrvID;
	foeInfo.playerID.dwPID = enemyInfo.player.PPlayer;
	foeInfo.incidentMap = enemyInfo.incidentMap;
	foeInfo.incidentPosX = enemyInfo.incidentPosX;
	foeInfo.incidentPosY = enemyInfo.incidentPosY;
	foeInfo.bKilled = enemyInfo.bKilled;
	foeInfo.incidentTime = enemyInfo.incidentTime;

	TRY_END;
}

bool CDealRelationSrvPkg::OnCreateUnionRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnCreateUnionRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( CreateUnionResult, pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnCreateUnionRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	/*if( NULL == pNotifyPlayer )
	{
	D_ERROR("OnCreateUnionRst,找不到通知的玩家\n");
	return false;
	}*/

	//如果成功，注册到mapserver
	if(pRelationMsg->returnCode == R_G_RESULT_UNION_SUCCEED)
	{
		if(NULL != pNotifyPlayer)
		{
			//在gateserver注册
			pNotifyPlayer->SetUnionID(pRelationMsg->_union.id);

			//在maperver上注册
			GMRegisterUnionMember registerMember;
			StructMemSet(registerMember, 0x00, sizeof(registerMember));

			registerMember.playerId = pNotifyPlayer->GetPlayerID();
			registerMember.unionID = pRelationMsg->_union.id;
			SafeStrCpy(registerMember.unionName, pRelationMsg->_union.name);
			registerMember.ownerUiid = pRelationMsg->_union.ownerUiid;
			registerMember.unionLevel = pRelationMsg->_union.level;
			registerMember.unionActive = pRelationMsg->_union.active;
			registerMember.registerType = GMRegisterUnionMember::CREATE_UNION;

			SendMsgToMapSrvByPlayer(GMRegisterUnionMember, pNotifyPlayer, registerMember);

			//工会成员增加日至
			CLogManager::DoRoleUnionChanage(pNotifyPlayer, pRelationMsg->_union.id, (unsigned char)LOG_SVC_NS::RCT_ADD);
		}
		else
		{
		}
	}
	else
	{
		if(NULL != pNotifyPlayer)
		{
			GMCreateUnionErrNtf errNtf;
			StructMemSet(errNtf, 0x00, sizeof(errNtf));
			errNtf.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
			SafeStrCpy(errNtf.recvPlayerName, pRelationMsg->captionName);

			SendMsgToMapSrvByPlayer(GMCreateUnionErrNtf, pNotifyPlayer, errNtf);
		}
		else
		{
			GFSendSysMailToPlayer sysMail;
			StructMemSet(sysMail, 0x00, sizeof(sysMail));

			SafeStrCpy(sysMail.recvPlayerName, pRelationMsg->captionName);
			SafeStrCpy(sysMail.sendMailTitle, "创建工会错误");
			SafeStrCpy(sysMail.sendMailContent, "");
			sysMail.attachItem[0].usItemTypeID = s_unionCostArg.itemID;
			sysMail.attachItem[0].ucCount = s_unionCostArg.itemCount;
			sysMail.silverMoney = s_unionCostArg.money;//双币种判断(创建时用银币，撤销时用金币?)；
			sysMail.goldMoney = 0;

			SendMsgToFunctionSrv( GFSendSysMailToPlayer, sysMail );
		}
	}

	GCCreateUnionResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	//防止union、member结构和relation的不一致造成整体拷贝出错
	resp.returnCode = pRelationMsg->returnCode;

	resp._union.id = pRelationMsg->_union.id;
	resp._union.active = pRelationMsg->_union.active;
	resp._union.level = pRelationMsg->_union.level;
	resp._union.num = pRelationMsg->_union.num;
	resp._union.ownerUiid = pRelationMsg->_union.ownerUiid;

	if(pRelationMsg->_union.nameLen < sizeof(resp._union.name))
	{
		resp._union.nameLen = pRelationMsg->_union.nameLen;
		SafeStrCpy( resp._union.name, pRelationMsg->_union.name );//, pRelationMsg->_union.nameLen);
	} else {
		resp._union.nameLen = sizeof(resp._union.name);
		SafeStrCpy( resp._union.name, pRelationMsg->_union.name );//, sizeof(resp._union.name));
	}

	if(pRelationMsg->_union.titleLen < sizeof(resp._union.title))
	{
		resp._union.titleLen = pRelationMsg->_union.titleLen;
		SafeStrCpy( resp._union.title, pRelationMsg->_union.title );//, pRelationMsg->_union.titleLen);
	} else {
		resp._union.titleLen = sizeof(resp._union.title);
		SafeStrCpy(resp._union.title, pRelationMsg->_union.title );//, sizeof(resp._union.title));
	}

	if ( pRelationMsg->_union.posListLen <= ARRAY_SIZE(resp._union.posList) ) //sizeof(resp._union.posList)/sizeof(resp._union.posList[0]))
	{
		resp._union.posListLen = pRelationMsg->_union.posListLen;
		for(int i = 0; i < pRelationMsg->_union.posListLen; i++)
		{
			if ( ( i >= ARRAY_SIZE(resp._union.posList) )
				|| ( i >= ARRAY_SIZE(pRelationMsg->_union.posList) )
				)
			{
				D_ERROR( "CDealRelationSrvPkg::OnCreateUnionRst, i(%d), resp._union.posList或pRelationMsg->_union.posList越界\n", i );
				return false;
			}
			resp._union.posList[i].seq = pRelationMsg->_union.posList[i].seq;
			resp._union.posList[i].right = pRelationMsg->_union.posList[i].right;
			if(pRelationMsg->_union.posList[i].posLen <= resp._union.posList[i].posLen)
			{
				resp._union.posList[i].posLen = pRelationMsg->_union.posList[i].posLen;
				StructMemCpy(resp._union.posList[i].pos, pRelationMsg->_union.posList[i].pos, pRelationMsg->_union.posList[i].posLen);
			}
		}
	}

	NewSendPlayerPkg( GCCreateUnionResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCCreateUnionResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnDestroyUnionRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnDestroyUnionRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( DestroyUnionResult, pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnDestroyUnionRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnDestroyUnionRst,找不到通知的玩家\n");
		return false;
	}

	GCDestroyUnionResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCDestroyUnionResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCDestroyUnionResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnDestroyUnionNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnDestroyUnionNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( UnionDestroyedNotify, pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnDestroyUnionNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnDestroyUnionNtf,找不到通知的玩家\n");
		return false;
	}

	GCUnionDestroyedNotify resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	NewSendPlayerPkg( GCUnionDestroyedNotify, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUnionDestroyedNotify, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnQueryUnionBasicInfo( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnQueryUnionBasicInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( QueryUnionBasicResult, pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnQueryUnionBasicInfo 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnQueryUnionBasicInfo,找不到通知的玩家\n");
		return false;
	}

	if(pRelationMsg->bGate)//到mapserver注册
	{
		if(RESULT_PLAYER_NO_UNION != pRelationMsg->returnCode)
		{
			//在gateserver注册
			pNotifyPlayer->SetUnionID(pRelationMsg->_union.id);

			GMRegisterUnionMember registerMember;
			StructMemSet(registerMember, 0x00, sizeof(registerMember));

			registerMember.playerId = pNotifyPlayer->GetPlayerID();
			registerMember.unionID = pRelationMsg->_union.id;
			SafeStrCpy(registerMember.unionName, pRelationMsg->_union.name);
			registerMember.ownerUiid = pRelationMsg->_union.ownerUiid;
			registerMember.unionLevel = pRelationMsg->_union.level;
			registerMember.unionActive = pRelationMsg->_union.active;

			SendMsgToMapSrvByPlayer(GMRegisterUnionMember, pNotifyPlayer, registerMember);

			//工会成员增加
			CLogManager::DoRoleUnionChanage(pNotifyPlayer, pRelationMsg->_union.id, (unsigned char)LOG_SVC_NS::RCT_ADD);
		}
	}

	GCQueryUnionBasicResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	//防止union、member结构和relation的不一致造成整体拷贝出错
	resp.returnCode = pRelationMsg->returnCode;

	resp._union.id = pRelationMsg->_union.id;
	resp._union.active = pRelationMsg->_union.active;
	resp._union.level = pRelationMsg->_union.level;
	resp._union.num = pRelationMsg->_union.num;
	resp._union.ownerUiid = pRelationMsg->_union.ownerUiid;

	if(pRelationMsg->_union.nameLen < sizeof(resp._union.name))
	{
		resp._union.nameLen = pRelationMsg->_union.nameLen;
		SafeStrCpy(resp._union.name, pRelationMsg->_union.name);//, pRelationMsg->_union.nameLen);
	} else {
		resp._union.nameLen = sizeof(resp._union.name);
		SafeStrCpy(resp._union.name, pRelationMsg->_union.name);//, sizeof(resp._union.name));
	}

	if(pRelationMsg->_union.titleLen < sizeof(resp._union.title))
	{
		resp._union.titleLen = pRelationMsg->_union.titleLen;
		SafeStrCpy( resp._union.title, pRelationMsg->_union.title );//, pRelationMsg->_union.titleLen);
	} else {
		resp._union.titleLen = sizeof(resp._union.title);
		SafeStrCpy( resp._union.title, pRelationMsg->_union.title );//, sizeof(resp._union.title));
	}

	//resp._union.owner
	resp.returnCode = pRelationMsg->returnCode;

	if ( pRelationMsg->_union.posListLen <= ARRAY_SIZE(resp._union.posList) )
	{
		resp._union.posListLen = pRelationMsg->_union.posListLen;
		for(int i = 0; i < pRelationMsg->_union.posListLen; i++)
		{
			if ( ( i >= ARRAY_SIZE(resp._union.posList) )
				|| ( i >= ARRAY_SIZE(pRelationMsg->_union.posList) )
				)
			{
				D_ERROR( "OnQueryUnionBasicInfo, i(%d), resp._union.posList或pRelationMsg->_union.posList越界\n", i );
				return false;
			}
			resp._union.posList[i].seq = pRelationMsg->_union.posList[i].seq;
			resp._union.posList[i].right = pRelationMsg->_union.posList[i].right;
			if(pRelationMsg->_union.posList[i].posLen <= sizeof(resp._union.posList[i].pos))
			{
				resp._union.posList[i].posLen = pRelationMsg->_union.posList[i].posLen;
				StructMemCpy( resp._union.posList[i].pos, pRelationMsg->_union.posList[i].pos, pRelationMsg->_union.posList[i].posLen );
			}
		}
	}

	NewSendPlayerPkg( GCQueryUnionBasicResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCQueryUnionBasicResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnQueryUnionMemberInfo( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnQueryUnionMemberInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( QueryUnionMembersResult, pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnQueryUnionMemberInfo 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnQueryUnionMemberInfo,找不到通知的玩家\n");
		return false;
	}

	GCQueryUnionMembersResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	unsigned int count = 0;
	if(pRelationMsg->members.count > 0)
	{
		for(count = 0; count < pRelationMsg->members.count; count++)
		{
			if ( ( count >= ARRAY_SIZE(resp.members.members) )
				|| ( count >= ARRAY_SIZE(pRelationMsg->members.members) )
				)
			{
				D_ERROR( "OnQueryUnionMemberInfo, count(%d), resp.members.members或pRelationMsg->members.members越界\n", count );
				break;
			}

			resp.members.members[count].gateId = pRelationMsg->members.members[count].gateId;
			resp.members.members[count].playerId = pRelationMsg->members.members[count].playerId;
			resp.members.members[count].uiid = pRelationMsg->members.members[count].uiid;
			resp.members.members[count].nameLen = pRelationMsg->members.members[count].nameLen;
			resp.members.members[count].job = pRelationMsg->members.members[count].job;
			resp.members.members[count].posSeq = pRelationMsg->members.members[count].posSeq;
			resp.members.members[count].level = pRelationMsg->members.members[count].level;
			resp.members.members[count].lastQuit = pRelationMsg->members.members[count].lastQuit;
			resp.members.members[count].forbid = pRelationMsg->members.members[count].forbid;
			resp.members.members[count].onLine = pRelationMsg->members.members[count].onLine;

			if(pRelationMsg->members.members[count].nameLen < sizeof(resp.members.members[count].name))
			{
				resp.members.members[count].nameLen = pRelationMsg->members.members[count].nameLen;
				SafeStrCpy( resp.members.members[count].name, pRelationMsg->members.members[count].name );
			} else {
				D_ERROR("OnQueryUnionMemberInfo,resp.members.members[count]传来的名字长度太大\n", pRelationMsg->members.members[count].nameLen );
			}

			if(pRelationMsg->members.members[count].titleLen < sizeof(resp.members.members[count].title))
			{
				resp.members.members[count].titleLen = pRelationMsg->members.members[count].titleLen;
				SafeStrCpy( resp.members.members[count].title, pRelationMsg->members.members[count].title );
			} else {
				D_ERROR("OnQueryUnionMemberInfo,resp.members.members[count]传来的title长度太大\n", pRelationMsg->members.members[count].titleLen );
			}

		}
	}

	resp.returnCode = pRelationMsg->returnCode;
	resp.members.count = count;

	NewSendPlayerPkg( GCQueryUnionMembersResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCQueryUnionMembersResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnAddUnionMember( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddUnionMember,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( AddUnionMemberConfirm, pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnAddUnionMember 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAddUnionMember,找不到通知的玩家\n");
		return false;
	}

	GCAddUnionMemberConfirm resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.id = 0;
	if(pRelationMsg->sponsorLen < sizeof(resp.name))
	{
		resp.nameLen = pRelationMsg->sponsorLen;
		SafeStrCpy( resp.name, pRelationMsg->sponsor );//, pRelationMsg->sponsorLen );
	} else {
		resp.nameLen = sizeof(resp.name);
		SafeStrCpy( resp.name, pRelationMsg->sponsor );//, sizeof(resp.name));
	}

	if(pRelationMsg->unionNameLen < sizeof(resp.unionName))
	{
		resp.unionNameLen = pRelationMsg->unionNameLen;
		SafeStrCpy( resp.unionName, pRelationMsg->unionName );//, pRelationMsg->unionNameLen);
	} else {
		resp.unionNameLen = sizeof(resp.unionName);
		SafeStrCpy( resp.unionName, pRelationMsg->unionName );//, sizeof(resp.unionName));
	}

	NewSendPlayerPkg( GCAddUnionMemberConfirm, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddUnionMemberConfirm, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnAddUnionMemberRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddUnionMemberRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( AddUnionMemberResult, pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnAddUnionMemberRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAddUnionMemberRst,找不到通知的玩家\n");
		return false;
	}

	GCAddUnionMemberResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;
	if(pRelationMsg->memberAddedNameLen < sizeof(resp.name))
	{
		resp.nameLen = pRelationMsg->memberAddedNameLen;
		SafeStrCpy(resp.name, pRelationMsg->memberAddedName);//, pRelationMsg->memberAddedNameLen);
	} else {
		resp.nameLen = sizeof(resp.name);
		SafeStrCpy(resp.name, pRelationMsg->memberAddedName);//, sizeof(resp.name));
	}

	NewSendPlayerPkg( GCAddUnionMemberResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddUnionMemberResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnAddUnionMemberNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddUnionMemberNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(AddUnionMemberNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnAddUnionMemberNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAddUnionMemberNtf,找不到通知的玩家\n");
		return false;
	}

	GCAddUnionMemberNotify resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.memberAdded.gateId = pRelationMsg->memberAdded.gateId;
	resp.memberAdded.playerId = pRelationMsg->memberAdded.playerId;
	resp.memberAdded.uiid = pRelationMsg->memberAdded.uiid;
	resp.memberAdded.nameLen = pRelationMsg->memberAdded.nameLen;
	resp.memberAdded.job = pRelationMsg->memberAdded.job;
	resp.memberAdded.posSeq = pRelationMsg->memberAdded.posSeq;
	resp.memberAdded.level = pRelationMsg->memberAdded.level;
	resp.memberAdded.lastQuit = pRelationMsg->memberAdded.lastQuit;
	resp.memberAdded.forbid = pRelationMsg->memberAdded.forbid;
	resp.memberAdded.onLine = pRelationMsg->memberAdded.onLine;

	if(pRelationMsg->memberAdded.nameLen < sizeof(resp.memberAdded.name))
	{
		resp.memberAdded.nameLen = pRelationMsg->memberAdded.nameLen;
		SafeStrCpy(resp.memberAdded.name, pRelationMsg->memberAdded.name);//, pRelationMsg->memberAdded.nameLen);
	} else {
		resp.memberAdded.nameLen = sizeof(resp.memberAdded.name);
		SafeStrCpy(resp.memberAdded.name, pRelationMsg->memberAdded.name);//, sizeof(resp.memberAdded.name));
	}

	if(pRelationMsg->memberAdded.titleLen < sizeof(resp.memberAdded.title))
	{
		resp.memberAdded.titleLen = pRelationMsg->memberAdded.titleLen;
		SafeStrCpy(resp.memberAdded.title, pRelationMsg->memberAdded.title);//, pRelationMsg->memberAdded.titleLen);
	} else {
		resp.memberAdded.titleLen = sizeof(resp.memberAdded.title);
		SafeStrCpy(resp.memberAdded.title, pRelationMsg->memberAdded.title);//, sizeof(resp.memberAdded.title));
	}

	/*if(pRelationMsg->memberAdded.posLen < sizeof(resp.memberAdded.pos))
	{
	resp.memberAdded.posLen = pRelationMsg->memberAdded.posLen;
	StructMemCpy(resp.memberAdded.pos, pRelationMsg->memberAdded.pos, pRelationMsg->memberAdded.posLen);
	}
	else
	{
	resp.memberAdded.posLen = sizeof(resp.memberAdded.pos);
	StructMemCpy(resp.memberAdded.pos, pRelationMsg->memberAdded.pos, sizeof(resp.memberAdded.pos));
	}*/

	NewSendPlayerPkg( GCAddUnionMemberNotify, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddUnionMemberNotify, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnRemoveUnionMemberRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRemoveUnionMemberRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(RemoveUnionMemberResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRemoveUnionMemberRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRemoveUnionMemberRst,找不到通知的玩家\n");
		return false;
	}

	if(pRelationMsg->returnCode == R_G_RESULT_UNION_SUCCEED)//通知mapserver解除注册
	{
		//取消在gateserver的注册
		pNotifyPlayer->SetUnionID(0);

		GMUnregisterUnionMember unregisterMember;
		StructMemSet(unregisterMember, 0x00, sizeof(unregisterMember));

		unregisterMember.playerId = pNotifyPlayer->GetPlayerID();

		SendMsgToMapSrvByPlayer(GMUnregisterUnionMember, pNotifyPlayer, unregisterMember);

		//记录工会日至
		CLogManager::DoRoleUnionChanage(pNotifyPlayer, 0, (unsigned char)LOG_SVC_NS::RCT_DEL);
	}

	GCRemoveUnionMemberResult resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;
	resp.memberRemoved = pRelationMsg->memberRemoved.uiid;

	NewSendPlayerPkg( GCRemoveUnionMemberResult, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCRemoveUnionMemberResult, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnRemoveUnionMemberNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRemoveUnionMemberNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(RemoveUnionMemberNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnRemoveUnionMemberNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRemoveUnionMemberNtf,找不到通知的玩家\n");
		return false;
	}

	GCRemoveUnionMemberNotify resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.flag = pRelationMsg->flag;
	resp.memberRemoved = pRelationMsg->memberRemoved.uiid;

	NewSendPlayerPkg( GCRemoveUnionMemberNotify, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCRemoveUnionMemberNotify, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

//// 工会职位对应权限修改通知
//bool CDealRelationSrvPkg::OnUninPosRightsNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	/*if ( NULL == pOwner )
//	{
//		D_ERROR( "OnUninPosRightsNtf,pOwner空\n" );
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	DealRelationPkgPre(UnionPosRightsNotify , pRelationMsg,pPkg,wPkgLen );
//	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
//	{
//		D_ERROR("CDealRelationSrvPkg::OnUninPosRightsNtf 发来的gateID 不对\n");
//		return false;
//	}
//
//	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
//	if( NULL == pNotifyPlayer )
//	{
//		D_ERROR("OnUninPosRightsNtf,找不到通知的玩家\n");
//		return false;
//	}
//
//	GCUnionPosRightsNtf resp;
//	StructMemSet(resp, 0x00, sizeof(resp));
//
//	resp.flag = pRelationMsg->flag;
//	resp.rights = pRelationMsg->rights;
//	if(pRelationMsg->posLen < sizeof(resp.pos))
//	{
//		resp.posLen = pRelationMsg->posLen;
//		memcpy(resp.pos, pRelationMsg->pos, pRelationMsg->posLen);
//	}
//	else
//	{
//		resp.posLen = sizeof(resp.pos);
//		memcpy(resp.pos, pRelationMsg->pos, sizeof(resp.pos));
//	}
//
//	NewSendPlayerPkg( GCUnionPosRightsNtf, pNotifyPlayer, resp );
//	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUnionPosRightsNtf, pNotifyPlayer, resp );
//	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);
//
//	return true;
//
//	TRY_END;*/
//	return false;
//}

//// 工会职位对应权限修改结果
//bool CDealRelationSrvPkg::OnModifyUnionPosRightRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	/*if ( NULL == pOwner )
//	{
//		D_ERROR( "OnModifyUnionPosRightRst,pOwner空\n" );
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	DealRelationPkgPre(ModifyUnionPosRighstResult , pRelationMsg,pPkg,wPkgLen );
//	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
//	{
//		D_ERROR("CDealRelationSrvPkg::OnModifyUnionPosRightRst 发来的gateID 不对\n");
//		return false;
//	}
//
//	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
//	if( NULL == pNotifyPlayer )
//	{
//		D_ERROR("OnModifyUnionPosRightRst,找不到通知的玩家\n");
//		return false;
//	}
//
//	GCModifyUnionPosRighstRst resp;
//	StructMemSet(resp, 0x00, sizeof(resp));
//
//	resp.returnCode = pRelationMsg->returnCode;
//	if(pRelationMsg->posLen < sizeof(resp.pos))
//	{
//		resp.posLen = pRelationMsg->posLen;
//		memcpy(resp.pos, pRelationMsg->pos, pRelationMsg->posLen);
//	}
//	else
//	{
//		resp.posLen = sizeof(resp.pos);
//		memcpy(resp.pos, pRelationMsg->pos, sizeof(resp.pos));
//	}
//
//	NewSendPlayerPkg( GCModifyUnionPosRighstRst, pNotifyPlayer, resp );
//	//MsgToPut* pNewMsg = CreatePlayerPkg( GCModifyUnionPosRighstRst, pNotifyPlayer, resp );
//	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);
//
//	return true;
//
//	TRY_END;*/
//	return false;
//}

//// 工会职位增加结果
//bool CDealRelationSrvPkg::OnAddUnionPosRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	/*if ( NULL == pOwner )
//	{
//		D_ERROR( "OnAddUnionPosRst,pOwner空\n" );
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	DealRelationPkgPre(AddUnionPosResult , pRelationMsg,pPkg,wPkgLen );
//	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
//	{
//		D_ERROR("CDealRelationSrvPkg::OnAddUnionPosRst 发来的gateID 不对\n");
//		return false;
//	}
//
//	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
//	if( NULL == pNotifyPlayer )
//	{
//		D_ERROR("OnAddUnionPosRst,找不到通知的玩家\n");
//		return false;
//	}
//
//	GCAddUnionPosRst resp;
//	StructMemSet(resp, 0x00, sizeof(resp));
//
//	resp.returnCode = pRelationMsg->returnCode;
//	if(pRelationMsg->posLen < sizeof(resp.pos))
//	{
//		resp.posLen = pRelationMsg->posLen;
//		memcpy(resp.pos, pRelationMsg->pos, pRelationMsg->posLen);
//	}
//	else
//	{
//		resp.posLen = sizeof(resp.pos);
//		memcpy(resp.pos, pRelationMsg->pos, sizeof(resp.pos));
//	}
//
//	NewSendPlayerPkg( GCAddUnionPosRst, pNotifyPlayer, resp );
//	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddUnionPosRst, pNotifyPlayer, resp );
//	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);
//
//	return true;
//
//	TRY_END;*/
//	return false;
//}

//// 工会职位减少结果
//bool CDealRelationSrvPkg::OnRemoveUnionPosRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	/*if ( NULL == pOwner )
//	{
//		D_ERROR( "OnRemoveUnionPosRst,pOwner空\n" );
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	DealRelationPkgPre(RemoveUnionPosResult , pRelationMsg,pPkg,wPkgLen );
//	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
//	{
//		D_ERROR("CDealRelationSrvPkg::OnRemoveUnionPosRst 发来的gateID 不对\n");
//		return false;
//	}
//
//	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
//	if( NULL == pNotifyPlayer )
//	{
//		D_ERROR("OnRemoveUnionPosRst,找不到通知的玩家\n");
//		return false;
//	}
//
//	GCRemoveUnionPosRst resp;
//	StructMemSet(resp, 0x00, sizeof(resp));
//
//	resp.returnCode = pRelationMsg->returnCode;
//	if(pRelationMsg->posLen < sizeof(resp.pos))
//	{
//		resp.posLen = pRelationMsg->posLen;
//		memcpy(resp.pos, pRelationMsg->pos, pRelationMsg->posLen);
//	}
//	else
//	{
//		resp.posLen = sizeof(resp.pos);
//		memcpy(resp.pos, pRelationMsg->pos, sizeof(resp.pos));
//	}
//
//	NewSendPlayerPkg( GCRemoveUnionPosRst, pNotifyPlayer, resp );
//	//MsgToPut* pNewMsg = CreatePlayerPkg( GCRemoveUnionPosRst, pNotifyPlayer, resp );
//	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);
//
//	return true;
//
//	TRY_END;*/
//	return false;
//}

// 转移公会会长职务结果
bool CDealRelationSrvPkg::OnTransformUnionCaptionRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnTransformUnionCaptionRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(TransformUnionCaptionResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnTransformUnionCaptionRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnTransformUnionCaptionRst,找不到通知的玩家\n");
		return false;
	}

	GCTransformUnionCaptionRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCTransformUnionCaptionRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCTransformUnionCaptionRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 转移公会会长职务通知
bool CDealRelationSrvPkg::OnTransformUninCaptionNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnTransformUninCaptionNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(TransformUnionCaptionNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnTransformUninCaptionNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnTransformUninCaptionNtf,找不到通知的玩家\n");
		return false;
	}

	GCTransformUnionCaptionNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	if(pRelationMsg->newCaptionNameLen < sizeof(resp.newCaptionName))
	{
		resp.newCaptionNameLen = pRelationMsg->newCaptionNameLen;
		SafeStrCpy(resp.newCaptionName, pRelationMsg->newCaptionName);//, pRelationMsg->newCaptionNameLen);
	}
	else
	{
		resp.newCaptionNameLen = sizeof(resp.newCaptionName);
		SafeStrCpy(resp.newCaptionName, pRelationMsg->newCaptionName);//, sizeof(resp.newCaptionName));
	}

	NewSendPlayerPkg( GCTransformUnionCaptionNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCTransformUnionCaptionNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 修改工会成员称号结果
bool CDealRelationSrvPkg::OnModifyUnionMemberTitleRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnModifyUnionMemberTitleRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(ModifyUnionMemberTitleResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyUnionMemberTitleRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnModifyUnionMemberTitleRst,找不到通知的玩家\n");
		return false;
	}

	GCModifyUnionMemberTitleRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCModifyUnionMemberTitleRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCModifyUnionMemberTitleRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 修改工会成员称号通知
bool CDealRelationSrvPkg::OnUnionMemberTitileNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionMemberTitileNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionMemberTitleNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionMemberTitileNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionMemberTitileNtf,找不到通知的玩家\n");
		return false;
	}

	GCUnionMemberTitleNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	if(pRelationMsg->memberNameLen < sizeof(resp.memberName))
	{
		resp.memberNameLen = pRelationMsg->memberNameLen;
		SafeStrCpy(resp.memberName, pRelationMsg->memberName);//, pRelationMsg->memberNameLen);
	} else {
		resp.memberNameLen = sizeof(resp.memberName);
		SafeStrCpy(resp.memberName, pRelationMsg->memberName);//, sizeof(resp.memberName));
	}

	if(pRelationMsg->titleLen < sizeof(resp.title))
	{
		resp.titleLen = pRelationMsg->titleLen;
		SafeStrCpy(resp.title, pRelationMsg->title);//, pRelationMsg->titleLen);
	} else {
		resp.titleLen = sizeof(resp.title);
		SafeStrCpy(resp.title, pRelationMsg->title);//, sizeof(resp.title));
	}

	NewSendPlayerPkg( GCUnionMemberTitleNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUnionMemberTitleNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 提升工会成员职位结果
bool CDealRelationSrvPkg::OnAdvanceUnionMemberPosRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAdvanceUnionMemberPosRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(AdvanceUnionMemberPosResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnAdvanceUnionMemberPosRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAdvanceUnionMemberPosRst,找不到通知的玩家\n");
		return false;
	}

	GCAdvanceUnionMemberPosRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCAdvanceUnionMemberPosRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAdvanceUnionMemberPosRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 提升工会成员职位通知
bool CDealRelationSrvPkg::OnAdvanceUnionMemerPosNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAdvanceUnionMemerPosNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(AdvanceUnionMemberPosNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnAdvanceUnionMemerPosNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAdvanceUnionMemerPosNtf,找不到通知的玩家\n");
		return false;
	}

	GCAdvanceUnionMemberPosNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.posSeq = pRelationMsg->posSeq;
	if(pRelationMsg->memberNameLen < sizeof(resp.memberName))
	{
		resp.memberNameLen = pRelationMsg->memberNameLen;
		SafeStrCpy( resp.memberName, pRelationMsg->memberName );//, pRelationMsg->memberNameLen);
	} else {
		resp.memberNameLen = sizeof(resp.memberName);
		SafeStrCpy( resp.memberName, pRelationMsg->memberName );//, sizeof(resp.memberName));
	}

	/*if(pRelationMsg->posLen < sizeof(resp.pos))
	{
	resp.posLen = pRelationMsg->posLen;
	StructMemCpy(resp.pos, pRelationMsg->pos, pRelationMsg->posLen);
	}
	else
	{
	resp.posLen = sizeof(resp.memberName);
	StructMemCpy(resp.pos, pRelationMsg->pos, sizeof(resp.pos));
	}*/

	NewSendPlayerPkg( GCAdvanceUnionMemberPosNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAdvanceUnionMemberPosNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 降低工会成员职位结果
bool CDealRelationSrvPkg::OnReduceUnionMemberPosRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnReduceUnionMemberPosRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(ReduceUnionMemberPosResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnReduceUnionMemberPosRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnReduceUnionMemberPosRst,找不到通知的玩家\n");
		return false;
	}

	GCReduceUnionMemberPosRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCReduceUnionMemberPosRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCReduceUnionMemberPosRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 降低工会成员职位通知
bool CDealRelationSrvPkg::OnReduceUnionMemberPosNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnReduceUnionMemberPosNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(ReduceUnionMemberPosNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnReduceUnionMemberPosNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnReduceUnionMemberPosNtf,找不到通知的玩家\n");
		return false;
	}

	GCReduceUnionMemberPosNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.posSeq = pRelationMsg->posSeq;
	if(pRelationMsg->memberNameLen < sizeof(resp.memberName))
	{
		resp.memberNameLen = pRelationMsg->memberNameLen;
		SafeStrCpy(resp.memberName, pRelationMsg->memberName);//, pRelationMsg->memberNameLen);
	} else {
		resp.memberNameLen = sizeof(resp.memberName);
		SafeStrCpy(resp.memberName, pRelationMsg->memberName);//, sizeof(resp.memberName));
	}

	/*if(pRelationMsg->posLen < sizeof(resp.pos))
	{
	resp.posLen = pRelationMsg->posLen;
	StructMemCpy(resp.pos, pRelationMsg->pos, pRelationMsg->posLen);
	}
	else
	{
	resp.posLen = sizeof(resp.memberName);
	StructMemCpy(resp.pos, pRelationMsg->pos, sizeof(resp.pos));
	}*/

	NewSendPlayerPkg( GCReduceUnionMemberPosNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCReduceUnionMemberPosNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 工会频道聊天结果
bool CDealRelationSrvPkg::OnUnionChannelSpeekRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionChannelSpeekRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionChannelSpeekResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionChannelSpeekRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionChannelSpeekRst,找不到通知的玩家\n");
		return false;
	}

	/*GCChat resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.nType = CT_UNION_CHAT;	
	const char *pRoleName = pNotifyPlayer->GetRoleName();
	size_t roleNameLen = strlen(pRoleName);
	resp.srcNameSize = (UINT)(roleNameLen <= sizeof(resp.srcName) ? roleNameLen : sizeof(resp.srcName));
	SafeStrCpy(resp.srcName, pRoleName);//, resp.srcNameSize);

	NewSendPlayerPkg( GCChat, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChat, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);*/

	return true;

	TRY_END;
	return false;
}

// 工会频道聊天通知
bool CDealRelationSrvPkg::OnUnionChannelSpeekNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionChannelSpeekNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionChannelSpeekNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionChannelSpeekNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionChannelSpeekNtf,找不到通知的玩家\n");
		return false;
	}

	GCChat resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.chatType = pRelationMsg->speekerLine == 2 ? CT_UNION_IRC_CHAT : CT_UNION_CHAT;	
	resp.extInfo = 0;//无额外信息；
	resp.srcNameSize = (pRelationMsg->speekerNameLen <= sizeof(resp.srcName)) ? pRelationMsg->speekerNameLen : sizeof(resp.srcName);
	SafeStrCpy( resp.srcName, pRelationMsg->speekerName );//, resp.srcNameSize);
	resp.chatSize = (UINT)(pRelationMsg->contentLen <= sizeof(resp.strChat) ? pRelationMsg->contentLen : sizeof(resp.strChat));
	SafeStrCpy( resp.strChat, pRelationMsg->content );//, resp.chatSize);

	NewSendPlayerPkg( GCChat, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChat, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 更新工会职务管理结果
bool CDealRelationSrvPkg::OnUpdateUnionPosCfgRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUpdateUnionPosCfgRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UpdateUnionPosCfgResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUpdateUnionPosCfgRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUpdateUnionPosCfgRst,找不到通知的玩家\n");
		return false;
	}

	GCUpdateUnionPosCfgRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCUpdateUnionPosCfgRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUpdateUnionPosCfgRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 更新工会职务管理通知
bool CDealRelationSrvPkg::OnUpdateUnionPosCfgNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUpdateUnionPosCfgNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UpdateUnionPosCfgNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUpdateUnionPosCfgNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUpdateUnionPosCfgNtf,找不到通知的玩家\n");
		return false;
	}

	GCUpdateUnionPosCfgNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	if(0 == pRelationMsg->posListLen)
	{
		resp.posListLen = 0;
		NewSendPlayerPkg( GCUpdateUnionPosCfgNtf, pNotifyPlayer, resp );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCUpdateUnionPosCfgNtf, pNotifyPlayer, resp );
	}
	else
	{
		int totalListSize = pRelationMsg->posListLen;
		int maxSegmentSize = ARRAY_SIZE(resp.posList);//sizeof(resp.posList)/sizeof(resp.posList[0]);

		int count = 0;
		for ( int i = 0; i < totalListSize; i++ )
		{
			if ( ( count >= ARRAY_SIZE(resp.posList) )
				|| ( i >= ARRAY_SIZE(pRelationMsg->posList) )
				)
			{
				D_ERROR( "OnUpdateUnionPosCfgNtf, i(%d), count(%d), resp.posList或pRelationMsg->posList越界\n", i, count );
				return false;
			}
			//数据拷贝
			resp.posList[count].right = pRelationMsg->posList[i].right;
			resp.posList[count].seq = pRelationMsg->posList[i].seq;
			if(pRelationMsg->posList[i].posLen < sizeof(resp.posList[count].pos))
			{
				resp.posList[count].posLen = pRelationMsg->posList[i].posLen;
				StructMemCpy( resp.posList[count].pos, pRelationMsg->posList[i].pos, pRelationMsg->posList[i].posLen );
			} else {
				resp.posList[count].posLen = sizeof(resp.posList[count].pos);
				StructMemCpy( resp.posList[count].pos, pRelationMsg->posList[i].pos, sizeof(resp.posList[count].pos) );
			}

			//计数
			count++;

			//count=maxsegmentsize或者i=totalListSize-1时发送
			if((count == maxSegmentSize) || (totalListSize -1 == i))
			{
				resp.posListLen = count;
				NewSendPlayerPkg( GCUpdateUnionPosCfgNtf, pNotifyPlayer, resp );
				//MsgToPut* pNewMsg = CreatePlayerPkg( GCUpdateUnionPosCfgNtf, pNotifyPlayer, resp );
				//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

				count = 0;
			}	
		}
	}

	return true;

	TRY_END;
	return false;
}

// 工会成员禁言结果
bool CDealRelationSrvPkg::OnForbidUnionSpeekRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnForbidUnionSpeekRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(ForbidUnionSpeekResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnForbidUnionSpeekRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnForbidUnionSpeekRst,找不到通知的玩家\n");
		return false;
	}

	GCForbidUnionSpeekRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCForbidUnionSpeekRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCForbidUnionSpeekRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 工会成员禁言通知
bool CDealRelationSrvPkg::OnForbidUnionSpeekNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnForbidUnionSpeekNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(ForbidUnionSpeekNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnForbidUnionSpeekNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnForbidUnionSpeekNtf,找不到通知的玩家\n");
		return false;
	}

	GCForbidUnionSpeekNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	if ( pRelationMsg->nameLen < sizeof(resp.name) )
	{
		resp.nameLen = pRelationMsg->nameLen;
		SafeStrCpy( resp.name, pRelationMsg->name );//, pRelationMsg->nameLen);
	} else {	
		resp.nameLen = sizeof(resp.name);
		SafeStrCpy( resp.name, pRelationMsg->name );//, sizeof(resp.name));
	}

	NewSendPlayerPkg( GCForbidUnionSpeekNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCForbidUnionSpeekNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 通知工会技能
bool CDealRelationSrvPkg::OnUnionSkills( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionSkills,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionSkillsListNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionSkills 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionSkills,找不到通知的玩家\n");
		return false;
	}

	if(pRelationMsg->bGate)//通知mapserver刷新
	{
		GMReportUnionSkills resp;
		StructMemSet(resp, 0x00, sizeof(resp));

		resp.playerId.dwPID = pRelationMsg->playerId;
		resp.playerId.wGID = pRelationMsg->gateId;
		resp.unionID = pRelationMsg->guildId;
		if(pRelationMsg->skillsListLen <= sizeof(resp.unionSkills)/sizeof(resp.unionSkills[0]))
		{
			for ( int i = 0; i< pRelationMsg->skillsListLen; i++ )
			{
				if ( ( i >= ARRAY_SIZE(resp.unionSkills) )
					|| ( i >= ARRAY_SIZE(pRelationMsg->skillsList) )
					)
				{
					D_ERROR( "CDealRelationSrvPkg::OnUnionSkills, i(%d) >= ARRAY_SIZE(resp.unionSkills)\n", i );
					return false;
				}
				resp.unionSkills[i] = pRelationMsg->skillsList[i].skillId;
			}			
		}

		SendMsgToMapSrvByPlayer(GMReportUnionSkills, pNotifyPlayer, resp);
	}

	GCUnionSkills resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	if ( pRelationMsg->skillsListLen <= ARRAY_SIZE(resp.unionSkill) )
	{
		for(int i = 0; i < pRelationMsg->skillsListLen; i++)
		{
			if ( ( i >= ARRAY_SIZE(resp.unionSkill) )
				|| ( i >= ARRAY_SIZE(pRelationMsg->skillsList) )
				)
			{
				D_ERROR( "CDealRelationSrvPkg::OnUnionSkills, i(%d), resp.unionSkill或pRelationMsg->skillsList越界\n", i );
				return false;
			}
			resp.unionSkill[i] = pRelationMsg->skillsList[i].skillId;
		}

		resp.skillLen = pRelationMsg->skillsListLen;
	}

	NewSendPlayerPkg( GCUnionSkills, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUnionSkills, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 发布任务结果
bool CDealRelationSrvPkg::OnIssueUnionTaskRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnIssueUnionTaskRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(PostUnionTasksListResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnIssueUnionTaskRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnIssueUnionTaskRst,找不到通知的玩家\n");
		return false;
	}

	GCIssueUnionTaskRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.result = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCIssueUnionTaskRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCIssueUnionTaskRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 发布任务通知
bool CDealRelationSrvPkg::OnIssueUnionTaskNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnIssueUnionTaskNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionTasksListNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnIssueUnionTaskNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnIssueUnionTaskNtf,找不到通知的玩家\n");
		return false;
	}

	//发送给mapserver
	GMIssueUnionTasks issuedTask;
	StructMemSet(issuedTask ,0x00, sizeof(issuedTask));

	issuedTask.playerId = pNotifyPlayer->GetPlayerID();
	issuedTask.taskEndTime = pRelationMsg->endTime;
	issuedTask.costActive = pRelationMsg->activePoint;

	/*if(pRelationMsg->taskList.taskCount <= sizeof(issuedTask.unionTasks)/sizeof(issuedTask.unionTasks[0]))
	{
	for(int i = 0; i < pRelationMsg->taskList.taskCount; i++)
	{
	issuedTask.unionTasks[i] = pRelationMsg->taskList.task[i];
	}
	}*/

	SendMsgToMapSrvByPlayer(GMIssueUnionTasks, pNotifyPlayer, issuedTask);

	//发送给客户端
	GCIssueUnionTaskNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.costActive = pRelationMsg->activePoint;
	resp.taskEndTime = pRelationMsg->endTime;

	NewSendPlayerPkg( GCIssueUnionTaskNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCIssueUnionTaskNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

//// 显示工会任务
//bool CDealRelationSrvPkg::OnDisplayUnionTasks( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	return false;
//}

// 学习工会技能结果
bool CDealRelationSrvPkg::OnLearnUnionSkillRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnLearnUnionSkillRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(StudyUnionSkillResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnLearnUnionSkillRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnLearnUnionSkillRst,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCLearnUnionSkillRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.result = pRelationMsg->returnCode;
	resp.skillID = pRelationMsg->skillId;
	//resp.costActive = pRelationMsg->;

	NewSendPlayerPkg( GCLearnUnionSkillRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCLearnUnionSkillRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

//// 学习工会技能通知
//bool CDealRelationSrvPkg::OnLearnUnionSkillNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	return false;
//}

// 修改工会活跃度通知
bool CDealRelationSrvPkg::OnChanageUnionActiveNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnChanageUnionActiveNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionActivePointNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnChanageUnionActiveNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnChanageUnionActiveNtf,找不到通知的玩家\n");
		return false;
	}

	//通知mapserver
	GMChangeUnionActive changeActive;
	StructMemSet(changeActive, 0x00, sizeof(changeActive));

	changeActive.playerId.dwPID = pRelationMsg->playerId;
	changeActive.playerId.wGID = pRelationMsg->gateId;
	changeActive.newValue = pRelationMsg->point;

	SendMsgToMapSrvByPlayer(GMChangeUnionActive, pNotifyPlayer, changeActive);

	//发送给客户端
	GCChanageUnionActive resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.val = pRelationMsg->point;

	NewSendPlayerPkg( GCChanageUnionActive, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChanageUnionActive, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 提升工会等级结果
bool CDealRelationSrvPkg::OnAdvanceUnionLevelRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnAdvanceUnionLevelRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(AdvanceUnionLevelResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnAdvanceUnionLevelRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAdvanceUnionLevelRst,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCAdvanceUnionLevelRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCAdvanceUnionLevelRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAdvanceUnionLevelRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 工会等级通知
bool CDealRelationSrvPkg::OnUnionLevelNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionLevelNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionLevelNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionLevelNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionLevelNtf,找不到通知的玩家\n");
		return false;
	}

	//更新mapserver
	GMReportUnionLevel unionLevel;
	unionLevel.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
	unionLevel.unionLevel = pRelationMsg->level;
	SendMsgToMapSrvByPlayer(GMReportUnionLevel, pNotifyPlayer, unionLevel);

	//发送给客户端
	GCUnionLevelNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.level = pRelationMsg->level;
	resp.active = pRelationMsg->active;

	NewSendPlayerPkg( GCUnionLevelNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUnionLevelNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 发布工会公告结果
bool CDealRelationSrvPkg::OnPostUnionBulletinRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnPostUnionBulletinRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(PostUnionBulletinResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnPostUnionBulletinRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPostUnionBulletinRst,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCPostUnionBulletinRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;

	NewSendPlayerPkg( GCPostUnionBulletinRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPostUnionBulletinRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 工会公告通知
bool CDealRelationSrvPkg::OnUnionBulletinNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionBulletinNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionBulletinNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionBulletinNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionBulletinNtf,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCUnionBulletinNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.seq = pRelationMsg->seq;
	resp.contentLen = (pRelationMsg->contentLen <= sizeof(resp.content)) ? pRelationMsg->contentLen : sizeof(resp.content);
	StructMemCpy(resp.content, pRelationMsg->content, resp.contentLen);

	NewSendPlayerPkg( GCUnionBulletinNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUnionBulletinNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 工会成员上线通知
bool CDealRelationSrvPkg::OnUnionMemberOnLineNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionMemberOnLineNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionMemberUplineNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionMemberOnLineNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionMemberOnLineNtf,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCUnionMemberOnOffLineNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.flag = true;
	resp.uiid = pRelationMsg->uplinePlayer;

	NewSendPlayerPkg( GCUnionMemberOnOffLineNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUnionMemberOnOffLineNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 工会成员下线通知
bool CDealRelationSrvPkg::OnUnionMemberOffLineNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionMemberOffLineNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionMemberOfflineNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionMemberOffLineNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionMemberOffLineNtf,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCUnionMemberOnOffLineNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.flag = false;
	resp.uiid = pRelationMsg->offlinePlayer;

	NewSendPlayerPkg( GCUnionMemberOnOffLineNtf, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUnionMemberOnOffLineNtf, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

// 工会日至结果
bool CDealRelationSrvPkg::OnQueryUnionLogRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnQueryUnionLogRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(QueryUnionLogResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnQueryUnionLogRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnQueryUnionLogRst,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCQueryUnionLogRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.bookmark = pRelationMsg->bookmark;
	if((pRelationMsg->logListLen > 0) && (pRelationMsg->logListLen <= sizeof(resp.logList)/sizeof(resp.logList[0])))
	{
		resp.logListLen = pRelationMsg->logListLen;
		for(int i = 0; i < pRelationMsg->logListLen; i++)
		{
			if ( ( i >= ARRAY_SIZE(resp.logList) )
				|| ( i >= ARRAY_SIZE(pRelationMsg->logList) )
				)
			{
				D_ERROR( "CDealRelationSrvPkg::OnQueryUnionLogRst, i(%d), resp.logList或pRelationMsg->logList越界\n", i );
				return false;
			}
			resp.logList[i].datetime = pRelationMsg->logList[i].datetime;
			resp.logList[i].eventId = pRelationMsg->logList[i].eventId;

			if ( pRelationMsg->logList[i].param1.paramLen <= ARRAY_SIZE(resp.logList[i].param1.param) )
			{
				resp.logList[i].param1.paramLen = pRelationMsg->logList[i].param1.paramLen;
			} else {
				resp.logList[i].param1.paramLen = ARRAY_SIZE(resp.logList[i].param1.param);
			}			
			StructMemCpy(resp.logList[i].param1.param, pRelationMsg->logList[i].param1.param, resp.logList[i].param1.paramLen);

			if ( pRelationMsg->logList[i].param2.paramLen <= ARRAY_SIZE(resp.logList[i].param2.param) )
			{
				resp.logList[i].param2.paramLen = pRelationMsg->logList[i].param2.paramLen;
			} else {
				resp.logList[i].param2.paramLen = ARRAY_SIZE( resp.logList[i].param2.param);
			}			
			StructMemCpy(resp.logList[i].param2.param, pRelationMsg->logList[i].param2.param, resp.logList[i].param2.paramLen);
		}		
	}

	NewSendPlayerPkg( GCQueryUnionLogRst, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCQueryUnionLogRst, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

// 工会邮件通知
bool CDealRelationSrvPkg::OnUnionMailNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionMailNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionEmailNotify , pRelationMsg,pPkg,wPkgLen );

	GFSendSysMailToPlayer sendMail;
	StructMemSet(sendMail, 0x00, sizeof(sendMail));

	SafeStrCpy( sendMail.recvPlayerName, pRelationMsg->name );//, sizeof(sendMail.recvPlayerName));
	SafeStrCpy( sendMail.sendMailTitle,  pRelationMsg->emailTitle );//, sizeof(sendMail.sendMailTitle));
	SafeStrCpy( sendMail.sendMailContent, pRelationMsg->emailContent );//, sizeof(sendMail.sendMailContent));

	SendMsgToFunctionSrv( GFSendSysMailToPlayer, sendMail );

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnUnionOwnerCheckRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionOwnerCheckRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionOwnerCheckResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionOwnerCheckRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionOwnerCheckRst,找不到通知的玩家\n");
		return false;
	}

	int roleIndex = pNotifyPlayer->GetRoleIndex(pRelationMsg->uiid);
	if(roleIndex < 0)
	{
		D_ERROR("OnUnionOwnerCheckRst,找不到对应的角色\n");
		return false;
	}

	if(0 != pRelationMsg->flag)//公会会长不可删除
	{
		GCDeleteRole gcDeleteRole;

		gcDeleteRole.byIsOK = GCDeleteRole::OP_UNION_OWNER_FAILED;
		gcDeleteRole.wRoleNo = roleIndex;

		NewSendPlayerPkg( GCDeleteRole, pNotifyPlayer, gcDeleteRole );
		//MsgToPut* pGCDeleteRole = CreatePlayerPkg( GCDeleteRole, pNotifyPlayer, gcDeleteRole );
		//pNotifyPlayer->SendPkgToPlayer( pGCDeleteRole );
		return false;
	}

	//删除操作
	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( pNotifyPlayer->GetAccount() );
	if ( NULL == pDbSrv )
	{
		GCSrvError srvError;
		srvError.errNO = 1001;

		NewSendPlayerPkg( GCSrvError, pNotifyPlayer, srvError );
		//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pNotifyPlayer, srvError );
		//if ( NULL != pNotifyPlayer )
		//{
		// pNotifyPlayer->SendPkgToPlayer( pErrMsg );//通知玩家没有对应的dbsrv;
		//}
		D_WARNING( "玩家发删除角色请求时，找不到玩家所在的dbsrvd\n\n" );
		return false;
	}

	GDDeletePlayerInfo deletePlayerInfo;
	deletePlayerInfo.playerID = pNotifyPlayer->GetPlayerID();
	SafeStrCpy( deletePlayerInfo.szAccount, pNotifyPlayer->GetAccount() );
	SafeStrCpy( deletePlayerInfo.strNickName, pNotifyPlayer->GetRoleName() );
	deletePlayerInfo.uiPlayerInfoID = pRelationMsg->uiid;
	//MsgToPut* pGDDeleteRole = CreateSrvPkg( GDDeletePlayerInfo, pDbSrv, deletePlayerInfo );
	//pDbSrv->SendPkgToSrv( pGDDeleteRole );
	SendMsgToSrv( GDDeletePlayerInfo, pDbSrv, deletePlayerInfo );

	//发送给RelationSrv，通知删除玩家
	CRelationSrv* pRelationSrv = CManRelationSrvs::GetRelationserver();
	if(  pRelationSrv )
	{
		G_R_DelPlayer delPlayer;
		delPlayer.uiid = pRelationMsg->uiid;
		//MsgToPut* pNewMsg = CreateRelationPkg( G_R_DelPlayer, pRelationSrv, delPlayer );
		//pRelationSrv->SendPkgToSrv( pNewMsg );
		SendMsgToRelationSrv( G_R_DelPlayer, delPlayer );
	}

	//通知所有的mapsrv删除该排行榜玩家
	GMPlayerDeleteRole removePlayer;
	removePlayer.race = pNotifyPlayer->GetRoleRace();
	removePlayer.playerUID = pRelationMsg->uiid;
	CManMapSrvs::SendBroadcastMsg<GMPlayerDeleteRole>( &removePlayer );

	//通知functionSrv删除这个玩家
	GFOnPlayerDeleteRole onDelRole;
	onDelRole.race = pNotifyPlayer->GetRoleRace();
	onDelRole.playerUID = pRelationMsg->uiid;
	SendMsgToFunctionSrv( GFOnPlayerDeleteRole, onDelRole );

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnModifyUnionPrestigeRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnModifyUnionPrestigeRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(ModifyUnionPrestigeResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyUnionPrestigeRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnModifyUnionPrestigeRst,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCModifyUnionPrestigeRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;
	NewSendPlayerPkg( GCModifyUnionPrestigeRst, pNotifyPlayer, resp );

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnModifyUnionPrestigeNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnModifyUnionPrestigeNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionPrestigeNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyUnionPrestigeNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnModifyUnionPrestigeNtf,找不到通知的玩家\n");
		return false;
	}

	//通知mapserver
	GMChangeUnionPrestige changePrestige;
	StructMemSet(changePrestige, 0x00, sizeof(changePrestige));

	changePrestige.playerId.dwPID = pRelationMsg->playerId;
	changePrestige.playerId.wGID = pRelationMsg->gateId;
	changePrestige.newValue = pRelationMsg->prestige;

	SendMsgToMapSrvByPlayer(GMChangeUnionPrestige, pNotifyPlayer, changePrestige);

	//发送给客户端
	GCModifyUnionPrestigeNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.prestige = pRelationMsg->prestige;
	NewSendPlayerPkg( GCModifyUnionPrestigeNtf, pNotifyPlayer, resp );

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnModifyUnionBadgeRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnModifyUnionBadgeRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(ModifyUnionBadgeResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyUnionBadgeRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnModifyUnionBadgeRst,找不到通知的玩家\n");
		return false;
	}

	//发送给客户端
	GCModifyUnionBadgeRst resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.returnCode = pRelationMsg->returnCode;
	NewSendPlayerPkg( GCModifyUnionBadgeRst, pNotifyPlayer, resp );

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnModifyUnionBadgeNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnModifyUnionBadgeNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionBadgeNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnModifyUnionBadgeNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnModifyUnionBadgeNtf,找不到通知的玩家\n");
		return false;
	}

	//通知mapserver
	GMChanageUnionBadge changeBadge;
	StructMemSet(changeBadge, 0x00, sizeof(changeBadge));

	changeBadge.playerId.dwPID = pRelationMsg->playerId;
	changeBadge.playerId.wGID = pRelationMsg->gateId;
	SafeStrCpy(changeBadge.newBadge, pRelationMsg->badgeData);

	SendMsgToMapSrvByPlayer(GMChanageUnionBadge, pNotifyPlayer, changeBadge);

	//发送给客户端
	GCModifyUnionBadgeNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	SafeStrCpy(resp.badgeData, pRelationMsg->badgeData);
	resp.dataLen =(UINT) strlen(resp.badgeData);
	NewSendPlayerPkg( GCModifyUnionBadgeNtf, pNotifyPlayer, resp );

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnUnionMultiNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionMultiNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(UnionMultiNotify , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnUnionMultiNtf 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionMultiNtf,找不到通知的玩家\n");
		return false;
	}

	//通知mapserver
	GMUnionMultiNtf ntf;
	StructMemSet(ntf, 0x00, sizeof(ntf));

	ntf.playerId.wGID = pRelationMsg->gateId;
	ntf.playerId.dwPID = pRelationMsg->playerId;
	ntf.type = pRelationMsg->type;
	ntf.multi = pRelationMsg->multi;
	ntf.endTime = pRelationMsg->endTime;

	SendMsgToMapSrvByPlayer(GMUnionMultiNtf, pNotifyPlayer, ntf);

	//发送给客户端
	GCUnionMultiNtf resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.type = pRelationMsg->type;
	resp.multi = pRelationMsg->multi;
	resp.endTime = pRelationMsg->endTime;

	NewSendPlayerPkg( GCUnionMultiNtf, pNotifyPlayer, resp );

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnGMQueryRelationRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnGMQueryRelationRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(QueryPlayerRelationResult , pRelationMsg,pPkg,wPkgLen );
	if( pRelationMsg->gmGateId != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealRelationSrvPkg::OnGMQueryRelationRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRelationMsg->gmPlayerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnGMQueryRelationRst,找不到通知的玩家\n");
		return false;
	}

	//发送给GM
	GTCmd_Query_Rsp resp;
	StructMemSet(resp, 0x00, sizeof(resp));
	resp.uType = GTCT_ROLE_RELATION;
	resp.xKey.unKey.xRoleKey.uRoleID = pRelationMsg->uiid ;
	resp.xInfo.none = GTCT_ROLE_RELATION;

	GTCmd_Info_Role_Relation &relationInfo =resp.xInfo.xInfo.xRoleRelation;
	bool needSend = true; 

	if(pRelationMsg->relation.friendinfosLen > 0)
	{
		unsigned int count = 0;
		for(unsigned int i = 0; i < pRelationMsg->relation.friendinfosLen; i++)
		{
			if ( ( count >= ARRAY_SIZE(relationInfo.xPerson) )
				|| ( i >= ARRAY_SIZE(pRelationMsg->relation.friendinfos) )
				)
			{
				D_ERROR( "OnGMQueryRelationRst, i(%d), relationInfo.xPerson或pRelationMsg->relation.friendinfos越界\n", i );
				return false;
			}
			relationInfo.xPerson[count].cType = pRelationMsg->relation.friendinfos[i].GroupID;
			relationInfo.xPerson[count].uID = pRelationMsg->relation.friendinfos[i].uiID;
			SafeStrCpy(relationInfo.xPerson[count].sName, pRelationMsg->relation.friendinfos[i].NickName);
			relationInfo.xPerson[count].uNameSize = (UINT)strlen(relationInfo.xPerson[count].sName);
			relationInfo.xPerson[count].cOnline = pRelationMsg->relation.friendinfos[i].EnterModel;
			relationInfo.xPerson[count].cSex = (pRelationMsg->relation.friendinfos[i].Flag & 0x02)? SEX_MALE :SEX_FEMALE;;
			relationInfo.xPerson[count].uMapID = pRelationMsg->relation.friendinfos[i].MapID;
			relationInfo.xPerson[count].uLevel = pRelationMsg->relation.friendinfos[i].playerLevel;
			relationInfo.xPerson[count].uClass = pRelationMsg->relation.friendinfos[i].playerClass;
			relationInfo.xPerson[count].uRace = pRelationMsg->relation.friendinfos[i].playerRace;
			SafeStrCpy(relationInfo.xPerson[count].sUnionName, pRelationMsg->relation.friendinfos[i].GuildName);
			relationInfo.xPerson[count].uUnionNameSize = (UINT)strlen(relationInfo.xPerson[count].sUnionName);

			count++;
			if ( count == ARRAY_SIZE(relationInfo.xPerson) ) //sizeof(relationInfo.xPerson)/sizeof(relationInfo.xPerson[0]))
			{
				relationInfo.uCount = count;
				NewSendPlayerPkg( GTCmd_Query_Rsp, pNotifyPlayer, resp );
				count = 0;
			}
		}

		if ( 0 == (pRelationMsg->relation.friendinfosLen % ARRAY_SIZE(relationInfo.xPerson)) ) //?什么意思?
		{
			needSend = false;
		} else {
			relationInfo.uCount = count;
		}
	}

	if ( needSend )
	{
		NewSendPlayerPkg( GTCmd_Query_Rsp, pNotifyPlayer, resp );
	}

	return true;
	TRY_END;
	return false;
}


bool CDealRelationSrvPkg::OnRecvPlayerUpLineTeamIDNotify(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvPlayerUpLineTeamIDNotify,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(PlayerUplineTeamIdNotify , pRelationMsg, pPkg,wPkgLen );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
		return false;



	pNotifyPlayer->SetCopyMapTeamIDInfo( pRelationMsg->teamId, pRelationMsg->playerFlag, pRelationMsg->teamFlag );

	GMSinglePlayerTeamIDNotify srvmsg;
	srvmsg.upLinePlayerID.wGID  = pRelationMsg->gateId;
	srvmsg.upLinePlayerID.dwPID = pRelationMsg->playerId;
	srvmsg.teamID = pRelationMsg->teamId;
	srvmsg.playerFlag = pRelationMsg->playerFlag;
	srvmsg.teamFlag = pRelationMsg->teamFlag;
	SendMsgToMapSrvByPlayer(GMSinglePlayerTeamIDNotify, pNotifyPlayer, srvmsg );


	D_DEBUG("接到TeamIDNotify 玩家 %s 的playerFlag:%d teamFlag:%d\n", pNotifyPlayer->GetRoleName(), srvmsg.playerFlag, srvmsg.teamFlag );

	return true;

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnRecvQueryUnionRstByPage(CRelationSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvQueryUnionRstByPage,pOwner空\n" );
		return false;
	}


	TRY_BEGIN;

	DealRelationPkgPre( QueryUnionRankResult , pRelationMsg, pPkg,wPkgLen );

	CPlayer* pPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pPlayer )
		return false;

	if ( pRelationMsg->len > 10 )
	{
		D_ERROR("接受的排行榜长度过长 %d\n",pRelationMsg->len );
		return false;
	}

	//如果消息有两页
	if ( pRelationMsg->len > MAX_CHARTPAGE_LEN )
	{
		//发送前半页
		GCRankInfoByPageRst rankInfoRst1;
		StructMemSet( rankInfoRst1 ,0x0, sizeof(GCRankInfoByPageRst) );
		rankInfoRst1.rankCount     = MAX_CHARTPAGE_LEN;
		rankInfoRst1.rankPageIndex = (char)pRelationMsg->page;
		for ( int i = 0 ; i< rankInfoRst1.rankCount ; i++ )
		{
			if ( ( i >= ARRAY_SIZE(pRelationMsg->rank) )
				|| ( i >= ARRAY_SIZE(rankInfoRst1.rankInfoArr) )
				)
			{
				D_ERROR( "OnRecvQueryUnionRstByPage, i(%d), pRelationMsg->rank或rankInfoRst1.rankInfoArr越界\n", i );
				return false;
			}
			ConvertUnionRankToCliRankInfo( pRelationMsg->rank[i], rankInfoRst1.rankInfoArr[i] );
		}
		NewSendPlayerPkg( GCRankInfoByPageRst, pPlayer, rankInfoRst1 );

		//发送后半页
		GCRankInfoByPageRst rankInfoRst2;
		StructMemSet( rankInfoRst2 ,0x0, sizeof(GCRankInfoByPageRst) );
		rankInfoRst2.rankCount     =  pRelationMsg->len - MAX_CHARTPAGE_LEN ;
		rankInfoRst2.rankPageIndex =  (char)pRelationMsg->page;
		rankInfoRst2.pageEnd       =  pRelationMsg->endFlag != 0 ? 0x1:0x0;
		for ( int i = 0 ; i< rankInfoRst2.rankCount ; i++ )
		{
			if ( ( (MAX_CHARTPAGE_LEN+i) >= ARRAY_SIZE(pRelationMsg->rank) )
				|| ( i >= ARRAY_SIZE(rankInfoRst2.rankInfoArr) )
				)
			{
				D_ERROR( "OnRecvQueryUnionRstByPage, i(%d)或MAX_CHARTPAGE_LEN+i越界\n", i );
				return false;
			}
			ConvertUnionRankToCliRankInfo( pRelationMsg->rank[ MAX_CHARTPAGE_LEN + i ], rankInfoRst2.rankInfoArr[i]  );
		}

		NewSendPlayerPkg( GCRankInfoByPageRst, pPlayer, rankInfoRst2 );
	} else { //if ( pRelationMsg->len > MAX_CHARTPAGE_LEN )
		GCRankInfoByPageRst rankInfoRst;
		StructMemSet( rankInfoRst ,0x0, sizeof(GCRankInfoByPageRst) );
		rankInfoRst.rankCount     = pRelationMsg->len;
		rankInfoRst.rankPageIndex = (char)pRelationMsg->page;
		rankInfoRst.pageEnd       = pRelationMsg->endFlag != 0? 0x1:0x0;
		for ( int i = 0 ; i< rankInfoRst.rankCount ; i++ )
		{
			if ( ( i >= ARRAY_SIZE(pRelationMsg->rank) )
				|| ( i >= ARRAY_SIZE(rankInfoRst.rankInfoArr) )
				)
			{
				D_ERROR( "OnRecvQueryUnionRstByPage2, i(%d), pRelationMsg->rank或rankInfoRst.rankInfoArr越界\n", i );
				return false;
			}
			ConvertUnionRankToCliRankInfo( pRelationMsg->rank[i], rankInfoRst.rankInfoArr[i] );
		}
		NewSendPlayerPkg( GCRankInfoByPageRst, pPlayer, rankInfoRst );
	}

	return false;
}

bool CDealRelationSrvPkg::OnRecvQueryUnionRstByUnionName(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvQueryUnionRstByUnionName,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( SearchUnionRankResult, pRelationMsg, pPkg,wPkgLen );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR( "OnRecvQueryUnionRstByUnionName, NULL == pNotifyPlayer\n" );
		return false;
	}

	GCQueryRankPlayerByNameRst queryRst;
	StructMemSet( queryRst, 0x0, sizeof(GCQueryRankPlayerByNameRst) );
	if( pRelationMsg->resultCode == R_G_RESULT_UNION_SUCCEED )
	{
		queryRst.queryRst  = true;
		ConvertUnionRankToCliRankInfo( pRelationMsg->unionInfo, queryRst.rankPlayer );
	} else {
		queryRst.queryRst  = false;
	}

	NewSendPlayerPkg( GCQueryRankPlayerByNameRst,pNotifyPlayer, queryRst );


	return false;
}


bool CDealRelationSrvPkg::OnRecvQueryRaceMasterRst(CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRecvQueryRaceMasterRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( QueryRaceMasterResult, pRelationMsg, pPkg,wPkgLen );

	D_DEBUG("接到Relationsrv发送来的QueryRaceMasterResult结果\n");

	GMRaceMasterNewBorn srvmsg;
	GFNewRaceMasterBorn newbornmsg;
	if ( pRelationMsg->len > sizeof(pRelationMsg->masters)/sizeof(UnionRaceMasterInfo) )
	{
		D_ERROR("CDealRelationSrvPkg::OnRecvQueryRaceMasterRst 传来的大小错误:%d\n",pRelationMsg->len );
		return false;
	}

	for ( unsigned int i = 0 ; i< pRelationMsg->len; i++ )
	{
		if ( i >= ARRAY_SIZE(pRelationMsg->masters) )
		{
			D_ERROR( "CDealRelationSrvPkg::OnRecvQueryRaceMasterRst, i(%d) >= ARRAY_SIZE(pRelationMsg->masters)\n", i );
			return false;
		}
		UnionRaceMasterInfo& racemasterinfo = pRelationMsg->masters[i];
		D_DEBUG("成为族长的工会信息如下:工会:%s 盟主:%s 种族:%d 族长UID:%d\n",  racemasterinfo.unionName,racemasterinfo.captionName,racemasterinfo.unionRace, racemasterinfo.captionId );
		if ( racemasterinfo.unionRace != 0  )//工会所属种族一定不为0
		{
			srvmsg.masterUID = racemasterinfo.captionId;
			srvmsg.racetype  = racemasterinfo.unionRace;
			SafeStrCpy( srvmsg.playerName, racemasterinfo.captionName);
			SafeStrCpy( srvmsg.unionName,  racemasterinfo.unionName );
			//通知所有的mapsrv
			CManMapSrvs::SendBroadcastMsg<GMRaceMasterNewBorn>( &srvmsg );

			//通知functionsrv
			newbornmsg.masteruid = racemasterinfo.captionId;
			newbornmsg.racetype  = racemasterinfo.unionRace;
			SafeStrCpy( newbornmsg.playerName, racemasterinfo.captionName );
			SafeStrCpy( newbornmsg.unionName, racemasterinfo.unionName );
			SendMsgToFunctionSrv( GFNewRaceMasterBorn, newbornmsg );

			D_DEBUG("工会(%s)盟主%s(%d)成为%d族的族长\n", newbornmsg.unionName, newbornmsg.playerName, newbornmsg.masteruid, newbornmsg.racetype );
		}
		else
		{
			D_ERROR("racemasterinfo.unionRace==0 || racemasterinfo.unionPretige==0 错误\n");
		}
	}

	return true;
	TRY_END;


	return false;
}


bool CDealRelationSrvPkg::OnQueryPlayerInfoResult( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnGMQueryRelationRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(QueryPlayerInfoResult , pRelationMsg, pPkg,wPkgLen );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
		return false;

	SafeStruct( GCIrcQueryPlayerInfoRst, climsg);
	climsg.errorCode = GCIrcQueryPlayerInfoRst::QUERY_SUCCESS;
	SafeStrCpy( climsg.GuildName, pRelationMsg->targetInfo.GuildName );
	climsg.GuildNameLen = pRelationMsg->targetInfo.GuildNameLen;
	climsg.mapID = pRelationMsg->targetInfo.MapID;
	climsg.playerClass = pRelationMsg->targetInfo.playerClass;
	climsg.playerLevel = pRelationMsg->targetInfo.playerLevel;
	SafeStrCpy( climsg.playerName, pRelationMsg->targetInfo.NickName );
	climsg.nameSize = pRelationMsg->targetInfo.NickNameLen;
	climsg.playerRace = pRelationMsg->targetInfo.playerRace;
	climsg.playerSex = pRelationMsg->targetInfo.Flag;
	NewSendPlayerPkg( GCIrcQueryPlayerInfoRst, pNotifyPlayer, climsg );

	TRY_END;
	return false;
}

void CDealRelationSrvPkg::ConvertUnionRankToCliRankInfo( const RelationServiceNS::UnionRankInfo& unionRankInfo, RankChartItem& rankItem )
{

	SafeStrCpy( rankItem.itemName ,unionRankInfo.unionName);
	SafeStrCpy( rankItem.itemClass,unionRankInfo.captionName );
	rankItem.itemNameLen = (char)unionRankInfo.unionNameLen;
	rankItem.itemClassLen = (char)unionRankInfo.captionNameLen;
	rankItem.itemLevel = (int)unionRankInfo.unionLevel;
	rankItem.itemRace = (char)unionRankInfo.unionRace;
	rankItem.rankInfo = unionRankInfo.unionPretige;
}

bool CDealRelationSrvPkg::OnTeamLogNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnTeamLogNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(TeamLogNotify , pRelationMsg,pPkg,wPkgLen );

	CLogManager::DoRoleTeamChanage(pRelationMsg->uiid, pRelationMsg->level, pRelationMsg->teamId, (unsigned char)pRelationMsg->logType );//记日至

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnFriendTweetNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealRelationSrvPkg::OnFriendTweetNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(ShowFriendTweet , pRelationMsg, pPkg,wPkgLen );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
		return false;

	GCShowFriendTweetNtf climsg;
	SafeStrCpy(climsg.friendName, pRelationMsg->friendName);
	climsg.nameLen = pRelationMsg->nameLen;
	if(climsg.nameLen > ARRAY_SIZE(climsg.friendName))
		climsg.nameLen = ARRAY_SIZE(climsg.friendName);

	climsg.tweet.type = pRelationMsg->tweet.type;
	climsg.tweet.createdTime = pRelationMsg->tweet.createdTime;
	climsg.tweet.tweetLen = pRelationMsg->tweet.tweetLen;
	SafeStrCpy(climsg.tweet.tweet, pRelationMsg->tweet.tweet);
	if(climsg.tweet.tweetLen > ARRAY_SIZE(climsg.tweet.tweet))
		climsg.tweet.tweetLen = ARRAY_SIZE(climsg.tweet.tweet);

	NewSendPlayerPkg( GCShowFriendTweetNtf, pNotifyPlayer, climsg );

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnQueryFriendTweetRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealRelationSrvPkg::OnQueryFriendTweetRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(QueryFriendTweetsResult , pRelationMsg, pPkg,wPkgLen );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
		return false;

	GCQueryFriendTweetsRst climsg;
	SafeStrCpy(climsg.friendName, pRelationMsg->friendName);
	climsg.nameLen = pRelationMsg->nameLen;
	if(climsg.nameLen > ARRAY_SIZE(climsg.friendName))
		climsg.nameLen = ARRAY_SIZE(climsg.friendName);

	climsg.tweet.type = pRelationMsg->tweet.type;
	climsg.tweet.createdTime = pRelationMsg->tweet.createdTime;
	climsg.tweet.tweetLen = pRelationMsg->tweet.tweetLen;
	SafeStrCpy(climsg.tweet.tweet, pRelationMsg->tweet.tweet);
	if(climsg.tweet.tweetLen > ARRAY_SIZE(climsg.tweet.tweet))
		climsg.tweet.tweetLen = ARRAY_SIZE(climsg.tweet.tweet);

	NewSendPlayerPkg( GCQueryFriendTweetsRst, pNotifyPlayer, climsg );

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnQueryFriendListRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealRelationSrvPkg::OnQueryFriendListRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(QueryFriendsListResult , pRelationMsg, pPkg,wPkgLen );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
		return false;

	GCQueryFriendsListRst climsg;
	climsg.targetUiid = pRelationMsg->targetUiid;
	climsg.lstLen = pRelationMsg->lstLen;
	if(climsg.lstLen > ARRAY_SIZE(climsg.friendsLst))
		climsg.lstLen = ARRAY_SIZE(climsg.friendsLst);

	if(ARRAY_SIZE(climsg.friendsLst) != ARRAY_SIZE(pRelationMsg->friendsLst))
	{
		D_ERROR( "CDealRelationSrvPkg::OnQueryFriendListRst, ARRAY_SIZE(climsg.friendsLst) != ARRAY_SIZE(pRelationMsg->friendsLst)\n" );
		return false;
	}

	StructMemSet(climsg.friendsLst, 0x00, sizeof(climsg.friendsLst));
	for(unsigned int i = 0; i < climsg.lstLen; i++)
	{
		climsg.friendsLst[i].friendID =  pRelationMsg->friendsLst[i].uiID;
		climsg.friendsLst[i].mapID =  pRelationMsg->friendsLst[i].MapID;
		climsg.friendsLst[i].playerLevel =  pRelationMsg->friendsLst[i].playerLevel;
		climsg.friendsLst[i].playerClass =  pRelationMsg->friendsLst[i].playerClass;
		climsg.friendsLst[i].playerRace =  pRelationMsg->friendsLst[i].playerRace;
		climsg.friendsLst[i].playerIcon =  pRelationMsg->friendsLst[i].playerIcon;
		climsg.friendsLst[i].isOnline =  pRelationMsg->friendsLst[i].Online;

		climsg.friendsLst[i].GuildNameLen =  pRelationMsg->friendsLst[i].GuildNameLen;
		StructMemCpy(climsg.friendsLst[i].GuildName, pRelationMsg->friendsLst[i].GuildName, sizeof(climsg.friendsLst[i].GuildName));
		if(climsg.friendsLst[i].GuildNameLen > ARRAY_SIZE(climsg.friendsLst[i].GuildName))
			climsg.friendsLst[i].GuildNameLen = ARRAY_SIZE(climsg.friendsLst[i].GuildName);

		climsg.friendsLst[i].FereNameLen =  pRelationMsg->friendsLst[i].FereNameLen;
		StructMemCpy(climsg.friendsLst[i].FereName, pRelationMsg->friendsLst[i].FereName, sizeof(climsg.friendsLst[i].FereName));
		if(climsg.friendsLst[i].FereNameLen > ARRAY_SIZE(climsg.friendsLst[i].FereName))
			climsg.friendsLst[i].FereNameLen = ARRAY_SIZE(climsg.friendsLst[i].FereName);

	}	

	NewSendPlayerPkg( GCQueryFriendsListRst, pNotifyPlayer, climsg );

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnFriendNonTeamGainNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealRelationSrvPkg::OnFriendNonTeamGainNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(NotifyAddExp , pRelationMsg, pPkg,wPkgLen );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
		return false;

	GMNotifyFriendGain farward;
	farward.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
	farward.inTeam = false;
	farward.expOrNot = pRelationMsg->exp;

	SendMsgToMapSrvByPlayer(GMNotifyFriendGain, pNotifyPlayer, farward);

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnQueryExpOfLevelUpReq( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealRelationSrvPkg::OnQueryExpOfLevelUpReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(QueryExpNeededWhenLvlUpResult , pRelationMsg, pPkg,wPkgLen );

	CMapSrv * pMapSrv = CManMapSrvs::GetMapserver(0);
	if(NULL == pMapSrv)
	{
		D_ERROR( "CDealRelationSrvPkg::OnQueryExpOfLevelUpReq, NULL == pMapSrv\n" );
		return false;
	}

	GMQueryExpNeededWhenLvlUpRequest farward;
	MsgToPut* pNewMsg = CreateSrvPkg( GMQueryExpNeededWhenLvlUpRequest, pMapSrv, farward );
	pMapSrv->SendPkgToSrv( pNewMsg );

	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnFriendTeamGainNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealRelationSrvPkg::OnFriendTeamGainNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(NotifyTeamGain , pRelationMsg, pPkg,wPkgLen );

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRelationMsg->playerId );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR( "CDealRelationSrvPkg::OnFriendTeamGainNtf, NULL == pNotifyPlayer, playerId(%d)\n", pRelationMsg->playerId );
		return false;
	}

	GMNotifyFriendGain farward;
	farward.lNotiPlayerID = pNotifyPlayer->GetPlayerID();
	farward.inTeam = true;
	farward.expOrNot = pRelationMsg->percent;

	SendMsgToMapSrvByPlayer(GMNotifyFriendGain, pNotifyPlayer, farward);

	GCFriendTeamGainAddition gainAddition;
	gainAddition.percent = pRelationMsg->percent;

	NewSendPlayerPkg(GCFriendTeamGainAddition, pNotifyPlayer, gainAddition);

	TRY_END;
	return false;
}

//Register( R_G_QUERY_NICKUNION_RST, &OnQueryNickUnionRst );//查询昵称战盟结果
bool CDealRelationSrvPkg::OnQueryNickUnionRst( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealRelationSrvPkg::OnQueryNickUnionRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre( RGQueryNickUnionRst, pQryRst, pPkg, wPkgLen );

	if ( NULL == pQryRst )
	{
		D_ERROR( "CDealRelationSrvPkg::OnQueryNickUnionRst, NULL == pQryRst\n" );
		return false;
	}

	if ( pQryRst->nickunionArrLen > ARRAY_SIZE(pQryRst->nickunionArr) )
	{
		D_ERROR( "CDealRelationSrvPkg::OnQueryNickUnionRst, pQryRst->nickunionArrLen(%d) >= ARRAY_SIZE(pQryRst->nickunionArr)(%d)\n"
			, pQryRst->nickunionArrLen, ARRAY_SIZE(pQryRst->nickunionArr) );
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pQryRst->queryPlayerID.PPlayer );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR( "CDealRelationSrvPkg::OnQueryNickUnionRst, NULL == pNotifyPlayer, playerId(%d)\n", pQryRst->queryPlayerID.PPlayer );
		return false;
	}

	GCQueryNickUnionRst gcRst;
	StructMemSet( gcRst, 0, sizeof(gcRst) );
	gcRst.rstArrLen = 0;

	for ( unsigned int i=0; i<pQryRst->nickunionArrLen; ++i )
	{
		if ( ( i>=ARRAY_SIZE(gcRst.rstArr) ) || ( i>=ARRAY_SIZE(pQryRst->nickunionArr) ) )
		{
			D_ERROR( "CDealRelationSrvPkg::OnQueryNickUnionRst, i(%d)>=ARRAY_SIZE(gcRst.rstArr) ) || ( i>=ARRAY_SIZE(pQryRst->nickunionArr)\n", i );
			break;
		}
		SafeStrCpy( gcRst.rstArr[i].nickName.qryName, pQryRst->nickunionArr[i].NickName.Name );
		gcRst.rstArr[i].nickName.nameLen = (BYTE) min( (UINT)strlen(gcRst.rstArr[i].nickName.qryName), (UINT)ARRAY_SIZE(gcRst.rstArr[i].nickName.qryName) );
		SafeStrCpy( gcRst.rstArr[i].unionName.qryName, pQryRst->nickunionArr[i].UnionName.Name );
		gcRst.rstArr[i].unionName.nameLen = (BYTE) min( (UINT)strlen(gcRst.rstArr[i].unionName.qryName), (UINT)ARRAY_SIZE(gcRst.rstArr[i].unionName.qryName) );
		++ gcRst.rstArrLen;
	}

	NewSendPlayerPkg( GCQueryNickUnionRst, pNotifyPlayer, gcRst );

	return true;
	TRY_END;
	return false;
}

bool CDealRelationSrvPkg::OnFriendNonTeamGainMailNtf( CRelationSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealRelationSrvPkg::OnFriendNonTeamGainMailNtf,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealRelationPkgPre(FriendDegreeGainEmail , pRelationMsg, pPkg,wPkgLen );

	GFSendSysMailToPlayer mailNtf;
	StructMemSet( mailNtf, 0x00, sizeof(mailNtf));

	StructMemCpy( mailNtf.recvPlayerName, pRelationMsg->name, pRelationMsg->nameLen);
	StructMemCpy( mailNtf.sendMailTitle, pRelationMsg->title, pRelationMsg->titleLen);
	StructMemCpy( mailNtf.sendMailContent, pRelationMsg->content, pRelationMsg->contentLen);
	mailNtf.attachItem[0].usItemTypeID =  pRelationMsg->itemId;
	SendMsgToFunctionSrv(GFSendSysMailToPlayer, mailNtf);

	return true;
	TRY_END;
	return false;
}
