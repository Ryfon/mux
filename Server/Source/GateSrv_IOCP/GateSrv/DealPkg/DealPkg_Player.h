﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/
#include "PkgSendDealDefine.h"

#pragma once


///玩家消息处理类;
class CPlayer;
class CDealPlayerPkg : public IDealPkg<CPlayer>
{
private:
	static CliProtocol m_cliprotocol;
private:
	CDealPlayerPkg() {};
	virtual ~CDealPlayerPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();
public:
	/************************************************************************/
	/*转发给CenterSrv*/
	/************************************************************************/

	/************************************************************************/
	/*转发给MapSrv*/
	/************************************************************************/
	///Register( C_G_USE_MONEY_TYPE, &OnUseMoneyType );//玩家切换所使用的货币
	static bool OnUseMoneyType( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//玩家选择显示的称号
	static bool OnHonorMask( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///玩家跑动消息处理;
	static bool OnPlayerRun( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家跑动中跨块；
	static bool OnPlayerBeyondBlock( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );


	//todo wcj 2011.01.04 似乎没用到，暂时删除
	///玩家登录消息处理;
	//static bool OnPlayerLogin( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家登录过程：请求随机字符串；
	static bool OnPlayerReqRdStr( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家登录过程：发来md5校验值；
	static bool OnPlayerUserMd5Value( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家创建角色;
	static bool OnPlayerCreateRole( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//玩家删除角色
	static bool OnPlayerDeleteRole( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家进入世界消息处理；
	static bool OnPlayerEnterWorld( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///玩家跳地图进入新地图请求;
	static bool OnPlayerSwitchEnterNewMap( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家选角色消息处理；
	static bool OnPlayerSelRole( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家攻击怪物处理；
	static bool OnPlayerAttackMonster( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家请求别个玩家的角色信息（用于界面显示）；
	static bool OnPlayerReqRoleInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家请求某怪物的相关信息（用于界面显示）；
	static bool OnPlayerReqMonsterInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///心跳检测包；	
	static bool OnPlayerTimeCheck( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//玩家聊天；
	static bool OnPlayerChat( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家返回的对服务器网络延时检测包的响应；
	static bool OnNetDetect( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///向玩家返回跳地图出错消息；
	static void SendSwitchMapFailed( CPlayer* pPlayer );

	/// 玩家切换地图
	static bool OnSwitchMapReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///玩家与NPC对话或item对话;
	static bool OnPlayerScriptChat( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///玩家超时被动或主动选择默认复活点复活；
	static bool OnDefaultRebirth( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///玩家重生准备好,发此消息通知SRV，SRV收到此消息后，将玩家Appear消息广播给复活点周围玩家
	static bool OnRebirthReady( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家查询包裹消息；
	static bool OnQueryPkg( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///玩家攻击玩家
	static bool OnAttackPlayer( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );	

	/////AOE攻击
	//static bool OnAoeAttack( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );	

	//@brief:当客户端查询具体的道具包信息时
	static bool  OnQueryItemPkgInfo( CPlayer* pOwner, const char* pPkg ,const unsigned short wPkgLen );

	//@brief:拾取一个道具
	static bool  OnPickOneItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//@brief:拾取包中的所有道具
	static bool  OnPickAllItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//@brief:装备道具
	static bool  OnUseItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	static bool  OnUnUseItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );

	//@brief:当玩家丢弃道具
	static bool  OnDropItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//@brief:交换道具
	static bool  OnSwapItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//激活骑乘道具
	static bool  OnActiveMountItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );

	//当邀请其他玩家到骑乘队伍时
	static bool  OnInvitePlayerToRideTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//当返回邀请骑乘的处理时
	static bool  OnReturnInviteRideTeamResult( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//当申请离开骑乘队伍时
	static bool  OnLeaveRideTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//分解道具
	static bool  OnSplitItem(CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	static bool  OnEquipItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );

	//当申请主动结束BUFF时
	static bool OnReqBuffEnd( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	static bool UseAssistSkill( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	///////////////////////////组队相关/////////////////////////////////////////////////
	//处理CGInvitePlayerJoinTeam
	static bool OnInvitePlayerJoinTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//处理CGRecvInviteJoinTeamFeedBack,组队的反馈
	static bool OnInviteTeamFeedBack( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//要求退出组队
	static bool OnReqQuitTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//要求剔出队员
	static bool OnBanTeamMember( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//要求解散组队
	static bool OnDestoryTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//要求改变组队开关
	static bool OnTeamSwitch( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//要求更换队长
	static bool OnChangeLeader( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//要求更改组队物品分配模式
	static bool OnChangeTeamItemMode( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//要求更改组队经验分配模式
	static bool OnChangeTeamExpMode( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//收到ROLL的回答
	static bool OnCGRollItemFeedBack( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	// 申请入对
	static bool OnCGReqJoinTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//申请入队的反馈
	static bool OnCGRecvReqJoinTeamFeedBack( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//更改ROLL的等级
	static bool OnCGChangeTeamRollLevel( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	///更改入队的屏蔽开关
	static bool OnCGChangeReqSwitch( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );


	///////////////////////好友相关////////////////////////////////////////////
	//加好友的请求
	static bool OnAddFriendReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//接到玩家对加好友请求的返回
	static bool OnRecvAddFriendResponse( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//删除好友
	static bool OnRecvDelFriendReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//创建好友组
	static bool OnCreateFriendGroupReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//删除好友组
	static bool OnDelFriendGroupReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//将好友从一个组，移到另一个组
	static bool OnMoveFriendFromGroupToOther( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//当改变好友组的姓名时
	static bool OnChangeFriendGroupName( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//当好友的聊天时
	static bool OnChatBetweenFriend( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//当加玩家到黑名单时
	static bool OnAddPlayerToBlackList( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//从黑名单删除好友
	static bool OnDelPlayerFromBlackList( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//锁定仇人
	static bool OnLockFoePlayer( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );
	//删除仇人
	static bool OnDeleteFoePlayer( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	///////////////////////物品交易///////////////////////////////////////////////////
	/// 发送交易请求
	static bool OnSendTradeReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	/// 请求交易的结果（ 同意或拒绝）
	static bool OnSendTradeReqResult( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	/// 增加或删除一个物品到交易栏
	static bool OnAddOrDeleteItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	/// 设置金钱
	static bool OnSetTradeMoney( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	/// 取消交易
	static bool OnCancelTrade( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	/// 锁定交易
	static bool OnLockTrade( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	/// 确认交易
	static bool OnConfirmTrade( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );

	//Register( C_G_ITEM_UPGRADE_PLATE, &OnCGItemUpgradePlate );//弹物品升级托盘
	static bool OnCGItemUpgradePlate( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//往托盘中放物品, CGAddSthToPlate
	static bool OnAddSthToPlate( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//从托盘中取物品,CGTakeSthFromPlate
	static bool OnTakeSthFromPlate( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//尝试托盘操作,CGPlateExec
	static bool OnPlateExec( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//关托盘,CGClosePlate
	static bool OnClosePlate( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//修复耐久度
	static bool OnRepairWearByItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//时间流逝性耐久度为0
	static bool OnTimedWearIsZero( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//客户端发送添加新的SP技能的请求???
	static bool OnCGAddSpSkillReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//客户端发来通过技能点升级技能请求
	static bool OnUpgradeSkillReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  );
	//删除任务的请求
	static bool OnDeleteTaskReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//获取客户数据
	static bool OnGetClientData( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//保存客户数据
	static bool OnSaveClientData( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	static bool OnClientSynchTime( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	///////////////////////群聊///////////////////////////////////////////////////
	//创建群聊
	static bool OnCreateChatGroup( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//销毁群聊
	static bool OnDestroyChatGroup( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//加入群聊成员（群主）
	static bool OnAddChatGroupMemberByOwner( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//加入群聊成员(自己)
	static bool OnAddChatGroupMember( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//群聊成员离开
	static bool OnRemoveChatGroupMember( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//群聊
	static bool OnChatGroupSpeak( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//Npc维修耐久度
	static bool OnRepairWearByNpc( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//IRC聊天工具进入世界
	static bool OnIrcEnterWorld( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//查询商店信息；
	static bool OnQueryShop( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//购买商店物品请求；
	static bool OnShopBuyReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//出售商品请求；
	static bool OnItemSoldReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen );

	//更改战斗模式
	static bool OnChangeBattleMode( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//玩家请求加活点；
	static bool OnAddAptReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//玩家使用伤害道具
	static bool OnUseDamageItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//查询恶魔时间
	static bool OnQueryEvilTime( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//查询维修道具的花费
	static bool OnQueryRepairItemCost( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//IRC查询玩家信息
	static bool OnIrcQueryPlayerInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//交换装备栏上的道具
	static bool OnSwapEquipItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//查询道具包对应页数的信息
	static bool OnQueryItemPkgPage( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	///////////////////////工会///////////////////////////////////////////////////
	// 创建工会
	static bool OnCreateUnion( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 解散工会
	static bool OnDestroyUnion( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 查询工会基本信息
	static bool OnQueryUnionBasicInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 查询工会成员信息
	static bool OnQueryUnionMemberInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 增加工会成员请求
	static bool OnAddUnionMemberReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 加入工会邀请的确认
	static bool OnConfirmUnionMemberAddReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 离开工会（主动离开/被驱逐离开）
	static bool OnRemoveUnionMemberReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 修改工会职位对应权限
	static bool OnModifyUnionPosRightReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 增加工会职位
	static bool OnAddUnionPosReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 减少工会职位
	static bool OnRemoveUnionPosReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 转移会长职位
	static bool OnTransformUnionCaptionReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 修改工会成员称号
	static bool OnModifyUninMemberTitleReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 提升工会成员职位
	static bool OnAdvanceUnionMemberPosReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 降低工会成员职位
	static bool OnReduceUninMemberposReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 工会频道聊天
	static bool OnUnionChannelSpeekReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 更新工会职位管理
	static bool OnUpdateUnionPosCfgReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 工会禁言
	static bool OnForbidUnionSpeekReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 发布工会任务
	static bool OnIssueUnionTaskReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 学习工会技能
	static bool OnLearnUnionSkillReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 提升工会等级
	static bool OnAdvanceUnionLevelReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	// 发布工会公告
	static bool OnPostUnionBulletinReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//获取工会日至
	static bool OnQueryUnionLogReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//修改威望
	static bool OnModifyUnionPrestigeReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//修改徽章
	static bool OnModifyUnionBadgeReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );




	//宠物更名
	static bool OnPlayerChangePetName( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//召唤宠物
	static bool OnPlayerSummonPet( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//唤回宠物 
	static bool OnPlayerClosePet( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//设置宠物技能
	static bool OnPlayerChangePetSkillState( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//改变宠物外形
	static bool OnPlayerChangePetImage( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );	

	//依据排行页数查询排行榜
	static bool OnQueryRankInfoByPageIndex( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//依据排行榜的玩家姓名查询
	static bool OnQueryRankInfoByPlayerName( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//已废弃，原ID号改由MOUSE_TIP使用，//动作分割消息
	//static bool OnUsePartionSkill( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//Register( C_G_MOUSETIP_REQ, &OnMouseTipReq );//鼠标停留信息查询(HP|MP)
	static bool OnMouseTipReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	///////////////////////商城///////////////////////////////////////////////////
	//请求帐户信息
	static bool OnAccountInfoReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//购买物品
	static bool OnPurchaseItemReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//vip信息
	static bool OnVipInfoReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//购买vip
	static bool OnPurchaseVipReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//领取Vip物品
	static bool OnFetchVipItemReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//领取非vip物品
	static bool OnFetchNonVipItemReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//赠送
	static bool OnPurchaseGiftReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	////宠物移动
	//static bool OnPetMove( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//玩家请求邮件列表
	static bool OnQueryMailListByPageIndex( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//查询邮件的详细
	static bool OnQueryMailDetialInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//删除邮件
	static bool OnDeleteMail( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	////拾取邮件附属道具
	static bool OnPickMailAttachMoney( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//拾取邮件附属金钱
	static bool OnPickMailAttachItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//发送邮件
	static bool OnSendMailToPlayer( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//删除所有邮件
	static bool OnDeleteAllMail( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家主动退出排队；
	static bool OnCancelQueue( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//右键查询玩家属性
	static bool OnReqPlayerDetailInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//查询套装信息
	static bool OnQuerySuitInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//清空套装信息
	static bool OnClearSuitInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//保存套装信息
	static bool OnSaveSuitInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//一键换装
	static bool OnChanageSuit( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家放弃采集道具
	static bool OnDiscardCollectItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家拾取采集道具
	static bool OnPickCollectItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家拾取所有采集道具
	static bool OnPickAllCollectItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//装备态/时装态切换
	static bool OnEquipFashionSwitch( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//停止变形
	static bool OnEndAnamorphic( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPutItemToProtectPlate( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnConfirmProtectPlate( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnReleaseItemSpecialProtect( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnCancelItemUnSpecialProtectWaitTime( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnTakeItemFromProtectPlate( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnUseSkill( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//Register( C_G_USE_SKILL_2, &OnUseSkill2 );//使用技能 **新技能协议,04.29新方向，线性及其它群攻，由cli传来其预判，服务器验证；
	static bool OnUseSkill2( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnCloseStoragePlate( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen   );

	static bool OnChangeStorageLockState( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnTakeItemFromStorage( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPutItemToStorage( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnTakeItemFromStorageIndexToPkgIndex(  CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPutItemToStorageIndexFromPkgIndex( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSetNewStoragePsd( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnResetStoragePsd( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnTakeItemCheckPassWord( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnChangeStorageLockStateCheckPsd( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnExtendStorage( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSortStorage( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnSwapStorageItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSortPkgItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSortPkgItemBegin( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSortPkgItemEnd( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGMPlayerIDReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGMQueryReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnItemNpcReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnBatchSellItemsReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRepurchaseItemReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnCloseOLTestDlg( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnAnswerQuestion( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerAnswerQuestion( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerDropOLTest( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerSendSysMailToOther( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( C_G_GLOBE_COPYINFO_QUERY, &OnPlayerGlobeCopyInfoQuery );//玩家查询全局副本信息；
	static bool OnPlayerGlobeCopyInfoQuery( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//Register( C_G_TEAM_COPYINFO_QUERY, &OnPlayerTeamCopyInfoQuery );//玩家查询组队副本信息；
	static bool OnPlayerTeamCopyInfoQuery( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//Register( C_G_RESET_COPY, &OnPlayerResetCopy );//玩家请求重置副本；
	static bool OnPlayerResetCopy( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( C_G_TIME_LIMIT_RES, &OnPlayerTimeLimitRes );//玩家点击倒计时确认框；
	static bool OnPlayerTimeLimitRes( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//选择攻城战复活点
	static bool OnSelectWarRebirthOption( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//请求回购列表
	static bool OnRepurchaseItemsListReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//族长选择操作类型
	static bool OnRaceMasterSelectOPType( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//族长对其他种族宣战
	static bool OnRaceMasterDeclareWarToOtherRace( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//族长设置新的公告
	static bool OnRaceMasterSetNewPublicNotice( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//当GM添加新的公告
	static bool OnGMReloadPublicNotices( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//当GM删除公告
	static bool OnGMRemovePublicNotice( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//GM重新加载新的公告
	static bool OnGMAddNewPublicNotice(  CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//GM修改公告
	static bool OnGMModifyPublicNotice( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//GM发布跑马灯
	static bool OnGMAddNewMarqueueNotice( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//GM查询所有的公告信息
	static bool OnGMQueryAllPublicNotices(  CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen ); 

	//返回角色选择
	static bool OnPlayerReturnSelRole( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerQueryMyBidState( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerQueryMyAuctions( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerQueryAuctionByCondition( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerBeginNewAuction( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerCancelMyAuction( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerBidAuctionItem( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnEnterPWMap( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShiftPWListReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShiftPW( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnManualUpgradeReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnAllocatePointReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnModEffectiveSkillID( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryFriendTweetReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryFriendListReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSetViewedConditionOfTweet( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryOfflinePlayerInfo( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen ); 

	static bool OnReplyCallMember( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( C_G_QUERY_NICKUNION, &OnQueryNickUnion );//查询昵称对应玩家；
	static bool OnQueryNickUnion( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家请求开始读条
	static bool OnStartCast( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家请求中断读条
	static bool OnStopCast( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen );

};

