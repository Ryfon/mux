﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/
#include "PkgSendDealDefine.h"

#pragma once


class CNewLoginSrv;
class CDealNewLoginSrvPkg : public IDealPkg<CNewLoginSrv>
{
private:
	static LoginServiceProtocol m_loginProtocol;

private:
	CDealNewLoginSrvPkg() {};
	virtual ~CDealNewLoginSrvPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();

public:
	/////新登录服务器返回对应玩家帐号的随机字符串
	//static bool OnRandomStringReturn( CNewLoginSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	///新登录服务器返回对应玩家帐号的随机字符串以及密钥用随机字符串
	static bool OnRandomNewStringReturn( CNewLoginSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	

	/////新登录服务器返回对应玩家帐号的md5验证结果；
	//static bool OnMd5CheckReturn( CNewLoginSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///新登录服务器返回对应玩家帐号的md5验证结果；
	static bool OnMd5NewCheckReturn( CNewLoginSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

#ifdef ANTI_ADDICTION
	//防沉迷状态通知
	static bool OnGameStateNotify( CNewLoginSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
#endif 
};