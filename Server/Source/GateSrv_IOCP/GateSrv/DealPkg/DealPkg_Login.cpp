﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"
 

//对loginsrv密码验证结果的处理，新旧loginsrv共用；
extern bool LoginPasswordCheckProcess( CPlayer* pPlayer, const char* playerAccount, bool isCheckOK, TYPE_ID sequenceID );

///初始化(注册各命令字对应的处理函数);
void CDealLoginSrvPkg::Init()
{
	Register( L_G_PLAYER_LOGIN_RETURN, &OnPlayerLoginReturn );
}
///loginsrv返回的玩家登录结果；
bool CDealLoginSrvPkg::OnPlayerLoginReturn( CLoginSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerLoginReturn,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(LGPlayerLoginReturn) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const LGPlayerLoginReturn* loginRst = (const LGPlayerLoginReturn*)pPkg;
	if ( SELF_SRV_ID != loginRst->playerID.wGID )
	{
		D_WARNING( "收到loginsrv发来非本GateSrv管理玩家的登录消息\n" );
		return false;
	}

	bool isloginok = ((loginRst->iRet)==0);//0成功,1:失败;

	CPlayer* pPlayer = CManPlayer::FindPlayer( loginRst->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "收到loginsrv发来登录结果消息时，在本GateSrv中找不到对应玩家\n" );
		return false;
	}

	LoginPasswordCheckProcess( pPlayer, loginRst->szAccount, isloginok, 0 );	

	//if ( isloginok ) 	
	//{
	//	if ( !pPlayer->SetCurStat( PS_LOGINED ) )
	//	{
	//		//登录成功，但置状态失败;
	//		D_WARNING( "玩家%s登录成功后置状态失败\n", loginRst->szAccount );
	//		pPlayer->ReqDestorySelf();//断开连接；
	//		return false;
	//	}

	//	//防止dbsrv不区分大小写(大小写不一样的帐号可以登录，
	//	//这样原来存的帐号就与PlayerInfo中的帐号不一致，
	//	//导致下线时centersrv找不到对应玩家从而在线管理出错之类的问题)，
	//	//现在这样做，玩家的帐号经过验证之后将被存成与mysql中大小写一致的帐号；
	//	pPlayer->SetAccount( loginRst->szAccount );
	//	//登录验证成功，向centersrv发在线唯一性验证；
	//	GEPlayerIsExist playerIsExist;
	//	playerIsExist.playerID = pPlayer->GetPlayerID();
	//	SafeStrCpy( playerIsExist.szAccount, pPlayer->GetAccount() );
	//	playerIsExist.accountLen = (int)strlen( playerIsExist.szAccount ) + 1;
	//	SafeStrCpy( playerIsExist.szPlayerName, pPlayer->GetRoleName() );
	//	playerIsExist.nameLen = (int)strlen( playerIsExist.szPlayerName ) + 1;
	//	CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
	//	if ( NULL == pCenterSrv )
	//	{
	//		GCSrvError srvError;
	//		srvError.errNO = 1001;
	//		MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pPlayer, srvError );
	//		pPlayer->SendPkgToPlayer( pErrMsg );//通知玩家没有对应的mapsrv;
	//		D_WARNING( "玩家登录成功后，找不到CenterSrv，在线唯一性验证失败\n\n" );
	//		return false;
	//	}
	//	if ( !pPlayer->SetCurStat( PS_CENTER_CHECKING ) )
	//	{
	//		D_ERROR( "置玩家%s进入centerCheck状态失败\n", pPlayer->GetAccount() );
	//		return false;
	//	}
	//	//D_DEBUG( "玩家%s登录验证成功，进行唯一性验证...\n", pPlayer->GetAccount() );
	//	MsgToPut* pPlayerIsExistQuery = CreateSrvPkg( GEPlayerIsExist, pCenterSrv, playerIsExist );
	//	pCenterSrv->SendPkgToSrv( pPlayerIsExistQuery );//向centersrv发查询消息；
	//	return true;
	//} else {
	//	//登录失败，通知玩家密码错误；
	//	D_DEBUG( "玩家%s登录验证失败，返回错误信息给玩家\n", pPlayer->GetAccount() );
	//	GCPlayerLogin playerLogin;
	//	playerLogin.byIsLoginOK = GCPlayerLogin::ISLOGINOK_FAILED_PWDWRONG;
	//	MsgToPut* pLoginSuc = CreatePlayerPkg( GCPlayerLogin, pPlayer, playerLogin );
	//	pPlayer->SendPkgToPlayer( pLoginSuc );//通知玩家登录失败；
	//	pPlayer->SetCurStat( PS_CONN );//重置玩家状态；
	//	return false;
	//}

	return true;
	TRY_END;
	return false;
}
