﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#pragma once

#include "PkgSendDealDefine.h"

#include "DealPkg_Map.h"
#include "DealPkg_DB.h"
#include "DealPkg_Center.h"
#include "DealPkg_Player.h"
#include "DealPkg_Relation.h"
#include "DealPkg_Login.h"
#include "DealPkg_NewLogin.h"
#include "DealPkg_Rank.h"
#include "DealPkg_StatManager.h"

static UnionCostArg s_unionCostArg;


