﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"




//增加好友的请求
bool CDealPlayerPkg::OnAddFriendReq(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnAddFriendReq,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGAddFriendReq, pClimsg, pPkg, wPkgLen );

	if ( pClimsg->nickNameSize >= MAX_NAME_SIZE )
		return false;

	unsigned int nickNameSize = ( pClimsg->nickNameSize - 1 );
	if ( ACE_OS::strncmp( (char *)pOwner->GetRoleName(), (char *)pClimsg->nickName, nickNameSize ) != 0  )
	{
		GEAddFriend addFriendMsg;
		StructMemSet( addFriendMsg, 0x0, sizeof(GEAddFriend) );
		addFriendMsg.luanchPlayerID = pOwner->GetPlayerID();
		addFriendMsg.groupID = pClimsg->groupid;//好友组ID
		SafeStrCpy( addFriendMsg.launchPlayerName, pOwner->GetRoleName() );
		SafeStrCpy( addFriendMsg.targetPlayerName, pClimsg->nickName );
		SendMsgToCenterSrv( GEAddFriend,addFriendMsg );

		D_DEBUG("向CenterSrv转发加好友请求\n");
	}
	else
	{
		//同名不给添加
		GCAddFriendResponse addFriendResp;
		StructMemSet( addFriendResp, 0x0, sizeof(GCAddFriendResponse) );
		addFriendResp.nErrcode = GCAddFriendResponse::ADD_FRIEND_REFUSED;
		addFriendResp.isAgree = false;

		NewSendPlayerPkg( GCAddFriendResponse, pOwner, addFriendResp );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddFriendResponse, pOwner , addFriendResp );
		//pOwner->SendPkgToPlayer( pNewMsg );
	}

	return true;
	TRY_END;
	return false;
}

//删除好友请求
bool CDealPlayerPkg::OnRecvDelFriendReq(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRecvDelFriendReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGDelFriendReq,  pClimsg, pPkg, wPkgLen );

	D_DEBUG("接到删除好友的请求,好友ID%d\n",pClimsg->friendID );

	GRDelFriend delFriendMsg;

	delFriendMsg.ByAdduiID = pClimsg->friendID;
	delFriendMsg.uiID  = pOwner->GetFullPlayerInfo().baseInfo.uiID;
	SendMsgToRelationSrv( GRDelFriend,delFriendMsg );
	D_DEBUG("向RelationSrv发送删除好友命令 %d del %d\n", delFriendMsg.uiID, delFriendMsg.ByAdduiID );

	return true;
	TRY_END;
	return false;
}

//返回是否同意加为好友
bool CDealPlayerPkg::OnRecvAddFriendResponse(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRecvAddFriendResponse,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddFriendResponse,pClimsg, pPkg, wPkgLen  );
	//如果发过来确实是上次邀请的玩家,则才向relationsrv发送请求

	if( pOwner->IsInFriendInviteList( pClimsg->reqPlayerID ) )
	{
		GEAgreeAddFriendResult reqAddFriendResult;
		reqAddFriendResult.isAddReqer = pClimsg->isAddReqer;
		reqAddFriendResult.errorCode    = (pClimsg->isAgree)? GCAddFriendResponse::ADD_FRIEND_OK : GCAddFriendResponse::ADD_FRIEND_REFUSED;
		reqAddFriendResult.reqPlayerID = pClimsg->reqPlayerID;
		reqAddFriendResult.responsePlayerID = pOwner->GetPlayerID();
		reqAddFriendResult.groupID = pClimsg->beAddedgroupID;
		SafeStrCpy( reqAddFriendResult.launchPlayerName,pClimsg->invitePlayerName );
		SafeStrCpy( reqAddFriendResult.targetPlayerName,pOwner->GetRoleName() );

		D_DEBUG("接到了玩家%s对%s加好友请求的回应:是否同意%d\n",reqAddFriendResult.targetPlayerName,reqAddFriendResult.launchPlayerName,reqAddFriendResult.errorCode );
		SendMsgToCenterSrv( GEAgreeAddFriendResult,  reqAddFriendResult );

		pOwner->RemoveFriendInvitePlayer( pClimsg->reqPlayerID );
	}else
	{
		return false;
	}

	return true;
	TRY_END;
	return false;
}




bool CDealPlayerPkg::OnCreateFriendGroupReq(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnCreateFriendGroupReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGCreateFriendGroupReq, pClimsg, pPkg, wPkgLen );

	D_DEBUG("接到了创建好友组的请求,创建人:%s 组名;%s\n", pOwner->GetRoleName(), pClimsg->groupName );

	GRAddGroup  addGroupMsg;
	StructMemSet( addGroupMsg, 0x0, sizeof(addGroupMsg) );
	SafeStrCpy2( addGroupMsg.GroupName, pClimsg->groupName, addGroupMsg.GroupNameLen );
	addGroupMsg.uiID =  pOwner->GetFullPlayerInfo().baseInfo.uiID;
	SendMsgToRelationSrv(GRAddGroup, addGroupMsg );

	D_DEBUG("向RelationSrv发送创建组的消息%s\n", addGroupMsg.GroupName );

	return true;
	TRY_END;
	return false;

}

bool CDealPlayerPkg::OnDelFriendGroupReq(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnDelFriendGroupReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGDelFriendGroupReq, pClimsg, pPkg, wPkgLen );

	D_DEBUG("接到了删除组的请求 请求人 %s 删除组的ID:%d\n",pOwner->GetRoleName(), pClimsg->groupID );

	GRDelGroup delGroupMsg;
	delGroupMsg.GroupID = pClimsg->groupID;
	delGroupMsg.uiID = pOwner->GetFullPlayerInfo().baseInfo.uiID;
	SendMsgToRelationSrv( GRDelGroup, delGroupMsg );

	D_DEBUG("向RelationSrv发送删除组的消息 组号%d\n",delGroupMsg.GroupID  );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnMoveFriendFromGroupToOther(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnMoveFriendFromGroupToOther,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGMoveFriendGroupReq, pClimsg, pPkg, wPkgLen  );

	D_DEBUG("接到了改变好友组的请求 好友ID:%d 从 组%d移到%d\n",pClimsg->friendID,  pClimsg->orgGroupID, pClimsg->newGroupID );

	GRFriendAlterGroupID alterFriendGroupMsg;
	alterFriendGroupMsg.FriendID = pClimsg->friendID;
	alterFriendGroupMsg.newGroupID = pClimsg->newGroupID;
	alterFriendGroupMsg.preGroupID = pClimsg->orgGroupID;
	alterFriendGroupMsg.uiID = pOwner->GetFullPlayerInfo().baseInfo.uiID;

	SendMsgToRelationSrv( GRFriendAlterGroupID,alterFriendGroupMsg );

	D_DEBUG("向RelationSrv发送了改变好友组的消息\n");

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnChangeFriendGroupName(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnChangeFriendGroupName,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGChgFriendGroupNameReq, pClimsg, pPkg, wPkgLen  );

	D_DEBUG("接到了改变好友组名的请求 组ID:%d 新的组名:%s\n",pClimsg->toupdateFriendGroup.groupID, pClimsg->toupdateFriendGroup.groupName );


	GRAlterGroupName alterGroupName;
	StructMemSet( alterGroupName, 0x0,sizeof(GRAlterGroupName) );
	alterGroupName.GroupID = pClimsg->toupdateFriendGroup.groupID;
	alterGroupName.uiID = pOwner->GetFullPlayerInfo().baseInfo.uiID;

	SafeStrCpy2( alterGroupName.newGroupName, pClimsg->toupdateFriendGroup.groupName, alterGroupName.newGroupNameLen );
	/*alterGroupName.newGroupNameLen = ACE_OS::snprintf(
	alterGroupName.newGroupName,
	sizeof(alterGroupName.newGroupName) ,
	"%s",
	pClimsg->toupdateFriendGroup.groupName
	);

	if ( alterGroupName.newGroupNameLen > pClimsg->toupdateFriendGroup.groupNameSize )
	alterGroupName.newGroupNameLen = pClimsg->toupdateFriendGroup.groupNameSize;*/

	SendMsgToRelationSrv( GRAlterGroupName, alterGroupName );
	D_DEBUG("向RelationSrv发送了改变组姓名的消息\n");

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnChatBetweenFriend(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnChatBetweenFriend,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGFriendChat, pClimsg, pPkg, wPkgLen  );

	D_DEBUG("接到了好友之间的聊天信息 聊天类型:%d 聊天内容:%s\n",pClimsg->friendChatType,pClimsg->chatContent );

	//如果组播
	if ( pClimsg->friendChatType == CGFriendChat::FRIEND_CHAT_GROUP  )
	{
		GRSendGroupMsg groupChatMSG;
		StructMemSet( groupChatMSG, 0x0, sizeof(groupChatMSG) );
		groupChatMSG.uiID    = pOwner->GetFullPlayerInfo().baseInfo.uiID;
		groupChatMSG.GroupID = pClimsg->groupID;
		SafeStrCpy2( groupChatMSG.Msg, pClimsg->chatContent, groupChatMSG.MsgLen );

		/*groupChatMSG.MsgLen = ACE_OS::snprintf(
		groupChatMSG.Msg,
		sizeof(groupChatMSG.Msg),
		"%s",
		pClimsg->chatContent
		);

		if ( (unsigned int)groupChatMSG.MsgLen > pClimsg->chatContentSize )
		groupChatMSG.MsgLen = pClimsg->chatContentSize;*/

		SendMsgToRelationSrv( GRSendGroupMsg,groupChatMSG  );

		D_DEBUG("向RelationSrv发送了好友组播消息\n");

	}//如果当个聊天
	else if( pClimsg->friendChatType == CGFriendChat::FRIEND_CHAT_SINGLE )
	{
		GRSendMsg singleChatMsg;
		StructMemSet( singleChatMsg, 0x0, sizeof(singleChatMsg) );
		singleChatMsg.uiID = pOwner->GetFullPlayerInfo().baseInfo.uiID;
		singleChatMsg.rcvuiID = pClimsg->friendID;
		SafeStrCpy2( singleChatMsg.Msg ,pClimsg->chatContent, singleChatMsg.MsgLen );

		/*singleChatMsg.MsgLen = ACE_OS::snprintf(
		singleChatMsg.Msg,
		sizeof(singleChatMsg.Msg),
		"%s",
		pClimsg->chatContent
		);

		if ( (unsigned int)singleChatMsg.MsgLen > pClimsg->chatContentSize )
		singleChatMsg.MsgLen = pClimsg->chatContentSize;*/

		SendMsgToRelationSrv( GRSendMsg, singleChatMsg );

		D_DEBUG("向RelationSrv发送了好友聊天信息\n");
	}

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnAddPlayerToBlackList(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddPlayerToBlackList,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddBlacklistReq, pClimsg, pPkg, wPkgLen  );

	D_DEBUG("接到了将玩家加入黑名单的消息 消息来源%s 被加的姓名%s\n", pOwner->GetRoleName(), pClimsg->nickName );

	GRAddToBlacklist addBlackList;
	StructMemSet( addBlackList, 0x0, sizeof(addBlackList) );
	addBlackList.uiID = pOwner->GetFullPlayerInfo().baseInfo.uiID;
	SafeStrCpy2( addBlackList.Name ,pClimsg->nickName,addBlackList.NameLen );
	/*addBlackList.NameLen = ACE_OS::snprintf(
	addBlackList.Name,
	sizeof(addBlackList.Name),
	"%s",
	pClimsg->nickName
	);

	if ( addBlackList.NameLen > pClimsg->nickNameSize )
	addBlackList.NameLen = pClimsg->nickNameSize;*/

	SendMsgToRelationSrv( GRAddToBlacklist,addBlackList );

	D_DEBUG("向RelationSrv发送了加入黑名单消息\n");

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnDelPlayerFromBlackList(CPlayer *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnDelPlayerFromBlackList,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGDelBlacklistReq, pSendTradeReq, pPkg, wPkgLen );

	GRDelFromBlacklist delBlackList;
	StructMemSet( delBlackList, 0x0, sizeof(delBlackList) );
	delBlackList.uiID = pOwner->GetFullPlayerInfo().baseInfo.uiID;
	SafeStrCpy2( delBlackList.Name, pSendTradeReq->addBlackName, delBlackList.NameLen );

	/*delBlackList.NameLen = ACE_OS::snprintf(
	delBlackList.Name,
	sizeof(delBlackList.Name),
	"%s",
	pSendTradeReq->addBlackName
	);

	if ( (int)delBlackList.NameLen > pSendTradeReq->addBlackNameLen )
	delBlackList.NameLen = pSendTradeReq->addBlackNameLen;*/

	SendMsgToRelationSrv( GRDelFromBlacklist,delBlackList );

	D_DEBUG("向RelationSrv发送了删除黑名单消息\n");

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnLockFoePlayer(CPlayer *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnLockFoePlayer,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGLockFoeReq, pSendLockFoeReq, pPkg, wPkgLen );

	if( pSendLockFoeReq->bLock )
	{
		GRLockEnemy grLockFoe;
		grLockFoe.LockuiID = pSendLockFoeReq->foeItemID;
		grLockFoe.uiID = pOwner->GetFullPlayerInfo().baseInfo.uiID;
		grLockFoe.IsLock = 0x01;
		SendMsgToRelationSrv( GRLockEnemy,grLockFoe );
		D_DEBUG("向RelationSrv发送锁定请求\n");
	}else
	{
		GRLockEnemy grUnlockFoe;
		grUnlockFoe.LockuiID = pSendLockFoeReq->foeItemID;
		grUnlockFoe.uiID = pOwner->GetFullPlayerInfo().baseInfo.uiID;
		grUnlockFoe.IsLock = 0x00;
		SendMsgToRelationSrv( GRLockEnemy,grUnlockFoe );
		D_DEBUG("向RelationSrv发送解缩请求\n");
	}

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnDeleteFoePlayer(CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	ACE_UNUSED_ARG( pPkg );
	ACE_UNUSED_ARG( wPkgLen );

	if( NULL == pOwner)
	{
		D_ERROR( "OnDeleteFoePlayer,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGDelFoeReq, pSendTradeReq, pPkg, wPkgLen );

	GRDelEnemy grDelFoe;
	grDelFoe.deluiID = pSendTradeReq->foeItemID;
	grDelFoe.uiID = pOwner->GetFullPlayerInfo().baseInfo.uiID;

	SendMsgToRelationSrv( GRDelEnemy, grDelFoe );

	D_DEBUG("向RelationSrv发送删除仇人请求\n");

	return true;
	TRY_END;
	return false;
}
bool CDealPlayerPkg::OnQueryFriendTweetReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "CDealPlayerPkg::OnQueryFriendTweetReq,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGQueryFriendTweetsReq, pCliMsg, pPkg, wPkgLen );

	QueryFriendTweetsRequest req;
	req.gateId = pOwner->GetPlayerID().wGID;
	req.playerId = pOwner->GetPlayerID().dwPID;
	req.targetUiid = pCliMsg->targetUiid;

	SendMsgToRelationSrv(QueryFriendTweetsRequest, req);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnQueryFriendListReq( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "CDealPlayerPkg::OnQueryFriendListReq,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGQueryFriendsListReq, pCliMsg, pPkg, wPkgLen );

	QueryFriendsListRequest req;
	req.gateId = pOwner->GetPlayerID().wGID;
	req.playerId = pOwner->GetPlayerID().dwPID;
	req.targetUiid = pCliMsg->targetUiid;

	SendMsgToRelationSrv(QueryFriendsListRequest, req);

	return true;
	TRY_END;
	return false;
}
