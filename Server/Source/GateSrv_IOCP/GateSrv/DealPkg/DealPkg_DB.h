﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/
#include "PkgSendDealDefine.h"

#pragma once
class CDbSrv;
///mapsrv消息处理类;
class CDealDbSrvPkg : public IDealPkg<CDbSrv>
{
private:
	CDealDbSrvPkg() {};
	virtual ~CDealDbSrvPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
public:
	////DB取信息开始;	Register( D_G_DBGET_PRE, &OnDbGetPre );
	static bool OnDbGetPre( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	////DB取信息结束(与M_G_DBSAVE_POST一一对应);	Register( D_G_DBGET_POST, &OnDbGetPost );
	static bool OnDbGetPost( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	////DB保存信息; 	Register( D_G_DBGET_INFO, &OnDbGetInfo );
	static bool OnDbGetInfo( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public:	
	///dbsrv发来的玩家角色列表信息;
	static bool OnPlayerInfoLogin( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//dbsrv发来的玩家创建角色操作结果
	static bool OnPlayerCreateRole( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//dbsrv发来的玩家删除角色操作结果
	static bool OnPlayerDeleteRole( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///dbsrv发来的玩家角色详细信息;
	static bool OnPlayerInfo( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	/////dbsrv发来的玩家包裹信息
	//static bool OnPlayerPkg( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///获取批量物品流水号
	static bool OnGetBatchItemSeq( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///更新道具信息
	static bool OnUpdateItemInfo( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///获取客户信息
	static bool OnGetClientData( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//查询玩家是否存在的返回
	static bool OnQueryPlayerExistRst( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//查询城市信息的返回
	static bool OnCityInfoResult( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//心跳检查
	static bool OnHeartbeatCheck( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//异常通知
	static bool OnExceptionNotify( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnQueryOffLinePlayerInfoRst( CDbSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
};
