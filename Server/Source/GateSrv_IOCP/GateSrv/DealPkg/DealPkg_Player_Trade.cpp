﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"




////////////////////////////////////////////////交易////////////////////////////////////////////

bool CDealPlayerPkg::OnSendTradeReq( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnSendTradeReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSendTradeReq, pSendTradeReq, pPkg, wPkgLen );

	GMSendTradeReq srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	srvmsg.targetPlayerID = pSendTradeReq->targetPlayerID;

	GateSrvSendMsgToMapSrv(GMSendTradeReq);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnSendTradeReqResult( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnSendTradeReqResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSendTradeReqResult, pSendTradeReqResult, pPkg, wPkgLen );

	GMSendTradeReqResult srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();

	PlayerID tempID;
	tempID.wGID = 0;
	tempID.dwPID = 0;

	srvmsg.targetPlayerID = tempID;
	if(pSendTradeReqResult->tResult == CGSendTradeReqResult::AGREE)
		srvmsg.bAgree = true;
	else
		srvmsg.bAgree = false;

	GateSrvSendMsgToMapSrv(GMSendTradeReqResult);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnAddOrDeleteItem( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddOrDeleteItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGAddOrDeleteItem, pAddOrDeleteItem, pPkg, wPkgLen );

	GMAddOrDeleteItem srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	srvmsg.tOperateType = (GMAddOrDeleteItem::eOperateType)(pAddOrDeleteItem->tResult);
	srvmsg.uiItemID = pAddOrDeleteItem->uiID;

	GateSrvSendMsgToMapSrv(GMAddOrDeleteItem);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnSetTradeMoney( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnSetTradeMoney,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGSetTradeMoney, pSetTradeMoney, pPkg, wPkgLen );

	GMSetTradeMoney srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	srvmsg.uiGoldMoney = pSetTradeMoney->uiGoldMoney;

	GateSrvSendMsgToMapSrv(GMSetTradeMoney);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnCancelTrade( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnCancelTrade,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGCancelTrade, pCancelTrade, pPkg, wPkgLen ); ACE_UNUSED_ARG( pCancelTrade );

	GMCancelTrade srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();

	GateSrvSendMsgToMapSrv(GMCancelTrade);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnLockTrade( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnLockTrade,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGLockTrade, pLockTrade, pPkg, wPkgLen );

	GMLockTrade srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();
	if(pLockTrade->tType == CGLockTrade::LOCK )
		srvmsg.bLock = true;
	else
		srvmsg.bLock = false;

	GateSrvSendMsgToMapSrv(GMLockTrade);

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnConfirmTrade( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnConfirmTrade,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	DealPlayerPkgPre( CGConfirmTrade, pConfirmTrade, pPkg, wPkgLen ); ACE_UNUSED_ARG( pConfirmTrade );

	GMConfirmTrade srvmsg;
	srvmsg.selfPlayerID = pOwner->GetPlayerID();

	GateSrvSendMsgToMapSrv(GMConfirmTrade);

	return true;
	TRY_END;
	return false;
}
