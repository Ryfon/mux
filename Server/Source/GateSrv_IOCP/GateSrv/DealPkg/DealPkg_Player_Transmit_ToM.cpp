﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"
 


///Register( C_G_USE_MONEY_TYPE, &OnUseMoneyType );//玩家切换所使用的货币
bool CDealPlayerPkg::OnUseMoneyType( CPlayer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnUseMoneyType,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGUseMoneyType, pUseMoneyType, pPkg, wPkgLen );

	if ( !pOwner->IsInFighting() )
	{
		D_WARNING( "玩家尚未进入战斗状态，不应该切换所使用的货币\n" );
		return false;
	}

	//消息转发mapsrv;
	GMUseMoneyType  srvmsg;
	srvmsg.moneyType = pUseMoneyType->moneyType;
	srvmsg.playerID = pOwner->GetPlayerID();//填入玩家在本GateSrv上的ID号；
	CMapSrv* pMapsrv = pOwner->GetMapSrv();//找到mapserver，将相应包转发出去；
	if ( NULL != pMapsrv )
	{
		GateSrvSendMsgToMapSrv( GMUseMoneyType );
	} else {
		D_WARNING( "收到切换货币消息时，找不到玩家所在的mapsrv:%d\n\n", pOwner->GetMapSrvID() );
	}

	return true;
	TRY_END;
	return false;
}