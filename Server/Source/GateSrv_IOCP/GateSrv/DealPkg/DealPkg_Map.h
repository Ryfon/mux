﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/
#include "PkgSendDealDefine.h"

#pragma once
 
class CMapSrv;
///mapsrv消息处理类;
class CDealMapSrvPkg : public IDealPkg<CMapSrv>
{
private:
	CDealMapSrvPkg() {};
	virtual ~CDealMapSrvPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();

public:
	///接收到mapsrv通知其自身是否可进消息；
	static bool OnMGMapIsFull( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//Register( M_G_MAPSRV_READY, &OnMGMapSrvReady );//对应mapsrv准备好；
	static bool OnMGMapSrvReady( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//Register( M_G_RANK_CACHE_INFO, &OnRankCacheInfo );//mapsrv发来缓存更新信息，转给functionsrv;
	static bool OnRankCacheInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

public:
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	///Register( M_G_DBSAVE_PRE,  &OnMGDbSavePre );
	static bool OnMGDbSavePre( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///Register( M_G_DBSAVE_INFO, &OnMGDbSaveInfo );
	static bool OnMGDbSaveInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///Register( M_G_DBSAVE_POST, &OnMGDbSavePost );
	static bool OnMGDbSavePost( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///Register( M_G_PLAYER_LEAVE_MAP, &OnMGPlayerLeaveMap );
	static bool OnMGPlayerLeaveMap( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	///Register( M_G_NDSMR_INFO, &OnMGNDSMRInfo );//非存盘,但跳地图需保存信息;
	static bool OnMGNDSMRInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public:    
    ///mapsrv通知gatesrv自身管理的地图列表；
	static bool OnManedMaps( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

    ///mapsrv通知gatesrv错误发生；
	static bool OnMGError( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///Register( M_G_USE_MONEY_TYPE, &OnUseMoneyType );//玩家切换所使用的货币
	static bool OnUseMoneyType( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///Register( M_G_HONOR_MASK, &OnHonorMask );//玩家切换所要显示的称号
	static bool OnHonorMask( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///mapsrv发来的玩家消息处理;
	static bool OnPlayerRun( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	
	///mapsrv发来的玩家出现消息处理;
	//static bool OnPlayerAppear( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	
	///mapsrv发来的玩家离线消息处理;
    static bool OnPlayerLeave( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	
	//Register( M_G_ENVNOTI_FLAG, &OnEnvNotiFlag );//玩家周围信息通知开始或结束标记；
	static bool OnEnvNotiFlag( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///mapsrv发来的其它玩家消息处理;
    static bool OnOtherPlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///Register( M_G_SIMPLE_PLAYER_INFO, &OnSimplePlayerInfo );//通知玩家，其它玩家简略信息(广播)；
    static bool OnSimplePlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///Register( M_G_SIMPLEST_INFO, &OnSimplestInfo );//通知玩家，其它玩家最简信息(聚集情形)；
    static bool OnSimplestInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//踢除玩家
	static bool OnKickPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//通知玩家或怪物位置信息，用于纠正客户端位置；
	static bool OnPlayerPosInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///mapsrv发来的玩家信息更新消息;
    //static bool OnPlayerNewInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///mapsrv发来的玩家包裹更新消息;
    //static bool OnPlayerNewPkg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///mapsrv发来的玩家聊天消息;
    static bool OnPlayerChat( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	
	/////临时协议，mapsrv发来的玩家连击消息处理;
 //   static bool OnPlayerCombo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	

	///mapsrv发来的怪物出生消息;
    static bool OnMonsterBorn( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
    ///mapsrv发来的怪物移动消息;
    static bool OnMonsterMove( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//mapsrv发来怪物消失消息
    static bool OnMonsterDisappear( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///mapsrv发来的玩家攻击怪物消息;
    static bool OnPlayerAttackTarget( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
    ///mapsrv发来的怪物攻击玩家消息;
    static bool OnMonsterAttackPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
    /////mapsrv发来的玩家攻击玩家消息;
    //static bool OnPlayerAttackPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

    ///mapsrv发来的怪物信息;
    static bool OnMonsterInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家角色信息更新；
    static bool OnRoleattUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家升级；
    static bool OnPlayerLevelUp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家非存盘信息更新；
	static bool OnPlayerTmpinfoUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///// 玩家切换地图
	//static bool OnSwitchMap( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///玩家完整信息
	static bool OnFullPlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///玩家进地图结果；
	static bool OnEnterMapRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

    ///玩家最终复活结果；MGFinalRebirthRst
	static bool OnFinalRebirthRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///通知玩家进入死亡倒计时;
    static bool OnPlayerDying( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///通知玩家复活点信息；
	static bool OnRebirthInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	/// 获取批量物品流水号
	static bool OnGetBatchItemSeq( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	/// 更新道具信息
	//static bool OnUpdateItemInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//@brief:当道具包出现时
	static bool  OnItemPkgAppear( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//@brief:当玩家查询道具包
	static bool  OnItemPkgInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//@brief:当道具包过期消失时
	static bool  OnItemPkgDisAppear( CMapSrv* pOwer, const char* pPkg ,const unsigned short wPkgLen );

	//@brief:查询道具包时，返回武器信息
	//static bool  OnItemPkgWeaponInfo( CMapSrv* pOwer, const char* pPkg ,const unsigned short wPkgLen );

	//@brief:查询道具包时，返回所有装备信息
	//static bool  OnItemPkgEquipInfo( CMapSrv* pOwer, const char* pPkg ,const unsigned short wPkgLen );

	////@brief:拾取道具成功
	//static bool  OnPickItemSuccess( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//@brief:拾取道具失败
	static bool  OnPickItemFailed( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//@brief:当一个玩家拾取道具时
	static bool  OnOnePlayerPickItem(  CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//@brief:一个玩家使用了一个道具
	static bool  OnPlayerUseItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//@brief:玩家使用道具失败
	static bool  OnPlayerUseItemFailed( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//@brief:一个玩家不使用一个道具
	static bool  OnPlayerUnUseItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool  OnPlayerSetPkgItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///返回玩家的详细信息
	//static bool  OnReturnItemDetialInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///返回玩家装备的简明信息
	//static bool  OnReturnEquipConciseInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///返回玩家包裹的简明信息
	static bool  OnReturnItemPkgConciseInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///返回玩家丢弃道具的结果
	static bool  OnReturnDropItemResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerBuffStart( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerBuffEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerBuffUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnMonsterBuffStart( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnMonsterBuffUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnMonsterBuffEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	/////返回玩家的金钱更新信息
	//static bool OnPlayerMoneyUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//返回地图的骑乘队伍
	static bool OnReturnRideTeamAppear( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///返回邀请骑乘的结果
	static bool OnReturnInviteRideTeamResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///返回队伍解散的消息
	static bool OnRideTeamDissolve( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///当骑乘状态被改变时
	static bool OnRideStateChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRideLeaderSwitchMap(  CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//被人邀请，加入骑乘队伍
	static bool OnBeInviteJoinRideTeam( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnReqBuffEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnBroadcast( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUseAssistSkill( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///设置玩家新位置(地图内跳转，只通知跳转者本人);
    static bool OnSetPlayerPos( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	////关于组队组员的信息/////////
	static bool OnMGTeamMemberDetailInfoUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///关于ROLL的结果
	static bool OnMGRollItemResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//关于MAPSRV发来要求ROLL的信息
	static bool OnMGReqAgreeRollItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//Roll点的结果返回
	static bool OnReturnRollNumResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//组队成员拾取物品的通知
	static bool OnRecvTeamMemberPickItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnNoticeTeamPlayerGetNewItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 发送交易请求错误
	static bool OnSendTradeReqError( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 收到交易请求
	static bool OnRecvTradeReq( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 请求交易的结果
	static bool OnTradeReqResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 交易栏中增加或删除一个物品
	static bool OnAddOrDeleteItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 交易金钱
	static bool OnSetTradeMoney( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 取消交易
	static bool OnCancelTrade( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 锁定交易
	static bool OnLockTrade( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 确定交易
	static bool OnConfirmTrade( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///指示客户端显示托盘；MGShowPlate
	static bool OnShowPlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///往托盘放置物品结果；MGAddSthToPlateRst
	static bool OnAddSthToPlateRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///从托盘取物品结果；MGTakeSthFromPlateRst
	static bool OnTakeSthFromPlateRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///托盘执行结果；MGPlateExecRst
	static bool OnPlateExecRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///对玩家关托盘的响应，或者主动关玩家托盘；MGClosePlate
	static bool OnClosePlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///给玩家添加了一个新任务；//MGPlayerNewTask
	static bool OnPlayerNewTask( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///通知玩家任务新状态；//MGPlayerTaskStatus
	static bool OnPlayerTaskStatus( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///怪物或其它玩家离开玩家视野,MGLeaveView;
	static bool OnLeaveView( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//通知gatesrv增加仇人信息
	static bool OnAddFoeInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//修复耐久度
	static bool OnRepairWearByItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//通知耐久度发生变化
	static bool OnNotifyWearChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerIsBusying( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//SP技能升级
	static bool OnSpSkillUpgrade( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//添加SP技能请求的回应
	static bool OnAddSpSkillResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//传来mapsrv
	static bool OnThreatList( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnChangeItemCri( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//关于更新技能
	static bool OnUpdateSkillResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
    //更新任务收集器的作用
	static bool OnUpdateTaskCounterNum( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//当更新玩家的任务记录时
	static bool OnTaskHistoryReturn( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//玩家装备道具，或卸载道具时
	static bool OnNoticePlayerEquipItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//玩家任务信息的通知
	static bool OnShowTaskUIMsg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//通知玩家洗点成果
	static bool OnCleanSkillPoint( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//npc维修耐久度
	static bool OnRepairWearByNpc( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//显示NPC商店
	static bool OnShowShop( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//NPC商店购买结果
	static bool OnShopBuyRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
    //商店售出结果
	static bool OnItemSoldRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//收到战斗模式的改变结果
	static bool OnChangeBattleMode( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//收到玩家使用恢复道具
	static bool OnPlayerUseHealItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//添加活点结果；
	static bool OnAddAptRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//查询的恶魔时间的返回
	static bool OnQueryEvilTimeRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//自身的好友信息更新
	static bool OnFriendInfoUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//查询修复道具的金钱
	static bool OnRepairItemMoneyRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//身上装备道具的升级
	static bool OnEquipItemUpDate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//跳转地图的实现
	static bool OnSwichMapReq( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//static bool OnInfoUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnBuffData( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnTargetGetDamage( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//交换装备栏道具的结果
	static bool OnSwapEquipItemRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnChangeTeamState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//天谴相关
	static bool OnPunishmentStart( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnPunishmentEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnBroadcastPunishInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnQueryPunishPlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//GM工具相关
	static bool OnGmCmdRequest( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnGmCmdResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//宠物相关 
	//宠物不稳信息(只发给宠物主人)
	static bool OnPetinfoInStable( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//宠物能力信息(只发给宠物主人)
	static bool OnPetAbility( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//宠物稳定信息(发给宠物主人周围其它玩家)
	static bool OnPetinfoStable( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//static bool OnSummonPet( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	//组播宠物召唤
	//static bool OnClosePet( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );		//组播唤回宠物
	//static bool OnChangePetName( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	//组播或者单发更改宠物姓名
	//static bool OnChangePetSkillState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );		//更改宠物技能状态
	//static bool OnChangePetImage( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );			//更改宠物外形
	//static bool OnUpdatePetExp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );			//宠物经验值的更新
	//static bool OnPetInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );			//组播宠物信息
	//static bool OnSelfPetInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );			//只发给宠物拥有者本人的详细宠物信息
	//static bool OnPetLevelUp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );		//宠物升级
	//static bool OnUpdatePetHungerDegree( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );		//宠物更新饥饿度
	//static bool OnUpdatePetState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );		//宠物更新状态

	////更新排行榜的最新信息
	//static bool OnUpdateRankChartInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );//更新排行榜服务器的信息
	////排行榜玩家上线
	//static bool OnRankPlayerUpLine( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//排行榜玩家下线
	static bool OnRankPlayerOffLine( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnRemoveRankPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	
	//显示已发布任务
	static bool OnDisplayUnionIssueTask( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//工会威望修改
	static bool OnChanageUnionPrestige( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//工会活跃度修改
	static bool OnChangeUnionActive( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//工会成员在场景中的显示信息
	static bool OnUnionMemberShowInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//关卡更新消息
	static bool OnUpdateCityInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );


#ifdef WAIT_QUEUE//排队相关
	///mapsrv通知排队次序已到，可以进map;
	static bool QueueOK( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///mapsrv通知排队信息;
	static bool OnGetQueryQueueResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
#endif

	//攻城相关
	static bool OnAttackCityBegin( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnAttackCityEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnUpdateWarTaskId( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnNotifyWarTaskState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	////使用UseSkill2后废弃，原ID号由MOUSE_TIP使用, by dzj, 10.05.31, static bool OnUsePartionSkillRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnMGMouseTip( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );//对鼠标停留信息查询的响应

	//	static bool OnPetMove( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//static bool OnRaceGroupNotify( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );		//种族的群发，天谴用

	//商城
	static bool OnCheckPkgFreeSpaceRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );		//检查包裹空闲空间
	static bool OnFetchVipItemRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );			//领取vip物品
	static bool OnFetchNonVipItemRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );		//领取非vip物品

	//邮件
	static bool OnPickMailItemRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );//玩家拾取包裹中的物品
	static bool OnPickMailMoneyRst( CMapSrv* pOwner, const char* pPkg,const unsigned short wPkgLen );//玩家拾取包裹中的金钱

	////全世界系统通知
	//static bool OnWorldNotify( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//右键查看玩家属性
	static bool OnReqPlayerDetailInfoRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//查询套装信息
	static bool OnQuerySuitInfoRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//清空套装信息
	static bool OnClearSuitInfoRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//保存套装信息
	static bool OnSaveSuitInfoRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//一键换装
	static bool OnChanageSuitRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//当玩家在NPC处打开邮箱时
	static bool OnPlayerShowMailBox( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//更新GATE上的善恶值
	static bool OnGateUpdateGoodEvilPoint( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//获得采集信息
	static bool OnCollectTaskRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnShowProtectPlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPutItemToProtectPlateRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdateProtectItemProp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	
	static bool OnReleaseItemSpecialProtectRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnCancelItemUnSpecialProtectWaitTimeRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnTaskItemFromProtectPlateRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnTryExecProtectPlateRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRecvItemWithProtectProp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnConfirmProtectPlateRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnTakeItemFromProtectPlateRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnLoadRankChartInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRankPlayerLevelUp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnRankPlayerRankInfoUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnUpdatePlayerItemInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerBaseInfoChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdatePlayerTaskRdInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnChangeTargetName( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnKickAllPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnNotifyActivityState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnNotifyNewTaskRecord( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdateNewTaskRecordInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSendMailToPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnExceptionNotify( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSendSpecialString( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUseSkillRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( M_G_RST_TO_ATKER, &OnRstToAtker );		//使用技能给攻击者的消息
	static bool OnRstToAtker( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//Register( M_G_NORMAL_ATTACK, &OnNormalAttack );		//使用普通技能给周围人的消息
	static bool OnNormalAttack( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//Register( M_G_SPLIT_ATTACK, &OnSplitAttack );		//使用分割技能给周围人的消息
	static bool OnSplitAttack( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSaveRebirthPosNtf( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUnionMultiReq( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShowStoragePlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnStorageSafeInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnStorageItemInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShowStoragePassWordDlg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnChangeStorageLockState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdateStorageValidRow( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdateStorageSecurityStrategy( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdateStorageItemInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnTakeItemFromStorageRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPutItemToStorageRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnSetNewPassWordRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnResetStoragePsdRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	
	static bool OnTakeItemCheckPsdRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnExtendStorageRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryStorageInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdateFunctionSrvStorageForbiddenInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdateFunctionSrvStorageItemInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdateFunctionSrvStorageSafeInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGMQueryResp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnNotifyRookieState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ); 

	static bool OnItemNpcStatusInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ); 

	static bool OnItemNpcID( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ); 

	static bool OnBatchSellItemsResp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRepurchaseItemResp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ); 
	
	static bool OnRepurchaseItemListNtf( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ); 

	static bool OnCloseOLTestDlg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShowOLTestProvueDlg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShowOLTestQuestion( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnAnswerQuestionRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnAnswerAllQuestion( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnSplitItemError( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnLogPlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	///玩家试图进副本请求；
	static bool OnPlayerTryCopyQuest( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );//玩家试图进副本请求；

	///玩家试图离开副本请求；
	static bool OnPlayerLeaveCopyQuest( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );//玩家试图离开副本请求；

	///副本不可再进指令；
	static bool OnCopyNomorePlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );//副本不可再进指令；

	///发往center的删除副本请求(在center发往mapsrv的删除副本请求之后，因为mapsrv收到那个请求后会先踢玩家，踢完后再发此G_E_DEL_COPY_QUEST给center，最后center发delcopy cmd给map)；	
	static bool OnDelCopyQuest( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );///发往center的删除副本请求(在center发往mapsrv的删除副本请求之后，因为mapsrv收到那个请求后会先踢玩家，踢完后再发此G_E_DEL_COPY_QUEST给center，最后center发delcopy cmd给map)；	

	///mapsrv玩家的outmapinfo改变；
	static bool OnPlayerOutMapInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );//mapsrv玩家的outmapinfo改变；

	///mapsrv对玩家查询伪副本信息的反馈；	
	static bool OnMGGlobeCopyInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );//mapsrv对玩家查询伪副本信息的反馈；

	///Register( M_G_TIMELIMIT, &OnMGTimeLimit );//mapsrv发来玩家倒计时框；
	static bool OnMGTimeLimit( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );//mapsrv发来玩家倒计时框；

	///
	static bool OnPlayerWarDying( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	
	///
	static bool OnWarRebirthOptionErr( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnRaceMasterTimeInfoExpire( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterDeclareWarInfoChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterTimeInfoChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterPublicMsgChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//static bool OnNoticeRaceMasterPublicMsg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnSaveRaceMasterInfoToDB( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterDeclareWarRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterGetSalaryRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterSetNewPublicMsgRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShowRaceMasterPlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShowRaceMasterDeclareWarPlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShowRaceMasterPublicMsgPlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnCloseRaceMasterPlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnShowRaceMasterAssignCityPlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnMapSrvLoadRaceMasterInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnResetRaceMasterDeclareWar( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//处理buff
	static bool OnBuffStart( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnBuffEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnBuffStatusUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnBuffHpMpUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPublicNoticeModify(  CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnPublicNoticeAdd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnPublicNoticeRemove(  CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnPublicMarqueueAdd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnParamMsgAdd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnRecvShowTempNotice( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//显示timer
	static bool OnDisplayWarTimer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//显示counter
	static bool OnDisplayWarCounter( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//角色反弹
	static bool OnCharacterDamageRebound( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///阻挡区状态变化
	static bool OnBlockQuadStateChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//当开始选族长时
	static bool OnStartSelectRaceMaster( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//中转工会操作的结果
	static bool OnForwardUnionCmdRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//返回角色选择
	static bool OnReturnSelRole( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//创建工会的消耗参数(用来创建失败时发送邮件)
	static bool OnUnionCostArg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerStartNewAuctionRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerStartNewAuctionNext( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerBidAuctionItemNext( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerBidItemErr( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerShowAuctionPlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//
	static bool OnReportPWInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnReportPWMapInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnModPWStateForward( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnModPWMapStateForward( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnShiftPWForward( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnKickOffPWForward( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnModEffectiveSkillIDRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnAddFriendDegreeReq( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnAddTweetNtf( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnFrinedNonTeamExpNtf( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnExpOfLevelUpRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnInviteCallMember( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//通知玩家开始读条
	static bool OnStartCast( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//通知玩家中断读条
	static bool OnStopCast( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家移动失败
	static bool OnRunFail( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

private:
	///玩家下线时的更新消息处理；
    static bool OfflineInfoProc( CMapSrv* pMapSrv, const MGFullPlayerInfo* pMGFullPlayerInfo );

public:
	static CMapSrv* GetMapSrvByMapcode( unsigned long mapCode )
	{
		map<unsigned long, CMapSrv*>::iterator iter = m_mapMapcodeToMapSrv.find( mapCode );
		if ( iter != m_mapMapcodeToMapSrv.end() )
		{
			return iter->second;
		} else {
			return NULL;
		}
	}
	
private:
	static map< unsigned long, CMapSrv* > m_mapMapcodeToMapSrv;
};
