﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/
#include "PkgSendDealDefine.h"

#pragma once
class CCenterSrv;
///centersrv消息处理类;
class CDealCenterSrvPkg : public IDealPkg<CCenterSrv>
{
private:
	CDealCenterSrvPkg() {};
	virtual ~CDealCenterSrvPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();

public:
	///centersrv返回的玩家唯一性验证结果，如果之前该角色不在线，则返回成功，否则返回失败；
	static bool OnPlayerIsExist( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///centresrv转发组队消息 
	static bool OnInvitePlayerJoinTeam( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///centresrv转发组队消息
	static bool OnInviteError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///转发过来的组队成功消息
	static bool OnInviteSuccess( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///centersrv发来的世界聊天消息EGSrvGroupChat；
	static bool OnSrvGroupChat( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//center发往gate密聊聊天消息，gate收到后应转发给密聊对象玩家,StrEGPrivateChat
	static bool OnPrivateChat( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///接到加好友的请求
	static bool OnRecvAddFriendReq( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///接到加好友出错的消息
	static bool OnRecvAddFriendError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///接到加好友成功的消息
	static bool OnRecvAddFriendOk( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///接到申请入队的消息
	static bool OnEGReqJoinTeam( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///接到申请入队错误的消息
	static bool OnEGReqJoinError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///接到反馈信息
	static bool OnFeedbackError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///聊天错误
	static bool OnChatError( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///加入群聊成员(群主)
	static bool OnAddChatGroupMemberByOwner( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///IRC查询好友仇人信息
	static bool OnIrcQueryPlayerInfo( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///IRC查询好友仇人信息的结果
	static bool OnIrcQueryPlayerInfoRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///查询天谴的结果
	static bool OnCheckPunishmentResult( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//GM命令的反馈
	static bool OnGmCmdResult( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGmCmdRequest( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//static bool OnRaceGroupNotify( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//查询是否玩家有未读邮件
	static bool OnRecvNoticePlayerHaveUnReadMail( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//全服通知活动状态
	static bool OnNotifyActivityState( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnGMQueryTargetCheckRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( E_G_NEW_COPY_CMD, &OnNewCopyCmd );//通知创建新副本；
	static bool OnNewCopyCmd( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( E_G_DEL_COPY_CMD, &OnDelCopyCmd );//通知删除旧副本；
	static bool OnDelCopyCmd( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( E_G_DEL_COPY_QUEST, &OnDelCopyQuest );//centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
	static bool OnDelCopyQuest( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( E_G_COPY_RES_RST, &OnCopyResRst );//返回给gate的副本相关操作结果
	static bool OnCopyResRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( E_G_ENTER_COPY_CMD, &OnEnterCopyCmd );//指令玩家进入副本；
	static bool OnEnterCopyCmd( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( E_G_LEAVE_COPY_CMD, &OnLeaveCopyCmd );//指令玩家离开副本
	static bool OnLeaveCopyCmd( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///通知玩家离开副本组队(删去玩家在各副本中的第一次进入信息，并从当前副本中踢出去)，会针对副本组队拥有的每个副本发送一次；
	static bool OnLeaveSTeamNoti( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( E_G_GLOBE_COPYINFOS, &OnGlobeCopyInfos );//收到center发来的全局副本信息查询结果；
	static bool OnGlobeCopyInfos( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	///Register( E_G_TEAM_COPYINFOS, &OnTeamCopyInfos );//收到center发来的组队副本信息查询结果；
	static bool OnTeamCopyInfos( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnEnterPWMapCheckRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnModifyPWStateRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnModifyPWstateNtf( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnModifyPWMapStateRst( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnModifyPWMapStateNtf( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	static bool OnPWMapPlayerNumNtf( CCenterSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
};
