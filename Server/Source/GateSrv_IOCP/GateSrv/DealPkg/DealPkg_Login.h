﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/
#include "PkgSendDealDefine.h"

#pragma once


class CLoginSrv;
///centersrv消息处理类;
class CDealLoginSrvPkg : public IDealPkg<CLoginSrv>
{
private:
	CDealLoginSrvPkg() {};
	virtual ~CDealLoginSrvPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();

public:
	///loginsrv返回的玩家登录结果；
	static bool OnPlayerLoginReturn( CLoginSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
};