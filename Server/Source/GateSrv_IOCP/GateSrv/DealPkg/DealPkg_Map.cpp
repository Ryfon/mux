﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"


map< unsigned long, CMapSrv* > CDealMapSrvPkg::m_mapMapcodeToMapSrv; 

void CDealMapSrvPkg::Init()
{
	Register( M_G_MAP_ISFULL, &OnMGMapIsFull );
	Register( M_G_MAPSRV_READY, &OnMGMapSrvReady );//对应mapsrv准备好；

	///////////////////////////////////////////////////////////////////////////
	Register( M_G_RANK_CACHE_INFO, &OnRankCacheInfo );//mapsrv发来缓存更新信息，转给functionsrv;
	///////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	Register( M_G_DBSAVE_PRE,  &OnMGDbSavePre );
	Register( M_G_DBSAVE_INFO, &OnMGDbSaveInfo );
	Register( M_G_DBSAVE_POST, &OnMGDbSavePost );
	Register( M_G_PLAYER_LEAVE_MAP, &OnMGPlayerLeaveMap );
	Register( M_G_NDSMR_INFO, &OnMGNDSMRInfo );//非存盘,但跳地图需保存信息;
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Register( M_G_ERROR, &OnMGError );//mapsrv通知gatesrv错误发生；
	Register( M_G_RUN, &OnPlayerRun );//玩家跑动；

	Register( M_G_USE_MONEY_TYPE, &OnUseMoneyType );//玩家切换所使用的货币
	Register( M_G_HONOR_MASK, &OnHonorMask );//玩家切换所要显示的称号

	//Register( M_G_PLAYER_APPEAR, &OnPlayerAppear );//玩家出现；
	Register( M_G_PLAYER_LEAVE, &OnPlayerLeave );//玩家离开；
	Register( M_G_ENVNOTI_FLAG, &OnEnvNotiFlag );//玩家周围信息通知开始或结束标记；

	Register( M_G_OTHER_PLAYER_INFO, &OnOtherPlayerInfo );//通知玩家，其它玩家信息；
	Register( M_G_SIMPLE_PLAYER_INFO, &OnSimplePlayerInfo );//通知玩家，其它玩家简略信息(广播)；
	Register( M_G_SIMPLEST_INFO, &OnSimplestInfo );//通知玩家，其它玩家最简信息(聚集情形)；


	Register( M_G_KICK_PLAYER, &OnKickPlayer );
	//Register( M_G_PLAYER_COMBO, &OnPlayerCombo );//临时协议，玩家连击；
	Register( M_G_PLAYER_POSINFO, &OnPlayerPosInfo );//通知玩家或怪物位置信息，用于纠正客户端位置；

	//Register( M_G_PLAYER_NEWINFO, &OnPlayerNewInfo );//Mapsrv更新玩家信息；
	//Register( M_G_PLAYER_NEWPKG, &OnPlayerNewPkg );//Mapsrv更新玩家信息；

	Register( M_G_CHAT, &OnPlayerChat );//聊天

	Register( M_G_MONSTER_BORN, &OnMonsterBorn );//怪物出生；
	Register( M_G_MONSTER_MOVE, &OnMonsterMove );//怪物移动；
	Register( M_G_MONSTER_DISAPPEAR, &OnMonsterDisappear );//怪物消失；
	Register( M_G_PLAYER_ATTACK_TARGET, &OnPlayerAttackTarget );//玩家打怪物；
	Register( M_G_MONSTER_ATTACK_PLAYER, &OnMonsterAttackPlayer );//怪物打玩家；

	Register( M_G_MONSTER_INFO, &OnMonsterInfo );//怪物信息；
	Register( M_G_ROLEATT_UPDATE, &OnRoleattUpdate );//玩家角色信息更新；

	Register( M_G_PLAYER_LEVELUP, &OnPlayerLevelUp );//玩家升级；

	Register( M_G_TMPINFO_UPDATE, &OnPlayerTmpinfoUpdate );//玩家非存盘信息更新；

	Register( M_G_MANED_MAP, &OnManedMaps );//mapsrv发来的该mapsrv管理的mapg列表；

	//Register( M_G_PLAYER_ATTACK_PLAYER, &OnPlayerAttackPlayer );//mapsrv发来的玩家攻击玩家消息；

	//Register( M_G_SWITCH_MAP, &OnSwitchMap);//地图切换

	Register( M_G_FULL_PLAYER_INFO, &OnFullPlayerInfo );//玩家完整信息；

	Register( M_G_ENTERMAP_RST, &OnEnterMapRst );//玩家进地图结果；

	Register( M_G_FINAL_REBIRTH_RST, &OnFinalRebirthRst );//玩家最终复活结果；MGFinalRebirthRst

	Register( M_G_PLAYER_DYING, &OnPlayerDying );//通知玩家进入死亡倒计时；

	Register( M_G_REBIRTH_INFO, &OnRebirthInfo );//通知玩家复活点信息；

	Register( M_G_BATCH_ITEM_SEQUENCE, &OnGetBatchItemSeq );// 获取批量物品流水号

	//Register( M_G_UPDATE_ITEM_INFO, &OnUpdateItemInfo );//更新道具信息

	Register( M_G_ITEMPACK_APPEAR, &OnItemPkgAppear );//通知周围的玩家道具包出现了

	Register( M_G_ITEMPACK_INFO, &OnItemPkgInfo );//返回麽个道具包的里的道具信息

	Register( M_G_ITEMPACK_DISAPPEAR, &OnItemPkgDisAppear );//么个道具消失掉

	//Register( M_G_WEAPON_INFO, &OnItemPkgWeaponInfo );//道具包中武器的详细信息

	//Register( M_G_EQUIP_INFO, &OnItemPkgEquipInfo );//道具包中道具的详细信息

	//Register( M_G_PICKITEM_SUCCESS, &OnPickItemSuccess );//拾取道具成功

	Register( M_G_PICKITEM_FAILED, &OnPickItemFailed );//拾取道具失败

	Register( M_G_ONECLIENTPICKITEM,&OnOnePlayerPickItem );//通知周围的人，拾取了道具

	Register(M_G_ONEPLAYER_USEITEM,&OnPlayerUseItem );//通知周围的人，玩家使用了道具

	Register( M_G_USEITEM_RESULT, &OnPlayerUseItemFailed );//使用道具失败的提示

	Register(M_G_UNUSEITEM, &OnPlayerUnUseItem);//通知周围的人，玩家卸载了道具

	Register( M_G_SET_PKGITEM ,&OnPlayerSetPkgItem);

	//Register(M_G_ITEM_DETAILINFO, &OnReturnItemDetialInfo );//返回玩家道具的详细信息

	//Register(M_G_EQUIP_CONCISEINFO, &OnReturnEquipConciseInfo );//返回玩家装备的简明信息

	Register(M_G_ITEMPKG_CONCISEINFO,&OnReturnItemPkgConciseInfo );//范围玩家道具包的简明信息

	Register( M_G_DROPITEM_RESULT, &OnReturnDropItemResult);//玩家丢弃道具返回结果

	Register( M_G_PLAYER_BUFFSTART, &OnPlayerBuffStart);	//玩家获得一个BUFF

	Register( M_G_PLAYER_BUFFEND, &OnPlayerBuffEnd);	//玩家的BUFF结束

	Register( M_G_PLAYER_BUFFUPDATE, &OnPlayerBuffUpdate);	//玩家的buff更新

	//	Register( M_G_MONEY_UPDATE, &OnPlayerMoneyUpdate );//更新玩家的金钱数目

	Register( M_G_MOUNTTEAM_APPEAR, &OnReturnRideTeamAppear );//骑乘队伍出现时

	Register( M_G_INVITE_RESULT, &OnReturnInviteRideTeamResult); //邀请组队的结果

	Register( M_G_BEINVITE_MOUNTTEAM, &OnBeInviteJoinRideTeam );//被邀请加入骑乘队伍

	Register( M_G_MOUNTSTATE_CHANGE, &OnRideStateChange);//玩家骑乘状态更新

	Register( M_G_ONRIDELEADER_SWITCHMAP,&OnRideLeaderSwitchMap );//骑乘队员对骑乘队长跳地图的反应

	Register( M_G_MOUNTTEAM_DISBAND,&OnRideTeamDissolve );//骑乘队伍解散时

	Register( M_G_MONSTER_BUFFSTART, &OnMonsterBuffStart );  //怪物BUFF开始

	Register( M_G_MONSTER_BUFFUPDATE, &OnMonsterBuffUpdate );	//怪物的BUFF更新

	Register( M_G_REQ_BUFFEND,	&OnReqBuffEnd );  //返回客户端请求的结果  

	Register( M_G_BROADCAST, &OnBroadcast );  //范围通知前提

	Register( M_G_USE_ASSISTSKILL, &OnUseAssistSkill );

	Register( M_G_SET_PLAYERPOS, &OnSetPlayerPos );//设置玩家新位置(地图内跳转，只通知跳转者本人);

	Register( M_G_SEND_TRADE_REQ_ERROR, &OnSendTradeReqError );//发送交易请求错误

	Register( M_G_RECV_TRADE_REQ, &OnRecvTradeReq );//收到交易请求

	Register( M_G_SEND_TRADE_REQ_RESULT, &OnTradeReqResult );//请求交易的结果

	Register( M_G_ADD_OR_DELETE_ITEM, &OnAddOrDeleteItem );//交易栏中增加或删除一个物品

	Register( M_G_SET_TRADE_MONEY, &OnSetTradeMoney );//设置交易金钱

	Register( M_G_CANCEL_TRADE, &OnCancelTrade );//取消交易

	Register( M_G_LOCK_TRADE, &OnLockTrade );//锁定交易

	Register( M_G_CONFIRM_TRADE, &OnConfirmTrade );//确定交易

	Register( M_G_SHOW_PLATE, &OnShowPlate );//指示客户端显示托盘；MGShowPlate

	Register( M_G_ADDSTH_TO_PLATE_RST, &OnAddSthToPlateRst );//往托盘放置物品结果；MGAddSthToPlateRst

	Register( M_G_TAKESTH_FROM_PLATE_RST, &OnTakeSthFromPlateRst );//从托盘取物品结果；MGTakeSthFromPlateRst

	Register( M_G_PLATE_EXEC_RST, &OnPlateExecRst );//托盘执行结果；MGPlateExecRst

	Register( M_G_CLOSE_PLATE, &OnClosePlate );//对玩家关托盘的响应，或者主动关玩家托盘；MGClosePlate

	Register( M_G_ROLLITEM_RESULT, &OnMGRollItemResult );//roll的结果

	Register( M_G_REQ_AGREEROLLITEM, &OnMGReqAgreeRollItem );//告诉客户端ROLL

	Register( M_G_MEMBER_DETIALINFO_UPDATE, &OnMGTeamMemberDetailInfoUpdate );

	Register( M_G_ROLLITEM_NUM,&OnReturnRollNumResult );//Roll到的点数通知

	Register( M_G_MEMBER_PICKITEM, &OnRecvTeamMemberPickItem );//组队队员拾取道具的通知

	Register( M_G_TEAMMEMBER_GET_NEWITEM , &OnNoticeTeamPlayerGetNewItem );

	Register( M_G_CHANGEITEMCRI ,&OnChangeItemCri );//改造道具时概率改变

	Register( M_G_PLAYER_NEWTASK, &OnPlayerNewTask );//给玩家添加了一个新任务；//MGPlayerNewTask

	Register( M_G_PLAYER_TASKSTATUS, &OnPlayerTaskStatus );//通知玩家任务新状态；//MGPlayerTaskStatus

	Register( M_G_LEAVE_VIEW, &OnLeaveView );//怪物或其它玩家离开玩家视野,MGLeaveView;

	Register( M_G_MONSTER_BUFF_END, &OnMonsterBuffEnd );//怪物BUFF结束通知

	Register( M_G_ADD_FOEINFO, &OnAddFoeInfo );//mapsrv通知添加仇人

	Register( M_G_REPAIR_WEAR_BY_ITEM, &OnRepairWearByItem);//修复耐久度

	Register( M_G_NOTIFY_WEAR_CHANGE, &OnNotifyWearChange);//通知耐久度发生变化

	Register( M_G_PLAYERBUSYING, &OnPlayerIsBusying );//当玩家正在忙时的通知

	Register( M_G_ADD_SPSKILL_RESULT, &OnAddSpSkillResult );//当玩家正在忙时的通知
	Register( M_G_SPSKILL_UPGRADE,	&OnSpSkillUpgrade );	 //ＳＰ技能升级
	Register( M_G_THREAT_LIST,	&OnThreatList );	 //威胁列表
	Register( M_G_UPGRADE_SKILL_RESULT,	&OnUpdateSkillResult );	 //技能升级

	Register( M_G_PLAYER_TASKRECORD_UPDATE,&OnUpdateTaskCounterNum );//更新任务的计数信息
	Register( M_G_PLAYER_TASKHISTORY, &OnTaskHistoryReturn );//更新任务记录的任务信息
	Register( M_G_EQUIP_PLAYERITEM, &OnNoticePlayerEquipItem );//玩家装备道具的时候
	Register( M_G_PLAYER_SHOWTASKUI ,&OnShowTaskUIMsg );//显示任务的UI信息
	Register( M_G_CLEAN_SKILLPOINT, &OnCleanSkillPoint );	//技能洗点的消息
	Register( M_G_REPAIR_WEAR_BY_NPC, &OnRepairWearByNpc);//npc维修耐久度

	Register( M_G_SHOW_SHOP, &OnShowShop );//显示NPC商店
	Register( M_G_SHOP_BUY_RST, &OnShopBuyRst );//NPC商店购买结果
	Register( M_G_ITEM_SOLD_RST, &OnItemSoldRst );//商店售出结果
	Register( M_G_CHANGE_BATTLE_MODE, &OnChangeBattleMode );//change mode
	Register( M_G_PLAYER_USE_HEAL_ITEM, &OnPlayerUseHealItem );

	Register( M_G_ADD_APT_RST, &OnAddAptRst );//添加活点结果；
	Register( M_G_EVILTIME_RST, &OnQueryEvilTimeRst );//恶魔时间的返回
	Register( M_G_FRIENDINFO_UPDATE, &OnFriendInfoUpdate  );//个人的好友信息更新
	Register( M_G_REPAIRITEM_COST_RST ,&OnRepairItemMoneyRst );
	//Register( M_G_INFO_UPDATE ,&OnInfoUpdate );		//战斗信息的更新
	Register( M_G_BUFF_DATA, &OnBuffData );		//BUFF的更新
	Register( M_G_EQUIPITEM_UPDATE, &OnEquipItemUpDate );//身上装备的信息更新
	Register( M_G_SWICHMAP_REQ, &OnSwichMapReq );//跳转地图的实现
	Register( M_G_TARGET_GET_DAMAGE, &OnTargetGetDamage );
	Register( M_G_SWAP_EQUIPITEM_RST ,&OnSwapEquipItemRst );
	Register( M_G_PUNISHMENT_START, &OnPunishmentStart );		//天谴开始
	Register( M_G_PUNISHMENT_END, &OnPunishmentEnd );			//天谴结束
	Register( M_G_BROADCAST_PUNISH_INFO, &OnBroadcastPunishInfo );	//组播天谴信息
	Register( M_G_QUERY_PUNISH_PLAYER_INFO,  &OnQueryPunishPlayerInfo );	//天谴玩家位置的反馈

	Register( M_G_GMCMD_REQUEST, &OnGmCmdRequest );		//GM命令结果的反馈
	Register( M_G_GMCMD_RESULT, &OnGmCmdResult );		//GM命令的结果

	Register( M_G_PETINFO_INSTABLE, &OnPetinfoInStable );//宠物不稳信息(只发给宠物主人)
	Register( M_G_PET_ABILITY, &OnPetAbility );//宠物能力信息(只发给宠物主人)
	Register( M_G_PETINFO_STABLE, &OnPetinfoStable );//宠物稳定信息(发给宠物主人周围其它玩家)
	//Register( M_G_SUMMON_PET,  &OnSummonPet );			//召唤宠物
	//Register( M_G_CLOSE_PET, &OnClosePet );				//关闭宠物
	//Register( M_G_CHANGE_PET_NAME, &OnChangePetName );		//更改宠物姓名
	//Register( M_G_CHANGE_PET_SKILL_STATE, &OnChangePetSkillState );		//更改宠物技能设置状态
	//Register( M_G_CHANGE_PET_IMAGE, &OnChangePetImage );		//更改宠物外形
	//Register( M_G_UPDATE_PET_EXP, &OnUpdatePetExp );		//更改宠物经验值
	//Register( M_G_PET_INFO, &OnPetInfo );		//组播宠物信息
	//Register( M_G_SELF_PET_INFO, &OnSelfPetInfo );	//自身宠物信息
	//Register( M_G_PET_LEVEL_UP, &OnPetLevelUp );	//宠物升级
	//Register( M_G_UPDATE_PET_HUNGER_DEGREE, &OnUpdatePetHungerDegree );
	//Register( M_G_UPDATE_PET_STATE, &OnUpdatePetState );

	//Register( M_G_UPDATE_RANKCHART , &OnUpdateRankChartInfo );
	//Register( M_G_RANKPLAYER_ONLINE, &OnRankPlayerUpLine );
	Register( M_G_RANKPLAYER_OFFLINE,&OnRankPlayerOffLine );
	Register( M_G_REMOVE_RANKPLAYER ,&OnRemoveRankPlayer );

	Register( M_G_DISPLAY_UNION_ISSUED_TASKS ,&OnDisplayUnionIssueTask );//显示工会已发布任务信息
	Register( M_G_CHANAGE_UNION_PRESTIGE_REQ ,&OnChanageUnionPrestige );//工会威望变化
	Register( M_G_CHANGE_UNION_ACTIVE ,&OnChangeUnionActive );//工会活跃度变化
	Register( M_G_UNION_MEMBER_SHOW_INFO, &OnUnionMemberShowInfo );//工会成员场景中显示信息

#ifdef WAIT_QUEUE
	//排队
	Register( M_G_QUERY_QUEUE_RESULT, &OnGetQueryQueueResult );	//mapsrv的反馈
	Register( M_G_QUEUE_OK, &QueueOK );	//mapsrv通知排队次序已到，可以进map;
#endif //WAIT_QUEUE

	//攻城
	Register( M_G_ATTACK_CITY_BEGIN, &OnAttackCityBegin );	//攻城开始
	Register( M_G_ATTACK_CITY_END, &OnAttackCityEnd ); //攻城结束
	Register( M_G_NOTIFY_WAR_TASK_STATE, &OnNotifyWarTaskState ); //攻城结束 !!! 该协议需要修改！！！
	Register( M_G_CHANGE_TEAM_STATE, &OnChangeTeamState );		//组队状态改变

	//使用UseSkill2后废弃，原ID号由MOUSE_TIP使用, by dzj, 10.05.31,Register( M_G_USE_PARTION_SKILL_RST, &OnUsePartionSkillRst );	//动作分割技能
	Register( M_G_MOUSE_TIP, &OnMGMouseTip );	//对目标的鼠标停留查询响应；

	//Register( M_G_PET_MOVE, &OnPetMove );	//宠物移动
	//Register( M_G_RACE_GROUP_NOTIFY, &OnRaceGroupNotify );	//宠物移动

	Register( M_G_CHECK_PKG_FREE_SPACE, &OnCheckPkgFreeSpaceRst );	//包裹空间检查
	Register( M_G_FETCH_VIP_ITEM, &OnFetchVipItemRst );				//领取vip物品
	Register( M_G_FETCH_NON_VIP_ITEM, &OnFetchNonVipItemRst );		//领取非vip物品
	Register( M_G_PICKMONEY_FROM_MAIL_RST , &OnPickMailMoneyRst );
	Register( M_G_PICKITEM_FROM_MAIL_RST ,&OnPickMailItemRst );

	//Register( M_G_WORLD_NOTIFY ,&OnWorldNotify );		//世界群发
	Register( M_G_REQ_PLAYER_DETAIL_INFO_RST ,&OnReqPlayerDetailInfoRst );		//世界群发
	Register( M_G_QUERY_SUIT_INFO_RST, &OnQuerySuitInfoRst );//查询套装信息
	Register( M_G_CLEAR_SUIT_INFO_RST, &OnClearSuitInfoRst );//清空套装信息
	Register( M_G_SAVE_SUIT_INFO_RST, &OnSaveSuitInfoRst );//保存套装信息
	Register( M_G_CHANGE_SUIT_RST, &OnChanageSuitRst);//一键换装

	Register( M_G_SHOW_MAILBOX, &OnPlayerShowMailBox );//显示玩家邮箱界面
	Register( M_G_GATE_UPDATE_GOODEVILPOINT, &OnGateUpdateGoodEvilPoint );//更新善恶值
	Register( M_G_COLLECT_TASK_RST, &OnCollectTaskRst );//通知采集信息

	Register( M_G_SHOW_PROTECTPLATE, &OnShowProtectPlate );
	Register( M_G_PUTITEM_TO_PROTECTPLATE_RST, &OnPutItemToProtectPlateRst );
	Register( M_G_SET_ITEM_PROTECTPROP, &OnUpdateProtectItemProp );
	Register( M_G_SENDITEM_WITH_PROTECTPROP, &OnRecvItemWithProtectProp );
	Register( M_G_RELEASE_SPECIALPROTECT_RST, &OnReleaseItemSpecialProtectRst );
	Register( M_G_CANCEL_SPECIALPROTECT_RST, &OnCancelItemUnSpecialProtectWaitTimeRst );
	Register( M_G_CONFIRM_PROTECTPLATE_RST, &OnConfirmProtectPlateRst );
	Register( M_G_TAKE_ITEM_FROM_PROTECTPLATE_RST ,&OnTakeItemFromProtectPlateRst );
	Register( M_G_UPDATE_CITY_INFO ,&OnUpdateCityInfo );	//更新关卡地图信息
	Register( M_G_LOAD_RANKINFO, &OnLoadRankChartInfo );
	Register( M_G_UPDATE_RANKPLAYER_LEVEL, &OnRankPlayerLevelUp );//当排行榜玩家更新等级
	Register( M_G_UPDATE_RANKPLAYER_RANKINFO ,&OnRankPlayerRankInfoUpdate );//玩家的排行信息更新

	Register( M_G_UPDATE_PLAYERITEM_INFO, &OnUpdatePlayerItemInfo );
	Register( M_G_UPDATE_TASK_INFO, &OnUpdatePlayerTaskRdInfo );
	Register( M_G_CHANGE_TARGET_NAME, &OnChangeTargetName );
	Register( M_G_KICK_ALL_PLAYER, &OnKickAllPlayer );		//踢所有玩家
	Register( M_G_NOTIFY_ACTIVITY_STATE, OnNotifyActivityState );	//通知玩家某个活动的状态
	Register( M_G_NOTIFY_NEW_TASK_RECORD,&OnNotifyNewTaskRecord );
	Register( M_G_BACK_TASK_UPDATE,&OnUpdateNewTaskRecordInfo );
	Register( M_G_SEND_SYSMAIL_TO_PLAYER, &OnSendMailToPlayer );//Map发送邮件给玩家
	Register( M_G_UPDATE_PLAYER_CHANGE_BASE_INFO ,&OnPlayerBaseInfoChange );
	Register( M_G_EXCEPTION_NOTIFY, &OnExceptionNotify );
	Register( M_G_SEND_SPECIAL_STRING, &OnSendSpecialString );		//发送特殊字符串给客户端
	Register( M_G_USE_SKILL_RST, &OnUseSkillRst );		//使用技能的反馈

	Register( M_G_RST_TO_ATKER, &OnRstToAtker );		//使用技能给攻击者的消息
	Register( M_G_NORMAL_ATTACK, &OnNormalAttack );		//使用普通技能给周围人的消息
	Register( M_G_SPLIT_ATTACK, &OnSplitAttack );		//使用分割技能给周围人的消息

	Register( M_G_SAVE_REBIRTH_POS_NTF, &OnSaveRebirthPosNtf); //保存复活点通知
	Register( M_G_UNION_MULTI_REQ, &OnUnionMultiReq);//工会活跃度或威望N倍

	Register( M_G_STORAGE_ITEM_INFO, &OnStorageItemInfo );//仓库道具信息
	Register( M_G_SHOW_STORAGE, &OnShowStoragePlate );//显示仓库托盘
	Register( M_G_STORAGE_SAFE_INFO, &OnStorageSafeInfo );//通知仓库的安全信息
	Register( M_G_SHOW_STORAGE_PASSWORD_DIALOG, &OnShowStoragePassWordDlg );//通知弹出设置密码框
	Register( M_G_CHANGE_STORAGE_LOCKSTATE , &OnChangeStorageLockState );//改变仓库的锁定状态
	Register( M_G_UPDATE_STORAGE_VALIDROW, &OnUpdateStorageValidRow );//改变仓库的可用行数
	Register( M_G_UPDATE_STORAGE_SECURITY_STRATEGY, &OnUpdateStorageSecurityStrategy );//改变仓库的安全策略
	Register( M_G_UPDATE_STORAGE_ITEMINFO_BYINDEX , &OnUpdateStorageItemInfo );//改变仓库对应位的道具信息
	Register( M_G_TAKEITEM_FROM_STORAGE_FAIL, &OnTakeItemFromStorageRst );//取出物品的结果
	Register( M_G_PUTITEM_TO_STORAGE_FAIL,&OnPutItemToStorageRst );//放入物品的结果
	Register( M_G_SET_NEWPASSWORD_ERROR, &OnSetNewPassWordRst );//设置密码的结果
	Register( M_G_RESET_PASSWORD_ERROR, &OnResetStoragePsdRst );//重新设置密码的结果
	Register( M_G_TAKEITEM_PASSWORD_ERROR, &OnTakeItemCheckPsdRst );//取出物品效验密码的结果
	Register( M_G_EXTEND_STORAGE_ERROR, &OnExtendStorageRst );//扩展仓库的结果
	Register( M_G_UPDATE_STORAGE_SAFE_INFO, &OnUpdateFunctionSrvStorageSafeInfo );
	Register( M_G_UPDATE_STORAGE_ITEM_INFO, &OnUpdateFunctionSrvStorageItemInfo );
	Register( M_G_UPDATE_STORAGE_FORBIDDEN_INFO, &OnUpdateFunctionSrvStorageForbiddenInfo );
	Register( M_G_QUERY_STORAGE_INFO ,&OnQueryStorageInfo );
	Register( M_G_GMQUERY_RESP, &OnGMQueryResp );
	Register( M_G_NOTIFY_ROOKIE_STATE, &OnNotifyRookieState );
	Register( M_G_ITEMNPC_INFO, &OnItemNpcStatusInfo );
	Register( M_G_ITEMNPC_ID, &OnItemNpcID );
	Register( M_G_BATCH_SELL_ITEMS_RESP, &OnBatchSellItemsResp );
	Register( M_G_REPURCHASE_ITEM_RESP, &OnRepurchaseItemResp );
	Register( M_G_REPURCHASE_ITEM_LIST, &OnRepurchaseItemListNtf );
	Register( M_G_CLOSE_OLTEST_PLATE, &OnCloseOLTestDlg );
	Register( M_G_SHOW_PREVUE_OLTEST_DLG, &OnShowOLTestProvueDlg);
	Register( M_G_SHOW_OLTEST_QUESTION, &OnShowOLTestQuestion );
	Register( M_G_ANSWER_QUESTION_RST, &OnAnswerQuestionRst );
	Register( M_G_SHOW_ANSWER_ALLQUESTIONS, &OnAnswerAllQuestion );
	Register( M_G_SPLIT_ITEM_ERROR, &OnSplitItemError );
	Register( M_G_LOG_PLAYER_INFO, &OnLogPlayerInfo );

	Register( M_G_PLAYER_TRY_COPY_QUEST, &OnPlayerTryCopyQuest );//玩家试图进副本请求(隐含创建新副本)；
	Register( M_G_PLAYER_LEAVE_COPY_QUEST, &OnPlayerLeaveCopyQuest );//玩家试图离开副本请求；
	Register( M_G_COPY_NOMORE_PLAYER, &OnCopyNomorePlayer );//副本不可再进指令；
	Register( M_G_DEL_COPY_QUEST, &OnDelCopyQuest );//发往center的删除副本请求(在center发往mapsrv的删除副本请求之后，因为mapsrv收到那个请求后会先踢玩家，踢完后再发此G_E_DEL_COPY_QUEST给center，最后center发delcopy cmd给map)；	
	Register( M_G_PLAYER_OUTMAPINFO, &OnPlayerOutMapInfo );//mapsrv玩家的outmapinfo改变；
	Register( M_G_GLOBE_COPYINFOS, &OnMGGlobeCopyInfo );//mapsrv对玩家查询伪副本信息的反馈；	

	Register( M_G_TIMELIMIT, &OnMGTimeLimit );//mapsrv发来玩家倒计时框；

	Register( M_G_PLAYER_WAR_DYING, &OnPlayerWarDying );//攻城战死亡倒计时
	Register( M_G_WAR_REBIRTH_OPTION_ERR, &OnWarRebirthOptionErr );//攻城战死亡错误

	Register( M_G_NOTICE_RACEMASTER_TIMEINFO_EXPIRE, &OnRaceMasterTimeInfoExpire);
	Register( M_G_NOTICE_RACEMASTER_DECALREWARINF0_CHANGE , &OnRaceMasterDeclareWarInfoChange );
	Register( M_G_NOTICE_RACEMASTER_TIMEINFO_CHANGE, &OnRaceMasterTimeInfoChange );
	//Register( M_G_NOTICE_RACEMASTER_PUBLICMSG, &OnNoticeRaceMasterPublicMsg );
	Register( M_G_NOTICE_RACEMASTER_PUBLICMSG_CHANGE, &OnRaceMasterPublicMsgChange );
	Register( M_G_UPDATE_RACEMASTER_INFO_TO_DB, &OnSaveRaceMasterInfoToDB );
	Register( M_G_RACEMASTER_DECLAREWAR_RST, &OnRaceMasterDeclareWarRst );
	Register( M_G_RACEMASTER_GET_SALARY_RST, &OnRaceMasterGetSalaryRst );
	Register( M_G_RACEMASTER_NOTICE_NEWMSG_RST, &OnRaceMasterSetNewPublicMsgRst );
	Register( M_G_SHOW_RACEMASTER_OPPLATE, &OnShowRaceMasterPlate );
	Register( M_G_SHOW_RACEMASTER_DECLAREWAR_PLATE, &OnShowRaceMasterDeclareWarPlate );
	Register( M_G_SHOW_RACEMASTER_PUBLICMSG_PLATE, &OnShowRaceMasterPublicMsgPlate );
	Register( M_G_CLOSE_RACEMASTER_PLATE, &OnCloseRaceMasterPlate );
	Register( M_G_SHOW_RACEMASTER_ASSIGNCITY_PLATE, &OnShowRaceMasterAssignCityPlate );
	Register( M_G_BUFF_START, &OnBuffStart );   //buff start
	Register( M_G_BUFF_END, &OnBuffEnd );  //buff end
	Register( M_G_BUFF_STATUS_UPDATE, &OnBuffStatusUpdate );  //on buff status update
	Register( M_G_BUFF_HPMP_UPDATE, &OnBuffHpMpUpdate );  //on hpmp Update
	Register( M_G_LOAD_RACEMASTER_INFO_FROM_DB,&OnMapSrvLoadRaceMasterInfo );
	Register( M_G_RESET_RACEMASTER_DECLAREWAR, &OnResetRaceMasterDeclareWar );

	Register( M_G_PUBLICNOTICE_MODIFY, &OnPublicNoticeModify );
	Register( M_G_PUBLICNOTICE_ADD, &OnPublicNoticeAdd );
	Register( M_G_PUBLICNOTICE_REMOVE, &OnPublicNoticeRemove );
	Register( M_G_MARQUEUE_MSG_ADD, &OnPublicMarqueueAdd );
	Register( M_G_PARAM_MSG_ADD, &OnParamMsgAdd );
	Register( M_G_SHOW_TEMPNOTICE_BYPOSITION, &OnRecvShowTempNotice );

	Register( M_G_DISPLAY_WAR_TIMER, &OnDisplayWarTimer );						//计时器
	Register( M_G_DISPLAY_WAR_COUNTER, &OnDisplayWarCounter );					//计数器
	Register( M_G_CHARATER_DAMAGE_REBOUND, &OnCharacterDamageRebound );  //伤害反弹
	Register( M_G_BLOCK_QUAD_STATE_CHANGE, &OnBlockQuadStateChange ); //阻挡区状态变化
	Register( M_G_START_SELECT_RACE,&OnStartSelectRaceMaster );//开始选族长
	Register( M_G_FORWARD_UNION_CMD_RST, &OnForwardUnionCmdRst );//中转检测的结果
	Register( M_G_RETURN_SEL_ROLE, &OnReturnSelRole );//返回角色选择
	Register( M_G_UNION_COST_ARG, &OnUnionCostArg);//创建工会的消耗参数

	Register( M_G_PLAYER_START_NEWAUCTION, &OnPlayerStartNewAuctionNext );
	Register( M_G_PLAYER_BID_ITEM_NEXT,  &OnPlayerBidAuctionItemNext );
	Register( M_G_PLAYER_NEWAUCTION_RST, &OnPlayerStartNewAuctionRst );
	Register( M_G_PLAYER_BID_ITEM_ERROR, &OnPlayerBidItemErr );
	Register( M_G_PLAYER_SHOW_AUCTIONPLATE,&OnPlayerShowAuctionPlate );

	Register( M_G_REPORT_PW_INFOS, &OnReportPWInfo );
	Register( M_G_REPORT_PW_MAP_INFOS, &OnReportPWMapInfo );
	Register( M_G_MODIFY_PW_STATE_FORWARD, &OnModPWStateForward );
	Register( M_G_MODIFY_PW_MAP_STATE_FORWARD, &OnModPWMapStateForward );
	Register( M_G_SHIFT_PW_REQ_FORWARD, &OnShiftPWForward );
	Register( M_G_KICKOFF_PW_REQ_FORWARD, &OnKickOffPWForward );
	Register( M_G_MOD_EFFECTIVE_SKILLID_RST, &OnModEffectiveSkillIDRst);

	Register( M_G_ADD_FRIEND_DEGREE_REQ, &OnAddFriendDegreeReq);
	Register( M_G_ADD_TWEET_NTF, &OnAddTweetNtf);
	Register( M_G_REPORT_FRIEND_NON_TEAM_GAIN, &OnFrinedNonTeamExpNtf);
	Register( M_G_QUERY_EXP_OF_LEVEL_UP_REQ, &OnExpOfLevelUpRst);
	Register( M_G_INVITE_CALL_MEMBER, &OnInviteCallMember);
	Register( M_G_START_CAST, &OnStartCast);
	Register( M_G_STOP_CAST, &OnStopCast);

}

bool CDealMapSrvPkg::OnRankCacheInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnRankCacheInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGRankCacheInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		D_ERROR( "OnRankCacheInfo,pOwner空, wPkgLen != sizeof(MGRankCacheInfo)\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "OnRankCacheInfo,pOwner空, NULL == pPkg\n" );
		return false;
	}

	const MGRankCacheInfo* pCacheInfo = (const MGRankCacheInfo*) pPkg;

	if ( NULL == pCacheInfo )
	{
		D_ERROR( "OnRankCacheInfo,pOwner空, NULL == pCacheInfo\n" );
		return false;
	}

	GFRankCacheInfo gfInfo;
	gfInfo.rankType = pCacheInfo->rankType;
	gfInfo.cacheRcd.DataFrom( pCacheInfo->cacheRcd );

	SendMsgToFunctionSrv( GFRankCacheInfo, gfInfo );

	return true;
	TRY_END;
	return false;
}

///Register( M_G_MAPSRV_READY, &OnMGMapSrvReady );//对应mapsrv准备好；
bool CDealMapSrvPkg::OnMGMapSrvReady( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMGMapSrvReady,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGMapSrvReady) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		D_ERROR( "OnMGMapSrvReady,pOwner空, wPkgLen != sizeof(MGMapSrvReady)\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "OnMGMapSrvReady,pOwner空, NULL == pPkg\n" );
		return false;
	}

	pOwner->SetMapSrvReady();//设置该mapsrv准备好；

	return true;
	TRY_END;
	return false;
}

///接收到mapsrv通知其自身是否可进消息；
bool CDealMapSrvPkg::OnMGMapIsFull( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMGMapIsFull,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGMapIsFull) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	//结构校验通过
	const MGMapIsFull* pMGMapIsFull = (const MGMapIsFull*)pPkg;
	
	pOwner->SetFullFlag( pMGMapIsFull->isFull );

	return true;
	TRY_END;
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
///Register( M_G_DBSAVE_PRE,  &OnMGDbSavePre );
bool CDealMapSrvPkg::OnMGDbSavePre( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMGDbSavePre,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGDbSavePre) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGDbSavePre* pMGDbSavePre = (const MGDbSavePre*)pPkg;

	pOwner->OnMGDbSavePre( pMGDbSavePre );

	//pOwner->OnMGDbSaving<MGSomeInfo>( pSomeInfo );

	return true;
	TRY_END;
	return false;
}

///Register( M_G_DBSAVE_INFO, &OnMGDbSaveInfo );
bool CDealMapSrvPkg::OnMGDbSaveInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMGDbSavePre,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGDbSaveInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGDbSaveInfo* pMGDbSaveInfo = (const MGDbSaveInfo*)pPkg;

	pOwner->OnMGDbSaveInfo( pMGDbSaveInfo );

	//pOwner->OnMGDbSaving<MGSomeInfo>( pSomeInfo );

	return true;
	TRY_END;
	return false;
}

///Register( M_G_DBSAVE_POST, &OnMGDbSavePost );
bool CDealMapSrvPkg::OnMGDbSavePost( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMGDbSavePre,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGDbSavePost) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGDbSavePost* pMGDbSavePost = (const MGDbSavePost*)pPkg;

	//pOwner->OnMGDbSaving<MGSomeInfo>( pSomeInfo );
	pOwner->OnMGDbSavePost( pMGDbSavePost );

	return true;
	TRY_END;
	return false;
}

///Register( M_G_PLAYER_LEAVE_MAP, &OnMGPlayerLeaveMap );
bool CDealMapSrvPkg::OnMGPlayerLeaveMap( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	//在此之前mapsrv更新的玩家信息已得到妥善处理
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMGDbSavePre,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGPlayerLeaveMap) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGPlayerLeaveMap* pMGDbSavePost = (const MGPlayerLeaveMap*)pPkg; ACE_UNUSED_ARG( pMGDbSavePost );

	/*
	1.如果是玩家下线，则为该玩家相关的最后一条消息，向DB发送存盘信息；
	2.如果是跳地图，则向新map所属mapsrv发送GMDbGetInfo;
	*/

	return true;
	TRY_END;
	return false;
}

///Register( M_G_NDSMR_INFO, &OnMGNDSMRInfo );//非存盘,但跳地图需保存信息;
bool CDealMapSrvPkg::OnMGNDSMRInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMGNDSMRInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGNDSMRInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGNDSMRInfo* pMGInfo = (const MGNDSMRInfo*)pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMGInfo->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		//找不到该玩家,可能该玩家已下线,忽略之
		return false;
	}

	pPlayer->OnMapNDSMRInfo( pMGInfo );

	return true;
	TRY_END;
	return false;
}

///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///mapsrv通知gatesrv自身管理的地图列表；
bool CDealMapSrvPkg::OnManedMaps( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnManedMaps,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGManedMap) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGManedMap* pManedMap = (const MGManedMap*)pPkg;

	if ( pManedMap->isCopyMapSrv )
	{
		//该srv是副本地图服务器，通知centersrv此副本服务器ID号；
		GECopyMapsrvNoti copyMapsrvNoti;
		copyMapsrvNoti.copyMapsrvID = pOwner->GetID();
		SendMsgToCenterSrv( GECopyMapsrvNoti, copyMapsrvNoti );
	}

	int manedNum = pManedMap->nMapNum;
	for ( int i=0; i<manedNum; ++i )
	{
		if ( i >= ARRAY_SIZE(pManedMap->lMapCode) )
		{
			D_ERROR( "mapsrv发来的地图列表中地图数%d有错\n", manedNum );
			return false;
		}
		m_mapMapcodeToMapSrv.insert( pair<unsigned long, CMapSrv*>(pManedMap->lMapCode[i], pOwner) );
	}

	//向DBSRV0发送城市请求的信息
	GDReqCityInfo	reqCityInfo;
	StructMemCpy( reqCityInfo.lMapCode, pManedMap->lMapCode, sizeof(pManedMap->lMapCode[0])*ARRAY_SIZE(reqCityInfo.lMapCode)/*?????*/ ); 
	reqCityInfo.nMapNum = pManedMap->nMapNum;
	reqCityInfo.reqMapsrvID = pOwner->GetID();
	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByID( 0 );
	if( NULL == pDbSrv )
	{
		D_ERROR("DbSrv为NULL\n");
		return false;
	}
	D_INFO("收到MapSrv的地图信息，向DBSRV0发送城市请求的信息");
	//MsgToPut* pNewMsg = CreateSrvPkg( GDReqCityInfo, pDbSrv, reqCityInfo );
	//pDbSrv->SendPkgToSrv( pNewMsg );
	SendMsgToSrv( GDReqCityInfo, pDbSrv, reqCityInfo );

	return true;
	TRY_END;
	return false;
}

///mapsrv通知gatesrv错误发生；
bool CDealMapSrvPkg::OnMGError( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMGError,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGError) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGError* pMgError = (const MGError*)pPkg;

	if ( pMgError->leavePlayerID.wGID != SELF_SRV_ID )
	{
		D_ERROR( "MGError，mapsrv通知的gate错误\n" );
		return false;
	}

	if ( pMgError->nErrCode == MGError::LEAVE_ERR_MAPDISMATCH )
	{
		//mapsrv处理玩家离开错误，应通知centersrv该玩家下线;
		D_ERROR( "mapsrv通知:玩家%d:%s，mapsrv中玩家所在地图与gatesrv通知的下线地图不匹配\n", pMgError->leavePlayerID.dwPID, pMgError->szLeaveAccount );
		//int jjj, 这里需要再仔细考虑下，CPlayer* pPlayer = CManPlayer::FindPlayer( pMgError->leavePlayerID.dwPID );
		CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
		if ( NULL == pCenterSrv )
		{
			D_ERROR( "玩家下线时mapsrv发生错误，且找不到centersrv\n" );
			return false;
		}

		GEPlayerOffline playerOffline;
		playerOffline.playerID = pMgError->leavePlayerID;
		SafeStrCpy( playerOffline.szAccount, pMgError->szLeaveAccount );
		//MsgToPut* pNewMsg = CreateSrvPkg( GEPlayerOffline, pCenterSrv, playerOffline );
		//pCenterSrv->SendPkgToSrv( pNewMsg );	
		SendMsgToCenterSrv( GEPlayerOffline, playerOffline );
		D_DEBUG( "1、通知centersrv,%s下线\n", playerOffline.szAccount );
				
		CLogManager::DoAccountLogout(pMgError->szLeaveAccount, 0, 0);//记录下线
	} else if ( pMgError->nErrCode == MGError::LEAVE_ERR_NOPLAYER ) {
		D_WARNING( "玩家(%d:%d)下线错误，mapsrv已无此玩家\n", pMgError->leavePlayerID.wGID, pMgError->leavePlayerID.dwPID );
		CPlayer* pPlayer = CManPlayer::FindPlayer( pMgError->leavePlayerID.dwPID );
		if ( NULL == pPlayer )
		{
			D_WARNING( "本gatesrv上亦无此玩家,正常结果\n" );
			return false;
		} else {
			D_WARNING( "本gatesrv上此玩家%s仍在,异常结果，踢去此玩家\n", pPlayer->GetAccount() );
			CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
			if ( NULL == pCenterSrv )
			{
				D_ERROR( "玩家下线时mapsrv发生错误，且找不到centersrv\n" );
				return false;
			}
			//通知CenterSrv，使CenterSrv能删除该玩家
			GEPlayerOffline playerOffline;
			playerOffline.playerID = pMgError->leavePlayerID;
			SafeStrCpy( playerOffline.szAccount, pMgError->szLeaveAccount );
			//MsgToPut* pNewMsg = CreateSrvPkg( GEPlayerOffline, pCenterSrv, playerOffline );
			//pCenterSrv->SendPkgToSrv( pNewMsg );	
			SendMsgToCenterSrv( GEPlayerOffline, playerOffline );
			D_DEBUG( "1、通知centersrv,%s下线\n", playerOffline.szAccount );

			pPlayer->SetIsLeaveNotiMapSrv( false );
			pPlayer->ReqDestorySelf();
		}
		//LEAVE_ERR_NOPLAYER
	} else if ( ( pMgError->nErrCode == MGError::APPEAR_ERR_NORMALERR ) 
		|| ( pMgError->nErrCode == MGError::APPEAR_ERR_POSITION )  //位置错误
		|| ( pMgError->nErrCode == MGError::APPEAR_ERR_MAPDISMATCH ) //地图找不到
		|| ( pMgError->nErrCode == MGError::APPEAR_ERR_SAME_ID ) //相同ID的上线消息
		) 
	{ 
		//mapsrv处理玩家appear错误，应通知centersrv该玩家下线;
		D_WARNING( "玩家%d appear错误:%d\n", pMgError->leavePlayerID.dwPID, pMgError->nErrCode );
		CPlayer* pPlayer = CManPlayer::FindPlayer( pMgError->leavePlayerID.dwPID );
		if ( NULL == pPlayer )
		{
			//玩家之前已经下线了
			D_ERROR( "收到玩家appear失败消息时，玩家已经不在线上\n" );
			return false;
		}
		if ( !(pPlayer->IsEnterWorlding()) )
		{
			D_ERROR( "收到玩家appear失败消息时，玩家不处于world entering状态\n" );
			return false;
		}
		//找不到登录服务器；
		GCSrvError srvError;
		srvError.errNO = 1002;//mapsrv出现失败

		NewSendPlayerPkg( GCSrvError, pPlayer, srvError );
		//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pPlayer, srvError );
		//pPlayer->SendPkgToPlayer( pErrMsg );
		pPlayer->ReqDestorySelf();
	} else if ( ( pMgError->nErrCode == MGError::FULLINFO_DISORDER_PKG0 ) //full info包顺序错，此时应该删去玩家；
		|| ( pMgError->nErrCode == MGError::FULLINFO_DISORDER_PKGN )
		)
	{
		D_ERROR( "接mapsrv通知，玩家%x完整信息发送顺序错:%d\n", pMgError->leavePlayerID.dwPID, pMgError->nErrCode );
		CPlayer* pPlayer = CManPlayer::FindPlayer( pMgError->leavePlayerID.dwPID );
		if ( NULL == pPlayer )
		{
			//玩家之前已经下线了
			D_ERROR( "玩家%x失败消息处理，玩家已经不在线上\n", pMgError->leavePlayerID.dwPID );
			return false;
		}

		if ( ( !(pPlayer->IsEnterWorlding()) )
			&& ( !(pPlayer->IsInFighting()) )
			)
		{
			D_ERROR( "玩家%x失败消息处理，玩家不处于进入世界状态，也不处于战斗状态\n", pMgError->leavePlayerID.dwPID );
			return false;
		}

		GCSrvError srvError; //通知客户端，同时执行删玩家操作；
		srvError.errNO = 1002;//mapsrv出现失败

		NewSendPlayerPkg( GCSrvError, pPlayer, srvError );
		//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pPlayer, srvError );
		//pPlayer->SendPkgToPlayer( pErrMsg );
		pPlayer->ReqDestorySelf();
	} else if ( MGError::MAPSRV_PLAYER_FULL == pMgError->nErrCode )
	{
		D_ERROR( "接mapsrv通知，玩家%x登录时对应map人已满!\n", pMgError->leavePlayerID.dwPID );
		CPlayer* pPlayer = CManPlayer::FindPlayer( pMgError->leavePlayerID.dwPID );
		if ( NULL == pPlayer )
		{
			//玩家之前已经下线了
			D_ERROR( "玩家%x失败消息处理，玩家已经不在线上\n", pMgError->leavePlayerID.dwPID );
			return false;
		}

		if ( ( !(pPlayer->IsEnterWorlding()) )
			&& ( !(pPlayer->IsInFighting()) )
			)
		{
			D_ERROR( "玩家%x失败消息处理，玩家不处于进入世界状态，也不处于战斗状态\n", pMgError->leavePlayerID.dwPID );
			return false;
		}

		GCSrvError srvError; //通知客户端，同时执行删玩家操作；
		srvError.errNO = 1002;//mapsrv出现失败

		NewSendPlayerPkg( GCSrvError, pPlayer, srvError );
		//MsgToPut* pErrMsg = CreatePlayerPkg( GCSrvError, pPlayer, srvError );
		//pPlayer->SendPkgToPlayer( pErrMsg );
		pPlayer->ReqDestorySelf();
	} else {
		D_ERROR( "收到mapsrv发来未知错误, 错误代码%d\n", pMgError->nErrCode );
	}

	return true;
	TRY_END;
	return false;
}

///Register( M_G_HONOR_MASK, &OnHonorMask );//玩家切换所要显示的称号
bool CDealMapSrvPkg::OnHonorMask( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnHonorMask,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGHonorMask) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGHonorMask* honorMask = (const MGHonorMask*)pPkg;
	if ( honorMask->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		return false;
	}

	//消息准备转发player;
	GCHonorMask climsg;
	StructMemSet( climsg, 0, sizeof(climsg) );

	climsg.honorMask = honorMask->honorMask;

	//群发检测(地图内广播会走此处)；
	if ( pOwner->BroadcastSend< GCHonorMask >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( honorMask->lNotiPlayerID.dwPID );
	if ( NULL == pNotiPlayer )
	{
		D_WARNING( "收到MapSrv切换称号设置结果时，在本GateSrv上找不到欲通知的玩家(%d:%d)\n", honorMask->lNotiPlayerID.wGID, honorMask->lNotiPlayerID.dwPID );
		return false;
	}

	//以下普通聊天消息；
	if ( ( !(pNotiPlayer->IsInFighting()) )
		&& ( !(pNotiPlayer->IsInSwitching()) )
		)
	{
		D_WARNING( "CDealMapSrvPkg::OnHonorMask,玩家尚未进入战斗状态，不应收到此类消息9\n" );
		return false;
	}

	NewSendPlayerPkg( GCHonorMask, pNotiPlayer, climsg );

	return true;
	TRY_END;
	return false;
}

///Register( M_G_USE_MONEY_TYPE, &OnUseMoneyType );//玩家切换所使用的货币
bool CDealMapSrvPkg::OnUseMoneyType( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnUseMoneyType,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGUseMoneyType) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGUseMoneyType* useMoneyType = (const MGUseMoneyType*)pPkg;
	if ( useMoneyType->playerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		return false;
	}

	//消息准备转发player;
	GCUseMoneyType climsg;
	StructMemSet( climsg, 0, sizeof(climsg) );

	climsg.moneyType = useMoneyType->moneyType;

	//群发检测(地图内广播会走此处)；
	if ( pOwner->BroadcastSend< GCUseMoneyType >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( useMoneyType->playerID.dwPID );
	if ( NULL == pNotiPlayer )
	{
		D_WARNING( "收到MapSrv使用钱币设置结果时，在本GateSrv上找不到欲通知的玩家(%d:%d)\n", useMoneyType->playerID.wGID, useMoneyType->playerID.dwPID );
		return false;
	}

	//以下普通聊天消息；
	if ( ( !(pNotiPlayer->IsInFighting()) )
		&& ( !(pNotiPlayer->IsInSwitching()) )
		)
	{
		D_WARNING( "CDealMapSrvPkg::OnUseMoneyType,玩家尚未进入战斗状态，不应收到此类消息9\n" );
		return false;
	}

	NewSendPlayerPkg( GCUseMoneyType, pNotiPlayer, climsg );

	return true;
	TRY_END;
	return false;
}

///mapsrv发来的玩家消息处理;
bool CDealMapSrvPkg::OnPlayerRun( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerRun,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGRun) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGRun* pRun = (const MGRun*)pPkg;

	//////////////////////////////////////////////////////////////////
	////int jjj, test;
	//if ( (pRun->lPlayerID.dwPID) % 1000 == 0 )
	//{
	//	D_ERROR( "测试：收到第%d个跑动消息\n", pRun->lPlayerID.dwPID );

	//	GMReqManedMaps reqManedMaps;//向mapsrv请求该mapsrv管理的地图列表；
	//	reqManedMaps.lGID = 0;
	//	MsgToPut* mapsrvpkg = CreateSrvPkg( GMReqManedMaps, pOwner, reqManedMaps );
	//	pOwner->SendPkgToSrv( mapsrvpkg );
	//}
	//return false;
	////int jjj, test;
	//////////////////////////////////////////////////////////////////

	if ( pRun->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		return false;
	}
	//消息转发mapsrv;
	GCRun  returnMsg;
	returnMsg.sequenceID = pRun->sequenceID;
	returnMsg.nErrCode = (MUX_PROTO::GCRun::ItemType)pRun->nErrCode;
	returnMsg.lPlayerID = pRun->lPlayerID;
	returnMsg.fOrgWorldPosX = pRun->fCurWorldPosX;
	returnMsg.fOrgWorldPosY = pRun->fCurWorldPosY;
	returnMsg.fSpeed = pRun->fSpeed;
	returnMsg.fNewWorldPosX = pRun->fTargetWorldPosX;
	returnMsg.fNewWorldPosY = pRun->fTargetWorldPosY;
	
	if ( pOwner->BroadcastSend< GCRun >( &returnMsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pRun->lNotiPlayerID.dwPID );//通知此玩家；
	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnPlayerRun,玩家尚未进入战斗状态，不应收到此类消息16\n" );
			return false;
		}

		//通知玩家本人时，更新玩家位置消息;
		if ( pRun->lNotiPlayerID.dwPID == pRun->lPlayerID.dwPID )
		{
			pNotiPlayer->SaveTargetPos( pRun->fTargetWorldPosX, pRun->fTargetWorldPosY );
		}

		NewSendPlayerPkg( GCRun, pNotiPlayer, returnMsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCRun, pNotiPlayer, returnMsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );
		//D_WARNING( "通知玩家%x：玩家%x跑动，目标点(%d,%d)，目标点坐标(%f,%f)\n\n"
		//	, pRun->lNotiPlayerID.dwPID, returnMsg.lPlayerID.dwPID, returnMsg.nNewX, returnMsg.nNewY, returnMsg.fNewCroodX, returnMsg.fNewCroodX );
	} else {
		//D_WARNING( "收到MapServer跑动包，但找不到对应玩家%x\n\n", pRun->lNotiPlayerID );
		////////////////////////////////////////////////////////////
		//COMM_BUG
		//static int nNum = 0;
		//++nNum;
		////if ( nNum % 10 == 0 )
		//{
		//	D_WARNING( "收到第%d个移动包\n", nNum );
		//}
		//if ( nNum ==298 )
		//{
		//	int jjj = 0;
		//}
		//COMM_BUG
		////////////////////////////////////////////////////////////

	}
	
	return true;
	TRY_END;
	return false;
}

/////mapsrv发来的玩家出现消息处理;
//bool CDealMapSrvPkg::OnPlayerAppear( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	ACE_UNUSED_ARG(pOwner);
//
//	//检验收包大小是否正确；
//	if ( wPkgLen != sizeof(MGPlayerAppear) )
//	{
//		//收包大小错误，可能是通信两端消息结构不一致；
//		return false;
//	}
//
//	//结构校验通过
//	const MGPlayerAppear* pPlayerAppear = (const MGPlayerAppear*)pPkg;
//	if ( pPlayerAppear->lNotiPlayerID.wGID != SELF_SRV_ID )
//	{
//		//欲通知的玩家不属于本GS上；
//		return false;
//	}
//
//	CPlayer* pStatPlayer = CManPlayer::FindPlayer( pPlayerAppear->lPlayerID.dwPID );
//	if ( NULL == pStatPlayer )
//	{
//		//在本gatesrv找不到消息中指明的出现玩家，可能在gatesrv发appear消息之后，收到mapsrv的appear回复之前玩家就下线了，
//		//此时，由于mapsrv已将其加入了自身的在线列表，因此需要另发一个消息告知mapsrv该玩家已下线，
//		//同样的情形也发生在与centersrv通信时(见收到centersrv唯一性验证结果时的处理）；
//		if ( pPlayerAppear->lNotiPlayerID == pPlayerAppear->lPlayerID )
//		{
//			//只在通知玩家自身时返回一次mapsrv该玩家已下线(收到mapsrv对周边玩家的通知时，不再重复通知mapsrv玩家下线)；
//			GMPlayerLeave  mapMsg;
//			mapMsg.lPlayerID = pPlayerAppear->lPlayerID;
//			mapMsg.lMapCode = pPlayerAppear->playerInfo.uiMP;
//			MsgToPut* pNewMsg = CreateSrvPkg( GMPlayerLeave, pOwner, mapMsg );
//			pOwner->SendPkgToSrv( pNewMsg );
//		}
//		return false;
//	}
//
//	if ( pPlayerAppear->lNotiPlayerID == pPlayerAppear->lPlayerID )
//	{
//	    pStatPlayer->SetPlayerInfo();//update player info;
//		if ( APPEAR_SWITCH_MAP!=pPlayerAppear->appearType)
//		{
//			//置玩家状态为地图中战斗ing;
//			if ( NULL != pStatPlayer )
//			{
//				if ( !pStatPlayer->SetCurStat( PS_MAPFIGHTING ) )
//				{
//					D_WARNING( "置玩家%s进入地图战斗状态失败\n", pStatPlayer->GetAccount() );
//					pStatPlayer->ReqDestorySelf();//断开连接；
//				}
//			}
//		}
//	}
//
//	//通知玩家本人在地图上出现成功，
//	//消息转发给客户端;
//	GCPlayerAppear  returnMsg;
//	returnMsg.appearType = pPlayerAppear->appearType;
//	returnMsg.lPlayerID = pPlayerAppear->lPlayerID;
//	returnMsg.playerInfo = pPlayerAppear->playerInfo;
//
//	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pPlayerAppear->lNotiPlayerID.dwPID );
//	if ( NULL != pNotiPlayer )
//	{
//		MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerAppear, pNotiPlayer, returnMsg );
//		pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端其它玩家出现；
//		//D_WARNING( "通知玩家%x，新玩家%x(角色号%d)在地图上出现(位置%d,%d)\n\n"
//		//	, pPlayerAppear->lNotiPlayerID.dwPID, returnMsg.lPlayerID.dwPID, returnMsg.wRoleNO
//		//	, returnMsg.nPosX, returnMsg.nPosY );
//	} else {
//		//D_WARNING( "收到MapServer玩家出现包，但找不到欲通知玩家%x\n\n", pPlayerAppear->lNotiPlayerID.dwPID );
//	}		
//	return true;
//	TRY_END;
//	return false;
//}

/////临时协议，mapsrv发来的玩家连击消息处理;
//bool CDealMapSrvPkg::OnPlayerCombo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	if( NULL == pOwner)
//	{
//		D_ERROR( "OnPlayerCombo,pOwner空\n" );
//		return false;
//	}
//
//	//检验收包大小是否正确；
//	if ( wPkgLen != sizeof(MGPlayerCombo) )
//	{
//		//收包大小错误，可能是通信两端消息结构不一致；
//		return false;
//	}
//
//	//结构校验通过
//	const MGPlayerCombo* pPlayerCombo = (const MGPlayerCombo*)pPkg;
//	if ( pPlayerCombo->lNotiPlayerID.wGID != SELF_SRV_ID )
//	{
//		//欲通知的玩家不在本GS上；
//		return false;
//	}
//
//	//消息转发player;
//	GCPlayerCombo  climsg;
//	climsg.playerID = pPlayerCombo->playerID;
//	climsg.comboNum = pPlayerCombo->comboNum;
//
//	BroadCastSendCheck( GCPlayerCombo, &climsg ); //群发的检查
//
//	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pPlayerCombo->lNotiPlayerID.dwPID );
//	if ( NULL != pNotiPlayer )
//	{
//		if ( !pNotiPlayer->IsInFighting() )
//		{
//			D_WARNING( "OnPlayerCombo,玩家尚未进入战斗状态，不应收到此类消息10\n" );
//			return false;
//		}
//
//		MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerCombo, pNotiPlayer, climsg );
//		pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
//		//D_WARNING( "收到MapServer玩家连击包，通知对象:%x, 连击玩家%x,连击数\n\n", pPlayerCombo->lNotiPlayerID.dwPID, climsg.playerID.dwPID, climsg.comboNum );
//	} else {
//		//D_WARNING( "收到MapServer玩家连击包，但找不到欲通知玩家%x\n\n", pPlayerCombo->lNotiPlayerID.dwPID );
//	}
//
//	return true;
//	TRY_END;
//	return false;
//}

/////mapsrv发来的玩家信息更新消息;
//bool CDealMapSrvPkg::OnPlayerNewInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//
//	TRY_BEGIN;
//
//	ACE_UNUSED_ARG(pOwner);
//
//	//检验收包大小是否正确；
//	if ( wPkgLen != sizeof(MGPlayerNewInfo) )
//	{
//		//收包大小错误，可能是通信两端消息结构不一致；
//		return false;
//	}
//
//	//结构校验通过
//	const MGPlayerNewInfo* playerNewInfo = (const MGPlayerNewInfo*) pPkg;
//	if ( playerNewInfo->lPlayerID.wGID != SELF_SRV_ID )
//	{
//		D_ERROR( "MapSrv发来非本GS管理玩家的信息更新1\n" );
//		return false;
//	}
//
//	//如果是常规更新消息则直接发给DBSrv，否则如果是离开MapSrv消息，则还需检查是否需要通知CenterSrv该玩家下线；
//	if ( playerNewInfo->infoType == MGPlayerNewInfo::INFO_NORMAL_UPDATE )
//	{
//		//检查该玩家是否还在本GateSrv上，如果不在，则出错；
//		CPlayer* pPlayer = CManPlayer::FindPlayer( playerNewInfo->lPlayerID.dwPID );
//		if ( NULL == pPlayer )
//		{
//			D_ERROR( "MapSrv发来非本GS管理玩家的信息更新1\n" );
//			return false;
//		}
//
//		if ( !pPlayer->IsInFighting() )
//		{
//			D_WARNING( "OnPlayerNewInfo,玩家尚未进入战斗状态，不应收到此类消息13\n" );
//			return false;
//		}
//
//		CDbSrv* pDbSrv = CManDbSrvs::GetDbserver();
//		if ( NULL == pDbSrv )
//		{
//			D_ERROR( "MapSrv发来玩家的信息更新时，找不到对应的DbSrv，严重错误！\n" );
//			return false;
//		}
//		//一切正常，通知DbSrv更新；
//		GDUpdatePlayerInfo updateInfo;
//		updateInfo.playerID = playerNewInfo->lPlayerID;
//		//updateInfo.playerInfo = playerNewInfo->playerInfo;
//		MsgToPut* gdMsg = CreateSrvPkg( GDUpdatePlayerInfo, pDbSrv, updateInfo );
//		pDbSrv->SendPkgToSrv( gdMsg );
//
//		//更新消息通知玩家；
//
//	} else if ( playerNewInfo->infoType == MGPlayerNewInfo::INFO_OFFLINE_UPDATE ) {
//		//检查玩家是否确实不在本GateSrv上，如果不在，说明其确实已下线，应该通知CenterSrv该玩家的下线消息;
//		//否则，说明玩家并未真正下线，只是转到其它MapSrv上玩去了，不能通知CenterSrv其下线消息；
//		//是在此处直接通知CenterSrv还是等后面的包裹消息来了再通知CenterSrv可以根据MapSrv的发包顺序确定或修改；
//		//另外，玩家跳MapSrv之前，应确保其前一个MapSrv的相关信息已经正确保存，以免新MapSrv与旧MapSrv之间的信息冲突！！！
//		bool isrealoffline = false;
//		CPlayer* pPlayer = CManPlayer::FindPlayer( playerNewInfo->lPlayerID.dwPID );
//		if ( NULL == pPlayer )
//		{
//			//如果当前没有这个PID，则玩家肯定已下线，否则继续检查帐号是否一致，如果不一致，则也说明其已经下线；
//			isrealoffline = true;
//		} else if ( pPlayer->GetRoleID() != playerNewInfo->uiRoleID ) {
//			//当前PID对应玩家的帐号不同于更新消息中的玩家帐号，说明之前的玩家确已下线；
//			isrealoffline = true;
//		}
//
//		//无论是否真正下线，都通知DbSrv;
//		CDbSrv* pDbSrv = CManDbSrvs::GetDbserver();
//		if ( NULL == pDbSrv )
//		{
//			D_ERROR( "MapSrv发来玩家的信息更新时，找不到对应的DbSrv，严重错误！\n" );
//			return false;
//		}
//		GDUpdatePlayerInfo updateInfo;
//		updateInfo.playerID = playerNewInfo->lPlayerID;
//		//updateInfo.playerInfo = playerNewInfo->playerInfo;
//		MsgToPut* gdMsg = CreateSrvPkg( GDUpdatePlayerInfo, pDbSrv, updateInfo );
//		pDbSrv->SendPkgToSrv( gdMsg );
//
//		if ( isrealoffline )
//		{
//			//确实已下线，则通知centersrv;
//			CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
//			if ( NULL == pCenterSrv )
//			{
//				D_ERROR( "玩家下线时找不到centersrv\n" );
//				return false;
//			}
//			GEPlayerOffline playerOffline;
//			playerOffline.playerID = playerNewInfo->lPlayerID;
//			SafeStrCpy( playerOffline.szAccount, playerNewInfo->playerInfo.szAccount );
//			MsgToPut* pNewMsg = CreateSrvPkg( GEPlayerOffline, pCenterSrv, playerOffline );
//			pCenterSrv->SendPkgToSrv( pNewMsg );
//
//		} else {
//			//玩家并未真正下线，可能是正准备跳MapSrv，此时将相应更新信息通知玩家；
//
//		}
//
//	} else {
//		D_ERROR( "MapSrv发来不可识别玩家信息更新类型\n" );
//	}
//
//	return true;
//	TRY_END;
//	return false;
//}

///mapsrv发来的玩家聊天消息;
bool CDealMapSrvPkg::OnPlayerChat( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerChat,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGChat) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGChat* playerChat = (const MGChat*)pPkg;
	if ( playerChat->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		return false;
	}

	//消息准备转发player;
	SafeStruct( GCChat, climsg);
	climsg.chatType = playerChat->chatType;
	climsg.extInfo = playerChat->extInfo;
	climsg.sourcePlayerID = playerChat->sourcePlayerID;
	SafeStrCpy( climsg.strChat, playerChat->strChat );
	climsg.chatSize = (UINT)strlen( climsg.strChat ) + 1;

	//群发检测(地图内广播会走此处)；
	if ( pOwner->BroadcastSend< GCChat >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	if ( ( CT_RACE_SYS_EVENT == playerChat->chatType )//如果为种族系统消息/或者种族通知,则直接转发给CentreSrv
		||  ( CT_RACE_NOTI == playerChat->chatType ) )//修改BUG 197975(同族玩家无法收到族长发布的种族公告)
	{
		//若为世界聊天消息，则转发centersrv;	
		GESrvGroupChat srvGroupChat;
		StructMemSet( srvGroupChat, 0x0, sizeof(srvGroupChat) );
		srvGroupChat.chatType = playerChat->chatType;
		srvGroupChat.extInfo = playerChat->extInfo;
		SafeStrCpy( srvGroupChat.chatContent, playerChat->strChat );

		//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
		//MsgToPut* pBrocastMsg = CreateSrvPkg( GESrvGroupChat, pCenterSrv, srvGroupChat );
		//pCenterSrv->SendPkgToSrv( pBrocastMsg );//送centersrv转发；
		SendMsgToCenterSrv( GESrvGroupChat, srvGroupChat );
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( playerChat->lNotiPlayerID.dwPID );
	if ( NULL == pNotiPlayer )
	{
		D_WARNING( "收到MapSrv聊天消息时，在本GateSrv上找不到欲通知的玩家(%d:%d)\n", playerChat->lNotiPlayerID.wGID, playerChat->lNotiPlayerID.dwPID );
		return false;
	}

	//以下普通聊天消息；
	if ( ( !(pNotiPlayer->IsInFighting()) )
		&& ( !(pNotiPlayer->IsInSwitching()) )
		)
	{
		D_WARNING( "OnPlayerChat,玩家尚未进入战斗状态，不应收到此类消息9\n" );
		return false;
	}

	if ( ( CT_WORLD_CHAT == playerChat->chatType )
		|| ( CT_RACE_BRO_CHAT == playerChat->chatType )
		)
	{
		//若为世界聊天消息，则转发centersrv;	
		GESrvGroupChat srvGroupChat;
		srvGroupChat.chatType = playerChat->chatType;
		srvGroupChat.extInfo = playerChat->extInfo;
		SafeStrCpy( srvGroupChat.chatPlayerName, pNotiPlayer->GetRoleName() );
		SafeStrCpy( srvGroupChat.chatContent, playerChat->strChat );

		//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
		//MsgToPut* pBrocastMsg = CreateSrvPkg( GESrvGroupChat, pCenterSrv, srvGroupChat );
		//pCenterSrv->SendPkgToSrv( pBrocastMsg );//送centersrv转发；
		SendMsgToCenterSrv( GESrvGroupChat, srvGroupChat );

		return true;
	}

	NewSendPlayerPkg( GCChat, pNotiPlayer, climsg );

	return true;
	TRY_END;
	return false;
}


/////mapsrv发来的玩家包裹更新消息;
//bool CDealMapSrvPkg::OnPlayerNewPkg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	ACE_UNUSED_ARG(pOwner);
//
//	//检验收包大小是否正确；
//	if ( wPkgLen != sizeof(MGPlayerNewPkg) )
//	{
//		//收包大小错误，可能是通信两端消息结构不一致；
//		return false;
//	}
//
//	//结构校验通过
//	const MGPlayerNewPkg* playerNewPkg = (const MGPlayerNewPkg*) pPkg;
//	if ( playerNewPkg->lPlayerID.wGID != SELF_SRV_ID )
//	{
//		D_ERROR( "MapSrv发来非本GS管理玩家的包裹更新1\n" );
//		return false;
//	}
//
//	//如果是常规更新消息则直接发给DBSrv，否则如果是离开MapSrv消息，则还需检查是否需要通知CenterSrv该玩家下线；
//	if ( playerNewPkg->infoType == MGPlayerNewInfo::INFO_NORMAL_UPDATE )
//	{
//		//检查该玩家是否还在本GateSrv上，如果不在，则出错；
//		CPlayer* pPlayer = CManPlayer::FindPlayer( playerNewPkg->lPlayerID.dwPID );
//		if ( NULL == pPlayer )
//		{
//			D_ERROR( "MapSrv发来非本GS管理玩家的包裹更新1\n" );
//			return false;
//		}
//
//		if ( !pPlayer->IsInFighting() )
//		{
//			D_WARNING( "OnPlayerNewPkg,玩家尚未进入战斗状态，不应收到此类消息15\n" );
//			return false;
//		}
//
//		CDbSrv* pDbSrv = CManDbSrvs::GetDbserver();
//		if ( NULL == pDbSrv )
//		{
//			D_ERROR( "MapSrv发来玩家的包裹更新时，找不到对应的DbSrv，严重错误！\n" );
//			return false;
//		}
//		//一切正常，通知DbSrv更新；
//		GDUpdatePlayerPkg updateInfo;
//		updateInfo.playerID = playerNewPkg->lPlayerID;
//		updateInfo.uiRoleID = playerNewPkg->uiRoleID;
//		updateInfo.pkgData = playerNewPkg->pkgData;
//		MsgToPut* gdMsg = CreateSrvPkg( GDUpdatePlayerPkg, pDbSrv, updateInfo );
//		pDbSrv->SendPkgToSrv( gdMsg );
//
//	} else if ( playerNewPkg->infoType == MGPlayerNewInfo::INFO_OFFLINE_UPDATE ) {
//		//检查玩家是否确实不在本GateSrv上，如果不在，说明其确实已下线，应该通知CenterSrv该玩家的下线消息;
//		//否则，说明玩家并未真正下线，只是转到其它MapSrv上玩去了，不能通知CenterSrv其下线消息；
//		//是在此处直接通知CenterSrv还是由前面的信息更新消息通知CenterSrv，可以根据MapSrv的发包顺序确定或修改；
//		//由前面的信息更新消息通知centersrv，也就是说，mapsrv必须先发包裹更新消息再发信息更新消息以保证正确通知centersrv;
//		//原因：包裹更新消息太大，无法在其中再存放玩家帐号，而通知centersrv时必须知道帐号；
//		//另外，玩家跳MapSrv之前，应确保其前一个MapSrv的相关信息已经正确保存，以免新MapSrv与旧MapSrv之间的信息冲突！！！
//		bool isrealoffline = false;
//		CPlayer* pPlayer = CManPlayer::FindPlayer( playerNewPkg->lPlayerID.dwPID );
//		if ( NULL == pPlayer )
//		{
//			//如果当前没有这个PID，则玩家肯定已下线，否则继续检查帐号是否一致，如果不一致，则也说明其已经下线；
//			isrealoffline = true;
//		} else if ( pPlayer->GetRoleID() != playerNewPkg->uiRoleID ) {
//			//当前PID对应玩家的帐号不同于更新消息中的玩家帐号，说明之前的玩家确已下线；
//			isrealoffline = true;
//		}
//
//		//无论是否真正下线，都通知DbSrv;
//		CDbSrv* pDbSrv = CManDbSrvs::GetDbserver();
//		if ( NULL == pDbSrv )
//		{
//			D_ERROR( "MapSrv发来玩家的包裹更新时，找不到对应的DbSrv，严重错误！\n" );
//			return false;
//		}
//		GDUpdatePlayerPkg updateInfo;
//		updateInfo.playerID = playerNewPkg->lPlayerID;
//		updateInfo.uiRoleID = playerNewPkg->uiRoleID;
//		updateInfo.pkgData = playerNewPkg->pkgData;
//		MsgToPut* gdMsg = CreateSrvPkg( GDUpdatePlayerPkg, pDbSrv, updateInfo );
//		pDbSrv->SendPkgToSrv( gdMsg );
//
//		if ( !isrealoffline )
//		{
//			//玩家并未真正下线，可能是正准备跳MapSrv，此时将相应更新信息通知玩家；
//		}
//
//	} else {
//		D_ERROR( "MapSrv发来不可识别玩家信息更新类型\n" );
//	}
//
//	return true;
//	TRY_END;
//	return false;
//};

//Register( M_G_ENVNOTI_FLAG, &OnEnvNotiFlag );//玩家周围信息通知开始或结束标记；
bool CDealMapSrvPkg::OnEnvNotiFlag( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnEnvNotiFlag,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGEnvNotiFlag) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	MGEnvNotiFlag* notiFlag = ( MGEnvNotiFlag*)pPkg;
	if ( notiFlag->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		return false;
	}

	//消息转发player;
	GCEnvNotiFlag* gcNotiInfo = (&notiFlag->gcNotiFlag);	

	if ( pOwner->BroadcastSend< GCEnvNotiFlag >( gcNotiInfo ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( notiFlag->lNotiPlayerID.dwPID );
	if ( NULL == pNotiPlayer )
	{
		return false;
	}

	if ( !pNotiPlayer->IsInFighting() )
	{
		D_WARNING( "OnEnvNotiFlag,玩家尚未进入战斗状态，不应收到此类消息7\n" );
		return false;
	}

	NewSendPlayerPkg( GCEnvNotiFlag, pNotiPlayer, *gcNotiInfo );

	return true;
	TRY_END;
	return false;
}

///Register( M_G_SIMPLE_PLAYER_INFO, &OnSimplePlayerInfo );//通知玩家，其它玩家简略信息(广播)；
bool CDealMapSrvPkg::OnSimplePlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnSimplePlayerInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGSimplePlayerInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	MGSimplePlayerInfo* simplePlayerInfo = (MGSimplePlayerInfo*)pPkg;
	if ( simplePlayerInfo->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		return false;
	}

	//消息转发player;
	GCSimplePlayerInfo* pOtherInfo = (&simplePlayerInfo->playerInfo);	

	if ( pOwner->BroadcastSend< GCSimplePlayerInfo >( pOtherInfo ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( simplePlayerInfo->lNotiPlayerID.dwPID );
	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnSimplePlayerInfo,玩家尚未进入战斗状态，不应收到此类消息7\n" );
			return false;
		}

		if ( pNotiPlayer->GetPlayerID() == pOtherInfo->otherPlayerID )
		{
			//跨块时，会将周围所有人的消息(包括自身)通知自己（不用BroadcastSend），据此更新自身位置；
			pNotiPlayer->SetCurPos( pOtherInfo->iPosX, pOtherInfo->iPosY );
		}

		NewSendPlayerPkg( GCSimplePlayerInfo, pNotiPlayer, *pOtherInfo );
	} else {
		//D_WARNING( "收到MapServer玩家simpleplayerinfo包，但找不到欲通知玩家%x\n\n", pPlayerleave->lNotiPlayerID.dwPID );
	}
	return true;
	TRY_END;
	return false;
}

///Register( M_G_SIMPLEST_INFO, &OnSimplestInfo );//通知玩家，其它玩家最简信息(聚集情形)；
bool CDealMapSrvPkg::OnSimplestInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	return true;
}

///mapsrv发来的其它玩家消息处理;
bool CDealMapSrvPkg::OnOtherPlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnOtherPlayerInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGOtherPlayerInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	MGOtherPlayerInfo* otherPlayerInfo = ( MGOtherPlayerInfo*)pPkg;
	if ( otherPlayerInfo->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		return false;
	}

	//消息转发player;
	GCOtherPlayerInfo* pOtherInfo = (&otherPlayerInfo->playerInfo);	


	//2008/06/19 辅助技能
	//ACE_OS::memcpy(climsg.buffData,otherPlayerInfo->buffData,BUFF_SIZE*sizeof(BuffID) );

	//BroadCastSendCheck( GCOtherPlayerInfo, &climsg ); //群发的检查

	if ( pOwner->BroadcastSend< GCOtherPlayerInfo >( pOtherInfo ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( otherPlayerInfo->lNotiPlayerID.dwPID );
	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnOtherPlayerInfo,玩家尚未进入战斗状态，不应收到此类消息7\n" );
			return false;
		}

		if ( pNotiPlayer->GetPlayerID() == pOtherInfo->otherPlayerID )
		{
			//跨块时，会将周围所有人的消息(包括自身)通知自己（不用BroadcastSend），据此更新自身位置；
			pNotiPlayer->SetCurPos( pOtherInfo->playerInfo.iPosX, pOtherInfo->playerInfo.iPosY );
		}

		NewSendPlayerPkg( GCOtherPlayerInfo, pNotiPlayer, *pOtherInfo );
	} else {
		//D_WARNING( "收到MapServer玩家otherplayerinfo包，但找不到欲通知玩家%x\n\n", pPlayerleave->lNotiPlayerID.dwPID );
	}
	return true;
	TRY_END;
	return false;
}

//通知玩家或怪物位置信息，用于纠正客户端位置；
bool CDealMapSrvPkg::OnPlayerPosInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "CDealMapSrvPkg::OnPlayerPosInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGPlayerPosInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	MGPlayerPosInfo* pPosInfo = (MGPlayerPosInfo*)pPkg;

	if ( pOwner->BroadcastSend< GCPlayerPosInfo >( &(pPosInfo->playerPosInfo) ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pPosInfo->lNotiPlayerID.dwPID );
	if ( NULL == pNotiPlayer )
	{
		//玩家之前已经下线了
		D_ERROR( "OnPlayerPosInfo，找不到欲通知的玩家%d\n", pPosInfo->lNotiPlayerID.dwPID );
		return false;
	}

	if( !pNotiPlayer->IsInFighting() )
	{
		D_ERROR( "OnPlayerPosInfo 玩家%d失败消息处理，玩家不处于战斗状态\n", pPosInfo->lNotiPlayerID.dwPID );
		return false;
	}

	NewSendPlayerPkg( GCPlayerPosInfo, pNotiPlayer, pPosInfo->playerPosInfo );	
	
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnKickPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "CDealMapSrvPkg::OnKickPlayer,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGKickPlayer) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const MGKickPlayer* pKickPlayer = (const MGKickPlayer*)pPkg;
	
	D_INFO("收到MapSrv踢人请求 遭受踢除的玩家PID %d\n", pKickPlayer->targetPlayerID.dwPID );
	CPlayer* pPlayer = CManPlayer::FindPlayer( pKickPlayer->targetPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		//玩家之前已经下线了
		//add by dzj, 09.10.10,收到mapsrv请求踢此人下线时，发现此人已不在，则通知mapsrv直接删去；
		GMPlayerLeave  leaveGMMsg;
		leaveGMMsg.lPlayerID = pKickPlayer->targetPlayerID;
		leaveGMMsg.lMapCode = 0;
		leaveGMMsg.nSpecialFlag = GMPlayerLeave::NOT_EXIST;//gatesrv已直接删去并通知DB;
		//MsgToPut* pNewMsg = CreateSrvPkg( GMPlayerLeave, pOwner, leaveGMMsg );
		//pOwner->SendPkgToSrv( pNewMsg );
		SendMsgToSrv( GMPlayerLeave, pOwner, leaveGMMsg );

		D_DEBUG( "OnKickPlayer,NOT_EXIST通知mapsrv%d,玩家(%d:%d)已离线\n", pOwner->GetID(), pKickPlayer->targetPlayerID.wGID, pKickPlayer->targetPlayerID.dwPID );

		return false;
	}

	//if( !pPlayer->IsInFighting() )
	//{
	//	D_ERROR( "OnKickPlayer 玩家%x失败消息处理，玩家不处于战斗状态\n", pKickPlayer->targetPlayerID.dwPID );
	//	return false;
	//}

	//未使用的代码，删去，by dzj, 09.10.10,SafeStruct( GCChat, cliMsg );
	//cliMsg.nType = CT_SYS_EVENT_NOTI;
	//SafeStrCpy( cliMsg.strChat, "您已经被GM踢下线" );
	//cliMsg.chatSize = (UINT)strlen( cliMsg.strChat ) +1 ;

	pPlayer->ReqDestorySelf();
	
	return true;
	TRY_END;
	return false;
}

///mapsrv发来的玩家离线消息处理;
bool CDealMapSrvPkg::OnPlayerLeave( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerLeave,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGPlayerLeave) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGPlayerLeave* pPlayerleave = (const MGPlayerLeave*)pPkg;
	if ( pPlayerleave->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		return false;
	}

	//消息转发player;
	GCPlayerLeave  climsg;
	climsg.lPlayerID = pPlayerleave->lPlayerID;
	climsg.lMapCode = pPlayerleave->lMapCode;

	if ( pOwner->BroadcastSend< GCPlayerLeave >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pPlayerleave->lNotiPlayerID.dwPID );
	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnPlayerLeave,玩家尚未进入战斗状态，不应收到此类消息11\n" );
			return false;
		}

		NewSendPlayerPkg( GCPlayerLeave, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerLeave, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
		//D_WARNING( "收到MapServer玩家离开包，通知对象:%x, 离线玩家%x\n\n", pPlayerleave->lNotiPlayerID.dwPID, climsg.lPlayerID.dwPID );
	} else {
		//D_WARNING( "收到MapServer玩家离开包，但找不到欲通知玩家%x\n\n", pPlayerleave->lNotiPlayerID.dwPID );
	}
	return true;
	TRY_END;
	return false;
}

///mapsrv发来的怪物出生消息;
bool CDealMapSrvPkg::OnMonsterBorn( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMonsterBorn,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGMonsterBorn) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGMonsterBorn* monsterBorn = (const MGMonsterBorn*)pPkg;
	if ( monsterBorn->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "收到MapServer怪物出生包，欲通知的对象:%x不在本ＧＳ上\n\n", monsterBorn->lNotiPlayerID.dwPID );
		return false;
	}

	//消息转发player;
	GCMonsterBorn  climsg;
	climsg.lMonsterID = monsterBorn->lMonsterID;
	climsg.lMonsterTypeID = monsterBorn->lMonsterTypeID;
	climsg.nMapPosX = monsterBorn->nMapPosX;
	climsg.nMapPosY = monsterBorn->nMapPosY;

	if ( pOwner->BroadcastSend< GCMonsterBorn >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( monsterBorn->lNotiPlayerID.dwPID );
	//unsigned short wCmd = MGMonsterBorn::wCmd;//用于日志；
	//unsigned long  lNotiPlayerID = (pNotiPlayer->GetPlayerID()).dwPID;//用于日志；
	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnMonsterBorn,玩家尚未进入战斗状态，不应收到此类消息2\n" );
			return false;
		}

		NewSendPlayerPkg( GCMonsterBorn, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCMonsterBorn, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
		//D_WARNING( "收到mapsrv怪物出生消息，怪物ID：%x，通知玩家:%x\n\n", climsg.lMonsterID, lNotiPlayerID );
	} else {
		//D_WARNING( "收到mapsrv消息%x，但找不到欲通知玩家%x\n\n", wCmd, lNotiPlayerID );
	}

	return true;
	TRY_END;
	return false;
}

///mapsrv发来的怪物移动消息;
bool CDealMapSrvPkg::OnMonsterMove( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMonsterMove,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGMonsterMove) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGMonsterMove* monsterMove = (const MGMonsterMove*)pPkg;
	if ( monsterMove->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "收到MapServer怪物移动包，欲通知的对象:%x不在本ＧＳ上\n\n", monsterMove->lNotiPlayerID.dwPID );
		return false;
	}

	/*
		TYPE_ID                  	lMonsterID;                                 	//怪物ID，地图内唯一
		TYPE_POS                 	nOrgMapPosX;                                	//出发点X；
		TYPE_POS                 	nOrgMapPosY;                                	//出发点Y；
		TYPE_POS                 	nNewMapPosX;                                	//目标点X；
		TYPE_POS                 	nNewMapPosY;                                	//目标点Y；
		TYPE_POS                 	nTargetMapPosX;                             	//怪物的远距离目标点,以此可以看上去流畅
		TYPE_POS                 	nTargetMapPosY;                             	//怪物的远距离目标点
		FLOAT                    	fSpeed;                                     	//移动速度
	*/

	//位置调试，D_DEBUG( "通知怪物移动：出发点(%d,%d), 速度(%f), 目标点(%d,%d)\n"
	//	, monsterMove->gcMonsterMove.nOrgMapPosX, monsterMove->gcMonsterMove.nOrgMapPosY, monsterMove->gcMonsterMove.fSpeed
	//	, monsterMove->gcMonsterMove.nNewMapPosX, monsterMove->gcMonsterMove.nNewMapPosY );

	//消息转发player;
	/*GCMonsterMove  climsg;
	climsg.lMonsterID = monsterMove->lMonsterID;
	climsg.nNewMapPosX = monsterMove->nNewMapPosX;
	climsg.nNewMapPosY = monsterMove->nNewMapPosY;
	climsg.nOrgMapPosX = monsterMove->nOrgMapPosX;
	climsg.nOrgMapPosY = monsterMove->nOrgMapPosY;
	climsg.nTargetMapPosX = monsterMove->nTargetMapPosX;
	climsg.nTargetMapPosY = monsterMove->nTargetMapPosY;
	climsg.fSpeed = monsterMove->fSpeed;*/

	GCMonsterMove* pGCMonsterMove = (GCMonsterMove *)&monsterMove->gcMonsterMove;


	if ( pOwner->BroadcastSend< GCMonsterMove >( pGCMonsterMove ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( monsterMove->lNotiPlayerID.dwPID );
	//unsigned short wCmd = MGMonsterMove::wCmd;//用于日志；
	//unsigned long  lNotiPlayerID = (pNotiPlayer->GetPlayerID()).dwPID;//用于日志；
	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnMonsterMove,玩家尚未进入战斗状态，不应收到此类消息6\n" );
			return false;
		}

		NewSendPlayerPkg( GCMonsterMove, pNotiPlayer, *pGCMonsterMove );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCMonsterMove, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
		//D_WARNING( "收到mapsrv消息%x，通知玩家:%x\n\n", wCmd, lNotiPlayerID );
	} else {
		//D_WARNING( "收到mapsrv消息%x，但找不到欲通知玩家%x\n\n", wCmd, lNotiPlayerID );
	}

	return true;
	TRY_END;
	return false;
}

//mapsrv发来怪物消失消息
bool CDealMapSrvPkg::OnMonsterDisappear( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMonsterDisappear,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGMonsterDisappear) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGMonsterDisappear* monsterDisappear = (const MGMonsterDisappear*)pPkg;
	if ( monsterDisappear->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "收到MapServer怪物消失包，欲通知的对象:%x不在本ＧＳ上\n\n", monsterDisappear->lNotiPlayerID.dwPID );
		return false;
	}

	//消息转发player;
	GCMonsterDisappear  climsg;
	climsg.lMonsterID = monsterDisappear->lMonsterID;
	climsg.disType = monsterDisappear->disType;

	if ( pOwner->BroadcastSend< GCMonsterDisappear >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( monsterDisappear->lNotiPlayerID.dwPID );
	//unsigned short wCmd = MGMonsterDisappear::wCmd;//用于日志；
	//unsigned long  lNotiPlayerID = (pNotiPlayer->GetPlayerID()).dwPID;//用于日志；
	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnMonsterDisappear,玩家尚未进入战斗状态，不应收到此类消息3\n" );
			return false;
		}

		NewSendPlayerPkg( GCMonsterDisappear, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCMonsterDisappear, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
		//D_WARNING( "收到mapsrv怪物消失消息，消失怪物:%d，第%d次消失，通知玩家:%x\n\n", climsg.lMonsterID, climsg.lDebugFlag, lNotiPlayerID );
	} else {
		//D_WARNING( "收到mapsrv消息%x，但找不到欲通知玩家%x\n\n", wCmd, lNotiPlayerID );
	}

	return true;
	TRY_END;
	return false;
}

/////mapsrv发来的玩家攻击玩家消息;
//bool CDealMapSrvPkg::OnPlayerAttackPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	if( NULL == pOwner)
//	{
//		D_ERROR( "OnPlayerAttackPlayer,pOwner空\n" );
//		return false;
//	}
//
//	//检验收包大小是否正确；
//	if ( wPkgLen != sizeof(MGPlayerAttackPlayer) )
//	{
//		//收包大小错误，可能是通信两端消息结构不一致；
//		return false;
//	}
//
//	//结构校验通过
//	const MGPlayerAttackPlayer* playerBeAttacked = (const MGPlayerAttackPlayer*)pPkg;
//	if ( playerBeAttacked->lNotiPlayerID.wGID != SELF_SRV_ID )
//	{
//		//欲通知的玩家不在本GS上；
//		D_WARNING( "收到MapServer玩家攻击玩家包，欲通知的对象的GID与本gatesrv不匹配\n\n", playerBeAttacked->lNotiPlayerID.dwPID );
//		return false;
//	}
//
//	//消息转发player;
//	GCPlayerAttackPlayer  climsg;
//
//	climsg.nErrCode = playerBeAttacked->nErrCode;
//	climsg.attackerID = playerBeAttacked->attackerID;
//	climsg.targetID = playerBeAttacked->targetID;
//	climsg.nSkillID = playerBeAttacked->nSkillID;
//	climsg.attackerPosX = playerBeAttacked->attackerPosX;
//	climsg.attackerPosY = playerBeAttacked->attackerPosY;
//	climsg.targetPosX = playerBeAttacked->targetPosX;
//	climsg.targetPosY = playerBeAttacked->targetPosY;
//	climsg.nDamage = playerBeAttacked->nDamage;
//	climsg.nLeftHp = playerBeAttacked->nLeftHp;
//
//	BroadCastSendCheck( GCPlayerAttackPlayer, &climsg ); //群发的检查
//
//	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( playerBeAttacked->lNotiPlayerID.dwPID );
//
//	if ( NULL != pNotiPlayer )
//	{
//		if ( !pNotiPlayer->IsInFighting() )
//		{
//			D_WARNING( "OnPlayerAttackPlayer,玩家尚未进入战斗状态，不应收到此类消息1\n" );
//			return false;
//		}
//
//		MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerAttackPlayer, pNotiPlayer, climsg );
//		pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
//	} else {
//		D_WARNING( "收到玩家攻击玩家包，本gatesrv找不到该玩家\n" );
//	}
//
//	return true;
//	TRY_END;
//	return false;
//}


///mapsrv发来的怪物攻击玩家消息;
bool CDealMapSrvPkg::OnMonsterAttackPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMonsterAttackPlayer,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGMonsterAttackPlayer) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGMonsterAttackPlayer* playerBeAttacked = (const MGMonsterAttackPlayer*)pPkg;
	if ( playerBeAttacked->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "收到MapServer玩家攻击怪物包，欲通知的对象的GID与本gatesrv不匹配\n\n", playerBeAttacked->lNotiPlayerID.dwPID );
		return false;
	}

	//消息转发player;
	GCMonsterAttackPlayer  climsg;

	climsg.nErrCode = playerBeAttacked->nErrCode;
	climsg.lPlayerID = playerBeAttacked->lPlayerID;
	climsg.battleFlag = playerBeAttacked->battleFlag;
	climsg.scopeFlag = playerBeAttacked->scopeFlag;
	climsg.lMonsterID = playerBeAttacked->lMonsterID;
	climsg.lMonsterType = playerBeAttacked->lMonsterType;
	climsg.nMapPosX = playerBeAttacked->nMapPosX;
	climsg.nMapPosY = playerBeAttacked->nMapPosY;
	climsg.nSkillID = playerBeAttacked->nSkillID;
	climsg.nCurseTime = playerBeAttacked->nCurseTime;
	climsg.nDamage = playerBeAttacked->nDamage;
	climsg.nLeftHp = playerBeAttacked->nLeftHp;

	if ( pOwner->BroadcastSend< GCMonsterAttackPlayer >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( playerBeAttacked->lNotiPlayerID.dwPID );

	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnMonsterAttackPlayer,玩家尚未进入战斗状态，不应收到此类消息1\n" );
			return false;
		}

		NewSendPlayerPkg( GCMonsterAttackPlayer, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCMonsterAttackPlayer, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
	} else {
		D_WARNING( "收到怪物攻击玩家包，本gatesrv找不到该玩家%x\n", playerBeAttacked->lNotiPlayerID.dwPID );
	}

	return true;
	TRY_END;
	return false;
}

///mapsrv发来的玩家打怪物消息;
bool CDealMapSrvPkg::OnPlayerAttackTarget( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerAttackMonster,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGPlayerAttackTarget) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGPlayerAttackTarget* creatureBeAttacked = (const MGPlayerAttackTarget*)pPkg;
	if ( creatureBeAttacked->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "收到MapServer玩家攻击怪物包，欲通知的对象:%x不在本ＧＳ上\n\n", creatureBeAttacked->lNotiPlayerID.dwPID );
		return false;
	}

	//消息转发player;
	SafeStruct( GCPlayerAttackTarget, climsg );
	climsg.sequenceID = creatureBeAttacked->sequenceID;
	climsg.nErrCode = creatureBeAttacked->nErrCode;
	climsg.scopeFlag = creatureBeAttacked->scopeFlag;	//是否列表中的一个以及是列表中的正常包还是结束包;
	climsg.lPlayerID = creatureBeAttacked->lPlayerID;
	climsg.battleFlag = creatureBeAttacked->battleFlag;
	climsg.skillID = creatureBeAttacked->skillID;//爆击类型；
	climsg.lCurseTime = creatureBeAttacked->lCurseTime;//攻击类型；
	climsg.lCooldownTime = creatureBeAttacked->lCooldownTime;
	climsg.targetID = creatureBeAttacked->targetID;
	climsg.lHpSubed = creatureBeAttacked->lHpSubed;
	climsg.lHpLeft = creatureBeAttacked->lHpLeft;
	climsg.nAtkPlayerHP = creatureBeAttacked->nAtkPlayerHP;
	climsg.nAtkPlayerMP = creatureBeAttacked->nAtkPlayerMP;
	climsg.consumeItemID = creatureBeAttacked->consumeItemTypeID;
	climsg.targetRewardID = creatureBeAttacked->rewardID;
	climsg.aoeMainTargetID = creatureBeAttacked->mainTargetID;
	climsg.aoeAttackPosX = creatureBeAttacked->aoePosX;
	climsg.aoeAttackPosY = creatureBeAttacked->aoePosY;

	if ( pOwner->BroadcastSend< GCPlayerAttackTarget >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( creatureBeAttacked->lNotiPlayerID.dwPID );

	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnPlayerAttackMonster,玩家尚未进入战斗状态，不应收到此类消息8\n" );
			return false;
		}

		NewSendPlayerPkg( GCPlayerAttackTarget, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerAttackTarget, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
	} else {

	}

	return true;
	TRY_END;
	return false;
}

///// 玩家切换地图
//bool CDealMapSrvPkg::OnSwitchMap( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	if( NULL == pOwner)
//	{
//		D_ERROR( "OnSwitchMap,pOwner空\n" );
//		return false;
//	}
//
//	// 检查消息长度
//	if ( wPkgLen != sizeof(MGSwitchMap) )
//	{
//		//收包大小错误y，可能是通信两端消息结构不一致；
//		return false;
//	}
//
//	// 检查gateID
//	MGSwitchMap *pSwitchMsg = (MGSwitchMap *)pPkg;
//	if ( pSwitchMsg->playerID.wGID != SELF_SRV_ID )
//	{
//		//欲通知的玩家不在本GS上；
//		D_WARNING( "收到MapServer玩家切换地图包, 对应玩家对象:%x不在本ＧＳ上\n\n", pSwitchMsg->playerID.dwPID );
//		return false;
//	}
//
//	GCSwitchMap switchMap;
//	switchMap.playerID = pSwitchMsg->playerID;
//	switchMap.nErrCode = pSwitchMsg->nErrCode;
//	switchMap.usMapID = pSwitchMsg->usMapID;
//	switchMap.iPosX = pSwitchMsg->iPosX;
//	switchMap.iPosY = pSwitchMsg->iPosY;
//
//	BroadCastSendCheck( GCSwitchMap, &switchMap ); //群发的检查
//
//	CPlayer* pPlayer = CManPlayer::FindPlayer(pSwitchMsg->playerID.dwPID);
//	if(pPlayer != NULL)
//	{
//		MsgToPut* pNewMsg = CreatePlayerPkg( GCSwitchMap, pPlayer, switchMap );
//		pPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
//	}
//
//	return true;
//	TRY_END
//	return true;
//}

///通知玩家进入死亡倒计时;
bool CDealMapSrvPkg::OnPlayerDying( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerDying,pOwner空\n" );
		return false;
	}

	// 检查消息长度
	if ( wPkgLen != sizeof(MGPlayerDying) )
	{
		//收包大小错误y，可能是通信两端消息结构不一致；
		return false;
	}

	// 检查gateID
	const MGPlayerDying* pPlayerDying = (const MGPlayerDying *)pPkg;
	if ( pPlayerDying->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "CDealMapSrvPkg::OnPlayerDyin, 对应玩家对象:%x不在本ＧＳ上\n\n", pPlayerDying->lNotiPlayerID.dwPID );
		return false;
	}

	GCPlayerDying tosend;
	tosend.dyingPlayerID = pPlayerDying->dyingPlayerID;
	tosend.countDownSec = pPlayerDying->countDownSec;

	if ( pOwner->BroadcastSend< GCPlayerDying >( &tosend ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pPlayer = CManPlayer::FindPlayer( pPlayerDying->lNotiPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		return false;
	}

	NewSendPlayerPkg( GCPlayerDying, pPlayer, tosend );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerDying, pPlayer, tosend );
	//pPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
	D_DEBUG("发送玩家%s死亡消息 GID:%d PID:%d countDownSec:%d\n", pPlayer->GetRoleName(), pPlayerDying->dyingPlayerID.wGID, pPlayerDying->dyingPlayerID.dwPID, tosend.countDownSec );

	return true;
	TRY_END;
	return false;
}

///通知玩家复活点信息；
bool CDealMapSrvPkg::OnRebirthInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnRebirthInfo,pOwner空\n" );
		return false;
	}

	// 检查消息长度
	if ( wPkgLen != sizeof(MGRebirthInfo) )
	{
		//收包大小错误y，可能是通信两端消息结构不一致；
		return false;
	}

	// 检查gateID
	const MGRebirthInfo* pRebirthInfo = (const MGRebirthInfo *)pPkg;
	if ( pRebirthInfo->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "CDealMapSrvPkg::OnRebirthInfo, 对应玩家对象:%x不在本ＧＳ上\n\n", pRebirthInfo->lNotiPlayerID.dwPID );
		return false;
	}

	GCRebirthInfo tosend;
	tosend.sequenceID = pRebirthInfo->sequenceID;
	tosend.dyingPlayerID = pRebirthInfo->dyingPlayerID;
	tosend.mapID = pRebirthInfo->mapID;
	tosend.nPosX = pRebirthInfo->nPosX;
	tosend.nPosY = pRebirthInfo->nPosY;

	if ( pOwner->BroadcastSend< GCRebirthInfo >( &tosend ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pRebirthInfo->lNotiPlayerID.dwPID );
	if ( pPlayer != NULL )
	{
		NewSendPlayerPkg( GCRebirthInfo, pPlayer, tosend );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCRebirthInfo, pPlayer, tosend );
		//pPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
	}

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnGetBatchItemSeq( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{	
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnGetBatchItemSeq,pOwner空\n" );
		return false;
	}

	ACE_UNUSED_ARG( pPkg );
	
	// 检查消息长度
	if ( wPkgLen != sizeof(MGBatchItemSequence) )
	{
		return false;
	}
	
	// 转发
	GDBatchItemSequence batchItemSeq;
	batchItemSeq.uiServerID = pOwner->GetID();
	
	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByID( 0 );//只找dbsrv 0，注意这个0不是seq顺序号，而是配置的dbsrv ID号;
	if ( NULL == pDbSrv )
	{
		D_WARNING( "找不到dbsrv 0，获取物品流水号错误\n\n" );
		return false;
	}

	//MsgToPut* pNewMsg = CreateSrvPkg( GDBatchItemSequence, pDbSrv, batchItemSeq );
	//pDbSrv->SendPkgToSrv( pNewMsg );
	SendMsgToSrv( GDBatchItemSequence, pDbSrv, batchItemSeq );

	return true;
	TRY_END;
	return false;
}

//bool CDealMapSrvPkg::OnUpdateItemInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	if( NULL == pOwner)
//	{
//		D_ERROR( "OnUpdateItemInfo,pOwner空\n" );
//		return false;
//	}
//
//	// 检查消息长度
//	if ( wPkgLen != sizeof(MGUpdateItemInfo) )
//	{
//		return false;
//	}
//
//	MGUpdateItemInfo *pUpdateItem = (MGUpdateItemInfo *)pPkg;
//
//	// 转发
//	GDUpdateItemInfo forwardItem;
//	forwardItem.playerID = pUpdateItem->playerID;
//	forwardItem.ucIndex = pUpdateItem->ucIndex;
//	memcpy(&(forwardItem.itemInfo), &(pUpdateItem->itemInfo), sizeof(EquipItemInfo)); 
//
//	CDbSrv* pDbSrv = CManDbSrvs::GetDbserver();
//	if ( NULL == pDbSrv )
//	{
//		D_WARNING( "找不到dbsrv，更新道具信息错误\n\n" );
//		return false;
//	}
//
//	MsgToPut* pNewMsg = CreateSrvPkg( GDUpdateItemInfo, pDbSrv, forwardItem );
//	pDbSrv->SendPkgToSrv( pNewMsg );
//
//	return true;
//	TRY_END;
//	return false;
//}

///通知AssignSrv帐号与地图号对应信息；
void AssignSrvReportAccountMap( const char* playerAccount, unsigned int mapid );

///玩家下线时的更新消息处理；
bool CDealMapSrvPkg::OfflineInfoProc( CMapSrv* pMapSrv, const MGFullPlayerInfo* pMGFullPlayerInfo )
{
	TRY_BEGIN;

	if( NULL == pMapSrv )
	{
		D_ERROR( "OfflineInfoProc,pOwner空\n" );
		return false;
	}

	FullPlayerInfo* fullPlayerInfo = NULL; 

	//由于多个玩家的下线消息可能会混杂到来，因此需要这样处理；
	static map< unsigned long, FullPlayerInfo* > bufOfflinePlayerInfo;
	map< unsigned long, FullPlayerInfo* >::iterator iter = bufOfflinePlayerInfo.find( pMGFullPlayerInfo->playerID.dwPID );
	if ( iter != bufOfflinePlayerInfo.end() )
	{
		fullPlayerInfo = iter->second;
	} else {
		fullPlayerInfo = NEW FullPlayerInfo;//新建一个临时保存结构，准备保存后续消息；
		bufOfflinePlayerInfo.insert( pair<unsigned long, FullPlayerInfo*>( pMGFullPlayerInfo->playerID.dwPID, fullPlayerInfo ) );
	}

	if ( NULL == fullPlayerInfo )
	{
		D_ERROR( "fullPlayerInfo空，不可能错误\n" );
		return false;
	}

	//如果信息完整，则应以此信息更新DB
	if ( MGFullPlayerInfo::MG_FULLINFO_OK == pMGFullPlayerInfo->validFlag )
	{
		if(!pMGFullPlayerInfo->bEnd)
		{
			//消息没收完，不处理，返回等待后续消息；
			ReceiveFullStr<MGFullPlayerInfo, FullPlayerInfo>(pMGFullPlayerInfo, fullPlayerInfo);
			return false;
		} else {		
			//消息接收完毕，进行正常处理；
			ReceiveFullStr<MGFullPlayerInfo, FullPlayerInfo>(pMGFullPlayerInfo, fullPlayerInfo);
		}

		///非APPEAR类型需要通知DBSrv;
		CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( fullPlayerInfo->baseInfo.szAccount );
		if ( NULL == pDbSrv )
		{
			D_ERROR( "MapSrv发来玩家的信息更新时，找不到对应的DbSrv，严重错误！\n" );
			NoDBPlayerInfoBackup( fullPlayerInfo->baseInfo.szAccount, *fullPlayerInfo );
			//return; //仍然要进行后续动作，例如通知centersrv该玩家下线等；
		} else {
			//保存玩家信息至DB；
			D_DEBUG( "OfflineInfoProc,玩家已先行断线,保存玩家%s:%d(%d,%d)信息至DB\n"
				, fullPlayerInfo->baseInfo.szAccount, fullPlayerInfo->baseInfo.usMapID, fullPlayerInfo->baseInfo.iPosX, fullPlayerInfo->baseInfo.iPosY );

			std::vector<GDUpdatePlayerInfo> vec;
			SendFullStr<FullPlayerInfo, GDUpdatePlayerInfo>(fullPlayerInfo,  vec);
			for(std::vector<GDUpdatePlayerInfo>::iterator i = vec.begin(); vec.end() != i; i++)
			{
				i->playerID = pMGFullPlayerInfo->playerID;
				i->uiRoleID = fullPlayerInfo->baseInfo.uiID;//如果无此信息,则DB无法更新数据库；
				i->isOffline = true;

				//MsgToPut* pNewMsg = CreateSrvPkg( GDUpdatePlayerInfo, pDbSrv, *i );
				//pDbSrv->SendPkgToSrv( pNewMsg );
				SendMsgToSrv( GDUpdatePlayerInfo, pDbSrv, *i );
			}
		}
	} else { //if ( MGFullPlayerInfo::MG_FULLINFO_OK == pMGFullPlayerInfo->validFlag )
		//如果消息包中玩家相关信息无效，则消息包中data存放的为相应玩家的帐号；
		D_ERROR( "OfflineInfoProc，MGFullPlayerInfo::MG_FULLINFO_OK ！= pMGFullPlayerInfo->validFlag，玩家%s的存盘信息可能不完整\n", pMGFullPlayerInfo->pszAccount );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//通知centersrv;
	CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
	if ( NULL == pCenterSrv )
	{
		D_ERROR( "玩家下线时找不到centersrv\n" );
		return false;
	}
	GEPlayerOffline playerOffline;
	playerOffline.playerID = pMGFullPlayerInfo->playerID;
	if ( MGFullPlayerInfo::MG_FULLINFO_OK == pMGFullPlayerInfo->validFlag )
	{
		//如果消息包中玩家相关信息有效，应从玩家full info中取帐号名；
		if ( NULL != fullPlayerInfo )
		{
			SafeStrCpy( playerOffline.szAccount, fullPlayerInfo->baseInfo.szAccount );
			//MsgToPut* pNewMsg = CreateSrvPkg( GEPlayerOffline, pCenterSrv, playerOffline );
			//pCenterSrv->SendPkgToSrv( pNewMsg );
			SendMsgToCenterSrv( GEPlayerOffline, playerOffline );
			D_DEBUG( "2、通知centersrv,%s下线\n", playerOffline.szAccount );

			CLogManager::DoRoleLeaveGame(fullPlayerInfo->baseInfo.uiID, fullPlayerInfo->baseInfo.usLevel, 0, 0);//记录角色下线
			CLogManager::DoAccountLogout(fullPlayerInfo->baseInfo.szAccount, 0, 0);//记录账号下线

			AssignSrvReportAccountMap( playerOffline.szAccount, fullPlayerInfo->baseInfo.usMapID );//正常下线时，通知assignsrv该玩家上次下线时的地图号；
		} else {
			D_ERROR( "收到玩家full info时，无法取玩家帐号，玩家下线通知centersrv失败\n" );
		}
	} else {
		//如果消息包中玩家相关信息无效，则消息包中data存放的为相应玩家的帐号，应从此取出传centersrv；
		SafeStrCpy( playerOffline.szAccount, pMGFullPlayerInfo->pszAccount );
		//MsgToPut* pNewMsg = CreateSrvPkg( GEPlayerOffline, pCenterSrv, playerOffline );
		//pCenterSrv->SendPkgToSrv( pNewMsg );
		SendMsgToCenterSrv( GEPlayerOffline, playerOffline );
		D_DEBUG( "3、通知centersrv,%s下线，但该玩家信息可能没有在DB正确保存\n", playerOffline.szAccount );

		CLogManager::DoRoleLeaveGame(fullPlayerInfo->baseInfo.uiID, fullPlayerInfo->baseInfo.usLevel, 0, 0);//记录角色下线
		CLogManager::DoAccountLogout(fullPlayerInfo->baseInfo.szAccount, 0, 0);//记录下线
	}

	//最后应将存在map中的临时信息删去；
	iter = bufOfflinePlayerInfo.find( pMGFullPlayerInfo->playerID.dwPID );
	if ( iter != bufOfflinePlayerInfo.end() )
	{
		delete iter->second; iter->second = NULL;
		bufOfflinePlayerInfo.erase( iter );
	}
	//如果是下线且玩家已真正下线，则通知centersrv;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return true;
	TRY_END;
	return false;
}

///玩家最终复活结果；MGFinalRebirthRst
bool CDealMapSrvPkg::OnFinalRebirthRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnEnterMapRst,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGFinalRebirthRst) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGFinalRebirthRst* pFinalRebirthRst = (const MGFinalRebirthRst*) pPkg;
	if ( pFinalRebirthRst->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		D_ERROR( "MapSrv发来非本GS管理玩家的进地图结果1\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pFinalRebirthRst->lNotiPlayerID.dwPID );

	if ( NULL == pPlayer ) 
	{
		D_ERROR( "收到玩家MGFinalRebirthRst消息时，玩家%d已下线\n", pFinalRebirthRst->lNotiPlayerID.dwPID );
		//之前的下线消息肯定已发到mapsrv，因此无需再通知, by dzj, 09.10.10 //通知mapsrv该玩家已下线；
		//GMPlayerLeave  mapMsg;
		//mapMsg.lPlayerID = pFinalRebirthRst->lNotiPlayerID;
		//mapMsg.lMapCode = 0;
		//mapMsg.nSpecialFlag = GMPlayerLeave::NOT_EXIST;//玩家已不存在于本gatesrv;
		//MsgToPut* pNewMsg = CreateSrvPkg( GMPlayerLeave, pOwner, mapMsg );
		//pOwner->SendPkgToSrv( pNewMsg );
		return false;
	}

	pPlayer->SetPlayerInfo();//update player info;
	if (MGFinalRebirthRst::FINAL_REBIRTH_SUC != pFinalRebirthRst->nErrCode) //进地图失败；
	{
		D_WARNING( "玩家%s最终地图复活失败，结果%d\n", pPlayer->GetAccount(), pFinalRebirthRst->nErrCode );
		pPlayer->ReqDestorySelf();//断开连接；
		return false;
	}

	//通知玩家本人在地图上出现成功，
	//消息转发给客户端;
	GCPlayerAppear  returnMsg;
	returnMsg.appearType = INFO_REBIRTH_APPEAR;
	returnMsg.lPlayerID = pPlayer->GetPlayerID();
	returnMsg.sequenceID = pFinalRebirthRst->sequenceID;
	returnMsg.playerInfo = pFinalRebirthRst->baseInfo;
	returnMsg.playerInfo.iPosX = pFinalRebirthRst->nPosX;		//机器人因为重新分配了位置,使用gatesrv的位置无效,所以要用mapsrv传来的新位置信息
	returnMsg.playerInfo.iPosY = pFinalRebirthRst->nPosY;
	SAFE_ARRSIZE( returnMsg.playerInfo.szAccount, returnMsg.playerInfo.accountSize );
	SAFE_ARRSIZE( returnMsg.playerInfo.szNickName, returnMsg.playerInfo.nameSize );

	returnMsg.playerSkills = pPlayer->GetFullPlayerInfo().skillInfo;

    NewSendPlayerPkg( GCPlayerAppear, pPlayer, returnMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerAppear, pPlayer, returnMsg );
	//pPlayer->SendPkgToPlayer( pNewMsg );//通知客户端其它玩家出现；

	return true;
	TRY_END;
	return false;
}

///玩家进地图结果；
bool CDealMapSrvPkg::OnEnterMapRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnEnterMapRst,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGEnterMapRst) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGEnterMapRst* pMGEnterMapRst = (const MGEnterMapRst*) pPkg;
	if ( pMGEnterMapRst->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		D_ERROR( "MapSrv发来非本GS管理玩家的进地图结果1\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMGEnterMapRst->lNotiPlayerID.dwPID );

	bool isCorrectPlayer = false;
	if ( NULL == pPlayer )
	{
		D_WARNING( "玩家(%d:%d)请求进入地图，但收到进入地图成功消息之前已下线\n", pMGEnterMapRst->lNotiPlayerID.wGID, pMGEnterMapRst->lNotiPlayerID.dwPID );
		isCorrectPlayer = false;
	} else {
		isCorrectPlayer = ( PS_DB_PRE == pPlayer->GetCurStat() ) ;
		if ( !isCorrectPlayer )
		{
			D_ERROR( "可能的严重错误，玩家%d:%s没有请求进入地图，但收到进入地图成功消息\n", pPlayer->GetPlayerDwPID(), pPlayer->GetAccount() );
		}
	}

	if ( !isCorrectPlayer ) 
	{
		D_ERROR( "收到玩家进地图成功消息时，玩家%d已下线，稍后可能会有相关的系列警告消息，应忽略之\n", pMGEnterMapRst->lNotiPlayerID.dwPID );
		//之前离线时已通知mapsrv，此处不需要再通知，by dzj, 09.10.10//通知mapsrv该玩家已下线；
		//GMPlayerLeave  mapMsg;
		//mapMsg.lPlayerID = pMGEnterMapRst->lNotiPlayerID;
		//mapMsg.lMapCode = 0;
		//mapMsg.nSpecialFlag = GMPlayerLeave::NOT_EXIST;//玩家已不存在于本gatesrv;
		//MsgToPut* pNewMsg = CreateSrvPkg( GMPlayerLeave, pOwner, mapMsg );
		//pOwner->SendPkgToSrv( pNewMsg );
		return false;
	}

	pPlayer->SetPlayerInfo();//update player info;
	if ( (MGEnterMapRst::ENTER_MAP_SUC != pMGEnterMapRst->nErrCode) //进地图失败；
		|| ( !pPlayer->SetCurStat( PS_MAPFIGHTING ) )
		)
	{
		D_WARNING( "置玩家%s进入地图失败，进地图结果%d，当前状态%d\n", pPlayer->GetAccount(), pMGEnterMapRst->nErrCode, pPlayer->GetCurStat() );
		pPlayer->ReqDestorySelf();//断开连接；
		return false;
	}

#ifdef DS_EPOLL
	if ( 0 != pPlayer->m_inittick )
	{
		D_DEBUG( "玩家帐号%s，登录费时%dtick\n", pPlayer->GetAccount(), GetTickCount()-pPlayer->m_inittick );
		pPlayer->m_inittick = 0;
	}
#endif //DS_EPOLL

	//通知玩家本人在地图上出现成功，
	//消息转发给客户端;
	GCPlayerAppear  returnMsg;
	returnMsg.sequenceID = pMGEnterMapRst->sequenceID;  //网络序列号
	returnMsg.appearType = INFO_MAP_ENTERED;
	returnMsg.lPlayerID = pPlayer->GetPlayerID();
	returnMsg.playerInfo = pPlayer->GetFullPlayerInfo().baseInfo;
	returnMsg.playerInfo.iPosX = pMGEnterMapRst->nPosX;		//机器人因为重新分配了位置,使用gatesrv的位置无效,所以要用mapsrv传来的新位置信息
	returnMsg.playerInfo.iPosY = pMGEnterMapRst->nPosY;
	SAFE_ARRSIZE( returnMsg.playerInfo.szAccount, returnMsg.playerInfo.accountSize );
	SAFE_ARRSIZE( returnMsg.playerInfo.szNickName, returnMsg.playerInfo.nameSize );

	returnMsg.playerSkills = pPlayer->GetFullPlayerInfo().skillInfo;

	NewSendPlayerPkg( GCPlayerAppear, pPlayer, returnMsg );

	CFunctionSrv::NotifyHonorToOnePlayer( pPlayer );//通知该玩家各称号的拥有者；

	//通知functionsrv该玩家上线；
	GFRankPlayerOnLine playerOnLine;
	playerOnLine.rankPlayerUID  = pPlayer->GetUID();
	playerOnLine.rankPlayerRace =  pPlayer->GetRace();
	playerOnLine.playerID = pPlayer->GetPlayerID();
	SafeStrCpy( playerOnLine.szPlayerName ,pPlayer->GetRoleName() );
	SendMsgToFunctionSrv( GFRankPlayerOnLine, playerOnLine );

	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerAppear, pPlayer, returnMsg );
	//pPlayer->SendPkgToPlayer( pNewMsg );//通知客户端其它玩家出现；

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnFullPlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnFullPlayerInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGFullPlayerInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGFullPlayerInfo* pMGFullPlayerInfo = (const MGFullPlayerInfo*) pPkg;
	if ( pMGFullPlayerInfo->playerID.wGID != SELF_SRV_ID )
	{
		D_ERROR( "MapSrv发来非本GS管理玩家的信息更新1\n" );
		return false;
	}

	CPlayer* pPlayer = CManPlayer::FindPlayer( pMGFullPlayerInfo->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		//玩家已下线;
		OfflineInfoProc( pOwner, pMGFullPlayerInfo );
		return true;
	}

	//获取玩家全局信息
	FullPlayerInfo& fullPlayerInfo = pPlayer->GetFullPlayerInfo(); 

	if ( pPlayer->GetRoleID() != fullPlayerInfo.baseInfo.uiID ) 
	{
		//当前PID对应玩家的帐号不同于更新消息中的玩家帐号，说明之前的玩家已下线；
		OfflineInfoProc( pOwner, pMGFullPlayerInfo );
		return true;
	}

	//以下处理欲更新的玩家还在线，且角色ID号一致，确实为目标玩家；
	if(!pMGFullPlayerInfo->bEnd)//更新玩家的full info;
	{
		pPlayer->SetFullInfoFalse();
		ReceiveFullStr<MGFullPlayerInfo, FullPlayerInfo>(pMGFullPlayerInfo, &fullPlayerInfo);
		if ( pMGFullPlayerInfo->infoType == INFO_SWITCH_UPDATE )
		{
			//跳地图时，不覆盖目标图信息；
			pPlayer->GetSwitchMapTgtInfo( fullPlayerInfo.baseInfo.usMapID, fullPlayerInfo.baseInfo.iPosX, fullPlayerInfo.baseInfo.iPosY );
		}
		return true;
	} else {		
		ReceiveFullStr<MGFullPlayerInfo, FullPlayerInfo>(pMGFullPlayerInfo, &fullPlayerInfo);
		if ( pMGFullPlayerInfo->infoType == INFO_SWITCH_UPDATE )
		{
			//跳地图时，不覆盖目标图信息；
			pPlayer->GetSwitchMapTgtInfo( fullPlayerInfo.baseInfo.usMapID, fullPlayerInfo.baseInfo.iPosX, fullPlayerInfo.baseInfo.iPosY );
		}
		pPlayer->SetFullInfoTrue();
	}

	pPlayer->SetPlayerInfo();//update player info;

	///非跳转地图时需要通知DBSrv，跳转地图时暂时不通知DB，因为此时玩家还在线，可以留待玩家真正下线时再通知DB;
	if ( INFO_SWITCH_UPDATE != pMGFullPlayerInfo->infoType )
	{
		CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( pPlayer->GetAccount() );
		if ( NULL == pDbSrv )
		{
			D_ERROR( "MapSrv发来玩家的信息更新时，找不到对应的DbSrv，严重错误！\n" );
			return false;
		}

		pPlayer->SaveFullInfoToDB( false );

	} else {
		//该fullinfo为跳地图时，离开的原地图发来的玩家信息；
		pPlayer->SwitchOrgMapSrvLeft();
	}

	return true;
	TRY_END
	return false;
}

///玩家非存盘信息更新；
bool CDealMapSrvPkg::OnPlayerTmpinfoUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerTmpinfoUpdate,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGTmpInfoUpdate) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGTmpInfoUpdate* tmpInfoUpdate = (const MGTmpInfoUpdate*)pPkg;
	if ( tmpInfoUpdate->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "收到MapServer玩家临时信息更新包，欲通知的对象:%x不在本ＧＳ上\n\n", tmpInfoUpdate->lNotiPlayerID.dwPID );
		return false;
	}

	//消息转发player;
	GCTmpInfoUpdate  climsg;
	climsg.playerID = tmpInfoUpdate->playerID;
	climsg.tmpInfo = tmpInfoUpdate->tmpInfo;

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( tmpInfoUpdate->lNotiPlayerID.dwPID );

	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnPlayerTmpinfoUpdate,玩家尚未进入战斗状态，不应收到此类消息5\n" );
			return false;
		}

		NewSendPlayerPkg( GCTmpInfoUpdate, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCTmpInfoUpdate, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
	} else {
	}

	return true;
	TRY_END;
	return false;
}

///玩家升级；
bool CDealMapSrvPkg::OnPlayerLevelUp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerLevelUp,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGPlayerLevelUp) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGPlayerLevelUp* levelupInfo = (const MGPlayerLevelUp*)pPkg;
	if ( levelupInfo->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "收到MapServer玩家升级包，欲通知的对象:%x不在本ＧＳ上\n\n", levelupInfo->lNotiPlayerID.dwPID );
		return false;
	}

	//消息转发player;
	GCPlayerLevelUp  climsg;
	climsg.playerID = levelupInfo->playerID;
	climsg.nNewLevel = levelupInfo->nNewLevel;
	climsg.levelUpType = (GCPlayerLevelUp::ItemType)levelupInfo->levelUpType;

	if ( pOwner->BroadcastSend< GCPlayerLevelUp >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( levelupInfo->lNotiPlayerID.dwPID );

	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnPlayerLevelUp,玩家尚未进入战斗状态，不应收到此类消息5\n" );
			return false;
		}

		NewSendPlayerPkg( GCPlayerLevelUp, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerLevelUp, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
	} else {
	}

	return true;
	TRY_END;
	return false;
}

///玩家角色信息更新；
bool CDealMapSrvPkg::OnRoleattUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnRoleattUpdate,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGRoleattUpdate) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGRoleattUpdate* roleattUpdate = (const MGRoleattUpdate*)pPkg;
	if ( roleattUpdate->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "收到MapServer玩家信息更新包，欲通知的对象:%x不在本ＧＳ上\n\n", roleattUpdate->lNotiPlayerID.dwPID );
		return false;
	}

	//消息转发player;
	GCRoleattUpdate  climsg;
	climsg.lChangedPlayerID = roleattUpdate->lChangedPlayerID;
	climsg.nType = roleattUpdate->nType;
	climsg.nOldValue = roleattUpdate->nOldValue;
	climsg.nNewValue = roleattUpdate->nNewValue;
	climsg.changeTime = roleattUpdate->changeTime;
	climsg.bFloat = roleattUpdate->bFloat;
	climsg.nAddType = roleattUpdate->nAddType;

	if ( pOwner->BroadcastSend< GCRoleattUpdate >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( roleattUpdate->lNotiPlayerID.dwPID );
	if ( NULL != pNotiPlayer )
	{
		//初始进入地图，响应玩家登录成功之前，会向客户端发送速度相关消息，if ( !pNotiPlayer->IsInFighting() )
		//{
		//	D_WARNING( "OnRoleattUpdate,玩家尚未进入战斗状态，不应收到此类消息5\n" );
		//	return false;
		//}

		NewSendPlayerPkg( GCRoleattUpdate, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCRoleattUpdate, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
	} else {
	}

	return true;
	TRY_END;
	return false;
}

///mapsrv发来的怪物信息;
bool CDealMapSrvPkg::OnMonsterInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnMonsterInfo,pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(MGMonsterInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//结构校验通过
	const MGMonsterInfo* monsterInfo = (const MGMonsterInfo*)pPkg;
	if ( monsterInfo->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		//欲通知的玩家不在本GS上；
		D_WARNING( "收到MapServer怪物信息包，欲通知的对象:%x不在本ＧＳ上\n\n", monsterInfo->lNotiPlayerID.dwPID );
		return false;
	}

	/*
		TYPE_ID                  	lMonsterID;                                 	//
		TYPE_ID                  	lMonsterType;                               	//怪物ID，地图内唯一
		TYPE_POS                 	nMapPosX;                                   	//怪物类型；
		TYPE_POS                 	nMapPosY;                                   	//怪物位置X
		FLOAT                    	fSpeed;                                     	//怪物移动速度,如果小于等于0则怪物静止
		FLOAT                    	fTargetX;                                   	//移动目标点X
		FLOAT                    	fTargetY;                                   	//移动目标点Y
		TYPE_UI32                	lMonsterHp;                                 	//怪物位置X
		PlayerID                 	rewardID;                                   	//所属掉落的玩家ID
	*/

	//位置调试，D_DEBUG( "通知怪物信息：位置(%d,%d), 速度(%f), 目标点(%f,%f)\n"
	//	, monsterInfo->gcmonsterInfo.nMapPosX, monsterInfo->gcmonsterInfo.nMapPosY, monsterInfo->gcmonsterInfo.fSpeed
	//	, monsterInfo->gcmonsterInfo.fTargetX, monsterInfo->gcmonsterInfo.fTargetY );

	////消息转发player;
	//GCMonsterInfo  climsg;
	//climsg.nMapPosX = monsterInfo->nMapPosX;
	//climsg.nMapPosY = monsterInfo->nMapPosY;
	//climsg.fSpeed   = monsterInfo->fSpeed;
	//climsg.fTargetX = monsterInfo->fTargetX;
	//climsg.fTargetY = monsterInfo->fTargetY;
	//climsg.lMonsterHp = monsterInfo->lMonsterHp;
	//climsg.lMonsterID = monsterInfo->lMonsterID;
	//climsg.lMonsterType = monsterInfo->lMonsterType;
	//climsg.rewardID = monsterInfo->rewardID;

	GCMonsterInfo *pMonsterInfo = const_cast<GCMonsterInfo *>( &monsterInfo->gcmonsterInfo );


	if ( pOwner->BroadcastSend< GCMonsterInfo >( pMonsterInfo) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( monsterInfo->lNotiPlayerID.dwPID );
	//unsigned short wCmd = MGMonsterInfo::wCmd;//用于日志；
	//unsigned long  lNotiPlayerID = (pNotiPlayer->GetPlayerID()).dwPID;//用于日志；
	if ( NULL != pNotiPlayer )
	{
		if ( !pNotiPlayer->IsInFighting() )
		{
			D_WARNING( "OnMonsterInfo,玩家尚未进入战斗状态，不应收到此类消息5\n" );
			return false;
		}

		NewSendPlayerPkg( GCMonsterInfo, pNotiPlayer, *pMonsterInfo );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCMonsterInfo, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );//通知客户端
		//D_WARNING( "收到mapsrv怪物消息%x，通知玩家:%x\n\n", wCmd, lNotiPlayerID );
	} else {
		//D_WARNING( "收到mapsrv消息%x，但找不到欲通知玩家%x\n\n", wCmd, lNotiPlayerID );
	}

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnItemPkgAppear(CMapSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	
	//D_DEBUG("道具包出现\n");

	if( NULL == pOwner)
	{
		D_ERROR( "OnItemPkgAppear,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGItemPackAppear) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}


	TRY_BEGIN;

	const MGItemPackAppear* pkgMsg = (const MGItemPackAppear *)pPkg;

	if( pkgMsg->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		D_WARNING("所通知的玩家不在GS服务器上\n");
		return false;
	}


	GCItemAppear climsg;
	climsg.ItemPackageID = pkgMsg->pkgID;
	climsg.PackageType = pkgMsg->type;
	climsg.x = pkgMsg->px;
	climsg.y = pkgMsg->py;
	climsg.dropOwnerID = pkgMsg->dropOwnerID;
	climsg.OwnerID = pkgMsg->OwnerID;
	climsg.itemIndex = pkgMsg->itemIndex;
	climsg.itemInfo = pkgMsg->itemInfo;

	if ( pOwner->BroadcastSend< GCItemAppear >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgMsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCItemAppear, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCItemPackageAppear, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );
		return true;
	}
	
	return true;
	TRY_END;
	return false;
}

//@查询道具包的信息
bool CDealMapSrvPkg::OnItemPkgInfo(CMapSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnItemPkgInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGItemPackInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	TRY_BEGIN;

	const MGItemPackInfo* pkgMsg =( const MGItemPackInfo* )pPkg;

	if( pkgMsg->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		D_WARNING("所通知的玩家不在GS服务器上\n");
		return false;
	}


	GCItemPackageInfo climsg;
	StructMemSet(climsg, 0x0, sizeof(GCItemPackageInfo) );
	climsg.itemCount = pkgMsg->itemCount;
	climsg.pkgIndex  = pkgMsg->pkgID;
	climsg.silverMoney = pkgMsg->silverMoney;
	climsg.goldMoney = pkgMsg->goldMoney;
	climsg.itemSize = pkgMsg->itemCount;
	StructMemCpy( climsg.ItemArr, pkgMsg->itemInfoArr, sizeof(ItemInfo_i)*MAXPACKAGEITEM );
	climsg.queryType = pkgMsg->queryType;   //??暂时1 修改
	
	if ( pOwner->BroadcastSend< GCItemPackageInfo >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgMsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCItemPackageInfo, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCItemPackageInfo, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}


//@brief:道具包消失
bool CDealMapSrvPkg::OnItemPkgDisAppear( CMapSrv* pOwner, const char* pPkg ,const unsigned short wPkgLen )
{
	//D_DEBUG("道具包消失\n");
	if( NULL == pOwner)
	{
		D_ERROR( "OnItemPkgDisAppear,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGItemPackDisAppear) )
	{
		return false;
	}

	TRY_BEGIN;

	const MGItemPackDisAppear* pkgMsg = (const MGItemPackDisAppear* )pPkg;

	if ( pkgMsg->lNotiPlayerID.wGID != SELF_SRV_ID )
	{
		return false;
	}

	GCItemsPkgDisappear climsg;
	climsg.ItemPackageID = pkgMsg->pkgID;

	if ( pOwner->BroadcastSend< GCItemsPkgDisappear >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgMsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCItemsPkgDisappear, pNotiPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCItemsPkgDisappear, pNotiPlayer, climsg );
		//pNotiPlayer->SendPkgToPlayer( pNewMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}

//bool CDealMapSrvPkg::OnItemPkgWeaponInfo(CMapSrv* pOwner, const char* pPkg ,const unsigned short wPkgLen )
//{
//	if ( wPkgLen != sizeof(MGWeaponItemProp) )
//	{
//		return false;
//	}
//
//	//D_DEBUG("返回武器信息\n");
//
//	TRY_BEGIN;
//
//	const MGWeaponItemProp* pkgMsg = ( const MGWeaponItemProp* )pPkg;
//
//	if ( pkgMsg->lNotiPlayerID.wGID != SELF_SRV_ID )
//	{
//		return false;
//	}
//
//	GCWeaponPackageInfo climsg;
//	climsg.ItemPackageID = pkgMsg->pkgID;
//	climsg.ItemTypeID = pkgMsg->itemID;
//	climsg.weaponInfo = pkgMsg->weaponInfo;
//
//	BroadCastSendCheck( GCWeaponPackageInfo, &climsg );  //群发检查
//
//	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgMsg->lNotiPlayerID.dwPID );
//	if ( pNotiPlayer )
//	{
//		MsgToPut* pMsg = CreatePlayerPkg(GCWeaponPackageInfo, pNotiPlayer, climsg);
//
//		pNotiPlayer->SendPkgToPlayer( pMsg );
//		return true;
//	}
//
//	TRY_END;
//
//	return false;
//}

//bool CDealMapSrvPkg::OnItemPkgEquipInfo(CMapSrv* pOwner, const char* pPkg ,const unsigned short wPkgLen )
//{
//	if ( wPkgLen != sizeof(MGEquipItemProp) )
//	{
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	const MGEquipItemProp* pkgMsg = ( const MGEquipItemProp* )pPkg;
//	if ( pkgMsg->lNotiPlayerID.wGID != SELF_SRV_ID )
//	{
//		return false;
//	}
//
//	GCEquipPackageInfo climsg;
//	climsg.ItemPackageID = pkgMsg->pkgID;
//	climsg.ItemTypeID = pkgMsg->itemID;
//	climsg.equipInfo = pkgMsg->equipInfo;
//
//	BroadCastSendCheck( GCEquipPackageInfo, &climsg );  //群发检查
//
//	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgMsg->lNotiPlayerID.dwPID );
//	if ( pNotiPlayer )
//	{
//		MsgToPut* pMsg = CreatePlayerPkg(GCEquipPackageInfo, pNotiPlayer, climsg);
//
//		pNotiPlayer->SendPkgToPlayer( pMsg );
//		return true;
//	}
//	TRY_END;
//	
//	return false;
//}

//bool CDealMapSrvPkg::OnPickItemSuccess(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	if( NULL == pOwner)
//	{
//		D_ERROR( "OnPickItemSuccess,pOwner空\n" );
//		return false;
//	}
//
//	if ( wPkgLen != sizeof(MGPickItemSuccess) )
//	{
//		return false;
//	}
//
//	//D_DEBUG("拾取道具成功!\n");
//
//	TRY_BEGIN;
//
//	ACE_UNUSED_ARG( pOwner );
//
//	const MGPickItemSuccess* pkgmsg = (MGPickItemSuccess *)pPkg;
//
//	GCPickItemSuccess clmsg;
//	clmsg.ItemInfo = pkgmsg->itemInfo;
//	clmsg.ItemPkgIndex = pkgmsg->itemIndex;
//
//	BroadCastSendCheck( GCPickItemSuccess, &clmsg );  //群发检查
//
//	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
//	if ( pNotiPlayer )
//	{
//		MsgToPut* pMsg = CreatePlayerPkg(GCPickItemSuccess, pNotiPlayer, clmsg);
//		pNotiPlayer->SendPkgToPlayer( pMsg );
//		return true;
//	}
//
//	return true;
//	TRY_END;
//	return false;
//}

bool CDealMapSrvPkg::OnPickItemFailed(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPickItemFailed,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPickItemFaild) )
	{
		return false;
	}

	//D_DEBUG("拾取道具失败!\n");

	TRY_BEGIN;

	ACE_UNUSED_ARG( pOwner );

	const MGPickItemFaild* pkgmsg = (MGPickItemFaild*)pPkg;

	GCPickItemFail climsg;
	climsg.result = (EPickItemResult)pkgmsg->result;
	climsg.itemTypeID = pkgmsg->itemID;

	if ( pOwner->BroadcastSend< GCPickItemFail >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCPickItemFail, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg(GCPickItemFail, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer( pMsg );
		return true;
	}
	
	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnPlayerUseItem(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerUseItem,pOwner空\n" );
		return false;
	}

	if ( wPkgLen!= sizeof(MGPlayerUseItem) )
	{
		return false;
	}

	//D_DEBUG("使用了道具\n");

	TRY_BEGIN;

	const MGPlayerUseItem* pUseItem = (const MGPlayerUseItem* )pPkg;

	GCUseItem climsg;
	climsg.useItem = pUseItem->litemInfo;
	climsg.UserID = pUseItem->lUsePlayerID;

	if ( pOwner->BroadcastSend< GCUseItem >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pUseItem->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCUseItem, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg(GCUseItem, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer(pMsg);
	}

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnPlayerUnUseItem(CMapSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerUnUseItem,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUnUseItem) )
	{
		return false;
	}

	//D_DEBUG("不使用了一个道具\n");

	const MGUnUseItem* pUnUse = ( const MGUnUseItem *)pPkg;

	GCUnUseItem climsg;
	climsg.equipIndex = pUnUse->equipIndex;
	climsg.playerID  = pUnUse->playerID;

	if ( pOwner->BroadcastSend< GCUnUseItem >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pUnUse->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCUnUseItem, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg(GCUnUseItem, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer(pMsg);
		return true;
	}

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnPlayerSetPkgItem(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerSetPkgItem,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGSetPkgItem) )
	{
		return false;
	}

	const MGSetPkgItem* pSetPkgItem = ( const MGSetPkgItem *)pPkg;

	GCSetPkgItem climsg;
	climsg.ItemInfo = pSetPkgItem->itemInfo;
	climsg.ItemPkgIndex = pSetPkgItem->pkgIndex;

	if ( pOwner->BroadcastSend< GCSetPkgItem >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}


	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pSetPkgItem->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCSetPkgItem, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg(GCSetPkgItem, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer(pMsg);
		return true;
	}

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnPlayerUseItemFailed(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerUseItemFailed,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUseItemResult) )
	{
		return true;
	}

	const MGUseItemResult* pUseItemResult = ( const MGUseItemResult *)pPkg;

	GCUseItemResult climsg;
	climsg.result = pUseItemResult->result;

	if ( pOwner->BroadcastSend< GCUseItemResult >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pUseItemResult->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCUseItemResult, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg( GCUseItemResult, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer(pMsg);
		return true;
	}

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnOnePlayerPickItem(CMapSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnOnePlayerPickItem,pOwner空\n" );
		return false;
	}

	if (  wPkgLen!= sizeof(MGOnePlayerPickItem) )
	{
		return false;
	}

// 	D_DEBUG("默认拾取了一个道具\n");

	TRY_BEGIN;

	const MGOnePlayerPickItem* pkgmsg = (MGOnePlayerPickItem *)pPkg;

	GCPlayerPickItem climsg;
	climsg.itemTypeID = pkgmsg->itemID;
	climsg.pkgIndex   = pkgmsg->pkgIndex;
	climsg.IsPickMoney = pkgmsg->bIsPickMoney;

	if ( pOwner->BroadcastSend< GCPlayerPickItem >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
        NewSendPlayerPkg( GCPlayerPickItem, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg(GCPlayerPickItem, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer( pMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}

//bool CDealMapSrvPkg::OnReturnItemDetialInfo(CMapSrv *pOwner, const char *pPkg, const unsigned short &wPkgLen)
//{
//	if( NULL == pOwner)
//	{
//		D_ERROR( "OnReturnItemDetialInfo,pOwner空\n" );
//		return false;
//	}
//
//	if ( wPkgLen != sizeof(MGItemDetailInfo) )
//	{
//		return false;
//	}
//
//	//D_DEBUG("返回了道具的详细信息\n");
//
//	TRY_BEGIN;
//
//	const MGItemDetailInfo* pkgmsg = ( const MGItemDetailInfo *)pPkg;
//
//	GCItemDetialInfo climsg;
//	climsg.itemDetialInfo = pkgmsg->itemDetialInfo;
//
//	BroadCastSendCheck( GCItemDetialInfo, &climsg );  //群发检查
//
//	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
//	if ( pNotiPlayer )
//	{
//		MsgToPut* pMsg = CreatePlayerPkg(GCItemDetialInfo, pNotiPlayer, climsg);
//		pNotiPlayer->SendPkgToPlayer( pMsg );
//		return true;
//	}
//
//	return true;
//	TRY_END;
//	return false;
//}

bool CDealMapSrvPkg::OnReturnDropItemResult(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnReturnDropItemResult,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGDropItemResult) )
	{
		return false;
	}

	TRY_BEGIN;

	const MGDropItemResult* pkgmsg = ( const MGDropItemResult *)pPkg;

	//D_DEBUG("返回了玩家丢弃包裹的结果 result:%d\n", pkgmsg->result );

	GCDropItemResult climsg;
	climsg.result = pkgmsg->result;
	climsg.itemUID = pkgmsg->uiItemID;


	if ( pOwner->BroadcastSend< GCDropItemResult >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
        NewSendPlayerPkg( GCDropItemResult, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg(GCDropItemResult, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer( pMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnReturnItemPkgConciseInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnReturnItemPkgConciseInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGItemPkgConciseInfo) )
	{
		return false;
	}

	TRY_BEGIN;

	const MGItemPkgConciseInfo* pkgmsg = ( const MGItemPkgConciseInfo *)pPkg;

	//D_DEBUG("返回了玩家包裹的基本信息 第%d页\n", pkgmsg->pageIndex );

	GCItemPkgConciseInfo climsg;
	//climsg.pageIndex = pkgmsg->pageIndex;
	climsg.pkgInfoSize =PKG_PAGE_SIZE;
	StructMemCpy( climsg.pkgInfo, pkgmsg->pkgInfo, sizeof(ItemPosition)*PKG_PAGE_SIZE);

	if ( pOwner->BroadcastSend< GCItemPkgConciseInfo >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}


	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCItemPkgConciseInfo, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg(GCItemPkgConciseInfo, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer( pMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}

//bool CDealMapSrvPkg::OnReturnEquipConciseInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	if( NULL == pOwner)
//	{
//		D_ERROR( "OnReturnEquipConciseInfo,pOwner空\n" );
//		return false;
//	}
//
//	if ( wPkgLen != sizeof(MGEquipConciseInfo) )
//	{
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	const MGEquipConciseInfo* pkgmsg = ( const MGEquipConciseInfo *)pPkg;
//
//	GCEquipConciseInfo climsg;
//	ACE_OS::memcpy( climsg.equipInfo.equips,pkgmsg->equipInfo.equips, sizeof(ItemInfo_i)*EQUIP_SIZE );
//	SAFE_ARRSIZE(climsg.equipInfo.equips, climsg.equipInfo.equipSize);
//
//	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
//	if ( pNotiPlayer )
//	{
//		MsgToPut* pMsg = CreatePlayerPkg(GCEquipConciseInfo, pNotiPlayer, climsg);
//		pNotiPlayer->SendPkgToPlayer( pMsg );
//		return true;
//	}
//
//	return true;
//	TRY_END;
//	return false;
//}


bool CDealMapSrvPkg::OnPlayerBuffStart( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerBuffStart,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerBufferStart) )
	{
		return false;
	}

	TRY_BEGIN;

	MGPlayerBufferStart* pkgmsg = ( MGPlayerBufferStart *)pPkg;

	
	if ( pOwner->BroadcastSend< GCPlayerBuffStart >( &pkgmsg->buffStart ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCPlayerBuffStart, pNotiPlayer, pkgmsg->buffStart );
		//MsgToPut* pMsg = CreatePlayerPkg(GCPlayerBuffStart, pNotiPlayer, pkgmsg->buffStart);
		//pNotiPlayer->SendPkgToPlayer( pMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnPlayerBuffEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerBuffEnd,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerBuffEnd) )
	{
		return false;
	}

	TRY_BEGIN;

	const MGPlayerBuffEnd* pkgmsg = ( const MGPlayerBuffEnd *)pPkg;

	GCPlayerBuffEnd climsg;
	climsg.buffID = pkgmsg->buffID;
	climsg.changePlayerID = pkgmsg->changePlayer;
	climsg.nBuffIndex = pkgmsg->nBuffIndex;

	if ( pOwner->BroadcastSend< GCPlayerBuffEnd >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCPlayerBuffEnd, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg( GCPlayerBuffEnd, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer( pMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnPlayerBuffUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnPlayerBuffUpdate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerBuffUpdate) )
	{
		return false;
	}

	TRY_BEGIN;

	const MGPlayerBuffUpdate* pkgmsg = ( const MGPlayerBuffUpdate *)pPkg;

	GCPlayerBuffUpdate climsg;
	climsg.changePlayerID = pkgmsg->lChangeID;
	climsg.nBuffIndex = pkgmsg->nBuffIndex;
	climsg.nChangeHP = pkgmsg->nChangeHP;
	climsg.nPlayerCurrentHP = pkgmsg->nPlayerCurrentHP;
	climsg.buffID = pkgmsg->buffID;
	climsg.createBuffPlayerID = pkgmsg->createBuffPlayer;
	climsg.battleFlag = pkgmsg->battleFlag;

	if ( pOwner->BroadcastSend< GCPlayerBuffUpdate >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCPlayerBuffUpdate, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg( GCPlayerBuffUpdate, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer( pMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}



bool CDealMapSrvPkg::OnMonsterBuffStart( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnMonsterBuffStart,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGMonsterBuffStart) )
	{
		return false;
	}

	TRY_BEGIN;

	MGMonsterBuffStart* pkgmsg = ( MGMonsterBuffStart *)pPkg;

	if ( pOwner->BroadcastSend< GCMonsterBuffStart >( &pkgmsg->buffStart ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCMonsterBuffStart, pNotiPlayer, pkgmsg->buffStart );
		//MsgToPut* pMsg = CreatePlayerPkg( GCMonsterBuffStart, pNotiPlayer, pkgmsg->buffStart );
		//pNotiPlayer->SendPkgToPlayer( pMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnMonsterBuffEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnMonsterBuffEnd,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGMonsterBuffEnd) )
	{
		return false;
	}

	TRY_BEGIN;

	MGMonsterBuffEnd* pSrvMsg = (MGMonsterBuffEnd*)pPkg;

	GCMonsterBuffEnd climsg;
	climsg.buffID = pSrvMsg->buffID;
	climsg.monsterID = pSrvMsg->monsterID;
	climsg.nBuffIndex = pSrvMsg->nBuffIndex;

	if ( pOwner->BroadcastSend< GCMonsterBuffEnd >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnMonsterBuffEnd，找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCMonsterBuffEnd, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCMonsterBuffEnd, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnMonsterBuffUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnMonsterBuffUpdate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGMonsterBuffUpdate) )
	{
		return false;
	}

	TRY_BEGIN;

	ACE_UNUSED_ARG( pOwner );

	const MGMonsterBuffUpdate* pkgmsg = ( const MGMonsterBuffUpdate *)pPkg;

	GCMonsterBuffUpdate climsg;
	climsg.monsterID = pkgmsg->monsterID;
	climsg.nBuffIndex = pkgmsg->nBuffIndex;
	climsg.buffID = pkgmsg->buffID;
	climsg.nChangeHP = pkgmsg->nChangeHP;
	climsg.nMonsterCurrentHP = pkgmsg->nMonsterCurrentHP;

	if ( pOwner->BroadcastSend< GCMonsterBuffUpdate >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotiPlayer = CManPlayer::FindPlayer( pkgmsg->lNotiPlayerID.dwPID );
	if ( pNotiPlayer )
	{
		NewSendPlayerPkg( GCMonsterBuffUpdate, pNotiPlayer, climsg );
		//MsgToPut* pMsg = CreatePlayerPkg( GCMonsterBuffUpdate, pNotiPlayer, climsg);
		//pNotiPlayer->SendPkgToPlayer( pMsg );
		return true;
	}

	return true;
	TRY_END;
	return false;
}

//bool CDealMapSrvPkg::OnPlayerMoneyUpdate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	if( NULL == pOwner)
//	{
//		D_ERROR( "OnPlayerMoneyUpdate,pOwner空\n" );
//		return false;
//	}
//	
//	TRY_BEGIN;
//
//	PKGSIZECHECK( MGMoneyUpdate );
//
//	const MGMoneyUpdate* srvmsg = ( const MGMoneyUpdate *)pPkg;
//
//	GCMoneyUpdate climsg;
//	climsg.money = srvmsg->money;
//
//	if ( pOwner->BroadcastSend< GCMoneyUpdate >( &climsg ) )
//	{
//		//已群发，不再单独发送；
//		return true;
//	}
//
//	//BroadCastSendCheck( GCMoneyUpdate, &climsg );  //群发检查
//	GateSrvSendMsgToPlayer( GCMoneyUpdate );
//
//	return true;
//	TRY_END;
//	return false;
//}

//骑乘队伍出现时
bool CDealMapSrvPkg::OnReturnRideTeamAppear(CMapSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnReturnRideTeamAppear,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;
	
	PKGSIZECHECK( MGMountTeamInfo );

	const MGMountTeamInfo* srvmsg = ( const MGMountTeamInfo *)pPkg;

	GCMountTeamInfo climsg;
	climsg.MountTeam[0] = srvmsg->MountTeam[0];
	climsg.MountTeam[1] = srvmsg->MountTeam[1];
	climsg.mountSize = 2;
	climsg.rideItem = srvmsg->rideItem;
	climsg.x = srvmsg->x;
	climsg.y = srvmsg->y;
	climsg.mountType = srvmsg->mountType;

	if ( pOwner->BroadcastSend< GCMountTeamInfo >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	GateSrvSendMsgToPlayer( GCMountTeamInfo );
	
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRideTeamDissolve(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRideTeamDissolve,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGMountTeamDisBand );

	const MGMountTeamDisBand* srvmsg = ( const MGMountTeamDisBand *)pPkg;

	GCMountTeamDisBand climsg;
	climsg.mountTeamID = srvmsg->mountTeamID;

	if ( pOwner->BroadcastSend< GCMountTeamDisBand >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	GateSrvSendMsgToPlayer( GCMountTeamDisBand );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRideStateChange(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRideStateChange,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGMountStateChange );

	const MGMountStateChange* srvmsg = ( const MGMountStateChange *)pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( srvmsg->lNotiPlayerID.dwPID );
	if ( pPlayer )
	{
		pPlayer->SetRideState( srvmsg->rideState ,srvmsg->attachInfo1, srvmsg->attachInfo2 );
		return true;
	}

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRideLeaderSwitchMap(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRideLeaderSwitchMap,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGOnRideLeaderSwichMap );

	const MGOnRideLeaderSwichMap* srvmsg = ( const MGOnRideLeaderSwichMap *)pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( srvmsg->lNotiPlayerID.dwPID );
	if ( pPlayer )
	{
		pPlayer->OnRideLeaderSwitchMap( srvmsg );
	}

	return true;
	TRY_END;
	return false;
}


//返回邀请骑乘的结果
bool CDealMapSrvPkg::OnReturnInviteRideTeamResult(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnReturnInviteRideTeamResult,pOwner空\n" );
		return false;
	}

	PKGSIZECHECK( MGInviteResult );

	TRY_BEGIN;

	const MGInviteResult* srvmsg = ( const MGInviteResult *)pPkg;

	GCInviteResult climsg;
	climsg.isAgree = srvmsg->isAgree;
	climsg.targetID = srvmsg->targetID;

	D_DEBUG("发送是否骑乘是否同意的请求:%d\n",climsg.isAgree );

	if ( pOwner->BroadcastSend< GCInviteResult >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	GateSrvSendMsgToPlayer( GCInviteResult );

	return true;
	TRY_END;
	return false;
}

//被人邀请加入骑乘队伍
bool CDealMapSrvPkg::OnBeInviteJoinRideTeam(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnBeInviteJoinRideTeam,pOwner空\n" );
		return false;
	}

	PKGSIZECHECK( MGBeInviteMount );

	TRY_BEGIN;

	const MGBeInviteMount* srvmsg = ( const  MGBeInviteMount *)pPkg;

	GCBeInviteMount climsg;
	climsg.launchID = srvmsg->launchID;

	if ( pOwner->BroadcastSend< GCBeInviteMount >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	GateSrvSendMsgToPlayer( GCBeInviteMount );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnReqBuffEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnReqBuffEnd,pOwner空\n" );
		return false;
	}
	
	PKGSIZECHECK( MGReqBuffEnd );

	TRY_BEGIN;

	const MGReqBuffEnd* srvmsg = ( const  MGReqBuffEnd *)pPkg;

	GCReqBuffEnd climsg;
	climsg.nErrorCode = srvmsg->nErrorCode;

	if ( pOwner->BroadcastSend< GCReqBuffEnd >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	GateSrvSendMsgToPlayer( GCReqBuffEnd );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnBroadcast( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnBroadcast,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	//特殊包，根据内容发送数组，不能直接进行等长比较, by dzj, 09.03.19；
	if ( wPkgLen > sizeof(MGBroadCast) )
	{
		D_DEBUG("SrvErr:MGBroadCast消息包大小错误\n" );
		return false;
	}

	/*
	int nCurNum;	//此次有效的人数
	unsigned long arrPlayerPID[MAX_NOTIPLAYER_NUM_PERGATE];
	*/
	int tgtNum = 0;
	StructMemCpy( tgtNum, pPkg, sizeof(int) );
	
	if( NULL == pOwner)
	{
		D_ERROR("MapSrv指针为空\n");
		return false;
	}

	pOwner->SetBroadCastTgt( tgtNum, pPkg+sizeof(int), wPkgLen-sizeof(int) );

	return true;
	TRY_END;
	return false;
}

///设置玩家新位置(地图内跳转，只通知跳转者本人);
bool CDealMapSrvPkg::OnSetPlayerPos( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnSetPlayerPos,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGSetPlayerPos );
	MGSetPlayerPos* srvmsg = (MGSetPlayerPos*)pPkg;

	//转发客户端；
	CPlayer* pPlayer = CManPlayer::FindPlayer( srvmsg->lNotiPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		return false;
	}

	pPlayer->SetCurPos( srvmsg->setpos.posX, srvmsg->setpos.posY );

	
	NewSendPlayerPkg( GCSetPlayerPos, pPlayer, srvmsg->setpos );
 //   MsgToPut* pNewMsg = CreatePlayerPkg( GCSetPlayerPos, pPlayer, setPlayerPos );
	//pPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnMGTeamMemberDetailInfoUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnMGTeamMemberDetailInfoUpdate,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGTeamMemberDetailInfoUpdate) )
	{
		return false;
	}

	MGTeamMemberDetailInfoUpdate* pMapSrvMsg = (MGTeamMemberDetailInfoUpdate*)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pMapSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnMGTeamMemberDetailInfoUpdate,找不到通知的玩家\n");
		return false;
	}

	SafeStruct( GCTeamMemberDetailInfoUpdate,  climsg );
	climsg.mapID = pMapSrvMsg->mapID;
	climsg.nPosX = pMapSrvMsg->nPosX;
	climsg.nPosY = pMapSrvMsg->nPosY;
	climsg.uiHP = pMapSrvMsg->uiHP;
	climsg.uiMP = pMapSrvMsg->uiMP;
	climsg.buffData = pMapSrvMsg->buffData;
	climsg.maxHP = pMapSrvMsg->maxHP;	//最大HP
	climsg.maxMP = pMapSrvMsg->maxMP;	//最大MP
	climsg.teamMemberID = pMapSrvMsg->teamMemberID;
	climsg.usPlayerLevel = pMapSrvMsg->usLevel;
	climsg.goodEvilPoint = pMapSrvMsg->goodevilPoint;

	NewSendPlayerPkg( GCTeamMemberDetailInfoUpdate, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCTeamMemberDetailInfoUpdate, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnMGRollItemResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnMGRollItemResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGRollItemResult) )
	{
		return false;
	}

	MGRollItemResult* pSrvMsg = (MGRollItemResult*)pPkg;

	if( pSrvMsg->lNotiPlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("GATEid 错误\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnMGRollItemResult,找不到通知的玩家\n");
		return false;
	}

	
	SafeStruct( GCRollItemResult, climsg );

	unsigned namelen = (unsigned)strlen( pSrvMsg->rollPlayerName ) + 1;
	climsg.NameSize = namelen > MAX_NAME_SIZE? MAX_NAME_SIZE:namelen;
	SafeStrCpy( climsg.rollPlayerName,pSrvMsg->rollPlayerName );
	climsg.rollID = pSrvMsg->rollID;
	climsg.rollItemID = pSrvMsg->rollItemID;

	NewSendPlayerPkg( GCRollItemResult, pNotifyPlayer, climsg );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnMGReqAgreeRollItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnMGReqAgreeRollItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGReqAgreeRollItem) )
	{
		return false;
	}
	
	MGReqAgreeRollItem* pSrvMsg = (MGReqAgreeRollItem*)pPkg;
	if( pSrvMsg->lNotiPlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("GATEid 错误\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnMGReqAgreeRollItem,找不到通知的玩家\n");
		return false;
	}

	SafeStruct( GCReqAgreeRollItem, climsg );
	climsg.rollID = pSrvMsg->rollID;
	climsg.rollItemID = pSrvMsg->rollItemID;

	NewSendPlayerPkg( GCReqAgreeRollItem, pNotifyPlayer, climsg );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnReturnRollNumResult(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnReturnRollNumResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(MGRollItemNum) )
	{
		return false;
	}

	MGRollItemNum* pSrvMsg = ( MGRollItemNum *)pPkg;
	if( pSrvMsg->lNotiPlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("GATEid 错误\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnReturnRollNumResult,找不到通知的玩家\n");
		return false;
	}

	GCRollItemNum rollItemNum;
	rollItemNum.rollNum = pSrvMsg->num;
	unsigned int namelen = (unsigned)strlen( pSrvMsg->rollPlayerName ) +1;
	rollItemNum.NameSize = namelen > MAX_NAME_SIZE ? MAX_NAME_SIZE:namelen;
	SafeStrCpy( rollItemNum.rollPlayerName,pSrvMsg->rollPlayerName );
	rollItemNum.rollID  = pSrvMsg->rollID;

	NewSendPlayerPkg( GCRollItemNum, pNotifyPlayer, rollItemNum );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRecvTeamMemberPickItem(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRecvTeamMemberPickItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(MGTeamMemberPickItem ) )
	{
		return false;
	}

	MGTeamMemberPickItem* pSrvMsg = ( MGTeamMemberPickItem *)pPkg;
	if( pSrvMsg->lNotiPlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("GATEid 错误\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvTeamMemberPickItem,找不到通知的玩家\n");
		return false;
	}

	GCTeamMemberPickItem pickItemMsg;
	pickItemMsg.getPlayerID = pSrvMsg->pickPlayerID;
	pickItemMsg.rollItemTypeID = pSrvMsg->itemTypeID;

	NewSendPlayerPkg( GCTeamMemberPickItem, pNotifyPlayer, pickItemMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnNoticeTeamPlayerGetNewItem(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnNoticeTeamPlayerGetNewItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGNoticeTeamMemberGetNewItem) )
		return false;

	MGNoticeTeamMemberGetNewItem* pSrvMsg = ( MGNoticeTeamMemberGetNewItem *)pPkg;

	GMTeamMemberGetNewItem srvmg;
	srvmg.TeamID = pSrvMsg->teamID;
	srvmg.itemTypeID = pSrvMsg->itemTypeID;
	srvmg.pickPlayerID = pSrvMsg->pickPlayerID;

	//通知所有的mapsrv
	CManMapSrvs::SendBroadcastMsg<GMTeamMemberGetNewItem>( &srvmg );
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnSendTradeReqError( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnSendTradeReqError,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGSendTradeReqError) )
		return false;

	MGSendTradeReqError *pSendTradeReqError = (MGSendTradeReqError *)pPkg;

	CPlayer * pNotifyPlayer  = CManPlayer::FindPlayer( pSendTradeReqError->lNotiPlayerID.dwPID);
	if(NULL == pNotifyPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCSendTradeReqError, pNotifyPlayer, pSendTradeReqError->errInfo );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRecvTradeReq( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnRecvTradeReq,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGRecvTradeReq) )
		return false;

	MGRecvTradeReq * pRecvTradeReq = (MGRecvTradeReq * )pPkg;
	GCRecvTradeReq msg;
	msg.sendReqPlayerID = pRecvTradeReq->sendPlayerId;
	msg.nameSize = sizeof(msg.sendPlayerName);
	SafeStrCpy( msg.sendPlayerName, pRecvTradeReq->sendPlayerName );//, sizeof(msg.sendPlayerName));

	// 接受请求的玩家
	CPlayer * pPlayer = CManPlayer::FindPlayer( pRecvTradeReq->lNotiPlayerID.dwPID);
	if(NULL == pPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCRecvTradeReq, pPlayer, msg );
	//MsgToPut *pGCRecvTradeReq = CreatePlayerPkg(GCRecvTradeReq, pPlayer, msg);
	//pPlayer->SendPkgToPlayer(pGCRecvTradeReq);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnTradeReqResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnTradeReqResult,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGSendTradeReqResult) )
		return false;

	MGSendTradeReqResult *pSendTradeReqResult = (MGSendTradeReqResult *)pPkg;
	GCSendTradeReqResult msg;
	msg.tResult = (GCSendTradeReqResult::eTradeReqResult)(pSendTradeReqResult->tResult);

	// 要通知的玩家
	CPlayer * pNotifyPlayer = CManPlayer::FindPlayer( pSendTradeReqResult->lNotiPlayerID.dwPID);
	if(NULL == pNotifyPlayer)
	{
		D_DEBUG("pNotifyPlayer\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCSendTradeReqResult, pNotifyPlayer, msg );
	//MsgToPut *pGCSendTradeReqResult = CreatePlayerPkg(GCSendTradeReqResult, pNotifyPlayer, msg);
	//pNotifyPlayer->SendPkgToPlayer(pGCSendTradeReqResult);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnAddOrDeleteItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddOrDeleteItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGAddOrDeleteItem) )
		return false;

	MGAddOrDeleteItem *pAddOrDeleteItem = (MGAddOrDeleteItem *)pPkg;

	GCAddOrDeleteItem msg;
	msg.tOperateType = (GCAddOrDeleteItem::eOperateType)(pAddOrDeleteItem->tOperateType);
	msg.tOperateResult =(GCAddOrDeleteItem::eOperateResult)(pAddOrDeleteItem->tResult);
	msg.uiID = pAddOrDeleteItem->uiItemID;
	msg.uiItemTypeID = pAddOrDeleteItem->uiItemTypeID;
	msg.usWearPoint = pAddOrDeleteItem->usWearPoint;
	msg.ucCount = pAddOrDeleteItem->ucCount;   
	msg.operatorID = pAddOrDeleteItem->operatePlayerID;
	msg.usAddId = pAddOrDeleteItem->usAddId;
	msg.ucLevelUp = pAddOrDeleteItem->ucLevelUp;
	
	//要通知的玩家
	CPlayer * pNotifyPlayer = CManPlayer::FindPlayer( pAddOrDeleteItem->lNotiPlayerID.dwPID);
	if(NULL == pNotifyPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCAddOrDeleteItem, pNotifyPlayer, msg );
	//MsgToPut *pGCAddOrDeleteItem = CreatePlayerPkg(GCAddOrDeleteItem, pNotifyPlayer, msg);
	//pNotifyPlayer->SendPkgToPlayer(pGCAddOrDeleteItem);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnSetTradeMoney( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnSetTradeMoney,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGSetTradeMoney) )
		return false;

	MGSetTradeMoney *pSetTradeMoney = (MGSetTradeMoney *)pPkg;

	GCSetTradeMoney msg;
	msg.operatorID = pSetTradeMoney->operatePlayerID;
	msg.uiGoldMoney = pSetTradeMoney->uiGoldMoney;
	msg.bOK = pSetTradeMoney->bOK;
	
	//要通知的玩家
	CPlayer * pNotifyPlayer = CManPlayer::FindPlayer( pSetTradeMoney->lNotiPlayerID.dwPID);
	if(NULL == pNotifyPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCSetTradeMoney, pNotifyPlayer, msg );
	//MsgToPut *pGCSetTradeMoney = CreatePlayerPkg(GCSetTradeMoney, pNotifyPlayer, msg);
	//pNotifyPlayer->SendPkgToPlayer(pGCSetTradeMoney);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnCancelTrade( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnCancelTrade,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGCancelTrade) )
		return false;

	MGCancelTrade * pCancelTrade = (MGCancelTrade *)pPkg;
	GCCancelTrade msg;
	
	// 要通知的玩家
	CPlayer * pNotifyPlayer = CManPlayer::FindPlayer( pCancelTrade->lNotiPlayerID.dwPID);
	if(NULL == pNotifyPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCCancelTrade, pNotifyPlayer, msg );
	//MsgToPut *pGCCancelTrade = CreatePlayerPkg(GCCancelTrade, pNotifyPlayer, msg);
	//pNotifyPlayer->SendPkgToPlayer(pGCCancelTrade);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnLockTrade( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnLockTrade,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGLockTrade) )
		return false;

	MGLockTrade *pLockTrade = (MGLockTrade *)pPkg;
	GCLockTrade msg;
	msg.operatePlayerID = pLockTrade->lOperatePlayerID;
	if(pLockTrade->bLock)
		msg.tType = GCLockTrade::LOCK;
	else
		msg.tType = GCLockTrade::UNLOCK;
	
	// 要通知的玩家
	CPlayer * pNotifyPlayer = CManPlayer::FindPlayer( pLockTrade->lNotiPlayerID.dwPID);
	if(NULL == pNotifyPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCLockTrade, pNotifyPlayer, msg );
	//MsgToPut *pGCLockTrade = CreatePlayerPkg(GCLockTrade, pNotifyPlayer, msg);
	//pNotifyPlayer->SendPkgToPlayer(pGCLockTrade);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnConfirmTrade( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnConfirmTrade,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGConfirmTrade) )
		return false;

	MGConfirmTrade *pConfirmTrade  = (MGConfirmTrade *)pPkg;
	GCConfirmTrade msg;
	msg.operatePlayerID = pConfirmTrade->lOperatePlayerID;
	msg.tResult = (GCConfirmTrade::eTradeResult)(pConfirmTrade->tResult);
	
	// 要通知的玩家
	CPlayer * pNotifyPlayer = CManPlayer::FindPlayer( pConfirmTrade->lNotiPlayerID.dwPID);
	if(NULL == pNotifyPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCConfirmTrade, pNotifyPlayer, msg );
	//MsgToPut *pGCConfirmTrade = CreatePlayerPkg(GCConfirmTrade, pNotifyPlayer, msg);
	//pNotifyPlayer->SendPkgToPlayer(pGCConfirmTrade);

	return true;
	TRY_END;
	return false;
}

///指示客户端显示托盘；MGShowPlate
bool CDealMapSrvPkg::OnShowPlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnShowPlate,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGShowPlate );
	const MGShowPlate* srvmsg = (const MGShowPlate*)pPkg;
	if( NULL == pOwner)
	{
		D_ERROR("OnShowPlate,MapSrv指针为空\n");
		return false;
	}

	GCShowPlate climsg;
	climsg.plateType = srvmsg->plateType;

	GateSrvSendMsgToPlayer( GCShowPlate );

	return true;
	TRY_END;
	return false;
}

///往托盘放置物品结果；MGAddSthToPlateRst
bool CDealMapSrvPkg::OnAddSthToPlateRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddSthToPlateRst,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGAddSthToPlateRst );
	const MGAddSthToPlateRst* srvmsg = (const MGAddSthToPlateRst*)pPkg;
	if( NULL == pOwner)
	{
		D_ERROR("OnAddSthToPlateRst,MapSrv指针为空\n");
		return false;
	}

	GCAddSthToPlateRst climsg;
	climsg.platePos = srvmsg->platePos;
	climsg.sthID    = srvmsg->sthID;
	climsg.isConFulfil = srvmsg->isFulfilCon;
	climsg.addRst = srvmsg->addRst;

	GateSrvSendMsgToPlayer( GCAddSthToPlateRst );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnAddFoeInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnAddFoeInfo,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if(wPkgLen != sizeof( MGAddFoeInfo ) )
	{
		D_ERROR("包长不对\n");
		return false;
	}

	MGAddFoeInfo* pSrvMsg = (MGAddFoeInfo*)pPkg;

	CPlayer* pFoePlayer = CManPlayer::FindPlayer( pSrvMsg->foePlayerID.dwPID );
	if( NULL == pFoePlayer )
	{
		return false;
	}

	SafeStruct( GRAddEnemy, relmsg);
	relmsg.player.PPlayer   = pSrvMsg->foePlayerID.dwPID;
	relmsg.player.GateSrvID = pSrvMsg->foePlayerID.wGID;
	relmsg.uiID = pFoePlayer->GetFullPlayerInfo().baseInfo.uiID;
	relmsg.MapID = pFoePlayer->GetMapID();
	relmsg.Sex = pFoePlayer->GetSex();
	relmsg.playerIcon = pFoePlayer->GetPortraitID();
	relmsg.playerRace = pFoePlayer->GetRace();
	relmsg.playerClass = (CHAR)pFoePlayer->GetClass();
	relmsg.playerLevel = pFoePlayer->GetLevel();
	SafeStrCpy( relmsg.Name, pFoePlayer->GetRoleName() );
	relmsg.NameLen = (UINT)strlen( relmsg.Name ) + 1;
	relmsg.addtouiID = pSrvMsg->playerUID;
	relmsg.posX = pSrvMsg->incidentPosX;
	relmsg.posY = pSrvMsg->incidentPosY;
	SendMsgToRelationSrv( GRAddEnemy, relmsg );

#ifdef _DEBUG
	D_DEBUG("向relationsrv发送增加仇人消息\n");
#endif
	
	return true;
	TRY_END;
	return false;
}


///从托盘取物品结果；MGTakeSthFromPlateRst
bool CDealMapSrvPkg::OnTakeSthFromPlateRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR("OnTakeSthFromPlateRst,MapSrv指针为空\n");
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGTakeSthFromPlateRst );
	const MGTakeSthFromPlateRst* srvmsg = (const MGTakeSthFromPlateRst*)pPkg;

	GCTakeSthFromPlateRst climsg;
	climsg.platePos = srvmsg->platePos;
	climsg.sthID    = srvmsg->sthID;
	climsg.isConFulfil = srvmsg->isFulfilCon;
	climsg.takeRst   = srvmsg->takeRst;

	GateSrvSendMsgToPlayer( GCTakeSthFromPlateRst );

	return true;
	TRY_END;
	return false;
}

///托盘执行结果；MGPlateExecRst
bool CDealMapSrvPkg::OnPlateExecRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR("OnPlateExecRst,MapSrv指针为空\n");
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGPlateExecRst );
	const MGPlateExecRst* srvmsg = (const MGPlateExecRst*)pPkg;

	GCPlateExecRst climsg;
	climsg.execRst = srvmsg->execRst;

	GateSrvSendMsgToPlayer( GCPlateExecRst );

	return true;
	TRY_END;
	return false;
}

///对玩家关托盘的响应，或者主动关玩家托盘；MGClosePlate
bool CDealMapSrvPkg::OnClosePlate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR("OnClosePlate,MapSrv指针为空\n");
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGClosePlate );
	const MGClosePlate* srvmsg = (const MGClosePlate*)pPkg;

	GCClosePlate climsg;

	GateSrvSendMsgToPlayer( GCClosePlate );

	return true;
	TRY_END;
	return false;
}

///给玩家添加了一个新任务；//MGPlayerNewTask
bool CDealMapSrvPkg::OnPlayerNewTask( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{

	if( NULL == pOwner)
	{
		D_ERROR("OnPlayerNewTask,MapSrv指针为空\n");
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGPlayerNewTask );
	const MGPlayerNewTask* srvmsg = (const MGPlayerNewTask*)pPkg;

	GCPlayerNewTask     climsg;
	climsg.taskID     = srvmsg->taskID;
	climsg.taskTypeID = srvmsg->taskTypeID;

	GateSrvSendMsgToPlayer( GCPlayerNewTask );

	return true;
	TRY_END;
	return false;
}

///通知玩家任务新状态；//MGPlayerTaskStatus
bool CDealMapSrvPkg::OnPlayerTaskStatus( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR("OnPlayerTaskStatus,MapSrv指针为空\n");
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGPlayerTaskStatus );
	const MGPlayerTaskStatus* srvmsg = (const MGPlayerTaskStatus*)pPkg;

	GCPlayerTaskStatus climsg;
	climsg.taskID    = srvmsg->taskID;
	climsg.taskStat  = srvmsg->taskStat;
	climsg.failTime  = srvmsg->failTime;//初始无失败计时;

	GateSrvSendMsgToPlayer( GCPlayerTaskStatus );

	return true;
	TRY_END;
	return false;
}

///怪物或其它玩家离开玩家视野,MGLeaveView;
bool CDealMapSrvPkg::OnLeaveView( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( NULL == pOwner)
	{
		D_ERROR( "OnLeaveView,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGLeaveView );
	const MGLeaveView* srvmsg = (const MGLeaveView*)pPkg;
	if( NULL == pOwner)
	{
		D_ERROR("OnLeaveView,MapSrv指针为空\n");
		return false;
	}

	GCLeaveView climsg;
	climsg.leaveMonsterID = srvmsg->leaveMonsterID;
	climsg.leavePlayerID  = srvmsg->leavePlayerID;

	GateSrvSendMsgToPlayer( GCLeaveView );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUseAssistSkill( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{

	if( NULL == pOwner)
	{
		D_ERROR("OnUseAssistSkill,MapSrv指针为空\n");
		return false;
	}

	TRY_BEGIN;

	PKGSIZECHECK( MGUseAssistSkill );
	const MGUseAssistSkill* srvmsg = (const MGUseAssistSkill*)pPkg;

	GCUseAssistSkill climsg;
	climsg.nErrorCode = srvmsg->nErrorCode;
	climsg.skillID = srvmsg->skillID;
	climsg.useSkillPlayer = srvmsg->useSkillPlayer;
	climsg.targetPlayer = srvmsg->targetPlayer;
	climsg.nAtkPlayerSpExp = srvmsg->nUseSkillPlayerSpExp;
	climsg.nAtkPlayerSpSkillExp = srvmsg->nUseSkillPlayerSkillExp;
	climsg.nAtkPlayerSpPower = srvmsg->nUseSkillPlayerSpPower;
	climsg.skillCurseTime = srvmsg->skillCurseTime;
	climsg.skillCooldownTime = srvmsg->skillCooldownTime;
	climsg.nAtkPlayerMp = srvmsg->nUseSkillPlayerMp;

	if ( pOwner->BroadcastSend< GCUseAssistSkill >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	GateSrvSendMsgToPlayer( GCUseAssistSkill );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRepairWearByItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnRepairWearByItem,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGRepairWearByItem) )
		return false;

	MGRepairWearByItem * pRepairWearByItem = (MGRepairWearByItem * )pPkg;
	GCRepairWearByItem msg;
	msg.itemRepairman = pRepairWearByItem->itemRepairman;
	msg.repairTarget = pRepairWearByItem->repairTarget;
	msg.tResult = (GCRepairWearByItem::eRepairResult)(pRepairWearByItem->tResult);
	msg.newWear = pRepairWearByItem->newWear;
	msg.uiRepairSilverMoney = pRepairWearByItem->uiRepairSilverMoney;
	msg.uiRepairGoldMoney = pRepairWearByItem->uiRepairGoldMoney;

	// 接受请求的玩家
	CPlayer * pPlayer = CManPlayer::FindPlayer( pRepairWearByItem->lNotiPlayerID.dwPID);
	if(NULL == pPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCRepairWearByItem, pPlayer, msg );
	//MsgToPut *pGCRepairWearByItem = CreatePlayerPkg(GCRepairWearByItem, pPlayer, msg);
	//pPlayer->SendPkgToPlayer(pGCRepairWearByItem);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnNotifyWearChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnNotifyWearChange,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if( wPkgLen != sizeof(MGNotifyWearChange) )
		return false;

	MGNotifyWearChange * pNotifyWearChange = (MGNotifyWearChange * )pPkg;
	
	GCNotifyItemWearChange msg;
	msg.itemSize = pNotifyWearChange->itemSize;
	SAFE_ARRSIZE(msg.items, msg.itemSize);
	StructMemCpy( (msg.items), &(pNotifyWearChange->items), sizeof(msg.items[0])*msg.itemSize );

	// 接受请求的玩家
	CPlayer * pPlayer = CManPlayer::FindPlayer( pNotifyWearChange->lNotiPlayerID.dwPID);
	if(NULL == pPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	//发送到客户端
	NewSendPlayerPkg( GCNotifyItemWearChange, pPlayer, msg );
	//MsgToPut *pGCNotifyWearChange = CreatePlayerPkg(GCNotifyItemWearChange, pPlayer, msg);
	//pPlayer->SendPkgToPlayer(pGCNotifyWearChange);

	return true;
	TRY_END;
	return false;

}

bool CDealMapSrvPkg::OnPlayerIsBusying(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerIsBusying,pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(MGPlayerBusying) )
		return false;

	MGPlayerBusying* pPlayerIsBusying = ( MGPlayerBusying *)pPkg;

	// 接受请求的玩家
	CPlayer * pPlayer = CManPlayer::FindPlayer( pPlayerIsBusying->lNotiPlayerID.dwPID);
	if(NULL == pPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	GCPlayerIsBusying playerBusying;
	StructMemSet( playerBusying, 0x0,sizeof(playerBusying) );
	SafeStrCpy2( playerBusying.PlayerName, pPlayerIsBusying->PlayerNickName,playerBusying.nameSize );
	/*playerBusying.nameSize = ACE_OS::snprintf(
		playerBusying.PlayerName,
		sizeof(playerBusying.PlayerName),
		"%s",
		pPlayerIsBusying->PlayerNickName
		);*/

	playerBusying.busyCode = (EPlayerBusyingReason)pPlayerIsBusying->errCode;

	//发送到客户端
	NewSendPlayerPkg( GCPlayerIsBusying, pPlayer, playerBusying );
	//MsgToPut *pNewMsg = CreatePlayerPkg(GCPlayerIsBusying, pPlayer, playerBusying);
	//pPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnChangeItemCri(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnChangeItemCri pOwner空\n" );
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(MGChangeItemCri))
		return false;

	MGChangeItemCri* pChangeItemCri = (MGChangeItemCri* )pPkg;

	// 接受请求的玩家
	CPlayer * pPlayer = CManPlayer::FindPlayer( pChangeItemCri->lNotiPlayerID.dwPID);
	if(NULL == pPlayer)
	{
		D_DEBUG("找不到要通知的玩家\n");
		return false;
	}

	GCChangeItemCri itemCri;
	itemCri.changeItemCri = pChangeItemCri->changeCri;

	//发送到客户端
	NewSendPlayerPkg( GCChangeItemCri, pPlayer, itemCri );
	//MsgToPut *pNewMsg = CreatePlayerPkg( GCChangeItemCri, pPlayer, itemCri );
	//pPlayer->SendPkgToPlayer(pNewMsg);
	return true;

	TRY_END;

	return false;
}


bool CDealMapSrvPkg::OnUpdateSkillResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( !pOwner )
		return false;

	TRY_BEGIN;

	if ( wPkgLen != sizeof(MGUpgradeSkillResult))
		return false;

	MGUpgradeSkillResult* pSrvMsg = (MGUpgradeSkillResult* )pPkg;

	// 接受请求的玩家
	CPlayer * pPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID);
	if(NULL == pPlayer)
	{
		D_DEBUG("CDealMapSrvPkg::OnUpdateSkillResult 找不到要通知的玩家\n");
		return false;
	}

	GCUpgradeSkillResult climsg;
	climsg.errorCode = pSrvMsg->errorCode;
	climsg.oldSkillID = pSrvMsg->oldSkillID;
	climsg.newSkillID = pSrvMsg->newSkillID;
	climsg.phySkillPoint = pSrvMsg->phySkillPoint;
	climsg.magSkillPoint = pSrvMsg->magSkillPoint;
	//发送到客户端
	NewSendPlayerPkg( GCUpgradeSkillResult, pPlayer, climsg );
	//MsgToPut *pNewMsg = CreatePlayerPkg( GCUpgradeSkillResult, pPlayer, climsg );
	//pPlayer->SendPkgToPlayer(pNewMsg);
	return true;

	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnSpSkillUpgrade( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnSpSkillUpgrade,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGSpSkillUpgrade) )
	{
		return false;
	}

	TRY_BEGIN;

	MGSpSkillUpgrade* pMapSrvMsg = (MGSpSkillUpgrade*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pMapSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnSpSkillUpgrade,找不到通知的玩家\n");
		return false;
	}
	
	GCSpSkillUpgrade climsg;
	climsg.oldSpSkillID = pMapSrvMsg->oldSpSkillID;
	climsg.newSpSkillID = pMapSrvMsg->newSpSkillID;
	NewSendPlayerPkg( GCSpSkillUpgrade, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCSpSkillUpgrade, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUpdateTaskCounterNum(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUpdateTaskCounterNum,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerTaskRecordUpdate) )
	{
		return false;
	}

	TRY_BEGIN;

	MGPlayerTaskRecordUpdate* pMapSrvMsg = (MGPlayerTaskRecordUpdate*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pMapSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUpdateTaskCounterNum,找不到通知的玩家\n");
		return false;
	}

	GCUpdateTaskRecordInfo climsg;
	climsg.taskRecord = pMapSrvMsg->taskRdInfo;
	climsg.taskType = pMapSrvMsg->taskType;
	NewSendPlayerPkg( GCUpdateTaskRecordInfo, pNotifyPlayer, climsg );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnTaskHistoryReturn(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnUpdateTaskCounterNum,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerTaskHistory) )
	{
		return false;
	}

	TRY_BEGIN;

	MGPlayerTaskHistory* pSrvMsg = ( MGPlayerTaskHistory *) pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnTaskHistoryReturn,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCTaskHistory, pNotifyPlayer, pSrvMsg->gcTaskHistory );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnNoticePlayerEquipItem(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnNoticePlayerEquipItem,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGNoticeEquipItem) )
	{
		return false;
	}


	TRY_BEGIN;

	MGNoticeEquipItem* pSrvMsg = ( MGNoticeEquipItem *) pPkg;

	GCPlayerEquipItem climsg;
	climsg.equipPlayer = pSrvMsg->equipPlayer;
	climsg.equipIndex = pSrvMsg->equipIndex;
	climsg.equipItem = pSrvMsg->equipItem;

	if ( pOwner->BroadcastSend< GCPlayerEquipItem >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnNoticePlayerEquipItem,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCPlayerEquipItem, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerEquipItem, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnShowTaskUIMsg(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "OnShowTaskUIMsg,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerShowTaskUI) )
	{
		return false;
	}

	TRY_BEGIN;

	MGPlayerShowTaskUI* pSrvMsg = ( MGPlayerShowTaskUI *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnShowTaskUIMsg,找不到通知的玩家\n");
		return false;
	}

	GCShowTaskUIInfo climsg;
	climsg.taskUIInfo = pSrvMsg->eTaskUIInfo;

	NewSendPlayerPkg( GCShowTaskUIInfo, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCShowTaskUIInfo, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnCleanSkillPoint( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnCleanSkillPoint,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGCleanSkillPoint) )
	{
		return false;
	}

	MGCleanSkillPoint* pMapSrvMsg = (MGCleanSkillPoint*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pMapSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnCleanSkillPoint,找不到通知的玩家\n");
		return false;
	}

	GCCleanSkillPoint climsg;
	climsg.assignablePhyPoint = pMapSrvMsg->assignablePhyPoint;
	climsg.assignableMagPoint = pMapSrvMsg->assignableMagPoint;
	StructMemCpy( climsg.skillData, pMapSrvMsg->skillData, sizeof(climsg.skillData) );
	climsg.skillSize = pMapSrvMsg->skillSize;

	NewSendPlayerPkg( GCCleanSkillPoint, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCCleanSkillPoint, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;

}

bool CDealMapSrvPkg::OnAddSpSkillResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddSpSkillResult,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGAddSpSkillResult) )
	{
		return false;
	}

	MGAddSpSkillResult* pMapSrvMsg = (MGAddSpSkillResult*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pMapSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnAddSpSkillResult,找不到通知的玩家\n");
		return false;
	}

	GCAddSpSkillResult climsg;
	climsg.skillID = pMapSrvMsg->addSpSkillID;
	climsg.bSuccess = pMapSrvMsg->bSuccess;
	climsg.nSkillDataIndex = pMapSrvMsg->nSkillDataIndex;

	NewSendPlayerPkg( GCAddSpSkillResult, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddSpSkillResult, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnThreatList( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnThreatList,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGThreatList) )
	{
		return false;
	}

	MGThreatList* pSrvMsg = (MGThreatList*)pPkg;

	GCThreatList climsg;
	climsg.threatList = pSrvMsg->threatList;

	if ( pOwner->BroadcastSend< GCThreatList >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnThreatList 找不到通知的玩家\n");
		return false;
	}
	
	NewSendPlayerPkg( GCThreatList, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCThreatList, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
	//D_DEBUG("向客户端发送威胁列表\n");

	return true;
	TRY_END;
	return false;
}

//显示NPC商店
bool CDealMapSrvPkg::OnShowShop( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnShowShop,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShowShop) )
	{
		return false;
	}

	MGShowShop* pRes = (MGShowShop*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnShowShop, 找不到通知的玩家\n");
		return false;
	}

	GCShowShop climsg;
	climsg.shopID = pRes->shopID;
	climsg.shopPageID = pRes->shopPageID;
	climsg.itemArrSize = sizeof( pRes->itemNum ) / sizeof( pRes->itemNum[0] ) ;
	StructMemCpy( climsg.itemNumArr, pRes->itemNum, min(sizeof(pRes->itemNum), sizeof(climsg.itemNumArr)) );
	
	NewSendPlayerPkg( GCShowShop, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCShowShop, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

//NPC商店购买结果
bool CDealMapSrvPkg::OnShopBuyRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnShowShop,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShopBuyRst) )
	{
		return false;
	}

	MGShopBuyRst* pRes = (MGShopBuyRst*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnShopBuyRst, 找不到通知的玩家\n");
		return false;
	}

	GCShopBuyRst climsg;
	climsg.shopID = pRes->shopID;                                     	//商店ID号
	climsg.shopPageID = pRes->shopPageID;                                 	//商店页码
	climsg.shopPagePos = pRes->shopPagePos;                                	//商品在页中位置
	climsg.itemID = pRes->itemID;                                     	//商品类型ID号
	climsg.itemNum = pRes->itemNum;                                    	//一次购买的商品数量
	climsg.moneyType = pRes->moneyType;                                  	//货币类型ID号
	climsg.moneyNum = pRes->moneyNum;                                   	//购买使用的货币数量
	climsg.itemPos = 0;                                    	//购买成功以后，新买物品在包裹中的位置
	climsg.buyRst = pRes->buyRst;                                     	//购买结果
	
	NewSendPlayerPkg( GCShopBuyRst, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCShopBuyRst, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

//商店售出结果
bool CDealMapSrvPkg::OnItemSoldRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnItemSoldRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGItemSoldRst) )
	{
		return false;
	}

	MGItemSoldRst* pRes = (MGItemSoldRst*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnItemSoldRst, 找不到通知的玩家\n");
		return false;
	}

	GCItemSoldRst climsg;
	climsg.itemID = pRes->itemID;
	climsg.itemPos = pRes->itemPos;
	climsg.itemNum = pRes->itemNum;
	climsg.silverMoneyNum = pRes->silverMoneyNum;
	climsg.playerSilverMoneyNum = 0;//原来就没有通知；
	climsg.soldRst = pRes->soldRst;
	
	NewSendPlayerPkg( GCItemSoldRst, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCItemSoldRst, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

//添加活点结果；
bool CDealMapSrvPkg::OnAddAptRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnAddAptRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGAddActivePtRst) )
	{
		return false;
	}

	MGAddActivePtRst* pRes = (MGAddActivePtRst*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnAddAptRst, 找不到通知的玩家\n");
		return false;
	}

	GCAddActivePtRst climsg;
	climsg.reqRst = pRes->reqRst;
	climsg.reqID = pRes->reqID;
	climsg.tgtPlayerID = pRes->tgtPlayerID;
	climsg.posX = pRes->posX;
	climsg.posY = pRes->posY;
	
	NewSendPlayerPkg( GCAddActivePtRst, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCAddActivePtRst, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnFriendInfoUpdate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "MGFriendInfoUpdate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGFriendInfoUpdate) )
	{
		return false;
	}


	MGFriendInfoUpdate* pRes = (MGFriendInfoUpdate *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnFriendInfoUpdate, 找不到通知的玩家\n");
		return false;
	}

	//等级更新一下
	pNotifyPlayer->GetFullPlayerInfo().baseInfo.usLevel = pRes->level;
	pNotifyPlayer->SetLevel( pRes->level );

	//通知RelationSrv
	pNotifyPlayer->OnUpdateSelfInfo( false );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRepairItemMoneyRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnRepairItemMoneyRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRepairItemCostRst) )
	{
		return false;
	}

	MGRepairItemCostRst* pRes = (MGRepairItemCostRst *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnRepairItemMoneyRst, 找不到通知的玩家\n");
		return false;
	}

	GCQueryRepairItemCostRst climsg;
	climsg.costSilverMoney = pRes->costSilverMoney;
	climsg.costGoldMoney = pRes->costGoldMoney;

	NewSendPlayerPkg( GCQueryRepairItemCostRst, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCQueryRepairItemCostRst, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

//bool CDealMapSrvPkg::OnInfoUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
//{
//	TRY_BEGIN;
//
//	if ( NULL == pOwner )
//	{
//		D_ERROR( "OnInfoUpdate,pOwner空\n" );
//		return false;
//	}
//
//	if ( wPkgLen != sizeof(MGInfoUpdate) )
//	{
//		return false;
//	}
//
//	MGInfoUpdate* pRes = (MGInfoUpdate *)pPkg;
//	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
//	if( NULL == pNotifyPlayer )
//	{
//		D_WARNING("OnInfoUpdate, 找不到通知的玩家\n");
//		return false;
//	}
//
//	
//	GCInfoUpdate climsg;
//	climsg.nAtkPlayerExp = pRes->nAtkPlayerExp;
//	climsg.nAtkPlayerHP = pRes->nAtkPlayerHP;
//	climsg.nAtkPlayerMP = pRes->nAtkPlayerMP;
//	climsg.nAtkPlayerMoney = pRes->nAtkPlayerMoney;
//	climsg.nAtkPlayerPhyExp = pRes->nAtkPlayerPhyExp;
//	climsg.nAtkPlayerMagExp = pRes->nAtkPlayerMagExp;
//	climsg.nAtkPlayerSpExp = pRes->nAtkSpExp;
//	climsg.nAtkPlayerSpPower = pRes->nAtkPlayerSpPower;
//	climsg.nAtkPlayerSpSkillExp = pRes->nAtkSkillSpExp;
//	climsg.useSkillID = pRes->nAtkSkillID;
//
//	NewSendPlayerPkg( GCInfoUpdate, pNotifyPlayer, climsg );
//	//MsgToPut* pNewMsg = CreatePlayerPkg( GCInfoUpdate, pNotifyPlayer, climsg );
//	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );
//
//	return true;
//	TRY_END;
//	return false;
//}


bool CDealMapSrvPkg::OnBuffData( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnBuffData,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGBuffData) )
	{
		return false;
	}

	MGBuffData* pRes = (MGBuffData *)pPkg;

	GCBuffData climsg;
	climsg.buffData = pRes->buffData;
	climsg.creatureID = pRes->targetID;

	if ( pOwner->BroadcastSend< GCBuffData >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnBuffData, 找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBuffData, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCBuffData, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnEquipItemUpDate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnEquipItemUpDate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGEquipItemUpdate) )
	{
		return false;
	}

	MGEquipItemUpdate* pSrv = (MGEquipItemUpdate *)pPkg;

	GCSetEquipItem climsg;
	climsg.changePlayerID = pSrv->changePlayerID;
	climsg.ItemInfo = pSrv->newEquipItem;
	climsg.ItemEquipIndex = pSrv->equipIndex;

	if ( pOwner->BroadcastSend< GCSetEquipItem >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrv->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnEquipItemUpDate, 找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCSetEquipItem, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCSetEquipItem, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnSwichMapReq(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnSwichMapReq,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "OnSwichMapReq, NULL == pPkg\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGSwichMapReq) )
	{
		return false;
	}

	MGSwichMapReq* pSrv = (MGSwichMapReq *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrv->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnSwichMapReq, 找不到通知的玩家\n");
		return false;
	}

	pNotifyPlayer->OnMapOrPlayerSwitchReq( pSrv->newMapID, pSrv->targetX, pSrv->targetY );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnTargetGetDamage( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnTargetGetDamage,pOwner空\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "OnTargetGetDamage, NULL == pPkg\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGTargetGetDamage) )
	{
		return false;
	}

	MGTargetGetDamage* pSrv = (MGTargetGetDamage *)pPkg;

	GCTargetGetDamage climsg;
	climsg.targetID = pSrv->targetID;                                   	//受到伤害的目标ID,可填怪物ID和玩家ID
	climsg.skillID = pSrv->skillID;                                    	//作用于目标身上的技能ID
	climsg.damage = pSrv->damage;                                     	//目标受到的伤害值
	climsg.battleFlag = pSrv->battleFlag;                                 	//用于给客户端识别到底是暴击,MISS,或者免疫的标志
	climsg.targetLeftHp = pSrv->targetLeftHp;                               	//目标剩余的血量
	climsg.notiecID = pSrv->noticeID;

	if ( pOwner->BroadcastSend< GCTargetGetDamage >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrv->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnTargetGetDamage, 找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCTargetGetDamage, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCTargetGetDamage, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnSwapEquipItemRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnSwapEquipItemRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGSwapEquipItemRst) )
	{
		return false;
	}

	MGSwapEquipItemRst* pSrv = (MGSwapEquipItemRst *)pPkg;

	GCSwapEquipResult climsg;
	climsg.equipFrom = pSrv->from;
	climsg.equipTo =pSrv->to;
	climsg.swapResult = pSrv->isSuccess;
	climsg.swapPlayerID = pSrv->swapPlayerID;

	if ( pOwner->BroadcastSend< GCSwapEquipResult >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrv->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnSwapEquipItemRst, 找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCSwapEquipResult, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCSwapEquipResult, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnChangeTeamState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "MGEvilTimeRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGChangeTeamState) )
	{
		return false;
	}

	MGChangeTeamState* pRes = (MGChangeTeamState *)pPkg;

	GCChangeTeamState climsg;
	climsg.changePlayerID = pRes->changePlayerID;
	climsg.teamState = pRes->teamState;

	if ( pOwner->BroadcastSend< GCChangeTeamState >( &climsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnChangeTeamState 找不到通知的玩家\n");
		return false;
	}

    NewSendPlayerPkg( GCChangeTeamState, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeTeamState, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
	
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnPunishmentStart( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnPunishStart,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPunishmentStart) )
	{
		return false;
	}

	ACE_UNUSED_ARG( pPkg );

	//暂时不做
	GFPunishmentBegin rankMsg;
	CFunctionSrv* pRankSrv = CManRankSrv::GetRankSrv();
	if( NULL == pRankSrv )
	{
		D_ERROR("CDealMapSrvPkg::OnPunishmentStart 找不到ranksrv\n");
		return false;
	}

	//MsgToPut* pNewMsg = CreateSrvPkg( GFPunishmentBegin, pRankSrv, rankMsg );
	//pRankSrv->SendPkgToSrv( pNewMsg );
	SendMsgToSrv( GFPunishmentBegin, pRankSrv, rankMsg );


	/*GECheckPunishment cenMsg;
	SendMsgToCenterSrv( GECheckPunishment, cenMsg );
	D_DEBUG("收到天谴开始消息，向RANKSRV确认开始人员\n");*/

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnPunishmentEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnPunishEnd,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPunishmentEnd) )
	{
		return false;
	}

	ACE_UNUSED_ARG( pPkg );
	
	GMPunishmentEnd mapMsg;
	CManMapSrvs::SendBroadcastMsg<GMPunishmentEnd>( &mapMsg );

	//告之玩家天谴已经结束
	GESrvGroupChat cenMsg;
	SafeStrCpy( cenMsg.chatContent, "本次天谴活动已结束" );
	cenMsg.chatType = CT_WORLD_CHAT;//世界聊天
	cenMsg.extInfo = CT_SYS_EVENT_NOTI;//系统事件
	SendMsgToCenterSrv( GESrvGroupChat, cenMsg );
	D_DEBUG("收到天谴结束消息,向mapsrv群发天谴结束消息\n");

	GENotifyActivityState stateMsg;
	stateMsg.activityState = GCNotifyActivityState::ACTIVITY_END;
	stateMsg.activityType = GCNotifyActivityState::PUNISHMENT_ACTIVITY;
	SendMsgToCenterSrv( GENotifyActivityState, stateMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnBroadcastPunishInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnBroadcastPunishPlayerInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGBroadcastPunishInfo) )
	{
		return false;
	}

	MGBroadcastPunishInfo* pPunishInfo = (MGBroadcastPunishInfo *)pPkg;

	GMPunishPlayerInfo mapMsg;
	mapMsg.punishPlayer = pPunishInfo->punishPlayer;
	CManMapSrvs::SendBroadcastMsg<GMPunishPlayerInfo>( &mapMsg );
	D_DEBUG("收到天谴人员信息更新消息,向mapsrv组播人员更新消息\n");
	
	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnQueryPunishPlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnQueryPunishPlayerInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGQueryPunishPlayerInfo) )
	{
		return false;
	}

	MGQueryPunishPlayerInfo* pPunishInfo = (MGQueryPunishPlayerInfo *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pPunishInfo->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnQueryPunishPlayerInfo 找不到通知的玩家\n");
		return false;
	}

	GCPunishPlayerInfo cliMsg;
	StructMemCpy( cliMsg.punishMember, pPunishInfo->members, sizeof(cliMsg.punishMember) );
	cliMsg.memberSize = pPunishInfo->memberSize;

	NewSendPlayerPkg( GCPunishPlayerInfo, pNotifyPlayer, cliMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPunishPlayerInfo, pNotifyPlayer, cliMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
	
	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnGmCmdRequest( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnGmCmdRequest,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGGmCmdRequest) )
	{
		return false;
	}

	MGGmCmdRequest* pRequest = (MGGmCmdRequest *)pPkg;

	GEGmCmdRequest cenMsg;
	cenMsg.requestPlayerID = pRequest->requestPlayerID;
	SafeStrCpy( cenMsg.strChat, pRequest->strChat );
	SafeStrCpy( cenMsg.targetName, pRequest->targetName );
	SendMsgToCenterSrv( GEGmCmdRequest, cenMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnGmCmdResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnGmCmdResult,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGGmCmdResult) )
	{
		return false;
	}

	MGGmCmdResult* pSrvMsg = (MGGmCmdResult *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pSrvMsg->requestPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealCenterSrvPkg::OnGmCmdResult 找不到通知的Player\n");
		return false;
	}

	
	SafeStruct( GCChat, sysMsg );
	if( pSrvMsg->resultCode )
	{
		SafeStrCpy( sysMsg.strChat, "使用GM命令成功" );
	}else{
		SafeStrCpy( sysMsg.strChat, "使用GM命令失败" );
	}
	sysMsg.chatSize = (UINT)strlen( sysMsg.strChat ) +1;
	sysMsg.chatType = CT_SYS_EVENT_NOTI;
	sysMsg.extInfo = 0;

	NewSendPlayerPkg( GCChat, pNotifyPlayer, sysMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChat, pNotifyPlayer, sysMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
	
	
	return true;
	TRY_END;
	return false;	
}


//宠物不稳信息(只发给宠物主人)
bool CDealMapSrvPkg::OnPetinfoInStable( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnPetinfoInStable,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPetinfoInstable) )
	{
		return false;
	}

	MGPetinfoInstable* pRes = (MGPetinfoInstable *)pPkg;

	//通知客户端
	GCPetinfoInstable* pCliMsg = &(pRes->petInfoInstable);

	if ( pOwner->BroadcastSend< GCPetinfoInstable >( pCliMsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("CDealMapSrvPkg::OnPetinfoInStable, 找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCPetinfoInstable, pNotifyPlayer, *pCliMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPetinfoInstable, pNotifyPlayer, *pCliMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
	
	return true;
	TRY_END;
	return false;
}

//宠物能力信息(只发给宠物主人)
bool CDealMapSrvPkg::OnPetAbility( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnPetAbility,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPetAbility) )
	{
		return false;
	}

	MGPetAbility* pRes = (MGPetAbility *)pPkg;

	//通知客户端
	GCPetAbility* pCliMsg = &(pRes->petAbility);

	if ( pOwner->BroadcastSend< GCPetAbility >( pCliMsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("CDealMapSrvPkg::OnPetAbility, 找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCPetAbility, pNotifyPlayer, *pCliMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPetAbility, pNotifyPlayer, *pCliMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
	
	return true;
	TRY_END;
	return false;
}

//宠物稳定信息(发给宠物主人周围其它玩家)
bool CDealMapSrvPkg::OnPetinfoStable( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnPetinfoStable,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPetinfoStable) )
	{
		return false;
	}

	MGPetinfoStable* pRes = (MGPetinfoStable *)pPkg;

	//通知客户端
	GCPetinfoStable* pCliMsg = &(pRes->petInfoStable);

	if ( pOwner->BroadcastSend< GCPetinfoStable >( pCliMsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("CDealMapSrvPkg::OnPetinfoStable, 找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCPetinfoStable, pNotifyPlayer, *pCliMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPetinfoStable, pNotifyPlayer, *pCliMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
	
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRankPlayerOffLine(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnRankPlayerOffLine,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRankPlayerOffLine) )
	{
		return false;
	}

	MGRankPlayerOffLine* pOffLine = (MGRankPlayerOffLine *)pPkg;
	if ( !pOffLine )
	{
		return false;
	}

	//获取排行榜服务器的指针
	GFRankPlayerOffLine playerOffLine;
	playerOffLine.rankPlayerRace = pOffLine->race;
	playerOffLine.rankPlayerUID =  pOffLine->playerUID;
	SendMsgToFunctionSrv( GFRankPlayerOffLine, playerOffLine );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRemoveRankPlayer(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnRemoveRankPlayer,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRemoveRankPlayer) ) 
	{
		return false;
	}

	MGRemoveRankPlayer* pRemovePlayer = ( MGRemoveRankPlayer *)pPkg;
	if ( !pRemovePlayer )
	{
		return false;
	}

	GFRemoveRankPlayerByChartID removePlayer;
	removePlayer.playerUID = pRemovePlayer->playerUID;
	removePlayer.rankChartID = pRemovePlayer->rankChartID;

	SendMsgToFunctionSrv( GFRemoveRankPlayerByChartID, removePlayer );


	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUpdateFunctionSrvStorageSafeInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnUpdateFunctionSrvStorageSafeInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUpdateStorageSafeInfo) ) 
	{
		return false;
	}

	MGUpdateStorageSafeInfo* pUpdateSafeInfo = ( MGUpdateStorageSafeInfo *)pPkg;

	SendMsgToFunctionSrv( GFUpdateStorageSafeInfo , pUpdateSafeInfo->updateSafeInfo );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnUpdateFunctionSrvStorageItemInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnUpdateFunctionSrvStorageItemInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUpdateStorageItemInfo) ) 
	{
		return false;
	}

	MGUpdateStorageItemInfo* pUpdateItemInfo = ( MGUpdateStorageItemInfo *)pPkg;

	SendMsgToFunctionSrv( GFUpdateStorageItemInfo ,pUpdateItemInfo->updateStorageItem );
	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUpdateFunctionSrvStorageForbiddenInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnUpdateFunctionSrvStorageForbiddenInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUpdateStorageForbiddenInfo) ) 
	{
		return false;
	}

	MGUpdateStorageForbiddenInfo* pUpdateForbiddenInfo = ( MGUpdateStorageForbiddenInfo *)pPkg;

	SendMsgToFunctionSrv( GFUpateStorageForbiddenInfo , pUpdateForbiddenInfo->updateForbiddenInfo );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnQueryStorageInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnQueryStorageInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGQueryStorageInfo) ) 
	{
		return false;
	}

	MGQueryStorageInfo* pQueryStorage = ( MGQueryStorageInfo *)pPkg;

	SendMsgToFunctionSrv( GFQueryStorageInfo , pQueryStorage->queryStorageInfo );
	return true;

	TRY_END;

	return false;
}


#ifdef WAIT_QUEUE
///mapsrv通知排队次序已到，可以进map;
bool CDealMapSrvPkg::QueueOK( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::QueueOK,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGQueueOK) ) 
	{
		return false;
	}

	MGQueueOK* pMsg = ( MGQueueOK *)pPkg;
	if ( NULL == pMsg )
	{
		return false;
	}

	CPlayer* pOkPlayer = CManPlayer::FindPlayer( pMsg->okPlayer.dwPID );
	if( NULL == pOkPlayer )
	{
		D_ERROR("CDealMapSrvPkg::QueueOK，找不到可进入的玩家，可能该玩家已下线\n");
		return false;
	}

	//如果玩家的确在排队状态，则再次尝试进地图，否则，说明玩家状态已发生变化(例如已取消排队)，不理会此消息
	if ( PS_QUEUEING != pOkPlayer->GetCurStat() )
	{
		//由于玩家状态变化(如取消排队的情况)时已通知mapsrv，因此这里不再通知；
		D_WARNING( "收到排队玩家%s可进消息时，玩家可能已取消排队\n", pOkPlayer->GetAccount() );
		return false;
	}

	//玩家再次尝试进地图；
	pOkPlayer->EnterMapSrv( false );//mapsrv通知可进，不需要排队；

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnGetQueryQueueResult( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealMapSrvPkg::OnGetQueryQueueResult,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGQueryQueueResult) ) 
	{
		return false;
	}

	MGQueryQueueResult* pResult = ( MGQueryQueueResult *)pPkg;
	if ( !pResult )
	{
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResult->reqPlayer.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnGetQueryQueueResult pNotifyPlayer为空\n");
		return false;
	}

	//告之客户端需要排队
	GCWaitQueueResult cliMsg;
	cliMsg.waitIndex = pResult->queueIndex;
	cliMsg.waitPlayer = pResult->reqPlayer;

	NewSendPlayerPkg( GCWaitQueueResult, pNotifyPlayer, cliMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCWaitQueueResult, pNotifyPlayer, cliMsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	pNotifyPlayer->SetCurStat( PS_QUEUEING );

	return true;
	TRY_END;
	return false;
}
#endif //WAIT_QUEUE


//bool CDealMapSrvPkg::OnRankPlayerUpLine(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	if ( NULL == pOwner )
//	{
//		D_ERROR( "CDealMapSrvPkg::OnRankPlayerUpLine,pOwner空\n" );
//		return false;
//	}
//
//	if ( wPkgLen != sizeof(MGRankPlayerOnLine) )
//	{
//		return false;
//	}
//
//	MGRankPlayerOnLine* pOnLine = (MGRankPlayerOnLine *)pPkg;
//	if ( !pOnLine )
//	{
//		return false;
//	}
//
//	//获取排行榜服务器的指针
//
//	GFRankPlayerOnLine playerOnLine;
//	playerOnLine.rankPlayerUID  = pOnLine->playerUID;
//	playerOnLine.rankPlayerRace =  pOnLine->race;
//	playerOnLine.playerID = pOnLine->lNotiPlayerID;
//	SafeStrCpy( playerOnLine.szPlayerName ,pOnLine->szPlayerName );
//	SendMsgToFunctionSrv( GFRankPlayerOnLine, playerOnLine );
//
//	return true;
//	TRY_END;
//	return false;
//}
//

bool CDealMapSrvPkg::OnQueryEvilTimeRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "MGEvilTimeRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGEvilTimeRst) )
	{
		return false;
	}

	MGEvilTimeRst* pRes = (MGEvilTimeRst *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnQueryEvilTimeRst, 找不到通知的玩家\n");
		return false;
	}

	//通知客户端时间
	GCEvilTimeRst climsg;
	climsg.evilTime = pRes->EvilTime;

	NewSendPlayerPkg( GCEvilTimeRst, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCEvilTimeRst, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnChangeBattleMode( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnChangeBattleMode,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGChangeBattleMode) )
	{
		return false;
	}

	MGChangeBattleMode* pRes = (MGChangeBattleMode*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnChangeBattleMode, 找不到通知的玩家\n");
		return false;
	}

	GCChangeBattleMode climsg;
	climsg.bSuccess = pRes->bSuccess;
	climsg.changeMode = pRes->mode;
	
	NewSendPlayerPkg( GCChangeBattleMode, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeBattleMode, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

//收到玩家使用恢复道具
bool CDealMapSrvPkg::OnPlayerUseHealItem( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnPlayerUseHealItem, pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerUseHealItem) )
	{
		return false;
	}

	MGPlayerUseHealItem* pRes = (MGPlayerUseHealItem*)pPkg;

	if ( pOwner->BroadcastSend< GCPlayerUseHealItem >( &pRes->gcHeal ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRes->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_WARNING("OnPlayerUseHealItem, 找不到通知的玩家\n");
		return false;
	}
	
	NewSendPlayerPkg( GCPlayerUseHealItem, pNotifyPlayer, pRes->gcHeal );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPlayerUseHealItem, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRepairWearByNpc( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnRepairWearByNpc,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRepairWearByNPC) )
	{
		return false;
	}

	MGRepairWearByNPC* pResp = (MGRepairWearByNPC*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRepairWearByNpc,找不到通知的玩家\n");
		return false;
	}

	GCRepairWearByNPC climsg;
	climsg.tResult = (GCRepairWearByNPC::eRepairResult)pResp->tResult;
	climsg.repairAll = pResp->repairAll;
	climsg.repairTarget = pResp->repairTarget;
	climsg.newWear = pResp->newWear;
	climsg.uiRepairSilverMoney = pResp->repairSilverMoney;
	climsg.uiRepairGoldMoney = pResp->repairGoldMoney;

	NewSendPlayerPkg( GCRepairWearByNPC, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCRepairWearByNPC, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;

}

bool CDealMapSrvPkg::OnDisplayUnionIssueTask( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnDisplayUnionIssueTask,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGDisplayUnionIssuedTasks) )
	{
		return false;
	}

	MGDisplayUnionIssuedTasks* pResp = (MGDisplayUnionIssuedTasks*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnDisplayUnionIssueTask,找不到通知的玩家\n");
		return false;
	}

	/*GCDisplayUnionTasks climsg;
	StructMemSet(climsg, 0x00, sizeof(climsg));

	climsg.result = 0;
	climsg.taskEndTime = pResp->taskEndTime;

	for(int i = 0; i < sizeof(pResp->unionTask)/sizeof(pResp->unionTask[0]); i++)
	{
		if((0 == pResp->unionTask[i]) || i >= sizeof(climsg.tasks)/sizeof(climsg.tasks[0]))
			break;

		climsg.tasks[i] = pResp->unionTask[i];
		climsg.taskLen++;
	}

	if(climsg.taskLen > 0 )
	{
	    NewSendPlayerPkg( GCDisplayUnionTasks, pNotifyPlayer, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCDisplayUnionTasks, pNotifyPlayer, climsg );
		//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );
	}*/

	GCUnionUISelectCmd climsg;
	StructMemSet(climsg, 0x00, sizeof(climsg));

	climsg.cmdID = pResp->taskEndTime;

	NewSendPlayerPkg( GCUnionUISelectCmd, pNotifyPlayer, climsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUnionUISelectCmd, pNotifyPlayer, climsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg  );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnChanageUnionPrestige( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnChanageUnionPrestige,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGChangeUnionPrestige) )
	{
		return false;
	}

	MGChangeUnionPrestige* pResp = (MGChangeUnionPrestige*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnChanageUnionPrestige,找不到通知的玩家\n");
		return false;
	}

	//发给Relationserver
	ModifyUnionPrestigeRequest resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.bAdd = pResp->addFlag;
	resp.point = pResp->changeValue;
	resp.uiid = pNotifyPlayer->GetSeledRole();

	SendMsgToRelationSrv(ModifyUnionPrestigeRequest, resp);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnChangeUnionActive( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnChangeUnionActive,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGChangeUnionActive) )
	{
		return false;
	}

	MGChangeUnionActive* pResp = (MGChangeUnionActive*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnChangeUnionActive,找不到通知的玩家\n");
		return false;
	}

	//发给Relationserver
	ModifyUnionActivePointRequest resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.bAdd = pResp->addFlag;
	resp.point = pResp->changeValue;
	resp.uiid = pNotifyPlayer->GetSeledRole();

	SendMsgToRelationSrv(ModifyUnionActivePointRequest, resp);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUnionMemberShowInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionMemberShowInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUnionMemberShowInfo) )
	{
		return false;
	}

	MGUnionMemberShowInfo* pResp = (MGUnionMemberShowInfo*)pPkg;
	if ( pOwner->BroadcastSend< GCUnionMemberShowInfo >( &pResp->showInfos ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionMemberShowInfo,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCUnionMemberShowInfo, pNotifyPlayer, pResp->showInfos );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnSaveRebirthPosNtf( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnSaveRebirthPosNtf,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGSaveRebirthPosNtf) )
	{
		return false;
	}

	MGSaveRebirthPosNtf* pResp = (MGSaveRebirthPosNtf*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnSaveRebirthPosNtf,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCSaveRebirthPosNtf, pNotifyPlayer, pResp->saveRebirthPos );

	return true;
	TRY_END;
	return false;

}

bool CDealMapSrvPkg::OnUnionMultiReq( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnUnionMultiReq,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUnionMultiReq) )
	{
		return false;
	}

	MGUnionMultiReq* pResp = (MGUnionMultiReq*)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUnionMultiReq,找不到通知的玩家\n");
		return false;
	}

	//发给Relationserver
	ModifyUnionMultiRequest resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.uiid = pNotifyPlayer->GetSeledRole();
	resp.type = pResp->type;
	resp.multi = pResp->multi;
	resp.endTime = pResp->endTime;

	SendMsgToRelationSrv(ModifyUnionMultiRequest, resp);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUpdateCityInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnUpdateCityInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUpdateCityInfo) )
	{
		return false;
	}

	MGUpdateCityInfo* pResp = (MGUpdateCityInfo*)pPkg;

	GDUpdateCityInfo updateMsg;
	updateMsg.updateCityInfo = pResp->updateCityInfo;
	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByID( 0 );
	if( NULL == pDbSrv )
	{
		return false;
	}
	//MsgToPut* pNewMsg = CreateSrvPkg( GDUpdateCityInfo, pDbSrv, updateMsg );
	//pDbSrv->SendPkgToSrv( pNewMsg );
	SendMsgToSrv( GDUpdateCityInfo, pDbSrv, updateMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnAttackCityBegin( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnAttackCityBegin,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGAttackCityBegin) )
	{
		return false;
	}

	MGAttackCityBegin* pBegin = (MGAttackCityBegin*)pPkg;
	
	GMAttackWarBegin warBegin;
	warBegin.actID = pBegin->actID;
	CManMapSrvs::SendBroadcastMsg<GMAttackWarBegin>(&warBegin );
	
	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnAttackCityEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnAttackCityEnd,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGAttackCityEnd) )
	{
		return false;
	}
	
	MGAttackCityEnd* pEnd = (MGAttackCityEnd*)pPkg;


	//step2 如果是GateSrv0就要向所有mapsrv转发结束消息
	if( CLoadSrvConfig::GetSelfID() == 0 )
	{
		GMAttackWarEnd endMsg;
		endMsg.warTaskId = pEnd->warTaskId;
		CManMapSrvs::SendBroadcastMsg<GMAttackWarEnd>( &endMsg );
	}

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnNotifyWarTaskState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnNotifyWarTaskState,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGNotifyWarTaskState) )
	{
		return false;
	}

	MGNotifyWarTaskState* pResp = (MGNotifyWarTaskState*)pPkg;
		
	//step 2 转发到各个mapsrv 通知mapsrv上的玩家该任务状态改变，就GateSrv0 需要转发
	if( CLoadSrvConfig::GetSelfID() == 0 )
	{
		GMNotifyWarTaskState mapMsg;
		mapMsg.warTaskId = pResp->warTaskInfo.warTaskId;
		mapMsg.taskState = pResp->warTaskInfo.taskState;
		CManMapSrvs::SendBroadcastMsg<GMNotifyWarTaskState>( &mapMsg );
	}

	return true;
	TRY_END;
	return false;
}

////使用UseSkill2后废弃，原ID号由MOUSE_TIP使用, by dzj,bool CDealMapSrvPkg::OnUsePartionSkillRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	if ( NULL == pOwner )
//	{
//		D_ERROR( "OnUsePartionSkillRst,pOwner空\n" );
//		return false;
//	}
//
//	if ( wPkgLen != sizeof(MGUsePartionSkillRst) )
//	{
//		return false;
//	}
//
//	MGUsePartionSkillRst* pResp = (MGUsePartionSkillRst*)pPkg;
//
//	GCUsePartionSkillRst cliMsg;
//	cliMsg.battleFlag = pResp->battleFlag;
//	cliMsg.StartTime = pResp->StartTime;
//	cliMsg.EndTime = pResp->EndTime;
//	cliMsg.nAtkPlayerHP = pResp->nAtkPlayerHP;
//	cliMsg.nAtkPlayerMP = pResp->nAtkPlayerMP;
//	cliMsg.FinalPositionPosX = pResp->FinalPositionPosX;
//	cliMsg.FinalPositionPosY = pResp->FinalPositionPosY;
//	cliMsg.targetID = pResp->targetID;
//	cliMsg.aoeMainTargetID = pResp->aoeMainTargetID;
//	cliMsg.targetRewardID = pResp->targetRewardID;
//	cliMsg.lCooldownTime = pResp->lCooldownTime;
//	cliMsg.lHpLeft = pResp->lHpLeft;
//	cliMsg.lHpSubed = pResp->lHpSubed;
//	cliMsg.scopeFlag = pResp->scopeFlag;
//	cliMsg.lPlayerID = pResp->lPlayerID;
//	cliMsg.MidwaySpeed = pResp->MidwaySpeed;
//	cliMsg.nErrCode = pResp->nErrCode;
//	cliMsg.MinMidwayTime = pResp->MinMidwayTime;
//	cliMsg.skillID = pResp->skillID;
//
//	if ( pOwner->BroadcastSend< GCUsePartionSkillRst >( &cliMsg ) )
//	{
//		//已群发，不再单独发送；
//		return true;
//	}
//
//	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
//	if( NULL == pNotifyPlayer )
//	{
//		return false;
//	}
//
//	NewSendPlayerPkg( GCUsePartionSkillRst, pNotifyPlayer, cliMsg );
//	//MsgToPut* pNewMsg = CreatePlayerPkg( GCUsePartionSkillRst, pNotifyPlayer, cliMsg );
//	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
//
//	return true;
//	TRY_END;
//	return false;
//}
//对鼠标停留信息查询的响应
bool CDealMapSrvPkg::OnMGMouseTip( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnMGMouseTip,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGMouseTip) )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealMapSrvPkg::OnMGMouseTip, NULL == pPkg\n" );
		return false;
	}

	MGMouseTip* pResp = (MGMouseTip*)pPkg;

	if ( pOwner->BroadcastSend< GCMouseTip >( &(pResp->gcMouseTip) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		return false;
	}

	NewSendPlayerPkg( GCMouseTip, pNotifyPlayer, (pResp->gcMouseTip) );

	return true;
	TRY_END;
	return false;
}

//bool CDealMapSrvPkg::OnPetMove( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	if ( NULL == pOwner )
//	{
//		D_ERROR( "OnUsePartionSkillRst,pOwner空\n" );
//		return false;
//	}
//
//	MGPetMove* pResp = (MGPetMove*)pPkg;
//	GCPetMove cliMsg;
//	cliMsg.fOrgWorldPosX = pResp->fOrgWorldPosX;
//	cliMsg.fOrgWorldPosY = pResp->fOrgWorldPosY;
//	cliMsg.fNewWorldPosX = pResp->fNewWorldPosX;
//	cliMsg.fNewWorldPosY = pResp->fNewWorldPosY;
//	cliMsg.fSpeed = pResp->fPetSpeed;
//	cliMsg.petOwner = pResp->petOwner;
//
//	if ( pOwner->BroadcastSend< GCPetMove >( &cliMsg ) )
//	{
//		//已群发，不再单独发送；
//		return true;
//	}
//
//	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pResp->lNotiPlayerID.dwPID );
//	if( NULL == pNotifyPlayer )
//	{
//		D_ERROR("CDealMapSrvPkg::OnPetMove pNotifyPlayer为NULL\n");
//		return false;
//	}
//
//	NewSendPlayerPkg( GCPetMove, pNotifyPlayer, cliMsg );
//	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPetMove, pNotifyPlayer, cliMsg );
//	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );
//
//	return true;
//	TRY_END;
//	return false;
//}

//
//bool CDealMapSrvPkg::OnRaceGroupNotify( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )		//种族的群发，天谴用
//{
//	TRY_BEGIN;
//
//	if ( NULL == pOwner )
//	{
//		D_ERROR( "OnRaceGroupNotify,pOwner空\n" );
//		return false;
//	}
//
//	if ( wPkgLen != sizeof(MGRaceGroupNotify) )
//	{
//		return false;
//	}
//
//	MGRaceGroupNotify* pResp = (MGRaceGroupNotify*)pPkg;
//	
//	GERaceGroupNotify cenMsg;
//	SafeStrCpy( cenMsg.chatContent, pResp->chatContent );
//	SafeStrCpy( cenMsg.chatPlayerName, pResp->chatPlayerName );
//	cenMsg.notifyRace = pResp->notifyRace;
//	cenMsg.chatType = CT_RACE_SYS_EVENT;
//	cenMsg.extInfo = cenMsg.notifyRace;
//	SendMsgToCenterSrv( GERaceGroupNotify, cenMsg );
//
//	return true;
//	TRY_END;
//	return false;
//}

bool CDealMapSrvPkg::OnCheckPkgFreeSpaceRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnCheckPkgFreeSpaceRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGCheckPkgFreeSpace) )
	{
		return false;
	}

	MGCheckPkgFreeSpace *pResp = (MGCheckPkgFreeSpace *)pPkg;
	if( pResp->lNotiPlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnCheckPkgFreeSpaceRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnCheckPkgFreeSpaceRst,找不到通知的玩家\n");
		return false;
	}
	
	if(pResp->result != 0)//出错直接返回
	{
		GCPurchaseItem purchaseItem;
		StructMemSet(purchaseItem, 0x00, sizeof(purchaseItem));
		
		purchaseItem.PurchaseResult = -1;
		purchaseItem.ItemID = pResp->ItemID;
		purchaseItem.ItemNum = pResp->ItemNum;

		NewSendPlayerPkg( GCPurchaseItem, pNotifyPlayer, purchaseItem );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCPurchaseItem, pNotifyPlayer, purchaseItem );
		//pNotifyPlayer->SendPkgToPlayer(pNewMsg);
	}
	else//转发至shopServer进行购买
	{
		// 发送工会信息给shop，shop根据最新的unionid确定星钻消费是否累计
		GSPlayerGuildInfo unionInfo;
		StructMemSet(unionInfo, 0x00, sizeof(unionInfo));

		PlayerID playerid = pNotifyPlayer->GetPlayerID();
		unionInfo.UserId.wGID = playerid.wGID;
		unionInfo.UserId.dwPID = playerid.dwPID;
		unionInfo.GuildNo = pNotifyPlayer->GetUnionID();
		unionInfo.RoleID = pNotifyPlayer->GetSeledRole();
		SendMsgToShopSrv(GSPlayerGuildInfo, unionInfo);

		//发送赠送消息
		GSPurchaseItem  gsPurchse;
		StructMemSet(gsPurchse, 0x00, sizeof(gsPurchse));

		const char *pAccount = pNotifyPlayer->GetAccount();
		size_t accountLen = strlen(pAccount);
		gsPurchse.AccountLen = (BYTE)(accountLen <= sizeof(gsPurchse.AccountLen) ? accountLen : sizeof(gsPurchse.Account));
		SafeStrCpy(gsPurchse.Account, pAccount);//, gsPurchse.AccountLen);
		gsPurchse.RoleID = pNotifyPlayer->GetSeledRole();
		gsPurchse.UserId.dwPID = pNotifyPlayer->GetPlayerID().dwPID;
		gsPurchse.UserId.wGID = pNotifyPlayer->GetPlayerID().wGID;
		gsPurchse.PurchaseType = pResp->PurchaseType;
		gsPurchse.CliCalc = pResp->CliCalc;
		gsPurchse.ItemID = pResp->ItemID;
		gsPurchse.ItemNum = pResp->ItemNum;

		SendMsgToShopSrv(GSPurchaseItem, gsPurchse);
	}

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnFetchVipItemRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnFetchVipItemRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGFetchVipItem) )
	{
		return false;
	}

	MGFetchVipItem *pResp = (MGFetchVipItem *)pPkg;
	if( pResp->lNotiPlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnFetchVipItemRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnFetchVipItemRst,找不到通知的玩家\n");
		return false;
	}

	if(pResp->result != 0)//领取失败,通知shopserver
	{
		GSFetchVipItemResult fetchVipItemRst;
		StructMemSet(fetchVipItemRst, 0x00, sizeof(fetchVipItemRst));

		SafeStrCpy( fetchVipItemRst.Account, pNotifyPlayer->GetAccount() );
		unsigned int tmplen = (unsigned int)(strlen( fetchVipItemRst.Account ) + 1);
		if ( tmplen < 0xff )
		{
			fetchVipItemRst.AccountLen = (unsigned char)tmplen;
		} else {
			D_ERROR( "OnFetchVipItemRst，vipAccount名字超长!\n" );
			fetchVipItemRst.AccountLen = 0;
		}
		fetchVipItemRst.RoleID = pNotifyPlayer->GetRoleID();
		fetchVipItemRst.VipID = pResp->vipID;
		fetchVipItemRst.Result = -1;

		SendMsgToShopSrv(GSFetchVipItemResult, fetchVipItemRst);
	}

	//发给领取结果给客户端
	GCFetchVipItem resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.Result = pResp->result != 0 ? -1 : 0;
	resp.VipID = pResp->vipID;

	NewSendPlayerPkg( GCFetchVipItem, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCFetchVipItem, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnFetchNonVipItemRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "OnFetchNonVipItemRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGFetchNonVipItem) )
	{
		return false;
	}

	MGFetchNonVipItem *pResp = (MGFetchNonVipItem *)pPkg;
	if( pResp->lNotiPlayerID.wGID != CLoadSrvConfig::GetSelfID() )
	{
		D_ERROR("CDealShopSrvPkg::OnFetchNonVipItemRst 发来的gateID 不对\n");
		return false;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pResp->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnFetchNonVipItemRst,找不到通知的玩家\n");
		return false;
	}

	if(pResp->result != 0)//领取失败,通知Functionserver
	{
		GFSendSysMailToPlayer sendMail;
		StructMemSet(sendMail, 0x00, sizeof(sendMail));

		SafeStrCpy(sendMail.recvPlayerName, pNotifyPlayer->GetRoleName());//, sizeof(sendMail.recvPlayerName));
		SafeStrCpy(sendMail.sendMailTitle, "购买物品");//, sizeof(sendMail.sendMailTitle));
		SafeStrCpy(sendMail.sendMailContent, "");//, sizeof(sendMail.sendMailContent));
		sendMail.attachItem[0].ucCount = pResp->itemNum;
		sendMail.attachItem[0].usItemTypeID = pResp->itemID;
		sendMail.silverMoney = 0;
		sendMail.goldMoney = 0;

		SendMsgToFunctionSrv( GFSendSysMailToPlayer, sendMail );
	}

	//发给领取结果给客户端
	GCFetchNonVipItem resp;
	StructMemSet(resp, 0x00, sizeof(resp));

	resp.Result = (pResp->result != 0 ? -1 : 0);
	resp.itemId.TransID = pResp->tranid;
	resp.itemId.ItemID = pResp->itemID;
	resp.itemId.ItemNum = pResp->itemNum;
	resp.itemId.OpeDate = 0;

	NewSendPlayerPkg( GCFetchNonVipItem, pNotifyPlayer, resp );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCFetchNonVipItem, pNotifyPlayer, resp );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnPickMailMoneyRst(CMapSrv* pOwner, const char* pPkg,const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPickMailMoneyRst, pOwer空\n");
		return false;
	}

	if ( wPkgLen != sizeof( MGPickMailMoneyRst ) )
	{
		return false;
	}

	MGPickMailMoneyRst* pPickMoney = ( MGPickMailMoneyRst *)pPkg;

	//如果拾取失败,通知FunctionSrv
	if ( !pPickMoney->isSuss )
	{
		GFPickMailMoneyError pickMoneyRst;
		pickMoneyRst.mailuid = pPickMoney->mailuid;
		pickMoneyRst.money   = pPickMoney->money;
		pickMoneyRst.pickPlayerUID = pPickMoney->pickPlayerUID;
		SendMsgToFunctionSrv( GFPickMailMoneyError, pickMoneyRst );
	}

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pPickMoney->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPickMailMoneyRst,找不到通知的玩家\n");
		return false;
	}

	GCPickMailAttachMoneyRst srvmsg;
	srvmsg.isSucc = pPickMoney->isSuss;
	srvmsg.mailuid = pPickMoney->mailuid;

	NewSendPlayerPkg( GCPickMailAttachMoneyRst, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPickMailAttachMoneyRst, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}

//
//bool CDealMapSrvPkg::OnWorldNotify( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	if ( pOwner == NULL )
//	{
//		D_ERROR("CDealMapSrvPkg::OnWorldNotify, pOwer空\n");
//		return false;
//	}
//
//	if ( wPkgLen != sizeof(MGWorldNotify) )
//	{
//		return false;
//	}
//
//	MGWorldNotify* pNotify = ( MGWorldNotify *)pPkg;
//
//	GESrvGroupChat cenMsg;
//	cenMsg.chatType = CT_SYS_EVENT_NOTI;
//	SafeStrCpy( cenMsg.chatContent,  pNotify->chatContent );
//	SendMsgToCenterSrv( GESrvGroupChat, cenMsg );
//	
//	return true;
//
//	TRY_END;
//	return false;
//}


bool CDealMapSrvPkg::OnReqPlayerDetailInfoRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnReqPlayerDetailInfo, pOwer空\n");
		return false;
	}

	if ( wPkgLen != sizeof(MGReqPlayerDetailInfoRst) )
	{
		return false;
	}

	MGReqPlayerDetailInfoRst* pRst = ( MGReqPlayerDetailInfoRst *)pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("OnReqPlayerDetailInfo 找不到所要转发的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCReqPlayerDetailInfoRst, pPlayer, pRst->reqRst );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCReqPlayerDetailInfoRst, pPlayer, pRst->reqRst );
	//pPlayer->SendPkgToPlayer( pNewMsg );
	
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnQuerySuitInfoRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnQuerySuitInfoRst, pOwer空\n");
		return false;
	}

	if ( wPkgLen != sizeof(MGQuerySuitInfoRst) )
	{
		return false;
	}

	MGQuerySuitInfoRst* pRst = ( MGQuerySuitInfoRst *)pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("OnQuerySuitInfoRst 找不到所要转发的玩家\n");
		return false;
	}
	
	GCQuerySuitInfoRst rst;
	rst.index = pRst->index;
	rst.result = pRst->ret;
	StructMemCpy(rst.suit, &pRst->suit, sizeof(rst.suit));

	NewSendPlayerPkg( GCQuerySuitInfoRst, pPlayer, rst );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCQuerySuitInfoRst, pPlayer, rst );
	//pPlayer->SendPkgToPlayer( pNewMsg );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnClearSuitInfoRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnClearSuitInfoRst, pOwer空\n");
		return false;
	}

	if ( wPkgLen != sizeof(MGClearSuitInfoRst) )
	{
		return false;
	}

	MGClearSuitInfoRst* pRst = ( MGClearSuitInfoRst *)pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("OnClearSuitInfoRst 找不到所要转发的玩家\n");
		return false;
	}

	GCClearSuitInfoRst rst;
	rst.index = pRst->index;
	rst.result = pRst->ret;

	NewSendPlayerPkg( GCClearSuitInfoRst, pPlayer, rst );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCClearSuitInfoRst, pPlayer, rst );
	//pPlayer->SendPkgToPlayer( pNewMsg );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnSaveSuitInfoRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnSaveSuitInfoRst, pOwer空\n");
		return false;
	}

	if ( wPkgLen != sizeof(MGSaveSuitInfoRst) )
	{
		return false;
	}

	MGSaveSuitInfoRst* pRst = ( MGSaveSuitInfoRst *)pPkg;

	CPlayer* pPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("OnSaveSuitInfoRst 找不到所要转发的玩家\n");
		return false;
	}

	GCSaveSuitInfoRst rst;
	rst.index = pRst->index;
	rst.result = pRst->ret;

	NewSendPlayerPkg( GCSaveSuitInfoRst, pPlayer, rst );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCSaveSuitInfoRst, pPlayer, rst );
	//pPlayer->SendPkgToPlayer( pNewMsg );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnChanageSuitRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnChanageSuitRst, pOwer空\n");
		return false;
	}

	if ( wPkgLen != sizeof(MGChangeSuitRst) )
	{
		return false;
	}

	MGChangeSuitRst* pRst = ( MGChangeSuitRst *)pPkg;
	CPlayer* pPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("OnChanageSuitRst 找不到所要转发的玩家\n");
		return false;
	}

	GCChanageSuitRst msg;
	StructMemSet(msg, 0x00, sizeof(msg));

	msg.index = pRst->index;
	msg.repairConfirm = pRst->repairConfirm;
	msg.result = pRst->result;

	NewSendPlayerPkg( GCChanageSuitRst, pPlayer, msg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChanageSuitRst, pPlayer, msg );
	//pPlayer->SendPkgToPlayer( pNewMsg );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnPlayerShowMailBox(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerShowMailBox,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGShowMailBox) != wPkgLen )
	{
		return false;
	}

	MGShowMailBox* pShowMailSrv = ( MGShowMailBox *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShowMailSrv->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPickMailItemRst,找不到通知的玩家\n");
		return false;
	}

	GFQueryMailListByPage srvmsg;
	srvmsg.mailType =  E_SYSYTEM_MAIL;
	srvmsg.pageIndex = 0;
	srvmsg.queryPlayerUID = pNotifyPlayer->GetRoleID();
	srvmsg.queryPlayerID  = pNotifyPlayer->GetPlayerID();
	SendMsgToFunctionSrv( GFQueryMailListByPage,srvmsg );
	return true;

	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnGateUpdateGoodEvilPoint( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerShowMailBox,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGGateUpdateGoodevilpoint) != wPkgLen )
	{
		return false;
	}

	MGGateUpdateGoodevilpoint* pUpdate = ( MGGateUpdateGoodevilpoint *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pUpdate->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnGateUpdateGoodEvilPoint,找不到通知的玩家\n");
		return false;
	}

	pNotifyPlayer->SetGoodEvilPoint( pUpdate->goodevilPoint );
	return true;
	
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnCollectTaskRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerShowMailBox,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGCollectTaskRst) != wPkgLen )
	{
		return false;
	}

	MGCollectTaskRst* pRst = ( MGCollectTaskRst *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnCollectTaskRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCCollectTaskRst, pNotifyPlayer, pRst->collectRst );
	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnShowProtectPlate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShowProtectPlate,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGShowProtectPlate) != wPkgLen )
	{
		return false;
	}

	MGShowProtectPlate* pRst = ( MGShowProtectPlate *)pPkg;
	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnShowProtectPlate,找不到通知的玩家\n");
		return false;
	}

	GCShowProtectItemPlate srvmsg;
	srvmsg.plateType = pRst->plateType;

	NewSendPlayerPkg( GCShowProtectItemPlate, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCShowProtectItemPlate, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;

	TRY_END;
	return false;

}

bool CDealMapSrvPkg::OnPutItemToProtectPlateRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPutItemToProtectPlateRst,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGPutItemToProtectPlateRst) != wPkgLen )
	{
		return false;
	}

	MGPutItemToProtectPlateRst* pRst = ( MGPutItemToProtectPlateRst *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPutItemToProtectPlateRst,找不到通知的玩家\n");
		return false;
	}

	GCPutItemToProtectPlateRst srvmsg;
	srvmsg.addItemRst = pRst->addItemRst;

	NewSendPlayerPkg( GCPutItemToProtectPlateRst, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPutItemToProtectPlateRst, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnUpdateProtectItemProp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnUpdateProtectItemProp,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGSetItemProtectProp) != wPkgLen )
	{
		return false;
	}

	MGSetItemProtectProp* pRst = ( MGSetItemProtectProp *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUpdateProtectItemProp,找不到通知的玩家\n");
		return false;
	}

	GCSetItemProtectProp srvmsg;
	srvmsg.opType = pRst->opType;
	srvmsg.protectNewProp = pRst->protectItem;

	NewSendPlayerPkg( GCSetItemProtectProp, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCSetItemProtectProp, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnReleaseItemSpecialProtectRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnReleaseItemSpecialProtectRst,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGReleaseSpecialPropRst) != wPkgLen )
	{
		return false;
	}

	MGReleaseSpecialPropRst* pRst = ( MGReleaseSpecialPropRst *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnReleaseItemSpecialProtectRst,找不到通知的玩家\n");
		return false;
	}

	GCReleaseSpecialProtectRst srvmsg;
	srvmsg.opRst = pRst->opRst;
	srvmsg.protectItemUID = pRst->itemUID;

	NewSendPlayerPkg( GCReleaseSpecialProtectRst, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCReleaseSpecialProtectRst, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnCancelItemUnSpecialProtectWaitTimeRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnCancelItemUnSpecialProtectWaitTimeRst,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGCancelSpecialPropRst) != wPkgLen )
	{
		return false;
	}

	MGCancelSpecialPropRst* pRst = ( MGCancelSpecialPropRst *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnReleaseItemSpecialProtectRst,找不到通知的玩家\n");
		return false;
	}

	GCCancelSpecialProtectRst srvmsg;
	srvmsg.opRst = pRst->opRst;
	srvmsg.protectItemUID = pRst->itemUID;

	NewSendPlayerPkg( GCCancelSpecialProtectRst, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCCancelSpecialProtectRst, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnRecvItemWithProtectProp(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRecvItemWithProtectProp,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGSendProtectItemWithProp) != wPkgLen )
	{
		return false;
	}

	MGSendProtectItemWithProp* pRst = ( MGSendProtectItemWithProp *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnRecvItemWithProtectProp,找不到通知的玩家\n");
		return false;
	}

	GCRecvItemWithProtectProp srvmsg;
	srvmsg.protectItemCount = pRst->itemCount;
	StructMemCpy( srvmsg.protectItemArr, pRst->protectItemArr, sizeof(srvmsg.protectItemArr) );

	NewSendPlayerPkg( GCRecvItemWithProtectProp, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCRecvItemWithProtectProp, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnConfirmProtectPlateRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnConfirmProtectPlateRst,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGConfirmProtectPlateRst) != wPkgLen )
	{
		return false;
	}

	MGConfirmProtectPlateRst* pRst = ( MGConfirmProtectPlateRst *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnConfirmProtectPlateRst,找不到通知的玩家\n");
		return false;
	}

	GCConfirmProtectPlateRst srvmsg;
	srvmsg.confirmRst = pRst->confirmRst;
	srvmsg.confirmType = pRst->confirmType;

	NewSendPlayerPkg( GCConfirmProtectPlateRst, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCConfirmProtectPlateRst, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnTakeItemFromProtectPlateRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnTakeItemFromProtectPlateRst,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGTakeItemFromProtectPlateRst) != wPkgLen )
	{
		return false;
	}

	MGTakeItemFromProtectPlateRst* pRst =(MGTakeItemFromProtectPlateRst *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnTakeItemFromProtectPlateRst,找不到通知的玩家\n");
		return false;
	}

	GCTaskItemFromProtectPlateRst srvmsg;
	srvmsg.takeItemID = pRst->itemUID;
	srvmsg.takeRst    = pRst->takeRst;

	NewSendPlayerPkg( GCTaskItemFromProtectPlateRst, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCTaskItemFromProtectPlateRst, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer( pNewMsg );

	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnLoadRankChartInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnQueryRankChartInfo,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGLoadRankInfo) != wPkgLen )
	{
		return false;
	}

	MGLoadRankInfo* pRst =(MGLoadRankInfo *)pPkg;

	GFLoadRankChartInfo srvmsg;
	srvmsg.mapsrvID = pOwner->GetID();
	SendMsgToFunctionSrv( GFLoadRankChartInfo, srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnPlayerBaseInfoChange(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerBaseInfoChange,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGUpdatePlayerChangeBaseInfo) != wPkgLen )
	{
		return false;
	}

	MGUpdatePlayerChangeBaseInfo* pRst = ( MGUpdatePlayerChangeBaseInfo *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPlayerBaseInfoChange,找不到通知的玩家\n");
		return false;
	}

	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( pNotifyPlayer->GetAccount() );
	if ( NULL == pDbSrv )
	{
		return false;
	}

	//MsgToPut* pPlayerInfoMsg = CreateSrvPkg( GDUpdatePlayerChangeBaseInfo, pDbSrv, pRst->baseChangeInfo );
	//pDbSrv->SendPkgToSrv( pPlayerInfoMsg );
	SendMsgToSrv( GDUpdatePlayerChangeBaseInfo, pDbSrv, pRst->baseChangeInfo );
	return true;

	TRY_END;

	return false;

}


bool CDealMapSrvPkg::OnUpdatePlayerItemInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnUpdatePlayerItemInfo,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGUpdatePlayerItemInfo) != wPkgLen )
	{
		return false;
	}

	MGUpdatePlayerItemInfo* pRst = ( MGUpdatePlayerItemInfo *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUpdatePlayerItemInfo,找不到通知的玩家\n");
		return false;
	}

	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( pNotifyPlayer->GetAccount() );
	if ( NULL == pDbSrv )
	{
		return false;
	}

	GDUpdateItemInfo srvmsg;
	srvmsg.itemInfo = pRst->itemInfo;
	srvmsg.playerUID = pRst->playerUID;
	srvmsg.isEquip = pRst->isEquip;
	srvmsg.itemIndex = pRst->itemIndex;
	//MsgToPut* pPlayerInfoMsg = CreateSrvPkg( GDUpdateItemInfo, pDbSrv, srvmsg );
	//pDbSrv->SendPkgToSrv( pPlayerInfoMsg );
	SendMsgToSrv( GDUpdateItemInfo, pDbSrv, srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnUpdatePlayerTaskRdInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnUpdatePlayerTaskRdInfo,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGUpdateTaskInfo) != wPkgLen )
	{
		return false;
	}

	MGUpdateTaskInfo* pRst = ( MGUpdateTaskInfo *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUpdatePlayerTaskRdInfo,找不到通知的玩家\n");
		return false;
	}

	CDbSrv* pDbSrv = CManDbSrvs::GetDbsrvByAccount( pNotifyPlayer->GetAccount() );
	if ( NULL == pDbSrv )
	{
		return false;
	}

	GDUpdateTaskRdInfo srvmsg;
	srvmsg.isBackTask = pRst->isBackTask;
	srvmsg.playerUID = pRst->playerUID;
	srvmsg.taskIndex = pRst->taskIndex;
	srvmsg.taskInfo = pRst->taskInfo;
	//MsgToPut* pPlayerInfoMsg = CreateSrvPkg( GDUpdateTaskRdInfo, pDbSrv, srvmsg );
	//pDbSrv->SendPkgToSrv( pPlayerInfoMsg );
	SendMsgToSrv( GDUpdateTaskRdInfo, pDbSrv, srvmsg );

	return true;

	TRY_END;

	return false;
}


bool CDealMapSrvPkg::OnChangeTargetName( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnChangeTargetName,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGChangeTargetName) != wPkgLen )
	{
		return false;
	}

	MGChangeTargetName* pRst = ( MGChangeTargetName *)pPkg;
	if ( pOwner->BroadcastSend< GCChangeTargetName >( &pRst->changeMsg ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealMapSrvPkg::OnChangeTargetName pNotifyPlayer为空\n");
		return false;
	}

	NewSendPlayerPkg( GCChangeTargetName, pNotifyPlayer, pRst->changeMsg );
	return true;
	TRY_END;

	return false;
}


bool CDealMapSrvPkg::OnKickAllPlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	ACE_UNUSED_ARG( pPkg );
	
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnKickAllPlayer,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGKickAllPlayer) != wPkgLen )
	{
		return false;
	}

	CManPlayer::KickAllPlayer();	

	return true;
	TRY_END;

	return false;
}


bool CDealMapSrvPkg::OnNotifyActivityState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;
	
	
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnNotifyActivityState,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGNotifyActivityState) != wPkgLen )
	{
		return false;
	}

	MGNotifyActivityState* pRst = ( MGNotifyActivityState *)pPkg;
	
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealMapSrvPkg::OnNotifyActivityState pNotifyPlayer为空\n");
		return false;
	}

	NewSendPlayerPkg( GCNotifyActivityState, pNotifyPlayer, pRst->gateMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnNotifyNewTaskRecord(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;


	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnNotifyNewTaskRecord,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGNotifyNewTaskRecord) != wPkgLen )
	{
		return false;
	}

	MGNotifyNewTaskRecord* pRst = ( MGNotifyNewTaskRecord *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealMapSrvPkg::OnNotifyNewTaskRecord pNotifyPlayer为空\n");
		return false;
	}

	NewSendPlayerPkg( GCNotifyNewTaskRecord, pNotifyPlayer, pRst->newTaskRecord );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUpdateNewTaskRecordInfo(CMapSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	TRY_BEGIN;


	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnUpdateNewTaskRecordInfo,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGUpdateBackTaskInfo) != wPkgLen )
	{
		return false;
	}

	MGUpdateBackTaskInfo* pRst = ( MGUpdateBackTaskInfo *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealMapSrvPkg::OnUpdateNewTaskRecordInfo pNotifyPlayer为空\n");
		return false;
	}

	NewSendPlayerPkg( GCUpdateNewTaskRecord, pNotifyPlayer, pRst->taskUpdate );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnSendMailToPlayer(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;


	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnSendMailToPlayer,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGSendSysMailToPlayer) != wPkgLen )
	{
		return false;
	}

	MGSendSysMailToPlayer* pRst = ( MGSendSysMailToPlayer *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer)
	{
		D_ERROR("CDealMapSrvPkg::OnSendMailToPlayer pNotifyPlayer为空\n");
		return false;
	}

	SendMsgToFunctionSrv( GFSendSysMailToPlayer, pRst->newSysMail );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnExceptionNotify( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;


	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnExceptionNotify,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGExceptionNotify) != wPkgLen )
	{
		return false;
	}

	MGExceptionNotify* pRst = ( MGExceptionNotify *)pPkg;

	stringstream ss;
	ss <<"MapSrv Id:"  <<pRst->srvId <<"发生异常";

	GESrvGroupChat allChat;
	SafeStrCpy( allChat.chatContent, ss.str().c_str() );
	allChat.chatType = CT_WORLD_CHAT;
	allChat.extInfo = CT_SYS_EVENT_NOTI;//系统事件
	SendMsgToCenterSrv( GESrvGroupChat, allChat );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnSendSpecialString( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;


	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnSendSpecialString,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGSendSpecialString) != wPkgLen )
	{
		return false;
	}

	MGSendSpecialString* pRst = ( MGSendSpecialString *)pPkg;
	if ( pOwner->BroadcastSend< GCSendSpecialString>( &(pRst->stringMsg) ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnSendSpecialString pNotifyPlayer为空\n");
		return false;
	}
	
	
	NewSendPlayerPkg( GCSendSpecialString, pNotifyPlayer, pRst->stringMsg );

	return true;
	TRY_END;
	return false;
}

//Register( M_G_RST_TO_ATKER, &OnRstToAtker );		//使用技能给攻击者的消息
bool CDealMapSrvPkg::OnRstToAtker( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRstToAtker,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGRstToAtker) != wPkgLen )
	{
		return false;
	}

	MGRstToAtker* pRst = ( MGRstToAtker *)pPkg;
	if ( pOwner->BroadcastSend< GCRstToAtker>( &(pRst->gcRstToAtker) ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnRstToAtker pNotifyPlayer为空\n");
		return false;
	}
	
	NewSendPlayerPkg( GCRstToAtker, pNotifyPlayer, pRst->gcRstToAtker );

	return true;
	TRY_END;
	return false;
}

//Register( M_G_NORMAL_ATTACK, &OnNormalAttack );		//使用普通技能给周围人的消息
bool CDealMapSrvPkg::OnNormalAttack( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnNormalAttack,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGNormalAttack) != wPkgLen )
	{
		return false;
	}

	MGNormalAttack* pRst = ( MGNormalAttack *)pPkg;
	if ( pOwner->BroadcastSend<GCNormalAttack>( &(pRst->gcNormalAttack) ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnNormalAttack pNotifyPlayer为空\n");
		return false;
	}
	
	NewSendPlayerPkg( GCNormalAttack, pNotifyPlayer, pRst->gcNormalAttack );
	return true;
	TRY_END;
	return false;
}

//Register( M_G_SPLIT_ATTACK, &OnSplitAttack );		//使用分割技能给周围人的消息
bool CDealMapSrvPkg::OnSplitAttack( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnSplitAttack,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGSplitAttack) != wPkgLen )
	{
		return false;
	}

	MGSplitAttack* pRst = (MGSplitAttack *)pPkg;
	if ( pOwner->BroadcastSend<GCSplitAttack>( &(pRst->gcSplitAttack) ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnSplitAttack pNotifyPlayer为空\n");
		return false;
	}
	
	NewSendPlayerPkg( GCSplitAttack, pNotifyPlayer, pRst->gcSplitAttack );
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUseSkillRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnUseSkillRst,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGUseSkillRst) != wPkgLen )
	{
		return false;
	}

	MGUseSkillRst* pRst = ( MGUseSkillRst *)pPkg;
	if ( pOwner->BroadcastSend< GCUseSkillRst>( &(pRst->skillRst) ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnSendSpecialString pNotifyPlayer为空\n");
		return false;
	}
	
	NewSendPlayerPkg( GCUseSkillRst, pNotifyPlayer, pRst->skillRst );
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRankPlayerRankInfoUpdate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{

	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRankPlayerRankInfoUpdate,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGUpdateRankPlayerRankInfo) != wPkgLen )
	{
		return false;
	}

	MGUpdateRankPlayerRankInfo* pRst = ( MGUpdateRankPlayerRankInfo *)pPkg;

	GMUpdateRankPlayerRankInfo updatemsg;
	updatemsg.rankChartID =  pRst->rankChartID;
	updatemsg.playerUID   =  pRst->playerUID;
	updatemsg.rankInfo    =  pRst->rankInfo;
	updatemsg.playerclass =  pRst->playerclass;
	updatemsg.level       =  pRst->level;
	updatemsg.race        =  pRst->race;
	SafeStrCpy( updatemsg.playerName, pRst->playerName );

	//通知所有的mapsrv
	CManMapSrvs::SendBroadcastMsg<GMUpdateRankPlayerRankInfo>( &updatemsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnRankPlayerLevelUp(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRankPlayerLevelUp,pOwner空\n" );
		return false;
	}

	if ( sizeof(MGUpdateRankPlayerLevel) != wPkgLen )
	{
		return false;
	}

	MGUpdateRankPlayerLevel* pRst = (MGUpdateRankPlayerLevel *)pPkg;

	GFUpdateRankPlayerLevel srvmsg;
	srvmsg.playerUID = pRst->playerUID;
	srvmsg.level     = pRst->level;
	srvmsg.race      = pRst->race;
	SendMsgToFunctionSrv( GFUpdateRankPlayerLevel, srvmsg );

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnPickMailItemRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPickMailItemRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPickMailItemRst) )
	{
		return false;
	}

	MGPickMailItemRst* pPickItem = ( MGPickMailItemRst *)pPkg;

	//如果拾取失败通知FunctionSrv
	if( !pPickItem->isSuss )//如果拾取失败
	{
		GFPickMailItemError pickItemRst;
		pickItemRst.itemInfo = pPickItem->itemInfo;
		pickItemRst.pickPlayerUID = pPickItem->pickPlayerUID;
		pickItemRst.mailuid  = pPickItem->mailuid;
		pickItemRst.pickIndex = pPickItem->itemIndex;
		SendMsgToFunctionSrv( GFPickMailItemError, pickItemRst );
	}

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pPickItem->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPickMailItemRst,找不到通知的玩家\n");
		return false;
	}

	GCPickMailAttachItemRst srvmsg;
	srvmsg.isSucc = pPickItem->isSuss;
	srvmsg.mailuid = pPickItem->mailuid;
	srvmsg.pickIndex = pPickItem->itemIndex;

	NewSendPlayerPkg( GCPickMailAttachItemRst, pNotifyPlayer, srvmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCPickMailAttachItemRst, pNotifyPlayer, srvmsg );
	//pNotifyPlayer->SendPkgToPlayer(pNewMsg);

	return true;

	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnShowStoragePlate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShowStoragePlate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShowStoragePlate) )
	{
		return false;
	}

	MGShowStoragePlate* pShowPlate = ( MGShowStoragePlate *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pShowPlate->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnShowStoragePlate,找不到通知的玩家\n");
		return false;
	}

	GCShowStoragePlate srvmsg;
	NewSendPlayerPkg( GCShowStoragePlate , pNotifyPlayer, srvmsg );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnStorageSafeInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnStorageSafeInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGStorageSafeInfo) )
	{
		return false;
	}

	MGStorageSafeInfo* pStorageInfo = ( MGStorageSafeInfo *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pStorageInfo->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnStorageSafeInfo,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCStorageSafeInfo , pNotifyPlayer, pStorageInfo->safeInfo );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnStorageItemInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnStorageItemInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGStorageItemInfo) )
	{
		return false;
	}

	MGStorageItemInfo* pStorageItemInfo = ( MGStorageItemInfo *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pStorageItemInfo->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnStorageItemInfo,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCStorageItemInfo , pNotifyPlayer, pStorageItemInfo->storageItemInfo );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnShowStoragePassWordDlg(CMapSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShowStoragePassWordDlg,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShowStoragePasswordDlg) )
	{
		return false;
	}

	MGShowStoragePasswordDlg* pStorageDlg = ( MGShowStoragePasswordDlg *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pStorageDlg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnShowStoragePassWordDlg,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCShowStoragePasswordDlg , pNotifyPlayer, pStorageDlg->showPsdDlg );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnChangeStorageLockState(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnChangeStorageLockState,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGChangeStorageLockState) )
	{
		return false;
	}

	MGChangeStorageLockState* pChangeLockState = ( MGChangeStorageLockState *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pChangeLockState->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnChangeStorageLockState,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCChangeStorageLockState , pNotifyPlayer, pChangeLockState->changeStorageState );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUpdateStorageValidRow(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnUpdateStorageValidRow,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUpdateStorageValidRow) )
	{
		return false;
	}

	MGUpdateStorageValidRow* pStorageValidRow = ( MGUpdateStorageValidRow *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pStorageValidRow->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUpdateStorageValidRow,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCUpdateStorageValidRow , pNotifyPlayer, pStorageValidRow->updateValidRow );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUpdateStorageSecurityStrategy(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnUpdateStorageSecurityStrategy,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUpdateStorageSecurityStrategy) )
	{
		return false;
	}

	MGUpdateStorageSecurityStrategy* pSecurityMode = ( MGUpdateStorageSecurityStrategy *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pSecurityMode->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUpdateStorageSecurityStrategy,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCUpdateStorageSecurityStrategy , pNotifyPlayer, pSecurityMode->securityMode );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUpdateStorageItemInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnUpdateStorageItemInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUpdateStorageItemInfoByIndex) )
	{
		return false;
	}

	MGUpdateStorageItemInfoByIndex* pUpdateItemInfo = ( MGUpdateStorageItemInfoByIndex *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pUpdateItemInfo->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnUpdateStorageItemInfo,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCUpdateStorageItemInfoByIndex , pNotifyPlayer, pUpdateItemInfo->updateStorageItem );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnTakeItemFromStorageRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnTakeItemFromStorageRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGTakeItemFromStorageError) )
	{
		return false;
	}

	MGTakeItemFromStorageError* pTakeItemError = ( MGTakeItemFromStorageError *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pTakeItemError->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnTakeItemFromStorageRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCTakeItemFromStorageError , pNotifyPlayer, pTakeItemError->takeItemError );

	return true;

	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnPutItemToStorageRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPutItemToStorageRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPutItemToStorageError) )
	{
		return false;
	}

	MGPutItemToStorageError* pPutItemError = ( MGPutItemToStorageError *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pPutItemError->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnPutItemToStorageRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCPutItemToStorageError , pNotifyPlayer, pPutItemError->putItemError );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnSetNewPassWordRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnSetNewPassWordRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGSetNewPassWordError) )
	{
		return false;
	}

	MGSetNewPassWordError* pSetPsdRst = ( MGSetNewPassWordError *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pSetPsdRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnSetNewPassWordRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCSetNewPassWordError , pNotifyPlayer, pSetPsdRst->setNewPsdError );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnResetStoragePsdRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnResetStoragePsdRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGResetPassWordError) )
	{
		return false;
	}

	MGResetPassWordError* pReSetPsdRst = ( MGResetPassWordError *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pReSetPsdRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnResetStoragePsdRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCResetPassWordError , pNotifyPlayer, pReSetPsdRst->resetPsdError );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnTakeItemCheckPsdRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnTakeItemCheckPsdRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGTakeItemCheckPsdError) )
	{
		return false;
	}

	MGTakeItemCheckPsdError* pTakeItemCheckRst = ( MGTakeItemCheckPsdError *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pTakeItemCheckRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnTakeItemCheckPsdRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCTakeItemCheckPsdError , pNotifyPlayer, pTakeItemCheckRst->checkPsdError );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnExtendStorageRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnExtendStorageRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGExtendStorageError) )
	{
		return false;
	}

	MGExtendStorageError* pExtendStorageRst = ( MGExtendStorageError *)pPkg;

	//通知client结果
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pExtendStorageRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnExtendStorageRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCExtendStorageError , pNotifyPlayer, pExtendStorageRst->extendError );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnGMQueryResp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnGMQueryResp,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGGMQueryResp) )
	{
		return false;
	}

	MGGMQueryResp* pRst = ( MGGMQueryResp *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnGMQueryResp,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GTCmd_Query_Rsp , pNotifyPlayer, pRst->resp );

	return true;

	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnNotifyRookieState( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnNotifyRookieState,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGNotifyRookieState) )
	{
		return false;
	}

	MGNotifyRookieState* pRst = ( MGNotifyRookieState *)pPkg;
		
	if ( pOwner->BroadcastSend< GCNotifyRookieState >( (&pRst->rookieMsg) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnGMQueryResp,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCNotifyRookieState , pNotifyPlayer, pRst->rookieMsg );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnItemNpcStatusInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnItemNpcStatusInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGItemNpcStatusInfo) )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealMapSrvPkg::OnItemNpcStatusInfo, NULL == pPkg\n" );
		return false;
	}

	MGItemNpcStatusInfo* pRst = ( MGItemNpcStatusInfo *)pPkg;
#ifdef _DEBUG 
	D_INFO("收到MGItemNpcStatusInfo 消息 共有%d Status\n", pRst->itemNpc.buildNpcCount );
	for( unsigned i = 0; i < pRst->itemNpc.buildNpcCount; ++i )
	{
		if ( i >= ARRAY_SIZE(pRst->itemNpc.buildNpcArr) )
		{
			D_ERROR( "CDealMapSrvPkg::OnItemNpcStatusInfo, i(%d) >= ARRAY_SIZE(pRst->itemNpc.buildNpcArr)\n", i );
			return false;
		}
		D_INFO("第%d条 : ItemSeq:%d ItemNpcStatus:%d\n", i, pRst->itemNpc.buildNpcArr[i].buildSeq, pRst->itemNpc.buildNpcArr[i].curStatus );
	}
#endif //_DEBUG

	if ( pOwner->BroadcastSend< GCBuildNpcStatusInfo>( &(pRst->itemNpc) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnGMQueryResp,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBuildNpcStatusInfo , pNotifyPlayer, pRst->itemNpc );

	return true;

	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnItemNpcID( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnItemNpcID,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGItemNpcID) )
	{
		return false;
	}

	MGItemNpcID* pRst = ( MGItemNpcID *)pPkg;
	if ( pOwner->BroadcastSend< GCBuildNpcID>( &(pRst->idInfo) ) )
	{
		//已群发，不再单独发送；
		return true;
	}
	

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnGMQueryResp,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBuildNpcID , pNotifyPlayer, pRst->idInfo );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnBatchSellItemsResp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnBatchSellItemsResp,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGBatchSellItemsResp) )
	{
		return false;
	}

	MGBatchSellItemsResp* pRst = ( MGBatchSellItemsResp *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnBatchSellItemsResp,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBatchSellItemsResp , pNotifyPlayer, pRst->resp );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRepurchaseItemResp( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRepurchaseItemResp,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGRepurchaseItemResp) )
	{
		return false;
	}

	MGRepurchaseItemResp* pRst = ( MGRepurchaseItemResp *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnRepurchaseItemResp,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCRepurchaseItemResp , pNotifyPlayer, pRst->resp );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnCloseOLTestDlg(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnCloseOLTestDlg,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGCloseOLTestPlate) )
	{
		return false;
	}

	MGCloseOLTestPlate* pRst =( MGCloseOLTestPlate *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnCloseOLTestDlg,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCCloseOLTestDlg, pNotifyPlayer, pRst->closeTestDlg );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnShowOLTestProvueDlg(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShowOLTestProvueDlg,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGShowProvueTestDlg) )
	{
		return false;
	}

	MGShowProvueTestDlg* pRst = (MGShowProvueTestDlg *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnShowOLTestProvueDlg,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCShowPrevueOLTestDlg,pNotifyPlayer, pRst->showPrevueDlg );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnShowOLTestQuestion(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShowOLTestQuestion,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGShowOLTestQuestion) )
	{
		return false;
	}

	MGShowOLTestQuestion* pRst =(MGShowOLTestQuestion *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnShowOLTestQuestion,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCShowOLTestQuestion, pNotifyPlayer, pRst->showQuestion );
	return  true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnAnswerQuestionRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnAnswerQuestionRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGAnswerQuestionRst) )
	{
		return false;
	}

	MGAnswerQuestionRst* pRst = (MGAnswerQuestionRst *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnAnswerQuestionRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCAnswerQuestionRst, pNotifyPlayer, pRst->answerQuestionRst );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnAnswerAllQuestion(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnAnswerAllQuestion,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGShowAnswerAllQuestionsDlg) )
	{
		return false;
	}

	MGShowAnswerAllQuestionsDlg* pRst = (MGShowAnswerAllQuestionsDlg *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnAnswerAllQuestion,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCShowAnswerAllQuestionsDlg , pNotifyPlayer, pRst->answerAllDlg );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnSplitItemError(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnSplitItemError,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGSplitItemError) )
	{
		return false;
	}

	MGSplitItemError* pRst = (MGSplitItemError *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnSplitItemError,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCSplitItemError , pNotifyPlayer, pRst->errMsg );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnRepurchaseItemListNtf( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRepurchaseItemListNtf,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGRepurchaseItemListNtf) )
	{
		return false;
	}

	MGRepurchaseItemListNtf* pRst = ( MGRepurchaseItemListNtf *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnRepurchaseItemListNtf,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCRepurchaseItemsListNtf , pNotifyPlayer, pRst->ntf );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnLogPlayerInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnLogPlayerInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGLogPlayerInfo) )
	{
		return false;
	}

	MGLogPlayerInfo* pRst = (MGLogPlayerInfo *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnLogPlayerInfo,找不到通知的玩家\n");
		return false;
	}

	switch(pRst->logID)
	{
		case MGLogPlayerInfo::ROLE_LEVEL_UP:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORoleLevelUP, pRst->logInfo.levelUp);
		}
		break;

		case MGLogPlayerInfo::SKILL_LEVEL_UP:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOSkillLevelUP, pRst->logInfo.skillLevelUp);
		}
		break;

		case MGLogPlayerInfo::ROLE_GET_ITEM:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORoleItemOwn, pRst->logInfo.getItem);
		}
		break;

		case MGLogPlayerInfo::ROLE_USE_ITEM:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORoleItemUse, pRst->logInfo.useItem);
		}
		break;

		case MGLogPlayerInfo::ROLE_DROP_ITEM:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORoleItemDrop, pRst->logInfo.dropItem);
		}
		break;

		case MGLogPlayerInfo::ROLE_TASK_CHANAGE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORoleQuestChange, pRst->logInfo.taskChanage);
		}
		break;

		case MGLogPlayerInfo::ROLE_DEAD:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORoleDead, pRst->logInfo.playerDead);
		}
		break;

		case MGLogPlayerInfo::ROLE_MAP_CHANAGE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORoleMapChange, pRst->logInfo.mapChanage);
		}
		break;

		case MGLogPlayerInfo::ROLE_TRADE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORoleTrade, pRst->logInfo.trade);
		}
		break;

		case MGLogPlayerInfo::ROLE_SP_POWER_CHANGE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOSPChange, pRst->logInfo.spChange);
		}
		break;

		case MGLogPlayerInfo::ROLE_ITEM_PROP_CHANGE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOItemPropertyChange, pRst->logInfo.itemChange);
		}
		break;

		case MGLogPlayerInfo::ROLE_MONEY_CHANGE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOMoneyChange, pRst->logInfo.moneyChange);
		}
		break;

		case MGLogPlayerInfo::ROLE_PET_ADD:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOPetAdd, pRst->logInfo.petAdd);
		}
		break;

		case MGLogPlayerInfo::ROLE_PET_LEVEL_UP:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOPetLevelup, pRst->logInfo.petLevelUp);
		}
		break;

		case MGLogPlayerInfo::ROLE_PET_GAIN_SKILL:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOPetFunctionAdd, pRst->logInfo.petAddSkill);
		}
		break;

		case MGLogPlayerInfo::ROLE_PET_STATE_CHANGE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOPetStateChange, pRst->logInfo.petStateChange);
		}
		break;

		case MGLogPlayerInfo::ROLE_USE_SKILL:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOUseSkill, pRst->logInfo.useskill);
		}
		break;

		case MGLogPlayerInfo::ROLE_KILL_PLAYER:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOKillPlayer, pRst->logInfo.killPlayer);
		}
		break;

		case MGLogPlayerInfo::ROLE_REBIRTH:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORelive, pRst->logInfo.rebirth);
		}
		break;

		case MGLogPlayerInfo::ROLE_REPAIR_ITEM:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOItemRepair, pRst->logInfo.repairItem);
		}
		break;

		case MGLogPlayerInfo::MONSTER_DIE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOMonsterDead, pRst->logInfo.monsterDead);
		}
		break;

		case MGLogPlayerInfo::INSTANCE_CHANGE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GORealInstanceLifeCycle, pRst->logInfo.instanceChange);
		}
		break;

		case MGLogPlayerInfo::NPC_SHOP_TRADE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOShopTrade, pRst->logInfo.npcShop);
		}
		break;	

		case MGLogPlayerInfo::WAREHOUSE_OPEN:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOWarehouseOpen, pRst->logInfo.warehouseOpen);
		}
		break;

		case MGLogPlayerInfo::WAREHOUSE_ITEM_OPERATE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOWarehouseItemTransport, pRst->logInfo.warehouseItemOperate);
		}
		break;

		case MGLogPlayerInfo::WAREHOUSE_MONEY_OPERATE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOWarehouseMoneyTransport, pRst->logInfo.warehouseMoneyOperate);
		}
		break;

		case MGLogPlayerInfo::WAREHOUSE_EXTEND:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOWarehouseExtend, pRst->logInfo.warehouseExtend);
		}
		break;

		case MGLogPlayerInfo::WAREHOUSE_PRIVATE:
		{
			NewSendMsgToLogSrv(LOG_SVC_NS::GOWarehousePrivateOP, pRst->logInfo.warehousePrivate);
		}
		break;

	default:
		break;
	}

	return true;

	TRY_END;

	return false;
}

///玩家试图进副本；
bool CDealMapSrvPkg::OnPlayerTryCopyQuest( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerTryCopyQuest,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerTryCopyQuest) )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerTryCopyQuest,wPkgLen != sizeof(MGPlayerTryCopyQuest)\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerTryCopyQuest,NULL == pPkg\n" );
		return false;
	}

	MGPlayerTryCopyQuest* pRst = (MGPlayerTryCopyQuest *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerTryCopyQuest,找不到通知的玩家\n");
		return false;
	}

	pRst->enterCopyReq.teamID = pNotifyPlayer->GetPlayerCopyFlag();

	if ( pRst->isPCopy )
	{
		//进伪副本请求，直接发进副本指令；
		pNotifyPlayer->OnPlayerEnterPCopyCmd( pRst->enterCopyReq.scrollType, pRst->enterCopyReq.copyMapID );
	} else {
		//进真副本请求，转发center;
		GEPlayerTryCopyQuest playerTryCopy;
		playerTryCopy.enterCopyReq = pRst->enterCopyReq;

		//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
		//if ( NULL == pCenterSrv )
		//{
		//	D_WARNING( "CDealPlayerPkg::OnPlayerTryCopyQuest找不到CenterSrv\n" );
		//	return false;
		//}

		//MsgToPut* pNewMsg = CreateSrvPkg( GEPlayerTryCopyQuest, pCenterSrv, playerTryCopy );
		//pCenterSrv->SendPkgToSrv( pNewMsg );
		SendMsgToCenterSrv( GEPlayerTryCopyQuest, playerTryCopy );
	}

	return true;
	TRY_END;
	return false;
}

///玩家试图离开副本请求；
bool CDealMapSrvPkg::OnPlayerLeaveCopyQuest( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerLeaveCopyQuest,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerLeaveCopyQuest) )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerLeaveCopyQuest,wPkgLen != sizeof(MGPlayerTryCopyQuest)\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerLeaveCopyQuest,NULL == pPkg\n" );
		return false;
	}

	MGPlayerLeaveCopyQuest* pMGMsg = (MGPlayerLeaveCopyQuest *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pMGMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerLeaveCopyQuest,找不到通知的玩家\n");
		return false;
	}

	if ( !(pNotifyPlayer->IsCurInCopy()) )
	{
		D_ERROR( "收到MGPlayerLeaveCopyQuest时，发现玩家%s不在副本中\n", pNotifyPlayer->GetAccount() );
		return false;
	}

	if ( pNotifyPlayer->IsCurPCopy() )
	{
		//若玩家当前在伪副本中，则直接发离开命令；
		pNotifyPlayer->OnPlayerLeavePCopyCmd();
	} else {
		//玩家当前可能是在真副本中，向centersrv转发；
		pNotifyPlayer->NotiCenterPlayerLeaveCopyQuest( pMGMsg->leaveCopyInfo.copyID, pMGMsg->leaveCopyInfo.enterReq.copyMapID, pNotifyPlayer->GetAccount() );
	}

	return true;
	TRY_END;
	return false;
}

///副本不可再进指令；
bool CDealMapSrvPkg::OnCopyNomorePlayer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnCopyNomorePlayer,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGCopyNomorePlayer) )
	{
		D_ERROR("CDealMapSrvPkg::OnCopyNomorePlayer,wPkgLen != sizeof(MGCopyNomorePlayer)\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealMapSrvPkg::OnCopyNomorePlayer,NULL == pPkg\n" );
		return false;
	}

	MGCopyNomorePlayer* pMGMsg = (MGCopyNomorePlayer *)pPkg;

	GECopyNomorePlayer copyNomorePlayer;
	copyNomorePlayer.copyid = pMGMsg->copyid;

	//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
	//if ( NULL == pCenterSrv )
	//{
	//	D_WARNING( "CDealPlayerPkg::OnCopyNomorePlayer找不到CenterSrv\n" );
	//	return false;
	//}

	//MsgToPut* pNewMsg = CreateSrvPkg( GECopyNomorePlayer, pCenterSrv, copyNomorePlayer );
	//pCenterSrv->SendPkgToSrv( pNewMsg );
	SendMsgToCenterSrv( GECopyNomorePlayer, copyNomorePlayer );

	return true;
	TRY_END;
	return false;
}

///发往center的删除副本请求(在center发往mapsrv的删除副本请求之后，因为mapsrv收到那个请求后会先踢玩家，踢完后再发此G_E_DEL_COPY_QUEST给center，最后center发delcopy cmd给map)；	
bool CDealMapSrvPkg::OnDelCopyQuest( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnDelCopyQuest,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGDelCopyQuest) )
	{
		D_ERROR("CDealMapSrvPkg::OnDelCopyQuest,wPkgLen != sizeof(MGDelCopyQuest)\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealMapSrvPkg::OnDelCopyQuest,NULL == pPkg\n" );
		return false;
	}

	MGDelCopyQuest* pMGMsg = (MGDelCopyQuest *)pPkg;

	GEDelCopyQuest copyNomorePlayer;
	copyNomorePlayer.copyid = pMGMsg->copyid;

	//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
	//if ( NULL == pCenterSrv )
	//{
	//	D_WARNING( "CDealPlayerPkg::OnDelCopyQuest找不到CenterSrv\n" );
	//	return false;
	//}

	//MsgToPut* pNewMsg = CreateSrvPkg( GEDelCopyQuest, pCenterSrv, copyNomorePlayer );
	//pCenterSrv->SendPkgToSrv( pNewMsg );
	SendMsgToCenterSrv( GEDelCopyQuest, copyNomorePlayer );

	return true;
	TRY_END;
	return false;
}

///mapsrv玩家的outmapinfo改变；
bool CDealMapSrvPkg::OnPlayerOutMapInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerOutMapInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerOutMapInfo) )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerOutMapInfo,wPkgLen != sizeof(MGPlayerOutMapInfo)\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerOutMapInfo,NULL == pPkg\n" );
		return false;
	}

	MGPlayerOutMapInfo* pMGMsg = (MGPlayerOutMapInfo *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pMGMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerOutMapInfo,找不到通知的玩家\n");
		return false;
	}

	pNotifyPlayer->OnSetOutMapInfo( pMGMsg->mapID, pMGMsg->posX, pMGMsg->posY );

	return true;
	TRY_END;
	return false;
}

///mapsrv对玩家查询伪副本信息的反馈；	
bool CDealMapSrvPkg::OnMGGlobeCopyInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnMGGlobeCopyInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGGlobeCopyInfos) )
	{
		D_ERROR("CDealMapSrvPkg::OnMGGlobeCopyInfo,wPkgLen != sizeof(MGPlayerOutMapInfo)\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealMapSrvPkg::OnMGGlobeCopyInfo,NULL == pPkg\n" );
		return false;
	}

	MGGlobeCopyInfos* pMGMsg = (MGGlobeCopyInfos *)pPkg;

	if ( pOwner->BroadcastSend< GCGlobeCopyInfos >( &(pMGMsg->globeCopyInfos) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pMGMsg->notiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnMGGlobeCopyInfo,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCGlobeCopyInfos, pNotifyPlayer, pMGMsg->globeCopyInfos );

	return true;
	TRY_END;
	return false;
}

///Register( M_G_TIMELIMIT, &OnMGTimeLimit );//mapsrv发来玩家倒计时框；
bool CDealMapSrvPkg::OnMGTimeLimit( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnMGTimeLimit,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGTimeLimit) )
	{
		D_ERROR("CDealMapSrvPkg::OnMGTimeLimit,wPkgLen != sizeof(MGPlayerOutMapInfo)\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR("CDealMapSrvPkg::OnMGTimeLimit,NULL == pPkg\n" );
		return false;
	}

	MGTimeLimit* pMGMsg = (MGTimeLimit *)pPkg;

	if ( pOwner->BroadcastSend< GCTimeLimit >( &(pMGMsg->limitTime) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pMGMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnMGGlobeCopyInfo,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCTimeLimit, pNotifyPlayer, pMGMsg->limitTime );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnPlayerWarDying( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerWarDying,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGPlayerWarDying) )
	{
		return false;
	}

	MGPlayerWarDying* pWarDying = ( MGPlayerWarDying *)pPkg;

	if ( pOwner->BroadcastSend< GCPlayerWarDying >( &(pWarDying->dying) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pWarDying->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerWarDying,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCPlayerWarDying , pNotifyPlayer, pWarDying->dying);

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnWarRebirthOptionErr( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnWarRebirthOptionErr,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof( MGWarRebirthOptionErr) )
	{
		return false;
	}

	MGWarRebirthOptionErr* pRebirthErr = ( MGWarRebirthOptionErr *)pPkg;

	if ( pOwner->BroadcastSend< GCSelectWarRebirthOptionErr >( &(pRebirthErr->warRebirthError) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRebirthErr->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnWarRebirthOptionErr,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCSelectWarRebirthOptionErr , pNotifyPlayer, pRebirthErr->warRebirthError);

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnRaceMasterTimeInfoExpire(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterTimeInfoExpire,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRaceMasterTimeInfoExpire) )
	{
		return false;
	}

	MGRaceMasterTimeInfoExpire* pRaceTimeInfoExpire = ( MGRaceMasterTimeInfoExpire *)pPkg;

	GMRaceMasterTimeInfoExpire srvmsg;
	StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
	srvmsg.raceType = pRaceTimeInfoExpire->racetype;

	//通知所有的mapsrv
	CManMapSrvs::SendBroadcastMsg<GMRaceMasterTimeInfoExpire>( &srvmsg );

	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnRaceMasterDeclareWarInfoChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterDeclareWarInfoChange,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRaceMasterDeclareWarInfoChange) )
	{
		return false;
	}

	MGRaceMasterDeclareWarInfoChange* pdeclarewarinfo = (MGRaceMasterDeclareWarInfoChange *)pPkg;

	GMRaceMasterDeclareInfoChange srvmsg;
	srvmsg.declarewarInfo = pdeclarewarinfo->declarewarinfo;
	srvmsg.raceType       = pdeclarewarinfo->racetype;
	//通知所有的mapsrv
	CManMapSrvs::SendBroadcastMsg<GMRaceMasterDeclareInfoChange>( &srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnRaceMasterTimeInfoChange(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterTimeInfoChange,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRaceMasterTimeExpireInfoChange) )
	{
		return false;
	}

	MGRaceMasterTimeExpireInfoChange* ptimeinfochange = (MGRaceMasterTimeExpireInfoChange *)pPkg;

	GMRaceMasterTimeInfoChange srvmsg;
	StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
	srvmsg.isGotSalary = ptimeinfochange->isGotSalary;
	srvmsg.lastupdatetime = ptimeinfochange->lastupdatetime;
	srvmsg.noticeCount  = ptimeinfochange->noticeCount;
	srvmsg.raceType  = ptimeinfochange->racetype;
	//通知所有的mapsrv
	CManMapSrvs::SendBroadcastMsg<GMRaceMasterTimeInfoChange>( &srvmsg );

	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnRaceMasterPublicMsgChange(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterPublicMsgChange,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGNoticeRaceMasterPublicMsgChange) )
	{
		return false;
	}

	MGNoticeRaceMasterPublicMsgChange* pChangeMsg = ( MGNoticeRaceMasterPublicMsgChange *)pPkg;

	GMOnRaceMasterSetNewPublicMsg srvmsg;
	StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );

	srvmsg.raceType = pChangeMsg->racetype;
	SafeStrCpy( srvmsg.msgArr, pChangeMsg->msgArr );
	//通知所有的mapsrv
	CManMapSrvs::SendBroadcastMsg<GMOnRaceMasterSetNewPublicMsg>( &srvmsg );

	return true;

	TRY_END;

	return false;
}

//bool CDealMapSrvPkg::OnNoticeRaceMasterPublicMsg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	TRY_BEGIN;
//
//	if ( pOwner == NULL )
//	{
//		D_ERROR("CDealMapSrvPkg::OnNoticeRaceMasterPublicMsg,pOwner空\n" );
//		return false;
//	}
//
//	if ( wPkgLen != sizeof(MGNoticeRaceMasterPublicMsg) )
//	{
//		return false;
//	}
//
//	MGNoticeRaceMasterPublicMsg* pPublicMsg = ( MGNoticeRaceMasterPublicMsg *)pPkg;
//
//	if ( pOwner->BroadcastSend< GCRaceMasterNoticesNewMsg >( &(pPublicMsg->newmsg) ) )
//	{
//		//已群发，不再单独发送；
//		return true;
//	}
//
//	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pPublicMsg->lNotiPlayerID.dwPID );
//	if( NULL == pNotifyPlayer )
//	{
//		D_ERROR("CDealMapSrvPkg::OnNoticeRaceMasterPublicMsg,找不到通知的玩家\n");
//		return false;
//	}
//
//	NewSendPlayerPkg( GCRaceMasterNoticesNewMsg,pNotifyPlayer, pPublicMsg->newmsg );
//
//	return true;
//	TRY_END;
//
//	return false;
//}

bool CDealMapSrvPkg::OnSaveRaceMasterInfoToDB(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnSaveRaceMasterInfoToDB,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUpdateRaceMasterInfoToDB) )
	{
		return false;
	}

	MGUpdateRaceMasterInfoToDB* pSaveInfo = (MGUpdateRaceMasterInfoToDB *)pPkg;

	GFUpdateRaceMasterInfo srvmsg;
	srvmsg.updateInfo = *pSaveInfo;
	SendMsgToFunctionSrv( GFUpdateRaceMasterInfo,srvmsg  );

	return true;
	TRY_END;
	
	return false;
}

bool CDealMapSrvPkg::OnRaceMasterDeclareWarRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterDeclareWarRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRaceMasterDeclareWarRst) )
	{
		return false;
	}

	MGRaceMasterDeclareWarRst* pDecalreWarInfo = (MGRaceMasterDeclareWarRst *)pPkg;

	GCRaceMasterDeclareWarRst srvmsg;
	srvmsg.result = pDecalreWarInfo->result;
	srvmsg.newDeclareWarInfo = pDecalreWarInfo->declarewarinfo;

	if ( pOwner->BroadcastSend< GCRaceMasterDeclareWarRst >( &(srvmsg) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pDecalreWarInfo->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterDeclareWarRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCRaceMasterDeclareWarRst, pNotifyPlayer, srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnRaceMasterGetSalaryRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterGetSalaryRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRaceMasterGetSalaryRst) )
	{
		return false;
	}

	MGRaceMasterGetSalaryRst* pgetsalary = ( MGRaceMasterGetSalaryRst *)pPkg;

	GCNoticeRaceMasterGetSalaryRst srvmsg;
	srvmsg.silverMoney = pgetsalary->silverMoney;
	srvmsg.goldMoney = pgetsalary->goldMoney;
	srvmsg.result = pgetsalary->result;

	if ( pOwner->BroadcastSend< GCNoticeRaceMasterGetSalaryRst >( &(srvmsg) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pgetsalary->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterGetSalaryRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCNoticeRaceMasterGetSalaryRst,pNotifyPlayer, srvmsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnRaceMasterSetNewPublicMsgRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{

	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterSetNewPublicMsgRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRaceMasterSetNewMsgRst) )
	{
		return false;
	}

	MGRaceMasterSetNewMsgRst* psetnewmsgRst = ( MGRaceMasterSetNewMsgRst *)pPkg;

	GCAddNewRaceMasterNoticeRst srvmsg;
	srvmsg.result = psetnewmsgRst->result;

	if ( pOwner->BroadcastSend< GCAddNewRaceMasterNoticeRst >( &(srvmsg) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  psetnewmsgRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnRaceMasterSetNewPublicMsgRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCAddNewRaceMasterNoticeRst,pNotifyPlayer, srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnShowRaceMasterPlate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShowRaceMasterPlate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShowRaceMasterOPPlate) )
	{
		return false;
	}

	MGShowRaceMasterOPPlate* pshowplate = ( MGShowRaceMasterOPPlate *)pPkg;

	if ( pOwner->BroadcastSend< GCShowRaceMasterOpPlate >( &(pshowplate->showplate) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pshowplate->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnShowRaceMasterPlate,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCShowRaceMasterOpPlate, pNotifyPlayer,  pshowplate->showplate );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnShowRaceMasterDeclareWarPlate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShowRaceMasterDeclareWarPlate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShowRaceMasterDeclareWarPlate) )
	{
		return false;
	}

	MGShowRaceMasterDeclareWarPlate* pdeclarewarplate = ( MGShowRaceMasterDeclareWarPlate *)pPkg;

	if ( pOwner->BroadcastSend< GCShowRaceMasterDeclareWarPlate >( &(pdeclarewarplate->showwarplate) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pdeclarewarplate->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnShowRaceMasterDeclareWarPlate,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCShowRaceMasterDeclareWarPlate , pNotifyPlayer, pdeclarewarplate->showwarplate );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnShowRaceMasterPublicMsgPlate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShowRaceMasterPublicMsgPlate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShowRaceMasterPublicMsgPlate) )
	{
		return false;
	}

	MGShowRaceMasterPublicMsgPlate* ppublicmsg = (MGShowRaceMasterPublicMsgPlate *)pPkg;

	if ( pOwner->BroadcastSend< GCShowRaceMasterNoticeMsgPlate >( &(ppublicmsg->showmsgplate) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  ppublicmsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnShowRaceMasterPublicMsgPlate,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCShowRaceMasterNoticeMsgPlate , pNotifyPlayer, ppublicmsg->showmsgplate );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnCloseRaceMasterPlate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnCloseRaceMasterPlate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGCloseRaceMasterPlate) )
	{
		return false;
	}

	MGCloseRaceMasterPlate* pcloseplate = (MGCloseRaceMasterPlate *)pPkg;

	if ( pOwner->BroadcastSend< GCRaceMasterClosePlate >( &(pcloseplate->closeplate) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pcloseplate->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnCloseRaceMasterPlate,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCRaceMasterClosePlate, pNotifyPlayer, pcloseplate->closeplate );

	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnMapSrvLoadRaceMasterInfo(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnMapSrvLoadRaceMasterInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGLoadRaceMasterInfoFromDB) )
	{
		return false;
	}

	MGLoadRaceMasterInfoFromDB* ploadmsg = (MGLoadRaceMasterInfoFromDB *)pPkg;
	
	GFQueryRaceMasterInfo srvmsg;
	SendMsgToFunctionSrv( GFQueryRaceMasterInfo, srvmsg );

	return true;
	TRY_END;
	return  true;
}

bool CDealMapSrvPkg::OnResetRaceMasterDeclareWar(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnResetRaceMasterDeclareWar,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGResetRaceMasterDeclareWarInfo) )
	{
		return false;
	}

	MGResetRaceMasterDeclareWarInfo* psrvmsg = ( MGResetRaceMasterDeclareWarInfo *)pPkg;

	GFResetRaceMasterDeclareWarInfo srvmsg;
	srvmsg.racetype = psrvmsg->racetype;
	SendMsgToFunctionSrv( GFResetRaceMasterDeclareWarInfo, srvmsg );

	return true;
	TRY_END;
	return true;
}

bool CDealMapSrvPkg::OnShowRaceMasterAssignCityPlate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShowRaceMasterAssignCityPlate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShowRaceMasterAssignCityPlate) )
	{
		return false;
	}

	MGShowRaceMasterAssignCityPlate* pAssignPlate = (MGShowRaceMasterAssignCityPlate *)pPkg;

	if ( pOwner->BroadcastSend< GCShowRaceMasterAssignCityPlate >( &(pAssignPlate->assignPlate) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pAssignPlate->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::MGShowRaceMasterAssignCityPlate,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCShowRaceMasterAssignCityPlate, pNotifyPlayer, pAssignPlate->assignPlate );

	return true;
	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnBuffStart( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnBuffStart,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGBuffStart) )
	{
		return false;
	}

	MGBuffStart* pBs = (MGBuffStart *)pPkg;
	if ( pOwner->BroadcastSend< GCBuffStart >( &pBs->buffStart ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pBs->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::MGShowRaceMasterAssignCityPlate,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBuffStart  , pNotifyPlayer ,pBs->buffStart );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnBuffEnd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnBuffEnd,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGBuffEnd) )
	{
		return false;
	}

	MGBuffEnd* pBe = (MGBuffEnd *)pPkg;
	if ( pOwner->BroadcastSend< GCBuffEnd >( &pBe->buffEnd ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pBe->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnBuffEnd,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBuffEnd  , pNotifyPlayer ,pBe->buffEnd );
	return true;

	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnBuffStatusUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnBuffStatusUpdate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGBuffStatusUpdate) )
	{
		return false;
	}

	MGBuffStatusUpdate* pBc = (MGBuffStatusUpdate *)pPkg;
	if ( pOwner->BroadcastSend< GCBuffStatusUpdate >( &pBc->buffChange ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pBc->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnBuffStatusUpdate,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBuffStatusUpdate  , pNotifyPlayer ,pBc->buffChange );
	return true;

	TRY_END;
	return false;
}


bool CDealMapSrvPkg::OnBuffHpMpUpdate( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnBuffHpMpUpdate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGBuffHpMpUpdate) )
	{
		return false;
	}

	MGBuffHpMpUpdate* pHmUpdate = (MGBuffHpMpUpdate *)pPkg;
	if ( pOwner->BroadcastSend< GCBuffHpMpUpdate >( &pHmUpdate->hpmpUpdate ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pHmUpdate->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnBuffHpMpUpdate,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBuffHpMpUpdate  , pNotifyPlayer , pHmUpdate->hpmpUpdate );
	return true;

	TRY_END;
	return false;
}


//显示timer
bool CDealMapSrvPkg::OnDisplayWarTimer( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnDisplayWarTimer,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGDisplayWarTimer) )
	{
		return false;
	}

	MGDisplayWarTimer* pDpWt = (MGDisplayWarTimer*)(pPkg);
	if ( pOwner->BroadcastSend< GCDisplayWarTimer >( &pDpWt->gcTimer ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pDpWt->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnDisplayWarTimer cannot find pid:%d\n", pDpWt->lNotiPlayerID.dwPID );
		return false;
	}
	
	NewSendPlayerPkg( GCDisplayWarTimer  , pNotifyPlayer , pDpWt->gcTimer );
	return true;

	TRY_END;
	return false;
}



//显示counter
bool CDealMapSrvPkg::OnDisplayWarCounter( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnDisplayWarCounter,pOwner空\n" );
		return false;
	}

	if( wPkgLen != sizeof(MGDisplayWarCounter) )
	{
		return false;
	}

	MGDisplayWarCounter* pDpWc = (MGDisplayWarCounter*)(pPkg);
	if ( pOwner->BroadcastSend< GCDisplayWarCounter >( &pDpWc->gcCounter ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pDpWc->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnDisplayWarTimer cannot find pid:%d\n", pDpWc->lNotiPlayerID.dwPID );
		return false;
	}

	NewSendPlayerPkg( GCDisplayWarCounter, pNotifyPlayer , pDpWc->gcCounter );
	return true;

	TRY_END;
	return false;
}


//角色反弹
bool CDealMapSrvPkg::OnCharacterDamageRebound( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnCharacterDamageRebound,pOwner空\n" );
		return false;
	}

	if( wPkgLen != sizeof(MGCharacterDamageRebound) )
	{
		return false;
	}

	MGCharacterDamageRebound* pDamReb = (MGCharacterDamageRebound*)(pPkg);
	if ( pOwner->BroadcastSend< GCCharacterDamageRebound >( &(pDamReb->gcRebound) ) )
	{
		//已群发，不再单独发送；
		return true;
	}



	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer( pDamReb->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("OnDisplayWarTimer cannot find pid:%d\n", pDamReb->lNotiPlayerID.dwPID );
		return false;
	}

	NewSendPlayerPkg( GCCharacterDamageRebound, pNotifyPlayer , pDamReb->gcRebound );
	return true;

	TRY_END;
	return false;
}



bool CDealMapSrvPkg::OnPublicNoticeModify( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_MAP_MSG_CHECK( MGPublicNoticeModify )
	
    if ( pOwner->BroadcastSend< GCGMModifyPublicNotice >( &pMsg->modifynotice ) )
    {
	    //已群发，不再单独发送；
	    return true;
	}

    CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
    if( NULL == pNotifyPlayer )
    {
	    D_ERROR("CDealMapSrvPkg::OnPublicNoticeModify,找不到通知的玩家\n");
	    return false;
	}
	
	NewSendPlayerPkg( GCGMModifyPublicNotice, pNotifyPlayer, pMsg->modifynotice );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnPublicNoticeAdd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_MAP_MSG_CHECK( MGPublicNoticeAdd );

    if ( pOwner->BroadcastSend< GCGMAddNewPublicNotice >( &pMsg->addnotice ) )
    {
	   //已群发，不再单独发送；
	   return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPublicNoticeAdd,找不到通知的玩家\n");
		return false;
	}
	NewSendPlayerPkg( GCGMAddNewPublicNotice, pNotifyPlayer, pMsg->addnotice );
	return true;

	TRY_END;

	return false;
}


bool CDealMapSrvPkg::OnPublicNoticeRemove( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	TRY_BEGIN;

	DEAL_MAP_MSG_CHECK( MGPublicNoticeRemove );

	if ( pOwner->BroadcastSend< GCGMRemovePublicNotice >( &pMsg->removenotice ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPublicNoticeRemove,找不到通知的玩家\n");
		return false;
	}
	NewSendPlayerPkg( GCGMRemovePublicNotice, pNotifyPlayer, pMsg->removenotice );
	return true;

	TRY_END;

	return false;
}


bool CDealMapSrvPkg::OnPublicMarqueueAdd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_MAP_MSG_CHECK( MGMarqueueNoticeAdd );

	if ( pOwner->BroadcastSend< GCRecvNewMarqueueNotice >( &pMsg->marqueuenotice ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPublicMarqueueAdd,找不到通知的玩家\n");
		return false;
	}
	NewSendPlayerPkg( GCRecvNewMarqueueNotice, pNotifyPlayer, pMsg->marqueuenotice );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnParamMsgAdd( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_MAP_MSG_CHECK( MGSendParamNotice );

	if ( pOwner->BroadcastSend< GCRecvParamMsgNotice >( &pMsg->parammsgnotice ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	if ( pMsg->isgloabl == 1 )//是否需要发送给所有的服务器
	{
		GMBroadCastParamNotice srvmsg;
		StructMemCpy( srvmsg.paramntoice, pMsg, sizeof(srvmsg.paramntoice) );
		CManMapSrvs::SendBroadcastMsg<GMBroadCastParamNotice>( &srvmsg );
	}
	else
	{
		CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
		if( NULL == pNotifyPlayer )
		{
			D_ERROR("CDealMapSrvPkg::OnParamMsgAdd,找不到通知的玩家\n");
			return false;
		}
		NewSendPlayerPkg( GCRecvParamMsgNotice, pNotifyPlayer, pMsg->parammsgnotice );
		return true;
	}

	TRY_END;

	return false;
}


bool CDealMapSrvPkg::OnRecvShowTempNotice( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_MAP_MSG_CHECK( MGShowTmpNoticeByPosition );

	if ( pOwner->BroadcastSend< GCShowTempNoticeByPosition >( &pMsg->showtmpnotice ) )
	{
		//已群发，不再单独发送；
		return true;
	}

   if ( pMsg->isbroadcast == 1 )//如果需要广播给服务器的人，那么通知所有的服务器
   {
	   GMBroadCastTempNotice srvmsg;
	   StructMemCpy( srvmsg.tempnotice, pMsg, sizeof(srvmsg.tempnotice));
	   CManMapSrvs::SendBroadcastMsg<GMBroadCastTempNotice>( &srvmsg );
   }
   else
   {
	   CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	   if( NULL == pNotifyPlayer )
	   {
		   D_ERROR("CDealMapSrvPkg::OnRecvShowTempNotice,找不到通知的玩家\n");
		   return false;
	   }
	   NewSendPlayerPkg( GCShowTempNoticeByPosition, pNotifyPlayer, pMsg->showtmpnotice );
	   return true;
   }

   TRY_END;
   return false;
}

bool CDealMapSrvPkg::OnBlockQuadStateChange( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnBlockQuadStateChange,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGBlockQuadStateChange) )
	{
		return false;
	}

	MGBlockQuadStateChange* pRst = ( MGBlockQuadStateChange *)pPkg;
	if ( pOwner->BroadcastSend< GCBlockQuadStateChange>( &(pRst->stateChange) ) )
	{
		//已群发，不再单独发送；
		return true;
	}

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnBlockQuadStateChange,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBlockQuadStateChange , pNotifyPlayer, pRst->stateChange );

	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnStartSelectRaceMaster(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnStartSelectRaceMaster,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGStartSelectRaceMaster) )
	{
		return false;
	}

	GFSelectRaceMaster selectracemaster;
	SendMsgToFunctionSrv( GFSelectRaceMaster,selectracemaster );
	return true;

	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnForwardUnionCmdRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnForwardUnionCmdRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGForwardUnionCmdRst) )
	{
		return false;
	}

	TRY_BEGIN;

	MGForwardUnionCmdRst* pRst = ( MGForwardUnionCmdRst *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnForwardUnionCmdRst,找不到通知的玩家\n");
		return false;
	}

	if(!pRst->flag)
	{
		return false;
	}

	switch(pRst->cmdID)
	{
	case (GCUnionUISelectCmd::CREATE_UNION) :
		{
			CreateUnionRequest srvmsg;
			StructMemSet(srvmsg, 0x00, sizeof(srvmsg));
			if(pRst->createUnion.nameLen < sizeof(srvmsg._union.name))
			{
				StructMemCpy( srvmsg._union.name, pRst->createUnion.name, pRst->createUnion.nameLen );
				srvmsg._union.nameLen = pRst->createUnion.nameLen;
			}
			else
			{
				StructMemCpy( srvmsg._union.name, pRst->createUnion.name, sizeof(srvmsg._union.name) );
				srvmsg._union.nameLen = sizeof(srvmsg._union.name);
			}

			srvmsg._union.ownerUiid = pNotifyPlayer->GetSeledRole();
			SendMsgToRelationSrv(CreateUnionRequest, srvmsg);
			break;
		}

	case (GCUnionUISelectCmd::ADVANCE_UNION_LEVEL) :
		{
			AdvanceUnionLevelRequest srvmsg;
			StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

			srvmsg.uiid = pNotifyPlayer->GetSeledRole();
			SendMsgToRelationSrv(AdvanceUnionLevelRequest, srvmsg);
			break;
		}

	case (GCUnionUISelectCmd::ISSUE_UNION_TASK) :
		{
			PostUnionTasksListRequest climsg;
			StructMemSet(climsg, 0x00, sizeof(climsg));

			climsg.durationTime = pRst->issueTask.taskValidInterval;
			climsg.uiid = pNotifyPlayer->GetSeledRole();

			SendMsgToRelationSrv(PostUnionTasksListRequest, climsg);
			break;
		}

	case (GCUnionUISelectCmd::LEARN_UNION_SKILL) :
		{
			StudyUnionSkillRequest srvmsg;
			StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

			srvmsg.uiid = pNotifyPlayer->GetSeledRole();
			srvmsg.skillId = pRst->learnUnionSkill.skillID;

			SendMsgToRelationSrv(StudyUnionSkillRequest, srvmsg);
			break;
		}

	case (GCUnionUISelectCmd::DESIGN_UNION_BADGE) :
		{
			ModifyUnionBadgeRequest srvmsg;
			StructMemSet(srvmsg, 0x00, sizeof(srvmsg));

			srvmsg.uiid = pNotifyPlayer->GetSeledRole();
			SafeStrCpy(srvmsg.badgeData, pRst->modifyUnionBadge.badgeData);
			srvmsg.dataLen = (UINT)strlen(srvmsg.badgeData);

			SendMsgToRelationSrv(ModifyUnionBadgeRequest, srvmsg);
			break;
		}
	}
	
	return true;

	TRY_END;
	return false;
}

///返回角色选择
bool CDealMapSrvPkg::OnReturnSelRole(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	DEAL_MAP_MSG_CHECK( MGReturnSelRole );


	CPlayer* pPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnReturnSelRole,找不到通知的玩家\n");
		return false;
	}
	pPlayer->OnPlayerReturnSelRole();
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnUnionCostArg( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnUnionCostArg,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGUnionCostArg) )
	{
		return false;
	}

	TRY_BEGIN;

	MGUnionCostArg* pRst = ( MGUnionCostArg *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnUnionCostArg,找不到通知的玩家\n");
		return false;
	}

	s_unionCostArg.money = pRst->arg.money;
	s_unionCostArg.itemID = pRst->arg.itemID;
	s_unionCostArg.itemCount = pRst->arg.itemCount;
	return true;
	TRY_END;
	return false;
}

bool CDealMapSrvPkg::OnPlayerStartNewAuctionRst(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerStartNewAuctionRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerStartNewAuctionRst) )
	{
		return false;
	}

	TRY_BEGIN;

	MGPlayerStartNewAuctionRst* pRst = (MGPlayerStartNewAuctionRst* )pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerStartNewAuctionRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg( GCBeginNewAuctionRst , pNotifyPlayer, pRst->newAuctionRst );
	return true;

	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnPlayerStartNewAuctionNext(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerStartNewAuctionNext,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerStartNewAuctionNext) )
	{
		return false;
	}

	TRY_BEGIN;

	MGPlayerStartNewAuctionNext* pRst = (MGPlayerStartNewAuctionNext *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerStartNewAuctionNext,找不到通知的玩家\n");
		return false;
	}

	SendMsgToFunctionSrv( GFPlayerStartNewAuction, pRst->startNewAuctionItem );
	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnPlayerBidAuctionItemNext(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerBidAuctionItemNext,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerBidItemNext) )
	{
		return false;
	}

	TRY_BEGIN;

	MGPlayerBidItemNext* pRst = (MGPlayerBidItemNext *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerBidAuctionItemNext,找不到通知的玩家\n");
		return false;
	}

	SendMsgToFunctionSrv( GFPlayerBidItemSection2, pRst->bidItemSection2 );
	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnPlayerBidItemErr(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerBidItemErr,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGPlayerBidItemErr) )
	{
		return false;
	}

	TRY_BEGIN;

	MGPlayerBidItemErr* pRst = ( MGPlayerBidItemErr *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerBidItemErr,找不到通知的玩家\n");
		return false;
	}

	GCBidAuctionItemRst climsg;
	climsg.auctionUID = pRst->auctionUID;
	climsg.rstNo      = pRst->rstNo;
	NewSendPlayerPkg( GCBidAuctionItemRst , pNotifyPlayer, climsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnPlayerShowAuctionPlate(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerShowAuctionPlate,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShowAuctionPlate) )
	{
		return false;
	}

	TRY_BEGIN;

	MGShowAuctionPlate* pRst = ( MGShowAuctionPlate *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnPlayerShowAuctionPlate,找不到通知的玩家\n");
		return false;
	}

	GCShowAuctionPlate  climsg;
	NewSendPlayerPkg( GCShowAuctionPlate , pNotifyPlayer, climsg );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnReportPWInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnReportPWInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGReportPWInfos) )
	{
		return false;
	}

	MGReportPWInfos* pRst = ( MGReportPWInfos *)pPkg;
	if(pRst->count > 0)
	{
		//记录信息
		CPseudowireManagerSingle::instance()->OnRecvPWInfo((PseudowireArg_i *)&pRst->pwList[0], pRst->count);
		
		//转发给centerserver
		MGReportPWInfos forward;
		forward.count = 0;
		for(unsigned char i = 0; i < pRst->count; i++)
		{
			if ( ( forward.count >= ARRAY_SIZE(forward.pwList) )
				|| ( i >= ARRAY_SIZE(pRst->pwList) )
				)
			{
				D_ERROR( "CDealMapSrvPkg::OnReportPWInfo, forward.count(%d)或i(%d)越界\n", forward.count, i );
				return false;
			}
			StructMemCpy( forward.pwList[forward.count], &pRst->pwList[i], sizeof(forward.pwList[0]) );
			forward.count++;

			if ( forward.count == ARRAY_SIZE(forward.pwList) )//sizeof(forward.pwList)/sizeof(forward.pwList[0]))
			{
				SendMsgToCenterSrv(MGReportPWInfos, forward);
				forward.count = 0;
			}
		}

		if ( forward.count > 0 )
		{
			SendMsgToCenterSrv(MGReportPWInfos, forward);
		}
	}

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnReportPWMapInfo( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnReportPWMapInfo,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGReportPWMapInfos) )
	{
		return false;
	}

	MGReportPWMapInfos* pRst = ( MGReportPWMapInfos *)pPkg;
	if(pRst->count > 0)
	{
		//记录信息
		CPseudowireManagerSingle::instance()->OnRecvPWMapInfo(pRst->pwMapList, pRst->count);

		//转发给centerserver
		MGReportPWMapInfos forward;
		forward.count = 0;
		for(unsigned char i = 0; i < pRst->count; i++)
		{
			if ( ( forward.count >= ARRAY_SIZE(forward.pwMapList) )
				|| ( i >= ARRAY_SIZE(pRst->pwMapList) )
				)
			{
				D_ERROR( "CDealMapSrvPkg::OnReportPWMapInfo, forward.count(%d)或i(%d)越界\n", forward.count, i );
				return false;
			}
			StructMemCpy( forward.pwMapList[forward.count], &pRst->pwMapList[i], sizeof(forward.pwMapList[0]) );

			forward.count++;
			if ( forward.count == ARRAY_SIZE(forward.pwMapList) )//sizeof(forward.pwMapList)/sizeof(forward.pwMapList[0]))
			{
				SendMsgToCenterSrv(MGReportPWMapInfos, forward);
				forward.count = 0;
			}
		}

		if(forward.count > 0)
			SendMsgToCenterSrv(MGReportPWMapInfos, forward);
	}

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnModPWStateForward( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnModPWStateForward,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGModifyPWStateForward) )
	{
		return false;
	}

	MGModifyPWStateForward* pRst = ( MGModifyPWStateForward *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnModPWStateForward,找不到通知的玩家\n");
		return false;
	}

	GEModifyPWStateReq modifyReq;
	modifyReq.pwid = pRst->pwId;
	modifyReq.newState = pRst->newState;
	SendMsgToCenterSrv( GEModifyPWStateReq, modifyReq );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnModPWMapStateForward( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnModPWMapStateForward,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGModifyPWMapStateForward) )
	{
		return false;
	}

	MGModifyPWMapStateForward* pRst = ( MGModifyPWMapStateForward *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnModPWMapStateForward,找不到通知的玩家\n");
		return false;
	}

	GEModifyPWMapStateReq modifyReq;
	modifyReq.pwid = pRst->pwId;
	modifyReq.pwMapid = pRst->pwMapId;
	modifyReq.newState = (unsigned char)pRst->newState;
	SendMsgToCenterSrv( GEModifyPWMapStateReq, modifyReq );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnShiftPWForward( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnShiftPWForward,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGShiftPWReqForward) )
	{
		return false;
	}

	MGShiftPWReqForward* pRst = ( MGShiftPWReqForward *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnShiftPWForward,找不到通知的玩家\n");
		return false;
	}

	GEEnterPWMapCheck enterCheck;
	enterCheck.isVip = false;/*pNotifyPlayer->IsVip();*/
	enterCheck.lNoticePlayerID = pNotifyPlayer->GetPlayerID();
	enterCheck.pwid = pRst->targetPWId;
	enterCheck.pwMapid = 0;/*玩家当前的伪线地图*/
	SendMsgToCenterSrv( GEEnterPWMapCheck, enterCheck );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnKickOffPWForward( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnKickOffPWForward,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGKickOffPWForward) )
	{
		return false;
	}

	MGKickOffPWForward* pRst = ( MGKickOffPWForward *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnKickOffPWForward,找不到通知的玩家\n");
		return false;
	}

	GEEnterPWMapCheck enterCheck;
	enterCheck.isVip = false;/*pNotifyPlayer->IsVip();*/
	enterCheck.lNoticePlayerID = pNotifyPlayer->GetPlayerID();
	enterCheck.pwid = pRst->targetPWId;
	enterCheck.pwMapid = 0;/*玩家当前的伪线地图*/
	SendMsgToCenterSrv( GEEnterPWMapCheck, enterCheck );

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnModEffectiveSkillIDRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnModEffectiveSkillIDRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGModifyEffectSkillIDRst) )
	{
		return false;
	}

	MGModifyEffectSkillIDRst* pRst = ( MGModifyEffectSkillIDRst *)pPkg;

	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pRst->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnModEffectiveSkillIDRst,找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg(GCModifyEffectiveSkillIDRst, pNotifyPlayer, pRst->rst);	

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnAddFriendDegreeReq( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnAddFriendDegreeReq,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGAddFriendlyDegreeReq) )
	{
		return false;
	}

	TRY_BEGIN;

	MGAddFriendlyDegreeReq* pMsg = (MGAddFriendlyDegreeReq *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnAddFriendDegreeReq, 找不到通知的玩家\n");
		return false;
	}

	AddFriendlyDegree farward;
	farward.gateId = pNotifyPlayer->GetPlayerID().wGID;
	farward.playerId = pNotifyPlayer->GetPlayerID().dwPID;
	farward.friendUiid = pMsg->friendUiid;
	farward.degreeAdded = pMsg->degreeAdded;

	SendMsgToRelationSrv(AddFriendlyDegree, farward);

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnAddTweetNtf( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnAddTweetNtf,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGAddTweetNtf) )
	{
		return false;
	}

	TRY_BEGIN;

	MGAddTweetNtf* pMsg = (MGAddTweetNtf *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnAddTweetNtf, 找不到通知的玩家\n");
		return false;
	}

	AddTweet farward;
	farward.gateId = pNotifyPlayer->GetPlayerID().wGID;
	farward.playerId = pNotifyPlayer->GetPlayerID().dwPID;
	farward.tweet.type = pMsg->tweet.type;
	farward.tweet.createdTime = pMsg->tweet.createdTime;
	farward.tweet.tweetLen = pMsg->tweet.tweetLen;
	SafeStrCpy(farward.tweet.tweet, pMsg->tweet.tweet);

	if(farward.tweet.tweetLen > ARRAY_SIZE(farward.tweet.tweet))
	{
		farward.tweet.tweetLen = ARRAY_SIZE(farward.tweet.tweet);
	}

	SendMsgToRelationSrv(AddTweet, farward);

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnFrinedNonTeamExpNtf( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnFrinedNonTeamExpNtf,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGReportFriendNonTeamGain) )
	{
		return false;
	}

	TRY_BEGIN;

	MGReportFriendNonTeamGain* pMsg = (MGReportFriendNonTeamGain *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnFrinedNonTeamExpNtf, 找不到通知的玩家\n");
		return false;
	}

	NotifyExpAdded farward;
	farward.gateId = pNotifyPlayer->GetPlayerID().wGID;
	farward.playerId = pNotifyPlayer->GetPlayerID().dwPID;
	farward.isBoss = pMsg->isBoss;
	farward.exp = pMsg->exp;

	SendMsgToRelationSrv(NotifyExpAdded, farward);

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnExpOfLevelUpRst( CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnExpOfLevelUpRst,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGQueryExpNeededWhenLvlUpResult) )
	{
		return false;
	}

	TRY_BEGIN;

	MGQueryExpNeededWhenLvlUpResult* pMsg = (MGQueryExpNeededWhenLvlUpResult *)pPkg;

	QueryExpNeededWhenLvlUpResult farward;
	if(ARRAY_SIZE(farward.lst) != ARRAY_SIZE(pMsg->lst))
	{
		D_ERROR("CDealMapSrvPkg::OnExpOfLevelUpRst,ARRAY_SIZE(farward.lst) != ARRAY_SIZE(pMsg->lst)\n" );
		return false;
	}

	farward.career = pMsg->career;
	farward.lvlFirst = pMsg->lvlFirst;
	farward.lvlLast = pMsg->lvlLast;
	StructMemCpy(farward.lst, pMsg->lst, sizeof(farward.lst));
	farward.lstLen = pMsg->lstLen;

	if(farward.lstLen > ARRAY_SIZE(farward.lst))
		farward.lstLen = ARRAY_SIZE(farward.lst);

	SendMsgToRelationSrv(QueryExpNeededWhenLvlUpResult, farward);

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnInviteCallMember(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnInviteCallMember,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGInviteCallMember) )
	{
		return false;
	}

	TRY_BEGIN;

	MGInviteCallMember* pMsg = (MGInviteCallMember *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnInviteCallMember, 找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg(GCInviteCallMember, pNotifyPlayer,pMsg->inviteCallMember);

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnStartCast(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnStartCast,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGStartCast) )
	{
		return false;
	}

	TRY_BEGIN;

	MGStartCast* pMsg = (MGStartCast *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnStartCast, 找不到通知的玩家\n");
		return false;
	}

	NewSendPlayerPkg(GCStartCast, pNotifyPlayer,pMsg->req);

	return true;
	TRY_END;

	return false;
}

bool CDealMapSrvPkg::OnStopCast(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnStopCast,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGStopCast) )
	{
		return false;
	}

	TRY_BEGIN;

	MGStopCast* pMsg = (MGStopCast *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::OnStopCast, 找不到通知的玩家\n");
		return false;
	}

	GCStopCast msg;
	msg.playerID = pMsg->lNotiPlayerID;

	NewSendPlayerPkg(GCStopCast, pNotifyPlayer,msg);

	return true;
	TRY_END;

	return false;
}

//玩家移动失败
bool CDealMapSrvPkg::OnRunFail(CMapSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( pOwner == NULL )
	{
		D_ERROR("CDealMapSrvPkg::OnRunFail,pOwner空\n" );
		return false;
	}

	if ( wPkgLen != sizeof(MGRunFail) )
	{
		return false;
	}

	TRY_BEGIN;

	MGRunFail* pMsg = (MGRunFail *)pPkg;
	CPlayer* pNotifyPlayer = CManPlayer::FindPlayer(  pMsg->lNotiPlayerID.dwPID );
	if( NULL == pNotifyPlayer )
	{
		D_ERROR("CDealMapSrvPkg::MGRunFail, 找不到通知的玩家\n");
		return false;
	}

	GCRunFail msg;
	msg = pMsg->rst;

	NewSendPlayerPkg(GCRunFail, pNotifyPlayer,msg);

	return true;
	TRY_END;

	return false;
}
