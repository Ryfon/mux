﻿#ifndef DEAL_SHOP_SRV_PKG_H
#define DEAL_SHOP_SRV_PKG_H


#include "../../../Base/PkgProc/SrvProtocol.h"
#include "../../../Base/PkgProc/DealPkgBase.h"


class CShopSrv;
class CLogSrv;

/// ShopSrv的消息处理类
class CDealShopSrvPkg : public IDealPkg<CShopSrv>
{
private:
	static ShopServer::ShopSrvProtocol m_ShopProtocol;
private:
	CDealShopSrvPkg() {};
	virtual ~CDealShopSrvPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();
public:
	/// 帐户信息
	static bool OnAccountInfo( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 购买物品
	static bool OnPurchaseItem( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///vip信息
	static bool OnVipInfo( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 购买vip
	static bool OnPurchaseVip( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 领取vip物品
	static bool OnFetchVipItem( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 赠送
	static bool OnPurchaseGift( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 领取非vip物品
	static bool OnFetchNonVipItem( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 检查赠送目标是否存在
	static bool OnCheckRoleByName(  CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	/// 查询排行榜的信息从shop返回
	static bool OnQueryShopRankRst( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//Register( S_G_NEW_RANKRST, &OnNewRankRst );//新的星钻持有排行；
	static bool OnNewRankRst( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///星钻累计转为工会威望通知
	static bool OnDiamondToPrestigeNotify( CShopSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

private:
	static bool ConvertShopRankInfoToCliRankInfo( RankChartItem& clirankinfo, const RankInfo& shoprankinfo );
};

/// LogSrv的消息处理类
class CDealLogSrvPkg : public IDealPkg<CLogSrv>
{
private:
	static LoginServiceNS::LoginServiceProtocol m_LogProtocol;
private:
	CDealLogSrvPkg() {};
	virtual ~CDealLogSrvPkg() {};

public:
	///初始化(注册各命令字对应的处理函数);
	static void Init();
public:

};

#endif/*DEAL_SHOP_SRV_PKG_H*/
