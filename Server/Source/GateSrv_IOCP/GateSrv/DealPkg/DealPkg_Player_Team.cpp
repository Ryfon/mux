﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: dzj
* 完成日期: 2007.12.17
*
*/

#include "../../../Base/Utility.h"
#include "../../../Base/LoadConfig/LoadSrvConfig.h"
#include "DealPkg_Map.h"

#include "../Player/Player.h"
#include "../OtherServer/otherserver.h"
#include "../AddictionPlayerList.h"
#include "../PseudowireManager.h"

#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4819 ) //"type cast" pointer truncation from ??* to ??
#endif //ACE_WIN32
#include "../../../Base/PkgProc/RelationServiceProtocol.h"
#ifdef ACE_WIN32
#pragma warning( pop )
#endif //ACE_WIN32

#include "../../../Base/PkgProc/ParserTool.h"

#include "../mapteleport/MapTeleporterManager.h"


#include "../toolmonitor/toolmonitor.h"
#include "../LogManager.h"


////////////////////////////////////////////////处理组队///////////////////////////////////////
bool CDealPlayerPkg::OnInvitePlayerJoinTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner)
	{
		D_ERROR( "OnInvitePlayerJoinTeam,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGInvitePlayerJoinTeam, pCliMsg, pPkg, wPkgLen );

	//组员不能组队
	if( pOwner->IsInTeam() )
	{
		if( !pOwner->IsTeamCaptain() )
		{
			GCInvitePlayerJoinTeam errmsg;
			SafeStrCpy( errmsg.playerName, pCliMsg->playerName );
			errmsg.nameSize = (int)strlen( errmsg.playerName );
			errmsg.nErrorCode = PLAYER_REFUSE;
			NewSendPlayerPkg( GCInvitePlayerJoinTeam, pOwner, errmsg );
			//MsgToPut* pNewMsg = CreatePlayerPkg( GCInvitePlayerJoinTeam, pOwner, errmsg );
			//pOwner->SendPkgToPlayer( pNewMsg );
			return false;
		}
	}  

	if(  !pOwner->GetTeamSwitch() )
	{
		GCInvitePlayerJoinTeam errmsg;
		SafeStrCpy( errmsg.playerName, pCliMsg->playerName );
		errmsg.nameSize = (int)strlen( errmsg.playerName );
		errmsg.nErrorCode = PLAYER_REFUSE;
		NewSendPlayerPkg( GCInvitePlayerJoinTeam, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCInvitePlayerJoinTeam, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		return true;
	}

	if( strcmp(pCliMsg->playerName, pOwner->GetRoleName() ) == 0)
	{
		D_ERROR("不能组自己\n");
		return false;
	}


	GEInvitePlayerJoinTeam srvmsg;
	SafeStrCpy( srvmsg.beInvitedPlayerName, pCliMsg->playerName );
	srvmsg.beInvitedPlayerNameLen = pCliMsg->nameSize;
	srvmsg.inviterInfo.invitePlayerRace = pOwner->GetRace();
	srvmsg.inviterInfo.invitePlayerID = pOwner->GetPlayerID();
	SafeStrCpy( srvmsg.inviterInfo.invitePlayerName, pOwner->GetRoleName() );
	srvmsg.inviterInfo.isEvil = pOwner->IsEvil();
	srvmsg.inviterInfo.invitePlayerNameLen = (UINT)strlen( srvmsg.inviterInfo.invitePlayerName );

	//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
	//if ( NULL == pCenterSrv )
	//{
	//	D_WARNING( "玩家组队后，找不到CenterSrv\n" );
	//	return false;
	//}
	//MsgToPut* pNewMsg = CreateSrvPkg( GEInvitePlayerJoinTeam, pCenterSrv, srvmsg );
	//pCenterSrv->SendPkgToSrv( pNewMsg );
	SendMsgToCenterSrv( GEInvitePlayerJoinTeam, srvmsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnInviteTeamFeedBack( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnInviteTeamFeedBack,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGRecvInviteJoinTeamFeedBack, pClimsg, pPkg, wPkgLen );

	//看看申请组队列表里有没有该ID
	if( !pOwner->IsInTeamInviteList( pClimsg->invitePlayerID )  )
	{
		D_ERROR("%s在列表里没有申请列表却发来应答入队消息\n", pOwner->GetRoleName() );
		return false;
	}

	if ( pOwner->IsInTeam() )
	{
		//防止玩家加入多支队伍;
		//1.不理会被邀请者;
		//2.GEERROR通知邀请者,???;
		GEInviteError errMsg;
		SafeStrCpy( errMsg.beInvitedPlayerName, pOwner->GetRoleName() );
		errMsg.beInvitedPlayerNameLen = (UINT)strlen( errMsg.beInvitedPlayerName ) + 1;
		errMsg.invitePlayerID = pClimsg->invitePlayerID;
		errMsg.nErrorCode = PLAYER_HAS_TEAM;
		//CCenterSrv* pSrv = CManCenterSrvs::GetCenterserver();
		//if(NULL == pSrv)
		//{
		//	D_ERROR("找不到CenterSrv\n");
		//	return false;
		//}
		//MsgToPut* pNewMsg = CreateSrvPkg( GEInviteError, pSrv,  errMsg );
		//pSrv->SendPkgToSrv( pNewMsg );
		SendMsgToCenterSrv( GEInviteError, errMsg );
		return false;
	}

	if( pClimsg->ucAnswer == CGRecvInviteJoinTeamFeedBack::NO )  //NO的话发error消息,失败
	{
		GEInviteError errmsg;
		SafeStrCpy( errmsg.beInvitedPlayerName, pOwner->GetRoleName() );
		errmsg.beInvitedPlayerNameLen = (UINT)strlen( errmsg.beInvitedPlayerName );
		errmsg.invitePlayerID = pClimsg->invitePlayerID;
		errmsg.nErrorCode = PLAYER_REFUSE;

		//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
		//if ( NULL == pCenterSrv )
		//{
		//	D_ERROR("找不到centresrv\n");
		//	return false;
		//}
		//MsgToPut* pNewMsg = CreateSrvPkg( GEInviteError, pCenterSrv, errmsg );
		//pCenterSrv->SendPkgToSrv( pNewMsg );
		SendMsgToCenterSrv( GEInviteError, errmsg );
	}else{

		//3.置被邀请者组队状态;??? //认为应该收到relationsrv消息再置
		GEInviteSuccess inviteSuccess;
		inviteSuccess.beInvitedPlayer = pOwner->GetMemberRelationSrvInfo();
		inviteSuccess.invitePlayerID = pClimsg->invitePlayerID;

		//CCenterSrv* pCenterSrv = CManCenterSrvs::GetCenterserver();
		//if ( NULL == pCenterSrv )
		//{
		//	D_ERROR("找不到centresrv\n");
		//	return false;
		//}
		//MsgToPut* pNewMsg = CreateSrvPkg( GEInviteSuccess, pCenterSrv, inviteSuccess );
		//pCenterSrv->SendPkgToSrv( pNewMsg );
		SendMsgToCenterSrv( GEInviteSuccess, inviteSuccess );
	}

	pOwner->RemoveTeamInvitePlayer( pClimsg->invitePlayerID );  //回复了清空队伍消息
	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnTeamSwitch( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnTeamSwitch,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGTeamSwitch, pClimsg, pPkg, wPkgLen );
	if( pClimsg->ucTeamSwitch == CGTeamSwitch::OPEN )
	{
		pOwner->SetTeamSwitch( true );
	}else{
		pOwner->SetTeamSwitch( false );
	}

	GCTeamSwitch reqmsg;
	reqmsg.nErrorCode = INVITE_SUCCESS;
	reqmsg.ucNewTeamSwitch = pClimsg->ucTeamSwitch;

	NewSendPlayerPkg( GCTeamSwitch, pOwner, reqmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCTeamSwitch, pOwner, reqmsg );
	//pOwner->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnReqQuitTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnReqQuitTeam,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGReqQuitTeam, pClimsg, pPkg, wPkgLen ); ACE_UNUSED_ARG( pClimsg );

	if( !pOwner->IsInTeam() )
	{
		GCReqQuitTeam errmsg;
		errmsg.nErrorCode = PLAYER_NOT_IN_TEAM;
		errmsg.quitMemberID = pOwner->GetPlayerID();

		NewSendPlayerPkg( GCReqQuitTeam, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCReqQuitTeam, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		return true;
	}

	PlayerID tmpID = pOwner->GetPlayerID();
	RemoveTeamMemberRequest reqmsg;
	reqmsg.teamInd = pOwner->GetTeamId();
	reqmsg.palyerId = tmpID.dwPID;
	reqmsg.gateId = tmpID.wGID;
	reqmsg.removedMode = 0;	//自动退出先设置为1???
	SendMsgToRelationSrv( RemoveTeamMemberRequest, reqmsg );
	D_DEBUG("向RELATIONSRV发送移出组员消息 退出人:%s GID:%d PID:%d \n", pOwner->GetRoleName(), tmpID.wGID, tmpID.dwPID );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnBanTeamMember( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnBanTeamMember,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGBanTeamMember, pClimsg, pPkg, wPkgLen );

	bool isCaptain = pOwner->IsTeamCaptain();
	bool isInTeam = pOwner->IsInTeam();
	if( isCaptain &&  isInTeam )
	{
		SafeStruct( RemoveTeamMemberRequest, banReq);
		banReq.palyerId = pClimsg->banPlayerID.dwPID;
		banReq.gateId = pClimsg->banPlayerID.wGID;
		banReq.removedMode = 1; //暂时设置为移除???
		banReq.requestSeq = 0;
		banReq.teamInd = CDealRelationSrvPkg::ConvertTeamIDToRSrvTeamInd( pClimsg->teamID );
		SendMsgToRelationSrv( RemoveTeamMemberRequest, banReq );
		D_INFO("向RelationSrv发送踢除组员消息 被踢的GID:%d PID:%d\n", banReq.gateId, banReq.palyerId );
		return true;
	}

	GCBanTeamMember errMsg;
	errMsg.nErrorCode = PLAYER_NOT_CAPTAIN;

	NewSendPlayerPkg( GCBanTeamMember, pOwner, errMsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCBanTeamMember, pOwner, errMsg );
	//pOwner->SendPkgToPlayer( pNewMsg );

	D_WARNING("移除组员错误 组队:%d 队长:%d\n", isInTeam, isCaptain );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnChangeLeader( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnChangeLeader,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGChangeLeader, pClimsg, pPkg, wPkgLen );

	//没组队,错!
	if( !pOwner->IsInTeam() )
	{
		SafeStruct( GCChangeLeader, errmsg);
		errmsg.nErrorCode = PLAYER_NOT_IN_TEAM;

		NewSendPlayerPkg( GCChangeLeader, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeLeader, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		return true;
	}

	//不是队长,错!
	if( !pOwner->IsTeamCaptain() )
	{
		SafeStruct( GCChangeLeader, errmsg);
		errmsg.nErrorCode = PLAYER_NOT_CAPTAIN;

		NewSendPlayerPkg( GCChangeLeader, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeLeader, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		return true;
	}

	//还需要比对TeamInd
	if( pClimsg->teamID != CDealRelationSrvPkg::ConvertRSrvTeamIndToTeamID( pOwner->GetTeamId() ) )
	{
		D_ERROR("传来的teamID不对\n");
		return false;
	}


	//relationSrv相关
	ModifyTeamCaptainRequest changeReq;
	StructMemSet( changeReq, 0, sizeof( changeReq ) );
	changeReq.gateId = pClimsg->newLeaderID.wGID;
	changeReq.playerId = pClimsg->newLeaderID.dwPID;
	changeReq.requestSeq = 0;
	changeReq.teamInd = CDealRelationSrvPkg::ConvertTeamIDToRSrvTeamInd(pClimsg->teamID);
	SendMsgToRelationSrv( ModifyTeamCaptainRequest, changeReq );
	D_DEBUG("向RELATIONSRV发送更换队长消息\n");

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnChangeTeamItemMode( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnChangeTeamItemMode,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGChangeTeamItemMode, pClimsg, pPkg, wPkgLen );

	//没组队,错!
	if( !pOwner->IsInTeam() )
	{
		GCChangeTeamItemMode errmsg;
		errmsg.nErrorCode = PLAYER_NOT_IN_TEAM;

		NewSendPlayerPkg( GCChangeTeamItemMode, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeTeamItemMode, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		D_ERROR("没组队却发送了更改物品分配MODE\n");
		return false;
	}

	//不是队长,错!
	if( !pOwner->IsTeamCaptain() )
	{
		GCChangeTeamItemMode errmsg;
		errmsg.nErrorCode = PLAYER_NOT_CAPTAIN;
		NewSendPlayerPkg( GCChangeTeamItemMode, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeTeamItemMode, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		D_ERROR("不是队长却发送了更改物品分配MODE\n");
		return false;
	}

	//还需要比对TeamInd
	if( pClimsg->teamID != CDealRelationSrvPkg::ConvertRSrvTeamIndToTeamID( pOwner->GetTeamId() ) )
	{
		D_ERROR("传来的teamID不对\n");
		return false;
	}

	SafeStruct( ModifyGoodSharedModeRequest, changeReq );
	changeReq.teamInd = pOwner->GetTeamId();
	changeReq.mode = pClimsg->ucItemMode;		//新的物品分配模式
	SendMsgToRelationSrv( ModifyGoodSharedModeRequest, changeReq );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnChangeTeamExpMode( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnChangeTeamExpMode,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGChangeTeamExpMode, pClimsg, pPkg, wPkgLen );

	//没组队,错!
	if( !pOwner->IsInTeam() )
	{
		GCChangeTeamExpMode errmsg;
		errmsg.nErrorCode = PLAYER_NOT_IN_TEAM;

		NewSendPlayerPkg( GCChangeTeamExpMode, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeTeamExpMode, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		D_ERROR("没组队却发送了更改队员经验MODE\n");
		return false;
	}

	//不是队长,错!
	if( !pOwner->IsTeamCaptain() )
	{
		GCChangeTeamExpMode errmsg;
		errmsg.nErrorCode = PLAYER_NOT_CAPTAIN;

		NewSendPlayerPkg( GCChangeTeamExpMode, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeTeamExpMode, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		D_ERROR("不是队长却发送了更改队员经验MODE\n");
		return false;
	}

	//还需要比对TeamInd
	if( pClimsg->teamID != CDealRelationSrvPkg::ConvertRSrvTeamIndToTeamID( pOwner->GetTeamId() ) )
	{
		D_ERROR("传来的teamID不对\n");
		return false;
	}

	SafeStruct( ModifyExpSharedModeRequest, changeReq );
	changeReq.teamInd = pOwner->GetTeamId();
	changeReq.mode = pClimsg->ucExpMode;		//新的物品分配模式
	changeReq.requestSeq = 0;
	SendMsgToRelationSrv( ModifyExpSharedModeRequest, changeReq );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnCGRollItemFeedBack( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnCGRollItemFeedBack,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGRollItemFeedBack, pClimsg, pPkg, wPkgLen );

	GMAgreeRollItem mapSrvMsg;
	mapSrvMsg.agree = pClimsg->agree;
	mapSrvMsg.playerID = pOwner->GetPlayerID();
	mapSrvMsg.rollItemID = pClimsg->rollID;
	SendMsgToMapSrvByPlayer( GMAgreeRollItem, pOwner, mapSrvMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealPlayerPkg::OnCGReqJoinTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnCGReqJoinTeam,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGReqJoinTeam, pClimsg, pPkg, wPkgLen );

	if( pOwner->IsInTeam() )
	{
		D_ERROR("申请人已经在组队中");
		return false;
	}

	GEReqJoinTeam srvmsg;
	SafeStrCpy( srvmsg.captainPlayerName, pClimsg->teamLeadName );
	srvmsg.captainPlayerNameLen = (UINT)strlen( srvmsg.captainPlayerName ) + 1;
	srvmsg.reqPlayer = pOwner->GetMemberRelationSrvInfo();
	srvmsg.isReqPlayerEvil = pOwner->IsEvil();
	SendMsgToCenterSrv( GEReqJoinTeam, srvmsg );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnCGRecvReqJoinTeamFeedBack( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnCGRecvReqJoinTeamFeedBack,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGRecvReqJoinTeamFeedBack, pClimsg, pPkg, wPkgLen );

	if( !pOwner->IsInTeamReqList( pClimsg->reqPlayerID ) )
	{
		//没记录申请信息就发来是作弊消息不回应
		return false;
	}

	pOwner->RemoveTeamReqPlayer( pClimsg->reqPlayerID );

	if( (!pOwner->IsInTeam()) || (!pOwner->IsTeamCaptain()) )
	{
		//发给申请的人
		GEReqJoinError errmsg;
		SafeStrCpy( errmsg.captainPlayerName, pOwner->GetRoleName() );
		errmsg.captainPlayerNameLen = (UINT)strlen( errmsg.captainPlayerName ) +1;
		errmsg.nErrorCode = PLAYER_REFUSE;
		errmsg.reqPlayerID = pClimsg->reqPlayerID;
		SendMsgToCenterSrv( GEReqJoinError, errmsg );

		//发给按确定的本人
		GCRecvReqJoinTeamFeedBackError errMsg;
		errMsg.nErrorCode = PLAYER_NOT_CAPTAIN;

		NewSendPlayerPkg( GCRecvReqJoinTeamFeedBackError, pOwner, errMsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCRecvReqJoinTeamFeedBackError, pOwner, errMsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		return false;
	}

	if( pClimsg->ucAnswer == CGRecvReqJoinTeamFeedBack::NO )
	{
		GEReqJoinError errmsg;
		SafeStrCpy( errmsg.captainPlayerName, pOwner->GetRoleName() );
		errmsg.captainPlayerNameLen = (UINT)strlen( errmsg.captainPlayerName ) +1;
		errmsg.nErrorCode = PLAYER_REFUSE;
		errmsg.reqPlayerID = pClimsg->reqPlayerID;
		SendMsgToCenterSrv( GEReqJoinError, errmsg );
	}else{
		//再发到CENTERSRV去验证下
		SafeStruct( AddTeamMemberRequest, srvmsg );
		srvmsg.requestSeq = 0;
		srvmsg.palyerId = pOwner->GetPlayerDwPID();
		srvmsg.gateId = pOwner->GetPlayerID().wGID;
		srvmsg.member.gateId = pClimsg->reqPlayerID.wGID;
		srvmsg.member.playerId = pClimsg->reqPlayerID.dwPID;
		srvmsg.teamInd = pOwner->GetTeamId();
		srvmsg.flag = 1;	//1代表申请入队
		SendMsgToRelationSrv( AddTeamMemberRequest, srvmsg );
	}



	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnCGChangeTeamRollLevel( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnCGChangeTeamRollLevel,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGChangeTeamRollLevel, pClimsg, pPkg, wPkgLen );

	if( pOwner->IsInTeam() && pOwner->IsTeamCaptain() )
	{
		ModifyTeamRollItemLevelRequest srvmsg;
		srvmsg.teamInd = pOwner->GetTeamId();
		srvmsg.level = pClimsg->ucRollLevel;
		srvmsg.playerId = pOwner->GetPlayerID().dwPID;
		srvmsg.gateId = pOwner->GetPlayerID().wGID;
		srvmsg.requestSeq = 0;
		SendMsgToRelationSrv( ModifyTeamRollItemLevelRequest, srvmsg );
		return true;
	}


	GCChangeTeamRollLevel errmsg;
	errmsg.nErrorCode = GCChangeTeamRollLevel::NOT_TEAM_CAPTAIN;

	NewSendPlayerPkg( GCChangeTeamRollLevel, pOwner, errmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeTeamRollLevel, pOwner, errmsg );
	//pOwner->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnCGChangeReqSwitch( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnCGChangeReqSwitch,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGChangeReqSwitch, pClimsg, pPkg, wPkgLen );

	if( pOwner->IsInTeam() && pOwner->IsTeamCaptain() )
	{
		GCChangeReqSwitch climsg;
		climsg.bReq = pClimsg->bReq;
		climsg.nErrorCode = GCChangeReqSwitch::SUCCESS;

		NewSendPlayerPkg( GCChangeReqSwitch, pOwner, climsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeReqSwitch, pOwner, climsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		pOwner->SetReqSwitch( pClimsg->bReq );	//设置被选
		return true;
	}

	GCChangeReqSwitch errmsg;
	errmsg.nErrorCode = GCChangeReqSwitch::NOT_TEAM_CAPTAIN;

	NewSendPlayerPkg( GCChangeReqSwitch, pOwner, errmsg );
	//MsgToPut* pNewMsg = CreatePlayerPkg( GCChangeReqSwitch, pOwner, errmsg );
	//pOwner->SendPkgToPlayer( pNewMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealPlayerPkg::OnDestoryTeam( CPlayer* pOwner, const  char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if( NULL == pOwner)
	{
		D_ERROR( "OnDestoryTeam,pOwner空\n" );
		return false;
	}

	DealPlayerPkgPre( CGDestoryTeam, pClimsg, pPkg, wPkgLen ); ACE_UNUSED_ARG( pClimsg );

	//没组队,错!
	if( !pOwner->IsInTeam() )
	{
		GCDestoryTeam errmsg;
		errmsg.nErrorCode = PLAYER_NOT_IN_TEAM;

		NewSendPlayerPkg( GCDestoryTeam, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCDestoryTeam, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		return false;
	}

	if( !pOwner->IsTeamCaptain() )
	{
		GCDestoryTeam errmsg;
		errmsg.nErrorCode = PLAYER_NOT_CAPTAIN;

		NewSendPlayerPkg( GCDestoryTeam, pOwner, errmsg );
		//MsgToPut* pNewMsg = CreatePlayerPkg( GCDestoryTeam, pOwner, errmsg );
		//pOwner->SendPkgToPlayer( pNewMsg );
		return false;
	}


	DestroyTeamRequest destoryRequest;
	StructMemSet( destoryRequest, 0, sizeof( DestroyTeamRequest ) );
	destoryRequest.teamInd = pOwner->GetTeamId();
	destoryRequest.requestSeq = 0;
	SendMsgToRelationSrv( DestroyTeamRequest, destoryRequest );
	D_DEBUG( "向RelationSrv发送组队消息 teamId:%s\n", destoryRequest.teamInd.teamInd );

	return true;
	TRY_END;
	return false;
}
