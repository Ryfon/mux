﻿#ifndef _LOG_MANAGER_H_
#define _LOG_MANAGER_H_


#include <time.h>

class CPlayer;


class CLogManager
{
public:
	//账号登陆
	static void DoAccountLogin(CPlayer* pPlayer, unsigned char type);

	//进入游戏
	static void DoRoleEnterGame(CPlayer *pPlayer, unsigned char type);

	//账号登出/离开游戏
	static void DoAccountLogout(CPlayer *pPlayer, unsigned char type);//账号/角色同时下线
	static void DoAccountLogout(const char *pAccount, unsigned int duration, unsigned char type);//账号下线
	
	//离开游戏
	static void DoRoleLeaveGame(unsigned int roleID, unsigned short roleLevel, unsigned int duration, unsigned char type);//角色下线
	
	//关系变化
	static void DoRoleRelationChanage( CPlayer *pPlayer, unsigned int relationRoleId, unsigned char relationType, unsigned char chanageType);

	//工会变化
	static void DoRoleUnionChanage( CPlayer *pPlayer, unsigned int unionID, unsigned char chanageType);

	//组队变化
	static void DoRoleTeamChanage( unsigned int roleID, unsigned short roleLevel, unsigned int teamID, unsigned char chanageType);

	//商城购买
	static void DoMarketPruchase(CPlayer *pPlayer, unsigned int diamond, unsigned int credit, unsigned int itemType, unsigned itemNum);

	//发送邮件
	static void DoSendMail(CPlayer *pPlayer, unsigned int recvRoleID, unsigned int mailID);

private:
	//当前时间
	static unsigned int Now(void){ return (unsigned int)time(NULL); }
};

#endif/*_LOG_MANAGER_H_*/
