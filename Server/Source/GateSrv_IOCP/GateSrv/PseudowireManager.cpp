﻿#include "PseudowireManager.h"
#include "../../Base/Utility.h"


CPseudowireManager::CPseudowireManager(void)
{

}

CPseudowireManager::~CPseudowireManager(void)
{
	Clear();
}

void CPseudowireManager::OnRecvPWInfo(PseudowireArg_i *pItems, unsigned char count)
{
	if((NULL == pItems) || (0 == count))
		return;

	for(unsigned char i = 0; i < count; i++)
	{
		AddPWItem(&pItems[i]);
	}

	return;
}

void CPseudowireManager::OnRecvPWMapInfo(PseudowireMapArg_i *pItems, unsigned char count)
{
	if((NULL == pItems) || (0 == count))
		return;

	for(unsigned char i = 0; i < count; i++)
	{
		AddPWMapItem(&pItems[i]);
	}

	return;
}

void CPseudowireManager::OnPWStateChange(unsigned short pwid, PseudowireArg_i::ePWState newState)
{
	PseudowireData *pPwData = GetPWItem(pwid);
	if((NULL != pPwData) && (newState != pPwData->psArg.state))
		pPwData->psArg.state = newState;

	return;
}

void CPseudowireManager::OnPWMapStateChange(unsigned short pwid, unsigned short mapid, PseudowireMapArg_i::ePWMapState newState)
{
	PseudowireMapData *pPwMapData = GetPWMapItem(pwid, mapid);
	if((NULL != pPwMapData) && (newState != pPwMapData->pwMapArg.state))
		pPwMapData->pwMapArg.state = newState;

	return;
}

void CPseudowireManager::OnPWMapPlayerNumChange(unsigned short pwid, unsigned short mapid, unsigned short newPlayerNum)
{
	PseudowireMapData *pPwMapData = GetPWMapItem(pwid, mapid);
	if((NULL != pPwMapData) && (newPlayerNum != pPwMapData->currenPlayerNum))
		pPwMapData->currenPlayerNum = newPlayerNum;

	return;
}

void CPseudowireManager::GetSelectPWOption(std::vector<PseudowireData*>& psMap, std::vector<PseudowireMapData*>& pswMap)
{
	for(std::map<unsigned short, PseudowireData*>::iterator iter = m_pwInfo.begin(); m_pwInfo.end() != iter; ++iter)
	{
		psMap.push_back(iter->second);
	}
	
	for(std::map<unsigned int, PseudowireMapData*>::iterator iter = m_pwMapInfo.begin(); m_pwMapInfo.end() != iter; ++iter)
	{
		pswMap.push_back(iter->second);
	}

	return;
}

void CPseudowireManager::GetShiftPWOption(unsigned short mapid, std::vector<unsigned short>& mapset)
{
	GetPWBySameMap(mapid, mapset);
	return;
}

void CPseudowireManager::Clear(void)
{
	for(std::map<unsigned short, PseudowireData*>::iterator iter = m_pwInfo.begin(); m_pwInfo.end() != iter; ++iter)
	{
		delete iter->second;
		iter->second = NULL;
	}
	m_pwInfo.clear();

	for(std::map<unsigned int, PseudowireMapData*>::iterator iter = m_pwMapInfo.begin(); m_pwMapInfo.end() != iter; ++iter)
	{
		delete iter->second;
		iter->second = NULL;
	}
	m_pwMapInfo.clear();

	return;
}

void CPseudowireManager::AddPWItem(PseudowireArg_i *pItem)
{
	if(NULL == pItem)
		return;

	std::map<unsigned short, PseudowireData*>::iterator iter = m_pwInfo.find(pItem->idx);
	if(m_pwInfo.end() == iter)
	{
		PseudowireData* pObject = NEW PseudowireData;
		if(NULL != pObject)
		{
			pObject->psArg = *pItem;
			pObject->lastTimeOfStateChange = 0;

			m_pwInfo[pObject->psArg.idx] = pObject;
		}
	}

	return;
}

void CPseudowireManager::AddPWMapItem(PseudowireMapArg_i *pItem)
{
	if(NULL == pItem)
		return;

	unsigned int id = GEN_DOUBLE_WORD(pItem->pwid, pItem->mapid);
	std::map<unsigned int, PseudowireMapData*>::iterator iter = m_pwMapInfo.find(id);
	if(m_pwMapInfo.end() == iter)
	{
		PseudowireMapData* pObject = NEW PseudowireMapData;
		if(NULL != pObject)
		{
			pObject->currenPlayerNum = 0;
			pObject->lastTimeOfStateChange =0;
			pObject->pwMapArg = *pItem;

			m_pwMapInfo[id] = pObject;
		}
	}

	return;
}

PseudowireData* CPseudowireManager::GetPWItem(unsigned short pwid)
{
	std::map<unsigned short, PseudowireData*>::iterator iter = m_pwInfo.find(pwid);
	if(m_pwInfo.end() != iter)
		return iter->second;
	
	return NULL;
}

PseudowireMapData* CPseudowireManager::GetPWMapItem(unsigned short pwid, unsigned short mapid)
{
	std::map<unsigned int, PseudowireMapData*>::iterator iter = m_pwMapInfo.find(GEN_DOUBLE_WORD(pwid, mapid));
	if(m_pwMapInfo.end() != iter)
		return iter->second;

	return NULL;
}

size_t CPseudowireManager::GetPWBySameMap(unsigned short mapid, std::vector<unsigned short>& mapset)
{
	for(std::map<unsigned int, PseudowireMapData*>::iterator iter = m_pwMapInfo.begin(); m_pwMapInfo.end() != iter; ++iter)
	{
		if(GET_LOW_WORD(iter->first) == mapid)
		{
			mapset.push_back(GET_HIGH_WORD(iter->first));
		}
	}

	return mapset.size();
}

