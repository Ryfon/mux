﻿/**
* @file otherserver.h
* @brief 定义服务器端的其它服务
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: otherserver.h
* 摘    要: 定义服务器端的其它服务
* 作    者: dzj
* 完成日期: 2007.12.10
*
*/

#pragma once

#include "../../../Base/PkgProc/OtherserverBase.h"
#include "../../../Base/PlayerIDBuffer.h"
#include "../Player/Player.h"

///loginsrv类；
class CLoginSrv : public CSrvBase
{
public:
	CLoginSrv( unsigned short byID, CSrvConn* pConn );
	virtual ~CLoginSrv();

public:
	///取自身ID(第几个centersrv)
	unsigned short GetID()
	{
		return m_wID;
	}

public:
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

private:
	///自身在同类SRV中的序号；
	unsigned short m_wID;//自身在同类SRV中的序号；
};

///loginsrv管理器
class CManLoginSrvs
{
private:
	CManLoginSrvs();//屏蔽此操作；
public:
	///添加新loginsrv;
	static void AddOneSrv( CLoginSrv* pLoginSrv )
	{
		if ( NULL == pLoginSrv )
		{
			D_WARNING( "CManLoginSrvs::AddOneSrv传入pLoginSrv空" );
			return;
		}
		m_mapLoginSrvs.insert( pair<unsigned short, CLoginSrv*>( pLoginSrv->GetID(), pLoginSrv ) );
	}
	///删loginsrv;
	static bool RemoveSrv( unsigned short wID ) //对应srv析构以及对应conn断开(destory)时都会调用，后一次调用实际无效果；
	{
		map<unsigned short, CLoginSrv*>::iterator tmpIter = m_mapLoginSrvs.find( wID );
		if ( tmpIter != m_mapLoginSrvs.end() )
		{
			m_mapLoginSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}
	///得到特定的loginsrv;
	static CLoginSrv* GetLoginserver( )
	{
		map<unsigned short, CLoginSrv*>::iterator tmpiter=m_mapLoginSrvs.find( 0 );
		if ( tmpiter != m_mapLoginSrvs.end() )
		{
			CLoginSrv* pSrv = tmpiter->second;
			return pSrv;
		} else {
			return NULL;
		}
	}

private:
	///本管理器管理的所有loginsrv;
	static map<unsigned short, CLoginSrv*> m_mapLoginSrvs;
};

///centersrv类；
class CCenterSrv : public CSrvBase
{
public:
	CCenterSrv( unsigned short byID, CSrvConn* pConn );
	virtual ~CCenterSrv();

public:
	///取自身ID(第几个centersrv)
	unsigned short GetID()
	{
		return m_wID;
	}

public:
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

private:
	///自身在同类SRV中的序号；
	unsigned short m_wID;//自身在同类SRV中的序号；
};

///centersrv管理器
class CManCenterSrvs
{
private:
	CManCenterSrvs();//屏蔽此操作；
public:
	///添加新centersrv;
	static void AddOneSrv( CCenterSrv* pCenterSrv )
	{
		if ( NULL == pCenterSrv )
		{
			D_WARNING( "CManCenterSrvs::AddOneSrv传入centersrv空" );
			return;
		}
		m_mapCenterSrvs.insert( pair<unsigned short, CCenterSrv*>( pCenterSrv->GetID(), pCenterSrv ) );
	}
	///删dbsrv;
	static bool RemoveSrv( unsigned short wID ) //对应srv析构以及对应conn断开(destory)时都会调用，后一次调用实际无效果；
	{
		map<unsigned short, CCenterSrv*>::iterator tmpIter = m_mapCenterSrvs.find( wID );
		if ( tmpIter != m_mapCenterSrvs.end() )
		{
			m_mapCenterSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}
	///得到特定的centersrv;
	static CCenterSrv* GetCenterserver( )
	{
		map<unsigned short, CCenterSrv*>::iterator tmpiter=m_mapCenterSrvs.find( 0 );
		if ( tmpiter != m_mapCenterSrvs.end() )
		{
			CCenterSrv* pSrv = tmpiter->second;
			return pSrv;
		} else {
			return NULL;
		}
	}

private:
	///本管理器管理的所有centersrv;
	static map<unsigned short, CCenterSrv*> m_mapCenterSrvs;
};


///dbsrv类；
class CDbSrv : public CSrvBase
{
public:
	CDbSrv( unsigned short byID, CSrvConn* pConn );
	virtual ~CDbSrv();

public:
	///取自身ID(第几个dbsrv)
	unsigned short GetID()
	{
		return m_wID;
	}

public:
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

private:
	///自身在同类SRV中的序号；
	unsigned short m_wID;//自身在同类SRV中的序号；
};

///dbsrv管理器
class CManDbSrvs
{
private:
	CManDbSrvs();//屏蔽此操作；
public:
	///添加新dbsrv;
	static void AddOneSrv( CDbSrv* pDbSrv )
	{
		if ( NULL == pDbSrv )
		{
			D_WARNING( "CManDbSrvs::AddOneSrv传入pDbSrv空" );
			return;
		}
		m_mapDbSrvs.insert( pair<unsigned short, CDbSrv*>( pDbSrv->GetID(), pDbSrv ) );
	}
	///删dbsrv;
	static bool RemoveSrv( unsigned short wID ) //对应srv析构以及对应conn断开(destory)时都会调用，后一次调用实际无效果；
	{
		map<unsigned short, CDbSrv*>::iterator tmpIter = m_mapDbSrvs.find( wID );
		if ( tmpIter != m_mapDbSrvs.end() )
		{
			m_mapDbSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}

	///找到对应帐号名应该访问的dbsrv编号;
	static unsigned long DbsrvHashStringToSeqNo( const char* accountName )
	{
		if ( NULL == accountName )
		{
			return 0;
		} else {
			if ( m_mapDbSrvs.size() <= 1 )
			{
				return 0;
			}
			//string hash from dan bernstein;
			unsigned long hash = 5381;
			int c = 0;
			while ( true )
			//while ( c = *accountName++ )
			{
				c = *accountName;
				if ( 0==c )
				{
					break;
				}				
				hash = ((hash << 5) + hash) + c;// hash * 33 + c;
				++accountName;
			}

			return hash % m_mapDbSrvs.size();
		}
	}

	static CDbSrv* GetDbsrvByAccount( const char* accountName )
	{
		unsigned long dbsrvSeqNo = DbsrvHashStringToSeqNo( accountName );
		return GetDbSrvBySeqNo( dbsrvSeqNo );
	}

	///得到特定的dbsrv;
	static CDbSrv* GetDbsrvByID( unsigned short byID )
	{
		map<unsigned short, CDbSrv*>::iterator tmpiter=m_mapDbSrvs.find( byID );
		if ( tmpiter != m_mapDbSrvs.end() )
		{
			CDbSrv* pSrv = tmpiter->second;
			return pSrv;
		} else {
			return NULL;
		}
	}

	static CDbSrv* GetValidDBSrv()
	{
		if ( m_mapDbSrvs.empty() )
			return NULL;

		map<unsigned short, CDbSrv*>::iterator tmpiter = m_mapDbSrvs.begin();
		return tmpiter->second;
	}

private:
	///根据在m_mapDbSrvs中的序号取相应的DbSrv
	static CDbSrv* GetDbSrvBySeqNo( unsigned long dbsrvSeqNo )
	{
		unsigned long curNo = 0;
		map<unsigned short, CDbSrv*>::iterator tmpiter = m_mapDbSrvs.begin();
		while ( ( dbsrvSeqNo > curNo ) //一直找到指定序号；
			&& ( tmpiter != m_mapDbSrvs.end() )//且没有越过m_mapDbSrvs.end();
			)
		{
			++tmpiter;
			++curNo;
		}

		if ( tmpiter != m_mapDbSrvs.end() )
		{
			return tmpiter->second;
		} else {
			D_ERROR( "不可能错误，GetDbSrvBySeqNo序号超出范围\n" );
			return NULL;
		}
	}

private:
	///本管理器管理的所有dbsrv;
	static map<unsigned short, CDbSrv*> m_mapDbSrvs;//所有dbsrv, pair(byID,CMapSrv*);
};

///玩家的存DB信息，一般下线时由DBPre开始DBPost结束中间为各式各样的DbSaveInfo，平常玩家在线保存时为单个的DbSaveInfo；
struct DBSaveInfo
{
	bool           isValid;
	MGDbSavePre    playerSavePre;//最近一位存DB者信息；
};

///mapsrv类;
class CMapSrv : public CSrvBase
{
public:
	CMapSrv( unsigned short byID, CSrvConn* pConn );
	virtual ~CMapSrv();

public:
	///取自身ID(第几个mapsrv)
	unsigned short GetID()
	{
		return m_wID;
	}

public:
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

public:
	void SetBroadCastTgt( int tgtNum/*目标个数*/, const char* pPkg/*目标ID数组*/, int pkgLen )
	{
		if ( MGBroadCast::NOTI_EVERYONE == tgtNum )
		{
			//广播给gate上的所有玩家；
			m_bBroadFlag = true;
			m_bBroadToEveryOne = true;
			return;
		}

		m_BroadCastTgt.nCurNum = tgtNum;
		if ( (int)sizeof(m_BroadCastTgt.arrPlayerPID[0]) * tgtNum != pkgLen )
		{
			//验证待拷贝大小；
			D_ERROR( "map发来的MGBroadCast包，包长有误！\n" );
			m_BroadCastTgt.nCurNum = 0;
			m_bBroadFlag = true;//仍然要设广播标记，否则后一个收到的广播包会导起混乱；
			return;
		}

		if ( sizeof( m_BroadCastTgt.arrPlayerPID ) < (unsigned int)pkgLen )
		{
			//确保目标内存足够；
			D_ERROR( "map发来的MGBroadCast包，超出收包大小，两边协议不一致！\n" );
			m_BroadCastTgt.nCurNum = 0;
			m_bBroadFlag = true;//仍然要设广播标记，否则后一个收到的广播包会导起混乱；
			return;
		}

		memcpy( &(m_BroadCastTgt.arrPlayerPID), pPkg, pkgLen );

		m_bBroadFlag = true;

		return;
	}

	inline bool IsBroadCast() { return m_bBroadFlag; }

	inline bool IsBroadToEveryOne() { return m_bBroadToEveryOne; }

	//进行群发，如果确实群发了，返回真，否则返回假；
	template <typename T_Msg>
	bool BroadcastSend(T_Msg* pMsg)
	{
		if ( !IsBroadCast() )
		{
			return false;
		}

		bool tmpIsBroToEveryOne = IsBroadToEveryOne();//因为马上要重置，因此暂存下有效值；
		m_bBroadFlag = false; //立刻重设广播标记；
		m_bBroadToEveryOne = false;

		if ( tmpIsBroToEveryOne )//广播给gate上的所有玩家；
		{
#ifdef DS_EPOLL
			GatePlayerBrocast( T_Msg, *pMsg );//全gate广播消息	
#else  //DS_EPOLL
			MsgToPut* pBrocastMsg = CreateGateBrocastPkg( T_Msg, *pMsg );//全gate广播消息；
			if ( NULL != g_pClientSender )
			{
				if ( g_pClientSender->PushMsg( pBrocastMsg ) < 0 ) //直接压入client通信队列，因为到了底层会解析广播包发送给所有的连入连接，而不是特定人;
				{
					D_ERROR( "0926dbg, BroadcastSend, PushMsg失败，但仍返回true，消息类型%x\n", T_Msg::wCmd );
					return true;
				}
			}
#endif //DS_EPOLL
		} else {
			CPlayer* pNotiPlayer = NULL;
			for( int i=0; i<m_BroadCastTgt.nCurNum; ++i)
			{
				if ( i >= ARRAY_SIZE(m_BroadCastTgt.arrPlayerPID) )
				{
					D_ERROR( "0926dbg, BroadcastSend, i(%d) >= ARRAY_SIZE(m_BroadCastTgt.arrPlayerPID), 目标数量%d，消息类型%x\n"
						, i, m_BroadCastTgt.nCurNum, T_Msg::wCmd );
					return true;
				}
				pNotiPlayer = CManPlayer::FindPlayer( m_BroadCastTgt.arrPlayerPID[i] );//通知此玩家；
				if ( NULL != pNotiPlayer )
				{
					if ( !pNotiPlayer->IsInFighting() )
					{
						D_WARNING( "BroadcastSend,玩家尚未进入战斗状态，不应收到此类消息17，%d\n", T_Msg::wCmd );
						continue;
					}

					NewSendPlayerPkg( T_Msg, pNotiPlayer, *pMsg );
					//MsgToPut* pNewMsg = CreatePlayerPkg( T_Msg, pNotiPlayer, *pMsg );
					//pNotiPlayer->SendPkgToPlayer( pNewMsg );
					//size_t namelen = strlen( pNotiPlayer->GetAccount() );
					//if ( namelen < 5 )
					//{
					//    D_DEBUG( "本次通知数：%d，序号:%d\n", m_BroadCastTgt.nCurNum, m_BroadCastTgt.testseqID );
					//	for ( int j=0; j<m_BroadCastTgt.nCurNum; ++j )
					//	{
					//		D_DEBUG( "内部玩家ID号:%d\n", m_BroadCastTgt.arrPlayerPID[j] );
					//	}
					//	if ( T_Msg::wCmd == GCOtherPlayerInfo::wCmd )
					//	{
					//		D_DEBUG( "通知%s:%s(%x)的信息，位置(%d,%d)\n", pNotiPlayer->GetAccount()
					//			, pMsg->playerInfo.szNickName, pMsg->otherPlayerID.dwPID
					//			, pMsg->playerInfo.iPosX, pMsg->playerInfo.iPosY );
					//	}					
					//}
				} else {
					D_DEBUG( "找不到指定的通知玩家%d，消息%x\n", m_BroadCastTgt.arrPlayerPID[i], T_Msg::wCmd );
				}
			}
		}

		return true;
	}

	//template<>
	//bool BroadcastSend<GCBuffData>( GCBuffData* pMsg )
	//{
	//	return true;
	//}

private:
	///自身在同类SRV中的序号；
	unsigned short m_wID;//自身在同类SRV中的序号；

private:
	MGBroadCast    m_BroadCastTgt;//广播目标；
	bool           m_bBroadFlag;//是否在等待广播消息；
	bool           m_bBroadToEveryOne;//是否广播至gate上的每一个玩家？

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
public:
	//新的存盘信息开始;
	bool OnMGDbSavePre( const MGDbSavePre* pMGDbSavePre );

	//某条待存盘信息；
	bool OnMGDbSaveInfo( const MGDbSaveInfo* pMGDbSaveInfo );

	//最近一次存盘信息结束；
	bool OnMGDbSavePost( const MGDbSavePost* pMGDbSavePost );

public:
	///设置该mapsrv的可进入标记；
	inline bool SetFullFlag( bool isFull ) 
	{
		if ( m_bIsFull != isFull )
		{
			D_DEBUG( "目标地图满调试，mapsrv%d full状态改变，原状态:%d，新状态%d\n", GetID(), (m_bIsFull?1:0), (isFull?1:0) );
		}
		m_bIsFull = isFull; 
		return true; 
	}
	///对应的mapsrv是否可进入；
	inline bool IsCanEnter() { return !m_bIsFull/* && m_bIsMapSrvReady */; }
	inline bool SetMapSrvReady() { m_bIsMapSrvReady = true; return true; }

private:
	DBSaveInfo  m_DbSaveInfo;
	bool        m_bIsFull;//是否可进入，mapsrv每隔一定时间发来，用于跳地图判断;
	bool        m_bIsMapSrvReady;//mapsrv是否准备好；
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
};

///mapsrv管理器
class CManMapSrvs
{
private:
	CManMapSrvs();//屏蔽此操作；
public:
	///添加新mapsrv;
	static void AddOneSrv( CMapSrv* pMapSrv )
	{
		if ( NULL == pMapSrv )
		{
			return;
		}
		m_mapMapSrvs.insert( pair<unsigned short, CMapSrv*>( pMapSrv->GetID(), pMapSrv ) );
	}
	///删mapsrv;
	static bool RemoveSrv( unsigned short wID ) //对应srv析构以及对应conn断开(destory)时都会调用，后一次调用实际无效果；
	{
		map<unsigned short, CMapSrv*>::iterator tmpIter = m_mapMapSrvs.find( wID );
		if ( tmpIter != m_mapMapSrvs.end() )
		{
			m_mapMapSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}

	///得到特定的mapsrv;
	static CMapSrv* GetMapserver( unsigned short byID )
	{
		map<unsigned short, CMapSrv*>::iterator tmpiter=m_mapMapSrvs.find( byID );
		if ( tmpiter != m_mapMapSrvs.end() )
		{
			CMapSrv* pSrv = tmpiter->second;
			return pSrv;
		} else {
			return NULL;
		}
	}

	template <typename T_Msg>
	static void SendBroadcastMsg(T_Msg* pMsg)
	{
		CMapSrv* pMapSrv = NULL;
		for( map<unsigned short, CMapSrv*>::iterator tmpIter = m_mapMapSrvs.begin(); tmpIter != m_mapMapSrvs.end(); ++tmpIter )
		{
			pMapSrv = tmpIter->second;
			if( NULL == pMapSrv)
				continue;
			//MsgToPut* pNewMsg = CreateSrvPkg( T_Msg, pMapSrv, *pMsg );
			//pMapSrv->SendPkgToSrv( pNewMsg );
			SendMsgToSrv( T_Msg, pMapSrv, *pMsg );
		}
	}

private:
	///本管理器管理的所有mapsrv;
	static map<unsigned short, CMapSrv*> m_mapMapSrvs;//所有mapsrv, pair(byID,CMapSrv*);
};


///RelationSrv类；
class CRelationSrv : public CSrvBase
{
public:
	CRelationSrv( unsigned short byID, CSrvConn* pConn );
	virtual ~CRelationSrv();

public:
	///取自身ID(第几个RelationSrv)
	unsigned short GetID()
	{
		return m_wID;
	}

public:
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

private:
	///自身在同类SRV中的序号；
	unsigned short m_wID;//自身在同类SRV中的序号；
};

///RelationSrv管理器
class CManRelationSrvs
{
private:
	CManRelationSrvs();//屏蔽此操作；
public:
	///添加新dbsrv;
	static void AddOneSrv( CRelationSrv* pRelationSrv )
	{
		if ( NULL == pRelationSrv )
		{
			D_WARNING( "CManRelationSrvs::AddOneSrv传入pRelationSrv空" );
			return;
		}
		m_mapRelationSrvs.insert( pair<unsigned short, CRelationSrv*>( pRelationSrv->GetID(), pRelationSrv ) );
	}
	///删relationsrv;
	static bool RemoveSrv( unsigned short wID ) //对应srv析构以及对应conn断开(destory)时都会调用，后一次调用实际无效果；
	{
		map<unsigned short, CRelationSrv*>::iterator tmpIter = m_mapRelationSrvs.find( wID );
		if ( tmpIter != m_mapRelationSrvs.end() )
		{
			m_mapRelationSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}
	///得到特定的relationsrv;
	static CRelationSrv* GetRelationserver(/* unsigned short byID, 目前暂时只设一个relationSrv*/ )
	{
		map<unsigned short, CRelationSrv*>::iterator tmpiter=m_mapRelationSrvs.find( 0/*byID,目前暂时只设一个CRelationSrv*/ );
		if ( tmpiter != m_mapRelationSrvs.end() )
		{
			CRelationSrv* pSrv = tmpiter->second;
			return pSrv;
		} else {
			return NULL;
		}
	}

private:
	///本管理器管理的所有relationsrv;
	static map<unsigned short, CRelationSrv*> m_mapRelationSrvs;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//新loginsrv...

///NewLoginSrv类；
class CNewLoginSrv : public CSrvBase
{
public:
	CNewLoginSrv( unsigned short byID, CSrvConn* pConn );
	virtual ~CNewLoginSrv();

public:
	///取自身ID(第几个NewLoginSrv)
	unsigned short GetID()
	{
		return m_wID;
	}

public:
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

private:
	///自身在同类SRV中的序号；
	unsigned short m_wID;//自身在同类SRV中的序号；
};

///NewLoginSrv管理器
class CManNewLoginSrv
{
private:
	CManNewLoginSrv();//屏蔽此操作；

public:
	///添加新NewLoginSrv;
	static void AddOneSrv( CNewLoginSrv* pNewLoginSrv )
	{
		if ( NULL == pNewLoginSrv )
		{
			D_WARNING( "CManNewLoginSrv::AddOneSrv传入pNewLoginSrv空" );
			return;
		}
		m_mapNewLoginSrvs.insert( pair<unsigned short, CNewLoginSrv*>( pNewLoginSrv->GetID(), pNewLoginSrv ) );
	}
	///删NewLoginSrv;
	static bool RemoveSrv( unsigned short wID ) //对应srv析构以及对应conn断开(destory)时都会调用，后一次调用实际无效果；
	{
		map<unsigned short, CNewLoginSrv*>::iterator tmpIter = m_mapNewLoginSrvs.find( wID );
		if ( tmpIter != m_mapNewLoginSrvs.end() )
		{
			m_mapNewLoginSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}
	///得到特定的NewLoginSrv;
	static CNewLoginSrv* GetNewLoginSrv(/* unsigned short byID, 目前暂时只设一个relationSrv*/ )
	{
		map<unsigned short, CNewLoginSrv*>::iterator tmpiter=m_mapNewLoginSrvs.find( 0/*byID,目前暂时只设一个NewLoginSrv*/ );
		if ( tmpiter != m_mapNewLoginSrvs.end() )
		{
			CNewLoginSrv* pSrv = tmpiter->second;
			return pSrv;
		} else {
			return NULL;
		}
	}

private:
	///本管理器管理的所有newloginsrv;
	static map<unsigned short, CNewLoginSrv*> m_mapNewLoginSrvs;
};

//新loginsrv...
/////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////
class  CFunctionSrv : public CSrvBase
{
public:
	CFunctionSrv( unsigned short byID,CSrvConn* pConn  );

	virtual ~CFunctionSrv();

public:
	///取自身ID
	unsigned short GetID()
	{
		return m_wID;
	}

public:
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

public:
	//处理functionsrv发来的称号信息；
	bool OnRcvHonorPlayer( const GCHonorPlayer& honorPlayer );

	//向某玩家通知称号信息(玩家进地图时)；
	static bool NotifyHonorToOnePlayer( CPlayer* pPlayer );

private:
	//全gatesrv广播称号信息；
	static bool AllGateBroastHonorInfo();

private:
	///自身在同类SRV中的序号；
	unsigned short m_wID;//自身在同类SRV中的序号；

private:
	static bool           m_isSomeHonorValid;//是否有有效的称号信息；
	static GCHonorPlayer  m_arrHonorPlayer[TIANHOU+1];
};

///RankSrv管理器
class CManRankSrv
{
private:
	CManRankSrv();//屏蔽此操作；

public:
	///添加新RankSrv;
	static void AddOneSrv( CFunctionSrv* pRankSrv )
	{
		if ( NULL == pRankSrv )
		{
			D_WARNING( "CManRankSrv::AddOneSrv传入CFunctionSrv空" );
			return;
		}
		m_mapRankSrvs.insert( pair<unsigned short, CFunctionSrv*>( pRankSrv->GetID(), pRankSrv ) );
		OnNewFunctionSrvConned();
	}
	///删pRankSrv;
	static bool RemoveSrv( unsigned short wID ) //对应srv析构以及对应conn断开(destory)时都会调用，后一次调用实际无效果；
	{
		map<unsigned short, CFunctionSrv*>::iterator tmpIter = m_mapRankSrvs.find( wID );
		if ( tmpIter != m_mapRankSrvs.end() )
		{
			m_mapRankSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}
	///得到特定的CFunctionSrv
	static CFunctionSrv* GetRankSrv(/* unsigned short byID, 目前暂时只设一个rankSrv*/ )
	{
		map<unsigned short, CFunctionSrv*>::iterator tmpiter=m_mapRankSrvs.find( 0/*byID,目前暂时只设一个rankSrv*/ );
		if ( tmpiter != m_mapRankSrvs.end() )
		{
			CFunctionSrv* pSrv = tmpiter->second;
			return pSrv;
		} else {
			return NULL;
		}
	}

private:
	//连上了functionsrv;
	static bool OnNewFunctionSrvConned();

private:
	///本管理器管理的所有newloginsrv;
	static map<unsigned short, CFunctionSrv*> m_mapRankSrvs;
};

///ShopSrv
class CShopSrv : public CSrvBase
{
public:
	CShopSrv( unsigned short byID, CSrvConn* pConn );
	virtual ~CShopSrv();

public:
	///取自身ID(第几个ShopSrv)
	unsigned short GetID()
	{
		return m_wID;
	}

public:
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

private:
	///自身在同类SRV中的序号；
	unsigned short m_wID;//自身在同类SRV中的序号；
};

///ShopSrv管理器
class CManShopSrvs
{
private:
	CManShopSrvs();//屏蔽此操作；

public:
	///添加新shopsrv;
	static void AddOneSrv( CShopSrv* pSrv )
	{
		if ( NULL == pSrv )
		{
			D_WARNING( "CManShopSrvs::AddOneSrv传入pSrv空" );
			return;
		}
		m_mapShopSrvs.insert( pair<unsigned short, CShopSrv*>( pSrv->GetID(), pSrv ) );
		OnNewShopSrvConned();
	}

	///删除shopsrv;
	static bool RemoveSrv( unsigned short wID ) //对应srv析构以及对应conn断开(destory)时都会调用，后一次调用实际无效果；
	{
		map<unsigned short, CShopSrv*>::iterator tmpIter = m_mapShopSrvs.find( wID );
		if ( tmpIter != m_mapShopSrvs.end() )
		{
			m_mapShopSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}

	///得到特定的shopsrv;
	static CShopSrv* GetShopserver(/* unsigned short byID, 目前暂时只设一个ShopSrv*/ )
	{
		map<unsigned short, CShopSrv*>::iterator tmpiter=m_mapShopSrvs.find( 0/*byID,目前暂时只设一个CRelationSrv*/ );
		if ( tmpiter != m_mapShopSrvs.end() )
		{
			CShopSrv* pSrv = tmpiter->second;
			return pSrv;
		} else {
			return NULL;
		}
	}

private:
	//连上了shopsrv;
	static bool OnNewShopSrvConned();

private:
	///本管理器管理的所有shopsrv;
	static map<unsigned short, CShopSrv*> m_mapShopSrvs;
};


///LogSrv
class CLogSrv : public CSrvBase
{
public:
	CLogSrv( unsigned short byID, CSrvConn* pConn );
	virtual ~CLogSrv();

public:
	///取自身ID(第几个LogSrv)
	unsigned short GetID()
	{
		return m_wID;
	}

public:
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

private:
	///自身在同类SRV中的序号；
	unsigned short m_wID;//自身在同类SRV中的序号；
};

///LogSrv管理器
class CManLogSrvs
{
private:
	CManLogSrvs();//屏蔽此操作；
public:
	///添加新logsrv;
	static void AddOneSrv( CLogSrv* pSrv )
	{
		if ( NULL == pSrv )
		{
			D_WARNING( "CManLogSrvs::AddOneSrv传入pSrv空" );
			return;
		}
		m_mapLogSrvs.insert( pair<unsigned short, CLogSrv*>( pSrv->GetID(), pSrv ) );
	}
	///删除logsrv;
	static bool RemoveSrv( unsigned short wID ) //对应srv析构以及对应conn断开(destory)时都会调用，后一次调用实际无效果；
	{
		map<unsigned short, CLogSrv*>::iterator tmpIter = m_mapLogSrvs.find( wID );
		if ( tmpIter != m_mapLogSrvs.end() )
		{
			m_mapLogSrvs.erase( tmpIter );
			return true;
		}
		return false;
	}
	///得到特定的logsrv;
	static CLogSrv* GetLogserver(/* unsigned short byID, 目前暂时只设一个LogSrv*/ )
	{
		map<unsigned short, CLogSrv*>::iterator tmpiter=m_mapLogSrvs.find( 0/*byID,目前暂时只设一个CLogSrv*/ );
		if ( tmpiter != m_mapLogSrvs.end() )
		{
			CLogSrv* pSrv = tmpiter->second;
			return pSrv;
		} else {
			return NULL;
		}
	}

private:
	///本管理器管理的所有logsrv;
	static map<unsigned short, CLogSrv*> m_mapLogSrvs;
};
