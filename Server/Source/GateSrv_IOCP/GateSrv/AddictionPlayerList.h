﻿

#ifndef ADDICTION_PLAYER_QUEUE
#define ADDICTION_PLAYER_QUEUE

#include "PkgProc/CliProtocol_T.h"
#include "Utility.h"
#include <list>
using namespace std;
using namespace MUX_PROTO;



class CPlayer;
struct AddictionPlayer
{
	PlayerID addictionPlayerID;
	ACE_Time_Value addictionTime;

	AddictionPlayer( const PlayerID& playerID, const ACE_Time_Value& time ):addictionPlayerID( playerID ), addictionTime( time )
	{}

	AddictionPlayer():addictionTime( ACE_Time_Value::zero )
	{
		addictionPlayerID.dwPID = 0;
		addictionPlayerID.wGID = 0;
	}
};

class CAddictionPlayerList
{
private:
	CAddictionPlayerList(void);
	~CAddictionPlayerList(void);

public:
	static void AddAddictionPlayer( const AddictionPlayer& addictionPlayer );

	static void TimeCheck();

	static bool IsDifferent( const PlayerID& playerID );

private:
	static list<AddictionPlayer>	m_addictionPlayerList;			//封装了需要待断的PLAYER ID和收到断线消息的时间,预计2秒后断开将没问题
};

#endif	//ADDICTION_PLAYER_QUEUE
