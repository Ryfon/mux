﻿#include "AddictionPlayerList.h"
#include "Player/Player.h"

list<AddictionPlayer>	CAddictionPlayerList::m_addictionPlayerList;			//


//加入时间,从前插,只要检测到现在时间与插入时间超过2秒, 则后续的可以直接进行断连操作
void CAddictionPlayerList::AddAddictionPlayer( const AddictionPlayer& addictionPlayer )
{
	if( IsDifferent( addictionPlayer.addictionPlayerID ) )
	{
		m_addictionPlayerList.push_front( addictionPlayer );
	}
}


//获取现在时间进行比较
void CAddictionPlayerList::TimeCheck()
{
	CPlayer* pDestroyPlayer = NULL;
	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	bool bDestroyed = false;		
	for( list<AddictionPlayer>::iterator iter = m_addictionPlayerList.begin(); iter != m_addictionPlayerList.end(); )
	{
		if( bDestroyed )
		{
			pDestroyPlayer = CManPlayer::FindPlayer( iter->addictionPlayerID.dwPID );
			if( NULL != pDestroyPlayer)
			{
				pDestroyPlayer->ReqDestorySelf();
			}
			iter = m_addictionPlayerList.erase( iter );
			continue;
		}

		if( (currentTime - iter->addictionTime).sec() > 2 )			//超过2秒就断连,给客户端的消息已经确保发送完成
		{
			bDestroyed = true;
			pDestroyPlayer = CManPlayer::FindPlayer( iter->addictionPlayerID.dwPID );
			if( NULL != pDestroyPlayer)
			{
				pDestroyPlayer->ReqDestorySelf();
			}
			iter = m_addictionPlayerList.erase( iter );
			continue;
		}
		iter++;
	}
}


//是否有重复的人添加的队列,要保证队列里的PLAYERid的唯一性
bool CAddictionPlayerList::IsDifferent( const PlayerID& playerID )
{
	for( list<AddictionPlayer>::iterator iter = m_addictionPlayerList.begin(); iter != m_addictionPlayerList.end(); iter++)
	{
		if( iter->addictionPlayerID == playerID )
			return false;
	}
	return true;
}
