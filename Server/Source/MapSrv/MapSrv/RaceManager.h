﻿/********************************************************************
	created:	2009/11/05
	created:	5:11:2009   16:42
	file:		RaceManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef RACE_MANAGER_H
#define RACE_MANAGER_H

#include <map>
#include "aceall.h"
#include "RaceMaster.h"

class CPlayer;

class CRaceManager
{
public:
	CRaceManager():m_raceMasterUID(0),m_raceType(0),m_olddeclarewarinfo(0),m_bforbiddenauthoriy(false){}

public:
	//接到新选出族长的通知时
	void NewRaceMasterBorn( unsigned int masteruid,const char* playename , const char* unionname,bool isGMcmd=false );

	//玩家进入地图时
	void OnPlayerEnterMap( CPlayer* pPlayer );

	//玩家离开地图时
	void OnPlayerLevelMap( CPlayer* pPlayer );

	//当玩家发布新的公告信息时
	void OnRaceMasterSetNewNoticeMessage( char* newMsgArr );

	//当玩家对其他种族宣战时
	void OnRaceMasterDeclareWarToOtherRace( unsigned short racetype );

	//当族长领取工资时
	void OnRaceMasterGetSalary();

	//设置种族类型
	void SetRaceManagerRaceType( unsigned short racetype ) { m_raceType = racetype; }

	//当被其他种族宣战时
	void OnBeDecalreWarByOtherRace( unsigned short otherrace );

	//监视族长的计时间器
	void MonitorRaceMasterTimer( struct tm* l_tm );

	//设置族长工资
	void SetSalary( int salary ) { m_raceMaster.SetRaceMasterSalary( salary); }

	//设置是否禁止族长权利
	void SetForbiddenAuthoriy( bool isForbidden ) { m_bforbiddenauthoriy = isForbidden ; }

	//族长权利是否被禁止
	bool IsForbiddenAuthoriy() { return m_bforbiddenauthoriy; }

	//重置族长宣战信息
	void ResetDeclareWar() { m_raceMaster.ResetDeclareWar(); }

	CPlayer* GetRaceMaster() { return m_raceMaster.GetRaceMasterPlayer(); }

public:
	//该管理器接收族长的信息
	void OnRecvRaceMasterInfo( unsigned int masterUID, unsigned int todayNoticeCount, unsigned int countUpdatetime, char declarewarinfo );

	//接到新的通知消息
	void OnRecvRaceMasterNewNotice( const char* szNewNotice );

	//当族长跳地图，离线时，接到族长对应的信息更新回调
	void RecvNewRaceMasterInfoUpdate(unsigned int todaynoticecount, unsigned int lastupdatetime, bool isGetSalary  );

	//当接到族长相关的时间信息过期时,回调
	void OnRecvRaceMasterTimeInfoExpireNotice();

	//当族长对其他种族宣战时，回调
	void OnRecvRaceMasterDeclareWarInfoUpdate( char declareWarinfo );

public:
	////族长的公告信息更新,通知所有服务器改变玩家的公告信息,并通知所有在线的所有玩家
	void OnRaceMasterNoticeMsgRefresh();

	////接到族长时间相关的信息刷新时,通知所有服务器改变族长时间相关信息
	void OnRaceMasterTimeInfoExpireRefresh();

	////族长跳地图时或下线时，将该地图的组长信息通知其他所有服务器，更新其他服务器信息
	void OnRaceMasterTimerInfoChange();

	////通知其他服务器族长宣战信息被修改了
	void OnRaceMasterDeclareInfoChange();

	bool IsRaceMaster( unsigned int playerUID );//是否是种族族长

	void SetOldDeclareWarInfo() { m_olddeclarewarinfo = m_raceMaster.GetDeclareWarInfo(); } 

	bool IsDeclareWarToOtherRace( unsigned short racetype );

	//  [1/5/2010 shenchenkai]
	////通知有异族入侵
	void NotifyInvade(unsigned short invadeRace);

	////通知有异族攻击npc
	void NotifyAttackNpc(unsigned short invadeRace);

public:
	void ShowOPPlate();

	void ShowDeclareWarPlate();

	void ShowSetNewPublicMsgPlate();

	void CloseRaceMasterPlate();

private:

	void SetRaceMaster( CPlayer* pPlayer );//将玩家设置为种族族长

	void OnRaceMasterLeaveThisMap();//当种族族长离开这个地图时

	void SendRaceMasterNoticeMsg( CPlayer* pPlayer , bool isOnLineNotice );//通知族长的公告信息给玩家

	void SetNewNoticeMsgError( int errNO );//设置新的公告信息的结果返回

	void DeclareWarToOtherRaceRst( int errNo , int declareInfo ) ; //宣战是否成功的结果返回

	void OnGetSalaryRst( int errNo , int money );//获取工资的结果返回

	void SaveRaceMasterInfoToDB();//更新族长信息到DB 

	void NoticeRaceMasterBorn( unsigned short racetype, const char* mastername, const char* unionname );//通知玩家族长诞生

	///向同族玩家发送普通系统消息；
	void SystemInfoSendToRace( const char* msg );

	///向同族玩家发送通告消息；
	void RaceNotiSendToRace();

	///种族通告单独发给某个人（在单人进地图的情形下会用到）
	void RaceNotiSendToOnePerson( CPlayer* pNotiPlayer );

	///宣战信息发送；
	void RaceMasterDeclareWarInfoSend( char declareWarinfo );

	time_t GetRaceMasterExpireTime();

private:
//	std::map<unsigned int, CPlayer *> mSameRacePlayerMap;//同种族玩家的列表
	unsigned int   m_raceMasterUID;//种族族长编号
	unsigned short m_raceType;//种族编号
	char           m_olddeclarewarinfo;
	CRaceMaster    m_raceMaster;//该种族族长对应的结构
	bool           m_bforbiddenauthoriy;//禁止族长权利
};


class CGateSrv;

//所有种族的管理器
class AllRaceMasterManager
{
	friend class ACE_Singleton<AllRaceMasterManager, ACE_Null_Mutex>;	

public:
	AllRaceMasterManager();

	~AllRaceMasterManager(){};

public:
	//玩家进入地图
	void OnPlayerEnterMap( CPlayer* pPlayer );

	//玩家离开地图
	void OnPlayerLeaveMap( CPlayer* pPlayer );

	//种族族长设置新的通知消息
	void RaceMasterSetNewNoticeMsg( unsigned int racetype, char* newMsg );

	//种族族长对其他种族宣战
	void RaceMasterDeclareWarToOtherRace( unsigned int racetype, unsigned int otherrace );

	//种族族长领取工资
	void RaceMasterGetSalary( unsigned int racetype );

	//监视族长的刷新时间
	void MontiorRaceMasterTimer();

	//从数据库查询族长信息
	void QueryRaceMasterFromDB( CGateSrv* pGateSrv );

public:

	//当接到族长设置新的种族公告时,所有服务器回调
	void OnRaceMasterSetNewRaceNotice( unsigned int racetype ,char* newNotice );

	//当族长管理接到计时相关的信息改变时
	void OnRecvRaceMasterTimerInfoChange( unsigned int racetype , unsigned int noticecount, unsigned int lasttime, bool isGetSalary );

	//当接到对应种族的信息更新时
	void OnRecvRaceMasterTimeExpireNotice( unsigned int racetype );

	//当接到Function传来的种族族长信息时
	void OnRecvRaceMasterInfo( unsigned int racetype , unsigned int masteruid, unsigned int noticecount, unsigned int lastupdatetime ,unsigned int declarewarinfo,  bool isGetSalary );

	//当接到relationSrv传来的新族长诞生时
	void OnRecvNewRaceMasterBorn( unsigned int racetype , unsigned int masteruid , const char* playername, const char* unionname ,bool isGMCmd = false );

	//当接到族长宣战信息改变时
	void OnRecvRaceMasterDeclareWarInfoUpdate( unsigned int racetype , char declarewarinfo );

	//离选族长只要一个小时了,禁止族长权利
	void OnRecvForbiddenRaceMasterAuthority();

	//谈出族长的托盘界面
	void ShowRaceMasterPlate( unsigned int raceType );

	//当族长选择接下来的操作类型
	void OnPlayerSelectOPType( CPlayer* pPlayer, unsigned int  racetype, char opType );

	//对族长对其他种族宣战
	void OnPlayerDeclareWarToOtherRace( CPlayer* pPlayer, unsigned short racetype, unsigned short otherracetype );

	//当族长设置新的种族公告时
	void OnPlayerSetNewPublicMsg( CPlayer* pPlayer, unsigned int racetype ,char* newNotice );
	
	//获取种族管理器
	CRaceManager* GetRaceManagerByRace( unsigned short racetype );

	//玩家是否是族长
	bool IsRaceMaster( CPlayer* pPlayer );

	//attrace种族是否向defrace种族宣战了
	bool IsDecalreWar( unsigned short attrace, unsigned short defrace );

	//重置所有种族的攻城战信息
	void ResetAllRaceDeclareWarInfo();

private:
	CRaceManager m_humanRaceMan;
	CRaceManager m_akaRaceMan;
	CRaceManager m_elfRaceMan;
	time_t       m_lastupdatetime;
};

typedef ACE_Singleton<AllRaceMasterManager, ACE_Null_Mutex> AllRaceMasterManagerSingle;

#endif