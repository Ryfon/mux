﻿#include "PseudowireManager.h"
#include "../../Base/XmlManager.h"
#include "../../Base/Utility.h"
#include "G_MProc.h"



CPseudowireManager::CPseudowireManager(void)
	:m_flag(false)
{
}

CPseudowireManager::~CPseudowireManager(void)
{
	Clear();
}

bool CPseudowireManager::Initialize(void)
{
	Clear();
	return LoadPWConfig();
}

bool CPseudowireManager::LoadPWConfig(void)
{
	const char* pszFileName = "config/pseudowire/pseudowireConfig.xml";

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR("伪线配置文件%s加载出错\n", pszFileName);
		return false;
	}

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("伪线配置文件%s可能为空\n", pszFileName);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);

	const char *pTemp = NULL;
	CElement *pElement = NULL;
	unsigned short pwIndex = 0;//用于记录和临时保存伪线号	
	for(std::vector<CElement *>::iterator i = childElements.begin(); i != childElements.end(); i++)
	{	
		pElement = *i;
		pTemp = pElement->Name();
		if(NULL != pTemp)
		{
			if( ACE_OS::strncmp("configure", pTemp, ACE_OS::strlen("configure")) == 0 )
			{
				m_flag = (STRING_TO_NUM(pElement->GetAttr("enable")) != 0) ? true : false;
				continue;
			}
			
			if( ACE_OS::strncmp("pseudoWire", pTemp, ACE_OS::strlen("pseudoWire")) == 0 )
			{	
				pwIndex = STRING_TO_NUM(pElement->GetAttr("index"));		
				const char *psName = pElement->GetAttr("name");
				unsigned char state = STRING_TO_NUM(pElement ->GetAttr("state"));

				PseudowireArg_i *pPWArg = FindPWItem(pwIndex);
				if(NULL == pPWArg)
				{
					pPWArg = NEW PseudowireArg_i;
					if(NULL != pPWArg)
					{
						pPWArg->idx = pwIndex;
						pPWArg->state = (PseudowireArg_i::ePWState)state;
						SafeStrCpy(pPWArg->name, psName);
						
						m_pwArgs.insert(make_pair(pwIndex, pPWArg));
					}
				}
				else
				{
					D_ERROR("伪线%d信息重复\n", pwIndex);
				}

				continue;
			}

			if( ACE_OS::strncmp("map", pTemp, ACE_OS::strlen("map")) == 0)
			{
				unsigned short mapid = STRING_TO_NUM(pElement->GetAttr("mapID"));
				unsigned short newThreadLimit = STRING_TO_NUM(pElement->GetAttr("newThreadLimit"));
				unsigned short banPoolLimit = STRING_TO_NUM(pElement->GetAttr("banPoolLimit"));
				unsigned short banVipLimit = STRING_TO_NUM(pElement->GetAttr("banVipLimit"));

				PseudowireMapArg_i *pPWMapInfo = FindPWMapItem(pwIndex, mapid);
				if(NULL == pPWMapInfo)
				{
					PseudowireMapArg_i *pPWMapArg = NEW PseudowireMapArg_i;
					if(NULL != pPWMapInfo)
					{
						pPWMapArg->pwid = pwIndex;
						pPWMapArg->mapid = mapid;
						pPWMapArg->newCondition = newThreadLimit;
						pPWMapArg->poolLimit = banPoolLimit;
						pPWMapArg->vipLimit = banVipLimit;
						pPWMapArg->state = PseudowireMapArg_i::PWM_UNACTIVE;

						m_pwMapArgs.insert(make_pair(GEN_DOUBLE_WORD(pwIndex, mapid), pPWMapArg));
					}
				}
				else
				{
					D_ERROR("伪线地图(%d,%d)信息重复\n", pwIndex, mapid);
				}

				continue;
			}
		}
	}

	return true;
	TRY_END;
	return false;
}

void CPseudowireManager::Clear(void)
{
	for(std::map<unsigned short, PseudowireArg_i*>::iterator iter = m_pwArgs.begin(); m_pwArgs.end() != iter; ++iter)
	{
		if ( NULL != iter->second )
		{
			delete iter->second;
			iter->second = NULL;
		}
	}

	m_pwArgs.clear();

	for(std::map<unsigned int, PseudowireMapArg_i*>::iterator iter = m_pwMapArgs.begin(); m_pwMapArgs.end() != iter; ++iter)
	{
		delete iter->second;
		iter->second = NULL;
	}

	m_pwMapArgs.clear();

	return;
}

inline bool CPseudowireManager::IsPWEnable(void)
{
	return m_flag;
}

void CPseudowireManager::ReportAllPWInfo(CGateSrv* pOwner)
{
	//报告伪线信息
	ReportPseudoWireInfo_i(pOwner);

	//报告伪线地图信息
	ReportPseudoWireMapInfo_i(pOwner);

	return;
};

void CPseudowireManager::ReportPseudoWireInfo_i(CGateSrv* pOwner)
{
	if(NULL == pOwner)
		return;

	MGReportPWInfos msg;
	unsigned char count = 0;
	for(std::map<unsigned short, PseudowireArg_i *>::iterator iter = m_pwArgs.begin(); m_pwArgs.end() != iter; ++iter)
	{
		if ( NULL == iter->second )
		{
			continue;
		}
		msg.pwList[count] = *(iter->second);
		count++;

		if(sizeof(msg.pwList)/sizeof(msg.pwList[0]) == count)
		{
			msg.count = count;
			MsgToPut *pMsg = CreateSrvPkg( MGReportPWInfos, pOwner, msg );
			pOwner->SendPkgToGateServer( pMsg );

			count = 0;
		}
	}

	if(0 != count)
	{
		msg.count = count;
		MsgToPut *pMsg = CreateSrvPkg( MGReportPWInfos, pOwner, msg );
		pOwner->SendPkgToGateServer( pMsg );
	}

	return;
}

void CPseudowireManager::ReportPseudoWireMapInfo_i(CGateSrv* pOwner)
{
	if(NULL == pOwner)
		return;

	MGReportPWMapInfos msg;
	unsigned char count = 0;
	for(std::map<unsigned int, PseudowireMapArg_i*>::iterator iter = m_pwMapArgs.begin(); m_pwMapArgs.end() != iter; ++iter)
	{
		if ( NULL == iter->second )
		{
			continue;
		}
		msg.pwMapList[count] = *(iter->second);
		count++;
		
		if(sizeof(msg.pwMapList)/sizeof(msg.pwMapList[0]) == count)
		{
			msg.count = count;
			MsgToPut *pMsg = CreateSrvPkg( MGReportPWMapInfos, pOwner, msg );
			pOwner->SendPkgToGateServer( pMsg);

			count = 0;
		}
	}

	if( 0 != count)
	{
		msg.count = count;
		MsgToPut *pMsg = CreateSrvPkg( MGReportPWMapInfos, pOwner, msg );
		pOwner->SendPkgToGateServer( pMsg );
	}

	return;
}

PseudowireArg_i* CPseudowireManager::FindPWItem(unsigned short pwIdx)
{
	std::map<unsigned short, PseudowireArg_i*>::iterator iter = m_pwArgs.find(pwIdx);
	if(m_pwArgs.end() != iter)
		return iter->second;
	
	return NULL;
}

PseudowireMapArg_i* CPseudowireManager::FindPWMapItem(unsigned short pwIdx, unsigned short mapid)
{
	std::map<unsigned int, PseudowireMapArg_i*>::iterator iter = m_pwMapArgs.find(GEN_DOUBLE_WORD(pwIdx, mapid));
	if(m_pwMapArgs.end() != iter)
		return iter->second;

	return NULL;
}

