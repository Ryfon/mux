﻿#include "Creature.h"
#include "LogicAIEngine.h"
#include "Player2.h"

namespace logicai
{
	static const std::string NewCreatureIdent()
	{
		static unsigned num = 0;
		char buf[32];
		sprintf(buf, "__creature_%u", ++num);
		return buf;
	}

	Creature::Creature(const unsigned ID)
		: GameObj(ID), ident_(NewCreatureIdent())
		, radius_(10.0f)
		, cureAttackInterval_(0)
		, speciSkillId_(0)
		, skillMode_(0)
		, notifySkillCasting_(false)
		, curHp_(0)
		, chaseSpeed_(5.5f)
		, escapeSpeed_(3.0f)
		, gohomeSpeed_(3.5f)
		, followingSpeed_(3.5f)
		, wanderSpeed_(1.0f)
		, patrolSpeed_(1.0f)
		, movingMobility_(true)
		, attackMobility_(true)
		, activeMobility_(false)
		, isFighting_(false)
		, bound_(false)
		, fait_(false)
		, notifyHpChanged_(false)
		, notifyAttacked_(false)
		, notifyLuaLost_(true)
		, disableTime_(0)
		, liveTime_(0)
		, notifyThreatList_(false)
		, aiPattern_(0)
		, escapeAbility_(false)
		, chaseAbility_(true)
		, isUpdate_(false)
		, chaseDist_(25.0f)
		, boundTime_(0)
		, notifyLuaView_(false)
		, notifyName_(false)
		, notifyWayPoint_(false)
		, maxMapX_(1.0f)
		, maxMapY_(1.0f)
		, alertingInterval_(0)
		, attackInterval_(0)
		, sight_(0)
		, notifyLuaUpdate_(false)
		, isDefend_(false)
		, nken_(0)
		, speedDown_(-1)
		, diedReason_(0)
	{
		speciStr_.first = 0;
		actions_.reserve(20);
	}

	Creature::~Creature()
	{
		// 假设：有A，B两个Creature，A的viewlist里面引用了B，
		// 当调用lua_close时，B被Lua栈首先释放，在释放A时，A清理viewlist导致B释放，但B已释放了，产生致命错误,
		// 但Reset已经将viewlist清空了，这个问题很蹊跷！
		/*for(std::vector<ObjPtr>::iterator it = viewList_.begin(); it != viewList_.end(); ++it)
			(*it).release();*/
	}

	void Creature::Rename(const std::string name)
	{
		name_ = name;
		notifyName_ = true;

		for(std::vector<ObjPtr>::const_iterator it =  viewList_.begin(); it != viewList_.end(); ++it)
		{
			if ((*it)->IsPlayer())
				CMonsterWrapper::ChangeMonsterName(GetMapInfo(), (*it)->GetMPPid(), GetId(), name.c_str());
		}
	}

	void Creature::SetMapInfo(const AIMapInfo & info)
	{
		mapInfo_ = info;
	}

	void Creature::SetRadius(float f)
	{
		if (f > 0)
			radius_ = f;
	}

	const position Creature::GetBornPos()const
	{
		return spawnedPos_;
	}

	const AIMapInfo & Creature::GetMapInfo()const
	{
		return mapInfo_;
	}

	const float Creature::GetMoveRadius()const
	{
		return radius_;
	}

	const std::string & Creature::GetName()const
	{
		return name_;
	}

	const std::string & Creature::GetIdent()const
	{
		return ident_;
	}

	const position & Creature::GetCurPos()const
	{
		return curPos_;
	}

	const unsigned Creature::GetSize() const
	{
		return size_;
	}

	const unsigned Creature::GetClassId()const
	{
		return type_;
	}

	const unsigned Creature::GetGateId()const
	{
		return 1000;
	}

	const MUX_PROTO::PlayerID Creature::GetMPPid()const
	{
		MUX_PROTO::PlayerID id;
		id.wGID = GetGateId();
		id.dwPID = GetId();
		return id;
	}

	const unsigned Creature::GetRace() const
	{
		return race_;
	}

	const bool Creature::GetEvil()const
	{
		return false;
	}

	const bool Creature::GetRogue()const
	{
		return false;
	}

	const bool Creature::IsCanBeAttacked()const
	{
		return isBeAttacked_;
	}

	const unsigned Creature::GetInherentStand()const
	{
		return inherentStand_;
	}

	const bool Creature::IsPlayer()const
	{
		return false;
	}

	const bool Creature::GetActiveFlag()const
	{
		return activeMobility_;
	}

	void Creature::SetActive(bool active)
	{
		activeMobility_ = active;
	}

	const bool Creature::IsInThreatList(const ObjPtr & obj)const
	{
		return threats_.HasCheck(obj);
	}

	void Creature::SetCurPos(const position & pos)
	{
		curPos_ = pos;
	}

	void Creature::SetCurPos(const float x, const float y)
	{
		curPos_.x = x;
		curPos_.y = y;
	}

	const unsigned Creature::GetMapId()const
	{
		return mapInfo_.mapID;
	}

	std::vector<ObjPtr> & Creature::GetViewObjList()
	{
		return viewList_;
	}

	void Creature::OnOtherBeyondView(GameObj & obj)
	{
		//LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void (GameObj *)>("OnOtherBeyondView")(&obj);
	}

	void Creature::OnAttacked(ObjPtr & attacker, unsigned reactTime, unsigned threat, bool debuf)
	{
		if (curHp_ > threat)
			curHp_ -= threat;
		else
			curHp_ = 0;

		if (!attackMobility_ || !attacker.get())
			return;

		AddThreat(attacker.get(), threat, debuf);		

		/*MUX_PROTO::TYPE_POS px, py;
		if (1 == CMonsterWrapper::GetMonsterPosition(GetMapInfo(), GetId(), px, py))
			SetCurPos(px, py);*/

		if (!isFighting_)
			Fight(reactTime);

		if (notifyHpChanged_ && threat)
		{
			LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void (float)>("_HandleHpChanged")((float)curHp_ / (float)maxHp_);
		}

		if (notifyAttacked_)
		{
			LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleAttacked")(attacker.get());
		}

	}

	void Creature::OnBorn()
	{
		TYPE_POS x, y;
		CMonsterWrapper::GetSpawnedPoint(mapInfo_, GetId(), x, y);
		spawnedPos_.x = x;
		spawnedPos_.y = y;

		curPos_ = spawnedPos_;

		CMonsterWrapper::GetMonsterSize(mapInfo_, GetId(), size_);
		if (size_ == 0)
			size_ = 1;
		else if (size_ == 1)
			size_ = 3;

		CMonsterWrapper::GetMonsterType(mapInfo_, GetId(), type_);
		CMonsterWrapper::GetMonsterInitHp(mapInfo_, GetId(), maxHp_);

		CMonsterWrapper::GetMonsterAttackInterval(mapInfo_, GetId(), attackInterval_);

		CMonsterWrapper::GetMonsterLevel(mapInfo_, GetId(), level_);
		CMonsterWrapper::GetMonsterRaceAttribute(mapInfo_, GetId(), race_);
		CMonsterWrapper::GetIsCanBeAttack(mapInfo_, GetId(), isBeAttacked_);
		CMonsterWrapper::GetInherentStand(mapInfo_, GetId(), inherentStand_);

		aiPattern_ = CMonsterWrapper::GetMonsterAIType(mapInfo_, GetId());

		unsigned aggressive = 0;
		CMonsterWrapper::GetIsAggressive(mapInfo_, GetId(), aggressive);
		activeMobility_ = aggressive != 0;

		curHp_ = maxHp_;
		InitSkills();

		MUX_PROTO::TYPE_POS maxX = 1, maxY = 1;
		CMonsterWrapper::GetMapExtent(mapInfo_, maxX, maxY);
		maxMapX_ = maxX;
		maxMapY_ = maxY;

		CMonsterWrapper::GetMonsterKen(mapInfo_, GetId(), nken_);

		name_ = CMonsterWrapper::GetMonsterName(mapInfo_, GetId());
		LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void ()>("_HandleBorn")();
	}

	void Creature::OnDied()
	{
		Notify_LostEvent();
		LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void ()>("_HandleDied")();

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			Action_base & action = actions_[i].Get<Action_base>();
			action.Stop();
			action.~Action_base();
		}
		actions_.clear();

		threats_.Clear();

		viewList_.clear();
	}

	void Creature::OnLost()
	{
		Notify_LostEvent();
		LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void ()>("_HandleLost")();

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			Action_base & action = actions_[i].Get<Action_base>();
			action.Stop();
			action.~Action_base();
		}
		actions_.clear();

		threats_.Clear();

		viewList_.clear();
	}

	void Creature::OnUpdate(unsigned)
	{
		bool goon = true;
		isUpdate_ = true;

		ACE_Time_Value now = ACE_OS::gettimeofday();

		unsigned long elapsed = (now - lastUpdateTime_).msec();

		liveTime_ += elapsed;

		if (elapsed > 600) 
			elapsed = 600;

		if (disableTime_)
		{
			if (disableTime_ > elapsed)
			{
				disableTime_ -= elapsed;
				goon = false;
			}
			else
			{
				disableTime_ = 0;
				if (!actions_.empty() && !boundTime_)
					actions_.back().Get<Action_base>().Resume();
			}
		}
		
		if (boundTime_)
		{
			if (boundTime_ > elapsed)
			{
				boundTime_ -= elapsed;
				goon = false;
			}
			else
			{
				boundTime_ = 0;
				if (!actions_.empty() && !disableTime_)
					actions_.back().Get<Action_base>().Resume();
			}
		}


		if (activeMobility_)
		{
			if (goon && alertingInterval_ == 0)
				ActiveAttackAlerting();

			++alertingInterval_;
			if (alertingInterval_ > 1)
				alertingInterval_ = 0;
		}

		while(!actions_.empty() && goon)
		{
			if (!actions_.back().Get<Action_base>().IsEnd())
				break;

			actions_.back().Get<Action_base>().OnLeave();
			actions_.back().Get<Action_base>().~Action_base();
			actions_.resize(actions_.size() - 1);

			if (!actions_.empty() && !actions_.back().Get<Action_base>().IsEnd())
			{
				actions_.back().Get<Action_base>().Resume();
				break;
			}
		}

		if (actions_.size() && goon)
		{
			for(size_t i = 0; i < actions_.size(); ++i)
			{
				actions_[i].Get<Action_base>().Run(elapsed);
			}
		}

		if (!disableTime_ && notifyLuaUpdate_)
			LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void (unsigned)>("_HandleUpdate")(elapsed);

		lastUpdateTime_ = now;

		if (speedDown_ != -1)
		{
			for(size_t i = 0; i < actions_.size(); ++i)
				actions_[i].Get<Action_base>().HandleSpeedDown(speedDown_);
			speedDown_ = -1;
		}

		isUpdate_ = false;
	}

	void Creature::OnReceivedDiedEvent(ObjPtr & obj)
	{
		for(std::vector<ActionHolder>::iterator it = actions_.begin(); it != actions_.end(); ++it)
		{
			it->Get<Action_base>().OnLostObj(obj);
		}

		threats_.RemoveObj(obj);

		LeaveView(obj);

		if (notifyLuaLost_)
			LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleLostTarget")(obj.get());
	}

	void Creature::OnReceivedLostEvent(ObjPtr & obj)
	{
		for(std::vector<ActionHolder>::iterator it = actions_.begin(); it != actions_.end(); ++it)
		{
			it->Get<Action_base>().OnLostObj(obj);
		}

		if (threats_.FirstCheck(obj))
		{
			threats_.RemoveObj(obj);
			threats_.ResetMaxSize(threats_.MaxSize() - 1);
		}
		else
		{
			threats_.RemoveObj(obj);
		}

		LeaveView(obj);

		if (notifyLuaLost_)
			LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleLostTarget")(obj.get());
	}

	void Creature::EnterView(ObjPtr & obj)
	{
		if (obj.get() != this)
		{
			if (viewList_.end() == std::find(viewList_.begin(), viewList_.end(), obj))
			{
				viewList_.push_back(obj);
				this->Observe_LostEvent(obj.get(), 0);

				if (obj->IsPlayer())
				{
					if (notifyName_)
						CMonsterWrapper::ChangeMonsterName(this->GetMapInfo(), obj->GetMPPid(), this->GetId(), name_.c_str());
					if (speciStr_.first)
						CMonsterWrapper::SendSpecialString(this->GetMapInfo(), obj->GetMPPid(), this->GetId(), speciStr_.first, speciStr_.second.c_str());
				}
			}

			if (notifyLuaView_ && obj->IsPlayer())
			{
				LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleObjEnterView")(obj.get());
			}
		}
	}

	void Creature::LeaveView(ObjPtr & obj)
	{
		viewList_.erase(std::remove(viewList_.begin(), viewList_.end(), obj), viewList_.end());

		if (notifyLuaView_ && obj->IsPlayer())
		{
			LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleObjLeaveView")(obj.get());
		}
	}

	void Creature::LeaveMap(ObjPtr & obj)
	{
		for(size_t i = 0; i < actions_.size(); ++i)
		{
			Action_base & action = actions_[i].Get<Action_base>();
			if (action.GetType() == ACTION_MOVE_FOLLOWED_TASK || 
				action.GetType() == ACTION_MOVE_FOLLOWING_TASK)
			{	
				action.OnLeave();
				actions_.erase(actions_.begin() + i);
				SetDieState(0);

				if (action.GetType() == ACTION_MOVE_FOLLOWED_TASK)
				{
					ActionFollowedTask * task = dynamic_cast<ActionFollowedTask *>(&action);
					if (task)
						task->GetTask().SetTaskFailed();
				}
				break;
			}
		}
	}

	void Creature::ChangeState(int state)
	{
		
	}

	void Creature::ChangeMoveState(int state)
	{
		//move_.ChangeMove(state);
	}

	void Creature::ChangeFightState(int state)
	{
		//fight_.ChangeFight(state);
	}

	void Creature::AddThreat(GameObj * obj, unsigned threat, bool)
	{
		if (obj && obj != this)
		{
			ObjPtr target(obj);
			if (1 == threats_.AddObj(target, threat))
				this->Observe_LostEvent(obj, 0);

			// TODO 强制攻击一个目标N秒

			//fight_.target_ = fight_.threatList_.GetMaxThreat();

			if (notifyThreatList_)
			{
				_ThreatList thlist;
				threats_.Dump(thlist);
				CMonsterWrapper::SendThreatList(this->GetMapInfo(), this->GetId(), thlist);
			}
		}
	}

	const bool Creature::ThreatListEmpty()const
	{
		return threats_.Size() == 0;
	}

	int Creature::GetAttackInterval()const
	{
		return attackInterval_;
	}

	unsigned Creature::GetSpeciSkill()const
	{
		return speciSkillId_;
	}

	void Creature::SetSpeciSkill(unsigned s)
	{
		speciSkillId_ = s;
	}

	int Creature::GetSkillMode()const
	{
		return skillMode_;
	}

	void Creature::SetSkillMode(int m, unsigned skillId)
	{
		if (m != SELECT_SPECI)
			skillMode_ = m; 
		speciSkillId_ = skillId;
	}

	ThreatList & Creature::GetThreatList()
	{
		return threats_;
	}

	void Creature::ClearThreatList()
	{
		threats_.Clear();

		if (notifyThreatList_)
		{
			_ThreatList thlist;
			threats_.Dump(thlist);
			CMonsterWrapper::SendThreatList(this->GetMapInfo(), this->GetId(), thlist);
		}
	}

	unsigned Creature::GetCurAttackInterval()const
	{
		return cureAttackInterval_;
	}

	void Creature::SetNotifySkillCasting(bool b)
	{
		notifySkillCasting_ = b;
	}

	void Creature::SetCurAttackInterval(unsigned t)
	{
		cureAttackInterval_ = t;
	}

	bool Creature::IsNotifySkillCasting()const
	{
		return notifySkillCasting_;
	}

	bool Creature::IsInsight(ObjPtr & obj)
	{
		if (obj.get())
		{
			return viewList_.end() != std::find(viewList_.begin(), viewList_.end(), obj);
		}
		return false;
	}

	void Creature::SetNotifySkillCastingFlag(bool f)
	{
		notifySkillCasting_ = f;
	}

	void Creature::ClearAllBuffs(bool buf)
	{	
		CMonsterWrapper::ClearMonsterBuff(this->GetMapInfo(), this->GetId(), buf);
	}

	void Creature::SetHpPercent(float f)
	{
		if (f < 0 || f > 100.0f) return;

		f /= 100.0f;
		CMonsterWrapper::SetMonsterHpPercent(this->GetMapInfo(), this->GetId(), f);
		curHp_ = (unsigned)(maxHp_ * f);
	}

	float Creature::GetHpPercent()const
	{
		return (float)curHp_ / maxHp_;
	}

	float Creature::GetChaseSpeed()
	{
		return chaseSpeed_;
	}

	float Creature::GetEscapeSpeed()
	{
		return escapeSpeed_;
	}

	float Creature::GetGoHomeSpeed()
	{
		return gohomeSpeed_;
	}

	void Creature::OnFaitBegin()
	{
		D_DEBUG("monster[%u type:%u]begin fait\n", GetId(), GetClassId());

		fait_ = true;

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			actions_[i].Get<Action_base>().OnFaitBegin();
		}
	}

	void Creature::OnFaitEnd()
	{
		D_DEBUG("monster[%u type:%u]stop fait\n", GetId(), GetClassId());
		fait_ = false;

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			actions_[i].Get<Action_base>().OnFaitEnd();
		}
	}

	void Creature::OnBoundBegin()
	{
		D_DEBUG("monster[%u type:%u]begin bound\n", GetId(), GetClassId());
		bound_ = true;

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			actions_[i].Get<Action_base>().OnBoundBegin();
		}
	}

	void Creature::OnBoundEnd()
	{
		D_DEBUG("monster[%u type:%u]stop bound\n", GetId(), GetClassId());

		bound_ = false;

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			actions_[i].Get<Action_base>().OnBoundEnd();
		}
	}

	float Creature::GetFollowingSpeed()
	{
		return followingSpeed_;
	}

	float Creature::GetWanderSpeed()
	{
		return wanderSpeed_;
	}

	float Creature::GetPatrolSpeed()
	{
		return patrolSpeed_;
	}

	CMuxSkillProp * Creature::GetSkill(ObjPtr & target)
	{
		if (!speciSkillId_)
		{
			if (skillMode_ == SELECT_SERI)
				skills_.SeriSelectSkill();
			else if (skillMode_ == SELECT_RANDOM)
				skills_.RandomSelectSkill();	
			else if (skillMode_ == SELECT_SMART)
			{
				position targetPos = target->GetCurPos();
				float dx = this->GetCurPos().x - targetPos.x;
				float dy = this->GetCurPos().y - targetPos.y;
				skills_.SmartSelectSkill(dx * dx + dy * dy);
			}
		}
		else
		{
			CMuxSkillProp * skill = 0;
			CMonsterWrapper::GetMonsterSkillProp(speciSkillId_, skill);
			if (!skill)
			{
				D_ERROR("error skill id:%u\n", speciSkillId_);
				skills_.SeriSelectSkill();
			}
			else
			{ 
				return skill;
			}
		}

		if (skills_.GetCurrentSkill())
		{
			speciSkillId_ = skills_.GetCurrentSkill()->skill->m_skillID;
			return skills_.GetCurrentSkill()->skill;
		}
		else
		{
			speciSkillId_ = 0;
			return 0;
		}
	}

	void Creature::AddSkill(unsigned skillId)
	{
		CMuxSkillProp * skill = 0;
		CMonsterWrapper::GetMonsterSkillProp(skillId, skill);
		if (!skill)
		{
			D_ERROR("error skill id:%u\n", skillId);
		}
		else
		{ 
			skills_.AddSkill(skillId, 0);
		}
	}

	void Creature::RemoveSkill(unsigned skillId)
	{
		skills_.RemoveSkill(skillId);
	}

	void Creature::SetMoveMode(int mode)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		AI_ASSERT(actions_.empty());

		switch(mode)
		{
		case ACTION_MOVE_IDLE:
			{
				ActionHolder idle;
				CreaturePtr creature(this);
				new(idle.data) ActionIdle(creature);
				actions_.push_back(idle);
			}
			break;
		case ACTION_MOVE_WANDER:
			{
				ActionHolder wander;
				CreaturePtr creature(this);
				new(wander.data) ActionWander(creature);
				actions_.push_back(wander);
				actions_.back().Get<Action_base>().Run(0);
			}
			break;
		}
	}

	void Creature::SetMoveIdle()
	{
		SetMoveMode(ACTION_MOVE_IDLE);
	}

	void Creature::SetMoveWander()
	{
		SetMoveMode(ACTION_MOVE_WANDER);
	}

	void Creature::SetMovePatrol(std::vector<position> wayPoints, int mode)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		AI_ASSERT(actions_.empty());

		ActionHolder holder;
		CreaturePtr creature(this);
		new(holder.data) ActionPatrol(creature, wayPoints, mode);
		holder.Get<ActionPatrol>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::Fight(unsigned reactTime)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (isFighting_ || !attackMobility_ || isDefend_)
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder fight;
		CreaturePtr creature(this);
		new(fight.data) ActionFight(creature, reactTime);
		actions_.push_back(fight);
		actions_.back().Get<Action_base>().OnEnter();
		isFighting_ = true;
	}

	void Creature::Help()
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (!isFighting_)
			return;

		if (viewList_.empty())
			return;

		CreaturePtr target(0);
		std::sort(viewList_.begin(), viewList_.end(), DistCheckPred(*this));
		for(size_t i = 0; i < viewList_.size(); ++i)
		{
			if (viewList_[i]->IsPlayer())
			{
				target = CreaturePtr(dynamic_cast<Creature *>(viewList_[i].get()));
				if (target.get() && !target->IsFighting() && target->GetClassId() == this->GetClassId())
					break;
				else
					target.reset();
			}
		}

		if (!target.get())
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder help;
		CreaturePtr creature(this);
		new(help.data) ActionHelp(creature, target);
		help.Get<Action_base>().OnEnter();
		actions_.push_back(help);
	}

	void Creature::MoveTo(const position pos, float speed)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		new(holder.data) ActionMoveTo(creature, pos, speed);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::Treat(GameObj * obj, unsigned skillId)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (!obj)
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		ObjPtr target(obj);
		new(holder.data) ActionTreat(creature, target, skillId);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::Follow(GameObj * obj)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (!obj)
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		ObjPtr target(obj);
		new(holder.data) ActionFollow(creature, target);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::CastAOE(unsigned skillId)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		new(holder.data) ActionCastAOE(creature, skillId);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::CastAOEAt(GameObj * obj, unsigned skillId)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (!obj)
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		ObjPtr target(obj);
		new(holder.data) ActionCastAOEAt(creature, target, skillId);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::Escape()
	{
		if (!isFighting_)
			return;

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			if (actions_[i].Get<Action_base>().GetType() > ACTION_FIGHT_NOOP &&
				actions_[i].Get<Action_base>().GetType() < ACTION_FIGHT_GOHOME)
			{
				actions_[i].Get<ActionFight>().Escape();
			}
		}
	}

	void Creature::Chase()
	{
		if (!isFighting_)
			return;

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			if (actions_[i].Get<Action_base>().GetType() > ACTION_FIGHT_NOOP &&
				actions_[i].Get<Action_base>().GetType() < ACTION_FIGHT_GOHOME)
			{
				actions_[i].Get<ActionFight>().Chase();
			}
		}
	}

	void Creature::GoHome()
	{
		if (!isFighting_)
			return;

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			if (actions_[i].Get<Action_base>().GetType() > ACTION_FIGHT_NOOP &&
				actions_[i].Get<Action_base>().GetType() < ACTION_FIGHT_GOHOME)
			{
				actions_[i].Get<ActionFight>().GoHome();
			}
		}
	}

	void Creature::StopFight()
	{
		if (!isFighting_)
			return;

		for(size_t i = 0; i < actions_.size(); ++i)
		{
			if (actions_[i].Get<Action_base>().GetType() > ACTION_FIGHT_NOOP &&
				actions_[i].Get<Action_base>().GetType() < ACTION_FIGHT_GOHOME)
			{
				actions_[i].Get<Action_base>().Stop();
			}
		}
	}

	void Creature::AttackTarget(GameObj * obj, unsigned attackTime)
	{
		if (actions_.capacity() <= actions_.size())

			return;
		if (!obj)
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		ObjPtr target(obj);
		new(holder.data) ActionAttackTarget(creature, target, attackTime);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::Defend()
	{
		if (actions_.capacity() <= actions_.size())
			return;

		isDefend_ = true;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		new(holder.data) ActionDefend(creature);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::MoveToByWayPoints(std::vector<position> wayPoints)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		ActionHolder holder;
		CreaturePtr creature(this);
		new(holder.data) ActionPatrol(creature, wayPoints, 0, false);
		holder.Get<ActionPatrol>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::InitSkills()
	{
		SkillSet * skills = MonsterSkillSet::Instance()->GetSkillSet(this->GetClassId());
		if (!skills)
			return;

		for(_SkillSet::const_iterator it = skills->m_Skills.begin(); 
			it != skills->m_Skills.end(); ++it)
		{
			skills_.AddSkill(it->m_ID, it->m_weight);
		}
	}

	GameObj * Creature::GetCurAttackTarget()
	{
		for(size_t i = 0; i < actions_.size(); ++i)
		{
			if (actions_[i].Get<Action_base>().GetType() > ACTION_FIGHT_NOOP &&
				actions_[i].Get<Action_base>().GetType() < ACTION_FIGHT_GOHOME)
			{
				actions_[i].Get<ActionFight>().GetAttackTarget();
				break;
			}
		}
		return 0;
	}

	void Creature::ActiveAttackAlerting()
	{
		if (viewList_.empty())
			return;

		for(size_t i = 0; i < viewList_.size(); ++i)
		{
			if (!threats_.HasCheck(viewList_[i]))
			{
				if (!IsCanAttack(viewList_[i]) || !CheckAttackSight(viewList_[i]))
					continue;

				bool res = true;
				//if (GetMapInfo().isNormalOrCopy == false)
					CMonsterWrapper::IsCanPass(GetMapInfo(), GetCurPos().x, GetCurPos().y, 
					viewList_[i]->GetCurPos().x, viewList_[i]->GetCurPos().y, res);

				if (res)
					AddThreat(viewList_[i].get(), 0, false);	
			}

			if (!isFighting_)
				Fight(0);
		}
	}

	bool Creature::IsCanAttack(ObjPtr & target)const
	{
		bool result = false;

		switch(race_)
		{
		case NPC_RACE_NEUTRAL:
			{
				if (target->IsPlayer())
				{
					result = target->GetEvil() || target->GetRogue();
				}
			}
			break;
		case NPC_RACE_HUMAN:
		case NPC_RACE_ERKA:
		case NPC_RACE_ELF:
			{
				if (!target->IsPlayer())
				{
					if (target->GetRace() == NPC_RACE_HUMAN || 
						target->GetRace() == NPC_RACE_ERKA || 
						target->GetRace() == NPC_RACE_ELF)
					{
						if (GetRace() != target->GetRace() && target->IsCanBeAttacked())
							result = true;
					}
					else if (target->GetRace() == NPC_RACE_MONSTER)
					{
						if (target->IsCanBeAttacked())
							result = true;
					}
				}
				else if (GetRace() != target->GetRace())
				{
					result = true;
				}
				else
				{
					result = target->GetEvil() || target->GetRogue();
					if (result)
						result = GetInherentStand() == 1 ? true : false;
				}
			}
			break;
		case NPC_RACE_MONSTER:
			{
				if (!target->IsPlayer())
				{
					if (target->GetRace() == NPC_RACE_HUMAN || 
						target->GetRace() == NPC_RACE_ERKA || 
						target->GetRace() == NPC_RACE_ELF)
						result = target->IsCanBeAttacked();
				}
				else
				{
					result = true;
				}
			}
			break;
		};

		return result;
	}
	
	void Creature::Speek(const std::string s)
	{
		CMonsterWrapper::MonsterSpeak(this->GetMapInfo(), this->GetId(), s.c_str(), CT_SCOPE_CHAT);
	}

	void Creature::Yell(const std::string s)
	{
		CMonsterWrapper::MonsterSpeak(this->GetMapInfo(), this->GetId(), s.c_str(), CT_MAP_BRO_CHAT);
	}

	void Creature::Boradcast(const std::string s)
	{
		CMonsterWrapper::MonsterSpeak(this->GetMapInfo(), this->GetId(), s.c_str(), CT_WORLD_CHAT);
	}

	void Creature::Disable(unsigned t)
	{
		if (!disableTime_ && !boundTime_)
		{
			if (!actions_.empty())
				actions_.back().Get<Action_base>().Pause();
		}
		disableTime_ = t * 1000;
	}

	void Creature::Bound(unsigned t) 
	{
		if (!boundTime_ && !disableTime_)
		{
			if (!actions_.empty())
				actions_.back().Get<Action_base>().Pause();
		}
		boundTime_ = t * 1000; 
	}

	void Creature::Unbound() 
	{ 
		if (boundTime_ && !disableTime_)
		{
			if (!actions_.empty())
				actions_.back().Get<Action_base>().Resume();
		}
		boundTime_ = 0; 
	}

	void Creature::Call(unsigned count, unsigned cls, bool cp)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		new(holder.data) ActionCall(creature, count, cls, cp);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::ExitFight()
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		new(holder.data) ActionExitFight(creature);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::SetMesh(int id)
	{
		CMonsterWrapper::SetMonsterMesh(this->GetMapInfo(), this->GetId(), id);
	}

	void Creature::SetMeshScale(float f)
	{
		CMonsterWrapper::SetMonsterSize(this->GetMapInfo(), this->GetId(), f);
	}

	void Creature::PlaySound(const std::string s, int m)
	{
		CMonsterWrapper::MonsterPlaySound(this->GetMapInfo(), this->GetId(), s.c_str(), (MUX_PROTO::CHAT_TYPE)m);
	}

	void Creature::Summon(unsigned cls, unsigned count, const position pos)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		new(holder.data) ActionSummon(creature, cls, count, pos);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::NotifyBindTaskSuccessed(int f)
	{
		for(size_t i = 0; i < actions_.size(); ++i)
		{
			Action_base & action = actions_[i].Get<Action_base>();
			if (action.GetType() == ACTION_MOVE_FOLLOWED_TASK)
			{	
				ActionFollowedTask * task = dynamic_cast<ActionFollowedTask *>(&action);
				if (task)
					task->GetTask().SetTaskSucceed(f);
			}
			else if (action.GetType() == ACTION_MOVE_FOLLOWING_TASK)
			{
				ActionFollowingTask * task = dynamic_cast<ActionFollowingTask *>(&action);
				if (task)
					task->GetTask().SetTaskSucceed(f);
			}
		}
	}

	void Creature::NotifyBindTaskFail()
	{
		for(size_t i = 0; i < actions_.size(); ++i)
		{
			Action_base & action = actions_[i].Get<Action_base>();
			if (action.GetType() == ACTION_MOVE_FOLLOWED_TASK)
			{	
				ActionFollowedTask * task = dynamic_cast<ActionFollowedTask *>(&action);
				if (task)
					task->GetTask().SetTaskFailed();
			}
			else if (action.GetType() == ACTION_MOVE_FOLLOWING_TASK)
			{
				ActionFollowingTask * task = dynamic_cast<ActionFollowingTask *>(&action);
				if (task)
					task->GetTask().SetTaskFailed();
			}
		}
	}

	void Creature::NotifyBindTaskDroped(unsigned taskId)
	{
		for(size_t i = 0; i < actions_.size(); ++i)
		{
			Action_base & action = actions_[i].Get<Action_base>();
			if (action.GetType() == ACTION_MOVE_FOLLOWED_TASK)
			{	
				ActionFollowedTask * task = dynamic_cast<ActionFollowedTask *>(&action);
				if (task && taskId == task->GetTask().GetTaskId())
					task->GetTask().DropTask();
			}
			else if (action.GetType() == ACTION_MOVE_FOLLOWING_TASK)
			{
				ActionFollowingTask * task = dynamic_cast<ActionFollowingTask *>(&action);
				if (task && taskId == task->GetTask().GetTaskId())
					task->GetTask().DropTask();
			}
		}
	}

	void Creature::SetDieState(int s)
	{
		SetDiedReason(s);
		CreaturePtr creature(this);
		LogicAIEngine::instance().GetObjMgr().AddDiedList(creature);
	}

	void Creature::SetUndefeat(int f)
	{
		CMonsterWrapper::SetMonsterUndefeat(this->GetMapInfo(), this->GetId(), f != 0);
	}

	void Creature::SetPkgDropFlag(bool f)
	{
		CMonsterWrapper::ChangeMonsterIsDrop(this->GetMapInfo(), this->GetId(), f);
	}

	void Creature::SpeciStr(int type, const std::string s)
	{
		for(std::vector<ObjPtr>::const_iterator it =  viewList_.begin(); it != viewList_.end(); ++it)
		{
			if ((*it)->IsPlayer())
				CMonsterWrapper::SendSpecialString(this->GetMapInfo(), (*it)->GetMPPid(), this->GetId(), type, s.c_str());
		}
	}

	void Creature::PersistSpeciStr(int type, const std::string s)
	{
		for(std::vector<ObjPtr>::const_iterator it =  viewList_.begin(); it != viewList_.end(); ++it)
		{
			if ((*it)->IsPlayer())
				CMonsterWrapper::SendSpecialString(this->GetMapInfo(), (*it)->GetMPPid(), this->GetId(), type, s.c_str());
		}
		speciStr_.first = type;
		speciStr_.second = s;
	}

	void Creature::SetNpcSeq(unsigned seq,unsigned status)
	{
		CMonsterWrapper::SetItemNpcSeq(this->GetMapInfo(), this->GetId(), seq, status);
	}

	void Creature::FollowedTaskBegin(GameObj * obj, PosVec wayPoints, unsigned taskId, unsigned waitTime)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		AI_ASSERT(obj && actions_.size() < 2);

		if (!obj)
			return;	

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		ObjPtr target(obj);
		new(holder.data) ActionFollowedTask(creature, target, wayPoints, taskId, waitTime);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	void Creature::FollowingTaskBegin(GameObj * obj, unsigned taskId, unsigned waitTime)
	{
		if (actions_.capacity() <= actions_.size())
			return;

		AI_ASSERT(obj && actions_.size() < 2);

		if (!obj)
			return;

		if (!actions_.empty())
			actions_.back().Get<Action_base>().Pause();

		ActionHolder holder;
		CreaturePtr creature(this);
		ObjPtr target(obj);
		new(holder.data) ActionFollowingTask(creature, target, taskId, waitTime);
		holder.Get<Action_base>().OnEnter();
		actions_.push_back(holder);
	}

	bool Creature::IsCanRun(Action_base * p)const
	{
		if (!p) return false;
		if (actions_.empty()) return true;

		if (actions_.back().data == (char *)p)
			return true;
		return false;
	}

	float CheckSightMap[] = 
	{
		10.0f, 10.0f, 11.0f, 13.0f, 14.0f, 15.0f, 16.0f, 17.0f, 18.0f, 18.0f, 18.0f
	};

	float CheckSightMap2[] =
	{
		10.0f, 10.0f, 9.0f, 8.0f, 8.0f, 8.0f, 8.0f, 8.0f, 8.0f, 8.0f, 8.0f
	};

	bool Creature::CheckAttackSight(ObjPtr & obj)const
	{
		/*
		int dv = this->GetLevel() - obj->GetLevel();
		if (dv > 10)
			return true;

		const position objPos = obj->GetCurPos();
		float dx = this->GetCurPos().x - objPos.x;
		float dy = this->GetCurPos().y - objPos.y;
		float sz = (float)size_;
		sz /= 2.0f;

		float sight = 0;
		if (dv >= 0)
			sight = CheckSightMap[dv] + nken_ + sz;
		else if (dv > -11)
			sight = CheckSightMap2[-dv] + nken_ + sz;
		else
			sight = 8.0f + nken_ + sz;

		if (sight < 1.0f )
			sight = 1.0f;

		return (dx * dx + dy * dy) <= sight * sight;
		*/

		const position objPos = obj->GetCurPos();
		float dx = this->GetCurPos().x - objPos.x;
		float dy = this->GetCurPos().y - objPos.y;
		if (dx > 0)
			dx += (size_ / 2.0f);
		else
			dx -= (size_ / 2.0f);
		if (dy > 0)
			dy += (size_ / 2.0f);
		else
			dy -= (size_ / 2.0f);

		float diff2 = dx * dx + dy * dy;
		float sight = (float)sight_ + (float)nken_;
		if (sight < LogicAIEngine::min_alert_dist)
			sight = LogicAIEngine::min_alert_dist;

		if (diff2 > sight * sight)
			return false;
		
		int dv = this->GetLevel() - obj->GetLevel();
		if (dv < 0)
		{
			sight += dv;
			if (sight < LogicAIEngine::min_alert_dist)
				sight = LogicAIEngine::min_alert_dist;
		}

		return diff2 <= sight * sight;
	}

	void Creature::NotifyLevelMapInfo(unsigned count)
	{
		LogicAIEngine::NotifyLevelMapInfo(this->GetMapInfo(), count);
	}

	void Creature::SetNotifyLuaUpdate(bool b)
	{
		notifyLuaUpdate_ = b; 
	}

	void Creature::HandleSpeedDown(int val)
	{
		speedDown_ = val;
	}

	void Creature::Reset()
	{
		this->observers_.clear();
		this->observing_.clear();
		viewList_.clear();
		threats_.Clear();
		for(std::size_t i = 0; i < actions_.size(); ++i)
			actions_[i].Get<Action_base>().~Action_base();
		actions_.clear();
	}

	void Creature::HandleThreatCured(ObjPtr caster, ObjPtr target, unsigned val)
	{
		if (threats_.HasCheck(target))
		{
			if (1 == threats_.AddObj(caster, val))
				this->Observe_LostEvent(caster.get(), 0);

			
			if (notifyThreatList_)
			{
				_ThreatList thlist;
				threats_.Dump(thlist);
				CMonsterWrapper::SendThreatList(this->GetMapInfo(), this->GetId(), thlist);
			}
		}
	}

	void Creature::SynchThreatList(GameObj * _creature)
	{
		Creature * creature = dynamic_cast<Creature *>(_creature);
		if (creature)
		{
			for(size_t c = 0; c < GetThreatList().Size(); ++c)
				creature->AddThreat(GetThreatList().Get(c).get(), 0, false);	
			creature->Fight(0);
		}

	}
}