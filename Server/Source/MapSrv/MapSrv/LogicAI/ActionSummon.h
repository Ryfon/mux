﻿#pragma once

#include <map>

#include "Action.h"

namespace logicai
{
	class ActionSummon : public Action_base
	{
	public:
		ActionSummon(CreaturePtr &, unsigned, unsigned, const position &);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnPause() { }

		virtual void OnFaitBegin() { }

		virtual void OnFaitEnd()  { }

		virtual void OnBoundBegin() { }

		virtual void OnBoundEnd() { }

		virtual void OnLostObj(ObjPtr &) { }

		virtual void OnUpdate(unsigned elapsed) { }

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return ACTION_ASSIST_SUMMON; }

		static void MonsterSummonedCallback(Creature & creature, unsigned flag);

	private:
		unsigned class_;
		position pos_;
		unsigned action_id_;
		unsigned count_;

		static unsigned action_count_;
		static std::map<unsigned, ActionSummon *> actions_;
	};
} //

