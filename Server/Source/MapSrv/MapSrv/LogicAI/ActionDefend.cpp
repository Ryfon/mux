﻿#include "ActionDefend.h"
#include "Creature.h"
#include "ActionFight.h"
#include "LogicAIEngine.h"

namespace logicai
{
	ActionDefend::ActionDefend(CreaturePtr & creature)
		: Action_base(creature)
		, target_(0)
		, curseTime_(0)
		, reactTime_(0)
	{

	}

	void ActionDefend::OnEnter()
	{

	}

	void ActionDefend::OnLeave()
	{
		Action_base::OnLeave();
	}

	void ActionDefend::OnPause()
	{

	}

	void ActionDefend::OnResume()
	{

	}

	void ActionDefend::OnStop()
	{
		Action_base::OnStop();
	}
	
	void ActionDefend::OnFaitBegin() 
	{

	}

	void ActionDefend::OnFaitEnd()
	{

	}

	void ActionDefend::OnBoundBegin() 
	{

	}

	void ActionDefend::OnBoundEnd() 
	{

	}

	void ActionDefend::OnLostObj(ObjPtr & obj)
	{
		if (target_.get())
		{
			AI_ASSERT(obj == target_);

			target_.reset();
		}
	}

	void ActionDefend::OnUpdate(unsigned elapsed)
	{
	}

	void ActionDefend::OnRun(unsigned elapsed)
	{
		unsigned curAttackInterval = creature_->GetCurAttackInterval() + elapsed;
		creature_->SetCurAttackInterval(curAttackInterval);

		if (creature_->ThreatListEmpty())
		{
			return;
		}

		unsigned needAttackInterval = curseTime_ + creature_->GetAttackInterval();
		if (curAttackInterval < needAttackInterval)
		{
			return;
		}

		if (!target_.get())
		{
			target_ = creature_->GetThreatList().GetMinDist();
		}

		CMuxSkillProp * skill = creature_->GetSkill(target_);
		if (!skill)
		{
			this->Stop();
			return ;
		}

		if (!ActionFight::CheckMaxAttackDist(creature_, target_, *skill))
		{
			creature_->GetThreatList().RemoveObj(target_);
			target_.reset();
			return;
		}

		int result = CMonsterWrapper::UseSkill(creature_->GetMapInfo(), creature_->GetId(), 
			target_->GetMPPid(), skill->m_skillID);
		if (-1 != result && 0 != result)
		{
			curseTime_ = skill->m_nCurseTime;
			creature_->SetCurAttackInterval(0);
			creature_->SetSpeciSkill(0);

			if (creature_->IsNotifySkillCasting())
			{
				LogicAIEngine::instance().L().Table(
					creature_->GetIdent().c_str()).Mfunction<void (unsigned)>("_HandleSkillCasting")(skill->m_skillID);
			}
		}
	}
}