﻿#pragma once

#include "Ldef.h"
#include "Lclass.h"

__BEGIN_SCRIPTS_L_NS__

class Lconstructor_base
{
protected:
	template<class T>
	static void reg(lua_State * pL, const char * fname, bool G, int (*L_f)(lua_State *))
	{
		Lclass<T>::L_getmetatable(pL);
		lua_getfield(pL, -1, "__internal_this_class_name");
		const char * name = lua_tostring(pL, -1);

		if (G)
		{
			lua_getglobal(pL, name);
			if (lua_isnil(pL, -1))
			{
				lua_pop(pL, 3);
				throw 0;
			}
		}
		else
		{
			lua_getfield(pL, -3, name);
			if (lua_isnil(pL, -1))
			{
				lua_pop(pL, 3);
				throw 0;
			}
		}

		lua_pushcfunction(pL, L_f);
		lua_setfield(pL, -2, fname);
		lua_pop(pL, 3);
	}
};

template<>
class  Lconstructor<void, void, void, void, void, void> : public Lconstructor_base
{
public:
	Lconstructor() { }

private:
	template<class T>
	static int L_new(lua_State * pL)
	{
		T * p = Lclass<T>::L_newuserdata(pL);

		try {
			new(p) T();
		}catch(...)
		{
			lua_pop(pL, 1);
			throw 0;
		}

		Lclass<T>::L_newtable(pL, p);
		return 1;
	}

	template<class T>
	static void reg(lua_State * pL, const char * fname, bool G)
	{
		Lconstructor_base::reg<T>(pL, fname, G, L_new<T>);
	}

	friend class L;
	friend class Ltable;
};

template<class A1>
class  Lconstructor<A1, void, void, void, void, void> : public Lconstructor_base
{
public:
	Lconstructor() { }

private:
	template<class T>
	static int L_new(lua_State * pL)
	{
		A1 arg1 = L_totopvalue_pop<A1>(*pL);

		T * p = Lclass<T>::L_newuserdata(pL);
		try {
			new(p) T(arg1);
		}catch(...)
		{
			lua_pop(pL, 1);
			throw 0;
		}

		Lclass<T>::L_newtable(pL, p);
		return 1;
	}

	template<class T>
	static void reg(lua_State * pL, const char * fname, bool G)
	{
		Lconstructor_base::reg<T>(pL, fname, G, L_new<T>);
	}

	friend class L;
	friend class Ltable;
};

template<class A1, class A2>
class  Lconstructor<A1, A2, void, void, void, void> : public Lconstructor_base
{
public:
	Lconstructor() { }

private:
	template<class T>
	static int L_new(lua_State * pL)
	{
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);

		T * p = Lclass<T>::L_newuserdata(pL);
		try {
			new(p) T(arg1, arg2);
		}catch(...)
		{
			lua_pop(pL, 1);
			throw 0;
		}

		Lclass<T>::L_newtable(pL, p);
		return 1;
	}

	template<class T>
	static void reg(lua_State * pL, const char * fname, bool G)
	{
		Lconstructor_base::reg<T>(pL, fname, G, L_new<T>);
	}

	friend class L;
	friend class Ltable;
};

template<class A1, class A2, class A3>
class  Lconstructor<A1, A2, A3, void, void, void> : public Lconstructor_base
{
public:
	Lconstructor() { }

private:
	template<class T>
	static int L_new(lua_State * pL)
	{
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);

		T * p = Lclass<T>::L_newuserdata(pL);
		try {
			new(p) T(arg1, arg2, arg3);
		}catch(...)
		{
			lua_pop(pL, 1);
			throw 0;
		}

		Lclass<T>::L_newtable(pL, p);
		return 1;
	}

	template<class T>
	static void reg(lua_State * pL, const char * fname, bool G)
	{
		Lconstructor_base::reg<T>(pL, fname, G, L_new<T>);
	}

	friend class L;
	friend class Ltable;
};

__END_SCRIPTS_L_NS__
