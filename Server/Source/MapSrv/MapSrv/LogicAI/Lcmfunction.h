﻿#pragma once

#include "Ldef.h"
#include "Lclass.h"

__BEGIN_SCRIPTS_L_NS__

template<class T>
class Lcmfunction_base
{
protected:
	static void reg(lua_State * pL, const char * fname, int (*L_f)(lua_State *))
	{
		Lclass<T>::L_getmetatable(pL);
		lua_pushcfunction(pL, L_f);
		lua_setfield(pL, -2, fname);
		lua_pop(pL, 1);
	}

	static T * L_getuserdata(lua_State * pL)
	{
		lua_getfield(pL, -1, "__internal_userdata");
		T * p = (T *)lua_touserdata(pL, -1);
		lua_pop(pL, 1);
		return p;
	}
};

template<class T, class R, R (T::*f)()>
class Lcmfunction<T, R (T::*)(), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)();
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, void *)
	{
		CF_LUA_TRY
		(p->*f)();
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, (typename L_unref<R>::result *)0) ;
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};

template<class T, class R, R (T::*f)()const>
class Lcmfunction<T, R (T::*)()const, f> : public Lcmfunction<T, R (T::*)(), (R (T::*)())f>
{
};

template<class R, class T, class A1, R (T::*f)(A1)>
class Lcmfunction<T, R (T::*)(A1), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1);
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 2)
			luaL_error(pL, "invalid arguments count for L_cmfunction1");
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};

template<class T, class R, class A1, R (T::*f)(A1)const>
class Lcmfunction<T, R (T::*)(A1)const, f> : public Lcmfunction<T, R (T::*)(A1), (R (T::*)(A1))f>
{
};


template<class R, class T, class A1, class A2, R (T::*f)(A1, A2)>
class Lcmfunction<T, R (T::*)(A1, A2), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1, arg2);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1, arg2);
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 3)
			luaL_error(pL, "invalid arguments count for L_cmfunction2");
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, arg2, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};


template<class R, class T, class A1, class A2, class A3, R (T::*f)(A1, A2, A3)>
class Lcmfunction<T, R (T::*)(A1, A2, A3), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1, arg2, arg3);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1, arg2, arg3);
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 4)
			luaL_error(pL, "invalid arguments count for L_cmfunction3");
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, arg2, arg3, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};

template<class R, class T, class A1, class A2, class A3, class A4, R (T::*f)(A1, A2, A3, A4)>
class Lcmfunction<T, R (T::*)(A1, A2, A3, A4), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1, arg2, arg3, arg4);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1, arg2, arg3, arg4);
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 5)
			luaL_error(pL, "invalid arguments count for L_cmfunction4");
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);				
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, arg2, arg3, arg4, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};

template<class R, class T, class A1, class A2, class A3, class A4, class A5, R (T::*f)(A1, A2, A3, A4, A5)>
class Lcmfunction<T, R (T::*)(A1, A2, A3, A4, A5), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1, arg2, arg3, arg4, arg5);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4,  A5 arg5, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1, arg2, arg3, arg4, arg5);
		CF_LUA_CATCH
		return 0;
	}


	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 6)
			luaL_error(pL, "invalid arguments count for L_cmfunction5");
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, arg2, arg3, arg4, arg5, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};

template<class R, class T, class A1, class A2, class A3, class A4, class A5, class A6, R (T::*f)(A1, A2, A3, A4, A5, A6)>
class Lcmfunction<T, R (T::*)(A1, A2, A3, A4, A5, A6), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1, arg2, arg3, arg4, arg5, arg6);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4,  A5 arg5, A6 arg6, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1, arg2, arg3, arg4, arg5, arg6);
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 7)
			luaL_error(pL, "invalid arguments count for L_cmfunction6");
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, arg2, arg3, arg4, arg5, arg6, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};

template<class R, class T, class A1, class A2, class A3, class A4, class A5, class A6, class A7,
R (T::*f)(A1, A2, A3, A4, A5, A6, A7)>
class Lcmfunction<T, R (T::*)(A1, A2, A3, A4, A5, A6, A7), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4,  A5 arg5, A6 arg6, A7 arg7, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 8)
			luaL_error(pL, "invalid arguments count for L_cmfunction7");
		A7 arg7 = L_totopvalue_pop<A7>(*pL);
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, arg2, arg3, arg4, arg5, arg6, arg7, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};

template<class R, class T, class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8,
R (T::*f)(A1, A2, A3, A4, A5, A6, A7, A8)>
class Lcmfunction<T, R (T::*)(A1, A2, A3, A4, A5, A6, A7, A8), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7,
		A8 arg8, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4,  A5 arg5, A6 arg6, A7 arg7, 
		A8 arg8, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 9)
			luaL_error(pL, "invalid arguments count for L_cmfunction8");
		A8 arg8 = L_totopvalue_pop<A8>(*pL);
		A7 arg7 = L_totopvalue_pop<A7>(*pL);
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};


template<class R, class T, 
class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8, class A9,
	R (T::*f)(A1, A2, A3, A4, A5, A6, A7, A8, A9)>
class Lcmfunction<T, R (T::*)(A1, A2, A3, A4, A5, A6, A7, A8, A9), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7,
		A8 arg8, A9 arg9, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4,  A5 arg5, A6 arg6, A7 arg7, 
		A8 arg8, A9 arg9, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 10)
			luaL_error(pL, "invalid arguments count for L_cmfunction9");
		A9 arg9 = L_totopvalue_pop<A9>(*pL);
		A8 arg8 = L_totopvalue_pop<A8>(*pL);
		A7 arg7 = L_totopvalue_pop<A7>(*pL);
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};

template<class R, 
class T, class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8, class A9, class A10,
	R (T::*f)(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10)>
class Lcmfunction<T, R (T::*)(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10), f> : public Lcmfunction_base<T>
{
private:
	template<class U>
	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7,
		A8 arg8, A9 arg9, A10 arg10, U *)
	{
		CF_LUA_TRY
		R result = (p->*f)(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	static int L_call(lua_State * pL, T * p, A1 arg1, A2 arg2, A3 arg3, A4 arg4,  A5 arg5, A6 arg6, A7 arg7, 
		A8 arg8, A9 arg9, A10 arg10, void *)
	{
		CF_LUA_TRY
		(p->*f)(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
		CF_LUA_CATCH
		return 0;
	}

	static int L_cmfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 11)
			luaL_error(pL, "invalid arguments count for L_cmfunction10");
		A10 arg10 = L_totopvalue_pop<A10>(*pL);
		A9 arg9 = L_totopvalue_pop<A9>(*pL);
		A8 arg8 = L_totopvalue_pop<A8>(*pL);
		A7 arg7 = L_totopvalue_pop<A7>(*pL);
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		T * p = Lcmfunction_base<T>::L_getuserdata(pL);
		return L_call(pL, p, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, (typename L_unref<R>::result *)0);
	}

	static void reg(lua_State * pL, const char * fname)
	{
		Lcmfunction_base<T>::reg(pL, fname, L_cmfunction);
	}

	friend class L;
};

__END_SCRIPTS_L_NS__
