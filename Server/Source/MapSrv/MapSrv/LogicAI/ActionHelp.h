﻿#pragma once

#include "Action.h"
#include "ActionFollow.h"
#include "Holder.h"

namespace logicai
{
	class ActionHelp : public Action_base
	{
	public:
		ActionHelp(CreaturePtr &, CreaturePtr &);
		
		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin() ;

		virtual void OnFaitEnd();

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return ACTION_ASSIST_HELP; }

		void HandleSpeedDown(int);
	private:
		CreaturePtr target_;
		Holder<ActionFollow> follow_;
	};
}
