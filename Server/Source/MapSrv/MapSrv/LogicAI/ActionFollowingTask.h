﻿#pragma once

#include "Action.h"
#include "ActionFollow.h"
#include "ActionTask.h"
#include "Holder.h"

namespace logicai
{
	class ActionFollowingTask :  public Action_base
	{
	public:
		ActionFollowingTask(CreaturePtr &, ObjPtr &, unsigned, unsigned);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin() { }

		virtual void OnFaitEnd()  { }

		virtual void OnBoundBegin() { }

		virtual void OnBoundEnd() { }

		virtual void OnLostObj(ObjPtr &);

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return ACTION_MOVE_FOLLOWING_TASK; }

		ActionTask & GetTask() { return task_; }

		void OnTargetLeaveMap(ObjPtr &);

		void HandleSpeedDown(int);

	private:
		ActionFollow follow_;
		ActionTask task_;
	};

}
