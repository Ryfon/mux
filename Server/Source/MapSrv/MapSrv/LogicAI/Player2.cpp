﻿#include "Player2.h"
#include "LogicAIEngine.h"

namespace logicai
{
	unsigned long long Player::g_update_count_ = 0;

	static const std::string NewPlayerIdent()
	{
		static unsigned num = 0;
		char buf[32];
		sprintf(buf, "__player_%u", ++num);
		return buf;
	}

	Player::Player(const unsigned objID, const unsigned gateID, const unsigned mapId) 
		: GameObj(objID), ident_(NewPlayerIdent()), mapId_(mapId), sex_(0), level_(0)
		, hp_(100.0f), race_(0), evil_(false), rogue_(false), update_count_(0)
	{
		playerID_.wGID = gateID;
		playerID_.dwPID = objID;

		curPos_.x = curPos_.y = 0;
	}

	Player::~Player()
	{
	}

	void Player::GetPlayerInfo()const
	{
		const char * name = 0;
		if (1 == CMonsterWrapper::GetPlayerInfo(playerID_, name, hpPercent_, sex_, level_))
		{
			name_ = std::string(name);
		}

		CMonsterWrapper::GetPlayerRace(playerID_, race_);
	}

	void Player::GetPlayerPosition()const
	{
		TYPE_POS x, y;
		if (1 == CMonsterWrapper::GetPlayerPosition(playerID_, x, y))
		{
			curPos_.x = x;
			curPos_.y = y;
		}
	}

	const std::string & Player::GetName()const
	{
		if (!name_.empty())
			return name_;

		GetPlayerInfo();
		return name_;
	}

	const std::string & Player::GetIdent()const
	{
		return ident_;
	}

	const position & Player::GetCurPos()const
	{
		/*if (update_count_ != g_update_count_ || update_count_ == 0)
		{
			GetPlayerPosition();
			update_count_ = g_update_count_;
		}*/
		GetPlayerPosition();
		return curPos_;
	}

	const unsigned Player::GetMapId()const
	{
		return mapId_;
	}

	const unsigned Player::GetSize() const
	{
		return 0;
	}

	const unsigned Player::GetRace()const
	{
		if (name_.empty())
			GetPlayerInfo();
		
		return race_;
	}

	const bool Player::GetEvil()const
	{
		CMonsterWrapper::IsPlayerEvil(playerID_, evil_);

		return evil_;
	}

	const bool Player::GetRogue()const
	{
		CMonsterWrapper::IsPlayerRogue(playerID_, rogue_);

		return rogue_;
	}

	void Player::OnDied()
	{
		Notify_LostEvent();
		LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void ()>("_HandleDied")();
		taskObj_.reset();
	}

	void Player::OnLost()
	{
		Notify_LostEvent();
		LogicAIEngine::instance().L().Table(GetIdent().c_str()).Mfunction<void ()>("_HandleLost")();
		taskObj_.reset();
	}

	void Player::OnUpdate(unsigned)
	{
	}

	void Player::OnReceivedDiedEvent(ObjPtr &)
	{
	}

	void Player::OnReceivedLostEvent(ObjPtr &)
	{
	}

	const bool Player::IsPlayer()const
	{
		return true;
	}

	const unsigned Player::GetGateId()const
	{
		return playerID_.wGID;
	}

	const MUX_PROTO::PlayerID Player::GetMPPid()const
	{
		return playerID_;
	}

	const int Player::GetSex()const
	{
		if (name_.empty())
			GetPlayerInfo();

		return sex_;
	}

	const float Player::GetHpPercent()const
	{
		if (update_count_ != g_update_count_|| update_count_ == 0)
		{
			const char * name;
			CMonsterWrapper::GetPlayerInfo(this->GetMPPid(), name, hp_, sex_, level_);

			update_count_ = g_update_count_;
		}
		return hp_;
	}

	const unsigned Player::GetLevel()const
	{
		if (update_count_ != g_update_count_|| update_count_ == 0)
		{
			GetPlayerInfo();
			update_count_ = g_update_count_;
		}

		return level_;
	}

	void Player::SetHpPercent(float f)
	{
		CMonsterWrapper::SetPlayerHpPercent(this->GetMPPid(), f / 100.0f);
	}

	void Player::OnAttacked(ObjPtr & obj, unsigned, bool)
	{
		if (obj.get())
			LogicAIEngine::instance().L().Table(this->GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleAttacked")(obj.get());
	}

	void Player::Reset()
	{
		taskObj_.reset();
	}

	bool Player::CheckValid()const
	{
		return 0 == CMonsterWrapper::IsPlayerEvil(this->GetMPPid(), evil_);
	}

	CreaturePtr & Player::GetTaskObj()
	{
		return taskObj_;
	}

	void Player::SetTaskObj(CreaturePtr obj)
	{
		taskObj_ = obj;
	}
}