﻿#include "LogicAIEngine.h"
#include "Creature.h"
#include "Player2.h"
#include "../../../Base/Utility.h"

namespace logicai
{
	static unsigned g_debug_player_view = 0;
	static unsigned g_debug_monster_view = 0;

	ObjPtr GetObjPtr(PlayerPtr & player)
	{
		ObjPtr tmp = player;
		return tmp;
	}

	ObjPtr GetObjPtr(CreaturePtr & creature)
	{
		ObjPtr tmp = creature;
		return tmp;
	}

	int foo()
	{
		LogicAIEngine::instance();
		return 0;
	}

	static int i = foo();

	float LogicAIEngine::min_alert_dist = 1.0f;

	LogicAIEngine & LogicAIEngine::instance()
	{
		static LogicAIEngine inst;
		return inst;
	}

	LogicAIEngine::LogicAIEngine() : L_(0)
	{
	}

	LogicAIEngine::~LogicAIEngine()
	{
		Finit();
	}

	ObjManager & LogicAIEngine::GetObjMgr()
	{
		return objMgr_;
	}

	scripts::L & LogicAIEngine::L()
	{
		if (L_)
			return *L_;
		throw std::runtime_error("invalid L");
	}
	
	const bool LogicAIEngine::Init()
	{
		EXCEPT_TRY
		if (!L_)
		{
			L_ = NEW scripts::L();

			using scripts::Lclass;
			using scripts::Lconstructor;
			using scripts::Lcfunction;
			using scripts::Lcmfunction;
			using scripts::Lfunction;

			L().reg(Lclass<GameObj>(), "_GameObj");
			L().reg(Lcmfunction<GameObj, const std::string & (GameObj::*)()const, &GameObj::GetIdent>(),	"GetIdent");
			L().reg(Lcmfunction<GameObj, const std::string & (GameObj::*)()const, &GameObj::GetName>(),		"GetName");
			L().reg(Lcmfunction<GameObj, const bool (GameObj::*)()const, &GameObj::IsPlayer>(),				"IsPlayer");
			L().reg(Lcmfunction<GameObj, const unsigned (GameObj::*)()const, &GameObj::GetId>(),			"GetId");
			L().reg(Lcmfunction<GameObj, const position & (GameObj::*)()const, &GameObj::GetCurPos>(),		"GetCurPos");
			L().reg(Lcmfunction<GameObj, const unsigned (GameObj::*)()const, &GameObj::GetMapId>(),			"GetMapId");
			L().reg(Lcmfunction<GameObj, const unsigned (GameObj::*)()const, &GameObj::GetRace>(),				"GetRace");
			L().reg(Lcmfunction<GameObj, const unsigned (GameObj::*)()const, &GameObj::GetLevel>(),				"GetLevel");
			L().reg(Lcmfunction<GameObj, void (GameObj::*)(GameObj *, unsigned), &GameObj::Observe_LostEvent>(),	"Observe_LostEvent");

			L().reg(Lclass<Creature>(), "_Creature");
			L().reg<Creature>(Lconstructor<const unsigned>());
			L().reg(Lcmfunction<Creature, const unsigned (Creature::*)()const, &Creature::GetClassId>(),	"GetClassId");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetChaseSpeed>(),				"SetChaseSpeed");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetEscapeSpeed>(),			"SetEscapeSpeed");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetGoHomeSpeed>(),			"SetGoHomeSpeed");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetFollowingSpeed>(),			"SetFollowingSpeed");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetWanderSpeed>(),			"SetWanderSpeed");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetPatrolSpeed>(),			"SetPatrolSpeed");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetRadius>(),				"SetRadius");
			L().reg(Lcmfunction<Creature, void (Creature::*)(GameObj *, unsigned, bool), &Creature::AddThreat>(),	"AddThreat");
			L().reg(Lcmfunction<Creature, float (Creature::*)()const, &Creature::GetHpPercent>(),					"GetHpPercent");
			L().reg(Lcmfunction<Creature, void (Creature::*)(const std::string), &Creature::Speek>(),			"Speek");
			L().reg(Lcmfunction<Creature, void (Creature::*)(), &Creature::SetMoveIdle>(),							"SetMoveIdle");
			L().reg(Lcmfunction<Creature, void (Creature::*)(), &Creature::SetMoveWander>(),						"SetMoveWander");
			L().reg(Lcmfunction<Creature, void (Creature::*)(std::vector<position>, int), &Creature::SetMovePatrol>(), "SetMovePatrol");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::ClearAllBuffs>(),						"ClearAllBuffs");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetActive>(),							"SetActive");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetHpPercent>(),						"SetHpPercent");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned), &Creature::Disable>(),							"Disable");
			L().reg(Lcmfunction<Creature, void (Creature::*)(const std::string), &Creature::Yell>(),				"Yell");
			L().reg(Lcmfunction<Creature, void (Creature::*)(const std::string), &Creature::Boradcast>(),			"Boradcast");
			L().reg(Lcmfunction<Creature, void (Creature::*)(int, unsigned), &Creature::SetSkillMode>(),							"SetSkillSelectFlag");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned,unsigned, bool), &Creature::Call>(),					"Call");
			L().reg(Lcmfunction<Creature, void (Creature::*)(int), &Creature::SetMesh>(),								"SetMesh");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetMeshScale>(),						"SetMeshScale");
			L().reg(Lcmfunction<Creature, void (Creature::*)(const std::string, int), &Creature::PlaySound>(),			"PlaySound");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned, unsigned, position), &Creature::Summon>(),		"Summon");
			L().reg(Lcmfunction<Creature, unsigned (Creature::*)()const, &Creature::GetLiveTime>(),					"GetBornAccTime");
			L().reg(Lcmfunction<Creature, void (Creature::*)(int), &Creature::NotifyBindTaskSuccessed>(),					"NotifyBindTaskSuccessed");
			L().reg(Lcmfunction<Creature, void (Creature::*)(),	   &Creature::NotifyBindTaskFail>(),						"NotifyBindTaskFail");
			L().reg(Lcmfunction<Creature, void (Creature::*)(int), &Creature::SetDieState>(),							"SetDieState");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetPkgDropFlag>(),						"SetPkgDropFlag");
			L().reg(Lcmfunction<Creature, void (Creature::*)(const std::string), &Creature::Rename>(),					"Rename");
			L().reg(Lcmfunction<Creature, void (Creature::*)(int, const std::string), &Creature::SpeciStr>(),			"SpeciStr");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned,unsigned), &Creature::SetNpcSeq>(),				"SetNpcSeq");
			L().reg(Lcmfunction<Creature, void (Creature::*)(int), &Creature::ChangeState>(),							"ChangeState"); // noop
			L().reg(Lcmfunction<Creature, void (Creature::*)(GameObj *, unsigned, bool), &Creature::AddThreat>(),		"AddThreat");
			L().reg(Lcmfunction<Creature, void (Creature::*)(int), &Creature::SetMoveMode>(),							"SetMoveMode");
			L().reg(Lcmfunction<Creature, bool (Creature::*)()const, &Creature::GetAttackMobility>(),					"GetAttackMobility");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetNotifySkillCasting>(),				"SetNotifySkillCasting");
			L().reg(Lcmfunction<Creature, void (Creature::*)(GameObj *, std::vector<position>,unsigned,unsigned), &Creature::FollowedTaskBegin>(), "FollowedTaskBegin");
			L().reg(Lcmfunction<Creature, void (Creature::*)(GameObj *, unsigned,unsigned), &Creature::FollowingTaskBegin>(), "FollowingTaskBegin");
			L().reg(Lcmfunction<Creature, unsigned (Creature::*)()const, &Creature::GetAIPattern>(),							  "GetAIPattern");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetMovingMobility>(),							  "SetMovingAbility");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetAttackMobility>(),							  "SetAttackAbility");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetActiveMobility>(),							  "SetActiveAbility");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetChaseAbility>(),								  "SetChaseAbility");
			L().reg(Lcmfunction<Creature, bool (Creature::*)()const, &Creature::IsFighting>(),								  "IsFighting");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned), &Creature::Fight>(),								  "Fight");
			L().reg(Lcmfunction<Creature, void (Creature::*)(), &Creature::Help>(),											  "Help");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned, unsigned, const position), &Creature::Summon>(),		  "Summon");
			L().reg(Lcmfunction<Creature, void (Creature::*)(const position, float), &Creature::MoveTo>(),							  "MoveTo");
			L().reg(Lcmfunction<Creature, void (Creature::*)(GameObj *, unsigned), &Creature::Treat>(),								  "Treat");
			L().reg(Lcmfunction<Creature, void (Creature::*)(GameObj *), &Creature::Follow>(),								  "Follow");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned), &Creature::CastAOE>(),								  "CastAOE");
			L().reg(Lcmfunction<Creature, void (Creature::*)(GameObj *, unsigned), &Creature::CastAOEAt>(),				  "CastAOEAt");
			L().reg(Lcmfunction<Creature, void (Creature::*)(GameObj *, unsigned), &Creature::AttackTarget>(),				 "AttackTarget");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned), &Creature::SetSight>(),								  "SetSight");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetNotifyHpChanged>(),						  "SetNotifyHpChanged");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetNotifyAttacked>(),							"SetNotifyAttacked");
			L().reg(Lcmfunction<Creature, void (Creature::*)(), &Creature::Escape>(),											"Escape");
			L().reg(Lcmfunction<Creature, void (Creature::*)(float), &Creature::SetChaseDist>(),								"SetChaseDist");
			L().reg(Lcmfunction<Creature, void (Creature::*)(), &Creature::ExitFight>(),										"ExitFight");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned), &Creature::Bound>(),									"Bound");
			L().reg(Lcmfunction<Creature, void (Creature::*)(), &Creature::Unbound>(),											"Unbound");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetNotifyViewObj>(),								"SetNotifyViewObj");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetNotifyWayPoint>(),							"SetNotifyWayPoint");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned), &Creature::NotifyLevelMapInfo>(),						"NotifyLevelMapInfo");
			L().reg(Lcmfunction<Creature, void (Creature::*)(int, const std::string), &Creature::PersistSpeciStr>(),			"PersistSpeciStr");
			L().reg(Lcmfunction<Creature, void (Creature::*)(bool), &Creature::SetNotifyLuaUpdate>(),							"SetNotifyLuaUpdate");
			L().reg(Lcmfunction<Creature, void (Creature::*)(), &Creature::Defend>(),											"Defend");
			L().reg(Lcmfunction<Creature, void (Creature::*)(std::vector<position>), &Creature::MoveToByWayPoints>(),		"MoveToByWayPoints");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned), &Creature::SetSpeciSkill>(),						"SetSpeciSkill");
			L().reg(Lcmfunction<Creature, int (Creature::*)(), &Creature::GetDiedReason>(),									"GetDiedReason");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned), &Creature::AddSkill>(),								"AddSkill");
			L().reg(Lcmfunction<Creature, void (Creature::*)(unsigned), &Creature::RemoveSkill>(),							"RemoveSkill");
			L().reg(Lcmfunction<Creature, void (Creature::*)(GameObj *), &Creature::SynchThreatList>(),					"SynchThreatList");

			L().reg(Lclass<Player>(), "_Player");
			L().reg<Player>(Lconstructor<const unsigned, const unsigned, const unsigned>());
			L().reg(Lcmfunction<Player, const int (Player::*)()const, &Player::GetSex>(),			"GetSex");
			L().reg(Lcmfunction<Player, const float (Player::*)()const, &Player::GetHpPercent>(), "GetHpPercent");
			L().reg(Lcmfunction<Player, void (Player::*)(float), &Player::SetHpPercent>(),	"SetHpPercent");
			L().reg(Lcmfunction<Player, float (Player::*)()const, &Player::GetMpPercent>(),	 "GetMpPercent");
			L().reg(Lcmfunction<Player, void (Player::*)(float), &Player::SetMpPercent>(),   "SetMpPercent");

			L().reg(Lcfunction<int (*)(unsigned, int, unsigned), &LogicAIEngine::BindMonsterTask>(), "BindMonsterTask");
			L().reg(Lcfunction<void (*)(float), &LogicAIEngine::SetMinAlertDist>(), "SetMinAlertDist");

			L().dofile("./config/scripts/monsterai/Event.lua");
			L().dofile("./config/scripts/monsterai/PatternSet.lua");
			L().dofile("./config/scripts/monsterai/PathSet.lua");
			L().dofile("./config/scripts/monsterai/GameObj.lua");
			L().dofile("./config/scripts/monsterai/Creature.lua");
			L().dofile("./config/scripts/monsterai/Player.lua");
			L().dofile("./config/scripts/monsterai/Loader.lua");

			InitSinValues();
		}
		return true;
		EXCEPT_CATCH
		exit(-1);
		return false;
	}

	void LogicAIEngine::Finit()
	{
		if (L_)
		{
			objMgr_.ClearAllObjs();
			delete L_;
			L_ = 0;
		}
	}

	void LogicAIEngine::Refresh()
	{
		EXCEPT_TRY
		LogicAIEngine::instance().L().dofile("./config/scripts/monsterai/Event.lua");
		LogicAIEngine::instance().L().dofile("./config/scripts/monsterai/PatternSet.lua");
		LogicAIEngine::instance().L().dofile("./config/scripts/monsterai/PathSet.lua");
		LogicAIEngine::instance().L().dofile("./config/scripts/monsterai/GameObj.lua");
		LogicAIEngine::instance().L().dofile("./config/scripts/monsterai/Creature.lua");
		LogicAIEngine::instance().L().dofile("./config/scripts/monsterai/Player.lua");
		LogicAIEngine::instance().L().dofile("./config/scripts/monsterai/Loader.lua");
		EXCEPT_CATCH
	}

	void LogicAIEngine::InitSinValues()
	{
		for (int i = 0; i < ARRAY_SIZE(sinValues_); ++i)
		{
			sinValues_[i] = sin((float)i *  3.14159265358979323846f / 180.0f);
		}
	}

	void LogicAIEngine::HandleTimer(unsigned long monsterId)
	{
		EXCEPT_TRY

		objMgr_.HandleDiedList();

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (creature.get())
		{
			if (objMgr_.IsFirstCreature(creature))
				++Player::g_update_count_;
			creature->OnUpdate(0);
		}
		
		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleMonsterBorn(unsigned long monsterId, const AIMapInfo & mapInfo)
	{
		EXCEPT_TRY

		CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		AI_ASSERT(!creature.get());

		if (creature.get())
		{
			D_ERROR("重复的monster ID:%u\n", creature->GetId());

			objMgr_.RemoveObj(creature);

			creature.reset();
		}

		unsigned long cls = 0;
		CMonsterWrapper::GetMonsterType(mapInfo, monsterId, cls);
		creature = CreaturePtr(dynamic_cast<Creature *>(L().Function<GameObj * (const unsigned, unsigned)>("CreateGameObj")(monsterId, cls)));
		if (!creature.get())
		{
			D_ERROR("创建monster(%u)实例失败\n", monsterId);
			return;
		}

		void * arg;
		new(&arg) CreaturePtr(creature);
		CMonsterWrapper::SetMonsterAIFlag(mapInfo, monsterId, arg);

		objMgr_.AddObj(creature);

		creature->SetMapInfo(mapInfo);
		creature->OnBorn();
		
		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleMonsterDied(unsigned long monsterId)
	{
		EXCEPT_TRY

		/*CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		AI_ASSERT(creature.get());*/
	
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
			p->reset();
		}

		AI_ASSERT(creature.get());
		if(!creature.get())
			return;

		objMgr_.RemoveObj(creature);

		creature->SetDiedReason(evtInfo->disType);
		creature->OnDied();		

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleDetectPlayer(unsigned long monsterId)
	{
		EXCEPT_TRY

//#ifdef AI_DEBUG_VIEW_SIGHT
//		D_DEBUG("current player enter-leave count: %u\n", ++g_debug_player_view);
//#endif

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		if (!evtInfo->detectedPlayerHP)
			return;

		PlayerPtr player = objMgr_.GetPlayerObj(evtInfo->detectedPlayerID);
		if (!player.get())
		{
			player = PlayerPtr(dynamic_cast<Player *>(L().Function<GameObj * (const unsigned, const unsigned, const unsigned)>("CreatePlayer")(
				evtInfo->detectedPlayerID.dwPID, evtInfo->detectedPlayerID.wGID, creature->GetMapId())));
			if (!player.get())
			{
				D_ERROR("创建player(%u)实例失败\n", monsterId);
				return;
			}

			objMgr_.AddObj(player);
		}

		ObjPtr obj = GetObjPtr(player);
		creature->EnterView(obj);

		EXCEPT_CATCH
	}


	void LogicAIEngine::HandleLosePlayer(unsigned long monsterId)
	{
		EXCEPT_TRY

//#ifdef AI_DEBUG_VIEW_SIGHT
//		if (g_debug_player_view)
//			D_DEBUG("current player enter-leave count: %u\n", --g_debug_player_view);
//		else
//			AI_ASSERT(false);
//#endif

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;;		

		PlayerPtr player = objMgr_.GetPlayerObj(evtInfo->leavePlayerID);

		if (!player.get())
			return;

		ObjPtr obj = GetObjPtr(player);
		if (evtInfo->leaveReason == AILR_LEAVEVIEW ||
			evtInfo->leaveReason == AILR_LEAVEMAP)
		{
			creature->LeaveView(obj);
		}
		else
		{
			objMgr_.RemoveObj(player);

			if (evtInfo->leaveReason == AILR_DIE)
			{
				creature->LeaveView(obj);
				player->OnDied();
			}
			else
			{
				creature->LeaveView(obj);
				player->OnLost();	
			}			
		}

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleDetectMonster(unsigned long monsterId)
	{
		EXCEPT_TRY

//#ifdef AI_DEBUG_VIEW_SIGHT
//		D_DEBUG("current monster enter-leave count: %u\n", ++g_debug_player_view);
//#endif

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		CreaturePtr target = objMgr_.GetCreatureObj(evtInfo->enterViewMonsterID);
		AI_ASSERT(target.get());

		if (target.get())
		{
			ObjPtr obj = GetObjPtr(target);
			creature->EnterView(obj);
		}

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleLoseMonster( unsigned long monsterId)
	{
		EXCEPT_TRY

//#ifdef AI_DEBUG_VIEW_SIGHT
//		if (g_debug_monster_view)
//			D_DEBUG("current monster enter-leave count: %u\n", --g_debug_monster_view);
//		else
//			//AI_ASSERT(false);
//		;
//#endif

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		CreaturePtr target = objMgr_.GetCreatureObj(evtInfo->leaveViewMonsterID);
		AI_ASSERT(target.get());

		if (target.get())
		{	
			ObjPtr obj = GetObjPtr(target);
			creature->LeaveView(obj);
		}

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleAttacked(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		ObjPtr attacker = objMgr_.GetGameObj(evtInfo->attackPlayerID);
		AI_ASSERT(attacker.get());
		
		if (attacker.get())
			creature->OnAttacked(attacker, evtInfo->reactTime, evtInfo->nDamage, evtInfo->bCreateByDebuff);

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleStartFait(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		creature->OnFaitBegin();

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleStopFait(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		creature->OnFaitEnd();

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleStartBound(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		creature->OnBoundBegin();

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleStopBound(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		creature->OnBoundEnd();

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleMosterBuffStart(unsigned long monsterId)
	{
		EXCEPT_TRY

		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		if (evtInfo->buffType == DEBUFF_SPEED_DOWN)
			creature->HandleSpeedDown(evtInfo->buffVal);

		EXCEPT_CATCH
	}
						
	void LogicAIEngine::HandleMonsterBuffEnd(unsigned long monsterId)
	{
		EXCEPT_TRY

		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		if (evtInfo->buffType == DEBUFF_SPEED_DOWN)
			creature->HandleSpeedDown(0);

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleStartFollow(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		MUX_PROTO::PlayerID playerId;
		if (0 == CMonsterWrapper::GetEscortHost(creature->GetMapInfo(), monsterId, playerId))
		{
			PlayerPtr player = objMgr_.GetPlayerObj(playerId);
			AI_ASSERT(player.get());

			if (player.get())
			{
				L().Table(creature->GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleFollowTaskBegin")(player.get());

				player->SetTaskObj(creature);
			}
		}

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleStopFollow(unsigned long monsterId)
	{
		EXCEPT_TRY

		return;

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleStartNotifyThreatList(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		creature->SetNotifyThreatList(true);

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleStopNotifyThreatList(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		creature->SetNotifyThreatList(false);

		EXCEPT_CATCH
	}


	void LogicAIEngine::HandleBeginChat(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleCreateNewLacquey(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		CreaturePtr target = objMgr_.GetCreatureObj(evtInfo->calledMonsterID);
		AI_ASSERT(target.get());

		if (target.get())
			L().Table(creature->GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleCreatureSummoned")(target.get());

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleCreateNewLacquey(unsigned long monsterId, unsigned flag)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr caller = objMgr_.GetCreatureObj(flag);
		AI_ASSERT(caller.get());
		if (!caller.get())
			return;

		CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		AI_ASSERT(creature.get());

		if (creature.get())
			L().Table(caller->GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleCreatureSummoned")(creature.get());

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandleMoveChange(unsigned long)
	{
		EXCEPT_TRY

		// noop

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandlePlayerAttacked(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		if (evtInfo->eventType != MEVENT_FOLLOWPLAYER_BEATTACK)
			return;

		ObjPtr attacker = objMgr_.GetGameObj(evtInfo->attackFollowingID);
		AI_ASSERT(attacker.get());

		if (creature.get() == attacker.get())
			return;

		if (attacker.get() && creature->GetAttackMobility())
		{
			creature->AddThreat(attacker.get(), evtInfo->nDamage, 0);
			creature->Fight(0);
		}

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandlePlayerCured(unsigned long monsterId)
	{
		EXCEPT_TRY

		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		ObjPtr caster = objMgr_.GetGameObj(evtInfo->addHpStarter);
		AI_ASSERT(caster.get());
		if (!caster.get())
			return;

		ObjPtr target = objMgr_.GetGameObj(evtInfo->addHpTarget);
		AI_ASSERT(target.get());
		if (!target.get())
			return;

		creature->HandleThreatCured(caster, target, evtInfo->addVal / 2);


		EXCEPT_CATCH
	}

	void LogicAIEngine::HandlePlayerDropTask(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		unsigned taskId = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			taskId = evtInfo->dropTaskID;
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (creature.get())
			creature->NotifyBindTaskDroped(taskId);

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandlePlayerDied(const PlayerID & id)
	{
		EXCEPT_TRY
		
		PlayerPtr player = objMgr_.GetPlayerObj(id);

		if (player.get())
		{
			objMgr_.RemoveObj(player);

			player->OnDied();
		}

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandlePlayerDied(unsigned long monsterId)
	{
		EXCEPT_TRY

		//CreaturePtr creature = objMgr_.GetCreatureObj(monsterId);
		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		PlayerPtr player = objMgr_.GetPlayerObj(evtInfo->diePlayerID);

		if (!player.get())
			return;

		ObjPtr obj = GetObjPtr(player);
		creature->LeaveView(obj);
		player->OnDied();

		EXCEPT_CATCH
	}

	void LogicAIEngine::HandlePlayerLeaveMap(const PlayerID & id, bool isSwitchMap)
	{
		EXCEPT_TRY

		PlayerPtr player = objMgr_.GetPlayerObj(id);

		AI_ASSERT(player.get());
		if (!player.get())
			return;

		objMgr_.RemoveObj(player);

		if (isSwitchMap && player->GetTaskObj().get())
		{
			CreaturePtr creature(player->GetTaskObj());
			ObjPtr obj(GetObjPtr(player));
			creature->LeaveMap(obj);
		}

		player->OnDied();

		EXCEPT_CATCH
	}

	void LogicAIEngine::OnBeKickBack(unsigned long monsterId)
	{
		EXCEPT_TRY

		CreaturePtr creature;
		AIEventInfo * evtInfo = 0;
		CMonsterWrapper::GetAIEventInfo(evtInfo);
		if (evtInfo)
		{
			CreaturePtr * p = reinterpret_cast<CreaturePtr *>(&evtInfo->selfAIFlag);
			creature = *p;
		}

		AI_ASSERT(creature.get());
		if (!creature.get())
			return;

		TYPE_POS x = evtInfo->kickBackPosX / 10.0f;
		TYPE_POS y = evtInfo->kickBackPosY / 10.0f;
		creature->SetCurPos(x, y);
		creature->Chase();

		EXCEPT_CATCH
	}

	float LogicAIEngine::FastSin(float angle)
	{
		angle = angle < 360.0f ? angle : angle / 360.0f;
		int index = (int)angle;
		float s = 1.0f;
		if (index < 0)
		{
			s = -1.0f;
			index = -index;
		}

		if ( index >= ARRAY_SIZE(sinValues_) )
		{
			D_ERROR( "LogicAIEngine::FastSin, index(%d) >= ARRYA_SIZE(sinValues_)\n", index );
			return 0;
		}

		return sinValues_[index] * s;
	}

	int LogicAIEngine::BindMonsterTask(unsigned classId, int taskId, unsigned failTime)
	{
		LogicAIEngine::instance().GetMonsterTaskMap()[classId] = std::pair<int, unsigned>(taskId, failTime);
		return 0;
	}

	void LogicAIEngine::SetMinAlertDist(float f)
	{
		min_alert_dist = f;
	}

	int LogicAIEngine::NotifyLevelMapInfo(const AIMapInfo & map, unsigned count)
	{
		CMonsterWrapper::NotifyLevelMapInfo(map, count);
		return 0;
	}

	std::pair<int,unsigned> LogicAIEngine::FindMonsterTask(unsigned classId)
	{
		 std::map<unsigned, std::pair<int, unsigned> >::const_iterator it = GetMonsterTaskMap().find(classId);
		if (it != GetMonsterTaskMap().end())
			return it->second;
		return std::make_pair((int)0, (unsigned)0);
	}

	std::map<unsigned, std::pair<int, unsigned> > & LogicAIEngine::GetMonsterTaskMap()
	{
		static std::map<unsigned, std::pair<int, unsigned> > mapTasks;

		return mapTasks;
	}

} // namespace logicai
