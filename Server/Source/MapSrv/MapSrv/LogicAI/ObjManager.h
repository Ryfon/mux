﻿#pragma once

#ifndef __MUX_LOGICAI_OBJMANAGER_H__
#define __MUX_LOGICAI_OBJMANAGER_H__

#include "GameObj.h"
#include "Creature.h"
#include "Player2.h"

#include <map>

namespace logicai
{
	class ObjManager
	{
	public:
		ObjManager() {}

		~ObjManager() {}

		void AddObj(GameObj & obj);
		void AddObj(CreaturePtr);
		void AddObj(PlayerPtr);
		void RemoveObj(ObjPtr);
		void RemoveObj(CreaturePtr);
		void RemoveObj(PlayerPtr);

		CreaturePtr GetCreatureObj(const unsigned id);
		PlayerPtr GetPlayerObj(const MUX_PROTO::PlayerID & id);
		ObjPtr GetGameObj(const MUX_PROTO::PlayerID & id);

		void AddDiedList(CreaturePtr & creature);

		void HandleDiedList();

		bool IsFirstCreature(CreaturePtr & creature)const;

		void ClearAllObjs();

	private:
		std::map<unsigned, CreaturePtr> creatures_;
		std::map<unsigned, std::map<unsigned, PlayerPtr> > players_;
		std::map<unsigned, CreaturePtr> died_list_;
	};
}
#endif
