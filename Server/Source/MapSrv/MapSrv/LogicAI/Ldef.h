﻿#pragma once

#include <cassert>
#include <stdexcept>
#include <string>

#include "Position.h"

#define CF_LUA_TRY try{
#define CF_LUA_CATCH }catch(const std::exception & e) { luaL_error(pL, e.what()); }  \
	catch(...) { luaL_error(pL, "unknown exception"); }

#define __BEGIN_SCRIPTS_L_NS__  namespace logicai { namespace scripts {
#define __END_SCRIPTS_L_NS__ } }

namespace logicai { class GameObj; }

__BEGIN_SCRIPTS_L_NS__

struct L_end_arg_tag { };

class L_base
{
protected:
	L_base() { }

	~L_base() { }

private:
	L_base(const L_base &);

	const L_base & operator=(const L_base &);
};

template<class T>
struct L_isvoid
{
	enum { result = false };
};

template<>
struct L_isvoid<void>
{
	enum { result = true };
};

template<int V>
struct L_int2type
{
	enum { value = V };
};

template<class T>
struct L_unref
{
	typedef T result;
};

template<class T>
struct L_unref<T &>
{
	typedef T result;
};

template<class T>
struct L_unref<const T &>
{
	typedef T result;
};

class L;

class Ltable;

template<class T>
class Lfield;

class Lfunction_base;

template<class F, bool M>
class Lfunction;

template<class F, F f>
class Lcfunction;

template<class T>
class Lclass;

template<
	class A1 = void, 
	class A2 = void, 
	class A3 = void, 
	class A4 = void, 
	class A5 = void, 
	class A6 = void
>
class Lconstructor;

class Lconstructor_base;

template<class T, class F, F f>
class Lcmfunction;

template<class T>
class Lcmfunction_base;

template<class T>
inline T L_tovalue(lua_State & l, int idx);

template<class T>
inline T L_totopvalue_pop(lua_State & l);

template<> 
inline void L_totopvalue_pop<void>(lua_State &)
{
	return;
}

template<> 
inline void L_tovalue<void>(lua_State &, int)
{
	return;
}

template<> 
inline float L_tovalue<float>(lua_State & l, int idx)
{
	if (!lua_isnumber(&l, idx))
		luaL_error(&l, "need number");

	return (float)lua_tonumber(&l, idx);
}

template<> 
inline const float L_tovalue<const float>(lua_State & l, int idx)
{
	return L_tovalue<float>(l, idx);
}

template<>
inline float L_totopvalue_pop<float>(lua_State & l)
{
	float result = L_tovalue<float>(l, -1);
	lua_pop(&l, 1);
	return result;
}

template<>
inline const float L_totopvalue_pop<const float>(lua_State & l)
{
	const float result = L_tovalue<float>(l, -1);
	lua_pop(&l, 1);
	return result;
}
//

template<> 
inline double L_tovalue<double>(lua_State & l, int idx)
{
	if (!lua_isnumber(&l, idx))
		luaL_error(&l, "need number");

	return lua_tonumber(&l, idx);
}

template<> 
inline const double L_tovalue<const double>(lua_State & l, int idx)
{
	if (!lua_isnumber(&l, idx))
		luaL_error(&l, "need number");

	return lua_tonumber(&l, idx);
}

template<>
inline double L_totopvalue_pop<double>(lua_State & l)
{
	double result = L_tovalue<double>(l, -1);
	lua_pop(&l, 1);
	return result;
}

template<>
inline const double L_totopvalue_pop<const double>(lua_State & l)
{
	const double result = L_tovalue<double>(l, -1);
	lua_pop(&l, 1);
	return result;
}

template<> 
inline int L_tovalue<int>(lua_State & l, int idx)
{
	if (!lua_isnumber(&l, idx))
		luaL_error(&l, "need number");

	return (int)lua_tointeger(&l, idx);
}

template<> 
inline const int L_tovalue<const int>(lua_State & l, int idx)
{
	return L_tovalue<int>(l, idx);
}

template<>
inline int L_totopvalue_pop<int>(lua_State & l)
{
	int result = L_tovalue<int>(l, -1);
	lua_pop(&l, 1);
	return result;
}

template<>
inline const int L_totopvalue_pop<const int>(lua_State & l)
{
	return L_totopvalue_pop<int>(l);
}

template<> 
inline unsigned L_tovalue<unsigned>(lua_State & l, int idx)
{
	if (!lua_isnumber(&l, idx))
		luaL_error(&l, "need number");

	return (unsigned)lua_tointeger(&l, idx);
}

template<> 
inline const unsigned L_tovalue<const unsigned>(lua_State & l, int idx)
{
	return L_tovalue<unsigned>(l, idx);
}

template<>
inline unsigned L_totopvalue_pop<unsigned>(lua_State & l)
{
	unsigned result = L_tovalue<unsigned>(l, -1);
	lua_pop(&l, 1);
	return result;
}

template<>
inline const unsigned L_totopvalue_pop<const unsigned>(lua_State & l)
{
	return L_totopvalue_pop<unsigned>(l);
}


template<> 
inline bool L_tovalue<bool>(lua_State & l, int idx)
{
	if (!lua_isboolean(&l, idx))
		luaL_error(&l, "need number");

	return 0 != lua_toboolean(&l, idx);
}

template<> 
inline const bool L_tovalue<const bool>(lua_State & l, int idx)
{
	return L_tovalue<bool>(l, idx);
}

template<>
inline bool L_totopvalue_pop<bool>(lua_State & l)
{
	bool result = L_tovalue<bool>(l, -1);
	lua_pop(&l, 1);
	return result;
}

template<>
inline const bool L_totopvalue_pop<const bool>(lua_State & l)
{
	return L_totopvalue_pop<bool>(l);
}

template<> 
inline const char * L_tovalue<const char *>(lua_State & l, int idx)
{
	if (!lua_isstring(&l, idx))
		luaL_error(&l, "need number");

	return lua_tostring(&l, idx);
}

template<>
inline const char * L_totopvalue_pop<const char *>(lua_State & l);
// no definition

template<> 
inline const std::string L_tovalue<const std::string>(lua_State & l, int idx)
{
	return std::string(L_tovalue<const char *>(l, idx));
}

template<>
inline const std::string L_totopvalue_pop<const std::string>(lua_State & l)
{
	const std::string result = L_tovalue<const char *>(l, -1);
	lua_pop(&l, 1);
	return result;
}

template<> 
inline std::string L_tovalue<std::string>(lua_State & l, int idx)
{
	return std::string(L_tovalue<const char *>(l, idx));
}

template<>
inline std::string L_totopvalue_pop<std::string>(lua_State & l)
{
	const std::string result = L_tovalue<const char *>(l, -1);
	lua_pop(&l, 1);
	return result;
}

inline int L_pushvalue(lua_State &l, unsigned n)
{
	lua_pushinteger(&l, n);
	return 1;
}

inline int L_pushvalue(lua_State &l, int n)
{
	lua_pushinteger(&l, n);
	return 1;
}

inline int L_pushvalue(lua_State &l, double n)
{
	lua_pushnumber(&l, n);
	return 1;
}

inline int L_pushvalue(lua_State &l, bool b)
{
	lua_pushboolean(&l, b);
	return 1;
}

inline int L_pushvalue(lua_State &l, const std::string & s)
{
	lua_pushstring(&l, s.c_str());
	return 1;
}

inline int L_pushvalue(lua_State &l, const char * s)
{
	if (!s) return 0;
	lua_pushstring(&l, s);
	return 1;
}

typedef std::vector<position> PosVec;

template<>
inline PosVec L_tovalue<PosVec>(lua_State & l, int idx)
{
	PosVec wayPoints;
	lua_pushvalue(&l, idx);
	for(int i = 1; true; ++i)
	{
		lua_pushinteger(&l, i);
		lua_gettable(&l, -2);
		if (lua_isnil(&l, -1))
		{
			lua_pop(&l, 1);
			break;
		}

		position pos;
		lua_pushinteger(&l, 1);
		lua_gettable(&l, -2);
		if (lua_isnil(&l, -1))
		{
			lua_pop(&l, 2);
			break;
		}
		pos.x = L_totopvalue_pop<float>(l);

		lua_pushinteger(&l, 2);
		lua_gettable(&l, -2);
		if (lua_isnil(&l, -1))
		{
			lua_pop(&l, 2);
			break;
		}
		pos.y = L_totopvalue_pop<float>(l);
		wayPoints.push_back(pos);
		lua_pop(&l, 1);
	}
	lua_pop(&l, 1);
	return wayPoints;
}

template<>
inline position L_tovalue<position>(lua_State & l, int idx)
{
	lua_pushvalue(&l, idx);
	lua_pushinteger(&l, 1);
	lua_gettable(&l, -2);
	position pos;
	pos.x = L_totopvalue_pop<float>(l);
	lua_pushinteger(&l, 2);
	lua_gettable(&l, -2);
	pos.y = L_totopvalue_pop<float>(l);
	lua_pop(&l, 1);
	return pos;
}

template<>
inline PosVec L_totopvalue_pop<PosVec>(lua_State & l)
{
	PosVec vec = L_tovalue<PosVec>(l, -1);
	lua_pop(&l, 1);
	return vec;
}

template<>
inline position L_totopvalue_pop<position>(lua_State & l)
{
	position pos = L_tovalue<position>(l, -1);
	lua_pop(&l, 1);
	return pos;
}

inline int L_pushvalue(lua_State & l, const position & pos)
{
	L_pushvalue(l, pos.x);
	L_pushvalue(l, pos.y);
	return 2;
}

template<>
logicai::GameObj * L_tovalue<logicai::GameObj *>(lua_State & l, int id);

template<>
logicai::GameObj * L_totopvalue_pop<logicai::GameObj *>(lua_State & l);

int L_pushvalue(lua_State & l,logicai::GameObj *);

/*
inline int L_pushvalue(lua_State & l, const position pos)
{
	L_pushvalue(l, pos.x);
	L_pushvalue(l, pos.y);
	return 2;
}
*/
// 防止T*被转成普通类型，用户必须实现自己T*版本
//inline int L_pushvalue(lua_State &l, void *);

__END_SCRIPTS_L_NS__
