﻿#pragma once

#include "ActionCall.h"
#include "ActionFight.h"
#include "ActionFollow.h"
#include "ActionFollowedTask.h"
#include "ActionFollowingTask.h"
#include "ActionHelp.h"
#include "ActionIdle.h"
#include "ActionMoveTo.h"
#include "ActionPatrol.h"
#include "ActionSummon.h"
#include "ActionTreat.h"
#include "ActionWander.h"
#include "ActionDefend.h"
#include "ActionCastAOE.h"
#include "ActionCastAOEAt.h"
#include "ActionAttackTarget.h"
#include "ActionExitFight.h"

namespace logicai
{
	struct ActionHolder
	{
		char data[MaxTypeSize<ActionCall, MaxTypeSize<ActionFight,
			MaxTypeSize<ActionFollow, MaxTypeSize<ActionFollowedTask,
			MaxTypeSize<ActionFollowingTask, MaxTypeSize<ActionHelp,
			MaxTypeSize<ActionIdle, MaxTypeSize<ActionMoveTo,
			MaxTypeSize<ActionPatrol, MaxTypeSize<ActionSummon, 
			MaxTypeSize<ActionTreat, MaxTypeSize<ActionWander, 
			MaxTypeSize<ActionDefend, MaxTypeSize<ActionCastAOE, 
			MaxTypeSize<ActionCastAOEAt, MaxTypeSize<ActionAttackTarget, 
			MaxTypeSize<ActionExitFight, void> > > > > > > > > > > > > > > > >::size];

		template<class A> A & Get()
		{
			return *((A *)data);
		}
	};
}
