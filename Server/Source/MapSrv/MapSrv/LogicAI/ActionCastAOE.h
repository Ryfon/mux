﻿#pragma once

#include "Action.h"

namespace logicai
{
	class ActionCastAOE : public Action_base
	{
	public:
		ActionCastAOE(CreaturePtr & creature, unsigned skillId);

		virtual void OnEnter() {}

		virtual void OnLeave();

		virtual void OnPause() {}

		virtual void OnResume() {}

		virtual void OnStop();

		virtual void OnLostObj(ObjPtr &) {}

		virtual void OnFaitBegin() {}

		virtual void OnFaitEnd() {}

		virtual void OnBoundBegin() {}

		virtual void OnBoundEnd(){}

		virtual void OnUpdate(unsigned elapsed) {}

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return 0;}

	private:
		unsigned skillId_;
	};
}
