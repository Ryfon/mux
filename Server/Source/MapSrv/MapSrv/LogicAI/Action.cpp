﻿#include "Action.h"
#include "Player2.h"
#include "Creature.h"

namespace logicai
{
	Action_base::Action_base(CreaturePtr & obj)
		: creature_(obj), state_(ACTION_STATE_RUN)
	{ 
	}

	Action_base::~Action_base()
	{ 
	}

	void Action_base::OnLeave()
	{
		creature_.reset();
	}

	void Action_base::OnStop()
	{
		//creature_.reset();
	}

	void Action_base::Run(unsigned elapsed)
	{ 
		if (state_ == ACTION_STATE_PAUSE) 
			this->OnUpdate(elapsed) ;
		else if (state_ == ACTION_STATE_RUN)
			this->OnRun(elapsed);
	}

	void Action_base::Pause() 
	{
		if (state_ == ACTION_STATE_RUN)
		{
			state_ = ACTION_STATE_PAUSE; 
			this->OnPause(); 
		} 
	}

	const bool Action_base::IsPause()const 
	{ 
		return state_ == ACTION_STATE_PAUSE;
	}

	void Action_base::Resume()
	{
		if (state_ == ACTION_STATE_PAUSE)
		{
			state_ = ACTION_STATE_RUN;
			this->OnResume();
		}
	}

	void Action_base::Stop()
	{
		if (state_ != ACTION_STATE_STOP)
		{
			state_ = ACTION_STATE_STOP;
			this->OnStop();
		}
	}

	const bool Action_base::IsEnd()const
	{
		return state_ == ACTION_STATE_STOP;
	}

	void Action_base::HandleSpeedDown(int)
	{
	}
}