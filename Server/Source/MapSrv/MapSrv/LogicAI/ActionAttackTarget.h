﻿#pragma once

#include "Action.h"
#include "ActionFollow.h"

namespace logicai
{
	class ActionAttackTarget : public ActionFollow
	{
	public:
		ActionAttackTarget(CreaturePtr &, ObjPtr &, unsigned);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnFaitBegin();

		virtual void OnFaitEnd();

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return 0; }

	private:
		unsigned attackTime_;
		unsigned curseTime_;
	};
}
