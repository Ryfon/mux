﻿#pragma once

#include "Action.h"
#include "ActionMoveTo.h"

namespace logicai
{
	class ActionWander : public Action_base
	{
	public:
		ActionWander(CreaturePtr &);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin();

		virtual void OnFaitEnd();

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return ACTION_MOVE_WANDER; }

		void HandleSpeedDown(int);

	private:
		void NewTargetPos();

		unsigned wanderIdleTime_;
		MoveControl2 move_;
	};
}
