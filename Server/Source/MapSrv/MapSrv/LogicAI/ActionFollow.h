﻿#pragma once

#include "Action.h"
#include "MoveControl2.h"

namespace logicai
{
	class ActionFollow : public Action_base
	{
	public:
		ActionFollow(CreaturePtr &, ObjPtr &);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin() ;

		virtual void OnFaitEnd() ;

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return ACTION_MOVE_FOLLOWING; }

		const bool IsLostTarget()const { return target_ == 0; }

		ObjPtr GetFollowTarget()const { return target_; }

		void HandleSpeedDown(int);

	private:
		class State;

		class TargetInsightState;
		friend class TargetInsightState;

		class TargetUninsightState;
		friend class TargetUninsightState;

		class TargetLostState;
		friend class TargetLostState;

		void ChangeState(State * newState);

		State * state_;
		ObjPtr target_;
		MoveControl2 move_;

	};

	class ActionFollow::State
	{
	public:
		virtual void OnEnter(ActionFollow &) = 0;

		virtual void OnLeave(ActionFollow &) = 0;

		virtual void OnPause(ActionFollow &) = 0;

		virtual void OnResume(ActionFollow &) = 0;

		virtual void OnStop(ActionFollow &) = 0;

		virtual void OnLostObj(ActionFollow &, ObjPtr &) = 0;

		virtual void OnUpdate(ActionFollow &, unsigned elapsed) = 0;

		virtual void OnRun(ActionFollow &, unsigned elapsed) = 0;
	};

	class ActionFollow::TargetInsightState : public ActionFollow::State
	{
	public:
		virtual void OnEnter(ActionFollow &);

		virtual void OnLeave(ActionFollow &);

		virtual void OnPause(ActionFollow &);

		virtual void OnResume(ActionFollow &);

		virtual void OnStop(ActionFollow &);

		virtual void OnLostObj(ActionFollow &, ObjPtr &);

		virtual void OnUpdate(ActionFollow &, unsigned elapsed);

		virtual void OnRun(ActionFollow &, unsigned elapsed);

		static TargetInsightState * instance();
	};

	class ActionFollow::TargetUninsightState : public ActionFollow::State
	{
	public:
		virtual void OnEnter(ActionFollow &);

		virtual void OnLeave(ActionFollow &);

		virtual void OnPause(ActionFollow &);

		virtual void OnResume(ActionFollow &);

		virtual void OnStop(ActionFollow &);

		virtual void OnLostObj(ActionFollow &, ObjPtr &);

		virtual void OnUpdate(ActionFollow &, unsigned elapsed);

		virtual void OnRun(ActionFollow &, unsigned elapsed);

		static TargetUninsightState * instance();
	};

	class ActionFollow::TargetLostState : public ActionFollow::State
	{
	public:
		virtual void OnEnter(ActionFollow &);

		virtual void OnLeave(ActionFollow &);

		virtual void OnPause(ActionFollow &);

		virtual void OnResume(ActionFollow &);

		virtual void OnStop(ActionFollow &);

		virtual void OnLostObj(ActionFollow &, ObjPtr &);

		virtual void OnUpdate(ActionFollow &, unsigned elapsed);

		virtual void OnRun(ActionFollow &, unsigned elapsed);

		static TargetLostState * instance();
	};
}
