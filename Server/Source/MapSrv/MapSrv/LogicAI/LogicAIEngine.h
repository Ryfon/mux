﻿#ifndef __MUX_LOGIC_AI_ENGINE_H__
#define __MUX_LOGIC_AI_ENGINE_H__

#pragma once

#include <cassert>
#include <map>
#include <cmath>

#include "L.h"
#include "ObjManager.h"

//#define AI_ASSERT(expr) assert(expr)
#define AI_ASSERT(expr)

namespace logicai
{
	const int svc_succeed = 1;

	class LogicAIEngine
	{
		float sinValues_[360];
		scripts::L * L_;
		ObjManager objMgr_;

		void InitSinValues();

		std::map<unsigned, std::pair<int, unsigned> > & GetMonsterTaskMap();

		LogicAIEngine();

	public:
		static float min_alert_dist;

		~LogicAIEngine();

		scripts::L & L();

		const bool Init();

		void Finit();

		static void Refresh();

		void HandleTimer(unsigned long monsterId);

		void HandleMonsterBorn(unsigned long monsterId, const AIMapInfo & mapInfo);

		void HandleMonsterDied(unsigned long monsterId);

		void HandleDetectPlayer(unsigned long monsterId);

		void HandleLosePlayer(unsigned long monsterId);

		void HandleDetectMonster(unsigned long monsterId);

		void HandleLoseMonster( unsigned long monsterId);

		void HandleAttacked(unsigned long monsterId);

		void HandleStartFait(unsigned long monsterId);

		void HandleStopFait(unsigned long monsterId);

		void HandleStartBound(unsigned long monsterId);

		void HandleStopBound(unsigned long monsterId);

		void HandleMosterBuffStart(unsigned long monsterId);

		void HandleMonsterBuffEnd(unsigned long monsterId);

		void HandleStartFollow(unsigned long monsterId);

		void HandleStopFollow(unsigned long monsterId);

		void HandleBeginChat(unsigned long monsterId);

		void HandleMoveChange(unsigned long monsterId);

		void HandleStartNotifyThreatList(unsigned long monsterId);

		void HandleStopNotifyThreatList(unsigned long monsterId);

		void HandleCreateNewLacquey(unsigned long monsterId);

		void HandleCreateNewLacquey(unsigned long monsterId, unsigned flag);

		void HandlePlayerAttacked(unsigned long monsterId);

		void HandlePlayerCured(unsigned long monsterId);

		void HandlePlayerDropTask(unsigned long monsterId);

		void HandlePlayerDied(const PlayerID & id);

		void HandlePlayerDied(unsigned long monsterId);

		void HandlePlayerLeaveMap(const PlayerID & id, bool isSwitchMap);

		void OnBeKickBack(unsigned long monsterId);

		float FastSin(float angle);

		const ACE_Time_Value & GetCurrentTime();

		ObjManager & GetObjMgr();

		static int BindMonsterTask(unsigned classId, int taskId, unsigned failTime);

		static void SetMinAlertDist(float f);

		static int NotifyLevelMapInfo(const AIMapInfo & map, unsigned count);

		std::pair<int, unsigned> FindMonsterTask(unsigned classId);

		static LogicAIEngine & instance();

	}; // class LogicAIEngine

} // namespace logicai

#endif

