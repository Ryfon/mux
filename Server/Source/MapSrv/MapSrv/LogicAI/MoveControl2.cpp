﻿#include "MoveControl2.h"
#include "Creature.h"

//#define DEBUG_LOCATION 1

namespace logicai
{
	MoveControl2::MoveControl2(CreaturePtr &obj, float speed, const position & pos)
		: obj_(obj)
		, targetPos_(pos)
		, speed_(speed)
		, xspeed_(1.0f)
		, yspeed_(0)
		, fspeed_(1.0f)
		, state_(0)
	{
		if (speed_ <= 0)
			speed_ = 1.5f;

		state_ = ReachState::instance();
	}

	MoveControl2::~MoveControl2()
	{
	}

	void MoveControl2::SetTargetPos(const position & pos, bool notifyStop)
	{
		if (targetPos_ != pos)
		{
			state_->Stop(*this, notifyStop);
			targetPos_ = pos;
		}
	}

	const position & MoveControl2::GetTargetPos()const
	{
		return targetPos_;
	}

	void MoveControl2::SetSpeed(float s)
	{
		if (s != speed_ && s > 0)
		{
			if (state_ == MovingState::instance())
			{
				xspeed_ = xspeed_ * s / speed_;
				yspeed_ = yspeed_ * s / speed_;

				CMonsterWrapper::MonsterStartMove(obj_->GetMapInfo(), obj_->GetId(), (_pos)obj_->GetCurPos().GetX(),
					(_pos)obj_->GetCurPos().GetY(), (_pos)targetPos_.GetX(), (_pos)targetPos_.GetY(), s * fspeed_);
			}
			speed_ = s;
		}
	}

	float MoveControl2::GetSpeed()const
	{
		return speed_;
	}

	void MoveControl2::SetSpeedf(float f)
	{
		if (f != fspeed_ && f > 0)
		{
			fspeed_ = f;
			if (state_ == MovingState::instance())
			{
				CMonsterWrapper::MonsterStartMove(obj_->GetMapInfo(), obj_->GetId(), (_pos)obj_->GetCurPos().GetX(),
					(_pos)obj_->GetCurPos().GetY(), (_pos)targetPos_.GetX(), (_pos)targetPos_.GetY(), speed_ * fspeed_);
			}
		}
	}

	float MoveControl2::GetSpeedf()const
	{
		return fspeed_;
	}

	void MoveControl2::Pause()
	{
		state_->Pause(*this);
	}

	bool MoveControl2::IsPause()const
	{
		return state_ == PauseState::instance();
	}

	void MoveControl2::Stop()
	{
		state_->Stop(*this, true);
	}

	bool MoveControl2::IsStop()const
	{
		return !obj_->GetMovingMobility() || state_ == StopState::instance();
	}

	void MoveControl2::Resume(bool doUpdate)
	{
		state_->Resume(*this);

		if (doUpdate)
			state_->Update(*this, 0);
	}

	bool MoveControl2::IsReach()const
	{
		return !obj_->GetMovingMobility() || state_ == ReachState::instance();
	}

	bool MoveControl2::IsMoving()const
	{
		return state_ != StopState::instance() && state_ != PauseState::instance();
	}

	void MoveControl2::FaitBegin()
	{
		if (IsMoving() && obj_->GetMovingMobility())
		{
			Stop();
			needResumeMove_ = true;
		}
	}

	void MoveControl2::BoundBegin()
	{
		if (IsMoving() && obj_->GetMovingMobility())
		{
			Stop();
			needResumeMove_ = true;
		}
	}

	void MoveControl2::FaitEnd()
	{
		if (needResumeMove_ && !obj_->IsBound() && obj_->GetMovingMobility())
		{
			Resume();
			needResumeMove_ = false;
		}
	}

	void MoveControl2::BoundEnd()
	{
		if (needResumeMove_ && !obj_->IsFait() && obj_->GetMovingMobility())
		{
			Resume();
			needResumeMove_ = false;
		}
	}

	void MoveControl2::Update(unsigned elapsed)
	{
		if (obj_->GetMovingMobility() && !obj_->IsFait() && !obj_->IsBound())
			state_->Update(*this, elapsed);
	}

	void MoveControl2::Reset()
	{
		if (!this->IsStop())
		{
			this->Stop();
			obj_.reset();
		}
	}

	void MoveControl2::HandleSpeedDown(int val)
	{
		if (val > 100 || val < 0) return;

		fspeed_ = (100 - val) / 100.0f;

		if (this->IsMoving())
		{
			this->Stop();
			this->Resume();
		}
	}

	MoveControl2::StopState * MoveControl2::StopState::instance()
	{
		static StopState inst;
		return &inst;
	}

	MoveControl2::PauseState * MoveControl2::PauseState::instance()
	{
		static PauseState inst;
		return &inst;
	}

	MoveControl2::BeginMoveState * MoveControl2::BeginMoveState::instance()
	{
		static BeginMoveState inst;
		return &inst;
	}

	MoveControl2::MovingState * MoveControl2::MovingState::instance()
	{
		static MovingState inst;
		return &inst;
	}

	MoveControl2::ReachState * MoveControl2::ReachState::instance()
	{
		static ReachState inst;
		return &inst;
	}

	void MoveControl2::StopState::Enter(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::StopState::Leave(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::StopState::Update(MoveControl2 & /*move*/, unsigned /*elapsed*/)
	{
	}

	void MoveControl2::StopState::Stop(MoveControl2 & /*move*/, bool)
	{
	}

	void MoveControl2::StopState::Pause(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::StopState::Resume(MoveControl2 & move)
	{
		this->Leave(move);
		move.state_ = MoveControl2::BeginMoveState::instance();
		move.state_->Enter(move);
	}

	void MoveControl2::PauseState::Enter(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::PauseState::Leave(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::PauseState::Update(MoveControl2 & /*move*/, unsigned /*elapsed*/)
	{
	}

	void MoveControl2::PauseState::Stop(MoveControl2 & move, bool)
	{
		this->Leave(move);
		move.state_ = MoveControl2::StopState::instance();
		move.state_->Enter(move);
	}

	void MoveControl2::PauseState::Pause(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::PauseState::Resume(MoveControl2 & move)
	{
		this->Leave(move);
		move.state_ = MoveControl2::BeginMoveState::instance();
		move.state_->Enter(move);
	}

	void MoveControl2::BeginMoveState::Enter(MoveControl2 & move)
	{
		if (move.obj_->IsUpdate())
			Update(move, 0);
	}

	void MoveControl2::BeginMoveState::Leave(MoveControl2 &)
	{
	}

	const char * now()
	{
		static char buf[128];
		time_t t = ::time(0);
		::strftime(buf, sizeof(buf), "%x %X", ::localtime(&t));
		return buf;
	}

	void MoveControl2::BeginMoveState::Update(MoveControl2 & move, unsigned /*elapsed*/)
	{
		if (move.obj_->GetCurPos() == move.targetPos_)
		{
			this->Leave(move);
			move.state_ = MoveControl2::ReachState::instance();
			move.state_->Enter(move);
			return;
		}

		float dx = move.targetPos_.x - move.obj_->GetCurPos().x;
		float dy = move.targetPos_.y - move.obj_->GetCurPos().y;
		float d = sqrt(dx * dx + dy * dy);
		move.xspeed_ = move.speed_ * dx / d;
		move.yspeed_ = move.speed_ * dy / d;

		CMonsterWrapper::MonsterStartMove(move.obj_->GetMapInfo(), move.obj_->GetId(), 
			(_pos)move.obj_->GetCurPos().GetX(), (_pos)move.obj_->GetCurPos().GetY(), (_pos)move.targetPos_.GetX(), 
			(_pos)move.targetPos_.GetY(), move.speed_ * move.fspeed_);

		move.beginPos_ = move.obj_->GetCurPos();

		this->Leave(move);
		move.state_ = MoveControl2::MovingState::instance();
		move.state_->Enter(move);

#ifdef DEBUG_LOCATION
		std::cout << now() << " begin moving from (" << move.beginPos_.x << "," << move.beginPos_.y << ") to (" << move.targetPos_.x << "," << move.targetPos_.y << ")" << std::endl;
#endif
	}

	void MoveControl2::BeginMoveState::Stop(MoveControl2 & move, bool)
	{
		this->Leave(move);
		move.state_ = MoveControl2::StopState::instance();
		move.state_->Enter(move);
	}

	void MoveControl2::BeginMoveState::Pause(MoveControl2 & move)
	{
		this->Leave(move);
		move.state_ = MoveControl2::PauseState::instance();
		move.state_->Enter(move);
	}

	void MoveControl2::BeginMoveState::Resume(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::MovingState::Enter(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::MovingState::Leave(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::MovingState::Update(MoveControl2 & move, unsigned elapsed)
	{
#ifdef DEBUG_LOCATION
		if (move.GetTargetPos().GetX() >= 512 || move.GetTargetPos().GetY() >= 512)
		{
			AI_ASSERT(false);
		}
#endif

		if (move.targetPos_ == move.obj_->GetCurPos())
		{
			Reach(move);
			return;
		}

		position curPos = move.obj_->GetCurPos();
		curPos.x += move.xspeed_ * move.fspeed_ * elapsed / 1000.0f;
		curPos.y += move.yspeed_ * move.fspeed_ * elapsed / 1000.0f;

		if (::abs(curPos.GetX() - move.beginPos_.GetX()) > ::abs(move.targetPos_.GetX() - move.beginPos_.GetX()))
			curPos.x = move.targetPos_.GetX();

		if (::abs(curPos.GetY() - move.beginPos_.GetY()) > ::abs(move.targetPos_.GetY() - move.beginPos_.GetY()))
			curPos.y = move.targetPos_.GetY();

		if (curPos.GetX() != move.obj_->GetCurPos().GetX() || curPos.GetY() != move.obj_->GetCurPos().GetY())
		{
			CMonsterWrapper::MonsterBeyondGrid(move.obj_->GetMapInfo(), move.obj_->GetId(),
				move.obj_->GetCurPos().GetX(), move.obj_->GetCurPos().GetY(), curPos.GetX(), curPos.GetY());

		}

#ifdef DEBUG_LOCATION
		std::cout << now() << " moving at location (" << curPos.x << "," << curPos.y << ")" << std::endl;
#endif

		move.obj_->SetCurPos(curPos);

		if (move.obj_->GetCurPos() == move.targetPos_)
		{
			Reach(move);
		}
	}

	void MoveControl2::MovingState::Reach(MoveControl2 & move)
	{
		CMonsterWrapper::MonsterStopMove(move.obj_->GetMapInfo(), move.obj_->GetId());

		_pos x = (_pos)move.obj_->GetCurPos().GetX(), y = (_pos)move.obj_->GetCurPos().GetY();
		CMonsterWrapper::GetMonsterPosition(move.obj_->GetMapInfo(), move.obj_->GetId(), x, y);
		move.obj_->SetCurPos(x, y);

		this->Leave(move);
		move.state_ = MoveControl2::ReachState::instance();
		move.state_->Enter(move);

#ifdef DEBUG_LOCATION
		std::cout << now() << " reaching target location (" << move.targetPos_.x << "," << move.targetPos_.y << ") from (" << move.beginPos_.x << "," << move.beginPos_.y << ")" << std::endl;
#endif
	}

	void MoveControl2::MovingState::Stop(MoveControl2 & move, bool notifyStop)
	{
		if (notifyStop)
			CMonsterWrapper::MonsterStopMove(move.obj_->GetMapInfo(), move.obj_->GetId());

		_pos x = (_pos)move.obj_->GetCurPos().x, y = (_pos)move.obj_->GetCurPos().y;
		CMonsterWrapper::GetMonsterPosition(move.obj_->GetMapInfo(), move.obj_->GetId(), x, y);
		move.obj_->SetCurPos(x, y);

		this->Leave(move);
		move.state_ = MoveControl2::StopState::instance();
		move.state_->Enter(move);
	}

	void MoveControl2::MovingState::Pause(MoveControl2 & move)
	{
		CMonsterWrapper::MonsterStopMove(move.obj_->GetMapInfo(), move.obj_->GetId());

		_pos x = (_pos)move.obj_->GetCurPos().x, y = (_pos)move.obj_->GetCurPos().y;
		CMonsterWrapper::GetMonsterPosition(move.obj_->GetMapInfo(), move.obj_->GetId(), x, y);
		move.obj_->SetCurPos(x, y);

		this->Leave(move);
		move.state_ = MoveControl2::PauseState::instance();
		move.state_->Enter(move);
	}

	void MoveControl2::MovingState::Resume(MoveControl2 & /*move*/)
	{
	}

	void MoveControl2::ReachState::Enter(MoveControl2 &)
	{
	}

	void MoveControl2::ReachState::Leave(MoveControl2 &)
	{
	}

	void MoveControl2::ReachState::Update(MoveControl2 & /*move*/, unsigned /*elapsed*/)
	{
	}

	void MoveControl2::ReachState::Stop(MoveControl2 & move, bool)
	{
		this->Leave(move);
		move.state_ = MoveControl2::StopState::instance();
		move.state_->Enter(move);
	}

	void MoveControl2::ReachState::Pause(MoveControl2 & move)
	{
		this->Leave(move);
		move.state_ = MoveControl2::PauseState::instance();
		move.state_->Enter(move);
	}

	void MoveControl2::ReachState::Resume(MoveControl2 & move)
	{
		this->Leave(move);
		move.state_ = MoveControl2::BeginMoveState::instance();
		move.state_->Enter(move);
	}

} // namespace logicai
