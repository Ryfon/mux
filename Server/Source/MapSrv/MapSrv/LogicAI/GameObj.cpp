﻿#include "GameObj.h"
#include "LogicAIEngine.h"

namespace logicai
{
	using scripts::Ltable;

	namespace scripts
	{
		template<>
		GameObj * L_tovalue<GameObj *>(lua_State & l, int idx)
		{
			return Lclass<GameObj>::L_getuserdata(l, idx);
		}

		template<>
		GameObj * L_totopvalue_pop<GameObj *>(lua_State & l)
		{
			GameObj * p = L_tovalue<GameObj *>(l, -1);
			lua_pop(&l, 1);
			return p;
		}

		int L_pushvalue(lua_State & l, GameObj * p)
		{
			if (!p)
				return 0;

			lua_getglobal(&l, p->GetIdent().c_str());
			return 1;
		}
	}

	GameObj::GameObj(const unsigned objId)
		: objId_(objId) 
		, ref_count_(0)
	{
		//lastUpdateTime_ = ACE_Time_Value::zero;
		lastUpdateTime_ = ACE_OS::gettimeofday();
	}

	GameObj::~GameObj()
	{
	}

	const unsigned GameObj::GetId()const
	{
		return objId_;
	}

	void GameObj::Observe_DiedEvent(GameObj * obj, unsigned)
	{
		Observe_LostEvent(obj, 0);			
	}

	void GameObj::Notify_DiedEvent()
	{
		Notify_LostEvent();
	}

	void GameObj::Observe_LostEvent(GameObj * obj, unsigned)
	{
		AI_ASSERT(obj);

		if (!obj || obj == this) return;

		if (obj->observers_.end() == std::find(obj->observers_.begin(), obj->observers_.end(), ObjPtr(this)))
		{
			obj->observers_.push_back(ObjPtr(this));
			this->observing_.push_back(ObjPtr(obj));
		}
	}

	void GameObj::UnobserveAll_LostEvent()
	{
		for(std::vector<ObjPtr>::iterator it = this->observing_.begin(); it != this->observing_.end(); ++it)
		{
			((*it))->observers_.erase(
				std::remove((*it)->observers_.begin(), (*it)->observers_.end(), ObjPtr(this)), (*it)->observers_.end());
		}

		this->observing_.clear();
	}

	void GameObj::Notify_LostEvent()
	{
		for(std::vector<ObjPtr>::iterator it = this->observers_.begin(); it != this->observers_.end(); ++it)
		{
			AI_ASSERT(it->get());

			EXCEPT_TRY
			ObjPtr obj(this);
			(*it)->OnReceivedLostEvent(obj);
			EXCEPT_CATCH

			(*it)->observing_.erase(
				std::remove((*it)->observing_.begin(), (*it)->observing_.end(), ObjPtr(this)), 
				(*it)->observing_.end());
		}

		this->observers_.clear();

		UnobserveAll_LostEvent();
	}

	void GameObj::AddRef()
	{
		++ref_count_;
		//if (this->IsPlayer()) D_DEBUG("player[%s] ref count: %u\n", this->GetName().c_str(), ref_count_);
	}

	void GameObj::ReleaseRef()
	{
		AI_ASSERT(ref_count_);
		--ref_count_;
		//if (this->IsPlayer()) D_DEBUG("player[%s] ref count: %u\n", this->GetName().c_str(), ref_count_);
	}

	unsigned GameObj::RefCount()const
	{
		return ref_count_;
	}


	void intrusive_traits<GameObj>::add_ref(GameObj * obj)
	{
		AI_ASSERT(obj);

		if (obj)
			obj->AddRef();
	}


	void intrusive_traits<GameObj>::release_ref(GameObj * obj)
	{
		AI_ASSERT(obj);

		if (obj)
		{
			obj->ReleaseRef();
			if (obj->RefCount() == 0)
				LogicAIEngine::instance().L().set_nil(obj->GetIdent().c_str());
		}
	}


	DistCheckPred::DistCheckPred(logicai::GameObj & _obj)
		: obj(_obj) { }

	const bool DistCheckPred::operator ()(const ObjPtr & left, const ObjPtr & right) const
	{
		float dx1 = obj.GetCurPos().x - left->GetCurPos().x;
		float dy1 = obj.GetCurPos().y - left->GetCurPos().y;
		float dx2 = obj.GetCurPos().x - right->GetCurPos().x;
		float dy2 = obj.GetCurPos().y - right->GetCurPos().y;

		return dx1 * dx1 + dy1 * dy1 < dx2 * dx2 + dy2 * dy2;
	}
}