﻿#include "ActionSummon.h"
#include "Creature.h"
#include "LogicAIEngine.h"

namespace logicai
{
	unsigned ActionSummon::action_count_ = 0;
	std::map<unsigned, ActionSummon *> ActionSummon::actions_;

	ActionSummon::ActionSummon(CreaturePtr & creature, unsigned cls, unsigned count, const position & pos)
		: Action_base(creature), class_(cls), pos_(pos), action_id_(++action_count_), count_(count)
	{
		actions_[action_id_] = this;
	}

	void ActionSummon::OnEnter()
	{
		for(unsigned i = 0; i < count_; ++i)
		{
			// 自动继承第一刀
			CMonsterWrapper::CallMonsterReq(this->creature_->GetId(), class_, this->creature_->GetMapInfo(), 
				pos_.x, pos_.y, this->creature_->GetId());
		}

	}

	void ActionSummon::OnLeave()
	{
		this->Stop();
		Action_base::OnLeave();
	}

	void ActionSummon::OnResume()
	{
	}

	void ActionSummon::OnStop()
	{
		actions_.erase(action_id_);
		Action_base::OnStop();
	}

	void ActionSummon::OnRun(unsigned elapsed)
	{
		this->Stop();
	}

	/*void ActionSummon::MonsterSummonedCallback(Creature & creature, unsigned flag)
	{
		std::map<unsigned, ActionSummon *>::iterator it = actions_.find(flag);
		if (it != actions_.end())
		{
			try {
				LogicAIEngine::instance().L().Table(
					it->second->creature_.GetName().c_str()).Mfunction<void (GameObj *)>("_HandleCreatureSummoned")(&creature);
			}catch(...) {
				actions_.erase(it);
				throw;
			}

			actions_.erase(it);
		}
	}*/
}