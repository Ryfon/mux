﻿#include "Event.h"

#include "GameObj.h"

namespace logicai
{
	const bool Event_base::operator==(const Event_base & r)const
	{
		return evtId == r.evtId 
			&& obj.GetId() == r.obj.GetId()
			&& seq == r.seq;
	}

	const bool Event_holder::operator==(const Event_holder & r)const
	{
		return Get<Event_base>() == r.Get<Event_base>();
	}
}