﻿#pragma once

#include "Action.h"
#include "ActionFollow.h"

namespace logicai
{
	class ActionTreat : public ActionFollow
	{
	public:
		ActionTreat(CreaturePtr &, ObjPtr &, unsigned);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin();

		virtual void OnFaitEnd() ;

		virtual void OnBoundBegin();

		virtual void OnBoundEnd() ;

		virtual void OnLostObj(ObjPtr &);

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return ACTION_ASSIST_TREAT; }

	private:
		unsigned skill_;
	};
}
