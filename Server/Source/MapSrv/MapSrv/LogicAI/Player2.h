﻿#pragma once

#ifndef __MUX_LOGICAI_PLAYER_H__
#define __MUX_LOGICAI_PLAYER_H__

#include "GameObj.h"
#include "Creature.h"

namespace logicai
{
	class Player : public GameObj
	{
	public:
		Player(const unsigned objID, const unsigned gateID, const unsigned mapId);

		virtual ~Player();

		virtual const std::string & GetName()const;

		virtual const std::string & GetIdent()const;

		virtual const position & GetCurPos()const;

		virtual const unsigned GetMapId()const;

		virtual const unsigned GetSize() const;

		virtual const unsigned GetRace()const;

		virtual const bool GetEvil()const;

		virtual const bool GetRogue()const;

		virtual const unsigned GetInherentStand()const { return 0; }

		virtual const bool IsCanBeAttacked()const { return true; }

		virtual void OnDied();

		virtual void OnLost();

		virtual void OnReceivedDiedEvent(ObjPtr &);

		virtual void OnReceivedLostEvent(ObjPtr &);

		virtual void OnUpdate(unsigned);

		virtual const bool IsPlayer()const;

		const unsigned GetGateId()const;

		virtual const MUX_PROTO::PlayerID GetMPPid()const;

		const int GetSex()const;

		const float GetHpPercent()const;

		const unsigned GetLevel()const;

		void SetHpPercent(float);

		virtual void EnterView(ObjPtr &) { }

		virtual void LeaveView(ObjPtr &) { }

		virtual void OnAttacked(ObjPtr &, unsigned, bool);

		virtual void Reset();

		float GetMpPercent()const { return 0; }
		
		void SetMpPercent(float) { }

		CreaturePtr & GetTaskObj();

		void SetTaskObj(CreaturePtr obj);

		bool CheckValid()const;

		static unsigned long long g_update_count_;

	private:
		void GetPlayerInfo()const;

		void GetPlayerPosition()const;

		mutable std::string name_;
		const std::string ident_;
		MUX_PROTO::PlayerID playerID_;
		unsigned mapId_;
		mutable char sex_;
		mutable int level_;
		mutable float hp_;
		mutable unsigned race_;
		mutable bool evil_;
		mutable bool rogue_;
		mutable float hpPercent_;
		mutable position curPos_;
		mutable unsigned long long update_count_;
		CreaturePtr taskObj_;
	};
}
#endif
