﻿#include "ActionFollow.h"
#include "Creature.h"
#include "ActionFight.h"
#include "LogicAIEngine.h"

namespace logicai
{
	ActionFollow::ActionFollow(CreaturePtr & creature, ObjPtr & target)
		: Action_base(creature), target_(target), move_(creature, 1.0f, target->GetCurPos())
	{
		state_ = TargetInsightState::instance();
	}

	void ActionFollow::OnEnter()
	{
		move_.SetSpeed(this->creature_->GetFollowingSpeed());

		this->creature_->Observe_LostEvent(target_.get(), 0);

		state_->OnEnter(*this);
	}

	void ActionFollow::OnLeave()
	{
		state_->OnLeave(*this);

		move_.Reset();
		Action_base::OnLeave();
	}

	void ActionFollow::OnPause()
	{
		state_->OnPause(*this);
	}

	void ActionFollow::OnResume()
	{
		state_->OnResume(*this);
	}

	void ActionFollow::OnStop()
	{
		state_->OnStop(*this);
		target_.reset();
		move_.Reset();
		Action_base::OnStop();
	}

	void ActionFollow::OnFaitBegin()
	{ 
		move_.FaitBegin();
	}

	void ActionFollow::OnFaitEnd()  
	{
		move_.FaitEnd();
	}

	void ActionFollow::OnBoundBegin()
	{
		move_.BoundBegin();
	}

	void ActionFollow::OnBoundEnd() 
	{
		move_.BoundEnd();
	}

	void ActionFollow::OnLostObj(ObjPtr & obj)
	{
		if (target_.get())
		{
			state_->OnLostObj(*this, obj);
			if (target_ == obj)
			{
				ChangeState(TargetLostState::instance());
				target_.reset();
			}
		}
	}

	void ActionFollow::OnUpdate(unsigned elapsed)
	{
		state_->OnUpdate(*this, elapsed);
	}

	void ActionFollow::OnRun(unsigned elapsed)
	{
		state_->OnRun(*this, elapsed);
	}

	void ActionFollow::ChangeState(State * newState)
	{
		if (state_ != newState)
		{
			state_->OnLeave(*this);
			state_ = newState;
			state_->OnEnter(*this);
		}
	}

	void ActionFollow::HandleSpeedDown(int val)
	{
		move_.HandleSpeedDown(val);
	}

	ActionFollow::TargetInsightState * ActionFollow::TargetInsightState::instance()
	{
		static TargetInsightState inst;
		return &inst;
	}

	void ActionFollow::TargetInsightState::OnEnter(ActionFollow & follow)
	{
		follow.move_.SetTargetPos(follow.target_->GetCurPos());
		follow.Resume();
	}

	void ActionFollow::TargetInsightState::OnLeave(ActionFollow & follow)
	{
		follow.move_.Stop();
	}

	void ActionFollow::TargetInsightState::OnPause(ActionFollow & follow)
	{
		follow.move_.Pause();
	}

	void ActionFollow::TargetInsightState::OnResume(ActionFollow & follow)
	{
		follow.move_.Resume();
	}

	void ActionFollow::TargetInsightState::OnStop(ActionFollow & follow)
	{
		follow.move_.Stop();
	}

	void ActionFollow::TargetInsightState::OnLostObj(ActionFollow & follow, ObjPtr & obj)
	{
		if (obj == follow.target_)
		{
			follow.move_.Stop();

			follow.ChangeState(ActionFollow::TargetLostState::instance());
		}
	}

	void ActionFollow::TargetInsightState::OnUpdate(ActionFollow & follow, unsigned elapsed)
	{
	}

	void ActionFollow::TargetInsightState::OnRun(ActionFollow & follow, unsigned elapsed)
	{
		if (follow.creature_->IsFait() || follow.creature_->IsBound())
			return;

		if (!follow.target_.get())
		{
			follow.ChangeState(ActionFollow::TargetLostState::instance());
			return;
		}

		follow.move_.Update(elapsed);

		if (!follow.creature_->IsInsight(follow.target_))
		{
			follow.ChangeState(ActionFollow::TargetUninsightState::instance());
			return ;
		}

		position pos = follow.target_->GetCurPos();
		if (fabs(pos.x - follow.move_.GetTargetPos().x) > 1.5f ||
			fabs(pos.y - follow.move_.GetTargetPos().y) > 1.5f)
		{
			follow.move_.SetTargetPos(pos);
			follow.move_.Resume();
		}
#ifdef WIN32
		if (follow.move_.IsReach() || (abs(pos.GetX() - follow.move_.GetTargetPos().GetX()) >= 3.0f ||
			abs(pos.GetY() - follow.move_.GetTargetPos().GetY()) >= 3.0f))
#else
		if (follow.move_.IsReach() || (fabs(pos.GetX() - follow.move_.GetTargetPos().GetX()) >= 3.0f ||
			fabs(pos.GetY() - follow.move_.GetTargetPos().GetY()) >= 3.0f))
#endif
		{
			follow.move_.SetTargetPos(GetTargetNextPos(follow.target_), false);
			follow.move_.Resume(true);
		}
	}

	ActionFollow::TargetUninsightState * ActionFollow::TargetUninsightState::instance()
	{
		static TargetUninsightState inst;
		return &inst;
	}

	void ActionFollow::TargetUninsightState::OnEnter(ActionFollow & follow)
	{
		follow.move_.Stop();
	}

	void ActionFollow::TargetUninsightState::OnLeave(ActionFollow &)
	{

	}

	void ActionFollow::TargetUninsightState::OnPause(ActionFollow &)
	{

	}

	void ActionFollow::TargetUninsightState::OnResume(ActionFollow &)
	{

	}

	void ActionFollow::TargetUninsightState::OnStop(ActionFollow &)
	{

	}

	void ActionFollow::TargetUninsightState::OnLostObj(ActionFollow & follow, ObjPtr & obj)
	{
		AI_ASSERT(obj == follow.target_);

		follow.ChangeState(ActionFollow::TargetLostState::instance());
	}

	void ActionFollow::TargetUninsightState::OnUpdate(ActionFollow & , unsigned elapsed)
	{
	}

	void ActionFollow::TargetUninsightState::OnRun(ActionFollow & follow, unsigned elapsed)
	{
		if (!follow.target_.get())
		{
			follow.ChangeState(ActionFollow::TargetLostState::instance());
			return;
		}

		if (follow.creature_->IsInsight(follow.target_))
		{
			follow.ChangeState(ActionFollow::TargetInsightState::instance());
			return ;
		}
	}

	ActionFollow::TargetLostState * ActionFollow::TargetLostState::instance()
	{
		static TargetLostState inst;
		return &inst;
	}

	void ActionFollow::TargetLostState::OnEnter(ActionFollow & follow)
	{
		follow.move_.Stop();
		follow.target_ = 0;
	}

	void ActionFollow::TargetLostState::OnLeave(ActionFollow &)
	{

	}

	void ActionFollow::TargetLostState::OnPause(ActionFollow &)
	{

	}

	void ActionFollow::TargetLostState::OnResume(ActionFollow &)
	{

	}

	void ActionFollow::TargetLostState::OnStop(ActionFollow &)
	{
	}

	void ActionFollow::TargetLostState::OnLostObj(ActionFollow &, ObjPtr &)
	{
	}

	void ActionFollow::TargetLostState::OnUpdate(ActionFollow &, unsigned elapsed)
	{
	}

	void ActionFollow::TargetLostState::OnRun(ActionFollow &, unsigned elapsed)
	{
	}
}