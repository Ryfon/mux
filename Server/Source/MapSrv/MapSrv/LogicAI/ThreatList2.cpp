﻿#include "ThreatList.h"
#include "Creature.h"
#include "Player2.h"

namespace logicai
{
	ThreatList::ThreatList(unsigned maxSize)
		: maxSize_(maxSize)
	{
	}

	int ThreatList::AddObj(ObjPtr & obj, unsigned threat)
	{
		int result = -1;
		unsigned idx = 0;
		if (HasCheck(obj, &idx))
		{
			if ( idx >= lst_.size() )
			{
				D_ERROR( "ThreatList::AddObj, idx(%d) >= lst_.size()\n", idx );
				return -1;
			}
			lst_[idx].threat += threat;
			result = 0;
		}
		else
		{
			if (Size() == MaxSize())
				CleanThreatNoChange();

			ThreatData data(obj, threat);
			lst_.push_back(data);

			result = 1;
		}

		std::sort(lst_.begin(), lst_.end(), std::greater<ThreatData>());
		if (Size() > MaxSize())
			lst_.erase(lst_.begin() + MaxSize(), lst_.end());
		return result;
	}

	void ThreatList::RemoveObj(ObjPtr & obj)
	{
		lst_.erase(std::remove(lst_.begin(), lst_.end(), obj), lst_.end());
	}

	ObjPtr ThreatList::GetMaxThreat()
	{
		if (lst_.empty())
			return ObjPtr(0);
		return lst_.front().obj;
	}
	
	unsigned ThreatList::MaxSize()const
	{
		return maxSize_;
	}

	void ThreatList::ResetMaxSize(unsigned size)
	{
		maxSize_ = size;
	}

	unsigned ThreatList::Size()const
	{
		return (unsigned)lst_.size();
	}

	const bool ThreatList::HasCheck(const ObjPtr & obj, unsigned * pIdx)const
	{
		std::vector<ThreatData>::const_iterator it = std::find(lst_.begin(), lst_.end(), obj);
		if (it != lst_.end())
		{
			if (pIdx) *pIdx = (unsigned)(it - lst_.begin());
			return true;
		}
		return false;
	}

	const bool ThreatList::FirstCheck(ObjPtr & obj)const
	{
		if (lst_.empty())
			return false;

		return lst_.front() == obj;
	}

	struct MinDist
	{
		const bool operator()(const ThreatList::ThreatData & l, const ThreatList::ThreatData & r)const
		{
			position lpos = l.obj->GetCurPos();
			position rpos = r.obj->GetCurPos();
			return lpos.x + lpos.y < rpos.x + rpos.y;
		}
	};

	ObjPtr ThreatList::GetMinDist()
	{
		std::vector<ThreatData>::const_iterator it = std::min_element(lst_.begin(), lst_.end(), MinDist());
		if (it != lst_.end())
			return it->obj;
		return ObjPtr(0);
	}

	void ThreatList::CleanThreatNoChange()
	{
		time_t now = time(0);
		bool loop = true;
		while(loop)
		{
			loop = false;
			std::vector<ThreatData>::iterator it;
			for( it = lst_.begin(); it != lst_.end(); ++it)
			{
				if (now - it->lastUpdate >= 30)
				{
					lst_.erase(it);
					loop = true;
					break;
				}
			}
		}
	}

	ObjPtr ThreatList::RandomCreature(time_t t)
	{
		return ObjPtr(0);
	}

	ObjPtr ThreatList::Get(unsigned idx)
	{
		if (idx >= Size())
			return 0;
		return lst_[idx].obj;
	}

	void ThreatList::Clear()
	{
		lst_.clear();
	}

	void ThreatList::Dump(_ThreatList & tl)const
	{
		tl.threatCount = 0;
		EXCEPT_TRY
		for(; tl.threatCount < lst_.size(); ++tl.threatCount)
		{
			tl.threatList[tl.threatCount].threatPlayerID = lst_[tl.threatCount].obj->GetMPPid();
			tl.threatList[tl.threatCount].threatValue = lst_[tl.threatCount].threat;
		}
		EXCEPT_CATCH
	}
}