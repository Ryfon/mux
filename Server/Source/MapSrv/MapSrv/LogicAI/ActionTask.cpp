﻿#include "ActionTask.h"
#include "Creature.h"
#include "ObjManager.h"
#include "LogicAIEngine.h"

namespace logicai
{
	ActionTask::ActionTask(CreaturePtr & creature, ObjPtr & target, unsigned taskId, unsigned waitTime)
		: Action_base(creature), target_(target), countdown_(0), taskId_(taskId), waitTime_(waitTime)
	{
		state_ = TargetInsightState::instance();
	}

	void ActionTask::OnEnter()
	{
		if (CMonsterWrapper::QueryPlayerTaskState(this->target_->GetMPPid(), taskId_) !=  NTS_TGT_NOT_ACHIEVE)
		{
			this->Stop();
			this->creature_->SetDieState(0);
			return;
		}
		this->creature_->Observe_LostEvent(target_.get(), 0);
	}

	void ActionTask::OnLeave()
	{
		state_->OnLeave(*this);
		Action_base::OnLeave();
	}

	void ActionTask::OnPause()
	{
		state_->OnPause(*this);
	}

	void ActionTask::OnResume()
	{
		state_->OnResume(*this);
	}

	void ActionTask::OnStop()
	{
		state_->OnStop(*this);
		Action_base::OnStop();
		target_.reset();
	}

	void ActionTask::OnLostObj(ObjPtr & obj)
	{
		if (!this->IsEnd() && obj == target_)
		{
			state_->OnLostObj(*this, obj);
			target_.reset();
		}
	}

	void ActionTask::OnUpdate(unsigned elapsed)
	{
		if (this->target_.get())
		{
			if (!this->target_->CheckValid())
				this->OnLostObj(this->target_);
		}
	}

	void ActionTask::OnRun(unsigned elapsed)
	{
		if (this->target_.get())
		{
			if (!this->target_->CheckValid())
				this->OnLostObj(this->target_);
		}

		state_->OnRun(*this, elapsed);
	}

	void ActionTask::ChangeState(State * newState)
	{
		if (state_ != newState)
		{
			state_->OnLeave(*this);
			state_ = newState;
			state_->OnEnter(*this);
		}
	}

	void ActionTask::SetTaskSucceed(int f)
	{
		if (target_.get())
		{
			CMonsterWrapper::SetTaskFinish(target_->GetMPPid(), taskId_, f != 0);
			this->Stop();
		}
	}

	void ActionTask::SetTaskFailed()
	{
		if (target_.get())
		{
			CMonsterWrapper::SetTaskFail(target_->GetMPPid(), taskId_);
			this->Stop();
		}
	}

	void ActionTask::DropTask()
	{
		if (target_.get())
		{
			this->Stop();
			this->creature_->SetDieState(0);
		}
	}

	ActionTask::TargetInsightState * ActionTask::TargetInsightState::instance()
	{
		static TargetInsightState inst;
		return &inst;
	}

	void ActionTask::TargetInsightState::OnEnter(ActionTask &)
	{
	}

	void ActionTask::TargetInsightState::OnLeave(ActionTask &)
	{
	}

	void ActionTask::TargetInsightState::OnPause(ActionTask &)
	{
	}

	void ActionTask::TargetInsightState::OnResume(ActionTask &)
	{
	}

	void ActionTask::TargetInsightState::OnStop(ActionTask &)
	{
	}

	void ActionTask::TargetInsightState::OnLostObj(ActionTask & task, ObjPtr & obj)
	{
		AI_ASSERT(obj == task.target_);

		task.ChangeState(ActionTask::TargetLostState::instance());
	}

	void ActionTask::TargetInsightState::OnUpdate(ActionTask & task, unsigned elapsed)
	{
		//this->OnRun(task, elapsed);
	}

	void ActionTask::TargetInsightState::OnRun(ActionTask & task, unsigned elapsed)
	{
		if (!task.creature_->IsInsight(task.target_))
		{
			task.countdown_ = task.waitTime_ * 1000;

			CMonsterWrapper::SetTaskFailTimeSt(task.target_->GetMPPid(), task.taskId_, task.waitTime_);

			task.ChangeState(ActionTask::TargetUninsightState::instance());
		}
	}

	ActionTask::TargetUninsightState * ActionTask::TargetUninsightState::instance()
	{
		static TargetUninsightState inst;
		return &inst;
	}

	void ActionTask::TargetUninsightState::OnEnter(ActionTask &)
	{
	}

	void ActionTask::TargetUninsightState::OnLeave(ActionTask &)
	{

	}

	void ActionTask::TargetUninsightState::OnPause(ActionTask &)
	{

	}

	void ActionTask::TargetUninsightState::OnResume(ActionTask &)
	{

	}

	void ActionTask::TargetUninsightState::OnStop(ActionTask &)
	{

	}

	void ActionTask::TargetUninsightState::OnLostObj(ActionTask & task, ObjPtr & obj)
	{
		if (obj == task.target_)
			task.ChangeState(ActionTask::TargetLostState::instance());
	}

	void ActionTask::TargetUninsightState::OnUpdate(ActionTask & , unsigned elapsed)
	{
	}

	void ActionTask::TargetUninsightState::OnRun(ActionTask & task, unsigned elapsed)
	{
		if (!task.target_.get())
		{
			task.ChangeState(ActionTask::TargetLostState::instance());
			return;
		}

		if (task.countdown_ > elapsed)
			task.countdown_ -= elapsed;
		else
			task.countdown_ = 0;

		if (task.countdown_ == 0)
		{
			task.ChangeState(ActionTask::TargetLostState::instance());
			return;
		}

		if (task.creature_->IsInsight(task.target_))
		{
			CMonsterWrapper::SetTaskFailTimeCancel(task.target_->GetMPPid(), task.taskId_);
			task.ChangeState(ActionTask::TargetInsightState::instance());
			return ;
		}
	}

	ActionTask::TargetLostState * ActionTask::TargetLostState::instance()
	{
		static TargetLostState inst;
		return &inst;
	}

	void ActionTask::TargetLostState::OnEnter(ActionTask & task)
	{
		CMonsterWrapper::SetTaskFail(task.target_->GetMPPid(), task.taskId_);
		//CMonsterWrapper::SetMonsterDie(task.creature_->GetMapInfo(), task.creature_->GetId(), false);
		LogicAIEngine::instance().GetObjMgr().AddDiedList(task.creature_);
		task.target_.reset();
		task.Stop();
	}

	void ActionTask::TargetLostState::OnLeave(ActionTask &)
	{

	}

	void ActionTask::TargetLostState::OnPause(ActionTask &)
	{

	}

	void ActionTask::TargetLostState::OnResume(ActionTask &)
	{

	}

	void ActionTask::TargetLostState::OnStop(ActionTask &)
	{
	}

	void ActionTask::TargetLostState::OnLostObj(ActionTask &, ObjPtr &)
	{
	}

	void ActionTask::TargetLostState::OnUpdate(ActionTask &, unsigned elapsed)
	{
	}

	void ActionTask::TargetLostState::OnRun(ActionTask &, unsigned elapsed)
	{

	}
}