﻿#pragma once

#include "Action.h"

namespace logicai
{
	class ActionDefend : public Action_base
	{
	public:
		ActionDefend(CreaturePtr &);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin();

		virtual void OnFaitEnd();

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return ACTION_FIGHT_ATTACK; }

	private:
		ObjPtr target_;
		unsigned curseTime_;
		unsigned reactTime_;
	};
}
