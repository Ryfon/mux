﻿#pragma once

#ifndef __MUX_LOGICAI_EVENT_H__
#define __MUX_LOGICAI_EVENT_H__

namespace logicai
{
	class GameObj;

	enum {
		EVENT_OBJ_DIED = 0,
		EVENT_OBJ_LEAVEVIEW,
		EVENT_OBJ_JUMPMAP,
		EVENT_OBJ_LOST,
	};

	struct Event_base
	{
		unsigned seq;
		GameObj & obj;
		unsigned evtId;

		Event_base(unsigned _seq, GameObj & _obj, unsigned evt)
			: seq(_seq), obj(_obj), evtId(evt) { }

		const bool operator==(const Event_base &)const;

	};

	struct DiedEvent : public Event_base
	{
		enum { EVENT_ID = EVENT_OBJ_DIED };

		DiedEvent(unsigned _seq, GameObj & _obj)
			: Event_base(_seq, _obj, EVENT_ID) { }


	};

	struct LeaveViewEvent : public Event_base
	{
		enum { EVENT_ID = EVENT_OBJ_LEAVEVIEW };

		LeaveViewEvent(unsigned _seq, GameObj & _obj)
			: Event_base(_seq, _obj, EVENT_ID) { }
	};

	struct JumpMapEvent : public Event_base
	{
		enum { EVENT_ID = EVENT_OBJ_JUMPMAP };

		JumpMapEvent(unsigned _seq, GameObj & _obj)
			: Event_base(_seq, _obj, EVENT_ID) { }
	};

	struct LoseEvent  : public Event_base // died, jum map or no reason
	{
		enum { EVENT_ID = EVENT_OBJ_LOST };

		LoseEvent(unsigned _seq, GameObj & _obj)
			: Event_base(_seq, _obj, EVENT_ID) { }
	};

	template<class H, class Tail>
	struct MaxTypeSize;

	template<class T>
	struct MaxTypeSize<T, void>
	{
		enum { size = sizeof(T) };
	};

	template<class T, class Tail>
	struct MaxTypeSize
	{
		enum { 
			Tsize = sizeof(T),
			Ttsize = Tail::size,
			size = Tsize > Ttsize ? Tsize : Ttsize
		};
	};

	struct Event_holder
	{
		char buf[MaxTypeSize<DiedEvent,MaxTypeSize<LeaveViewEvent,
			MaxTypeSize<JumpMapEvent, MaxTypeSize<LoseEvent, void> > > >::size];
	
		template<class E> E & Get()const
		{
			return *((E *)buf);
		}

		const bool operator==(const Event_holder &)const;
	};
}
#endif
