﻿#pragma once

#include "Action.h"
#include "MoveControl2.h"

namespace logicai
{
	enum PATROL_MODE
	{
		PATROL_SURGE = 0,
		PATROL_CIRCLE
	};

	class ActionPatrol : public Action_base
	{
	public:
		ActionPatrol(CreaturePtr &, const std::vector<position> &, int, bool loop = true);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin();

		virtual void OnFaitEnd();

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnRun(unsigned elapsed);

		virtual void OnUpdate(unsigned elapsed);

		virtual int GetType()const { return ACTION_MOVE_PARTOL; } 

		size_t GetCurPosIdx()const;

		const bool IsReachSomePos()const;

		const size_t GetWayPointsSize()const { return wayPoints_.size(); }

		void StopMoving() { move_.Stop(); }

		void HandleSpeedDown(int);

	private:
		const position & GetNextPos();

		std::vector<position> wayPoints_;
		int patrolMode_;
		size_t curPosIdx_;
		size_t isReachSomPos_;
		MoveControl2 move_;
		const bool loop_;
	}; //
} //
