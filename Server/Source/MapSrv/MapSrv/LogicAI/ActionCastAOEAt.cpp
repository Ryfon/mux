﻿#include "ActionCastAOEAt.h"
#include "Creature.h"

namespace logicai
{
	ActionCastAOEAt::ActionCastAOEAt(CreaturePtr & creature, ObjPtr & target, unsigned skillId)
		: ActionTreat(creature, target, skillId)
	{
	}

	void ActionCastAOEAt::OnEnter()
	{
		ActionTreat::OnEnter();
	}

	void ActionCastAOEAt::OnLeave()
	{
		ActionTreat::OnLeave();
	}

	void ActionCastAOEAt::OnPause()
	{
		ActionTreat::OnPause();
	}

	void ActionCastAOEAt::OnResume()
	{
		ActionTreat::OnResume();
	}

	void ActionCastAOEAt::OnStop()
	{
		ActionTreat::OnStop();
	}

	void ActionCastAOEAt::OnLostObj(ObjPtr & obj)
	{
		ActionTreat::OnLostObj(obj);
	}

	void ActionCastAOEAt::OnFaitBegin()
	{
		ActionTreat::OnFaitBegin();
	}

	void ActionCastAOEAt::OnFaitEnd()
	{
		ActionTreat::OnFaitEnd();
	}

	void ActionCastAOEAt::OnBoundBegin()
	{
		ActionTreat::OnBoundBegin();
	}

	void ActionCastAOEAt::OnBoundEnd()
	{
		ActionTreat::OnBoundEnd();
	}

	void ActionCastAOEAt::OnUpdate(unsigned elapsed)
	{
		ActionTreat::OnUpdate(elapsed);
	}

	void ActionCastAOEAt::OnRun(unsigned elapsed)
	{
		ActionTreat::OnRun(elapsed);
	}
}