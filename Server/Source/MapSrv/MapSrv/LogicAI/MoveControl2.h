﻿#pragma once

#ifndef __MUX_LOGICAI_MOVECONTROL2_H__
#define __MUX_LOGICAI_MOVECONTROL2_H__

#include "../monster/monsterwrapper.h"
#include "Action.h"

namespace logicai
{
	class Creature;

	class MoveControl2
	{
	public:
		typedef MUX_PROTO::TYPE_POS _pos;

		MoveControl2(CreaturePtr & obj, float speed, const position & pos);

		virtual ~MoveControl2();

		void SetTargetPos(const position & pos, bool notifyStop = true);

		const position & GetTargetPos()const;

		void SetSpeed(float);

		float GetSpeed()const;

		void SetSpeedf(float);

		float GetSpeedf()const;

		void SetMobility(bool);

		void Pause();

		bool IsPause()const;

		void Stop();

		bool IsStop()const;

		void Resume(bool = false);

		bool IsReach()const;

		bool IsMoving()const;

		void FaitBegin();

		void BoundBegin();

		void FaitEnd();

		void BoundEnd();

		void Update(unsigned elapsed);

		void Reset();

		void HandleSpeedDown(int);

		class State;
		friend class State;

		class StopState;
		friend class StopState;

		class PauseState;
		friend class PauseState;

		class ReachState;
		friend class ReachState;

		class BeginMoveState;
		friend class BeginMoveState;

		class MovingState;
		friend class MovingState;

	private:
		CreaturePtr obj_;
		position beginPos_;
		position targetPos_;
		float speed_;
		float xspeed_;
		float yspeed_;
		float fspeed_;
		State * state_;

		bool needResumeMove_;

	}; // class MoveControl

	class MoveControl2::State
	{
	public:
		virtual ~State() { }

		virtual void Enter(MoveControl2 &) = 0;

		virtual void Leave(MoveControl2 &) = 0;

		virtual void Update(MoveControl2 &, unsigned elapsed) = 0;

		virtual void Stop(MoveControl2 &, bool) = 0;

		virtual void Pause(MoveControl2 &) = 0;

		virtual void Resume(MoveControl2 &) = 0;
	};

	class MoveControl2::StopState : public MoveControl2::State
	{
	public:
		virtual void Enter(MoveControl2 &);

		virtual void Leave(MoveControl2 &);

		virtual void Update(MoveControl2 &, unsigned elapsed);

		virtual void Stop(MoveControl2 &, bool);

		virtual void Pause(MoveControl2 &);

		virtual void Resume(MoveControl2 &);

		static StopState * instance();
	};

	class MoveControl2::PauseState : public MoveControl2::State
	{
	public:
		virtual void Enter(MoveControl2 &);

		virtual void Leave(MoveControl2 &);

		virtual void Update(MoveControl2 &, unsigned elapsed);

		virtual void Stop(MoveControl2 &, bool);

		virtual void Pause(MoveControl2 &);

		virtual void Resume(MoveControl2 &);

		static PauseState * instance();
	};

	class MoveControl2::ReachState : public MoveControl2::State
	{
	public:
		virtual void Enter(MoveControl2 &);

		virtual void Leave(MoveControl2 &);

		virtual void Update(MoveControl2 &, unsigned elapsed);

		virtual void Stop(MoveControl2 &, bool);

		virtual void Pause(MoveControl2 &);

		virtual void Resume(MoveControl2 &);

		static ReachState * instance();

	};

	class MoveControl2::BeginMoveState  : public MoveControl2::State
	{
	public:
		virtual void Enter(MoveControl2 &);

		virtual void Leave(MoveControl2 &);

		virtual void Update(MoveControl2 &, unsigned elapsed);

		virtual void Stop(MoveControl2 &, bool);

		virtual void Pause(MoveControl2 &);

		virtual void Resume(MoveControl2 &);

		static BeginMoveState * instance();
	};

	class MoveControl2::MovingState : public MoveControl2::State
	{
		void Reach(MoveControl2 &);
	public:
		virtual void Enter(MoveControl2 &);

		virtual void Leave(MoveControl2 &);

		virtual void Update(MoveControl2 &, unsigned elapsed);

		virtual void Stop(MoveControl2 &, bool);

		virtual void Pause(MoveControl2 &);

		virtual void Resume(MoveControl2 &);

		static MovingState * instance();
	};

} // namespace logicai
#endif
