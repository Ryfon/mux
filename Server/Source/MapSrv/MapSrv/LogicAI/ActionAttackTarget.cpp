﻿#include "ActionAttackTarget.h"
#include "Creature.h"
#include "LogicAIEngine.h"

namespace logicai
{
	ActionAttackTarget::ActionAttackTarget(CreaturePtr & creature, ObjPtr & obj, unsigned attackTime)
		: ActionFollow(creature, obj)
		, attackTime_(attackTime)
		, curseTime_(0)
	{
		attackTime_ *= 1000;
	}

	void ActionAttackTarget::OnEnter()
	{
		ActionFollow::OnEnter();
	}

	void ActionAttackTarget::OnLeave()
	{
		ActionFollow::OnLeave();
	}

	void ActionAttackTarget::OnPause()
	{
		ActionFollow::OnPause();
	}

	void ActionAttackTarget::OnResume()
	{
		ActionFollow::OnResume();
	}

	void ActionAttackTarget::OnStop()
	{
		ActionFollow::OnStop();
	}

	void ActionAttackTarget::OnLostObj(ObjPtr & obj)
	{
		ActionFollow::OnLostObj(obj);
	}

	void ActionAttackTarget::OnFaitBegin()
	{
		ActionFollow::OnFaitBegin();
	}

	void ActionAttackTarget::OnFaitEnd()
	{
		ActionFollow::OnFaitEnd();
	}

	void ActionAttackTarget::OnBoundBegin()
	{
		ActionFollow::OnFaitBegin();
	}

	void ActionAttackTarget::OnBoundEnd()
	{
		ActionFollow::OnBoundEnd();
	}

	void ActionAttackTarget::OnUpdate(unsigned elapsed)
	{

	}

	void ActionAttackTarget::OnRun(unsigned elapsed)
	{
		if (attackTime_ < elapsed)
		{
			this->Stop();
			return;
		}

		attackTime_ -= elapsed;
	
		if (!this->GetFollowTarget().get())
		{
			this->Stop();
			return;
		}

		ActionFollow::OnRun(elapsed);

		ObjPtr target = this->GetFollowTarget();
		CMuxSkillProp * skill = this->creature_->GetSkill(target);
		if (!skill)
		{
			this->Stop();
			return ;
		}

		this->creature_->SetCurAttackInterval(this->creature_->GetCurAttackInterval() + elapsed);

		if (this->creature_->GetCurAttackInterval() < curseTime_ + this->creature_->GetAttackInterval())
			return;

		if (ActionFight::CheckMaxAttackDist(this->creature_, target, *skill))
		{
			int result = 0;
			result = CMonsterWrapper::UseSkill(this->creature_->GetMapInfo(), this->creature_->GetId(), this->GetFollowTarget()->GetMPPid(), skill->m_skillID);
			if (-1 != result && 0 != result)
			{
				curseTime_ = skill->m_nCurseTime;
				this->creature_->SetCurAttackInterval(0);
				this->creature_->SetSpeciSkill(0);

				if (this->creature_->IsNotifySkillCasting())
				{
					LogicAIEngine::instance().L().Table(
						this->creature_->GetIdent().c_str()).Mfunction<void (unsigned)>("_HandleSkillCasting")(skill->m_skillID);
				}
			}
		}
	}
}