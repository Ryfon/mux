﻿#pragma once

#include "Action.h"

namespace logicai
{
	class ActionCall : public Action_base
	{
	public:
		ActionCall(CreaturePtr &, unsigned, unsigned, bool);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnFaitBegin();

		virtual void OnFaitEnd();

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return ACTION_ASSIST_CALL; }

		//static void MonsterCalledCallback(Creature & creature, unsigned flag);

	private:
		unsigned action_id_;
		unsigned class_;
		unsigned count_;
		bool copy_threatlist_;
		
		static unsigned action_count_;
		std::map<unsigned, ActionCall *> actions_;
	};
}
