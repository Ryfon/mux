﻿#pragma once

#include "Action.h"
#include "MoveControl2.h"

namespace logicai
{
	class Creature;
	class GameObj;

	position GetTargetNextPos(ObjPtr & obj);

	class ActionFight : public Action_base
	{
	public:
		ActionFight(CreaturePtr &, unsigned reactTime);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin();

		virtual void OnFaitEnd() ;

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const;

		void HandleSpeedDown(int);

		ObjPtr GetAttackTarget() { return target_; }

		void Chase();

		void Escape();

		void GoHome();

		static bool CheckMaxAttackDist(CreaturePtr &, ObjPtr &, CMuxSkillProp &);

	private:
		class State;
		
		class AttackState;
		friend class AttackState;

		class ChaseState;
		friend class ChaseState;

		class EscapeState;
		friend class EscapeState;

		class GoHomeState;
		friend class GoHomeState;

		void ChangeState(State * newState);

		State * state_;
		ObjPtr target_;
		unsigned curseTime_;
		position returnPos_;
		unsigned reactTime_;
		unsigned gohomeReactTime_;

		bool needResumeMove_;

		MoveControl2 move_;
	}; //

	class ActionFight::State
	{
	public:
		virtual ~State() { }

		virtual void OnEnter(ActionFight &) = 0;

		virtual void OnLeave(ActionFight &) = 0;

		virtual void OnPause(ActionFight &) = 0;

		virtual void OnResume(ActionFight &) = 0;

		virtual void OnStop(ActionFight &) = 0;

		virtual void OnUpdate(ActionFight &, unsigned elapsed) = 0;

		virtual void OnRun(ActionFight &, unsigned elapsed) = 0;

		virtual int GetState() = 0;
	}; //

	class ActionFight::AttackState : public ActionFight::State
	{
	public:
		virtual void OnEnter(ActionFight &);

		virtual void OnLeave(ActionFight &);

		virtual void OnPause(ActionFight &);

		virtual void OnResume(ActionFight &);

		virtual void OnStop(ActionFight &);

		virtual void OnUpdate(ActionFight &, unsigned elapsed);

		virtual void OnRun(ActionFight &, unsigned elapsed);

		virtual int GetState() { return ACTION_FIGHT_ATTACK; }

		static AttackState * instance();

		static void SelectAttackTarget(ActionFight &);

	}; //

	class ActionFight::ChaseState : public ActionFight::State
	{
	public:
		virtual void OnEnter(ActionFight &);

		virtual void OnLeave(ActionFight &);

		virtual void OnPause(ActionFight &);

		virtual void OnResume(ActionFight &);

		virtual void OnStop(ActionFight &);

		virtual void OnUpdate(ActionFight &, unsigned elapsed);

		virtual void OnRun(ActionFight &, unsigned elapsed);

		virtual int GetState() { return ACTION_FIGHT_CHASE; }

		static ChaseState * instance();
	}; //

	class ActionFight::EscapeState : public ActionFight::State
	{
	public:
		virtual void OnEnter(ActionFight &);

		virtual void OnLeave(ActionFight &);

		virtual void OnPause(ActionFight &);

		virtual void OnResume(ActionFight &);

		virtual void OnStop(ActionFight &);

		virtual void OnUpdate(ActionFight &, unsigned elapsed);

		virtual void OnRun(ActionFight &, unsigned elapsed);

		virtual int GetState() { return ACTION_FIGHT_ESCAPE; }

		static EscapeState * instance();
	}; //

	class ActionFight::GoHomeState : public ActionFight::State
	{
	public:
		virtual void OnEnter(ActionFight &);

		virtual void OnLeave(ActionFight &);

		virtual void OnPause(ActionFight &);

		virtual void OnResume(ActionFight &);

		virtual void OnStop(ActionFight &);

		virtual void OnUpdate(ActionFight &, unsigned elapsed);

		virtual void OnRun(ActionFight &, unsigned elapsed);

		virtual int GetState() { return ACTION_FIGHT_GOHOME; }

		static GoHomeState * instance();
	}; //

} //
