﻿#include "ActionTreat.h"
#include "Creature.h"
#include "ActionFight.h"
#include "LogicAIEngine.h"

namespace logicai
{
	ActionTreat::ActionTreat(CreaturePtr & creature, ObjPtr & target, unsigned skill)
		: ActionFollow(creature, target), skill_(skill)
	{

	}

	void ActionTreat::OnEnter()
	{
		ActionFollow::OnEnter();
	}

	void ActionTreat::OnLeave()
	{
		ActionFollow::OnLeave();
	}

	void ActionTreat::OnPause()
	{
		ActionFollow::OnPause();
	}

	void ActionTreat::OnResume()
	{
		ActionFollow::OnResume();
	}

	void ActionTreat::OnStop()
	{
		ActionFollow::OnStop();
	}

	void ActionTreat::OnFaitBegin() 
	{
		ActionFollow::OnFaitBegin();
	}

	void ActionTreat::OnFaitEnd()  
	{
		ActionFollow::OnFaitEnd();
	}

	void ActionTreat::OnBoundBegin() 
	{
		ActionFollow::OnBoundBegin();
	}

	void ActionTreat::OnBoundEnd()
	{
		ActionFollow::OnBoundEnd();
	}

	void ActionTreat::OnLostObj(ObjPtr & obj)
	{
		ActionFollow::OnLostObj(obj);
	}

	void ActionTreat::OnUpdate(unsigned elapsed)
	{
		ActionFollow::OnUpdate(elapsed);
	}

	void ActionTreat::OnRun(unsigned elapsed)
	{
		if (!this->GetFollowTarget().get())
		{
			this->Stop();
			return;
		}

		ActionFollow::OnRun(elapsed);

		CMuxSkillProp * skill = 0;
		CMonsterWrapper::GetMonsterSkillProp(skill_, skill);
		if (!skill)
		{
			this->Stop();
			return ;
		}

		ObjPtr target = this->GetFollowTarget();
		if (ActionFight::CheckMaxAttackDist(this->creature_, target, *skill))
		{
			int result = 0;
			result = CMonsterWrapper::UseSkill(this->creature_->GetMapInfo(), this->creature_->GetId(), this->GetFollowTarget()->GetMPPid(), skill_);
			if (-1 != result && 0 != result)
			{
				if (this->creature_->IsNotifySkillCasting())
				{
					LogicAIEngine::instance().L().Table(
						this->creature_->GetIdent().c_str()).Mfunction<void (unsigned)>("_HandleSkillCasting")(skill->m_skillID);
				}

				this->Stop();
			}
		}
	}
}