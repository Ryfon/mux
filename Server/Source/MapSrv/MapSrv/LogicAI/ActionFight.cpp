﻿#include "ActionFight.h"
#include "Creature.h"
#include "LogicAIEngine.h"

#include <ace/Trace.h>

namespace logicai
{	
	ActionFight::ActionFight(CreaturePtr & creature, unsigned reactTime)
		: Action_base(creature), state_(0), target_(0)
		, curseTime_(0), move_(creature, 1.0f, creature->GetCurPos())
		, reactTime_(reactTime)
		, gohomeReactTime_(1000 * 2)
		, needResumeMove_(false)
	{
		state_ = ActionFight::AttackState::instance();
	}

	void ActionFight::OnEnter()
	{
		curseTime_ = 0;
		this->creature_->SetFightingFlag(true);

		returnPos_ = this->creature_->GetCurPos();
		this->creature_->SetCurAttackInterval(500000);

		state_->OnEnter(*this);

		bool bOK = true;
		CMonsterWrapper::IsBlockedPoint(this->creature_->GetMapInfo(), this->creature_->GetId(),
			this->creature_->GetCurPos().GetX(), this->creature_->GetCurPos().GetY(), bOK);
		if (!bOK)
		{
			state_ = ActionFight::ChaseState::instance();
			state_->OnEnter(*this);
		}

		LogicAIEngine::instance().L().Table(creature_->GetIdent().c_str()).Mfunction<void ()>("_HandleFirstEnterFight")();
	}

	void ActionFight::OnLeave()
	{
		state_->OnLeave(*this);
		this->creature_->SetFightingFlag(false);
		LogicAIEngine::instance().L().Table(creature_->GetIdent().c_str()).Mfunction<void ()>("_HandleLeaveFight")();

		move_.Reset();
		Action_base::OnLeave();
	}

	void ActionFight::OnPause()
	{
		state_->OnPause(*this);
	}

	void ActionFight::OnResume()
	{
		state_->OnResume(*this);
	}

	void ActionFight::OnStop()
	{
		state_->OnStop(*this);

		move_.Reset();
		Action_base::OnStop();
	}

	void ActionFight::OnFaitBegin() 
	{
		move_.FaitBegin();
	}

	void ActionFight::OnFaitEnd()  
	{
		move_.FaitEnd();
	}

	void ActionFight::OnBoundBegin() 
	{
		move_.BoundBegin();
	}

	void ActionFight::OnBoundEnd() 
	{ 
		move_.BoundEnd();
	}

	void ActionFight::OnLostObj(ObjPtr & obj)
	{
		if (obj == target_)
		{
			target_.reset();
		}
	}

	void ActionFight::OnUpdate(unsigned elapsed)
	{
		state_->OnUpdate(*this, elapsed);
	}

	void ActionFight::OnRun(unsigned elapsed)
	{
		if (reactTime_ > elapsed)
		{
			reactTime_ -= elapsed;
			return;
		}

		if (state_ == GoHomeState::instance())
			state_->OnRun(*this, elapsed);
		else
		{
			state_->OnRun(*this, elapsed);
		}
	}

	int ActionFight::GetType()const
	{
		return state_->GetState();
	}

	void ActionFight::HandleSpeedDown(int val)
	{
		move_.HandleSpeedDown(val);
	}

	void ActionFight::ChangeState(State * newState)
	{
		if (state_ != newState)
		{
			state_->OnLeave(*this);
			state_ = newState;
			state_->OnEnter(*this);

			if (this->creature_->IsUpdate() && this->creature_->IsCanRun(this))
				state_->OnRun(*this, 0);
		}
	}

	void ActionFight::Chase()
	{
		ChangeState(ChaseState::instance());
	}

	void ActionFight::Escape()
	{
		ChangeState(EscapeState::instance());
	}

	void ActionFight::GoHome()
	{
		ChangeState(GoHomeState::instance());
	}

	bool ActionFight::CheckMaxAttackDist(CreaturePtr & creature, ObjPtr & target, CMuxSkillProp & skill)
	{
		bool result = false;
		CMonsterWrapper::IsInAttackRange(creature->GetMapInfo(), creature->GetId(), 
			target->GetMPPid(), creature->GetSize() + skill.m_nAttDistMax + 1, result);
		return result;
	}

	ActionFight::AttackState * ActionFight::AttackState::instance()
	{
		static AttackState inst;
		return &inst;
	}

	void ActionFight::AttackState::OnEnter(ActionFight & fight)
	{
		
	}

	void ActionFight::AttackState::OnLeave(ActionFight &)
	{
		
	}

	void ActionFight::AttackState::OnPause(ActionFight &)
	{

	}

	void ActionFight::AttackState::OnResume(ActionFight &)
	{

	}

	void ActionFight::AttackState::OnStop(ActionFight &)
	{

	}

	void ActionFight::AttackState::SelectAttackTarget(ActionFight & fight)
	{
		fight.target_ = fight.creature_->GetThreatList().GetMaxThreat();
	}

	void ActionFight::AttackState::OnUpdate(ActionFight & fight, unsigned elapsed)
	{
		fight.creature_->SetCurAttackInterval(fight.creature_->GetCurAttackInterval() + elapsed);
		if (fight.curseTime_ > elapsed)
			fight.curseTime_ -= elapsed;
		else
			fight.curseTime_ = 0;
	}

	void ActionFight::AttackState::OnRun(ActionFight & fight, unsigned elapsed)
	{
		unsigned curAttackInterval = fight.creature_->GetCurAttackInterval() + elapsed;
		fight.creature_->SetCurAttackInterval(curAttackInterval);

		if (fight.creature_->IsFait())
			return;

		if (fight.creature_->ThreatListEmpty())
		{
			fight.ChangeState(ActionFight::GoHomeState::instance());
			return;
		}
	
		SelectAttackTarget(fight);
		if (!fight.target_.get())
		{
			fight.ChangeState(ActionFight::GoHomeState::instance());
			return;
		}

		if (fight.curseTime_ > elapsed)
		{
			fight.curseTime_ -= elapsed;
			return;
		}
		else if (fight.curseTime_ >= elapsed / 2)
		{
			fight.curseTime_ = 0;
			return;
		}
		
		CMuxSkillProp * skill = fight.creature_->GetSkill(fight.target_);
		if (!skill)
		{
			D_ERROR("failed to get skill for monster[%u type:%u]\n", fight.creature_->GetId(), fight.creature_->GetClassId());
			fight.ChangeState(ActionFight::GoHomeState::instance());
			return ;
		}

		if (!ActionFight::CheckMaxAttackDist(fight.creature_, fight.target_, *skill))
		{
			if (fight.creature_->GetChaseAbility())
				fight.ChangeState(ActionFight::ChaseState::instance());
			return ;
		}

		unsigned needAttackInterval = fight.curseTime_ + fight.creature_->GetAttackInterval();
		if (curAttackInterval < needAttackInterval)
		{
			return;
		}

		ObjPtr target = fight.target_;
		int result = CMonsterWrapper::UseSkill(fight.creature_->GetMapInfo(), fight.creature_->GetId(), 
			fight.target_->GetMPPid(), skill->m_skillID);
		if (-1 != result && 0 != result)
		{
			//D_DEBUG("%s[%d] is attacking %s\n", fight.creature_->GetName().c_str(), fight.creature_->GetId(), target->GetName().c_str());
			fight.curseTime_ = skill->m_nCurseTime;
			fight.creature_->SetCurAttackInterval(0);
			fight.creature_->SetSpeciSkill(0);

			if (fight.creature_->IsNotifySkillCasting())
			{
				LogicAIEngine::instance().L().Table(
					fight.creature_->GetIdent().c_str()).Mfunction<void (unsigned)>("_HandleSkillCasting")(skill->m_skillID);
			}
		}
	}

	ActionFight::ChaseState * ActionFight::ChaseState::instance()
	{
		static ChaseState inst;
		return &inst;
	}

	position GetTargetNextPos(ObjPtr & obj)
	{
		position pos = obj->GetCurPos();
		if (obj->IsPlayer())
		{
			position tmp;
			if (0 == CMonsterWrapper::GetPlayerTargetPos(obj->GetMPPid(), tmp.x, tmp.y))
			{
				if (tmp != pos)
				{
					float dx = tmp.x - pos.x;
					float dy = tmp.y - pos.y;
					if (dx >= 1.0f)
						pos.x += 2.0f;
					else if (dx <= -1.0f)
						pos.x -= 2.0f;
					if (dy > 1.0f)
						pos.y += 2.0f;
					else if (dy < -1.0f)
						pos.y -= 2.0f;
				}
			}
		}
		return pos;
	}

	void ActionFight::ChaseState::OnEnter(ActionFight & fight)
	{
		if (fight.target_.get())
		{
			fight.move_.SetSpeed(fight.creature_->GetChaseSpeed());
			fight.move_.SetTargetPos(GetTargetNextPos(fight.target_));
			CMuxSkillProp * skill = fight.creature_->GetSkill(fight.target_);
			if (skill && !CheckMaxAttackDist(fight.creature_, fight.target_, *skill) &&
				!fight.creature_->IsFait() && !fight.creature_->IsBound())
			{
				fight.move_.Resume();
				return;
			}		

			bool isOK = true;
			CMonsterWrapper::IsBlockedPoint(fight.creature_->GetMapInfo(), fight.creature_->GetId(), 
				fight.creature_->GetCurPos().GetX(), fight.creature_->GetCurPos().GetY(), isOK);
			if (!isOK)
			{
				fight.move_.Resume();
				return;
			}
		}
	}

	void ActionFight::ChaseState::OnLeave(ActionFight & fight)
	{
		fight.move_.Stop();
		
	}

	void ActionFight::ChaseState::OnPause(ActionFight & fight)
	{
		fight.move_.Pause();
	}

	void ActionFight::ChaseState::OnResume(ActionFight & fight)
	{
		fight.move_.Resume();
	}

	void ActionFight::ChaseState::OnStop(ActionFight & fight)
	{
		fight.move_.Stop();
	}

	void ActionFight::ChaseState::OnUpdate(ActionFight & fight, unsigned elapsed)
	{
		fight.creature_->SetCurAttackInterval(fight.creature_->GetCurAttackInterval() + elapsed);
		if (fight.curseTime_ > elapsed)
			fight.curseTime_ -= elapsed;
		else
			fight.curseTime_ = 0;
	}

	void ActionFight::ChaseState::OnRun(ActionFight & fight, unsigned elapsed)
	{
		fight.creature_->SetCurAttackInterval(fight.creature_->GetCurAttackInterval() + elapsed);
		if (fight.curseTime_ > elapsed)
			fight.curseTime_ -= elapsed;
		else
			fight.curseTime_ = 0;

		if (fight.creature_->IsFait() || fight.creature_->IsBound())
			return;		

		float dx = fight.creature_->GetCurPos().x - fight.returnPos_.x;
		float dy = fight.creature_->GetCurPos().y - fight.returnPos_.y;
		if (dx * dx + dy * dy > fight.creature_->GetChaseDist() * fight.creature_->GetChaseDist())
		{
			fight.ChangeState(ActionFight::GoHomeState::instance());
			return;
		}

		if (!fight.target_.get())
		{
			fight.ChangeState(ActionFight::AttackState::instance());
			return;
		}

		CMuxSkillProp * skill = fight.creature_->GetSkill(fight.target_);
		if (!skill)
		{
			fight.ChangeState(ActionFight::GoHomeState::instance());
			return ;
		}

		fight.move_.Update(elapsed);
		
		if (ActionFight::CheckMaxAttackDist(fight.creature_, fight.target_, *skill))
		{
			bool isOK = true;
			CMonsterWrapper::IsBlockedPoint(fight.creature_->GetMapInfo(), fight.creature_->GetId(), 
				fight.creature_->GetCurPos().GetX(), fight.creature_->GetCurPos().GetY(), isOK);
			if (isOK)
			{
				fight.ChangeState(ActionFight::AttackState::instance());
				return;
			}
		}

		//position enemyPos = fight.target_->GetCurPos();
		position enemyPos = GetTargetNextPos(fight.target_);

		/*
		if (fight.move_.IsReach())
		{
			D_WARNING("cannot attack target when reaching\n");
		}*/

		//D_DEBUG("chase distance : dx=%f dy=%f\n", abs(enemyPos.x - );

#ifdef WIN32
		if (fight.move_.IsReach() || (abs(enemyPos.GetX() - fight.move_.GetTargetPos().GetX()) >= 1.5f ||
			abs(enemyPos.GetY() - fight.move_.GetTargetPos().GetY()) >= 1.5f))
#else
		if (fight.move_.IsReach() || (fabs(enemyPos.GetX() - fight.move_.GetTargetPos().GetX()) >= 1.5f ||
			fabs(enemyPos.GetY() - fight.move_.GetTargetPos().GetY()) >= 1.5f))
#endif
		{
			//fight.move_.SetTargetPos(GetTargetNextPos(fight.target_), false);
			fight.move_.SetTargetPos(enemyPos, false);
			fight.move_.Resume(true);
		}
	}

	ActionFight::EscapeState * ActionFight::EscapeState::instance()
	{
		static EscapeState inst;
		return &inst;
	}

	void ActionFight::EscapeState::OnEnter(ActionFight & fight)
	{
		AI_ASSERT(fight.target_.get());

		position enemyPos = fight.target_->GetCurPos(), pos;
		float dx = enemyPos.x - fight.creature_->GetCurPos().x;
		float dy = enemyPos.y - fight.creature_->GetCurPos().y;

		if (dx > 0)
		{
			dx = fight.creature_->GetCurPos().x - rand() % 6 - 21;

			if (dx <= 0)
				dx = enemyPos.x + rand() % 6 + 21;
		}
		else
		{
			dx = enemyPos.x + rand() % 6 + 21;

			if (dx >= 512)
				dx = fight.creature_->GetCurPos().x - rand() % 6 - 21;
		}

		if (dy > 0)
		{
			dy = fight.creature_->GetCurPos().y - rand() % 6 - 21;

			if (dy <= 0)
				dy = enemyPos.y + rand() % 6 + 21;
		}
		else
		{
			dy = enemyPos.y + rand() % 6 + 21;

			if (dy >= 512)
				dy = fight.creature_->GetCurPos().y - rand() % 6 - 21;
		}

		fight.move_.SetSpeed(fight.creature_->GetEscapeSpeed());
		fight.move_.SetTargetPos(pos);
		fight.move_.Resume();
	}

	void ActionFight::EscapeState::OnLeave(ActionFight & fight)
	{
		fight.move_.Stop();
	}

	void ActionFight::EscapeState::OnPause(ActionFight & fight)
	{
		fight.move_.Pause();
	}

	void ActionFight::EscapeState::OnResume(ActionFight & fight)
	{
		fight.move_.Resume();
	}

	void ActionFight::EscapeState::OnStop(ActionFight & fight)
	{
		fight.move_.Stop();
	}

	void ActionFight::EscapeState::OnUpdate(ActionFight & fight, unsigned elapsed)
	{
		fight.creature_->SetCurAttackInterval(fight.creature_->GetCurAttackInterval() + elapsed);
		if (fight.curseTime_ > elapsed)
			fight.curseTime_ -= elapsed;
		else
			fight.curseTime_ = 0;
	}

	void ActionFight::EscapeState::OnRun(ActionFight & fight, unsigned elapsed)
	{
		fight.creature_->SetCurAttackInterval(fight.creature_->GetCurAttackInterval() + elapsed);
		if (fight.curseTime_ > elapsed)
			fight.curseTime_ -= elapsed;
		else
			fight.curseTime_ = 0;

		if (fight.creature_->IsBound() || fight.creature_->IsFait())
			return;

		if (!fight.target_.get())
		{
			fight.ChangeState(ActionFight::AttackState::instance());
			return;
		}

		if (fight.move_.IsReach())
		{
			fight.state_ = ActionFight::AttackState::instance();
			return;
		}

		fight.move_.Update(elapsed);
	}

	ActionFight::GoHomeState * ActionFight::GoHomeState::instance()
	{
		static GoHomeState inst;
		return &inst;
	}

	void ActionFight::GoHomeState::OnEnter(ActionFight & fight)
	{
		fight.creature_->ClearAllBuffs(false);
		fight.creature_->ClearAllBuffs(true);

		fight.creature_->GetThreatList().ResetMaxSize(10);

		fight.creature_->ResetFait();
		fight.creature_->ResetBound();

		fight.move_.SetSpeed(fight.creature_->GetGoHomeSpeed());
		fight.move_.SetTargetPos(fight.returnPos_);

		fight.target_ = 0;

		CMonsterWrapper::ReturnStarted(fight.creature_->GetMapInfo(), fight.creature_->GetId());
	}

	void ActionFight::GoHomeState::OnLeave(ActionFight & fight)
	{
		CMonsterWrapper::ReturnEnded(fight.creature_->GetMapInfo(), fight.creature_->GetId());

		fight.creature_->SetHpPercent(100.0f);

		fight.creature_->ClearThreatList();
	}

	void ActionFight::GoHomeState::OnPause(ActionFight & fight)
	{
		fight.move_.Pause();
	}

	void ActionFight::GoHomeState::OnResume(ActionFight & fight)
	{
		fight.move_.Resume();
	}

	void ActionFight::GoHomeState::OnStop(ActionFight & fight)
	{
		fight.move_.Stop();
	}

	void ActionFight::GoHomeState::OnUpdate(ActionFight &, unsigned elapsed)
	{

	}

	void ActionFight::GoHomeState::OnRun(ActionFight & fight, unsigned elapsed)
	{
		if (fight.gohomeReactTime_ > elapsed)
		{
			fight.gohomeReactTime_ -= elapsed;
			return;
		}

		if (fight.creature_->IsFait() || fight.creature_->IsBound())
			return;

		if (fight.move_.IsReach())
		{
			fight.Stop();
			return;
		}

		fight.move_.Resume();
		fight.move_.Update(elapsed);
	}

} //