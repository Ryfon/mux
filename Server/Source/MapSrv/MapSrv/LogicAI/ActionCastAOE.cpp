﻿#include "ActionCastAOE.h"
#include "Creature.h"

namespace logicai
{
	ActionCastAOE::ActionCastAOE(CreaturePtr &creature, unsigned int skillId)
		 : Action_base(creature), skillId_(skillId) 
	{ }

	void ActionCastAOE::OnRun(unsigned int elapsed)
	{
		// TOTO;
	}

	void ActionCastAOE::OnLeave()
	{
		Action_base::OnLeave();
	}

	void ActionCastAOE::OnStop()
	{
		Action_base::OnStop();
	}
}