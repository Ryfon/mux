﻿#pragma once

#include "Action.h"
#include "ActionTask.h"
#include "ActionPatrol.h"
#include "Holder.h"

namespace logicai
{
	class ActionFollowedTask : public Action_base
	{
	public:
		ActionFollowedTask(CreaturePtr &, ObjPtr &, const std::vector<position> &, unsigned, unsigned);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin() { }

		virtual void OnFaitEnd()  { }

		virtual void OnBoundBegin() { }

		virtual void OnBoundEnd() { }

		virtual void OnLostObj(ObjPtr &);

		virtual void OnRun(unsigned elapsed);

		virtual void OnUpdate(unsigned elapsed);

		virtual int GetType()const { return ACTION_MOVE_FOLLOWED_TASK; }

		ActionTask & GetTask() { return task_; }

		void OnTargetLeaveMap(ObjPtr &);

		void HandleSpeedDown(int);

	private:
		ActionTask task_;
		ActionPatrol patrol_;

	};
} //
