﻿#ifndef __MUX_SKILL_LIST_H__
#define __MUX_SKILL_LIST_H__

#pragma once

#include <vector>
#include <algorithm>
#include <cmath>

#include "../NpcSkill.h"
#include "../AISys/MonsterSkillSet.h"

namespace logicai
{
	enum SKILL_SELECT_MODE
	{
		SELECT_SERI = 0,
		SELECT_RANDOM,
		SELECT_SPECI,
		SELECT_SMART
	};

	struct SkillData
	{
		SkillData()
		{
			skill = NULL;
			accUseTime = 0;
			weight = 0;
		}

		CMuxSkillProp * skill;
		unsigned accUseTime;
		int weight;

		const bool operator<(const SkillData & other)const
		{
			if (skill && other.skill)
			{
				return skill->m_nAttDistMax < other.skill->m_nAttDistMax;
			}
			return false;
		}
	};

	class SkillList
	{
	private:
		std::vector<SkillData> skills_;
		unsigned curSkillIndex_;

		struct test_skill_id
		{
			unsigned skillId;

			test_skill_id(unsigned _id) : skillId(_id) { }

			const bool operator()(const SkillData & data)const
			{
				if (!data.skill)
					return false;
				return data.skill->m_skillID == skillId;
			}

		}; // struct test_skill_id

	public:
		SkillList() : curSkillIndex_((unsigned)-1)
		{ }

		~SkillList()
		{ }

		void AddSkill(unsigned skillId, int weight)
		{
			CMuxSkillProp * skill = 0;
			CMonsterWrapper::GetMonsterSkillProp(skillId, skill);
			if (!skill)
			{
				return;
			}

			for(std::vector<SkillData>::iterator it = skills_.begin(); it != skills_.end(); ++it)
			{
				if ( NULL == it->skill )
				{
					D_ERROR( "SkillList::AddSkill, NULL == it->skill\n" );
					return;
				}

				if (it->skill->m_skillID == skillId)
					return;
			}

			SkillData skillData;
			skillData.skill = skill;
			skillData.accUseTime = 0;
			skillData.weight = weight;
			skills_.push_back(skillData);
			std::sort(skills_.begin(), skills_.end());
		}

		void RemoveSkill(unsigned skillId)
		{
			unsigned i = 0;
			for(std::vector<SkillData>::iterator it = skills_.begin(); it != skills_.end(); ++it, ++i)
			{
				if ( NULL == it->skill )
				{
					D_ERROR( "SkillList::RemoveSkill, NULL == it->skill\n" );
					return;
				}

				if (it->skill->m_skillID == skillId)
				{
					skills_.erase(it);
					if (i == curSkillIndex_)
						curSkillIndex_ = (unsigned)-1;
					break;
				}
			}
		}

		void SeriSelectSkill()
		{
			if (skills_.size() == 0)
				curSkillIndex_ = (unsigned)-1;
			else if (curSkillIndex_ == (unsigned)-1)
				curSkillIndex_ = 0;
			else if (curSkillIndex_ == skills_.size() - 1)
				curSkillIndex_ = 0;
			else
				++curSkillIndex_;
		}

		void RandomSelectSkill()
		{
			if (skills_.size() == 0)
				curSkillIndex_ = (unsigned)-1;
			else if (skills_.size() == 1)
				curSkillIndex_ = 0;
			else
				curSkillIndex_ = rand() % ((unsigned)skills_.size());
		}

		void SmartSelectSkill(const float dist2)
		{
			curSkillIndex_ = -1;

			int i = 0;
			for(std::vector<SkillData>::const_iterator it = skills_.begin(); it != skills_.end(); ++it, ++i)
			{
				if ( NULL == it->skill )
				{
					D_ERROR( "SkillList::SmartSelectSkill, NULL == it->skill\n" );
					return;
				}
				if ((it->skill->m_nAttDistMax) * (it->skill->m_nAttDistMax) >= dist2)
				{
					curSkillIndex_ = i;
					return;
				}
			}

			if (skills_.size())
				curSkillIndex_ = (unsigned int)(skills_.size() - 1);
		}

		void SpeciSelectSkill(unsigned id)
		{
			if (skills_.size() == 0)
			{
				curSkillIndex_ = (unsigned)-1;
				return;
			} 
			if ( skills_.size() == 1 )
			{
				if ( NULL == skills_[0].skill )
				{
					D_ERROR( "SpeciSelectSkill, NULL == skills_[0].skill, id:%d", id );
					return;
				}
				if ( id == skills_[0].skill->m_skillID )
				{
					curSkillIndex_ = 0;
					return;
				}
				curSkillIndex_ = (unsigned int)-1;
				return;
			} 

			for(unsigned i = 0; i < skills_.size(); ++i)
			{
				if ( NULL == skills_[i].skill )
				{
					D_ERROR( "SpeciSelectSkill, NULL == skills_[i].skill, id:%d, i:%d", id, i );
					return;
				}
				if (skills_[i].skill->m_skillID == id)
				{
					curSkillIndex_ = i;
					return;
				}
			}
			curSkillIndex_ = (unsigned)-1;
		}

		SkillData * GetCurrentSkill()
		{
			if (curSkillIndex_ == (unsigned)-1)
				return 0;

			if (skills_.size() > curSkillIndex_)
				return &skills_[curSkillIndex_];
			else
				curSkillIndex_ = (unsigned)-1;

			return 0;
		}

		void AddSkillCoolTime(unsigned elapsedTime)
		{
			for(std::vector<SkillData>::iterator it = skills_.begin(); it != skills_.end(); ++it)
			{
				if (it->accUseTime)
					it->accUseTime += elapsedTime;
			}
		}

	}; // SkillList

} // namespace logicai

#endif

