﻿#pragma once

#include "Action.h"
#include "ActionTreat.h"

namespace logicai
{
	class ActionCastAOEAt : public ActionTreat
	{
	public:
		ActionCastAOEAt(CreaturePtr &, ObjPtr &, unsigned);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnFaitBegin();

		virtual void OnFaitEnd();

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return 0;}

	private:
		unsigned skillId_;
	};
}
