﻿#pragma once

#ifndef __MUX_LOGICAI_WAYPOINTLIST_H__
#define __MUX_LOGICAI_WAYPOINTLIST_H__

#include "GameObj.h"

namespace logicai
{
	class WayPointList
	{
	public:
		WayPointList(bool CIRCLE = true)
			: CIRCLE_(CIRCLE)
			, curIdx_(-1)
		{
		}

		const position * GetNextWayPoint()
		{
			if (lst_.empty())
				return 0;

			if (curIdx_ == -1)
				curIdx_ = 0;
			else
			{
				if (!CIRCLE_ && curIdx_ == lst_.size() - 1)
				{
					curIdx_ = 0;
					std::reverse(lst_.begin(), lst_.end());
				}

				if (curIdx_ >= lst_.size() - 1)
					curIdx_ = 0;
				else
					++curIdx_;
			}

			if ( curIdx_ >= lst_.size() )
			{
				D_ERROR( "GetNextWayPoint, curIdx_(%d) >= lst_.size()", curIdx );
				return NULL;
			}

			return &lst_[curIdx_];
		}

		void AddWayPoint(const position & pos)
		{
			lst_.push_back(pos);
		}
	
	private:
		std::vector<position> lst_;
		size_t curIdx_;
		bool CIRCLE_;
	};
}
#endif
