﻿#pragma once

#include "Ldef.h"
#include "Ltable.h"

__BEGIN_SCRIPTS_L_NS__

class Lfunction_base : public L_base
{
protected:
	Lfunction_base(lua_State & l, const char * fname)
		: L_(&l)
	{
		lua_getglobal(L_, fname);
		if (!lua_isfunction(L_, -1))
		{
			int ltype = lua_type(L_, -1);
			lua_pop(L_, 1);
			throw std::runtime_error(std::string("expect cfunction[") +
				std::string(fname) + std::string("] value(a ") + 
				std::string(lua_typename(L_, ltype)) +
				std::string(" value)"));
		}
	}

	Lfunction_base(lua_State & l, Ltable & , const char * fname)
		: L_(&l)
	{
		lua_getfield(L_, -1, fname);
		if (!lua_isfunction(L_, -1))
		{
			int ltype = lua_type(L_, -1);
			lua_pop(L_, 1);
			throw std::runtime_error(std::string("expect cfunction[") +
				std::string(fname) + std::string("] value(a ") + 
				std::string(lua_typename(L_, ltype)) +
				std::string(" value)"));
		}
	}

	Lfunction_base(const Lfunction_base & f)
	{
		lua_pushvalue(f.L_, -1);
	}

	~Lfunction_base()
	{
		lua_pop(L_, 1);
	}

	void call(int nargs/*, int nresults*/)const
	{
		if (0 != lua_pcall(L_, nargs, LUA_MULTRET, 0/*TODO*/))
		{
			std::string desc(lua_tostring(L_, -1));
			lua_pop(L_, 1);
			throw std::runtime_error(desc);
		}
	}

	int L_push_function_table(lua_State * /*pL*/, bool M)
	{
		lua_pushvalue(L_, -1);
		int nargs = 0;
		if (M)
		{
			lua_pushvalue(L_, -3);
			nargs += 1;
		}
		return nargs;
	}

	lua_State * L_;

private:

	const Lfunction_base & operator=(const Lfunction_base &);

};

template<class R, bool M>
class Lfunction<R (), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()()
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, class A1, bool M>
class Lfunction<R (A1), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, class A1, class A2, bool M>
class Lfunction<R (A1, A2), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1, A2 a2)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		nargs += L_pushvalue(*L_, a2);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, class A1, class A2, class A3, bool M>
class Lfunction<R (A1, A2, A3), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1, A2 a2, A3 a3)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		nargs += L_pushvalue(*L_, a2);
		nargs += L_pushvalue(*L_, a3);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, class A1, class A2, class A3, class A4, bool M>
class Lfunction<R (A1, A2, A3, A4), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1, A2 a2, A3 a3, A4 a4)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		nargs += L_pushvalue(*L_, a2);
		nargs += L_pushvalue(*L_, a3);
		nargs += L_pushvalue(*L_, a4);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, class A1, class A2, class A3, class A4, class A5, bool M>
class Lfunction<R (A1, A2, A3, A4, A5), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		nargs += L_pushvalue(*L_, a2);
		nargs += L_pushvalue(*L_, a3);
		nargs += L_pushvalue(*L_, a4);
		nargs += L_pushvalue(*L_, a5);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, class A1, class A2, class A3, class A4, class A5, class A6, bool M>
class Lfunction<R (A1, A2, A3, A4, A5, A6), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		nargs += L_pushvalue(*L_, a2);
		nargs += L_pushvalue(*L_, a3);
		nargs += L_pushvalue(*L_, a4);
		nargs += L_pushvalue(*L_, a5);
		nargs += L_pushvalue(*L_, a6);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, class A1, class A2, class A3, class A4, class A5, class A6, class A7, bool M>
class Lfunction<R (A1, A2, A3, A4, A5, A6, A7), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		nargs += L_pushvalue(*L_, a2);
		nargs += L_pushvalue(*L_, a3);
		nargs += L_pushvalue(*L_, a4);
		nargs += L_pushvalue(*L_, a5);
		nargs += L_pushvalue(*L_, a6);
		nargs += L_pushvalue(*L_, a7);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8, bool M>
class Lfunction<R (A1, A2, A3, A4, A5, A6, A7, A8), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		nargs += L_pushvalue(*L_, a2);
		nargs += L_pushvalue(*L_, a3);
		nargs += L_pushvalue(*L_, a4);
		nargs += L_pushvalue(*L_, a5);
		nargs += L_pushvalue(*L_, a6);
		nargs += L_pushvalue(*L_, a7);
		nargs += L_pushvalue(*L_, a8);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, 
class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8, class A9, bool M>
class Lfunction<R (A1, A2, A3, A4, A5, A6, A7, A8, A9), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, A9 a9)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		nargs += L_pushvalue(*L_, a2);
		nargs += L_pushvalue(*L_, a3);
		nargs += L_pushvalue(*L_, a4);
		nargs += L_pushvalue(*L_, a5);
		nargs += L_pushvalue(*L_, a6);
		nargs += L_pushvalue(*L_, a7);
		nargs += L_pushvalue(*L_, a8);
		nargs += L_pushvalue(*L_, a9);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};

template<class R, 
class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8, class A9, class A10, bool M>
class Lfunction<R (A1, A2, A3, A4, A5, A6, A7, A8, A9, A10), M> : public Lfunction_base
{
public:
	Lfunction(lua_State & l, const char * fname) : Lfunction_base(l, fname) { }

	Lfunction(lua_State & l, Ltable & t, const char * fname) : Lfunction_base(l, t, fname) { }

	R operator()(A1 a1, A2 a2, A3 a3, A4 a4, A5 a5, A6 a6, A7 a7, A8 a8, A9 a9, A10 a10)
	{
		int nargs = Lfunction_base::L_push_function_table(L_, M);
		nargs += L_pushvalue(*L_, a1);
		nargs += L_pushvalue(*L_, a2);
		nargs += L_pushvalue(*L_, a3);
		nargs += L_pushvalue(*L_, a4);
		nargs += L_pushvalue(*L_, a5);
		nargs += L_pushvalue(*L_, a6);
		nargs += L_pushvalue(*L_, a7);
		nargs += L_pushvalue(*L_, a8);
		nargs += L_pushvalue(*L_, a9);
		nargs += L_pushvalue(*L_, a10);
		Lfunction_base::call(nargs);
		return L_totopvalue_pop<R>(*L_);
	}
};
	
__END_SCRIPTS_L_NS__

