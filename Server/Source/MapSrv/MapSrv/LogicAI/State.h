﻿#pragma once

namespace logicai
{
	enum 
	{
		STATE_TYPE_MOVE = 0,
		STATE_TYPE_ATTACK,
		STATE_TYPE_ASSIST,
		STATE_TYPE_TASK,
		STATE_TYPE_ALERT
	};

	class State
	{
	public:
		State() { }

		virtual ~State() { }

		virtual void Update(unsigned elasped) = 0;

		virtual int GetState()const = 0;
	};

} // namespace logicai