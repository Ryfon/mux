﻿#pragma once

#include "../Fsm/IFsm.h"
#include "../monster/monsterwrapper.h"
#include "Position.h"
#include "GameObj.h"

namespace logicai
{
	class Creature;
	class GameObj;

	enum{
		ACTION_MOVE_NOOP = 0,
		ACTION_MOVE_IDLE,
		ACTION_MOVE_WANDER,
		ACTION_MOVE_LINETO,
		ACTION_MOVE_PARTOL,
		ACTION_MOVE_FOLLOWED,
		ACTION_MOVE_FOLLOWING,
		ACTION_MOVE_FOLLOWED_TASK,
		ACTION_MOVE_FOLLOWING_TASK,


		ACTION_FIGHT_NOOP,
		ACTION_FIGHT_ATTACK,
		ACTION_FIGHT_CHASE,
		ACTION_FIGHT_ESCAPE,
		ACTION_FIGHT_GOHOME,

		ACTION_ASSIST_NOOP,
		ACTION_ASSIST_CALL,
		ACTION_ASSIST_HELP,
		ACTION_ASSIST_TREAT,
		ACTION_ASSIST_SUMMON,

		ACTION_END
	};

	class Action_base
	{
	public:
		Action_base(CreaturePtr & obj);

		virtual ~Action_base();

		virtual void OnEnter() = 0;

		virtual void OnLeave() = 0;

		virtual void OnPause() = 0;

		virtual void OnResume() = 0;

		virtual void OnStop() = 0;

		virtual void OnLostObj(ObjPtr &) = 0;

		virtual void OnFaitBegin() = 0;

		virtual void OnFaitEnd() = 0;

		virtual void OnBoundBegin() = 0;

		virtual void OnBoundEnd() = 0;

		virtual void OnUpdate(unsigned elapsed) = 0;

		virtual void OnRun(unsigned elapsed) = 0;

		virtual int GetType()const = 0;

		virtual void HandleSpeedDown(int);

		void Run(unsigned elapsed);

		void Pause();

		const bool IsPause()const;

		void Resume();

		void Stop();

		const bool IsEnd()const;

	protected:
		enum { ACTION_STATE_PAUSE, ACTION_STATE_STOP, ACTION_STATE_RUN };
		CreaturePtr creature_;
		int state_;

	}; // class Action_base

} // namespace logicai
