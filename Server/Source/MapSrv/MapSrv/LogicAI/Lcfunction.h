﻿#pragma once

#include "Ldef.h"

__BEGIN_SCRIPTS_L_NS__

template<void (*f)()>
class Lcfunction<void (*)(), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		CF_LUA_TRY
		f();
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, R (*f)()>
class Lcfunction<R (*)(), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		CF_LUA_TRY
		R result = f();
		return L_pushvalue(pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class A1, void (*f)(A1)>
class Lcfunction<void (*)(A1), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 1)
			luaL_error(pL, "invalid arguments count for L_cfunction1");
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		CF_LUA_TRY
		f(arg1);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
};

template<class R, class A1, R (*f)(A1)>
class Lcfunction<R (*)(A1), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		CF_LUA_TRY
		R result = f(arg1);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, class A1, class A2, R (*f)(A1, A2)>
class Lcfunction<R (*)(A1, A2), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 2)
			luaL_error(pL, "invalid arguments count for L_cfunction2");
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		CF_LUA_TRY
		R result = f(arg1, arg2);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, class A1, class A2, class A3, R (*f)(A1, A2, A3)>
class Lcfunction<R (*)(A1, A2, A3), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 3)
			luaL_error(pL, "invalid arguments count for L_cfunction3");
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		CF_LUA_TRY
		R result = f(arg1, arg2, arg3);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, class A1, class A2, class A3, class A4, R (*f)(A1, A2, A3, A4)>
class Lcfunction<R (*)(A1, A2, A3, A4), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 4)
			luaL_error(pL, "invalid arguments count for L_cfunction4");
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		CF_LUA_TRY
		R result = f(arg1, arg2, arg3, arg4);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, class A1, class A2, class A3, class A4, class A5, R (*f)(A1, A2, A3, A4, A5)>
class Lcfunction<R (*)(A1, A2, A3, A4, A5), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 5)
			luaL_error(pL, "invalid arguments count for L_cfunction5");
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		CF_LUA_TRY
		R result = f(arg1, arg2, arg3, arg4, arg5);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, class A1, class A2, class A3, class A4, class A5, class A6, R (*f)(A1, A2, A3, A4, A5, A6)>
class Lcfunction<R (*)(A1, A2, A3, A4, A5, A6), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 6)
			luaL_error(pL, "invalid arguments count for L_cfunction6");
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		CF_LUA_TRY
		R result = f(arg1, arg2, arg3, arg4, arg5, arg6);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, class A1, class A2, class A3, class A4, class A5, class A6, class A7,
	R (*f)(A1, A2, A3, A4, A5, A6, A7)>
class Lcfunction<R (*)(A1, A2, A3, A4, A5, A6, A7), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 7)
			luaL_error(pL, "invalid arguments count for L_cfunction7");
		A7 arg7 = L_totopvalue_pop<A7>(*pL);
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		CF_LUA_TRY
		R result = f(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8,
	R (*f)(A1, A2, A3, A4, A5, A6, A7, A8)>
class Lcfunction<R (*)(A1, A2, A3, A4, A5, A6, A7, A8), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 8)
			luaL_error(pL, "invalid arguments count for L_cfunction8");
		A8 arg8 = L_totopvalue_pop<A8>(*pL);
		A7 arg7 = L_totopvalue_pop<A7>(*pL);
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		CF_LUA_TRY
		R result = f(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8, class A9,
	R (*f)(A1, A2, A3, A4, A5, A6, A7, A8, A9)>
class Lcfunction<R (*)(A1, A2, A3, A4, A5, A6, A7, A8, A9), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 9)
			luaL_error(pL, "invalid arguments count for L_cfunction9");
		A9 arg9 = L_totopvalue_pop<A9>(*pL);
		A8 arg8 = L_totopvalue_pop<A8>(*pL);
		A7 arg7 = L_totopvalue_pop<A7>(*pL);
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		Ltable t(pL);
		CF_LUA_TRY
		R result = f(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

template<class R, 
class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8, class A9, class A10,
	R (*f)(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10)>
class Lcfunction<R (*)(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10), f>
{
private:
	static int L_cfunction(lua_State * pL)
	{
		int n = lua_gettop(pL);
		if (n != 10)
			luaL_error(pL, "invalid arguments count for L_cfunction10");
		A10 arg10 = L_totopvalue_pop<A10>(*pL);
		A9 arg9 = L_totopvalue_pop<A9>(*pL);
		A8 arg8 = L_totopvalue_pop<A8>(*pL);
		A7 arg7 = L_totopvalue_pop<A7>(*pL);
		A6 arg6 = L_totopvalue_pop<A6>(*pL);
		A5 arg5 = L_totopvalue_pop<A5>(*pL);
		A4 arg4 = L_totopvalue_pop<A4>(*pL);
		A3 arg3 = L_totopvalue_pop<A3>(*pL);
		A2 arg2 = L_totopvalue_pop<A2>(*pL);
		A1 arg1 = L_totopvalue_pop<A1>(*pL);
		Ltable t(pL);
		CF_LUA_TRY
		R result = f(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
		return L_pushvalue(*pL, result);
		CF_LUA_CATCH
		return 0;
	}

	friend class L;
	friend class Ltable;
};

__END_SCRIPTS_L_NS__
