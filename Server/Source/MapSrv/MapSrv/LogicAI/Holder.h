﻿#pragma once

#ifndef __MUX_LOGICAI_HOLDER_H__
#define __MUX_LOGICAI_HOLDER_H__

namespace logicai
{
	template<class T>
	struct Holder
	{
		char data[sizeof(T)];

		T & Get() 
		{
			return *((T *)data);
		}
	};
}
#endif
