﻿#pragma once

#ifndef __MUX_LOGICAI_GAMEOBJ_H__
#define __MUX_LOGICAI_GAMEOBJ_H__

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <functional>

#include "../Fsm/IFsm.h"
#include "../monster/monsterwrapper.h"
#include "../Lua/include/lua.hpp"
#include "L.h"
#include "Event.h"
#include "Position.h"

#define EXCEPT_TRY try {
#define EXCEPT_CATCH }catch(int n) { D_ERROR("[%s:%d] catch int exception : %d\n", __FILE__, __LINE__, n); }  \
	catch(const std::exception & e) { D_ERROR("[%s:%d] catch std except: %s\n", __FILE__, __LINE__, e.what()); } \
	catch(...) { D_ERROR("[%s:%d] catch unknown exception\n", __FILE__, __LINE__); }

namespace logicai
{
	const float _M_PI_2 = 1.57079632679489661923f;

	typedef scripts::PosVec PosVec;

	class GameObj;
	class Creature;
	class Player;

	template<class T>
	struct intrusive_traits;

	template<>
	struct intrusive_traits<GameObj>
	{
		static void add_ref(GameObj * obj);
		static void release_ref(GameObj * obj);
	};

	template<>
	struct intrusive_traits<Creature> : public intrusive_traits<GameObj> { };

	template<>
	struct intrusive_traits<Player> : public intrusive_traits<GameObj> { };

	template<class T>
	class intrusive_shared_ptr
	{
	public:
		intrusive_shared_ptr(): p_(0)
		{
		}

		intrusive_shared_ptr(T * p, bool add_ref = true): p_(p)
		{
			if (p_)
				intrusive_traits<T>::add_ref(p_);
		}

		intrusive_shared_ptr(const intrusive_shared_ptr & sp): p_(sp.p_)
		{
			if (p_)
				intrusive_traits<T>::add_ref(p_);
		}

		template<class U>
		intrusive_shared_ptr(const intrusive_shared_ptr<U> & sp): p_(sp.get())
		{
			if (p_)
				intrusive_traits<T>::add_ref(p_);
		}

		~intrusive_shared_ptr()
		{
			this->reset();
		}

		intrusive_shared_ptr & operator=(const intrusive_shared_ptr & sp)
		{
			if (this != &sp)
				intrusive_shared_ptr(sp).swap(*this);
			return *this;
		}

		template<class U>
		intrusive_shared_ptr & operator=(const intrusive_shared_ptr<U> & sp)
		{
			if (p_ != sp.get())
				intrusive_shared_ptr(sp).swap(*this);
			return *this;
		}

		void reset()
		{
			if (p_)
			{
				intrusive_traits<T>::release_ref(p_);
				p_ = 0;
			}
		}

		void swap(intrusive_shared_ptr & sp)
		{
			(std::swap)(p_, sp.p_);
		}

		T * operator->()const
		{
			if (p_) return p_;
			throw std::domain_error("derefence zero pointer");
		}

		T * get()const
		{
			return p_;
		}

		bool operator==(const intrusive_shared_ptr & other)const { return p_ == other.p_; }
		bool operator!=(const intrusive_shared_ptr & other)const { return p_ != other.p_; }

	private:
		T * p_;
	};


	typedef intrusive_shared_ptr<GameObj> ObjPtr;
	typedef intrusive_shared_ptr<Creature> CreaturePtr;
	typedef intrusive_shared_ptr<Player> PlayerPtr;

	class GameObj
	{
	public:
		GameObj(const unsigned objId);

		virtual ~GameObj();

		const unsigned GetId()const;

		virtual const std::string & GetName()const = 0;

		virtual const std::string & GetIdent()const = 0;

		virtual const position & GetCurPos()const = 0;

		virtual const unsigned GetMapId()const = 0;

		virtual const unsigned GetGateId()const = 0;

		virtual const MUX_PROTO::PlayerID GetMPPid()const = 0;

		virtual const unsigned GetSize() const = 0;

		virtual const unsigned GetRace()const = 0;

		virtual const unsigned GetLevel()const = 0;

		virtual const bool GetEvil()const = 0;

		virtual const bool GetRogue()const = 0;

		virtual const bool IsCanBeAttacked()const = 0;

		virtual const unsigned GetInherentStand()const = 0;

		virtual void OnUpdate(unsigned) = 0;

		virtual void OnDied() = 0;

		virtual void OnLost() = 0;

		virtual const bool IsPlayer()const = 0;

		void Observe_DiedEvent(GameObj *, unsigned);

		void Notify_DiedEvent();

		virtual void OnReceivedDiedEvent(ObjPtr &) = 0;

		void Observe_LostEvent(GameObj *, unsigned);

		void UnobserveAll_LostEvent();

		void Notify_LostEvent();

		virtual void OnReceivedLostEvent(ObjPtr &) = 0;

		virtual void EnterView(ObjPtr &) = 0;

		virtual void LeaveView(ObjPtr &) = 0;
		
		virtual void Reset() = 0;

		virtual bool CheckValid()const { return true; }

		void AddRef();

		void ReleaseRef();

		unsigned RefCount()const;
/*
		void Register_timerout(unsigned, unsigned);

		virtual void OnTimeout(unsigned, unsigned);

		void Observe_HpChange(GameObj *, float, unsigned);

		void Notify_HpChange(float);

		void OnReceivedHpChanged(GameObj *, float, unsigned);
		*/

	protected:
		GameObj(const GameObj &);
		const GameObj & operator=(const GameObj &);

		const unsigned objId_;
		ACE_Time_Value lastUpdateTime_;

		unsigned ref_count_;

		std::vector<ObjPtr> observers_;;
		std::vector<ObjPtr> observing_;
	};

	namespace scripts
	{
		/*template<>
		GameObj * L_tovalue<GameObj *>(lua_State & l, int id);*/

		/*template<>
		GameObj * L_totopvalue_pop<GameObj *>(lua_State & l);

		int L_pushvalue(lua_State & l, GameObj *);*/
	}

	struct DistCheckPred
	{
		GameObj & obj;

		DistCheckPred(GameObj &);

		const bool operator()(const ObjPtr & left, const ObjPtr & right)const;
	};
}
#endif
