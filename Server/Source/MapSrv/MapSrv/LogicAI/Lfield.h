﻿#pragma once

#include "Ldef.h"

__BEGIN_SCRIPTS_L_NS__

class Lfield_base : public L_base
{
public:
	Lfield_base(lua_State & l, const char * name)
		: L_(&l)
	{
		lua_getglobal(L_, name);
		if (lua_isnil(L_, -1))
		{
			lua_pop(L_, 1);
			throw 0;
		}
	}

	Lfield_base(lua_State & l, const Ltable &, const char * name)
		: L_(&l)
	{
		lua_getfield(L_, -1, name);
		if (lua_isnil(L_, -1))
		{
			lua_pop(L_, 1);
			throw 0;
		}
	}

	~Lfield_base()
	{
		lua_pop(L_, 1);
	}

protected:
	lua_State * L_;

}; // class Lvalue_base

template<class T>
class Lfield : public Lfield_base
{
public:
	Lfield(lua_State & l, const char * name) : Lfield_base(l, name) { }

	Lfield(lua_State & l, const Ltable & t, const char * name) : Lfield_base(l, t, name) { }

	T get()const
	{
		return L_tovalue<T>(*L_, -1);
	}

}; // class Lfield

__END_SCRIPTS_L_NS__
