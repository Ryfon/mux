﻿#pragma once

#include "Action.h"
#include "MoveControl2.h"

namespace logicai
{
	class ActionMoveTo : public Action_base
	{
	public:
		ActionMoveTo(CreaturePtr &, const position &, float);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin();

		virtual void OnFaitEnd() ;

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnLostObj(ObjPtr &);

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		virtual int GetType()const { return ACTION_MOVE_LINETO; }

		void HandleSpeedDown(int);

	private:
		MoveControl2 move_;
	};
}
