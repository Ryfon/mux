﻿#include "ActionFollowedTask.h"
#include "Creature.h"
#include "LogicAIEngine.h"

namespace logicai
{
	ActionFollowedTask::ActionFollowedTask(CreaturePtr & creature, ObjPtr & target, 
		const std::vector<position> & wayPoints, unsigned taskId, unsigned waitTime)
		: task_(creature, target, taskId, waitTime)
		, patrol_(creature, wayPoints, PATROL_SURGE)
		, Action_base(creature)
	{
		creature->SetNotifyWayPoint(false);
	}

	void ActionFollowedTask::OnEnter()
	{
		task_.OnEnter();
		patrol_.OnEnter();
	}

	void ActionFollowedTask::OnLeave()
	{
		task_.OnLeave();
		patrol_.OnLeave();
	}

	void ActionFollowedTask::OnPause()
	{
		task_.OnPause();
		patrol_.OnPause();
	}

	void ActionFollowedTask::OnResume()
	{
		task_.OnResume();
		patrol_.OnResume();
	}

	void ActionFollowedTask::OnStop()
	{
		task_.OnStop();
		patrol_.OnResume();
	}

	void ActionFollowedTask::OnLostObj(ObjPtr & obj)
	{
		task_.OnLostObj(obj);
		patrol_.OnLostObj(obj);

		if (task_.IsEnd() || patrol_.IsEnd())
			this->Stop();
	}

	void ActionFollowedTask::OnTargetLeaveMap(ObjPtr & obj)
	{
		if (obj.get() == task_.GetTaskTarget().get())
			this->Stop();
	}

	void ActionFollowedTask::HandleSpeedDown(int val)
	{
		patrol_.HandleSpeedDown(val);
		task_.HandleSpeedDown(val);
	}

	void ActionFollowedTask::OnUpdate(unsigned elapsed)
	{
		task_.OnUpdate(elapsed);
		patrol_.OnUpdate(elapsed);

		if (task_.IsEnd() || patrol_.IsEnd())
			this->Stop();
	}

	void ActionFollowedTask::OnRun(unsigned elapsed)
	{
		if (this->creature_->IsFait() || this->creature_->IsBound())
			return;

		task_.OnRun(elapsed);
		patrol_.OnRun(elapsed);

		if (patrol_.IsReachSomePos())
		{
			if (patrol_.GetCurPosIdx() == patrol_.GetWayPointsSize())
				patrol_.Stop();

			LogicAIEngine::instance().L().Table(
				this->creature_->GetIdent().c_str()).Mfunction<void (unsigned)>("_HandlePatrolAt")(patrol_.GetCurPosIdx());
			
		}
	
		if (task_.IsEnd() || patrol_.IsEnd())
			this->Stop();

		if (task_.GetTaskTarget().get())
		{
			if (!this->creature_->IsInsight(task_.GetTaskTarget()))
			{
				patrol_.Pause();
			}
			else if (patrol_.IsPause())
			{
				patrol_.Resume();
			}
		}

	}
}