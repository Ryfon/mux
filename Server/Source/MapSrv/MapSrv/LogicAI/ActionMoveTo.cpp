﻿#include "ActionMoveTo.h"
#include "Creature.h"

namespace logicai
{
	ActionMoveTo::ActionMoveTo(CreaturePtr & creature, const position & pos, float speed)
		: Action_base(creature), move_(creature, speed, pos)
	{
	}

	void ActionMoveTo::OnEnter()
	{
		move_.Resume();
	}

	void ActionMoveTo::OnLeave()
	{
		move_.Stop();
		move_.Reset();
		Action_base::OnLeave();
	}

	void ActionMoveTo::OnPause()
	{
		move_.Pause();
	}

	void ActionMoveTo::OnResume()
	{
		move_.Resume();
	}

	void ActionMoveTo::OnStop()
	{
		move_.Stop();
		move_.Reset();
		Action_base::OnStop();
	}

	void ActionMoveTo::OnFaitBegin() 
	{
		move_.FaitBegin();
	}

	void ActionMoveTo::OnFaitEnd()  
	{
		move_.FaitEnd();
	}

	void ActionMoveTo::OnBoundBegin() 
	{ 
		move_.BoundBegin();
	}

	void ActionMoveTo::OnBoundEnd() 
	{
		move_.BoundEnd();
	}

	void ActionMoveTo::OnLostObj(ObjPtr &)
	{
	}

	void ActionMoveTo::OnUpdate(unsigned elapsed)
	{
	}

	void ActionMoveTo::OnRun(unsigned elapsed)
	{
		move_.Update(elapsed);

		if (move_.IsReach())
			this->Stop();
	}

	void ActionMoveTo::HandleSpeedDown(int val)
	{
		move_.HandleSpeedDown(val);
	}
}