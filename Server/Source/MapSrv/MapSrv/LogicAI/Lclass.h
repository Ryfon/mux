﻿#pragma once

#include "Ldef.h"

__BEGIN_SCRIPTS_L_NS__

template<class T>
class Lclass
{
public:
	static T * L_getuserdata(lua_State & l, int idx)
	{
		// TODO: check

		lua_getfield(&l, idx, "__internal_userdata");
		T * p = (T *)lua_touserdata(&l, -1);
		lua_pop(&l, 1);
		return p;
	}

private:
	template<class, class, class, class, class, class>
	friend class Lconstructor;

	friend class Lconstructor_base;

	template<class, class F, F f>
	friend class Lcmfunction;

	template<class>
	friend class Lcmfunction_base;

	friend class L;

	static int L_getmetatable(lua_State * pL)
	{
		lua_pushlightuserdata(pL, (void *)&Key);
		lua_gettable(pL, LUA_REGISTRYINDEX);
		if (lua_isnil(pL, -1))
		{
			lua_pop(pL, 1);
			lua_newtable(pL);
			lua_pushlightuserdata(pL, (void *)&Key);
			lua_pushvalue(pL, -2);
			lua_settable(pL, LUA_REGISTRYINDEX);
		
			lua_pushcfunction(pL, L_destroy);
			lua_setfield(pL, -2, "__gc");

			lua_pushvalue(pL, -1);
			lua_setfield(pL, -2, "__index");
			return 1;
		}
		return 0;
	}

	static T * L_newuserdata(lua_State * pL)
	{
		T * p = (T *)lua_newuserdata(pL, sizeof(T));
		if (!p)
			throw std::bad_alloc();

		L_getmetatable(pL);
		lua_setmetatable(pL, -2);
		return p;
	}

	static void L_newtable(lua_State * pL, T * p)
	{
		lua_newtable(pL);
		L_getmetatable(pL);
		lua_setmetatable(pL, -2);

		lua_pushvalue(pL, -2);
		lua_setfield(pL, -2, "__internal_userdata");
		lua_remove(pL, -2);
	}

	static int L_destroy(lua_State * pL)
	{
		T * p = (T *)lua_touserdata(pL, -1);
		if (p)
		{
			p->~T();
		}
		return 0;
	}

	static void reg(lua_State * pL, const char * name, bool G)
	{
		L_getmetatable(pL);
		lua_pushstring(pL, name);
		lua_setfield(pL, -2, "__internal_this_class_name");
		lua_pop(pL, 1);

		if (G)
		{
			lua_getglobal(pL, name);
			if (lua_isnil(pL, -1))
			{
				lua_pop(pL, 1);
				lua_newtable(pL);
				lua_pushvalue(pL, -1);
				lua_setglobal(pL, name);
			}
		}
		else
		{
			lua_getfield(pL, -1, name);
			if (lua_isnil(pL, -1))
			{
				lua_pop(pL, 1);
				lua_newtable(pL);
				lua_pushvalue(pL, -1);
				lua_setfield(pL, -2, name);
			}
		}

		L_getmetatable(pL);
		lua_setfield(pL, -2, "__this_class_mt");
		lua_pop(pL, 1);
	}

	template<class B>
	static void reg(lua_State * pL)
	{
		L_getmetatable(pL);
		lua_newtable(pL);
		Lclass<B>::L_getmetatable(pL);
		lua_setfield(pL, -2, "__index");
		lua_setmetatable(pL, -2);
		lua_pop(pL, 1);
	}

	static int Key;

	template<class>
	friend class Lclass;

}; // class Lclass;

template<class T> int Lclass<T>::Key = 0;

__END_SCRIPTS_L_NS__
