﻿#include "ActionFollowingTask.h"

namespace logicai
{
	ActionFollowingTask::ActionFollowingTask(CreaturePtr & creature, ObjPtr & target, unsigned taskId, unsigned waitTime)
		: task_(creature, target, taskId, waitTime)
		, follow_(creature, target)
		, Action_base(creature)
	{

	}

	void ActionFollowingTask::OnEnter()
	{
		task_.OnEnter();
		follow_.OnEnter();
	}

	void ActionFollowingTask::OnLeave()
	{
		task_.OnLeave();
		follow_.OnLeave();
	}

	void ActionFollowingTask::OnPause()
	{
		task_.OnPause();
		follow_.OnPause();
	}

	void ActionFollowingTask::OnResume()
	{
		task_.OnResume();
		follow_.OnResume();
	}

	void ActionFollowingTask::OnStop()
	{
		task_.OnStop();
		follow_.OnStop();
	}

	void ActionFollowingTask::OnLostObj(ObjPtr & obj)
	{
		task_.OnLostObj(obj);
		follow_.OnLostObj(obj);

		if (task_.IsEnd() || follow_.IsEnd())
			this->Stop();
	}

	void ActionFollowingTask::OnTargetLeaveMap(ObjPtr & obj)
	{
		if (obj.get() == task_.GetTaskTarget().get())
			this->Stop();
	}

	void ActionFollowingTask::OnUpdate(unsigned elapsed)
	{
		task_.OnUpdate(elapsed);
		follow_.OnUpdate(elapsed);

		if (task_.IsEnd() || follow_.IsEnd())
			this->Stop();
	}

	void ActionFollowingTask::OnRun(unsigned elapsed)
	{
		task_.OnRun(elapsed);
		follow_.OnRun(elapsed);

		if (task_.IsEnd() || follow_.IsEnd())
			this->Stop();
	}

	void ActionFollowingTask::HandleSpeedDown(int val)
	{
		follow_.HandleSpeedDown(val);
		task_.HandleSpeedDown(val);
	}
}