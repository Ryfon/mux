﻿#pragma once

#include "Ldef.h"
#include "Lfield.h"
#include "Lcfunction.h"
#include "Lclass.h"
#include "Lcmfunction.h"

__BEGIN_SCRIPTS_L_NS__

class Ltable : public L_base
{
public:
	Ltable(lua_State & l, const char * tname)
		: L_(&l)
	{
		lua_getglobal(L_, tname);
		if (!lua_istable(L_, -1))
		{
			int ltype = lua_type(L_, -1);
			lua_pop(L_, 1);
			throw std::runtime_error(std::string("expect table[") +
				std::string(tname) + std::string("] value(a ") + 
				std::string(lua_typename(L_, ltype)) +
				std::string(" value)"));
		}
	}

	Ltable(lua_State & l, const Ltable &, const char * tname)
		: L_(&l)
	{
		lua_getfield(L_, -1, tname);
		if (!lua_istable(L_, -1))
		{
			int ltype = lua_type(L_, -1);
			lua_pop(L_, 1);
			throw std::runtime_error(std::string("expect table[") +
				std::string(tname) + std::string("] value(a ") + 
				std::string(lua_typename(L_, ltype)) +
				std::string(" value)"));
		}
	}

	Ltable(lua_State & l) : L_base(), L_(&l)
	{
		if (!lua_istable(L_, -1))
		{
			int ltype = lua_type(L_, -1);
			lua_pop(L_, 1);
			throw std::runtime_error(std::string("expect table value(a ") + 
				std::string(lua_typename(L_, ltype)) +
				std::string(" value)"));
		}
	}
	
	Ltable(const Ltable & t) : L_base(), L_(t.L_)
	{
		lua_pushvalue(t.L_, -1);
	}

	~Ltable()
	{
		lua_pop(L_, 1);
	}

	template<class F, F f>
	void reg(const Lcfunction<F, f>, const char * fname)
	{
		lua_pushcfunction(L_, (Lcfunction<F, f>::L_cfunction));
		lua_setfield(L_, -2, fname);
	}

	template<class T>
	void reg(const Lclass<T>, const char * name)const
	{
		Lclass<T>::reg(L_, name, false);
	}

	template<class T, class A1, class A2, class A3, class A4, class A5, class A6>
	void reg(const Lconstructor<A1, A2, A3, A4, A5, A6>)const
	{
		Lconstructor<A1, A2, A3, A4, A5, A6>::template reg<T>(L_, "new", false);
	}

	template<class T, class F, F f>
	void reg(const Lcmfunction<T, F, f>, const char * fname)const
	{
		Lcmfunction<T, F, f>::reg(L_, fname);
	}

	template<class F> 
	Lfunction<F, false> Function(const char * fname)
	{
		return Lfunction<F, false>(*L_, *this, fname);
	}

	template<class F>
	Lfunction<F, true> Mfunction(const char * fname)
	{
		return Lfunction<F, true>(*L_, *this, fname);
	}

	Ltable Table(const char * tname)
	{
		return Ltable(*L_, *this, tname);
	}

	template<class T>
	Lfield<T> Field(const char * name)
	{
		return Lfield<T>(*L_, *this, name);
	}

protected:
	lua_State * L_;

}; // class Ltable

__END_SCRIPTS_L_NS__
