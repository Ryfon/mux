﻿#pragma once

#ifndef __MUX_LOGICAI_CREATURE_H__
#define __MUX_LOGICAI_CREATURE_H__

#include "GameObj.h"
#include "SkillList.h"
#include "ThreatList.h"
#include "ActionHolder.h"
#include "../Lua/include/lua.hpp"
#include "Ldef.h"
#include "ObjManager.h"

namespace logicai
{
	class Player;

	class Creature : public GameObj
	{
	public:
		Creature(const unsigned ID);

		~Creature();

		void Rename(const std::string);

		void SetMapInfo(const AIMapInfo & info);

		void SetRadius(float);

		const position GetBornPos()const;

		const AIMapInfo & GetMapInfo()const;

		const float GetHPPercent()const;

		const float GetMoveRadius()const;

		virtual const std::string & GetName()const;

		virtual const std::string & GetIdent()const;

		virtual const position & GetCurPos()const;

		virtual const unsigned GetSize() const;

		virtual const unsigned GetClassId()const;

		virtual const unsigned GetGateId()const;

		const MUX_PROTO::PlayerID GetMPPid()const;

		virtual const unsigned GetRace() const;

		virtual const bool GetEvil()const;

		virtual const bool GetRogue()const;

		virtual const bool IsCanBeAttacked()const;

		virtual const unsigned GetInherentStand()const;

		virtual const bool IsPlayer()const;

		virtual const bool GetActiveFlag()const;

		virtual const bool IsInThreatList(const ObjPtr &)const;

		virtual void SetActive(bool);

		virtual void SetCurPos(const position & pos);

		virtual void SetCurPos(const float x, const float y);

		virtual const unsigned GetMapId()const;

		virtual std::vector<ObjPtr> & GetViewObjList();

		virtual void OnOtherBeyondView(GameObj &);

		virtual void OnAttacked(ObjPtr & attacker, unsigned reactTime, unsigned threat, bool);

		virtual void OnBorn();

		virtual void OnDied();

		virtual void OnLost();

		virtual void OnFaitBegin();

		virtual void OnFaitEnd();

		virtual void OnBoundBegin();

		virtual void OnBoundEnd();

		virtual void OnUpdate(unsigned);

		virtual void OnReceivedDiedEvent(ObjPtr &);

		virtual void OnReceivedLostEvent(ObjPtr &);

		virtual void EnterView(ObjPtr &);

		virtual void LeaveView(ObjPtr &);

		void LeaveMap(ObjPtr &);

		void ChangeState(int state);

		void ChangeMoveState(int state);

		void ChangeFightState(int state);

		void AddThreat(GameObj * obj, unsigned threat, bool); 

		const bool ThreatListEmpty()const;

		CMuxSkillProp * GetSkill(ObjPtr &); 

		void AddSkill(unsigned skillId);

		void RemoveSkill(unsigned skillId);

		int GetAttackInterval()const;

		unsigned GetSpeciSkill()const;

		void SetSpeciSkill(unsigned);

		int GetSkillMode()const;

		void SetSkillMode(int m, unsigned skillId);

		ThreatList & GetThreatList();

		void ClearThreatList();

		unsigned GetCurAttackInterval()const;

		void SetCurAttackInterval(unsigned);

		void SetNotifySkillCasting(bool);

		bool IsNotifySkillCasting()const;

		bool IsInsight(ObjPtr & obj);

		void SetNotifySkillCastingFlag(bool = true);

		void ClearAllBuffs(bool buf);

		void SetHpPercent(float v);

		float GetHpPercent()const;

		float GetChaseSpeed();

		float GetEscapeSpeed();

		float GetGoHomeSpeed();

		float GetFollowingSpeed();

		float GetWanderSpeed();

		float GetPatrolSpeed();

		void SetChaseSpeed(float s) { chaseSpeed_ = s; }

		void SetEscapeSpeed(float s) { escapeSpeed_ = s; }

		void SetGoHomeSpeed(float s) { gohomeSpeed_ = s; }

		void SetFollowingSpeed(float s) { followingSpeed_ = s; }

		void SetWanderSpeed(float s) { wanderSpeed_ = s; }

		void SetPatrolSpeed(float s) { patrolSpeed_ = s; }

		void SetMoveMode(int);

		void SetMoveWander();

		void SetMoveIdle();

		void SetMovePatrol(std::vector<position>,int);

		bool GetMovingMobility()const { return movingMobility_; }

		bool GetAttackMobility()const { return attackMobility_; }

		bool GetActiveMoblity()const { return activeMobility_; }

		void SetMovingMobility(bool m) { movingMobility_ = m; }

		void SetAttackMobility(bool m) { attackMobility_ = m; }

		void SetActiveMobility(bool m) { activeMobility_ = m; }

		bool IsFighting()const { return isFighting_; }

		void SetFightingFlag(bool f) { isFighting_ = f; }

		void Fight(unsigned reactTime);

		void Help();

		void Summon(unsigned, unsigned, const position);

		void MoveTo(const position, float speed);

		void Treat(GameObj *, unsigned);

		void Follow(GameObj *);

		void CastAOE(unsigned);

		void CastAOEAt(GameObj *, unsigned);

		void Escape();

		void Chase();

		void GoHome();

		void StopFight();

		void AttackTarget(GameObj *, unsigned);

		void Defend();

		void MoveToByWayPoints(std::vector<position>);

		void InitSkills();

		bool IsBound()const { return bound_; }

		bool IsFait()const { return fait_; }

		void ResetFait() { fait_ = false; }

		void ResetBound() { bound_ = false; }

		GameObj * GetCurAttackTarget();

		bool IsNotifyHpChanged()const { return notifyHpChanged_; }

		void SetNotifyHpChanged(bool b) { notifyHpChanged_ = b; }

		bool IsNotifyAttacked()const { return notifyAttacked_; }

		void SetNotifyAttacked(bool b) { notifyAttacked_ = b; } 

		void ActiveAttackAlerting();

		bool IsCanAttack(ObjPtr & target)const;

		void Speek(const std::string);

		void Yell(const std::string);

		void Boradcast(const std::string);

		void Disable(unsigned t);

		void Bound(unsigned t);

		void Unbound();

		void Call(unsigned, unsigned, bool);

		void ExitFight();

		void SetMesh(int);

		void SetMeshScale(float);

		void PlaySound(const std::string, int);

		const unsigned GetLevel()const { return level_; }

		unsigned GetLiveTime()const { return liveTime_; }

		void NotifyBindTaskSuccessed(int);

		void NotifyBindTaskFail();

		void NotifyBindTaskDroped(unsigned taskId);

		void SetDieState(int);

		void SetUndefeat(int);
		
		void SetPkgDropFlag(bool);

		void SpeciStr(int type, const std::string);

		void PersistSpeciStr(int type, const std::string);

		void SetNpcSeq(unsigned,unsigned);

		void SetNotifyThreatList(bool b) { notifyThreatList_ = b; }

		void FollowedTaskBegin(GameObj *, PosVec, unsigned, unsigned);

		void FollowingTaskBegin(GameObj *, unsigned,unsigned);

		unsigned GetAIPattern()const { return aiPattern_; }

		bool GetChaseAbility()const { return chaseAbility_; }

		void SetChaseAbility(bool b) { chaseAbility_ = b; }

		void SetSight(unsigned s) { sight_ = s; }

		bool IsUpdate()const { return isUpdate_; }

		bool IsCanRun(Action_base *)const;

		bool CheckAttackSight(ObjPtr &)const;

		float GetChaseDist()const { return chaseDist_; }

		void SetChaseDist(float d) { chaseDist_ = d; }

		void SetNotifyViewObj(bool b) { notifyLuaView_ = b; }

		bool GetNotifyViewObj()const { return notifyLuaView_; }

		void SetNotifyWayPoint(bool b ) { notifyWayPoint_ = b; }

		bool GetNotifyWayPoint()const { return notifyWayPoint_; }

		void NotifyLevelMapInfo(unsigned);

		float GetMaxMapX()const { return maxMapX_; }

		float GetMaxMapY()const { return maxMapY_; }

		void SetNotifyLuaUpdate(bool b);

		void HandleSpeedDown(int);

		int GetDiedReason() { return diedReason_; }

		void SetDiedReason(int f) { diedReason_ = f; }

		void HandleThreatCured(ObjPtr caster, ObjPtr target, unsigned val);

		void SynchThreatList(GameObj * creature);

		virtual void Reset();
	private:
		std::string name_;
		std::string ident_;
		position curPos_;
		position spawnedPos_;
		AIMapInfo mapInfo_;
		float radius_;
		int size_;
		unsigned long type_;
		unsigned int maxHp_;
		unsigned int curHp_;
		unsigned long level_;
		unsigned race_;
		bool isBeAttacked_;
		unsigned int inherentStand_;
		int attackInterval_;
		unsigned cureAttackInterval_;
		unsigned speciSkillId_;
		int skillMode_;
		bool notifySkillCasting_;

		float chaseSpeed_;
		float escapeSpeed_;
		float gohomeSpeed_;
		float followingSpeed_;
		float wanderSpeed_;
		float patrolSpeed_;

		bool attackMobility_;
		bool movingMobility_;
		bool activeMobility_;
		bool escapeAbility_;
		bool chaseAbility_;

		bool isFighting_;

		bool bound_;
		bool fait_;

		bool notifyHpChanged_;
		bool notifyAttacked_;

		bool notifyLuaLost_;

		unsigned disableTime_;
		unsigned liveTime_;

		ThreatList threats_;
		SkillList skills_;

		bool notifyThreatList_;

		unsigned aiPattern_;

		unsigned sight_;

		bool isUpdate_;

		float chaseDist_;

		unsigned boundTime_;

		bool notifyLuaView_;
		bool notifyName_;

		bool notifyWayPoint_;

		float maxMapX_;
		float maxMapY_;

		unsigned alertingInterval_;

		bool notifyLuaUpdate_;

		bool isDefend_;

		int nken_;

		int speedDown_;

		int diedReason_;

		std::pair<int, std::string> speciStr_;

		std::vector<ObjPtr> viewList_;
		std::vector<ActionHolder> actions_;
	};	
};
#endif
