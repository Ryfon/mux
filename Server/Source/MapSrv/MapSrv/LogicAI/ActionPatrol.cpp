﻿#include "ActionPatrol.h"
#include "Creature.h"
#include "LogicAIEngine.h"

namespace logicai
{
	ActionPatrol::ActionPatrol(CreaturePtr & creature, const std::vector<position> & wayPoints, int mode, bool loop)
		: Action_base(creature), wayPoints_(wayPoints), patrolMode_(mode), curPosIdx_(-1)
		, isReachSomPos_(false), move_(creature, creature->GetPatrolSpeed(), creature->GetCurPos())
		, loop_(loop)
	{
		if (wayPoints_.empty())
			D_ERROR("路点为空！\n");
	}

	void ActionPatrol::OnEnter()
	{
		move_.SetTargetPos(GetNextPos());
		move_.Resume();
	}

	void ActionPatrol::OnLeave()
	{
		move_.Stop();
		move_.Reset();
		Action_base::OnLeave();
	}

	void ActionPatrol::OnPause()
	{
		move_.Pause();
	}

	void ActionPatrol::OnResume()
	{
		move_.Resume();
	}

	void ActionPatrol::OnStop()
	{
		move_.Stop();
		move_.Reset();
		Action_base::OnStop();
	}

	void ActionPatrol::OnFaitBegin()
	{ 
		move_.FaitBegin();
	}

	void ActionPatrol::OnFaitEnd()  
	{ 
		move_.FaitEnd();
	}

	void ActionPatrol::OnBoundBegin()
	{ 
		move_.BoundBegin();
	}

	void ActionPatrol::OnBoundEnd() 
	{
		move_.BoundEnd();
	}

	void ActionPatrol::OnLostObj(ObjPtr &)
	{
	}

	void ActionPatrol::HandleSpeedDown(int val)
	{
		move_.HandleSpeedDown(val);
	}

	void ActionPatrol::OnRun(unsigned elapsed)
	{
		if (!isReachSomPos_)
		{
			move_.Update(elapsed);

			if (move_.IsReach())
			{
				isReachSomPos_ = curPosIdx_;

				if (creature_->GetNotifyWayPoint())
					LogicAIEngine::instance().L().Table(
						creature_->GetIdent().c_str()).Mfunction<void (unsigned)>("_HandlePatrolAt")(isReachSomPos_);
			}
		}
		else
		{
			if (loop_ || curPosIdx_ != wayPoints_.size())
			{
				move_.SetTargetPos(GetNextPos());
				move_.Resume();
				isReachSomPos_ = 0;
			}
			else
			{
				this->Stop();
			}
		}
	}

	void ActionPatrol::OnUpdate(unsigned elapsed)
	{
		isReachSomPos_ = false;
	}

	size_t ActionPatrol::GetCurPosIdx()const
	{
		/*if (isReachSomPos_)
		{
			std::vector<position>::const_iterator it = std::find(
				wayPoints_.begin(), wayPoints_.end(), this->creature_->GetCurPos());
			if (wayPoints_.end() != it && it - wayPoints_.begin() == curPosIdx_)
				return curPosIdx_;
		}
		return 0;*/
		return isReachSomPos_;
	}

	const bool ActionPatrol::IsReachSomePos()const
	{
		return isReachSomPos_ != 0;
	}

	const position & ActionPatrol::GetNextPos()
	{
		AI_ASSERT(wayPoints_.size());

		if (curPosIdx_ == -1)
		{
			curPosIdx_ = 0;
		}
		else if (patrolMode_ == PATROL_SURGE)
		{
			if (curPosIdx_ >= wayPoints_.size())
				std::reverse(wayPoints_.begin(), wayPoints_.end());			
		}
		
		if (curPosIdx_ >= wayPoints_.size())
			curPosIdx_ = 0;

		return wayPoints_.at(curPosIdx_++);
	}
}