﻿#pragma once

#ifndef __MUX_LOGICAI_THREATLIST_H__
#define __MUX_LOGICAI_THREATLIST_H__

#include "GameObj.h"
#include "../AISys/ThreatList.h"

namespace logicai
{
	class ThreatList
	{
	public:
		ThreatList(unsigned maxSize = 10);

		int AddObj(ObjPtr &, unsigned);

		void RemoveObj(ObjPtr &);

		ObjPtr GetMaxThreat();
		
		unsigned MaxSize()const;

		void ResetMaxSize(unsigned size);

		unsigned Size()const;

		const bool HasCheck(const ObjPtr &, unsigned * pIdx = 0)const;

		const bool FirstCheck(ObjPtr &)const;

		ObjPtr GetMinDist();

		void CleanThreatNoChange();

		ObjPtr RandomCreature(time_t t = 30);

		ObjPtr Get(unsigned);

		void Clear();

		void Dump(_ThreatList & tl)const;

		struct ThreatData
		{
			ObjPtr   obj;
			unsigned threat;
			time_t  lastUpdate;

			ThreatData(ObjPtr & _obj, unsigned _threat)
				: obj(_obj), threat(_threat), lastUpdate(::time(&lastUpdate))  { }

			const bool operator<(const ThreatData & r)const
			{
				return threat < r.threat;
			}

			const bool operator>(const ThreatData & r)const
			{
				return threat > r.threat;
			}

			const bool operator==(const ObjPtr & r)const
			{
				return obj == r;
			}
		};

	public:

		std::vector<ThreatData> lst_;
		unsigned maxSize_;
	};
};
#endif
