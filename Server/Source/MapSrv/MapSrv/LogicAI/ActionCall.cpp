﻿#include "ActionCall.h"
#include "Creature.h"
#include "LogicAIEngine.h"

namespace logicai
{
	ActionCall::ActionCall(CreaturePtr & creature, unsigned count, unsigned cls, bool cp)
		: Action_base(creature), class_(cls), count_(count), copy_threatlist_(cp)
	{

	}

	void ActionCall::OnEnter()
	{
	}

	void ActionCall::OnLeave()
	{
		Action_base::OnLeave();
	}

	void ActionCall::OnPause()
	{

	}

	void ActionCall::OnResume()
	{

	}

	void ActionCall::OnStop()
	{
		Action_base::OnStop();
	}

	void ActionCall::OnLostObj(ObjPtr &)
	{

	}

	void ActionCall::OnFaitBegin()
	{

	}

	void ActionCall::OnFaitEnd()
	{

	}

	void ActionCall::OnBoundBegin()
	{

	}

	void ActionCall::OnBoundEnd()
	{

	}

	void ActionCall::OnUpdate(unsigned)
	{

	}

	void ActionCall::OnRun(unsigned)
	{
		if (this->creature_->IsFait() || this->creature_->IsBound())
			return;

		for(size_t i = 0, count = 0; i < this->creature_->GetViewObjList().size() && count < count_; ++i)
		{
			if (this->creature_->GetViewObjList()[i]->IsPlayer())
				continue;

			Creature * target = dynamic_cast<Creature *>(this->creature_->GetViewObjList()[i].get());
			if (!target)
				continue;

			if ((class_ == 0 || target->GetClassId() == class_) && !target->IsFighting() && target->GetRace() == 20
				&& target->GetActiveFlag())
			{
				if (copy_threatlist_)
				{
					for(size_t c = 0; c < this->creature_->GetThreatList().Size(); ++c)
						target->AddThreat(this->creature_->GetThreatList().Get(c).get(), 0, false);			
				}
				target->Fight(0);

				LogicAIEngine::instance().L().Table(
					this->creature_->GetIdent().c_str()).Mfunction<void (GameObj *)>("_HandleMonsterCalled")(target);
			}
			++count;
		}

		this->Stop();
	}
}