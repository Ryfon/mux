﻿#include "ActionWander.h"
#include "Creature.h"
#include "LogicAIEngine.h"

namespace logicai
{
	ActionWander::ActionWander(CreaturePtr & creature)
		: Action_base(creature)
		, move_(creature, creature->GetWanderSpeed(), creature->GetCurPos())
		, wanderIdleTime_(0)
	{
	}

	void ActionWander::OnEnter()
	{
		NewTargetPos();
		move_.Resume();
	}

	void ActionWander::OnLeave()
	{
		move_.Stop();
		move_.Reset();
		Action_base::OnLeave();
	}

	void ActionWander::OnPause()
	{
		move_.Pause();
	}

	void ActionWander::OnResume()
	{
		move_.Resume();
	}

	void ActionWander::OnStop()
	{
		move_.Stop();
		move_.Reset();
		Action_base::OnStop();
	}

	void ActionWander::OnFaitBegin() 
	{
		move_.FaitBegin();
	}

	void ActionWander::OnFaitEnd() 
	{
		move_.FaitEnd();
	}

	void ActionWander::OnBoundBegin()
	{
		move_.BoundBegin();
	}

	void ActionWander::OnBoundEnd() 
	{
		move_.BoundEnd();
	}

	void ActionWander::OnLostObj(ObjPtr &)
	{
	}

	void ActionWander::OnUpdate(unsigned elapsed)
	{
	}

	void ActionWander::HandleSpeedDown(int val)
	{
		move_.HandleSpeedDown(val);
	}

	void ActionWander::OnRun(unsigned elapsed)
	{
		if (move_.IsReach())
		{
			if (!wanderIdleTime_)
			{
				wanderIdleTime_ = (rand() % 9 + 4) * 1000;
			}

			if (wanderIdleTime_ > elapsed)
				wanderIdleTime_ -= elapsed;
			else
				wanderIdleTime_ = 0;

			if (!wanderIdleTime_)
			{
				NewTargetPos();
			}			
		}

		move_.Update(elapsed);		
	}

	void ActionWander::NewTargetPos()
	{
		float radius = (rand() % 100) / 100.0f * (creature_->GetMoveRadius() - 4.0f) + 4.0f;
		float angle = (rand() % 340) / 1.0f + 20.0f;

		//D_DEBUG("wander angle %d value %f\n", (int)angle, LogicAIEngine::instance().FastSin(angle));

		position pos;
		//pos.y = creature_->GetCurPos().y + LogicAIEngine::instance().FastSin(angle) * radius;
		//pos.x = creature_->GetCurPos().x + LogicAIEngine::instance().FastSin(_M_PI_2 - angle) * radius;
		pos.x = creature_->GetBornPos().x + LogicAIEngine::instance().FastSin(angle) * radius;
		pos.y = creature_->GetBornPos().y + LogicAIEngine::instance().FastSin(270.0f - angle) * radius;

		if (pos.GetX() < creature_->GetBornPos().GetX() - creature_->GetMoveRadius() || 
			pos.GetX() > creature_->GetBornPos().GetX() + creature_->GetMoveRadius())
		{
			pos.x = creature_->GetBornPos().x;
		}
		if (pos.GetY() < creature_->GetBornPos().GetY() - creature_->GetMoveRadius() || 
			pos.GetY() > creature_->GetBornPos().GetY() + creature_->GetMoveRadius())
		{
			pos.y = creature_->GetBornPos().y;
		}

		if (pos.x < 0)		pos.x = 0;
		else if (pos.GetX() >= creature_->GetMaxMapX())	pos.x = creature_->GetMaxMapX() - 1;
		if (pos.y < 0)		pos.y = 0;
		else if (pos.GetY() >= creature_->GetMaxMapY())	pos.y = creature_->GetMaxMapY() - 1;

		bool res = true;
		//if (creature_->GetMapInfo().isNormalOrCopy == false)
			CMonsterWrapper::IsCanPass(creature_->GetMapInfo(), creature_->GetCurPos().GetX(), creature_->GetCurPos().GetY(),
				pos.GetX(), pos.GetY(), res);

		if (res)
		{
			move_.SetTargetPos(pos);
			move_.Resume();
		}
	}
}