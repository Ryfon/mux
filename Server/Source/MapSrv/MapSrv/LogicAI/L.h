﻿#pragma once

#include <stdexcept>

#include "Ldef.h"
#include "Lfield.h"
#include "Ltable.h"
#include "Lfunction.h"
#include "Lmfunction.h"
#include "Lcfunction.h"
#include "Lclass.h"
#include "Lconstructor.h"
#include "Lcmfunction.h"

__BEGIN_SCRIPTS_L_NS__

class L : public L_base
{
public:
	L() : L_(0)
	{
		L_ = luaL_newstate();
		luaL_openlibs(L_);
	}

	~L()
	{
		lua_close(L_);
		L_ = 0;
	}

	void dostring(const char * s)
	{
		if (!s) return;
		if (0 != luaL_dostring(L_, s))
		{
			std::runtime_error err(lua_tostring(L_, -1));
			lua_pop(L_, 1);
			throw err;
		}
	}

	void dofile(const char * path)
	{
		if (!path) return;
		if (0 != luaL_dofile(L_, path))
		{
			std::runtime_error err(lua_tostring(L_, -1));
			lua_pop(L_, 1);
			throw err;
		}
	}

	template<class F, F f>
	void reg(const Lcfunction<F, f>, const char * fname)
	{
		lua_pushcfunction(L_, (Lcfunction<F, f>::L_cfunction));
		lua_setglobal(L_, fname);
	}

	template<class T>
	void reg(const Lclass<T>, const char * name)const
	{
		Lclass<T>::reg(L_, name, true);
	}

	template<class T, class A1, class A2, class A3, class A4, class A5, class A6>
	void reg(const Lconstructor<A1, A2, A3, A4, A5, A6>)const
	{
		Lconstructor<A1, A2, A3, A4, A5, A6>::template reg<T>(L_, "new", true);
	}

	template<class T, class F, F f>
	void reg(const Lcmfunction<T, F, f>, const char * fname)const
	{
		Lcmfunction<T, F, f>::reg(L_, fname);
	}

	template<class D, class B>
	void reg()
	{
		Lclass<D>::reg(L_);
	}

	template<class T>
	Lfield<T> Field(const char * name)const
	{
		return Lfield<T>(*L_, name);
	}

	template<class F>
	Lfunction<F, false> Function(const char * name)const
	{
		return Lfunction<F, false>(*L_, name);
	}

	Ltable Table(const char * tname)const
	{
		return Ltable(*L_, tname);
	}

	Ltable create_table(const char * tname)const
	{
		lua_newtable(L_);
		lua_setglobal(L_, tname);
		return Ltable(*L_, tname);
	}

	void set_nil(const char * name)const
	{
		lua_pushnil(L_);
		lua_setglobal(L_, name);
	}

	int stack_size()
	{
		return lua_gettop(L_);
	}

private:
	lua_State * L_;

}; // class L


__END_SCRIPTS_L_NS__

