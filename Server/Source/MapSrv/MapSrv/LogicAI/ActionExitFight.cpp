﻿#include "ActionExitFight.h"
#include "Creature.h"

namespace logicai
{
	ActionExitFight::ActionExitFight(CreaturePtr & creature)
		: Action_base(creature)
	{
	}

	void ActionExitFight::OnLeave()
	{
		Action_base::OnLeave();
	}

	void ActionExitFight::OnStop()
	{
		Action_base::OnStop();
	}

	void ActionExitFight::OnRun(unsigned elapsed) 
	{ 
		this->creature_->GoHome();
		this->Stop();
	}
}