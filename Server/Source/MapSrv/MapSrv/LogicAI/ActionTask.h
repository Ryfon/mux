﻿#pragma once

#include "Action.h"

namespace logicai
{
	class ActionTask : public Action_base
	{
	public:
		ActionTask(CreaturePtr &, ObjPtr &, unsigned, unsigned);

		virtual void OnEnter();

		virtual void OnLeave();

		virtual void OnPause();

		virtual void OnResume();

		virtual void OnStop();

		virtual void OnFaitBegin() { }

		virtual void OnFaitEnd()  { }

		virtual void OnBoundBegin() { }

		virtual void OnBoundEnd() { }

		virtual void OnLostObj(ObjPtr &);

		virtual void OnUpdate(unsigned elapsed);

		virtual void OnRun(unsigned elapsed);

		void SetTaskSucceed(int);

		void SetTaskFailed();

		void DropTask();

		unsigned GetTaskId()const { return taskId_; }

		int GetType()const { return 0; }

		ObjPtr & GetTaskTarget() { return target_; }

	protected:
		class State;

		class TargetInsightState;
		friend class TargetInsightState;

		class TargetUninsightState;
		friend class TargetUninsightState;

		class TargetLostState;
		friend class TargetLostState;

		void ChangeState(State * newState);

		State * state_;
		ObjPtr target_;
		unsigned countdown_;
		unsigned taskId_;
		unsigned waitTime_;
	};

	class ActionTask::State
	{
	public:
		virtual void OnEnter(ActionTask &) = 0;

		virtual void OnLeave(ActionTask &) = 0;

		virtual void OnPause(ActionTask &) = 0;

		virtual void OnResume(ActionTask &) = 0;

		virtual void OnStop(ActionTask &) = 0;

		virtual void OnLostObj(ActionTask &, ObjPtr &) = 0;

		virtual void OnUpdate(ActionTask &, unsigned elapsed) = 0;

		virtual void OnRun(ActionTask &, unsigned elapsed) = 0;
	};

	class ActionTask::TargetInsightState : public ActionTask::State
	{
	public:
		virtual void OnEnter(ActionTask &);

		virtual void OnLeave(ActionTask &);

		virtual void OnPause(ActionTask &);

		virtual void OnResume(ActionTask &);

		virtual void OnStop(ActionTask &);

		virtual void OnLostObj(ActionTask &, ObjPtr &);

		virtual void OnUpdate(ActionTask &, unsigned elapsed);

		virtual void OnRun(ActionTask &, unsigned elapsed);

		static TargetInsightState * instance();
	};

	class ActionTask::TargetUninsightState : public ActionTask::State
	{
	public:
		virtual void OnEnter(ActionTask &);

		virtual void OnLeave(ActionTask &);

		virtual void OnPause(ActionTask &);

		virtual void OnResume(ActionTask &);

		virtual void OnStop(ActionTask &);

		virtual void OnLostObj(ActionTask &, ObjPtr &);

		virtual void OnUpdate(ActionTask &, unsigned elapsed);

		virtual void OnRun(ActionTask &, unsigned elapsed);

		static TargetUninsightState * instance();
	};

	class ActionTask::TargetLostState : public ActionTask::State
	{
	public:
		virtual void OnEnter(ActionTask &);

		virtual void OnLeave(ActionTask &);

		virtual void OnPause(ActionTask &);

		virtual void OnResume(ActionTask &);

		virtual void OnStop(ActionTask &);

		virtual void OnLostObj(ActionTask &, ObjPtr &);

		virtual void OnUpdate(ActionTask &, unsigned elapsed);

		virtual void OnRun(ActionTask &, unsigned elapsed);

		static TargetLostState * instance();
	};
}
