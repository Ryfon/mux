﻿#include "ObjManager.h"
#include "LogicAIEngine.h"

namespace logicai
{
	void ObjManager::AddObj(GameObj & obj)
	{
		if (obj.GetMPPid().wGID == 1000)
		{
			creatures_[obj.GetId()] = CreaturePtr(dynamic_cast<Creature *>(&obj));
		}
		else
		{
			players_[obj.GetGateId()][obj.GetId()] = PlayerPtr(dynamic_cast<Player *>(&obj));
		}
	}

	void ObjManager::AddObj(CreaturePtr creature)
	{
		AI_ASSERT(creature->IsPlayer() == false);
		if (!creature->IsPlayer())
			creatures_[creature->GetId()] = creature;
	}

	void ObjManager::AddObj(PlayerPtr player)
	{
		AI_ASSERT(player->IsPlayer() == true);
		if (player->IsPlayer())
			players_[player->GetGateId()][player->GetId()] = player;
	}

	void ObjManager::RemoveObj(ObjPtr obj)
	{
		if (obj->GetMPPid().wGID == 1000)
		{
			creatures_.erase(obj->GetId());
		}
		else
		{
			players_[obj->GetGateId()].erase(obj->GetId());
		}
	}

	void ObjManager::RemoveObj(CreaturePtr creature)
	{
		creatures_.erase(creature->GetId());
	}

	void ObjManager::RemoveObj(PlayerPtr player)
	{
		players_[player->GetGateId()].erase(player->GetId());
	}

	CreaturePtr ObjManager::GetCreatureObj(const unsigned id)
	{
		std::map<unsigned, CreaturePtr>::iterator it = creatures_.find(id);
		if (it != creatures_.end())
			return it->second;
		return CreaturePtr(0);
	}

	PlayerPtr ObjManager::GetPlayerObj(const MUX_PROTO::PlayerID & id)
	{
		AI_ASSERT(id.wGID != 1000);
		std::map<unsigned, PlayerPtr> & players = players_[id.wGID];
		std::map<unsigned, PlayerPtr>::iterator it = players.find(id.dwPID);
		if (it != players.end())
			return it->second;
		return PlayerPtr(0);
	}

	ObjPtr ObjManager::GetGameObj(const MUX_PROTO::PlayerID & id)
	{
		if (id.wGID == 1000)
			return GetCreatureObj(id.dwPID);
		else
			return GetPlayerObj(id);
	}

	void ObjManager::AddDiedList(CreaturePtr & creature)
	{
		AI_ASSERT(creature.get());
		if (creature.get())
			died_list_[creature->GetId()] = creature;
	}

	void ObjManager::HandleDiedList()
	{
		if (died_list_.empty()) 
			return;

		Creature* pTmpCreature = NULL;
		for(std::map<unsigned, CreaturePtr>::iterator it = died_list_.begin(); 
			it != died_list_.end(); ++it)
		{
			pTmpCreature = it->second.get();
			if ( NULL == pTmpCreature )
			{
				D_ERROR( "ObjManager::HandleDiedList, NULL == pTmpCreature\n" );
				return;
			}
			CMonsterWrapper::SetMonsterDie(pTmpCreature->GetMapInfo(), pTmpCreature->GetId(), pTmpCreature->GetDiedReason());
			//CMonsterWrapper::SetMonsterDie(it->second->GetMapInfo(), it->second->GetId(), false);
		}
		died_list_.clear();
	}

	bool ObjManager::IsFirstCreature(CreaturePtr & creature)const
	{
		if (creatures_.empty() != false && creature.get() == creatures_.begin()->second.get())
			return true;
		return false;
	}

	void ObjManager::ClearAllObjs()
	{		
		do {
			Creature* pTmpCreature = NULL;
			for(std::map<unsigned, CreaturePtr>::iterator it = creatures_.begin();
				it != creatures_.end(); ++it)
			{
				pTmpCreature = it->second.get();
				if ( NULL == pTmpCreature )
				{
					D_ERROR( "ObjManager::ClearAllObjs, NULL == pTmpCreature\n" );
					return;
				}
				pTmpCreature->Reset();
				//it->second->Reset();
			}
		}while(false);

		do {
			for(std::map<unsigned, std::map<unsigned, PlayerPtr> >::iterator it = players_.begin();
				it != players_.end(); ++it)
			{
				Player* pTmpPlayer = NULL;
				for(std::map<unsigned, PlayerPtr>::iterator pit = it->second.begin();
					pit != it->second.end(); ++pit)
				{
					pTmpPlayer = pit->second.get();
					if ( NULL == pTmpPlayer )
					{
						D_ERROR( "ObjManager::ClearAllObjs, NULL == pTmpPlayer\n" );
						return;
					}
					pTmpPlayer->Reset();
					//pit->second->Reset();
				}
			}
		}while(false);

		creatures_.clear();
		players_.clear();
		died_list_.clear();
	}
}