﻿#pragma once

#ifndef __MUX_LOGICAI_POSITION_H__ 
#define __MUX_LOGICAI_POSITION_H__

#include "../monster/monsterwrapper.h"

namespace logicai
{
	struct position
	{
		float x, y;

		position() : x(0), y(0) { }

		MUX_PROTO::TYPE_POS GetX()const 
		{ 
			MUX_PROTO::TYPE_POS tmp = (MUX_PROTO::TYPE_POS)x;
			if (x - tmp >= 0.5f)
				if (++tmp > 511)
					return 511;
			return tmp;
		}

		MUX_PROTO::TYPE_POS GetY()const
		{
			MUX_PROTO::TYPE_POS tmp = (MUX_PROTO::TYPE_POS)y;
			if (y - tmp >= 0.5f)
				if (++tmp > 511)
					return 511;
			
			return tmp;
		}

		const bool operator==(const position & r)const
		{
			//return (TYPE_POS)x == (TYPE_POS)r.x && (TYPE_POS)y == (TYPE_POS)r.y;
			return GetX() == r.GetX() && GetY() == r.GetY();
		}

		const bool operator!=(const position & r)const
		{
			return !(*this == r);
		}
	}; //

} //
#endif
