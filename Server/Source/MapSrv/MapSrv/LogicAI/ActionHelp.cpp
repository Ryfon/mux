﻿#include "ActionHelp.h"
#include "Creature.h"

namespace logicai
{
	ActionHelp::ActionHelp(CreaturePtr & creature, CreaturePtr & target)
		: Action_base(creature), target_(target)
	{
		ObjPtr tmp = target;
		new(follow_.data) ActionFollow(creature_, tmp);
	}

	void ActionHelp::OnEnter()
	{
		follow_.Get().OnEnter();
	}

	void ActionHelp::OnLeave()
	{
		follow_.Get().OnLeave();
		Action_base::OnLeave();
	}

	void ActionHelp::OnPause()
	{
		follow_.Get().OnPause();
	}

	void ActionHelp::OnResume()
	{
		follow_.Get().OnResume();
	}

	void ActionHelp::OnStop()
	{
		follow_.Get().OnStop();
		Action_base::OnStop();
	}

	void ActionHelp::OnFaitBegin()
	{ 
		follow_.Get().OnFaitBegin();
	}

	void ActionHelp::OnFaitEnd()
	{ 
		follow_.Get().OnFaitEnd();
	}

	void ActionHelp::OnBoundBegin()
	{
		follow_.Get().OnBoundBegin();
	}

	void ActionHelp::OnBoundEnd() 
	{
		follow_.Get().OnBoundEnd();
	}

	void ActionHelp::OnLostObj(ObjPtr & obj)
	{
		if (target_.get() && obj == target_)
		{
			follow_.Get().OnLostObj(obj);

			target_.reset();
			this->Stop();
		}
	}

	void ActionHelp::OnUpdate(unsigned elapsed)
	{
	}

	void ActionHelp::OnRun(unsigned elapsed)
	{
		if (this->creature_->IsFighting() || this->creature_->IsBound())
			return;

		if (!target_.get())
		{
			this->Stop();
			return;
		}

		follow_.Get().OnRun(elapsed);

		if (follow_.Get().IsLostTarget())
		{
			target_ = 0;
			this->Stop();
		}

		float dx = this->creature_->GetCurPos().x - target_->GetCurPos().x;
		float dy = this->creature_->GetCurPos().y - target_->GetCurPos().y;
		if (dx * dx + dy * dy <= 3.0)
		{
			for(size_t i = 0; i < this->creature_->GetThreatList().Size(); ++i)
			{
				target_->AddThreat(this->creature_->GetThreatList().Get(i).get(), 0, false);
			}

			if (!target_->ThreatListEmpty())
				target_->Fight(0);

			this->Stop();
		}
	}

	void ActionHelp::HandleSpeedDown(int val)
	{
		follow_.Get().HandleSpeedDown(val);
	}
}