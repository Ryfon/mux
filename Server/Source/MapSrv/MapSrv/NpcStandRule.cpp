﻿#include "NpcStandRule.h"
#include "Player/Player.h"
#include "monster/monster.h"


bool CNpcStandRule::OnTalkingToNpc( CPlayer* pPlayer, CMonster* pMonster )
{
	if( NULL == pPlayer || NULL == pMonster )
		return false;

	//怪物不可交互
	if( pMonster->GetRaceAttribute() == NPC_RACE_MONSTER )
		return false;

	//不同种族的NPC并且不是中立的NPC不能进行交互
	if( pPlayer->GetRace() != pMonster->GetRaceAttribute() && pMonster->GetRaceAttribute() != NPC_RACE_NEUTRAL )
		return false;

	//中立的都可以进行交互
	if( pMonster->GetRaceAttribute() == NPC_RACE_NEUTRAL )
		return true;

	//当玩家为恶时
	if( pPlayer->GetGoodEvilPoint() < 0 || pPlayer->IsRogueState() )
	{
		if( pMonster->GetInherentStand() == 1 )		//光明NPC不可以交互
			return false;
		if( pMonster->GetInherentStand() == 0 )		//邪恶NPC可以交互
			return true;
	}

	return true;		//善的玩家均可交互
}


bool CNpcStandRule::OnAttackNpc( CPlayer* pAttackPlayer, CMonster* pTargetMonster )
{
	if( NULL == pAttackPlayer || NULL == pTargetMonster )
		return false;

	//如果该NPC是不可被攻击的则任何情况下都不能被攻击
	if( !pTargetMonster->IsCanBeAttacked() )
		return false;

	//如果是全领模式的话，任何非CANBEATTAKCED属性NPC都能打
	if( pAttackPlayer->GetBattleMode() == FULL_FIELD_MODE )
		return true;
	
	//如果NPC是同种族的,任何情况下都不能攻击
	if(  pTargetMonster->GetRaceAttribute() == pAttackPlayer->GetRace() )
	{
		//是恶玩家的话
		if( pAttackPlayer->GetGoodEvilPoint() < 0 || pAttackPlayer->IsRogueState() )
		{
			if( pTargetMonster->GetInherentStand() == 1 )
				return true;
		}
		return false;
	}

	//中立的.善玩家不能打,恶玩家看GetInherentStand
	if( pTargetMonster->GetRaceAttribute() == NPC_RACE_NEUTRAL )
	{
		//是恶玩家的话
		if( pAttackPlayer->GetGoodEvilPoint() < 0 || pAttackPlayer->IsRogueState() )
		{
			return true;
		}

		//善玩家都不能打
		return false;
	}

	//剩下的就是不同种族的NPC和怪物了 都能打...
	return true;	
}


//有益的技能
bool CNpcStandRule::OnUseAssistSkillToNpc( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster )
{
	if( NULL == pUseSkillPlayer || NULL == pTargetMonster )
		return false;

	//只有善玩家可以对 中立的怪物和本族的NPC使用,其他均不可
	if( pUseSkillPlayer->GetGoodEvilPoint() >= 0 && !pUseSkillPlayer->IsRogueState() )
	{
		if( pTargetMonster->GetRaceAttribute() == pUseSkillPlayer->GetRace() || pTargetMonster->GetRaceAttribute() == NPC_RACE_NEUTRAL )
		{
			return true;
		}
	}

	//其余情况均不可
	return false;
}
