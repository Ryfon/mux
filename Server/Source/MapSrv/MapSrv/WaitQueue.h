﻿

#ifndef WAITQUEUE_H
#define WAITQUEUE_H

#include <list>
#include "Utility.h"
#include "PkgProc/CliProtocol_T.h"
using namespace MUX_PROTO;
using namespace std;

class CWaitQueue
{
private:
	CWaitQueue(void);
	~CWaitQueue(void);

public:
	///是否需要排队；
	static bool IfNeedWait( const PlayerID& playerID )
	{
		if ( m_waitPlayerList.empty() )
		{
			return false;//无需排队;
		}

		AddOneWaitPlayer( playerID );
		return true;
	}

public:
	///添加一个排队者；
	static void AddOneWaitPlayer( const PlayerID& playerID );

public:
	///有人离开mapsrv，排队有空可进入;
	static void OnSomeOneLeave();

	static void WaitQueTimeProcess();			//定时调用方法

	static void UpdateQueueIndex();		//向队列中的等待成员发送新的等待序列

	static bool RemoveWaitPlayer( const PlayerID& quitPlayerID );		//退出的玩家ID 

	static void RemoveWaitPlayerOnGateSrv( unsigned int gateSrvId );	//根据GID 删除对应的playerID 

private:
	static list<PlayerID> m_waitPlayerList;		//玩家等待队列
};


#endif //WAITQUEUE_H
