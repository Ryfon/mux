﻿#ifndef TRADE_INFO_H
#define TRADE_INFO_H

#include ".././Base/PkgProc/CliProtocol.h"
#include <map>

class CTradeInfoManager;
class CTradeInfo
{
	friend class CTradeInfoManager;
public:
	// 交易最大数目
	enum
	{
		MAX_TRADE_NUM = 30,
		INVALAD_INDEX = -1
	};

	/// 交易方索引
	enum eTradePlayerIndex
	{
		INVITE_PLAYER = 0,//邀请方
		TARGET_PLAYER = 1 //受邀方
	};

	/// 交易方数据
	typedef struct StrTradeData
	{
		enum eState
		{
			TRADE_INIT = 0,//初始态
			TRADE_LOCK = 1,//锁定态
			TRADE_CONFIRM = 2//确认态
		};

		eState state;	//交易状态
		int iItemIndexs[MAX_TRADE_NUM];//交易物品列表(每个物品在对应包裹中的索引)
		unsigned int uiMoney;

		unsigned short usCount;//交易数量
	}TradeData;

private:
	/// 构造函数
	CTradeInfo(void);

	/// 构造函数
	CTradeInfo(PlayerID invitePlayerID, PlayerID targetPlayerID);

	/// 析构函数
	~CTradeInfo(void);
public:
	/// 设置/获取邀请方ID
	void InvitePlayerID(PlayerID &uiPlayerID);
	PlayerID InvitePlayerID(void);

	/// 设置/获取受邀方ID
	void TargetPlayerID(PlayerID &uiPlayerID);
	PlayerID TargetPlayerID(void);

	/// 设置/获取状态
	void State(eTradePlayerIndex index, TradeData::eState newState);
	TradeData::eState State(eTradePlayerIndex index);

	/// 增加/删除物品
	bool AddItem(eTradePlayerIndex index, int iItemIndex);
	void DeleteItem(eTradePlayerIndex index, int iItemIndex);

	/// 获取物品信息
	void TradeItemInfo(eTradePlayerIndex index, int *&pItemIndexs, unsigned short &usCount);

	/// 设置/获取金钱
	void Money(eTradePlayerIndex index, unsigned int uiMoney);
	unsigned int Money(eTradePlayerIndex index);

	/// 全部确认?
	bool BothConfirmTrade(void);

private:
	/// 邀请方
	PlayerID m_invitePlayerID;
	
	/// 受邀方
	PlayerID m_targetPlayerID;

	/// 交易双方数据
	TradeData m_data[2];
};

class CTradeInfoManager
{
public:
	static CTradeInfo * GetTradeInfo(unsigned int tradeID);
	static CTradeInfo * NewTradeInfo(unsigned int& outtradeID);
	static CTradeInfo * NewTradeInfo(PlayerID invitePlayerID, PlayerID targetPlayerID,unsigned int& outtradeID);
	static void	DelTradeInfo(unsigned int tradeID);
private:
	static std::map<unsigned int,CTradeInfo*> m_tradeInfoMap;
	static unsigned int	m_count;
private:
	CTradeInfoManager();
	~CTradeInfoManager();
	CTradeInfoManager( const CTradeInfoManager& ); 
	CTradeInfoManager& operator = ( const CTradeInfoManager& );
};


#endif/*TRADE_INFO_H*/
