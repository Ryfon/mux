﻿#include "PlayerBuffManager.h"
#include "Player/Player.h"

CPlayerBuffManager::CPlayerBuffManager():m_pOwner( NULL ), m_buffNum( 0 ),m_buffPosFlag(0)
{
//错误地重置，因为数组成员包含了ACE_Time_Value成员，该成员不能memset, StructMemSet(m_pPlayerBuffs,0,sizeof(m_pPlayerBuffs));
	m_buffAffect.Reset();
}

CPlayerBuffManager::~CPlayerBuffManager(void)
{
}


void CPlayerBuffManager::AttachOwner( CPlayer* pPlayer )	//设置宿主
{
	m_pOwner = pPlayer;
}


bool CPlayerBuffManager::IsHaveControlBuff()
{
	return ( m_buffAffect.m_bFaint || m_buffAffect.m_bBondage || m_buffAffect.m_bSpeedDown );
}


void CPlayerBuffManager::SetSelfExplosionInfo( bool bFlag, unsigned int arg )
{
	m_buffAffect.m_bSelfExplosion = bFlag;
	m_buffAffect.selfExplosionArg = arg;
}

unsigned int CPlayerBuffManager::GetSelfExplosionArg()
{
	return m_buffAffect.selfExplosionArg;
}


bool CPlayerBuffManager::IsSelfExplosion()
{
	return m_buffAffect.m_bSelfExplosion;
}


void CPlayerBuffManager::SetSpeedDown( bool bFlag )
{
	m_buffAffect.m_bSpeedDown = bFlag;
}


void CPlayerBuffManager::SetSpeedUp( bool bFlag )
{
	m_buffAffect.m_bSpeedUp = bFlag;
}


bool CPlayerBuffManager::IsSpeedUp()
{
	return m_buffAffect.m_bSpeedUp;
}


bool CPlayerBuffManager::IsSpeedDown()
{
	return m_buffAffect.m_bSpeedDown;
}


bool CPlayerBuffManager::IsSpPowerSpeedUp()
{
	return m_buffAffect.m_bSpPowerSpeedUp;
}


void CPlayerBuffManager::SetSpPowerSpeedUp( bool bFlag )
{
	m_buffAffect.m_bSpPowerSpeedUp = bFlag;
}


bool CPlayerBuffManager::IsMoveImmunity()
{
	return m_buffAffect.m_bMoveImmunity;
}


void CPlayerBuffManager::SetMoveImmunity( bool bFlag )
{
	m_buffAffect.m_bMoveImmunity = bFlag;
}


void CPlayerBuffManager::SetFaint( bool bFlag )		//设置晕旋
{
	m_buffAffect.m_bFaint = bFlag;
}


bool CPlayerBuffManager::IsFaint()
{
	return m_buffAffect.m_bFaint;
}


bool CPlayerBuffManager::IsBondage()	//是否束缚
{
	return m_buffAffect.m_bBondage;
}


void CPlayerBuffManager::SetBondage( bool bFlag )
{
	m_buffAffect.m_bBondage = bFlag;
}


void CPlayerBuffManager::SetInvicible( bool bFlag )
{
	m_buffAffect.m_bInvicible = bFlag;
}


bool CPlayerBuffManager::IsInvicible()		//是否无敌
{
	return	m_buffAffect.m_bInvicible; 		
}


void CPlayerBuffManager::SetAbsorbDamage( unsigned int damage )	//设置吸收伤害量
{
	if( damage == 0 )
	{
		if( IsInAbsorbState() )
		{
			EndAbsorbBuff();	
		}
	}else
	{
		m_buffAffect.m_nAbsorbDamage = damage;
	}
}


bool CPlayerBuffManager::IsHaveBuff()
{
	return (m_buffNum != 0);
}


bool CPlayerBuffManager::AbsorbCheck( unsigned int& damage )
{
	if( damage > m_buffAffect.m_nAbsorbDamage )
	{
		damage -= m_buffAffect.m_nAbsorbDamage;
		EndAbsorbBuff();
		return false;
	}else{
		m_buffAffect.m_nAbsorbDamage -= damage;
		damage = 0;
		return true;
	}
}


bool CPlayerBuffManager::IsInAbsorbState()
{
	return (m_buffAffect.m_nAbsorbDamage != 0);
}


//是否能上骑乘
bool CPlayerBuffManager::IsCanOnMount()
{
	if( NULL == m_pOwner)
		return false;

	if( m_buffAffect.m_bFaint || m_buffAffect.m_bBondage || m_buffAffect.m_bInvicible || m_pOwner->IsDying() )
		return false;
	return true;
}



void CPlayerBuffManager::OnBuffDie()
{
	if( !IsHaveBuff() || NULL == m_pOwner )
	{
		return;	
	}


	for( unsigned int i=0; i<ARRAY_SIZE(m_pPlayerBuffs); ++i )
	{
		if( !IsPosValid(i) )
		{
			continue;
		}

		if( m_pPlayerBuffs[i].IsDeathClear() )
		{
			PlayerBuffEndWrapper( &m_pPlayerBuffs[i], i );
		}
	}
	
	m_pOwner->UpdateBuffData( 0 );
	//m_pOwner->SetNeedNotiTeamMember();
}


bool CPlayerBuffManager::CreateMuxBuff( MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID )
{
	if( NULL == pBuffProp || NULL == m_pOwner )
	{
		return false;
	}

	bool isFindSame = false;

	if( IsHaveBuff() )
	{
		//0为不看规则的顶推 只看自己
		if( pBuffProp->PushIndex == 0 )
		{
			for( unsigned int i=0; i < ARRAY_SIZE(m_pPlayerBuffs); ++i )
			{
				if( !IsPosValid(i) )
					continue;

				if( m_pPlayerBuffs[i].GetCreateID() == createID && m_pPlayerBuffs[i].GetBuffID() == pBuffProp->mID ) 	
				{
					isFindSame = true;
					//  [1/27/2010 shenchenkai]
					//跳地图时buff会两次初始化（为什么这么做原因不明），第二次初始化就会把前一次的buff顶掉，时间重走，这里做个保护
					if ( pBuffProp->nPeriodOfTime > 0 && buffTime < pBuffProp->nPeriodOfTime )
					{
						return false;
					}

					//说明是可叠加的buff
					unsigned int buffLevel = m_pPlayerBuffs[i].GetBuffLevel();
					if( pBuffProp->TierNum != 0 && buffLevel < pBuffProp->TierNum )
					{
						m_pPlayerBuffs[i].OnPlayerBuffChangeStatus( m_pOwner, i, pBuffProp, buffLevel + 1, true, createID );
						break;
					}else{
						m_pPlayerBuffs[i].OnPlayerBuffChangeStatus( m_pOwner, i, pBuffProp, buffLevel, false, createID );
						break;
					}
				}
			}
		}else{
			//找相同pushindex的
			for( unsigned int i=0; i < ARRAY_SIZE(m_pPlayerBuffs); ++i )
			{
				if( !IsPosValid(i) )
					continue;

				if( m_pPlayerBuffs[i].GetPushIndex() == pBuffProp->PushIndex )
				{
					isFindSame = true;

					if( m_pPlayerBuffs[i].GetBuffID() == pBuffProp->mID )
					{
						//  [1/27/2010 shenchenkai]
						if ( pBuffProp->nPeriodOfTime > 0 && buffTime < pBuffProp->nPeriodOfTime )
						{
							return false;
						}

						//替换升级
						unsigned int buffLevel = m_pPlayerBuffs[i].GetBuffLevel();
						if( pBuffProp->TierNum != 0 && buffLevel < pBuffProp->TierNum )
						{
							m_pPlayerBuffs[i].OnPlayerBuffChangeStatus( m_pOwner, i, pBuffProp, buffLevel + 1, true, createID );
							break;
						}else
						{
							m_pPlayerBuffs[i].OnPlayerBuffChangeStatus( m_pOwner, i, pBuffProp, buffLevel, false, createID );
							break;
						}
					}else
					{
						//对值或者后顶前进行比较
						if( pBuffProp->PushRule == 0 )
						{
							m_pPlayerBuffs[i].OnPlayerBuffChangeStatus( m_pOwner, i, pBuffProp, 1, false, createID );
							break;
						}else if( pBuffProp->PushRule == 1 )
						{
							if( m_pPlayerBuffs[i].GetBuffValue() <= pBuffProp->Value )
							{
								m_pPlayerBuffs[i].OnPlayerBuffChangeStatus( m_pOwner, i, pBuffProp, 1, false, createID );
								break;
							}else
							{
								return false;  //顶推失败
							}
						}
					}
				}
			}
		}
	}//is have buff

	if( !isFindSame )
	{
		unsigned int index = 0;
		if( FindInvalidIndex( index ) )
		{
			PlayerBuffStartWrapper( &m_pPlayerBuffs[index], index, pBuffProp, buffTime, createID );
		}else{
			D_WARNING("所有栏位已满 此buff将不产生效果\n");
		}
	}
	return true;
}


bool CPlayerBuffManager::DeleteMuxBuff( unsigned int buffID )		//结束一种类型的buff
{
	bool isFind = false;
	for( unsigned int i = 0; i < ARRAY_SIZE( m_pPlayerBuffs ); ++i )
	{
		if( IsPosValid(i) )
		{
			if( m_pPlayerBuffs[i].GetBuffID() == buffID )
			{
				isFind = true;
				PlayerBuffEndWrapper( &m_pPlayerBuffs[i], i );
			}
		}
	}
	return isFind;
}


void CPlayerBuffManager::BuffProcess()
{
	//如果没有任何buff 直接返回
	if( !IsHaveBuff() )
	{
		return;
	}

	for( unsigned int i = 0; i < ARRAY_SIZE( m_pPlayerBuffs ); ++i )
	{
		if( IsPosValid(i) )
		{
			if( !m_pPlayerBuffs[i].OnPlayerBuffUpdate( m_pOwner, i ) )
			{
				PlayerBuffEndWrapper( &m_pPlayerBuffs[i], i );
			}
		}
	}

	if ( NULL != m_pOwner )
	{
		m_pOwner->UpdateBuffData( 0 );
		//m_pOwner->SetNeedNotiTeamMember();
	}
}


void CPlayerBuffManager::UpdateBuffData( PlayerInfo_5_Buffers& data, unsigned int saveType  )
{
	if( !IsHaveBuff() || NULL == m_pOwner )
	{
		return;
	}

	unsigned int count = 0;
	unsigned int buffSize = ARRAY_SIZE(data.buffData);
	for( unsigned int j = 0; j < ARRAY_SIZE( m_pPlayerBuffs ); ++j )
	{
		if( count >= buffSize )
			break;

		if( IsPosValid(j) )
		{
			if( saveType == 1 )
			{
				if(  m_pPlayerBuffs[j].IsSaveDb() )
				{
					data.buffData[count].uiBuffID = m_pPlayerBuffs[j].GetBuffID();
					data.buffData[count].uiLeftDurSeconds = m_pPlayerBuffs[j].GetBuffTime();
					count++;
				}
			}else
			{
				data.buffData[count].uiBuffID = m_pPlayerBuffs[j].GetBuffID();
				data.buffData[count].uiLeftDurSeconds = m_pPlayerBuffs[j].GetBuffTime();
				count++;
			}
		}
	}
	data.buffSize = count;
}


bool CPlayerBuffManager::ActiveEndBuff( unsigned int buffIndex )
{
	if( NULL == m_pOwner )
		return false;

	if( IsPosValid( buffIndex) )
	{
		if( !m_pPlayerBuffs[buffIndex].IsRightClickClear() )
		{
			return false;
		}

		PlayerBuffEndWrapper(  &m_pPlayerBuffs[buffIndex], buffIndex );
	}

	m_pOwner->UpdateBuffData( 0 );
	//m_pOwner->SetNeedNotiTeamMember();

	//return false;
	return true;
}


bool CPlayerBuffManager::EndAbsorbBuff()
{
	return EndBuffByType( (unsigned int)DAMAGE_ABSORB );
}
 

//结束变羊debuff
bool CPlayerBuffManager::EndSheepDebuff()
{
	return EndBuffByType( (unsigned int)DEBUFF_SHEEP );
}

bool CPlayerBuffManager::PlayerBuffEndWrapper( CMuxBuffEle* pBuffEle, unsigned int buffIndex )
{
	if( NULL == pBuffEle )
		return false;

	SetPosInValid( buffIndex );
	SubBuffNum();
	pBuffEle->OnPlayerBuffEnd( m_pOwner, buffIndex );
	return true;
}


bool CPlayerBuffManager::PlayerBuffStartWrapper( CMuxBuffEle* pBuffEle, unsigned int buffIndex, MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID )
{
	if( NULL == pBuffEle )
		return false;

	if( pBuffEle->OnPlayerBuffStart( m_pOwner, buffIndex, pBuffProp, buffTime, createID ) )
	{
		SetPosValid( buffIndex );
		AddBuffNum();
		return true;
	}

	D_WARNING("OnPlayerBuffStart false\n");
	return false;
}


void CPlayerBuffManager::AddMuxBuffEffect( MuxBuffProp* pBuffProp )
{
	if( NULL == pBuffProp )
	{
		D_ERROR("CPlayer::AddMuxBuffEffect pBuffProp为NULL\n");
		return;
	}

	if( pBuffProp->AssistType == BUFF_SP_COMPLEX )
	{
		for( unsigned int i = 0; i < pBuffProp->vecBuffProp.size(); ++i )
		{
			AddSingleMuxBuffEffect( pBuffProp->vecBuffProp[i] );
		}
	}else
	{
		AddSingleMuxBuffEffect( pBuffProp );
	}
}


void CPlayerBuffManager::AddSingleMuxBuffEffect( MuxBuffProp* pBuffProp )
{
	if( NULL == pBuffProp )
	{
		D_ERROR( "CPlayer::AddSingleMuxBuffEffect pBuffProp为NULL\n" );
		return;
	}

	if ( NULL == m_pOwner )
	{
		D_ERROR( "CPlayerBuffManager::AddSingleMuxBuffEffect, NULL == m_pOwner\n" );
		return;
	}

	int nBuffVal = pBuffProp->Value;
	switch( pBuffProp->AssistType )
	{
	case BUFF_HP_HOT:                    //HP持续上升
		//m_hpmpUpdateInfo.hpUpdateVal += nBuffVal;
		break;
	
	case BUFF_PHY_ATTACK_UP:           	//物理攻击的提升
		m_buffAffect.nPhyAttack += nBuffVal;
		break;
	
	case BUFF_MAG_ATTACK_UP:            	//魔法攻击的提升
		m_buffAffect.nMagAttack += nBuffVal;
		break;
	
	case BUFF_PHY_DEFEND_UP:          	//物理防御提升
		m_buffAffect.nPhyDefend += nBuffVal;
		break;
	
	case BUFF_MAG_DEFNED_UP:        	//魔法防御提升
		m_buffAffect.nMagDefend += nBuffVal;
		break;
		
	case BUFF_PHY_HIT_UP:          	//物理命中提升
		m_buffAffect.nPhyHitLevel += nBuffVal;
		break;

	case BUFF_MAG_HIT_UP:           	//魔法命中提升
		m_buffAffect.nMagHitLevel += nBuffVal;
		break;

	case BUFF_PHY_CRI_UP:            	//物理暴击
		m_buffAffect.nPhyCriLevel += nBuffVal;
		break;

	case BUFF_MAG_CRI_UP:            	//魔法暴击等级上升
		m_buffAffect.nMagCriLevel += nBuffVal;
		break;

	case BUFF_ADD_FAINT_RATE:         	//增加暴几率，怪物用
		break;

	case BUFF_ADD_FAINT_TIME:          //增加晕的时间, 怪物用
		break;

	case DEBUFF_SPEED_DOWN:          	//速度下降
		{
			//m_buffAffect.fSpeedAffect -= nBuffVal / (float)100;
			m_buffAffect.nSpeedAffect.push_back( -nBuffVal );
			SetSpeedDown( true );
			//还需要给客户端发送减速消息
			m_pOwner->NotifySpeedChange();
		}
		break;

	case DEBUFF_FAINT:           	//控制类的DEBUFF,晕旋
		{
			SetFaint( true );
			if ( m_pOwner->GetCastManager().IsInCast() )
			{
				m_pOwner->GetCastManager().OnStop();
			}
			if ( m_pOwner->IsMoving() )
			{
				m_pOwner->ResetMoveStat();
			}
			D_DEBUG("玩家%s 开始晕旋\n", m_pOwner->GetNickName() );
		}
		break;

	case DEBUFF_BONDAGE:         	//控制类的DEBUFF,束缚
		{
			SetBondage( true );
			if ( m_pOwner->GetCastManager().IsInCast() )
			{
				m_pOwner->GetCastManager().OnStop();
			}
			if ( m_pOwner->IsMoving() )
			{
				m_pOwner->ResetMoveStat();
			}
		}
		break;

	case BUFF_HP_DOT:          	//HP持续减少
		//m_hpmpUpdateInfo.hpUpdateVal -= nBuffVal;
		break;

	case DEBUFF_PHY_ATTACK_DOWN:          	//物理攻击下降
		m_buffAffect.nPhyAttack -= nBuffVal;
		break;

	case DEBUFF_MAG_ATTACK_DOWN:         	//魔法攻击下降
		m_buffAffect.nMagAttack -= nBuffVal;
		break;

	case DEBUFF_PHY_DEFEND_DOWN:        	//物理防御下降
		m_buffAffect.nPhyDefend -= nBuffVal;
		break;

	case DEBUFF_MAG_DEFEND_DOWN:     	//魔法防御下降
		m_buffAffect.nMagDefend -= nBuffVal;
		break;

	//case BUFF_STR:            	//增加STR
	//	m_buffAffect.nStr += nBuffVal;
	//	break;

	//case BUFF_AGI:           	//增加AGI
	//	m_buffAffect.nAgi += nBuffVal;
	//	break;

	//case BUFF_INT:           	//增加INT
	//	m_buffAffect.nInt += nBuffVal;
	//	break;
	
	case BUFF_MAXHP_UP:       //max hp上
		m_buffAffect.nMaxHpUp += nBuffVal;
		m_pOwner->AddMaxHp( nBuffVal );
		break;

	case BUFF_ADD_PHYCRI_DAMAGE:         	//提高物理暴击追加伤害
		m_buffAffect.nPhyCriDamAddition += nBuffVal;
		break;

	case BUFF_ADD_MAGCRI_DAMAGE:         	//提高魔法暴击追加伤害
		m_buffAffect.nMagCriDmgAddition += nBuffVal;
		break;

	case BUFF_RATE_PHYDAMAGE_UP:        	//提高物理伤害的百分比,float型
		m_buffAffect.nAddPhyDamageRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGDAMAGE_UP:      	//提高魔法伤害的百分比
		m_buffAffect.nAddMagDamageRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_RESIST_FAINT:          	//提高抗击晕的百分比
		m_buffAffect.fResistFaintRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_RESIST_BONDAGE:         	//提高抗束缚的百分比
		m_buffAffect.fResistBondageRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_ADD_FAINT:      	//提高击晕的百分比
		m_buffAffect.fAddFaintRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_ADD_BONDAGE:         	//提高击晕的百分比
		m_buffAffect.fAddBondageRate += nBuffVal / (float)100;
		break;

	case BUFF_ADD_AOE_RANGE:        	//提高范围攻击的作用范围
		m_buffAffect.nAddAoeRange += nBuffVal;
		break;

	case BUFF_ADD_ATK_DISTANCE:         	//提高攻击技能的施放距离
		m_buffAffect.nAddAtkDistance += nBuffVal;
		break;

	case BUFF_ADD_DOT_DAMAGE:          	//提高每次DOT技能的伤害值
		m_buffAffect.fDotDmgUp += nBuffVal / (float)100;
		break;

	case BUFF_DECREASE_CD:      	//减少每个技能的COOL DOWN时间
		m_buffAffect.nDecreaseCdTime += nBuffVal;
		break;

	case DEBUFF_SPECIAL_DOT:       	//DOT的每跳的伤害需要和人物的属性相,这里只是个基础值???
		break;

	case BUFF_RATE_PHYCRI_HIT_UP:          	//提高物理暴击百分比
		m_buffAffect.fAddPhyCriRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGCRI_HIT_UP:        	//提高魔法暴击百分比
		m_buffAffect.fAddMagCriRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_PHYHIT_UP:          	//提高物理攻击时的命中百分比
		m_buffAffect.fAddPhyHitRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGHIT_UP:			//40提高魔法攻击时的命中百分比
		m_buffAffect.fAddMagHitRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_PHYCRI_DAMAGE_UP:         	//提高物理暴击攻击的伤害比率
		m_buffAffect.fAddPhyCriDmgRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGCRI_DAMAGE_UP:          	//提高魔法暴击攻击的伤害比率
		m_buffAffect.fAddMagCriDmgRate += nBuffVal / (float)100;
		break;

	case BUFF_PHYJOUK_LEVEL:        	//提高物理攻击闪避等级
		m_buffAffect.fPhyJoukRate += nBuffVal / (float)100;
		break;

	case BUFF_MAGJOUK_LEVEL:          	//提高魔法攻击闪避等级
		m_buffAffect.fMagJoukRate += nBuffVal / (float)100;
		break;

	case BUFF_PERCENT_HOT:
		//m_hpmpUpdateInfo.perHp += nBuffVal/100.f; 
		break;

	case DAMAGE_ABSORB:
		SetAbsorbDamage( nBuffVal );
		break;

	case MOVE_IMMUNITY:
		SetMoveImmunity( true );
		m_pOwner->EndControlBuff();
		break;

	case ALL_IMMUNITY:
		SetInvicible( true );
		m_pOwner->EndAllDebuff();	//无敌状态开启时 所有debuff都会消除
		break;

	case SP_SPEEDUP:
		SetSpPowerSpeedUp( true );
		m_buffAffect.fSpPowerUpRate += nBuffVal / 100.f;
		break;

	case SELF_EXPLOSION:
		SetSelfExplosionInfo( true, /*nBuffVal*/pBuffProp->mID );
		break;

	case BUFF_SPEED_UP:
		SetSpeedUp( true );
		//m_buffAffect.fSpeedAffect += nBuffVal / (float)100;
		m_buffAffect.nSpeedAffect.push_back( nBuffVal );
		m_pOwner->NotifySpeedChange();
		break;

	case MAG_HOT:
		//m_hpmpUpdateInfo.mpUpdateVal += nBuffVal;
		break;

	case MAG_PER_HOT:
		//m_hpmpUpdateInfo.perMp += nBuffVal/100.f;
		break;

	case BASE_HP_UP:       	//按基础值的百分比提升HP
		m_buffAffect.fBaseHpUp += nBuffVal/100.f;
		break;

	case DEBUFF_SHEEP:         		//变羊
		m_buffAffect.isSheep = true;
		break;

	case DEBUFF_SLEEP:       	//睡眠
		m_buffAffect.isSleep = true;
		break;

	case CLIENT_BUFF:         	//客户端buff,无任何作用
		break;

	case X_EXP:         	//X倍EXP
		m_buffAffect.timesExp = nBuffVal;
		break;

	case X_SKILL_EXP:         	//X倍技能熟练度
		m_buffAffect.timesPhyMagExp = nBuffVal; 
		break;

	case TIMER_BUFF:        	//定时放新的buff
		break;

	case DAMAGE_REBOUND:
		SetDamageReboundInfo( true, pBuffProp->mID );
		break;

	default:
		D_ERROR("无效的BUFF类型:%d\n", pBuffProp->AssistType );
		break;
	}
	TRY_END;
}


void CPlayerBuffManager::DelMuxBuffEffect( MuxBuffProp* pBuffProp, unsigned int buffLevel )
{
	if ( NULL == pBuffProp )
	{
		D_ERROR( "CPlayer::DelMuxBuffEffect，输入pBuffProp空\n" );
		return;
	}

	if( pBuffProp->AssistType == BUFF_SP_COMPLEX )
	{
		for( unsigned int i =0; i<pBuffProp->vecBuffProp.size(); ++i )
		{
			DelSingleMuxBuffEfect( pBuffProp->vecBuffProp[i], buffLevel );
		}
	}else{
		DelSingleMuxBuffEfect( pBuffProp, buffLevel );
	}
}

void CPlayerBuffManager::DelSingleMuxBuffEfect( MuxBuffProp* pBuffProp, unsigned int buffLevel )
{
	if ( NULL == pBuffProp )
	{
		D_ERROR( "CPlayer::DelSingleMuxBuffEfect，输入buff空\n" );
		return;
	}
	
	int nBuffVal = pBuffProp->Value * buffLevel;
	switch( pBuffProp->AssistType )
	{
	case BUFF_HP_HOT:                    //HP持续上升
		break;
	
	case BUFF_PHY_ATTACK_UP:           	//物理攻击的提升
		m_buffAffect.nPhyAttack -= nBuffVal;
		break;
	
	case BUFF_MAG_ATTACK_UP:            	//魔法攻击的提升
		m_buffAffect.nMagAttack -= nBuffVal;
		break;
	
	case BUFF_PHY_DEFEND_UP:          	//物理防御提升
		m_buffAffect.nPhyDefend -= nBuffVal;
		break;
	
	case BUFF_MAG_DEFNED_UP:        	//魔法防御提升
		m_buffAffect.nMagDefend -= nBuffVal;
		break;
		
	case BUFF_PHY_HIT_UP:          	//物理命中提升
		m_buffAffect.nPhyHitLevel -= nBuffVal;
		break;

	case BUFF_MAG_HIT_UP:           	//魔法命中提升
		m_buffAffect.nMagHitLevel -= nBuffVal;
		break;

	case BUFF_PHY_CRI_UP:            	//物理暴击
		m_buffAffect.nPhyCriLevel -= nBuffVal;
		break;

	case BUFF_MAG_CRI_UP:            	//魔法暴击等级上升
		m_buffAffect.nPhyCriLevel -= nBuffVal;

		break;

	case BUFF_ADD_FAINT_RATE:         	//增加暴几率，怪物用
		break;

	case BUFF_ADD_FAINT_TIME:          //增加晕的时间, 怪物用
		break;

	case DEBUFF_SPEED_DOWN:          	//速度下降
		//m_buffAffect.fSpeedAffect += nBuffVal / (float)100;
		for (size_t i = 0; i<m_buffAffect.nSpeedAffect.size(); ++i)
		{
			if (m_buffAffect.nSpeedAffect[i] == -nBuffVal)
			{
				m_buffAffect.nSpeedAffect.erase(m_buffAffect.nSpeedAffect.begin() + i);
				break;
			}
		}
		SetSpeedDown( false );
		//还需要给客户端发送减速消息
		m_pOwner->NotifySpeedChange();
		break;

	case DEBUFF_FAINT:           	//控制类的DEBUFF,晕旋
		SetFaint( false );
		D_DEBUG("玩家%s 晕旋结束\n", m_pOwner->GetNickName() );
		break;

	case DEBUFF_BONDAGE:         	//控制类的DEBUFF,束缚
		SetBondage( false );
		break;

	case BUFF_HP_DOT:          	//HP持续减少
		break;

	case DEBUFF_PHY_ATTACK_DOWN:          	//物理攻击下降
		m_buffAffect.nPhyAttack += nBuffVal;
		break;

	case DEBUFF_MAG_ATTACK_DOWN:         	//魔法攻击下降
		m_buffAffect.nMagAttack += nBuffVal;
		break;

	case DEBUFF_PHY_DEFEND_DOWN:        	//物理防御下降
		m_buffAffect.nPhyDefend += nBuffVal;
		break;

	case DEBUFF_MAG_DEFEND_DOWN:     	//魔法防御下降
		m_buffAffect.nMagDefend += nBuffVal;
		break;

	//case BUFF_STR:            	//增加STR
	//	m_buffAffect.nStr -= nBuffVal;
	//	break;

	//case BUFF_AGI:           	//增加AGI
	//	m_buffAffect.nAgi -= nBuffVal;
	//	break;

	//case BUFF_INT:           	//增加INT
	//	m_buffAffect.nInt -= nBuffVal;
	//	break;

	case BUFF_MAXHP_UP:       //max hp上
		m_buffAffect.nMaxHpUp -= nBuffVal;
		m_pOwner->SubMaxHp( nBuffVal );
		break;

	case BUFF_ADD_PHYCRI_DAMAGE:         	//提高物理暴击追加伤害
		m_buffAffect.nPhyCriDamAddition -= nBuffVal;
		break;

	case BUFF_ADD_MAGCRI_DAMAGE:         	//提高魔法暴击追加伤害
		m_buffAffect.nMagCriDmgAddition -= nBuffVal;
		break;

	case BUFF_RATE_PHYDAMAGE_UP:        	//提高物理伤害的百分比,float型
		m_buffAffect.nAddPhyDamageRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGDAMAGE_UP:      	//提高魔法伤害的百分比
		m_buffAffect.nAddMagDamageRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_RESIST_FAINT:          	//提高抗击晕的百分比
		m_buffAffect.fResistFaintRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_RESIST_BONDAGE:         	//提高抗束缚的百分比
		m_buffAffect.fResistBondageRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_ADD_FAINT:      	//提高击晕的百分比
		m_buffAffect.fAddFaintRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_ADD_BONDAGE:         	//提高击晕的百分比
		m_buffAffect.fAddBondageRate -= nBuffVal / (float)100;
		break;

	case BUFF_ADD_AOE_RANGE:        	//提高范围攻击的作用范围
		m_buffAffect.nAddAoeRange -= nBuffVal;
		break;

	case BUFF_ADD_ATK_DISTANCE:         	//提高攻击技能的施放距离
		m_buffAffect.nAddAtkDistance -= nBuffVal;
		break;

	case BUFF_ADD_DOT_DAMAGE:          	//提高每次DOT技能的伤害值
		m_buffAffect.fDotDmgUp -= nBuffVal / (float)100;
		break;

	case BUFF_DECREASE_CD:      	//减少每个技能的COOL DOWN时间
		m_buffAffect.nDecreaseCdTime -= nBuffVal;
		break;

	case DEBUFF_SPECIAL_DOT:       	//DOT的每跳的伤害需要和人物的属性相,这里只是个基础值???
		break;

	case BUFF_RATE_PHYCRI_HIT_UP:          	//提高物理暴击百分比
		m_buffAffect.fAddPhyCriRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGCRI_HIT_UP:        	//提高魔法暴击百分比
		m_buffAffect.fAddMagCriRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_PHYHIT_UP:          	//提高物理攻击时的命中百分比
		m_buffAffect.fAddPhyHitRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGHIT_UP:			//40提高魔法攻击时的命中百分比
		m_buffAffect.fAddMagHitRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_PHYCRI_DAMAGE_UP:         	//提高物理暴击攻击的伤害比率
		m_buffAffect.fAddPhyCriDmgRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGCRI_DAMAGE_UP:          	//提高魔法暴击攻击的伤害比率
		m_buffAffect.fAddMagCriDmgRate -= nBuffVal / (float)100;
		break;

	case BUFF_PHYJOUK_LEVEL:        	//提高物理攻击闪避几率
		m_buffAffect.fPhyJoukRate -= nBuffVal / (float)100;
		break;

	case BUFF_MAGJOUK_LEVEL:          	//提高魔法攻击闪避几率
		m_buffAffect.fMagJoukRate -= nBuffVal / (float)100;
		break;

	case BUFF_PERCENT_HOT:
	    break;

	case DAMAGE_ABSORB:
		m_buffAffect.m_nAbsorbDamage = 0;
		break;

	case MOVE_IMMUNITY:
		SetMoveImmunity( false );
		break;

	case ALL_IMMUNITY:
		SetInvicible( false );
		break;

	case SP_SPEEDUP:
		SetSpPowerSpeedUp( false );
		m_buffAffect.fSpPowerUpRate -= nBuffVal / 100.f;
		break;

	case SELF_EXPLOSION:
		SetSelfExplosionInfo( false, 0 );
		break;

	case BUFF_SPEED_UP:
		SetSpeedDown( false );
		//m_buffAffect.fSpeedAffect -= nBuffVal / (float)100;
		for (size_t i = 0; i<m_buffAffect.nSpeedAffect.size(); ++i)
		{
			if (m_buffAffect.nSpeedAffect[i] == nBuffVal)
			{
				m_buffAffect.nSpeedAffect.erase(m_buffAffect.nSpeedAffect.begin() + i);
				break;
			}
		}
		m_pOwner->NotifySpeedChange();
		break;

	case MAG_HOT:
		break;

	case BASE_HP_UP:       	//按基础值的百分比提升HP
		m_buffAffect.fBaseHpUp -= nBuffVal/ 100.f;
		break;

	case DEBUFF_SHEEP:         		//变羊
		m_buffAffect.isSheep = false;
		break;

	case DEBUFF_SLEEP:       	//睡眠
		m_buffAffect.isSleep = false;
		break;

	case CLIENT_BUFF:         	//客户端buff,无任何作用
		break;

	case X_EXP:         	//X倍EXP
		m_buffAffect.timesExp = 1;
		break;

	case X_SKILL_EXP:         	//X倍技能熟练度
		m_buffAffect.timesPhyMagExp = 1; 
		break;

	case TIMER_BUFF:        	//定时放新的buff
		break;

	case DAMAGE_REBOUND:
		SetDamageReboundInfo( false, 0 );
		break;

	default:
		D_ERROR("无效的BUFF类型:%d\n", pBuffProp->AssistType);
		break;
	}
	TRY_END;
}


//获得属性
int CPlayerBuffManager::GetBuffAgi()
{
	return m_buffAffect.nAgi;
}

int CPlayerBuffManager::GetBuffAoeRange()
{
	return m_buffAffect.nAddAoeRange;
}	


int CPlayerBuffManager::GetBuffInt()
{
	return m_buffAffect.nInt;
}


int CPlayerBuffManager::GetBuffStr()
{
	return m_buffAffect.nStr;
}


int CPlayerBuffManager::GetBuffPhyAtk()
{
	return m_buffAffect.nPhyAttack;
}

int CPlayerBuffManager::GetBuffMagAtk()
{
	return m_buffAffect.nMagAttack;
}


int CPlayerBuffManager::GetBuffPhyDef()
{
	return m_buffAffect.nPhyDefend;
}

int CPlayerBuffManager::GetBuffMagDef()
{
	return m_buffAffect.nMagDefend;
}


int CPlayerBuffManager::GetBuffPhyHit()
{
	return m_buffAffect.nPhyHitLevel;
}

int CPlayerBuffManager::GetBuffMagHit()
{
	return m_buffAffect.nMagHitLevel;
}


int CPlayerBuffManager::GetBuffMaxHpUp()
{
	return m_buffAffect.nMaxHpUp;
}


float CPlayerBuffManager::GetBuffSpeedAffect()
{
	//return m_buffAffect.fSpeedAffect;
	int nMax = 0;
	int nMin = 0;
	for (size_t i = 0; i<m_buffAffect.nSpeedAffect.size(); ++i)
	{
		if (m_buffAffect.nSpeedAffect[i] > 0)
		{
			if (m_buffAffect.nSpeedAffect[i] > nMax)
			{
				nMax = m_buffAffect.nSpeedAffect[i];
			}
		}
		else
		{
			if (m_buffAffect.nSpeedAffect[i] < nMin)
			{
				nMin = m_buffAffect.nSpeedAffect[i];
			}
		}
	}
	return (nMax+nMin)/(float)100;
}


float CPlayerBuffManager::GetBuffSpPowerUpRate()
{
	return m_buffAffect.fSpPowerUpRate;
}


int CPlayerBuffManager::GetBuffPhyCriLevel()
{
	return m_buffAffect.nPhyCriLevel;
}


int CPlayerBuffManager::GetBuffMagCriLevel()
{
	return m_buffAffect.nMagCriLevel;
}


int CPlayerBuffManager::GetBuffPhyCriDamAdd()
{
	return m_buffAffect.nPhyCriDamAddition;
}

int CPlayerBuffManager::GetBuffMagCriDamAdd()
{
	return m_buffAffect.nMagCriDmgAddition;
}


float CPlayerBuffManager::GetBuffBaseHpUp()
{
	return m_buffAffect.fBaseHpUp;
}


unsigned int CPlayerBuffManager::GetDrArg()
{
	return m_buffAffect.DrArg;
}

//删除加速buff
bool CPlayerBuffManager::EndSpeedUpBuff()
{
	return EndBuffByType( (unsigned int)BUFF_SPEED_UP );
}


//删除控制buff
void CPlayerBuffManager::EndControlBuff()
{
	for( unsigned int i =0; i< ARRAY_SIZE(m_pPlayerBuffs); ++i )
	{
		if( !IsPosValid( i ) )
		{
			continue;
		}

		BUFF_TYPE bfType = (BUFF_TYPE)m_pPlayerBuffs[i].GetBuffType();
		if( bfType == DEBUFF_FAINT || bfType == DEBUFF_BONDAGE || bfType == DEBUFF_SHEEP || bfType == DEBUFF_SLEEP )
		{
			PlayerBuffEndWrapper( &m_pPlayerBuffs[i], i );
		}
	}
}


//删除所有debuff
void CPlayerBuffManager::EndAllDebuff()
{
	for( unsigned int i =0; i< ARRAY_SIZE(m_pPlayerBuffs); ++i )
	{
		if( !IsPosValid( i ) )
		{
			continue;
		}

		if( !m_pPlayerBuffs[i].IsGoodBuff() )
		{
			PlayerBuffEndWrapper( &m_pPlayerBuffs[i], i );
		}
	}
}


bool CPlayerBuffManager::EndWeakBuff()
{
	return EndBuffByID( WEAK_BUFF_ID );
}

bool CPlayerBuffManager::ActiveEndRestBuff()
{
	for( unsigned int i =0; i< ARRAY_SIZE(m_pPlayerBuffs); ++i )
	{
		if( !IsPosValid( i ) )
		{
			continue;
		}

		if( m_pPlayerBuffs[i].IsRestBuff() )
		{
			PlayerBuffEndWrapper( &m_pPlayerBuffs[i], i );
			m_pOwner->UpdateBuffData( 0 );
			//m_pOwner->SetNeedNotiTeamMember();
			return true;
		}
	}
	return false;
}


bool CPlayerBuffManager::RelieveBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve/*是否只是探测可能性*/ )
{
	if( !isRealRelieve )
	{
		for( unsigned int i =0; i< ARRAY_SIZE(m_pPlayerBuffs); ++i )
		{
			if( !IsPosValid( i ) )
			{
				continue;
			}

			if( m_pPlayerBuffs[i].GetRelieveType() == relieveType && m_pPlayerBuffs[i].GetRelieveLevel() <= relieveLevel )	
				return true;
		}
		return false;
	}else{
		unsigned int count = 0;
		bool isRelieve = false;
		for( unsigned int i =0; i< ARRAY_SIZE(m_pPlayerBuffs); ++i )
		{
			if( count == relieveNum )
				break;

			if( !IsPosValid( i ) )
			{
				continue;
			}

			if( m_pPlayerBuffs[i].GetRelieveType() == relieveType && m_pPlayerBuffs[i].GetRelieveLevel() <= relieveLevel )	
			{
				isRelieve = true;
				PlayerBuffEndWrapper( &m_pPlayerBuffs[i], i );
				count++;
			}
		}
	    return isRelieve;
	}
}

bool CPlayerBuffManager::EndBuffByType( unsigned int buffType )
{
	for( unsigned int i =0; i< ARRAY_SIZE(m_pPlayerBuffs); ++i )
	{
		if( !IsPosValid( i ) )
		{
			continue;
		}

		if( m_pPlayerBuffs[i].GetBuffType() == buffType )
		{
			PlayerBuffEndWrapper( &m_pPlayerBuffs[i], i );
			return true;
		}
	}
	return false;

}


bool CPlayerBuffManager::EndBuffByID( unsigned int buffID )
{
	for( unsigned int i =0; i< ARRAY_SIZE(m_pPlayerBuffs); ++i )
	{
		if( !IsPosValid( i ) )
		{
			continue;
		}

		if( m_pPlayerBuffs[i].GetBuffID() == buffID )
		{
			PlayerBuffEndWrapper( &m_pPlayerBuffs[i], i );
			return true;
		}
	}
	return false;
}


//设置伤害反弹属性
void CPlayerBuffManager::SetDamageReboundInfo( bool bFlag, unsigned int buffID )
{
	m_buffAffect.m_bDamageRebound = bFlag;
	m_buffAffect.DrArg = buffID;
}


void CPlayerBuffManager::GetSwitchMapBuffData( MGTempBuffData& bfData )
{
	if( !IsHaveBuff() || NULL == m_pOwner )
	{
		return;
	}

	unsigned int count = 0;
	unsigned int buffSize = ARRAY_SIZE(bfData.buffDataArr);
	for( unsigned int j = 0; j < ARRAY_SIZE( m_pPlayerBuffs ); ++j )
	{
		if( count >= buffSize )
			break;

		if( IsPosValid(j) )
		{
			bfData.buffDataArr[count].buffID = m_pPlayerBuffs[j].GetBuffID();
			bfData.buffDataArr[count].buffTime = m_pPlayerBuffs[j].GetBuffTime();
			count++;
		}
	}
	bfData.buffArrSize = count;
	bfData.lNotiPlayerID = m_pOwner->GetFullID();
}







