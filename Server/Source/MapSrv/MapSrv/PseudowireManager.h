﻿#ifndef PSEUDO_WIRE_MANAGER_H
#define PSEUDO_WIRE_MANAGER_H


#include "../../Base/aceall.h"
#include <map>
#include <vector>
#include "../../Base/PkgProc/SrvProtocol.h"

using namespace MUX_PROTO;


class CGateSrv;


class CPseudowireManager
{
	friend class ACE_Singleton<CPseudowireManager, ACE_Null_Mutex>;
public:
	CPseudowireManager(void);
	~CPseudowireManager(void);

public:
	//初始化伪线配置
	bool Initialize(void);

	//加载配置文件
	bool LoadPWConfig(void);

	//清除
	void Clear(void);

	//是否启用伪线机制
	bool IsPWEnable(void);	

	//报告所有伪线的简要信息
	void ReportAllPWInfo(CGateSrv* pOwner);

private:
	void ReportPseudoWireInfo_i(CGateSrv* pOwner);

	void ReportPseudoWireMapInfo_i(CGateSrv* pOwner);

	PseudowireArg_i* FindPWItem(unsigned short pwIdx);

	PseudowireMapArg_i* FindPWMapItem(unsigned short pwIdx, unsigned short mapid);

private:
	bool m_flag;//是否启用伪线？

	std::map<unsigned short, PseudowireArg_i*> m_pwArgs;//伪线参数
	std::map<unsigned int, PseudowireMapArg_i*> m_pwMapArgs;//伪线地图参数

};

typedef ACE_Singleton<CPseudowireManager, ACE_Null_Mutex> CPseudowireManagerSingle;


#endif/*PSEUDO_WIRE_MANAGER_H*/
