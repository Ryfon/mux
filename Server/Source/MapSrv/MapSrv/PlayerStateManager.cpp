﻿#include "PlayerStateManager.h"
#include "RideState.h"
#include <assert.h>
#include "../../Base/Utility.h"
#include "Player/Player.h"

bool CPlayerStateManager::IsDoubleRideItem()
{
	if( NULL == m_pPlayer )
	{
		D_ERROR( "CPlayerStateManager::IsDoubleRideItem, NULL == m_pPlayer\n" );
		return false;
	}

	const ItemInfo_i* pMountItem = m_pPlayer->GetValidDriveEquip();
	if ( NULL == pMountItem )
	{
		D_ERROR("CPlayerStateManager::IsDoubleRideItem, 玩家%s, 骑乘装备位置上无道具 \n", m_pPlayer->GetAccount() );
		return false;
	}

	//const PlayerInfo_2_Equips& equipInfo = m_pPlayer->GetFullPlayerInfo()->equipInfo;
	//unsigned int driveIndex = EQUIPTYPE_POS[E_DRIVE];
	//if ( driveIndex >= ARRAY_SIZE(equipInfo.equips) )
	//{
	//	D_ERROR( "CPlayerStateManager::IsDoubleRideItem, driveIndex(%d) >= ARRAY_SIZE(equipInfo.equips)\n", driveIndex );
	//	return false;
	//}

	////获取骑乘位置上的道具
	//const ItemInfo_i& itemInfo = equipInfo.equips[ driveIndex ];

	//if ( itemInfo.uiID == PlayerInfo_2_Equips::INVALIDATE )
	//{
	//	D_ERROR("骑乘位置上根本没有道具\n");
	//	return false;
	//}

	if ( 1 == pMountItem->usItemTypeID / 100000 % 10 )
	{
		return true;
	} else {
		return false;
	}

	return false;
}

//骑乘队长状态
void CPlayerStateManager::SetPlayerRideLeaderState()
{
	m_rideState.SetPlayerRideType( CRideState::E_LEADER );
	m_rideState.OnBegin();
}

//骑乘队员状态
void CPlayerStateManager::SetPlayerRideAssistState( RideTeam* pTeam )
{
	if ( NULL == pTeam )
	{
		D_ERROR( "CPlayerStateManager::SetPlayerRideAssistState, NULL == pTeam\n" );
		return;
	}

	m_rideState.SetPlayerRideType( CRideState::E_ASSIST );
	m_rideState.SetRideTeam( pTeam );
	m_rideState.OnBegin();
}

//玩家至于普通状态
void CPlayerStateManager::SetPlayerNormalState()
{
	m_normalState.OnBegin();
}

void CPlayerStateManager::SetPlayerRideStateDisappear()
{
	if ( !m_pPlayer )
		return;

	RideTeam* pTeam =  RideTeamManagerSingle::instance()->GetRideTeamByID( m_rideState.GetRideTeamID() );
	if ( !pTeam )
		return;
	
	//如果是队长
	if( pTeam->IsTeamLeader( m_pPlayer ) )
	{
		SetPlayerRideLeaderStateDisappear( pTeam );
	}
	else
	{
		SetPlayerRideAssistStateDisappear( pTeam );
	}
}

//骑乘队长状态消失
void CPlayerStateManager::SetPlayerRideLeaderStateDisappear( RideTeam* pTeam )
{
	if ( !pTeam )
		return;

	if( !m_pPlayer )
		return;

	if ( m_rideState.GetPlayerRideType() == CRideState::E_LEADER )
	{
		pTeam->DissolveRideTeam();
		//解散完毕后,回收内存
		RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam );
	}
	else
	{
		D_ERROR("%s 不属于骑乘队长,但发送来解散骑乘队伍的消息\n", m_pPlayer->GetNickName() );
	}
}

void CPlayerStateManager::OnPlayerBeGodPunished()
{
	if( !m_pPlayer )
		return;

	if ( m_pPlayer->IsRideState() )
	{
		RideTeam* pTmpTeam = m_pPlayer->GetSelfStateManager().GetRideState().GetRideTeam();
		if ( pTmpTeam )
		{
			pTmpTeam->DissolveRideTeam();
			//解散完毕后,回收内存
			RideTeamManagerSingle::instance()->RecoverRideTeam( pTmpTeam );
		}
		else
		{
			D_ERROR("天谴玩家%s在骑乘状态,但是没有找到骑乘队伍的指针\n", m_pPlayer->GetNickName() );
		}
	}
}


bool CPlayerStateManager::IsCanFight()
{
	if( NULL == m_pPlayer )
		return false;
	return m_bFight;
}


//骑乘队员状态消失
void CPlayerStateManager::SetPlayerRideAssistStateDisappear(RideTeam* pTeam )
{
	if ( !pTeam )
		return;

	if( !m_pPlayer )
		return;

	if ( m_rideState.GetPlayerRideType() == CRideState::E_ASSIST )
	{
		pTeam->DissolveRideTeam();

		//解散完毕后,回收内存
		RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam );
	}
	else
	{
		D_ERROR("%s 玩家不属于骑乘队员,但发来了离开骑乘队伍的请求\n", m_pPlayer->GetNickName() );
	}
}

void CPlayerStateManager::OnPlayerBeAttack()
{
	if ( NULL == m_pPlayer )
	{
		D_ERROR( "CPlayerStateManager::OnPlayerBeAttack, NULL == m_pPlayer\n" );
		return;
	}

	if ( IsOnRideState() )
	{
		//获取骑乘队伍
		RideTeam* pTeam =  RideTeamManagerSingle::instance()->GetRideTeamByID( m_rideState.GetRideTeamID() );
		if( !pTeam )
		{
			D_ERROR( "玩家%s被打，处于骑乘状态，但找不到骑乘队伍的指针\n", m_pPlayer->GetAccount() );
			return;
		}

		CPlayer* pTeamLeader = pTeam->GetRideLeader();
		if ( !pTeamLeader )
		{
			D_ERROR( "OnPlayerBeAttack, 玩家%s, 骑乘队伍找不到骑乘队长\n", m_pPlayer->GetAccount() );
			return;
		}

		//判断是否应该被打下马
		bool isDisMount = true;
		if ( pTeamLeader->GetSelfStateManager().GetRideState().GetPlayerRideState() == E_RIDE_EQUIP )//如果是骑乘装备,有抗下马几率
		{
			const ItemInfo_i* pMountItem = pTeamLeader->GetValidDriveEquip();
			if ( NULL == pMountItem )
			{
				D_ERROR("OnPlayerBeAttack, 玩家%s, 获取骑乘队伍中队长%s的骑乘装备位置上的道具，结果该位为空\n", m_pPlayer->GetAccount(), pTeamLeader->GetAccount() );
				return;
			}

			//获得骑乘装备的属性
			MountProperty* pMountProp = CMountPropertyManagerSingle::instance()->GetSpecialPropertyT( pMountItem->usItemTypeID );
			if ( NULL == pMountProp )
			{
				D_ERROR("OnPlayerBeAttack, 玩家%s, 找不到队长%s对应骑乘装备%d的属性\n", m_pPlayer->GetAccount(), pTeamLeader->GetAccount(), pMountItem->usItemTypeID  );
				return;
			}

			int stunCri = pMountProp->BeKickCri;
			if ( GET_ITEM_LEVELUP( pMountItem->ucLevelUp ) > 0 )
			{
				//获取追加后的属性
				unsigned int levelUpID  =  pMountProp->brand * 100u + GET_ITEM_LEVELUP( pMountItem->ucLevelUp );

				MountLevelUpProp* pProp = MountLevelUpPropManagerSingle::instance()->GetMountLevelUpProp( levelUpID );
				if ( NULL != pProp )
				{
					stunCri += pProp->BeKickCri;
				} else {
					D_ERROR("OnPlayerBeAttack, 玩家%s, 获取队长%s骑乘装备的升级属性失败,升级ID:%d\n", m_pPlayer->GetAccount(), pTeamLeader->GetAccount(), pMountItem->ucLevelUp  );
				}
			}


			int criRst = RandNumber( 1, 100 );
			if ( criRst > stunCri )
			{
				D_DEBUG("OnPlayerBeAttack, 玩家%s, 玩家%s骑乘被攻击，随到了%d， 抗击晕概率%d,下马\n", m_pPlayer->GetAccount(), pTeamLeader->GetNickName(), criRst, stunCri );
			}
			else
			{
				D_DEBUG("OnPlayerBeAttack, 玩家%s, 玩家%s骑乘被攻击，随到了%d， 抗击晕概率%d,不下马\n", m_pPlayer->GetAccount(), pTeamLeader->GetNickName(), criRst, stunCri );
				isDisMount = false;
			}
		}//如果是骑乘道具，则直接下马:)

		if ( isDisMount )
		{
			//解散这个队伍,双人骑乘的状态修改
			pTeam->DissolveRideTeam();
			RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam );
		}
	}
}

void CPlayerStateManager::OnPlayerDie()
{
	//如果死亡的时候在邀请骑乘状态，则更新邀请的状态信息
	if ( m_rideState.IsInviteState() )
		m_rideState.ResetRideInviteState();

	//如果死亡时还在骑乘状态，那么解散队伍
	if ( IsOnRideState() )
	{
		RideTeam* pTeam =  RideTeamManagerSingle::instance()->GetRideTeamByID( m_rideState.GetRideTeamID() );
		if ( pTeam )
		{
			//不管怎样,骑乘队伍中只要有任意玩家死亡,都解散玩家
			pTeam->DissolveRideTeam();
			//解散完毕后,回收内存
			RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam );

		} else {
			//assert(false);
			D_ERROR( "CPlayerStateManager::OnPlayerDie, assert(false)\n" );
		}
	}
}


void CPlayerStateManager::AttachPlayer(CPlayer *pPlayer)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CPlayerStateManager::AttachPlayer, NULL == pPlayer\n" );
		m_pPlayer = NULL;
		return;
	}
	m_pPlayer = pPlayer;
	CRideState& rideState  = pPlayer->GetSelfStateManager().GetRideState();
	rideState.ReSet();
	CGroupState& groupState = pPlayer->GetSelfStateManager().GetGroupState();
	groupState.ReSet();
	m_bFaint = false;
	m_bBondage = false;
}
