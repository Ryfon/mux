﻿#pragma once

class CPlayer;
class CMonster;

class CRacePrestigeManager
{
public:
	CRacePrestigeManager(void);
	~CRacePrestigeManager(void);

	void AttachPlayer( CPlayer* pPlayer );		//获得寄主
	void AddRacePrestige( unsigned int addValue );			//增加种族声望
	bool SubRacePrestige( unsigned int subValue );			//减少种族声望,为false表示减少不了
	void OnKillOther( CPlayer* pKilledPlayer );				//对打死一个玩家时对种族声望的处理
	void OnKillNpc( CMonster* pDeadNpc );				//当杀死一个NPC时


private:
	CPlayer* m_pOwner;
};
