﻿#include "CSortItem.h"
#include "Player/Player.h"

#include "MuxMap/muxmapbase.h"
#include "LogManager.h"

void CSortPkgItem::OnBegin(char sortpage)
{
	if (!m_isActive)
	{
		switch(sortpage)
		{
		case 0://整理普通背包
			m_begin = 0;
			m_end = NORMAL_PKG_SIZE;
			break;
		case 1://整理任务背包
			m_begin = NORMAL_PKG_SIZE;
			m_end = PACKAGE_SIZE;
			break;
		default:
			return;
		}
		m_isActive = true;
	}
}

void CSortPkgItem::OnEnd()
{
	if (m_isActive)
	{
		//备份现在的背包
		if ( NULL == m_pPlayer )
		{
			return;
		}
		FullPlayerInfo * playerInfo = m_pPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CSortPkgItem::OnEnd NULL == playerInfo\n");
			return;
		}
		for ( unsigned int i = m_begin; i<m_end; ++i)
		{
			if (PlayerInfo_3_Pkgs::INVALIDATE == playerInfo->pkgInfo.pkgs[i].uiID)
			{
				continue;
			}
			if (!m_pPlayer->IsLeftUpPkgIndex(i,playerInfo->pkgInfo.pkgs[i].uiID))
			{
				continue;
			}
			m_itemInfoBak[i] = playerInfo->pkgInfo.pkgs[i];
			m_uidSet.insert(playerInfo->pkgInfo.pkgs[i].uiID);
			m_itemInfoMap.insert(std::make_pair(m_itemInfoBak[i].uiID,&m_itemInfoBak[i]));
		}
		//检查
		for(unsigned int i = m_begin; i<m_end; ++i)
		{
			if (PlayerInfo_3_Pkgs::INVALIDATE == m_itemInfo[i])
			{
				continue;
			}
			if ( m_uidSet.find(m_itemInfo[i]) == m_uidSet.end() )
			{//有不存在的道具，排序中止
				SetInit();
				return;
			}
			m_uidSet.erase(m_itemInfo[i]);
		}
		if (!m_uidSet.empty())
		{//缺少道具，排序中止
			SetInit();
			return;
		}

		//清除背包
		m_pPlayer->ClearPkgItem(m_begin,m_end);

		//开始填充
		for (unsigned i = m_begin; i<m_end; ++i)
		{
			if (PlayerInfo_3_Pkgs::INVALIDATE == m_itemInfo[i])
			{
				continue;
			}
			std::map<unsigned int,ItemInfo_i*>::iterator it = m_itemInfoMap.find(m_itemInfo[i]);
			if ( it == m_itemInfoMap.end())
			{//前面已检查过，不可能出现这种情况
				OnSortFail();
				return;
			}
			ItemInfo_i * itemInfo = it->second;
			if ( NULL == itemInfo )
			{
				OnSortFail();
				return;
			}
			if (m_pPlayer->IsFreePkgIndex(i,CBaseItem::GetWidth(itemInfo->usItemTypeID),CBaseItem::GetHeight(itemInfo->usItemTypeID)))
			{
				m_pPlayer->SetPkgItem(*itemInfo,i);
			}
			else
			{
				OnSortFail();
				return;
			}
		}

		//通知玩家更新背包信息
		std::set<unsigned int>  pkgChangeIndex;//有变动的包裹位置
		for (unsigned i = m_begin; i<m_end; ++i)
		{
			if (PlayerInfo_3_Pkgs::INVALIDATE != m_itemInfo[i])
			{
				pkgChangeIndex.insert(i);
			}
			if (PlayerInfo_3_Pkgs::INVALIDATE != m_itemInfoBak[i].uiID)
			{
				pkgChangeIndex.insert(i);
			}
		}
		for (std::set<unsigned int>::iterator it = pkgChangeIndex.begin(); it != pkgChangeIndex.end(); ++it)
		{
			m_pPlayer->NoticePkgItemChange(*it);
		}

		//状态还原
		SetInit();

	}
}

void CSortPkgItem::OnRecvItemInfo(const ItemUidPosition* itemInfo, unsigned int size )
{
	if (m_isActive)
	{
		for (unsigned int i = 0; i<size; ++i)
		{
			unsigned int index = itemInfo[i].index;
			if (index != -1)
			{
				if ( PlayerInfo_3_Pkgs::INVALIDATE != itemInfo[i].itemUid)
				{
					if (index >= m_begin && index < m_end)
					{
						m_itemInfo[index] = itemInfo[i].itemUid;
					}
				}
			}
		}
	}
}

void CSortPkgItem::SetInit()
{
	m_isActive = false;
	StructMemSet(m_itemInfo,0,sizeof(m_itemInfo));
	StructMemSet(m_itemInfoBak,0,sizeof(m_itemInfoBak));
	m_uidSet.clear();
	m_itemInfoMap.clear();
	m_begin = m_end = 0;
}

void CSortPkgItem::OnSortFail()
{
	if ( NULL == m_pPlayer )
	{
		return;
	}
	//将备份的背包信息写回玩家背包
	m_pPlayer->ClearPkgItem(m_begin,m_end);
	for (unsigned int i = m_begin; i<m_end; ++i)
	{
		if (PlayerInfo_3_Pkgs::INVALIDATE == m_itemInfoBak[i].uiID)
		{
			continue;
		}
		m_pPlayer->SetPkgItem(m_itemInfoBak[i],i);
	}
	SetInit();
}
