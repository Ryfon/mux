﻿#include "PoolManager.h"
#include "Player/Player.h"

extern unsigned int g_maxMapSrvPlayerNum;

///开辟内存
bool CPoolManager::Intialize(int nMapCapacity, int nPlayerCapacity, int nMonsterCapacity)
{
	TRY_BEGIN;

	ACE_UNUSED_ARG( nMapCapacity ); ACE_UNUSED_ARG( nPlayerCapacity );
	m_pPlayerPool = NEW TCache<CPlayer>(g_maxMapSrvPlayerNum*2); 
	m_pGateSrvPool = NEW TCache< CGateSrv >( 32 );//最多32个GateSrv;
	m_pScampBuffPool = NEW TCache< CScampBuff >( g_maxMapSrvPlayerNum*2 );
	m_pSkillPool = NEW TCache<CMuxPlayerSkill>( g_maxMapSrvPlayerNum*20 );

	return true;
	TRY_END;
	return false;
}

void CPoolManager::Clear()
{
	TRY_BEGIN;

	if ( NULL != m_pGateSrvPool )
	{
		m_pGateSrvPool->Clear();
		delete m_pGateSrvPool; m_pGateSrvPool = NULL;
	}

	if ( NULL != m_pPlayerPool )
	{
		m_pPlayerPool->Clear();
		delete m_pPlayerPool; m_pPlayerPool = NULL;
	}
 
	if( NULL != m_pScampBuffPool )
	{
		m_pScampBuffPool->Clear();
		delete m_pScampBuffPool; m_pScampBuffPool = NULL;
	}

	if( NULL != m_pSkillPool )
	{
		m_pSkillPool->Clear();
		delete m_pSkillPool; m_pSkillPool = NULL;
	}	


	TRY_END;
}

///取对象
CPlayer* CPoolManager::RetrivePlayer()
{
	if ( NULL == m_pPlayerPool )
	{
		return NULL;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//CPlayer实例中增加了一个唯一ID号，在每次回收重用时都递增，为了保证任何时候都能取到实例属性，所有实例都要从池中取出来而不能临时NEW;
	//return m_pPlayerPool->RetrieveOrCreate();
	return m_pPlayerPool->Retrieve();
	//CPlayer实例中增加了一个唯一ID号，在每次回收重用时都递增，为了保证任何时候都能取到实例属性，所有实例都要从池中取出来而不能临时NEW;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}


//取一个技能对象
CMuxPlayerSkill* CPoolManager::RetriveSkill()
{
	if ( NULL == m_pSkillPool )
	{
		return NULL;
	}
	return m_pSkillPool->Retrieve();
}


CGateSrv* CPoolManager::RetriveGateSrv()
{
	if ( NULL == m_pGateSrvPool )
	{
		return NULL;
	}
	return m_pGateSrvPool->Retrieve();
}
 

void CPoolManager::ReleaseGateSrv( CGateSrv* pGateSrv )
{
	if ( NULL != pGateSrv )
	{
		pGateSrv->PoolObjInit();//递增唯一计数；
	}
	if ( NULL != m_pGateSrvPool )
	{
		m_pGateSrvPool->Release( pGateSrv );
	}
}


//释放技能
void CPoolManager::ReleaseSkill( CMuxPlayerSkill* pSkill )
{
	if ( NULL != pSkill )
	{
		pSkill->PoolObjInit();
	}
	if ( NULL != m_pSkillPool )
	{
		m_pSkillPool->Release( pSkill );
	}
}

void CPoolManager::Release( CPlayer* pPlayer )
{
	if ( NULL != pPlayer )
	{
		pPlayer->PoolObjInit();//递增唯一计数；
	}
	if ( NULL != m_pPlayerPool )
	{
		m_pPlayerPool->Release( pPlayer );
	}
}


void CPoolManager::ReleaseRogueBuff( CScampBuff* pBuff )
{
	if ( NULL != m_pScampBuffPool )
	{
		m_pScampBuffPool->Release( pBuff );
	}
}
