﻿#pragma once

class CLevelProp
{
public:
	CLevelProp(void);
	~CLevelProp(void);

public:
	unsigned long GetObjID()
	{
		return m_lLevel;
	}

public:
	unsigned long m_lLevel;		//ID号
	unsigned long lMaxHP;		//HP的最大值
	unsigned long lMaxMP;		//MP的最大值
	unsigned int nStrength;		//力量
	unsigned int nAgility;		//敏捷
	unsigned int nIntelligence;	//智力	
	unsigned int m_nPhyAttack;	//物理攻击
	unsigned int m_nMagAttack;	//魔法攻击
	unsigned int m_nPhyDefend;	//物理防御
	unsigned int m_nMagDefend;	//魔法防御
	unsigned int m_nPhyHit;	//物理命中
	unsigned int m_nMagHit;	//魔法命中
	unsigned long lNextLevelExp;	//下一级需要的经验
	unsigned long lLastLevelExp;	//上一级需要的经验
};
