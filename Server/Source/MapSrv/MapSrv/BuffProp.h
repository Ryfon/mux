﻿



#ifndef ASSSKILLPROP_H
#define ASSSKILLPROP_H

#include "../../Base/PkgProc/SrvProtocol.h"
#include "ManObject.h"
#include "XmlManager.h"
using namespace MUX_PROTO;


//辅助技能的属性
class CBuffProp
{
	friend class CManBuffProp;

private:
	CBuffProp( const CBuffProp& map);
	CBuffProp& operator = ( const CBuffProp& map ); //屏蔽这两个操作；

public:
	enum PUSH_RULE  //顶推的规则
	{
		BLUE,	//蓝 0
		RED,	//红 1
		YELLOW,	//.黄 2
		ABSORB_IMMUNITY,  //吸收免疫规则
		RULE_INVALID, //4无效的规则

	};


public:
	CBuffProp(void);
	virtual ~CBuffProp(void);

	inline TYPE_ID GetObjID()
	{
		return m_nBuffID;
	}

	inline TYPE_ID GetBuffID()
	{
		return m_nBuffID;
	}

	bool IsDotType();

	inline bool IsComplexType()
	{
		if( m_buffType == BUFF_SP_COMPLEX )
			return true;
		return false;
	}

	inline BUFF_TYPE GetBuffPropType() { return m_buffType; };

	inline int GetBuffPropVal() { return m_iVal; };

	inline bool IsDebuff() { return m_bDebuff; };

	inline int GetSpaceTime() { return m_nSpaceTime; };

	inline PUSH_RULE GetPushRule() { return m_pushRule; };

	inline USHORT GetBuffIndex() { return m_buffIndex; };

	inline TYPE_UI8 GetAssistChoice() { return m_nAssistChoice; };

	inline void PushLinkBuffProp( CBuffProp* pProp ){ m_vecLinkBuffProp.push_back( pProp );  };

	void GetLinkVec(vector<CBuffProp*>& vecLinkBuff) { vecLinkBuff = m_vecLinkBuffProp; };

	//辅助类的技能属性
private:
	TYPE_ID m_nBuffID;			//技能的ID,通过ID来寻找
	TYPE_UI8 m_nAssistChoice;	//这个BUFF属于人还是怪物
	USHORT m_buffIndex;			//这个BUFF规定放在人中的栏位
	BUFF_TYPE m_buffType;		//buff的类型
	PUSH_RULE m_pushRule;		//顶推的规则
	int m_iVal;					//更新的值int型，如果需要浮点型就用百分比
	bool m_bDebuff;					//是否是有利BUFF,怪物的时候需要用
	int m_nSpaceTime;		//hot dot跳的时间
	vector<CBuffProp*> m_vecLinkBuffProp;
};

class CManBuffProp : public CObjectManager< CBuffProp >
{
private:
	CManBuffProp(){};
	~CManBuffProp(){};

public:
	static bool Init();


private:
	static bool LoadBuffXML(const char* pszFileName);

	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
	{
		if ( NULL == pEle || NULL == attName )
		{
			return false;
		}
		if ( NULL == pEle->GetAttr( attName ) )
		{
			D_ERROR( "服务器配置XML文件第%d条记录属性%s失败\n", nRecord, attName );
			return false;
		}
		return true;
	}
};

#endif /*ASSSKILLPROP_H*/

