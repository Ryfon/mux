﻿/********************************************************************
	created:	2009/10/27
	created:	27:10:2009   14:44
	file:		OLTestPlate.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef OLTEST_H
#define OLTEST_H

#include <vector>

class CPlayer;
class OLTestPlate;

struct TestSession
{
	friend class  OLTestPlate;

private:
	unsigned int curtaskID;//当前的答题任务所关联的任务编号
	unsigned int curquestionID;//当前问题的编号
	unsigned int curanswerID;//当前问题的答案的编号
	unsigned int curexpiretime;//回答当前问题所限制的时间
	unsigned int curansweredcount;//当前已经完成的答题数目
	unsigned int curtargetcount;//当前答题session所要完成的答题数目
	

	TestSession():curtaskID(0),curquestionID(0),curanswerID(0),curexpiretime(0),curansweredcount(0),curtargetcount(0){}

	void ClearSession()
	{
	    curtaskID = curanswerID = curquestionID = curexpiretime = curansweredcount = curtargetcount = 0;
	}

	void IncAnsweredCount() { curansweredcount++; }

	unsigned int GetCurAnsweredCount() { return curansweredcount; }

	unsigned int GetTargetCount() { return curtargetcount; }

	unsigned int GetTestSessionTaskID() { return curtaskID; }

	void SetTestSessionTaskID( unsigned int taskID )
	{
		curtaskID = taskID;
	}

	void SetCurQuestionInfo( unsigned int questionid, unsigned int answerid )
	{
		curquestionID = questionid;
		curanswerID   = answerid;
	}

	void SetTestSessionExpireTime( unsigned int expiretime )
	{
		curexpiretime = expiretime;
	}

	bool IsReachTargetCount() { return  curansweredcount == curtargetcount ;}
};

class OLTestPlate
{
public:
	OLTestPlate():mpAttachPlayer(NULL){}

	//弹出出题目界面
	void ShowOLTestPlate( unsigned int taskID ,bool isNeedPreDialog );

	//回答问题
	void AnswerTestQuestion( unsigned int selectID );

	//关闭错误提示托盘
	void CloseAnswerQuestionWaitDlg();
	
	//时间过了,答题失败
	void TimeOutAnswerQuestionFail();

	//关闭题目托盘
	void CloseOLTestPlate();

	//依附玩家
	void AttachPlayer( CPlayer* pPlayer ){ mpAttachPlayer = pPlayer ;  ClearPlate(); }

	//是否正在限时答题
	bool IsTimeLimitAnswerNow(){ return mTestSession.curexpiretime > 0; }

	//答题是否过期,过期了通知失败
	void CheckIfOutExpireTime();

	//放弃本次答题
	void DropOLTest();

	void RefreshOLTest();

	void ClearOLTestCurQuestion() { mTestSession.curquestionID =  mTestSession.curanswerID = mTestSession.curexpiretime = 0; }

private:
	void ClearPlate() 
	{
		mTestSession.ClearSession();
		questionVec.clear();
	}

	void ShowPrevueDialog();

	void ShowAnswerAllQuestionDlg();//弹出回答完所有问题的对话框

	void AskQuestion();

	void ShowQuestion( unsigned int questionID, unsigned int answerid, unsigned int limittime );

	unsigned int SelectQuestion();

	void AswerQuestionRst( bool isRight );

	bool IsAnswerRight( unsigned int selectRst ) { return  selectRst == mTestSession.curanswerID; }

	void AnswerQuestionWrong();

	void AnswerQuestionRight();

	bool IsQuestioning() { return mTestSession.curquestionID != 0 ; }

	void SetOLTestSessionInfo( unsigned int curtaskid, unsigned int questionstart, unsigned int questionend, unsigned int alreadyansweredcount , unsigned int targetcount );

private:
	CPlayer* mpAttachPlayer;//托盘属于的玩家
	std::vector<unsigned int> questionVec;//session生成时产生的答题集
	TestSession mTestSession;
};

#endif
