﻿#pragma once

//#ifdef AI_SUP_CODE

#include <list>
#include <map>
#include <string>

#include "Utility.h"
#include "../Lua/include/lua.hpp"

using namespace std;

typedef struct  SkillUsageEntryTag
{
	unsigned long	m_ID; //技能ID
	int				m_weight;       //选择时的权重

	SkillUsageEntryTag()
	{
		m_ID = 0; 
		m_weight = 0;
	};

} SkillUsageEntry;

typedef std::list<SkillUsageEntry>  _SkillSet;
class SkillSet
{
public:
	unsigned long m_ID; //monster class' ID
	_SkillSet     m_Skills;

	SkillSet(unsigned long id,_SkillSet& Skills):m_Skills(Skills)
	{
		m_ID = id;
	}

	~SkillSet();/*
	{

	}*/

	static bool Load(lua_State* L,int index,
		_SkillSet& Skills, unsigned long& ID, string& ErrorCode);/*
	{
		return true;
	}*/
};

//typedef std::list<CAIRule*> AIRULE_LIST;
class MonsterSkillSet
{
public:
	typedef std::map<unsigned long, SkillSet> SKILLSET_MAP;
	
	MonsterSkillSet();/* {}*/
	~MonsterSkillSet();/* {}*/

	//read one rule from lua data file...
	//index with 1 as the first element
	bool Load(lua_State* L, const char* DataFile, string& ErrorCode);/*
	{
		return true;
	}*/

	static MonsterSkillSet* Instance();/*
	{
		static MonsterSkillSet inst;
		return &inst;
	}*/

	SkillSet* GetSkillSet(unsigned long MID);
private:
	SKILLSET_MAP m_SkillSetMap;
	int m_Count;
};


//#endif //AI_SUP_CODE
