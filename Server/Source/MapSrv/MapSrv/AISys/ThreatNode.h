﻿#pragma once

//#ifdef AI_SUP_CODE
#include "../Player/Player.h"
//#include "../Movement/TimeTracker.h"

//威胁值超过毫秒之后,还未变化的话,则将该威胁对象从列表中剔除
#define TIMELIMIT_WAIT_CHANGE 30000   //单位：毫秒

typedef struct ThreatData 
{
	CMuxCreature*	m_pThreatObj;
	unsigned int	m_ThreatValue;
	//CTimeTracker m_Timer; //监视30秒内威胁值是否有变化
	ACE_Time_Value m_UpdateTimeStamp; //time stamp for the last update of threat value
	
	ThreatData(CMuxCreature* pThreatObj, unsigned int InitValue = 0)
	{
		m_ThreatValue = InitValue;
		m_pThreatObj = pThreatObj;
		m_UpdateTimeStamp = ACE_OS::gettimeofday();
	}

	void IncThreatValue(unsigned int amount)
	{
		m_ThreatValue += amount;
		m_UpdateTimeStamp = ACE_OS::gettimeofday();
	}

	//检查自上一次更新威胁值以来计时器是否已超过30秒
	bool IsTimerPassed()
	{
		//return m_Timer.Passed();
		ACE_Time_Value now(ACE_OS::gettimeofday());
		int TimeElapsed = (int)((now - m_UpdateTimeStamp).sec()); //获得逝去的秒数 
		return (TimeElapsed >= 30);
	}

}ThreatInfo, *PThreatInfo;

//已废弃?修改吴红升报来潜在时注释掉，by dzj, 10.06.17,struct CThreatNode
//{
//public:
//	explicit CThreatNode(PThreatInfo pThreatInfo);
//	explicit CThreatNode(CMuxCreature* pThreatObj,unsigned long InitValue = 0,
//				int TimeLimit = TIMELIMIT_WAIT_CHANGE);
//	~CThreatNode(void);
//
//	CMuxCreature* GetThreatObj()
//	{
//		if(m_pThreatInfo != NULL)
//			return m_pThreatInfo->m_pThreatObj;
//		else
//			return NULL;
//	}; 
//
//	unsigned int GetThreatValue()
//	{
//		if(m_pThreatInfo != NULL)
//			return m_pThreatInfo->m_ThreatValue;
//		else
//			return 0;
//	}
//
//	void IncThreatValue(unsigned long amount);
//
//	void MutateCore(CThreatNode* p);
//
//	//void OnUpdate(int TimeElapsed);
//
//	bool IsTimerPassed()
//	{
//		return m_pThreatInfo->IsTimerPassed();
//	}
//
//	//很多个ThreatNode以链表的方式构成威胁列表，威胁值从大到小依次排列。
//	CThreatNode* m_pPrev;
//	CThreatNode* m_pNext;
//
//	PThreatInfo m_pThreatInfo;
//
//	//代码规范 add by hyn 7.7
//private:
//	CThreatNode(const CThreatNode&);
//	CThreatNode& operator = ( const CThreatNode& );//屏蔽这两个操作；
//};


//#endif //AI_SUP_CODE
