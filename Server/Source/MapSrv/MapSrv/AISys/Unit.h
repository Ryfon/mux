﻿#pragma once

#include "PkgProc/CliProtocol.h"
#include "../Fsm/IFsm.h"

//#ifdef AI_SUP_CODE

enum UNITTYPE
{
	PLAYER_UNIT = 1,
	MONSTER_UNIT = 2
};

class CUnit
{
public:
	explicit CUnit(UNITTYPE unitType);
	virtual ~CUnit(void);

	virtual unsigned long GetID() = 0;
	virtual void GetPosition(TYPE_POS& x, TYPE_POS& y) = 0;

	static int GetDistance(CUnit* pUnit1, CUnit* pUnit2);

	UNITTYPE GetType()	{ return m_Type;}
private:
	UNITTYPE m_Type;

private:
	CUnit( const CUnit& u);  
	CUnit& operator = ( const CUnit& u);
};

//#endif //AI_SUP_CODE
