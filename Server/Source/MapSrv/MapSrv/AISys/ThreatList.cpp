﻿//
//#include "ThreatList.h"
//#include "PlayerUnit.h"
//
//CThreatList::CThreatList()
//{
//	m_pHead = m_pTail = NULL;
//	m_Size = 0;
//	m_Capacity = DEFAULT_CAPACITY_THREATLIST;
//	m_MaxCapacity = DEFAULT_CAPACITY_THREATLIST;
//}
//
//CThreatList::~CThreatList(void)
//{
//	Clear();
//}
//
//bool CThreatList::IsEmpty()
//{
//	return (m_Size == 0);
//}
//
//void CThreatList::Clear()
//{
//	CThreatNode* p = m_pHead;
//	CThreatNode* q = NULL;
//
//	while(p != NULL)
//	{
//		q = p->m_pNext;
//		delete p;
//		p = q;
//	}
//
//	m_pHead = m_pTail = NULL;
//	m_Size = 0;
//	ResetMaxCapacity();
//}
//
//void CThreatList::AddNode(CMuxCreature* pThreatObj, unsigned int InitValue)
//{
//	//若存在的话，则不作任何处理
//	CThreatNode* pNode = FindNode(pThreatObj);
//
//	//已存在，无需加入
//	if(pNode != NULL)
//		return; 
//	else
//	{
//		InsertNewNode(pThreatObj,InitValue);
//	}
//}
//
//void CThreatList::DelNode(CMuxCreature* pThreatObj, bool isDied)
//{
//	if(m_Size == 0)
//		return;
//
//	CThreatNode* p = FindNode(pThreatObj);
//	if(p == NULL)  //NOT FOUND
//		return; 
//
//	DelNodeX(p, isDied);
//}
//
//void CThreatList::DelNodeX(CThreatNode* p, bool isDied)
//{
//	//注意: 位于No.1的Node被删除时,相应的处理和其他Node的删除处理有所不同
//	if(m_Size == 1)
//	{
//		delete p;
//		m_pHead = m_pTail = NULL;
//		m_Size--;
//
//		//additional process when No.1 node has been killed 
//		if(isDied == true)
//			m_Capacity--;
//	}
//	else  //size > 1
//	{
//		//分三种情形来处理
//		//case a: p locates at the head of list
//		if(isHead(p) == true)
//		{
//			CThreatNode* q = p->m_pNext;
//			q->m_pPrev = p->m_pPrev;
//			m_pHead = q;
//
//			//additional process for No.1 node that has been killed 
//			if(isDied == true)
//				m_Capacity--;
//		}
//		else if(isTail(p) == true) //at tail
//		{
//			CThreatNode* q = p->m_pPrev;
//			q->m_pNext = p->m_pNext;
//			m_pTail = q;
//		}
//		else  //at the middle of 
//		{
//			CThreatNode* q = p->m_pNext;
//			CThreatNode* o = p->m_pPrev;
//			o->m_pNext = q;
//			q->m_pPrev = o;			
//		}
//
//		//final manpulate shared by all three cases with the size > 1
//		delete p;
//		m_Size--;
//	}
//}
//
//CThreatNode* CThreatList::FindNode(CMuxCreature* pThreatObj)
//{
//	CThreatNode* p = m_pHead;
//	//CThreatNode* q = NULL;
//
//	while(p != NULL)
//	{
//		if(p->GetThreatObj() == pThreatObj)
//			return p;
//
//		p = p->m_pNext;
//	}
//
//	return NULL;
//}
//
////的的确确要把一个新Node插入到链表中去
////算法上，从后往前来寻找插入的位置
//void CThreatList::InsertNewNode(CMuxCreature* pThreatObj, unsigned int InitValue)
//{
//	CThreatNode* pNewNode = NULL;
//	if(m_Size == 0)
//	{
//		pNewNode = NEW CThreatNode(pThreatObj,InitValue);
//		m_pHead = m_pTail = pNewNode;
//		m_Size++;
//		return;
//	}
//	else if(m_Size >= 1 && m_Size < m_Capacity)
//	{
//		InsertNewNodeX(pThreatObj, InitValue);
//		return;
//	}
//	else //m_Size == m_Capacity,达到了最大极限，
//		 //如果满足一定的条件，需要将末尾的元素挤出去
//	{
//		//将30秒钟威胁值未变的Node给删除掉
//		//CleanNoChangedNode();
//
//		if(m_Size >= 1 && m_Size < m_Capacity)
//		{
//			//清洗有效果，空出了新位置
//			InsertNewNodeX(pThreatObj, InitValue);
//			return;
//		}
//		else
//		{
//			//注意对头哨兵、尾哨兵、计数的维护
//			//检查是否要将末尾的node挤出去
//			if(InitValue > m_pTail->GetThreatValue())
//			{
//				//需要将末尾的node挤出去
//				//实际操作时，不需废掉Tail这个Node，只需狸猫换太子。
//				delete m_pTail->m_pThreatInfo;
//				m_pTail->m_pThreatInfo = NEW ThreatInfo(pThreatObj,InitValue);
//				return;
//			}
//			else //不需进行任何实质操作了
//				return;
//		}
//	}
//}
//
////适用于这样的情形：m_Size >= 1 && m_Size < m_Capacity
//void CThreatList::InsertNewNodeX(CMuxCreature* pThreatObj, unsigned int InitValue)
//{
//	CThreatNode* pNewNode = NEW CThreatNode(pThreatObj,InitValue);
//	CThreatNode* pInsLoc = GetInsertPos(pNewNode);
//
//	if(pInsLoc == NULL) //则插在队列的头部
//	{
//		pNewNode->m_pNext = m_pHead; 
//		m_pHead->m_pPrev = pNewNode;
//		m_pHead = pNewNode;
//	}
//	else
//	{   //插在pInsLoc之后
//		CThreatNode* q = pInsLoc->m_pNext; 
//		pNewNode->m_pNext = q; 
//		pNewNode->m_pPrev = pInsLoc;
//		pInsLoc->m_pNext = pNewNode; 
//
//		if(q == NULL) //插在了队尾
//		{
//			m_pTail = pNewNode;
//		}
//		else
//		{
//			q->m_pPrev = pNewNode;
//		}
//	}
//
//	m_Size++;
//	return;
//}
//
////获得应该的插入位置，应该将Q插在返回node的后面
//CThreatNode* CThreatList::GetInsertPos(CThreatNode* pNewNode)
//{
//	CThreatNode* p = m_pTail;
//
//	while(p != NULL && p->GetThreatValue() < pNewNode->GetThreatValue())
//	{
//		p = p->m_pPrev;
//	}
//
//	//最终获得的P，应该将Q插在P之后
//	return p;
//}
//
////pNode的威胁值增加了，把它往前移到合适的新位置
//void CThreatList::MoveForward(CThreatNode*& p)
//{
//	CThreatNode* q = p->m_pPrev;
//	while(q != NULL && q->GetThreatValue() < p->GetThreatValue())
//	{
//		//交换两个node的核心指针
//		p->MutateCore(q);
//		p = q;
//		q = q->m_pPrev;
//	}
//}
//
//void CThreatList::CleanNoChangedNode()
//{
//	if(m_Size > 1)
//	{
//		CThreatNode* p = m_pTail;
//		while(p != NULL)
//		{
//			//因为可能涉及到删除,故而要留好后路
//			CThreatNode* q = p->m_pPrev;
//			if(p->IsTimerPassed() == true)
//			{
//				//最新的策划意图：将三十秒内未变化的全部删掉
//				DelNodeX(p, false);
//				//break; //只删一个
//			}
//			p = q;
//		}
//	}	
//}
//
///*
//void CThreatList::CleanNoChangedNode()
//{
//	if(m_Size > 1)
//	{
//		CThreatNode* p = m_pHead->m_pNext;
//		while(p != NULL)
//		{
//			//因为可能涉及到删除,故而要留好后路
//			CThreatNode* q = p->m_pNext;
//			if(p->IsTimerPassed() == true)
//			{
//				DelNodeX(p, false);
//			}
//			p = q;
//		}
//	}	
//}
//*/
//void CThreatList::IncThreatValue(CPlayer* pThreatObj, unsigned int amount)
//{
//	CThreatNode* pNode = FindNode(pThreatObj);
//
//	//已存在，无需加入
//	if(pNode == NULL)
//		return; 
//	else
//	{
//		pNode->IncThreatValue(amount);	
//		//CPlayer* pPlayer = pNode->GetThreatObj();
//		////因为傅晓强暂时没时间修改，现暂时屏蔽傅晓强的调试语句，by dzj, 08.07,???,邓子建检查;printf("Threat Value of %s increased with %d\n", pPlayer->GetNickName(), amount);
//
//		MoveForward(pNode);
//	}
//}
//
//void CThreatList::Dump()
//{
//	printf("----ThreatList's size: %d\n", GetSize());
//	CThreatNode* p = m_pHead;
//	int i = 1;
//	while(p != NULL)
//	{
//		CMuxCreature* thObj = p->GetThreatObj();
//		if(thObj->GetType() == CMuxCreature::CREA_TYPE_PLAYER)  //目前只允许怪打人
//		{
//			CPlayer* pPlayer = (CPlayer*)thObj;
//			CPlayerUnit pPlayerUnit(pPlayer); 
//			printf("No.%d, Role Name:%s, Value:%d\n", i, pPlayer->GetNickName(), p->GetThreatValue());
//		}
//		else  //威胁对象为另一怪物
//		{
//
//		}
//
//		i++;
//		p = p->m_pNext;
//	}
//	printf("----End of ThreatList\n");
//}
//
//void CThreatList::Dump(ThreatList& thlist)
//{
//	CThreatNode* p = m_pHead;
//	int i = 0;
//	while(p != NULL)
//	{
//		CMuxCreature* pCreature = p->GetThreatObj();
//
//		if(pCreature->GetType() == CMuxCreature::CREA_TYPE_PLAYER)
//		{
//			thlist.threatList[i].threatPlayerID = ((CPlayer*)pCreature)->GetFullID();
//			thlist.threatList[i].threatValue = p->GetThreatValue();
//		}
//		else //CMuxCreature::CREA_TYPE_MONSTER
//		{
//			//do nothing at the present
//		}
//
//		p = p->m_pNext;
//		i++;
//	}
//
//	thlist.threatCount = i;
//}
//
//void CThreatList::CheckNoChangedNode()
//{
//	if(GetSize() <= 1)
//		return;
//
//	CThreatNode* p = m_pHead->m_pNext;
//
//	while(p != NULL)
//	{
//		CThreatNode* q = p->m_pNext;
//
//		if(p->IsTimerPassed())
//			DelNodeX(p, false);
//
//		p = q;
//	}
//}
//
//namespace ruleai
//{
//	const bool ThreatList::ThreatData::operator <(const ThreatData & data)const
//	{
//		return value > data.value;
//	}
//
//	ThreatList::ThreatList(unsigned maxSize /* =  */)
//		: max_size_(max_size_), max_capacity_(maxSize)
//	{ }
//
//	ThreatList::~ThreatList()
//	{ }
//
//	void ThreatList::Clear()
//	{
//		list_.clear();
//	}
//
//	const unsigned ThreatList::Size()const
//	{
//		return (unsigned)list_.size();
//	}
//
//	void ThreatList::ResetMaxSize(unsigned maxSize)
//	{
//		max_size_ = maxSize;
//	}
//
//	const unsigned ThreatList::MaxSize()const
//	{
//		return max_size_;
//	}
//
//	void ThreatList::ResetMaxCapacity(unsigned maxSize)
//	{
//		max_capacity_ = max_size_ > maxSize ? max_size_ : maxSize;
//	}
//
//	const unsigned ThreatList::MaxCapacity()const
//	{
//		return max_capacity_;
//	}
//
//	void ThreatList::Push(CMuxCreature * threatener, int value)
//	{
//		if (HasCreature(threatener))
//			return;
//
//		ThreatData data;
//		data.threatener = threatener;
//		data.value = value;
//		time(&data.lastThreatUpdate);
//		list_.push_back(data);
//
//		std::sort(list_.begin(), list_.end());
//		if (list_.size() > max_size_)
//			CleanThreatNoChangeCreature();
//		if (list_.size() > max_size_)
//			list_.resize(max_size_);
//		
//	}
//
//	void ThreatList::UpateThreat(CMuxCreature * threatener, int added)
//	{
//		for(ListType::iterator it = list_.begin(); it != list_.end(); ++it)
//		{
//			if (it->threatener == threatener)
//			{
//				it->value += added;
//				time(&it->lastThreatUpdate);
//				break;
//			}
//		}
//
//		std::sort(list_.begin(), list_.end());
//	}
//
//	void ThreatList::PushOrUpdateThreat(CMuxCreature * threatener, int value)
//	{
//		for(ListType::iterator it = list_.begin(); it != list_.end(); ++it)
//		{
//			if (it->threatener == threatener)
//			{
//				it->value += value;
//				time(&it->lastThreatUpdate);
//				return;
//			}
//		}
//
//		Push(threatener, value);
//	}
//
//	void ThreatList::Pop(CMuxCreature * threatener)
//	{
//		for(ListType::iterator it = list_.begin(); it != list_.end(); ++it)
//		{
//			if (it->threatener == threatener)
//			{
//				list_.erase(it);
//				break;
//			}
//		}
//	}
//
//	const bool ThreatList::HasCreature(CMuxCreature * threatener)const
//	{
//		for(ListType::const_iterator it = list_.begin(); it != list_.end(); ++it)
//		{
//			if (it->threatener == threatener)
//			{
//				return true;
//			}
//		}
//		return false;
//	}
//
//	void ThreatList::GetMaxThreatCreature(CMuxCreature *&threatener)
//	{
//		threatener = 0;
//		if (!list_.empty())
//		{
//			threatener = list_.front().threatener;
//		}
//	}
//
//	void ThreatList::CleanThreatNoChangeCreature()
//	{
//		time_t now;
//		time(&now);
//		bool loop = true;
//		while(loop)
//		{
//			ListType::iterator it;
//			for( it = list_.begin(); it != list_.end(); ++it)
//			{
//				if (now - it->lastThreatUpdate >= 30)
//				{
//					list_.erase(it);
//					loop = true;
//					break;
//				}
//				else
//					loop = false;
//			}
//
//		}
//		
//	}
//
//	void ThreatList::Dump(_ThreatList & tl)const
//	{
//		int i = 0;
//		for(ListType::const_iterator it = list_.begin(); it != list_.end(); ++it, ++i)
//		{
//			CMuxCreature* pCreature = it->threatener;
//
//			if(pCreature->GetType() == CMuxCreature::CREA_TYPE_PLAYER)
//			{
//				tl.threatList[i].threatPlayerID = ((CPlayer*)pCreature)->GetFullID();
//				tl.threatList[i].threatValue = it->value;
//			}
//		}
//	}
//} // namespace ruleai
