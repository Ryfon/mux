﻿//#ifdef AI_SUP_CODE
//#include "../Lua/include/lua.hpp"
#include "MonsterSkillSet.h"
#include <memory>
//#include "Utility.h"

MonsterSkillSet gMonsterSkillSet;

SkillSet::~SkillSet()
{		
}

bool ParserSUEntry(lua_State* L, int index, SkillUsageEntry* entry, string& ErrorCode)
{
	ACE_UNUSED_ARG(ErrorCode);
	lua_pushnumber(L, index);
	lua_gettable(L, -2);  //获取ActSet[index]，它表示一条AI规则

	lua_getfield(L, -1, "SkillID");
	entry->m_ID = (unsigned long)(lua_tointeger(L, -1));
	lua_pop(L, 1);

	lua_getfield(L, -1, "Weight");
	entry->m_weight = (int)(lua_tointeger(L, -1));
	lua_pop(L, 1);

	lua_pop(L, 1); //Remove index
	return true;
}

bool SkillSet::Load(lua_State* L,int index,
		  _SkillSet& Skills, unsigned long& ID, 
		  string& ErrorCode)
{
	bool bRes = true;
	lua_pushnumber(L, index);
	lua_gettable(L, -2);  //获取MonsterSkillSet[index]，它表示一个SkillSet记录

	lua_getfield(L, -1, "MID");
	ID = (unsigned long)(lua_tointeger(L, -1));
	lua_pop(L, 1);  //remove "ID" key
	//printf("RuleSet ID:%d\n", RuleSetID);

	//--------SkillSet字段是个数组-------------------
	lua_getfield(L, -1, "SkillSet");
	if(lua_istable(L, -1) != 1)
	{
		ErrorCode = "'RuleSet should be a table!'";
		bRes = false;
	}
	
	if (bRes)
	{
		int n = (int)(lua_objlen(L, -1));
		if (n > 0)
		{
			for(int i=1; i<=n; i++)
			{
				try{ 
					SkillUsageEntry entry;

					bRes = ParserSUEntry(L, i, &entry, ErrorCode);
					if(bRes == false)
					{
						break;
					}
					else
					{
						Skills.push_back(entry);
					}
				}catch(...)
				{
					bRes = false;
				}
			}
		}
		else
		{
			bRes = false;
		}
	}

	lua_pop(L, 1);  //remove "SkillSet" key	
	//----------------------------------------------
	lua_pop(L, 1); //Remove index
	return bRes;
}

MonsterSkillSet::MonsterSkillSet(void)
{
	m_Count = 0;
	m_SkillSetMap.clear();
}

MonsterSkillSet::~MonsterSkillSet(void)
{
}

MonsterSkillSet* MonsterSkillSet::Instance()
{
	return &gMonsterSkillSet;
}

SkillSet* MonsterSkillSet::GetSkillSet(unsigned long MID)
{
	SKILLSET_MAP::iterator iter = m_SkillSetMap.find(MID);
	if(iter == m_SkillSetMap.end())
		return NULL;
	else
		return &iter->second;
}

bool MonsterSkillSet::Load(lua_State* L, const char* DataFile, string& ErrorCode)
{
	bool bRes = true;

	int res = luaL_dofile(L, DataFile);
	if(res != 0)  //出错了
	{
		ErrorCode = string(lua_tostring(L, -1));
		return false;		
	}

	lua_getglobal(L, "SkillSetCount");
	if(!lua_isnumber(L, -1))
	{
		ErrorCode = "'SkillSetCount' should be a integer";
		lua_pop(L, 1); //remove robot number
		return false;
	}

	m_Count = (int)lua_tointeger(L, -1);
	lua_pop(L, 1); //remove RuleCount 


	lua_getglobal(L, "MonsterSkillSet");
	if(!lua_istable(L, -1))
	{
		ErrorCode = "'MonsterSkillSet' should be a table containing Records for Rule Set";
		bRes = false;
		goto EndProc;
	}

	for(int i=0; i<m_Count; i++)
	{
		unsigned long ID; //monster class' ID
		_SkillSet     Skills;

		SkillSet::Load(L, i+1, Skills, ID, ErrorCode);
		SkillSet set(ID, Skills);
		m_SkillSetMap.insert(make_pair(ID,set));
	}

EndProc:
	lua_pop(L, 1); //remove RuleSetRepository
	return bRes;

}

//#endif //AI_SUP_CODE
