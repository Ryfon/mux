﻿#pragma once

#include <queue>
#include <vector>
#include <algorithm>
#include <ctime>

//#ifdef AI_SUP_CODE
#include "ThreatNode.h"

//威胁列表中，最大的栏位数目
#define DEFAULT_CAPACITY_THREATLIST  2
//#define MAX_CAPACITY_THREATLIST		10

//已废弃?修改吴红升报来bug时注释掉，by dzj, 10.06.17,class CThreatList
//{
//public:
//	CThreatList();
//	~CThreatList(void);
//
//	void SetMaxCapa(int MaxCapacity)
//	{
//		m_Capacity = m_MaxCapacity = MaxCapacity;
//	};
//
//	int GetSize() { return m_Size;};
//	void Clear();
//
//	bool IsEmpty();
//
//	void AddNode(CMuxCreature* pThreatObj, unsigned int InitValue = 0);
//	void DelNode(CMuxCreature* pThreatObj, bool isDied);
//
//	void IncThreatValue(CPlayer* pThreatObj, unsigned int amount);
//
//	CThreatNode* FindNode(CMuxCreature* pThreatObj);
//
//	//pNode的威胁值增加了，把它往前移到合适的新位置
//	void MoveForward(CThreatNode*& pNode);
//
//	//对排在非第一位的Node逐一进行检查,看威胁值是否在30秒时间内有变化
//	//void Update(int TimeElapsed);
//
//	//转入回归态时使用
//	void ResetMaxCapacity()
//	{
//		m_Capacity = m_MaxCapacity;
//	}
//
//	CMuxCreature* GetFirst()
//	{
//		if(m_Size == 0)
//			return NULL;
//		else
//			return m_pHead->GetThreatObj();
//	}
//
//	void Dump();
//	void Dump(ThreatList& thlist);
//
//	void CheckNoChangedNode();
//
//private:
//	CThreatNode* m_pHead;
//	CThreatNode* m_pTail;
//
//	int m_Size; //当前的节点数目
//	int m_Capacity; //容量,不是一个固定值,在第一位的node被删除后会变化的
//	int m_MaxCapacity;
//
//	//的的确确要把一个新Node插入到链表中去
//	//算法上，从后往前来寻找插入的位置
//	void InsertNewNode(CMuxCreature* pThreatObj, unsigned int InitValue);
//
//	//适用于这样的情形：m_Size >= 1 && m_Size < m_Capacity
//	void InsertNewNodeX(CMuxCreature* pThreatObj, unsigned int InitValue);
//
//	//获得应该的插入位置
//	CThreatNode* GetInsertPos(CThreatNode* pNewNode);
//
//	bool isHead(CThreatNode* p)
//	{
//		return (p == m_pHead);
//	}
//
//	bool isTail(CThreatNode* p)
//	{
//		return (p == m_pTail);
//	}
//
//	void DelNodeX(CThreatNode* pNode, bool isDied);
//
//	//将30秒钟威胁值未变的Node给删除掉
//	void CleanNoChangedNode();
//};


//#endif //AI_SUP_CODE

typedef ThreatList _ThreatList;

namespace ruleai
{
	class ThreatList
	{
		struct ThreatData
		{
			CMuxCreature * threatener;
			int value;
			time_t lastThreatUpdate;

			const bool operator <(const ThreatData & data)const;

		}; // struct ThreatData

		typedef std::vector<ThreatData> ListType;
		ListType list_;
		unsigned max_size_;
		unsigned max_capacity_;

	public:
		ThreatList(unsigned maxSize = (unsigned)-1);

		~ThreatList();

		void ResetMaxSize(unsigned maxSize);

		const unsigned MaxSize()const;

		void ResetMaxCapacity(unsigned maxSize);

		const unsigned MaxCapacity()const;

		void Clear();

		const unsigned Size()const;

		void Push(CMuxCreature * threatener, int value);

		void UpateThreat(CMuxCreature * threatener, int added);

		void PushOrUpdateThreat(CMuxCreature * threatener, int value);

		void Pop(CMuxCreature * threatener);

		const bool HasCreature(CMuxCreature * threatener)const;

		void GetMaxThreatCreature(CMuxCreature * &threatener);
	
		void CleanThreatNoChangeCreature();

		void Dump(_ThreatList & tl)const;

	}; // class ThreatList

} // namespace ruleai
