﻿//#include "../monster/monsterwrapper.h"
//#include "PatrolPathTemplate.h"
//#include "MonsterSkillSet.h"
//#include "RuleAIEngine.h"
//#include "MonsterUnit.h"
//#include "PlayerUnit.h"
//#include "LuaBinder.h"
//
//#include "../Movement/TargetedMG.h"
////#include "FightControl.h"
////#include "PatrolControl.h"
////#include "FleeControl.h"
//#include "EscortAI.h"
//
#include "../LogicAI/LogicAIEngine.h"
//
#include "RuleAIEngine.h"
CRuleAIEngine gRuleAIEngine;

CRuleAIEngine::CRuleAIEngine()
{
	m_luaState = NULL;
	m_luaState = lua_open();

	luaL_openlibs(m_luaState); 
	logicai::LogicAIEngine::instance().Init();
}

CRuleAIEngine::~CRuleAIEngine()
{
	logicai::LogicAIEngine::instance().Finit();
	lua_close(m_luaState);
}

CRuleAIEngine * CRuleAIEngine::Instance()
{
	static CRuleAIEngine inst;
	return &inst;
}

bool CRuleAIEngine::RegisterUser(unsigned long monsterId)
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}

bool CRuleAIEngine::UnRegisterUser(unsigned long monsterId)
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}

bool CRuleAIEngine::LoadData(const char* RuleAIFile, string& ErrorCode)
{
	ACE_UNUSED_ARG( RuleAIFile );
	ACE_UNUSED_ARG( ErrorCode );
	return true;
}

int CRuleAIEngine::GetAIID()	
{ 
	return 0; 
};

bool CRuleAIEngine::IsResEvent(const MONSTER_STAT& curStat, const MONSTER_EVENT& curEvent)
{ 
	ACE_UNUSED_ARG(curStat);
	ACE_UNUSED_ARG(curEvent);
	return true;
};

bool CRuleAIEngine::CheckTransCon( unsigned long monsterId )
{
	ACE_UNUSED_ARG(monsterId);
	return true; 
};

bool CRuleAIEngine::OnEnter( unsigned long monsterId )
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}

bool CRuleAIEngine::OnExit( unsigned long monsterId )
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}

bool CRuleAIEngine::OnUpdate( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleTimer(monsterId);
	return true;
}

bool CRuleAIEngine::OnPlayerDetected( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleDetectPlayer(monsterId);
	return true;
}

bool CRuleAIEngine::OnPlayerLeaveView( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleLosePlayer(monsterId);
	return true;
}

bool CRuleAIEngine::OnAttackTargetLeave( unsigned long monsterId )
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}

bool CRuleAIEngine::OnTooFarFromBornPt( unsigned long monsterId )
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}

bool CRuleAIEngine::OnSelfHpTooLow( unsigned long monsterId )
{
	ACE_UNUSED_ARG( monsterId);
	return true;
}

bool CRuleAIEngine::OnBeAttacked(unsigned long monsterId)
{
	logicai::LogicAIEngine::instance().HandleAttacked(monsterId);
	return 0;
}

bool CRuleAIEngine::OnNpcDetected( unsigned long monsterId )
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}


bool CRuleAIEngine::OnNpcLeave( unsigned long monsterId )
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}

bool CRuleAIEngine::OnSelfBorn(unsigned long monsterId, const AIMapInfo& mapInfo)
{
	logicai::LogicAIEngine::instance().HandleMonsterBorn(monsterId, mapInfo);
	return 0;
}

bool CRuleAIEngine::OnSelfDie( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleMonsterDied(monsterId);
	return 0;
}

bool CRuleAIEngine::OnDebuff(unsigned long monsterId)
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}

bool CRuleAIEngine::OnFaintStart( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleStartFait(monsterId);
	return true;
}

bool CRuleAIEngine::OnFaintEnd( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleStopFait(monsterId);
	return true;
}

bool CRuleAIEngine::OnBondageStart( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleStartBound(monsterId);
	return 0;
}

bool CRuleAIEngine::OnBondageEnd( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleStopBound(monsterId);
	return 0;
}

bool CRuleAIEngine::OnDebugThreatListStart( unsigned long monsterId)
{
	logicai::LogicAIEngine::instance().HandleStartNotifyThreatList(monsterId);
	return true;
}

bool CRuleAIEngine::OnDebugThreatListEnd( unsigned long monsterId)
{
	logicai::LogicAIEngine::instance().HandleStopNotifyThreatList(monsterId);
	return true;
}

bool CRuleAIEngine::OnPlayerOutAtkRange(unsigned long )
{
	return true;
}

bool CRuleAIEngine::OnStartFollowing( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleStartFollow(monsterId);
	return true;
}

bool CRuleAIEngine::OnFollowPlayerStartMoving( unsigned long monsterId )
{
	ACE_UNUSED_ARG( monsterId );
	return true;
}

bool CRuleAIEngine::OnMonsterEnterView( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleDetectMonster(monsterId);
	return true;
}

bool CRuleAIEngine::OnMonsterLeaveView( unsigned long monsterId )
{
	logicai::LogicAIEngine::instance().HandleLoseMonster(monsterId);
	return true;
}

bool CRuleAIEngine::ExeRuleLogic(unsigned long, MONSTER_EVENT)
{
	return true;
}

bool CRuleAIEngine::OnMonsterBeChat( unsigned long monsterID )
{
	logicai::LogicAIEngine::instance().HandleBeginChat(monsterID);
	return true;
}

bool CRuleAIEngine::OnSurPlayerMovChg( unsigned long monsterID )
{
	logicai::LogicAIEngine::instance().HandleMoveChange(monsterID);
	return true;
}

bool CRuleAIEngine::OnMonsterBuffStart(unsigned long monsterID)
{
	logicai::LogicAIEngine::instance().HandleMosterBuffStart(monsterID);
	return true;
}

bool CRuleAIEngine::OnMonsterBuffEnd(unsigned long monsterID)
{
	logicai::LogicAIEngine::instance().HandleMonsterBuffEnd(monsterID);
	return true;
}

bool CRuleAIEngine::OnCalledMonsterBorn(unsigned long monsterID)
{
	//logicai::LogicAIEngine::instance().HandleCreateNewLacquey(monsterID);
	return true;
}

bool CRuleAIEngine::OnAttackCityStart( unsigned long monsterID )
{
	ACE_UNUSED_ARG( monsterID );
	return true;
}


bool CRuleAIEngine::OnAttackCityEnd( unsigned long monsterID )
{
	ACE_UNUSED_ARG( monsterID );
	return true;
}


bool CRuleAIEngine::OnFollowingPlayerBeAttack( unsigned long monsterID )
{
	ACE_UNUSED_ARG( monsterID );
	logicai::LogicAIEngine::instance().HandlePlayerAttacked(monsterID);
	return true;
}


bool CRuleAIEngine::OnPlayerAddHp( unsigned long monsterID )
{
	logicai::LogicAIEngine::instance().HandlePlayerCured(monsterID);
	return true;
}

bool CRuleAIEngine::OnPlayerDie( const PlayerID& id )
{
	logicai::LogicAIEngine::instance().HandlePlayerDied(id);
	return true;
}


bool CRuleAIEngine::OnPlayerDropTask(unsigned long monsterID )
{
	logicai::LogicAIEngine::instance().HandlePlayerDropTask(monsterID);
	return true;
}

bool CRuleAIEngine::OnCallMonsterBorn( unsigned int monsterID, unsigned int aiFlag )
{
	logicai::LogicAIEngine::instance().HandleCreateNewLacquey(monsterID, aiFlag);
	return true;
}

bool CRuleAIEngine::OnPlayerDie( unsigned long monsterID )
{
	logicai::LogicAIEngine::instance().HandlePlayerDied(monsterID);
	return true;
}

bool CRuleAIEngine::OnPlayerLeaveMap(const PlayerID & id, bool isSwitchMap)
{
	logicai::LogicAIEngine::instance().HandlePlayerLeaveMap(id, isSwitchMap);
	return true;
}

bool CRuleAIEngine::OnBeKickBack( unsigned long monsterID )
{
	logicai::LogicAIEngine::instance().OnBeKickBack(monsterID);
	return true;
}