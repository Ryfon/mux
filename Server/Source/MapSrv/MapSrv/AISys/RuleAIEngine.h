﻿#pragma once
#include "../monster/monster.h"
#include "../Fsm/IFsm.h"

#//include "AIRuleReposi.h"
//#include "AIRuleSetReposi.h"
//#include "StrResource.h"
//#include "AISys.h"
//class CAISys;
#include "../LogicAI/LogicAIEngine.h"

class CRuleAIEngine :
	public IMonsterFsm
{
public:
	//typedef std::map<CMonster*, CAISys*> MONSTER_AI_MAP;
	lua_State* m_luaState;

	CRuleAIEngine(void);

	~CRuleAIEngine(void);

	static CRuleAIEngine* Instance();

	lua_State* GetLuaState() { return m_luaState; };

	bool RegisterUser(unsigned long monsterId);

	bool UnRegisterUser(unsigned long monsterId);

	//CAISys* GetAISys(CMonster* pMonster);

	bool LoadData(const char* RuleAIFile, string& ErrorCode);

	///取该AI状态机的AI编号；
	int GetAIID();

	///状态机是否响应特定状态下的特定事件；
	bool IsResEvent(const MONSTER_STAT& curStat, const MONSTER_EVENT& curEvent);

	///检查是否满足转换条件，如果满足则执行转换；
	bool CheckTransCon(unsigned long monsterId);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	应该明白，在这些事件响应函数的实现中，应该包含两部分内容：
	（1）固定的逻辑，维持AI各组件的持续运转。用C++语言固化。
	（2）执行行为规则中定义的逻辑。
	*/

	//以下为事件响应函数，响应特定事件...
	bool OnEnter(unsigned long monsterId);

	///退出状态；
	bool OnExit(unsigned long monsterId);

	///定时执行的更新函数(由程序框架定时调用，调用间隔目前初定约100毫秒)，目前脚本中不设此函数，但C++接口可能需要；
	bool OnUpdate(unsigned long monsterId);

	///发现玩家；
	bool OnPlayerDetected(unsigned long monsterId);

	///玩家离开视野；
	bool OnPlayerLeaveView(unsigned long monsterId);

	///攻击目标离开视野；
	bool OnAttackTargetLeave(unsigned long monsterId);

	///自身离开出生点太远；
	bool OnTooFarFromBornPt(unsigned long monsterId);

	///自身HP太小；
	bool OnSelfHpTooLow(unsigned long monsterId);

	///自身被打；
	bool OnBeAttacked(unsigned long monsterId);

	///发现NPC;
	bool OnNpcDetected(unsigned long monsterId);

	///NPC离开;
	bool OnNpcLeave(unsigned long monsterId);

	///怪物自身出生
	bool OnSelfBorn(unsigned long monsterId, const AIMapInfo & mapInfo);

	///怪物自身死亡
	bool OnSelfDie(unsigned long monsterId);

	bool OnDebuff(unsigned long monsterId);

	bool OnFaintStart(unsigned long monsterId);

	bool OnFaintEnd(unsigned long monsterId);

	bool OnBondageStart(unsigned long monsterId);

	bool OnBondageEnd(unsigned long monsterId);

	bool OnDebugThreatListStart(unsigned long monsterId);

	bool OnDebugThreatListEnd(unsigned long monsterId);

	//攻击者离开怪物的可攻击范围,该事件不由服务器击发,由AI来击发
	bool OnPlayerOutAtkRange(unsigned long monsterId);

	bool OnStartFollowing(unsigned long monsterId);

	bool OnFollowPlayerStartMoving(unsigned long monsterId);

	///怪物进入视野，收到此事件后，通过GetAIEventInfo取得具体的进入视野怪物信息；
	bool OnMonsterEnterView(unsigned long monsterId);

	///怪物离开视野，收到此事件后，通过GetAIEventInfo取得具体的离开视野怪物信息；
	bool OnMonsterLeaveView(unsigned long monsterId);

	bool OnMonsterBeChat( unsigned long monsterID );

	bool OnSurPlayerMovChg( unsigned long monsterID );

	bool OnMonsterBuffStart( unsigned long monsterID );

	bool OnMonsterBuffEnd( unsigned long monsterID );

	bool OnCalledMonsterBorn(unsigned long monsterID);

	bool OnAttackCityStart( unsigned long monsterID );
	bool OnAttackCityEnd( unsigned long monsterID );
	bool OnFollowingPlayerBeAttack( unsigned long monsterID );
	bool OnPlayerAddHp( unsigned long monsterID );
	//当玩家丢弃任务时，检测是否需要删除跟随的npc
	//bool OnPlayerDropTask( unsigned int taskID, unsigned long monsterID );
	bool OnPlayerDropTask( unsigned long monsterID );
	bool OnPlayerDie( unsigned long monsterID );
	bool OnBeKickBack( unsigned long monsterID );

	static bool OnPlayerDie( const PlayerID& id );
	static bool OnCallMonsterBorn( unsigned int monsterID, unsigned int aiFlag );
	static bool OnPlayerLeaveMap(const PlayerID & id, bool isSwitchMap);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	//void DoTransStateEx(MONSTER_STAT newState, CMonster* pMonster);

	bool ExeRuleLogic(unsigned long monsterId, MONSTER_EVENT Event);

private:
	//bool ExeAction(MonsterAction& Action, CMonster* pMonster);

	//内部的辅助函数
	//void DoSpeak(MonsterAction& Action, CMonster* pMonster);
	//void DoTransState(MonsterAction& Action, CMonster* pMonster);
	//void DoAddSkill(MonsterAction& Action, CMonster* pMonster);
	//void DoRandFight(MonsterAction& Action, CMonster* pMonster);
	//void DoSeriFight(MonsterAction& Action, CMonster* pMonster);

	//void DoRandPatrol(MonsterAction& Action, CMonster* pMonster);
	//void DoPathPatrol(MonsterAction& Action, CMonster* pMonster);
	//void DoResumePatrol(MonsterAction& Action, CMonster* pMonster);

	//void DoSetActAtkFlag(MonsterAction& Action, CMonster* pMonster);
	//void DoStartFlee(MonsterAction& Action, CMonster* pMonster);
	//void DoSetCapaThList(MonsterAction& Action, CMonster* pMonster);
	//void DoStartFight(MonsterAction& Action, CMonster* pMonster);

	//void DoKeepIdle(MonsterAction& Action, CMonster* pMonster);

	//void DoStartReturn(MonsterAction& Action, CMonster* pMonster);

	//void DoSetSpeed(MonsterAction& Action, CMonster* pMonster);

	//调用Lua中计算威胁值的函数
	//int CalThreatByF(int nDamage);
	//调用Lua中计算Debuff技能伤害造成的威胁值函数
	//int CalThreatByG(int nDamage);

	//检查某个函数是否在Lua脚本有定义
	//bool CheckFuncDef(const char* FuncName);

	//CAIRuleReposi		m_RuleReposi;
	//CAIRuleSetReposi	m_RuleSetReposi;
	//CStrResource        m_StrResource;

	//MONSTER_AI_MAP   m_AISysSet; //系统中所有怪物的AI部件集合

private:
	//CRuleAIEngine( const CRuleAIEngine& r) { } 
	//CRuleAIEngine& operator = ( const CRuleAIEngine& r) { }

	//bool LoadDefaultSpeeds(string& ErrorCode);

	//bool LoadDefaultSpeed(lua_State* L, string& SpeedVarName,
	//		float* Speed, string& ErrorCode);

};

extern CRuleAIEngine gRuleAIEngine;
