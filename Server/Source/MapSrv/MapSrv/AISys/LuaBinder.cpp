﻿///** @file PlayerBinder.cpp 
//@brief 
//<pre>
//*	Copyright (c) 2007，第九城市游戏研发中心
//*	All rights reserved.
//*
//*	当前版本：
//*	作    者：傅晓强
//*	完成日期：2008-06-23
//*   详细说明：以Light User Data的方式，将CMonster实例的方法和属性暴露给Lua脚本。
//*             
//*	取代版本：
//*	作    者：
//*	完成日期：
//</pre>*/
//
//#include "../Lua/include/lua.hpp"
//#include "MonsterUnit.h"
//#include "Binder.h"
//
//#define MONSTER_TYPE_IN_LUA  "Monster"
//
////这个函数是在C++中使用的，用于在脚本初始化阶段，将CPlayer*以
////轻用户数据的方式植入Lua环境中。具体实现使用了Lua中注册表的方式。
////同后面的函数从性质上截然不同。
////----------------C++ Library Fuctions Used in Lua-------------------------------------
////结合了已有的绑定机制。
////在我们的应用中，在为机器人创建脚本时，会把<threadState, CSript*>记录到Lua的全局表(LUA_GLOBALSINDEX)中。
////在获得CPlayer时，我们先取出CSript*，由它获得相关联的CPlayer*。
////CBinder binder(L);
//
//static int bnd_GetHPPercent(lua_State* L)
//{
//	CBinder binder(L);
//	lua_getfield(L, LUA_REGISTRYINDEX, "this");
//	CMonsterUnit* pUnit = (CMonsterUnit*)binder.checkusertype(1,MONSTER_TYPE_IN_LUA);
//	binder.pushnumber(pUnit->GetHPPercent());
//	return 1;
//}
//
//static int bnd_GetHP(lua_State* L)
//{
//	CBinder binder(L);
//
//	lua_getfield(L, LUA_REGISTRYINDEX, "this");
//	CMonsterUnit* pUnit = (CMonsterUnit*)binder.checkusertype(-1,MONSTER_TYPE_IN_LUA);
//	lua_pop(L, 1);
//	binder.pushnumber(pUnit->GetHP());
//	return 1;
//}
//
//static int bnd_GetBPDist(lua_State* L)
//{
//	CBinder binder(L);
//
//	lua_getfield(L, LUA_REGISTRYINDEX, "this");
//	CMonsterUnit* pUnit = (CMonsterUnit*)binder.checkusertype(-1,MONSTER_TYPE_IN_LUA);
//	lua_pop(L, 1);
//
//	TYPE_POS mx, my, hx, hy;
//	pUnit->GetPosition(mx, my);
//	pUnit->GetSpawnedPoint(hx, hy);
//
//	int dist = abs(mx - hx) + abs(my - hy); 
//	binder.pushnumber(dist);
//	return 1;
//}
//
//int PushMonsterPtr(lua_State* L, CMonsterUnit* pUnit)
//{
//	lua_pushlightuserdata(L, (void*)pUnit);
//	lua_setfield(L, LUA_REGISTRYINDEX, "this");
//	return 1;
//}
//
//static const luaL_reg monsterlib[] = 
//{	
//	{"GetHP",				bnd_GetHP},
//	{"GetHPPercent",		bnd_GetHPPercent},
//	//{"GetMana",				bnd_GetMana},	
//	//{"GetManaPercent",      bnd_GetManaPercent},
//	{"GetBPDist",			bnd_GetBPDist},
//
//
//	{NULL, NULL}
//};
//
//int luaopen_monster(lua_State* L)
//{
//	CBinder binder(L);
//	binder.init("Monster", monsterlib);
//	return 1;
//}
///*
//void stackDump(lua_State* L)
//{
//	int i;
//	printf("----begin of dump stack----\n");
//	int top = lua_gettop(L);
//	for(i = 1; i<= top; i++)
//	{
//		int t = lua_type(L, i);
//		switch(t)
//		{
//		case LUA_TSTRING:
//			{
//				printf("'%s'", lua_tostring(L, i));
//				break;
//			}
//		case LUA_TBOOLEAN:
//			{
//				printf(lua_toboolean(L, i) ? "true" : "false");
//				break;
//			}
//		case LUA_TNUMBER:
//			{
//				printf("%g", lua_tonumber(L, i));
//				break;
//			}
//
//		case LUA_TLIGHTUSERDATA:
//			{
//				void* udata = lua_touserdata(L,i);
//				printf("lightuserdata:%x", udata);
//
//				//if(udata == NULL) 
//				//	luaL_typerror(L,index,tname);    
//
//				break;
//			}
//
//		case LUA_TFUNCTION:
//			{
//				printf("function");
//				break;
//			}
//
//		default:
//			{
//				printf("%s", lua_typename(L, i));
//				break;
//			}
//
//		}
//		printf(",");  
//	}
//	printf("\n----end of dump stack----\n");
//}
//*/
//
///*
////返回true，表示计算过程顺利完成，条件表达式的真值从result中获得
////返回false，表示计算过程中出错，出错原因保存在ErrorCode，
//bool EvaCondExp(lua_State* L, 
//				CMonsterUnit* pUnit,
//				const char* exp, 
//				bool& result, 
//				string& ErrorCode)
//{
//	bool NoError;
//	stringstream buf;
//	buf << "return (" <<  exp << ");";
//	string str = buf.str();
//
//	PushMonsterPtr(L, pUnit);
//
//	//stackDump(L);
//	int error = luaL_dostring(L, str.c_str());
//
//	if(error == 0) //successfully execute
//	{
//		//r == 1 for true and other value
//		//r == 0 for false and nil
//		int r = lua_toboolean(L, -1);
//		result = ( r == 1) ? true : false;
//
//		lua_pop(L, 1);
//		NoError = true;
//	}
//	else
//	{
//		//Error happened
//		//ErrorCode = string(lua_tostring(L, -1));
//		ErrorCode = lua_tostring(L, -1);
//		lua_pop(L, 1);
//		NoError = false;
//	}
//
//	//PopMonsterPtr(L);
//	return NoError;
//}
//*/
