﻿#include "Unit.h"
#include <math.h>
#include <stdlib.h>
#include "Utility.h"

CUnit::CUnit(UNITTYPE unitType)
{
	m_Type = unitType;
}

CUnit::~CUnit(void)
{

}

int CUnit::GetDistance(CUnit* pUnit1, CUnit* pUnit2)
{
	if (NULL == pUnit1 || NULL == pUnit2)
	{
		D_WARNING( "CUnit::GetDistance传入pUnit 空" );
		return 0;
	}


	TYPE_POS x1, y1, x2, y2;

	pUnit1->GetPosition(x1, y1);
	pUnit2->GetPosition(x2, y2);

	double dx = x1 - x2;
	double dy = y1 - y2;

	int dist = (int)(sqrt(dx*dx + dy*dy));
	return dist;
}

