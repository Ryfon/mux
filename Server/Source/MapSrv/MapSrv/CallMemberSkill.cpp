﻿#include "CallMemberSkill.h"
#include "Player/Player.h"
#include "MuxMap/muxmapbase.h"

CallMember::CallMember():
m_posX(0),
m_posY(0),
m_mapID(0),
m_teamID(0),
m_callMemberID(0),
m_time(0)
{

}

void CallMember::AddCalledMember(const PlayerID& playerID )
{
	m_calledMember.insert(playerID);
}

void CallMember::SetMapInfo(TYPE_POS posX, TYPE_POS posY, unsigned short mapID,unsigned int teamID)
{
	m_posX = posX;
	m_posY = posY;
	m_mapID = mapID;
	m_teamID = teamID;
}

void CallMember::OnReplyCall(CPlayer * pPlayer, int isAgree)
{
	if ( NULL == pPlayer)
	{
		return;
	}
	PlayerID playerID = pPlayer->GetFullID();
	set<PlayerID>::iterator it = m_calledMember.find(playerID);
	if (it != m_calledMember.end())
	{
		m_calledMember.erase(it);
		if ( 0 == isAgree)
		{//不同意
			
		}
		else
		{//同意
			if (pPlayer->GetMapID() == m_mapID)
			{//在同一张地图内
				CMuxMap* pMap = pPlayer->GetCurMap();
				if ( NULL != pMap )
				{
					pMap->OnPlayerInnerJump( pPlayer, m_posX, m_posY );
				}
			}
		}
	}
}

void CallMember::SetID(unsigned int id)
{
	m_callMemberID = id;
}

void CallMember::SetTime(unsigned int time)
{
	m_time = time;
}

CallMember * CallMemberManager::GetCallMember(unsigned int id)
{
	map<unsigned int,CallMember*>::iterator it = m_callMemberMap.find(id);
	if (it == m_callMemberMap.end())
	{
		return NULL;
	}
	return it->second;
}

CallMember * CallMemberManager::CreateCallMember()
{
	CallMember * callMember = NEW CallMember();
	callMember->SetID(m_count++);
	callMember->SetTime(GetTickCount());
	return callMember;
}

void CallMemberManager::AddCallMember(CallMember* callMember)
{
	if ( NULL == callMember )
	{
		return;
	}
	m_callMemberMap.insert(make_pair(callMember->m_callMemberID,callMember));
}

void CallMemberManager::DelCallMember(unsigned int id)
{
	map<unsigned int,CallMember*>::iterator it = m_callMemberMap.find(id);
	if (it == m_callMemberMap.end())
	{
		return;
	}
	pair<unsigned int, unsigned int> p;
	p.first = it->second->m_mapID;
	p.second = it->second->m_teamID;
	m_callMemberIndex.erase(p);
	delete it->second;
	it->second = NULL;
	m_callMemberMap.erase(it);
}

bool CallMemberManager::StartCallMember(CPlayer* pPlayer)
{
	if ( NULL == pPlayer )
	{
		return false;
	}
	if (pPlayer->IsInTeam())
	{
		CGroupTeam* pTeam = pPlayer->GetSelfStateManager().GetGroupState().GetGroupTeam();
		if ( NULL == pTeam )
		{
			D_ERROR("CallMemberManager::StartCallMember NULL == pTeam\n");
			return false;
		}
		unsigned int mapID = pPlayer->GetMapID();
		unsigned int teamID = pTeam->GetGroupTeamID();
		//检查是否在同一张地图上有队伍的其他成员使用召唤
		pair<unsigned int,unsigned int> check;
		check.first = mapID;
		check.second = teamID;
		if (m_callMemberIndex.find(check) != m_callMemberIndex.end())
		{
			unsigned int callMemberID = m_callMemberIndex[check];
			CallMember * callMember = GetCallMember(callMemberID);
			if (NULL == callMember)
			{
				m_callMemberIndex.erase(check);
			}
			else
			{
				unsigned int now = GetTickCount();
				if (now - callMember->m_time > CALL_OVER_TIME)
				{//上次的召唤已完成
					DelCallMember(callMemberID);
					m_callMemberIndex.erase(check);
				}
				else
				{//一个队伍不能同时使用召唤
					return false;
				}
			}
		}

		CallMember * newCallMember = CreateCallMember();
		if ( NULL == newCallMember )
		{
			return false;
		}
		TYPE_POS posX = pPlayer->GetPosX();
		TYPE_POS posY = pPlayer->GetPosY();
		newCallMember->SetMapInfo(posX,posY,mapID,teamID);
		AddCallMember(newCallMember);
		m_callMemberIndex.insert(make_pair(check,newCallMember->GetCallMemberID()));

		MGInviteCallMember invite;
		invite.inviteCallMember.callMemberID = newCallMember->GetCallMemberID();
		invite.inviteCallMember.mapID = mapID;
		invite.inviteCallMember.posX = posX;
		invite.inviteCallMember.posY = posY;
		invite.inviteCallMember.nameSize = MAX_NAME_SIZE;
		SafeStrCpy(invite.inviteCallMember.callName,pPlayer->GetNickName());
		invite.inviteCallMember.callPlayerID = pPlayer->GetFullID();

		vector<CPlayer*> teamMember;
		pTeam->GetTeamMemberOnTheMap(mapID,teamMember);
		for (unsigned int i = 0; i<teamMember.size(); ++i)
		{
			CPlayer * targetPlayer = teamMember[i];
			if ( NULL == targetPlayer )
			{
				continue;
			}
			if (targetPlayer->GetFullID() != pPlayer->GetFullID())
			{
				targetPlayer->SendPkg<MGInviteCallMember>(&invite);
				newCallMember->AddCalledMember(targetPlayer->GetFullID());
			}
		}
		return true;

	}
	return false;
}

void CallMemberManager::OnReplyCallMember(CPlayer * pPlayer,int isAgree,unsigned int callMemberID)
{
	CallMember * callMember = GetCallMember(callMemberID);
	if (NULL == callMember)
	{
		return;
	}
	unsigned int time = GetTickCount();
	if (time - callMember->m_time > CALL_OVER_TIME)
	{//已经过期
		DelCallMember(callMemberID);
		return;
	}
	callMember->OnReplyCall(pPlayer,isAgree);
	if (callMember->IsMemberEmpty())
	{
		DelCallMember(callMemberID);
	}
}
