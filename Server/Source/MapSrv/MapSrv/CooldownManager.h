﻿


#ifndef COOLDOWN_H
#define COOLDOWN_H


#include <map>
#include "Utility.h"
#include "../../Base/PkgProc/CliProtocol.h"
using namespace std;

class CCooldownManager
{
public:
	//检查技能是否在cooldown时间内,true表示可以使用,false表示不能使用
	bool CheckCooldown( TYPE_ID cdType, const ACE_Time_Value& cooldownTime );

	void OfflineProc();  //下线的时候要COOLDOWN存盘

	

private:
	map<TYPE_ID, ACE_Time_Value> m_mapCdTimer;

};

#endif
