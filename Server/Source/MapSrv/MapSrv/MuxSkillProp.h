﻿#pragma once
#include <vector>
#include "PkgProc/CliProtocol_T.h"
#include "Utility.h"
using namespace std;
using namespace MUX_PROTO;


#define MASK_CLASS_ARCHER	1
#define MASK_CLASS_MAGE		2
#define MASK_CLASS_BLADER	4
#define MASK_CLASS_PALADIN	8
#define MASK_CLASS_ASSASS	16
#define MASK_CLASS_WARRIOR	32

typedef struct strMuxPlayerDamageProp
{
	unsigned int mID;
	float		 fAttack;
	unsigned int mNumAffix;
	unsigned int mNum;
	unsigned int mNumDamage;
	unsigned int mCriAddDamage;
	unsigned int mFactTime;
	unsigned int mEffectMoveSpeed;
}MuxPlayerDamageProp;

typedef struct strMuxNpcDamageProp
{
	unsigned int mID;
	float fHit;
	float addHit;
	float nCritical;
	unsigned int nAddAtt;
	unsigned int attMin;
	unsigned int attMax;
}MuxNpcDamageProp;

typedef struct strMuxItemSkillProp
{
	unsigned int mID;
	unsigned int HealType;
	unsigned int DamageMethod;
	unsigned int MinHeal;
	unsigned int MaxHeal;
}MuxItemSkillProp;

typedef struct strMuxBuffProp
{
	unsigned int mID;
	unsigned int BuffLevel;
	unsigned int SubType;
	unsigned int AssistChoice;
	unsigned int AssistType;
	unsigned int PushIndex;
	unsigned int PushRule;	
	unsigned int nPeriodOfTime;
	float		 LinkRate;
	unsigned int AssistObject;
	unsigned int nActionSpace;
	unsigned int Value;
	unsigned int AffectLowestLevel; 
	bool		 isDeathClear;
	bool		 isOffLineSave;
	bool		 isRightClickClear;
	unsigned int bgType;
	unsigned int TierNum;
	vector<strMuxBuffProp*> vecBuffProp;
}MuxBuffProp;

typedef struct strMuxSpiltProp
{
	unsigned int mID;
	unsigned int StartTime;
	unsigned int MidwaySpeed;
	unsigned int EndTime;
	unsigned int DamageSplit;
	unsigned int MinMidwayTime;
}MuxSpiltProp;

class CMuxSkillProp
{
public:
	CMuxSkillProp();
	~CMuxSkillProp();

public:
	//是否是范围技能
	bool IsAoeSkill();
	//是否是工会技能
	bool IsGuildSkill();
	//是否是好技能
	bool IsGoodSkill();
	//是否是普通攻击
	bool IsNormalSkill();
	//获得AOE的范围
	unsigned int GetAoeRange();
	//是否是SP技能
	bool IsSpSkill();
	//获取下一个级别的ID号
	unsigned int GetNextSkillID();

	bool IsAssistSkill()
	{
		return (m_pBuffProp != NULL) || (m_pMuxItemSkillProp != NULL) || (m_relieveID != 0);
	}


	int GetClassMask(unsigned int job )
	{
		switch(job)
		{
		case CLASS_WARRIOR:   //剑士
			return MASK_CLASS_WARRIOR;
			break;

		case CLASS_ASSASSIN:  //刺客
			return MASK_CLASS_ASSASS;
			break;

		case CLASS_PALADIN:  //圣骑士
			return MASK_CLASS_PALADIN;
			break;

		case CLASS_BLADER:  //魔剑士
			return MASK_CLASS_BLADER;
			break;

		case CLASS_HUNTER:  //弓手
			return MASK_CLASS_MAGE;
			break;

		case CLASS_MAGE:  //魔法师
			return MASK_CLASS_ARCHER;
			break;

		default:
			D_ERROR("传入职业编号为无效的职业编号\n");
			return 0;
			break;
		}
	}

	bool IsClassSkill(unsigned int job)
	{
		return (m_nClass & GetClassMask( job )) > 0 ;
	}


public:
	unsigned int m_skillID;					//技能ID编号
	unsigned int m_skillType;				//技能类型
	unsigned int m_upgradeLevel;			
	unsigned int m_target;			//"1：自己 2：队伍  3：对象 4：地面"
	unsigned int m_shape;			// 0：单一目标 1：圆（自身为圆心）2：圆（目标为圆心）3：线形（自身为起点）4：线形（目标为起点）
	unsigned int m_range;			//作用范围
	unsigned int m_area;			//作用射程，当shape＝3or4时起作用
	unsigned int m_kickBackRange;	//击退距离
	bool         m_deadKickBack;    //死亡时是否击退
	unsigned int m_upgradePoint;	
	unsigned int m_publicCDType;
	unsigned int m_spLevel;
	unsigned int m_skillDegree;
	unsigned int m_nUllageSp;
	unsigned int m_nUllageMp;
	float		 m_nUllageMpPer;				//按百分比扣除
	unsigned int m_nUllageItem;
	unsigned int m_nItemNum;
	unsigned int m_nCurseTime;
	unsigned int m_nCoolDown;
	unsigned int m_nClass;
	unsigned int m_nAddExp;
	unsigned int m_nNextSkillID;
	unsigned int m_nAttDistMin;
	unsigned int m_nAttDistMax;
	unsigned int m_NpcRank;
	unsigned int m_MaxTargetLevel;
	unsigned int m_MinTargetLevel;
	unsigned int m_BasicSkill;
	unsigned int m_level;
	unsigned int m_upgradeExp;			//升到本级需要的经验值
	unsigned int m_upgradeMoney;		//升到本级需要金钱
	unsigned int m_upgradeStr;			//升到本级需要力量
	unsigned int m_upgradeVit;			//升到本级需要体质
	unsigned int m_upgradeAgi;			//升到本级需要敏捷
	unsigned int m_upgradeSpi;			//升到本级需要精神
	unsigned int m_upgradeInt;			//升到本级需要智力
	unsigned int m_subExp;				//升到本级需要扣除的经验值
	unsigned int m_subMoney;			//升到本级需要扣除的金钱
	unsigned int m_weaponSkill;			//判断技能是否装备武器才生效 0：不装备武器即生效 1：装备主手武器才生效 2：装备副手武器才生效 3：主副手均装备武器才生效"
	unsigned int m_castTime;			//技能需要读条的时间（单位：秒）
	bool m_isGoodSkill;					//好技能还是坏技能
	bool m_isNormalSkill;				//是否是普通攻击
	bool m_isGuildSkill;				//是否是公会技能
	//以下为子表的ID
	MuxPlayerDamageProp*	m_pDamageProp;
	MuxNpcDamageProp*		m_pNpcDamageProp;
	MuxItemSkillProp*      m_pMuxItemSkillProp;
	MuxBuffProp*			m_pBuffProp;
	MuxSpiltProp*			m_pMuxSpiltProp;
	unsigned int m_relieveID;			//解除型ID
};

