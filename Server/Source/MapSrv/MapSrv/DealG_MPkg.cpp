﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: hyn
* 完成日期: 2007.12.17
*
*/

#include "DealG_MPkg.h"
#include "PkgProc/SrvProtocol.h"
#include "G_MProc.h"
#include "MuxMap/MapCommonHead.h"
#include "MuxMap/mannormalmap.h"
//移至gatesrv #include "MapTeleporterManager.h"
#include "monster/monster.h"
#include "Item/CItemPackage.h"
#include "Item/UseItemRule.h"
#include "Item/CEquipItemManager.h"
#include "Item/CItemIDGenerator.h"
#include "Item/ItemManager.h"
#include "Item/RepairWearRule.h"
#include "GroupTeamManager.h"
#include "Rank/rankeventmanager.h"
#include "npcshop/npcshop.h"
#include "BattleUtility.h"
#include "Union/UnionManager.h"
#include "Item/ItemBuilder.h"
#ifdef WAIT_QUEUE
	#include "WaitQueue.h"
#endif /*WAIT_QUEUE*/

#ifdef OPEN_PUNISHMENT
	#include "Activity/PunishmentManager.h"
#endif /*OPEN_PUNISHMENT*/

#ifdef OPEN_PET
	#include "Player/MuxPet.h"
#endif  //OPEN_PET

#include "NpcStandRule.h"
#include "Activity/WarTaskManager.h"

#include "MuxMap/manmuxcopy.h"
#include "RaceManager.h"
#include "ItemAttributeHelper.h"
#include "ManPlayerProp.h"
#include "CallMemberSkill.h"

extern unsigned int g_maxMapSrvPlayerNum;

void CDealG_MPkg::Init()
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//#define G_M_DBGET_PRE                    0x15ca     //DB信息开始;
	Register( G_M_DBGET_PRE, &OnDbGetPre );
	//#define G_M_DBGET_POST                   0x15cb     //DB信息结束(与G_M_DBGET_PRE一一对应);
	Register( G_M_DBGET_POST, &OnDbGetPost );
	//#define G_M_PLAYER_ENTER_MAP             0x15cc     //玩家离开地图(在G_M_DBGET_POST之后发送);
	Register( G_M_PLAYER_ENTER_MAP, &OnPlayerEnterMap );
	//#define G_M_DBGET_INFO                   0x15cd     //DB取到的信息；
	Register( G_M_DBGET_INFO, &OnDbGetInfo );
	//#define G_M_NDSMR_INFO                   0x15ce     //no db, switch map reserve信息；
	Register( G_M_NDSMR_INFO, &OnNDSMRInfo );
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Register( G_M_RUN, &OnPlayerRun );//玩家跑动的处理
	Register( G_M_PLAYER_BEYONDBLOCK, &OnPlayerBeyondBlock );//玩家跑动中跨块；

	Register( G_M_USE_MONEY_TYPE, &OnGMUseMoneyType );//玩家设置所使用的钱币；
	Register( G_M_HONOR_MASK, &OnGMHonorMask );//玩家选择显示的称号；
	Register( G_M_BIAS_INFO, &OnGMBiasInfo );//排行榜阈值相关信息；

	//Register( G_M_PLAYER_APPEAR, &OnPlayerAppear );
	Register( G_M_PLAYER_LEAVE, &OnPlayerLeave );
	Register( G_M_ATTACK_MONSTER, &OnPlayerAttackMonster );//by dzj, 07.12.27;
	//Register( G_M_PLAYER_COMBO, &OnPlayerCombo );
	Register( G_M_REQ_ROLEINFO, &OnReqRoleInfo );
	Register( G_M_REQ_MONSTERINFO, &OnReqMonsterInfo );
//	Register( G_M_PLAYER_PKG, &OnPlayerPkg );
	Register( G_M_CHAT, &OnPlayerChat );
	Register( G_M_REQ_MANED_MAPS, &OnReqMannedMap );  //by hyn 03.27
	Register( G_M_ATTACK_PLAYER, &OnPlayerAttackPlayer );
//移至gatesrv	Register( G_M_SWITCH_MAP, &OnSwitchMap);
	Register( G_M_SCRIPT_CHAT, &OnPlayerScriptChat );//玩家发来与脚本对话信息；
	Register( G_M_FULL_PLAYER_INFO, &OnFullPlayerInfo );
	Register( G_M_DEFAULT_REBIRTH, &OnDefaultRebirth );//玩家倒计时时间到，或者玩家主动选择至复活点复活;
	//Register( G_M_REBIRTH_READY, &OnRebirthReady );//玩家复活点复活准备完毕，收到后SRV应将玩家加入地图，并向周围人广播;

//道具
	Register( G_M_QUEARY_ITEMPACK,&OnQueryItemPkgInfo );//玩家查询道具包信息
	Register( G_M_PICKITEM, &OnPickItemFromPkg );//玩家拾取一个道具时
	Register( G_M_PICKALLITEM, &OnPickAllItemFromPkg );//玩家拾取所有道具时
	Register( G_M_BATCH_ITEM_SEQUENCE ,&OnQueryNewItemRange );//请求新的道具ID范围
	Register( G_M_USEITEM, &OnUseItem );//使用道具
	Register( G_M_UNUSEITEM, &OnUnUseItem );//不使用道具
	Register( G_M_SWAPITEM, &OnSwapItem);//交换道具
	Register( G_M_DROPITEM, &OnDropItem );//玩家丢弃道具
	Register( G_M_SPLITITEM,&OnSplitItem );//玩家分解道具
	Register( G_M_EQUIPITEM,&OnEquipItem );//玩家使用装备道具

//工会
	Register( G_M_REGISTER_UNION_MEMBER, &OnRegisterUnionMember );	//注册工会成员
	Register( G_M_REPORT_UNION_SKILLS, &OnReportUnionSkills );	//通告工会技能
	Register( G_M_REPORT_UNION_LEVEL, &OnReportUnionLevel );   //通告工会等级
	Register( G_M_UNREISTER_UNION_MEMBER, &OnUnregisterUnionMember );	//取消工会成员注册
	Register( G_M_ISSUE_UNION_TASKS, &OnIssueUnionTask );	//发布任务成功
	Register( G_M_CHANGE_UNION_ACTIVE, &OnChanageUnionActive );	//通知工会活跃度改变
	Register( G_M_CHANAGE_UNION_PRESTIGE, &OnChanageUnionPrestige );//通知工会威望改变
	Register( G_M_CHANAGE_UNION_BADGE, &OnChanageUnionBadge );//通知工会徽章改变
	Register( G_M_FORWARD_UNION_CMD, &OnForwardUnionCmd );
	Register( G_M_CREATE_UNION_ERR_NTF, &OnCreateUnionErrNtf);

//骑乘
	Register( G_M_ACTIVEMOUNT,&OnActiveMountItem );//玩家激活骑乘道具
	Register( G_M_INVITEMOUNT,&OnInvitePlayerJoinMountTeam);//玩家邀请别人加入骑乘队伍
	Register( G_M_AGREE_BEINVITE,&OnAgreeBeInviteMount);//返回是否同意加入骑乘队伍
	Register( G_M_LEAVE_MOUNTTEAM,&OnLeaveMountTeam);//离开骑乘队伍
	Register( G_M_NEWMAP_SETRIDESTATE,&OnNewMapSetPlayerRideState);//更新玩家的骑乘状态
	Register( G_M_RIRDELEADER_SWITCHMAP,&OnRideLeaderSwitchMap );//当骑乘队长开始跳地图时

//组队
	//Register( G_M_TEAM_ENTRANCE, &OnPlayerEntranceTeam );
	Register( G_M_TEAM_DETAILINFO, &OnRecvTeamDetialInfo );
	//Register( G_M_TEAM_EXIT, &OnPlayerExitTeam );
	Register( G_M_TEAM_KICKPLAYER, &OnRecvKickPlayerOnTeam );
	Register( G_M_CHANGETEAM_LEADER,&OnRecvChangeTeamLeader ); 
	Register( G_M_TEAM_DISBAND,&OnRecvDisBandTeam );
	Register( G_M_CHANGETEAM_ITEMMODE,&OnRecvChangeItemShareMode );
	Register( G_M_CHANGETEAM_EXPREMODE, &OnRecvChangeExpreShareMode );
	Register( G_M_CHANGETEAM_ROLLLEVEL, &OnRecvChangeRollLevelItemMode );
	Register( G_M_AGREE_ROLLITEM, &OnRecvIsAgreeRollItem );
	Register( G_M_TEAMMEMBER_GETNEWITEM , &OnTeamMemberGetNewItem );
	
	//托盘
	Register( G_M_ITEM_UPGRADE_PLATE, &OnPlayerReqItemUpPlate);//玩家请求装备升级托盘
	Register( G_M_ADDSTH_TO_PLATE,    &OnPlayerAddSthToPlate );//玩家往托盘中加物品
	Register( G_M_TAKESTH_FROM_PLATE, &OnPlayerTakeSthFromPlate );//玩家从托盘中取物品
	Register( G_M_PLATE_EXEC,         &OnPlayerTryPlateExec );//玩家尝试执行托盘操作
	Register( G_M_CLOSE_PLATE,        &OnPlayerClosePlate );//玩家关闭托盘


	Register( G_M_SEND_TRADE_REQ, &OnSendTradeReq );//发送交易申请
	
	Register( G_M_SEND_TRADE_REQ_RESULT, &OnSendTradeReqResult );//请求交易的结果（ 同意或拒绝）
	
	Register( G_M_ADD_OR_DELETE_ITEM, &OnAddOrDeleteItem );//增加或删除一个物品到交易栏

	Register( G_M_SET_TRADE_MONEY, &OnSetTradeMoney );//交易金钱
	
	Register( G_M_CANCEL_TRADE, &OnCancelTrade );//取消交易
	
	Register( G_M_LOCK_TRADE, &OnLockTrade );//锁定交易
	
	Register( G_M_CONFIRM_TRADE, &OnConfirmTrade );//确认交易

	Register( G_M_REPAIR_WEAR_BY_ITEM, &OnRepairWearByItem); //用修复性道具修复其他道具

	Register( G_M_TIMED_WEAR_IS_ZERO, &OnTimedWearIsZero); //时间性耐久度为0

	Register( G_M_ADD_SPSKILL_REQ, &OnAddSpSkillReq); //

	Register( G_M_REQ_BUFFEND, &OnReqBuffEnd );	

	Register( G_M_USE_ASSISTSKILL, &OnPlayerUseAssistSkill);  //使用辅助技能

	Register( G_M_UPGRADE_SKILL, &OnUpgradeSkill);  //升级技能

	Register( G_M_DELETE_TASK , &OnDeleteTask );//删除任务

	Register( G_M_REPAIR_WEAR_BY_NPC, &OnRepairWearByNpc);

	Register( G_M_QUERY_SHOP, &OnQueryShop );//查询商店物品数量

	Register( G_M_SHOP_BUY_REQ, &OnShopBuyReq );//请求购买商店物品

	Register( G_M_ITEM_SOLD_REQ, &OnItemSoldReq );//请求售出包裹物品

	Register( G_M_CHANGE_BATTLEMODE, &OnChangeBattleMode ); //请求更改战斗模式

	Register( G_M_ADD_APT_REQ, &OnAddAptReq ); //请求添加活点, //GMAddActivePtReq

	Register( G_M_QUERY_EVILTIME, &OnQueryEvilTime );//查询恶魔时间

	Register( G_M_USE_DAMAGE_ITEM, &OnUseDamageConsumeItem );//使用伤害型消耗道具　

	Register( G_M_QUERY_REPAIRITEM_COST, &OnQueryRepairItemCost );//维修道具的花费

	Register( G_M_SWAP_EQUIPITEM , &OnSwapEquipItem );//交换装备栏道具

	Register( G_M_QUERY_PKGITEMPAGE,&OnQueryItemPkgPage );//查询道具包对应页的道具信息

#ifdef OPEN_PUNISHMENT
	Register( G_M_PUNISHMENT_START, &OnPunishmentStart );		//通知天谴开始
	Register( G_M_PUNISHMENT_END, &OnPunishmentEnd );			//通知天谴结束
	Register( G_M_PUNISH_PLAYER_INFO, &OnPunishPlayerInfo );	//接收天谴玩家的消息更新
#endif /*ifdef OPEN_PUNISHMENT*/

	Register( G_M_GMCMD_REQUEST, &OnGmCmdRequest ); 
#ifdef OPEN_PET
	Register( G_M_SUMMON_PET, &OnSummonPet );		//召唤宠物
	Register( G_M_CHANGE_PET_NAME, &OnChangePetName );		//更改宠物姓名
	Register( G_M_CLOSE_PET, &OnClosePet );			//唤回宠物 
	Register( G_M_CHANGE_PET_IMAGE, &OnChangePetImage );	//更改宠物的外形
	Register( G_M_CHANGE_PET_SKILL_STATE, &OnChangePetSkillState );		//更改宠物的技能状态
#endif //OPEN_PET

	Register( G_M_RANKCHART_INFO_UPDATE, &OnRecvRankChartInfo );//排行榜信息更新
	//Register( G_M_PLAYER_DELETE_ROLE ,&OnRecvPlayerDeleteRole );
	//Register( G_M_REMOVE_CHART_RANKPLAYER, &OnRecvRemoveRankChartPlayer );
#ifdef WAIT_QUEUE
	Register( G_M_PLAYER_QUIT_QUEUE, &OnRemoveWaitPlayer );		//玩家撤销排队
#endif  /*WAIT_QUEUE*/


	Register( G_M_NOTIFY_WAR_TASK_STATE, &OnNotifyWarTaskState );

	//Register( G_M_USE_PARTITION_SKILL, OnUsePartionSkill );//分割技能，使用UseSkill2后废弃，原ID号由GMMouseTipReq使用；
	Register( G_M_MOUSETIP_REQ, OnGMMouseTipReq );//鼠标停留信息查询(HP|MP)

	Register( G_M_CHECK_PKG_FREE_SPACE, OnCheckPkgFreeSpace );	//检查包裹可用空间
	Register( G_M_FETCH_VIP_ITEM, OnFetchVipItem );				//领取vip道具
	Register( G_M_FETCH_NON_VIP_ITEM, OnFetchNonVipItem );		//领取非vip道具

	Register( G_M_PICKITEM_FROM_MAIL  ,&OnPickMailItem );
	Register( G_M_PICKMONEY_FROM_MAIL, &OnPickMailMoney );
	Register( G_M_REQ_PLAYER_DETAIL_INFO, &OnReqPlayerDetailInfo );

	Register( G_M_QUERY_SUIT_INFO, &OnQuerySuitInfo );//获取套装信息
	Register( G_M_CLEAR_SUIT_INFO, &OnClearSuitInfo );//清除套装信息
	Register( G_M_SAVE_SUIT_INFO, &OnSaveSuitInfo );//保存套装信息
	Register( G_M_CHANGE_SUIT, &OnChanageSuit );//一键换装
	
	Register( G_M_DISCARD_COLLECT_ITEM, &OnDiscardCollectItem );//放弃采集道具
	Register( G_M_PICK_COLLECT_ITEM, &OnPickCollectItem );//拾取采集道具
	Register( G_M_PICK_ALL_COLLECT_ITEM, &OnPickAllCollectItem );//拾取所有采集道具

	Register( G_M_EQUIP_FASHION_SWITCH, &OnEquipFashionSwitch );//装备态/时装态切换
	Register( G_M_END_ANAMORPHIC, &OnEndAnamorphic);//停止变形

	Register( G_M_PUTITEM_TO_PROTECTPLATE, &OnPutItemToProtectPlate );//将道具放入保护道具的托盘
	Register( G_M_CONFIRM_PROTECTPLATE, &OnConfirmProtectPlate );//执行道具托盘
	Register( G_M_RELEASE_SPECIALPROTECT, &OnReleaseItemSpecialProtect );//特殊保护道具进入非保护状态
	Register( G_M_CANCEL_SPECIALPROTECT, &OnCancelItemUnSpecialProtectWaitTime );//取消特殊保护的消失等待时间
	Register( G_M_TAKE_ITEM_FROM_PROTECTPLATE , &OnTakeItemFromProtectPlate );//从保护托盘中取出道具  
	//Register( G_M_UPDATE_RANKPLAYER_RANKINFO, &OnRcvUpdateRankPlayerRankInfo );
	Register( G_M_CITY_INFO , &OnCityInfo );	//获得关卡地图信息
#ifdef ANTI_ADDICTION
	Register( G_M_GAME_STATE_NOTIFY , &OnGameStateNotify );	//获得防沉迷状态
#endif /*ANTI_ADDICTION*/
	Register( G_M_ATTACK_WAR_BEGIN , &OnAttackWarBegin );	//获得关卡地图信息
	Register( G_M_ATTACK_WAR_END ,	&OnAttackWarEnd );	//获得关卡地图信息
	Register( G_M_NOTIFY_RELATION_NUMBER ,	&OnNotifyRelationNumber );	//更新好友仇人人数
	Register( G_M_USE_SKILL ,	&OnUseSkill );	//使用技能
    Register( G_M_USE_SKILL_2, &OnUseSkill2 );  //使用技能2，6.30版本矩形攻击相应调整所有范围攻击
	Register( G_M_UNION_MULTI_NTF, &OnUnionMultiNtf );//通知工会活跃度或威望n倍

	Register( G_M_CLOSE_STORAGE_PLATE, &OnCloseStoragePlate );//关闭仓库
	Register( G_M_CHANGE_STORAGE_LOCKSTATE, &OnChangeStorageLockState );//改变仓库状态
	Register( G_M_TAKE_ITEM_STORAGE, &OnTakeItemFromStorage );//取出物品从仓库
	Register( G_M_TAKE_ITEM_FROM_STORAGE_TO_PKG, &OnTakeItemFromStorageIndexToPkgIndex );
	Register( G_M_PUT_ITEM_TO_STORAGE , &OnPutItemToStorage );
	Register( G_M_PUT_ITEM_FROM_PKG_TO_STORAGE, &OnPutItemToStorageIndexFromPkgIndex );
	Register( G_M_SET_NEW_STORAGE_PASSWORD , &OnSetNewStoragePassWord );
	Register( G_M_RESET_STORAGE_PASSWORD , &OnResetStoragePassWord );
	Register( G_M_TAKEITEM_CHECK_PASSWORD, &OnTakeItemCheckPassWord );
	Register( G_M_CHANGELOCKSTATE_CHECK_PASSWORD , &OnChangeStorageLockStateCheckPsd );
	Register( G_M_EXTEND_STORAGE , &OnExtendStorage );
	Register( G_M_SORT_STORAGE , & OnSortStorage );
	Register( G_M_STORAGE_SAFE_INFO, &OnRecvStorageSafeInfo );
	Register( G_M_STORAGE_ITEM_INFO, &OnRecvStorageItemInfo );
	Register( G_M_STORAGE_FORBIDDEN_INFO, &OnRecvStorageForbiddenInfo );
	Register( G_M_SWAP_STORAGE_ITEM, &OnSwapStorageItem );
	Register( G_M_GMQUERY_REQ, &OnGMQueryReq );
	Register( G_M_SORT_PKG_ITEM, &OnSortPkgItem );
	Register( G_M_SORT_PKG_ITEM_BEGIN, &OnSortPkgItemBegin );
	Register( G_M_SORT_PKG_ITEM_END, &OnSortPkgItemEnd );
	Register( G_M_BATCH_SELL_ITEM_REQ, &OnBatchSellItemsReq );
	Register( G_M_CLOSE_OLTEST_PLATE, &OnCloseOLTestPlate );
	Register( G_M_ANSWER_OLTEST_QUESTION, &OnAnswerQuestion );
	Register( G_M_REPURCHASE_ITEM_REQ, &OnRepurchaseItemReq );
	Register( G_M_DROP_OLTEST, &OnDropOLTest );
	//Register( G_M_RANKCHART_REFRESH , &OnRefreshRankChart );
	Register( G_M_SINGLE_PLAYER_TEAMID_NOTIFY, &OnRecvSinglePlayerTeamID );
#ifdef ITEM_NPC
	Register( G_M_ITEM_NPC_REQ, &OnItemNpcReq );
#endif //ITEM_NPC

	Register( G_M_NEW_COPY_CMD, &OnNewCopyCmd );//创建新副本指令；
	Register( G_M_DEL_COPY_CMD, &OnDelCopyCmd );//删除旧副本指令；	
	Register( G_M_DEL_COPY_QUEST, &OnDelCopyQuest );//centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
    Register( G_M_LEAVE_STEAM_NOTI, &OnLeaveSTeamNoti );//玩家离开副本组队通知；
	Register( G_M_QUERY_GLOBE_COPYINFOS, &OnQueryGlobeCopyInfos );//玩家向mapsrv查询全局副本信息(该mapsrv管理的全部伪副本)

	Register( G_M_TIMELIMIT_RES, &OnPlayerTimeLimitRes );//玩家点击倒计时框；

	Register( G_M_REPURCHASE_ITEMS_LIST_REQ, &OnRepurchaseItemsListReq );//获取回购列表
	Register( G_M_SELECT_WAR_REBIRTH_OPTION, &OnSelectWarRebirthPtOption );//选择攻城战复活点

	Register( G_M_ON_RACEMASTER_SETNEW_PUBLICMSG, &OnRaceMasterSetNewNotice );
	Register( G_M_RACEMASTER_TIMEINFO_CHANGE, &OnRaceMasterTimeInfoChange );
	Register( G_M_RACEMASTER_TIMEINFO_EXPIRE, &OnRaceMasterTimeInfoExpire );
	Register( G_M_RACEMASTER_DECLAREWARINFO_CHANGE, &OnRaceMasterDeclareWarInfoChange );
	Register( G_M_RACEMASTER_DETIALINFO, &OnRecvRaceMasterDetialInfo );
	Register( G_M_RACEMASTER_NEW_BORN, &OnRecvNewRaceMasterBorn );
	Register( G_M_RACEMASTER_SELECT_OPTYPE, &OnRaceMasterSelectOpType );
	Register( G_M_RACEMASTER_DECLAREWAR_TO_OTHERRACE, &OnRaceMasterDeclareWarToOtherRace );
	Register( G_M_RACEMASTER_SETNEW_PUBLICMSG, &OnRaceMasterSetNewPublicMsg );
	Register( G_M_LOAD_RACEMASTER_FROM_DB_RST,&OnLoadRaceMasterInfoRst );
	Register( G_M_FORBIDDEN_RACEMASTER_AUTHORITY, &OnForbiddenRaceMasterAuthority );

	Register( G_M_ADD_PUBLICNOTICE, &OnPublicNoticeAdd );
	Register( G_M_MODIFY_PUBLICNOTICE, &OnPublicNoticeModify );
	Register( G_M_REMOVE_PUBLICNOTICE, &OnPublicNoticeRemove );
	Register( G_M_ADD_MARQUEUE_NOTICE, &OnMarqueueNoticeAdd );
	Register( G_M_BROADCAST_TMPNOTICE, &OnTempNoticeBroadcast );
	Register( G_M_BROADCAST_PARAMNOTICE,&OnParamNoticeBroadcast );

	Register( G_M_DIAMOND_TO_PRESTIGE_NOT, &OnDiamondToPrestigeNtf );

	Register( G_M_START_NEWAUCTION, &OnPlayerStartNewAuction );
	Register( G_M_PLAYER_BID_ITEM,  &OnPlayerBidItem );
	Register( G_M_PLAYER_SUBMONEY,  &OnPlayerBidItemErrorAddMoney );

	Register( G_M_MANUAL_UPGRADE_REQ,  &OnManualUpgradeReq );
	Register( G_M_ALLOCATE_POINT_REQ,  &OnAllocatePointReq );
	Register( G_M_MOD_EFFECTIVE_SKILL_ID, &OnModEffectiveSkill);

	Register( G_M_FRIEND_GAIN_NTF,  &OnFriendGainNtf );
	Register( G_M_QUERY_EXP_OF_LEVEL_UP,  &OnQueryExpOfLevelUpReq );
	Register( G_M_REPLY_CALL_MEMBER, &OnReplyCallMember );

	Register( G_M_START_CAST, &OnStartCast );
	Register( G_M_STOP_CAST, &OnStopCast );
	Register( G_M_GMACCOUNT_NTF, &OnGMAccountNtf);

	return;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
//DB信息开始;	//Register( G_M_DBGET_PRE, &OnDbGetPre );
bool CDealG_MPkg::OnDbGetPre( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ) 
{ 
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		D_ERROR( "CDealG_MPkg::OnDbGetPre，pOwner空\n" );
		return false;
	}

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMDbGetPre) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		D_ERROR( "CDealG_MPkg::OnDbGetPre，收包大小错误\n" );
		return false;
	}

	const GMDbGetPre* pDbGetPre = (const GMDbGetPre*)pPkg;
	CPlayer* pPlayer = pOwner->FindPlayer( pDbGetPre->playerID.dwPID );
	if ( NULL != pPlayer )
	{
		D_ERROR( "OnDbGetPre，试图新加%s时，该玩家%s现在已存在\n", pDbGetPre->accountName, pPlayer->GetAccount() );
		return false;
	}

	if ( pDbGetPre->isNeedQueueCheck )
	{
		//检查当前是否有排队者，如果有，则进入排队队尾；
		if ( CWaitQueue::IfNeedWait( pDbGetPre->playerID ) )
		{
			D_INFO( "玩家(%s--%s)加入排队\n", pDbGetPre->accountName, pDbGetPre->roleName );
			return true;
		}
		if ( CPlayerManager::GetPlayerSize() >= g_maxMapSrvPlayerNum ) //即使当前没有人在排队，但如果服务器人数达到了预定限额，则也限制新人进入；
		{
			//当前地图人数大于3000，开始排队;
			CWaitQueue::AddOneWaitPlayer( pDbGetPre->playerID );
			D_INFO( "玩家(%s--%s)加入排队\n", pDbGetPre->accountName, pDbGetPre->roleName );
			return true;
		}
	}

	pPlayer = g_poolManager->RetrivePlayer();
	if ( NULL == pPlayer )
	{
		D_ERROR( "已排队情形下不应到此，达到玩家数上限，玩家(%s--%s)进入排队\n", pDbGetPre->accountName, pDbGetPre->roleName );
		//排队通知(第一个排队的人)
		CWaitQueue::AddOneWaitPlayer( pDbGetPre->playerID );
		return true;
	}

	D_DEBUG( "玩家(%s--%s)进入mapsrv\n", pDbGetPre->accountName, pDbGetPre->roleName );

	pPlayer->InitNameInfo( pOwner, pDbGetPre->playerID, pDbGetPre->accountName, pDbGetPre->roleName );

	//设置该玩家的帐号名与角色名；
	pOwner->AddAccountPlayer( pDbGetPre->playerID.dwPID, pDbGetPre->accountName, pPlayer );		

	//玩家此时还未真正进入战斗，从现在开始到向客户端返回appear消息之前，如果有任意一步失败,
	//则:向gatesrv返回MGError消息的同时，要同时将pPlayer实例删去，因为后续gatesrv不会再发来leave消息了；
	pPlayer->SetPreFightFlag();

	return true;
	TRY_END;
	return false;	
};

//DB信息结束(与G_M_DBGET_PRE一一对应);	//Register( G_M_DBGET_POST, &OnDbGetPost );
bool CDealG_MPkg::OnDbGetPost( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ) 
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMDbGetPost) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMDbGetPost* pDbGetPost = (const GMDbGetPost*)pPkg;
	CPlayer* pPlayer = pOwner->FindPlayer( pDbGetPost->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "OnDbGetPost，不存在对应该ID%d的玩家，可能在排队中,忽略此消息\n", pDbGetPost->playerID.dwPID );
		return false;
	}

	D_DEBUG( "玩家(%s)存盘信息收毕\n", pPlayer->GetAccount() );

	return true;
	TRY_END;
	return false;	
};

//DB取到的信息；	//Register( G_M_DBGET_INFO, &OnDbGetInfo );
bool CDealG_MPkg::OnDbGetInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMDbGetInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMDbGetInfo* pDbGetInfo = (const GMDbGetInfo*)pPkg;
	CPlayer* pPlayer = pOwner->FindPlayer( pDbGetInfo->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_ERROR( "OnDbGetInfo，不存在对应该ID%d的玩家，可能在排队中，忽略此消息\n", pDbGetInfo->playerID.dwPID );
		return false;
	}

	pPlayer->OnDbGetInfo( pDbGetInfo );

	//D_DEBUG( "玩家(%s)存盘信息，类型%d收毕\n", pPlayer->GetAccount(), pDbGetInfo->infoType );

	return true;
	TRY_END;
	return false;		
};

//玩家离开地图(在G_M_DBGET_POST之后发送);	//Register( G_M_PLAYER_ENTER_MAP, &OnPlayerEnterMap );
bool CDealG_MPkg::OnPlayerEnterMap( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ) 
{ 
	ACE_UNUSED_ARG( pOwner ); ACE_UNUSED_ARG( pPkg ); ACE_UNUSED_ARG( wPkgLen );
	return true; 
};

//#define G_M_NDSMR_INFO               //no db, switch map reserve信息；	Register( G_M_NDSMR_INFO, &OnNDSMRInfo );
bool CDealG_MPkg::OnNDSMRInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMNDSMRInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMNDSMRInfo* pNdsmrInfo = (const GMNDSMRInfo*)pPkg;
	CPlayer* pPlayer = pOwner->FindPlayer( pNdsmrInfo->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_ERROR( "OnNDSMRInfo，不存在对应该ID%d的玩家，可能在排队中，错误\n", pNdsmrInfo->playerID.dwPID );
		return false;
	}

	pPlayer->OnNDSMRInfo( pNdsmrInfo );

	return true;
	TRY_END;
	return false;		
}
///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///玩家攻击怪物处理，by dzj, 07.12.27  // changed by hyn 08.3.6 
bool CDealG_MPkg::OnPlayerAttackMonster( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

//	//检验收包大小是否正确；
//	if ( wPkgLen != sizeof(GMAttackMonster) )
//	{
//		//收包大小错误，可能是通信两端消息结构不一致；
//		return false;
//	}
//
//	const GMAttackMonster* pAttackMonsterInfo = (const GMAttackMonster*)pPkg;
//	//以下2种情况是严重错误，需要让gatesrv立即把客户端踢下线
//	CPlayer* pAttackPlayer = pOwner->FindPlayer( pAttackMonsterInfo->attackerID.dwPID );
//	if ( NULL == pAttackPlayer )
//	{
//		D_WARNING("收到玩家攻击monster消息,但攻击者:(%d:%d)不存在，通知gatesrv踢此玩家\n"
//			, pAttackMonsterInfo->attackerID.wGID, pAttackMonsterInfo->attackerID.dwPID );
//		///改用kickPlayer, by dzj, 09.10.10;
//		pOwner->NotifyGateSrvKickPlayer( pAttackMonsterInfo->attackerID );
////		MGError errmsg;
////		errmsg.nErrCode = MGError::LEAVE_ERR_NOPLAYER;
////		errmsg.leavePlayerID = pAttackMonsterInfo->attackerID;
////		errmsg.lNotiPlayerID = pAttackMonsterInfo->attackerID;
////#ifdef GATE_SENDV
////		bool isNewPkg = false;
////		MsgToPut* pNewMsg = CreateGateSrvPkg( MGError, pOwner, errmsg, isNewPkg );
////		if ( isNewPkg )
////		{
////			pOwner->SendOldRsvNew( pNewMsg );
////		}
////#else  //GATE_SENDV
////		MsgToPut* pNewMsg = CreateSrvPkg( MGError, pOwner, errmsg ); 
////		pOwner->SendPkgToGateServer(  pNewMsg );
////#endif //GATE_SENDV
//		return false;
//	}
//
//	CNewMap* pTempMap = pAttackPlayer->GetCurMap();
//	if( NULL == pTempMap )
//	{
//		D_WARNING("CDealG_MPkg::OnPlayerAttackMonster 玩家(%d:%d)地图指针为空，通知gatesrv踢此玩家\n"
//			, pAttackMonsterInfo->attackerID.wGID, pAttackMonsterInfo->attackerID.dwPID );
//		///改用kickPlayer, by dzj, 09.10.10;
//		pOwner->NotifyGateSrvKickPlayer( pAttackMonsterInfo->attackerID );
////		MGError errmsg;
////		errmsg.nErrCode = MGError::LEAVE_ERR_MAPDISMATCH;
////		errmsg.leavePlayerID = pAttackMonsterInfo->attackerID;
////		errmsg.lNotiPlayerID = pAttackMonsterInfo->attackerID;
////#ifdef GATE_SENDV
////		bool isNewPkg = false;
////		MsgToPut* pNewMsg = CreateGateSrvPkg( MGError, pOwner, errmsg, isNewPkg );
////		if ( isNewPkg )
////		{
////			pOwner->SendOldRsvNew( pNewMsg );
////		}
////#else  //GATE_SENDV
////		MsgToPut* pNewMsg = CreateSrvPkg( MGError, pOwner, errmsg ); 
////	    pOwner->SendPkgToGateServer(  pNewMsg );
////#endif //GATE_SENDV
//		return false;
//	}
//	
//	BattleArgs args;
//	args.skillID = pAttackMonsterInfo->lSkillType;
//	args.targetID.dwPID = pAttackMonsterInfo->lMonsterID;
//	args.targetID.wGID = MONSTER_GID;
//	args.battleType = COMMON_SKILL_PVC;
//	args.mainAoeTargetID = args.targetID;
//	args.attackPosX = pAttackMonsterInfo->fAttackerPosX;
//	args.attackPosY = pAttackMonsterInfo->fAttackerPosY;
//
//	GMUseSkill gm;
//	gm.useSkill.skillID = pAttackMonsterInfo->lSkillType;
//	gm.useSkill.targetID.dwPID = pAttackMonsterInfo->lMonsterID;
//	gm.useSkill.targetID.wGID = MONSTER_GID;
//	gm.useSkill.nAttackerPosX = pAttackPlayer->GetPosX();
//	gm.useSkill.nAttackerPosY = pAttackPlayer->GetPosY();
//	gm.useSkill.skillType = COMMON_SKILL;
//	gm.useSkill.sequenceID = pAttackMonsterInfo->sequenceID;
//	//return pTempMap->OnPlayerUseSkill( pAttackPlayer, &gm );
//
//	//return pTempMap->OnPlayerAttackMonster( pAttackPlayer, pAttackMonsterInfo );  //其他错误情况搬到地图逻辑里处理
//	return pTempMap->HandlePlayerBattle( pAttackPlayer, &args );

	return true;
	TRY_END;
	return false;	
}

bool CDealG_MPkg::OnPlayerRun(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMRun) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMRun* pRun = (const GMRun*)pPkg;
	CPlayer* pTempPlayer = pOwner->FindPlayer( pRun->lPlayerID.dwPID );
	if( pTempPlayer == NULL)
	{
		D_WARNING("[OnPlayerRun] GHandleID:%d PlayerID:%d 并不存在\n",pOwner->GetHandleID(), pRun->lPlayerID.dwPID);
		return false;
	}

	//D_INFO("收到GMRun 玩家:%s,squenceID:%d,tgt(%f,%f)\n", pTempPlayer->GetAccount(), pRun->sequenceID, pRun->fTargetWorldPosX, pRun->fTargetWorldPosY );	

	//D_INFO( "Player Run, Name %s, orgPT(%d,%d), newPT(%d,%d)\n", pTempPlayer->GetAccount(), pTempPlayer->GetPosX(), pTempPlayer->GetPosY(), pRun->nOrgMapPosX, pRun->nOrgMapPosY );

	if ( pTempPlayer->IsWaitToDel() )
	{
		D_ERROR( "CDealG_MPkg::OnPlayerRun，玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	//if ( pTempPlayer->IsInSwitchMap() )
	//{
	//	D_ERROR( "CDealG_MPkg::OnPlayerRun，玩家处于跳转状态，忽略除leave与FullInfo之外的其它任何消息\n" );
	//	return false;
	//}

	if( pTempPlayer->IsDying() )
	{
		D_ERROR(" CDealG_MPkg::OnPlayerRun,玩家处于死亡状态,不接受移动包\n");  //如果接收了有可能会在节点中 add 2遍
		return false;
	}

	if( pTempPlayer->IsWaitingClient() )
	{
		D_ERROR("CDealG_MPkg::OnPlayerRun,玩家处于等待客户端消息中, 不接收移动包\n" );
		return false;
	}

	bool ret = false;
	do 
	{
		//7.8 hyn buff 效果如果是晕眩或者束缚 移动是不能成功的
		if( pTempPlayer->IsFaint() || pTempPlayer->IsBondage() )
		{
			D_WARNING( "%s试图在晕眩和束缚状态下移动，拒绝\n", pTempPlayer->GetNickName() );
			break;
		}

		if( pTempPlayer->IsInRest() )
		{
			D_WARNING("%s试图在主动休息状态下移动，拒绝\n", pTempPlayer->GetNickName() );
			break;
		}

		CNewMap* pTempMap = pTempPlayer->GetCurMap();
		if( pTempMap == NULL )
		{
			D_ERROR( "[OnPlayerRun] There is no map ID:%d",pTempPlayer->GetMapID() );
			break;
		} 
		else 
		{
			ret = pTempMap->OnPlayerNewMove( pTempPlayer, pRun );//新的通知机制，在玩家身上保存了附近玩家信息；
			break;
		}
	} while(0);

	if ( !ret )
	{
		MGRunFail msg;
		msg.rst.posX = pTempPlayer->GetPosX();
		msg.rst.posY = pTempPlayer->GetPosY();
		pTempPlayer->SendPkg<MGRunFail>(&msg);
	}	

	TRY_END;
	return false;
}

//Register( G_M_BIAS_INFO, &OnGMBiasInfo );//排行榜阈值相关信息；
bool CDealG_MPkg::OnGMBiasInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMBiasInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMBiasInfo* pGMBiasInfo = (const GMBiasInfo*)pPkg;

	RankEventManager::OnBiasInfo( pGMBiasInfo );

	return true;
	TRY_END;
	return false;
}

//Register( G_M_HONOR_MASK, &OnGMHonorMask );//玩家选择显示的称号；	
bool CDealG_MPkg::OnGMHonorMask( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMHonorMask) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMHonorMask* pHonorMask = (const GMHonorMask*)pPkg;
	CPlayer* pPlayer = pOwner->FindPlayer( pHonorMask->playerID.dwPID );
	if( pPlayer == NULL)
	{
		D_WARNING("OnGMHonorMask, GHandleID:%d PlayerID:%d 不存在\n", pOwner->GetHandleID(), pHonorMask->playerID.dwPID);
		return false;
	}

	if ( pPlayer->IsWaitToDel() )
	{
		D_ERROR( "OnGMHonorMask，玩家%s处于待删状态，忽略除leave之外的其它任何消息\n", pPlayer->GetAccount() );
		return false;
	}

	if( pPlayer->IsDying() )
	{
		D_ERROR( "OnGMHonorMask,玩家%s处于死亡状态,不处理该消息\n", pPlayer->GetAccount() );  //如果接收了有可能会在节点中 add 2遍
		return false;
	}

	if( pPlayer->IsWaitingClient() )
	{
		D_ERROR( "OnGMHonorMask,玩家%s处于等待客户端消息中, 不处理该消息\n", pPlayer->GetAccount() );
		return false;
	}

	pPlayer->SetHonorMask( pHonorMask->honorMask );

	//记录好友动态
	unsigned int honor = (unsigned int)pHonorMask->honorMask;
	pPlayer->AddFriendTweet(MUX_PROTO::TWEET_TYPE_GET_TITLE, (const void *)&honor, sizeof(unsigned int));

	MGHonorMask mgMsg;
	mgMsg.honorMask = (EHonorMask) (pPlayer->GetHonorMask());
	pPlayer->SendPkg<MGHonorMask>( &mgMsg );//通知玩家设置结果；

	return true;
	TRY_END;
	return false;
}

//Register( G_M_USE_MONEY_TYPE, &OnGMUseMoneyType );//玩家设置所使用的钱币；
bool CDealG_MPkg::OnGMUseMoneyType( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMUseMoneyType) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMUseMoneyType* pUseMoneyType = (const GMUseMoneyType*)pPkg;
	CPlayer* pPlayer = pOwner->FindPlayer( pUseMoneyType->playerID.dwPID );
	if( pPlayer == NULL)
	{
		D_WARNING("OnGMUseMoneyType, GHandleID:%d PlayerID:%d 不存在\n", pOwner->GetHandleID(), pUseMoneyType->playerID.dwPID);
		return false;
	}

	if ( pPlayer->IsWaitToDel() )
	{
		D_ERROR( "OnGMUseMoneyType，玩家%s处于待删状态，忽略除leave之外的其它任何消息\n", pPlayer->GetAccount() );
		return false;
	}

	if( pPlayer->IsDying() )
	{
		D_ERROR( "OnGMUseMoneyType,玩家%s处于死亡状态,不处理该消息\n", pPlayer->GetAccount() );  //如果接收了有可能会在节点中 add 2遍
		return false;
	}

	if( pPlayer->IsWaitingClient() )
	{
		D_ERROR( "OnGMUseMoneyType,玩家%s处于等待客户端消息中, 不处理该消息\n", pPlayer->GetAccount() );
		return false;
	}

	pPlayer->SetUseMoneyType( pUseMoneyType->moneyType );

	return true;
	TRY_END;
	return false;
}

//玩家跑动中跨块；
bool CDealG_MPkg::OnPlayerBeyondBlock( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMPlayerBeyondBlock) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMPlayerBeyondBlock* pBeyondBlock = (const GMPlayerBeyondBlock*)pPkg;
	CPlayer* pBeyondPlayer = pOwner->FindPlayer( pBeyondBlock->movePlayerID.dwPID );
	if( pBeyondPlayer == NULL)
	{
		D_WARNING("[OnPlayerBeyondBlock] GHandleID:%d PlayerID:%d 不存在\n", pOwner->GetHandleID(), pBeyondBlock->movePlayerID.dwPID);
		return false;
	}

	if ( pBeyondPlayer->IsWaitToDel() )
	{
		D_ERROR( "CDealG_MPkg::OnPlayerBeyondBlock，玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	if( pBeyondPlayer->IsDying() )
	{
		D_ERROR(" CDealG_MPkg::OnPlayerBeyondBlock,玩家处于死亡状态,不接受移动跨块包\n");  //如果接收了有可能会在节点中 add 2遍
		return false;
	}

	if( pBeyondPlayer->IsWaitingClient() )
	{
		D_ERROR("CDealG_MPkg::OnPlayerBeyondBlock,玩家处于等待客户端消息中, 不接收移动跨块包\n" );
		return false;
	}

	//7.8 hyn buff 效果如果是晕眩或者束缚 移动是不能成功的
	if( pBeyondPlayer->IsFaint() || pBeyondPlayer->IsBondage() )
	{
		D_WARNING( "%s试图在晕眩和束缚状态下跨块，拒绝\n", pBeyondPlayer->GetNickName() );
		return false;
	}

	CNewMap* pTempMap = pBeyondPlayer->GetCurMap();
	if( pTempMap == NULL )
	{
		D_ERROR( "[OnPlayerBeyondBlock] There is no map ID:%d",pBeyondPlayer->GetMapID() );
		return false;
	}

	//D_WARNING( "收到客户端%s移动跨块消息，暂时没有处理\n", pBeyondPlayer->GetNickName() );
	//return false;
	return pTempMap->OnPlayerBeyondBlock( pBeyondPlayer, pBeyondBlock );//新的通知机制，在玩家身上保存了附近玩家信息；
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnPlayerLeave( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMPlayerLeave) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	CNewMap* pTempMap = NULL;
	CPlayer* pTempPlayer = NULL;

	const GMPlayerLeave* pLeave = (const GMPlayerLeave*)pPkg;
	//D_DEBUG("收到离线消息%d \n",1);
	pTempPlayer = pOwner->FindPlayer( pLeave->lPlayerID.dwPID );
	if( pTempPlayer == NULL )
	{
		////此玩家应该在排队队列，直接移去；
		//if ( !(CWaitQueue::RemoveWaitPlayer( pLeave->lPlayerID )) )
		CWaitQueue::RemoveWaitPlayer( pLeave->lPlayerID );
		if ( GMPlayerLeave::QUIT_QUEUE == pLeave->nSpecialFlag)
		{
			return true;
		}
		
		{
			//无论是否排队，都要通知gate此玩家不存在，以使gate通知center;
			D_WARNING( "CDealG_MPkg::OnPlayerLeave，玩家(%d:%d)不存在于本mapsrv，通知gate，若该玩家还在，则直接删去\n"
				, pLeave->lPlayerID.wGID, pLeave->lPlayerID.dwPID );
			MGError errmsg;
			errmsg.nErrCode = MGError::LEAVE_ERR_NOPLAYER;
			errmsg.leavePlayerID = pLeave->lPlayerID;
			errmsg.lNotiPlayerID = pLeave->lPlayerID;
#ifdef GATE_SENDV
			bool isNewPkg = false;
			MsgToPut* pNewMsg = CreateGateSrvPkg( MGError, pOwner, errmsg, isNewPkg );
			if ( isNewPkg )
			{
				pOwner->SendOldRsvNew( pNewMsg );
			}
#else  //GATE_SENDV
			MsgToPut* pNewMsg = CreateSrvPkg( MGError, pOwner, errmsg ); 
			pOwner->SendPkgToGateServer(  pNewMsg );
#endif //GATE_SENDV
		}
		return true;
	}

	//这是waittodel状态下唯一可以处理的消息，因此不判断pTempPlayer->IsWaitToDel()asdf
	D_DEBUG( "玩家%s从gatesrv发来离开mapsrv消息,消息标记%d，消息中离开地图号%d，当前玩家所在地图:%d，是否在副本中:%d\n", pTempPlayer->GetAccount()
		, pLeave->nSpecialFlag, pLeave->lMapCode, pTempPlayer->GetMapID(), pTempPlayer->IsCurPlayerCopyInfoSet()?1:0 );//副本调试日志;

	//不能在leave消息中调用NotifyGateSrvKickPlayer，因为对于跳副本或从副本离开的情形，会出现gate永远等待原mapsrv的离开消息而无法下线的情形if ( GMPlayerLeave::MAP_SWITCH_COPY == pLeave->nSpecialFlag )
	//{
	//	//dzj check,完全消去mapsrv上的下一跳信息//若为跳副本，则必须玩家的下一跳副本信息已置，否则说明过程中发生了意外错误（例如连接发来进副本与跳地图消息，而跳地图消息消去了下一跳副本信息）
	//	//if ( !( pTempPlayer->IsNextPlayerCopyInfoSet() ) )
	//	//{
	//	//	//玩家下一跳信息已被冲掉，此时简单的处理办法是踢玩家下线；
	//	//	D_ERROR( "收到gate发来的跳副本指令，但没有欲进的副本信息，意外错误！踢玩家%s\n", pTempPlayer->GetAccount() );
	//	//	pOwner->NotifyGateSrvKickPlayer( pTempPlayer->GetFullID() );
	//	//	return false;
	//	//}
	//}

	if ( ( GMPlayerLeave::NOT_EXIST == pLeave->nSpecialFlag )
		|| ( GMPlayerLeave::QUIT_QUEUE == pLeave->nSpecialFlag )
		)
	{
		//gatesrv上已不存在此玩家，可能玩家在发出进世界请求后就断了线，此时gatesrv已经将玩家直接删去了，因此mapsrv应该将该玩家直接删去而无需再通知gate;
		pOwner->IssueGateDeletePlayer( pLeave->lPlayerID.dwPID, pLeave->nSpecialFlag, false );	//删除玩家，唯一不需要通知gate与db进行更新的删除；
		return true;
	}

	if((long)pLeave->lMapCode != pTempPlayer->GetMapID() )
	{
		D_ERROR( "[HandleLeave]%s(%d,%d),Message MapID:%d and Player's MapID:%d is not same\n"
			, pTempPlayer->GetAccount(), pTempPlayer->GetGID(), pTempPlayer->GetPID(), pLeave->lMapCode, pTempPlayer->GetMapID() );
	}

	pOwner->IssueGateDeletePlayer( pLeave->lPlayerID.dwPID, pLeave->nSpecialFlag );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnReqRoleInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMReqRoleInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMReqRoleInfo* pReqRole = (const GMReqRoleInfo*) pPkg;
	CGateSrv* pProc = CManG_MProc::FindProc( pReqRole->lReqPlayerID.wGID );
	if( pProc == NULL)
	{
		D_ERROR( "OnReqRoleInfo,找不到所请求玩家的GATESRV:%d\n", pReqRole->lReqPlayerID.wGID );
		return false;
	}

	CPlayer* pReqPlayer = pProc->FindPlayer( pReqRole->lReqPlayerID.dwPID );	
	if( pReqPlayer == NULL )
	{
		D_WARNING("OnReqRoleInfo, gate:%d，找不到PlayerID:%d\n", pReqRole->lReqPlayerID.wGID, pReqRole->lReqPlayerID.dwPID );
		return false;
	}

	if ( pReqPlayer->IsWaitToDel() )
	{
		//D_ERROR( "CDealG_MPkg::OnReqRoleInfo，被请求玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	//if ( pReqPlayer->IsInSwitchMap() )
	//{
	//	D_ERROR( "CDealG_MPkg::OnReqRoleInfo，玩家处于跳转状态，忽略除leave与FullInfo之外的其它任何消息\n" );
	//	return false;
	//}

	//因为req的玩家可能不在同一个gatesrv上，所以需要事先去寻找gatesrv 03.11 hyn
	CPlayer* pNoticePlayer = pOwner->FindPlayer(pReqRole->lSelfPlayerID.dwPID);
	if( pNoticePlayer == NULL )
	{
		D_WARNING("[HandleReqRole2] GHandleID:%d PlayerID:%d 并不存在\n",pOwner->GetHandleID(),pReqRole->lSelfPlayerID.dwPID);
		return false;
	}

	if ( pNoticePlayer->IsWaitToDel() )
	{
		//D_ERROR( "CDealG_MPkg::OnReqRoleInfo，请求玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	//if ( pNoticePlayer->IsInSwitchMap() )
	//{
	//	D_ERROR( "CDealG_MPkg::OnReqRoleInfo，玩家处于跳转状态，忽略除leave与FullInfo之外的其它任何消息\n" );
	//	return false;
	//}

	CNewMap* pTempMap = pNoticePlayer->GetCurMap();
	if( NULL == pTempMap )
	{
		D_ERROR("玩家的地图指针失效\n");
		return false;
	}
	
	return pTempMap->OnReqRoleInfo(pReqPlayer, pNoticePlayer);

	TRY_END;
	return false;
}


bool CDealG_MPkg::OnReqMonsterInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMReqMonsterInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMReqMonsterInfo* pReqMonsterInfo = (GMReqMonsterInfo*)pPkg;
	
	CPlayer* pNoticePlayer = pOwner->FindPlayer( pReqMonsterInfo->lSelfPlayerID.dwPID );
	if( pNoticePlayer == NULL)
	{
		D_WARNING("[HandleMonsterInfo] GHandleID:%d PlayerID:%d 并不存在\n",pOwner->GetHandleID(),pReqMonsterInfo->lSelfPlayerID.dwPID);
		return false;
	}

	if ( pNoticePlayer->IsWaitToDel() )
	{
		//D_ERROR( "CDealG_MPkg::OnReqMonsterInfo，玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	//if ( pNoticePlayer->IsInSwitchMap() )
	//{
	//	D_ERROR( "CDealG_MPkg::OnReqMonsterInfo，玩家处于跳转状态，忽略除leave与FullInfo之外的其它任何消息\n" );
	//	return false;
	//}

	CNewMap *pTempMap = pNoticePlayer->GetCurMap();
	if( NULL == pTempMap )
	{
		D_ERROR("玩家的地图指针失效\n");
		return false;
	}

	return pTempMap->OnReqMonsterInfo(pNoticePlayer, pReqMonsterInfo);
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnPlayerChat(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMChat) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMChat* pChat = (const GMChat*)pPkg;
	CPlayer* pPlayer = pOwner->FindPlayer( pChat->sourcePlayerID.dwPID );
	if( NULL == pPlayer)
	{
		D_ERROR( "找不到说话的玩家%x\n", pChat->sourcePlayerID.dwPID );
		return false;
	}

	//if ( 0==strcmp( pPlayer->GetAccount(), "dzj2") )
	//{
	//	D_INFO( "chat info req:%s\n", pChat->strChat );
	//}

	if ( pPlayer->IsWaitToDel() )
	{
		//D_ERROR( "CDealG_MPkg::OnPlayerChat，玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	//if ( pPlayer->IsInSwitchMap() )
	//{
	//	D_ERROR( "CDealG_MPkg::OnPlayerChat，玩家处于跳转状态，忽略除leave与FullInfo之外的其它任何消息\n" );
	//	return false;
	//}

	CNewMap *pMap = pPlayer->GetCurMap();
	if( NULL == pMap)
	{
		D_ERROR( "玩家%s当前不在任何地图上\n", pPlayer->GetAccount() );
		return false;
	}

	return pMap->HandlePlayerChat(pPlayer, pChat);	
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnReqMannedMap(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMReqManedMaps) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	//
	//作为gatesrv发过来的第一个包
	const GMReqManedMaps* pReqMaps = (const GMReqManedMaps*)pPkg;
	if( pOwner->GetGateSrvID() == INVALID_GID )
	{
		D_DEBUG("收到GatesSrv的第一个包 ID:%d\n", pReqMaps->lGID );
		CManG_MProc::AddGateSrv( pReqMaps->lGID, pOwner );
		pOwner->SetGateSrvID( pReqMaps->lGID );

		if ( !RankEventManager::IsLoadCmdIssued() )
		{
			//若之前未发过装载排行榜信息的指令，则发送一次；
			RankEventManager::LoadAllRankChartInfo();
		}

		if ( !ItemIDGeneratorSingle::instance()->IsRequest() )
		{
			ItemIDGeneratorSingle::instance()->RequestNewIDRange();
		}
	}

	MGManedMap returnReqMap; 
	StructMemSet( returnReqMap, 0, sizeof(returnReqMap) );
	returnReqMap.isCopyMapSrv = CLoadSrvConfig::IsCopyMapSrv();
	vector<long> veCNewMapID;
	CManNormalMap::GetMannedMap( veCNewMapID );//请求的为本mapsrv管理的普通地图;
	int curpos = 0;
	int arrsize = ( sizeof(returnReqMap.lMapCode) / sizeof(returnReqMap.lMapCode[0]) );
	int outmapnum = 0;
	for(vector<long>::iterator iter=veCNewMapID.begin(); iter!=veCNewMapID.end(); ++iter )
	{
		curpos = (int) ( iter - veCNewMapID.begin() );
		if ( curpos < arrsize )
		{
			++outmapnum;
			returnReqMap.lMapCode[curpos] = veCNewMapID[curpos];
		} else {
			D_ERROR( "本MapSrv管理的地图数%d超过%d，超出部分无法通知gatesrv", curpos, arrsize );
		}
	}
	returnReqMap.nMapNum = outmapnum;
#ifdef GATE_SENDV
	bool isNewPkg = false;
	MsgToPut* pNewMsg = CreateGateSrvPkg( MGManedMap, pOwner, returnReqMap, isNewPkg );
	if ( isNewPkg )
	{
		pOwner->SendOldRsvNew( pNewMsg );
	}
#else  //GATE_SENDV
	MsgToPut *pMsg = CreateSrvPkg( MGManedMap, pOwner, returnReqMap );
	pOwner->SendPkgToGateServer( pMsg );
#endif //GATE_SENDV

	D_DEBUG("向GatesSrv发送管理的地图信息\n");

	return true;
	TRY_END;
	return false;
}

//玩家与NPC对话
bool CDealG_MPkg::OnPlayerScriptChat(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMScriptChat) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	// 查找玩家
	const GMScriptChat* pScriptChat = (const GMScriptChat*) pPkg;
	CPlayer *pPlayer = pOwner->FindPlayer( pScriptChat->playerID.dwPID );
	if(NULL == pPlayer)
	{
		D_WARNING("收到玩家脚本对话消息,但在本mapsrv上找不到该玩家:%x\n", pScriptChat->playerID.dwPID );
		return false;
	}

	if ( pPlayer->IsDying() )
	{
		D_DEBUG("玩家%s死亡,不能与NPC交互了\n",pPlayer->GetNickName() );
		return false;
	}

	if ( pPlayer->IsWaitToDel() )
	{
		//D_ERROR( "CDealG_MPkg::OnPlayerScriptChat，玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	//if ( pPlayer->IsInSwitchMap() )
	//{
	//	D_ERROR( "CDealG_MPkg::OnPlayerScriptChat，玩家处于跳转状态，忽略除leave与FullInfo之外的其它任何消息\n" );
	//	return false;
	//}

	CNewMap* pMap = pPlayer->GetCurMap();
	if(NULL == pMap)
	{
		D_WARNING("收到玩家脚本对话消息,但在本mapsrv上找不到该玩家所在的地图\n" );
		return false;
	}

	//根据脚本对象：NPC或Item分别处理
	if ( ST_NPC_SCRIPT == pScriptChat->tgtType )
	{
		CMonster* pMonster = pMap->GetMonsterPtrInMap( pScriptChat->tgtID );
		if ( NULL == pMonster )
		{
			D_WARNING("收到玩家与NPC聊天消息,但在该玩家所在地图上找不到欲对话的NPC对象\n" );
			return false;
		}

		if( pMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pMonster->IsToDel()  )
		{
			D_WARNING("怪物已死亡在待删状态,任何与之进行的对话无效\n");
			return false;
		}

		if( !CNpcStandRule::OnTalkingToNpc( pPlayer, pMonster ) )
		{
			D_WARNING( "NPC(类型:%d)立场机制不符,不能和该NPC开展对话，玩家:%s\n", pMonster->GetClsID(), pPlayer->GetAccount() );
			return false;
		}

		if( !IsInAccurateDistance( pPlayer->GetPosX(), pPlayer->GetPosY(), pMonster->GetPosX(), pMonster->GetPosY(), 7/*客户端显示位置容许误差2*/ ) )
		{
			D_WARNING( "交谈NPC(类型%d)与玩家%s距离相差太远\n", pMonster->GetClsID(), pPlayer->GetAccount() );
			return false;
		}

		pMonster->OnMonsterBeChat();	
		pPlayer->OnChatToNpc( pMonster, pScriptChat->nOption );
	} else if ( ST_ITEM_SCRIPT == pScriptChat->tgtType ) {
		CBaseItem* pItem = pPlayer->GetItemDelegate().GetItemByItemUID( pScriptChat->tgtID );
		if ( NULL == pItem )
		{
			D_WARNING("收到玩家与Item聊天消息,但找不到欲对话的Item对象\n" );
			return false;
		}
		pPlayer->OnChatToItem( pItem, pScriptChat->nOption );
	}else if( ST_AREA_SCRIPT == pScriptChat->tgtType )	
	{
		pPlayer->OnChatToArea( pScriptChat->nOption );
	}
	else
	{
		D_WARNING( "CDealG_MPkg::OnPlayerScriptChat，无效的对话脚本类型\n" );
	}

	return true;
	TRY_END;
	return false;
}

///玩家攻击玩家消息
bool CDealG_MPkg::OnPlayerAttackPlayer(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	////检验收包大小是否正确；
	//if ( wPkgLen != sizeof(GMAttackPlayer) )
	//{
	//	//收包大小错误，可能是通信两端消息结构不一致；
	//	return false;
	//}

	//const GMAttackPlayer* pAttackPlayerInfo = (const GMAttackPlayer*)pPkg;

	//CPlayer* pAttackPlayer = pOwner->FindPlayer( pAttackPlayerInfo->attackerID.dwPID );
	//if ( NULL == pAttackPlayer )
	//{
	//	D_WARNING("收到玩家攻击玩家消息,但在本mapsrv上找不到攻击者:%x\n", pAttackPlayerInfo->attackerID.dwPID );
	//	return false;
	//}

	//CNewMap* pTempMap = pAttackPlayer->GetCurMap();
	//if( NULL == pTempMap )
	//{
	//	D_ERROR( "CDealG_MPkg::OnPlayerAttackPlayer，玩家所在的地图不存在，踢此玩家%s\n", pAttackPlayer->GetAccount() );
	//	pOwner->NotifyGateSrvKickPlayer( pAttackPlayer->GetFullID() );
	//	//MGError errMsg;
	//	//errMsg.nErrCode = MGError::APPEAR_ERR_MAPDISMATCH;
	//	//errMsg.leavePlayerID = pAttackPlayer->GetFullID();
	//	//errMsg.lNotiPlayerID = pAttackPlayer->GetFullID();
	//	//pAttackPlayer->SendPkg< MGError>( &errMsg );
	//	return false;
	//}

	//BattleArgs args;
	//args.skillID = pAttackPlayerInfo->lSkillType;
	//args.targetID = pAttackPlayerInfo->targetID;
	//args.mainAoeTargetID = pAttackPlayerInfo->targetID;
	//args.sequenceID = pAttackPlayerInfo->sequenceID;
	//args.battleType = COMMON_SKILL_PVP;
	//args.attackPosX = pAttackPlayerInfo->fAttackerPosX;
	//args.attackPosY = pAttackPlayerInfo->fAttackerPosY;
	////return pTempMap->OnPlayerAttackPlayer( pAttackPlayer, pAttackPlayerInfo );
	//return pTempMap->HandlePlayerBattle( pAttackPlayer, &args );
	return true;
	TRY_END;
	return false;
}

//玩家倒计时时间到，或者玩家主动选择至复活点复活;
bool CDealG_MPkg::OnDefaultRebirth(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMDefaultRebirth) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMDefaultRebirth* pDefaultRebirth = (const GMDefaultRebirth*)pPkg;

	CPlayer* pPlayer = pOwner->FindPlayer( pDefaultRebirth->playerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_WARNING("收到玩家DefaultRebirth消息,但在本mapsrv上找不到该玩家:%x\n", pDefaultRebirth->playerID.dwPID );
		return false;
	}

	if ( pPlayer->IsWaitToDel() )
	{
		//D_ERROR( "CDealG_MPkg::OnDefaultRebirth，玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	//if ( pPlayer->IsInSwitchMap() )
	//{
	//	D_ERROR( "CDealG_MPkg::OnDefaultRebirth，玩家处于跳转状态，忽略除leave与FullInfo之外的其它任何消息\n" );
	//	return false;
	//}

	//D_DEBUG("玩家%s主动选择复活\n", pPlayer->GetAccount() );	
	//pPlayer->OnDefaultRebirth( pDefaultRebirth->sequenceID );//向玩家发复活点信息；
	pPlayer->NewDirectRebirth();
	
	return false;
	TRY_END;
	return true;
}

////玩家复活点复活准备完毕，收到后SRV应将玩家加入地图，并向周围人广播;
//bool CDealG_MPkg::OnRebirthReady(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	if ( NULL == pOwner )
//	{
//		return false;
//	}
//	if ( NULL == pPkg )
//	{
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	//检验收包大小是否正确；
//	if ( wPkgLen != sizeof(GMRebirthReady) )
//	{
//		//收包大小错误，可能是通信两端消息结构不一致；
//		return false;
//	}
//
//	const GMRebirthReady* pRebirthReady = (const GMRebirthReady*)pPkg;
//
//	CPlayer* pPlayer = pOwner->FindPlayer( pRebirthReady->rebirthPlayerID.dwPID );
//	if ( NULL == pPlayer )
//	{
//		D_WARNING("收到玩家RebirthReady消息,但在本mapsrv上找不到该玩家:%x\n", pRebirthReady->rebirthPlayerID.dwPID );
//		return false;
//	}
//
//	if ( pPlayer->IsWaitToDel() )
//	{
//		//D_ERROR( "CDealG_MPkg::OnRebirthReady，玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
//		return false;
//	}
//
//	//if ( pPlayer->IsInSwitchMap() )
//	//{
//	//	D_ERROR( "CDealG_MPkg::OnRebirthReady，玩家处于跳转状态，忽略除leave与FullInfo之外的其它任何消息\n" );
//	//	return false;
//	//}
//
//	//D_DEBUG( "收到玩家%s复活准备完成消息\n", pPlayer->GetAccount() );
//
//	pPlayer->OnRebirthReady( pRebirthReady->sequenceID );//重生准备处理;
//
//	return true;
//	TRY_END;
//	return false;
//}

//此消息作为新的apperar消息来使用
bool CDealG_MPkg::OnFullPlayerInfo(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	//检验收包大小是否正确；
	if ( wPkgLen != sizeof(GMFullPlayerInfo) )
	{
		//收包大小错误，可能是通信两端消息结构不一致；
		return false;
	}

	const GMFullPlayerInfo* pFullPlayerInfo = (const GMFullPlayerInfo*)pPkg;
	unsigned int sequenceID = pFullPlayerInfo->sequenceID;
	CPlayer* pTempPlayer = pOwner->FindPlayer( pFullPlayerInfo->playerID.dwPID );

	if( NULL == pTempPlayer )  //表明是发过来的第一个包,需要新建一个player来缓存数据
	{
		D_ERROR( "CDealG_MPkg::OnFullPlayerInfo，未找到对应玩家，可能处于排队中\n" );
		return false;			
	} 


	if ( pTempPlayer->IsWaitToDel() )
	{
		//D_ERROR( "CDealG_MPkg::OnFullPlayerInfo，玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	FullPlayerInfo* pCurPlayerInfo = pTempPlayer->GetFullPlayerInfo();
	if ( NULL == pCurPlayerInfo )
	{
		D_ERROR( "CDealG_MPkg::OnFullPlayerInfo，不可能错误1\n" );
		return false;
	}

	//如果返回true，说明收包已经成功
	if( !ReceiveFullStr<GMFullPlayerInfo,FullPlayerInfo>( pFullPlayerInfo, pCurPlayerInfo ) )
	{
		pTempPlayer->SetFullInfoFalse();//置包信息不完整标记；
		//没有收完full info，返回，直到收完后才进行后续处理；
		return true;
	}
	pTempPlayer->SetFullInfoTrue();//置包信息完整标记;

   	//包信息已完整，将玩家加入昵称映射管理器;
    if ( !CPlayerManager::InsertRolePlayer( pTempPlayer->GetNickName(), pTempPlayer ) )
	{
		D_ERROR( "CDealG_MPkg::OnFullPlayerInfo，InsertRolePlayer%s(%d:%d)失败，不再执行后续AfterFullInfoProc操作，踢去此玩家\n"
			, pTempPlayer->GetNickName(), pTempPlayer->GetFullID().wGID, pTempPlayer->GetFullID().dwPID );
		pOwner->NotifyGateSrvKickPlayer( pTempPlayer->GetFullID() );
		return false;
	}
	
	if ( !(pTempPlayer->IsCurPlayerCopyInfoSet()) )
	{
		//未置欲进副本信息，进普通地图处理；
		NormalMapAfterFullInfoProc( pOwner, pTempPlayer );
	} else {
		//欲进副本信息已置，副本上线处理；
		CopyMapAfterFullInfoProc( pOwner, pTempPlayer );
	}
	

	return true;
	TRY_END;
	return false;
}

///普通地图FullInfo后处理，与CopyMapAfterFullInfoProc对应；
bool CDealG_MPkg::NormalMapAfterFullInfoProc( CGateSrv* pGateSrv, CPlayer* pPlayer )
{
	if ( NULL == pGateSrv )
	{
		return false;
	}
	if ( NULL == pPlayer )
	{
		return false;
	}

	//full info中的最后一个包也已收到，开始让玩家正式上线
	CNewMap *pTempMap = CManNormalMap::FindMap( pPlayer->GetMapID() );//普通地图才进此函数；
	if( NULL == pTempMap )
	{ 
		D_WARNING("[NormalMapAfterFullInfoProc] MapServer上无此地图ID:%d，踢去此玩家%s\n", pPlayer->GetMapID(), pPlayer->GetAccount() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		return false;
	}

	if( ! pPlayer->InitPlayer( pTempMap ) )
	{
		//生成玩家失败
		D_WARNING( "收到玩家进入世界消息，但初始化玩家失败，踢去此玩家%s\n", pPlayer->GetAccount() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		return false;
	}

	//通知玩家出现
	unsigned long dyingleft = 0;
	pPlayer->OnlineProc( dyingleft );

	//是否越界
	if ( !pTempMap->IsGridValid( pPlayer->GetPosX(), pPlayer->GetPosY() ) )
	{
		D_ERROR( "%s:appear消息位置越界\n X:%d Y:%d，踢去此玩家\n", pPlayer->GetAccount(), pPlayer->GetPosX(), pPlayer->GetPosY() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//通知gatesrv进入mapsrv成功，在此之后，gatesrv将正式认为玩家进入了fighting状态；
	D_DEBUG( "通知玩家%s登录地图%d,点(%d,%d)成功\n", pPlayer->GetAccount(), pPlayer->GetMapID(), pPlayer->GetPosX(), pPlayer->GetPosY() );

	pPlayer->RegisterInvincible();

	pPlayer->SendEnterMapRstSuc();

	//进入地图后记录装备的信息以及装备的属性对玩家属性的加成
	ItemManagerSingle::instance()->RecordPlayerItemDetialInfo( pPlayer );

	ItemManagerSingle::instance()->CheckPlayerEquipItem( pPlayer );

	//更新玩家的信息
	//MGTmpInfoUpdate tmpInfoUpdate;
	//tmpInfoUpdate.playerID = pPlayer->GetFullID();
	////该消息已废除，因为相关属性已另行计算或通知, by dzj, 10.05.26, tmpInfoUpdate.tmpInfo = pPlayer->GetTmpProp();
	//pPlayer->SendPkg< MGTmpInfoUpdate >(&tmpInfoUpdate);

	//通知速度
	//pTempPlayer->NoticeSpeedChange();

	pPlayer->ResetPreFightFlag();

	pPlayer->NotifyDefaultRebirthPt();

#ifdef HAS_PATH_VERIFIER
	pPlayer->NotifyBlockQuadInfo();
#endif
	//通知gatesrv进入mapsrv成功，在此之后，gatesrv将正式认为玩家进入了fighting状态；
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if ( !(pTempMap->OnPlayerAppear( pPlayer )) )
	{
		D_ERROR( "NormalMapAfterFullInfoProc，玩家%s进普通地图处理，appear失败，其当前地图%d(%d,%d)\n"
			, pPlayer->GetAccount(), pPlayer->GetMapID(), pPlayer->GetPosX(), pPlayer->GetPosY() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		return false;
	}

	if ( dyingleft > 0 )
	{
		pPlayer->SendDyingPkg( dyingleft );//上次退出时仍有死亡倒计时；
	}

	pPlayer->CheckWeakState();

	////重新通知客户端速度
	pPlayer->NoticeSpeedChange();

	//玩家进入该地图的种族管理器
	AllRaceMasterManagerSingle::instance()->OnPlayerEnterMap( pPlayer );

	pPlayer->NotifyActivityState();

	//通知下一等级需要经验
	pPlayer->NotifyNextLevelExp();

#ifdef STAGE_MAP_SCRIPT
	pTempMap->OnMapPlayerEnterScriptHandle( pPlayer );//不能在复活的时候调用，因此不能合并至OnPlayerAppear；
#endif //STAGE_MAP_SCRIPT

	return true;
}

///副本FullInfo后处理，与NormalMapAfterFullInfoProc对应；
bool CDealG_MPkg::CopyMapAfterFullInfoProc( CGateSrv* pGateSrv, CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		return false;
	}
	if ( NULL == pGateSrv )
	{
		return false;
	}

	if ( !(pPlayer->IsCurPlayerCopyInfoSet()) )
	{
		D_ERROR( "CopyMapAfterFullInfoProc，玩家%s进副本处理，但玩家欲进副本信息未置，其当前地图%d(%d,%d)\n"
			, pPlayer->GetAccount(), pPlayer->GetMapID(), pPlayer->GetPosX(), pPlayer->GetPosY() );
		return false;
	}

	unsigned int nextCopyID = 0;
	unsigned short nextCopyMapID =0;
	bool isGetCopyIDOK = pPlayer->GetCurCopyMapInfo( nextCopyID, nextCopyMapID );
	if ( !isGetCopyIDOK )
	{
		D_WARNING("[CopyMapAfterFullInfoProc] 找不到玩家%s所在副本\n", pPlayer->GetAccount() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		return false;
	}

	CMuxMap* pOwnerMap = NULL;
	bool isCopyFound = false;
	if ( pPlayer->IsPlayerCurPCopy() )
	{
		//伪副本；
		pOwnerMap = CManNormalMap::FindMap( nextCopyMapID );
		isCopyFound = (NULL != pOwnerMap);
	} else {
		//真副本；
		isCopyFound = CManMuxCopy::FindCopyByCopyID( nextCopyID, pOwnerMap );
	}

	if ( ( !isCopyFound ) 
		|| (NULL == pOwnerMap) 
		)
	{
		D_ERROR( "CopyMapAfterFullInfoProc，玩家%s进副本处理，找不到其欲进入的副本%d，其当前地图%d(%d,%d)\n"
			, pPlayer->GetAccount(), nextCopyID, pPlayer->GetMapID(), pPlayer->GetPosX(), pPlayer->GetPosY() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		return false;
	}

	unsigned int toEnterScrollNum = 0;

	if ( !(pPlayer->GetPlayerToUseScrollType( toEnterScrollNum )) )
	{
		D_ERROR( "CopyMapAfterFullInfoProc，玩家%s进副本处理，找不到欲使用的卷轴类型，欲进入的副本%d，其当前地图%d(%d,%d)\n"
			, pPlayer->GetAccount(), nextCopyID, pPlayer->GetMapID(), pPlayer->GetPosX(), pPlayer->GetPosY() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		return false;
	}

	//bool isOrgEnterThis = false;
	///* const char* roleName, unsigned int& scrollType--in_out scrollType, bool& isOrgEnterThis--之前是否进过此副本 */
	//if ( !(pOwnerMap->PlayerEnterCheck( pPlayer->GetFullID()/*, pPlayer->GetNickName()*/, toEnterScrollType, isOrgEnterThis )) )
	//{
	//	D_ERROR( "CopyMapAfterFullInfoProc，玩家%s进副本处理，PlayerEnterCheck失败，欲进入的副本%d，其当前地图%d(%d,%d)\n"
	//		, pPlayer->GetAccount(), nextCopyID, pPlayer->GetMapID(), pPlayer->GetPosX(), pPlayer->GetPosY() );
	//	pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
	//	return false;
	//}

	if ( toEnterScrollNum <= 0 )
	{
		D_DEBUG( "玩家%s再次进入副本%d,copyid%d\n", pPlayer->GetAccount(), nextCopyMapID, nextCopyID );			
	} else {
		D_DEBUG( "玩家%s第一次进入副本%d, copyid%d\n", pPlayer->GetAccount(), nextCopyMapID, nextCopyID );			
		//此处扣副本券;
	}
	
	pPlayer->SetPlayerCurScrollType( toEnterScrollNum );//设置玩家当前使用的副本券类型，实际由于语义改变，多种副本券类型改由任务实现，此处toEnterScrollNum只用于表明需扣的副本券数量，因此本句已无作用；

	if ( !(pPlayer->InitPlayer( pOwnerMap )) )
	{
		D_WARNING( "CopyMapAfterFullInfoProc，收到玩家%s进世界消息，初始化玩家失败， 踢去此玩家\n", pPlayer->GetAccount() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		return false;
	}

	if ( toEnterScrollNum > 0 )
	{
		if ( !(pPlayer->OnNpcRemoveItem( COPY_SCROLL_TYPE/*副本券ID号*/, toEnterScrollNum )) )
		{
			D_WARNING( "取走玩家副本券(数量%d)失败，可能过程中副本券消失，踢此玩家下线\n", toEnterScrollNum );
			pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
			return false;
		}		
	}

	unsigned long dyingleft = 0;
	pPlayer->OnlineProc( dyingleft );

	//是否越界
	if ( !pOwnerMap->IsGridValid( pPlayer->GetPosX(), pPlayer->GetPosY() ) )
	{
		D_ERROR( "%s:位置越界(%d,%d)，踢去此玩家\n", pPlayer->GetAccount(), pPlayer->GetPosX(), pPlayer->GetPosY() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );

		return false;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//通知gatesrv进入mapsrv成功，在此之后，gatesrv将正式认为玩家进入了fighting状态；
	if ( pPlayer->IsPlayerCurPCopy() )
	{
		//伪副本；
		D_DEBUG( "通知玩家%s登录伪副本%d,点(%d,%d)成功\n", pPlayer->GetAccount(), pPlayer->GetMapID(), pPlayer->GetPosX(), pPlayer->GetPosY() );
	} else {
		//真副本；
		D_DEBUG( "通知玩家%s登录真副本%d, copyid%d,点(%d,%d)成功\n", pPlayer->GetAccount(), pPlayer->GetMapID(), nextCopyID, pPlayer->GetPosX(), pPlayer->GetPosY() );
	}

	pPlayer->RegisterInvincible();

	pPlayer->SendEnterMapRstSuc();

	//进入地图后记录装备的信息以及装备的属性对玩家属性的加成
	ItemManagerSingle::instance()->RecordPlayerItemDetialInfo( pPlayer );

	//pPlayer->ReloadSecondProp();
	ItemManagerSingle::instance()->CheckPlayerEquipItem( pPlayer );

	////更新玩家的信息
	////该消息已废除，因为相关属性已另行计算或通知, by dzj, 10.05.26, MGTmpInfoUpdate tmpInfoUpdate;
	//tmpInfoUpdate.playerID = pPlayer->GetFullID();
	//tmpInfoUpdate.tmpInfo = pPlayer->GetTmpProp();
	//pPlayer->SendPkg< MGTmpInfoUpdate >(&tmpInfoUpdate);

	//通知速度
	//pTempPlayer->NoticeSpeedChange();

	pPlayer->ResetPreFightFlag();

	pPlayer->NotifyDefaultRebirthPt();

#ifdef HAS_PATH_VERIFIER
	pPlayer->NotifyBlockQuadInfo();
#endif
	//通知gatesrv进入mapsrv成功，在此之后，gatesrv将正式认为玩家进入了fighting状态；
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if ( !(pOwnerMap->OnPlayerAppear( pPlayer )) )
	{
		D_ERROR( "CopyMapAfterFullInfoProc，玩家%s进副本处理，在副本%d中appear失败，其当前地图%d(%d,%d)\n"
			, pPlayer->GetAccount(), nextCopyID, pPlayer->GetMapID(), pPlayer->GetPosX(), pPlayer->GetPosY() );
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		return false;
	}

	if ( dyingleft > 0 )
	{
		pPlayer->SendDyingPkg( dyingleft );//上次退出时仍有死亡倒计时；
	}

	////重新通知客户端速度
	pPlayer->NoticeSpeedChange();

	pPlayer->NotifyActivityState();

	//通知下一等级需要经验
	pPlayer->NotifyNextLevelExp();

#ifdef STAGE_MAP_SCRIPT
	pOwnerMap->OnMapPlayerEnterScriptHandle( pPlayer );//不能在复活的时候调用，因此不能合并至OnPlayerAppear；
#endif //STAGE_MAP_SCRIPT

	////调试进出副本；
	//pPlayer->IssuePlayerLeaveCopyHF();//直接通知玩家下线
	//pPlayer->IssuePlayerLeaveCopyLF();//直接通知玩家下线
	if ( pPlayer->IsPlayerCurPCopy() )
	{
		//伪副本有可能在玩家进入前就已关闭，此时应通知倒计时；
		pOwnerMap->CheckIfPCopyClose( pPlayer );
	}

	return true;
}

bool CDealG_MPkg::OnQueryItemPkgInfo(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GMQueryItemPkg) )
	{
		return false;
	}

	const GMQueryItemPkg* queryMsg = (const GMQueryItemPkg* )pPkg;

	//@brief:获取角色的ID号
	CPlayer* pPlayer = pOwner->FindPlayer( queryMsg->lplayerID.dwPID );
	if ( pPlayer )
	{
		if ( pPlayer->IsDying() )
			return false;

		CNewMap* pMap = pPlayer->GetCurMap();//有副本之后，玩家可能在普通地图中，也可能在副本中,by dzj,09.10.15；//CManNormalMap::FindMap( pPlayer->GetMapID() );
		if ( pMap )
		{
			//@brief:查询道具包的信息
			pMap->HandleQueryItemsPkg( pPlayer, queryMsg->pkgID, queryMsg->queryType );
		} else {
			D_ERROR( "OnQueryItemPkgInfo,玩家所在地图空\n" );
		}
	} else {
		D_ERROR("查询包裹信息，接到了从GateSrv发送过来的消息,根据ID号获取玩家角色时为NULL");
	}

	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnPickAllItemFromPkg(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GMPickAllItem) )
	{
		return false;
	}

	const GMPickAllItem* pPickmsg = (const GMPickAllItem*)pPkg;


	CPlayer* pPlayer = pOwner->FindPlayer( pPickmsg->playID.dwPID );
	if ( !pPlayer )
		return false;

	if ( pPlayer->IsDying() )
		return false;

	//@brief:拾取所有的道具
	CItemsPackage* pItemPkg = CItemPackageManagerSingle::instance()->GetItemPackage( pPickmsg->pkgID );
	if ( pItemPkg )
	{
		pItemPkg->OnPickAllItems( pPlayer );
	}

	return true;
	TRY_END;
	return false;
}


//2008/05/04 当接收到DB发送过来的消息时
bool CDealG_MPkg::OnQueryNewItemRange(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GMBatchItemSequence) )
	{
		return false;
	}

	const GMBatchItemSequence* pItemSequence = (const GMBatchItemSequence*)pPkg;

	ItemIDGeneratorSingle::instance()->OnGetNewIDRange( pItemSequence->uiStartSequence, pItemSequence->uiEndSequence );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnUseItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen!= sizeof(GMUseItem) )
	{
		return false;
	}

	const GMUseItem* pUseItem = ( const GMUseItem *)pPkg;

	//@brief:找到该玩家
	CPlayer* pPlayer = pOwner->FindPlayer( pUseItem->lPlayerID.dwPID );
	if ( !pPlayer )
		return false;

	if ( pPlayer->IsDying() )
		return false;

	EquipItemManagerSingle::instance()->UseItem( pPlayer, pUseItem->itemUID );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnEquipItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof( GMEquipItem ) )
	{
		return false;
	}

	const GMEquipItem* pEquipItem = ( const GMEquipItem *)pPkg;

	//@brief:找到该玩家
	CPlayer* pPlayer = pOwner->FindPlayer( pEquipItem->lPlayerID.dwPID );
	if ( !pPlayer )
		return false;

	if ( pPlayer->IsDying() )
		return false;

	//道具是否是时装
	CBaseItem* pItem = pPlayer->GetItemDelegate().GetPkgItem( pEquipItem->equipitemUID );
	if(NULL == pItem)
		return false;
	
	CItemPublicProp* pPublicProp = pItem->GetItemPublicProp();
	if(NULL == pPublicProp)
		return false;

	if(pPublicProp->GetItemTypeID() != E_TYPE_FASHION)
		EquipItemManagerSingle::instance()->EquipItem( pPlayer,pEquipItem->equipitemUID,pEquipItem->equipIndex,pEquipItem->equipReturnIndex );
	else
		UseFashionRule::Execute( pPlayer, pEquipItem->equipitemUID, pEquipItem->equipIndex );

	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnUnUseItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GMUnUseItem) ) 
	{
		return false;
	}

	const GMUnUseItem* pUnUseItem = (const GMUnUseItem *)pPkg;

	CPlayer* pPlayer = pOwner->FindPlayer( pUnUseItem->unUsePlayer.dwPID );
	if ( !pPlayer )
		return false;

	if( pPlayer->IsDying() )
		return false;
	
	if(pUnUseItem->from < EQUIP_SIZE)
		EquipItemManagerSingle::instance()->UnUseItem( pPlayer ,0, pUnUseItem->from, pUnUseItem->to );
	else
		UseFashionRule::UnuseItem(pPlayer ,0, pUnUseItem->from - EQUIP_SIZE, pUnUseItem->to);	

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnSwapItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GMSwapItem) )
	{
		return false;
	}

	const GMSwapItem* pSwapItem = ( const GMSwapItem *)pPkg;

	CPlayer* pPlayer =  pOwner->FindPlayer( pSwapItem->playerID.dwPID );
	if ( !pPlayer )
		return false;

	if ( pPlayer->IsDying() )
		return false;

	EquipItemManagerSingle::instance()->SwapItemOnItemPkg( pPlayer, pSwapItem->from, pSwapItem->to );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnDropItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GMPlayerDropItem) )
	{
		return false;
	}

	const  GMPlayerDropItem* pDropMsg = (const GMPlayerDropItem *)(pPkg);

	CPlayer* pPlayer = pOwner->FindPlayer( pDropMsg->dropPlayer.dwPID );
	if ( !pPlayer)
		return false;

	if ( pPlayer->IsDying() )
		return false;

	D_DEBUG("%s 玩家主动丢弃道具 %d\n",pPlayer->GetNickName(),pDropMsg->itemUID   );

	//检查是否是任务物品,任务物品不能被删除,客户端不应该发此消息
	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CDealG_MPkg::OnDropItem, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs&   pkgInfo   = playerInfo->pkgInfo;
	for ( int i=NORMAL_PKG_SIZE; i<ARRAY_SIZE(pkgInfo.pkgs); ++i )
	{
		const ItemInfo_i& itemInfo = pkgInfo.pkgs[i];
		if (  itemInfo.uiID == pDropMsg->itemUID )
		{
			return false;//任务物品不可丢弃
		}
	}

	pPlayer->OnDropItem( pDropMsg->itemUID );

	return true;
	TRY_END;
	return false;	
}

bool CDealG_MPkg::OnSplitItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GMSplitItem) )
	{
		return false;
	}

	const GMSplitItem* pSplitItem = ( const GMSplitItem *)( pPkg );

	CPlayer* pPlayer = pOwner->FindPlayer( pSplitItem->splitPlayer.dwPID );
	if( !pPlayer )
		return false;

	//拆开道具
	pPlayer->SplitItem( pSplitItem->itemUID, pSplitItem->splitNum );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnPickItemFromPkg(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	if ( wPkgLen != sizeof(GMPickItem) )
	{
		return false;
	}

	const GMPickItem* pPickmsg = (const GMPickItem*)pPkg;

	CPlayer* pPlayer = pOwner->FindPlayer( pPickmsg->playID.dwPID );
	if ( !pPlayer )
		return false;

	if ( pPlayer->IsDying() )
		return false;

	//@brief:拾取一个道具
	CItemsPackage* pItemPkg = CItemPackageManagerSingle::instance()->GetItemPackage( pPickmsg->pkgID );
	if (  pItemPkg )
	{
		//s32 reason = -1;
		s32 itemID = pPickmsg->ItemID;

		pItemPkg->OnPickOneItem( pPlayer,itemID,pPickmsg->ItemPkgIndex );

		//@当道具为包为空时且道具包金钱被拾取
		if( pItemPkg->CheckItemPackEleEmpty()/* && !pItemPkg->ItemPkgHaveMoney() */ )
		{
			//移除道具包
			if ( CItemPackageManagerSingle::instance()->RemoveItemPackage(pItemPkg) )
			{
				//@回收道具包 
				CItemPackageManagerSingle::instance()->RecoverItemPackage( pItemPkg );
			}
		}
	}

	return true;
	TRY_END;
	return false;
}

//玩家执行上马操作
bool CDealG_MPkg::OnActiveMountItem(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT(GMActiveMount);
	
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( !pPlayer )
		return false;

	if ( pPlayer->IsDying() )
		return false;

	if( pPlayer->IsAnamorphicState() )
	{
		pPlayer->SendSystemChat("变形状态下无法使用骑乘");
		return false;
	}

	if ( pPlayer->IsHaveControlBuff() )
	{
		pPlayer->SendSystemChat("玩家有束缚性BUFF,不能执行上马操作");
		return false;
	}

	if ( pPlayer->IsMoving() )
	{
		pPlayer->SendSystemChat("玩家移动中,不允许执行上马操作");
		return false;
	}
	
	//是否在副本前置区域
	if ( pPlayer->GetSelfStateManager().IsInDunGeonPreArea() )
	{
		pPlayer->SendSystemChat("处于副本前置区，不能上马");
		return false;
	}

	if ( pPlayer->GetSelfStateManager().IsDoubleRideItem() )
	{
		pPlayer->SendSystemChat("双人骑乘道具，不能单独上马");
		return false;
	}

	if ( !pPlayer->IsRideState() )
	{
		//设置骑乘类型为装备
		pPlayer->GetSelfStateManager().GetRideState().SetRideItemState( E_RIDE_EQUIP );

		//将玩家处于骑乘状态
		pPlayer->GetSelfStateManager().SetPlayerRideLeaderState();
	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnAgreeBeInviteMount(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	CHECK_FUNC_PARAM_IS_NULL;

	TRY_BEGIN;

	PKG_CONVERT(GMAgreeBeInvite);

	//发起邀请的玩家
	CGateSrv* pGateSrv = CManG_MProc::FindProc( pMsg->launchID.wGID );
	if ( !pGateSrv )
	{
		D_ERROR("找不到发起邀请的骑乘玩家的GateSrv\n");
		return false;
	}

	//发起本次骑乘的玩家
	CPlayer* pLuanchPlayer = pGateSrv->FindPlayer( pMsg->launchID.dwPID );
	if ( !pLuanchPlayer )
	{
		D_DEBUG("发起骑乘邀请的玩家不在服务器了:%d\n",pMsg->launchID.dwPID  );
		return false;
	}

	pGateSrv = CManG_MProc::FindProc( pMsg->playerID.wGID );
	if ( !pGateSrv )
	{
		D_ERROR("找不到反馈骑乘邀请的玩家的GateSrv\n");
		return false;
	}

	//接受本次邀请的玩家
	CPlayer* pTargetPlayer = pGateSrv->FindPlayer( pMsg->playerID.dwPID );
	if ( !pTargetPlayer )
	{
		D_DEBUG("反馈骑乘邀请的玩家在服务器找不到:%d\n",pMsg->playerID.dwPID );
		return false;
	}

	//从pLuanchPlayer玩家邀请列表中删除被邀请pTargetPlaye玩家
	pLuanchPlayer->GetSelfStateManager().GetRideState().RemoveInvitePlayer( pTargetPlayer->GetDBUID() );
	//从pTargetPlayer被邀请玩家中将邀请pLuanchPlaye玩家删除
	pTargetPlayer->GetSelfStateManager().GetRideState().RemoveInviteLaunchPlayer( pLuanchPlayer->GetDBUID() );

	if ( pTargetPlayer->IsRideState() )
	{
		D_DEBUG("对方处于骑乘状态,不能被邀请上马\n");
		return false;
	}

	if ( pLuanchPlayer->IsAnamorphicState() )
	{
		pLuanchPlayer->SendSystemChat("变形状态下无法使用骑乘");
		return false;
	}

	if ( !pLuanchPlayer->GetSelfStateManager().IsDoubleRideItem() )
	{
		D_DEBUG("%s 非双人骑乘道具，不能接受骑乘应答\n", pLuanchPlayer->GetNickName() );
		pTargetPlayer->SendSystemChat("双人骑乘失败");
		return false;
	}

	//如果邀请玩家已经在骑乘状态了
	if ( pLuanchPlayer->IsRideState() )
	{
		CRideState& ridestate = pLuanchPlayer->GetSelfStateManager().GetRideState();
		if ( ridestate.GetPlayerRideState() == E_RIDE_CONSUME )//检查是否是可允许邀请的道具
			return false;

		if ( ridestate.IsRideTeamFull() )//检查队伍是否已经满了
			return false;
	}

	if ( pTargetPlayer->IsAnamorphicState() )
	{
		pTargetPlayer->SendSystemChat("变形状态下无法使用骑乘");
		return false;
	}

	if ( !pLuanchPlayer->IsCanOnMount() )
	{
		pLuanchPlayer->SendSystemChat("当前状态下您不能骑乘");
		return false;
	}

	if ( !pTargetPlayer->IsCanOnMount() )
	{
		pTargetPlayer->SendSystemChat("当前状态下您不能骑乘");
		return false;
	}

	if( !IsInAccurateDistance( pLuanchPlayer->GetPosX(),pLuanchPlayer->GetPosY(), pTargetPlayer->GetPosX(), pTargetPlayer->GetPosY(), 6/*客户端显示位置容许误差2*/ ) )
	{
		pLuanchPlayer->SendSystemChat("玩家之间距离过远,无法上马");
		return false;
	}

	//发送是否同意消息给发起骑乘请求的玩家
	bool IsAgree =pMsg->isAgree;
	MGInviteResult inviteResult;
	inviteResult.isAgree  = IsAgree;
	inviteResult.targetID = pMsg->playerID;
	pLuanchPlayer->SendPkg<MGInviteResult>(&inviteResult);

	//如果同意加入队伍
	if ( IsAgree )
	{
		CRideState& rideState = pLuanchPlayer->GetSelfStateManager().GetRideState();
		//如果之前没有处于骑乘状态
		if ( !rideState.IsActive() )
		{
			rideState.SetRideItemState( E_RIDE_EQUIP );
			pLuanchPlayer->GetSelfStateManager().SetPlayerRideLeaderState();
		}
		else
		{
			//骑乘道具不能邀请玩家
			if ( rideState.GetPlayerRideState() == E_RIDE_CONSUME )
			{
				D_ERROR("消耗品道具是不能邀请玩家的\n");
				return false;
			}
		}
		//骑乘队伍编号
		RideTeam* pTeam = rideState.GetRideTeam();
		if ( pTeam == NULL )
		{
			D_ERROR("要加入的骑乘队伍编号为 NULL\n");
			return false;
		}

		//如果创建队伍成功,将被邀请玩家加入骑乘队伍
		pTargetPlayer->GetSelfStateManager().SetPlayerRideAssistState( pTeam );
	}

	return true;
	TRY_END;
	return false;
}

//当玩家离开骑乘队伍时
bool CDealG_MPkg::OnLeaveMountTeam(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT(GMLeaveMountTeam);

	//接受的玩家
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( !pPlayer )
	{
		D_DEBUG("解除骑乘时找不到玩家:%d\n",pMsg->playerID.dwPID);
		return false;
	}

	bool isActive = pPlayer->GetSelfStateManager().IsOnRideState();
	if ( isActive )//如果骑乘处于激活状态
	{
		//骑乘状态消失
		pPlayer->GetSelfStateManager().SetPlayerRideStateDisappear();
	}
	else
	{
		D_ERROR("未处于骑乘状态就收到了解除骑乘状态的消息\n");
	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnInvitePlayerJoinMountTeam(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	CHECK_FUNC_PARAM_IS_NULL;

	TRY_BEGIN;

	PKG_CONVERT(GMInviteMount);

	CGateSrv* pGateSrv = CManG_MProc::FindProc( pMsg->playerID.wGID );
	if ( !pGateSrv )
	{
		D_ERROR("找不到发起邀请的骑乘玩家的GateSrv\n");
		return false;
	}
	//发起骑乘的玩家
	CPlayer* plaunchPlayer = pGateSrv->FindPlayer( pMsg->playerID.dwPID );
	if ( !plaunchPlayer )
	{
		D_ERROR("发起邀请骑乘的玩家在GateSrv找不到");
		return false;
	}

	if ( !plaunchPlayer->IsCanOnMount() )
		return false;

	//被邀请的任意一方都不能在非骑乘区域
	if ( plaunchPlayer->GetSelfStateManager().IsInDunGeonPreArea() )
	{
		plaunchPlayer->SendSystemChat("您处于非骑乘区域，不能发起邀请");
		return false;
	}

	//如果是双人骑乘道具，才能邀请，否则不能邀请玩家
	if ( !plaunchPlayer->GetSelfStateManager().IsDoubleRideItem() )
	{
		plaunchPlayer->SendSystemChat("非双人骑乘道具，无法发起骑乘邀请");
		return false;
	}

	//骑乘消耗性道具不能邀请玩家
	if ( plaunchPlayer->GetSelfStateManager().GetRideState().GetPlayerRideState() == E_RIDE_CONSUME )
	{
		D_DEBUG("处于骑乘道具的玩家不能邀请别人:%d\n",plaunchPlayer->GetFullID().dwPID);
		return false;
	}

	pGateSrv = CManG_MProc::FindProc( pMsg->targetID.wGID );
	if ( !pGateSrv )
	{
		D_ERROR("找不到接受骑乘邀请的玩家的GateSr\n");
		return false;
	}

	//接受骑乘的玩家
	CPlayer* ptargetlayer = pGateSrv->FindPlayer( pMsg->targetID.dwPID );
	if ( !ptargetlayer )
	{
		D_DEBUG("被邀请骑乘的玩家找不到\n");
		return false;
	}

	//邀请的玩家已经被邀请过了，正在等待回应，如果此时玩家再次发出邀请，则提示:邀请已发送，请您耐心等待对方回应
	if ( plaunchPlayer->GetSelfStateManager().GetRideState().IsInvitingTargetPlayer( ptargetlayer->GetDBUID() ) )
	{
		plaunchPlayer->SendSystemChat("邀请已发送，请您耐心等待对方回应");
		return false;
	}

	//检测被邀请玩家是否可骑乘
	if( !ptargetlayer->IsCanOnMount() )
	{
		plaunchPlayer->SendSystemChat("玩家处于不可骑乘状态,不能发送邀请");
		return false;
	}
	
	//玩家处于不可骑乘区域
	if( ptargetlayer->GetSelfStateManager().IsInDunGeonPreArea() )
	{
		plaunchPlayer->SendSystemChat("被邀请玩家处于非骑乘区域,不能发起邀请");
		return false;
	}

	//对方处于骑乘状态不能邀请
	if ( ptargetlayer->IsRideState() )
	{
		D_DEBUG("处于骑乘状态的玩家不能被邀请骑乘:%d\n",ptargetlayer->GetFullID().dwPID);
		return false;
	}

	//(玩家正在升级，改造，追加道具||玩家已经处于邀请状态了) 这两种情况下提示玩家正在忙
	if( ptargetlayer->GetSelfStateManager().GetRideState().IsInviteState() || ptargetlayer->IsPlayerForgingItem() )
	{
		MGPlayerBusying srvmsg;
		srvmsg.errCode =MGPlayerBusying::E_RIDETEAM;
		ACE_OS::strncpy( srvmsg.PlayerNickName, ptargetlayer->GetNickName(),MAX_NAME_SIZE  );
		plaunchPlayer->SendPkg<MGPlayerBusying>( &srvmsg );
		return false;
	}

	//性别判定
	if ( plaunchPlayer->GetSex() == ptargetlayer->GetSex() )
	{
		plaunchPlayer->SendSystemChat("同性别玩家不能互相邀请骑乘!");
		return false;
	}

	//种族判定
	if ( plaunchPlayer->GetRace() != ptargetlayer->GetRace() )
	{
		plaunchPlayer->SendSystemChat("不同种族不能邀请骑乘");
		return false;
	}

	//距离判定
	if( !IsInAccurateDistance( plaunchPlayer->GetPosX(), plaunchPlayer->GetPosY(), ptargetlayer->GetPosX(),ptargetlayer->GetPosY(), 6/*客户端显示位置容许误差2*/ ) )
	{
		plaunchPlayer->SendSystemChat("邀请的骑乘玩家之间的距离过远,邀请失败!!");
		return false;
	}

	//1.记录launchPlayer玩家已经邀请了targetlayer玩家
	plaunchPlayer->GetSelfStateManager().GetRideState().PushInviteTargetPlayerList( ptargetlayer->GetDBUID() );
	//2.记录targetlayer玩家已经被launchPlayer玩家邀请了
	ptargetlayer->GetSelfStateManager().GetRideState().PushInviteLaunchPlayerList( plaunchPlayer->GetFullID(), plaunchPlayer->GetDBUID() );

	//发送邀请给被邀请玩家
	MGBeInviteMount srvmsg;
	srvmsg.launchID = plaunchPlayer->GetFullID();
	ptargetlayer->SendPkg<MGBeInviteMount>(&srvmsg);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnNewMapSetPlayerRideState(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT(GMNewMapSetPlayerRideState);

	//接受的玩家
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( !pPlayer )
	{
		D_DEBUG("玩家进入新地图，设置骑乘状态，但是找不到对应的玩家\n");
		return false;
	}
	pPlayer->OnEnterNewMapSetRideState( pMsg->rideState, pMsg->attachInfo );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRideLeaderSwitchMap(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT(GMRideLeaderSwitchMap);

	//首先找到玩家所属的服务器
	CGateSrv* pGateSrv = CManG_MProc::FindProc( pMsg->playerID.wGID );
	if ( !pGateSrv )
	{
		D_ERROR("骑乘切换地图，找不到GateSrv\n");
		return false;
	}
	//在该地图上找到玩家
	CPlayer* pPlayer = pGateSrv->FindPlayer( pMsg->playerID.dwPID );
	if ( !pPlayer )
	{
		D_DEBUG("骑乘队长切换了地图,但是根据GateSrv记录的信息,找不到对应的骑乘队员!!\n");
		return false;
	}

	pPlayer->OnRideLeaderSwitchMap( pMsg );

	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnReqBuffEnd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMReqBuffEnd );
	CPlayer* plaunchPlayer = pOwner->FindPlayer( pMsg->changePlayer.dwPID );
	if ( NULL == plaunchPlayer )
	{
		D_DEBUG("结束BUFF的玩家找不到:%d\n", pMsg->changePlayer.dwPID);
		//MGReqBuffEnd errMsg;
		//errMsg.nErrorCode = MGReqBuffEnd::FAILED;
		//MsgToPut* pMsg = CreateSrvPkg( MGReqBuffEnd, pOwner, errMsg );
		//pOwner->SendPkgToGateServer( pMsg );
		return false;
	}
	
	bool isSuccess = plaunchPlayer->ActiveEndBuff( pMsg->buffIndex );

   if( !isSuccess )
   {
		MGReqBuffEnd errMsg;
		errMsg.nErrorCode = MGReqBuffEnd::FAILED;
		plaunchPlayer->SendPkg< MGReqBuffEnd >( &errMsg );
   }
	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnPlayerUseAssistSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMUseAssistSkill );

	bool bPlayer = pMsg->bPlayer;
	CPlayer* pUseSkillPlayer = pOwner->FindPlayer( pMsg->useSkillPlayer.dwPID );
	if( NULL == pUseSkillPlayer )
	{
		D_ERROR("CDealG_MPkg::OnPlayerUseAssistSkill 找不到gatesrv上的玩家\n");
		return false;
	}

	CMuxMap* pMap = pUseSkillPlayer->GetCurMap();
	if( NULL == pMap )
	{
		D_ERROR("玩家上的地图指针不存在\n");
		return false;
	}

	if( bPlayer )
	{
		return pMap->OnPlayerUseAssistSkillToPlayer( pUseSkillPlayer, pMsg->targetPlayer, pMsg->skillID, pMsg->isUnionSkill );
	}
	else
	{
		return pMap->OnPlayerUseAssistSkillToMonster( pUseSkillPlayer, pMsg->targetPlayer.dwPID, pMsg->skillID, pMsg->isUnionSkill );
	}
	
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnRecvTeamDetialInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMTeamDetialInfo );

	GroupTeamManagerSingle::instance()->OnRecvTeamDetialInfo( pMsg->TeamID,
		pMsg->playerArr,
		pMsg->memberSeqArr,
		pMsg->ArrSize ,
		(ITEM_SHAREMODE)pMsg->itemShareMode,
		(EXPERIENCE_SHAREMODE)pMsg->expShareMode,
		pMsg->rollItemMode,
		pMsg->leaderIndex
		);

	CGateSrv* pProc = CManG_MProc::FindProc( pMsg->lNoticePlayerID.wGID );
	if( NULL == pProc )
	{
		D_ERROR("找更新的组员的GATESRV错误\n");
		return false;
	}

	CPlayer* pUpdatePlayer = pProc->FindPlayer( pMsg->lNoticePlayerID.dwPID );
	if( NULL == pUpdatePlayer )
	{
		D_ERROR("找不到更新的玩家指针\n");
		return false;
	}

	pUpdatePlayer->SetNeedNotiTeamMember();

	return true;
	TRY_END;
	return false;
}
 
//bool CDealG_MPkg::OnPlayerEntranceTeam(CGateSrv *pOwner, const char *pPkg, const unsigned short &wPkgLen)
//{
//	TRY_BEGIN;
//
//	PKG_CONVERT( GMTeamEntrance );
//
//	GroupTeamManagerSingle::instance()->OnRecvPlayerEnterGroupTeam( 
//		pMsg->TeamID,
//		pMsg->playerID,
//		pMsg->memberSeq );
//
//	return true;
//	TRY_END;
//	return false;
//}

//bool CDealG_MPkg::OnPlayerExitTeam(CGateSrv *pOwner, const char *pPkg, const unsigned short &wPkgLen)
//{
//	TRY_BEGIN;
//
//	PKG_CONVERT( GMTeamExit );
//
//	GroupTeamManagerSingle::instance()->OnRecvPlayerLeaveGroupTeam(
//		pMsg->TeamID,
//		pMsg->playerID,
//		pMsg->memberSeq
//		);
//
//	return true;
//	TRY_END;
//	return false;
//}
 
bool CDealG_MPkg::OnRecvIsAgreeRollItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMAgreeRollItem );

	GroupTeamManagerSingle::instance()->OnRecvPlayerIsAgreeRollItem(
		pMsg->rollItemID,
		pMsg->playerID,
		pMsg->agree
		);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRecvChangeTeamLeader(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeTeamLeader );

	GroupTeamManagerSingle::instance()->OnRecvChangeTeamLeader(
		pMsg->TeamID,
		pMsg->playerID
		);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRecvChangeRollLevelItemMode(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeTeamRollLevel );

	GroupTeamManagerSingle::instance()->OnRecvChangeTeamRollItemLevel(
		pMsg->TeamID,
		pMsg->rollItemLevel
		);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRecvChangeItemShareMode(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeTeamItemMode );
	
	GroupTeamManagerSingle::instance()->OnRecvChangeTeamItemShareMode( pMsg->TeamID, pMsg->itemShareMode );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRecvChangeExpreShareMode(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeTeamExpreMode );

	GroupTeamManagerSingle::instance()->OnRecvChangeTeamExpreShareMode( pMsg->TeamID,pMsg->expreShareMode );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRecvKickPlayerOnTeam(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMKickTeamPlayer );

	GroupTeamManagerSingle::instance()->OnRecvKickPlayerOnTeam( pMsg->TeamID, pMsg->playerID );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRecvDisBandTeam(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMDisBandTeam );

	GroupTeamManagerSingle::instance()->OnRecvDisBandTeam( pMsg->TeamID );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnTeamMemberGetNewItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMTeamMemberGetNewItem );

	GroupTeamManagerSingle::instance()->OnRecvTeamPlayerGetNewItem( pMsg->TeamID, pMsg->itemTypeID, pMsg->pickPlayerID );

	TRY_END;
	return false;
}

/*
	//#define  G_M_ADDSTH_TO_PLATE 0x1528 //玩家往托盘中加物品
	typedef struct StrGMAddSthToPlate
	{
        static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_ADDSTH_TO_PLATE;
		PlayerID selfPlayerID;//操作执行玩家；
		EPlateType plateType;//托盘类型
		EPlatePos  platePos;//放置的位置；
		ItemInfo_i sthID;//所添加物品的ID号；
	}GMAddSthToPlate;
	MSG_SIZE_GUARD(GMAddSthToPlate);

	//#define  G_M_TAKESTH_FROM_PLATE 0x1529 //玩家从托盘中取物品
	typedef struct StrGMTakeSthFromPlate
	{
        static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_TAKESTH_FROM_PLATE;
		PlayerID selfPlayerID;//操作执行玩家；
		EPlatePos platePos;//取走位置；
		ItemInfo_i sthID;//取走物品的ID号；
	}GMTakeSthFromPlate;
	MSG_SIZE_GUARD(GMTakeSthFromPlate);

	//#define  G_M_PLATE_EXEC         0x152a //玩家尝试执行托盘操作
	typedef struct StrGMPlateExec
	{
        static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_PLATE_EXEC;
		PlayerID selfPlayerID;//操作执行玩家；
	}GMPlateExec;
	MSG_SIZE_GUARD(GMPlateExec);

	//#define  G_M_CLOSE_PLATE        0x152b //玩家关闭托盘
	typedef struct StrGMClosePlate
	{
        static const unsigned char  byPkgType = G_M;
		static const unsigned short wCmd = G_M_CLOSE_PLATE;
		PlayerID selfPlayerID;//操作执行玩家；
	}GMClosePlate;
	MSG_SIZE_GUARD(GMClosePlate);
*/

//Register( G_M_ITEM_UPGRADE_PLATE, &OnPlayerReqItemUpPlate);//玩家请求装备升级托盘
bool CDealG_MPkg::OnPlayerReqItemUpPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMItemUpgradePlate );

	CPlayer* pReqPlayer = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID );
	if ( NULL == pReqPlayer )
	{
		D_WARNING( "CDealG_MPkg::OnPlayerReqItemUpPlate, NULL == pReqPlayer\n" );
		return false;
	}

	pReqPlayer->ShowPlateCmd( PLATE_UPGRADE );//弹升级托盘；

	return true;
	TRY_END;
	return false;
}

//玩家往托盘中加物品
bool CDealG_MPkg::OnPlayerAddSthToPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMAddSthToPlate );

	CPlayer* pPlatePlayer = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID );
	if ( NULL == pPlatePlayer )
	{
		return false;
	}

	pPlatePlayer->OnAddSthToPlate( pMsg->plateType, pMsg->sthID, pMsg->platePos );

	return true;
	TRY_END;
	return false;
}

//玩家从托盘中取物品
bool CDealG_MPkg::OnPlayerTakeSthFromPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMTakeSthFromPlate );

	CPlayer* pPlatePlayer = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID );
	if ( NULL == pPlatePlayer )
	{
		return false;
	}

	pPlatePlayer->OnTakeSthFromPlate( pMsg->sthID, pMsg->platePos );

	return true;
	TRY_END;
	return false;
}

//玩家尝试执行托盘操作
bool CDealG_MPkg::OnPlayerTryPlateExec( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPlateExec );

	CPlayer* pPlatePlayer = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID );
	if ( NULL == pPlatePlayer )
	{
		return false;
	}

	pPlatePlayer->OnExecPlate();

	return true;
	TRY_END;
	return false;
}

//玩家关闭托盘
bool CDealG_MPkg::OnPlayerClosePlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMClosePlate );

	CPlayer* pPlatePlayer = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID );
	if ( NULL == pPlatePlayer )
	{
		return false;
	}

	pPlatePlayer->OnClosePlate();

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnSendTradeReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSendTradeReq);

	CPlayer *pSelf = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID);
	if (NULL == pSelf)
		return false;
	
	/// 请求是否成功,如果不成功记录失败原因
	int iErrorNo = 0;
	do 
	{
		PlayerID selfRecvFromPlayer = pSelf->RecvFromPlayer();
		if(false/*(selfRecvFromPlayer.dwPID != 0) || (selfRecvFromPlayer.wGID != 0)*/)
		{	
			iErrorNo = GCSendTradeReqError::SENDER_HAS_RECV_TRADE_REQ;//1.发送方已收到邀请
			break;
		}

		/*CTradeInfo * pSelfTradeInfo = pSelf->TradeInfo();*/
		if(false/*NULL != pSelfTradeInfo*/)
		{		
			iErrorNo = GCSendTradeReqError::SENDER_IN_TRADING;//2.发送方已在交易中
			break;
		}
			
		CPlayer *pPeer = NULL;
		if(pMsg->targetPlayerID.wGID == pMsg->selfPlayerID.wGID)
			pPeer = pOwner->FindPlayer( pMsg->targetPlayerID.dwPID);
		else
		{
			CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( pMsg->targetPlayerID.wGID );
			if(NULL != pPeerGateSrv)
				pPeer = pPeerGateSrv->FindPlayer( pMsg->targetPlayerID.dwPID);
		}

		if(NULL == pPeer)
		{				
			iErrorNo = GCSendTradeReqError::RECVER_NOT_EXIST; //3.接收方不存在
			break;
		}
				
		if(pSelf->GetRace() != pPeer->GetRace())
		{
			iErrorNo = GCSendTradeReqError::DIFFERENT_RACE;//11.种族不同
			break;
		}

		if(pPeer->IsDying())
		{
			iErrorNo = GCSendTradeReqError::RECVER_HAS_DIED;//4.接收方已死亡
			break;
		}
					
		PlayerID peerRecvFromPlayer = pPeer->RecvFromPlayer();
		//if((peerRecvFromPlayer.dwPID != 0) || (peerRecvFromPlayer.wGID != 0))//如果收到过交易请求
		//{
		//	// 先前的请求者已不存在，则可以接受其他请求
		//	CPlayer *pRecvPlayerFirst = pOwner->FindPlayer(peerRecvFromPlayer.dwPID);
		//	if(NULL == pRecvPlayerFirst)
		//	{
		//		//取消旧的请求
		//		MGCancelTrade msg;
		//		pPeer->SendPkg<MGCancelTrade>(&msg);

		//		// 重置
		//		peerRecvFromPlayer.dwPID = 0;
		//		peerRecvFromPlayer.wGID = 0;
		//		pPeer->RecvFromPlayer(peerRecvFromPlayer);
		//	}
		//}

		if((peerRecvFromPlayer.dwPID != 0) || (peerRecvFromPlayer.wGID != 0))
		{
			iErrorNo = GCSendTradeReqError::RECVER_HAS_RECV_TRADE;//5.接收方已收到邀请
			break;
		}
						
		CTradeInfo *pPeerTradeInfo = pPeer->TradeInfo();
		if(NULL != pPeerTradeInfo)
		{
			iErrorNo = GCSendTradeReqError::RECVER_IN_TRADING;//6.接收方已在交易中
			break;
		}
		
		if(false/*拥抱状态*/)
		{
			iErrorNo = GCSendTradeReqError::RECVER_IN_CUDDLING;//7.接收方已在拥抱状态
			break;
		}
			
		if((pSelf->IsPlayerForgingItem()) || (pPeer->IsPlayerForgingItem())/*非常态：锻造道具的状态*/)
		{
			iErrorNo = GCSendTradeReqError::RECVER_IN_UNNORAML;//8.接收方已在非常态
			break;
		}

		if( pSelf->IsEvil() != pPeer->IsEvil() )
		{
			if( pPeer->IsEvil() )
			{
				iErrorNo = GCSendTradeReqError::RECVER_IS_EVIL;
				break;
			}
			if( pSelf->IsEvil() )
			{
				iErrorNo = GCSendTradeReqError::SENDER_IS_EVIL;
				break;
			}
		}
		
		pPeer->RecvFromPlayer(pMsg->selfPlayerID);

		// 满足发送条件,发送给受邀方
		MGRecvTradeReq msg;
		msg.sendPlayerId = pMsg->selfPlayerID;
		SafeStrCpy(msg.sendPlayerName, pSelf->GetNickName());
		
		pPeer->SendPkg<MGRecvTradeReq>(&msg);								
	} while(0);
	
	if(0 != iErrorNo)// 发送请求失败
	{
		MGSendTradeReqError msg;
		msg.errInfo.tResult = (GCSendTradeReqError::eTradeError)(iErrorNo);

		pSelf->SendPkg<MGSendTradeReqError>(&msg);
	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnSendTradeReqResult( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSendTradeReqResult);

	CPlayer *pSelf = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID);
	if (NULL == pSelf)
		return false;

	// 对方是否存在？
	PlayerID peerPlayerID = pSelf->RecvFromPlayer();
	
	CPlayer *pPeer = NULL;
	if(peerPlayerID.wGID == pMsg->selfPlayerID.wGID)
		pPeer = pOwner->FindPlayer( peerPlayerID.dwPID);
	else
	{
		CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( peerPlayerID.wGID );
		if(NULL != pPeerGateSrv)
			pPeer = pPeerGateSrv->FindPlayer( peerPlayerID.dwPID);
	}

	if(NULL == pPeer)
	{
		PlayerID tempID;
		tempID.dwPID = 0;
		tempID.wGID = 0;

		pSelf->RecvFromPlayer(tempID);

		//如果自身"已同意"，则通知自身
		if(pMsg->bAgree)
		{
			MGSendTradeReqResult msg;
			msg.tResult = MGSendTradeReqResult::SENDER_NOT_EXIST;
			msg.lSelfPlayerID = pMsg->selfPlayerID;

			pSelf->SendPkg<MGSendTradeReqResult>(&msg);
		}

		return false;
	}

	//自己拒绝
	if(!pMsg->bAgree)
	{
		// 重置
		PlayerID tempID;
		tempID.dwPID = 0;
		tempID.wGID = 0;

		pSelf->RecvFromPlayer(tempID);

		MGSendTradeReqResult msg;
		msg.lSelfPlayerID = pMsg->selfPlayerID;
		msg.tResult = (MGSendTradeReqResult::eTradeResult)(GCSendTradeReqResult::TRADE_REQ_REFUSE);

		pPeer->SendPkg<MGSendTradeReqResult>(&msg);

		return false;
	}
	
	/// 判断是否满足交易条件,如果不满足记录原因
	int iErrorNo = 0;
	if(pSelf->IsDying())
		iErrorNo = MGSendTradeReqResult::RECVER_HAS_DIED;//1.自己已死亡
	else
	{
		CTradeInfo *pSelfTrade = pSelf->TradeInfo();
		if(NULL != pSelfTrade)
			iErrorNo = MGSendTradeReqResult::RECVER_IN_TRADING;//2.自己在交易状态
		else
		{
			if(false/*拥抱*/)
				iErrorNo = MGSendTradeReqResult::RECVER_IN_CUDDLING;//3.自己拥抱状态
			else
			{
				//PlayerID tradeFromPlayer = pSelf->RecvFromPlayer();
				//CPlayer *pPeer = pOwner->FindPlayer( tradeFromPlayer.dwPID);
				if(NULL == pPeer)
					iErrorNo = MGSendTradeReqResult::SENDER_NOT_EXIST;//4.对方不存在
				else
				{
					if(pPeer->IsDying())
						iErrorNo = MGSendTradeReqResult::SENDER_HAS_DIED;//5.对方已死亡
					else
					{
						CTradeInfo *pPeerTrade = pPeer->TradeInfo();
						if(NULL != pPeerTrade)
							iErrorNo = MGSendTradeReqResult::SENDER_IN_TRADING;//6.对方已在交易中
						else
						{
							if(false/*拥抱*/)
								iErrorNo = MGSendTradeReqResult::SENDER_IN_CUDDLING;//7.对方在拥抱状态
							else 
							{
								CMuxMap *pMap = pSelf->GetCurMap();
								ACE_UNUSED_ARG( pMap );
								if( !IsInAccurateDistance(pSelf->GetPosX(), pSelf->GetPosY(), pPeer->GetPosX(), pPeer->GetPosY(), 7/*客户端显示位置容许误差2*/))
									iErrorNo = MGSendTradeReqResult::BOTH_OUT_TRADE_RANGE;//8.双方在5m外
								else
								{
									// 交易建立成功
									unsigned int tradeID = 0;
									CTradeInfo *pTradeInfo = CTradeInfoManager::NewTradeInfo(pSelf->RecvFromPlayer(), pMsg->selfPlayerID,tradeID);
									if(NULL == pTradeInfo)
									{
										PlayerID tempID;
										tempID.dwPID = 0;
										tempID.wGID = 0;
										
										pSelf->RecvFromPlayer(tempID);
										iErrorNo = MGSendTradeReqResult::OTHER_ERROR;//9.其他错误
									}
									else
									{
										pSelf->TradeInfo(tradeID);
										pPeer->TradeInfo(tradeID);

										MGSendTradeReqResult msg;
										msg.tResult = (MGSendTradeReqResult::eTradeResult)(GCSendTradeReqResult::TRADE_CREATE_OK);
										msg.lSelfPlayerID = pMsg->selfPlayerID;
										
										// 发送给自己和对方
										pSelf->SendPkg<MGSendTradeReqResult>(&msg);
										pPeer->SendPkg<MGSendTradeReqResult>(&msg);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if(iErrorNo != 0)
	{
		PlayerID tempID;
		tempID.dwPID = 0;
		tempID.wGID = 0;
		
		pSelf->RecvFromPlayer(tempID);

		MGSendTradeReqResult msg;
		msg.tResult = (MGSendTradeReqResult::eTradeResult)(iErrorNo);
		msg.lSelfPlayerID = pMsg->selfPlayerID;
		
		// 发送给双方
		pSelf->SendPkg<MGSendTradeReqResult>(&msg);
		pPeer->SendPkg<MGSendTradeReqResult>(&msg);
	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnAddOrDeleteItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealG_MPkg::OnAddOrDeleteItem, NULL == pOwner\n" );
		return false;
	}
	if ( NULL == pPkg )
	{
		D_ERROR( "CDealG_MPkg::OnAddOrDeleteItem, NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMAddOrDeleteItem);

	CPlayer *pSelf = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID);
	if (NULL == pSelf)
	{
		D_ERROR( "CDealG_MPkg::OnAddOrDeleteItem, NULL == pSelf\n" );
		return false;
	}
	
	CTradeInfo *pTrade = pSelf->TradeInfo();
	if (NULL == pTrade)
	{
		D_ERROR( "CDealG_MPkg::OnAddOrDeleteItem, NULL == pTrade\n" );
		return false;
	}
	
	// 根据交易信息获取对方信息
	PlayerID peerPlayerID;
	CTradeInfo::eTradePlayerIndex tIndex, tPeerIndex;
	if(pTrade->InvitePlayerID() == pMsg->selfPlayerID)
	{
		peerPlayerID = pTrade->TargetPlayerID();
		tIndex = CTradeInfo::INVITE_PLAYER;
		tPeerIndex = CTradeInfo::TARGET_PLAYER;
	} else {
		peerPlayerID = pTrade->InvitePlayerID();
		tIndex = CTradeInfo::TARGET_PLAYER;
		tPeerIndex = CTradeInfo::INVITE_PLAYER;
	}

	// 根据物品编号获取在包裹中的位置
	int iItemIndex = -1;
	unsigned int uiItemTypeID = 0;
	FullPlayerInfo* pFullPlayerInfo = pSelf->GetFullPlayerInfo();
	if ( NULL == pFullPlayerInfo )
	{
		D_ERROR( "CDealG_MPkg::OnAddOrDeleteItem, NULL == pFullPlayerInfo\n" );
		return false;
	}
	for ( int i = 0; i < ARRAY_SIZE(pFullPlayerInfo->pkgInfo.pkgs); i++ )
	{
		if(pFullPlayerInfo->pkgInfo.pkgs[i].uiID == pMsg->uiItemID)
		{	
			iItemIndex = i;
			uiItemTypeID = pFullPlayerInfo->pkgInfo.pkgs[i].usItemTypeID;
			break;
		}
	}


	// 回复
	MGAddOrDeleteItem msg;

	// 没有找到
	bool bError = false;
	if ( iItemIndex == -1 )
	{
		bError = true;
	} else if( iItemIndex >= NORMAL_PKG_SIZE ) {//如果是任务物品，不能被交易
		bError = true;
	} else {
		// 根据操作类型进行操作
		if(pMsg->tOperateType == GMAddOrDeleteItem::ADD_ITEM)
		{
			unsigned int errType = 0;
			if ( !pSelf->IsCanTradeWithPlayer( pMsg->uiItemID , errType ) )//如果不能与玩家交易,返回错误原因
			{
				if ( errType == 1 )//绑定道具
				{
					msg.tResult = MGAddOrDeleteItem::FAIL_BIND_ITEM;
				} else {//如果是保护道具
					msg.tResult = MGAddOrDeleteItem::FAIL_PROTECT_ITEM;
				}				
				msg.operatePlayerID = pMsg->selfPlayerID;
				msg.tOperateType = (MGAddOrDeleteItem::eOperateType)(pMsg->tOperateType);
				msg.uiItemID = pMsg->uiItemID;
				pSelf->SendPkg<MGAddOrDeleteItem>(&msg);
				return false;
			}
			if ( !pTrade->AddItem(tIndex, iItemIndex) )
			{
				bError = true;
			}
		} else {
			pTrade->DeleteItem(tIndex, iItemIndex);
		}
	}

	if ( !bError )
	{
		if ( iItemIndex >= ARRAY_SIZE(pFullPlayerInfo->pkgInfo.pkgs) )
		{
			D_ERROR( "CDealG_MPkg::OnAddOrDeleteItem, iItemIndex(%d)越界\n", iItemIndex );
			return false;
		}
		msg.operatePlayerID = pMsg->selfPlayerID;
		msg.tOperateType = (MGAddOrDeleteItem::eOperateType)(pMsg->tOperateType);
		msg.uiItemID = pMsg->uiItemID;
		msg.uiItemTypeID = uiItemTypeID;
		msg.usWearPoint = pFullPlayerInfo->pkgInfo.pkgs[iItemIndex].usWearPoint;
		msg.ucCount = pFullPlayerInfo->pkgInfo.pkgs[iItemIndex].ucCount;
		//去除追加，原位置用于随机集 msg.usAddId = pFullPlayerInfo->pkgInfo.pkgs[iItemIndex].randSet/*usAddId*/;
		msg.usAddId = 0;
		msg.ucLevelUp = pFullPlayerInfo->pkgInfo.pkgs[iItemIndex].ucLevelUp;
		msg.tResult = MGAddOrDeleteItem::SUCCESS;

		pSelf->SendPkg<MGAddOrDeleteItem>(&msg);

		CPlayer *pPeer = NULL;
		if(peerPlayerID.wGID == pMsg->selfPlayerID.wGID)
			pPeer = pOwner->FindPlayer( peerPlayerID.dwPID);
		else
		{
			CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( peerPlayerID.wGID );
			if(NULL != pPeerGateSrv)
				pPeer = pPeerGateSrv->FindPlayer( peerPlayerID.dwPID);
		}

		if(NULL != pPeer)
			pPeer->SendPkg<MGAddOrDeleteItem>(&msg);

		// 如果任一方已锁定/交易确认，则重置
		CTradeInfo::TradeData::eState currentState = pTrade->State(tIndex);
		CTradeInfo::TradeData::eState peerCurrentState = pTrade->State(tPeerIndex);
		if((CTradeInfo::TradeData::TRADE_LOCK == currentState) || (CTradeInfo::TradeData::TRADE_CONFIRM == currentState))
		{
			//取消锁定(selfPlayerID是主动方)
			pTrade->State(tIndex, CTradeInfo::TradeData::TRADE_INIT);

			MGLockTrade msg;
			msg.lOperatePlayerID = pMsg->selfPlayerID;
			msg.bLock = false;

			pSelf->SendPkg<MGLockTrade>(&msg);
			pPeer->SendPkg<MGLockTrade>(&msg);
		}

		if((CTradeInfo::TradeData::TRADE_LOCK == peerCurrentState) || (CTradeInfo::TradeData::TRADE_CONFIRM == peerCurrentState))
		{
			//取消锁定(peerPlayerID是被动方)
			pTrade->State(tPeerIndex, CTradeInfo::TradeData::TRADE_INIT);

			MGLockTrade msg;
			msg.lOperatePlayerID = peerPlayerID;
			msg.bLock = false;

			pSelf->SendPkg<MGLockTrade>(&msg);
			pPeer->SendPkg<MGLockTrade>(&msg);
		}
	} else {
		msg.operatePlayerID = pMsg->selfPlayerID;
		msg.tOperateType = (MGAddOrDeleteItem::eOperateType)(pMsg->tOperateType);
		msg.uiItemID = pMsg->uiItemID;
		msg.tResult = MGAddOrDeleteItem::FAILURE;

		pSelf->SendPkg<MGAddOrDeleteItem>(&msg);
	}
	
	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnSetTradeMoney( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSetTradeMoney);

	CPlayer *pSelf = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID);
	if (NULL == pSelf)
		return false;

	CTradeInfo *pTrade = pSelf->TradeInfo();
	if(NULL == pTrade)
		return false;

	// 根据交易信息获取对方信息
	PlayerID peerPlayerID;
	CTradeInfo::eTradePlayerIndex tIndex, tPeerIndex;
	if(pTrade->InvitePlayerID() == pMsg->selfPlayerID)
	{
		tIndex = CTradeInfo::INVITE_PLAYER;
		tPeerIndex = CTradeInfo::TARGET_PLAYER;

		peerPlayerID = pTrade->TargetPlayerID();
	}
	else
	{
		tIndex = CTradeInfo::TARGET_PLAYER;
		tPeerIndex = CTradeInfo::INVITE_PLAYER;

		peerPlayerID = pTrade->InvitePlayerID();
	}

	//CPlayer *pPeer = pOwner->FindPlayer(peerPlayerID.dwPID);
	CPlayer *pPeer = NULL;
	if(peerPlayerID.wGID == pMsg->selfPlayerID.wGID)
		pPeer = pOwner->FindPlayer( peerPlayerID.dwPID);
	else
	{
		CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( peerPlayerID.wGID );
		if(NULL != pPeerGateSrv)
			pPeer = pPeerGateSrv->FindPlayer( peerPlayerID.dwPID);
	}
	
	if(NULL == pPeer)
		return false;

	// 判断是否足够交易？
	FullPlayerInfo * playerInfo = pSelf->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CDealG_MPkg::OnSetTradeMoney, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	if(pMsg->uiGoldMoney > pSelf->GetGoldMoney() )
	{
		MGSetTradeMoney msg;
		msg.operatePlayerID = pMsg->selfPlayerID;
		msg.uiGoldMoney = pMsg->uiGoldMoney;
		msg.bOK = false;
		
		pSelf->SendPkg<MGSetTradeMoney>(&msg);
		return true;
	}

	// 设置金钱并回复
	pTrade->Money(tIndex, pMsg->uiGoldMoney);

	MGSetTradeMoney msg;
	msg.operatePlayerID = pMsg->selfPlayerID;
	msg.uiGoldMoney = pMsg->uiGoldMoney;
	msg.bOK = true;

	pSelf->SendPkg<MGSetTradeMoney>(&msg);
	pPeer->SendPkg<MGSetTradeMoney>(&msg);

	// 如果任一方已锁定/交易确认，则重置
	CTradeInfo::TradeData::eState currentState = pTrade->State(tIndex);
	CTradeInfo::TradeData::eState peerCurrentState = pTrade->State(tPeerIndex);
	if((CTradeInfo::TradeData::TRADE_LOCK == currentState) || (CTradeInfo::TradeData::TRADE_CONFIRM == currentState))
	{
		//取消锁定(selfPlayerID是主动方)
		pTrade->State(tIndex, CTradeInfo::TradeData::TRADE_INIT);

		MGLockTrade msg;
		msg.lOperatePlayerID = pMsg->selfPlayerID;
		msg.bLock = false;

		pSelf->SendPkg<MGLockTrade>(&msg);
		pPeer->SendPkg<MGLockTrade>(&msg);
	}

	if((CTradeInfo::TradeData::TRADE_LOCK == peerCurrentState) || (CTradeInfo::TradeData::TRADE_CONFIRM == peerCurrentState))
	{
		//取消锁定(peerPlayerID是被动方)
		pTrade->State(tPeerIndex, CTradeInfo::TradeData::TRADE_INIT);

		MGLockTrade msg;
		msg.lOperatePlayerID = peerPlayerID;
		msg.bLock = false;

		pSelf->SendPkg<MGLockTrade>(&msg);
		pPeer->SendPkg<MGLockTrade>(&msg);
	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnCancelTrade( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMCancelTrade); 

	CPlayer *pSelf = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID);
	if (NULL == pSelf)
		return false;

	CTradeInfo *pTradeInfo = pSelf->TradeInfo();
	if(NULL == pTradeInfo)
		return false;

	// 根据交易信息获取对方信息
	CPlayer *pPeer = NULL;
	PlayerID invitePlayerID = pTradeInfo->InvitePlayerID();
	PlayerID targetPlayerID = pTradeInfo->TargetPlayerID();
	if(invitePlayerID == pMsg->selfPlayerID)
	{
		if(targetPlayerID.wGID == pMsg->selfPlayerID.wGID)
			pPeer = pOwner->FindPlayer(targetPlayerID.dwPID);
		else
		{
			CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( targetPlayerID.wGID );
			if(NULL != pPeerGateSrv)
				pPeer = pPeerGateSrv->FindPlayer( targetPlayerID.dwPID);
		}
	}
	else
	{	
		if(invitePlayerID.wGID == pMsg->selfPlayerID.wGID)
			pPeer = pOwner->FindPlayer(invitePlayerID.dwPID);
		else
		{
			CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( invitePlayerID.wGID );
			if(NULL != pPeerGateSrv)
				pPeer = pPeerGateSrv->FindPlayer( invitePlayerID.dwPID);
		}
	}

	if(NULL != pPeer)
	{
		// 删除交易信息
		pSelf->DelTradeInfo();

		// 设置双方的相关信息
		PlayerID tempID;
		tempID.dwPID = 0;
		tempID.wGID = 0;

		pSelf->RecvFromPlayer(tempID);
		pSelf->TradeInfo(0);

		pPeer->RecvFromPlayer(tempID);
		pPeer->TradeInfo(0);

		// 通知交易的对方
		MGCancelTrade msg;
		pPeer->SendPkg<MGCancelTrade>(&msg);
	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnLockTrade( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMLockTrade);

	CPlayer *pSelf = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID);
	if (NULL == pSelf)
		return false;

	CTradeInfo *pTradeInfo = pSelf->TradeInfo();
	if(NULL == pTradeInfo)
		return false;

	// 根据交易信息获取对方信息
	CPlayer *pPeer = NULL;
	CTradeInfo::eTradePlayerIndex tIndex;
	PlayerID invitePlayerID = pTradeInfo->InvitePlayerID();
	PlayerID targetPlayerID = pTradeInfo->TargetPlayerID();
	if(invitePlayerID == pMsg->selfPlayerID)
	{
		tIndex = CTradeInfo::INVITE_PLAYER;
		if(targetPlayerID.wGID == pMsg->selfPlayerID.wGID)
			pPeer = pOwner->FindPlayer(targetPlayerID.dwPID);
		else
		{
			CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( targetPlayerID.wGID );
			if(NULL != pPeerGateSrv)
				pPeer = pPeerGateSrv->FindPlayer( targetPlayerID.dwPID);
		}
	}
	else
	{
		tIndex = CTradeInfo::TARGET_PLAYER;
		if(invitePlayerID.wGID == pMsg->selfPlayerID.wGID)
			pPeer = pOwner->FindPlayer(invitePlayerID.dwPID);
		else
		{
			CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( invitePlayerID.wGID );
			if(NULL != pPeerGateSrv)
				pPeer = pPeerGateSrv->FindPlayer( invitePlayerID.dwPID);
		}
	}

	if(NULL == pPeer)
		return false;

	// 设置交易状态
	CTradeInfo::TradeData::eState currentState = pTradeInfo->State(tIndex);
	if(pMsg->bLock && (CTradeInfo::TradeData::TRADE_INIT == currentState))
	{
		// 锁定
		pTradeInfo->State(tIndex, CTradeInfo::TradeData::TRADE_LOCK);
	}
	else if(!pMsg->bLock && (CTradeInfo::TradeData::TRADE_LOCK == currentState))
	{
		// 取消锁定
		pTradeInfo->State(tIndex, CTradeInfo::TradeData::TRADE_INIT);
	}
	else
	{	
		// 忽略
		return true;
	}

	// 通知双方
	MGLockTrade msg;
	msg.lOperatePlayerID = pMsg->selfPlayerID;
	msg.bLock = pMsg->bLock;

	pSelf->SendPkg<MGLockTrade>(&msg);
	pPeer->SendPkg<MGLockTrade>(&msg);

	return true;
    TRY_END;
	return false;
}

bool CDealG_MPkg::OnConfirmTrade( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMConfirmTrade);

	CPlayer* pSelf = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID);
	if ( NULL == pSelf )
	{
		D_WARNING( "CDealG_MPkg::OnConfirmTrade, NULL == pSelf\n" );
		return false;
	}

	CTradeInfo* pTradeInfo = pSelf->TradeInfo();
	if ( NULL == pTradeInfo )
	{
		D_WARNING( "CDealG_MPkg::OnConfirmTrade, NULL == pTradeInfo\n" );
		return false;
	}

	// 根据交易信息获取对方信息
	CPlayer *pPeer = NULL;
	CTradeInfo::eTradePlayerIndex tIndex, tPeerIndex;
	
	PlayerID invitePlayerID = pTradeInfo->InvitePlayerID();
	PlayerID targetPlayerID = pTradeInfo->TargetPlayerID();
	if(invitePlayerID == pMsg->selfPlayerID)
	{
		tIndex = CTradeInfo::INVITE_PLAYER;
		tPeerIndex = CTradeInfo::TARGET_PLAYER;

		if(targetPlayerID.wGID == pMsg->selfPlayerID.wGID)
		{
			pPeer = pOwner->FindPlayer(targetPlayerID.dwPID);
		} else {
			CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( targetPlayerID.wGID );
			if(NULL != pPeerGateSrv)
			{
				pPeer = pPeerGateSrv->FindPlayer( targetPlayerID.dwPID);
			}
		}
	} else {
		tIndex = CTradeInfo::TARGET_PLAYER;
		tPeerIndex = CTradeInfo::INVITE_PLAYER;

		//pPeer = pOwner->FindPlayer(invitePlayerID.dwPID);
		if(invitePlayerID.wGID == pMsg->selfPlayerID.wGID)
		{
			pPeer = pOwner->FindPlayer(invitePlayerID.dwPID);
		} else {
			CGateSrv *pPeerGateSrv = CManG_MProc::FindProc( invitePlayerID.wGID );
			if (NULL != pPeerGateSrv)
			{
				pPeer = pPeerGateSrv->FindPlayer( invitePlayerID.dwPID);
			}
		}
	}

	if ( NULL == pPeer )
	{
		D_WARNING( "CDealG_MPkg::OnConfirmTrade, NULL == pPeer\n" );
		return false;
	}

	// 确认操作前置状态必须为锁定
	if ( CTradeInfo::TradeData::TRADE_LOCK != pTradeInfo->State(tIndex) )
	{
		D_WARNING( "CDealG_MPkg::OnConfirmTrade, CTradeInfo::TradeData::TRADE_LOCK != pTradeInfo->State(tIndex)\n" );
		return false;
	}

	// 回复
	MGConfirmTrade msg;
	msg.lOperatePlayerID = pMsg->selfPlayerID;

	if(CTradeInfo::TradeData::TRADE_CONFIRM != pTradeInfo->State(tPeerIndex))//对方未确认
	{
		pTradeInfo->State(tIndex, CTradeInfo::TradeData::TRADE_CONFIRM);

		msg.tResult = MGConfirmTrade::PEER_NOT_CONFIRM;
		pSelf->SendPkg<MGConfirmTrade>(&msg);

		msg.tResult = MGConfirmTrade::PEER_CONFIRM;
		pPeer->SendPkg<MGConfirmTrade>(&msg);

		return true;
	} else {
		if( pSelf->IsEvil() == pPeer->IsEvil() )
		{
			pTradeInfo->State(tIndex, CTradeInfo::TradeData::TRADE_CONFIRM);
		} else {
			bool isEvil = pSelf->IsEvil();
			msg.tResult = isEvil ? MGConfirmTrade::SELF_IS_EVIL : MGConfirmTrade::PEER_IS_EVIL;	
			pSelf->SendPkg<MGConfirmTrade>(&msg);

			msg.tResult = isEvil ? MGConfirmTrade::PEER_IS_EVIL : MGConfirmTrade::SELF_IS_EVIL;
			pPeer->SendPkg<MGConfirmTrade>(&msg);
		}
	}

	if ( pTradeInfo->BothConfirmTrade() )
	{
		//1.物品
		unsigned short usTradeCount, usPeerTradeCount;
		int* pTradeItems = NULL;
		int* pPeerTradeItems = NULL;		
		pTradeInfo->TradeItemInfo(tIndex, pTradeItems, usTradeCount);
		pTradeInfo->TradeItemInfo(tPeerIndex, pPeerTradeItems, usPeerTradeCount);

		//2.金钱
		unsigned int uiMoney, uiPeerMoney;
		uiMoney = pTradeInfo->Money(tIndex);
		uiPeerMoney = pTradeInfo->Money(tPeerIndex);

		//3交换
		int iTradeResult = pSelf->OnTradeSwapItem(pPeer, pTradeItems, pPeerTradeItems, uiMoney, uiPeerMoney);
		if ( -1 == iTradeResult )
		{
			msg.tResult = MGConfirmTrade::OTHER_ERROR;
			pSelf->SendPkg<MGConfirmTrade>(&msg);
			pPeer->SendPkg<MGConfirmTrade>(&msg);
		} else if ((int)MGConfirmTrade::SELF_PKG_SPACE_NOT_ENOUGH == iTradeResult) {//空间不足
			msg.tResult = MGConfirmTrade::SELF_PKG_SPACE_NOT_ENOUGH;
			pSelf->SendPkg<MGConfirmTrade>(&msg);

			msg.tResult = MGConfirmTrade::PEER_PKG_SPACE_NOT_ENOUGH;
			pPeer->SendPkg<MGConfirmTrade>(&msg);
		} else if((int)MGConfirmTrade::PEER_PKG_SPACE_NOT_ENOUGH == iTradeResult) {//对方空间不足
			msg.tResult = MGConfirmTrade::PEER_PKG_SPACE_NOT_ENOUGH;
			pSelf->SendPkg<MGConfirmTrade>(&msg);

			msg.tResult = MGConfirmTrade::SELF_PKG_SPACE_NOT_ENOUGH;
			pPeer->SendPkg<MGConfirmTrade>(&msg);
		} else if((int)MGConfirmTrade::SELF_MONEY_OVERFLOW == iTradeResult) {//金钱越界
			msg.tResult = MGConfirmTrade::SELF_MONEY_OVERFLOW;
			pSelf->SendPkg<MGConfirmTrade>(&msg);

			msg.tResult = MGConfirmTrade::PEER_MONEY_OVERFLOW;
			pPeer->SendPkg<MGConfirmTrade>(&msg);
		} else if((int)MGConfirmTrade::PEER_MONEY_OVERFLOW == iTradeResult) {//对方金钱越界
			msg.tResult = MGConfirmTrade::PEER_MONEY_OVERFLOW;
			pSelf->SendPkg<MGConfirmTrade>(&msg);

			msg.tResult = MGConfirmTrade::SELF_MONEY_OVERFLOW;
			pPeer->SendPkg<MGConfirmTrade>(&msg);
		} else {	
			msg.tResult = MGConfirmTrade::SUCCESS;
			pSelf->SendPkg<MGConfirmTrade>(&msg);
			pPeer->SendPkg<MGConfirmTrade>(&msg);
		}
	}//if(pTradeInfo->BothConfirmTrade())

	//重置交易信息
	PlayerID tempID;
	tempID.dwPID = 0;
	tempID.wGID = 0;

	pSelf->DelTradeInfo();
	pSelf->RecvFromPlayer(tempID);
	pSelf->TradeInfo(0);
	pPeer->RecvFromPlayer(tempID);
	pPeer->TradeInfo(0);
	
	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRepairWearByItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRepairWearByItem);

	CPlayer *pSelf = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID);
	if (NULL == pSelf)
		return false;

	MGRepairWearByItem resp;
	StructMemSet( resp, 0x00, sizeof(resp));
	resp.itemRepairman = pMsg->itemRepairman;
	resp.repairTarget  = pMsg->repairTarget;

	//修改BUG:原来为ride状态不能维修，现在为可以维修.BUGID:196142
	if ( /*(pSelf->GetSelfStateManager().IsOnRideState()) || */(pSelf->IsFaint()) )//骑乘状态不能使用维修道具,通知客户端使用道具失败
	{
		resp.tResult = MGRepairWearByItem::OTHER_ERROR;
		pSelf->SendPkg<MGRepairWearByItem>(&resp);
		return false;
	}

	//获取维修道具的位置，以及ItemInfo信息
	ItemInfo_i *pRepairmanInfo = NULL; int usRepairmanIndex = 0;        bool bItemInPkg = false;
	pRepairmanInfo = pSelf->GetItemInfoByItemUID(  pMsg->itemRepairman, usRepairmanIndex ,bItemInPkg );

	//获取被维修道具的位置　 以及ItemInfo信息
	ItemInfo_i *pRepairTargetInfo = NULL; int usRepairTargetIndex = 0; 	bool isEquipItem = false;
	pRepairTargetInfo = pSelf->GetItemInfoByItemUID( pMsg->repairTarget,usRepairTargetIndex, isEquipItem );

	if((NULL == pRepairmanInfo) || (NULL == pRepairTargetInfo))// 没有找到两件物品
	{
		//resp.tResult = MGRepairWearByItem::OTHER_ERROR;//修改BUG:196872 因为client的宠物发来了连续的维修道具的消息。一般情况下，不可能出现这个问题。除非逻辑错误 或者 client作弊 
		return false;
	}
	else
	{
		CBaseItem* pBeRepairItem  = pSelf->GetItemDelegate().GetItemByItemUID( pRepairTargetInfo->uiID );//被维修道具
		CBaseItem *pRepairmanBase = pSelf->GetItemDelegate().GetItemByItemUID( pRepairmanInfo->uiID );//维修道具
		if(NULL == pRepairmanBase || NULL == pBeRepairItem )
		{
			resp.tResult = MGRepairWearByItem::OTHER_ERROR;
		}
		else
		{
			//是否具有修复功能(功能性道具且具有修复功能)
			CFunctionItem *pRepairmanFunctionProp = dynamic_cast<CFunctionItem *>(pRepairmanBase);
			if(NULL == pRepairmanFunctionProp)//非功能性道具
				resp.tResult = MGRepairWearByItem::OTHER_ERROR;
			else
			{
				const FunctionProperty *pRepairmanFuncProperty = pRepairmanFunctionProp->GetFunctionProperty();
				if(NULL == pRepairmanFuncProperty)
					resp.tResult = MGRepairWearByItem::OTHER_ERROR;
				else
				{	
					if((pRepairmanFuncProperty->Type != 9) || (pRepairmanInfo->ucCount <= 0))//无修复功能
						resp.tResult = MGRepairWearByItem::REPAIRMAN_NO_REPAIR_FUNCTION;
					else
					{
						//是否可以修复(是否可以修复，是否有足够的修复费用)
						CItemPublicProp *pTargetPublicProp = pBeRepairItem->GetItemPublicProp();
						if(NULL == pTargetPublicProp)
							resp.tResult = MGRepairWearByItem::OTHER_ERROR;
						else
						{
							if(pTargetPublicProp->m_repairMode != 0 /*|| ((pTargetPublicProp->m_itemTypeID != E_TYPE_WEAPON) && (pTargetPublicProp->m_itemTypeID != E_TYPE_EQUIP))*/)
							{
								resp.tResult = MGRepairWearByItem::TARGET_DO_NOT_REPAIR;
							}
							else
							{
								unsigned int uiRepairMoney = CRepairWairRuleManagerSingle::instance()->CalcRepairWearMoney(pTargetPublicProp->m_maxWear - pRepairTargetInfo->usWearPoint,pTargetPublicProp->m_itemLevel, pTargetPublicProp->m_itemMassLevel, pRepairTargetInfo->ucLevelUp);
								if(uiRepairMoney == 0)
								{
									resp.tResult = MGRepairWearByItem::TARGET_DO_NOT_REPAIR;
								} else {
									bool checkmoneynum = false;
									if ( pSelf->IsUseGoldMoney() )
									{
										//使用金币
										checkmoneynum = (pSelf->GetGoldMoney() < uiRepairMoney);
									} else {
										//使用银币
										checkmoneynum = (pSelf->GetSilverMoney() < uiRepairMoney);
									}
									if ( checkmoneynum ) 
									{
										resp.tResult = MGRepairWearByItem::NO_ENOUGH_REPAIR_MONEY;
									} else {
										//修复(有修复功能的道具使用次数/*耐久度*/减少，目标道具耐久度恢复到上限, 扣除修复费用)
										if(pRepairmanInfo->ucCount == 1)
										{
											pSelf->OnDropItem(pRepairmanInfo->uiID);
										}
										else
										{
											pRepairmanInfo->ucCount--;
											pSelf->NoticePkgItemChange(usRepairmanIndex);
										}

										unsigned int oldwearpoint = pRepairTargetInfo->usWearPoint;
										pRepairTargetInfo->usWearPoint = pTargetPublicProp->m_maxWear;
										if ( isEquipItem )//如果是在玩家的装备栏上
										{
											//如果耐久度下降到0了,证明原来的装备属性已经扣掉了
											if ( oldwearpoint == 0)
											{
												//这时候应该在把该装备的属性加上去
												//pBeRepairItem->AddItemPropToPlayer( pSelf, *pRepairTargetInfo );
												//pSelf->RepaireEquipItemAddProp( *pRepairTargetInfo,usRepairTargetIndex );
												ItemAttributeHelper::OnRepairWearPointEmtpyItem( pSelf, *pRepairTargetInfo );
											}
											pSelf->NoticeEquipItemChange( usRepairTargetIndex );
										}

										resp.newWear = pRepairTargetInfo->usWearPoint;
										resp.tResult = MGRepairWearByItem::OK;
										if ( pSelf->IsUseGoldMoney() )
										{
											//使用金币
											pSelf->SubGoldMoney( uiRepairMoney );
											resp.uiRepairGoldMoney = uiRepairMoney;
										} else {
											//使用银币
											pSelf->SubSilverMoney( uiRepairMoney );
											resp.uiRepairSilverMoney = uiRepairMoney;
										}										
										
										CLogManager::DoRepairItem(pSelf, 0, uiRepairMoney, pRepairTargetInfo->uiID, pRepairTargetInfo->usItemTypeID);//记日至
									}
								}
							}
						}
					}
				}
			}	
		}
	}
	
	pSelf->SendPkg<MGRepairWearByItem>(&resp);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnTimedWearIsZero( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealG_MPkg::OnTimedWearIsZero, NULL == pOwner\n" );
		return false;
	}
	if ( NULL == pPkg )
	{
		D_ERROR( "CDealG_MPkg::OnTimedWearIsZero, NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMTimedWearIsZero);

	CPlayer *pSelf = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID);
	if ( NULL == pSelf )
	{
		D_ERROR( "CDealG_MPkg::OnTimedWearIsZero, NULL == pSelf\n" );
		return false;
	}

	//查找相关的物品
	FullPlayerInfo* pFullPlayerInfo = pSelf->GetFullPlayerInfo();
	if ( NULL == pFullPlayerInfo )
	{
		D_ERROR( "CDealG_MPkg::OnTimedWearIsZero, NULL == pFullPlayerInfo\n" );
		return false;
	}

	for(int i=0; i<ARRAY_SIZE(pFullPlayerInfo->pkgInfo.pkgs); ++i)
	{
		if(pMsg->itemID == pFullPlayerInfo->pkgInfo.pkgs[i].uiID)
		{	
			CItemPublicProp *pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(pFullPlayerInfo->pkgInfo.pkgs[i].usItemTypeID);
			if((NULL != pPublicProp) && (ACE_BIT_ENABLED(pPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY)))
			{
				time_t now;
				ACE_OS::time(&now);
				
				if ( now > (time_t)(pFullPlayerInfo->pkgInfo.pkgs[i].usWearPoint) )
				{
					if(ACE_BIT_ENABLED(pPublicProp->m_wearOMode, CItemPublicProp::DESTROY))//耐久度为零时要求销毁,则销毁
					{
						pSelf->OnDropItem(pMsg->itemID);
						return true;
					}
				}
			}
		}
	}

	for ( int i=0; i<ARRAY_SIZE(pFullPlayerInfo->equipInfo.equips); ++i )
	{
		if(pMsg->itemID == pFullPlayerInfo->equipInfo.equips[i].uiID)
		{	
			CItemPublicProp *pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(pFullPlayerInfo->equipInfo.equips[i].usItemTypeID);
			if((NULL != pPublicProp) && (ACE_BIT_ENABLED(pPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY)))
			{
				time_t now;
				ACE_OS::time(&now);

				if(now > (time_t)pFullPlayerInfo->equipInfo.equips[i].usWearPoint)
				{
					if(ACE_BIT_ENABLED(pPublicProp->m_wearOMode, CItemPublicProp::DESTROY))//耐久度为零时要求销毁,则销毁
					{
						pSelf->OnDropItem(pMsg->itemID);
						return true;
					}
				}
			}
		}
	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnAddSpSkillReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	ACE_UNUSED_ARG( pOwner ); ACE_UNUSED_ARG( pPkg ); ACE_UNUSED_ARG( wPkgLen );

	return false;  //这个协议废除！
	/*PKG_CONVERT( GMAddSpSkillReq );
	
	CPlayer* pReqPlayer = pOwner->FindPlayer( pMsg->reqPlayerID.dwPID );
	if( NULL == pReqPlayer )
	{
		D_ERROR("请求的玩家找不到\n");
		return false;
	}
	
	MGAddSpSkillResult addResult;
	addResult.addSpSkillID = pMsg->spSkillID;
	CPlayerSkillProp* pSkillProp = CManPlayerSkillProp::FindObj( pMsg->spSkillID );
	if( NULL == pSkillProp )
	{
		addResult.bSuccess = false;
		pReqPlayer->SendPkg< MGAddSpSkillResult>( &addResult );
		return false;
	}

	if( !pSkillProp->IsSpSkill() )
	{
		addResult.bSuccess = false;
	}

	if( !pSkillProp->IsClassSkill( pReqPlayer->GetClass() ) )
	{
		addResult.bSuccess = false;
	}

	if( pReqPlayer->GetSpLevel() < pSkillProp->m_spLevel )
	{
		addResult.bSuccess = false;
	}

	
	
	if( !pReqPlayer->AddSkill( pSkillProp ) )
	{
		addResult.bSuccess = false;
	}else
	{
		addResult.bSuccess = true;
	}

	pReqPlayer->SendPkg<MGAddSpSkillResult>( &addResult );

	return true;*/
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnUpgradeSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	
	PKG_CONVERT( GMUpgradeSkill );
	
	CPlayer* pUpdatePlayer = pOwner->FindPlayer( pMsg->reqPlayerID.dwPID );
	if( !pUpdatePlayer )
		return false;
	
	return pUpdatePlayer->UpgradeBattleSkill( pMsg->skillID );
	
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnDeleteTask(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMDeleteTask );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if( !pPlayer )
		return false;

	#ifdef OPEN_PUNISHMENT
		else if( pMsg->taskType == NTS_PUNISH_TASK )
		{
			pPlayer->GiveUpPunishTask();
		}
	#endif //OPEN_PUNISHMENT
	else{
		pPlayer->DeleteTask( pMsg->TaskID );
		CLogManager::DoTaskChanage(pPlayer, pMsg->TaskID, (BYTE)LOG_SVC_NS::QCT_GIVEUP);//记日至
	}

	return true;
	TRY_END;
	return false;
}

//查询商店物品数量
bool CDealG_MPkg::OnQueryShop( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMQueryShop );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lQueryPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		return false;
	}

	CNpcShopManager::ShowNpcShop( pPlayer, pMsg->shopID, pMsg->shopPageID );

	return true;
	TRY_END;
	return false;
}

//请求购买商店物品
bool CDealG_MPkg::OnShopBuyReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMShopBuyReq );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lBuyerID.dwPID );
	if( NULL == pPlayer )
	{
		return false;
	}

	CNpcShopManager::NpcShopBuy( pPlayer, *pMsg );	

	return true;
	TRY_END;
	return false;
}

//请求售出包裹物品
bool CDealG_MPkg::OnItemSoldReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMItemSoldReq );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->sellerID.dwPID );
	if( NULL == pPlayer )
	{
		return false;
	}

	pPlayer->SoldPkgItem( *pMsg );

	return true;
	TRY_END;
	return false;
}

//请求添加活点, //GMAddActivePtReq
bool CDealG_MPkg::OnAddAptReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMAddActivePtReq );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->reqPlayer.dwPID );
	if( NULL == pPlayer )
	{
		return false;
	}

	pPlayer->OnAddAPT( pMsg->reqID, pMsg->nickName );

	return true;
	TRY_END;
	return false;
}

//查询恶魔时间
bool CDealG_MPkg::OnQueryEvilTime(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMQueryEvilTime );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if( NULL == pPlayer )
	{
		return false;
	}

	//恶魔值小于<0代表恶魔状态
	if ( pPlayer->GetGoodEvilMode() != EVIL_MODE  )
		return false;

	//返回给客户端
	MGEvilTimeRst evilTimeMsg;
	evilTimeMsg.EvilTime = pPlayer->GetEvilTime();
	pPlayer->SendPkg<MGEvilTimeRst>( &evilTimeMsg );

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnUseDamageConsumeItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMUseDamageItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->attackPlayer.dwPID );
	if ( NULL == pPlayer )
	{
		return false;
	}

	UseDamageConsumeRule::Execute( pPlayer, (void *)pPkg );

	return true;
	TRY_END;

	return false;
}


bool CDealG_MPkg::OnQueryRepairItemCost(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMQueryRepairItemCost );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->selfPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		return false;
	}

	pPlayer->QueryRepairAllItemCost();

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnSwapEquipItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSwapEquipItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->swapPlayer.dwPID );
	if ( NULL == pPlayer )
	{
		return false;
	}

	pPlayer->SwapEquipItem( pMsg->from, pMsg->to );

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnQueryItemPkgPage(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMQueryItemPkgPage );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->queryPlayer.dwPID );
	if ( NULL == pPlayer )
	{
		return false;
	}

	pPlayer->QueryItemPkgPage( (unsigned char)pMsg->pageIndex );

	return true;
	TRY_END;
	return false;
}



#ifdef OPEN_PUNISHMENT 
bool CDealG_MPkg::OnPunishmentStart( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	
	PKG_CONVERT( GMPunishmentStart );

	CPunishmentManager::StartPunishment( pMsg );
	D_DEBUG("收到天谴开始消息\n");

	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnPunishmentEnd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	
	PKG_CONVERT( GMPunishmentEnd );

	CPunishmentManager::EndPunishment();
	D_DEBUG("收到天谴结束消息\n");

	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnPunishPlayerInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	
	PKG_CONVERT( GMPunishPlayerInfo );

	CPunishmentManager::UpdatePunishPlayerinfo( pMsg->punishPlayer );
	D_DEBUG("收到天谴人员更新消息\n");

	return true;
	TRY_END;
	return false;
}

#endif  /*OPEN_PUNISHMENT*/




bool CDealG_MPkg::OnNotifyWarTaskState( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMNotifyWarTaskState );
	//？？？
		 
	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnAttackWarBegin( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMAttackWarBegin );
	CWarTaskManager::OnAttackCityBegin( pMsg->actID );
	 
	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnAttackWarEnd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMAttackWarEnd );
	CWarTaskManager::OnAttackCityEnd( pMsg->warTaskId );
	 
	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnChangeBattleMode( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeBattleMode );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->reqID.dwPID );
	if( NULL == pPlayer )
	{
		return false;
	}

	bool bSuccess;
	if( !pPlayer->IsAlive() )
	{
		bSuccess = false;
	}else if( pPlayer->IsPlayerForgingItem() ){
		bSuccess = false;
	}else{
		bSuccess = true;
	}

	if( bSuccess )
	{
		bSuccess = pPlayer->ChangeBattleMode( pMsg->mode );
	}

	MGChangeBattleMode srvmsg;
	srvmsg.bSuccess = bSuccess;
	srvmsg.lNotiPlayerID = pPlayer->GetFullID();
	srvmsg.mode = pMsg->mode;
	
	pPlayer->SendPkg<MGChangeBattleMode>( &srvmsg );

	return true;
	TRY_END;
	return false;	
}

//使用UseSkill2后废弃，原ID号由GMMouseTipReq使用，bool CDealG_MPkg::OnUsePartionSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	if ( NULL == pOwner )
//	{
//		return false;
//	}
//	if ( NULL == pPkg )
//	{
//		return false;
//	}
//
//	TRY_BEGIN;	
//
//	D_ERROR( "CDealG_MPkg::OnUsePartionSkill,收到废弃的旧分割技能消息(被UseSkill2代替)\n" );
//
//	/*
//    PKG_CONVERT( GMUsePartionSkill );
//
//	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->attackPlayerID.dwPID );
//	if ( NULL == pPlayer )
//	{
//		D_ERROR("CDealG_MPkg::OnAoeAttack 找不到攻击玩家%d\n",  pMsg->attackPlayerID.dwPID  );
//		return false;
//	}
//
//	CMuxMap* pMap = pPlayer->GetCurMap();
//	if( NULL == pMap )
//	{
//		D_ERROR("CDealG_MPkg::OnAoeAttack 找不到玩家%s所在地图\n", pPlayer->GetNickName() );
//		return false;
//	}
//
//	BattleArgs args;
//	args.skillID = pMsg->partionSkillId;
//	args.sequenceID = pMsg->sequenceID;
//	args.targetID = pMsg->targetID;
//	args.battleType = ( pMsg->targetID.wGID == MONSTER_GID ) ? PARTION_SKILL_PVC : PARTION_SKILL_PVP;
//	args.mainAoeTargetID = pMsg->targetID;
//	args.movePosX = pMsg->movePosX;
//	args.movePosY = pMsg->movetPosY;
//	pMap->HandlePlayerBattle( pPlayer, &args );
//	*/
//
//	return true;
//	TRY_END;
//	return false;
//}
//Register( G_M_MOUSETIP_REQ, OnGMMouseTipReq );//鼠标停留信息查询(HP|MP)
bool CDealG_MPkg::OnGMMouseTipReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;	

    PKG_CONVERT( GMMouseTipReq );

	CPlayer* pQryPlayer = pOwner->FindPlayer( pMsg->qryPlayerID.dwPID );
	if ( NULL == pQryPlayer )
	{
		D_ERROR("CDealG_MPkg::OnGMMouseTipReq 找不到查询玩家%d\n", pMsg->qryPlayerID.dwPID );
		return false;
	}

	CPlayer* pTgtPlayer = pOwner->FindPlayer( pMsg->tgtPlayerID.dwPID );
	if ( NULL == pTgtPlayer )
	{
		D_ERROR("CDealG_MPkg::OnGMMouseTipReq 找不到目标玩家%d\n", pMsg->tgtPlayerID.dwPID );
		return false;
	}

	if ( pQryPlayer->GetMapID() != pTgtPlayer->GetMapID() )
	{
		D_ERROR("CDealG_MPkg::OnGMMouseTipReq 查询玩家%s与目标玩家%s不在同一地图上\n", pQryPlayer->GetAccount(), pTgtPlayer->GetAccount() );
		return false;
	}

	MGMouseTip mouseTip;
	mouseTip.gcMouseTip.tgtPlayerID = pMsg->tgtPlayerID;
	mouseTip.gcMouseTip.curHP = pTgtPlayer->GetCurrentHP();
	mouseTip.gcMouseTip.maxHP = pTgtPlayer->GetMaxHP();
	mouseTip.gcMouseTip.curMP = pTgtPlayer->GetCurrentMP();
	mouseTip.gcMouseTip.maxMP = pTgtPlayer->GetMaxMP();

	pQryPlayer->SendPkg<MGMouseTip>( &mouseTip );

	return true;
	TRY_END;
	return false;
}


#ifdef OPEN_PET
bool CDealG_MPkg::OnSummonPet( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	
	PKG_CONVERT( GMSummonPet );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->ownerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealG_MPkg::OnSummonPet 找不到PLAYER的ID %d\n", pMsg->ownerID.dwPID );
		return false;
	}

	pPlayer->SummonPet();
	
	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnClosePet( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	
	PKG_CONVERT( GMClosePet );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->ownerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealG_MPkg::OnClosePet 找不到PLAYER的ID %d\n", pMsg->ownerID.dwPID );
		return false;
	}

	pPlayer->ClosePet();
	
	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnChangePetName( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	
	PKG_CONVERT( GMChangePetName );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->ownerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealG_MPkg::OnChangePetName 找不到PLAYER的ID %d\n", pMsg->ownerID.dwPID );
		return false;
	}

	pPlayer->SetPetName( pMsg->petName );
	
	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnChangePetImage( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	
	PKG_CONVERT( GMChangePetImage );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->ownerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealG_MPkg::OnChangePetImage 找不到PLAYER的ID %d\n", pMsg->ownerID.dwPID );
		return false;
	}

	if ( ! pPlayer->SetPetImagePos( pMsg->imageID ) )
	{
		pPlayer->SendSystemChat("宠物不可使用此外形\n");
	}
	
	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnChangePetSkillState( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;
	
	PKG_CONVERT( GMChangePetSkillState );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->ownerID.dwPID );
	if( NULL == pPlayer )
	{
		D_ERROR("CDealG_MPkg::OnChangePetSkillState 找不到PLAYER的ID %d\n", pMsg->ownerID.dwPID );
		return false;
	}

	pPlayer->SetPetSkillState( pMsg->petSkillState );

	return true;
	TRY_END;
	return false;
}
#endif //OPEN_PET

bool CDealG_MPkg::OnRepairWearByNpc(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, NULL == pOwner\n" );
		return false;
	}
	if ( NULL == pPkg )
	{
		D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRepairWearByNPC );

	CPlayer* pSelf = pOwner->FindPlayer( pMsg->selfId.dwPID );
	if ( NULL == pSelf )
	{
		D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, NULL == pSelf\n" );
		return false;
	}

	FullPlayerInfo* pFullPlayerInfo = pSelf->GetFullPlayerInfo();
	if ( NULL == pFullPlayerInfo )
	{
		D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, NULL == pFullPlayerInfo\n" );
		return false;
	}
	unsigned int leftMoney = 0;
	if ( pSelf->IsUseGoldMoney() )
	{
		//使用金币
		leftMoney = pSelf->GetGoldMoney();
	} else {
		//使用银币
		leftMoney = pSelf->GetSilverMoney();//双币种判断
	}
	unsigned int newWear = 0;

	MGRepairWearByNPC resp;//回复
	StructMemSet( resp, 0, sizeof(resp) );
	resp.repairAll = pMsg->repairAll;
	resp.repairTarget = pMsg->itemID;

	if(!pMsg->repairAll)//修理单个
	{
		bool        isEquipItem = false;//维修的道具是否是装备在身上的物品
		int         repaireItemIndex = -1;
		ItemInfo_i  *pRepairedItem = pSelf->GetItemInfoByItemUID( pMsg->itemID ,repaireItemIndex, isEquipItem );

		if(pRepairedItem == NULL)//没有找到
		{
			resp.tResult = MGRepairWearByNPC::OTHER_ERROR;
		} else {
			resp.tResult = (MGRepairWearByNPC::eRepairResult)(pSelf->CheckItemCanRepair(pRepairedItem, leftMoney, newWear));
			if(resp.tResult == MGRepairWearByNPC::OK)
			{
				resp.newWear = newWear;
				if ( pSelf->IsUseGoldMoney() )
				{
					//使用金币
					resp.repairGoldMoney = pSelf->GetGoldMoney() - leftMoney; //双币种判断
				} else {
					//使用银币
					resp.repairSilverMoney = pSelf->GetSilverMoney() - leftMoney; //双币种判断
				}

				//如果是装备在身上的物品
				if ( isEquipItem )
				{
					int oldWearPoint  = pRepairedItem->usWearPoint;//记录原来的耐久度，看是否维修后是否应该改变道具状态
					pRepairedItem->usWearPoint = newWear;
					//得通知耐久度改变了
					pSelf->NoticeEquipItemChange( repaireItemIndex );
					//如果耐久度下降到了0,证明原来的装备属性已经扣掉了
					if ( oldWearPoint == 0)
					{
						CBaseItem* pBeRepairItem   = pSelf->GetItemDelegate().GetItemByItemUID( pRepairedItem->uiID );
						if ( pBeRepairItem )
						{
							//这时候应该在把该装备的属性加上去

							ItemAttributeHelper::OnRepairWearPointEmtpyItem( pSelf, *pRepairedItem );
						} else {
							D_ERROR("维修好道具后，添加道具属性给玩家%s时，找不到玩家的道具%d\n",pSelf->GetNickName(), pRepairedItem->uiID );
						}
					}
					
				} else {//如果是包裹中的道具
					pRepairedItem->usWearPoint = newWear;
				}
				//更新金钱
				if ( pSelf->IsUseGoldMoney() )
				{
					//使用金币
					pSelf->SubGoldMoney( resp.repairGoldMoney );//双币种判断
				} else {
					//使用银币
					pSelf->SubSilverMoney( resp.repairSilverMoney );//双币种判断
				}
				
				CLogManager::DoRepairItem(pSelf, 0, resp.repairSilverMoney, pRepairedItem->uiID, pRepairedItem->usItemTypeID);//记日至
			}
		}

		pSelf->SendPkg<MGRepairWearByNPC>(&resp);
	} else {//修理所有物品
		NeedRepairedItems needRepairedItems[EQUIP_SIZE + PACKAGE_SIZE];	
		unsigned int   needRepairedEquipItemIndex[EQUIP_SIZE];
		unsigned short repairedCount = 0;
		unsigned short repairedEquipItemCount = 0;//有多少维修的装备物品
		MGRepairWearByNPC::eRepairResult result = MGRepairWearByNPC::TARGET_DO_NOT_REPAIR;
		unsigned int logMoney = 0;//left的备份,计算单个物品修复费用是用到,用作记日至!!!!!
		
		for(unsigned int i = 0; i<ARRAY_SIZE(pFullPlayerInfo->equipInfo.equips); i++)
		{
			if ( pFullPlayerInfo->equipInfo.equips[i].uiID > 0)
			{
				logMoney = leftMoney;//备份,用作记日至
				result = (MGRepairWearByNPC::eRepairResult) ( pSelf->CheckItemCanRepair(&(pFullPlayerInfo->equipInfo.equips[i]), leftMoney, newWear) );
				if ( (result == MGRepairWearByNPC::OTHER_ERROR) 
					|| (result == MGRepairWearByNPC::NO_ENOUGH_REPAIR_MONEY)
					)
				{
					break;
				} else if ( result == MGRepairWearByNPC::OK ) {
					if ( ( repairedCount >= ARRAY_SIZE(needRepairedItems) )
						|| ( repairedCount >= ARRAY_SIZE(needRepairedEquipItemIndex) )
						)
					{
						D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, repairedCount(%d)越界\n", repairedCount );
						break;
					}
					needRepairedItems[repairedCount].pItem = &(pFullPlayerInfo->equipInfo.equips[i]);
					needRepairedItems[repairedCount].newWear = newWear;
					needRepairedItems[repairedCount].costMoney = logMoney - leftMoney;
					needRepairedEquipItemIndex[repairedCount] = i;

					repairedCount++;
					repairedEquipItemCount++;
				}
			}
		}
		
		if ( (result != MGRepairWearByNPC::OTHER_ERROR) 
			&& (result != MGRepairWearByNPC::NO_ENOUGH_REPAIR_MONEY)
			)
		{
			for (unsigned int i=0; i<ARRAY_SIZE(pFullPlayerInfo->pkgInfo.pkgs); ++i)
			{
				if ( pFullPlayerInfo->pkgInfo.pkgs[i].uiID > 0 )
				{
					logMoney = leftMoney;//备份,用作记日至
					result = (MGRepairWearByNPC::eRepairResult)(pSelf->CheckItemCanRepair(&(pFullPlayerInfo->pkgInfo.pkgs[i]), leftMoney, newWear));
					if ( (result == MGRepairWearByNPC::OTHER_ERROR) 
						|| (result == MGRepairWearByNPC::NO_ENOUGH_REPAIR_MONEY)
						)
					{
						break;
					} else if ( result == MGRepairWearByNPC::OK ) {
						if ( repairedCount >= ARRAY_SIZE(needRepairedItems) )
						{
							D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, repairedCount(%d)越界\n", repairedCount );
							break;
						}
						needRepairedItems[repairedCount].pItem = &(pFullPlayerInfo->pkgInfo.pkgs[i]);
						needRepairedItems[repairedCount].newWear = newWear;
						needRepairedItems[repairedCount].costMoney = logMoney - leftMoney;

						repairedCount++;
					}
				}
			}
		}

		if((result == MGRepairWearByNPC::OTHER_ERROR) || (result == MGRepairWearByNPC::NO_ENOUGH_REPAIR_MONEY))
		{
			resp.tResult = result;
		} else {
			if(repairedCount == 0)
			{
				resp.tResult = MGRepairWearByNPC::TARGET_DO_NOT_REPAIR;
			} else {
				resp.tResult = MGRepairWearByNPC::OK;
				if ( pSelf->IsUseGoldMoney() )
				{
					//使用金币
					resp.repairGoldMoney = pSelf->GetGoldMoney() - leftMoney;//双币种判断
				} else {
					//使用银币
					resp.repairSilverMoney = pSelf->GetSilverMoney() - leftMoney;//双币种判断
				}

				//维修身上装备，判断是否要将属性加给玩家
				for ( unsigned int i=0; i<repairedEquipItemCount; ++i )
				{
					if ( i >= ARRAY_SIZE(needRepairedItems) )
					{
						D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, 越界2, repairedEquipItemCount(%d)\n", repairedEquipItemCount );
						break;
					}
					NeedRepairedItems& repairItem = needRepairedItems[i];
					if ( NULL == repairItem.pItem )
					{
						D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, NULL == repairItem.pItem， 一\n" );
						break;
					}
					//如果维修的道具的耐久度为0了，则代表已经扣除了装备的属性，现在维修后，需要加上去
					int oldwearpoint = repairItem.pItem->usWearPoint;
					repairItem.pItem->usWearPoint = repairItem.newWear;
					if ( oldwearpoint == 0 )
					{
						ItemAttributeHelper::OnRepairWearPointEmtpyItem( pSelf,  *(repairItem.pItem) );
					}
					CLogManager::DoRepairItem(pSelf, 0, needRepairedItems[i].costMoney, needRepairedItems[i].pItem->uiID, needRepairedItems[i].pItem->usItemTypeID );//记日至
				}

				//维修包裹中的装备
				for ( int i=repairedEquipItemCount; i<repairedCount; ++i )
				{
					if ( i >= ARRAY_SIZE(needRepairedItems) )
					{
						D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, 越界3, repairedCount(%d)\n", repairedCount );
						break;
					}
					if ( NULL == needRepairedItems[i].pItem )
					{
						D_ERROR( "CDealG_MPkg::OnRepairWearByNpc, NULL == needRepairedItems[i].pItem， 二\n" );
						break;
					}
					needRepairedItems[i].pItem->usWearPoint = needRepairedItems[i].newWear;
					CLogManager::DoRepairItem(pSelf, 0, needRepairedItems[i].costMoney, needRepairedItems[i].pItem->uiID, needRepairedItems[i].pItem->usItemTypeID );//记日至
				}

				//更新金钱及耐久度
				if ( pSelf->IsUseGoldMoney() )
				{
					//使用金币
					pSelf->SubGoldMoney( resp.repairGoldMoney );//双币种判断
				} else {
					//使用银币
					pSelf->SubSilverMoney( resp.repairSilverMoney );//双币种判断
				}
			}
		}

		pSelf->SendPkg<MGRepairWearByNPC>(&resp);

		//最新耐久度信息
		if ( (resp.tResult == MGRepairWearByNPC::OK) && (repairedCount > 0) )
		{
			MGNotifyWearChange notify;
			StructMemSet( notify, 0x00, sizeof(notify));

			for (int i=0; i<repairedCount; ++i )
			{
				if ( notify.itemSize >= ARRAY_SIZE(notify.items) )
				{
					D_ERROR( "通知最新耐久度，通知数超限, repairedCount(%d)", repairedCount );
					break;
				}
				if ( i >= ARRAY_SIZE(needRepairedItems) )
				{
					D_ERROR( "通知最新耐久度，取更新道具下标超限, i(%d)\n", i );
					break;
				}
				if ( NULL == needRepairedItems[i].pItem )
				{
					D_ERROR( "通知最新耐久度，NULL == needRepairedItems[i].pItem, i(%d)\n", i );
					break;
				}
				notify.items[notify.itemSize].itemID = needRepairedItems[i].pItem->uiID;
				notify.items[notify.itemSize].newWear = needRepairedItems[i].pItem->usWearPoint;
				notify.itemSize++;

				if ( notify.itemSize == ARRAY_SIZE(notify.items) )
				{
					pSelf->SendPkg<MGNotifyWearChange>(&notify);
					notify.itemSize = 0;
				}
			}

			if ( notify.itemSize > 0 )
			{
				pSelf->SendPkg<MGNotifyWearChange>(&notify);
			}
		}//if((resp.tResult == MGRepairWearByNPC::OK) && (repairedCount > 0))

	}
	
	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnRecvRankChartInfo(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRankPlayerInfoUpdate );

	//RankEventManagerSingle::instance()->RecvRankPlayerUpdateInfo( pMsg );
	RankEventManager::RecvRankPlayerUpdateInfo( pMsg );
	
	return true;

	TRY_END;
	return false;

}

//bool CDealG_MPkg::OnRecvPlayerDeleteRole(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	if ( NULL == pOwner )
//	{
//		return false;
//	}
//	if ( NULL == pPkg )
//	{
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	PKG_CONVERT( GMPlayerDeleteRole );
//
//	//RankEventManagerSingle::instance()->OnDeletePlayerRole( pMsg->race, pMsg->playerUID );
//	RankEventManager::OnDeletePlayerRole( pMsg->race, pMsg->playerUID );
//
//	return true;
//
//	TRY_END;
//
//	return false;
//}

//bool CDealG_MPkg::OnRecvRemoveRankChartPlayer(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
//{
//	if ( NULL == pOwner )
//	{
//		return false;
//	}
//	if ( NULL == pPkg )
//	{
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	PKG_CONVERT( GMRemoveChartPlayer );
//
//	//RankEventManagerSingle::instance()->RemoveRankChartPlayer( pMsg->rankChartID, pMsg->playerUID );
//	RankEventManager::RemoveRankChartPlayer( pMsg->rankChartID, pMsg->playerUID );
//
//	return true;
//
//	TRY_END;
//
//	return false;
//}

#ifdef WAIT_QUEUE
bool CDealG_MPkg::OnRemoveWaitPlayer( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPlayerQuitQueue );
	
	CWaitQueue::RemoveWaitPlayer( pMsg->quitQueuePlayer );
	
	return true;
	TRY_END;
	return false;
}
#endif /*WAIT_QUEUE*/






bool CDealG_MPkg::OnCheckPkgFreeSpace( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMCheckPkgFreeSpace );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	MGCheckPkgFreeSpace checkSpace;
	StructMemSet( checkSpace, 0x00, sizeof(checkSpace));

	if(pPlayer->CanGetItem( pMsg->ItemID, pMsg->ItemNum ))
		checkSpace.result = 0;
	else
		checkSpace.result = -1;

	checkSpace.PurchaseType = pMsg->PurchaseType;
	checkSpace.ItemID = pMsg->ItemID;
	checkSpace.ItemNum = pMsg->ItemNum;
	checkSpace.CliCalc = pMsg->CliCalc;
	
	pPlayer->SendPkg<MGCheckPkgFreeSpace>(&checkSpace);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnFetchVipItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMFetchVipItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
	{
		D_ERROR( "CDealG_MPkg::OnFetchVipItem, NULL == pPlayer\n" );
		return false;
	}

	bool success = true;
	MGFetchVipItem fetchVipItem;
	StructMemSet( fetchVipItem, 0x00, sizeof(fetchVipItem));

	if(pMsg->arrNum > sizeof(pMsg->itemArr)/sizeof(pMsg->itemArr[0]))
	{
		success = false;
		D_ERROR( "CDealG_MPkg::OnFetchVipItem, 玩家%s, 道具包实际数目大于最大数目\n", pPlayer->GetAccount() );	
	}
	else
	{		
		if ( pMsg->arrNum > ITEMS_PER_PAGE ) //ITEMS_PER_PAGE为pMsg中itemArr的大小，确保其不会越过tmpitemarr
		{
			success = false;
		} else {
			unsigned int tmpitemarr[ITEMS_PER_PAGE*2];
			for ( int i=0; i<pMsg->arrNum; ++i )
			{
				if ( ( i >= ARRAY_SIZE(pMsg->itemArr) )
					|| ( i*2+1 >= ARRAY_SIZE(tmpitemarr) )
					)
				{
					success = false;
					D_ERROR( "CDealG_MPkg::OnFetchVipItem, 玩家%s, i(%d) >= ARRAY_SIZE(pMsg->itemArr) || i*2+1 >= ARRAY_SIZE(tmpitemarr)，操作中断\n"
						, pPlayer->GetAccount(), i );
					return false;
				}
				tmpitemarr[i*2] = pMsg->itemArr[i].ItemTypeID;
				tmpitemarr[i*2+1] = pMsg->itemArr[i].ItemNum;
			}
			success = pPlayer->NpcCanGiveItem( tmpitemarr, pMsg->arrNum*2 );
			//for(int i = 0; i < pMsg->arrNum; i++)
			//{
			//	if(!pPlayer->CanGetItem( pMsg->itemArr[i].ItemTypeID, pMsg->itemArr[i].ItemNum ))
			//	{
			//		success = false;
			//		break;
			//	}
			//}
		}
	}

	fetchVipItem.vipID = pMsg->vipID;
	if(success)
		fetchVipItem.result = 0;
	else 
		fetchVipItem.result = -1;

	pPlayer->SendPkg<MGFetchVipItem>(&fetchVipItem);

	if(success)
	{	
		for(int i = 0; i < pMsg->arrNum; i++)
		{
			if ( i >= ARRAY_SIZE(pMsg->itemArr) )
			{
				D_ERROR( "获取vip道具, i(%d) >= ARRAY_SIZE(pMsg->itemArr), 中断操作\n", i );
				return false;
			}
			if(CItemBuilderSingle::instance()->GMCreateItem( pPlayer, pMsg->itemArr[i].ItemTypeID, pMsg->itemArr[i].ItemNum ))
			{
				D_DEBUG("获取vip道具成功, vipid= %d, itemid=%d, itemnum=%d\n", pMsg->vipID, pMsg->itemArr[i].ItemTypeID, pMsg->itemArr[i].ItemNum);
			} else {
				D_DEBUG("获取vip道具出错, vipid= %d, itemid=%d, itemnum=%d\n", pMsg->vipID, pMsg->itemArr[i].ItemTypeID, pMsg->itemArr[i].ItemNum);
			}
		}	
	}

	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnFetchNonVipItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMFetchNonVipItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	MGFetchNonVipItem fetchNonVipItem;
	StructMemSet( fetchNonVipItem, 0x00, sizeof(fetchNonVipItem));

	fetchNonVipItem.tranid = pMsg->tranid;
	if(pPlayer->CanGetItem( pMsg->itemID, pMsg->itemNum ))
		fetchNonVipItem.result = 0;
	else
		fetchNonVipItem.result = -1;

	pPlayer->SendPkg<MGFetchNonVipItem>(&fetchNonVipItem);

	if(fetchNonVipItem.result == 0)
	{	
		if(CItemBuilderSingle::instance()->GMCreateItem( pPlayer, pMsg->itemID, pMsg->itemNum ))
		{
			D_DEBUG("获取非vip道具成功, tranid= %d, itemid=%d, itemnum=%d\n", pMsg->tranid, pMsg->itemID, pMsg->itemNum);
		}
		else
		{
			D_DEBUG("获取非vip道具出错, tranid= %d, itemid=%d, itemnum=%d\n", pMsg->tranid, pMsg->itemID, pMsg->itemNum);
		}	
	}	

	////星钻消费增加工会威望
	//if((pMsg->diamond > 0) && (NULL != pPlayer->Union()))
	//{
	//	pPlayer->ChanageUnionPrestige((unsigned short)CUnionConfig::COST_STAR, pMsg->diamond);
	//}

	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnPickMailItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPickMailItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->pickPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	unsigned int itemInfoArr[2] = {0,0};
	itemInfoArr[0] =  pMsg->itemInfo.usItemTypeID;
	itemInfoArr[1] =  pMsg->itemInfo.ucCount;
	bool bPickSucc = pPlayer->NpcCanGiveItem( itemInfoArr, 2 );
	if ( bPickSucc )
	{
		/*GetMailItem( unsigned int itemTypeID, unsigned int itemCount, unsigned int itemLevel, unsigned int randSet, unsigned int luckySeed, unsigned int excelSeed );*/
		bPickSucc = pPlayer->GetMailItem( pMsg->itemInfo.usItemTypeID, pMsg->itemInfo.ucCount, pMsg->itemInfo.ucLevelUp
			, pMsg->itemInfo.randSet/*新增随机集*/, 0, 0 );//邮件寄送物品的随机属性需要在发送的地方增加；
	}


	MGPickMailItemRst srvmsg;
	srvmsg.isSuss= bPickSucc;
	srvmsg.itemIndex = pMsg->itemIndex;
	srvmsg.itemInfo  = pMsg->itemInfo;
	srvmsg.mailuid   = pMsg->mailuid;
	srvmsg.lNotiPlayerID  = pMsg->pickPlayerID;
	srvmsg.pickPlayerUID = pPlayer->GetDBUID();

	pPlayer->SendPkg<MGPickMailItemRst>( &srvmsg );

	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnPickMailMoney(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPickMailMoney );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->pickPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	bool bPickSucc = false;
	if ( (pPlayer->GetSilverMoney() + pMsg->money) <= MAX_MONEY )
	{
		pPlayer->AddSilverMoney( pMsg->money, 1 );//双币种判断
		bPickSucc  = true ;
	}

	MGPickMailMoneyRst srvmsg;
	srvmsg.isSuss  = bPickSucc;
	srvmsg.mailuid = pMsg->mailuid;
	srvmsg.money   = pMsg->money;
	srvmsg.lNotiPlayerID = pMsg->pickPlayerID;
	srvmsg.pickPlayerUID = pPlayer->GetDBUID();


	pPlayer->SendPkg<MGPickMailMoneyRst>( &srvmsg );

	return true;

	TRY_END;
	return false;

}


bool CDealG_MPkg::OnReqPlayerDetailInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMReqPlayerDetailInfo );

	CGateSrv* pGateSrv = CManG_MProc::FindProc( pMsg->reqTarget.wGID );
	if( NULL == pGateSrv )
	{
		return false;
	}
	CPlayer* pTarget = pGateSrv->FindPlayer( pMsg->reqTarget.dwPID );
	if( NULL == pTarget )
	{
		D_ERROR("服务器找不到查询属性的玩家\n");
		return false;
	}

	CPlayer* pReqPlayer = pOwner->FindPlayer( pMsg->reqPlayer.dwPID );
	if( NULL == pReqPlayer )
	{
		D_ERROR("服务器找不到申请的玩家\n");
		return false;
	}

	ItemAddtionProp& itemProp = pTarget->GetItemAddtionProp(); 

	MGReqPlayerDetailInfoRst gateMsg;
	StructMemSet( gateMsg, 0x0, sizeof(gateMsg) );
	gateMsg.reqRst.uiHp = pTarget->GetCurrentHP();
	gateMsg.reqRst.uiMp = pTarget->GetCurrentMP();
	gateMsg.reqRst.usStr = pTarget->GetUIStr();
	gateMsg.reqRst.usInt = pTarget->GetUIInt();
	gateMsg.reqRst.usAgi = pTarget->GetUIAgi();
	gateMsg.reqRst.usCorp = pTarget->GetUIVit();
	gateMsg.reqRst.usSprit = pTarget->GetUISpi();

	//630待加,体质/精神属性, 已加，by dzj, 06.10.22;

	//gateMsg.reqRst.uiPhyAtkMin = pTarget->GetUIPhyAtkMin();
	//gateMsg.reqRst.uiPhyAtkMax = pTarget->GetUIPhyAtkMax();
	//gateMsg.reqRst.uiMagAtkMin = pTarget->GetUIMagAtkMin();
	//gateMsg.reqRst.uiMagAtkMax = pTarget->GetUIMagAtkMax();
	//gateMsg.reqRst.uiPhyDef = pTarget->GetUIPhyDef();
	//gateMsg.reqRst.uiMagDef = pTarget->GetUIMagDef();
	//gateMsg.reqRst.uiPhyHit = pTarget->GetUIPhyHit();
	//gateMsg.reqRst.uiMagHit = pTarget->GetUIMagHit();
	//gateMsg.reqRst.uiPhyCriLevel = pTarget->GetPhyCriLevel();
	//gateMsg.reqRst.uiMagCriLevel = pTarget->GetMagCriLevel();
	//gateMsg.reqRst.uiPhyDodge = pTarget->GetUIPhyDodge();
	//gateMsg.reqRst.uiMagDodge = pTarget->GerUIMagDodge();
	//gateMsg.reqRst.antiFaint = itemProp.Antistun;
	//gateMsg.reqRst.antiBondage = itemProp.Antitie;
	//gateMsg.reqRst.antiPhyCri = itemProp.PhysicsCriticalDerate;
	//gateMsg.reqRst.antiMagCri = itemProp.MagicCriticalDerate;
	gateMsg.reqRst.goodEvilPoint = pTarget->GetGoodEvilPoint();
	gateMsg.reqRst.evilOnlineTime = pTarget->GetEvilTime();
	//gateMsg.reqRst.racePrestige  = (int)pTarget->GetRacePrestige();//copy...,
	FullPlayerInfo * playerInfo = pTarget->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CDealG_MPkg::OnReqPlayerDetailInfo, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_2_Equips equips = playerInfo->equipInfo;
	PlayerInfo_Fashions fashions = pTarget->Fashions();
	StructMemCpy(gateMsg.reqRst.equips.equips, equips.equips, sizeof(gateMsg.reqRst.equips.equips));
	gateMsg.reqRst.equips.equipSize = EQUIP_SIZE;
	StructMemCpy(gateMsg.reqRst.fashions.fashionData, fashions.fashionData, sizeof(gateMsg.reqRst.fashions.fashionData));
	gateMsg.reqRst.fashions.fashionSize = MAX_FASHION_COUNT;

	CUnion* pUnion = pTarget->Union();
	if( pUnion )
	{
		SafeStrCpy( gateMsg.reqRst.strUnionName , pUnion->GetName() );
		gateMsg.reqRst.unionNameLen = (char)( strlen( gateMsg.reqRst.strUnionName ) + 1 );
	}

	pReqPlayer->SendPkg<MGReqPlayerDetailInfoRst>( &gateMsg );

	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnQuerySuitInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMQuerySuitInfo );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	MGQuerySuitInfoRst rst;
	rst.index = pMsg->index;
	rst.ret = pPlayer->Suit(pMsg->index, rst.suit) ? 0 : -1;

	pPlayer->SendPkg<MGQuerySuitInfoRst>( &rst );
	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnClearSuitInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMClearSuitInfo );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	MGClearSuitInfoRst rst;
	rst.index = pMsg->index;
	rst.ret = pPlayer->Suit(pMsg->index, NULL, true) ? 0 : -1;

	pPlayer->SendPkg<MGClearSuitInfoRst>( &rst );
	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnSaveSuitInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSaveSuitInfo );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	MGSaveSuitInfoRst rst;
	rst.index = pMsg->index;
	rst.ret = pPlayer->Suit(pMsg->index, (SuitInfo_i *)&pMsg->suit, false) ? 0 : -1;

	pPlayer->SendPkg<MGSaveSuitInfoRst>( &rst );

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnChanageSuit( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeSuit );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	int result = EquipItemManagerSingle::instance()->ChanageSuit( pPlayer, pMsg->index, pMsg->repairConfirm);
	MGChangeSuitRst rst;
	rst.index = pMsg->index;
	rst.repairConfirm = false;
	if(result == 0)
		rst.result = 0;
	else if(result == 1)
		rst.result = (unsigned short)GCChanageSuitRst::NON_FULL_SUIT;
	else 
		rst.result = (unsigned short)GCChanageSuitRst::OTHER_ERROR;


	pPlayer->SendPkg<MGChangeSuitRst>(&rst);

	return true;
	TRY_END;

	return false;
}


bool CDealG_MPkg::OnDiscardCollectItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMDiscardCollectItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	pPlayer->ClearCollectInfo();

	return true;
	TRY_END;

	return false;
}


bool CDealG_MPkg::OnPickCollectItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPickCollectItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	pPlayer->OnPickOneCollectItem( pMsg->index );
	
	return true;
	TRY_END;

	return false;
}


bool CDealG_MPkg::OnPickAllCollectItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPickAllCollectItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	
	pPlayer->OnPickAllCollectItem();

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnEquipFashionSwitch( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMEquipFashionSwitch );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;
	
	pPlayer->EquipFashionSwitch();

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnEndAnamorphic( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMEndAnamorphic );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	pPlayer->EndAnamorphic();
	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnPutItemToProtectPlate(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPutItemToProtectItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	pPlayer->GetProtectItemPlate().PutItemToProtectPlate( pMsg->itemUID );

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnConfirmProtectPlate(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMConfirmProtectPlate );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	if ( pMsg->isConfirm )
	{
		pPlayer->GetProtectItemPlate().TryExecPlate();
	}
	else
	{
		pPlayer->GetProtectItemPlate().CancelPlate();
	}

	return true;
	TRY_END;

	return false;
}


bool CDealG_MPkg::OnReleaseItemSpecialProtect(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRleaseItemSpecialProp );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	pPlayer->GetProtectItemManager().ReleaseSpecialItemProtect(  pMsg->itemUID );


	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnCancelItemUnSpecialProtectWaitTime(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMCancelItemSpecialProp );


	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	pPlayer->GetProtectItemManager().CancelItemUnSpecialProtectWaitTime( pMsg->itemUID );

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnTakeItemFromProtectPlate(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMTakeItemFromProtectPlate );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	pPlayer->GetProtectItemPlate().TakeItemFromProtectPlate( pMsg->itemUID );

	return true;

	TRY_END;

	return false;
}

//bool CDealG_MPkg::OnRcvUpdateRankPlayerRankInfo(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	if ( NULL == pOwner )
//	{
//		return false;
//	}
//	if ( NULL == pPkg )
//	{
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	PKG_CONVERT( GMUpdateRankPlayerRankInfo );
//
//	//RankEventManagerSingle::instance()->OnUpdateRankPlayerInfo( pMsg->rankChartID, pMsg->playerUID, pMsg->rankInfo,  pMsg->playerclass, pMsg->level, pMsg->race, pMsg->playerName );
//	RankEventManager::OnUpdateRankPlayerInfo( pMsg->rankChartID, pMsg->playerUID, pMsg->rankInfo,  pMsg->playerclass, pMsg->level, pMsg->race, pMsg->playerName );
//
//
//	TRY_END;
//	return false;
//}


bool CDealG_MPkg::OnCityInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMCityInfo );

	for( int i =0; i< pMsg->cityNum; ++i )
	{
		if ( i >= ARRAY_SIZE(pMsg->cityInfoArr) )
		{
			D_ERROR( "CDealG_MPkg::OnCityInfo, i(%d) >= ARRAY_SIZE(pMsg->cityInfoArr)\n", i );
			return false;
		}

		CMuxMap* pMap = CManNormalMap::FindMap( pMsg->cityInfoArr[i].uiMapID );//此消息只针对普通地图，而非副本地图；
		if( NULL == pMap )
		{
			D_WARNING("在此mapsrv上没有ID:%d的关卡地图\n", pMsg->cityInfoArr[i].uiMapID );
			continue;
		}
		
		CityInfo* pNewCityInfo = NEW CityInfo( pMsg->cityInfoArr[i] );
		//if( pMap->IsHaveCityInfo() )	//如果已经有 则要防止内存泄露,删除
		//{
		//	pMap->ClearCityInfo();
		//}
		//pMap->SetCityInfo( pNewCityInfo );
		if ( !pMap->SetCityInfo( pNewCityInfo ) )//检查是否有CityInfo放在SetCityInfo中
		{
			delete pNewCityInfo;
			pNewCityInfo = NULL;
		}
		D_INFO("接收到地图号%d的关卡地图信息\n", pMsg->cityInfoArr[i].uiMapID );
	}

	return true;
	TRY_END;
	return false;
}

#ifdef ANTI_ADDICTION
bool CDealG_MPkg::OnGameStateNotify( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMGameStateNotify );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		return false;
	}

	pPlayer->SetAddictionState( pMsg->state );
	if( pMsg->state == 1 )
	{
		//pPlayer->SendSystemChat("您已经进入疲劳时间状态\n");
	}
	
	return true;
	TRY_END;

	return false;
}
#endif

bool CDealG_MPkg::OnNotifyRelationNumber( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMNotifyRelationNumber );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer )
	{
		return false;
	}

	switch( pMsg->mode )
	{
	case GMNotifyRelationNumber::Number_Set:
		( pMsg->type == GMNotifyRelationNumber::Relation_Friend )? pPlayer->SetFriendNum( pMsg->relationNum ):
			pPlayer->SetEnemyNum( pMsg->relationNum );
		break;

	case GMNotifyRelationNumber::Number_Add:
		{
			   ( pMsg->type == GMNotifyRelationNumber::Relation_Friend )? pPlayer->AddFriendNum( pMsg->relationNum ) : pPlayer->AddEnemyNum( pMsg->relationNum );
			if ( pMsg->type == GMNotifyRelationNumber::Relation_Friend  )
			{
				unsigned int sexinfo = (unsigned int)pMsg->friendsex;
				sexinfo  = sexinfo<<16;
				sexinfo |= ( pPlayer->GetRace()!=pMsg->friendsex )?1:0;//加的玩家与被加的玩家性别是否是一样的
				//所加好友的性别放高位 两者性别是否一样放低位

				//加好友成功，好友计数
				pPlayer->TryCounterEvent( CT_FRIEND, sexinfo , 1 );
			}
		}
		break;

	case GMNotifyRelationNumber::Number_Sub:
		( pMsg->type == GMNotifyRelationNumber::Relation_Friend )? pPlayer->SubFriendNum( pMsg->relationNum ):
			pPlayer->SubEnemyNum( pMsg->relationNum );
		break;
	
	default:
		break;
	}
	
	D_INFO("收到更改关系数量消息 修改mode:%d type:%d value:%d\n", pMsg->mode, pMsg->type, pMsg->relationNum );
	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnUseSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMUseSkill );

	CPlayer* pUseSkillPlayer = pOwner->FindPlayer( pMsg->attackID.dwPID );
	if( NULL == pUseSkillPlayer )
	{
		return false;
	}
	CMuxMap* pMap = pUseSkillPlayer->GetCurMap();
	if( NULL == pMap )
	{
		D_ERROR( "OnUseSkill，玩家%s所在地图空\n", pUseSkillPlayer->GetNickName() );
		return false;
	}

	pMap->OnPlayerUseSkill( pUseSkillPlayer, pMsg );


	return true;
	TRY_END;
	return false;
}

//Register( G_M_USE_SKILL_2, &OnUseSkill2 );  //使用技能2，6.30版本矩形攻击相应调整所有范围攻击
bool CDealG_MPkg::OnUseSkill2( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMUseSkill2 );

	CPlayer* pUseSkillPlayer = pOwner->FindPlayer( pMsg->attackID.dwPID );
	if( NULL == pUseSkillPlayer )
	{
		return false;
	}
	CMuxMap* pMap = pUseSkillPlayer->GetCurMap();
	if( NULL == pMap )
	{
		D_ERROR( "OnUseSkill2，玩家%s所在地图空\n", pUseSkillPlayer->GetNickName() );
		return false;
	}

	pMap->OnPlayerUseSkill2( pUseSkillPlayer, pMsg );

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnUnionMultiNtf(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMUnionMultiNtf );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerId.dwPID );
	if( NULL == pPlayer )
	{
		return false;
	}

	CUnion *pUnion = pPlayer->Union();
	BYTE multi = (BYTE)pMsg->multi;
	if(NULL != pUnion)
	{
		if(pMsg->type == 0)
		{
			pUnion->SetNTimesActive(multi, pMsg->endTime);		
		}
		else
		{
			pUnion->SetNTimesPrestige(multi, pMsg->endTime);
		}
	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnCloseStoragePlate(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMCloseStoragePlate );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().CloseStoragePlate();

	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnChangeStorageLockState(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeStorageLockState );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandlePlayerChangeStorageLockState();

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnTakeItemFromStorage(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMTakeItemFromStorage );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandleTakeItemFromStorageToPkg( pMsg->storageIndex );

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnTakeItemFromStorageIndexToPkgIndex(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMTakeItemFromStorageIndexToPkgIndex );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;
	
	pPlayer->GetPlayerStorage().HandleTakeItemFromStorageIndexToPkgIndex( pMsg->storageIndex, pMsg->pkgIndex );

	return true;
	TRY_END;
	
	return false;
}

bool CDealG_MPkg::OnPutItemToStorage(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPutItemToStorage );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandlePutItemToStorageFromPkg( pMsg->pkgIndex );

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnPutItemToStorageIndexFromPkgIndex(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPutItemFromPkgIndexToStorageIndex );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandlePutItemToStorageIndexFromPkgIndex( pMsg->storageIndex, pMsg->pkgIndex );
	
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnSetNewStoragePassWord(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSetNewStoragePassWord );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandlePlayerSetNewPassWord( pMsg->szStoragePassWord, pMsg->securityMode );
	
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnResetStoragePassWord(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMResetStoragePassWord );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandlePlayerResetPassWord( pMsg->szOldStoragePassWord, pMsg->szStoragePassWord, pMsg->securityMode );

	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnTakeItemCheckPassWord(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMTakeItemCheckPassWord );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandlePlayerTakeItemPsdCheck( pMsg->szStoragePassWord );
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnChangeStorageLockStateCheckPsd(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeStorageStateCheckPassWord );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandleInputPsdToUnLockStorage( pMsg->szStoragePassWord );

	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnExtendStorage(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMExtendStorage );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandlePlayerExtendStorage();

	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnSortStorage(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSortStorage );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandleSortStorageLableItem( pMsg->storageLable );

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnRecvStorageItemInfo(CGateSrv *pOwner, const char *pPkg, const unsigned short wPkgLen)
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMStorageItemInfo );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().RecvStorageInfo( pMsg->storageItemArr, pMsg->labelIndex );

	return true;
	TRY_END;

	return false;
}

bool CDealG_MPkg::OnRecvStorageSafeInfo(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMStorageSafeInfo );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().RecvStorageSecurityInfo( pMsg->validRow, pMsg->securityMode, pMsg->szStoragePsd );
	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRecvStorageForbiddenInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMStorageForbiddenInfo );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;


	pPlayer->GetPlayerStorage().RecvForbiddenInfo( pMsg->forbiddenTime , pMsg->inputErrCount );
	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnSwapStorageItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSwapStorageItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerStorage().HandlePlayerSwapStorageItem(  pMsg->fromIndex, pMsg->toIndex );
	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnSortPkgItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSortPkgItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->sortPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetSortPkgItem().OnRecvItemInfo(pMsg->sortPkgInfo.pkgInfo,PKG_PAGE_SIZE);
	//EquipItemManagerSingle::instance()->OnSortPkgItem( pPlayer, pMsg->sortPkgInfo.sortLabelIndex );
	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnSortPkgItemBegin(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSortPkgItemBegin );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->sortPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetSortPkgItem().OnBegin(pMsg->sortPage);

	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnSortPkgItemEnd(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSortPkgItemEnd );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->sortPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetSortPkgItem().OnEnd();

	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnCloseOLTestPlate(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMCloseOLTestDlg );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->closePlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerOLTestPlate().CloseAnswerQuestionWaitDlg();
	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnAnswerQuestion(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMAnswerOLTestQuestion );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->answerPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerOLTestPlate().AnswerTestQuestion( pMsg->selectID );
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnDropOLTest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMDropOLTest );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->dropTestPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->GetPlayerOLTestPlate().DropOLTest();
	return true;

	TRY_END;

	return false;
}

//bool CDealG_MPkg::OnRefreshRankChart(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
//{
//	if ( NULL == pOwner )
//	{
//		return false;
//	}
//
//	if ( NULL == pPkg )
//	{
//		return false;
//	}
//
//	TRY_BEGIN;
//
//	PKG_CONVERT( GMRankChartRefresh );
//
//	//RankEventManagerSingle::instance()->RefreshRankChartByChartID( pMsg->rankChartID );
//	RankEventManager::RefreshRankChartByChartID( pMsg->rankChartID );
//
//	return true;
//	TRY_END;
//	return false;
//}

bool CDealG_MPkg::OnRecvSinglePlayerTeamID(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSinglePlayerTeamIDNotify );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->upLinePlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->SetCopyTeamInfo( pMsg->teamID, pMsg->playerFlag, pMsg->teamFlag );

	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnGMQueryReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		D_ERROR( "CDealG_MPkg::OnGMQueryReq, NULL == pOwner\n" );
		return false;
	}

	if ( NULL == pPkg )
	{
		D_ERROR( "CDealG_MPkg::OnGMQueryReq, NULL == pPkg\n" );
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMGMQueryReq );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->targetPlayerId.dwPID );//这里似乎有错，gatesrv与dwPID不一定相符，by dzj, 10.06.08;
	if ( NULL == pPlayer )
	{
		D_ERROR( "CDealG_MPkg::OnGMQueryReq, NULL == pPlayer，dwPID(%d:%d)\n", pMsg->targetPlayerId.wGID, pMsg->targetPlayerId.dwPID );
		return false;
	}

	CGateSrv* pGateSrv = CManG_MProc::FindProc( pMsg->gmPlayerId.wGID );
	if( NULL == pGateSrv )
	{
		D_ERROR("CDealG_MPkg::OnGMQueryReq找不到GateSrv%d 不能发放消息\n", pMsg->gmPlayerId.wGID);
		return false;
	}

	FullPlayerInfo* pFullPlayerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == pFullPlayerInfo )
	{
		D_ERROR( "CDealG_MPkg::OnGMQueryReq，玩家%s的fullPlayerInfo为空\n", pPlayer->GetAccount() );
		return false;
	}

	MGGMQueryResp srvmsg;
	StructMemSet( srvmsg, 0x00, sizeof(srvmsg));
	srvmsg.lNotiPlayerID = pMsg->gmPlayerId;
	srvmsg.resp.uType = pMsg->gmReq.uType;
	srvmsg.resp.xInfo.none = pMsg->gmReq.uType;
	srvmsg.resp.xKey = pMsg->gmReq.xKey;

	switch(pMsg->gmReq.uType)
	{
		case GTCT_ROLE_BASE://角色基本信息
			{
				PlayerInfo_1_Base &targetBase = pFullPlayerInfo->baseInfo;
				PlayerSecondProp& targetTempInfo = pPlayer->GetTmpProp();

				GTCmd_Info_Role_Base &baseInfo = srvmsg.resp.xInfo.xInfo.xRoleBase;
				
				SafeStrCpy(baseInfo.sAccountName, targetBase.szAccount);                  	//
				baseInfo.uAccountNameSize = targetBase.accountSize;							//账号名长度
				baseInfo.uRoleID = targetBase.uiID;											//角色ID
				SafeStrCpy(baseInfo.sRoleName, targetBase.szNickName);						//
				baseInfo.uRoleNameSize = targetBase.nameSize;								//角色名长度
				baseInfo.uOnline = true;                                    				//是否在线
				baseInfo.shFlag = pPlayer->GetSHFlag();                                       		//性别
				baseInfo.uLevel = targetBase.usLevel;                                     	//角色等级
				baseInfo.uClass = targetBase.usClass;                                     	//角色职业
				baseInfo.uRace = targetBase.ucRace;                                      	//角色种族
				baseInfo.uRacePrestige = targetBase.racePrestige;							//角色种族声望
				baseInfo.iEvilPoint = targetBase.sGoodEvilPoint;                            //善恶值
				baseInfo.uEvilTime = targetBase.uiEvilTime;                                 //魔头时间
				baseInfo.uSilverMoney = targetBase.dwSilverMoney;                           //银币，双币种判断
				baseInfo.uGoldMoney = targetBase.dwGoldMoney;                           //银币，双币种判断
				baseInfo.uMapID = targetBase.usMapID;                                     	//当前地图编号
				baseInfo.uMapX = targetBase.iPosX;                                      	//当前地图X坐标
				baseInfo.uMapY = targetBase.iPosY;                                      	//当前地图Y坐标
				baseInfo.uHP = targetBase.uiHP;                                        		//当前HP
				baseInfo.uMP = targetBase.uiMP;                                        		//当前MP
				baseInfo.uSP = targetBase.usSpPower;//copy...,baseInfo.uSP = targetBase.usSpExp;                                        	//SP

				baseInfo.uMaxHP = targetTempInfo.lMaxHP;									//最大HP
				baseInfo.uMaxMP = targetTempInfo.lMaxMP;									//最大MP
				baseInfo.uStg = pPlayer->GetStrength();//targetTempInfo.nStrength;                                   //力量
				baseInfo.uInt = pPlayer->GetIntelligence();//targetTempInfo.nIntelligence;                               //智力
				baseInfo.uAgi = pPlayer->GetAgility();//targetTempInfo.nAgility;                                    //敏捷
				baseInfo.uPhyAttack = targetTempInfo.nPhysicsAttack;                        //物理攻击
				baseInfo.uPhyDefence = targetTempInfo.nPhysicsDefend;                       //物理防御
				baseInfo.uPhyHit = targetTempInfo.nPhysicsHit;                              //物理命中
				baseInfo.uMagAttack = targetTempInfo.nMagicAttack;                          //魔法攻击
				baseInfo.uMagDefence = targetTempInfo.nMagicDefend;                         //魔法防御
				baseInfo.uMagHit = targetTempInfo.nMagicHit;                                //魔法命中
			
				CUnion *pUnion = pPlayer->Union();
				if(NULL != pUnion)
				{
					SafeStrCpy(baseInfo.sUnionName, pUnion->GetName());
					baseInfo.uUnionNameSize = (UINT)strlen(baseInfo.sUnionName);
				}
			}
			break;
		case GTCT_ROLE_SKILL://角色技能
			{
				srvmsg.resp.xInfo.xInfo.xRoleSkill.skill = pFullPlayerInfo->skillInfo;
			}
			break;
		case GTCT_ROLE_BAG://角色背包
			{
				D_ERROR("GM工具，查询角色背包信息，由于装备信息结构改变，暂时无效\n");
				//PlayerInfo_3_Pkgs &targetPkgs = pPlayer->GetFullPlayerInfo()->pkgInfo;
				//GTCmd_Info_Role_Bag &pkg = srvmsg.resp.xInfo.xInfo.xRoleBag;
				//for(int i = 0; i < PACKAGE_SIZE; i++)
				//{
				//	pkg.xBag[pkg.uCount].uSlot = i;//copy...
				//	pkg.xBag[pkg.uCount].xItem = targetPkgs.pkgs[i];
				//	pkg.uCount++;
				//	
				//	if(pkg.uCount == sizeof(pkg.xBag)/sizeof(pkg.xBag[0]))
				//	{
				//		MsgToPut* pNewMsg = CreateSrvPkg( MGGMQueryResp, pGateSrv, srvmsg );
				//		pGateSrv->SendPkgToGateServer( pNewMsg );

				//		pkg.uCount = 0;
				//	}
				//}
			}
			break;
		case GTCT_ROLE_QUEST://角色任务
			{
				//const TaskRecordInfo *pTargetTaskInfo = pFullPlayerInfo->taskInfo;
				GTCmd_Info_Role_Quest& task = srvmsg.resp.xInfo.xInfo.xRoleQuest;
				for(int i = 0; i < MAX_TASKRD_SIZE; i++ )
				{
					if ( ( task.uCount >= ARRAY_SIZE(task.xQuest) )
						|| ( i >= ARRAY_SIZE(pFullPlayerInfo->taskInfo) )
						)
					{
						D_ERROR( "CDealG_MPkg::OnGMQueryReq, GTCT_ROLE_QUEST, task.uCount(%d)或i(%d)越界\n", task.uCount, i );
						return false;
					}
					task.xQuest[task.uCount].uID = pFullPlayerInfo->taskInfo[i].taskID;
					task.xQuest[task.uCount].uTime = pFullPlayerInfo->taskInfo[i].taskTime;
					task.xQuest[task.uCount].uState = pFullPlayerInfo->taskInfo[i].taskState;
					task.uCount++;

					if ( task.uCount == ARRAY_SIZE(task.xQuest) )
					{
						MsgToPut* pNewMsg = CreateSrvPkg( MGGMQueryResp, pGateSrv, srvmsg );
						pGateSrv->SendPkgToGateServer( pNewMsg );
						task.uCount = 0;
					}
				}// for(int i = 0; i < MAX_TASKRD_SIZE; i++ )
			}
			break;
		case GTCT_ROLE_EQUIP://角色装备
			{
				srvmsg.resp.xInfo.xInfo.xRoleEquip.equip = pFullPlayerInfo->equipInfo;
			}
			break;

		case GTCT_ROLE_PET://宠物信息
			{
				CMuxPet *pPet = pPlayer->GetPet();
				if((NULL != pPet) && pPlayer->IsActivePet())
				{
					MGPetinfoStable   *pPetStable = NULL;
					MGPetinfoInstable *pPetInstable = NULL;
					MGPetAbility      *pPetAbility = NULL;

					pPet->GetDetailInfo(pPetStable, pPetInstable, pPetAbility);
					if((NULL != pPetStable) && (NULL != pPetInstable) && (NULL != pPetAbility))
					{
						GTCmd_Info_Role_Pet &petInfo = srvmsg.resp.xInfo.xInfo.xRolePet;
						petInfo.uID = pPlayer->GetPlayerUID();
						SafeStrCpy(petInfo.sPetName, pPetStable->petInfoStable.petName);
						petInfo.uPetNameSize = (UINT)strlen(petInfo.sPetName);
						petInfo.uPetLookUse = pPetStable->petInfoStable.lookPos;

						petInfo.uPetLevel = pPetInstable->petInfoInstable.petLevel;
						petInfo.uPetHappiness = pPetInstable->petInfoInstable.petHunger;
						petInfo.uPetExp =pPetInstable->petInfoInstable.petExp;
						petInfo.uPetSkill =pPetInstable->petInfoInstable.petSkillSet;

						SafeStrCpy(petInfo.uPetLook,pPetAbility->petAbility.lookValid);
						petInfo.uPetLookSize = (UINT)strlen(petInfo.uPetLook);
					}
				}
			}
			break;

		default:
			{
				D_ERROR( "CDealG_MPkg::OnGMQueryReq, 无效的pMsg->gmReq.uType(%d)\n", pMsg->gmReq.uType );
				return false;
			}
	}
	
	MsgToPut* pNewMsg = CreateSrvPkg( MGGMQueryResp, pGateSrv, srvmsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );

	return true;

	TRY_END;
	return false;
}



bool  CDealG_MPkg::OnBatchSellItemsReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMBatchSellItemsReq );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->SoldGreyPkgItems( *pMsg );
	return true;

	TRY_END;
	return false;
}

bool  CDealG_MPkg::OnRepurchaseItemReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRepurchaseItemReq );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->RepurchaseItem( *pMsg );
	return true;

	TRY_END;
	return false;
}

#ifdef ITEM_NPC
bool CDealG_MPkg::OnItemNpcReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMItemNpcReq);

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	CMuxMap* pMap = pPlayer->GetCurMap();
	if( NULL == pMap )
		return false;

	PlayerID characterID;
	if( !pMap->GetCharaterIDByItemSeq( pMsg->req.buildSeq, characterID ) )
	{
		return false;
	}

	MGItemNpcID idInfo;
	idInfo.idInfo.characterID = characterID;
	idInfo.idInfo.buildSeq = pMsg->req.buildSeq;
	pPlayer->SendPkg<MGItemNpcID>( &idInfo );

	//如果状态不为初始状态再发一个状态变化的更新消息
	unsigned int changedStatus = 0;
	if( pMap->IsItemNpcChange( pMsg->req.buildSeq, changedStatus) )
	{
		MGItemNpcStatusInfo npcStatus;
		npcStatus.itemNpc.buildNpcCount = 1;
		npcStatus.itemNpc.buildNpcArr[0].buildSeq = pMsg->req.buildSeq;
		npcStatus.itemNpc.buildNpcArr[0].curStatus = changedStatus;
		pPlayer->SendPkg<MGItemNpcStatusInfo>( &npcStatus );
	}

	return true;
	TRY_END;
	return false;
}
#endif //ITEM_NPC

//收到创建新副本指令
bool CDealG_MPkg::OnNewCopyCmd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	TRY_BEGIN;

	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMNewCopyCmd );

	bool iscopycreated = CManMuxCopy::CreateNewCopy( pMsg->copyInfo.enterReq.copyMapID, pMsg->copyInfo.copyID );
	if ( !iscopycreated )
	{
		D_DEBUG( "不可能错误，副本(mapid:%d,uid:%d)创建失败\n", pMsg->copyInfo.enterReq.copyMapID, pMsg->copyInfo.copyID  );//副本调试日志;
		return false;
	}

	D_DEBUG( "副本(mapid:%d,uid:%d)已创建\n", pMsg->copyInfo.enterReq.copyMapID, pMsg->copyInfo.copyID );

	return true;
	TRY_END;
	return false;
}

//收到删除旧副本指令
bool CDealG_MPkg::OnDelCopyCmd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	TRY_BEGIN;

	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMDelCopyCmd );

	bool iscopydeled = CManMuxCopy::ForceDelCopy( pMsg->copyInfo );
	if ( !iscopydeled )
	{
		D_DEBUG( "OnDelCopyQuest,不可能错误，副本(mapid:%d,uid:%d)删除失败\n", pMsg->copyInfo.enterReq.copyMapID, pMsg->copyInfo.copyID  );//副本调试日志;
		return false;
	}

	D_DEBUG( "OnDelCopyQuest,副本(mapid:%d,uid:%d)已删除\n", pMsg->copyInfo.enterReq.copyMapID, pMsg->copyInfo.copyID );

	return true;
	TRY_END;
	return false;
}

///centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
bool CDealG_MPkg::OnDelCopyQuest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	TRY_BEGIN;

	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMDelCopyQuest );

	bool isissued= CManMuxCopy::OnCenterSrvDelCopyQuest( pMsg->copyInfo );
	if ( !isissued )
	{
		D_DEBUG( "OnDelCopyQuest,不可能错误，副本(mapid:%d,uid:%d)删除失败\n", pMsg->copyInfo.enterReq.copyMapID, pMsg->copyInfo.copyID  );//副本调试日志;
		return false;
	}

	D_DEBUG( "OnDelCopyQuest,副本(mapid:%d,uid:%d)准备发起删除\n", pMsg->copyInfo.enterReq.copyMapID, pMsg->copyInfo.copyID );

	return true;
	TRY_END;
	return false;
}

///玩家离开副本组队通知；
bool CDealG_MPkg::OnLeaveSTeamNoti( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	TRY_BEGIN;

	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMLeaveSTeamNoti );

	//1、找到指定玩家；
	//2、若玩家不在副本中，打出错信息，否则发起踢玩家出副本；

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->leavePlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_ERROR( "CDealG_MPkg::OnLeaveSTeamNoti， 找不到玩家%d\n", pMsg->leavePlayerID.dwPID );
		return false;
	}

	if ( !(pPlayer->IsPlayerCurInCopy()) )
	{
		D_ERROR( "CDealG_MPkg::OnLeaveSTeamNoti， 玩家%s当前不在副本中\n", pPlayer->GetAccount() );
		return false;
	}

	pPlayer->IssuePlayerLeaveCopyHF();

	//CNewMap* pCopyMap = NULL;
	/////通过副本ID找副本
	//if ( !CManMuxCopy::FindCopyByCopyID( pMsg->copyID, pCopyMap ) )
	//{
	//	D_ERROR( "CDealG_MPkg::OnLeaveSTeamNoti, 找不到玩家(%d,%d)欲清信息的副本%d\n"
	//		, pMsg->leavePlayerID.wGID, pMsg->leavePlayerID.dwPID, pMsg->copyID );
	//	return false;
	//}

	//if ( NULL == pCopyMap )
	//{
	//	D_ERROR( "CDealG_MPkg::OnLeaveSTeamNoti, 玩家(%d,%d)欲离开的副本%d指针空\n"
	//		, pMsg->leavePlayerID.wGID, pMsg->leavePlayerID.dwPID, pMsg->copyID );
	//	return false;
	//}

	//D_DEBUG( "CDealG_MPkg::OnLeaveSTeamNoti, 玩家(%d,%d)离开副本组队，清副本%d中该玩家的第一次进入信息\n"
	//	, pMsg->leavePlayerID.wGID, pMsg->leavePlayerID.dwPID, pMsg->copyID );

	//pCopyMap->OnPlayerLeaveSTeamProc( pMsg->leavePlayerID );

	return true;
	TRY_END;
	return false;
}

///玩家向mapsrv查询全局副本信息(该mapsrv管理的全部伪副本)
bool CDealG_MPkg::OnQueryGlobeCopyInfos( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	TRY_BEGIN;

	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMQueryGlobeCopyInfos );

	CManMuxCopy::OnQueryGlobePCopyInfo( pMsg->notiPlayerID );

	return true;
	TRY_END;
	return false;
}

//Register( G_M_TIMELIMIT_RES, &OnPlayerTimeLimitRes );//玩家点击倒计时框；
bool CDealG_MPkg::OnPlayerTimeLimitRes( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	TRY_BEGIN;

	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMTimeLimitRes );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->resPlayerID.dwPID );
	if ( NULL == pPlayer )
	{
		D_ERROR( "CDealG_MPkg::OnPlayerTimeLimitRes， 找不到玩家%d\n", pMsg->resPlayerID.dwPID );
		return false;
	}

	if ( 1 == pMsg->limitReason )
	{
		//玩家确认踢出副本；
		pPlayer->IssuePlayerLeaveCopyLF();
	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnRepurchaseItemsListReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRepurchaseItemsListReq );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	pPlayer->RepurchaseListNtf();//通知回购列表;
	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnSelectWarRebirthPtOption( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSelectWarRebirthOption );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	if ( pPlayer->IsWaitToDel() )
	{
		return false;
	}

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CDealG_MPkg::OnSelectWarRebirthPtOption, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	//玩家是否死亡？
	if ( !pPlayer->IsDying() )
	{
		//当前非死亡状态，不理会；
		D_ERROR( "OnSelectWarRebirthPtOption,玩家当前状态不是死亡，而是%d\n", playerInfo->stateInfo.m_CurBaseState );
		return false;
	}

	//玩家死于攻城战？
	if(!playerInfo->stateInfo.ForWarDie())
	{
		D_ERROR( "OnSelectWarRebirthPtOption,玩家当前状态不是攻城战死亡\n" );
		return false;
	}

	pPlayer->OnSelectWarRebirthOption(pMsg->req.rebirthOption);
	return true;

	TRY_END;
	return false;
}


bool CDealG_MPkg::OnRaceMasterSetNewNotice(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMOnRaceMasterSetNewPublicMsg );

	AllRaceMasterManagerSingle::instance()->OnRaceMasterSetNewRaceNotice( pMsg->raceType, (char *)&pMsg->msgArr[0] );
	return true;

	TRY_END;

	return false;
}


bool CDealG_MPkg::OnRaceMasterTimeInfoChange(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRaceMasterTimeInfoChange );

	AllRaceMasterManagerSingle::instance()->OnRecvRaceMasterTimerInfoChange( pMsg->raceType, pMsg->noticeCount, pMsg->lastupdatetime, pMsg->isGotSalary );
	return true;

	TRY_END;

	return false;
}


bool CDealG_MPkg::OnRaceMasterTimeInfoExpire(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRaceMasterTimeInfoExpire );

	AllRaceMasterManagerSingle::instance()->OnRecvRaceMasterTimeExpireNotice( pMsg->raceType );
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnRaceMasterDeclareWarInfoChange(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRaceMasterDeclareInfoChange );

	AllRaceMasterManagerSingle::instance()->OnRecvRaceMasterDeclareWarInfoUpdate( pMsg->raceType, pMsg->declarewarInfo );
	return true;


	TRY_END;

	return false;
}

bool CDealG_MPkg::OnRecvRaceMasterDetialInfo(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT(GMRecvRaceMasterDetialInfo);

	AllRaceMasterManagerSingle::instance()->OnRecvRaceMasterInfo( pMsg->raceType, pMsg->masterUID, pMsg->noticeCount, pMsg->lastupdatetime, pMsg->declarewarinfo,
		pMsg->isGotSalary );
	
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnRecvNewRaceMasterBorn(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRaceMasterNewBorn);

	AllRaceMasterManagerSingle::instance()->OnRecvNewRaceMasterBorn( pMsg->racetype, pMsg->masterUID, pMsg->playerName, pMsg->unionName );

	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnRaceMasterSelectOpType(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRaceMasterSelectOpType );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->selectPlayerID.dwPID );
	if ( NULL == pPlayer )
		return false;


	AllRaceMasterManagerSingle::instance()->OnPlayerSelectOPType( pPlayer, pPlayer->GetRace() ,pMsg->opType );
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnRaceMasterDeclareWarToOtherRace(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT(GMRaceMasterDeclareWarToOtherRace);

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;
	
	AllRaceMasterManagerSingle::instance()->OnPlayerDeclareWarToOtherRace( pPlayer, pMsg->race, pMsg->otherracetype );
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnRaceMasterSetNewPublicMsg(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRaceMasterSetNewPublicMsg );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( NULL == pPlayer )
		return false;

	AllRaceMasterManagerSingle::instance()->OnPlayerSetNewPublicMsg( pPlayer, pMsg->race, (char *)&pMsg->msgArr[0] );
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnLoadRaceMasterInfoRst(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMMapSrvLoadRaceMasterRst );

	AllRaceMasterManagerSingle::instance()->OnRecvRaceMasterInfo( pMsg->race,  pMsg->masteruid, pMsg->todaymodifycount,pMsg->lastupdatetime,
		pMsg->declarewarinfo ,
		pMsg->isgotsalary
		);
	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnForbiddenRaceMasterAuthority(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}

	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMForbiddenRaceMasterAuthority );

	AllRaceMasterManagerSingle::instance()->OnRecvForbiddenRaceMasterAuthority();
	return true;

	TRY_END;

	return false;
}


bool CDealG_MPkg::OnPublicNoticeAdd(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	CHECK_FUNC_PARAM_IS_NULL;

	TRY_BEGIN;

	PKG_CONVERT( GMAddNewPublicNotice );

	MGPublicNoticeAdd addmsg;
	StructMemSet( addmsg, 0x0, sizeof(addmsg) );
	StructMemCpy( addmsg.addnotice.newnotice, &pMsg->newnotice, sizeof(addmsg.addnotice.newnotice) );
	//CManNormalMap::BroadCastToAllPlayers<MGPublicNoticeAdd>( &addmsg );
	if ( 0 == CLoadSrvConfig::GetSelfID() ) //因为只需调用一次就发到了所有玩家，因此只mapsrv0发，稍后管传淇修改，只在初始源头发；
	{
		CManG_MProc::SendToAllSrvAllPlayer<MGPublicNoticeAdd>( &addmsg );
	}

	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnPublicNoticeModify(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	CHECK_FUNC_PARAM_IS_NULL;

	TRY_BEGIN;

	PKG_CONVERT( GMModifyPublicNotice );

	MGPublicNoticeModify modifymsg;
	StructMemSet( modifymsg, 0x0, sizeof(modifymsg) );
	StructMemCpy( modifymsg.modifynotice.updatenotice, &pMsg->modifynotice, sizeof(modifymsg.modifynotice.updatenotice) );
	//CManNormalMap::BroadCastToAllPlayers<MGPublicNoticeModify>( &modifymsg );
	if ( 0 == CLoadSrvConfig::GetSelfID() ) //因为只需调用一次就发到了所有玩家，因此只mapsrv0发，稍后管传淇修改，只在初始源头发；
	{
		CManG_MProc::SendToAllSrvAllPlayer<MGPublicNoticeModify>( &modifymsg );
	}

	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnPublicNoticeRemove(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	CHECK_FUNC_PARAM_IS_NULL;

	TRY_BEGIN;

	PKG_CONVERT( GMRemovePublicNotice );

	MGPublicNoticeRemove removemsg;
	StructMemSet( removemsg, 0x0, sizeof(removemsg) );
	removemsg.removenotice.msgid = pMsg->msgid;
	//CManNormalMap::BroadCastToAllPlayers<MGPublicNoticeRemove>( &removemsg );
	if ( 0 == CLoadSrvConfig::GetSelfID() ) //因为只需调用一次就发到了所有玩家，因此只mapsrv0发，稍后管传淇修改，只在初始源头发；
	{
		CManG_MProc::SendToAllSrvAllPlayer<MGPublicNoticeRemove>( &removemsg );
	}

	return true;

	TRY_END;

	return false;
}

bool CDealG_MPkg::OnMarqueueNoticeAdd(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	CHECK_FUNC_PARAM_IS_NULL;

	TRY_BEGIN;

	PKG_CONVERT( GMAddMarqueueNotice );

	MGMarqueueNoticeAdd marqueuemsg;
	StructMemSet( marqueuemsg, 0x0, sizeof(marqueuemsg) );
	StructMemCpy( marqueuemsg.marqueuenotice.newnotice, &pMsg->marqueue, sizeof(marqueuemsg.marqueuenotice.newnotice));
	//CManNormalMap::BroadCastToAllPlayers<MGMarqueueNoticeAdd>( &marqueuemsg );
	if ( 0 == CLoadSrvConfig::GetSelfID() ) //因为只需调用一次就发到了所有玩家，因此只mapsrv0发，稍后管传淇修改，只在初始源头发；
	{
		CManG_MProc::SendToAllSrvAllPlayer<MGMarqueueNoticeAdd>( &marqueuemsg );
	}

	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnTempNoticeBroadcast(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	CHECK_FUNC_PARAM_IS_NULL;

	TRY_BEGIN;

	PKG_CONVERT( GMBroadCastTempNotice );
	GMBroadCastTempNotice* pTempMsg = const_cast<GMBroadCastTempNotice *>( pMsg );
	//变成非全局广播的临时消息，逐个发送
	pTempMsg->tempnotice.isbroadcast = 0;
	//CManNormalMap::BroadCastToAllPlayers<MGShowTmpNoticeByPosition>( &pTempMsg->tempnotice );
	if ( 0 == CLoadSrvConfig::GetSelfID() ) //因为只需调用一次就发到了所有玩家，因此只mapsrv0发，稍后管传淇修改，只在初始源头发；
	{
		CManG_MProc::SendToAllSrvAllPlayer<MGShowTmpNoticeByPosition>( &pTempMsg->tempnotice );
	}

	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnParamNoticeBroadcast(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	CHECK_FUNC_PARAM_IS_NULL;

	TRY_BEGIN;

	PKG_CONVERT( GMBroadCastParamNotice );
	GMBroadCastParamNotice* pParamNotice = const_cast<GMBroadCastParamNotice *>( pMsg );
	pParamNotice->paramntoice.isgloabl = 0;
	//变成非全局广播的临时消息，逐个发送
	//CManNormalMap::BroadCastToAllPlayers<MGSendParamNotice>( &pParamNotice->paramntoice );
	if ( 0 == CLoadSrvConfig::GetSelfID() ) //因为只需调用一次就发到了所有玩家，因此只mapsrv0发，稍后管传淇修改，只在初始源头发；
	{
		CManG_MProc::SendToAllSrvAllPlayer<MGSendParamNotice>( &pParamNotice->paramntoice );
	}

	return true;

	TRY_END;
	return false;
}

bool CDealG_MPkg::OnDiamondToPrestigeNtf(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMDiamondToPrestigeNtf );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->notiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	if(NULL == pPlayer->Union())
	{
		D_ERROR("玩家%s没有工会信息，但是收到星钻转化为威望的请求\n", pPlayer->GetNickName());
		return false;
	}
		
	pPlayer->ChanageUnionPrestige((unsigned short)CUnionConfig::COST_STAR, pMsg->prestige * 100);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnGMAccountNtf(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMGMAccountNtf );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->notiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	pPlayer->SetGmAccount();
	return true;
	TRY_END;
	return false;
}



bool CDealG_MPkg::OnPlayerStartNewAuction(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPlayerStartNewAuction );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if ( pPlayer )
	{
		AuctionManager& auctionManager= pPlayer->GetAuctionManager();
		auctionManager.OnStartNewAuction( pMsg->pkgIndex, pMsg->origiprice, pMsg->fixprice, pMsg->expiretype );	
	}

	return true;

	TRY_END
	return false;
}

bool CDealG_MPkg::OnPlayerBidItem(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMPlayerBidItem );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if ( pPlayer )
	{
		AuctionManager& auctionManager= pPlayer->GetAuctionManager();
		auctionManager.OnPlayerBidItemByNewPrice( pMsg->auctionUID, pMsg->lastprice, pMsg->newprice );
	}
	
	return true;
	TRY_END
	return false;
}

bool CDealG_MPkg::OnPlayerBidItemErrorAddMoney(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMSubPlayerMoney );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerID.dwPID );
	if ( pPlayer )
	{
		pPlayer->AddSilverMoney( pMsg->subMoney );//双币种判断
	}
	return true;
	TRY_END
	return false;
}

bool CDealG_MPkg::OnManualUpgradeReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMManualUpgradeReq );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if(NULL != pPlayer)
	{
		pPlayer->ManualUpgradeLevel();
	}
	
	return true;
	TRY_END
	return false;
}

bool CDealG_MPkg::OnAllocatePointReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMAllocatePointReq );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if(NULL != pPlayer)
	{
		pPlayer->AllocatePoint( pMsg->req.toStr, pMsg->req.toVit, pMsg->req.toAgi, pMsg->req.toSpi, pMsg->req.toInt );
		//pPlayer->ReloadSecondProp();
		ItemManagerSingle::instance()->CheckPlayerEquipItem( pPlayer );
	}

	return true;
	TRY_END
		return false;
}

bool CDealG_MPkg::OnModEffectiveSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMModifyEffectSkillID );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if(NULL != pPlayer)
	{
		pPlayer->ModifyEffectiveSkill( pMsg->req.skillID, pMsg->req.incFlag );
	}

	return true;
	TRY_END
		return false;
}

bool CDealG_MPkg::OnFriendGainNtf( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMNotifyFriendGain );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if(NULL != pPlayer)
	{
		pPlayer->OnRecvAdditionalFriendGain(pMsg->inTeam, pMsg->expOrNot);
	}

	return true;
	TRY_END
	return false;
}

bool CDealG_MPkg::OnQueryExpOfLevelUpReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMQueryExpNeededWhenLvlUpRequest );
	
	const std::map<unsigned long, CClassLevelProp*>& classSet = CManPlayerProp::GetAllClassLevelProp();
	if(classSet.size() > 0)
	{
		for(std::map<unsigned long, CClassLevelProp*>::const_iterator iter = classSet.begin(); classSet.end() != iter; ++iter)
		{
			CClassLevelProp *pClassLevelProp = iter->second;
			if(NULL != pClassLevelProp)
			{
				const std::map<unsigned long,CLevelProp*>& propSet = pClassLevelProp->PropSet();
				if(propSet.size() > 0)
				{
					MGQueryExpNeededWhenLvlUpResult rst;
					bool firstFlag = true;
					unsigned int count = 0;
					for(std::map<unsigned long,CLevelProp*>::const_iterator i = propSet.begin() ; propSet.end() != i; ++i)
					{
						count++;

						if(i->second != NULL)
						{
							if(firstFlag)
							{	
								firstFlag = false;

								rst.lvlFirst = rst.lvlLast = (unsigned short)i->second ->m_lLevel;
								rst.lstLen = 0;
							}
							else
							{
								rst.lvlLast = (unsigned short)i->second->m_lLevel;
							}

							rst.lst[rst.lstLen] = i->second->lNextLevelExp;
							rst.lstLen++;
						}

						if((rst.lstLen == ARRAY_SIZE(rst.lst)/*达到一组上限*/)
							|| ((count == propSet.size()) && (rst.lstLen > 0))/* 结束时*/)
						{
							MsgToPut* pNewMsg = CreateSrvPkg( MGQueryExpNeededWhenLvlUpResult, pOwner, rst );
							pOwner->SendPkgToGateServer(pNewMsg);

							firstFlag = true;//发送一次
						}
					}
				}
			}
		}
	}

	return true;
	TRY_END
	return false;
}

bool CDealG_MPkg::OnReplyCallMember(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMReplyCallMember );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->replyPlayerID.dwPID );
	CallMemberManagerSingle::instance()->OnReplyCallMember(pPlayer,pMsg->replyCallMember.agree,pMsg->replyCallMember.callMemberID);

	return true;
	TRY_END
	return false;
}

bool CDealG_MPkg::OnStartCast(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMStartCast );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if ( NULL != pPlayer )
	{
		pPlayer->GetCastManager().OnStart( pMsg->req.skillID, pMsg->req.castTime );
	}

	return true;
	TRY_END
	return false;
}

bool CDealG_MPkg::OnStopCast(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMStopCast );
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if ( NULL != pPlayer )
	{
		pPlayer->GetCastManager().OnStop();
	}

	return true;
	TRY_END
	return false;
}
