﻿#include "MonsterBuffs.h"



CMonsterBuffs::CMonsterBuffs(void)
{
	m_buffPosFlag = 0;
	m_buffNum = 0;
	m_pOwner = NULL;
	//StructMemSet(m_pMonsterBuffs,0,sizeof(m_pMonsterBuffs));
	StructMemSet(m_buffData,0,sizeof(m_buffData));
}

CMonsterBuffs::~CMonsterBuffs(void)
{
}


bool CMonsterBuffs::CreateMuxBuff( MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID )
{
	if( NULL == pBuffProp || NULL == m_pOwner )
	{
		return false;
	}

	bool isFindSame = false;
	//0为不看规则的顶推 只看自己
	if( pBuffProp->PushIndex == 0 )
	{
		for( unsigned int i=0; i < ARRAY_SIZE(m_pMonsterBuffs); ++i )
		{
			if( !IsPosValid(i) )
				continue;

			if( m_pMonsterBuffs[i].GetCreateID() == createID && m_pMonsterBuffs[i].GetBuffID() == pBuffProp->mID ) 	
			{
				isFindSame = true;
				//说明是可叠加的buff
				unsigned int buffLevel = m_pMonsterBuffs[i].GetBuffLevel();
				if( pBuffProp->TierNum != 0 && buffLevel < pBuffProp->TierNum )
				{
					m_pMonsterBuffs[i].OnMonsterBuffChangeStatus( m_pOwner, i, pBuffProp, buffLevel + 1, true, createID );
					break;
				}else{
					m_pMonsterBuffs[i].OnMonsterBuffChangeStatus( m_pOwner, i, pBuffProp, buffLevel, false, createID );
					break;
				}
			}
		}
	}else{
		//找相同pushindex的
		for( unsigned int i=0; i < ARRAY_SIZE(m_pMonsterBuffs); ++i )
		{
			if( !IsPosValid(i) )
				continue;

			if( m_pMonsterBuffs[i].GetPushIndex() == pBuffProp->PushIndex )
			{
				isFindSame = true;
				if( m_pMonsterBuffs[i].GetBuffID() == pBuffProp->mID )
				{
					//替换升级
					unsigned int buffLevel = m_pMonsterBuffs[i].GetBuffLevel();
					if( pBuffProp->TierNum != 0 && buffLevel < pBuffProp->TierNum )
					{
						m_pMonsterBuffs[i].OnMonsterBuffChangeStatus( m_pOwner, i, pBuffProp, buffLevel + 1, true, createID );
						break;
					}else
					{
						m_pMonsterBuffs[i].OnMonsterBuffChangeStatus( m_pOwner, i, pBuffProp, buffLevel, false, createID );
						break;
					}
				}else
				{
					//对值或者后顶前进行比较
					if( pBuffProp->PushRule == 0 )
					{
						m_pMonsterBuffs[i].OnMonsterBuffChangeStatus( m_pOwner, i, pBuffProp, 1, false, createID );
						break;
					}else if( pBuffProp->PushRule == 1 )
					{
						if( m_pMonsterBuffs[i].GetBuffValue() <= pBuffProp->Value )
						{
							m_pMonsterBuffs[i].OnMonsterBuffChangeStatus( m_pOwner, i, pBuffProp, 1, false, createID );
							break;
						}else
						{
							return false;  //顶推失败
						}
					}
				}
			}
		}
	}

	if( !isFindSame )
	{
		unsigned int index = 0;
		if( FindInvalidIndex( index ) )
		{
			MonsterBuffStartWrapper( &m_pMonsterBuffs[index], index, pBuffProp, buffTime, createID );
		}else{
			D_WARNING("所有栏位已满 此buff将不产生效果\n");
		}
	}
	return true;
}


void CMonsterBuffs::BuffProcess()
{
	//如果没有任何buff 直接返回
	if( !IsHaveBuff() )
	{
		return;
	}

	for( unsigned int i = 0; i < ARRAY_SIZE( m_pMonsterBuffs ); ++i )
	{
		if( IsPosValid(i) )
		{
			if( !m_pMonsterBuffs[i].OnMonsterBuffUpdate( m_pOwner, i ) )
			{
				MonsterBuffEndWrapper( &m_pMonsterBuffs[i], i );
			}
		}
	}

	UpdateMonsterBuffData();
}





bool CMonsterBuffs::MonsterBuffStartWrapper( CMuxBuffEle* pBuffEle, unsigned int buffIndex, MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID )
{
	if( NULL == pBuffEle )
		return false;

	if( pBuffEle->OnMonsterBuffStart( m_pOwner, buffIndex, pBuffProp, buffTime, createID ) )
	{
		AddBuffNum();
		SetPosValid( buffIndex );
		return true;
	}

	D_WARNING("OnMonsterBuffStart return false\n");
	return false;
}


bool CMonsterBuffs::MonsterBuffEndWrapper( CMuxBuffEle* pBuffEle, unsigned int buffIndex )
{
	if( NULL == pBuffEle )
		return false;

	SubBuffNum();
	SetPosInValid( buffIndex );
	pBuffEle->OnMonsterBuffEnd( m_pOwner, buffIndex );
	return true;
}



//解除性BUFF
bool CMonsterBuffs::RelieveBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve/*是否只是探测可能性*/ )
{
	if( !isRealRelieve )
	{
		for( unsigned int i =0; i< ARRAY_SIZE(m_pMonsterBuffs); ++i )
		{
			if( !IsPosValid( i ) )
			{
				continue;
			}

			if( m_pMonsterBuffs[i].GetRelieveType() == relieveType && m_pMonsterBuffs[i].GetRelieveLevel() <= relieveLevel )	
				return true;
		}
		return false;
	}else{
		unsigned int count = 0;
		bool isRelieve = false;
		for( unsigned int i =0; i< ARRAY_SIZE(m_pMonsterBuffs); ++i )
		{
			if( count == relieveNum )
				break;

			if( !IsPosValid( i ) )
			{
				continue;
			}

			if( m_pMonsterBuffs[i].GetRelieveType() == relieveType && m_pMonsterBuffs[i].GetRelieveLevel() <= relieveLevel )	
			{
				isRelieve = true;
				MonsterBuffEndWrapper( &m_pMonsterBuffs[i], i );
				count++;
			}
		}
	    return isRelieve;
	}
}


void CMonsterBuffs::UpdateMonsterBuffData()
{
	StructMemSet( m_buffData, 0, sizeof( m_buffData ) );

	unsigned int count = 0;
	unsigned int buffSize = ARRAY_SIZE(m_buffData.buffData);
	for( unsigned int j = 0; j < ARRAY_SIZE( m_pMonsterBuffs ); ++j )
	{
		if( count >= buffSize )
			break;

		if( IsPosValid(j) )
		{
			m_buffData.buffData[count].uiBuffID = m_pMonsterBuffs[j].GetBuffID();
			m_buffData.buffData[count].uiLeftDurSeconds = m_pMonsterBuffs[j].GetBuffTime();
			count++;

		}
	}
	m_buffData.buffSize = count;
}


