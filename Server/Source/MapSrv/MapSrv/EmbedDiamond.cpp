﻿#include "EmbedDiamond.h"
#include "Player/Player.h"

void EmbedDiamondItemCSInfo::AffectPlayer( CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "EmbedDiamondItemCSInfo::AffectPlayer, NULL == pPlayer\n" );
		return;
	}

	for ( unsigned int i = 0; i< embidDiamondNum; i++ )
	{
		if ( i >= ARRAY_SIZE(embidDiamondsArr) )
		{
			D_ERROR( "EmbedDiamondItemCSInfo::AffectPlayer, i(%d) >= ARRAY_SIZE(embidDiamondsArr)\n", i );
			return;
		}
		EmbedDiamondInfo& diamondInfo = embidDiamondsArr[i];
		int& pItemAttribute =  pPlayer->GetNewItemAddBaseAtt( (ItemBaseAddAtt)diamondInfo.attributeID );//镶嵌影响装备基本属性，稍后修改镶嵌系统时，告知策划对应ID号, by dzj, 10.05.21；
		//if( pItemAttribute ){
			pItemAttribute += diamondInfo.randValue;
		//}
	}	
}

void EmbedDiamondItemCSInfo::UnAffectPlayer(CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "EmbedDiamondItemCSInfo::UnAffectPlayer, NULL == pPlayer\n" );
		return;
	}

	for ( unsigned int i = 0; i< embidDiamondNum; i++ )
	{
		if ( i >= ARRAY_SIZE(embidDiamondsArr) )
		{
			D_ERROR( "EmbedDiamondItemCSInfo::UnAffectPlayer, i(%d) >= ARRAY_SIZE(embidDiamondsArr)\n", i );
			return;
		}
		EmbedDiamondInfo& diamondInfo = embidDiamondsArr[i];
		int& pItemAttribute =  pPlayer->GetNewItemAddBaseAtt( (ItemBaseAddAtt)diamondInfo.attributeID );//镶嵌影响装备基本属性，稍后修改镶嵌系统时，告知策划对应ID号, by dzj, 10.05.21；
		//if( pItemAttribute ){
			pItemAttribute -= diamondInfo.randValue;
		//}
	}	
}


void EmbedDiamondManager::OnPlayerDropItem( unsigned int itemUID )
{
	if ( !m_pAttachPlayer )
		return;

	std::vector<EmbedDiamondItemCSInfo>::iterator iter = embedDiamondItemVec.begin();
	for(; iter != embedDiamondItemVec.end();  )
	{
		if ( (*iter).ownerItemUID == itemUID )
		{
			iter = embedDiamondItemVec.erase( iter );
		}
		else
			++iter;
	}
}

void EmbedDiamondManager::OnPlayerEquipItem(unsigned int itemUID )
{
	if ( !m_pAttachPlayer )
		return;

	EmbedDiamondItemCSInfo* pEmbedItem = GetEmbedDiamondItemByItemUID( itemUID );
	if ( pEmbedItem )
	{
		pEmbedItem->AffectPlayer( m_pAttachPlayer );
	}
}

void EmbedDiamondManager::OnPlayerUnEquipItem(unsigned int itemUID )
{
	if ( !m_pAttachPlayer )
		return;

	EmbedDiamondItemCSInfo* pEmbedItem = GetEmbedDiamondItemByItemUID( itemUID );
	if ( pEmbedItem )
	{
		pEmbedItem->UnAffectPlayer( m_pAttachPlayer );
	}
}