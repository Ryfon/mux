﻿#include "RaceManager.h"
#include "Player/Player.h"
#include "PlayerManager.h"
#include "XmlManager.h"
#include "Activity/WarTaskManager.h"

///向同族玩家发送普通系统消息；
void CRaceManager::SystemInfoSendToRace( const char* msg )
{
	if ( NULL == msg )
	{
		D_ERROR( "CRaceManager::SystemInfoSendToRace，输入消息空\n" );
		return;
	}

	MGChat notiRaceMsg;
	notiRaceMsg.sourcePlayerID.wGID = 0;
	notiRaceMsg.sourcePlayerID.dwPID = 0;
	SafeStrCpy( notiRaceMsg.strChat, msg );
	notiRaceMsg.chatType = CT_RACE_SYS_EVENT;
	notiRaceMsg.extInfo = m_raceType;//种族号填入额外信息；

	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("CRaceManager::SystemInfoSendToRace 找不到随机GateSrv，不能发放消息\n");
		return;
	}

	notiRaceMsg.lNotiPlayerID.wGID  = (unsigned short)pGateSrv->GetGateSrvID();
	notiRaceMsg.lNotiPlayerID.dwPID = 0;

	MsgToPut* pNewMsg = CreateSrvPkg( MGChat, pGateSrv, notiRaceMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
	return;
}

///向同族玩家发送通告消息；
void CRaceManager::RaceNotiSendToRace()
{
	const char* notimsg = m_raceMaster.GetMasterNoticeMsgArr();
	if ( NULL == notimsg )
	{
		D_ERROR( "CRaceManager::RaceNotiSendToRace，待发通告消息空\n" );
		return;
	}

	if ( strlen( notimsg ) <= 0 )
	{
		D_DEBUG( "CRaceManager::RaceNotiSendToRace，无待发通告消息\n" );
		return;
	}

	MGChat notiRaceMsg;
	notiRaceMsg.sourcePlayerID.wGID = 0;
	notiRaceMsg.sourcePlayerID.dwPID = 0;
	SafeStrCpy( notiRaceMsg.strChat, notimsg );
	notiRaceMsg.chatType = CT_RACE_NOTI;
	notiRaceMsg.extInfo = m_raceType;//种族号填入额外信息；

	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("CRaceManager::SelfRaceNoti 找不到随机GateSrv，不能发放消息\n");
		return;
	}
	notiRaceMsg.lNotiPlayerID.wGID  = (unsigned short)pGateSrv->GetGateSrvID();
	notiRaceMsg.lNotiPlayerID.dwPID = 0;

	MsgToPut* pNewMsg = CreateSrvPkg( MGChat, pGateSrv, notiRaceMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );

	return;
}

///种族通告单独发给某个人（在单人进地图的情形下会用到）
void CRaceManager::RaceNotiSendToOnePerson( CPlayer* pNotiPlayer )
{
	if ( NULL == pNotiPlayer )
	{
		D_ERROR( "CRaceManager::RaceNotiSendToOnePerson，输入通知玩家空\n" );
		return;
	}

	const char* notimsg = m_raceMaster.GetMasterNoticeMsgArr();
	if ( NULL == notimsg )
	{
		D_ERROR( "CRaceManager::RaceNotiSendToOnePerson，待发通告消息空\n" );
		return;
	}

	if ( strlen( notimsg ) <= 0 )
	{
		D_DEBUG( "CRaceManager::RaceNotiSendToOnePerson，无待发通告消息\n" );
		return;
	}

	MGChat notiRaceMsg;
	notiRaceMsg.sourcePlayerID.wGID = 0;
	notiRaceMsg.sourcePlayerID.dwPID = 0;
	SafeStrCpy( notiRaceMsg.strChat, notimsg );
	notiRaceMsg.chatType = CT_RACE_NOTI_SINGLE;//单人种族通告；
	notiRaceMsg.extInfo = m_raceType;//种族号填入额外信息；

	pNotiPlayer->SendPkg<MGChat>( &notiRaceMsg );

	return;
}

void CRaceManager::NewRaceMasterBorn(unsigned int masteruid,const char* playename , const char* unionname,bool isGMcmd )
{
	if ( !playename  || !unionname )
		return;

	//族长诞生时,继承上一个族长的宣战信息
	m_raceMaster.SetRaceMasterInfo( masteruid, 5, (unsigned int)time(NULL) , m_raceMaster.GetDeclareWarInfo() );
	m_raceMaster.SetRaceMasterSalary( 100000 );

	if ( !isGMcmd )//如果不是GM指令，则选新族长后恢复组长权利,否则保持原样
		SetForbiddenAuthoriy( false );

	CPlayer* pPreRaceMaster= m_raceMaster.GetRaceMasterPlayer();
	if ( pPreRaceMaster  )//之前的族长状态回收
		m_raceMaster.NoticeRaceMasterStateChange( false );

	CPlayer* pPlayer = CPlayerManager::FindPlayerByRoleName( playename );
	if ( pPlayer )
	{
		if( IsRaceMaster( pPlayer->GetDBUID() ) )//如果是族长
		{
			SetRaceMaster( pPlayer );
			pPlayer->OnSimpleInfoChange();
			if ( pPreRaceMaster ){
				pPreRaceMaster->OnSimpleInfoChange();
			}
		}
		else
		{
			D_ERROR("被选为族长玩家%s,通过姓名寻找的UID:%d和族长的UID:%d不一致\n", playename, pPlayer->GetDBUID(), masteruid );
		}
	}
	else
	{
		D_DEBUG("%s工会的%s成为族长，但在这个mapsrv找不到该玩家(masteruid:%d)\n",unionname, playename, masteruid );
	}

	//通知所有在线的玩家,种族族长诞生
	//NoticeRaceMasterBorn( m_raceType, playename, unionname );
}

void CRaceManager::NoticeRaceMasterBorn(unsigned short racetype, const char* mastername, const char* unionname )
{
	if ( racetype != m_raceType )
	{
		D_ERROR("CRaceManager::NoticeRaceMasterBorn 通知种族不对 %d 变成了 %d\n", m_raceType, racetype );
		return;
	}

	if ( !mastername )
		return;

	if ( !unionname )
		return;

	static stringstream raceMasterBorn;
	raceMasterBorn << "新族长诞生: " << unionname << " 工会的 " << mastername << "成为新的族长";
	SystemInfoSendToRace( raceMasterBorn.str().c_str() );
	raceMasterBorn.str("");

	return;
}

///宣战信息发送；
void CRaceManager::RaceMasterDeclareWarInfoSend(char declareWarinfo )
{
	int tmpint = m_olddeclarewarinfo^declareWarinfo;
	switch( tmpint )
	{
	case 1<<1:
		{
			const char* msg = {"本种族对人类宣战,同胞们加油,让我们杀光人类吧"};
			SystemInfoSendToRace( msg );
		}
		break;
	case 1<<2:
		{
			const char* msg= {"本种族对艾卡宣战,同胞们加油,让我们杀光艾卡吧"};
			SystemInfoSendToRace( msg );
		}
		break;
	case 1<<3:
		{
			const char* msg = {"本种族对精灵宣战,同胞们加油,让我们杀光精灵吧"};
			SystemInfoSendToRace( msg );
		}
		break;
	case (1<<1)<<3:
		{
			const char* msg = {"人类对本种族宣战,同胞们加油,一起抵抗人类的入侵吧"};
			SystemInfoSendToRace( msg );
		}
		break;
	case (1<<2)<<3:
		{
			const char* msg = {"艾卡对本种族宣战,同胞们加油,一起抵抗艾卡的入侵吧"};
			SystemInfoSendToRace( msg );
		}
		break;
	case (1<<3)<<3:
		{
			const char* msg = {"精灵对本种族宣战,同胞们加油,一起抵抗精灵的入侵吧"};
			SystemInfoSendToRace( msg );
		}
		break;
	default:
		break;
	}

	return;
}

time_t CRaceManager::GetRaceMasterExpireTime()
{
	unsigned int dayofweek = 0, hour = 0, min = 0;
	CWarTaskManager::GetWarStartTime( dayofweek,hour,min );//获取攻城战每周什么时候开始
	if ( hour == 0 )
	{
		hour = 20;
		min  = 30;
	}
	else
	{
		hour = hour + 1;
	}
	

	D_DEBUG("通过CWarTaskManager::GetWarStartTime 获取攻城战时间 在每周的 星期%d  hour:%d  min:%d 时间开始\n",dayofweek, hour,min );

	if ( dayofweek > 0 )//如果攻城战的开始的前一天
		dayofweek--;
	else if( dayofweek == 0 )//如果是星期天，则宣战截至日期在星期六
		dayofweek = 6;

	D_DEBUG("所以宣战截止时间在每周 星期 %d  hour:%d  min:%d 的时间截止\n", dayofweek, hour, min );

	time_t starttime = 0, curtime = time(NULL);

	struct tm curtm = *ACE_OS::localtime( &curtime );
	int subday = dayofweek - curtm.tm_wday;//看相差几天
	if ( subday > 0 )//如果时间还没到
	{
		time_t ftime = curtime + subday * 86400;
		struct tm furtm = *ACE_OS::localtime( &ftime );
		furtm.tm_hour = hour;
		furtm.tm_min  = min;
		furtm.tm_sec  = 0;
		starttime = mktime( &furtm );

#ifdef _DEBUG
		struct tm tmp =  *ACE_OS::localtime( &starttime );
		D_DEBUG("%d 族的宣战截至日期为 %d %d %d %d\n",m_raceType , tmp.tm_mon+1, tmp.tm_mday, tmp.tm_hour, tmp.tm_min );
#endif

	}
	else if( subday < 0 )//如果已经过了
	{
		time_t ftime = curtime + ( subday + 7 )* 86400;
		struct tm furtm = *ACE_OS::localtime( &ftime );
		furtm.tm_hour = hour;
		furtm.tm_min  = min;
		furtm.tm_sec  = 0;
		starttime = mktime( &furtm );

#ifdef _DEBUG
		struct tm tmp =  *ACE_OS::localtime( &starttime );
		D_DEBUG("%d 族的宣战截至日期为 %d %d %d %d\n",m_raceType, tmp.tm_mon+1, tmp.tm_mday, tmp.tm_hour, tmp.tm_min );
#endif
	}
	else if( subday == 0 )//如果就在当天
	{
		curtm.tm_hour = hour;
		curtm.tm_min  = min;
		curtm.tm_sec  = 0;
		starttime = mktime( &curtm );

#ifdef _DEBUG
		struct tm tmp =  *ACE_OS::localtime( &starttime );
		D_DEBUG("%d 族的宣战截至日期为 %d %d %d %d\n",m_raceType, tmp.tm_mon+1, tmp.tm_mday, tmp.tm_hour, tmp.tm_min );
#endif
	}

	return starttime;
}

//当玩家进地图时
void CRaceManager::OnPlayerEnterMap(CPlayer* pPlayer )
{
	if ( !pPlayer )
		return;

	if ( pPlayer->GetRace() != m_raceType )
		return;

	unsigned int playerUID =  pPlayer->GetDBUID();
	//mSameRacePlayerMap.insert( std::pair<unsigned int, CPlayer *>( playerUID, pPlayer ) );

	if ( IsRaceMaster(playerUID) )//如果是种族族长
	{
		SetRaceMaster( pPlayer );//将玩家设置为种族族长
	}

	SendRaceMasterNoticeMsg( pPlayer , true );//玩家上线时通知族长公告
}

//当玩家离开地图时
void CRaceManager::OnPlayerLevelMap( CPlayer* pPlayer )
{
	if ( !pPlayer )
		return;

	if ( pPlayer->GetRace() != m_raceType )
		return;

	//std::map<unsigned int, CPlayer *>::iterator lter = mSameRacePlayerMap.find( pPlayer->GetDBUID() );
	//if ( lter != mSameRacePlayerMap.end() )
	//{
	//	mSameRacePlayerMap.erase( lter );
	//}

	if ( IsRaceMaster( pPlayer->GetDBUID() ) )//如果玩家是族长
	{
		OnRaceMasterLeaveThisMap();//族长玩家离开这个服务器
	}
}

//当族长离开这个地图时,需要将族长设置的信息放入数据库和通知其他服务器
void CRaceManager::OnRaceMasterLeaveThisMap()
{
	if ( !m_raceMaster.GetRaceMasterPlayer() )
		return;

	//将族长的信息通知其他服务器
	OnRaceMasterTimerInfoChange();

	//将族长的信息存入数据库
	SaveRaceMasterInfoToDB();

	//本服务器族长信息重置
	m_raceMaster.RaceMasterLeave();
}

void CRaceManager::OnRaceMasterTimerInfoChange()
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( pMaster )
	{
		MGRaceMasterTimeExpireInfoChange srvmsg;
		StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
		srvmsg.lastupdatetime = m_raceMaster.GetLastUpdateTime();
		srvmsg.noticeCount    = m_raceMaster.GetTodayNoticeCount();
		srvmsg.isGotSalary    = m_raceMaster.IsGotSalary();

		pMaster->SendPkg<MGRaceMasterTimeExpireInfoChange>( &srvmsg );
	}
}

bool CRaceManager::IsDeclareWarToOtherRace(unsigned short racetype )
{
	return m_raceMaster.IsReadyDeclareWar( racetype );
}

void CRaceManager::NotifyInvade(unsigned short invadeRace)
{
	SystemInfoSendToRace("有异族入侵");
}

void CRaceManager::NotifyAttackNpc(unsigned short invadeRace)
{
	SystemInfoSendToRace("有异族攻打NPC");
}

void CRaceManager::OnRaceMasterSetNewNoticeMessage( char* newMsgArr )
{
	if ( !newMsgArr )
		return;

	if( m_raceMaster.GetTodayNoticeCount() > 0 )//检查今天是否还有通知权限
	{
		//拷贝新的发布信息
		//ACE_OS::memcpy( m_raceMaster.GetMasterNoticeMsgArr(), newMsgArr, MAX_RACE_NOTICE_LEN );
		m_raceMaster.SetMasterNoticeMsgArr( newMsgArr );//对于种族公告发布者来说设置了两次，可能会出现先设为新再设为旧再设为新的情况？

		//当种族族长改变公告时,通知所有在线的同种族玩家
		OnRaceMasterNoticeMsgRefresh();

		//减少玩家可发言的次数
		m_raceMaster.DecTodayNoticeMsgCount();
	}
	else//如果没有更改次数了，通知client错误
	{
		SetNewNoticeMsgError( 1 );
	}
}

void CRaceManager::SetNewNoticeMsgError(int errNO )
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( !pMaster )
		return;

	MGRaceMasterSetNewMsgRst srvmsg;
	srvmsg.result = errNO;
	pMaster->SendPkg<MGRaceMasterSetNewMsgRst>( &srvmsg );
}

void CRaceManager::OnRaceMasterNoticeMsgRefresh()
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( !pMaster )
		return;

	if ( m_raceMaster.IsMsgEmpty() )
		return;

	MGNoticeRaceMasterPublicMsgChange srvmsg;
	StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
	SafeStrCpy( srvmsg.msgArr, m_raceMaster.GetMasterNoticeMsgArr() );
	srvmsg.racetype = m_raceType;
	pMaster->SendPkg<MGNoticeRaceMasterPublicMsgChange>( &srvmsg );//更新其它mapsrv上的种族公告信息；

	//通知其它srv公告消息改变的同时，第一时刻通知全服所有玩家(只有同族玩家才显示)
	RaceNotiSendToRace();

	return;
}

//当族长对其他种族宣战时
void CRaceManager::OnRaceMasterDeclareWarToOtherRace( unsigned short racetype )
{
	if ( !m_raceMaster.GetRaceMasterPlayer() )
	{
		D_ERROR("族长不在该地图,但接到了宣战消息\n");
		return;
	}

	int errNo =  0;
	do 
	{
		//同种族检测
		if( m_raceType  == racetype )
		{
			D_ERROR("不能同种族对同种族宣战, 种族编号:%d racetype 族长:%s\n", racetype, m_raceMaster.GetRaceMasterName() );
			errNo = 2;
			break;
		}

		//种族范围检测
		if ( racetype == 0 || racetype > 3 )
		{
			D_ERROR("宣战的种族错误:%d \n", racetype );
			errNo = 1;
			break;
		}

		if( time(NULL) > GetRaceMasterExpireTime() )
		{
			D_ERROR("%s宣战，但已经过了宣战时间，不能宣战了 宣战截止时间%d\n", m_raceMaster.GetRaceMasterPlayer()->GetNickName(), (unsigned int)GetRaceMasterExpireTime());
			errNo = 2;
			m_raceMaster.GetRaceMasterPlayer()->SendSystemChat("超出宣战时间，请下次再来");
			break;
		}

		//是否已经宣过战了
		if ( IsDeclareWarToOtherRace( racetype ) )
		{
			D_ERROR("已近对该种族宣战了，但还是接到了宣战该种族的消息\n" );
			errNo = 3;
			break;
		}

		SetOldDeclareWarInfo();
		//标记对其他种族宣战标志位变化
		m_raceMaster.MaskDeclareWarSign( racetype );
		D_DEBUG("种族 %d 族长%s 对 其他种族 %d 宣战\n", m_raceType,  m_raceMaster.GetRaceMasterPlayer()->GetNickName() , racetype );

		//将宣战信息通知给其他服务器
		OnRaceMasterDeclareInfoChange();

		//获取被宣战种族信息，将被宣战信息改写，发送到其他服务器
		CRaceManager* praceManger =  AllRaceMasterManagerSingle::instance()->GetRaceManagerByRace( racetype );
		if ( praceManger )
		{
			praceManger->SetOldDeclareWarInfo();

			//被别的种族宣战,改变信息
			praceManger->OnBeDecalreWarByOtherRace( m_raceType );
			D_DEBUG("种族 %d 被其他种族 %d 宣战\n", racetype,m_raceType );

			//将被宣战信息更改，发送给其他服务器
			praceManger->OnRaceMasterDeclareInfoChange();
		}
		
		return;

	} while( false );

	//如果为0，成功，则后面的参数有意义
	//如果不为0，失败，则后面的参数没有意义
	DeclareWarToOtherRaceRst( errNo, 0 ); 

	return;
}

void CRaceManager::DeclareWarToOtherRaceRst( int errNo , int declareInfo )
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( pMaster )
	{
		MGRaceMasterDeclareWarRst srvmsg;
		srvmsg.declarewarinfo = m_raceMaster.GetDeclareWarInfo();
		srvmsg.result =  errNo ;
		pMaster->SendPkg<MGRaceMasterDeclareWarRst>( &srvmsg );
	}
}


void CRaceManager::OnRaceMasterDeclareInfoChange()
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( pMaster )
	{
		MGRaceMasterDeclareWarInfoChange srvmsg;
		StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
		srvmsg.racetype =  m_raceType;
		srvmsg.declarewarinfo = m_raceMaster.GetDeclareWarInfo();
		pMaster->SendPkg<MGRaceMasterDeclareWarInfoChange>( &srvmsg );
		//第一时刻全种族通知；
		RaceMasterDeclareWarInfoSend( srvmsg.declarewarinfo );
	}
}

void CRaceManager::OnRaceMasterGetSalary()
{
	//CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	//if ( IsForbiddenAuthoriy() )
	//{
	//	pMaster->SendSystemChat("距离选新族长还有一个小时，不能使用族长功能");
	//	D_DEBUG("%s,距离选新族长还有一个小时，不能使用族长功能", pMaster->GetNickName() );
	//}
	//else
	{
		if( !m_raceMaster.IsGotSalary() )//是否已经领取过工资
		{
			int  gotmoney  = m_raceMaster.MasterGetSalary();//领取工资
			if ( gotmoney != 0 )
			{
				OnGetSalaryRst( 0, gotmoney );//通知client
			}
		}
		else
		{
			OnGetSalaryRst( 1, 0 );//如果失败,通知client
		}
	}
}

void CRaceManager::OnGetSalaryRst(int errNo , int money )
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( pMaster )
	{
		MGRaceMasterGetSalaryRst srvmsg;
		srvmsg.silverMoney = money;//双币种判断
		srvmsg.goldMoney = 0;
		srvmsg.result = errNo;

		pMaster->SendPkg<MGRaceMasterGetSalaryRst>( &srvmsg );
	}
}

void CRaceManager::OnBeDecalreWarByOtherRace(unsigned short otherrace )
{
	m_raceMaster.MaskBeDeclareWarSign( otherrace );
}

void CRaceManager::SendRaceMasterNoticeMsg(CPlayer* pPlayer , bool isOnLineNotice )
{
	if ( !pPlayer )
		return;

	//CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	//if ( pMaster )
	//{
		//MGNoticeRaceMasterPublicMsg srvmsg;//发送公告
		//ACE_OS::memset( &srvmsg, 0x0, sizeof(MGNoticeRaceMasterPublicMsg) );
		//srvmsg.newmsg.msgLen = ACE_OS::snprintf(
		//	srvmsg.newmsg.msgArr,
		//	MAX_RACE_NOTICE_LEN - 1 ,
		//	"%s",
		//	m_raceMaster.GetMasterNoticeMsgArr()
		//	);
		//srvmsg.newmsg.onlineNotice = isOnLineNotice;

		//pPlayer->SendPkg<MGNoticeRaceMasterPublicMsg>( &srvmsg );
	//}

	if ( m_raceMaster.IsMsgEmpty() )
	{
		//D_DEBUG("%s 族长公告为空,不需要发送上线通告\n", pMaster->GetNickName() );
		return;
	}

	RaceNotiSendToOnePerson( pPlayer );//向上线者发送种族通告；
}

//设置族长的存盘信息
void CRaceManager::OnRecvRaceMasterInfo(unsigned int masterUID, unsigned int todayNoticeCount, unsigned int countUpdatetime, char declarewarinfo )
{
	m_raceMaster.SetRaceMasterInfo( masterUID, todayNoticeCount,countUpdatetime, declarewarinfo );
}

//玩家是否是族长
bool CRaceManager::IsRaceMaster(unsigned int playerUID )
{
	return ( playerUID == m_raceMaster.GetMasterUID() );
}

//将玩家设置为族长
void CRaceManager::SetRaceMaster( CPlayer* pPlayer )
{
	if( m_raceMaster.SetRaceMasterPlayer( pPlayer ) )
	{
		OnRaceMasterTimeInfoExpireRefresh();//通知所有服务器，族长时间相关信息需要刷新
	}
}

//通知所有服务器刷新族长的时间信息
void CRaceManager::OnRaceMasterTimeInfoExpireRefresh()
{
	CPlayer* pPlayer = 	m_raceMaster.GetRaceMasterPlayer();
	if ( pPlayer )
	{
		MGRaceMasterTimeInfoExpire srvmsg;
		srvmsg.racetype = m_raceType;
		pPlayer->SendPkg<MGRaceMasterTimeInfoExpire>( &srvmsg );

		D_DEBUG("%d 族长计时信息过期,通知所有服务器刷新\n", m_raceType );
	}
}

//当有玩家下线，玩家切换地图时，回调这个函数
void CRaceManager::RecvNewRaceMasterInfoUpdate( unsigned int todaynoticecount, unsigned int lastupdatetime, bool isGetSalary )
{
	m_raceMaster.SetRaceMasterInfo( m_raceMaster.GetMasterUID(), todaynoticecount, lastupdatetime, m_raceMaster.GetDeclareWarInfo() );
	if ( isGetSalary )//玩家是否领取过工资
	{
		m_raceMaster.SetRaceMasterSalary( 0 );
	}
	else
		m_raceMaster.SetRaceMasterSalary( 100000 );
}


//当接到族长时间相关的信息过期时,重新刷新族长的计时信息，如果玩家在这个服务器，需要把最新数据存盘
void CRaceManager::OnRecvRaceMasterTimeInfoExpireNotice()
{
	m_raceMaster.SetRaceMasterInfo( m_raceMaster.GetMasterUID(), 5, (unsigned int)time(NULL) , m_raceMaster.GetDeclareWarInfo()  );
	m_raceMaster.SetRaceMasterSalary( 100000 );
	
	if ( m_raceMaster.GetRaceMasterPlayer() )
	{
		SaveRaceMasterInfoToDB();
	}
}


void CRaceManager::SaveRaceMasterInfoToDB()
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( pMaster )
	{
		MGUpdateRaceMasterInfoToDB srvmsg;
		StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
		srvmsg.masterUID      = m_raceMaster.GetMasterUID();
		srvmsg.declareWarInfo = m_raceMaster.GetDeclareWarInfo();
		srvmsg.isGotSalary    = m_raceMaster.IsGotSalary();
		srvmsg.lastupdatetime = m_raceMaster.GetLastUpdateTime();
		srvmsg.noticeCount    = m_raceMaster.GetTodayNoticeCount();
		srvmsg.racetype       = m_raceType;

		pMaster->SendPkg<MGUpdateRaceMasterInfoToDB>( &srvmsg );
	}
}


void CRaceManager::OnRecvRaceMasterDeclareWarInfoUpdate(char declareWarinfo )
{
	//第一时刻通知，而不是等到现在，int tmpint = m_olddeclarewarinfo^declareWarinfo;
	//switch( tmpint )
	//{
	//case 1<<1:
	//	{
	//		const char* msg = {"本种族对人类宣战,同胞们加油,让我们杀光人类吧"};
	//		SelfRaceSystemInfoSend( msg );
	//	}
	//	break;
	//case 1<<2:
	//	{
	//		const char* msg= {"本种族对艾卡宣战,同胞们加油,让我们杀光艾卡吧"};
	//		SelfRaceSystemInfoSend( msg );
	//	}
	//	break;
	//case 1<<3:
	//	{
	//		const char* msg = {"本种族对精灵宣战,同胞们加油,让我们杀光精灵吧"};
	//		SelfRaceSystemInfoSend( msg );
	//	}
	//	break;
	//case (1<<1)<<3:
	//	{
	//		const char* msg = {"人类对本种族宣战,同胞们加油,一起抵抗人类的入侵吧"};
	//		SelfRaceSystemInfoSend( msg );
	//	}
	//	break;
	//case (1<<2)<<3:
	//	{
	//		const char* msg = {"艾卡对本种族宣战,同胞们加油,一起抵抗艾卡的入侵吧"};
	//		SelfRaceSystemInfoSend( msg );
	//	}
	//	break;
	//case (1<<3)<<3:
	//	{
	//		const char* msg = {"精灵对本种族宣战,同胞们加油,一起抵抗精灵的入侵吧"};
	//		SelfRaceSystemInfoSend( msg );
	//	}
	//	break;
	//default:
	//	break;
	//}

	m_raceMaster.m_DeclareWarInfo |= declareWarinfo;
	SetOldDeclareWarInfo();

	//如果玩家在这个地图，那么通知组长改变了
	if ( m_raceMaster.GetRaceMasterPlayer() )
	{
		DeclareWarToOtherRaceRst( 0, m_raceMaster.GetDeclareWarInfo() ); 
	}
}


void CRaceManager::OnRecvRaceMasterNewNotice(const char* szNewNotice )
{
	//ACE_OS::memcpy( m_raceMaster.GetMasterNoticeMsgArr(),szNewNotice, MAX_RACE_NOTICE_LEN );
	if ( NULL == szNewNotice )
	{
		D_ERROR( "CRaceManager::OnRecvRaceMasterNewNotice，输入信息空\n" );
		return;
	}
	m_raceMaster.SetMasterNoticeMsgArr( szNewNotice );//对于种族公告发布者来说设置了两次，可能会出现先设为新再设为旧再设为新的情况？

	//应在最初地方直接全种族通知，而不用转发各mapsrv分别通知//修改后，通知在线的所有玩家
	//std::map<unsigned int, CPlayer *>::iterator lter =  mSameRacePlayerMap.begin();
	//for ( ;lter != mSameRacePlayerMap.end(); ++lter )
	//{
	//	CPlayer* pPlayer = lter->second;
	//	if ( pPlayer )
	//	{
	//		SendRaceMasterNoticeMsg( pPlayer, false );
	//	}
	//}

	if ( m_raceMaster.GetRaceMasterPlayer() )
	{
		SetNewNoticeMsgError(0);
	}
}

void CRaceManager::MonitorRaceMasterTimer(struct tm* l_tm )
{
	if ( !l_tm )
		return;

	if ( !m_raceMaster.GetRaceMasterPlayer() )
		return;

	if ( !m_raceMaster.GetLastUpdateTime() )
		return;

	time_t lastupdatetime = (time_t)m_raceMaster.GetLastUpdateTime();

	struct tm updatetime = *ACE_OS::localtime( &lastupdatetime );

	if ( updatetime.tm_year != l_tm->tm_year 
		|| updatetime.tm_mon != l_tm->tm_mon 
		|| updatetime.tm_mday != l_tm->tm_mday )
	{
		OnRaceMasterTimeInfoExpireRefresh();//当计时到期后，通知所以服务器更新玩家计时信息
	}		
}

void CRaceManager::ShowOPPlate()
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( pMaster )
	{
		MGShowRaceMasterOPPlate srvmsg;
		pMaster->SendPkg<MGShowRaceMasterOPPlate>( &srvmsg );
	}
}

void CRaceManager::ShowDeclareWarPlate()
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( pMaster )
	{
		if ( IsForbiddenAuthoriy() )
		{
			pMaster->SendSystemChat("距离选新族长还有一个小时，不能使用族长功能");
			D_DEBUG("%s,距离选新族长还有一个小时，不能使用族长功能", pMaster->GetNickName() );
		}
		else
		{
			time_t expiretime = GetRaceMasterExpireTime();//获得本次宣战的截止时间
			MGShowRaceMasterDeclareWarPlate srvmsg;
			srvmsg.showwarplate.declareWarInfo = m_raceMaster.GetDeclareWarInfo();
			srvmsg.showwarplate.endtime  = (unsigned int)expiretime;
			pMaster->SendPkg<MGShowRaceMasterDeclareWarPlate>( &srvmsg );	
		}	
	}
}

void CRaceManager::ShowSetNewPublicMsgPlate()
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( pMaster )
	{
		if ( IsForbiddenAuthoriy() )
		{
			pMaster->SendSystemChat("距离选新族长还有一个小时，不能使用族长功能");
			D_DEBUG("%s,距离选新族长还有一个小时，不能使用族长功能", pMaster->GetNickName() );
		}
		else
		{
			MGShowRaceMasterPublicMsgPlate srvmsg;
			srvmsg.showmsgplate.limitcount = m_raceMaster.GetTodayNoticeCount();
			pMaster->SendPkg<MGShowRaceMasterPublicMsgPlate>( &srvmsg );
		}
	}
}

void CRaceManager::CloseRaceMasterPlate()
{
	CPlayer* pMaster = m_raceMaster.GetRaceMasterPlayer();
	if ( pMaster )
	{
		MGCloseRaceMasterPlate srvmsg;
		srvmsg.closeplate.plateType = 0;
		pMaster->SendPkg<MGCloseRaceMasterPlate>( &srvmsg );
	}
}




/************************************************************************/
/*                                                                      */
/************************************************************************/

CRaceManager* AllRaceMasterManager::GetRaceManagerByRace(unsigned short racetype )
{
	switch( racetype )
	{
	case 1:
		return &m_humanRaceMan;
		break;
	case 2:
		return &m_akaRaceMan;
		break;
	case 3:
		return &m_elfRaceMan;
		break;
	default:
		break;
	}
	return NULL;
}

AllRaceMasterManager::AllRaceMasterManager()
{
	m_humanRaceMan.SetRaceManagerRaceType( (unsigned short)E_HUMAN );
	m_akaRaceMan.SetRaceManagerRaceType( (unsigned short)E_AIKA );
	m_elfRaceMan.SetRaceManagerRaceType( (unsigned short)E_FAIRY );

	m_lastupdatetime   = (unsigned int)time(NULL); 
}

void AllRaceMasterManager::OnPlayerEnterMap(CPlayer* pPlayer )
{
	if ( pPlayer )
	{
		unsigned short racetype = pPlayer->GetRace();
		CRaceManager* raceMan = GetRaceManagerByRace( racetype );
		if ( raceMan )
		{
			raceMan->OnPlayerEnterMap( pPlayer );
		}
	}
}

void AllRaceMasterManager::OnPlayerLeaveMap(CPlayer* pPlayer )
{
	if ( pPlayer )
	{
		unsigned short racetype = pPlayer->GetRace();
		CRaceManager* raceMan = GetRaceManagerByRace( racetype );
		if ( raceMan )
		{
			raceMan->OnPlayerLevelMap( pPlayer );
		}
	}
}

void AllRaceMasterManager::RaceMasterSetNewNoticeMsg(unsigned int racetype, char* newMsg )
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		pRaceMan->OnRaceMasterSetNewNoticeMessage( newMsg );
	}
}

void AllRaceMasterManager::OnRecvRaceMasterTimerInfoChange( unsigned int racetype , unsigned int noticecount, unsigned int lasttime, bool isGetSalary )
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		pRaceMan->RecvNewRaceMasterInfoUpdate( noticecount, lasttime, isGetSalary );
	}
}

void AllRaceMasterManager::OnRecvRaceMasterTimeExpireNotice(unsigned int racetype )
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		pRaceMan->OnRecvRaceMasterTimeInfoExpireNotice();
	}
}

void AllRaceMasterManager::RaceMasterDeclareWarToOtherRace(unsigned int racetype, unsigned int otherrace)
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		pRaceMan->OnRaceMasterDeclareWarToOtherRace( otherrace );
	}
}

void AllRaceMasterManager::RaceMasterGetSalary(unsigned int racetype )
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		pRaceMan->OnRaceMasterGetSalary();
	}
}

void AllRaceMasterManager::MontiorRaceMasterTimer()
{
	time_t curtime = time(NULL);

	if ( curtime - m_lastupdatetime > 30 )
	{
		struct tm now_tm = *ACE_OS::localtime( &curtime );

		m_humanRaceMan.MonitorRaceMasterTimer( &now_tm );
		m_akaRaceMan.MonitorRaceMasterTimer( &now_tm );
		m_elfRaceMan.MonitorRaceMasterTimer( &now_tm );

		m_lastupdatetime = curtime;
	}
}

void AllRaceMasterManager::QueryRaceMasterFromDB( CGateSrv* pGateSrv )
{
	if ( !pGateSrv )
		return;

	MGLoadRaceMasterInfoFromDB srvmsg;
	MsgToPut* pNewMsg = CreateSrvPkg( MGLoadRaceMasterInfoFromDB, pGateSrv, srvmsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
}

void AllRaceMasterManager::OnRaceMasterSetNewRaceNotice(unsigned int racetype ,char* newNotice )
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		pRaceMan->OnRecvRaceMasterNewNotice( newNotice );
	}
}

//从服务器接到族长的相关信息
void AllRaceMasterManager::OnRecvRaceMasterInfo(unsigned int racetype , unsigned int masteruid, unsigned int noticecount, unsigned int lastupdatetime ,
												unsigned int declarewarinfo, bool isGetSalary  )
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		//设置族长的　当天通知次数,当天更新时间,宣战信息等信息
		pRaceMan->OnRecvRaceMasterInfo( masteruid, noticecount, lastupdatetime ,declarewarinfo );
		if ( isGetSalary )//玩家是否领取过工资
		{
			pRaceMan->SetSalary( 0 );
		}
		else
		{
			pRaceMan->SetSalary( 100000 );
		}
	}
}

void AllRaceMasterManager::ShowRaceMasterPlate(unsigned int raceType )
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( raceType );
	if ( pRaceMan )
	{
		pRaceMan->ShowOPPlate();
	}
}

void AllRaceMasterManager::OnRecvNewRaceMasterBorn(unsigned int racetype , unsigned int masteruid , const char* playername, const char* unionname ,bool isGMCmd /* = false  */)
{
	if ( !playername || !unionname )
		return;

	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		pRaceMan->NewRaceMasterBorn( masteruid, playername , unionname ,isGMCmd );
	}
}

void AllRaceMasterManager::OnRecvRaceMasterDeclareWarInfoUpdate(unsigned int racetype , char declarewarinfo )
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		pRaceMan->OnRecvRaceMasterDeclareWarInfoUpdate( declarewarinfo );
	}
}

void AllRaceMasterManager::OnRecvForbiddenRaceMasterAuthority()
{
	m_humanRaceMan.SetForbiddenAuthoriy( true );
	m_akaRaceMan.SetForbiddenAuthoriy( true );
	m_elfRaceMan.SetForbiddenAuthoriy( true );
	D_DEBUG("服务器接到了禁止使用族长功能的消息\n");
}

void AllRaceMasterManager::OnPlayerSelectOPType(CPlayer* pPlayer, unsigned int racetype, char opType )
{
	if ( !pPlayer )
		return;

	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		if( pRaceMan->IsRaceMaster( pPlayer->GetDBUID() ) )
		{
			switch( opType )
			{
			case 0:
				pRaceMan->ShowDeclareWarPlate();
				break;
			case 1:
				pRaceMan->ShowSetNewPublicMsgPlate();
				break;
			case 2:
				pRaceMan->OnRaceMasterGetSalary();
				break;
			default:
				break;
			}
		}
		else
		{
			pPlayer->SendSystemChat("你不是族长，操作失败!");
		}
	}
}

void AllRaceMasterManager::OnPlayerDeclareWarToOtherRace(CPlayer* pPlayer, unsigned short racetype, unsigned short otherracetype )
{
	if ( !pPlayer )
		return;

	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		if ( pRaceMan->IsRaceMaster( pPlayer->GetDBUID() ) )
		{
			if ( pRaceMan->IsForbiddenAuthoriy() )
			{
				CPlayer* pMaster = pRaceMan->GetRaceMaster();
				if ( pMaster )
					pMaster->SendSystemChat("距离选新族长还有一个小时，不能使用族长功能");
			}
			else
			{
				pRaceMan->OnRaceMasterDeclareWarToOtherRace( otherracetype );
			}
		} 
		else
		{
			pPlayer->SendSystemChat("你不是族长，操作失败!");
		}
	}
}

bool AllRaceMasterManager::IsRaceMaster(CPlayer* pPlayer )
{
	if ( !pPlayer )
		return false;

	CRaceManager* pRaceMan = GetRaceManagerByRace( pPlayer->GetRace() );
	if ( pRaceMan )
	{
		return pRaceMan->IsRaceMaster( pPlayer->GetDBUID() );
	}

	return false;
}

bool AllRaceMasterManager::IsDecalreWar(unsigned short attrace, unsigned short defrace )
{
	CRaceManager* pRaceMan = GetRaceManagerByRace( attrace );
	if ( pRaceMan )
	{
		return pRaceMan->IsDeclareWarToOtherRace( defrace );
	}
	return false;
}

void AllRaceMasterManager::ResetAllRaceDeclareWarInfo()
{
	//重置所有种族的宣战信息
	D_DEBUG("该地图攻城战结束,重置所有种族的宣战信息\n");

	m_humanRaceMan.ResetDeclareWar();
	m_akaRaceMan.ResetDeclareWar();
	m_elfRaceMan.ResetDeclareWar();

	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( pGateSrv )
	{
		MGResetRaceMasterDeclareWarInfo srvmsg;
		srvmsg.racetype = RACE_HUMAN;
		MsgToPut* pNewMsg = CreateSrvPkg( MGResetRaceMasterDeclareWarInfo, pGateSrv, srvmsg );
		pGateSrv->SendPkgToGateServer( pNewMsg );

		srvmsg.racetype = RACE_ARKA;
		MsgToPut* pNewMsg2 = CreateSrvPkg( MGResetRaceMasterDeclareWarInfo, pGateSrv, srvmsg );
		pGateSrv->SendPkgToGateServer( pNewMsg2 );
		
		srvmsg.racetype = RACE_ELF;
		MsgToPut* pNewMsg3 = CreateSrvPkg( MGResetRaceMasterDeclareWarInfo, pGateSrv, srvmsg );
		pGateSrv->SendPkgToGateServer( pNewMsg3 );
	}
}

void AllRaceMasterManager::OnPlayerSetNewPublicMsg( CPlayer* pPlayer, unsigned int racetype ,char* newNotice )
{
	if ( !pPlayer )
		return;

	CRaceManager* pRaceMan = GetRaceManagerByRace( racetype );
	if ( pRaceMan )
	{
		if ( pRaceMan->IsRaceMaster( pPlayer->GetDBUID() ) )
		{
			if ( pRaceMan->IsForbiddenAuthoriy() )
			{
				CPlayer* pMaster = pRaceMan->GetRaceMaster();
				if ( pMaster )
					pMaster->SendSystemChat("距离选新族长还有一个小时，不能使用族长功能");
			}
			else
			{
				pRaceMan->OnRaceMasterSetNewNoticeMessage( newNotice );
			}
		}
		else
		{
			pPlayer->SendSystemChat("你不是族长，操作失败!");
		}
	}
}