﻿
#include "PathVerifier.h"

CPathVerifier::CPathVerifier(int iMapID, int iSizeX, int iSizeY, unsigned int** ppuiMapInfo)
: m_iMapID(iMapID), m_iSizeX(iSizeX), m_iSizeY(iSizeY), m_ppuiMapInfo(ppuiMapInfo),
  m_arrBlockQuads(NULL), m_iNumBlockQuads(0), m_iNumBlockQuadEnabled(0) 
{
}

CPathVerifier::~CPathVerifier(void)
{
}
