﻿/** @file PathVerifier.h 
@brief 地图可行走验证, 抽象类
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：Shi Yazheng
*	完成日期：2009-09-27
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef PATHVERIFIER_H
#define PATHVERIFIER_H

#include <vector>
#include <math.h>
// 抽象类
class CPathVerifier
{
public:
	/**
	*	<BR>功能说明：构造函数
	*	<BR>可访问性：公共
	*	<BR>注    释：必须在子类构造列表中调用， 本类不负责任何指针参数的内存释放
	*	@param	[in]	地图 id	
	*	@param	[in]	地图 x 方向尺寸
	*	@param	[in]	地图 y 方向尺寸
	*	@param	[in]	以二维数组表示的原始地图信息 ( 从.map 文件中读入). 
	*	@return	 
	*/ 
	CPathVerifier(int iMapID, int iSizeX, int iSizeY, unsigned int** ppuiMapInfo);
	virtual ~CPathVerifier(void);

	/**
	*	<BR>功能说明：验证地图上两点间直线是否可行走
	*	<BR>可访问性：公共
	*	<BR>注    释：纯虚函数
	*	@param	[in]	iStartX	起始点
	*	@param	[in]	iStartY
	*	@param	[in]	iEndX	结束点
	*	@param	[in]	iEndY
	*	@param	[in]	bUseExtraDDATest	当专用方法测试失败是否使用 dda 测试
	*	@return	是否可行走 
	*/ 
	virtual inline bool VerifiyWalkable(int iStartX, int iStartY, int iEndX, int iEndY, bool bUseExtraDDATest = true) = 0;

	// 使用 dda 测试
	inline bool DDAVerify(int iStartX, int iStartY, int iEndX, int iEndY);

	/**
	*	<BR>功能说明：验证两点构成的直线是否能通过 BlockQuad 碰撞
	*	<BR>可访问性：公共
	*	<BR>注    释：纯虚函数
	*	@param	[in]	iStartX	起始点
	*	@param	[in]	iStartY
	*	@param	[in]	iEndX	结束点
	*	@param	[in]	iEndY
	*	@return	是否可行走 
	*/ 
	virtual inline bool BlockQuadTest(int iStartX, int iStartY, int iEndX, int iEndY);

	// 碰撞矩形
	class CBlockQuad
	{
	public:
		CBlockQuad()
		{
		}

		CBlockQuad(int iID, int iMinX, int iMinY, int iMaxX, int iMaxY)
			: m_iID(iID), m_iMinX(iMinX), m_iMinY(iMinY), 
				m_iMaxX(iMaxX), m_iMaxY(iMaxY), m_bEnable(false)
		{
			m_iCenterX = (m_iMinX+m_iMaxX) >> 1;
			m_iCenterY = (m_iMinY+m_iMaxY) >> 1;
			m_iExtendX = m_iCenterX - m_iMinX;
			m_iExtendY = m_iCenterY - m_iMinY;
		}

		void Set(int iID, int iMinX, int iMinY, int iMaxX, int iMaxY)
		{
			m_iID = iID; 
			m_iMinX = iMinX;
			m_iMinY = iMinY; 
			m_iMaxX = iMaxX; 
			m_iMaxY = iMaxY; 
			m_bEnable = false;

			m_iCenterX = (m_iMinX+m_iMaxX) >> 1;
			m_iCenterY = (m_iMinY+m_iMaxY) >> 1;
			m_iExtendX = m_iCenterX - m_iMinX;
			m_iExtendY = m_iCenterY - m_iMinY;
		}

		void SetEnable(bool bEnable) {m_bEnable = bEnable;}

		bool Enable(void){return m_bEnable;}

		int GetID(void){return m_iID;}

		friend class CPathVerifier;
	protected:
		int m_iID;				// id
		int m_iMinX, m_iMinY;	// 矩形的范围
		int m_iMaxX, m_iMaxY;

		int m_iCenterX, m_iCenterY;	// 中心点
		int m_iExtendX, m_iExtendY;	// 
		bool m_bEnable;			// 是否开启
	};

	// 设置碰撞矩形
	inline void SetBlockQuads(int iNum, CBlockQuad* pBlockQuads);
	inline void SetBlockQuadEnable(int iID, bool bEnable);

protected:
	// 重新计算开启的 BlockQuad 数量
	inline void _RefreshNumBlockQuadEnabled();

	// 线段与aabb 的碰撞测试, 返回是否通过测试
	inline bool _BlockQuadTest(int iStartX, int iStartY, int iEndX, int iEndY, CBlockQuad* pTagBlockQuad);

	int				m_iMapID;		// 地图编号
	int				m_iSizeX;		// 地图 x 方向尺寸
	int				m_iSizeY;		// 地图 y 方向尺寸
	unsigned int**	m_ppuiMapInfo;	// 从 .map 文件读入的原始地图信息

	CBlockQuad*		m_arrBlockQuads;	// 碰撞矩形列表
	int				m_iNumBlockQuads;	// 碰撞矩形数量
	int				m_iNumBlockQuadEnabled;	// 开启的 BlockQuad 数量
	
};

#include "PathVerifier.inl"

#endif
