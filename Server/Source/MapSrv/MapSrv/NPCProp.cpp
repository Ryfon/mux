﻿#include "NPCProp.h"
#include "XmlManager.h"

CNPCProp::CNPCProp(void):m_lNPCClsID(0),m_usLevel(0),m_usMonsterRank(0),m_lExp(0),m_fSpeed(0.0f)
,m_npcType(0),m_npcSize(0),m_ucRaceAttribute(0),m_usExpLimitLevel(0),m_uiMaxHP(0),m_nHpUp(0)
,m_nHit(0),m_nPhysicsDef(0),m_nMagicDef(0),m_fPhysicDodge(0.0f),m_fMagicDodge(0.0f),m_fResistFaint(0.0f)
,m_fResistCollapse(0.0f),m_fResistBondage(0.0f),m_usSightScope(0),m_usPatrolScope(0),m_usChasingScope(0)
,m_usChasingTime(0),m_usAIType(0),m_lAIID(0),m_usScriptID(0),m_DeathScriptID(0),m_dropSetID(0),m_inherentStand(0)
,m_usRaceAttribute(0),m_nIsAggressive(0),m_bCanBeAttacked(false),m_bCanBeAttackedPvc(false),m_bIsCallAI(false)
,m_nAttackInterval(0),m_areaId(0),m_targetSet(0),isNeedNotiOtherMonster(false)
{
	StructMemSet(m_npcName,0,sizeof(m_npcName));
}

CNPCProp::~CNPCProp(void)
{
}


bool CManNPCProp::IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
{
	if ( NULL == pEle || NULL == attName )
	{
		return false;
	}
	if ( NULL == pEle->GetAttr( attName ) )
	{
		D_ERROR( "服务器配置XML文件第%d条记录属性%s失败\n", nRecord, attName );
		return false;
	}
	return true;
}


bool CManNPCProp::Init()
{
	bool bRet1 = LoadFromXML("config/npc/npc_info_n.xml");	
	bool bRet2 = LoadFromXML("config/npc/npc_info_m.xml");	
	return (bRet1 && bRet2);
}


bool CManNPCProp::LoadFromXML(const char* szFileName)
{
	TRY_BEGIN;

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(szFileName))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if( elementSet.empty() )
	{
		D_WARNING("%s为空集\n",szFileName);
		return true;
	}
	CElement* tmpEle = NULL;
	CNPCProp* pNpcProp = NULL;
	
	int nCurPos = 0;
	unsigned int rootLevel =  pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			nCurPos++;
			pNpcProp = NEW CNPCProp;
	   	 	if( ! IsXMLValValid(tmpEle, "nId", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_lNPCClsID = atoi( tmpEle->GetAttr("nId") );

			if( ! IsXMLValValid(tmpEle, "nNPCName", nCurPos) )
			{
				return false;
			}
			SafeStrCpy( pNpcProp->m_npcName, tmpEle->GetAttr("nNPCName") );

			if( ! IsXMLValValid(tmpEle, "nLevel", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_usLevel = atoi( tmpEle->GetAttr("nLevel") );
			if( ! IsXMLValValid(tmpEle, "nExp", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_lExp = atoi( tmpEle->GetAttr("nExp") );

			if( ! IsXMLValValid(tmpEle, "nAttackInterval", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_nAttackInterval = atoi( tmpEle->GetAttr("nAttackInterval"));

			if( ! IsXMLValValid(tmpEle, "nExpLimitLevel", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_usExpLimitLevel = atoi( tmpEle->GetAttr("nExpLimitLevel") );
			if( ! IsXMLValValid(tmpEle, "nMaxHp", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_uiMaxHP = atoi( tmpEle->GetAttr("nMaxHp"));
			if( ! IsXMLValValid(tmpEle, "nHpUp", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_nHpUp = atoi( tmpEle->GetAttr("nHpUp") );
			if( ! IsXMLValValid(tmpEle, "nSpeed", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_fSpeed = (float)atof( tmpEle->GetAttr("nSpeed"));
			if( ! IsXMLValValid(tmpEle, "nNPCType", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_npcType = atoi( tmpEle->GetAttr("nNPCType"));
			if( ! IsXMLValValid(tmpEle, "nMonsterRank", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_usMonsterRank = atoi( tmpEle->GetAttr("nMonsterRank"));
			if( ! IsXMLValValid(tmpEle, "nNPCSize", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_npcSize = atoi( tmpEle->GetAttr("nNPCSize"));
			if( ! IsXMLValValid(tmpEle, "nRaceAttribute", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_ucRaceAttribute = (unsigned char)atoi( tmpEle->GetAttr("nRaceAttribute"));
			if( ! IsXMLValValid(tmpEle, "mPhysicsDefence", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_nPhysicsDef = atoi( tmpEle->GetAttr("mPhysicsDefence"));
			if( ! IsXMLValValid(tmpEle, "mMagicDefence", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_nMagicDef = atoi( tmpEle->GetAttr("mMagicDefence"));
			if( ! IsXMLValValid(tmpEle, "mPhysicsJouk", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_fPhysicDodge = (float)atof( tmpEle->GetAttr("mPhysicsJouk"));
			if( ! IsXMLValValid(tmpEle, "mMagicJouk", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_fMagicDodge = (float)atof( tmpEle->GetAttr("mMagicJouk"));
			if( ! IsXMLValValid(tmpEle, "mResistFaint", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_fResistFaint = (float)atof( tmpEle->GetAttr("mResistFaint"));
	   	 	if( ! IsXMLValValid(tmpEle, "mResistCollapse", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_fResistCollapse = (float)atof( tmpEle->GetAttr("mResistCollapse"));
			if( ! IsXMLValValid(tmpEle, "mResistBondage", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_fResistBondage = (float)atof( tmpEle->GetAttr("mResistBondage"));
			if( ! IsXMLValValid(tmpEle, "nKen", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_usSightScope = atoi( tmpEle->GetAttr("nKen"));

			if( ! IsXMLValValid(tmpEle, "nRaceAttribute", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_usRaceAttribute = (unsigned short)atoi( tmpEle->GetAttr("nRaceAttribute"));

			if( ! IsXMLValValid(tmpEle, "nAIType", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_usAIType = atoi( tmpEle->GetAttr("nAIType") );
			if( ! IsXMLValValid(tmpEle, "nAIID", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_lAIID = atoi( tmpEle->GetAttr("nAIID"));
			if( ! IsXMLValValid( tmpEle, "nScriptId", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_usScriptID = atoi( tmpEle->GetAttr("nScriptId") );

			if( ! IsXMLValValid( tmpEle, "targetSet", nCurPos) )
			{
				return false;
			}
			pNpcProp->m_targetSet = atoi( tmpEle->GetAttr("targetSet") );

			/*unsigned long  m_DeathScriptID; //死亡调用脚本，只适用于BOSS级怪物(3==m_usMonsterRank)；*/
			pNpcProp->m_DeathScriptID = 0;
			if ( ( 3 == pNpcProp->m_usMonsterRank )
				|| ( 2 == pNpcProp->m_usMonsterRank )
				)
			{
				//boss怪物，且有DeathScript;
				if( IsXMLValValid( tmpEle, "DeathScript", nCurPos) )
				{
					pNpcProp->m_DeathScriptID = atoi( tmpEle->GetAttr("DeathScript") );
				}
			}

			if ( ! IsXMLValValid( tmpEle,"nDropSet", nCurPos ) )
			{
				return false;
			}
			pNpcProp->m_dropSetID = atoi( tmpEle->GetAttr("nDropSet") );

			//m_inherentStand
			if ( ! IsXMLValValid( tmpEle,"nInherentStand", nCurPos ) )
			{
				return false;
			}
			pNpcProp->m_inherentStand = atoi( tmpEle->GetAttr("nInherentStand") );

			if ( ! IsXMLValValid( tmpEle,"CanBeAttacked", nCurPos ) )
			{
				return false;
			}
			pNpcProp->m_bCanBeAttacked = (atoi( tmpEle->GetAttr("CanBeAttacked") ) != 0 );

			if ( ! IsXMLValValid( tmpEle,"CanBeAttackedPVC", nCurPos ) )
			{
				pNpcProp->m_bCanBeAttackedPvc = true;
			}
			else
			{
				pNpcProp->m_bCanBeAttackedPvc = (atoi( tmpEle->GetAttr("CanBeAttackedPVC") ) != 0 );
			}

			if ( ! IsXMLValValid( tmpEle,"IsCallAI", nCurPos ) )
			{
				return false;
			}
			pNpcProp->m_bIsCallAI = ( atoi( tmpEle->GetAttr("IsCallAI") ) != 0 );

			
			if ( ! IsXMLValValid( tmpEle,"detectable", nCurPos ) )
			{
				return false;
			}
			pNpcProp->isNeedNotiOtherMonster = ( atoi( tmpEle->GetAttr("detectable") ) != 0 );

			if ( ! IsXMLValValid( tmpEle,"nIsAggressive", nCurPos ) )
			{
				return false;
			}
			pNpcProp->m_nIsAggressive = (unsigned int)atoi( tmpEle->GetAttr("nIsAggressive")) ;
		}
		AddObject(pNpcProp); //加入配置类
	}

	return true;
	TRY_END;
	return false;
}

