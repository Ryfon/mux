﻿#pragma once

#include <map>
#include "../../Base/Utility.h"
#include "../../Base/XmlManager.h"
using namespace std;

class CBuffProp;

enum EItemSkillType
{
	HEAL,
	DAMAGE,
	BUFF,
	RELIEVE,
	ITEM_INVALIDE
};

class CItemSkill
{
public:
	CItemSkill(void);
	~CItemSkill(void);

public:
	int m_nID;					  	
	EItemSkillType m_skillType;   //道具技能的类型
	int m_damageMethod;			//0 定植 1按百分比
	int m_nNpcType;		//攻击Npc判定用的类型
	int m_nNpcRank;		//攻击npc判定用的rank
	int m_iMaxTargetLevel;	//对npc使用时可适用的最高等级
	int m_iMinTargetLevel;	//对npc使用时可适用的最低等级
	int m_iMaxVal;			//伤害 或者 恢复的最大数值
	int m_iMinVal;			//伤害 或者 恢复的最小数值
	int m_iMaxAttDist;		//使用伤害技能 最大的
	int m_iMinAttDist;		//使用伤害技能 最小的
	int m_nTarget;			//技能的作用对象
	int m_nShape;			//范围伤害技能的形状
	int m_nRange;			//范围伤害的范围
	int m_nPeriodTime;		//BUFF持续时间
	int m_nLinkID;			//LINK的
	int m_nLinkRate;		//LINK的概率
	int m_nAssistObject;	//BUFF类道具的作用对象
	int m_nCurseTime;			//item技能的动作施放时间
	int m_nPublicCdType;		//公共CD的类型
	int m_cdTime;				//此类型技能的cd时间
	int m_nHealType;			//回复技能回复的类型
	unsigned int m_targetSet;	//目标集
	CBuffProp* m_buffProp;		//LINK的BUFF属性
};


class CItemSkillManager
{
public:
	static bool LoadFromXML(const char* pszFileName);

	static CItemSkill* FindItemSkill( int skillID );

	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord );

	static void Release();

private:
	CItemSkillManager(){};
	~CItemSkillManager(){};

private:
	static map<int, CItemSkill*> m_mapItemSkill;
};
