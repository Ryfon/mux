﻿/** @file PlayerStateManager.h
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-6-24
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

/************************************************************************/
/* 管理玩家的服务器状态                                                 */
/************************************************************************/

#include <map>
#include "PlayerState.h"
#include "Activity/WarTaskManager.h"


#ifndef PLAYERSTATE_MANAGER_H
#define PLAYERSTATE_MANAGER_H

class CPlayer;
class RideTeam;

typedef enum E_PLAYERSTATE
{
	E_STATE_NORMAL = 0x0,//普通状态
	E_STATE_RIDE,//骑乘状态
	E_STATE_MAX,
}EPLAYERSTATE;

class CPlayerStateManager
{
	friend class IPlayerState;

public:
	CPlayerStateManager():m_curState(E_STATE_NORMAL),m_pPlayer(NULL),m_playerState(&m_normalState),m_bFaint(false),m_bBondage(false),m_bDunGeon(false)
	{
		InitStataManager();
	}

	~CPlayerStateManager(){}

public:

	//获取玩家的当前状态
	EPLAYERSTATE GetCurrentState(){ return m_curState; }

	//设置玩家处于骑乘队长状态
	void SetPlayerRideLeaderState();

	//设置玩家处于骑乘状态
	void SetPlayerRideAssistState( RideTeam* pTeam );

	//设置玩家处于普通状态
	void SetPlayerNormalState();

	//玩家骑乘状态消失
	void SetPlayerRideStateDisappear();

	//获取所监视的玩家指针
	CPlayer* GetAttachPlayer() { return m_pPlayer; }

	void AttachPlayer(CPlayer *pPlayer);

	//判断是否处于骑乘状态
	bool IsOnRideState() { return m_rideState.IsActive(); }

	//获取骑乘状态引用
	CRideState& GetRideState() { return  m_rideState; }

	//获取玩家组队状态
	CGroupState& GetGroupState() { return m_groupState;}
	//玩家是否处于组队状态
	bool IsGroupState() { return m_groupState.IsActive(); }

	void InitStataManager()
	{
		m_normalState.SetAttachPlayer( this );
		m_rideState.SetAttachPlayer( this );
		m_groupState.SetAttachPlayer( this );
		m_bFaint = false;
		m_bBondage = false;
		m_bDunGeon = false;
	}

	//当玩家被攻击时,在外界调用此函数
	void OnPlayerBeAttack();
	//获得组队的指针
	CGroupTeam* GetGroupTeam() { return m_groupState.GetGroupTeam(); }

	//玩家死亡时回调
	void OnPlayerDie();

	bool IsFaint()
	{
		return m_bFaint;
	}

	bool IsBondage()
	{
		return m_bBondage;
	}

	bool IsDoubleRideItem();

	//是否在副本前置区域
	bool IsInDunGeonPreArea()
	{
		return m_bDunGeon;
	}

	void SetFaint(bool bFaint)
	{
		m_bFaint = bFaint;
	}


	void SetBondage( bool bBondage )
	{
		m_bBondage = bBondage;
	}

	//设置副本区域的属性
	void SetDunGeonPreArea( bool bDunGeon )
	{
		m_bDunGeon = bDunGeon;
	}

	//是否进入了PK区域
	void SetPKArea( bool bPK )
	{
		m_bPK = bPK;
	}

	//是否进入战斗区域
	void SetFightArea( bool bFight )
	{
		m_bFight = bFight;
	}

	//是否处于安全区
	bool IsInSafeArea()
	{
		return !m_bFight;
	}

	//玩家是否能进行PVP的战斗
	bool IsCanPvP()
	{
		return m_bPK;
	}

	//玩家能否进行战斗
	bool IsCanFight();

	//玩家骑乘队长状态消失
	void SetPlayerRideLeaderStateDisappear( RideTeam* pTeam );

	//当玩家被天谴时
	void OnPlayerBeGodPunished();

private:

	//玩家骑乘队员状态消失
	void SetPlayerRideAssistStateDisappear( RideTeam* pTeam );

private:
	CPlayerStateManager& operator=( const CPlayerStateManager& );
	CPlayerStateManager( const CPlayerStateManager& );

private:
	EPLAYERSTATE m_curState;
	CPlayer* m_pPlayer;
	IPlayerState* m_playerState;
	bool m_bFaint;		//晕眩
	bool m_bBondage;		//束缚
	bool m_bDunGeon;    //是否在副本前置区域
	bool m_bPK;         //是否在PK区域
	bool m_bFight;      //是否战斗

private:
	CNormalState m_normalState;//普通状态
	CRideState m_rideState;//骑乘状态
	CGroupState m_groupState;//组队状态

};

#endif






