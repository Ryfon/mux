﻿#ifndef SPSKILLMANAGER_H
#define SPSKILLMANAGER_H

#include <map>
using namespace std;

typedef struct StrSpInfo
{
	unsigned short	usSpLevel;
	unsigned int	nextLevelExp;
}SpInfo;


class CSpSkillManager
{
public:
	CSpSkillManager(void);
	~CSpSkillManager(void);

public:
	static bool Init()
	{
		return LoadFromXML("config/character/spleveldegree.xml");
	}

	static unsigned short GetPowerPerBean()
	{
		return m_spPowerPerBean;
	}


	static unsigned int GetSpLevelExp( unsigned short usSpLevel )
	{
		map<unsigned short, unsigned int>::iterator iter = m_spLevelExp.find( usSpLevel );
		if( iter != m_spLevelExp.end() )
		{
			return iter->second;
		}
		return 0;
	}


	static void FindSpInfo( unsigned int uiCurrentSpExp, SpInfo& spInfo )
	{
		for( unsigned short usTmpLevel = 1; usTmpLevel <5 ; usTmpLevel++ )
		{
			map<unsigned short, unsigned int>::iterator iter = m_spLevelExp.find( usTmpLevel );
			if( iter != m_spLevelExp.end() )
			{
				if( uiCurrentSpExp < iter->second )
				{
					spInfo.nextLevelExp = iter->second;
					spInfo.usSpLevel = usTmpLevel;
					return;
				}
			}else{
				//D_ERROR("找不到SP Level\n");
				break;
			}
		}
		spInfo.nextLevelExp = 0;
		spInfo.usSpLevel = 0;
		return;
	}

	static unsigned int GetMaxSpLevel()
	{
		return (unsigned int)(m_spLevelExp.size() + 1);
	}

	static unsigned int GetProLuck()
	{
		return m_proLuck;
	}

	static unsigned int GetCoeLuck()
	{
		return m_coeLuck;
	}

	static void SetProLuck( unsigned int proLuck)
	{
		m_proLuck = proLuck;
	}

	static void SetCoeLuck( unsigned int coeLuck )
	{
		m_coeLuck = coeLuck;
	}
	
	static unsigned char GetDeadWearRate(void)
	{
		if(m_deadWearRate > 100)
			m_deadWearRate = m_deadWearRate % 100;

		return m_deadWearRate;
	}

private:
	static map<unsigned short, unsigned int> m_spLevelExp;			//对应每个SP等级对应的经验
	static unsigned short m_spPowerPerBean;							//每颗豆的力量
	static bool LoadFromXML(const char* szFileName);				//读取XML
	static unsigned int m_proLuck;									//幸运系统，发生幸运暴击的概率
	static unsigned int m_coeLuck;									//增加经验的比例
	static unsigned char m_deadWearRate;										//死亡耐久度损失比

private:
	static void AddLevelSpExp(unsigned short usLevel, unsigned int uiExp);
	static void SetSpPowerPerBean(unsigned short usPowerPerBean);
};


#endif
