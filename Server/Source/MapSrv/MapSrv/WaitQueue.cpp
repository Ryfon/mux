﻿#include "WaitQueue.h"
#include "PlayerManager.h"
#include "G_MProc.h"

#define MAX_MAP_PLAYER_SIZE 2800
#define MAX_PROCESS_ENTER_NUM  5	//一次轮询最多可进的个数

extern unsigned int g_maxMapSrvPlayerNum;

list<PlayerID> CWaitQueue::m_waitPlayerList;

CWaitQueue::CWaitQueue(void)
{
}

CWaitQueue::~CWaitQueue(void)
{
}

///添加一个排队者；
void CWaitQueue::AddOneWaitPlayer( const PlayerID& playerID )
{
	D_INFO( "PlayerID(%d:%d)加入排队\n", playerID.wGID, playerID.dwPID );
	m_waitPlayerList.push_back( playerID );

	//通知玩家进入排队；
	MGQueryQueueResult gateMsg;
	gateMsg.queueIndex = (UINT)m_waitPlayerList.size();
	gateMsg.reqPlayer = playerID;
	CGateSrv* pGateSrv = CManG_MProc::FindProc( playerID.wGID );
	if( NULL == pGateSrv )
	{
		D_ERROR("CWaitQueue::AddOneWaitPlayer找不到GateSrv%d\n", playerID.wGID );
		return;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( MGQueryQueueResult, pGateSrv, gateMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );

	return;
}

bool CWaitQueue::RemoveWaitPlayer( const PlayerID& quitPlayerID )		//退出的玩家ID 
{
	D_INFO( "尝试从排队中删PlayerID(%d:%d)...\n", quitPlayerID.wGID, quitPlayerID.dwPID );
	//先遍历找到PlayerID
	if( m_waitPlayerList.empty() )
	{
		return false;
	}

	for( list<PlayerID>::iterator iter = m_waitPlayerList.begin(); iter != m_waitPlayerList.end(); ++iter )
	{
		if( (*iter) == quitPlayerID )
		{
			D_INFO( "PlayerID(%d:%d)排队中删去成功\n", quitPlayerID.wGID, quitPlayerID.dwPID );
			m_waitPlayerList.erase( iter );   //从vector中删除此元素
			return true;							 //直接返回
		}
	}

	return false;
}


void CWaitQueue::RemoveWaitPlayerOnGateSrv( unsigned int gateSrvID )	//根据GID 删除对应的playerID 
{
	//先遍历找到PlayerID
	if( m_waitPlayerList.empty() )
	{
		return;
	}
	
	D_INFO( "尝试从排队中删GateSrvID为%d上的所有Player\n", gateSrvID );
	for( list<PlayerID>::iterator iter = m_waitPlayerList.begin(); iter != m_waitPlayerList.end(); )
	{
		if( (*iter).wGID == gateSrvID )
		{
			D_INFO( "PlayerID(%d:%d)排队中删去成功\n", (*iter).wGID, (*iter).dwPID );
			iter = m_waitPlayerList.erase( iter );   //从vector中删除此元素
			continue;
		}
		++iter;
	}
}


void CWaitQueue::UpdateQueueIndex()		//向队列中的等待成员发送新的等待序列
{
	MGQueryQueueResult gateMsg;

	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if ( NULL == pGateSrv )
	{
		//没有gate0;
		D_ERROR( "CWaitQueue::UpdateQueueIndex，找不到gatesrv0\n" );
		return;
	}

	UINT count = 0;
	for( list<PlayerID>::iterator iter = m_waitPlayerList.begin(); iter != m_waitPlayerList.end(); ++iter )
	{
		//暂时这样做,优化方法以后再想
		gateMsg.reqPlayer = *iter;
		gateMsg.queueIndex = ++count;

		if ( ( NULL != pGateSrv ) 
			&& ( pGateSrv->GetGateSrvID() != gateMsg.reqPlayer.wGID )
			)
		{
			pGateSrv = CManG_MProc::FindProc( gateMsg.reqPlayer.wGID );
		}

		if( NULL == pGateSrv )
		{
			D_ERROR("CWaitQueue::UpdateQueueIndex 找不到%d的GateSrv\n", gateMsg.reqPlayer.wGID );
			continue;
		}

		MsgToPut* pNewMsg = CreateSrvPkg( MGQueryQueueResult, pGateSrv, gateMsg );
		pGateSrv->SendPkgToGateServer( pNewMsg );
	}
}

///有人离开mapsrv，排队有空可进入;
void CWaitQueue::OnSomeOneLeave()
{
	if ( m_waitPlayerList.empty() )
	{
		//无排队者；
		return;
	}

	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if ( NULL == pGateSrv )
	{
		//没有gate0;
		D_ERROR( "CWaitQueue::OnSomeOneLeave，找不到gatesrv0\n" );
		return;
	}

	MsgToPut* pNewMsg = NULL;
	
	UINT playerSize = CPlayerManager::GetPlayerSize();
	if ( playerSize >= g_maxMapSrvPlayerNum )
	{
		//mapsrv上的人仍然很多，不放新人进入；
		return;
	}

	UINT allowNum = g_maxMapSrvPlayerNum - playerSize;

	MGQueueOK waitok;
	for( UINT i = 0; i<allowNum; ++i )
	{
		if( m_waitPlayerList.empty() )
		{
			break;
		}

		waitok.okPlayer = *(m_waitPlayerList.begin());
		m_waitPlayerList.pop_front();

		if ( ( NULL!=pGateSrv ) 
			&& ( waitok.okPlayer.wGID != pGateSrv->GetGateSrvID() ) 
			)
		{
			pGateSrv = CManG_MProc::FindProc( waitok.okPlayer.dwPID );
		}
		if( NULL == pGateSrv )
		{
			D_ERROR("CWaitQueue::OnSomeOneLeave() 找不到%d的GateSrv\n", waitok.okPlayer.wGID );
			continue;
		}

		pNewMsg = CreateSrvPkg( MGQueueOK, pGateSrv, waitok );
		if ( NULL != pNewMsg )
		{
			pGateSrv->SendPkgToGateServer( pNewMsg );
		}			
	}

	return;
}

void CWaitQueue::WaitQueTimeProcess()		//定时调用方法
{
	if ( m_waitPlayerList.empty() )
	{
		//无排队者；
		return;
	}

	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if ( NULL == pGateSrv )
	{
		//没有gate0;
		D_ERROR( "1:CWaitQueue::WaitQueTimeProcess，找不到gatesrv0\n" );
		return;
	}
	MsgToPut* pNewMsg = NULL;

	//前50人每次都通知，50名以后的人，间隔通知；
	MGQueryQueueResult newPos;
	unsigned int curpos = 0;
	list<PlayerID>::iterator iter = m_waitPlayerList.begin();
	for ( ; iter!=m_waitPlayerList.end(); ++iter )
	{
		if ( curpos >= 50 )
		{
			break;
		}
		newPos.reqPlayer = *iter;
		newPos.queueIndex = ++curpos;
		if ( ( NULL != pGateSrv ) 
			&& ( newPos.reqPlayer.wGID != pGateSrv->GetGateSrvID() ) 
			)
		{
			pGateSrv = CManG_MProc::FindProc( newPos.reqPlayer.dwPID );
		}
		if( NULL == pGateSrv )
		{
			D_ERROR("2:CWaitQueue::WaitQueTimeProcess() 找不到%d的GateSrv\n", newPos.reqPlayer.wGID );
			continue;
		}

		pNewMsg = CreateSrvPkg( MGQueryQueueResult, pGateSrv, newPos );
		if ( NULL != pNewMsg )
		{
			pGateSrv->SendPkgToGateServer( pNewMsg );
		}
	}

	static unsigned int deternum = 0;
	++deternum;
	if ( deternum % 2 )
	{
		//每两次通知一次排队靠后的人；
		return;
	}

	for ( ; iter!=m_waitPlayerList.end(); ++iter )
	{
		newPos.reqPlayer = *iter;
		newPos.queueIndex = ++curpos;
		if ( (deternum/2)%3 != (curpos%3) )
		{
			//每次通知队列中1/3的人；
			continue;
		}
		if ( ( NULL != pGateSrv ) 
			&& ( newPos.reqPlayer.wGID != pGateSrv->GetGateSrvID() ) 
			)
		{
			pGateSrv = CManG_MProc::FindProc( newPos.reqPlayer.dwPID );
		}
		if( NULL == pGateSrv )
		{
			D_ERROR("3:CWaitQueue::WaitQueTimeProcess() 找不到%d的GateSrv\n", newPos.reqPlayer.wGID );
			continue;
		}

		pNewMsg = CreateSrvPkg( MGQueryQueueResult, pGateSrv, newPos );
		if ( NULL != pNewMsg )
		{
			pGateSrv->SendPkgToGateServer( pNewMsg );
		}
	}

	return;
}
