﻿#pragma once
#include "ManObject.h"
#include <map>
#include "XmlManager.h"


class CNPCProp
{
	friend class CManNPCProp;
	friend class CMonster;

private:
	CNPCProp( const CNPCProp& map);
	CNPCProp& operator = ( const CNPCProp& map ); //屏蔽这两个操作；

public:
	CNPCProp(void);
	virtual ~CNPCProp(void);

public:
	unsigned long GetObjID()
	{
		return m_lNPCClsID;
	}

	bool IsCallAI()
	{
		return m_bIsCallAI;
	}

	bool IsCanBeAttacked()
	{
		return m_bCanBeAttacked;
	}

	bool IsCanBeAttackedPvc()
	{
		return m_bCanBeAttackedPvc;
	}

	unsigned short GetNpcType()
	{
		return m_npcType;
	}

//具体的属性
private:
	unsigned long m_lNPCClsID;	//NPC的clsID   
	unsigned short m_usLevel;	//NPC的等级
	unsigned short m_usMonsterRank;	//NPC的rank,3:BOSS;
	unsigned long m_lExp;		//NPC的经验值
	float m_fSpeed;	//怪物的初始速度
	unsigned short m_npcType;	//NPC的类型
	unsigned int m_npcSize;	//NPC的大小
	unsigned char m_ucRaceAttribute;	//NPC的种族
	unsigned short m_usExpLimitLevel;	//获取经验的限制等级
	unsigned int m_uiMaxHP;		//NPC初始的最大生命值
	int m_nHpUp;		//NPC的生命恢复率
	int m_nHit;			//NPC初始命中
	int m_nPhysicsDef;	//NPC初始物理防御
	int m_nMagicDef;	//NPC初始魔法防御
	float m_fPhysicDodge;	//NPC初始物理回避
	float m_fMagicDodge;	//NPC初始魔法回避
	float m_fResistFaint;		//NPC初始抗晕几率
	float m_fResistCollapse;	//NPC初始抗击倒几率
	float m_fResistBondage;		//NPC初始抗束缚几率
	int m_usSightScope;		//视野范围
	unsigned short m_usPatrolScope;		//巡逻范围
	unsigned short m_usChasingScope;	//追击范围
	unsigned short m_usChasingTime;		//追击时间
	unsigned short m_usAIType;	//AI类型
	unsigned long  m_lAIID;		//AI的ID
	unsigned long  m_usScriptID;	//绑定的脚本ID
	unsigned long  m_DeathScriptID; //死亡调用脚本，只适用于BOSS级怪物(3==m_usMonsterRank)；
	unsigned long  m_dropSetID; //怪物掉落的道具集ID
	unsigned long  m_inherentStand;//nInherentStand,固有立场:0中立，1敌对，2同盟, by dzj, 07.22;
	unsigned short m_usRaceAttribute;	//怪物种族的属性　０　人类　１　艾卡　２　精灵　　３后其他
	unsigned int m_nIsAggressive;		//是否主动攻击
	bool m_bCanBeAttacked;	//是否能被攻击
	bool m_bCanBeAttackedPvc;	//是否能被玩家攻击
	bool m_bIsCallAI;		//是否调用AI
	char m_npcName[32];		//记录NPC的姓名
	int m_nAttackInterval;	//攻击间隔
	unsigned int m_areaId;		//在攻城改变该id的功能NPC把该状态的功能NPC置成可打,结束时再置回
	unsigned int m_targetSet;	//NPC集合用于ITEMSKILL
	bool isNeedNotiOtherMonster;	//是否需要通知ENTER 和 LEAVE事件
};

class CManNPCProp : public CObjectManager<CNPCProp>
{
public:
	//初始化
	static bool Init();

	~CManNPCProp(){}

protected:
	CManNPCProp(){}

private:
	//读取xml
	static bool LoadFromXML(const char* pszFileName);

	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord );
};
