﻿/********************************************************************
	created:	2008/12/03
	created:	3:12:2008   19:11
	file:		GoodEvilControl.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#pragma  once

#include <vector>
#include <time.h>
#include "aceall.h"

class CPlayer;
class CMonster;


class CGoodEviControl
{

public:
	CGoodEviControl():m_pAttachPlayer(NULL){}
	~CGoodEviControl(){}

public:
	void AttachPlayer( CPlayer* pPlayer )
	{
		m_pAttachPlayer = pPlayer;
		CheckLoseEvilTime();
	}

	//减去善恶值
	void SubGoodEvilPoint( unsigned int evilNum );
	//增加善恶值
	void AddGoodEvilPoint( unsigned int evilNum );
	//通知善恶值改变
	void NoticeGoodEvilPointChange( int oldPoint );
	//开始流氓状态
	void StartRogueState();
	//结束流氓状态
	void EndRogueState();
	//进入恶魔计时模式
	void StartEvilTimeMode();
	//结束恶魔计时模式
	bool EndEvilTimeMode();
	//更新恶魔计时模式的时间
	bool UpdateEvilTime();
	//检查上次的恶魔时间是否已经走完了
	void CheckLoseEvilTime();
	//当杀死其他玩家时
	void OnKillerOtherPlayer( CPlayer* pKiller );
	//获取恶魔时间
	unsigned int GetEvilTime();
	//当杀死NPC时
	void OnKillNpc( CMonster* pDeadNpc );

	unsigned int GetGoodEvilControlID();

private:
	void ResetEvilTime();

	void InsertEvilTime();
private:
	CPlayer* m_pAttachPlayer;
	time_t   mEvilUpdateTime;
};


class CEvilTimeControlManager
{
	friend class ACE_Singleton<CEvilTimeControlManager, ACE_Null_Mutex>;	
public:
	CEvilTimeControlManager();

	void PushEvilTimeControl( CGoodEviControl* pControl );

	void UpdateEvilTime();

	void RemoveGoodEvilControl( unsigned int playerUID );

private:
	std::vector<CGoodEviControl *> mEvilTimeVec;
	time_t updateTime;

};
typedef ACE_Singleton<CEvilTimeControlManager, ACE_Null_Mutex> CEvilTimeControlManagerSingle;

