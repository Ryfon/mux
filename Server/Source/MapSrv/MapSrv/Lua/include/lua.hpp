﻿// lua.hpp
// Lua header files for C++
// <<extern "C">> not supplied automatically because Lua also compiles as C++

extern "C" {
#include "../LuaLib/lua.h"
#include "../LuaLib/lualib.h"
#include "../LuaLib/lauxlib.h"
}

#ifndef CLUA_RS
#define CLUA_RS

#define OWNER_TB_NAME ("owner")

///用于操作lua堆栈后恢复堆栈；
class CLuaRestoreStack
{
public:
	///构造;
	CLuaRestoreStack( lua_State* pState )
	{
		m_pState = pState;
		if ( NULL != m_pState )
		{
			m_iTop = lua_gettop (m_pState);
		}
	}

   virtual ~CLuaRestoreStack (void)
   {
      lua_settop (m_pState, m_iTop);
   }

protected:
   lua_State *m_pState;
   int m_iTop;
};
#endif// CLUA_RS
