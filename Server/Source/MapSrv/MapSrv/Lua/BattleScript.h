﻿
#ifndef BATTLIESCRIPT_H
#define BATTLIESCRIPT_H

#include "Utility.h"


#include "LuaScript.h"
#include <vector>
#include <map>
#include <fstream>
using namespace std;


//决定搞5张全局表 
//1 回调接口表
//2 攻击玩家表 p0 (第一阶段 第二阶段压入栈的元素可能会不一样)
//3 被攻击者玩家 p1 (第一阶段 第二阶段压入栈的元素可能会不一样)
//4 攻击怪物表 c0  (第一阶段 第二阶段压入栈的元素可能会不一样)
//5 被攻击怪物表 c1  (第一阶段 第二阶段压入栈的元素可能会不一样)


//脚本处在什么响应函数中
enum EReactionFunc
{
	EFnCheckBattleFirstStage = 0,
	EFnConsumeStagePvc = 1,
	EFnConsumeStagePvp = 2,
	EFnCheckPvpBattleSecondStage = 3,
	EFnCheckPveBattleSecondStage = 4,
	EFnPvpDamage = 5,
	EFnPvcDamage = 6,
	EFnCvpDamage = 7,
	EFnCvcDamage = 8,
	EFnCheckSkillSpilt = 9,
	EFnPvcDamageRebound = 10,
	EFnPvpDamageRebound = 11,
	EFnCvpDamageRebound = 12,
	EFnCvcDamageRebound = 13,
	//关于buff4个函数
	EFnAddBuffPvc = 14,
	EFnAddBuffPvp = 15,
	EFnAddBuffCvp = 16,
	EFnAddBuffCvc = 17,
	//关于加血4个函数
	EFnPvcHealNum = 18,
	EFnCvpHealNum = 19,
	EFnCvcHealNum = 20,
	EFnPvpHealNum = 21,
	EFnExlosionPvc = 22,	//自爆2个
	EFnExlosionPvp = 23,
	EFnRelievePvc = 24,		//解除技能PVC
	EFnRelievePvp = 25,		//解除技能PVP
	EFnItemDamagePvc = 26,
	EFnItemDamagePvp = 27,
	EReactionFunc_Invalid = 28,
	EFnConsumeMpStagePvc = 29,
	EFnConsumeMpStagePvp = 30,
};

class CPlayer;
class CMonster;

class CBattleScript : public CLuaScriptBase
{
public:
	CBattleScript(void);

	virtual ~CBattleScript(void);

	bool Init( const char* luaScriptFile );

	void InitComponent();		//初始化

//调用lua脚本的函数
public:
	bool FnCheckSkillSpilt( unsigned short arrRst[5] );		//获取动作分割的参数
	bool FnCheckBattleFirstStage( CPlayer* pAtkPlayer, unsigned int skillID, unsigned int& ret );		//调用LUA脚本战斗阶段第一阶段
	bool FnConsumeStagePvc( CPlayer* pAtkPlayer, unsigned int& ret );						//调用lua脚本的扣除消耗阶段（这里不扣除MP）
	bool FnConsumeStagePvp( CPlayer* pAtkPlayer, unsigned int& ret );						//调用lua脚本的扣除消耗阶段（这里不扣除MP）
	bool FnConsumeMpStagePvc( CPlayer* pAtkPlayer, unsigned int& ret );						//调用lua脚本的扣除MP阶段
	bool FnConsumeMpStagePvp( CPlayer* pAtkPlayer, unsigned int& ret );						//调用lua脚本的扣除MP阶段

	bool FnCheckPvpBattleSecondStage( CPlayer* pAtkPlayer, CPlayer* pTgtPlayer, unsigned int& ret );	//调用LUA脚本战斗第二阶段
	bool FnCheckPveBattleSecondStage( CPlayer* pAtkPlayer, CMonster* pTgtMonster, unsigned int& ret );		//调用PVC第二阶段

	//伤害4个函数
	bool FnPvpDamage( CPlayer* pAtkPlayer, CPlayer* pTgtPlayer, unsigned int skillID, vector<int>* pVecRst );		//-- PVP 计算伤害,应用攻击结果
	bool FnPvcDamage( CPlayer* pAtkPlayer, CMonster* pTgtMonster,unsigned int skillID, vector<int>* pVecRst );		//-- PVC 计算伤害,应用攻击结果
	bool FnCvpDamage( CMonster* pAtkMonster, CPlayer* pTgtPlayer, unsigned int skillID, vector<int>* pVecRst );
	bool FnCvcDamage( CMonster* pAtkMonster, CMonster* pTgtMonster, unsigned int skillID, vector<int>* pVecRst );

	//伤害反弹4个函数
	bool FnPvcDamageRebound( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst );
	bool FnPvpDamageRebound( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst );
	bool FnCvpDamageRebound( CMonster* pAtkMonster, CPlayer* pTargetPlayer, unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst );
	bool FnCvcDamageRebound( CMonster* pAtkMonster, CMonster* pTargetMonster, unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst );

	//关于buff4个函数
	bool FnAddBuffPvc( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int buffID, vector<int>* pVecRst );
	bool FnAddBuffPvp( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int buffID, vector<int>* pVecRst );
	bool FnAddBuffCvp( CMonster* pAtkMonster, CPlayer* pTargetPlayer, unsigned int buffID, vector<int>* pVecRst );
	bool FnAddBuffCvc( CMonster* pAtkMonster, CMonster* pTgtMonster, unsigned int buffID, vector<int>* pVecRst );

	//关于加血4个函数
	bool FnPvcHealNum( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int healID, vector<int>* pVecRst );
	bool FnPvpHealNum( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int healID, vector<int>* pVecRst );
	bool FnCvpHealNum( CMonster* pAtkMonster, CPlayer* pTargetPlayer, unsigned int healID, vector<int>* pVecRst );
	bool FnCvcHealNum( CMonster* pAtkMonster, CMonster* pTgtMonster, unsigned int healID, vector<int>* pVecRst );

	//关于自爆的伤害值处理
	bool FnExlosionPvc( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int explosionArg, vector<int>* pVecRst );
	bool FnExlosionPvp( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int exlosiongArg, vector<int>* pVecRst );

	//关于relieve2个
	bool FnRelievePvc( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int relieveID, vector<int>* pVecRst );
	bool FnRelievePvp( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int relieveID, vector<int>* pVecRst );

	//关于伤害道具的2个
	bool FnItemDamagePvc( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int skillID, vector<int>* pVecRst );
	bool FnItemDamagePvp( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int skillID, vector<int>* pVecRst );

	//给予一次技能流程初始的参数
	bool InitSkillProcessArg( bool isUseItem, unsigned int skillID );


private:
	bool CallScriptOnCheckSkillSpilt( unsigned short arrRst[5] );		//检测动作分割
	bool CallScriptOnFnCheckBattleFirstStage( unsigned int& ret );  //1返回值
	bool CallScriptOnFnConsumeStagePvc( unsigned int& ret );	//1返回值
	bool CallScriptOnFnConsumeStagePvp( unsigned int& ret );	//1返回值 
	bool CallScriptOnFnConsumeMpStagePvc( unsigned int& ret );	//1返回值
	bool CallScriptOnFnConsumeMpStagePvp( unsigned int& ret );	//1返回值
	
	bool CallScriptOnCheckPvpBattleSecondStage( unsigned int& ret );	//1返回值
	bool CallScriptOnCheckPvcBattleSecondStage( unsigned int& ret );	//1返回值

	//4个伤害计算的
	bool CallScriptOnFnPvpDamage( vector<int>* pVecRst );		//3返回值
	bool CallScriptOnFnPvcDamage( vector<int>* pVecRst );		//3返回值
	bool CallScriptOnFnCvpDamage( vector<int>* pVecRst );		//3 返回值
	bool CallScriptOnFnCvcDamage( vector<int>* pVecRst );		//3 返回值

	//伤害反弹4个函数
	bool CallScriptOnFnPvcDamageRebound( unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst );
	bool CallScriptOnFnPvpDamageRebound( unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst );
	bool CallScriptOnFnCvpDamageRebound( unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst );
	bool CallScriptOnFnCvcDamageRebound( unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst );

	//关于buff4个函数
	bool CallScriptOnFnAddBuffPvc( unsigned int buffID, vector<int>* pVecRst );
	bool CallScriptOnFnAddBuffPvp( unsigned int buffID, vector<int>* pVecRst );
	bool CallScriptOnFnAddBuffCvp( unsigned int buffID, vector<int>* pVecRst );
	bool CallScriptOnFnAddBuffCvc( unsigned int buffID, vector<int>* pVecRst );

	//关于加血4个函数 hp.mp,sp,battleFlag
	bool CallScriptOnFnPvcHealNum( unsigned int healID, vector<int>* pVecRst );
	bool CallScriptOnFnPvpHealNum( unsigned int healID, vector<int>* pVecRst );
	bool CallScriptOnFnCvpHealNum( unsigned int healID, vector<int>* pVecRst );
	bool CallScriptOnFnCvcHealNum( unsigned int healID, vector<int>* pVecRst );

	//关于自爆2个
	bool CallScriptOnFnExlosionPvc( unsigned int arg, vector<int>* pVecRst );
	bool CallScriptOnFnExlosionPvp( unsigned int arg, vector<int>* pVecRst );

	//关于relieve2个
	bool CallScriptOnFnRelievePvc( unsigned int relieveID, vector<int>* pVecRst );
	bool CallScriptOnFnRelievePvp( unsigned int relieveID, vector<int>* pVecRst );
	
	//关于伤害道具的2个
	bool CallScriptOnFnItemDamagePvc(  unsigned int skillID, vector<int>* pVecRst );
	bool CallScriptOnFnItemDamagePvp(  unsigned int skillID, vector<int>* pVecRst );

private:
	void SetField( lua_State* pState, const char* index, int value );   //设置table的key和value
	void SetField( lua_State* pState, int key, int value );//设置table的key和value
	bool FillFirstStageData( lua_State* pState, CPlayer* pAtkPlayer , bool isAttack=true/*原本设计只给攻击者用的，却发现被攻击者也会用这个函数，加参数作区分*/);
	bool FillSecondStagePlayer( lua_State* pState,CPlayer* pPlayer );			//设置第2.1阶段玩家
	bool FillSecondStageMonster( lua_State* pState );			//设置第2.1阶段怪物

	bool FillBattleStagep0( lua_State* pState );
	bool FillBattleStagep1( lua_State* pState );
	bool FillBattleStagem1( lua_State* pState );
	bool FillBattleStagem0( lua_State* pState );

	bool FillBuffP0( lua_State* pState );
	bool FillBuffP1( lua_State* pState );
	bool FillBuffM1( lua_State* pState );

	bool FillItemDamageP1( lua_State* pState );
	bool FillItemDamageM1( lua_State* pState );

	bool FillExplosion( lua_State* pState );
	

private:
	CBattleScript( const CBattleScript& );  //屏蔽这两个操作；
	CBattleScript& operator = ( const CBattleScript& );//屏蔽这两个操作；

public:
	//服务器需实现的扩展接口列表
	bool ExIsSkillInCD( unsigned int skillID, const ACE_Time_Value& cd);								//-- 检查技能是否在 CD 
	bool ExPlayerHaveItem( unsigned int itemID, unsigned int num);				//-- 玩家包裹中是否有物品
	bool ExPlayerConsume(unsigned int mp, unsigned int sp, unsigned int itemID, unsigned int num); //-- 玩家消耗
	bool ExAddSkillExp( unsigned int skillID, unsigned int numExp);				//-- 加技能经验
	bool ExSendAtkSysChat(const char* content);  //-- 给某玩家发送系统消息
	bool ExSendTgtSysChat(const char* content); 
	bool ExSendSysChat( const char* pContent );
	bool ExRandRate( float rate );
	bool ExSetAbsorbDamage( unsigned int damage );	//减少吸收 需要结束absorb buff
	unsigned int ExPlayerDistance( int x1, int y1, int x2, int y2 );		//算2点之间距离
	bool ExAddPlayerExp( unsigned int type, unsigned int num );
	bool ExEndTgtPlayerSheepBuff();	 //结束目标玩家的变羊状态 
	bool ExLog( const char* pContent );
	bool ExEndTgtPlayerAllDebuff();			//所有的debuff
	bool ExEndTgtPlayerControlBuff();		//控制类的技能
	bool ExRelieveTgtPlayerBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve );			//真正清除
	bool ExRelieveTgtMonsterBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum,  bool isRealRelieve );			//真正清除
	bool ExAtkHaveItem( unsigned int itemID, unsigned int num );
	bool ExTgtHaveItem( unsigned int itemID, unsigned int num );
	bool ExAtkCreateWeakBuff( unsigned int time );


//应用对象
private:
	CPlayer* m_pAtkPlayer;		//攻击玩家
	CPlayer* m_pTgtPlayer;		//目标玩家
	CMonster* m_pAtkMonster;	//攻击怪物
	CMonster* m_pTgtMonster;	//目标怪物
	unsigned int m_useSkillID;	//战斗的技能ID
	ofstream m_file;			//文件日志
	EReactionFunc m_reactFunc;	//处在哪个响应函数中
	

private:
	const PlayerID& GetCreatID( unsigned int useType );
};


class CBattleScriptManager
{
private:
	CBattleScriptManager();//屏蔽此构造;
	~CBattleScriptManager();//屏蔽此析构;
public:
	///从配置中读入脚本;
	static bool Init()
	{
		CBattleScript* pScript = NEW CBattleScript;
		if ( pScript->Init( "config/scripts/battle.lua" ) ) //FCAL_SCRIPT_NAME
		{
			m_pBattleScript = pScript;
		} else {
			delete pScript; m_pBattleScript = NULL; pScript = NULL;
			return false;
		}
		return true;
	}

	static bool Reload()
	{	
		CBattleScript* pScript = NEW CBattleScript;
		if ( pScript->Init( "config/scripts/battle.lua" ) ) //FCAL_SCRIPT_NAME
		{
			if( NULL != m_pBattleScript )
			{
				delete m_pBattleScript;m_pBattleScript = NULL;
			}
			m_pBattleScript = pScript;
		} else {
			delete pScript; pScript = NULL;
			return false;
		}
		return true;
	}
	
	//释放管理的脚本；
	static void Release()
	{ 
		if( NULL != m_pBattleScript )
		{
			delete m_pBattleScript; m_pBattleScript = NULL;
		}
	}

	
	///寻找对应职业,找到对应的脚本
	static CBattleScript* GetScript()
	{
		return m_pBattleScript;
	};

private:
	static CBattleScript* m_pBattleScript;	//战斗脚本
};




#endif //BATTLIESCRIPT_H