﻿/**
* @file MapScript.h
* @brief 定义地图脚本
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MapScript.h
* 摘    要: 定义地图脚本
* 作    者: dzj
* 开始日期: 2009.03.31
*
*/

#pragma once

#include "Utility.h"

#ifdef STAGE_MAP_SCRIPT

#include "../Lua/LuaScript.h"
#include "XmlManager.h"
#include "../../../Base/PkgProc/CliProtocol_T.h"
using namespace MUX_PROTO;


#include <vector>
#include <string>

using namespace std;
/*
--MAP_90001 
nScriptID = 90001
states, start = {}, 1 
states[0] = 
{
	name = "IDLE",
	links = function()
								return 1;	 --无条件的转换。
	        end,
}

states[1] = 
{
	name = "FIRST_STAGE",
       --关卡开始;
	OnStageInit = function()
			end,
       --玩家进入；
	OnPlayerEnter = function()
			    MuxLib.NpcShowMsg( "#110009000");	--[[这笔买卖我一定要大赚一笔！]]
	                end,
       --玩家离开；
	OnPlayerLeave = function()
			    MuxLib.NpcShowMsg( "#110009000");	--[[这笔买卖我一定要大赚一笔！]]
	                end,
       --计数器到达；
	OnCounterMeet = function( counterID )
			    MuxLib.NpcShowMsg( "#110009000");	--[[这笔买卖我一定要大赚一笔！]]
	                end,
       --计时器到达；
	OnTimerMeet = function( counterID )
		         MuxLib.NpcShowMsg( "#110009000");	--[[这笔买卖我一定要大赚一笔！]]
	              end,  
       --转换条件函数（保留）
	links = function()
		   return nil;
	        end,
}
*/

enum EMapScriptState
{
	On_Stage_Init = 0,
	On_Player_Enter = 1,
	On_Player_Leave = 2,
	On_Counter_Meet = 3,
	On_Timer_Meet = 4,
	On_DetailTimer_Meet = 5,
	On_Npc_Dead = 6,
	On_Player_Die = 7,
	Map_Script_State_Max = 8
};

class CPlayer;
class CMuxMap;
///地图脚本，修改自NPC对话脚本；
class CMapScript : public CLuaScriptBase
{
	friend class CMapScriptManager;

private:
	CMapScript(): m_ID(0),m_pScriptUser(NULL),m_pScriptMap(NULL),m_strUiInfo(""),m_callScriptState(On_Stage_Init){};
	virtual ~CMapScript (void) {};

	//从文件中装入脚本；
	bool InitMapScript( const char* luaScriptFile );


public:
	unsigned long GetID() { return m_ID; }

	inline void SetScriptUser( CPlayer* pPlayer ) { m_pScriptUser = pPlayer; };

	inline void SetScriptMap( CMuxMap* pMuxMap ) { m_pScriptMap = pMuxMap; }

	inline CPlayer* GetScriptUser()
	{
		return m_pScriptUser;
	}

	inline CMuxMap* GetScriptMap()
	{
		return m_pScriptMap;
	}

public:///以下为事件接口;

	///阶段初始化；
	bool OnStageInit( CMuxMap* pMap );

	///事件：玩家进入关卡地图；
	bool OnPlayerEnter( CMuxMap* pMap, CPlayer* pEnterPlayer );

	///事件：玩家离开关卡地图；
	bool OnPlayerLeave( CMuxMap* pMap, CPlayer* pLeavePlayer );

	///事件：计数器到达；
	bool OnCounterMeet( CMuxMap* pMap, unsigned int counterID );

	///事件：计时器到达；
	bool OnTimerMeet( CMuxMap* pMap, unsigned int timerID );
	///事件， 定时器到达
	bool OnDetailTimerMeet( CMuxMap* pMap, unsigned int timerId );
	//事件，地图上某个怪物被杀
	bool OnNpcDead( CMuxMap* pMap, CPlayer* pKiller, unsigned int monsterTypeId );
	//事件, 当玩家死亡
	bool OnPlayerDie( CMuxMap* pMap, CPlayer* diePlayer );

	bool OnActivityStart( CMuxMap* pMap, unsigned int actID  );	//活动开始 
	bool OnActivityEnd( CMuxMap* pMap, unsigned int actID );		//活动结束

	//进入 离开区域
	bool OnPlayerEnterArea( CMuxMap* pMap, CPlayer* pMovePlayer, unsigned int areaID );
	bool OnPlayerLeaveArea( CMuxMap* pMap, CPlayer* pMovePlayer, unsigned int areaID );

	//Npc首次被击
	bool OnNpcFirstBeAttacked( CMuxMap* pMap, CPlayer* pAtker, unsigned int monsterID );
	bool OnNotifyLevelMapInfo( CMuxMap* pMap, unsigned int count );

public:///以下为提供脚本回调的接口；
	//取地图实例保存的脚本用数据
	bool ExtGetMapScriptTarget( int& getVal );
	//保存地图实例脚本用数据；
	bool ExtSetMapScriptTarget( int tosetVal );

	///地图内广播；
	bool ExtBrocastMsg( const char* notimsg );

	///添加地图计数器；
	bool ExtAddCounterOfMap( unsigned int counterClsId, unsigned int counterType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount );
	///添加地图计时器；
	bool ExtAddTimerOfMap( unsigned int timerClsID, unsigned int resTime/*到期时间，单位:分钟*/ );
	///查询地图计时器
	bool ExtIsHaveTimerOfMap( unsigned int timerClsID );
	///添加定点时间计时器,固定小时分钟到的时候触发
	bool ExtAddDetailTimerOfMap( unsigned int timerId, unsigned hour, unsigned minute );
	///地图脚本通知对应玩家消息(例如进入地图的玩家，离开地图的玩家等)
	bool ExtNotiResPlayerMsg( const char* notimsg );

	///副本地图设置玩家的outmapinfo;
    bool ExtSetPlayerOutPos( unsigned short mapid, unsigned short posX, unsigned short posY, unsigned short extent );	
	///副本地图设置所有玩家的outmapinfo;
    bool ExtSetAllPlayerOutPos( unsigned short mapid, unsigned short posX, unsigned short posY, unsigned short extent );	
	///副本地图踢相应玩家；
	bool ExtKickResPlayer();	
	///副本结束(同时踢所有玩家)；
	bool ExtEndCopy();
	///设置某伪副本可进；
	bool ExtPCopyOpen();
	///设置某伪副本不可进；
	bool ExtPCopyClose();

	///调整对应玩家位置至指定点(例如进入地图的玩家)
	bool ExtSetResPlayerPos( unsigned short posX, unsigned short posY, unsigned short extent );

	///设置副本TD标记；
	bool ExtSetTDFlag();	

    ///调整对应玩家的副本阶段
	bool ExtSetResPlayerCopyStage( unsigned short copyStage );

	bool ExtSendStandardUiInfo(unsigned int infoNo );

	///NPC通知交互者显示信息；
	void ExtSetNpcShowMsg( const char* showMsg )
	{
		m_strUiInfo << "@0:";
		m_strUiInfo << showMsg; //例如:"::1001;王;李;"
	}
	///NPC设置NPC选项信息
	void ExtSetNpcOption( int nOption, const char* optionMsg )
	{
		m_strUiInfo << "@" << nOption << ":" << optionMsg;
	}
	///NPC中Item向对话玩家发送对话界面信息；
	void ExtNpcSendUiInfo();
	///NPC发送关闭对话界面指令
	void ExtNpcCloseUiInfo();
	///NPC检测是否需要执行stage转换
	void ExtCheckTransCon()
	{
		if ( NULL != m_pScriptUser )
		{
			//CallScriptCheckTransCon( m_pScriptUser );
			NewCallScriptCheckTransCon( m_pScriptUser );
		}
		return;
	}

	void ExtAttackWarEnd( unsigned int taskId );					//给脚本回调攻城战结束,去除任务编号,该任务编号由任务NPC添加
	bool ExtIsAttackWar();		//是否是攻城时间
	bool ExtCreateNpc( unsigned int npcTypeId, unsigned short posX, unsigned short posY );		//创建NPC
	bool ExtNotifyWorldChat( const char* notimsg );												//世界通知
	bool ExtDelNpc( unsigned int npcTypeId );
	bool ExtDelNpcByMapID( unsigned int mapID, unsigned int npcTypeId );
	bool ExtNotifyWarTaskState( unsigned int taskId, ETaskStatType state );				//通知拥有这个攻城任务的玩家任务完成
	bool ExtDeleteTimer( unsigned int timerId );
	bool ExtDeleteDetailTimer( unsigned int detailTimerId );	
	//根据SET中的点集随即取一个刷出定制数量的NPC
	bool ExtCreateNpcBySet( unsigned int npcTypeId, unsigned int createSet, unsigned int npcNum );
	//获取怪物最后一刀玩家种族
	unsigned int GetNpcKillerRace(); 
	//修改地图归属种族
	bool ModifyMapRace( unsigned int race );
	//Get MapRace 
	unsigned int GetMapRace();
	//开启指定NPC生成器
	bool OpenNpcCreator( unsigned int npcCreatorID );
	bool CloseNpcCreator( unsigned int npcCreatorID );
	//获取城市等级
	unsigned int GetCityLevel();
	//设置该地图物件NPC的状态
	bool SetItemNpcStatus( unsigned int itemSeq, unsigned int status );
	//修改复活点种族归属
	bool ModifyRebirthRace( unsigned int mapID, unsigned int race );
	//获取地图内计数器数字
	unsigned int GetMapCounterNum();
	//修改跳转点的种族
	bool ModifyTelpotRace( unsigned int mapID, unsigned int race );
	//创建buff	
	bool CreateBuff( unsigned int buffID, unsigned int buffTime );
	//删除  
	bool DeleteBuff( unsigned int buffID );
	//杀死当前地图某种族玩家接口
	bool KillAllPlayerByRace( unsigned int race );
	//获取当前玩家姓名
	const char* GetScriptUserName();
	//删除玩家身上的道具
	bool RemoveUserItem( unsigned int itemID, unsigned int num );
	//传送玩家
	bool SwitchMapPlayer( unsigned int race, unsigned int mapID, unsigned int posX1, unsigned int posY1, unsigned int posX2, unsigned int posY2 );
	//创建WarTimer
	bool AddWarTimer( unsigned int timerID, unsigned int leftSecond, unsigned int startInfomTime, unsigned int isClockwise, bool isInformClient, const char* timerWordInfo );
	//删除WarTimer
	bool DelWarTimer( unsigned int timerID );
	//创建WarCounter
	bool AddWarCounter( unsigned int counterID, unsigned int counterNum, const char* szCounterInfo );
	//删除warCounter
	bool DelWarCounter( unsigned int counterID );
	//获得计数器的数字
	unsigned int GetWarCounterNum( unsigned int counterID );
	//全地图通知计数器信息
	bool MapDisplayWarCounterInfo( unsigned int counterID );
	//设置计数器的数字
	bool SetWarCounterNum( unsigned int counterID, unsigned int counterNum );
	//单玩家通知计数器的数字
	bool PlayerDisplayWarCounterInfo( unsigned int counterID );
	//发送在制定玩家制定屏幕位置公告
	bool ExtShowNotice( const char* content, char position, char isglobal, unsigned int maxshowcount, unsigned int distancetime );
	//在制定屏幕位置弹出可变参数公告
	bool ExtShowParamNotice( unsigned int msgid, char position, char isglobal,unsigned int maxshowcount, unsigned int distancetime,
		unsigned int validparam,const char* param1,const char* param2, const char* param3,const char* param4,const char* param5 );

	//修改碰撞点属性
	bool ExtSetBlockQuad( unsigned int regionID, unsigned int val );
	//创建地图上玩家的BUFF
	bool CreateMapBuff( unsigned int race, unsigned int buffID, unsigned int buffTime );
	//攻城战真正开始
	bool ExtWarRealStart(void);

private:
	bool ReadScriptID()
	{
		CLuaRestoreStack rs( m_pLuaState );

		lua_getglobal( m_pLuaState, "nScriptID" );
		if ( ! lua_isnumber( m_pLuaState, -1 ) )
		{
			return false;
		}		

		m_ID = (int) lua_tonumber( m_pLuaState, -1 );

		return true;

	}

	//本函数以后可以改为同类型脚本只读一次；
	bool GetStartStage( int& nStage )
	{
		CLuaRestoreStack rs( m_pLuaState );

		lua_getglobal( m_pLuaState, "start" );
		if ( ! lua_isnumber( m_pLuaState, -1 ) )
		{
			return false;
		}		

		nStage = (int) lua_tonumber( m_pLuaState, -1 );

		return true;
	}
	
private://根据当前stage调用脚本相应函数；

	///根据当前stage调用脚本OnOption；
	bool CallScriptOnStageInit();

	///根据当前stage调用脚本OnPlayerEnter；
	bool CallScriptOnPlayerEnter();

	///根据当前stage调用脚本OnPlayerLeave；
	bool CallScriptOnPlayerLeave();

	///根据当前stage调用脚本OnCounterMeet；
	bool CallScriptOnCounterMeet( unsigned int counterID );

	///根据当前stage调用脚本OnTimerMeet；
	bool CallScriptOnTimerMeet( unsigned int timerID );

	//根据当前stage调用脚本OnDetailTimerMeet
	bool CallScriptOnDetailTimerMeet( unsigned int timerId );

	//当某个怪物死的时候调用
	bool CallScriptOnNpcDead( unsigned int npcTypeId );
	//当玩家死亡
	bool CallScriptOnPlayerDie();

	bool CallScriptOnActivityStart( unsigned int actID );
	bool CallScriptOnActivityEnd( unsigned int actID );

	bool CallScriptOnEnterArea( unsigned int areaID );
	bool CallScriptOnLeaveArea( unsigned int areaID );
	
	//NPC首次被击
	bool CallScriptOnNpcFirstBeAttacked( unsigned int monsterClsID );
	//TD 
	bool CallScriptOnNotifyLevelMapInfo( unsigned int count );

	///检测脚本当前stage的所有状态转换条件，如果需要转换，则执行转换；
	///本函数供脚本在适当时候调用；
	bool CallScriptCheckTransCon( CPlayer* pOwner );

	///检测脚本链接函数，找到转换目标执行转换,新条件转移机制；
	bool NewCallScriptCheckTransCon( CPlayer* pOwner );

	void InitComponent();   //初始化成员
	void SetMapScriptState( EMapScriptState scriptState );

private:
	unsigned long m_ID;//对话脚本ID号；
	CPlayer*      m_pScriptUser;//当前事件的触发玩家，若调用事件有对应玩家，则必须设置；
	CMuxMap*      m_pScriptMap;//当前事件的对应地图，每次调用脚本事件时必须设置；
	stringstream   m_strUiInfo;//当前脚本对话信息；
	EMapScriptState  m_callScriptState;		//由于该脚本会多次回调,需要增加是由哪个lua脚本调用的
};

class CMapScriptManager
{
private:
	//屏蔽这两个函数；
	CMapScriptManager()
	{
	}
	~CMapScriptManager();

public:
	static void Release()
	{
		for ( map<unsigned long, CMapScript*>::iterator iter = m_mapDwordFsms.begin(); 
			iter != m_mapDwordFsms.end(); ++iter )
		{
			if ( NULL != iter->second )
			{
				delete iter->second; iter->second = NULL;
			}
		}

		m_mapDwordFsms.clear();
		m_mapLuaStateFsms.clear();
	}

	///初始化管理器;
	static bool Init()
	{
		return true;
	}

public:
	///从指定路径装入脚本;
	static CMapScript* LoadScript( const char* scriptPath )
	{
		CMapScript* pMapScript = NEW CMapScript;
		bool bRet = pMapScript->InitMapScript( scriptPath );
		if( !bRet )
		{
			delete pMapScript; pMapScript = NULL;
			return NULL;
		}
		AddScript( pMapScript );
		return pMapScript;
	}
private:

	///向管理器中添加新脚本；
	static bool AddScript( CMapScript* pScript )
	{
		if ( NULL == pScript )
		{
			return false;
		}

		map<unsigned long, CMapScript*>::iterator iter = m_mapDwordFsms.find( pScript->GetID() );
		if ( iter != m_mapDwordFsms.end() )
		{
			//原来就有该脚本；
			D_WARNING( "CMapScriptManager::AddScript,重复装载脚本%d\n", pScript->GetID() );
			return true;
		} else {			
			m_mapDwordFsms.insert( pair<unsigned long, CMapScript*>( pScript->GetID(), pScript ) );
			m_mapLuaStateFsms.insert( pair<lua_State*, CMapScript*>( pScript->GetState(), pScript ) );
		}
		return true;
	}

public:
	///根据脚本ID号寻找脚本；
	static CMapScript* FindScripts( unsigned long scriptID )
	{
		TRY_BEGIN;

		map<unsigned long, CMapScript*>::iterator iter = m_mapDwordFsms.find( scriptID );
		if ( iter != m_mapDwordFsms.end() )
		{
			return iter->second;
		} else {
			//根据第一位判断脚本类型，并找到相应的脚本目录，检查是否有相应脚本文件，如果有，则载入；
			if ( scriptID < 1000 )
			{
				return NULL;
			}
			stringstream strID;
			strID.str( "" );
			strID.clear();
			//npc相关脚本；
			strID << "config/scripts/mapscripts/map" << scriptID << ".lua";

			CMapScript* pMapScript = LoadScript( strID.str().c_str() );
			strID.str( "" );
			return pMapScript;
		}

		return NULL;
		TRY_END;
		return NULL;
	}

	///根据lua state寻找脚本；
	static CMapScript* FindScripts( lua_State* inState )
	{
		if ( NULL == inState )
		{
			return NULL;
		}

		map<lua_State*, CMapScript*>::iterator iter = m_mapLuaStateFsms.find( inState );
		if ( iter != m_mapLuaStateFsms.end() )
		{
			return iter->second;
		} else {
			return NULL;
		}
	}


private:
	static map<unsigned long, CMapScript*> m_mapDwordFsms;//脚本ID号<---->脚本指针映射;
	static map<lua_State*, CMapScript*> m_mapLuaStateFsms;//lua状态<---->脚本指针映射;
};

#endif //STAGE_MAP_SCRIPT

