﻿/**
* @file ScriptExtent.cpp
* @brief 提供脚本扩展函数
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: ScriptExtent.cpp
* 摘    要: 提供脚本扩展函数
* 作    者: dzj
* 完成日期: 2008.01.12
*
*/

#include "ScriptExtent.h"
#include "FightPreScript.h"
#include "FightCalScript.h"
#include "MapScript.h"
#include "../Player/Player.h"
#include "../monster/monster.h"
#include "../GMCmdManager.h"
#include "LevelUpScript.h"
#include "BattleScript.h"
#include "../RaceManager.h"
#include "../MuxMap/mannormalmap.h"
#include "../MuxMap/manmuxcopy.h"

bool             CNormalScriptExtent::m_isvecInited = false;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
vector<ToRegFns> CNormalScriptExtent::m_vecRegFns;

bool             CAIScriptExtent::m_isvecInited = false;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
vector<ToRegFns> CAIScriptExtent::m_vecRegFns;

#ifdef STAGE_MAP_SCRIPT
bool             CMapScriptExtent::m_isvecInited = false;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
vector<ToRegFns> CMapScriptExtent::m_vecRegFns;
#endif //STAGE_MAP_SCRIPT

bool             CFightScriptExtent::m_isvecInited = false;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
vector<ToRegFns> CFightScriptExtent::m_vecRegFns;

#ifdef LEVELUP_SCRIPT
bool             CLevelUpScriptExtent::m_isvecInited = false;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
vector<ToRegFns> CLevelUpScriptExtent::m_vecRegFns;
#endif //LEVELUP_SCRIPT

bool             CBattleScriptExtent::m_isvecInited = false;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
vector<ToRegFns> CBattleScriptExtent::m_vecRegFns;

bool             CCalSecondPropertyExtent::m_isvecInited = false;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
vector<ToRegFns> CCalSecondPropertyExtent::m_vecRegFns;

///唯一注册到lua的函数实体，具体函数使用closure区分， modified from a codeproject article by dzj.(http://www.codeproject.com/KB/cpp/luaincpp.aspx)；
//org comments:
//============================================================================
// int LuaCallback
//---------------------------------------------------------------------------
// Lua C-API calling that figures out which object to hand over to
//
// Parameter   Dir      Description
// ---------   ---      -----------
// lua         IN       State variable
//
// Return
// ------
// Number of return varaibles on the stack
//
// Comments
// --------
// This is the function lua calls for each C-Function that is
// registered with lua. At the time of registering the function
// with lua, we make lua record the method "number" so we can
// know what method was actually called. The lua stack is the
// following structure:
// 0: 'this' (table)
// 1 - ...: parameters passed in
//
//============================================================================
//modified by dzj， 根据closure找到vector中对应的函数并进行调用;
//int LuaCallback( lua_State* pLuaState )
//{
//	// Locate the psudo-index for the function number
//	int iNumberIdx = lua_upvalueindex( 1 );
//	// Get the method index
//	int iMethodIdx = (int) lua_tonumber( pLuaState, iNumberIdx );//找到函数序号；
//
//	int nRetsOnStack = 0;
//	nRetsOnStack = CScriptExtent::CallFn( iMethodIdx, pLuaState );//调用相应函数；
// //   if ( lua_islightuserdata( pLuaState, 1 ) )
//	//{
//	//	//使用this调用；
//	//	int jjj = 0;
//	//} else if ( lua_istable( pLuaState, 1) ) {
//	//	//使用MuxLib:调用，应为muxlib表'table'；
//	//	int jjj = lua_type( pLuaState, 1);
//	//	lua_pushnumber( pLuaState, jjj );
//	//	const char* tmptype = lua_typename( pLuaState, jjj );
//	//	lua_pop( pLuaState, -1 );
//	//} else {
//	//	//使用MuxLib.调用, 如果没有参数，jjj为-1'no Value'；
//	//	int jjj = lua_type( pLuaState, 1);
//	//	lua_pushnumber( pLuaState, jjj );
//	//	const char* tmptype = lua_typename( pLuaState, jjj );
//	//	lua_pop( pLuaState, -1 );
//	//}
//
//	if ( nRetsOnStack < 0 )
//	{
//		//lua_pushstring( pLuaState, "LuaCallback -> Failed to call the class function" );
//		//lua_error( pLuaState );
//		D_ERROR( "不可能到此处，LUA扩展函数执行失败！" );
//	}
//
//	// Number of return variables
//	return nRetsOnStack;
//}

template < typename T_Extent >
int LuaCallbackMeta( lua_State* pLuaState )
{
	// Locate the psudo-index for the function number
	int iNumberIdx = lua_upvalueindex( 1 );
	// Get the method index
	int iMethodIdx = (int) lua_tonumber( pLuaState, iNumberIdx );//找到函数序号；

	int nRetsOnStack = T_Extent::CallFn( iMethodIdx, pLuaState );//调用相应函数；

	if ( nRetsOnStack < 0 )
	{
		//lua_pushstring( pLuaState, "LuaCallback -> Failed to call the class function" );
		//lua_error( pLuaState );
		D_ERROR( "不可能到此处，LUA扩展函数执行失败！" );
	}

	// Number of return variables
	return nRetsOnStack;
}

///初始化，注册各NPC对话脚本用扩展函数至lua；
void CNormalScriptExtent::Init( lua_State* pLuaState )
{
	//待注册函数入vector;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//注册脚本中使用的MuxLib接口函数...
	//目前不同类型脚本使用同一组接口函数，虽然某类脚本实际上只能使用这些接口函数中的一小部分
	//以后拟改为特定类型脚本只注册特定组别函数，以防止误用，并允许同名函数在不同类脚本中的物理意义不一样，
	//例如：在战斗判断脚本中只能使用战斗判断相关扩展接口而不能使用战斗计算相关脚本
	//      又如，对于名字MuxLib.GetAttackPlayerLevel，其实际注册函数将随着注册者的脚本类型不同而不同，从而最终调用不同的实现；
	if ( !m_isvecInited )
	{
		m_isvecInited = true;
		m_vecRegFns.push_back( ToRegFns( "Debug",               Debug ) );              //调试信息打印；
		m_vecRegFns.push_back( ToRegFns( "NpcShowMsg",          NpcShowMsg ) );         //置提示信息；
		m_vecRegFns.push_back( ToRegFns( "NpcShowCustomizeMsg",          NpcShowCustomizeMsg ) );         //置提示信息；
		m_vecRegFns.push_back( ToRegFns( "NpcSetOption",        NpcSetOption ) );       //置选项信息；
		m_vecRegFns.push_back( ToRegFns( "NpcSendUiInfo",       NpcSendUiInfo ) );      //向对话玩家发送对话界面信息；
		m_vecRegFns.push_back( ToRegFns( "NpcCloseUiInfo",      NpcCloseUiInfo ) );      //NPC发送关对话框消息
		m_vecRegFns.push_back( ToRegFns( "NpcCheckTransCon",    NpcCheckTransCon ) );   //检测是否需要执行转换；
		m_vecRegFns.push_back( ToRegFns( "NpcGetPlayerName",    NpcGetPlayerName ) );   //检测是否需要执行转换；
		m_vecRegFns.push_back( ToRegFns( "NpcGetPlayerSex" ,    NpcGetPlayerSex ) );    //获得玩家的性别

		m_vecRegFns.push_back( ToRegFns( "NpcQueryLevelLowerThan", NpcQueryLevelLowerThan ) );//查询：调用者等级是否小于特定值；
		m_vecRegFns.push_back( ToRegFns( "NpcQueryLevelMoreThan", NpcQueryLevelMoreThan ) );//查询：调用者等级是否大于特定值；
		m_vecRegFns.push_back( ToRegFns( "NpcQueryNeverAcceptMission", NpcQueryNeverAcceptMission ) );//查询：调用者是否从未接过特定任务；
		m_vecRegFns.push_back( ToRegFns( "NpcQueryOnMissionList", NpcQueryOnMissionList ) );//查询：指定任务在玩家的任务列表中；
		m_vecRegFns.push_back( ToRegFns( "NpcQueryIsMissionNotOnList", NpcQueryIsMissionNotOnList ) );//查询：指定任务不在玩家的任务列表中；
		m_vecRegFns.push_back( ToRegFns( "NpcQueryNotAchieveMissionTarget", NpcQueryNotAchieveMissionTarget ) );//查询：指定任务在任务列表但未达成目标
		m_vecRegFns.push_back( ToRegFns( "NpcQueryAchieveMissionTarget", NpcQueryAchieveMissionTarget ) );//查询：指定任务在任务列表达成目标
		m_vecRegFns.push_back( ToRegFns( "NpcQueryIsFinishMissionBefore", NpcQueryIsFinishMissionBefore ) );//查询：完成过任务
		m_vecRegFns.push_back( ToRegFns( "NpcQueryPackageHave", NpcQueryPackageHave ) );//查询：包裹中是否有此物体
		m_vecRegFns.push_back( ToRegFns( "NpcQueryPackageHaveNot", NpcQueryPackageHaveNot ) );//查询：包裹中是否没有此物体
		m_vecRegFns.push_back( ToRegFns( "NpcQueryPackageCanContain",NpcQueryPackageCanContain ) );//查询这个包裹是否能放入道具
		m_vecRegFns.push_back( ToRegFns( "NpcOpenStandardUI", NpcOpenStandardUI) );//通知客户端任务有关的失败信息
		m_vecRegFns.push_back( ToRegFns( "NpcCanGiveMoney" , NpcCanGiveMoney) );//NPC能否给与金钱
		m_vecRegFns.push_back( ToRegFns( "NpcQueryRideState",NpcQueryRideState) );//查询玩家是否在骑乘状态


		m_vecRegFns.push_back( ToRegFns( "NpcGiveTask", NpcGiveTask ) );                //操作：给玩家任务
		m_vecRegFns.push_back( ToRegFns( "NpcProcessTask", NpcProcessTask ) );          //操作：执行任务(将任务属性设置为已达成目标但未交还)
		m_vecRegFns.push_back( ToRegFns( "NpcFinishTask", NpcFinishTask ) );            //操作：完成任务

		m_vecRegFns.push_back( ToRegFns( "NpcAddExp",           NpcAddExp ) );          //NPC给玩家加经验；
		m_vecRegFns.push_back( ToRegFns( "NpcAddMoney",         NpcAddMoney ) );        //NPC给玩家加金钱；
		m_vecRegFns.push_back( ToRegFns( "NpcAddProperty",      NpcAddProperty ) );     //NPC给玩家加属性;
		m_vecRegFns.push_back( ToRegFns( "NpcSetProperty",      NpcSetProperty ) );     //NPC给玩家设属性;

		m_vecRegFns.push_back( ToRegFns( "NpcTransPlayerToCopy",  NpcTransPlayerToCopy ) );//操作：让玩家进指定副本；
		m_vecRegFns.push_back( ToRegFns( "NpcTransPlayerToPCopy", NpcTransPlayerToPCopy ) );//操作：让玩家进指定伪副本；
		m_vecRegFns.push_back( ToRegFns( "NpcIsPCopyOpen",        NpcIsPCopyOpen ) );//查询：玩家是否可进指定伪副本；

		m_vecRegFns.push_back( ToRegFns( "NpcAddPlayerCounter", NpcAddPlayerCounter ) );//NPC给玩家身上加计数器；
		m_vecRegFns.push_back( ToRegFns( "NpcGiveItem",         NpcGiveItem ) );        //NPC给玩家道具；
		m_vecRegFns.push_back( ToRegFns( "NpcRemoveItem",       NpcRemoveItem ) );      //NPC取走玩家道具；
		m_vecRegFns.push_back( ToRegFns( "NpcRemoveScriptItem", NpcRemoveScriptItem) ); //NPC取走触发脚本的道具
		m_vecRegFns.push_back( ToRegFns( "NpcRefreshMonster",   NpcRefreshMonster ) );  //NPC在特定位置内刷特定数量怪物；
		m_vecRegFns.push_back( ToRegFns( "NpcShowPlateCmd",     NpcShowPlateCmd ) );    //NPC指令客户端弹出托盘；
		m_vecRegFns.push_back( ToRegFns( "NpcGiveSkill",        NpcGiveSkill) );        //NPC增加技能
		m_vecRegFns.push_back( ToRegFns( "NpcShowEmailCmd" ,    NpcShowEmailCmd ) );    //NPC显示邮件界面
		m_vecRegFns.push_back( ToRegFns( "NpcShowProtectPlateCmd", NpcShowProtectPlateCmd ) );//NPC显示保护保护道具托盘
		m_vecRegFns.push_back( ToRegFns( "NpcSendSysMail",      NpcSendSysMail) );      //NPC发送邮件给玩家
		m_vecRegFns.push_back( ToRegFns( "NpcShowStorage",      NpcShowStorage) );      //NPC弹出仓库界面
		m_vecRegFns.push_back( ToRegFns( "NpcGiveItemRandomLevel", NpcGiveItemRandomLevel) );//NPC给随机等级的装备
		m_vecRegFns.push_back( ToRegFns( "NpcBeginOLTest",      NpcBeginOLTest ) );//NPC开始答题系统
		m_vecRegFns.push_back( ToRegFns( "NpcIsHaveSkill",      NpcIsHaveSkill) );//玩家是否含有对应编号的技能
		m_vecRegFns.push_back( ToRegFns( "NpcRunDropSet",       NpcRunDropSet) );//运行掉落集

		m_vecRegFns.push_back( ToRegFns( "NpcShowShop",        NpcShowShop) );          //NPC显示NPC商店    

		m_vecRegFns.push_back( ToRegFns( "NpcSwitchMap",       NpcSwitchMap) );         //指令玩家跳转地图;

		m_vecRegFns.push_back( ToRegFns( "NpcCreatePlayerNpc",  NpcCreatePlayerNpc ) ); //NPC指令创建一个跟随玩家的NPC；	
		m_vecRegFns.push_back( ToRegFns( "NpcCheckPlayerNpc",   NpcCheckPlayerNpc ) );  //NPC指令创建一个跟随玩家的NPC；	
		m_vecRegFns.push_back( ToRegFns( "NpcDelPlayerNpc",     NpcDelPlayerNpc ) );    //NPC删去跟随玩家的NPC；	

		m_vecRegFns.push_back( ToRegFns( "NpcQueryIntelligenceMoreThan",  NpcQueryIntelligenceMoreThan ) ); //玩家智能检测，大于
		m_vecRegFns.push_back( ToRegFns( "NpcQueryIntelligenceLessThan",  NpcQueryIntelligenceLessThan ) ); //玩家智能检测，小于

		m_vecRegFns.push_back( ToRegFns( "NpcQueryIntelligenceLessThan",  NpcQueryIntelligenceLessThan ) ); //玩家智能检测，小于

		m_vecRegFns.push_back( ToRegFns( "NpcQueryPropertyEqual",  NpcQueryPropertyEqual ) ); //玩家智能检测，小于
		m_vecRegFns.push_back( ToRegFns( "NpcQueryPropertyLowerThan",  NpcQueryPropertyLowerThan ) ); //玩家智能检测，小于
		m_vecRegFns.push_back( ToRegFns( "NpcQueryPropertyMoreThan",  NpcQueryPropertyMoreThan ) ); //玩家智能检测，小于
		m_vecRegFns.push_back( ToRegFns( "NpcShowPunishPlayerInfo", NpcShowPunishPlayerInfo ) );	//显示天谴玩家位置  
		m_vecRegFns.push_back( ToRegFns( "NpcChatNpcDisappear", NpcChatNpcDisappear ) );	//显示天谴玩家位置 
		m_vecRegFns.push_back( ToRegFns( "NpcDisplayIssuedTask", NpcDisplayIssuedTask ) );	//显示工会已发布任务
		m_vecRegFns.push_back( ToRegFns( "NpcSelectUnionCmd", NpcSelectUnionCmd ) );	//工会NPC对话中选择命令
		m_vecRegFns.push_back( ToRegFns( "NpcHaveIssuedUnionTask", NpcHaveIssuedUnionTask ) );	//检查工会是否发布任务
		m_vecRegFns.push_back( ToRegFns( "NpcQueryIsUnionLeader", NpcQueryIsUnionLeader ) );//检查是否是公会会长
		m_vecRegFns.push_back( ToRegFns( "NpcQueryUnionLevel", NpcQueryUnionLevel ) );//查询工会等级
		m_vecRegFns.push_back( ToRegFns( "NpcPetSkillLearn", NpcPetSkillLearn ) );	//宠物技能学习/*AUTOMATIC_ATTACK_MASK:2,AUTOMATIC_USE_DRUGS_MASK:4,AUTOMATIC_PICK_MASK:8,WEAPON_LEVELUP_MASK:16,OFFLINE_LEVELUP_MASK:32*/；
		m_vecRegFns.push_back( ToRegFns( "NpcSkillLearn", NpcSkillLearn ) );	//学习技能
		m_vecRegFns.push_back( ToRegFns( "NpcChatSysDialog", NpcChatSysDialog ) );	//显示npc对话系统对话框
		m_vecRegFns.push_back( ToRegFns( "NpcIsEvil", NpcIsEvil ) );	//显示npc对话系统对话框
		m_vecRegFns.push_back( ToRegFns( "NpcRandNumber", NpcRandNumber ) );	//显示npc对话系统对话框

		m_vecRegFns.push_back( ToRegFns( "NpcGivePunishTask", NpcGivePunishTask ) );	//给予天谴任务
		m_vecRegFns.push_back( ToRegFns( "NpcIsInPunishment", NpcIsInPunishment ) );	//是否在天谴活动中,接天谴任务的先置条件
		m_vecRegFns.push_back( ToRegFns( "NpcIsNotHavePunishTask", NpcIsNotHavePunishTask ) );	//是否接过天谴任务
		m_vecRegFns.push_back( ToRegFns( "NpcIsPunishTaskAchieve", NpcIsPunishTaskAchieve ) );	//天谴任务是否完成
		m_vecRegFns.push_back( ToRegFns( "NpcGetPunishCounter", NpcGetPunishCounter ) );	//获取天谴杀人计数器
		m_vecRegFns.push_back( ToRegFns( "NpcGetPunishTargetName", NpcGetPunishTargetName ) );	//获取天谴目标姓名
		m_vecRegFns.push_back( ToRegFns( "NpcAddRacePrestige", NpcAddRacePrestige ) );			//增加种族声望
		m_vecRegFns.push_back( ToRegFns( "NpcFinishPunishTask", NpcFinishPunishTask ) );			//结束天谴任务
		m_vecRegFns.push_back( ToRegFns( "NpcCreateBuff", NpcCreateBuff ) );			//结束天谴任务	
		m_vecRegFns.push_back( ToRegFns( "NpcSaveRebirthPos", NpcSaveRebirthPos ) );//保存复活点

#ifdef NEW_NPC_INTERFACE
		m_vecRegFns.push_back( ToRegFns( "NpcIsInUnion", NpcIsInUnion ) );			//是否在工会中
		m_vecRegFns.push_back( ToRegFns( "NpcIsUnionLeader", NpcIsUnionLeader ) );			//是否工会盟主
		m_vecRegFns.push_back( ToRegFns( "NpcIsInTeam",  NpcIsInTeam ) );		//3 判断是否为组队状态	
		m_vecRegFns.push_back( ToRegFns( "NpcIsTeamCaptain", NpcIsTeamCaptain ) );			//4 判断是否为队长
		m_vecRegFns.push_back( ToRegFns( "NpcTeamNumberMoreThan", NpcTeamNumberMoreThan ) );			//5 队伍人数大于
		m_vecRegFns.push_back( ToRegFns( "NpcTeamNumberLowerThan", NpcTeamNumberLowerThan ) );			//5 队伍人数小雨
		m_vecRegFns.push_back( ToRegFns( "NpcTeamNumberEqual", NpcTeamNumberEqual ) );			//5 队伍人数等于
		m_vecRegFns.push_back( ToRegFns( "NpcFriendNumberMoreThan", NpcFriendNumberMoreThan ) );		//6 好友人数大于
		m_vecRegFns.push_back( ToRegFns( "NpcFriendNumberLowerThan", NpcFriendNumberLowerThan ) );		//6 好友人数小于
		m_vecRegFns.push_back( ToRegFns( "NpcFrinedNumberEqual", NpcFrinedNumberEqual ) );		//6 好友人数等于
		m_vecRegFns.push_back( ToRegFns( "NpcFoeNumberMoreThan", NpcFoeNumberMoreThan ) );		//7 仇人人数大于
		m_vecRegFns.push_back( ToRegFns( "NpcFoeNumberLowerThan", NpcFoeNumberLowerThan ) );		//7 仇人人数小于
		m_vecRegFns.push_back( ToRegFns( "NpcFoeNumberEqual", NpcFoeNumberEqual ) );		//7 仇人人数等于
#endif //NEW_NPC_INTERFACE

#ifdef ANTI_ADDICTION
		m_vecRegFns.push_back( ToRegFns( "NpcIsTriedOrAddictionState", NpcIsTriedOrAddictionState ) );			//是否在沉迷或者疲劳状态
#endif //ANTI_ADDICTION

		m_vecRegFns.push_back( ToRegFns( "NpcCreateNpcByChatTarget", NpcCreateNpcByChatTarget ) );			//根据对话NPC位置生产怪物
		m_vecRegFns.push_back( ToRegFns( "NpcAddMark", NpcAddMark ) );			//添加标记
		m_vecRegFns.push_back( ToRegFns( "NpcCheckMark", NpcCheckMark ) );			//检查标记
		m_vecRegFns.push_back( ToRegFns( "NpcCheckExTime", NpcCheckExTime ) );			//检查时间
		m_vecRegFns.push_back( ToRegFns( "NpcCheckTime", NpcCheckTime ) );			//比较2个时间
		m_vecRegFns.push_back( ToRegFns( "NpcShowRaceMasterPlate",NpcShowRaceMasterPlate ) );//弹出族长界面
		m_vecRegFns.push_back( ToRegFns( "NpcCheckMapSwitchRace",NpcCheckMapSwitchRace ) );//判断地图的跳转点归属是否和指定种族一致
		m_vecRegFns.push_back( ToRegFns( "NpcCanGiveNpcFollowTask", NpcCanGiveNpcFollowTask ) );//玩家是否能接受npc跟随任务

		m_vecRegFns.push_back( ToRegFns( "NpcShowNotices", NpcShowNotices) );//弹出固定提示
		m_vecRegFns.push_back( ToRegFns( "NpcShowParamsNotices", NpcShowParamsNotices) );//弹出可变参数的提示

		m_vecRegFns.push_back( ToRegFns( "NpcGetPlayerPosX", NpcGetPlayerPosX) );
		m_vecRegFns.push_back( ToRegFns( "NpcGetPlayerPosY", NpcGetPlayerPosY) );
		m_vecRegFns.push_back( ToRegFns( "NpcGetPlayerMapID",NpcGetPlayerMapID) );
		m_vecRegFns.push_back( ToRegFns( "NpcPlayerCallMonster",NpcPlayerCallMonster) );
		m_vecRegFns.push_back( ToRegFns( "NpcPlayerRandMove", NpcPlayerRandMove) );
		m_vecRegFns.push_back( ToRegFns( "NpcShowAuctionPlate",NpcShowAuctionPlate) );
	}
	
	//...注册各脚本中使用的MuxLib接口函数
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	CLuaRestoreStack rs( pLuaState );//准备恢复堆栈;

	//新函数表；
	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "MuxLib" );//置全局变量"MuxLib"为新表；

	lua_getglobal( pLuaState, "MuxLib" );//push表"MuxLib"；
	//注册向量中的函数至MuxLib表；
	ToRegFns* pToReg = NULL;
	for ( vector<ToRegFns>::iterator tmpiter=m_vecRegFns.begin();
		tmpiter!=m_vecRegFns.end(); ++tmpiter )
	{
		pToReg = &(*tmpiter);
		lua_pushstring( pLuaState, pToReg->fnName );
		lua_pushnumber( pLuaState, (lua_Number)(tmpiter-m_vecRegFns.begin()) );//closure注册为vec中的序号；
		lua_pushcclosure( pLuaState, LuaCallbackMeta<CNormalScriptExtent>, 1);
		lua_settable( pLuaState, -3 );//MuxLib[pToReg->fnName] = LuaCallback(with closure=本项vec序号)；
	}

	return;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///NPC对话脚本扩展...

/**
//NPC对话扩展函数定义通用部分；
//1、判断该pState是否对应一个CNormalScript脚本，如果不是，则出错返回，
//   即：必须在AI脚本中调用，且调用该脚本者必须为CNormalScript对象;
*/
#define NPCNORMAL_EXT_FUNPRE \
	if ( NULL == pState )\
    {\
	    return 0;\
    }\
	CNormalScript* pScript = CNormalScriptManager::FindScripts( pState );\
	if ( NULL == pScript  )\
	{\
		return 0;\
	}\

///脚本扩展，用于调试；
int CNormalScriptExtent::Debug( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;	
//#ifndef _DEBUG
//	ACE_UNUSED_ARG( pState );
//	return 0;
//#else //_DEBUG
	if ( lua_isstring( pState, -1 ) )
	{
		const char* debugstr = lua_tostring( pState, -1 );
		D_DEBUG( "normal脚本%d调试信息:%s\n", pScript->GetID(), debugstr );
	} else {
		D_DEBUG( "normal脚本%d调试信息输入参数错误\n", pScript->GetID() );
	}

	return 0;
//#endif //_DEBUG
}

///NPC通知交互者显示信息；
int CNormalScriptExtent::NpcShowMsg( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;	
	if ( lua_isstring( pState, -1 ) )
	{
		const char* showMsg = lua_tostring( pState, -1 );
		pScript->ExtSetNpcShowMsg( showMsg );
	} else {
		D_ERROR( "normal脚本%d调用NpcShowMsg时，输入参数错", pScript->GetID() );
	}
	return 0;
}


//NPC通知自定义的显示消息
int CNormalScriptExtent::NpcShowCustomizeMsg( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;	
	if ( lua_isstring( pState, -1 ) )
	{
		const char* showMsg = lua_tostring( pState, -1 );
		pScript->ExtSetNpcShowCustomizeMsg( showMsg );
	} else {
		D_ERROR( "normal脚本%d调用NpcShowCustomizeMsg时，输入参数错", pScript->GetID() );
	}
	return 0;
}

///NPC设置NPC选项信息
int CNormalScriptExtent::NpcSetOption( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if ( lua_isstring(pState, -1) && lua_isnumber(pState, -2) )
	{
		int nOption = (int) ( lua_tonumber( pState, -2 ) );
		const char* optionMsg = lua_tostring( pState, -1 );
		pScript->ExtSetNpcOption( nOption, optionMsg );
	} else {
		D_ERROR( "normal脚本%d调用NpcSetOption时，输入参数错", pScript->GetID() );
	}
	return 0;
}

///NPC向对话玩家发送对话界面信息；
int CNormalScriptExtent::NpcSendUiInfo( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	pScript->ExtNpcSendUiInfo();
	return 0;
}

///NPC发送关对话框消息
int CNormalScriptExtent::NpcCloseUiInfo( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	pScript->ExtNpcCloseUiInfo();
	return 0;
}

///NPC检测是否需要执行stage转换
int CNormalScriptExtent::NpcCheckTransCon( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	pScript->ExtCheckTransCon();
	return 0;
}

///NPC对话脚本取聊天玩家的名字
int CNormalScriptExtent::NpcGetPlayerName( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	lua_pushstring( pState, pScript->ExtGetPlayerName() );
	return 1;
}

///查询：调用者等级是否小于特定值；
int CNormalScriptExtent::NpcQueryLevelLowerThan( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryLevelLowerThan时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int checkLevel = (unsigned int) ( lua_tonumber( pState, -1 ) );
	bool isLarger = pScript->ExtQueryPlayerLevelLowerThan( checkLevel );

	lua_pushboolean( pState, isLarger );
	return 1;
}
///查询：调用者等级是否大于特定值；
int CNormalScriptExtent::NpcQueryLevelMoreThan( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryLevelMoreThan时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int checkLevel = (unsigned int) ( lua_tonumber( pState, -1 ) );
	bool isLarger = pScript->ExtQueryPlayerLevelMoreThan( checkLevel );
	
	lua_pushboolean( pState, isLarger );
	return 1;
}

///查询：调用者是否从未接过特定任务；
int CNormalScriptExtent::NpcQueryNeverAcceptMission( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryNeverAcceptMission时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int taskId = (unsigned int) ( lua_tonumber( pState, -1 ) );
	bool isFufil = pScript->ExtQueryNeverAcceptTask( taskId );
	
	lua_pushboolean( pState, isFufil );
	return 1;
}

///查询：指定任务在玩家的任务列表中；
int CNormalScriptExtent::NpcQueryOnMissionList( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryOnMissionList时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int taskId = (unsigned int) ( lua_tonumber( pState, -1 ) );
	bool isFufil = pScript->ExtQueryOnMissionList( taskId );
	
	lua_pushboolean( pState, isFufil );
	return 1;
}

int CNormalScriptExtent::NpcQueryIsMissionNotOnList( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryIsMissionNotOnList时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int taskId = (unsigned int) ( lua_tonumber( pState, -1 ) );
	bool isFufil = pScript->ExtQueryOnMissionList( taskId );
	
	lua_pushboolean( pState, !isFufil );
	return 1;
}

///查询：指定任务在任务列表但未达成目标
int CNormalScriptExtent::NpcQueryNotAchieveMissionTarget( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryNotAchieveMissionTarget时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int taskId = (unsigned int) ( lua_tonumber( pState, -1 ) );
	bool isFufil = pScript->ExtQueryNotAchieveMissionTarget( taskId );
	
	lua_pushboolean( pState, isFufil );
	return 1;
}

///查询：指定任务在任务列表达成目标
int CNormalScriptExtent::NpcQueryAchieveMissionTarget( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryAchieveMissionTarget时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int taskId = (unsigned int) ( lua_tonumber( pState, -1 ) );
	bool isFufil = pScript->ExtQueryAchieveMissionTarget( taskId );
	
	lua_pushboolean( pState, isFufil );
	return 1;
}

///查询：完成过任务
int CNormalScriptExtent::NpcQueryIsFinishMissionBefore( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryIsFinishMissionBefore时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int taskId = (unsigned int) ( lua_tonumber( pState, -1 ) );
	bool isFufil = pScript->ExtQueryIsFinishMissionBefore( taskId );
	
	lua_pushboolean( pState, isFufil );
	return 1;
}

///查询：包裹中是否有此物体
int CNormalScriptExtent::NpcQueryPackageHave( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -2 ) || !lua_isnumber( pState , -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryPackageHave时，输入参数错", pScript->GetID() );
		return 0;
	}

	unsigned int itemTypeID = (unsigned int) ( lua_tonumber( pState, -2 ) );
	unsigned int itemCount  = (unsigned int) ( lua_tonumber( pState, -1) );
	bool isHave = pScript->ExtQueryPackageHave( itemTypeID , itemCount );
	
	lua_pushboolean( pState, isHave );
	return 1;
}

int CNormalScriptExtent::NpcQueryPackageHaveNot( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryPackageHaveNot时，输入参数错", pScript->GetID() );
		return 0;
	}

	unsigned int itemTypeID = (unsigned int) ( lua_tonumber( pState, -2 ) );
	unsigned int itemCount  = (unsigned int) ( lua_tonumber( pState, -1 ) );
	bool isHave = pScript->ExtQueryPackageHave( itemTypeID, itemCount );


	lua_pushboolean( pState, !isHave );
	return 1;
}

int CNormalScriptExtent::NpcQueryPackageCanContain( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryPackageCanContain时，输入参数错", pScript->GetID() );
		return 0;
	}

#define  MAX_ITEMCOUNT 20

	unsigned int itemArr[MAX_ITEMCOUNT];
	for ( unsigned i = 0 ; i< MAX_ITEMCOUNT; i+=2 )
	{
		int k = i+1;
		int index1 = k ;
		int index2 = index1 + 1;
		if ( i+1 >= ARRAY_SIZE(itemArr) )//这里这个循环不好，要改，具体意思再看，by dzj, 10.06.08;
		{
			D_ERROR( "CNormalScriptExtent::NpcQueryPackageCanContain, i(%d)+1 >= ARRAY_SIZE(itemArr)\n", i );
			break;
		}
		itemArr[i]   = (unsigned int) ( lua_tonumber( pState, index1 ) );
		itemArr[i+1] = (unsigned int) ( lua_tonumber( pState, index2 ) );
	}
	bool isCanGive = pScript->ExtQueryPackageCanContain( itemArr, MAX_ITEMCOUNT );
	lua_pushboolean( pState, isCanGive );
	return 1;
}

int CNormalScriptExtent::NpcOpenStandardUI(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcOpenStandardUI时，输入参数错", pScript->GetID() );
		return 0;
	}

	unsigned int uiInfo  = (unsigned int) ( lua_tonumber( pState, -1 ) );
	pScript->ExtSendStandardUiInfo( uiInfo );
	return 1;
}

///操作：给玩家任务
int CNormalScriptExtent::NpcGiveTask( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcGiveTask时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int taskID = (unsigned int) ( lua_tonumber( pState, -1 ) );
	pScript->ExtGiveTask( taskID );

	return 0;
}

///操作：执行任务(将任务属性设置为已达成目标但未交还)
int CNormalScriptExtent::NpcProcessTask( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcProcessTask时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int taskID = (unsigned int) ( lua_tonumber( pState, -1 ) );
	pScript->ExtProcessTask( taskID );

	return 0;
}

///操作：完成任务
int CNormalScriptExtent::NpcFinishTask( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcFinishTask时，输入参数错", pScript->GetID() );
		lua_pushnumber( pState, -1 );
		return 1;
	}

	unsigned int taskID = (unsigned int) ( lua_tonumber( pState, -1 ) );
	lua_pushnumber( pState, pScript->ExtFinishTask( taskID ) );

	return 1;
}

///操作：给玩家加经验
int CNormalScriptExtent::NpcAddExp( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcAddExp时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int expToAdd = (unsigned int) ( lua_tonumber( pState, -1 ) );
	pScript->ExtAddExp( expToAdd );

	return 0;
}

///操作：给玩家加金钱
int CNormalScriptExtent::NpcAddMoney( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcAddMoney时，输入参数错", pScript->GetID() );
		return 0;
	}
	unsigned int moneyToAdd = (unsigned int) ( lua_tonumber( pState, -1 ) );
	pScript->ExtAddMoney( moneyToAdd );

	return 0;
}

///操作：给玩家加属性;
int CNormalScriptExtent::NpcAddProperty( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( (!lua_isnumber( pState, -1 ))
		|| (!lua_isnumber( pState, -2 ))
		)
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcAddProperty时，输入参数错", pScript->GetID() );
		return 0;
	}

	int valToAdd = (int) ( lua_tonumber( pState, -1 ) );
	unsigned int typeToAdd = (unsigned int) ( lua_tonumber( pState, -2 ) );

	pScript->ExtAddProperty( typeToAdd, valToAdd );

	return 0;
}

///操作：给玩家设属性;
int CNormalScriptExtent::NpcSetProperty( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( (!lua_isnumber( pState, -1 ))
		|| (!lua_isnumber( pState, -2 ))
		)
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcSetProperty时，输入参数错", pScript->GetID() );
		return 0;
	}

	int valToSet = (int) ( lua_tonumber( pState, -1 ) );
	unsigned int typeToSet = (unsigned int) ( lua_tonumber( pState, -2 ) );

	pScript->ExtSetProperty( typeToSet, valToSet );

	return 0;
}

///操作：让玩家进指定副本；
int CNormalScriptExtent::NpcTransPlayerToCopy( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if (!lua_isnumber( pState, -1 ))
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcTransPlayerToCopy时，输入参数错", pScript->GetID() );
		return 0;
	}

	unsigned short copyMapID = (unsigned short) ( lua_tonumber( pState, -1 ) );
	
	pScript->ExtTransPlayerToCopy( copyMapID, 0 );

	return 0;
}

///操作：让玩家进指定伪副本；
int CNormalScriptExtent::NpcTransPlayerToPCopy( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if (!lua_isnumber( pState, -1 ))
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcTransPlayerToPCopy时，输入参数错", pScript->GetID() );
		return 0;
	}

	unsigned short pcopyMapID = (unsigned short) ( lua_tonumber( pState, -1 ) );
	
	pScript->ExtTransPlayerToPCopy( pcopyMapID, 0 );

	return 0;
}

///NPC给玩家身上加计数器；
int CNormalScriptExtent::NpcAddPlayerCounter( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if (   lua_isnumber( pState, -1 ) //计数完成后清的任务ID号；
		&& lua_isnumber( pState, -2 ) //计数目标值
		&& lua_isnumber( pState, -3 ) //计数目标类型范围max
		&& lua_isnumber( pState, -4 ) //计数目标类型范围min
		&& lua_isnumber( pState, -5 ) //计数器类型,杀怪、交友等
		)
	{
		unsigned int taskID        = (unsigned int) ( lua_tonumber( pState, -1 ) );
		unsigned int targetCount   = (unsigned int) ( lua_tonumber( pState, -2 ) );
		unsigned int counterIdMax  = (unsigned int) ( lua_tonumber( pState, -3 ) );
		unsigned int counterIdMin  = (unsigned int) ( lua_tonumber( pState, -4 ) );
		unsigned int counterIdType = (unsigned int) ( lua_tonumber( pState, -5 ) );
		pScript->ExtAddPlayerCounter( counterIdType, counterIdMin, counterIdMax, targetCount, taskID );
		return 0;
	} else {
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcAddPlayerCounter时，输入参数错", pScript->GetID() );
		return 0;
	}

	return 0;
}

///NPC给玩家道具；
int CNormalScriptExtent::NpcGiveItem( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( (!lua_isnumber( pState, -1 ))
		|| (!lua_isnumber( pState, -2 ))
		)
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcGiveItem时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int itemNum = (unsigned int) ( lua_tonumber( pState, -1 ) );
	unsigned int itemTypeID = (unsigned int) ( lua_tonumber( pState, -2 ) );
	pScript->ExtGiveItem( itemTypeID, itemNum );

	return 0;
}

int CNormalScriptExtent::NpcGiveItemRandomLevel( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if (   (!lua_isnumber( pState, -1))
		|| (!lua_isnumber( pState, -2))
		|| (!lua_isnumber( pState, -3))
		|| (!lua_isnumber( pState, -4)) )
	{
		D_ERROR("normal脚本 %d 调用NpcGiveItemRandomLevel,输入参数出错", pScript->GetID()  );
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int levelend   = (unsigned int )( lua_tonumber( pState, -1 ) );
	unsigned int levelstart = (unsigned int )( lua_tonumber( pState, -2 ) );
	unsigned int itemcount  = (unsigned int )( lua_tonumber( pState, -3 ) );
	unsigned int itemtype   = (unsigned int )( lua_tonumber( pState, -4 ) );
	if( pScript->ExtGiveItemRondomLevel( itemtype, itemcount , levelstart, levelend ) == 0 )
	{
		lua_pushboolean( pState, false );
		return 1;
	}

	lua_pushboolean( pState, true );
	return 1;
}

///NPC拿走玩家道具；
int CNormalScriptExtent::NpcRemoveItem( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( (! lua_isnumber( pState, -1 )) || (! lua_isnumber( pState, -2 ) ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcRemoveItem时，输入参数错", pScript->GetID() );
		return 0;
	}

	unsigned int itemNum = (unsigned int) ( lua_tonumber( pState, -1 ) );
	unsigned int itemTypeID = (unsigned int) ( lua_tonumber( pState, -2 ) );
	pScript->ExtRemoveItem( itemTypeID, itemNum );

	return 0;
}

int CNormalScriptExtent::NpcRemoveScriptItem(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	pScript->ExtRemoveScriptItem();
	return 0;
}

///NPC在特定位置内刷特定数量怪物；
int CNormalScriptExtent::NpcRefreshMonster( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( lua_isnumber( pState, -1 ) 
		&& lua_isnumber( pState, -2 )
		&& lua_isnumber( pState, -3 )
		&& lua_isnumber( pState, -4 )
		)
	{
		unsigned short posY = (unsigned short) ( lua_tonumber( pState, -1 ) );
		unsigned short posX = (unsigned short) ( lua_tonumber( pState, -2 ) );
		unsigned int monsterNum = (unsigned int) ( lua_tonumber( pState, -3 ) );
		unsigned int monsterID = (unsigned int) ( lua_tonumber( pState, -4 ) );
		pScript->ExtRefreshMonster( monsterID, monsterNum, posX, posY );
		return 0;
	} else {
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcRefreshMonster时，输入参数错", pScript->GetID() );
		return 0;
	}

	return 0;
}

///NPC指令弹出托盘
int CNormalScriptExtent::NpcShowPlateCmd( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( lua_isnumber( pState, -1 )	)
	{
		int plateType = (int) ( lua_tonumber( pState, -1 ) );
		pScript->ExtShowPlateCmd( plateType );
		return 0;
	} else {
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcShowPlateCmd时，输入参数错", pScript->GetID() );
		return 0;
	}

	return 0;
}

int CNormalScriptExtent::NpcShowEmailCmd(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	pScript->ExtShowEmailCmd();

	return 0;
}

int CNormalScriptExtent::NpcShowProtectPlateCmd(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( lua_isnumber( pState, -1 )	)
	{
		int plateType = (int) ( lua_tonumber( pState, -1 ) );
		pScript->ExtNpcShowProtectItemPlate( plateType );
		return 0;
	} else {
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcShowProtectPlateCmd时，输入参数错", pScript->GetID() );
		return 0;
	}
	return 0;
}

///创建一个跟随玩家的NPC
int CNormalScriptExtent::NpcCreatePlayerNpc( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( lua_isnumber( pState, -1 )	)
	{
		unsigned int taskID = (unsigned int) ( lua_tonumber( pState, -1 ) );
		unsigned int npcType = (unsigned int) ( lua_tonumber( pState, -2 ) );
		pScript->ExtCreatePlayerNpc( npcType, taskID );
		return 0;
	} else {
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcCreatePlayerNpc时，输入参数错", pScript->GetID() );
		return 0;
	}

	return 0;
}

///删去跟随玩家的NPC
int CNormalScriptExtent::NpcDelPlayerNpc( lua_State* pState )
{
	//参数：NPC类型ID号；
	NPCNORMAL_EXT_FUNPRE;

	if ( lua_isnumber( pState, -1 )	)
	{
		unsigned int npcType = (unsigned int) ( lua_tonumber( pState, -1 ) );
		pScript->ExtDelPlayerNpc( npcType );//删跟随玩家的NPC；
		return 0;
	} else {
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcDelPlayerNpc时，输入参数错", pScript->GetID() );
	}

	return 0;
}


///检查是否有跟随玩家的NPC
int CNormalScriptExtent::NpcCheckPlayerNpc( lua_State* pState )
{
	//参数：NPC类型ID号；
	NPCNORMAL_EXT_FUNPRE;

	if ( lua_isnumber( pState, -1 )	)
	{
		unsigned int npcType = (unsigned int) ( lua_tonumber( pState, -1 ) );
		bool isHave = pScript->ExtCheckPlayerNpc( npcType );//是否有跟随玩家的NPC；
		lua_pushboolean( pState, isHave );
		return 1;
	} else {
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcCheckPlayerNpc时，输入参数错", pScript->GetID() );
	}

	lua_pushboolean( pState, false );
	return 1;
}

///玩家智能检测，大于
int CNormalScriptExtent::NpcQueryIntelligenceMoreThan( lua_State* pState )
{
	//参数:检测值;
	NPCNORMAL_EXT_FUNPRE;

	bool isFulfil = false;

	if ( lua_isnumber( pState, -1 )	)
	{
		unsigned int checkValue = (unsigned int) ( lua_tonumber( pState, -1 ) );
		isFulfil = pScript->ExtQueryIntelligenceMoreThan( checkValue );
	} else {
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryIntelligenceMoreThan时，输入参数错", pScript->GetID() );
	}

	lua_pushboolean( pState, isFulfil );

	return 1;
}

///玩家智能检测， 小于
int CNormalScriptExtent::NpcQueryIntelligenceLessThan( lua_State* pState )
{
	//参数:检测值;	
	//参数:检测值;
	NPCNORMAL_EXT_FUNPRE;

    bool isFulfil = false;

	if ( lua_isnumber( pState, -1 )	)
	{
		unsigned int checkValue = (unsigned int) ( lua_tonumber( pState, -1 ) );
		isFulfil = pScript->ExtQueryIntelligenceLessThan( checkValue );
	} else {
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryIntelligenceLessThan时，输入参数错", pScript->GetID() );
	}

	lua_pushboolean( pState, isFulfil );

	return 1;
}


int CNormalScriptExtent::NpcQueryPropertyEqual( lua_State* pState ) //参数:检测值;
{
	NPCNORMAL_EXT_FUNPRE;

	if ( (!lua_isnumber( pState, -1 ))
		|| (!lua_isnumber( pState, -2 ))
		)
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryPropertyEqual时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	int valToAdd = (int) ( lua_tonumber( pState, -1 ) );
	unsigned int typeToAdd = (unsigned int) ( lua_tonumber( pState, -2 ) );
	
	bool bRet = pScript->ExtNpcQueryPropertyEqual( typeToAdd, valToAdd );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CNormalScriptExtent::NpcQueryPropertyLowerThan( lua_State* pState ) //参数:检测值;
{
	NPCNORMAL_EXT_FUNPRE;

	if ( (!lua_isnumber( pState, -1 ))
		|| (!lua_isnumber( pState, -2 ))
		)
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryPropertyLowerThan时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	int valCheck= (int) ( lua_tonumber( pState, -1 ) );
	unsigned int type = (unsigned int) ( lua_tonumber( pState, -2 ) );
	bool bRet = pScript->ExtNpcQueryPropertyLowerThan( (EAttribute)type, valCheck );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CNormalScriptExtent::NpcQueryPropertyMoreThan( lua_State* pState ) //参数:检测值;
{
	NPCNORMAL_EXT_FUNPRE;

	if ( (!lua_isnumber( pState, -1 ))
		|| (!lua_isnumber( pState, -2 ))
		)
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcQueryPropertyMoreThan时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	int valCheck = (int) ( lua_tonumber( pState, -1 ) );
	unsigned int type = (unsigned int) ( lua_tonumber( pState, -2 ) );
	
	bool bRet = pScript->ExtNpcQueryPropertyMoreThan( type, valCheck );
	lua_pushboolean( pState, bRet );

	return 1;
}


int CNormalScriptExtent::NpcGiveSkill( lua_State* pState )//参数:检测值;	
{
	NPCNORMAL_EXT_FUNPRE;

	if ( (!lua_isnumber( pState, -1 ) ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcGiveSkill时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int skillID = (unsigned int) ( lua_tonumber( pState, -1 ) );
	CPlayer* pScriptUser = pScript->GetScriptUser();
	if( NULL != pScriptUser )
	{
		bool bRet = pScriptUser->AddSkillByID( skillID );
		lua_pushboolean( pState, bRet );
		return 1;
	}

	lua_pushboolean( pState, false );
	return 1;
}

//显示ＮＰＣ商店，参数:商店ＩＤ号;	
int CNormalScriptExtent::NpcShowShop( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( !lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcShowShop时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	int shopID = (int) ( lua_tonumber( pState, -1 ) );
	
	bool bRet = pScript->ExtNpcShowShop( shopID );

	lua_pushboolean( pState, bRet );

	return 1;
}

//显示工会发布任务
int CNormalScriptExtent::NpcDisplayIssuedTask( lua_State* pState )
{
	ACE_UNUSED_ARG( pState );
	D_ERROR( "调用已取消接口：NpcDisplayIssuedTask\n" );
	//09.04.28,取消工会任务新实现,NPCNORMAL_EXT_FUNPRE;

	//bool bRet = pScript->ExtDisplayUnionIssuedTask();
	//lua_pushboolean( pState, bRet );

	return 1;
}

int CNormalScriptExtent::NpcSelectUnionCmd( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( !lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcSelectUnionCmd时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int cmdID = (unsigned int) ( lua_tonumber( pState, -1 ) );

	bool bRet = pScript->ExtSelectUnionCmd( cmdID );

	lua_pushboolean( pState, bRet );

	return 1;
}

int CNormalScriptExtent::NpcHaveIssuedUnionTask( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	bool bRet = pScript->ExtHaveIssuedUnionTask();
	lua_pushboolean( pState, bRet );

	return 1;
}

int CNormalScriptExtent::NpcQueryIsUnionLeader( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	bool bRet = pScript->ExtIsUnionLeader();
	lua_pushboolean( pState, bRet );

	return 1;
}

int CNormalScriptExtent::NpcQueryUnionLevel( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR( "normal脚本%d调用NpcQueryUnionLevel时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	int arg = (int) ( lua_tonumber( pState, -1 ) );
	bool bRet = (pScript->ExtQueryUnionLevel() >= arg ? true : false);
	lua_pushboolean( pState, bRet );

	return 1;
}

int CNormalScriptExtent::NpcShowStorage(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	bool bRet = pScript->ExtShowStorage();
	lua_pushboolean( pState, bRet );

	return 1;
}

int CNormalScriptExtent::NpcBeginOLTest( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( !lua_isnumber( pState, -1 ) )
	{
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int taskID = ( unsigned int )( lua_tonumber( pState, -1 ) );
	bool bRet = pScript->ExtBeginOLTest( taskID );
	lua_pushboolean( pState, bRet );

	return 1;
}


#ifdef OPEN_PUNISHMENT
//显示天谴玩家的位置
int CNormalScriptExtent::NpcShowPunishPlayerInfo( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	
	bool bRet = pScript->ExtNpcShowPunishPlayerInfo();
	lua_pushboolean( pState, bRet );

	return 1;
}

//给予天谴任务
int CNormalScriptExtent::NpcGivePunishTask( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	bool bRet = pScript->ExtGivePunishTask();
	lua_pushboolean( pState, bRet );

	return 1;
}

//是否在天谴活动中,接天谴任务的先置条件
int CNormalScriptExtent::NpcIsInPunishment( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	
	bool bRet = pScript->ExtIsInPunishment();
	lua_pushboolean( pState, bRet );

	return 1;
}


//是否接过天谴任务
int CNormalScriptExtent::NpcIsNotHavePunishTask( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	bool bRet = pScript->ExtIsHavePunishTask();
	lua_pushboolean( pState, !bRet );
	
	return 1;
}


//天谴任务是否完成
int CNormalScriptExtent::NpcIsPunishTaskAchieve( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	bool bRet = pScript->IsPunishTaskAchieve();
	lua_pushboolean( pState, bRet );
	return 1;
}


//获取天谴杀人计数器
int CNormalScriptExtent::NpcGetPunishCounter( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	int counter = pScript->GetPunishCounter();
	lua_pushnumber( pState, counter );
	return 1;
}


//获取天谴目标姓名
int CNormalScriptExtent::NpcGetPunishTargetName( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	string targetName;	
	pScript->GetPunishTargetName( targetName );
	lua_pushstring( pState, targetName.c_str() );
	return 1;
}


//完成天谴任务
int CNormalScriptExtent::NpcFinishPunishTask( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	int ret = pScript->ExtFinishPunishTask();
	lua_pushnumber( pState, ret );
	
	return 1;
}
#endif //OPEN_PUNISHMENT


int CNormalScriptExtent::NpcAddRacePrestige( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if ( !lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcAddRacePrestige时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int addVal = (unsigned int)( lua_tonumber( pState, -1 ));
	bool bRet = pScript->ExtAddRacePrestige( addVal );
	lua_pushboolean( pState, bRet );

	return 1;
}


int CNormalScriptExtent::NpcCreateBuff( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if ( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState,-2 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcCreateBuff时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int buffTime = (unsigned int)( lua_tonumber( pState, -1) );
	unsigned int buffTypeID = (unsigned int)( lua_tonumber( pState,-2) );
	
	bool bRet = pScript->ExtNpcCreateBuff( buffTypeID, buffTime );
	lua_pushboolean( pState, bRet );

	return 1;
}

int CNormalScriptExtent::NpcSaveRebirthPos( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( lua_isnumber( pState, -1 ) 
		&& lua_isnumber( pState, -2 )
		&& lua_isnumber( pState, -3 )
		&& lua_isnumber( pState, -4)
		)
	{
		unsigned short bottomOfRanage = (unsigned short) ( lua_tonumber( pState, -1 ) );
		unsigned short rightOfRanage = (unsigned short) ( lua_tonumber( pState, -2 ) );
		unsigned short topOfRanage = (unsigned short) ( lua_tonumber( pState, -3 ) );
		unsigned short leftOfRanage = (unsigned short) ( lua_tonumber( pState, -4 ) );
		bool bRet = pScript->ExtNpcSaveRebirthPos(leftOfRanage, topOfRanage, rightOfRanage, bottomOfRanage);
		lua_pushboolean( pState, bRet );
		return 1;
	} else {
		//传送参数错；
		D_ERROR( "normal脚本%d调用NpcSaveRebirthPos时，输入参数错", pScript->GetID() );
		return 0;
	}
}

//让对话Npc消失(死亡)
int CNormalScriptExtent::NpcChatNpcDisappear( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
		if ( !lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcChatNpcDisappear时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int arg = (unsigned int)lua_tonumber( pState, -1 );
	bool isDropItem = ( arg != 0 );
	bool bRet = pScript->ExtDisappearChatNpc( isDropItem );
	lua_pushboolean( pState, bRet );

	return 1;
}

//宠物技能学习/*AUTOMATIC_ATTACK_MASK:1,AUTOMATIC_USE_DRUGS_MASK:2,AUTOMATIC_PICK_MASK:3,WEAPON_LEVELUP_MASK:4,OFFLINE_LEVELUP_MASK:5*/；
int CNormalScriptExtent::NpcPetSkillLearn( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	
	if ( !lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcPetSkillLearn时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int petskillid = (unsigned int)( lua_tonumber( pState, -1 ));
	bool bRet = pScript->ExtNpcPetSkillLearn( petskillid );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcSkillLearn( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( !lua_isnumber( pState, -1 ) )
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcSkillLearn时，输入参数错", pScript->GetID() );
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int skillID = (unsigned int)( lua_tonumber( pState, -1 ));
	bool bRet = pScript->ExtNpcSkillLearn( skillID );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcChatSysDialog( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;	
	if ( lua_isstring( pState, -1 ) )
	{
		const char* showMsg = lua_tostring( pState, -1 );
		pScript->ExtNpcChatSysDialog( showMsg );
	} else {
		D_ERROR( "normal脚本%d调用NpcChatSysDialog时，输入参数错", pScript->GetID() );
	}
	return 0;

}


int CNormalScriptExtent::NpcIsEvil( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	
	bool bRet = pScript->ExtNpcIsEvil();
	lua_pushboolean( pState, bRet );
	return 1;
}


int CNormalScriptExtent::NpcRandNumber( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ))
	{
		//未传送参数；
		D_ERROR( "normal脚本%d调用NpcRandNumber时，输入参数错", pScript->GetID() );
		return 1;
	}

	int b = (int)( lua_tonumber( pState, -1 ));
	int a = (int)( lua_tonumber( pState, -2 ));
	int randnumber = pScript->ExtNpcRandNumber( a, b );
	lua_pushnumber( pState, randnumber );
	return 1;
}

int CNormalScriptExtent::NpcCanGiveMoney( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( lua_isnumber( pState, -1 )  )//如果
	{
		int money = (unsigned int) ( lua_tonumber( pState, -1 ) );
		bool isCanGiveMoney = pScript->ExtCanGiveMoney( money );
		lua_pushboolean( pState , isCanGiveMoney );
		return 1;
	}
	return 0;
}

int CNormalScriptExtent::NpcGetPlayerSex(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	int sex = pScript->ExtGetPlayerSex();
	lua_pushnumber( pState, sex );
	return 1;
}

int CNormalScriptExtent::NpcSendSysMail(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	bool issendrst = false;
	if ( (!lua_isnumber(pState , -13))//money
		|| (!lua_isstring(pState, -14))//content
		|| (!lua_isstring( pState, -15))//title 
		)
	{
		D_ERROR( "normal脚本%d调用NpcSendSysMail时，基本调用参数错\n", pScript->GetID() );
		lua_pushboolean( pState, issendrst );
		return 1;
	}

	const char* pTitle   = lua_tostring( pState , -15 );
	const char* pContent = lua_tostring( pState , -14 );
	if ( NULL == pTitle || NULL == pContent )
	{
		D_ERROR( "normal脚本%d调用NpcSendSysMail时，参数错(邮件标题空)\n", pScript->GetID() );
		lua_pushboolean( pState, issendrst );
		return 1;
	}
	unsigned int  money  = (unsigned int)lua_tonumber( pState , -13 );

	static vector<unsigned int> npcMailItemType;
	static vector<int> npcMailItemCount;
	npcMailItemType.clear();
	npcMailItemCount.clear();
	int readpos = 0;
	int itemcount = 0;
	int itemtype = 0;
	for ( unsigned int i=0; i<6; ++i )
	{
		readpos = -1 - i*2;
		if ( !lua_isnumber( pState, readpos/*item count*/ ) 
			|| !lua_isnumber( pState, readpos-1/*item type*/ )
			)
		{
			continue;
		}
		itemcount = (int)lua_tonumber( pState, readpos );
		itemtype = (unsigned int)lua_tonumber( pState, readpos-1 );
		if ( itemcount < 0 )
		{
			D_ERROR( "normal脚本%d调用NpcSendSysMail时，参数itemcount%d(itemtype%d)错\n", pScript->GetID(), itemcount, itemtype );
			continue;
		}
		if ( ( 0 == itemcount ) || ( 0 == itemtype ) )
		{
			continue;
		}
		npcMailItemType.push_back( itemtype );
		npcMailItemCount.push_back( itemcount );
	}

	issendrst = pScript->ExtSendMail( pTitle, pContent, money, npcMailItemType, npcMailItemCount );

	lua_pushboolean( pState, issendrst );
	return 1;

	//旧方法，替换，by dzj, 10.06.22; unsigned int itemArr[12] = { 0 };
	//unsigned int itemIndex = 0;
	//for ( unsigned int i = 0 ;  i < 12 && itemIndex < 12 ; i+=2 )
	//{
	//	int k = -1 - i ;
	//	int j = -2 - i;
	//	if ( !lua_isnumber( pState, k ) || !lua_isnumber( pState, j ) )
	//	{
	//		continue;
	//	}

	//	int itemCount  =  (int)lua_tonumber( pState, k );
	//	int itemType   =  (int)lua_tonumber( pState, j );

	//	if ( itemCount > 0 && itemType > 0 )
	//	{
	//		npcMailItemType.push_back( itemType );
	//		npcMailItemCount.push_back( itemCount );
	//		//if ( itemIndex+1 >= ARRAY_SIZE(itemArr) ) //这个循环不好，要改，具体意义再看，by dzj, 10.06.08;
	//		//{
	//		//	D_ERROR( "CNormalScriptExtent::NpcSendSysMail, itemIndex(%d)+1 >= ARRAY_SIZE(itemArr)\n", itemIndex );
	//		//	break;
	//		//}
	//		//itemArr[itemIndex]     =  itemType;
	//		//itemArr[itemIndex + 1] =  itemCount;
	//		//itemIndex+=2;
	//	}
	//}
	//issendrst = pScript->ExtSendMail(  pTitle , pContent, money, itemArr  );

	//lua_pushboolean( pState, issendrst );
	//return 1;
}

///指令玩家跳转地图,参数:目标地图ID号，目标点（X，Y）坐标;
int CNormalScriptExtent::NpcSwitchMap( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( lua_isnumber( pState, -1 ) 
		&& lua_isnumber( pState, -2 )
		&& lua_isnumber( pState, -3 )
		)
	{
		unsigned short posY = (unsigned short) ( lua_tonumber( pState, -1 ) );
		unsigned short posX = (unsigned short) ( lua_tonumber( pState, -2 ) );
		unsigned int   mapID = (unsigned int) ( lua_tonumber( pState, -3 ) );
		pScript->ExtNpcSwitchMap( mapID, posX, posY );
		return 0;
	} else {
		//传送参数错；
		D_ERROR( "normal脚本%d调用NpcSwitchMap时，输入参数错", pScript->GetID() );
		return 0;
	}
}

int CNormalScriptExtent::NpcQueryRideState(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	bool isRide = pScript->ExtIsOnRideState();
	lua_pushboolean( pState, isRide );
	return 1;
}

#ifdef NEW_NPC_INTERFACE
	//1 判断是否加入了战盟
	int CNormalScriptExtent::NpcIsInUnion( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		bool bRet = pScript->ExtNpcIsInUnion();
		lua_pushboolean( pState, bRet );
		return 1;
	}


	int CNormalScriptExtent::NpcIsUnionLeader( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		bool bRet = pScript->ExtNpcIsUnionLeader();
		lua_pushboolean( pState, bRet );
		return 1;
	}

		
	//3 判断是否为组队状态
	int CNormalScriptExtent::NpcIsInTeam( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		bool bRet = pScript->ExtNpcIsInTeam();
		lua_pushboolean( pState, bRet );
		return 1;
	}

	//4 判断是否为队长
	int CNormalScriptExtent::NpcIsTeamCaptain( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		bool bRet = pScript->ExtNpcIsTeamCaptain();
		lua_pushboolean( pState, bRet );
		return 1;
	}

	//5 判断小队有几人
	int CNormalScriptExtent::NpcTeamNumberMoreThan( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;
		if( !lua_isnumber( pState, -1 ) )
		{
			D_ERROR("CNormalScriptExtent::NpcTeamNumberMoreThan 输入参数错误\n");
			return 0;
		}
	    
		unsigned int teamnumber = (unsigned int)lua_tonumber( pState, -1 );
		bool bRet = pScript->ExtNpcTeamNumberMoreThan( teamnumber );
		lua_pushboolean( pState, bRet );
		return 1;
	}


	int CNormalScriptExtent::NpcTeamNumberLowerThan( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		if( !lua_isnumber( pState, -1 ) )
		{
			D_ERROR("CNormalScriptExtent::NpcTeamNumberLowerThan 输入参数错误\n");
			return 0;
		}

		unsigned int teamnum = (unsigned int)lua_tonumber( pState, -1);
		bool bRet = pScript->ExpNpcTeamNumberLowerThan( teamnum );
		lua_pushboolean( pState, bRet );
		return 1;
	}


	int CNormalScriptExtent::NpcTeamNumberEqual( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		if( !lua_isnumber( pState, -1 ) )
		{
			D_ERROR("CNormalScriptExtent::NpcTeamNumberEqual 输入参数错误\n");
			return 0;
		}

		unsigned int teamnum = (unsigned int)lua_tonumber( pState, -1 );
		bool bRet = pScript->ExtNpcTeamNumberEqual( teamnum );
		lua_pushboolean( pState, bRet );
		return 1;
	}

	//6 判断好友有几人
	int CNormalScriptExtent::NpcFriendNumberMoreThan( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		if( !lua_isnumber( pState, -1 ) )
		{
			D_ERROR("NpcFriendNumberMoreThan 输入参数错误\n");
			return 0;
		}

		unsigned int val = (unsigned int)lua_tonumber( pState, -1 );
		unsigned int friendnum = pScript->ExtNpcGetFriendNumber();
		lua_pushnumber( pState, val>friendnum );
		return 1;
	}

	int CNormalScriptExtent::NpcFriendNumberLowerThan( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		if( !lua_isnumber( pState, -1 ) )
		{
			D_ERROR("NpcFriendNumberLowerThan 输入参数错误\n");
			return 0;
		}

		unsigned int val = (unsigned int)lua_tonumber( pState, -1 );
		unsigned int friendnum = pScript->ExtNpcGetFriendNumber();
		lua_pushnumber( pState, val<friendnum );
		return 1;
	}

	int CNormalScriptExtent::NpcFrinedNumberEqual( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		if( !lua_isnumber( pState, -1 ) )
		{
			D_ERROR("NpcFrinedNumberEqual 输入参数错误\n");
			return 0;
		}

		unsigned int val = (unsigned int)lua_tonumber( pState, -1 );
		unsigned int friendnum = pScript->ExtNpcGetFriendNumber();
		lua_pushnumber( pState, val==friendnum );
		return 1;
	}
	

	//7 判断仇人有几人
	int CNormalScriptExtent::NpcFoeNumberMoreThan( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		if( !lua_isnumber( pState, -1 ) )
		{
			D_ERROR("NpcFoeNumberMoreThan 输入参数错误\n");
			return 0;
		}

		unsigned int val = (unsigned int)lua_tonumber( pState, -1 );
		unsigned int enemyNum = pScript->ExtNpcGetFoeNumber();
		lua_pushnumber( pState, val>enemyNum );
		return 1;
	}

	int CNormalScriptExtent::NpcFoeNumberLowerThan( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		if( !lua_isnumber( pState, -1 ) )
		{
			D_ERROR("NpcFoeNumberLowerThan 输入参数错误\n");
			return 0;
		}

		unsigned int val = (unsigned int)lua_tonumber( pState, -1 );
		unsigned int enemyNum = pScript->ExtNpcGetFoeNumber();
		lua_pushnumber( pState, val<enemyNum );
		return 1;
	}

	int CNormalScriptExtent::NpcFoeNumberEqual( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;

		if( !lua_isnumber( pState, -1 ) )
		{
			D_ERROR("NpcFoeNumberEqual 输入参数错误\n");
			return 0;
		}

		unsigned int val = (unsigned int)lua_tonumber( pState, -1 );
		unsigned int enemyNum = pScript->ExtNpcGetFoeNumber();
		lua_pushnumber( pState, val==enemyNum );
		return 1;
	}


#endif //NEW_NPC_INTERFACE

#ifdef ANTI_ADDICTION
	int CNormalScriptExtent::NpcIsTriedOrAddictionState( lua_State* pState )
	{
		NPCNORMAL_EXT_FUNPRE;
		bool bRet = pScript->IsInTriedOrAddictionState();
		lua_pushboolean( pState, bRet );
		return 1;
	}
#endif //ANTI_ADDICTION


int CNormalScriptExtent::NpcCreateNpcByChatTarget( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("NpcCreateNpcByChatTarget 输入参数错误\n");
		return 0;
	}

	unsigned int npcTypeId = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int npcNum = (unsigned int)lua_tonumber( pState, -1 );
	
	bool bRet = pScript->ExtNpcCreateNpcByChatTarget( npcTypeId, npcNum );
	lua_pushboolean( pState, bRet );
    return 1;
}


//添加标记
int CNormalScriptExtent::NpcAddMark( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("NpcAddMark 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int mark = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->AddMark( mark );
	lua_pushboolean( pState, bRet );
	return 1;
}

//检查标记
int CNormalScriptExtent::NpcCheckMark( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("NpcCheckMark 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}
	
	unsigned int mark = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->CheckMark( mark );
	lua_pushboolean( pState, bRet );
	return 1;
}


//检查当前时间
int CNormalScriptExtent::NpcCheckExTime( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("NpcCheckExTime 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int time = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->CheckExTime( time );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcShowRaceMasterPlate(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	bool bRet = pScript->ExtShowRaceMasterPlate();
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcCheckMapSwitchRace( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("NpcCheckMapSwitchRace 输入参数错误\n");
		return 0;
	}

	unsigned int mapid = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtCheckMapSwitchRace((long)mapid );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcCanGiveNpcFollowTask(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	bool bRet = pScript->ExtCanGetNpcFollowTask();
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcShowNotices(lua_State *pState)
{
	NPCNORMAL_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1) ||
		!lua_isnumber( pState, -2) ||
		!lua_isnumber( pState, -3) ||
		!lua_isnumber( pState, -4) ||
		!lua_isstring( pState, -5) )
	{
		D_ERROR("NpcShowNotices 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int distancetime = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int maxshowtime  = (unsigned int)lua_tonumber( pState, -2 );
	char isgolabl  = (char)lua_tonumber( pState, -3 );
	char _position = (char)lua_tonumber( pState, -4 );
	const char* content= (char *)lua_tostring( pState, -5 );

	bool bRet = pScript->ExtShowNotice( content,_position, isgolabl, maxshowtime, distancetime );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcShowParamsNotices(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if (  !lua_isnumber( pState,-10)
		||!lua_isnumber( pState,-9)
		||!lua_isnumber( pState,-8)
		||!lua_isnumber( pState,-7)
		||!lua_isnumber( pState,-6) 
		)
	{
		D_ERROR("CNormalScriptExtent::MSShowParamsNotices 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int validparamcount = 0;
	const char* param5 = lua_tostring( pState, -1 );
	if ( param5 ) validparamcount++;
	const char* param4 = lua_tostring( pState, -2 );
	if ( param4 ) validparamcount++;
	const char* param3 = lua_tostring( pState, -3 );
	if ( param3 ) validparamcount++;
	const char* param2 = lua_tostring( pState, -4 );
	if ( param2 ) validparamcount++;
	const char* param1 = lua_tostring( pState, -5 );
	if ( param1 ) validparamcount++;

	unsigned int distancetime = (unsigned int)lua_tonumber( pState,-6 );
	unsigned int showmaxcount = (unsigned int)lua_tonumber( pState,-7 );
	char isglobal = (char)lua_tonumber( pState,  -8 );
	char _position = (char)lua_tonumber( pState, -9 );
	unsigned int msgid =(unsigned int)lua_tonumber( pState, -10 );

	bool bRet = pScript->ExtShowParamNotice( msgid, _position, isglobal, showmaxcount,  distancetime, validparamcount, param1, param2, param3, param4, param5 );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcIsHaveSkill(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if ( !lua_isnumber( pState, -1) )
	{
		D_ERROR("CNormalScriptExtent::NpcIsHaveSkill 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int skillid = (unsigned int)lua_tonumber( pState ,-1 );
	bool bRet = pScript->ExtIsHaveSkill( skillid );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcRunDropSet( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if ( !lua_isnumber( pState, -1) )
	{
		D_ERROR("CNormalScriptExtent::NpcRunDropSet 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int dorpsetid = (unsigned int)lua_tonumber( pState ,-1 );
	bool bRet = pScript->ExtExecDropSetID( dorpsetid );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcGetPlayerPosX( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	unsigned mapid, posX, posY;
	pScript->ExtGetPlayerPosInfo( mapid, posX, posY );
	lua_pushnumber( pState, posX );
	return 1;
}

int CNormalScriptExtent::NpcGetPlayerPosY(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	unsigned mapid, posX, posY;
	pScript->ExtGetPlayerPosInfo( mapid, posX, posY );
	lua_pushnumber( pState, posY );
	return 1;
}

int CNormalScriptExtent::NpcGetPlayerMapID(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	unsigned mapid, posX, posY;
	pScript->ExtGetPlayerPosInfo( mapid, posX, posY );
	lua_pushnumber( pState, mapid );
	return 1;
}

int CNormalScriptExtent::NpcPlayerCallMonster( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState,-2 ) )
	{
		D_ERROR("NpcPlayerCallMonster 输入参数错误\n");
		return 1;
	}

	unsigned int range = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int num = (unsigned int)lua_tonumber( pState, -2 );
	bool bRet = pScript->ExtPlayerCallMonster( num, range );
	lua_pushboolean( pState, bRet );
	return 1;	

}

int CNormalScriptExtent::NpcPlayerRandMove(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	bool bRet = pScript->ExtPlayerRandMove();
	lua_pushboolean( pState, bRet );
	return 1;
}

int CNormalScriptExtent::NpcShowAuctionPlate(lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	bool bRet = pScript->ExtShowAuctionPlate();
	lua_pushboolean( pState, bRet );
	return 1;
}

///查询某伪副本是否可进
int CNormalScriptExtent::NpcIsPCopyOpen( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("NpcIsPCopyOpen 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int pcopymapid = (unsigned int)lua_tonumber( pState, -1 );

	bool bRet = pScript->ExtIsPCopyOpen( pcopymapid );
	lua_pushboolean( pState, bRet );

	return 1;
}

//比较2个时间
int CNormalScriptExtent::NpcCheckTime( lua_State* pState )
{
	NPCNORMAL_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState,-2 ) )
	{
		D_ERROR("NpcCheckTime 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int time2 = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int time1 = (unsigned int)lua_tonumber( pState, -2 );
	bool bRet = pScript->CheckTime( time1, time2 );
	lua_pushboolean( pState, bRet );
	return 1;	
}


	

///...NPC对话脚本扩展
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///初始化，注册各AI脚本用扩展函数至lua；
void CAIScriptExtent::Init( lua_State* pLuaState )
{
	//待注册函数入vector;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//注册各脚本中使用的MuxLib接口函数...
	//目前不同类型脚本使用同一组接口函数，虽然某类脚本实际上只能使用这些接口函数中的一小部分
	//以后拟改为特定类型脚本只注册特定组别函数，以防止误用，并允许同名函数在不同类脚本中的物理意义不一样，
	//例如：在战斗判断脚本中只能使用战斗判断相关扩展接口而不能使用战斗计算相关脚本
	//      又如，对于名字MuxLib.GetAttackPlayerLevel，其实际注册函数将随着注册者的脚本类型不同而不同，从而最终调用不同的实现；
	if ( !m_isvecInited )
	{
		m_isvecInited = true;
		m_vecRegFns.push_back( ToRegFns( "Debug", Debug ) );//调试信息打印；
	}

	//...注册各脚本中使用的MuxLib接口函数
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	CLuaRestoreStack rs( pLuaState );//准备恢复堆栈;

	//新函数表；
	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "MuxLib" );//置全局变量"MuxLib"为新表；

	lua_getglobal( pLuaState, "MuxLib" );//push表"MuxLib"；
	//注册向量中的函数至MuxLib表；
	ToRegFns* pToReg = NULL;
	for ( vector<ToRegFns>::iterator tmpiter=m_vecRegFns.begin();
		tmpiter!=m_vecRegFns.end(); ++tmpiter )
	{
		pToReg = &(*tmpiter);
		lua_pushstring( pLuaState, pToReg->fnName );
		lua_pushnumber( pLuaState, (lua_Number)(tmpiter-m_vecRegFns.begin()) );//closure注册为vec中的序号；
		lua_pushcclosure( pLuaState, LuaCallbackMeta<CAIScriptExtent>, 1);
		lua_settable( pLuaState, -3 );//MuxLib[pToReg->fnName] = LuaCallback(with closure=本项vec序号)；
	}

	return;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///AI脚本扩展...

/**
//AI扩展函数定义通用部分；
//1、判断该pState是否对应一个CMonsterScriptFsm脚本，如果不是，则出错返回，
//   即：必须在AI脚本中调用，且调用该脚本者必须为CMonsterScriptFsm对象;
*/
#define AI_EXT_FUNPRE \
	if ( NULL == pState )\
    {\
	    return 0;\
    }\
	CMonsterScriptFsm* pScript = CMonsterScriptFsm::FindScripts( pState );\
	if ( NULL == pScript  )\
	{\
		return 0;\
	}\

///脚本扩展，用于调试；
int CAIScriptExtent::Debug( lua_State* pState )
{
//#ifndef _DEBUG
//	ACE_UNUSED_ARG( pState );
//	return 0;
//#else //_DEBUG
	if ( !lua_isnumber( pState, -1 ) )
	{
		if ( lua_isstring( pState, -1 ) )
		{
			const char* debugstr = lua_tostring( pState, -1 );
			if ( NULL == g_debugPlayer ) 
			{
			    D_DEBUG( "脚本调试信息:%s\n", debugstr );
			} else {
				g_debugPlayer->SendSystemChat( debugstr );
			}
		} else {
			D_DEBUG( "脚本调试信息输入参数错误\n" );
		}
	} else {
		double debugdb = lua_tonumber( pState, -1 );
		if ( lua_isstring( pState, -2 ) )
		{
			const char* debugstr = lua_tostring( pState, -2 );
			D_DEBUG( "脚本调试信息:%s(%f)\n", debugstr, debugdb );
		} else {
			D_DEBUG( "脚本调试信息输入参数错误\n" );
		}
	}

	return 0;
//#endif //_DEBUG
}
///...AI脚本扩展
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///地图脚本扩展...
#ifdef STAGE_MAP_SCRIPT
///初始化，注册各扩展函数至lua；
void CMapScriptExtent::Init( lua_State* pLuaState )
{
	if ( !m_isvecInited )
	{
		m_isvecInited = true;
		//待注册函数入vector;
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//注册各脚本中使用的MuxLib接口函数...
		//目前不同类型脚本使用同一组接口函数，虽然某类脚本实际上只能使用这些接口函数中的一小部分
		//以后拟改为特定类型脚本只注册特定组别函数，以防止误用，并允许同名函数在不同类脚本中的物理意义不一样，
		//例如：在战斗判断脚本中只能使用战斗判断相关扩展接口而不能使用战斗计算相关脚本
		//      又如，对于名字MuxLib.GetAttackPlayerLevel，其实际注册函数将随着注册者的脚本类型不同而不同，从而最终调用不同的实现；
		m_vecRegFns.push_back( ToRegFns( "MSBrocastMsg", MSBrocastMsg ) );//调试信息打印；	
		m_vecRegFns.push_back( ToRegFns( "MSSetPlayerOutPos", MSSetPlayerOutPos ) );//副本地图设置玩家的outmapinfo;
		m_vecRegFns.push_back( ToRegFns( "MSSetAllPlayerOutPos", MSSetAllPlayerOutPos ) );//副本地图设置玩家的outmapinfo;
		m_vecRegFns.push_back( ToRegFns( "MSKickResPlayer", MSKickResPlayer ) );//副本地图踢相应玩家；
		m_vecRegFns.push_back( ToRegFns( "MSEndCopy", MSEndCopy ) );//副本结束(同时踢所有玩家)；
		m_vecRegFns.push_back( ToRegFns( "MSPCopyOpen", MSPCopyOpen ) );//设置某伪副本可进；
		m_vecRegFns.push_back( ToRegFns( "MSPCopyClose", MSPCopyClose ) );//设置某伪副本不可进；

		m_vecRegFns.push_back( ToRegFns( "MSSetResPlayerCopyStage", MSSetResPlayerCopyStage ) );//设置对应的副本阶段；

		m_vecRegFns.push_back( ToRegFns( "MSSetResPlayerPos", MSSetResPlayerPos ) );//设置相应玩家位置；
		m_vecRegFns.push_back( ToRegFns( "MSSetTDFlag", MSSetTDFlag ) );//设置TD副本标记；	

		m_vecRegFns.push_back( ToRegFns( "MSNotiResPlayerMsg", MSNotiResPlayerMsg ) );
		m_vecRegFns.push_back( ToRegFns( "MSAddCounterOfMap", MSAddCounterOfMap ) );
		m_vecRegFns.push_back( ToRegFns( "MSAddTimerOfMap", MSAddTimerOfMap ) );
		m_vecRegFns.push_back( ToRegFns( "MSIsHaveTimerOfMap", MSIsHaveTimerOfMap ) );
		m_vecRegFns.push_back( ToRegFns( "MSAddDetailTimerOfMap", MSAddDetailTimerOfMap) );
		m_vecRegFns.push_back( ToRegFns( "MSAttackCityEnd", MSAttackCityEnd) );
		m_vecRegFns.push_back( ToRegFns( "MSWorldNotify", MSWorldNotify) );
		m_vecRegFns.push_back( ToRegFns( "MSCreateNpc", MSCreateNpc) );
		m_vecRegFns.push_back( ToRegFns( "MSDelNpc", MSDelNpc) );
		m_vecRegFns.push_back( ToRegFns( "MSDeleteTimer", MSDeleteTimer) );
		m_vecRegFns.push_back( ToRegFns( "MSDeleteDetailTimer", MSDeleteDetailTimer) );
		m_vecRegFns.push_back( ToRegFns( "MSNotifyWarTaskState", MSNotifyWarTaskState) );
		m_vecRegFns.push_back( ToRegFns( "MSIsInAttackWar", MSIsInAttackWar) );
		m_vecRegFns.push_back( ToRegFns( "MSCreateNpcBySet", MSCreateNpcBySet) );
		m_vecRegFns.push_back( ToRegFns( "MSGetLastKillRace", MSGetLastKillRace) );
		m_vecRegFns.push_back( ToRegFns( "MSModifyMapRace", MSModifyMapRace) ); //修改地图归属种族
		m_vecRegFns.push_back( ToRegFns( "MSGetMapRace", MSGetMapRace) ); 	//获取地图归属种族
		m_vecRegFns.push_back( ToRegFns( "MSGetCityLevel", MSGetCityLevel) ); 	//获取城市等级
		m_vecRegFns.push_back( ToRegFns( "MSSetItemNpcStatus", MSSetItemNpcStatus) ); 	//设置物件NPC状态
		m_vecRegFns.push_back( ToRegFns( "MSModifyRebirthPtRace", MSModifyRebirthPtRace) ); 	//修改复活点种族归属
		m_vecRegFns.push_back( ToRegFns( "MSOpenCreator", MSOpenCreator) ); 	//开启生成器
		m_vecRegFns.push_back( ToRegFns( "MSCloseCreator", MSCloseCreator) ); 	//关闭生成器
		m_vecRegFns.push_back( ToRegFns( "MSModifyTelpotRace", MSModifyTelpotRace) ); 	//修改专送点的种族
		m_vecRegFns.push_back( ToRegFns( "MSIsDeclareWar", MSIsDeclareWar) );//是否已经对其他种族宣战了
		m_vecRegFns.push_back( ToRegFns( "MSCreateBuff", MSCreateBuff) ); 	//创建buff
		m_vecRegFns.push_back( ToRegFns( "MSDeleteBuff", MSDeleteBuff) );//删除buff
		m_vecRegFns.push_back( ToRegFns( "MSKillPlayerByRace", MSKillPlayerByRace) );//杀死所有玩家
		m_vecRegFns.push_back( ToRegFns( "MSDelPlayerItem", MSDelPlayerItem) );	//删除玩家身上某道具
		m_vecRegFns.push_back( ToRegFns( "MSGetScriptUserName", MSGetScriptUserName) );	//获得当前玩家姓名
		m_vecRegFns.push_back( ToRegFns( "MSSwitchMapPlayer", MSSwitchMapPlayer) );//传送玩家
		m_vecRegFns.push_back( ToRegFns( "MSAddWarTimer", MSAddWarTimer) );//创建WarTimer
		m_vecRegFns.push_back( ToRegFns( "MSDelWarTimer", MSDelWarTimer) );//删除WarTimer
		m_vecRegFns.push_back( ToRegFns( "MSAddWarCounter", MSAddWarCounter) );//创建WarCounter
		m_vecRegFns.push_back( ToRegFns( "MSDelWarCounter", MSDelWarCounter) );//删除warCounter
		m_vecRegFns.push_back( ToRegFns( "MSGetWarCounterNum", MSGetWarCounterNum) );//获得计数器的数字
		m_vecRegFns.push_back( ToRegFns( "MSMapDisplayWarCounterInfo", MSMapDisplayWarCounterInfo) );//全地图通知计数器信息
		m_vecRegFns.push_back( ToRegFns( "MSSetWarCounterNum", MSSetWarCounterNum) );//设置计数器的数字
		m_vecRegFns.push_back( ToRegFns( "MSPlayerDisplayWarCounterInfo", MSPlayerDisplayWarCounterInfo) );//单玩家通知计数器的数字
		m_vecRegFns.push_back( ToRegFns( "MSShowNotices" , MSShowNotices) );
		m_vecRegFns.push_back( ToRegFns( "MSShowParamsNotices",MSShowParamsNotices ) );
		m_vecRegFns.push_back( ToRegFns( "MSChgComblockAttr" , MSChgComblockAttr) );//设置碰撞块属性
		m_vecRegFns.push_back( ToRegFns( "MSCreateMapBuff" , MSCreateMapBuff) );//地图上所有玩家创建buff
		m_vecRegFns.push_back( ToRegFns( "MSDelNpcByMapID" , MSDelNpcByMapID) );//地图上删除Npc
		m_vecRegFns.push_back( ToRegFns( "MSGetMapScriptTarget" , MSGetMapScriptTarget) );//取地图实例保存的脚本用数据
		m_vecRegFns.push_back( ToRegFns( "MSSetMapScriptTarget" , MSSetMapScriptTarget) );//保存地图实例脚本用数据
		m_vecRegFns.push_back( ToRegFns( "MSWarRealStart" , MSWarRealStart) );//攻城战真正开始时间
	}

	//...注册各脚本中使用的MuxLib接口函数
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	CLuaRestoreStack rs( pLuaState );//准备恢复堆栈;

	//新函数表；
	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "MuxLib" );//置全局变量"MuxLib"为新表；

	lua_getglobal( pLuaState, "MuxLib" );//push表"MuxLib"；
	//注册向量中的函数至MuxLib表；
	ToRegFns* pToReg = NULL;
	for ( vector<ToRegFns>::iterator tmpiter=m_vecRegFns.begin();
		tmpiter!=m_vecRegFns.end(); ++tmpiter )
	{
		pToReg = &(*tmpiter);
		lua_pushstring( pLuaState, pToReg->fnName );
		lua_pushnumber( pLuaState, (lua_Number)(tmpiter-m_vecRegFns.begin()) );//closure注册为vec中的序号；
		lua_pushcclosure( pLuaState, LuaCallbackMeta<CMapScriptExtent>, 1);
		lua_settable( pLuaState, -3 );//MuxLib[pToReg->fnName] = LuaCallback(with closure=本项vec序号)；
	}

	return;
}

/**
//地图扩展函数定义通用部分；
//1、判断该pState是否对应一个CMapScript脚本，如果不是，则出错返回，
//   即：必须在地图脚本中调用，且调用该脚本者必须为CMapScript对象;
*/
#define MAP_EXT_FUNPRE \
	if ( NULL == pState )\
    {\
	    return 0;\
    }\
	CMapScript* pScript = CMapScriptManager::FindScripts( pState );\
	if ( NULL == pScript  )\
	{\
		return 0;\
	}\

///副本地图脚本中设置玩家副本阶段
int CMapScriptExtent::MSSetResPlayerCopyStage( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( !lua_isnumber(pState, -1) )
	{
		D_ERROR( "CMapScriptExtent::MSSetResPlayerCopyStage，脚本参数非法" );
		return 0;
	}

	unsigned short copyStage = (unsigned short)lua_tonumber( pState, -1 );

	pScript->ExtSetResPlayerCopyStage( copyStage );

	return 0;
}

///副本地图设置所有玩家的outmapinfo;
int CMapScriptExtent::MSSetAllPlayerOutPos( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ( !lua_isnumber(pState, -1) )
		|| ( !lua_isnumber(pState, -2) )
		|| ( !lua_isnumber(pState, -3) )
		|| ( !lua_isnumber(pState, -4) )
		)
	{
		D_ERROR( "CMapScriptExtent::MSSetAllPlayerOutPos，脚本参数非法" );
		return 0;
	}

	unsigned short extent = (unsigned short)lua_tonumber( pState, -1 );
	unsigned short posY   = (unsigned short)lua_tonumber( pState, -2 );
	unsigned short posX   = (unsigned short)lua_tonumber( pState, -3 );
	unsigned short mapid  = (unsigned short)lua_tonumber( pState, -4 );

	pScript->ExtSetAllPlayerOutPos( mapid, posX, posY, extent );

	return 0;
}

///副本地图设置玩家的outmapinfo;
int CMapScriptExtent::MSSetPlayerOutPos( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ( !lua_isnumber(pState, -1) )
		|| ( !lua_isnumber(pState, -2) )
		|| ( !lua_isnumber(pState, -3) )
		|| ( !lua_isnumber(pState, -4) )
		)
	{
		D_ERROR( "CMapScriptExtent::MSSetPlayerOutPos，脚本%d参数非法\n", pScript->GetID() );
		return 0;
	}

	unsigned short extent = (unsigned short)lua_tonumber( pState, -1 );
	unsigned short posY   = (unsigned short)lua_tonumber( pState, -2 );
	unsigned short posX   = (unsigned short)lua_tonumber( pState, -3 );
	unsigned short mapid  = (unsigned short)lua_tonumber( pState, -4 );

	pScript->ExtSetPlayerOutPos( mapid, posX, posY, extent );

	return 0;
}

///副本地图踢相应玩家；
int CMapScriptExtent::MSKickResPlayer( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	pScript->ExtKickResPlayer();

	return 0;
}

///副本结束(同时踢所有玩家)；
int CMapScriptExtent::MSEndCopy( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	pScript->ExtEndCopy();

	return 0;
}

///设置某伪副本可进；
int CMapScriptExtent::MSPCopyOpen( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	pScript->ExtPCopyOpen();

	return 0;
}

///设置某伪副本不可进；
int CMapScriptExtent::MSPCopyClose( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	pScript->ExtPCopyClose();

	return 0;
}

///地图脚本调整对应玩家位置(例如进入地图玩家等)
int CMapScriptExtent::MSSetResPlayerPos( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ( !lua_isnumber(pState, -1) )
		|| ( !lua_isnumber(pState, -2) )
		|| ( !lua_isnumber(pState, -3) )
		)
	{
		D_ERROR( "CMapScriptExtent::MSSetResPlayerPos，脚本参数非法" );
		return 0;
	}

	unsigned short extent = (unsigned short)lua_tonumber( pState, -1 );
	unsigned short posY = (unsigned short)lua_tonumber( pState, -2 );
	unsigned short posX = (unsigned short)lua_tonumber( pState, -3 );

	pScript->ExtSetResPlayerPos( posX, posY, extent );

	return 0;
}

///设置本地图的TD标记；
int CMapScriptExtent::MSSetTDFlag( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	pScript->ExtSetTDFlag();

	return 0;
}

///地图脚本通知对应玩家消息(例如进入地图的玩家，离开地图的玩家等)
int CMapScriptExtent::MSNotiResPlayerMsg( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ! lua_isstring( pState, -1 ) )
	{
		D_ERROR( "CMapScriptExtent::MSNotiResPlayerMsg，脚本参数非字符串" );
		return 0;
	}

	const char* notimsg = lua_tostring( pState, -1 );

	pScript->ExtNotiResPlayerMsg( notimsg );

	return 0;
}

int CMapScriptExtent::MSWorldNotify( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ! lua_isstring( pState, -1 ) )
	{
		D_ERROR( "CMapScriptExtent::MSWorldNotify，脚本参数非字符串" );
		return 0;
	}

	const char* notimsg = lua_tostring( pState, -1 );

	if ( NULL != notimsg )
	{
		pScript->ExtNotifyWorldChat( notimsg );
	}

	return 0;
}


int CMapScriptExtent::MSDeleteTimer( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		D_ERROR( "CMapScriptExtent::MSDeleteTimer，脚本参数非数字" );
		return 0;
	}

	unsigned int timerId = (unsigned int)lua_tonumber( pState, -1 );

	pScript->ExtDeleteTimer( timerId );
	return 0;
}


int CMapScriptExtent::MSDeleteDetailTimer( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) )
	{
		D_ERROR( "CMapScriptExtent::MSDeleteDetailTimer，脚本参数非数字" );
		return 0;
	}

	unsigned int timerId = (unsigned int)lua_tonumber( pState, -1 );

	pScript->ExtDeleteDetailTimer( timerId );
	return 0;
}


int CMapScriptExtent::MSNotifyWarTaskState( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ! lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR( "CMapScriptExtent::MSNotifyWarTaskState，脚本参数非数字" );
		return 0;
	}

	unsigned int taskId = (unsigned int)( lua_tonumber(pState, -2) );
	ETaskStatType taskState = (ETaskStatType)((unsigned int)( lua_tonumber( pState, -1 ) ));

	pScript->ExtNotifyWarTaskState( taskId, taskState );
	return 0;
}


int CMapScriptExtent::MSIsInAttackWar( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	bool bRet = pScript->ExtIsAttackWar();
	lua_pushboolean( pState, bRet );
	return 1;
}


int CMapScriptExtent::MSCreateNpcBySet( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) || !lua_isnumber( pState, -3 ))
	{
		D_ERROR("CreateNpcBySet 输入参数错误\n");
		return 0;
	}

	unsigned int npcTypeId = (unsigned int)lua_tonumber( pState, -3 );
	unsigned int createSetID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int npcNum = (unsigned int)lua_tonumber( pState, -1 );

	D_DEBUG( "MSCreateNpcBySet,脚本指令刷NPC%d,%d,%d\n", npcTypeId, createSetID, npcNum );

	bool bRet = pScript->ExtCreateNpcBySet( npcTypeId, createSetID, npcNum );
	lua_pushboolean( pState, bRet );
	return 1;
}

//获取最后一刀种族
int CMapScriptExtent::MSGetLastKillRace( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	
	unsigned int race = pScript->GetNpcKillerRace();
	lua_pushnumber( pState, race );
	return 1;
}


//修改地图归属种族
int CMapScriptExtent::MSModifyMapRace( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("MSModifyMapRace 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int race = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ModifyMapRace( race );
	lua_pushboolean( pState, bRet );
	return 1;
}


//获取地图归属种族
int CMapScriptExtent::MSGetMapRace( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	unsigned int race = pScript->GetMapRace();
	lua_pushnumber( pState, race );
	return 1;
}


int CMapScriptExtent::MSGetCityLevel( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	unsigned int cityLevel = pScript->GetCityLevel();
	lua_pushnumber( pState, cityLevel );
	return 1;
}


int CMapScriptExtent::MSSetItemNpcStatus( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("MSSetItemNpcStatus 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int status = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int itemSeq = (unsigned int)lua_tonumber( pState, -2 );

	bool bRet = pScript->SetItemNpcStatus( itemSeq, status );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CMapScriptExtent::MSModifyRebirthPtRace( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("MSModifyRebirthPtRace 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int mapID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int race = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ModifyRebirthRace( mapID, race );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CMapScriptExtent::MSOpenCreator( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("MSOpenCreator 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int creatorID = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->OpenNpcCreator( creatorID );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CMapScriptExtent::MSCloseCreator( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("MSCloseCreator 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int creatorID = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->CloseNpcCreator( creatorID );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CMapScriptExtent::MSModifyTelpotRace( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ))
	
	{
		D_ERROR("MSModifyTelpotRace 输入参数错误\n");
		return 0;
	}
	
	unsigned int mapID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int race = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ModifyTelpotRace( mapID, race );
	lua_pushboolean( pState, bRet );
	return 1;
}


//添加buff
int CMapScriptExtent::MSCreateBuff( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	
	{
		D_ERROR("MSCreateBuff 输入参数错误\n");
		return 0;
	}

	unsigned int buffID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int buffTime = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->CreateBuff( buffID, buffTime );
	lua_pushboolean( pState, bRet );
	return 1;
}



//删除buff
int CMapScriptExtent::MSDeleteBuff( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ))
	
	{
		D_ERROR("MSDeleteBuff 输入参数错误\n");
		return 0;
	}

	unsigned int buffID = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->DeleteBuff( buffID );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CMapScriptExtent::MSKillPlayerByRace( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ))
	{
		D_ERROR("MSKillPlayerByRace 输入参数错误\n");
		return 0;
	}

	unsigned int race = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->KillAllPlayerByRace( race );
	lua_pushboolean( pState, bRet );
	return 1;
}


//删除玩家身上某道具
int CMapScriptExtent::MSDelPlayerItem( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ))
	{
		D_ERROR("MSDelPlayerItem 参数错误\n");
		return 0;
	}

	unsigned int itemID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int num = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->RemoveUserItem( itemID, num );
	lua_pushboolean( pState, bRet );
	return 1;
}
	

//获得当前玩家姓名
int CMapScriptExtent::MSGetScriptUserName( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	const char* pName = pScript->GetScriptUserName();
	if( NULL == pName )
	{
		D_ERROR("当前脚本的相应函数不能获取UserName\n");
		return 0;
	}

	lua_pushstring( pState, pName );
	return 1;
}



//传送玩家
int CMapScriptExtent::MSSwitchMapPlayer( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) || !lua_isnumber( pState, -3 ) || !lua_isnumber( pState, -4 ) || !lua_isnumber( pState, -5 ) || !lua_isnumber( pState, -6 ))
	{
		D_ERROR("MSSwichMapPlayer 参数错误\n");
		return 0;
	}

	unsigned int race = (unsigned int)lua_tonumber( pState, -6 );
	unsigned int mapID = (unsigned int)lua_tonumber( pState, -5 );
	unsigned int posX1 = (unsigned int)lua_tonumber( pState, -4 );
	unsigned int posY1 = (unsigned int)lua_tonumber( pState, -3 );
	unsigned int posX2 = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int posY2 = (unsigned int)lua_tonumber( pState, -1 );

	D_INFO("Enter CMapScriptExtent::MSSwitchMapPlayer race:%d mapID:%d posX1:%d posY1:%d posX2:%d posY2:%d\n",race,mapID,posX1,posY1,posX2,posY2);

	bool bRet = pScript->SwitchMapPlayer( race, mapID, posX1, posY1, posX2, posY2 );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CMapScriptExtent::MSIsDeclareWar( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ))
	
	{
		D_ERROR("MSIsDeclareWar 输入参数错误\n");
		return 0;
	}

	unsigned int atkRace = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int defRace = (unsigned int)lua_tonumber( pState, -1 );
	
	bool bRet = AllRaceMasterManagerSingle::instance()->IsDecalreWar( atkRace, defRace );
	lua_pushboolean( pState, bRet );

	return 1;
}


//创建WarTimer
int CMapScriptExtent::MSAddWarTimer( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -6 ) || !lua_isnumber( pState, -5 ) || !lua_isnumber( pState, -4 ) || !lua_isnumber( pState, -3 ) || !lua_isnumber( pState, -2 ) || !lua_isstring( pState, -1 ) )
	
	{
		D_ERROR("MSAddWarTimer 输入参数错误\n");
		return 0;
	}

	unsigned int timerID = (unsigned int)lua_tonumber( pState, -6 );
	unsigned int leftSecond = (unsigned int)lua_tonumber( pState, -5 );
	unsigned int startInformTime = (unsigned int)lua_tonumber( pState, -4 );
	unsigned int isInformClient = (unsigned int)lua_tonumber( pState, -3 );
	unsigned int isClockWise = (unsigned int)lua_tonumber( pState, -2 );
	const char* timerInfo = lua_tostring( pState, -1 );
	if ( NULL == timerInfo )
	{
		return 0;
	}


	bool bRet = pScript->AddWarTimer( timerID, leftSecond, startInformTime, isClockWise, ( isInformClient != 0),timerInfo );
	lua_pushboolean( pState, bRet );
	return 1;
}


//删除WarTimer
int CMapScriptExtent::MSDelWarTimer( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 )  )
	
	{
		D_ERROR("MSDelWarTimer 输入参数错误\n");
		return 0;
	}

	unsigned int timerID = (unsigned int)lua_tonumber( pState, -1 );
	
	//bool bRet = pScript->DelWarTimer( timerID );
	bool bRet = true;
	CManNormalMap::OnDelWarTimer( timerID );
	CManMuxCopy::OnDelWarTimer( timerID );
	lua_pushboolean( pState, bRet );
	return 1;
}


//创建WarCounter
int CMapScriptExtent::MSAddWarCounter( lua_State* pState )
{
		MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -3 ) ||  !lua_isnumber( pState, -2 ) || !lua_isstring( pState, -1 ) )
	
	{
		D_ERROR("MSAddWarCounter 输入参数错误\n");
		return 0;
	}

	unsigned int counterID = (unsigned int)lua_tonumber( pState, -3 );
	unsigned int counterNum = (unsigned int)lua_tonumber( pState, -2 );
	const char* szCounterInfo = lua_tostring( pState, -1 );
	if ( NULL == szCounterInfo )
	{
		return 0;
	}

	bool bRet = pScript->AddWarCounter( counterID, counterNum, szCounterInfo );
	lua_pushboolean( pState, bRet );
	return 1;

}


//删除warCounter
int CMapScriptExtent::MSDelWarCounter( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 )  )
	
	{
		D_ERROR("MSDelWarCounter 输入参数错误\n");
		return 0;
	}

	unsigned int counterID = (unsigned int)lua_tonumber( pState, -1 );
	
	bool bRet = pScript->DelWarCounter( counterID );
	lua_pushboolean( pState, bRet );
	return 1;
}


//获得计数器的数字
int CMapScriptExtent::MSGetWarCounterNum( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 )  )
	{
		D_ERROR("MSGetWarCounterNum 输入参数错误\n");
		return 0;
	}

	unsigned int counterID = (unsigned int)lua_tonumber( pState, -1 );
	
	unsigned int count = pScript->GetWarCounterNum( counterID );
	lua_pushnumber( pState, count );
	return 1;
}


//全地图通知计数器信息
int CMapScriptExtent::MSMapDisplayWarCounterInfo( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 )  )
	{
		D_ERROR("MSMapDisplayWarCounterInfo 输入参数错误\n");
		return 0;
	}

	unsigned int counterID = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->MapDisplayWarCounterInfo( counterID );
	return 1;
}



//设置计数器的数字
int CMapScriptExtent::MSSetWarCounterNum( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("MSSetWarCounterNum 输入参数错误\n");
		return 0;
	}

	unsigned int counterID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int counterNum = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->SetWarCounterNum( counterID, counterNum );
	lua_pushboolean( pState, bRet );
	return 1;
}


//单玩家通知计数器的数字
int CMapScriptExtent::MSPlayerDisplayWarCounterInfo( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("MSPlayerDisplayWarCounterInfo 输入参数错误\n");
		return 0;
	}
	
	unsigned int counterID = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->PlayerDisplayWarCounterInfo( counterID );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CMapScriptExtent::MSShowNotices(lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1) ||
		!lua_isnumber( pState, -2) ||
		!lua_isnumber( pState, -3) ||
		!lua_isnumber( pState, -4) ||
		!lua_isstring( pState, -5 )
		)
	{
		D_ERROR("NpcShowNotices 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int distancetime  = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int maxshowcount  = (unsigned int)lua_tonumber( pState, -2 );
	char isgolabl  = (char)lua_tonumber( pState, -3 );
	char _position = (char)lua_tonumber( pState, -4 );
	const char* content= (char *)lua_tostring( pState, -5 );
	if ( NULL == content )
	{
		return 0;
	}

	bool bRet = pScript->ExtShowNotice( content,_position, isgolabl, maxshowcount, distancetime );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CMapScriptExtent::MSShowParamsNotices(lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if (  !lua_isnumber( pState,-10)
		||!lua_isnumber( pState,-9)
		||!lua_isnumber( pState,-8)
		||!lua_isnumber( pState,-7)
		||!lua_isnumber( pState,-6) 
	 )
	{
		D_ERROR("CMapScriptExtent::MSShowParamsNotices 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int validparamcount = 0;
	const char* param5 = lua_tostring( pState, -1 );
	if ( param5 ) validparamcount++;
	const char* param4 = lua_tostring( pState, -2 );
	if ( param4 ) validparamcount++;
	const char* param3 = lua_tostring( pState, -3 );
	if ( param3 ) validparamcount++;
	const char* param2 = lua_tostring( pState, -4 );
	if ( param2 ) validparamcount++;
	const char* param1 = lua_tostring( pState, -5 );
	if ( param1 ) validparamcount++;

	unsigned int distancetime = (unsigned int)lua_tonumber( pState,-6 ); 
	unsigned int showmaxcount = (unsigned int)lua_tonumber( pState,-7 );
	char isglobal = (char)lua_tonumber( pState,  -8 );
	char _position = (char)lua_tonumber( pState, -9 );
	unsigned int msgid =(unsigned int)lua_tonumber( pState, -10 );

	bool bRet = pScript->ExtShowParamNotice( msgid, _position, isglobal, showmaxcount,  distancetime, validparamcount, param1, param2, param3, param4, param5 );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CMapScriptExtent::MSChgComblockAttr( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("MSChgComblockAttr 输入参数错误\n");
		return 0;
	}

	unsigned int regionID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int val = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtSetBlockQuad( regionID, val );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CMapScriptExtent::MSCreateMapBuff( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) || !lua_isnumber( pState, -3 ) )
	{
		D_ERROR("MSCreateMapBuff 输入参数错误\n");
		return 0;
	}

	unsigned int race = (unsigned int)lua_tonumber( pState, -3 );
	unsigned int buffID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int buffTime = (unsigned int)lua_tonumber( pState, -1 );
	
	bool bRet = pScript->CreateMapBuff( race, buffID, buffTime );
	lua_pushboolean( pState, bRet );
	return 1;
}

//取地图实例保存的脚本用数据
int CMapScriptExtent::MSGetMapScriptTarget( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	//无调用参数，if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) || !lua_isnumber( pState, -3 ) )
	//{
	//	D_ERROR("MSGetMapScriptTarget 输入参数错误\n");
	//	return 0;
	//}

	int getVal = 0;
	bool isok = pScript->ExtGetMapScriptTarget( getVal );
	lua_pushnumber( pState, getVal );
	if ( !isok )
	{
		D_ERROR( "不可能错误，ExtGetMapScriptTarget失败\n" );//依然往下执行，防止脚本取不到返回值发生异常；
	}
	return 1;
}

//保存地图实例脚本用数据；
int CMapScriptExtent::MSSetMapScriptTarget( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("MSSetMapScriptTarget 输入参数错误\n");
		return 0;
	}

	int tosetval = (int)lua_tonumber( pState, -1 );
	pScript->ExtSetMapScriptTarget( tosetval );

	return 0;
}

int CMapScriptExtent::MSWarRealStart( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	pScript->ExtWarRealStart( );

	return 0;
}

///地图内广播；
int CMapScriptExtent::MSBrocastMsg( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ! lua_isstring( pState, -1 ) )
	{
		D_ERROR( "CMapScriptExtent::MSBrocastMsg，脚本参数非字符串" );
		return 0;
	}

	const char* notimsg = lua_tostring( pState, -1 );
	if ( NULL == notimsg )
	{
		return 0;
	}

	pScript->ExtBrocastMsg( notimsg );

	return 0;
}

///地图脚本计数器添加
int CMapScriptExtent::MSAddCounterOfMap( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	//堆栈序检查输入参数；
	if ( ( ! lua_isnumber( pState, -1 ) )
		|| ( ! lua_isnumber( pState, -2 ) )
		|| ( ! lua_isnumber( pState, -3 ) )
		|| ( ! lua_isnumber( pState, -4 ) )
		|| ( ! lua_isnumber( pState, -5 ) )
		)
	{
		D_ERROR( "CMapScriptExtent::MSAddCounterOfMap，脚本输入参数错误" );
		return 0;
	}
	
	unsigned int targetCount  = (unsigned int) ( lua_tonumber( pState, -1 ) );
	unsigned int counterIdMax = (unsigned int) ( lua_tonumber( pState, -2 ) );
	unsigned int counterIdMin = (unsigned int) ( lua_tonumber( pState, -3 ) );
	unsigned int counterType  = (unsigned int) ( lua_tonumber( pState, -4 ) );
	unsigned int counterClsId = (unsigned int) ( lua_tonumber( pState, -5 ) );

	pScript->ExtAddCounterOfMap( counterClsId, counterType, counterIdMin, counterIdMax, targetCount );

	return 0;
}

///地图脚本计时器添加
int CMapScriptExtent::MSAddTimerOfMap( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	//堆栈序检查输入参数；
	if ( ( ! lua_isnumber( pState, -1 ) )
		|| ( ! lua_isnumber( pState, -2 ) )
		)
	{
		D_ERROR( "CMapScriptExtent::MSAddTimerOfMap，脚本输入参数错误" );
		return 0;
	}
	
	unsigned int resTime  = (unsigned int) ( lua_tonumber( pState, -1 ) );//相对时刻，单位分钟；
	unsigned int timerClsID  = (unsigned int) ( lua_tonumber( pState, -2 ) );

	pScript->ExtAddTimerOfMap( timerClsID, resTime );

	return 0;
}

int CMapScriptExtent::MSIsHaveTimerOfMap( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	//堆栈序检查输入参数；
	if ( ( ! lua_isnumber( pState, -1 ) ) )
	{
		D_ERROR( "CMapScriptExtent::MSIsHaveTimerOfMap，脚本输入参数错误" );
		return 0;
	}

	unsigned int timerClsID  = (unsigned int) ( lua_tonumber( pState, -1 ) );

	bool bRet = pScript->ExtIsHaveTimerOfMap( timerClsID );
	lua_pushboolean( pState, bRet );
	return 0;
}


int CMapScriptExtent::MSAddDetailTimerOfMap( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	//堆栈序检查输入参数；
	if ( ( ! lua_isnumber( pState, -1 ) )
		|| ( ! lua_isnumber( pState, -2 )) 
		|| ( ! lua_isnumber( pState, -3 ))
		)   // id 小时 分钟 3参
	{
		D_ERROR( "CMapScriptExtent::MSAddDetailTimerOfMap，脚本输入参数错误" );
		return 0;
	}
	
	unsigned int minute  = (unsigned int) ( lua_tonumber( pState, -1 ) );//相对时刻，单位分钟；
	unsigned int hour  = (unsigned int) ( lua_tonumber( pState, -2 ) );		//小时
	unsigned int timerId = (unsigned int)( lua_tonumber( pState, -3 ));	//TIMER id

	pScript->ExtAddDetailTimerOfMap( timerId, hour, minute );

	return 0;
}


int CMapScriptExtent::MSAttackCityEnd( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ( ! lua_isnumber( pState, -1 ) ) )
	{
		D_ERROR("CMapScriptExtent::MSAttackCityEnd 脚本输入参数错误");
		return 0;
	}

	unsigned int taskId = (unsigned int) ( lua_tonumber( pState, -1 ) );
	pScript->ExtAttackWarEnd( taskId );
	return 0;
}

int CMapScriptExtent::MSCreateNpc( lua_State* pState )
{
	MAP_EXT_FUNPRE;
	
	if ( ( ! lua_isnumber( pState, -1 ) )
		|| ( ! lua_isnumber( pState, -2 )) 
		|| ( ! lua_isnumber( pState, -3 ))
		)   // id 小时 分钟 3参
	{
		D_ERROR( "CMapScriptExtent::MSCreateNpc，脚本输入参数错误" );
		return 0;
	}

	unsigned short posY = (unsigned int)( lua_tonumber( pState, -1 ) );
	unsigned short posX = (unsigned int )(lua_tonumber( pState, -2 ));
	unsigned int npcTypeId = (unsigned int)( lua_tonumber( pState, -3 ) );

	D_DEBUG( "MSCreateNpc,脚本指令刷NPC%d,坐标(%d, %d)\n", npcTypeId, posX, posY );	

	pScript->ExtCreateNpc( npcTypeId, posX, posY );
	return 0;
}


int	CMapScriptExtent::MSDelNpc( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ( ! lua_isnumber( pState, -1 ) ) )
	{
		D_ERROR("CMapScriptExtent::MSDelNpc 脚本输入参数错误");
		return 0;
	}

	unsigned int npcTypeId = (unsigned int)(lua_tonumber( pState, -1 ) );

	D_DEBUG( "MSDelNpc,脚本指令删NPC%d\n", npcTypeId );	

	pScript->ExtDelNpc( npcTypeId );

	return 0;
}


//该mapsrv上的map删除
int CMapScriptExtent::MSDelNpcByMapID( lua_State* pState )
{
	MAP_EXT_FUNPRE;

	if ( ( ! lua_isnumber( pState, -1 ) ) || ( ! lua_isnumber( pState, -2 ) ) )
	{
		D_ERROR("CMapScriptExtent::MSDelNpcByMapID 脚本输入参数错误");
		return 0;
	}

	unsigned int mapID = (unsigned int)(lua_tonumber( pState, -2 ) );
	unsigned int npcTypeId = (unsigned int)(lua_tonumber( pState, -1 ) );

	bool bRet = pScript->ExtDelNpcByMapID( mapID, npcTypeId );

	D_DEBUG( "MSDelNpcByMapID,脚本指令删NPC%d,mapID%d\n", npcTypeId, mapID );	
	lua_pushboolean( pState, bRet );
	return 1;
}

#endif //STAGE_MAP_SCRIPT
///...地图脚本扩展
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///初始化，注册各战斗计算以及战斗判断用扩展函数至lua；
void CFightScriptExtent::Init( lua_State* pLuaState )
{
	if ( !m_isvecInited )
	{
		m_isvecInited = true;
		//待注册函数入vector;
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//注册各脚本中使用的MuxLib接口函数...
		//目前不同类型脚本使用同一组接口函数，虽然某类脚本实际上只能使用这些接口函数中的一小部分
		//以后拟改为特定类型脚本只注册特定组别函数，以防止误用，并允许同名函数在不同类脚本中的物理意义不一样，
		//例如：在战斗判断脚本中只能使用战斗判断相关扩展接口而不能使用战斗计算相关脚本
		//      又如，对于名字MuxLib.GetAttackPlayerLevel，其实际注册函数将随着注册者的脚本类型不同而不同，从而最终调用不同的实现；

		m_vecRegFns.push_back( ToRegFns( "Debug", Debug ) );//调试信息打印；

		///战斗判断脚本接口
		m_vecRegFns.push_back( ToRegFns( "FPreGetAttackPlayerLevel", FPreGetAttackPlayerLevel ) );//战斗判断，取攻击玩家等级;
		m_vecRegFns.push_back( ToRegFns( "FPreGetTargetMonsterType", FPreGetTargetMonsterType ) );//战斗判断，取攻击目标怪物HP;
		m_vecRegFns.push_back( ToRegFns( "FPreGetTargetPlayerLevel", FPreGetTargetPlayerLevel ) );//战斗判断，取目标玩家等级;
		m_vecRegFns.push_back( ToRegFns( "FPreGetTargetPlayerRace",  FPreGetTargetPlayerRace) );//战斗判断，取目标玩家阵营;
		m_vecRegFns.push_back( ToRegFns( "FPreGetAttackPlayerRace",  FPreGetAttackPlayerRace) );//战斗判断，取攻击玩家阵营;
		m_vecRegFns.push_back( ToRegFns( "FPreGetAttackPlayerBattleMode",  FPreGetAttackPlayerBattleMode) );//战斗判断，取攻击玩家战斗模式

		///战斗计算公式脚本接口  ///////////////////////攻击者：玩家////////////////////
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerLevel", FCalGetAtkPlayerLevel ) );//1战斗判断，取攻击玩家等级;
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMinPhyAttack", FCalGetAtkPlayerMinPhyAttack ) );//2 战斗判断，最小物理攻击
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMaxPhyAttack", FCalGetAtkPlayerMaxPhyAttack ) );//3 战斗判断， 最大装备物理攻击
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMinMagAttack", FCalGetAtkPlayerMinMagAttack ) );//4 获取攻击者装备最小魔法攻击
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMaxMagAttack", FCalGetAtkPlayerMaxMagAttack ) );//5 获取攻击者装备最大魔法攻击
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerClass", FCalGetAtkPlayerClass ) );//6 获取攻击者的职业
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkSkillCriRate", FCalGetAtkSkillCriRate ) );//7 获取攻击者攻击技能的暴击率
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkSkillAttackRate", FCalGetAtkSkillAttackRate ) );//8 获取攻击者攻击技能的攻击系数
		m_vecRegFns.push_back( ToRegFns( "FCalGetAdditionalSkillDamage", FCalGetAdditionalSkillDamage ) );//9 获取攻击技能的附加伤害
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerStr", FCalGetAtkPlayerStr ) );//10 获取攻击者的STR
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerAgi", FCalGetAtkPlayerAgi ) );//11 获取攻击者的AGI
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerInt", FCalGetAtkPlayerInt ) );//12 获取攻击者的INT
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerPhyAttack", FCalGetAtkPlayerPhyAttack ) );//13 获取攻击者的物理攻击
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMagAttack", FCalGetAtkPlayerMagAttack ) );//14 获取攻击者的魔法攻击
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerPhyDefend", FCalGetAtkPlayerPhyDefend ) );//15 获取攻击者的物理防御
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMagDefend", FCalGetAtkPlayerMagDefend ) );//16 获取攻击者的魔法防御
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerPhyHit", FCalGetAtkPlayerPhyHit ) );//17 获取攻击者的物理命中
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMagHit", FCalGetAtkPlayerMagHit ) );//18 获取攻击者的魔法命中
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerPhyCriLevel", FCalGetAtkPlayerPhyCriLevel ) );//19 获取攻击者的物理暴击等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMagCriLevel", FCalGetAtkPlayerMagCriLevel ) );//20 获取攻击者的魔法暴击等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerPhyAttackRate", FCalGetAtkPlayerPhyAttackRate ) );//21 获取攻击者的物理攻击百分比
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMagAttackRate", FCalGetAtkPlayerMagAttackRate ) );//22 获取攻击者的魔法攻击百分比
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerAdditionPhyCriDamage", FCalGetAtkPlayerAdditionPhyCriDamage ) );//23 获取攻击者附加物理暴击伤害
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerAdditionMagCriDamage", FCalGetAtkPlayerAdditionMagCriDamage ) );	//24 获取攻击者附加魔法暴击伤害
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerPhyDamageUpRate", FCalGetAtkPlayerPhyDamageUpRate ) );//25 获取攻击者物理伤害提升百分比 
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMagDamageUpRate", FCalGetAtkPlayerMagDamageUpRate ) );//26 获取攻击者魔法伤害提升百分比
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerAdditionalPhyCriRate", FCalGetAtkPlayerAdditionalPhyCriRate ) );//27 获取BUFF附加提升物理暴击率
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerAdditionalMagCriRate", FCalGetAtkPlayerAdditionalMagCriRate ) );//28 获取BUFF附加提升魔法暴击率
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerPhyCriDamageUpRate", FCalGetAtkPlayerPhyCriDamageUpRate ) );//29 获取技能附加物理暴击伤害提升百分比
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMagCriDamageUpRate", FCalGetAtkPlayerMagCriDamageUpRate ) );//30 获取技能附加魔法暴击伤害提升百分比
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerPhyRiftLevel", FCalGetAtkPlayerPhyRiftLevel ) );//31 获取攻击者物理穿透等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMagRiftLevel", FCalGetAtkPlayerMagRiftLevel ) );//32 获取攻击者魔法穿透等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerAdditionalPhyDamage", FCalGetAtkPlayerAdditionalPhyDamage ) );//33 获取攻击者额外物理伤害
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerAdditionalMagDamage", FCalGetAtkPlayerAdditionalMagDamage ) );////34 获取攻击者额外魔法伤害
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkSkillType", FCalGetAtkSkillType ) );//35 获取攻击技能的类型 物理 魔法
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkSkillAdditionCriDamage", FCalGetAtkSkillAdditionCriDamage ) );//36 获取攻击技能的暴击附加伤害
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkSkillID", FCalGetAtkSkillID ) );//37 获取攻击技能的ID号
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkSkillCriDamageRate", FCalGetAtkSkillCriDamageRate ) );//38 获取攻击技能的暴击伤害系数
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerPhyHitRate", FCalGetAtkPlayerPhyHitRate ) );//39 获取人物的物理命中率
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkPlayerMagHitRate", FCalGetAtkPlayerMagHitRate ) );//40 获取人物的魔法命中率
		///////////////////////////////////被攻击者：玩家////////////////////////////////////////////////////////////////////////////////
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerLevel", FCalGetTgtPlayerLevel ) );//1 获取被攻击玩家的等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerPhyDefend", FCalGetTgtPlayerPhyDefend ) );//2 获取被攻击玩家的物理防御
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerMagDefend", FCalGetTgtPlayerMagDefend ) );//3 获取被攻击玩家的魔法防御
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerPhyDefendRate", FCalGetTgtPlayerPhyDefendRate ) );//4 获取被攻击玩家的物理防御百分比
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerMagDefendRate", FCalGetTgtPlayerMagDefendRate ) );//5 获取被攻击玩家的魔法防御百分比
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerPhyCriDerateLevel", FCalGetTgtPlayerPhyCriDerateLevel ) );//6 获取被攻击玩家的减免物理暴击等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerMagCriDerateLevel", FCalGetTgtPlayerMagCriDerateLevel ) );//7 获取被攻击玩家的减免魔法暴击等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerPhyCriDamageDerate", FCalGetTgtPlayerPhyCriDamageDerate ) );//8 获取被攻击者物理暴击伤害减免
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerMagCriDamageDerate", FCalGetTgtPlayerMagCriDamageDerate ) );//9 获取被攻击者魔法暴击伤害减免
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerPhyJouk", FCalGetTgtPlayerPhyJouk ) );//10 获取被攻击者物理闪避等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerMagJouk", FCalGetTgtPlayerMagJouk ) );//11 获取被攻击者魔法闪避等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerPhyDamageDerate", FCalGetTgtPlayerPhyDamageDerate ) );//12 获取被攻击者物理伤害减免
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerMagDamageDerate", FCalGetTgtPlayerMagDamageDerate ) );//13 获取被攻击者魔法伤害减免
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerPhyJoukRate", FCalGetTgtPlayerPhyJoukRate ) );//14 获取被攻击者物理闪避百分比
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerMagJoukRate", FCalGetTgtPlayerMagJoukRate ) );//15 获取被攻击者魔法闪避百分比
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerStr", FCalGetTgtPlayerStr ) );//16 获取被攻击者的STR
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerAgi", FCalGetTgtPlayerAgi ) );//17 获取被攻击的AGI
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerInt", FCalGetTgtPlayerInt ) );//18 获取被攻击者的INT
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtPlayerClass", FCalGetTgtPlayerClass ) );//18 获取被攻击者的INT
		/////////////////////////////////////////////////////////攻击者：怪物//////////////////////////////////////////
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterLevel", FCalGetAtkMonsterLevel ) );//1 攻击怪物的等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterSkillType", FCalGetAtkMonsterSkillType ) );//2 攻击怪物的技能类型
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterSkillHit", FCalGetAtkMonsterSkillHit ) );//3 攻击怪物的技能命中
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterSkillAddHit", FCalGetAtkMonsterSkillAddHit ) );//4 怪物攻击技能的附加命中
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterSkillCriRate", FCalGetAtkMonsterSkillCriRate ) );//5 怪物攻击技能的暴击率
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterSkillMinAtk", FCalGetAtkMonsterSkillMinAtk ) );//6 怪物攻击技能的最小攻击
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterSkillMaxAtk", FCalGetAtkMonsterSkillMaxAtk ) );//7 怪物攻击技能的最大攻击
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterSkillID", FCalGetAtkMonsterSkillID ) );//8 怪物攻击技能的ID号
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterRank", FCalGetAtkMonsterRank ) );//9 怪物的Rank
		m_vecRegFns.push_back( ToRegFns( "FCalGetAtkMonsterSkillAdditionDamage", FCalGetAtkMonsterSkillAdditionDamage ) );//10 怪物技能的伤害追加
		//////////////////////////////////////////////////被攻击者：怪物//////////////////////////////////
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtMonsterLevel", FCalGetTgtMonsterLevel ) );//1 被攻击者怪物的等级
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtMonsterPhyDef", FCalGetTgtMonsterPhyDef ) );//2 被攻击怪物的物理防御
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtMonsterMagDef", FCalGetTgtMonsterMagDef ) );//3 被攻击怪物的魔法防御
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtMonsterPhyJouk", FCalGetTgtMonsterPhyJouk ) );//4 被攻击怪物的物理回避
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtMonsterMagJouk", FCalGetTgtMonsterMagJouk ) );//5 被攻击怪物的魔法回避
		m_vecRegFns.push_back( ToRegFns( "FCalGetTgtMonsterRank", FCalTgtMonsterRank ) );//6 被攻击怪物的rank
		///////////////////////////////////////////////////通用接口/////////////////////////////////////////////
		m_vecRegFns.push_back( ToRegFns( "FCalRandRate", FCalRandRate ) );//1  输入0~1的值 返回true or false
		m_vecRegFns.push_back( ToRegFns( "FCalRandNumber", FCalRandNumber ) );//2  返回a到b中间的任何一值	
		//...注册各脚本中使用的MuxLib接口函数
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

	CLuaRestoreStack rs( pLuaState );//准备恢复堆栈;

	//新函数表；
	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "MuxLib" );//置全局变量"MuxLib"为新表；

	lua_getglobal( pLuaState, "MuxLib" );//push表"MuxLib"；
	//注册向量中的函数至MuxLib表；
	ToRegFns* pToReg = NULL;
	for ( vector<ToRegFns>::iterator tmpiter=m_vecRegFns.begin();
		tmpiter!=m_vecRegFns.end(); ++tmpiter )
	{
		pToReg = &(*tmpiter);
		lua_pushstring( pLuaState, pToReg->fnName );
		lua_pushnumber( pLuaState, (lua_Number)(tmpiter-m_vecRegFns.begin()) );//closure注册为vec中的序号；
		lua_pushcclosure( pLuaState, LuaCallbackMeta<CFightScriptExtent>, 1);
		lua_settable( pLuaState, -3 );//MuxLib[pToReg->fnName] = LuaCallback(with closure=本项vec序号)；
	}

	return;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///战斗判断脚本扩展...

/**
//战斗判断扩展函数定义通用部分；
//1、判断该pState是否对应一个CFightPreScript脚本，如果不是，则出错返回，
//   即：必须在FightPre脚本中调用，且调用该脚本者必须为CFightPreScript对象;
*/
#define FPRE_EXT_FUNPRE \
	if ( NULL == pState )\
    {\
	    return 0;\
    }\
	CFightPreScript* pScript = CFightPreScriptManager::FindScripts( pState );\
	if ( NULL == pScript  )\
	{\
		return 0;\
	}\

///战斗判断，取被攻击玩家的阵营;
int CFightScriptExtent::FPreGetTargetPlayerRace( lua_State* pState )
{
	FPRE_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if ( NULL != pPlayer )
	{
		unsigned short usRace = pPlayer->GetRace();
		lua_pushnumber( pState, usRace );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗判断脚本错误，实际攻击者不是玩家，返回玩家种族0" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


///战斗判断，取攻击玩家的等级
int CFightScriptExtent::FPreGetAttackPlayerLevel( lua_State* pState )
{
	FPRE_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackerPlayer();
	if ( NULL != pPlayer )
	{
		int playerLevel = pPlayer->GetLevel();
		lua_pushnumber( pState, playerLevel );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗判断脚本错误，实际攻击者不是玩家，返回玩家等级0" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


int CFightScriptExtent::FPreGetAttackPlayerRace( lua_State* pState )
{
	FPRE_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackerPlayer();
	if ( NULL != pPlayer )
	{
		unsigned short usRace = pPlayer->GetRace();
		lua_pushnumber( pState, usRace );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗判断脚本错误，实际攻击者不是玩家，返回玩家等级0" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


int CFightScriptExtent::FPreGetTargetPlayerLevel( lua_State* pState )
{
	FPRE_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if ( NULL != pPlayer )
	{
		unsigned short usLevel = pPlayer->GetLevel();
		lua_pushnumber( pState, usLevel );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗判断脚本错误，实际攻击者不是玩家，返回玩家种族0" );
		lua_pushnumber( pState, 0 );
	}

	return 1;


}

///战斗判断，取被攻击怪物的类型
int CFightScriptExtent::FPreGetTargetMonsterType( lua_State* pState )
{
	FPRE_EXT_FUNPRE;

	CMonster* pMonster = pScript->GetTargetMonster();
	if ( NULL != pMonster )
	{
		unsigned short monsterType = pMonster->GetNpcType();
		lua_pushnumber( pState, monsterType );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗判断脚本错误，实际攻击目标不是怪物，返回怪物类型0" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


int CFightScriptExtent::FPreGetAttackPlayerBattleMode( lua_State* pState )
{
	FPRE_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackerPlayer();
	if ( NULL != pPlayer )
	{
		BATTLE_MODE battleMode = pPlayer->GetBattleMode();
		lua_pushnumber( pState, (int)battleMode );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗判断脚本错误 FPreGetAttackPlayerBattleMode \n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

///...战斗判断脚本扩展
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
///战斗计算公式脚本扩展；

/**
//战斗判断扩展函数定义通用部分；
//1、判断该pState是否对应一个CFightPreScript脚本，如果不是，则出错返回，
//   即：必须在FightPre脚本中调用，且调用该脚本者必须为CFightPreScript对象;
*/
#define FCAL_EXT_FUNPRE \
	if ( NULL == pState )\
    {\
	    return 0;\
    }\
	CFightCalScript* pScript = CFightCalScriptManager::FindScripts( pState );\
	if ( NULL == pScript  )\
	{\
		return 0;\
	}\

///脚本扩展，用于调试；
int CFightScriptExtent::Debug( lua_State* pState )
{
//#ifndef _DEBUG
//	ACE_UNUSED_ARG( pState );
//	return 0;
//#else //_DEBUG
	if ( !lua_isnumber( pState, -1 ) )
	{
		if ( lua_isstring( pState, -1 ) )
		{
			const char* debugstr = lua_tostring( pState, -1 );
			if ( NULL == g_debugPlayer ) 
			{
			    D_DEBUG( "脚本调试信息:%s\n", debugstr );
			} else {
				g_debugPlayer->SendSystemChat( debugstr );
			}
		} else {
			D_DEBUG( "脚本调试信息输入参数错误\n" );
		}
	} else {
		double debugdb = lua_tonumber( pState, -1 );
		if ( lua_isstring( pState, -2 ) )
		{
			const char* debugstr = lua_tostring( pState, -2 );
			D_DEBUG( "脚本调试信息:%s(%f)\n", debugstr, debugdb );
		} else {
			D_DEBUG( "脚本调试信息输入参数错误\n" );
		}
	}

	return 0;
//#endif //_DEBUG
}


//1 获取攻击者的等级
int CFightScriptExtent::FCalGetAtkPlayerLevel( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		unsigned short playerLevel = pPlayer->GetLevel();
		lua_pushnumber( pState, playerLevel );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回玩家等级0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

//2 获取攻击者装备最小物理攻击
int CFightScriptExtent::FCalGetAtkPlayerMinPhyAttack( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& ItemProp = pPlayer->GetItemAddtionProp();
		int nMinPhyAttack = ItemProp.AttackPhyMin;
		lua_pushnumber( pState, nMinPhyAttack );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际目标不是玩家，返回最小物理攻击0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

//3 获取攻击者装备最大物理攻击
int CFightScriptExtent::FCalGetAtkPlayerMaxPhyAttack( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& ItemProp = pPlayer->GetItemAddtionProp();
		int nMaxPhyAttack = ItemProp.AttackPhyMax;
		lua_pushnumber( pState, nMaxPhyAttack );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回最大物理攻击0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

//4 获取攻击者装备最小魔法攻击
int CFightScriptExtent::FCalGetAtkPlayerMinMagAttack( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp();
		int nMinMagAttack = itemProp.AttackMagMin;
		lua_pushnumber( pState, nMinMagAttack );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回最小魔法攻击0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

//5 获取攻击者装备最大魔法攻击
int CFightScriptExtent::FCalGetAtkPlayerMaxMagAttack( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& pItemProp = pPlayer->GetItemAddtionProp();
		int nMaxMagAttack = pItemProp.AttackMagMax;
		lua_pushnumber( pState, nMaxMagAttack );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回最大魔法攻击0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

//6 获取攻击者的职业
int CFightScriptExtent::FCalGetAtkPlayerClass( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int playerClass = pPlayer->GetClass();
		lua_pushnumber( pState, playerClass );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回玩家职业0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

//7 获取攻击者攻击技能的暴击率
int CFightScriptExtent::FCalGetAtkSkillCriRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	//CPlayerSkill* pSkill = pScript->GetPlayerSkill();

	//if ( NULL != pSkill )
	//{
	//	float criRate = pSkill->GetCriticalRate();
	//	lua_pushnumber( pState, criRate );
	//} else {
	//	//攻击者不是玩家；
	//	D_WARNING( "战斗计算公式脚本错误，取技能错，返回技能暴击几率0\n" );
	//	lua_pushnumber( pState, 0 );
	//}

	return 1;
}


//8 获取攻击者攻击技能的攻击系数
int CFightScriptExtent::FCalGetAtkSkillAttackRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	//CPlayerSkill* pSkill = pScript->GetPlayerSkill();

	//if ( NULL != pSkill )
	//{
	//	float attackRate = pSkill->GetSkillAttack();
	//	lua_pushnumber( pState, attackRate );
	//} else {
	//	//攻击者不是玩家；
	//	D_WARNING( "战斗计算公式脚本错误，取技能错，返回技能暴击系数0\n" );
	//	lua_pushnumber( pState, 0 );
	//}

	return 1;
}


//9 获取攻击技能的附加伤害
int CFightScriptExtent::FCalGetAdditionalSkillDamage( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	//CPlayerSkill* pSkill = pScript->GetPlayerSkill();

	//if ( NULL != pSkill )
	//{
	//	int nAdditionDamage = pSkill->GetSkillAffix();
	//	lua_pushnumber( pState, nAdditionDamage );
	//} else {
	//	//攻击者不是玩家；
	//	D_WARNING( "战斗计算公式脚本错误，取技能错，返回技能伤害奖励0\n" );
	//	lua_pushnumber( pState, 0 );
	//}

	return 1;
}


//10 获取攻击者的STR
int CFightScriptExtent::FCalGetAtkPlayerStr( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		unsigned int uiStr = pPlayer->GetScriptStr();
		lua_pushnumber( pState, uiStr );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的str0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

//11 获取攻击者的AGI
int CFightScriptExtent::FCalGetAtkPlayerAgi( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		unsigned int uiAgi = pPlayer->GetScriptAgi();
		lua_pushnumber( pState, uiAgi );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的agi0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//12 获取攻击者的INT
int CFightScriptExtent::FCalGetAtkPlayerInt( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int uiInt = pPlayer->GetScriptInt();
		lua_pushnumber( pState, uiInt );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的int0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//13 获取攻击者的物理攻击
int CFightScriptExtent::FCalGetAtkPlayerPhyAttack( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int nPhyAttack = pPlayer->GetPhyAttack();
		lua_pushnumber( pState, nPhyAttack );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理攻击0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//14 获取攻击者的魔法攻击
int CFightScriptExtent::FCalGetAtkPlayerMagAttack( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int nMagAttack = pPlayer->GetMagAttack();
		lua_pushnumber( pState, nMagAttack );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法攻击0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//15 获取攻击者的物理防御
int CFightScriptExtent::FCalGetAtkPlayerPhyDefend( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int nPhyDefend = pPlayer->GetPhyDefend();
		lua_pushnumber( pState, nPhyDefend );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理防御0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//16 获取攻击者的魔法防御
int CFightScriptExtent::FCalGetAtkPlayerMagDefend( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int nMagDefend = pPlayer->GetMagDefend();
		lua_pushnumber( pState, nMagDefend );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法防御0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//17 获取攻击者的物理命中
int CFightScriptExtent::FCalGetAtkPlayerPhyHit( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int phyHit = pPlayer->GetPhyHit();
		lua_pushnumber( pState, phyHit );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理命中0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//18 获取攻击者的魔法命中;
int CFightScriptExtent::FCalGetAtkPlayerMagHit( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int magHit = pPlayer->GetMagHit();
		lua_pushnumber( pState, magHit );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法命中0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//19 获取攻击者的物理暴击等级
int CFightScriptExtent::FCalGetAtkPlayerPhyCriLevel( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int nPhyCriLevel = pPlayer->GetPhyCriLevel();
		lua_pushnumber( pState, nPhyCriLevel );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理暴击等级0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//20 获取攻击者的魔法暴击等级 
int CFightScriptExtent::FCalGetAtkPlayerMagCriLevel( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int nMagCriLevel = pPlayer->GetMagCriLevel();
		lua_pushnumber( pState, nMagCriLevel );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法暴击等级0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//21 获取攻击者的物理攻击百分比
int CFightScriptExtent::FCalGetAtkPlayerPhyAttackRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp();
		int PhysicsAttackPercent = itemProp.PhysicsAttackPercent;//
		lua_pushnumber( pState, PhysicsAttackPercent );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理攻击百分比0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//22 获取攻击者的魔法攻击百分比
int CFightScriptExtent::FCalGetAtkPlayerMagAttackRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp();
		int MagicAttackPercent = itemProp.MagicAttackPercent;
		lua_pushnumber( pState, MagicAttackPercent );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法攻击百分比0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}



//23 获取攻击者附加物理暴击伤害
int CFightScriptExtent::FCalGetAtkPlayerAdditionPhyCriDamage( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int nAdditionPhyCriDamage = pPlayer->GetPhyCriDamage();
		lua_pushnumber( pState, nAdditionPhyCriDamage );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的附加物理暴击伤害0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//24 获取攻击者附加魔法暴击伤害
int CFightScriptExtent::FCalGetAtkPlayerAdditionMagCriDamage( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		int nAdditionMagCriDamage = pPlayer->GetMagCriDamage();
		lua_pushnumber( pState, nAdditionMagCriDamage );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的附加魔法暴击伤害0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//25 获取攻击者物理伤害提升百分比 
int CFightScriptExtent::FCalGetAtkPlayerPhyDamageUpRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect();
		float fAddPhyDamageRate = pBuffAffect->nAddPhyDamageRate;
		lua_pushnumber( pState, fAddPhyDamageRate );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理伤害百分比0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//26 获取攻击者魔法伤害提升百分比 
int CFightScriptExtent::FCalGetAtkPlayerMagDamageUpRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect();
		float fAddMagDamageRate = pBuffAffect->nAddMagDamageRate;
		lua_pushnumber( pState, fAddMagDamageRate );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法伤害百分比0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//27 获取BUFF附加提升物理暴击率
int CFightScriptExtent::FCalGetAtkPlayerAdditionalPhyCriRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect();
		float fAddPhyCriRate = pBuffAffect->fAddPhyCriRate;
		lua_pushnumber( pState, fAddPhyCriRate );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理暴击率0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//28 获取BUFF附加提升魔法暴击率
int CFightScriptExtent::FCalGetAtkPlayerAdditionalMagCriRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect();
		float fAddMagCriRate = pBuffAffect->fAddMagCriRate;
		lua_pushnumber( pState, fAddMagCriRate );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法暴击率0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

//29 获取技能附加物理暴击伤害提升百分比
int CFightScriptExtent::FCalGetAtkPlayerPhyCriDamageUpRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect();
		float fAddPhyCriDmgRate = pBuffAffect->fAddPhyCriDmgRate;
		lua_pushnumber( pState, fAddPhyCriDmgRate );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理暴击伤害提升百分比0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//30 获取技能附加魔法暴击伤害提升百分比
int CFightScriptExtent::FCalGetAtkPlayerMagCriDamageUpRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect();
		float fAddMagCriDmgRate = pBuffAffect->fAddMagCriDmgRate;
		lua_pushnumber( pState, fAddMagCriDmgRate );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法暴击伤害提升百分比0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//31 获取攻击者物理穿透等级
int CFightScriptExtent::FCalGetAtkPlayerPhyRiftLevel( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp();
		int nPhyRiftLevel = itemProp.PhysicsRift;
		lua_pushnumber( pState, nPhyRiftLevel );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理穿透等级0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//32 获取攻击者魔法穿透等级
int CFightScriptExtent::FCalGetAtkPlayerMagRiftLevel( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp();
		int nMagRiftLevel = itemProp.MagicRift;
		lua_pushnumber( pState, nMagRiftLevel );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法穿透等级0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//33 获取攻击者额外物理伤害
int CFightScriptExtent::FCalGetAtkPlayerAdditionalPhyDamage( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp();
		int PhysicsAttackAdd = itemProp.PhysicsAttackAdd;
		lua_pushnumber( pState, PhysicsAttackAdd );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家额外物理伤害的0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//34 获取攻击者额外魔法伤害
int CFightScriptExtent::FCalGetAtkPlayerAdditionalMagDamage( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp();
		int MagicAttackAdd = itemProp.MagicAttackAdd;
		lua_pushnumber( pState, MagicAttackAdd );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的额外魔法伤害0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//35 获取攻击技能的类型 物理 魔法
int CFightScriptExtent::FCalGetAtkSkillType( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	//CPlayerSkill* pSkill = pScript->GetPlayerSkill();

	//if ( NULL != pSkill )
	//{
	//	int nType = pSkill->GetType();
	//	lua_pushnumber( pState, nType );
	//} else {
	//	//攻击者不是玩家；
	//	D_WARNING( "战斗计算公式脚本错误，取技能错，返回技能的类型0\n" );
	//	lua_pushnumber( pState, 0 );
	//}

	return 1;
}


//36 获取攻击技能的暴击附加伤害
int CFightScriptExtent::FCalGetAtkSkillAdditionCriDamage( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	//CPlayerSkill* pSkill = pScript->GetPlayerSkill();

	//if ( NULL != pSkill )
	//{
	//	int criDamageAffix = pSkill->GetCriAddDamage();
	//	lua_pushnumber( pState, criDamageAffix );
	//} else {
	//	//攻击者不是玩家；
	//	D_WARNING( "战斗计算公式脚本错误，取技能错，返回技能的暴击伤害奖励0\n" );
	//	lua_pushnumber( pState, 0 );
	//}

	return 1;
}


//37 获取攻击技能的ID号
int CFightScriptExtent::FCalGetAtkSkillID( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	//CPlayerSkill* pSkill = pScript->GetPlayerSkill();

	//if ( NULL != pSkill )
	//{
	//	unsigned long ulSkillID = pSkill->GetSkillID();
	//	lua_pushnumber( pState, ulSkillID );
	//} else {
	//	//攻击者不是玩家；
	//	D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的技能ID0\n" );
	//	lua_pushnumber( pState, 0 );
	//}

	return 1;
}


//38 获取攻击技能的暴击伤害系数
int CFightScriptExtent::FCalGetAtkSkillCriDamageRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	//CPlayerSkill* pSkill = pScript->GetPlayerSkill();

	//if ( NULL != pSkill )
	//{
	//	float fCriDamageRate = pSkill->GetCriticalDamage();
	//	lua_pushnumber( pState, fCriDamageRate );
	//} else {
	//	//攻击者不是玩家；
	//	D_WARNING( "战斗计算公式脚本错误，取技能错，返回技能暴击系数0\n" );
	//	lua_pushnumber( pState, 0 );
	//}

	return 1;
}


//39 获取人物的物理命中率
int CFightScriptExtent::FCalGetAtkPlayerPhyHitRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect();
		float fAddPhyHitRate = pBuffAffect->fAddPhyHitRate;
		lua_pushnumber( pState, fAddPhyHitRate );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回增加物理命中率0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//40 获取人物的魔法命中率
int CFightScriptExtent::FCalGetAtkPlayerMagHitRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetAttackPlayer();

	if ( NULL != pPlayer )
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect();
		float fAddMagHitRate = pBuffAffect->fAddMagHitRate;
		lua_pushnumber( pState, fAddMagHitRate );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回增加魔法命中率0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}

////////////////////////////////被攻击者:玩家接口/////////////////////////////
//1 获取被攻击玩家的等级
int CFightScriptExtent::FCalGetTgtPlayerLevel( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		int nLevel = pPlayer->GetLevel();
		lua_pushnumber( pState, nLevel );
	}else {
		D_WARNING(" 战斗计算公式错误，返回返回玩家等级为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//2 获取被攻击玩家的物理防御
int CFightScriptExtent::FCalGetTgtPlayerPhyDefend( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		int nPhyDefend = pPlayer->GetPhyDefend();
		lua_pushnumber( pState, nPhyDefend );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家物理防御为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//3 获取被攻击玩家的魔法防御
int CFightScriptExtent::FCalGetTgtPlayerMagDefend( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		int nMagDefend = pPlayer->GetMagDefend();
		lua_pushnumber( pState, nMagDefend );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家魔法防御为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}

//4 获取被攻击玩家的物理防御百分比
int CFightScriptExtent::FCalGetTgtPlayerPhyDefendRate( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int PhysicsDefendPercent = itemProp.PhysicsDefendPercent;
		lua_pushnumber( pState, PhysicsDefendPercent );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家物理防御百分比为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//5 获取被攻击玩家的魔法防御百分比
int CFightScriptExtent::FCalGetTgtPlayerMagDefendRate( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int MagicDefendPercent = itemProp.MagicDefendPercent;
		lua_pushnumber( pState, MagicDefendPercent );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家魔法防御百分比为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//6 获取被攻击玩家的减免物理暴击等级
int CFightScriptExtent::FCalGetTgtPlayerPhyCriDerateLevel( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int PhysicsCriticalDerate = itemProp.PhysicsCriticalDerate;
		lua_pushnumber( pState, PhysicsCriticalDerate );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家减免物理暴击等级为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}

//7 获取被攻击玩家的减免魔法暴击等级
int CFightScriptExtent::FCalGetTgtPlayerMagCriDerateLevel( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int MagicCriticalDamageDerate = itemProp.MagicCriticalDerate;
		lua_pushnumber( pState, MagicCriticalDamageDerate );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家减免魔法暴击等级为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//8 获取被攻击者物理暴击伤害减免
int CFightScriptExtent::FCalGetTgtPlayerPhyCriDamageDerate( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int PhysicsCriticalDamageDerate = itemProp.PhysicsCriticalDamageDerate;
		lua_pushnumber( pState, PhysicsCriticalDamageDerate );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家物理暴击伤害减免为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//9 获取被攻击者魔法暴击伤害减免
int CFightScriptExtent::FCalGetTgtPlayerMagCriDamageDerate( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int MagicCriticalDamageDerate = itemProp.MagicCriticalDamageDerate;
		lua_pushnumber( pState, MagicCriticalDamageDerate );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家魔法暴击伤害减免为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//10 获取被攻击者物理闪避等级
int CFightScriptExtent::FCalGetTgtPlayerPhyJouk( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int PhysicsJouk = itemProp.PhysicsJouk;
		lua_pushnumber( pState, PhysicsJouk );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家物理闪避等级为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//11 获取被攻击者魔法闪避等级
int CFightScriptExtent::FCalGetTgtPlayerMagJouk( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int MagicJouk = itemProp.MagicJouk;
		lua_pushnumber( pState, MagicJouk );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回玩家魔法闪避等级为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//12 获取被攻击者物理伤害减免
int CFightScriptExtent::FCalGetTgtPlayerPhyDamageDerate( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int PhysicsDeratePercent = itemProp.PhysicsDeratePercent;
		lua_pushnumber( pState, PhysicsDeratePercent );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回物理伤害减免为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//13 获取被攻击者魔法伤害减免
int CFightScriptExtent::FCalGetTgtPlayerMagDamageDerate( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		ItemAddtionProp& itemProp = pPlayer->GetItemAddtionProp(); 
		int MagicDeratePercent = itemProp.MagicDeratePercent;
		lua_pushnumber( pState, MagicDeratePercent );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回魔法伤害减免为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//14 获取被攻击者物理闪避百分比
int CFightScriptExtent::FCalGetTgtPlayerPhyJoukRate( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect(); 
		float fPhyJoukRate = pBuffAffect->fPhyJoukRate;
		lua_pushnumber( pState, fPhyJoukRate );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回物理闪避百分比为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//15 获取被攻击者魔法闪避百分比
int CFightScriptExtent::FCalGetTgtPlayerMagJoukRate( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		AttributeEffect* pBuffAffect = pPlayer->GetBuffEffect(); 
		float fMagJoukRate = pBuffAffect->fMagJoukRate;
		lua_pushnumber( pState, fMagJoukRate );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回魔法闪避百分比为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//16 获取被攻击者的STR
int CFightScriptExtent::FCalGetTgtPlayerStr( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		int nStr = pPlayer->GetScriptStr();
		lua_pushnumber( pState, nStr );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回str为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}

//17 获取被攻击的AGI
int CFightScriptExtent::FCalGetTgtPlayerAgi( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		int nAgi = pPlayer->GetScriptAgi();
		lua_pushnumber( pState, nAgi );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回agi为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//18 获取被攻击者的INT
int CFightScriptExtent::FCalGetTgtPlayerInt( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		int nInt = pPlayer->GetScriptInt();
		lua_pushnumber( pState, nInt );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回int为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


int CFightScriptExtent::FCalGetTgtPlayerClass( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CPlayer* pPlayer = pScript->GetTargetPlayer();
	if( NULL != pPlayer)
	{
		unsigned char ucClass = pPlayer->GetClass();
		lua_pushnumber( pState, ucClass );
	}else {
		D_WARNING(" 战斗计算公式错误，被攻击者不是玩家，返回int为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}

/////////////////////////////////被攻击者：玩家//////////////////////////////////////////


//////////////////////////////攻击者：怪物///////////////////////////////
//1 攻击怪物的等级
int CFightScriptExtent::FCalGetAtkMonsterLevel( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CMonster* pMonster = pScript->GetAttackMonster();
	if( NULL != pMonster)
	{
		int nLevel = pMonster->GetLevel();
		lua_pushnumber( pState, nLevel );
	}else {
		D_WARNING(" 战斗计算公式错误，实际攻击者不是怪物，返回怪物等级ID为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}

//2 攻击怪物的技能类型
int CFightScriptExtent::FCalGetAtkMonsterSkillType( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CNpcSkill* pNpcSkill = pScript->GetNpcSkill();

	if( NULL != pNpcSkill)
	{
		int nType = pNpcSkill->m_nType;
		lua_pushnumber( pState, nType );
	}else {
		D_WARNING( "战斗计算公式脚本错误，实现攻击者不是怪物，返回怪物技能类型为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//3 攻击怪物的技能命中
int CFightScriptExtent::FCalGetAtkMonsterSkillHit( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CNpcSkill* pNpcSkill = pScript->GetNpcSkill();

	if( NULL != pNpcSkill)
	{
		float fHit = pNpcSkill->m_fHit;
		lua_pushnumber( pState, fHit );
	}else {
		D_WARNING(" 战斗计算公式错误，实际攻击者不是怪物，返回怪物技能命中为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//4 怪物攻击技能的附加命中
int CFightScriptExtent::FCalGetAtkMonsterSkillAddHit( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CNpcSkill* pNpcSkill = pScript->GetNpcSkill();

	if( NULL != pNpcSkill)
	{
		float fAddHit = pNpcSkill->m_fAddHit;
		lua_pushnumber( pState, fAddHit );
	}else {
		D_WARNING(" 战斗计算公式错误，实际攻击者不是怪物，返回怪物技能附加命中为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//5 怪物攻击技能的暴击率
int CFightScriptExtent::FCalGetAtkMonsterSkillCriRate( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CNpcSkill* pNpcSkill = pScript->GetNpcSkill();

	if( NULL != pNpcSkill)
	{
		float fCritical = pNpcSkill->m_fCritical;
		lua_pushnumber( pState, fCritical );
	}else {
		D_WARNING(" 战斗计算公式错误，实际攻击者不是怪物，返回怪物技能暴击为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//6 怪物攻击技能的最小攻击
int CFightScriptExtent::FCalGetAtkMonsterSkillMinAtk( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CNpcSkill* pNpcSkill = pScript->GetNpcSkill();

	if( NULL != pNpcSkill)
	{
		int nAttMin = pNpcSkill->m_nAttMin;
		lua_pushnumber( pState, nAttMin );
	}else {
		D_WARNING(" 战斗计算公式错误，实际攻击者不是怪物，返回怪物技能最小攻击为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//7 怪物攻击技能的最大攻击
int CFightScriptExtent::FCalGetAtkMonsterSkillMaxAtk( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CNpcSkill* pNpcSkill = pScript->GetNpcSkill();

	if( NULL != pNpcSkill)
	{
		int nAttMax = pNpcSkill->m_nAttMax;
		lua_pushnumber( pState, nAttMax );
	}else {
		D_WARNING(" 战斗计算公式错误，实际攻击者不是怪物，返回怪物技能最大攻击为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//8 怪物攻击技能的ID号
int CFightScriptExtent::FCalGetAtkMonsterSkillID( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CNpcSkill* pNpcSkill = pScript->GetNpcSkill();

	if( NULL != pNpcSkill)
	{
		int nSkillID = pNpcSkill->m_lNpcSkillID;
		lua_pushnumber( pState, nSkillID );
	}else {
		D_WARNING(" 战斗计算公式错误，实际攻击者不是怪物，返回怪物技能ID为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//9 怪物的Rank
int CFightScriptExtent::FCalGetAtkMonsterRank( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CMonster* pMonster = pScript->GetAttackMonster();

	if ( NULL != pMonster )
	{
		unsigned short usRank = pMonster->GetRank();
		lua_pushnumber( pState, usRank );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击怪物的rank0\n" );
		lua_pushnumber( pState, 0 );
	}
	return 1;
}


//10 怪物技能的伤害追加
int CFightScriptExtent::FCalGetAtkMonsterSkillAdditionDamage( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CNpcSkill* pNpcSkill = pScript->GetNpcSkill();

	if( NULL != pNpcSkill)
	{
		int nAddDamage = pNpcSkill->m_nAddAtt;
		lua_pushnumber( pState, nAddDamage );
	}else {
		D_WARNING(" 战斗计算公式错误，实际攻击者不是怪物，返回怪物技能附加伤害为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}
///////////////////////////////攻击者：怪物////////////////////////////


/////////////////////////////被攻击者：怪物//////////////////////////
//1 被攻击者怪物的等级
int CFightScriptExtent::FCalGetTgtMonsterLevel( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CMonster* pMonster = pScript->GetTargetMonster();
	if( NULL != pMonster)
	{
		int nLevel = pMonster->GetLevel();
		lua_pushnumber( pState, nLevel );
	}else {
		D_WARNING(" 战斗计算公式错误，实际攻击者不是怪物，返回怪物等级等级为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}


//2 被攻击怪物的物理防御
int CFightScriptExtent::FCalGetTgtMonsterPhyDef( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CMonster* pMonster = pScript->GetTargetMonster();

	if ( NULL != pMonster )
	{
		int nPhyDef = pMonster->GetPhyDef();
		lua_pushnumber( pState, nPhyDef );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击怪物的物理防御0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//3 被攻击怪物的魔法防御
int CFightScriptExtent::FCalGetTgtMonsterMagDef( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CMonster* pMonster = pScript->GetTargetMonster();

	if ( NULL != pMonster )
	{
		int nMagicDef = pMonster->GetMagDef();
		lua_pushnumber( pState, nMagicDef );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回怪物的魔法防御0\n" );
		lua_pushnumber( pState, 0 );
	}
	return 1;
}


//4 被攻击怪物的物理回避
int CFightScriptExtent::FCalGetTgtMonsterPhyJouk( lua_State* pState)
{
	FCAL_EXT_FUNPRE;

	CMonster* pMonster = pScript->GetTargetMonster();

	if ( NULL != pMonster )
	{
		float fPhyDodge = pMonster->GetPhyDodge();
		lua_pushnumber( pState, fPhyDodge );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的物理回避0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//5 被攻击怪物的魔法回避
int CFightScriptExtent::FCalGetTgtMonsterMagJouk( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CMonster* pMonster = pScript->GetTargetMonster();

	if ( NULL != pMonster )
	{
		float fMagDodge = pMonster->GetMagDodge();
		lua_pushnumber( pState, fMagDodge );
	} else {
		//攻击者不是玩家；
		D_WARNING( "战斗计算公式脚本错误，实际攻击者不是玩家，返回攻击玩家的魔法回避0\n" );
		lua_pushnumber( pState, 0 );
	}

	return 1;
}


//6 被攻击怪物的rank
int CFightScriptExtent::FCalTgtMonsterRank( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	CMonster* pMonster = pScript->GetTargetMonster();
	if( NULL != pMonster )
	{
		unsigned short usRank = pMonster->GetRank();
		lua_pushnumber( pState, usRank );
	}
	else{
		D_WARNING(" 战斗计算公式错误，返回目标怪物RANK为0\n");
		lua_pushnumber( pState, 0);
	}
	return 1;
}
///////////////////////////////被攻击者怪物：///////////////////////////


///////////////////////通用//////////////////////////////////
//1  输入0~1的值 返回true or false
int CFightScriptExtent::FCalRandRate( lua_State* pState )
{
	FCAL_EXT_FUNPRE;

	float inRate = 0;
	if ( ! lua_isnumber( pState, -1 ) )
	{
		D_ERROR( "战斗计算公式脚本FCalRandRate输入参数个数错误\n" );
		lua_pushboolean( pState, false );
	} else {
		inRate = (float)(lua_tonumber( pState, -1 ));
		lua_pushboolean( pState, RandRate( inRate ) );
	}
	
	return 1;
}

//2  返回a到b中间的任何一值
int CFightScriptExtent::FCalRandNumber( lua_State* pState )
{
	int a =0, b=0; //a，b区间
	if ( ! lua_isnumber( pState, -1 ) ||  ! lua_isnumber( pState, -2 ) )
	{
		D_ERROR( "战斗计算公式脚本FCalRandNumber输入参数个数错误\n" );
		lua_pushnumber( pState, 0 );
	} else {
		b = (int)(lua_tonumber( pState, -1 ));
		a = (int)(lua_tonumber( pState, -2 ));
		lua_pushnumber( pState, RandNumber(a,b) );
	}
	return 1;
}
//////////////////////////////////////通用////////////////////////


///战斗计算公式脚本扩展；
////////////////////////////////////////////////////////////////

//////////////////////////////升级脚本的扩展/////////////////////////////////////////////////////
#ifdef LEVELUP_SCRIPT

#define LEVELUP_EXT_FUNPRE \
	if ( NULL == pState )\
    {\
	    return 0;\
    }\
	CLevelUpScript* pScript = CLevelUpScriptManager::FindScripts( pState );\
	if ( NULL == pScript  )\
	{\
		return 0;\
	}\


void CLevelUpScriptExtent::Init( lua_State* pLuaState )
{
	if ( !m_isvecInited )
	{
		m_isvecInited = true;
		m_vecRegFns.push_back( ToRegFns( "SendSystemMsg", SendSystemMsg ) );	//发送系统消息
		m_vecRegFns.push_back( ToRegFns( "LevelSetBaseMaxHp", LevelSetBaseMaxHp ) );	//设置最大HP
		m_vecRegFns.push_back( ToRegFns( "LevelSetBaseMaxMp", LevelSetBaseMaxMp ) );	//设置最大MP
		m_vecRegFns.push_back( ToRegFns( "LevelSetStr", LevelSetStr ) );	//设置STR
		m_vecRegFns.push_back( ToRegFns( "LevelSetAgi", LevelSetAgi ) );	//设置AGI
		m_vecRegFns.push_back( ToRegFns( "LevelSetInt", LevelSetInt ) );	//设置INT
		m_vecRegFns.push_back( ToRegFns( "LevelSetBasePhyAtk", LevelSetBasePhyAtk ) );	//设置基础物攻
		m_vecRegFns.push_back( ToRegFns( "LevelSetBaseMagAtk", LevelSetBaseMagAtk ) );	//设置基础魔攻
		m_vecRegFns.push_back( ToRegFns( "LevelSetBasePhyDef", LevelSetBasePhyDef ) );	//设置基础物理防御力
		m_vecRegFns.push_back( ToRegFns( "LevelSetBaseMagDef", LevelSetBaseMagDef ) );	//设置基础魔法防御力
		m_vecRegFns.push_back( ToRegFns( "LevelSetBasePhyHit", LevelSetBasePhyHit ) );	//设置基础物理命中
		m_vecRegFns.push_back( ToRegFns( "LevelSetBaseMagHit", LevelSetBaseMagHit ) );	//设置基础魔法命中
		m_vecRegFns.push_back( ToRegFns( "LevelSetNextLevelExp", LevelSetNextLevelExp ) );	//设置下级经验
		m_vecRegFns.push_back( ToRegFns( "LevelAddSkill", LevelAddSkill ) );	//添加技能
		m_vecRegFns.push_back( ToRegFns( "LevelAddItem", LevelAddItem ) );	//添加道具
		m_vecRegFns.push_back( ToRegFns( "LevelAddBuff", LevelAddBuff ) );	//添加一个buff
		m_vecRegFns.push_back( ToRegFns( "LevelAddSpBean", LevelAddSpBean ) );	//添加sp豆
		m_vecRegFns.push_back( ToRegFns( "LevelSex", LevelSex ) );	//获取性别
		m_vecRegFns.push_back( ToRegFns( "LevelRace", LevelRace ) );	//获取性别
		m_vecRegFns.push_back( ToRegFns( "LevelSendSysMail",LevelSendSysMail) );//赠送邮件
		m_vecRegFns.push_back( ToRegFns( "LevelShowNotices", LevelShowNotices) );
		m_vecRegFns.push_back( ToRegFns( "LevelShowParamsNotices", LevelShowParamsNotices) );
	}

	///////////////////////////////////加入MuxLib表/////////////////////////////////////////
	CLuaRestoreStack rs( pLuaState );//准备恢复堆栈;

	//新函数表；
	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "MuxLib" );//置全局变量"MuxLib"为新表；

	lua_getglobal( pLuaState, "MuxLib" );//push表"MuxLib"；

	//注册向量中的函数至MuxLib表；
	ToRegFns* pToReg = NULL;
	for ( vector<ToRegFns>::iterator tmpiter=m_vecRegFns.begin();
		tmpiter!=m_vecRegFns.end(); ++tmpiter )
	{
		pToReg = &(*tmpiter);
		lua_pushstring( pLuaState, pToReg->fnName );
		lua_pushnumber( pLuaState, (lua_Number)(tmpiter-m_vecRegFns.begin()) );//closure注册为vec中的序号；
		lua_pushcclosure( pLuaState, LuaCallbackMeta<CLevelUpScriptExtent>, 1);
		lua_settable( pLuaState, -3 );//MuxLib[pToReg->fnName] = LuaCallback(with closure=本项vec序号)；
	}
}


int CLevelUpScriptExtent::SendSystemMsg( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	
	if ( ! lua_isstring( pState, -1 ) )
	{
		D_ERROR( "CLevelUpScriptExtent::SendSystemMsg，脚本参数非字符串" );
		return 0;
	}

	const char* pMsg = lua_tostring( pState, -1 );
	bool bRet = pScript->ExtSendSystemMsg( pMsg );
	lua_pushboolean( pState, bRet );
	return 1;
}

//设置最大基础hp
int CLevelUpScriptExtent::LevelSetBaseMaxHp( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetBaseMaxHp,脚本参数非数字\n");
		return 0;
	}

	unsigned int maxHp = (unsigned int)lua_tonumber( pState, - 1 );
	bool bRet = pScript->ExtLevelSetBaseMaxHp( maxHp );
	lua_pushboolean( pState, bRet );

	return 1;
}

//设置最大基础mp
int CLevelUpScriptExtent::LevelSetBaseMaxMp( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetBaseMaxMp,脚本参数非数字\n");
		return 0;
	}

	unsigned int maxMp = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetBaseMaxMp( maxMp );
	lua_pushboolean( pState, bRet );
	return 1;
}

//设置力量
int CLevelUpScriptExtent::LevelSetStr( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetStr,脚本参数非数字\n");
		return 0;
	}

	unsigned int baseStr = (unsigned int)lua_tonumber( pState, - 1 );
	bool bRet = pScript->ExtLevelSetStr( baseStr );
	lua_pushboolean( pState, bRet );
	return 1;
}


//设置敏捷
int CLevelUpScriptExtent::LevelSetAgi( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetAgi,脚本参数非数字\n");
		return 0;
	}

	unsigned int baseAgi = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetAgi( baseAgi );
	lua_pushboolean( pState, bRet );
	return 1;
}


//设置智力
int CLevelUpScriptExtent::LevelSetInt( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetInt,脚本参数非数字\n");
		return 0;
	}

	unsigned int baseInt = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetInt( baseInt );
	lua_pushboolean( pState, bRet );
	return 1;
}

//设置基础物理攻击力
int CLevelUpScriptExtent::LevelSetBasePhyAtk( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetBasePhyAtk,脚本参数非数字\n");
		return 0;
	}

	unsigned int basePhyAtk = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetBasePhyAtk( basePhyAtk );
	lua_pushboolean( pState, bRet );
	return 1;
}

//设置基础魔法攻击力
int CLevelUpScriptExtent::LevelSetBaseMagAtk( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetBaseMagAtk,脚本参数非数字\n");
		return 0;
	}

	unsigned int baseMagAtk = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetBaseMagAtk( baseMagAtk );
	lua_pushboolean( pState, bRet );
	return 1;
}


//设置基础物理防御力
int CLevelUpScriptExtent::LevelSetBasePhyDef( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetBasePhyDef,脚本参数非数字\n");
		return 0;
	}

	unsigned int basePhyDef = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetBasePhyDef( basePhyDef );
	lua_pushboolean( pState, bRet );
	return 1;
}


//设置基础魔法防御力
int CLevelUpScriptExtent::LevelSetBaseMagDef( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetBaseMagDef,脚本参数非数字\n");
		return 0;
	}

	unsigned int baseMagDef = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetBaseMagDef( baseMagDef );
	lua_pushboolean( pState, bRet );
	return 1;
}


//设置基础物理命中
int CLevelUpScriptExtent::LevelSetBasePhyHit( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetBasePhyHit,脚本参数非数字\n");
		return 0;
	}

	unsigned int basePhyHit = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetBasePhyHit( basePhyHit );
	lua_pushboolean( pState, bRet );
	return 1;
}


//设置基础魔法命中
int CLevelUpScriptExtent::LevelSetBaseMagHit( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetBaseMagHit,脚本参数非数字\n");
		return 0;
	}

	unsigned int baseMagHit = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetBaseMagHit( baseMagHit );
	lua_pushboolean( pState, bRet );
	return 1;
}


//设置下级经验
int CLevelUpScriptExtent::LevelSetNextLevelExp( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelSetNextLevelExp,脚本参数非数字\n");
		return 0;
	}

	unsigned int nextLevelExp = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelSetNextLevelExp( nextLevelExp );
	lua_pushboolean( pState, bRet );
	return 1;
}


//添加技能
int CLevelUpScriptExtent::LevelAddSkill( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelAddSkill,脚本参数非数字\n");
		return 0;
	}

	unsigned int newSkillID = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelAddSkill( newSkillID );
	lua_pushboolean( pState, bRet );
	return 1;
}


//添加道具
int CLevelUpScriptExtent::LevelAddItem( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelAddItem,脚本参数非数字\n");
		return 0;
	}

	unsigned int itemID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int itemNum = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelAddItem( itemID, itemNum );
	lua_pushboolean( pState, bRet );
	return 1;
}


//添加一个buff
int CLevelUpScriptExtent::LevelAddBuff( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState,-2 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelAddBuff,脚本参数非数字\n");
		return 0;
	}

	unsigned int buffTime = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int buffID = (unsigned int)lua_tonumber( pState, -2 );
	bool bRet = pScript->ExtLevelAddBuff( buffID, buffTime );
	lua_pushboolean( pState, bRet );
	return 1;
}


//添加sp豆
int CLevelUpScriptExtent::LevelAddSpBean( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("CLevelUpScriptExtent::LevelAddSpBean,脚本参数非数字\n");
		return 0;
	}

	unsigned int beanNumber = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExtLevelAddSpBean( beanNumber );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CLevelUpScriptExtent::LevelSex( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;

	unsigned char sex = pScript->ExtLevelSex();
	lua_pushnumber( pState, sex );
	return 1;
}


int CLevelUpScriptExtent::LevelRace( lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;

	unsigned short race = pScript->ExtLevelRace();
	lua_pushnumber( pState, race );
	return 1;
}

int CLevelUpScriptExtent::LevelSendSysMail(lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;

	bool issendrst = false;
	if ( (!lua_isnumber(pState , -13))//money
		|| (!lua_isstring(pState, -14))//content
		|| (!lua_isstring( pState, -15))//title 
		)
	{
		D_ERROR( "levelup脚本调用LevelSendSysMail时，基本调用参数错\n" );
		lua_pushboolean( pState, issendrst );
		return 1;
	}

	const char* pTitle   = lua_tostring( pState , -15 );
	const char* pContent = lua_tostring( pState , -14 );
	if ( NULL == pTitle || NULL == pContent )
	{
		D_ERROR( "levelup脚本调用LevelSendSysMail时，参数错(邮件标题空)\n" );
		lua_pushboolean( pState, issendrst );
		return 1;
	}
	unsigned int  money  = (unsigned int)lua_tonumber( pState , -13 );

	static vector<unsigned int> levelupMailItemType;
	static vector<int> levelupMailItemCount;
	levelupMailItemType.clear();
	levelupMailItemCount.clear();
	int readpos = 0;
	int itemcount = 0;
	int itemtype = 0;
	for ( unsigned int i=0; i<6; ++i )
	{
		readpos = -1 - i*2;
		if ( !lua_isnumber( pState, readpos/*item count*/ ) 
			|| !lua_isnumber( pState, readpos-1/*item type*/ )
			)
		{
			continue;
		}
		itemcount = (int)lua_tonumber( pState, readpos );
		itemtype = (unsigned int)lua_tonumber( pState, readpos-1 );
		if ( itemcount < 0 )
		{
			D_ERROR( "levelup脚本调用LevelSendSysMail时，参数itemcount%d(itemtype%d)错\n", itemcount, itemtype );
			continue;
		}
		if ( ( 0 == itemcount ) || ( 0 == itemtype ) )
		{
			continue;
		}
		levelupMailItemType.push_back( itemtype );
		levelupMailItemCount.push_back( itemcount );
	}

	issendrst = pScript->ExtSendMail( pTitle, pContent, money, levelupMailItemType, levelupMailItemCount );

	lua_pushboolean( pState, issendrst );
	return 1;

	//旧方法，替换，by dzj, 10.06.22; if (   lua_isnumber( pState , -13)//money
	//	&& lua_isstring( pState,  -14 )//content
	//	&& lua_isstring( pState,  -15) )//title 
	//{
	//	const char* pTitle   = lua_tostring( pState , -15 );
	//	const char* pContent = lua_tostring( pState , -14 );
	//	if ( NULL == pTitle || NULL == pContent )
	//	{
	//		return 0;
	//	}
	//	unsigned int  money  = (unsigned int)lua_tonumber( pState , -13 );

	//	unsigned int itemArr[12] = { 0 };
	//	unsigned int itemIndex = 0;
	//	for ( unsigned int i = 0 ;  i < 12 && itemIndex < 12 ; i+=2 )
	//	{
	//		int k = -1 - i ;
	//		int j = -2 - i;
	//		if ( !lua_isnumber( pState, k ) || !lua_isnumber( pState, j ) )
	//		{
	//			continue;
	//		}

	//		int itemCount  =  (int)lua_tonumber( pState, k );
	//		int itemType   =  (int)lua_tonumber( pState, j );

	//		if ( itemCount > 0 && itemType > 0 )
	//		{
	//			if ( itemIndex+1 >= ARRAY_SIZE(itemArr) ) //这个循环不好，要改，具体意义再看，by dzj, 10.06.08
	//			{
	//				D_ERROR( "CLevelUpScriptExtent::LevelSendSysMail, itemIndex(%d)+1 >= ARRAY_SIZE(itemArr)\n", itemIndex );
	//				break;
	//			}
	//			itemArr[itemIndex]     =  itemType;
	//			itemArr[itemIndex + 1] =  itemCount;
	//			itemIndex+=2;
	//		}
	//	}
	//	pScript->ExtSendMail( pTitle, pContent, money, itemArr );
	//}
	//lua_pushboolean( pState, true );

	return 1;
}

int CLevelUpScriptExtent::LevelShowNotices(lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1) ||
		!lua_isnumber( pState, -2) ||
		!lua_isnumber( pState, -3) ||
		!lua_isnumber( pState, -4) ||
		!lua_isstring( pState, -5 )
		)
	{
		D_ERROR("NpcShowNotices 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int distancetime  = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int maxshowcount  = (unsigned int)lua_tonumber( pState, -2 );
	char isgolabl  = (char)lua_tonumber( pState, -3 );
	char _position = (char)lua_tonumber( pState, -4 );
	const char* content= (char *)lua_tostring( pState, -5 );
	if ( NULL == content )
	{
		return 0;
	}

	bool bRet = pScript->ExtShowNotice( content,_position, isgolabl, maxshowcount, distancetime );
	lua_pushboolean( pState, bRet );
	return 1;

}

int CLevelUpScriptExtent::LevelShowParamsNotices(lua_State* pState )
{
	LEVELUP_EXT_FUNPRE;

	if (  !lua_isnumber( pState,-10)
		||!lua_isnumber( pState,-9)
		||!lua_isnumber( pState,-8)
		||!lua_isnumber( pState,-7)
		||!lua_isnumber( pState,-6) 
		)
	{
		D_ERROR("CMapScriptExtent::MSShowParamsNotices 输入参数错误\n");
		lua_pushboolean( pState, false );
		return 1;
	}

	unsigned int validparamcount = 0;
	const char* param5 = lua_tostring( pState, -1 );
	if ( param5 ) validparamcount++;
	const char* param4 = lua_tostring( pState, -2 );
	if ( param4 ) validparamcount++;
	const char* param3 = lua_tostring( pState, -3 );
	if ( param3 ) validparamcount++;
	const char* param2 = lua_tostring( pState, -4 );
	if ( param2 ) validparamcount++;
	const char* param1 = lua_tostring( pState, -5 );
	if ( param1 ) validparamcount++;

	unsigned int distancetime = (unsigned int)lua_tonumber( pState,-6 );
	unsigned int showmaxcount = (unsigned int)lua_tonumber( pState,-7 );
	char isglobal = (char)lua_tonumber( pState,  -8 );
	char _position = (char)lua_tonumber( pState, -9 );
	unsigned int msgid =(unsigned int)lua_tonumber( pState, -10 );

	bool bRet = pScript->ExtShowParamNotice( msgid, _position, isglobal, showmaxcount,  distancetime, validparamcount, param1, param2, param3, param4, param5 );
	lua_pushboolean( pState, bRet );
	return 1;
}

#endif //LEVELUP_SCRIPT

#define BATTLE_EXT_FUNPRE \
	if ( NULL == pState )\
    {\
	    return 0;\
    }\
	CBattleScript* pScript = CBattleScriptManager::GetScript();\
	if ( NULL == pScript  )\
	{\
		return 0;\
	}


//获得
void CBattleScriptExtent::Init( lua_State* pLuaState )
{
	if ( !m_isvecInited )
	{
		m_isvecInited = true;
		m_vecRegFns.push_back( ToRegFns( "ExSendAtkSysChat", ExSendAtkSysChat ) );	//发送系统消息
		m_vecRegFns.push_back( ToRegFns( "ExSendTgtSysChat", ExSendTgtSysChat ) );	//发送系统消息
		m_vecRegFns.push_back( ToRegFns( "ExIsSkillInCD", ExIsSkillInCD ) );	//是否在CD
		m_vecRegFns.push_back( ToRegFns( "ExPlayerHaveItem", ExPlayerHaveItem ) );	//是否有物品
		m_vecRegFns.push_back( ToRegFns( "ExPlayerConsume", ExPlayerConsume ) );	//减去消耗
		m_vecRegFns.push_back( ToRegFns( "ExAddSkillExp", ExAddSkillExp ) );	//增加技能经验
		m_vecRegFns.push_back( ToRegFns( "ExRandRate", ExRandRate ) );	//随即概率
		m_vecRegFns.push_back( ToRegFns( "ExRandNumber", ExRandNumber ) );	//区间数字
		m_vecRegFns.push_back( ToRegFns( "ExSetAbsorbDamage", ExSetAbsorbDamage ) );	//设置吸收量
		m_vecRegFns.push_back( ToRegFns( "ExPlayerDistance", ExPlayerDistance ) );	//返回2点距离平方和
		m_vecRegFns.push_back( ToRegFns( "ExAddPlayerExp", ExAddPlayerExp ) );	//增加玩家的经验
		m_vecRegFns.push_back( ToRegFns( "ExEndTgtPlayerSheepBuff", ExEndTgtPlayerSheepBuff ) );	//结束变羊
		m_vecRegFns.push_back( ToRegFns( "ExLog", ExLog ) );	//打日志
		m_vecRegFns.push_back( ToRegFns( "ExEndTgtPlayerAllDebuff", ExEndTgtPlayerAllDebuff ) );	//结束所有debuff
		m_vecRegFns.push_back( ToRegFns( "ExEndTgtPlayerControlBuff", ExEndTgtPlayerControlBuff ) );	//结束控制型buff
		m_vecRegFns.push_back( ToRegFns( "ExRelieveTgtPlayerBuff", ExRelieveTgtPlayerBuff ) );	//relieve Buff
		m_vecRegFns.push_back( ToRegFns( "ExDetectReliveTgtPlayerBuff", ExDetectReliveTgtPlayerBuff ) );	//检测relieve可能性
		m_vecRegFns.push_back( ToRegFns( "ExRelieveTgtMonsterBuff", ExRelieveTgtMonsterBuff ) );	//relieve Buff
		m_vecRegFns.push_back( ToRegFns( "ExDetectReliveTgtMonsterBuff", ExDetectReliveTgtMonsterBuff ) );	//检测relieve可能性
		m_vecRegFns.push_back( ToRegFns( "ExAtkHaveItem", ExAtkHaveItem ) );
		m_vecRegFns.push_back( ToRegFns( "ExTgtHaveItem", ExTgtHaveItem ) );
		m_vecRegFns.push_back( ToRegFns( "ExAtkCreateWeakBuff", ExAtkCreateWeakBuff ) );
	}

	///////////////////////////////////加入MuxLib表/////////////////////////////////////////
	CLuaRestoreStack rs( pLuaState );//准备恢复堆栈;

	//创建4个全局表 p0 p1 c0 c1
	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "p0" );

	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "p1" );

	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "m0" );
	
	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "m1" );

	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "args" );

	//新函数表；
	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "MuxLib" );//置全局变量"MuxLib"为新表；
	lua_getglobal( pLuaState, "MuxLib" );//push表"MuxLib"；

	//注册向量中的函数至MuxLib表；
	ToRegFns* pToReg = NULL;
	for ( vector<ToRegFns>::iterator tmpiter=m_vecRegFns.begin();
		tmpiter!=m_vecRegFns.end(); ++tmpiter )
	{
		pToReg = &(*tmpiter);
		lua_pushstring( pLuaState, pToReg->fnName );
		lua_pushnumber( pLuaState, (lua_Number)(tmpiter-m_vecRegFns.begin()) );//closure注册为vec中的序号；
		lua_pushcclosure( pLuaState, LuaCallbackMeta<CBattleScriptExtent>, 1);
		lua_settable( pLuaState, -3 );//MuxLib[pToReg->fnName] = LuaCallback(with closure=本项vec序号)；
	}
	
}


int CBattleScriptExtent::ExSendAtkSysChat( lua_State* pState )
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isstring( pState, -1)  )
	{
		D_ERROR("CBattleScriptExtent::ExSendAtkSysChat\n");
		return 0;
	}
	
	const char* pChat = lua_tostring(pState, -1 );
	if ( NULL == pChat )
	{
		return 0;
	}
	bool bRet = pScript->ExSendAtkSysChat( pChat );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExSendTgtSysChat( lua_State* pState )				//针对目标玩家输出log
{
	BATTLE_EXT_FUNPRE;
	
	if( !lua_isstring( pState, -1)  )
	{
		D_ERROR("CBattleScriptExtent::ExSendTgtSysChat\n");
		return 0;
	}
	
	const char* pChat = lua_tostring(pState, -1 );
	if ( NULL == pChat )
	{
		return 0;
	}
	bool bRet = pScript->ExSendTgtSysChat( pChat );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CBattleScriptExtent::ExIsSkillInCD( lua_State* pState )					//-- 检查技能是否在 CD 
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2) )
	{
		D_ERROR("ExIsSkillInCD 输入参数错误\n");
		return 0;
	}
	
	unsigned int cdTime = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int pubType = (unsigned int)lua_tonumber( pState, -2 );
	ACE_Time_Value cd( 0, cdTime * 1000 );

	bool bRet = pScript->ExIsSkillInCD( pubType, cd );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExPlayerHaveItem( lua_State* pState)				//-- 玩家包裹中是否有物品
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("BatIsSkillInCD 输入参数错误\n");
		return 0;
	}

	unsigned int itemID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int itemNum = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExPlayerHaveItem( itemID, itemNum );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExPlayerConsume(lua_State* pState)					 //-- 玩家消耗
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) || !lua_isnumber( pState, -3 ) || !lua_isnumber( pState, -4 ) )
	{
		D_ERROR("BatPlayerConsume 参数不对\n");
		return 0;
	}

	unsigned int num = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int itemID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int mp = (unsigned int)lua_tonumber( pState, -4 );
	unsigned int sp = (unsigned int)lua_tonumber( pState, -3 );

	bool bRet = pScript->ExPlayerConsume( mp, sp, itemID, num );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExAddSkillExp( lua_State* pState)				//-- 加技能经验
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("ExAddSkillExp 参数类型或者个数不对\n");
		return 0;
	}

	unsigned int expNum = (unsigned int)lua_tonumber( pState, -1 );
	unsigned int skillID = (unsigned int)lua_tonumber( pState, -2 );
	bool bRet = pScript->ExAddSkillExp( skillID, expNum );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExRandRate( lua_State* pState )			//减少效率
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("ExRandRate 参数类型或者个数不对\n");
		return 0;
	}
	
	double rate = lua_tonumber( pState, -1 );
	bool bRet = RandRate( (float)rate );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExRandNumber( lua_State* pState )				//
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("ExRandNumber 参数类型或者个数不对\n");
		return 0;
	}

	int a = (int)lua_tonumber( pState, -2 );
	int b = (int)lua_tonumber( pState, -1 );

	int num = RandNumber( a, b );
	lua_pushnumber( pState, num );
	return 1;
}


int CBattleScriptExtent::ExSetAbsorbDamage( lua_State* pState )		//设置吸收
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) )
	{
		D_ERROR("ExSetAbsorbDamage 参数类型或者个数不对\n");
		return 0;
	}

	unsigned int damage = (unsigned int)lua_tonumber( pState, -1 );

	bool bRet = pScript->ExSetAbsorbDamage( damage );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExPlayerDistance( lua_State* pState )  	//算出2点距离的平方和
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) ||!lua_isnumber( pState, -3 ) || !lua_isnumber( pState, -4 ))
	{
		D_ERROR("ExPlayerDistance 输入参数不对\n");
		return 0;
	}

	int x1 = (int)lua_tonumber( pState, -4 );
	int y1 = (int)lua_tonumber( pState, -3 );
	int x2 = (int)lua_tonumber( pState, -2 );
	int y2 = (int)lua_tonumber( pState, -1 );
	unsigned int ret = pScript->ExPlayerDistance( x1, y1, x2, y2 );
	lua_pushnumber( pState, ret );
	return 1;
}


int CBattleScriptExtent::ExAddPlayerExp( lua_State* pState )		//增加攻击者的经验
{
	BATTLE_EXT_FUNPRE;
		
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("ExPlayerExp 输入参数不对\n");
		return 0;
	}

	unsigned int type = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int num = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExAddPlayerExp( type, num );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExEndTgtPlayerSheepBuff( lua_State* pState )		//结束目标变羊状态
{
	BATTLE_EXT_FUNPRE;
		
	bool bRet = pScript->ExEndTgtPlayerSheepBuff();
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExLog( lua_State* pState )		//打出日志
{
	BATTLE_EXT_FUNPRE;
	if( !lua_isstring( pState, -1 ) )
	{
		D_ERROR("CBattleScriptExtent::ExLog 参数个数类型不正确\n");
		return false;
	}

	const char* pContent = lua_tostring( pState, -1 );

	bool bRet = pScript->ExLog( pContent );
	lua_pushboolean( pState, bRet );
	return 1;
}


int CBattleScriptExtent::ExEndTgtPlayerAllDebuff( lua_State* pState )			//所有的debuff
{
	BATTLE_EXT_FUNPRE;
	
	bool bRet = pScript->ExEndTgtPlayerAllDebuff();
	lua_pushboolean( pState, bRet );
	return 1;
}

int CBattleScriptExtent::ExEndTgtPlayerControlBuff( lua_State* pState )		//控制类的技能
{
	BATTLE_EXT_FUNPRE;

	bool bRet = pScript->ExEndTgtPlayerControlBuff();
	lua_pushboolean( pState, bRet );
	return 1;
}


//relieve Buff
int CBattleScriptExtent::ExRelieveTgtPlayerBuff( lua_State* pState )
{
	BATTLE_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) || !lua_isnumber( pState, -3 ))
	{
		D_ERROR("CBattleScriptExtent::ExRelieveTgtPlayerBuff 参数个数类型不正确\n");
		return false;
	}

	unsigned int relieveType = (unsigned int)lua_tonumber( pState, -3 );
	unsigned int relieveLevel = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int relieveNum = (unsigned int)lua_tonumber( pState, -1 );

	bool bRet = pScript->ExRelieveTgtPlayerBuff( relieveType, relieveLevel, relieveNum, true );
	lua_pushboolean( pState, bRet );
	return 1;
}

//检测relieve可能性
int CBattleScriptExtent::ExDetectReliveTgtPlayerBuff( lua_State* pState )
{
	BATTLE_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) || !lua_isnumber( pState, -3 ))
	{
		D_ERROR("CBattleScriptExtent::ExRelieveTgtPlayerBuff 参数个数类型不正确\n");
		return false;
	}

	unsigned int relieveType = (unsigned int)lua_tonumber( pState, -3 );
	unsigned int relieveLevel = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int relieveNum = (unsigned int)lua_tonumber( pState, -1 );

	bool bRet = pScript->ExRelieveTgtPlayerBuff( relieveType, relieveLevel, relieveNum, false );
	lua_pushboolean( pState, bRet );
	return 1;
}


//relieve Buff
int CBattleScriptExtent::ExRelieveTgtMonsterBuff( lua_State* pState )
{
	BATTLE_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) || !lua_isnumber( pState, -3 ))
	{
		D_ERROR("CBattleScriptExtent::ExRelieveTgtPlayerBuff 参数个数类型不正确\n");
		return false;
	}

	unsigned int relieveType = (unsigned int)lua_tonumber( pState, -3 );
	unsigned int relieveLevel = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int relieveNum = (unsigned int)lua_tonumber( pState, -1 );

	bool bRet = pScript->ExRelieveTgtMonsterBuff( relieveType, relieveLevel, relieveNum, false );
	lua_pushboolean( pState, bRet );
	return 1;
}


//检测relieve可能性
int CBattleScriptExtent::ExDetectReliveTgtMonsterBuff( lua_State* pState )
{
	BATTLE_EXT_FUNPRE;
	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) || !lua_isnumber( pState, -3 ))
	{
		D_ERROR("CBattleScriptExtent::ExRelieveTgtPlayerBuff 参数个数类型不正确\n");
		return false;
	}

	unsigned int relieveType = (unsigned int)lua_tonumber( pState, -3 );
	unsigned int relieveLevel = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int relieveNum = (unsigned int)lua_tonumber( pState, -1 );

	bool bRet = pScript->ExRelieveTgtMonsterBuff( relieveType, relieveLevel, relieveNum, true );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CBattleScriptExtent::ExAtkHaveItem( lua_State* pState )
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("ExAtkHaveItem 输入参数错误\n");
		return 0;
	}

	unsigned int itemID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int itemNum = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExAtkHaveItem( itemID, itemNum );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CBattleScriptExtent::ExTgtHaveItem( lua_State* pState )
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 ) || !lua_isnumber( pState, -2 ) )
	{
		D_ERROR("ExAtkHaveItem 输入参数错误\n");
		return 0;
	}

	unsigned int itemID = (unsigned int)lua_tonumber( pState, -2 );
	unsigned int itemNum = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExTgtHaveItem( itemID, itemNum );
	lua_pushboolean( pState, bRet );
	return 1;
}

int CBattleScriptExtent::ExAtkCreateWeakBuff( lua_State* pState )
{
	BATTLE_EXT_FUNPRE;

	if( !lua_isnumber( pState, -1 )  )
	{
		D_ERROR("ExAtkCreateWeakBuff 输入参数错误\n");
		return 0;
	}

	unsigned int time = (unsigned int)lua_tonumber( pState, -1 );
	bool bRet = pScript->ExAtkCreateWeakBuff( time );
	lua_pushboolean( pState, bRet );
	return 1;
}

void CCalSecondPropertyExtent::Init( lua_State* pLuaState )
{
	if ( !m_isvecInited )
	{
		//若有注册的MuxLib函数，则在此处压入m_vecRegFns;
		m_isvecInited = true;
	}

	///////////////////////////////////加入MuxLib表/////////////////////////////////////////
	CLuaRestoreStack rs( pLuaState );//准备恢复堆栈;

	//新函数表；
	lua_newtable( pLuaState );
	lua_setglobal( pLuaState, "MuxLib" );//置全局变量"MuxLib"为新表；
	lua_getglobal( pLuaState, "MuxLib" );//push表"MuxLib"；

	//注册向量中的函数至MuxLib表；
	ToRegFns* pToReg = NULL;
	for ( vector<ToRegFns>::iterator tmpiter=m_vecRegFns.begin();
		tmpiter!=m_vecRegFns.end(); ++tmpiter )
	{
		pToReg = &(*tmpiter);
		lua_pushstring( pLuaState, pToReg->fnName );
		lua_pushnumber( pLuaState, (lua_Number)(tmpiter-m_vecRegFns.begin()) );//closure注册为vec中的序号；
		lua_pushcclosure( pLuaState, LuaCallbackMeta<CBattleScriptExtent>, 1);
		lua_settable( pLuaState, -3 );//MuxLib[pToReg->fnName] = LuaCallback(with closure=本项vec序号)；
	}
}


