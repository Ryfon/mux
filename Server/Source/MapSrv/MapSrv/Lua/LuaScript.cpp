﻿// by dzj, modified from a codeproject article(http://www.codeproject.com/KB/cpp/luaincpp.aspx)；
// ---------------------------------------------------------------------------
// FILE NAME            : LuaScript.cpp
// ---------------------------------------------------------------------------
// DESCRIPTION :
//
// Simple debugging routines
// 
// ---------------------------------------------------------------------------
// VERSION              : 1.00
// DATE                 : 1-Sep-2005
// AUTHOR               : Richard Shephard
// ---------------------------------------------------------------------------
// LIBRARY INCLUDE FILES
#include <assert.h>
#include "LuaScript.h"

#include "ScriptExtent.h"

#include "../../../Base/Utility.h"

// ---------------------------------------------------------------------------

//============================================================================
// CLuaScriptBase::CLuaScriptBase
//---------------------------------------------------------------------------
// Constructor. Sets up the lua stack and the "this" table
//
// Parameter            Dir      Description
// ---------            ---      -----------
// CLuaVirtualMachine   IN       VM to run on
//
// Return
// ------
// None.
//
//============================================================================
CLuaScriptBase::CLuaScriptBase () : m_nArgs (0)
{
	m_pLuaState = lua_open ();
	assert( m_pLuaState && "CLuaScriptBase::CLuaScriptBase -> lua_open failed ");

	if ( m_pLuaState ) 
	{
		luaL_openlibs(m_pLuaState);//for lua 512, by dzj;
	}

	////建一个owner表，用于存放调用本脚本的脚本使用者指针；
	//lua_newtable( m_pLuaState );
	//lua_setglobal( m_pLuaState, OWNER_TB_NAME );
}

//============================================================================
// CLuaScriptBase::~CLuaScriptBase
//---------------------------------------------------------------------------
// Destructor
//
// Parameter   Dir      Description
// ---------   ---      -----------
// None.
//
// Return
// ------
// None.
//
//============================================================================
CLuaScriptBase::~CLuaScriptBase(void)
{
	if (m_pLuaState)
	{
		lua_close (m_pLuaState);
		m_pLuaState = NULL;
	}
}

//template <typename T_Extent/*扩展接口提供类*/>
//bool CLuaScriptBase::LoadFrom(const char *strFilename)
//{
//	assert(strFilename != NULL && "CLuaScriptBase::CompileFile -> strFilename == NULL");
//
//	T_Extent::Init( m_pLuaState );//将相关函数注册到新的lua_State;
//
//	bool fSuccess = false;
//	int iErr = luaL_loadfile(m_pLuaState, strFilename);
//
//	if (iErr == 0)
//	{
//		// Call main...
//		iErr = lua_pcall(m_pLuaState, 0, LUA_MULTRET, 0);
//		if (iErr == 0)
//		{
//			fSuccess = true;
//		}
//	} else {
//		if ( iErr == LUA_ERRSYNTAX )
//		{
//			//语法错误；
//			D_WARNING( "脚本%s语法错误", strFilename );
//		} else if ( iErr == LUA_ERRMEM ) {
//			//内存相关错误;
//			D_WARNING( "装载脚本%s时，发生内存错误", strFilename );
//		} else {
//			//其它错误；
//			D_WARNING( "装载脚本%s时，发生未知错误", strFilename );
//		}
//	}
//
//	return fSuccess;
//}

