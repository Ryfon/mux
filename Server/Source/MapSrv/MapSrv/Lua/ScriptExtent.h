﻿/**
* @file ScriptExtent.h
* @brief 提供脚本扩展函数
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: ScriptExtent.h
* 摘    要: 提供脚本扩展函数
* 作    者: dzj
* 完成日期: 2008.01.12
*
*/

#pragma once

#include "Utility.h"
#include "include/lua.hpp"
#include "LuaScript.h"

#include <vector>
using namespace std;

typedef int (*PFN_Reg)( lua_State* pState );
///注册到script的函数，实际注册本结构实例在vec中的序号；
struct ToRegFns
{
	ToRegFns( const char* inFnName, PFN_Reg pInFn )
	{
		SafeStrCpy( fnName, inFnName );
		pfnReg = pInFn;
	}

	char fnName[64];//在script中的函数名;
	PFN_Reg pfnReg;//函数；
};

/////唯一注册到lua的函数实体，具体函数使用closure区分， modified from a codeproject article by dzj.(http://www.codeproject.com/KB/cpp/luaincpp.aspx)；
//int LuaCallback( lua_State* pLuaState );

///唯一注册到lua的函数实体，具体函数使用closure区分， modified from a codeproject article by dzj.(http://www.codeproject.com/KB/cpp/luaincpp.aspx)；
template < typename T_Extent >
int LuaCallbackMeta( lua_State* pLuaState );

///用于NpcChat的扩展接口，为NPC对话脚本提供可调用函数；
class CNormalScriptExtent
{
public:
	///初始化，注册各扩展函数至lua；
	static void Init( lua_State* pLuaState );

public:
	///脚本扩展，用于调试；
	static int Debug( lua_State* pState);

	///NPC通知交互者显示信息；
	static int NpcShowMsg( lua_State* pState );
	//NPC通知自定义的显示消息
	static int NpcShowCustomizeMsg( lua_State* pState );
	///NPC设置NPC选项信息
	static int NpcSetOption( lua_State* pState );
	///NPC向对话玩家发送对话界面信息；
	static int NpcSendUiInfo( lua_State* pState );
	///NPC发送关对话框消息
	static int NpcCloseUiInfo( lua_State* pState );
	///NPC检测是否需要执行stage转换
	static int NpcCheckTransCon( lua_State* pState );
	///NPC对话脚本取聊天玩家的名字
	static int NpcGetPlayerName( lua_State* pState );

	/*任务相关*/
	///查询：调用者等级是否大于特定值；
	static int NpcQueryLevelLowerThan( lua_State* pState );
	///查询：调用者等级是否大于特定值；
	static int NpcQueryLevelMoreThan( lua_State* pState );
	///查询：调用者是否从未接过特定任务；
	static int NpcQueryNeverAcceptMission( lua_State* pState );
	///查询：指定任务在玩家的任务列表中；
	static int NpcQueryOnMissionList( lua_State* pState );

	static int NpcQueryIsMissionNotOnList( lua_State* pState );

	///查询：指定任务在任务列表但未达成目标
	static int NpcQueryNotAchieveMissionTarget( lua_State* pState );
	///查询：指定任务在任务列表达成目标
	static int NpcQueryAchieveMissionTarget( lua_State* pState );
	///查询：完成过任务
	static int NpcQueryIsFinishMissionBefore( lua_State* pState );
	///查询：包裹中是否有此物体
	static int NpcQueryPackageHave( lua_State* pState );	
	///查询：包裹中是不是没有这个物品
	static int NpcQueryPackageHaveNot( lua_State* pState );
	///查询:包裹中是否有空间容纳将要放进去的道具
	static int NpcQueryPackageCanContain( lua_State* pState );
	///通知显示任务的UI
	static int NpcOpenStandardUI( lua_State* pState );
	///操作：给玩家任务
	static int NpcGiveTask( lua_State* pState );
	///操作：执行任务(将任务属性设置为已达成目标但未交还)
	static int NpcProcessTask( lua_State* pState );
	///操作：完成任务
	static int NpcFinishTask( lua_State* pState );
	///操作：给玩家加经验
	static int NpcAddExp( lua_State* pState );
	///操作：给玩家加金钱
	static int NpcAddMoney( lua_State* pState );
	///操作：给玩家加属性;
	static int NpcAddProperty( lua_State* pState );
	///操作：给玩家设属性;
	static int NpcSetProperty( lua_State* pState );

	///操作：让玩家进指定副本；
	static int NpcTransPlayerToCopy( lua_State* pState );

	///操作：让玩家进指定伪副本；
	static int NpcTransPlayerToPCopy( lua_State* pState );

	///查询某伪副本是否可进
	static int NpcIsPCopyOpen( lua_State* pState );

	///NPC给玩家身上加计数器；
	static int NpcAddPlayerCounter( lua_State* pState );
	///NPC给玩家道具；
	static int NpcGiveItem( lua_State* pState );
	///NPC给玩家的道具随即一些等级
	static int NpcGiveItemRandomLevel( lua_State* pState );
	///NPC拿走玩家道具；
	static int NpcRemoveItem( lua_State* pState );
	///NPC取走触发脚本道具
	static int NpcRemoveScriptItem( lua_State* pState );
	///NPC在特定位置内刷特定数量怪物；
	static int NpcRefreshMonster( lua_State* pState );

	///NPC指令弹出托盘
	static int NpcShowPlateCmd( lua_State* pState );

	//NPC指令弹出邮件
	static int NpcShowEmailCmd( lua_State* pState );

	//NPC指令弹出保护道具托盘 
	static int NpcShowProtectPlateCmd( lua_State* pState );

	///创建一个跟随玩家的NPC
	static int NpcCreatePlayerNpc( lua_State* pState );//参数：NPC类型ID号；

	///检查是否有跟随玩家的NPC
	static int NpcCheckPlayerNpc( lua_State* pState );//参数：NPC类型ID号；
	///删去跟随玩家的NPC
	static int NpcDelPlayerNpc( lua_State* pState );//参数：NPC类型ID号；

	///玩家智能检测，大于
	static int NpcQueryIntelligenceMoreThan( lua_State* pState );//参数:检测值;

	///玩家智能检测， 小于
	static int NpcQueryIntelligenceLessThan( lua_State* pState );//参数:检测值;	

	static int NpcQueryPropertyEqual( lua_State* pState );//参数:检测值;	

	static int NpcQueryPropertyLowerThan( lua_State* pState );//参数:检测值;	

	static int NpcQueryPropertyMoreThan( lua_State* pState );//参数:检测值;	

	static int NpcGiveSkill( lua_State* pState );//参数:检测值;
	///指示玩家显示NPC商店；
	static int NpcShowShop( lua_State* pState );//参数:商店ＩＤ号;
	///指令玩家跳转地图;
	static int NpcSwitchMap( lua_State* pState );//参数:目标地图ID号，目标点（X，Y）坐标;
	///查询玩家是否在骑马状态
	static int NpcQueryRideState( lua_State* pState );
	//让对话Npc消失
	static int NpcChatNpcDisappear( lua_State* pState );
	//显示工会发布任务
	static int NpcDisplayIssuedTask( lua_State* pState );
	//工会npc对话中选择命令
	static int NpcSelectUnionCmd( lua_State* pState );
	//检查工会任务是否已经发布
	static int NpcHaveIssuedUnionTask( lua_State* pState );
	//加查玩家是否是公会会长
	static int NpcQueryIsUnionLeader( lua_State* pState );
	//查询工会等级
	static int NpcQueryUnionLevel( lua_State* pState );
	//查询玩家的仓库
	static int NpcShowStorage( lua_State* pState );
	//玩家开始答题系统
	static int NpcBeginOLTest( lua_State* pState );
	//查询是否在攻城时间中

	//宠物技能学习/*AUTOMATIC_ATTACK_MASK:2,AUTOMATIC_USE_DRUGS_MASK:4,AUTOMATIC_PICK_MASK:8,WEAPON_LEVELUP_MASK:16,OFFLINE_LEVELUP_MASK:32*/；
	static int NpcPetSkillLearn( lua_State* pState );
	//技能学习
	static int NpcSkillLearn( lua_State* pState );

	//显示npc对话系统对话框
	static int NpcChatSysDialog( lua_State* pState );
	//查询是否是魔头
	static int NpcIsEvil( lua_State* pState );
	//NPC随机取函数
	static int NpcRandNumber( lua_State* pState );
	//获取玩家的性别
	static int NpcGetPlayerSex( lua_State* pState );
	//检测玩家金钱是否达到上限
	static int NpcCanGiveMoney( lua_State* pState );
	//NPC发送邮件给玩家
	static int NpcSendSysMail( lua_State* pState );
#ifdef OPEN_PUNISHMENT
	//显示天谴相关信息
	static int NpcShowPunishPlayerInfo( lua_State* pState );
	//给予天谴任务
	static int NpcGivePunishTask( lua_State* pState );
	//是否在天谴活动中,接天谴任务的先置条件
	static int NpcIsInPunishment( lua_State* pState );
	//是否接过天谴任务
	static int NpcIsNotHavePunishTask( lua_State* pState );
	//天谴任务是否完成
	static int NpcIsPunishTaskAchieve( lua_State* pState );
	//获取天谴杀人计数器
	static int NpcGetPunishCounter( lua_State* pState );
	//获取天谴目标姓名
	static int NpcGetPunishTargetName( lua_State* pState );
	//完成天谴任务
	static int NpcFinishPunishTask( lua_State* pState );
#endif //OPEN_PUNISHMENT
	//增加种族声望
	static int NpcAddRacePrestige( lua_State* pState );
	//创建BUFF
	static int NpcCreateBuff( lua_State* pState );
	//保存复活点
	static int NpcSaveRebirthPos( lua_State* pState );

#ifdef NEW_NPC_INTERFACE
	//1 判断是否加入了战盟
	static int NpcIsInUnion( lua_State* pState );	

	//是否是战盟盟主
	static int NpcIsUnionLeader( lua_State* pState );
	
	//3 判断是否为组队状态
	static int NpcIsInTeam( lua_State* pState );

	//4 判断是否为队长
	static int NpcIsTeamCaptain( lua_State* pState );

	//5 判断小队有几人
	static int NpcTeamNumberMoreThan( lua_State* pState );
	static int NpcTeamNumberLowerThan( lua_State* pState );
	static int NpcTeamNumberEqual( lua_State* pState );

	//6 判断好友有几人
	static int NpcFriendNumberMoreThan( lua_State* pState );
	static int NpcFriendNumberLowerThan( lua_State* pState );
	static int NpcFrinedNumberEqual( lua_State* pState );
	
	//7 判断仇人有几人
	static int NpcFoeNumberMoreThan( lua_State* pState );
	static int NpcFoeNumberLowerThan( lua_State* pState );
	static int NpcFoeNumberEqual( lua_State* pState );

#endif	//NEW_NPC_INTERFACE

#ifdef ANTI_ADDICTION
	static int NpcIsTriedOrAddictionState( lua_State* pState );
#endif //ANTI_ADDICTION
	
	static int NpcCreateNpcByChatTarget( lua_State* pState );
	//添加标记
	static int NpcAddMark( lua_State* pState );
	//检查标记
	static int NpcCheckMark( lua_State* pState );
	//检查当前时间
	static int NpcCheckExTime( lua_State* pState );
	//比较2个时间
	static int NpcCheckTime( lua_State* pState );
	//弹出族长界面
	static int NpcShowRaceMasterPlate( lua_State* pState );
	//地图跳转点的种族归属是否和指定种族一致
	static int NpcCheckMapSwitchRace( lua_State* pState );
	//能否接受护送任务
	static int NpcCanGiveNpcFollowTask( lua_State* pState );
	//提示玩家公告 ( content ,position, isallmap ) position 0:跑马灯 1:B中央公告显示, isallmap:是不是所有服务器 
	static int NpcShowNotices( lua_State* pState );
	//提示可变参数的公告( msgid, position,isallmap, isparam1...param5 )  position 0:跑马灯 1:B中央公告显示, isallmap:是不是所有服务器 
	static int NpcShowParamsNotices( lua_State* pState );
	//玩家是否含有技能
	static int NpcIsHaveSkill( lua_State* pState );
	//玩家运行了凋落集
	static int NpcRunDropSet( lua_State* pState );
	//获取玩家的位置信息
	static int NpcGetPlayerPosX( lua_State* pState );
	static int NpcGetPlayerPosY( lua_State* pState );
	static int NpcGetPlayerMapID( lua_State* pState );
	//吸引玩家周围怪
	static int NpcPlayerCallMonster( lua_State* pState );
	//玩家随机移动
	static int NpcPlayerRandMove( lua_State* pState );
	//显示玩家拍卖行界面
	static int NpcShowAuctionPlate( lua_State* pState );

public:
	///调用注册的某个函数，由LuaCallback调用；
	static int CallFn( int nFnId, lua_State* pState )
	{
		if ( (nFnId<0) || (nFnId >= (int)m_vecRegFns.size()) )
		{
			//调用序号非法;
			return -1;
		}

		return m_vecRegFns[nFnId].pfnReg( pState );
	}

private:
	CNormalScriptExtent(){}   //constructor
	~CNormalScriptExtent(){}  //destructor

private:
	static bool             m_isvecInited;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
	static vector<ToRegFns> m_vecRegFns;
};

///用于AI的扩展接口，为AI脚本提供可调用函数；
class CAIScriptExtent
{
public:
	///初始化，注册各扩展函数至lua；
	static void Init( lua_State* pLuaState );

public:
	///脚本扩展，用于调试；
	static int Debug( lua_State* pState);

public:
	///调用注册的某个函数，由LuaCallback调用；
	static int CallFn( int nFnId, lua_State* pState )
	{
		if ( (nFnId<0) || (nFnId >= (int)m_vecRegFns.size()) )
		{
			//调用序号非法;
			return -1;
		}

		return m_vecRegFns[nFnId].pfnReg( pState );
	}

private:
	CAIScriptExtent(){}   //constructor
	~CAIScriptExtent(){}  //destructor

private:
	static bool             m_isvecInited;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
	static vector<ToRegFns> m_vecRegFns;
};

#ifdef STAGE_MAP_SCRIPT
///用于地图脚本的扩展接口，为AI脚本提供可调用函数；
class CMapScriptExtent
{
public:
	///初始化，注册各扩展函数至lua；
	static void Init( lua_State* pLuaState );

public:
	///调用注册的某个函数，由LuaCallback调用；
	static int CallFn( int nFnId, lua_State* pState )
	{
		if ( (nFnId<0) || (nFnId >= (int)m_vecRegFns.size()) )
		{
			//调用序号非法;
			return -1;
		}

		return m_vecRegFns[nFnId].pfnReg( pState );
	}

public:
	///副本地图脚本中设置玩家副本阶段
	static int MSSetResPlayerCopyStage( lua_State* pState );
	///副本地图设置玩家的outmapinfo;
    static int MSSetPlayerOutPos( lua_State* pState );	
	///副本地图设置所有玩家的outmapinfo;
    static int MSSetAllPlayerOutPos( lua_State* pState );	
	///副本地图踢相应玩家；
	static int MSKickResPlayer( lua_State* pState );	
	///副本结束(同时踢所有玩家)；
	static int MSEndCopy( lua_State* pState );
	///设置某伪副本可进；
	static int MSPCopyOpen( lua_State* pState );
	///设置某伪副本不可进；
	static int MSPCopyClose( lua_State* pState );
	///地图脚本调整对应玩家位置(例如进入地图玩家等)
	static int MSSetResPlayerPos( lua_State* pState );

	///设置本地图的TD标记；
	static int MSSetTDFlag( lua_State* pState );

	///地图脚本通知对应玩家消息(例如进入地图的玩家，离开地图的玩家等)
	static int MSNotiResPlayerMsg( lua_State* pState );
	///地图内广播；
	static int MSBrocastMsg( lua_State* pState );
	///地图脚本计数器添加
	static int MSAddCounterOfMap( lua_State* pState );
	///地图脚本计时器添加
	static int MSAddTimerOfMap( lua_State* pState );
	///地图脚本计时器查询
	static int MSIsHaveTimerOfMap( lua_State* pState );
	///地图脚本添加定时器	
	static int MSAddDetailTimerOfMap( lua_State* pState );
	//地图脚本给脚本回调攻城结束
	static int MSAttackCityEnd( lua_State* pState );
	//创建NPC
	static int MSCreateNpc( lua_State* pState );
	//删除特定Id的Npc
	static int MSDelNpc( lua_State* pState );
	//该mapsrv上的map删除
	static int MSDelNpcByMapID( lua_State* pState );
	//全服喊话
	static int MSWorldNotify( lua_State* pState );
	//删除计时器
	static int MSDeleteTimer( lua_State* pState );
	//删除定时器
	static int MSDeleteDetailTimer( lua_State* pState );
	//通知攻城任务状态
	static int MSNotifyWarTaskState( lua_State* pState );
	//是否在攻城中
	static int MSIsInAttackWar( lua_State* pState );
	//
	static int MSCreateNpcBySet( lua_State* pState );
	//获取最后一刀种族
	static int MSGetLastKillRace( lua_State* pState );
	//修改地图归属种族
	static int MSModifyMapRace( lua_State* pState );
	//获取地图归属种族
	static int MSGetMapRace( lua_State* pState );
	//获取城市等级
	static int MSGetCityLevel( lua_State* pState );
	//设置物件NPC的状态
	static int MSSetItemNpcStatus( lua_State* pState );
	//修改复活点种族归属
	static int MSModifyRebirthPtRace( lua_State* pState );
	//开启关闭生成器
	static int MSOpenCreator( lua_State* pState );
	static int MSCloseCreator( lua_State* pState );
	//修改跳转点种族
	static int MSModifyTelpotRace( lua_State* pState );
	//种族A是否对种族B宣战
	static int MSIsDeclareWar( lua_State* pState );
	//添加buff
	static int MSCreateBuff( lua_State* pState );
	//删除buff
	static int MSDeleteBuff( lua_State* pState );
	//杀死地图上所有玩家
	static int MSKillPlayerByRace( lua_State* pState );
	//删除玩家身上某道具
	static int MSDelPlayerItem( lua_State* pState );
	//获得当前玩家姓名
	static int MSGetScriptUserName( lua_State* pState );
	//传送玩家
	static int MSSwitchMapPlayer( lua_State* pState );
	//创建WarTimer
	static int MSAddWarTimer( lua_State* pState );
	//删除WarTimer
	static int MSDelWarTimer( lua_State* pState );
	//创建WarCounter
	static int MSAddWarCounter( lua_State* pState );
	//删除warCounter
	static int MSDelWarCounter( lua_State* pState );
	//获得计数器的数字
	static int MSGetWarCounterNum( lua_State* pState );
	//全地图通知计数器信息
	static int MSMapDisplayWarCounterInfo( lua_State* pState );
	//设置计数器的数字
	static int MSSetWarCounterNum( lua_State* pState );
	//单玩家通知计数器的数字
	static int MSPlayerDisplayWarCounterInfo( lua_State* pState );
	//通知公告或跑马灯
	static int MSShowNotices( lua_State* pState );
	//提示可变参数的公告( msgid, position,isallmap, isparam1...param5 )  position 0:跑马灯 1:B中央公告显示, isallmap:是不是所有服务器 
	static int MSShowParamsNotices( lua_State* pState );
	//设置碰撞块属性
	static int MSChgComblockAttr( lua_State* pState );
	//给地图上所有玩家创建一个buff
	static int MSCreateMapBuff( lua_State* pState );

	//取地图实例保存的脚本用数据
    static int MSGetMapScriptTarget( lua_State* pState );
	//保存地图实例脚本用数据；
    static int MSSetMapScriptTarget( lua_State* pState );
	//设置攻城战真正时间
	static int MSWarRealStart( lua_State* pState );

private:
	CMapScriptExtent(){}   //constructor
	~CMapScriptExtent(){}  //destructor

private:
	static bool             m_isvecInited;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
	static vector<ToRegFns> m_vecRegFns;
};
#endif //STAGE_MAP_SCRIPT

///扩展脚本类，为脚本提供可调用函数；
class CFightScriptExtent
{
public:
	///初始化，注册各扩展函数至lua；
	static void Init( lua_State* pLuaState );

public:
	///脚本扩展，用于调试；
	static int Debug( lua_State* pState);
	////////////////////////////////////////////////////////////////
	///战斗判断脚本扩展；
	///战斗判断，取攻击玩家的等级;
	static int FPreGetAttackPlayerLevel( lua_State* pState );
	//战斗判断，获取攻击玩家阵营
	static int FPreGetAttackPlayerRace( lua_State* pState );

	///战斗判断，取被攻击玩家的等级
	static int FPreGetTargetPlayerLevel( lua_State* pState );
	///战斗判断，取被攻击玩家的等级
	static int FPreGetTargetPlayerRace( lua_State* pState );

	///战斗判断，取被攻击怪物的类型;
	static int FPreGetTargetMonsterType( lua_State* pState );

	//战斗判断, 取攻击玩家的战斗模式
	static int FPreGetAttackPlayerBattleMode( lua_State* pState );

	///战斗判断脚本扩展；
	////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////
	///战斗计算公式脚本扩展；
	///////攻击者：玩家//////////////////////////////////////////////////
	//1 获取攻击者的等级
	static int FCalGetAtkPlayerLevel( lua_State* pState );
	//2 获取攻击者装备最小物理攻击
	static int FCalGetAtkPlayerMinPhyAttack( lua_State* pState );
	//3 获取攻击者装备最大物理攻击
	static int FCalGetAtkPlayerMaxPhyAttack( lua_State* pState );
	//4 获取攻击者装备最小魔法攻击
	static int FCalGetAtkPlayerMinMagAttack( lua_State* pState );
	//5 获取攻击者装备最大魔法攻击
	static int FCalGetAtkPlayerMaxMagAttack( lua_State* pState );
	//6 获取攻击者的职业
	static int FCalGetAtkPlayerClass( lua_State* pState );
	//7 获取攻击者攻击技能的暴击率
	static int FCalGetAtkSkillCriRate( lua_State* pState );
	//8 获取攻击者攻击技能的攻击系数
	static int FCalGetAtkSkillAttackRate( lua_State* pState );
	//9 获取攻击技能的附加伤害
	static int FCalGetAdditionalSkillDamage( lua_State* pState );
	//10 获取攻击者的STR
	static int FCalGetAtkPlayerStr( lua_State* pState );
	//11 获取攻击者的AGI
	static int FCalGetAtkPlayerAgi( lua_State* pState );
	//12 获取攻击者的INT
	static int FCalGetAtkPlayerInt( lua_State* pState );
	//13 获取攻击者的物理攻击
	static int FCalGetAtkPlayerPhyAttack( lua_State* pState );
	//14 获取攻击者的魔法攻击
	static int FCalGetAtkPlayerMagAttack( lua_State* pState );
	//15 获取攻击者的物理防御
	static int FCalGetAtkPlayerPhyDefend( lua_State* pState );
	//16 获取攻击者的魔法防御
	static int FCalGetAtkPlayerMagDefend( lua_State* pState );
	//17 获取攻击者的物理命中
	static int FCalGetAtkPlayerPhyHit( lua_State* pState );
	//18 获取攻击者的魔法命中
	static int FCalGetAtkPlayerMagHit( lua_State* pState );
	//19 获取攻击者的物理暴击等级
	static int FCalGetAtkPlayerPhyCriLevel( lua_State* pState );
	//20 获取攻击者的魔法暴击等级
	static int FCalGetAtkPlayerMagCriLevel( lua_State* pState );
	//21 获取攻击者的物理攻击百分比
	static int FCalGetAtkPlayerPhyAttackRate( lua_State* pState );
	//22 获取攻击者的魔法攻击百分比
	static int FCalGetAtkPlayerMagAttackRate( lua_State* pState );
	//23 获取攻击者附加物理暴击伤害
	static int FCalGetAtkPlayerAdditionPhyCriDamage( lua_State* pState );
	//24 获取攻击者附加魔法暴击伤害
	static int FCalGetAtkPlayerAdditionMagCriDamage( lua_State* pState );
	//25 获取攻击者物理伤害提升百分比 
	static int FCalGetAtkPlayerPhyDamageUpRate( lua_State* pState );
	//26 获取攻击者魔法伤害提升百分比
	static int FCalGetAtkPlayerMagDamageUpRate( lua_State* pState );
	//27 获取BUFF附加提升物理暴击率
	static int FCalGetAtkPlayerAdditionalPhyCriRate( lua_State* pState );
	//28 获取BUFF附加提升魔法暴击率
	static int FCalGetAtkPlayerAdditionalMagCriRate( lua_State* pState );
	//29 获取技能附加物理暴击伤害提升百分比
	static int FCalGetAtkPlayerPhyCriDamageUpRate( lua_State* pState );
	//30 获取技能附加魔法暴击伤害提升百分比
	static int FCalGetAtkPlayerMagCriDamageUpRate( lua_State* pState );
	//31 获取攻击者物理穿透等级
	static int FCalGetAtkPlayerPhyRiftLevel( lua_State* pState );
	//32 获取攻击者魔法穿透等级
	static int FCalGetAtkPlayerMagRiftLevel( lua_State* pState );
	//33 获取攻击者额外物理伤害
	static int FCalGetAtkPlayerAdditionalPhyDamage( lua_State* pState );
	//34 获取攻击者额外魔法伤害
	static int FCalGetAtkPlayerAdditionalMagDamage( lua_State* pState );
	//35 获取攻击技能的类型 物理 魔法
	static int FCalGetAtkSkillType( lua_State* pState );
	//36 获取攻击技能的暴击附加伤害
	static int FCalGetAtkSkillAdditionCriDamage( lua_State* pState );
	//37 获取攻击技能的ID号
	static int FCalGetAtkSkillID( lua_State* pState );
	//38 获取攻击技能的暴击伤害系数
	static int FCalGetAtkSkillCriDamageRate( lua_State* pState );
	//39 获取人物的物理命中率
	static int FCalGetAtkPlayerPhyHitRate( lua_State* pState );
	//40 获取人物的魔法命中率
	static int FCalGetAtkPlayerMagHitRate( lua_State* pState );

	////////////////////////////////////////攻击者：玩家//////////////////////

	////被攻击者：玩家//////////////////////////////////////
	//1 获取被攻击玩家的等级
	static int FCalGetTgtPlayerLevel( lua_State* pState );
	//2 获取被攻击玩家的物理防御
	static int FCalGetTgtPlayerPhyDefend( lua_State* pState );
	//3 获取被攻击玩家的魔法防御
	static int FCalGetTgtPlayerMagDefend( lua_State* pState );
	//4 获取被攻击玩家的物理防御百分比
	static int FCalGetTgtPlayerPhyDefendRate( lua_State* pState );
	//5 获取被攻击玩家的魔法防御百分比
	static int FCalGetTgtPlayerMagDefendRate( lua_State* pState );
	//6 获取被攻击玩家的减免物理暴击等级
	static int FCalGetTgtPlayerPhyCriDerateLevel( lua_State* pState );
	//7 获取被攻击玩家的减免魔法暴击等级
	static int FCalGetTgtPlayerMagCriDerateLevel( lua_State* pState );
	//8 获取被攻击者物理暴击伤害减免
	static int FCalGetTgtPlayerPhyCriDamageDerate( lua_State* pState );
	//9 获取被攻击者魔法暴击伤害减免
	static int FCalGetTgtPlayerMagCriDamageDerate( lua_State* pState );
	//10 获取被攻击者物理闪避等级
	static int FCalGetTgtPlayerPhyJouk( lua_State* pState );
	//11 获取被攻击者魔法闪避等级
	static int FCalGetTgtPlayerMagJouk( lua_State* pState );
	//12 获取被攻击者物理伤害减免
	static int FCalGetTgtPlayerPhyDamageDerate( lua_State* pState );
	//13 获取被攻击者魔法伤害减免
	static int FCalGetTgtPlayerMagDamageDerate( lua_State* pState );
	//14 获取被攻击者物理闪避百分比
	static int FCalGetTgtPlayerPhyJoukRate( lua_State* pState );
	//15 获取被攻击者魔法闪避百分比
	static int FCalGetTgtPlayerMagJoukRate( lua_State* pState );
	//16 获取被攻击者的STR
	static int FCalGetTgtPlayerStr( lua_State* pState );
	//17 获取被攻击的AGI
	static int FCalGetTgtPlayerAgi( lua_State* pState );
	//18 获取被攻击者的INT
	static int FCalGetTgtPlayerInt( lua_State* pState );
	//19 获取被攻击者的职业
	static int FCalGetTgtPlayerClass( lua_State* pState );

	////////////////////////被攻击者：玩家/////////////////

	///////////////////////攻击者：怪物///////////////////
	//1 攻击怪物的等级
	static int FCalGetAtkMonsterLevel( lua_State* pState );
	//2 攻击怪物的技能类型
	static int FCalGetAtkMonsterSkillType( lua_State* pState );
	//3 攻击怪物的技能命中
	static int FCalGetAtkMonsterSkillHit( lua_State* pState );
	//4 怪物攻击技能的附加命中
	static int FCalGetAtkMonsterSkillAddHit( lua_State* pState );
	//5 怪物攻击技能的暴击率
	static int FCalGetAtkMonsterSkillCriRate( lua_State* pState );
	//6 怪物攻击技能的最小攻击
	static int FCalGetAtkMonsterSkillMinAtk( lua_State* pState );
	//7 怪物攻击技能的最大攻击
	static int FCalGetAtkMonsterSkillMaxAtk( lua_State* pState );
	//8 怪物攻击技能的ID号
	static int FCalGetAtkMonsterSkillID( lua_State* pState );
	//9 怪物的Rank
	static int FCalGetAtkMonsterRank( lua_State* pState );
	//10 怪物技能的伤害追加
	static int FCalGetAtkMonsterSkillAdditionDamage( lua_State* pState );
	//////////////////////////////攻击者:怪物/////////////////////////

	///////////////////////////被攻击者：怪物///////////////////////
	//1 被攻击者怪物的等级
	static int FCalGetTgtMonsterLevel( lua_State* pState );
	//2 被攻击怪物的物理防御
	static int FCalGetTgtMonsterPhyDef( lua_State* pState );
	//3 被攻击怪物的魔法防御
	static int FCalGetTgtMonsterMagDef( lua_State* pState );
	//4 被攻击怪物的物理回避
	static int FCalGetTgtMonsterPhyJouk( lua_State* pState );
	//5 被攻击怪物的魔法回避
	static int FCalGetTgtMonsterMagJouk( lua_State* pState );
	//6 被攻击怪物的rank
	static int FCalTgtMonsterRank( lua_State* pState );
	///////////////////////////////被攻击者：怪物///////////////

	//////////////通用接口:///////////////////////////
	//1  输入0~1的值 返回true or false
	static int FCalRandRate( lua_State* pState );
	//2  返回a到b中间的任何一值
	static int FCalRandNumber( lua_State* pState );


	///战斗计算公式脚本扩展；
	////////////////////////////////////////////////////////////////

public:
	///调用注册的某个函数，由LuaCallback调用；
	static int CallFn( int nFnId, lua_State* pState )
	{
		if ( (nFnId<0) || (nFnId >= (int)m_vecRegFns.size()) )
		{
			//调用序号非法;
			return -1;
		}

		return m_vecRegFns[nFnId].pfnReg( pState );
	}


private:
	CFightScriptExtent(){}    //constructor
	~CFightScriptExtent(){}   //destructor

private:
	static bool             m_isvecInited;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
	static vector<ToRegFns> m_vecRegFns;
};


#ifdef LEVELUP_SCRIPT

///扩展脚本类，为脚本提供可调用函数；
class CLevelUpScriptExtent
{
public:
	///初始化，注册各扩展函数至lua；
	static void Init( lua_State* pLuaState );

public:
	///调用注册的某个函数，由LuaCallback调用；
	static int CallFn( int nFnId, lua_State* pState )
	{
		if ( (nFnId<0) || (nFnId >= (int)m_vecRegFns.size()) )
		{
			//调用序号非法;
			return -1;
		}

		return m_vecRegFns[nFnId].pfnReg( pState );
	}

public:
	//发送系统聊天消息
	static int SendSystemMsg( lua_State* pState );
	//设置最大基础hp
	static int LevelSetBaseMaxHp( lua_State* pState );
	//设置最大基础mp
	static int LevelSetBaseMaxMp( lua_State* pState );
	//设置力量
	static int LevelSetStr( lua_State* pState );
	//设置敏捷
	static int LevelSetAgi( lua_State* pState );
	//设置智力
	static int LevelSetInt( lua_State* pState );
	//设置基础物理攻击力
	static int LevelSetBasePhyAtk( lua_State* pState );
	//设置基础魔法攻击力
	static int LevelSetBaseMagAtk( lua_State* pState );
	//设置基础物理防御力
	static int LevelSetBasePhyDef( lua_State* pState );
	//设置基础魔法防御力
	static int LevelSetBaseMagDef( lua_State* pState );
	//设置基础物理命中
	static int LevelSetBasePhyHit( lua_State* pState );
	//设置基础魔法命中
	static int LevelSetBaseMagHit( lua_State* pState );
	//设置下级经验
	static int LevelSetNextLevelExp( lua_State* pState );
	//添加技能
	static int LevelAddSkill( lua_State* pState );
	//添加道具
	static int LevelAddItem( lua_State* pState );
	//添加一个buff
	static int LevelAddBuff( lua_State* pState );
	//添加sp豆
	static int LevelAddSpBean( lua_State* pState );
	//获取性别
	static int LevelSex( lua_State* pState );
	//获取种族
	static int LevelRace( lua_State* pState );
	//升级赠送邮件
	static int LevelSendSysMail( lua_State* pState );
	//通知公告或跑马灯
	static int LevelShowNotices( lua_State* pState );
	//提示可变参数的公告
	static int LevelShowParamsNotices( lua_State* pState );

private:
	CLevelUpScriptExtent(){}    //constructor
	~CLevelUpScriptExtent(){}   //destructor

private:
	static bool             m_isvecInited;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
	static vector<ToRegFns> m_vecRegFns;
};

#endif //LEVELUP_SCRIPT

class CBattleScriptExtent 
{
public:
	///初始化，注册各扩展函数至lua；
	static void Init( lua_State* pLuaState );

public:
	///调用注册的某个函数，由LuaCallback调用；
	static int CallFn( int nFnId, lua_State* pState )
	{
		if ( (nFnId<0) || (nFnId >= (int)m_vecRegFns.size()) )
		{
			//调用序号非法;
			return -1;
		}

		return m_vecRegFns[nFnId].pfnReg( pState );
	}

private:
	CBattleScriptExtent(){}    //constructor
	~CBattleScriptExtent(){}   //destructor

	static int ExSendAtkSysChat( lua_State* pState );				//针对攻击玩家输出log
	static int ExSendTgtSysChat( lua_State* pState );				//针对目标玩家输出log
	static int ExIsSkillInCD( lua_State* pState );					//-- 检查技能是否在 CD 
	static int ExPlayerHaveItem( lua_State* pState);				//-- 玩家包裹中是否有物品
	static int ExPlayerConsume(lua_State* pState);					 //-- 玩家消耗
	static int ExAddSkillExp( lua_State* pState);				//-- 加技能经验
	static int ExRandRate( lua_State* pState );					//减少效率
	static int ExRandNumber( lua_State* pState );				//
	static int ExSetAbsorbDamage( lua_State* pState );		//设置吸收
	static int ExPlayerDistance( lua_State* pState );	//算出2点距离的平方和
	static int ExAddPlayerExp( lua_State* pState );			//增加攻击者的经验
	static int ExEndTgtPlayerSheepBuff( lua_State* pState );		//结束目标变羊状态 
	static int ExLog( lua_State* pState );		//打出日志
	static int ExEndTgtPlayerAllDebuff( lua_State* pState );			//所有的debuff
	static int ExEndTgtPlayerControlBuff( lua_State* pState );		//控制类的技能
	//relieve Buff
	static int ExRelieveTgtPlayerBuff( lua_State* pState );
	//检测relieve可能性
	static int ExDetectReliveTgtPlayerBuff( lua_State* pState );
	//relieve Buff
	static int ExRelieveTgtMonsterBuff( lua_State* pState );
	//检测relieve可能性
	static int ExDetectReliveTgtMonsterBuff( lua_State* pState );
	static int ExAtkHaveItem( lua_State* pState );
	static int ExTgtHaveItem( lua_State* pState );
	static int ExAtkCreateWeakBuff( lua_State* pState );


private:
    static bool             m_isvecInited;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
	static vector<ToRegFns> m_vecRegFns;
};

class CCalSecondPropertyExtent
{
public:
	///初始化，注册各扩展函数至lua；
	static void Init( lua_State* pLuaState );

public:
	///调用注册的某个函数，由LuaCallback调用；
	static int CallFn( int nFnId, lua_State* pState )
	{
		if ( (nFnId<0) || (nFnId >= (int)m_vecRegFns.size()) )
		{
			//调用序号非法;
			return -1;
		}

		return m_vecRegFns[nFnId].pfnReg( pState );
	}

private:
	CCalSecondPropertyExtent(){}    //constructor
	~CCalSecondPropertyExtent(){}   //destructor

private:
	static bool             m_isvecInited;//m_vecRegFns是否已初始化，已初始化者不再填充，原bug，同一类型脚本多个初始化时，会反复向vec中重复填充；
	static vector<ToRegFns> m_vecRegFns;
};



