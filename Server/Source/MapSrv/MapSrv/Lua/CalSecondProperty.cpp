﻿#include "CalSecondProperty.h"
#include "ScriptExtent.h"
#include "../Player/Player.h"

CCalSecondPropertyScript* CCalSecondPropertyScriptManager::m_pCalSecondPropScript;	//二级属性换算脚本

CCalSecondPropertyScript::CCalSecondPropertyScript(void)
:m_pPlayer(NULL)
{
}

CCalSecondPropertyScript::~CCalSecondPropertyScript(void)
{

}

bool CCalSecondPropertyScript::Init( const char* luaScriptFile )
{
	return LoadFrom<CCalSecondPropertyExtent>( luaScriptFile );
}

bool CCalSecondPropertyScript::FnCalPhyAtk( CPlayer * player, int & ret ) 
{
	m_pPlayer = player;
	return CallScriptCalPhyAtk( ret );
}

bool CCalSecondPropertyScript::FnCalPhyDef( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalPhyDef( ret );
}

bool CCalSecondPropertyScript::FnCalPhyHit( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalPhyHit( ret );
}

bool CCalSecondPropertyScript::FnCalPhyJouk( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalPhyJouk( ret );
}

bool CCalSecondPropertyScript::FnCalMagAtk( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalMagAtk( ret );
}

bool CCalSecondPropertyScript::FnCalMagDef( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalMagDef( ret );
}

bool CCalSecondPropertyScript::FnCalMagHit( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalMagHit( ret );
}

bool CCalSecondPropertyScript::FnCalMagJouk( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalMagJouk( ret );
}

bool CCalSecondPropertyScript::FnCalExcellent( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalExcellent( ret );
}

bool CCalSecondPropertyScript::FnCalCritical( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalCritical( ret );
}

bool CCalSecondPropertyScript::FnCalHP( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalHP( ret );
}

bool CCalSecondPropertyScript::FnCalHPrecovery( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalHPrecovery( ret );
}

bool CCalSecondPropertyScript::FnCalMP( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalMP( ret );
}

bool CCalSecondPropertyScript::FnCalMPrecovery( CPlayer * player, int & ret )
{
	m_pPlayer = player;
	return CallScriptCalMPrecovery( ret );
}

bool CCalSecondPropertyScript::CallScriptCalPhyAtk( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalPhyAtk" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalPhyAtk函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseAtk为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalPhyAtk函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalPhyDef( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalPhyDef" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalPhyDef函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseDef为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalPhyDef函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalPhyHit( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalPhyHit" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalPhyHit函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseDef为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalPhyHit函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalPhyJouk( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalPhyJouk" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalPhyJouk函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseDef为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalPhyJouk函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalMagAtk( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalMagAtk" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalMagAtk函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseDef为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalMagAtk函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalMagDef( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalMagDef" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalMagDef函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseDef为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalMagDef函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalMagHit( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalMagHit" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalMagHit函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseDef为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalMagHit函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalMagJouk( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalMagJouk" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalMagJouk函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseDef为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalMagJouk函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalExcellent( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalExcellent" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalExcellent函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseDef为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalExcellent函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalCritical( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalCritical" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalCritical函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalBaseDef为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalCritical函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalHP( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalHP" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalHP函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalHP为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalHP函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalHPrecovery( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalHPrecovery" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalHPrecovery函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalHPrecovery为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalHPrecovery函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalMP( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalMP" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalMP函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalMP为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalMP函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}

bool CCalSecondPropertyScript::CallScriptCalMPrecovery( int & ret )
{
	ret = 0;
	if (NULL == m_pPlayer)
	{
		return NULL;
	}
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCalMPrecovery" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义转换函数；
		D_ERROR( "战斗脚本未定义FnCalMPrecovery函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, m_pPlayer->GetStrength() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetVitality() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetAgility() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetSpirit() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetIntelligence() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetClass() );
	lua_pushnumber( m_pLuaState, m_pPlayer->GetLevel() );

	//调用检测函数；FnCalMPrecovery为7输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 7, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCalMPrecovery函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	ret =  (int)(lua_tonumber(m_pLuaState, -1));	

	return true;
}
