﻿/**
* @file FightPreScript.cpp
* @brief 战斗判定脚本
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: FightPreScript.cpp
* 摘    要: 管理战斗判定脚本
* 作    者: dzj
* 完成日期: 2008.03.04
*
*/

#include "FightPreScript.h"
#include "../Player/Player.h"
#include "ScriptExtent.h"

const char* FPRE_SCRIPT_NAME = "fightprechk.lua";

CFightPreScript* CFightPreScriptManager::m_pFightPreScript;
//map<const char*, CFightPreScript*> CFightPreScriptManager::m_NameToScripts;
//map<lua_State*, CFightPreScript*>  CFightPreScriptManager::m_StateToScripts;

bool CFightPreScript::Init( const char* luaScriptFile ) 
{ 
	return LoadFrom<CFightScriptExtent>( luaScriptFile );
};

///玩家直接攻击怪物;
FIGHTPRE_RST CFightPreScript::OnPvcDirectCheck( CPlayer* pPlayer/*attacker*/, CMonster* pTargetMonster )
{
	ResetPaticipant();
	m_pAttackPlayer = pPlayer;
	m_pTargetMonster = pTargetMonster;
	return CallScriptOnPvcDirectCheck();
}

///玩家强制攻击怪物;
FIGHTPRE_RST CFightPreScript::OnPvcForceCheck( CPlayer* pPlayer/*attacker*/, CMonster* pTargetMonster )
{
	ResetPaticipant();
	m_pAttackPlayer = pPlayer;
	m_pTargetMonster = pTargetMonster;
	return CallScriptOnPvcForceCheck();
}

///怪物直接攻击玩家;
FIGHTPRE_RST CFightPreScript::OnCvpDirectCheck( CMonster* pAttackMonster/*attacker*/, CPlayer* pTargetPlayer )
{
	ResetPaticipant();
	m_pAttackMonster = pAttackMonster;
	m_pTargetPlayer = pTargetPlayer;
	return CallScriptOnCvpDirectCheck();
}

///玩家直接攻击玩家;
FIGHTPRE_RST CFightPreScript::OnPvpDirectCheck( CPlayer* pPlayer/*attacker*/, CPlayer* pTargetPlayer )
{
	ResetPaticipant();
	m_pAttackPlayer = pPlayer;
	m_pTargetPlayer = pTargetPlayer;
	return CallScriptOnPvpDirectCheck();
}

///调用脚本PVC战斗判断函数，玩家直接攻击怪物；
FIGHTPRE_RST CFightPreScript::CallScriptOnPvcDirectCheck()
{
	g_debugPlayer = m_pAttackPlayer;

	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnPvcDirectCheck" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "脚本未定义PVC战斗判断函数:OnPvcDirectCheck" );
		return FPRE_CHECK_FAILED;
	}
	//调用检测函数；
	if ( ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
		|| ( ! lua_isnumber( m_pLuaState, -1 ) )   //或者返回值非数字值；
		)
	{
		D_ERROR( "执行PVC战斗判断函数失败1" );
		return FPRE_CHECK_FAILED;
	}

	g_debugPlayer = NULL;

	//取调用结果；
	return (FIGHTPRE_RST) ( (int)(lua_tonumber( m_pLuaState, -1 )) );
}

///调用脚本PVC战斗判断函数，玩家强制攻击怪物；
FIGHTPRE_RST CFightPreScript::CallScriptOnPvcForceCheck()
{
	g_debugPlayer = m_pAttackPlayer;

	CLuaRestoreStack rs( m_pLuaState );

    ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnPvcForceCheck" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "脚本未定义PVC战斗判断函数:OnPvcForceCheck" );
		return FPRE_CHECK_FAILED;
	}
	//调用检测函数；
	if ( ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
		|| ( ! lua_isnumber( m_pLuaState, -1 ) )   //或者返回值非数字值；
		)
	{
		D_ERROR( "执行PVC战斗判断函数失败2" );
		return FPRE_CHECK_FAILED;
	}

	g_debugPlayer = NULL;
	//取调用结果；
	return (FIGHTPRE_RST) ( (int)(lua_tonumber( m_pLuaState, -1 )) );
}

///调用脚本PVC战斗判断函数，怪物直接攻击玩家；
FIGHTPRE_RST CFightPreScript::CallScriptOnCvpDirectCheck()
{
	g_debugPlayer = m_pAttackPlayer;

	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnCvpDirectCheck" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "脚本未定义CVP战斗判断函数:OnCvpDirectCheck" );
		return FPRE_CHECK_FAILED;
	}
	//调用检测函数；
	if ( ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
		|| ( ! lua_isnumber( m_pLuaState, -1 ) )   //或者返回值非数字值；
		)
	{
		D_ERROR( "执行CVP战斗判断函数失败3" );
		return FPRE_CHECK_FAILED;
	}

	g_debugPlayer = NULL;

	//取调用结果；
	return (FIGHTPRE_RST) ( (int)(lua_tonumber( m_pLuaState, -1 )) );
}

///调用脚本PVC战斗判断函数，玩家直接攻击玩家；
FIGHTPRE_RST CFightPreScript::CallScriptOnPvpDirectCheck()
{
	g_debugPlayer = m_pAttackPlayer;

	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnPvpDirectCheck" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "脚本未定义PVP战斗判断函数:OnPvpDirectCheck" );
		return FPRE_CHECK_FAILED;
	}
	//调用检测函数；
	if ( ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
		|| ( ! lua_isnumber( m_pLuaState, -1 ) )   //或者返回值非数字值；
		)
	{
		D_ERROR( "执行PVP战斗判断函数失败4" );
		return FPRE_CHECK_FAILED;
	}

	g_debugPlayer = NULL;

	//取调用结果；
	return (FIGHTPRE_RST) ( (int)(lua_tonumber( m_pLuaState, -1 )) );
}


////释放管理的脚本；
//void  CFightPreScriptManager::Release()
//{
//	D_WARNING( "释放战斗判断脚本管理器成功\n" );
//	map<const char*, CFightPreScript*>::iterator tmpIter	= m_NameToScripts.begin();
//	for ( tmpIter=m_NameToScripts.begin(); tmpIter!=m_NameToScripts.end(); ++tmpIter )
//	{
//		delete tmpIter->second; tmpIter->second = NULL;
//	}
//	m_NameToScripts.clear();
//	m_StateToScripts.clear();
//}
//
/////寻找对应名字的脚本，用于CNpc；
//CFightPreScript* CFightPreScriptManager::FindScripts( const char* strName )
//{
//	D_WARNING( "取战斗判断脚本%s，当前管理的脚本数%d\n", strName, m_NameToScripts.size() );
//	if ( m_NameToScripts.size() > 0 )
//	{
//		D_WARNING( "当前管理的第一个战斗判断脚本为%s\n", m_NameToScripts.begin()->first );
//		D_WARNING( "取战斗判断脚本成功!\n" );
//		return m_NameToScripts.begin()->second;
//	} else {
//		return NULL;
//	}
//
//	//map<const char*, CFightPreScript*>::iterator tmpIter = m_NameToScripts.find( strName );
//	//if ( tmpIter == m_NameToScripts.end() )
//	//{
//	//	D_WARNING( "取战斗判断脚本失败!\n" );
//	//	return NULL;
//	//}
//	//D_WARNING( "取战斗判断脚本成功!\n" );
//	//return tmpIter->second;
//}
//
