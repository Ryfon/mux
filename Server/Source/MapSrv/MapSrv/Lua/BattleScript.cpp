﻿#include "BattleScript.h"
#include "ScriptExtent.h"
#include "../Player/Player.h"
#include "../MuxPlayerSkill.h"
#include "../monster/monster.h"
#include "../MuxSkillConfig.h"
#include "../SpSkillManager.h"
#include "../MuxMap/muxmapbase.h"



CBattleScript* CBattleScriptManager::m_pBattleScript;	//战斗脚本

#define LUAHASH_NUMKEY //使用整数作为键


//键为字符串
#define LUAMETATABLE_SET_BOOLEAN( pState, key, value )\
	lua_pushstring( pState, key );\
	lua_pushboolean( pState, value );\
	lua_settable( pState, -3 );\

//键为字符串
#define LUAMETATABLE_SET_NUMBER( pState, key, value )\
	lua_pushnumber( pState, value );\
	lua_setfield( pState, -2, key );\

//键为整数
#define LUATABLE_SET_BOOLEAN( pState, key, value )\
	lua_pushnumber( pState, key );\
	lua_pushboolean( pState, value );\
	lua_rawset( pState, -3);\

//键为整数
#define LUATABLE_SET_NUMBER( pState, key, value )\
	lua_pushnumber( pState, value );\
	lua_rawseti( pState, -2, key );\

namespace BATTEL_SCRIPT
{
	enum PM	//p0 p1 m0 m1用
	{
		PID						= 1,
		GID						= 2,
		Race					= 3,
		Class					= 4,
		Level					= 5,
		BaseState				= 6,
		SP						= 7,
		MP						= 8,
		HP						= 9,
		//BaseHp					= 10,
		//BaseMp					= 11,
		Str						= 12,
		Agi						= 13,
		Int						= 14,
		TimesExp				= 15,
		TimesPhyMagExp			= 16,
		BattleMode				= 17,
		UnionID					= 18,
		TeamID					= 19,
		x						= 20,
		y						= 21,
		BuffRange				= 22,
		npcType					= 23,
		InherentStand			= 24,
		//MaxHp					= 25,
		TargetSet				= 26,
		Size					= 27,
		MinPhyAttack			= 28,
		MaxPhyAttack			= 29,
		MinMagAttack			= 30,
		MaxMagAttack			= 31,
		PhyAttack				= 32,
		MagAttack				= 33,
		PhyHit					= 34,
		MagHit					= 35,
		PhyCriLevel				= 36,
		MagCriLevel				= 37,
		PhyAttackRate			= 38,
		MagAttackRate			= 39,
		AdditionPhyCriDamage	= 40,
		AdditionMagCriDamage	= 41,
		PhyDamageUpRate			= 42,
		MagDamageUpRate			= 43,
		AdditionalPhyCriRate	= 44,
		AdditionalMagCriRate	= 45,
		PhyCriDamageUpRate		= 46,
		MagCriDamageUpRate		= 47,
		PhyRiftLevel			= 48,
		MagRiftLevel			= 49,
		AdditionalPhyDamage		= 50,
		AdditionalMagDamage		= 51,
		PhyHitRate				= 52,
		MagHitRate				= 53,
		IncreaseFaint			= 54,
		Stun					= 55,
		IncreaseBondage			= 56,
		Tie						= 57,
		absorbdamage			= 58,
		PhyDefend				= 59,
		MagDefend				= 60,
		PhyDefendRate			= 61,
		MagDefendRate			= 62,
		PhyCriDerateLevel		= 63,
		MagCriDerateLevel		= 64,
		PhyCriDamageDerate		= 65,
		MagCriDamageDerate		= 66,
		PhyJouk					= 67,
		MagJouk					= 68,
		PhyDamageDerate			= 69,
		MagDamageDerate			= 70,
		PhyJoukRate				= 71,
		MagJoukRate				= 72,
		ResistFaint				= 73,
		AntiStun				= 74,
		ResistBondage			= 75,
		AntiTie					= 76,
		DrArg					= 77,
		MaxHP					= 78,
		MaxMP					= 79,
		BaseHP					= 80,
		BaseMP					= 81,
		PhyDef					= 82,
		MagDef					= 83,
		Rank					= 84,
		IsBondage				= 85,
		IsFaint					= 86,
		IsSheep					= 87,
		IsSleeping				= 88,
		IsInvicible				= 89,
		IsEquipWeapon			= 90,
		IsOlInvicible			= 91,
		IsEvil					= 92,
		IsRogue					= 93,
		IsHero					= 94,
		IsBattleArea			= 95,
		IsPkArea				= 96,
		IsRookie				= 97,
		CanBeAttacked			= 98,
		CanBeAttackedPVC		= 99,
		DamageRebound			= 100,
		IsReturn				= 101,
		MoveImmunity			= 102,
		Strength				= 103,
		Vitality				= 104,
		Agility					= 105,
		Spirit					= 106,
		Intelligence			= 107,
		Excellent				= 108,//卓越一击
		Critical				= 109,//会心一击
	};
	enum ARGS
	{
		skillID					= 1,
		IsUseItem				= 2,
		Damage					= 3,
		BattleFlag				= 4,
		BuffID					= 5,
		HealID					= 6,
	};
};

CBattleScript::CBattleScript(void)
{
}

CBattleScript::~CBattleScript(void)
{
	//init open 
	m_file.close();
}


void CBattleScript::InitComponent()		//初始化
{
	m_pAtkPlayer = NULL;		//攻击玩家
	m_pTgtPlayer = NULL;		//目标玩家
	m_pAtkMonster = NULL;		//攻击怪物
	m_pTgtMonster = NULL;		//目标怪物
	m_reactFunc = EReactionFunc_Invalid;	//无效的枚举
}

bool CBattleScript::Init( const char* luaScriptFile )
{
	//初始化一个日志
	m_file.open("BattleScript.log");
	return LoadFrom<CBattleScriptExtent>( luaScriptFile );
}


bool CBattleScript::FnCheckBattleFirstStage( CPlayer* pAtkPlayer, unsigned int skillID, unsigned int& ret )		//调用LUA脚本战斗阶段第一阶段
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_useSkillID = skillID;
	m_reactFunc = EFnCheckBattleFirstStage;
	return CallScriptOnFnCheckBattleFirstStage( ret );
}


bool CBattleScript::FnConsumeStagePvc( CPlayer* pAtkPlayer, unsigned int& ret )						//调用lua脚本的扣除消耗阶段（这里不扣除MP）
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_reactFunc = EFnConsumeStagePvc;
	return CallScriptOnFnConsumeStagePvc( ret );
}


bool CBattleScript::FnConsumeStagePvp( CPlayer* pAtkPlayer, unsigned int& ret )						//调用lua脚本的扣除消耗阶段（这里不扣除MP）
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_reactFunc = EFnConsumeStagePvp;
	return CallScriptOnFnConsumeStagePvp( ret );
}

bool CBattleScript::FnConsumeMpStagePvc( CPlayer* pAtkPlayer, unsigned int& ret )					//调用lua脚本的扣除MP阶段
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_reactFunc = EFnConsumeMpStagePvc;
	return CallScriptOnFnConsumeMpStagePvc( ret );
}

bool CBattleScript::FnConsumeMpStagePvp( CPlayer* pAtkPlayer, unsigned int& ret )					//调用lua脚本的扣除MP阶段
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_reactFunc = EFnConsumeMpStagePvp;
	return CallScriptOnFnConsumeMpStagePvp( ret );
}

bool CBattleScript::FnCheckPvpBattleSecondStage( CPlayer* pAtkPlayer, CPlayer* pTgtPlayer, unsigned int& ret )	//调用LUA脚本战斗第二阶段
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtPlayer = pTgtPlayer;
	m_reactFunc = EFnCheckPvpBattleSecondStage;
	return CallScriptOnCheckPvpBattleSecondStage( ret );
}

bool CBattleScript::FnCheckPveBattleSecondStage( CPlayer* pAtkPlayer, CMonster* pTgtMonster, unsigned int& ret )		//调用PVC第二阶段
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtMonster = pTgtMonster;
	m_reactFunc = EFnCheckPveBattleSecondStage;
	return CallScriptOnCheckPvcBattleSecondStage( ret );
}

bool CBattleScript::FnPvpDamage( CPlayer* pAtkPlayer, CPlayer* pTgtPlayer, unsigned int skillID, vector<int>* pVecRst )		//-- PVP 计算伤害,应用攻击结果
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtPlayer = pTgtPlayer;
	m_useSkillID = skillID;
	m_reactFunc = EFnPvpDamage;
	return CallScriptOnFnPvpDamage( pVecRst );
}

bool CBattleScript::FnPvcDamage( CPlayer* pAtkPlayer, CMonster* pTgtMonster, unsigned int skillID,  vector<int>* pVecRst )	//-- PVE 计算伤害,应用攻击结果
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtMonster = pTgtMonster;
	m_useSkillID = skillID;
	m_reactFunc = EFnPvcDamage;
	return CallScriptOnFnPvcDamage( pVecRst );
}

bool CBattleScript::FnCheckSkillSpilt( unsigned short arrRst[5] )			//获取动作分割的参数
{

	m_reactFunc = EFnCheckSkillSpilt;

	return CallScriptOnCheckSkillSpilt( arrRst );
}


bool CBattleScript::FnCvpDamage( CMonster* pAtkMonster, CPlayer* pTgtPlayer, unsigned int skillID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkMonster = pAtkMonster;
	m_pTgtPlayer = pTgtPlayer;
	m_useSkillID = skillID;
	m_reactFunc = EFnCvpDamage;
	return CallScriptOnFnCvpDamage( pVecRst );
}


bool CBattleScript::FnCvcDamage( CMonster* pAtkMonster, CMonster* pTgtMonster, unsigned int skillID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkMonster = pAtkMonster;
	m_pTgtMonster = pTgtMonster;
	m_useSkillID = skillID;
	m_reactFunc = EFnCvcDamage;
	return CallScriptOnFnCvcDamage( pVecRst );
}


bool CBattleScript::CallScriptOnFnCheckBattleFirstStage( unsigned int& ret )
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCheckBattleFirstStage" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCheckBattleFirstStage函数\n" );
		return false;
	}

	//推入战斗脚本p0
	lua_getglobal( m_pLuaState, "p0" );
	FillFirstStageData( m_pLuaState, m_pAtkPlayer );
	lua_setglobal( m_pLuaState, "p0" );

	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	//调用检测函数；FnHandlePlayerBattleFirstStage为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnHandlePlayerBattleFirstStage函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	ret = (unsigned int)lua_tonumber( m_pLuaState, -1 );
	return true;
}

bool CBattleScript::CallScriptOnFnConsumeStagePvc( unsigned int& ret )
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnConsumeStagePvc" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnConsumeStagePvc函数\n" );
		return false;
	}

	//调用检测函数；CallScriptOnFnConsumeStage为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnConsumeStagePvc函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	ret = (unsigned int)lua_tonumber( m_pLuaState, -1 );
	return true;
}


bool CBattleScript::CallScriptOnFnConsumeStagePvp( unsigned int& ret)		//1返回值 
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnConsumeStagePvp" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnConsumeStagePvp函数\n" );
		return false;
	}

	//调用检测函数；CallScriptOnFnConsumeStage为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnConsumeStagePvp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	ret = (unsigned int)lua_tonumber( m_pLuaState, -1 );
	return true;
}

bool CBattleScript::CallScriptOnFnConsumeMpStagePvc( unsigned int& ret )		//1返回值
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnConsumeMpStagePvc" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnConsumeMpStagePvc函数\n" );
		return false;
	}

	//调用检测函数；CallScriptOnFnConsumeMpStagePvc为0输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnConsumeMpStagePvc函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	ret = (unsigned int)lua_tonumber( m_pLuaState, -1 );
	return true;
}

bool CBattleScript::CallScriptOnFnConsumeMpStagePvp( unsigned int& ret )		//1返回值
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnConsumeMpStagePvp" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnConsumeMpStagePvp函数\n" );
		return false;
	}

	//调用检测函数；CallScriptOnFnConsumeMpStagePvp为0输入参数,1返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnConsumeMpStagePvp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	ret = (unsigned int)lua_tonumber( m_pLuaState, -1 );
	return true;
}

bool CBattleScript::CallScriptOnCheckPvpBattleSecondStage( unsigned int& ret )
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCheckPvpBattleSecondStage" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCheckPvpBattleSecondStage函数\n" );
		return false;
	}

	//推入战斗脚本p0
	lua_getglobal( m_pLuaState, "p0" );
	FillSecondStagePlayer( m_pLuaState, m_pAtkPlayer );
	lua_setglobal( m_pLuaState, "p0" );

	lua_getglobal( m_pLuaState, "p1" );
	//为什么p1会用到FirstStage？填false表示是被攻击者，防止被当作攻击者而被取消无敌状态
	FillFirstStageData( m_pLuaState, m_pTgtPlayer, false );
	FillSecondStagePlayer( m_pLuaState, m_pTgtPlayer );
	lua_setglobal( m_pLuaState, "p1" );

	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
#endif
	lua_setglobal( m_pLuaState, "args" );


	//调用检测函数；FnHandlePlayerBattleFirstStage为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCheckPvpBattleSecondStage函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	ret = (unsigned int)lua_tonumber( m_pLuaState, -1 );

	////临时加一点打印
	//if ( ret == BATTLE_MODE_ERROR )
	//{
	//	D_INFO( "出现在该战斗模式下,你无法对目标使用这个技能\n" );
	//	D_INFO( "args.skillID = %d\n", m_useSkillID );

	//	D_INFO( "p0 = %s\n", m_pAtkPlayer->GetNickName() );
	//	D_INFO( "BattleMode = %d ", m_pAtkPlayer->GetBattleMode() );
	//	D_INFO( "IsEvil = %d ", (int)m_pAtkPlayer->IsEvil() );
	//	D_INFO( "IsRogue = %d ", (int)m_pAtkPlayer->IsRogueState() );
	//	D_INFO( "IsHero = %d ", (int)( m_pAtkPlayer->GetGoodEvilPoint() > 0 ) );
	//	D_INFO( "IsBattleArea = %d ", (int)m_pAtkPlayer->GetSelfStateManager().IsCanFight() );
	//	D_INFO( "IsPkArea = %d ", (int)m_pAtkPlayer->GetSelfStateManager().IsCanPvP() );
	//	D_INFO( "UnionID = %d ", (unsigned int)(m_pAtkPlayer->Union()) );
	//	D_INFO( "TeamID = %d ",  m_pAtkPlayer->GetTeamID() );
	//	D_INFO( "x = %d ", m_pAtkPlayer->GetPosX() );
	//	D_INFO( "y = %d ", m_pAtkPlayer->GetPosY() );
	//	D_INFO( "BuffRange = %d ", m_pAtkPlayer->GetBuffAtkDistance() );
	//	D_INFO( "IsRookie = %d ", (int)!m_pAtkPlayer->IsRookieCanBeAttack() );
	//	D_INFO( "IsOlInvicible = %d ", (int)m_pAtkPlayer->CheckInvincible(false) );
	//	D_INFO( "PID = %d ", m_pAtkPlayer->GetPID() );
	//	D_INFO( "Race = %d \n", m_pAtkPlayer->GetRace() );

	//	D_INFO( "p1 = %s\n", m_pTgtPlayer->GetNickName() );
	//	D_INFO( "BattleMode = %d ", m_pTgtPlayer->GetBattleMode() );
	//	D_INFO( "IsEvil = %d ", (int)m_pTgtPlayer->IsEvil() );
	//	D_INFO( "IsRogue = %d ", (int)m_pTgtPlayer->IsRogueState() );
	//	D_INFO( "IsHero = %d ", (int)( m_pTgtPlayer->GetGoodEvilPoint() > 0 ) );
	//	D_INFO( "IsBattleArea = %d ", (int)m_pTgtPlayer->GetSelfStateManager().IsCanFight() );
	//	D_INFO( "IsPkArea = %d ", (int)m_pTgtPlayer->GetSelfStateManager().IsCanPvP() );
	//	D_INFO( "UnionID = %d ", (unsigned int)(m_pTgtPlayer->Union()) );
	//	D_INFO( "TeamID = %d ",  m_pTgtPlayer->GetTeamID() );
	//	D_INFO( "x = %d ", m_pTgtPlayer->GetPosX() );
	//	D_INFO( "y = %d ", m_pTgtPlayer->GetPosY() );
	//	D_INFO( "BuffRange = %d ", m_pTgtPlayer->GetBuffAtkDistance() );
	//	D_INFO( "IsRookie = %d ", (int)!m_pTgtPlayer->IsRookieCanBeAttack() );
	//	D_INFO( "IsOlInvicible = %d ", (int)m_pTgtPlayer->CheckInvincible(false) );
	//	D_INFO( "PID = %d ", m_pTgtPlayer->GetPID() );
	//	D_INFO( "Race = %d ", m_pTgtPlayer->GetRace() );
	//	D_INFO( "BaseState = %d ", m_pTgtPlayer->GetBaseState() );
	//	D_INFO( "IsRookie = %d \n", (int)m_pTgtPlayer->IsRookieState() );
	//}
	return true;
}


bool CBattleScript::CallScriptOnCheckPvcBattleSecondStage( unsigned int& ret )
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnCheckPveBattleSecondStage" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCheckPveBattleSecondStage函数\n" );
		return false;
	}

	//推入战斗脚本p0
	lua_getglobal( m_pLuaState, "p0" );
	FillSecondStagePlayer( m_pLuaState, m_pAtkPlayer );
	lua_setglobal( m_pLuaState, "p0" );

	//m1为被攻击怪物
	FillSecondStageMonster( m_pLuaState );
	

	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	//调用检测函数；FnCheckPveBattleSecondStage为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCheckPveBattleSecondStage函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	ret = (unsigned int)lua_tonumber( m_pLuaState, -1 );
	return true;
}


bool CBattleScript::CallScriptOnFnPvpDamage( vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );

	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnPvpDamage pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnPvpDamage" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnPvpDamage函数\n" );
		return false;
	}

	//推入战斗脚本p0
	lua_getglobal( m_pLuaState, "p0" );
	FillBattleStagep0( m_pLuaState );
	lua_setglobal( m_pLuaState, "p0" );

	//m1为被攻击怪物
	lua_getglobal( m_pLuaState, "p1" );
	FillBattleStagep1( m_pLuaState );
	lua_setglobal( m_pLuaState, "p1" );

	//调用检测函数；CallScriptOnHandlePveDamage为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnPvpDamage函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//3返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnPvcDamage( vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnPvcDamage pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnPvcDamage" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnPvcDamage函数\n" );
		return false;
	}

	//推入战斗脚本p0
	lua_getglobal( m_pLuaState, "p0" );
	FillBattleStagep0( m_pLuaState );
	lua_setglobal( m_pLuaState, "p0" );

	//m1为被攻击怪物
	lua_getglobal( m_pLuaState, "m1" );
	FillBattleStagem1( m_pLuaState );
	lua_setglobal( m_pLuaState, "m1" );

	//调用检测函数；CallScriptOnHandlePveDamage为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnPvcDamage函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//3返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnCvpDamage( vector<int>* pVecRst )		//3 返回值
{
	CLuaRestoreStack rs( m_pLuaState );
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnCvpDamage pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();


	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnCvpDamage" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCvpDamage函数\n" );
		return false;
	}

	//推入战斗脚本p1
	lua_getglobal( m_pLuaState, "p1" );
	FillBattleStagep1( m_pLuaState );
	lua_setglobal( m_pLuaState, "p1" );

	lua_getglobal( m_pLuaState, "m0" );
	FillBattleStagem0( m_pLuaState );
	lua_setglobal( m_pLuaState, "m0" );

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	//调用检测函数；CallScriptOnFnCvpDamage为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnHandleDamageCvp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//2返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnCvcDamage( vector<int>* pVecRst )		//3 返回值
{
	CLuaRestoreStack rs( m_pLuaState );
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnCvcDamage pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnCvcDamage" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCvcDamage函数\n" );
		return false;
	}

	//推入战斗脚本p1
	lua_getglobal( m_pLuaState, "m1" );
	FillBattleStagem1( m_pLuaState );
	lua_setglobal( m_pLuaState, "m1" );

	lua_getglobal( m_pLuaState, "m0" );
	FillBattleStagem0( m_pLuaState );
	lua_setglobal( m_pLuaState, "m0" );

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	//调用检测函数；FnHandleDamageCvc为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCvcDamage函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//2返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnCheckSkillSpilt( unsigned short arrRst[5] )		//检测动作分割
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "FnCheckSkillSpilt" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCheckSkillSpilt函数\n" );
		return false;
	}


	//调用检测函数；FnCheckSkillSpilt为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 5, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCheckSkillSpilt函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	arrRst[0] = (int)(lua_tonumber(m_pLuaState, -5));
	arrRst[1] = (int)(lua_tonumber(m_pLuaState, -4));
	arrRst[2] = (int)(lua_tonumber(m_pLuaState, -3));
	arrRst[3] = (int)(lua_tonumber(m_pLuaState, -2));
	arrRst[4] = (int)(lua_tonumber(m_pLuaState, -1));

	return true;
}


bool CBattleScript::FillFirstStageData( lua_State* pState, CPlayer* pAtkPlayer, bool isAttack )
{
	if( !pAtkPlayer )
		return false;

	AttributeEffect* pBuffEffect = pAtkPlayer->GetBuffEffect();
	if( NULL == pAtkPlayer )
		return false;

#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PID, pAtkPlayer->GetPID() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::GID, pAtkPlayer->GetGID() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Race, pAtkPlayer->GetRace() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Class, pAtkPlayer->GetClass() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Level, pAtkPlayer->GetLevel() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::BaseState , pAtkPlayer->GetBaseState() );
	int powerperBean = CSpSkillManager::GetPowerPerBean();
	if (powerperBean <= 0)
	{
		D_ERROR( "FillFirstStageData, error powerperBean %d\n", powerperBean );
		powerperBean = 1;
	}
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::SP, pAtkPlayer->GetSpPower()/powerperBean );		//SP豆数
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MP, pAtkPlayer->GetCurrentMP() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::BaseHP, pAtkPlayer->GetBaseMaxHp() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::BaseMP, pAtkPlayer->GetBaseMaxMp() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsBondage, pAtkPlayer->IsBondage() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsFaint, pAtkPlayer->IsFaint() );	
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Str, pBuffEffect->nStr );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Agi, pBuffEffect->nAgi );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Int, pBuffEffect->nInt );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsSheep,  pAtkPlayer->IsSheep() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsSleeping, pAtkPlayer->IsInSleep() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::TimesPhyMagExp, pAtkPlayer->GetTimesPhyMagExp() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsInvicible, pAtkPlayer->IsInvicibleState() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsEquipWeapon, pAtkPlayer->IsEquipWeapon() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsOlInvicible, pAtkPlayer->CheckInvincible(isAttack) );
#else
	LUAMETATABLE_SET_NUMBER( pState, "PID", pAtkPlayer->GetPID() );
	LUAMETATABLE_SET_NUMBER( pState, "GID", pAtkPlayer->GetGID() );
	LUAMETATABLE_SET_NUMBER( pState, "Race", pAtkPlayer->GetRace() );
	LUAMETATABLE_SET_NUMBER( pState, "Class", pAtkPlayer->GetClass() );
	LUAMETATABLE_SET_NUMBER( pState, "Level", pAtkPlayer->GetLevel() );
	LUAMETATABLE_SET_NUMBER( pState, "BaseState" , pAtkPlayer->GetBaseState() );
	LUAMETATABLE_SET_NUMBER( pState, "SP", pAtkPlayer->GetSpPower()/CSpSkillManager::GetPowerPerBean() );		//SP豆数
	LUAMETATABLE_SET_NUMBER( pState, "MP", pAtkPlayer->GetCurrentMP() );
	LUAMETATABLE_SET_NUMBER( pState, "BaseHP", pAtkPlayer->GetBaseMaxHp() );
	LUAMETATABLE_SET_NUMBER( pState, "BaseMP", pAtkPlayer->GetBaseMaxMp() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsBondage", pAtkPlayer->IsBondage() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsFaint", pAtkPlayer->IsFaint() );	
	LUAMETATABLE_SET_NUMBER( pState, "Str", pBuffEffect->nStr );
	LUAMETATABLE_SET_NUMBER( pState, "Agi", pBuffEffect->nAgi );
	LUAMETATABLE_SET_NUMBER( pState, "Int", pBuffEffect->nInt );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsSheep",  pAtkPlayer->IsSheep() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsSleeping", pAtkPlayer->IsInSleep() );
	//LUAMETATABLE_SET_NUMBER( pState, "TimesExp", pAtkPlayer->GetTimesExp() );
	LUAMETATABLE_SET_NUMBER( pState, "TimesPhyMagExp", pAtkPlayer->GetTimesPhyMagExp() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsInvicible", pAtkPlayer->IsInvicibleState() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsEquipWeapon", pAtkPlayer->IsEquipWeapon() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsOlInvicible", pAtkPlayer->CheckInvincible(isAttack) );
#endif
	
	return true;

}


void CBattleScript::SetField( lua_State* pState, const char* index, int value )   //设置table的key和value
{
	lua_pushnumber( pState, (double)value );
	lua_setfield( pState, -2, index );
}

void CBattleScript::SetField(lua_State* pState, int key, int value )
{
	lua_pushnumber( pState, (double)value );
	lua_rawseti( pState, -2, key );
}


bool CBattleScript::ExSendAtkSysChat( const char* pContent )
{
	if( !m_pAtkPlayer )
		return false;

	m_pAtkPlayer->SendSystemChat( pContent );
	return true;
}



bool CBattleScript::ExSendTgtSysChat(const char* content)
{
	if( !m_pTgtPlayer )
		return false;

	m_pTgtPlayer->SendSystemChat( content );
	return true;
}


bool CBattleScript::ExRandRate( float rate )
{
	return RandRate( rate );
}


unsigned int CBattleScript::ExPlayerDistance( int x1, int y1, int x2, int y2 )
{
	unsigned int dx = abs( x1 - x2 );
	unsigned int dy = abs( y1 - y2);
	return dx*dx+dy*dy;
}


bool CBattleScript::ExAddPlayerExp( unsigned int type, unsigned int num )
{
	D_ERROR( "调用已废弃功能接口ExAddPlayerExp" );
	return true;
	//10.09.20已废弃//if( NULL == m_pAtkPlayer )
	//	return false;

	//if( type == 0 )
	//{
	//	m_pAtkPlayer->AddPhyExp( num );
	//}else if( type == 1 )
	//{
	//	m_pAtkPlayer->AddMagExp( num );
	//}else if( type == 2 )
	//{
	//	m_pAtkPlayer->AddSpPower( num );
	//}else if( type == 3 )
	//{
	//	m_pAtkPlayer->AddSpExp( num );
	//}
	//return true;
}


bool CBattleScript::ExIsSkillInCD( unsigned int cdType, const ACE_Time_Value& cdTime )								//-- 检查技能是否在 CD 
{
	if( !m_pAtkPlayer )
		return false;

	return m_pAtkPlayer->CheckCd( cdType, cdTime ); //???成功了就置有问题
}


bool CBattleScript::ExPlayerHaveItem( unsigned int itemID, unsigned int num)				//-- 玩家包裹中是否有物品
{
	if( !m_pAtkPlayer )
		return false;

	return  m_pAtkPlayer->IsPkgHave( itemID );
}



bool CBattleScript::ExPlayerConsume(unsigned int mp, unsigned int sp, unsigned int itemID, unsigned int num)  //-- 玩家消耗
{
	ACE_UNUSED_ARG( itemID );
	ACE_UNUSED_ARG( num );

	if( !m_pAtkPlayer )
		return false;

	//由于每次必扣蓝，因此战斗脚本中不再扣蓝，by dzj, 10.05.24, m_pAtkPlayer->SubMP( mp );
	m_pAtkPlayer->SubSpPower( sp * CSpSkillManager::GetPowerPerBean() );
	//减道具

	return true;
}


bool CBattleScript::ExAddSkillExp( unsigned int skillID, unsigned int numExp)//-- 加技能经验
{
	if( !m_pAtkPlayer )
		return false;

	CMuxPlayerSkill* pSkill = m_pAtkPlayer->FindMuxSkill( skillID );
	if( NULL == pSkill )
		return false;

	pSkill->AddSpExp( numExp );
	return true;
}




bool CBattleScript::ExEndTgtPlayerSheepBuff()	 //结束目标玩家的变羊状态 
{
	if( NULL == m_pTgtPlayer )
	{
		return false;
	}
		
	return m_pTgtPlayer->EndSheepDebuff();
}


bool CBattleScript::ExLog( const char* pContent )
{
	m_file << pContent <<endl;
	return true;
}


bool CBattleScript::FillSecondStagePlayer( lua_State* pState, CPlayer* pPlayer )
{
	if( !pPlayer )
		return false;

#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::BattleMode, pPlayer->GetBattleMode() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsEvil, pPlayer->IsEvil() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsRogue, pPlayer->IsRogueState() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsHero, ( pPlayer->GetGoodEvilPoint() > 0 ) );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsBattleArea, pPlayer->GetSelfStateManager().IsCanFight() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsPkArea, pPlayer->GetSelfStateManager().IsCanPvP() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::UnionID, (unsigned int)(pPlayer->Union()) );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::TeamID,  pPlayer->GetTeamID() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::x, pPlayer->GetPosX() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::y, pPlayer->GetPosY() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::BuffRange, pPlayer->GetBuffAtkDistance() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsRookie, !pPlayer->IsRookieCanBeAttack() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsOlInvicible, pPlayer->CheckInvincible(false) );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PID, pPlayer->GetPID() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::GID, pPlayer->GetGID() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Race, pPlayer->GetRace() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Strength, pPlayer->GetStrength() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Vitality, pPlayer->GetVitality() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Agility, pPlayer->GetAgility() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Spirit, pPlayer->GetSpirit() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Intelligence, pPlayer->GetIntelligence() );
	
#else
	LUAMETATABLE_SET_NUMBER( pState, "BattleMode", pPlayer->GetBattleMode() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsEvil", pPlayer->IsEvil() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsRogue", pPlayer->IsRogueState() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsHero", ( pPlayer->GetGoodEvilPoint() > 0 ) );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsBattleArea", pPlayer->GetSelfStateManager().IsCanFight() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsPkArea", pPlayer->GetSelfStateManager().IsCanPvP() );
	LUAMETATABLE_SET_NUMBER( pState, "UnionID", (unsigned int)(pPlayer->Union()) );
	LUAMETATABLE_SET_NUMBER( pState, "TeamID",  pPlayer->GetTeamID() );
	LUAMETATABLE_SET_NUMBER( pState, "x", pPlayer->GetPosX() );
	LUAMETATABLE_SET_NUMBER( pState, "y", pPlayer->GetPosY() );
	LUAMETATABLE_SET_NUMBER( pState, "BuffRange", pPlayer->GetBuffAtkDistance() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsRookie", !pPlayer->IsRookieCanBeAttack() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsOlInvicible", pPlayer->CheckInvincible(false) );
	LUAMETATABLE_SET_NUMBER( pState, "PID", pPlayer->GetPID() );
	LUAMETATABLE_SET_NUMBER( pState, "GID", pPlayer->GetGID() );
	LUAMETATABLE_SET_NUMBER( pState, "Race", pPlayer->GetRace() );
#endif
	return true;
}


bool CBattleScript::FillSecondStageMonster( lua_State* pState )			//设置第2.1阶段怪物
{
	if( !m_pTgtMonster )
		return false;

	lua_getglobal( pState, "m1" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::npcType, m_pTgtMonster->GetNpcType() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Race,	m_pTgtMonster->GetRaceAttribute() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::InherentStand, m_pTgtMonster->GetNpcType() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::HP, m_pTgtMonster->GetHp() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::CanBeAttacked, m_pTgtMonster->IsCanBeAttacked() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::CanBeAttackedPVC, m_pTgtMonster->IsCanBeAttackedPvc() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::x,  m_pTgtMonster->GetPosX() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::y,  m_pTgtMonster->GetPosY() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxHP, m_pTgtMonster->GetMaxHp() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Level, m_pTgtMonster->GetLevel() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::TargetSet, m_pTgtMonster->GetTargetSet() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Size, m_pTgtMonster->GetMonsterSize() );
#else
	LUAMETATABLE_SET_NUMBER( pState, "npcType", m_pTgtMonster->GetNpcType() );
	LUAMETATABLE_SET_NUMBER( pState, "Race",	m_pTgtMonster->GetRaceAttribute() );
	LUAMETATABLE_SET_NUMBER( pState, "InherentStand", m_pTgtMonster->GetNpcType() );
	LUAMETATABLE_SET_NUMBER( pState, "HP", m_pTgtMonster->GetHp() );
	LUAMETATABLE_SET_BOOLEAN( pState, "CanBeAttacked", m_pTgtMonster->IsCanBeAttacked() );
	LUAMETATABLE_SET_BOOLEAN( pState, "CanBeAttackedPVC", m_pTgtMonster->IsCanBeAttackedPvc() );
	LUAMETATABLE_SET_NUMBER( pState, "x",  m_pTgtMonster->GetPosX() );
	LUAMETATABLE_SET_NUMBER( pState, "y",  m_pTgtMonster->GetPosY() );
	LUAMETATABLE_SET_NUMBER( pState, "MaxHP", m_pTgtMonster->GetMaxHp() );
	LUAMETATABLE_SET_NUMBER( pState, "Level", m_pTgtMonster->GetLevel() );
	LUAMETATABLE_SET_NUMBER( pState, "TargetSet", m_pTgtMonster->GetTargetSet() );
	LUAMETATABLE_SET_NUMBER( pState, "Size", m_pTgtMonster->GetMonsterSize() );
#endif
	lua_setglobal( pState, "m1" );

	return true;
}


bool CBattleScript::FillBattleStagep0( lua_State* pState )
{
	if( !m_pAtkPlayer )
		return false;

	ItemAddtionProp& itemProp = m_pAtkPlayer->GetItemAddtionProp();
	AttributeEffect* pBuffAffect = m_pAtkPlayer->GetBuffEffect();
	if( NULL == pBuffAffect )
		return false;
	
#ifdef LUAHASH_NUMKEY
	unsigned int value = itemProp.AttackPhyMin;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MinPhyAttack, value );

	value = itemProp.AttackPhyMax;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxPhyAttack, value );

	value = itemProp.AttackMagMin;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MinMagAttack, value );

	value = itemProp.AttackMagMax;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxMagAttack, value );

	value = m_pAtkPlayer->GetPhyAttack();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyAttack, value );

	value = m_pAtkPlayer->GetMagAttack();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagAttack, value );

	value = m_pAtkPlayer->GetPhyHit();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyHit, value );

	value = m_pAtkPlayer->GetMagHit();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagHit, value );

	value = m_pAtkPlayer->GetPhyCriLevel();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyCriLevel, value );

	value = m_pAtkPlayer->GetMagCriLevel();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagCriLevel, value );

	value = itemProp.PhysicsAttackPercent;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyAttackRate, value );

	value = itemProp.MagicAttackPercent;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagAttackRate, value );

	value = m_pAtkPlayer->GetPhyCriDamage();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AdditionPhyCriDamage, value );

	value = m_pAtkPlayer->GetMagCriDamage();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AdditionMagCriDamage, value );

	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyDamageUpRate, pBuffAffect->nAddPhyDamageRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagDamageUpRate, pBuffAffect->nAddMagDamageRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AdditionalPhyCriRate, pBuffAffect->fAddPhyCriRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AdditionalMagCriRate, pBuffAffect->fAddMagCriRate );

	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyCriDamageUpRate, pBuffAffect->fAddPhyCriDmgRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagCriDamageUpRate, pBuffAffect->fAddMagCriDmgRate );

	value = itemProp.PhysicsRift;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyRiftLevel, value );

	value = itemProp.MagicRift;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagRiftLevel, value );

	value = itemProp.PhysicsAttackAdd;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AdditionalPhyDamage, value );

	value = itemProp.MagicAttackAdd;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AdditionalMagDamage, value );

	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyHitRate, pBuffAffect->fAddPhyHitRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagHitRate, pBuffAffect->fAddMagHitRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::IncreaseFaint, pBuffAffect->fAddFaintRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Stun, itemProp.Stun );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::IncreaseBondage, pBuffAffect->fAddBondageRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Tie, itemProp.Tie );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::absorbdamage, m_pAtkPlayer->GetAbsorbDamage() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Int, m_pAtkPlayer->GetBuffInt() );

	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Excellent, m_pAtkPlayer->GetExcellent() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Critical, m_pAtkPlayer->GetCritical() );
#else
	unsigned int value = itemProp.AttackPhyMin;
	LUAMETATABLE_SET_NUMBER( pState, "MinPhyAttack", value );

	value = itemProp.AttackPhyMax;
	LUAMETATABLE_SET_NUMBER( pState, "MaxPhyAttack", value );

	value = itemProp.AttackMagMin;
	LUAMETATABLE_SET_NUMBER( pState, "MinMagAttack", value );

	value = itemProp.AttackMagMax;
	LUAMETATABLE_SET_NUMBER( pState, "MaxMagAttack", value );

	value = m_pAtkPlayer->GetPhyAttack();
	LUAMETATABLE_SET_NUMBER( pState, "PhyAttack", value );

	value = m_pAtkPlayer->GetMagAttack();
	LUAMETATABLE_SET_NUMBER( pState, "MagAttack", value );

	value = m_pAtkPlayer->GetPhyHit();
	LUAMETATABLE_SET_NUMBER( pState, "PhyHit", value );

	value = m_pAtkPlayer->GetMagHit();
	LUAMETATABLE_SET_NUMBER( pState, "MagHit", value );

	value = m_pAtkPlayer->GetPhyCriLevel();
	LUAMETATABLE_SET_NUMBER( pState, "PhyCriLevel", value );

	value = m_pAtkPlayer->GetMagCriLevel();
	LUAMETATABLE_SET_NUMBER( pState, "MagCriLevel", value );

	value = itemProp.PhysicsAttackPercent;
	LUAMETATABLE_SET_NUMBER( pState, "PhyAttackRate", value );

	value = itemProp.MagicAttackPercent;
	LUAMETATABLE_SET_NUMBER( pState, "MagAttackRate", value );

	value = m_pAtkPlayer->GetPhyCriDamage();
	LUAMETATABLE_SET_NUMBER( pState, "AdditionPhyCriDamage", value );

	value = m_pAtkPlayer->GetMagCriDamage();
	LUAMETATABLE_SET_NUMBER( pState, "AdditionMagCriDamage", value );

	LUAMETATABLE_SET_NUMBER( pState, "PhyDamageUpRate", pBuffAffect->nAddPhyDamageRate );
	LUAMETATABLE_SET_NUMBER( pState, "MagDamageUpRate", pBuffAffect->nAddMagDamageRate );
	LUAMETATABLE_SET_NUMBER( pState, "AdditionalPhyCriRate", pBuffAffect->fAddPhyCriRate );
	LUAMETATABLE_SET_NUMBER( pState, "AdditionalMagCriRate", pBuffAffect->fAddMagCriRate );

	LUAMETATABLE_SET_NUMBER( pState, "PhyCriDamageUpRate", pBuffAffect->fAddPhyCriDmgRate );
	LUAMETATABLE_SET_NUMBER( pState, "MagCriDamageUpRate", pBuffAffect->fAddMagCriDmgRate );

	value = itemProp.PhysicsRift;
	LUAMETATABLE_SET_NUMBER( pState, "PhyRiftLevel", value );

	value = itemProp.MagicRift;
	LUAMETATABLE_SET_NUMBER( pState, "MagRiftLevel", value );

	value = itemProp.PhysicsAttackAdd;
	LUAMETATABLE_SET_NUMBER( pState, "AdditionalPhyDamage", value );

	value = itemProp.MagicAttackAdd;
	LUAMETATABLE_SET_NUMBER( pState, "AdditionalMagDamage", value );

	LUAMETATABLE_SET_NUMBER( pState, "PhyHitRate", pBuffAffect->fAddPhyHitRate );
	LUAMETATABLE_SET_NUMBER( pState, "MagHitRate", pBuffAffect->fAddMagHitRate );
	LUAMETATABLE_SET_NUMBER( pState, "IncreaseFaint", pBuffAffect->fAddFaintRate );
	LUAMETATABLE_SET_NUMBER( pState, "Stun", itemProp.Stun );
	LUAMETATABLE_SET_NUMBER( pState, "IncreaseBondage", pBuffAffect->fAddBondageRate );
	LUAMETATABLE_SET_NUMBER( pState, "Tie", itemProp.Tie );
	LUAMETATABLE_SET_NUMBER( pState, "absorbdamage", m_pAtkPlayer->GetAbsorbDamage() );
	LUAMETATABLE_SET_NUMBER( pState, "Int", m_pAtkPlayer->GetBuffInt() );
#endif

	return true;
}


bool CBattleScript::FillBattleStagep1( lua_State* pState )
{
	if( !m_pTgtPlayer )
		return false;

	ItemAddtionProp& itemProp = m_pTgtPlayer->GetItemAddtionProp();
	AttributeEffect* pBuffAffect = m_pTgtPlayer->GetBuffEffect();
	if( NULL == pBuffAffect )
		return false;

#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PID, m_pTgtPlayer->GetPID() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::GID, m_pTgtPlayer->GetGID() );
	unsigned int value = m_pTgtPlayer->GetPhyDefend();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyDefend, value );

	value = m_pTgtPlayer->GetMagDefend();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagDefend, value );

	value = itemProp.PhysicsDefendPercent;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyDefendRate, value );

	value = itemProp.MagicDefendPercent;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagDefendRate, value );

	value = itemProp.PhysicsCriticalDerate;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyCriDerateLevel, value );

	value = itemProp.MagicCriticalDerate;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagCriDerateLevel, value );

	value = itemProp.PhysicsCriticalDamageDerate;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyCriDamageDerate, value );

	value = itemProp.MagicCriticalDamageDerate;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagCriDamageDerate, value );

	//value = itemProp.PhysicsJouk;
	value = m_pTgtPlayer->GetPhyJouk();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyJouk, value );

	//value = itemProp.MagicJouk;
	value = m_pTgtPlayer->GetMagJouk();
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagJouk, value );

	value = itemProp.PhysicsDeratePercent;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyDamageDerate, value );

	value = itemProp.MagicDeratePercent;
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagDamageDerate, value );

	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyJoukRate, pBuffAffect->fPhyJoukRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagJoukRate, pBuffAffect->fMagJoukRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::ResistFaint, pBuffAffect->fResistFaintRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AntiStun, itemProp.Antistun );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::ResistBondage, pBuffAffect->fResistBondageRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AntiTie, itemProp.Antitie );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::absorbdamage, m_pTgtPlayer->GetAbsorbDamage() );
	LUATABLE_SET_NUMBER( pState,  BATTEL_SCRIPT::Str,  pBuffAffect->nStr );
	LUATABLE_SET_NUMBER( pState,  BATTEL_SCRIPT::Int,  pBuffAffect->nInt );
	LUATABLE_SET_NUMBER( pState,  BATTEL_SCRIPT::Agi,  pBuffAffect->nAgi );
	LUATABLE_SET_NUMBER( pState,  BATTEL_SCRIPT::GID,  m_pTgtPlayer->GetGID() );
	LUATABLE_SET_NUMBER( pState,  BATTEL_SCRIPT::Class, m_pTgtPlayer->GetClass() );
	LUATABLE_SET_NUMBER( pState,  BATTEL_SCRIPT::Level, m_pTgtPlayer->GetLevel() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::DamageRebound, m_pTgtPlayer->IsDamageRebound() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsSheep, m_pTgtPlayer->IsSheep() );
	LUATABLE_SET_NUMBER( pState,  BATTEL_SCRIPT::DrArg, m_pTgtPlayer->GetDrArg() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::MoveImmunity, m_pTgtPlayer->IsMoveImmunityState() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsInvicible, m_pTgtPlayer->IsInvicibleState() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsOlInvicible, m_pTgtPlayer->CheckInvincible(false) );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxHP, m_pTgtPlayer->GetMaxHP() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxMP, m_pTgtPlayer->GetMaxMP() );
#else
	LUAMETATABLE_SET_NUMBER( pState, "PID", m_pTgtPlayer->GetPID() );
	LUAMETATABLE_SET_NUMBER( pState, "GID", m_pTgtPlayer->GetGID() );
	unsigned int value = m_pTgtPlayer->GetPhyDefend();
	LUAMETATABLE_SET_NUMBER( pState, "PhyDefend", value );

	value = m_pTgtPlayer->GetMagDefend();
	LUAMETATABLE_SET_NUMBER( pState, "MagDefend", value );
	
	value = itemProp.PhysicsDefendPercent;
	LUAMETATABLE_SET_NUMBER( pState, "PhyDefendRate", value );

	value = itemProp.MagicDefendPercent;
	LUAMETATABLE_SET_NUMBER( pState, "MagDefendRate", value );

	value = itemProp.PhysicsCriticalDerate;
	LUAMETATABLE_SET_NUMBER( pState, "PhyCriDerateLevel", value );

	value = itemProp.MagicCriticalDerate;
	LUAMETATABLE_SET_NUMBER( pState, "MagCriDerateLevel", value );

	value = itemProp.PhysicsCriticalDamageDerate;
	LUAMETATABLE_SET_NUMBER( pState, "PhyCriDamageDerate", value );

	value = itemProp.MagicCriticalDamageDerate;
	LUAMETATABLE_SET_NUMBER( pState, "MagCriDamageDerate", value );

	value = itemProp.PhysicsJouk;
	LUAMETATABLE_SET_NUMBER( pState, "PhyJouk", value );

	value = itemProp.MagicJouk;
	LUAMETATABLE_SET_NUMBER( pState, "MagJouk", value );

	value = itemProp.PhysicsDeratePercent;
	LUAMETATABLE_SET_NUMBER( pState, "PhyDamageDerate", value );

	value = itemProp.MagicDeratePercent;
	LUAMETATABLE_SET_NUMBER( pState, "MagDamageDerate", value );

	LUAMETATABLE_SET_NUMBER( pState, "PhyJoukRate", pBuffAffect->fPhyJoukRate );
	LUAMETATABLE_SET_NUMBER( pState, "MagJoukRate", pBuffAffect->fMagJoukRate );
	LUAMETATABLE_SET_NUMBER( pState, "ResistFaint", pBuffAffect->fResistFaintRate );
	LUAMETATABLE_SET_NUMBER( pState, "AntiStun", itemProp.Antistun );
	LUAMETATABLE_SET_NUMBER( pState, "ResistBondage", pBuffAffect->fResistBondageRate );
	LUAMETATABLE_SET_NUMBER( pState, "AntiTie", itemProp.Antitie );
	LUAMETATABLE_SET_NUMBER( pState, "absorbdamage", m_pTgtPlayer->GetAbsorbDamage() );
	LUAMETATABLE_SET_NUMBER( pState,  "Str",  pBuffAffect->nStr );
	LUAMETATABLE_SET_NUMBER( pState,  "Int",  pBuffAffect->nInt );
	LUAMETATABLE_SET_NUMBER( pState,  "Agi",  pBuffAffect->nAgi );
	LUAMETATABLE_SET_NUMBER( pState,  "GID",  m_pTgtPlayer->GetGID() );
	LUAMETATABLE_SET_NUMBER( pState,  "Class", m_pTgtPlayer->GetClass() );
	LUAMETATABLE_SET_NUMBER( pState,  "Level", m_pTgtPlayer->GetLevel() );
	LUAMETATABLE_SET_BOOLEAN( pState, "DamageRebound", m_pTgtPlayer->IsDamageRebound() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsSheep", m_pTgtPlayer->IsSheep() );
	LUAMETATABLE_SET_NUMBER( pState,  "DrArg", m_pTgtPlayer->GetDrArg() );
	LUAMETATABLE_SET_BOOLEAN( pState, "MoveImmunity", m_pTgtPlayer->IsMoveImmunityState() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsInvicible", m_pTgtPlayer->IsInvicibleState() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsOlInvicible", m_pTgtPlayer->CheckInvincible(false) );
	LUAMETATABLE_SET_NUMBER( pState, "MaxHP", m_pTgtPlayer->GetMaxHP() );
	LUAMETATABLE_SET_NUMBER( pState, "MaxMP", m_pTgtPlayer->GetMaxMP() );
#endif
	return true;
}


bool CBattleScript::FillBattleStagem1( lua_State* pState )
{
	if( !m_pTgtMonster )
		return false;

#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PID, m_pTgtMonster->GetMonsterID() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::GID, MONSTER_GID );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyDef, m_pTgtMonster->GetPhyDef() );

	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagDef, m_pTgtMonster->GetMagDef() );

	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyJouk, m_pTgtMonster->GetPhyDodge() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagJouk, m_pTgtMonster->GetMagDodge() );

	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Rank, m_pTgtMonster->GetRank() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::ResistBondage, m_pTgtMonster->GetResistBondage() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::ResistFaint, m_pTgtMonster->GetNpcResistFaint() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::DamageRebound, m_pTgtMonster->IsDamageRebound() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::DrArg, m_pTgtMonster->GetDrArg() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsReturn, !m_pTgtMonster->GetAttackFlag() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Level, m_pTgtMonster->GetLevel() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxHP, m_pTgtMonster->GetMaxHp() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyJoukRate, m_pTgtMonster->GetPhyJoukRate() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagJoukRate, m_pTgtMonster->GetMagJoukRate() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsSheep,  m_pTgtMonster->IsSheep() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsSleeping, m_pTgtMonster->IsInSleep() );
#else
	LUAMETATABLE_SET_NUMBER( pState, "PID", m_pTgtMonster->GetMonsterID() );
	LUAMETATABLE_SET_NUMBER( pState, "GID", MONSTER_GID );
	LUAMETATABLE_SET_NUMBER( pState, "PhyDef", m_pTgtMonster->GetPhyDef() );

	LUAMETATABLE_SET_NUMBER( pState, "MagDef", m_pTgtMonster->GetMagDef() );

	LUAMETATABLE_SET_NUMBER( pState, "PhyJouk", m_pTgtMonster->GetPhyDodge() );
	LUAMETATABLE_SET_NUMBER( pState, "MagJouk", m_pTgtMonster->GetMagDodge() );

	LUAMETATABLE_SET_NUMBER( pState, "Rank", m_pTgtMonster->GetRank() );
	LUAMETATABLE_SET_NUMBER( pState, "ResistBondage", m_pTgtMonster->GetResistBondage() );
	LUAMETATABLE_SET_NUMBER( pState, "ResistFaint", m_pTgtMonster->GetNpcResistFaint() );
	LUAMETATABLE_SET_BOOLEAN( pState, "DamageRebound", m_pTgtMonster->IsDamageRebound() );
	LUAMETATABLE_SET_NUMBER( pState, "DrArg", m_pTgtMonster->GetDrArg() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsReturn", !m_pTgtMonster->GetAttackFlag() );
	LUAMETATABLE_SET_NUMBER( pState, "Level", m_pTgtMonster->GetLevel() );
	LUAMETATABLE_SET_NUMBER( pState, "MaxHP", m_pTgtMonster->GetMaxHp() );
	LUAMETATABLE_SET_NUMBER( pState, "PhyJoukRate", m_pTgtMonster->GetPhyJoukRate() );
	LUAMETATABLE_SET_NUMBER( pState, "MagJoukRate", m_pTgtMonster->GetMagJoukRate() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsSheep",  m_pTgtMonster->IsSheep() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsSleeping", m_pTgtMonster->IsInSleep() );
#endif

	return true;
}


bool CBattleScript::FillBattleStagem0( lua_State* pState )
{
	if( !m_pAtkMonster )
	{
		D_ERROR("FillBattleStatem0 m_pAtkMonster = NULL");
		return false;
	}

#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PID, m_pAtkMonster->GetMonsterID() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::GID, MONSTER_GID );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Level, m_pAtkMonster->GetLevel() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Rank,  m_pAtkMonster->GetRank() );

	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AdditionPhyCriDamage, m_pAtkMonster->GetBuffPhyCriDamAdd() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AdditionMagCriDamage, m_pAtkMonster->GetBuffMagCriDamAdd() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyDamageUpRate, m_pAtkMonster->GetAddPhyDamageRate() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagDamageUpRate, m_pAtkMonster->GetAddMagDamageRate() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::PhyCriDamageUpRate, m_pAtkMonster->GetAddPhyCriDmgRate() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MagCriDamageUpRate, m_pAtkMonster->GetAddMagCriDmgRate() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsSheep,  m_pAtkMonster->IsSheep() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsSleeping, m_pAtkMonster->IsInSleep() );
#else
	LUAMETATABLE_SET_NUMBER( pState, "PID", m_pAtkMonster->GetMonsterID() );
	LUAMETATABLE_SET_NUMBER( pState, "GID", MONSTER_GID );
	LUAMETATABLE_SET_NUMBER( pState, "Level", m_pAtkMonster->GetLevel() );
	LUAMETATABLE_SET_NUMBER( pState, "Rank",  m_pAtkMonster->GetRank() );

	LUAMETATABLE_SET_NUMBER( pState, "AdditionPhyCriDamage", m_pAtkMonster->GetBuffPhyCriDamAdd() );
	LUAMETATABLE_SET_NUMBER( pState, "AdditionMagCriDamage", m_pAtkMonster->GetBuffMagCriDamAdd() );
	LUAMETATABLE_SET_NUMBER( pState, "PhyDamageUpRate", m_pAtkMonster->GetAddPhyDamageRate() );
	LUAMETATABLE_SET_NUMBER( pState, "MagDamageUpRate", m_pAtkMonster->GetAddMagDamageRate() );
	LUAMETATABLE_SET_NUMBER( pState, "PhyCriDamageUpRate", m_pAtkMonster->GetAddPhyCriDmgRate() );
	LUAMETATABLE_SET_NUMBER( pState, "MagCriDamageUpRate", m_pAtkMonster->GetAddMagCriDmgRate() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsSheep",  m_pAtkMonster->IsSheep() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsSleeping", m_pAtkMonster->IsInSleep() );
#endif

	return true;
}


bool CBattleScript::FillBuffP0( lua_State* pState )
{
	if( NULL == m_pAtkPlayer )
		return false;

	ItemAddtionProp& itemProp = m_pAtkPlayer->GetItemAddtionProp();
	AttributeEffect* pBuffAffect = m_pAtkPlayer->GetBuffEffect();
	if( NULL == pBuffAffect )
		return false;

	lua_getglobal( pState, "p0" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::IncreaseFaint, pBuffAffect->fAddFaintRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Stun, itemProp.Stun );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::IncreaseBondage, pBuffAffect->fAddBondageRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::Tie, itemProp.Tie );
#else
	LUAMETATABLE_SET_NUMBER( pState, "IncreaseFaint", pBuffAffect->fAddFaintRate );
	LUAMETATABLE_SET_NUMBER( pState, "Stun", itemProp.Stun );
	LUAMETATABLE_SET_NUMBER( pState, "IncreaseBondage", pBuffAffect->fAddBondageRate );
	LUAMETATABLE_SET_NUMBER( pState, "Tie", itemProp.Tie );
#endif
	lua_setglobal( pState, "p0" );

	return true;
}


bool CBattleScript::FillBuffP1( lua_State* pState )
{
	if( NULL == m_pTgtPlayer )
		return false;

	ItemAddtionProp& itemProp = m_pTgtPlayer->GetItemAddtionProp();
	AttributeEffect* pBuffAffect = m_pTgtPlayer->GetBuffEffect();
	if( NULL == pBuffAffect )
		return false;

	lua_getglobal( pState, "p1" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::ResistFaint, pBuffAffect->fResistFaintRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AntiStun, itemProp.Antistun );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::ResistBondage, pBuffAffect->fResistBondageRate );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::AntiTie, itemProp.Antitie );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsInvicible, m_pTgtPlayer->IsInvicibleState() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::MoveImmunity, m_pTgtPlayer->IsMoveImmunityState() );
#else
	LUAMETATABLE_SET_NUMBER( pState, "ResistFaint", pBuffAffect->fResistFaintRate );
	LUAMETATABLE_SET_NUMBER( pState, "AntiStun", itemProp.Antistun );
	LUAMETATABLE_SET_NUMBER( pState, "ResistBondage", pBuffAffect->fResistBondageRate );
	LUAMETATABLE_SET_NUMBER( pState, "AntiTie", itemProp.Antitie );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsInvicible", m_pTgtPlayer->IsInvicibleState() );
	LUAMETATABLE_SET_BOOLEAN( pState, "MoveImmunity", m_pTgtPlayer->IsMoveImmunityState() );
#endif
	lua_setglobal( pState, "p1" );

	return true;
}


bool CBattleScript::FillBuffM1( lua_State* pState )
{
	if( NULL == m_pTgtMonster )
		return false;

	lua_getglobal( pState, "m1" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::ResistBondage, m_pTgtMonster->GetResistBondage() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::ResistFaint, m_pTgtMonster->GetNpcResistFaint() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsReturn, !m_pTgtMonster->GetAttackFlag() );
#else
	LUAMETATABLE_SET_NUMBER( pState, "ResistBondage", m_pTgtMonster->GetResistBondage() );
	LUAMETATABLE_SET_NUMBER( pState, "ResistFaint", m_pTgtMonster->GetNpcResistFaint() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsReturn", !m_pTgtMonster->GetAttackFlag() );
#endif
	lua_setglobal( pState, "m1" );

	return true;
}

bool CBattleScript::FillItemDamageP1( lua_State* pState )
{
	if (NULL == m_pTgtPlayer)
		return false;

	lua_getglobal( pState, "p1" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxHP, m_pTgtPlayer->GetMaxHP() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsReturn, !m_pTgtPlayer->GetAttackFlag() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsInvicible, m_pTgtPlayer->IsInvicibleState() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::absorbdamage, m_pTgtPlayer->GetAbsorbDamage() );
#else
	LUAMETATABLE_SET_NUMBER( pState, "MaxHP", m_pTgtPlayer->GetMaxHP() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsReturn", !m_pTgtPlayer->GetAttackFlag() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsInvicible", m_pTgtPlayer->IsInvicibleState() );
	LUAMETATABLE_SET_NUMBER( pState, "absorbdamage", m_pTgtPlayer->GetAbsorbDamage() );
#endif
	lua_setglobal( pState, "p1" );

	return true;
}

bool CBattleScript::FillItemDamageM1( lua_State* pState )
{
	if (NULL == m_pTgtMonster)
		return false;

	lua_getglobal( pState, "m1" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxHP, m_pTgtMonster->GetMaxHp() );
	LUATABLE_SET_BOOLEAN( pState, BATTEL_SCRIPT::IsReturn, !m_pTgtMonster->GetAttackFlag() );
#else
	LUAMETATABLE_SET_NUMBER( pState, "MaxHP", m_pTgtMonster->GetMaxHp() );
	LUAMETATABLE_SET_BOOLEAN( pState, "IsReturn", !m_pTgtMonster->GetAttackFlag() );
#endif
	lua_setglobal( pState, "m1" );

	return true;
}

bool CBattleScript::FillExplosion( lua_State* pState )
{
	if (NULL == m_pAtkPlayer)
		return false;

	lua_getglobal( pState, "p0" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::HP, m_pAtkPlayer->GetCurrentHP() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MP, m_pAtkPlayer->GetCurrentMP() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxHP, m_pAtkPlayer->GetMaxHP() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::MaxMP, m_pAtkPlayer->GetMaxMP() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::BaseHP, m_pAtkPlayer->GetBaseMaxHp() );
	LUATABLE_SET_NUMBER( pState, BATTEL_SCRIPT::BaseMP, m_pAtkPlayer->GetBaseMaxMp() );
#else
	LUAMETATABLE_SET_NUMBER( pState, "HP", m_pAtkPlayer->GetCurrentHP() );
	LUAMETATABLE_SET_NUMBER( pState, "MP", m_pAtkPlayer->GetCurrentMP() );
	LUAMETATABLE_SET_NUMBER( pState, "MaxHP", m_pAtkPlayer->GetMaxHP() );
	LUAMETATABLE_SET_NUMBER( pState, "MaxMP", m_pAtkPlayer->GetMaxMP() );
	LUAMETATABLE_SET_NUMBER( pState, "BaseHP", m_pAtkPlayer->GetBaseMaxHp() );
	LUAMETATABLE_SET_NUMBER( pState, "BaseMP", m_pAtkPlayer->GetBaseMaxMp() );
#endif
	lua_setglobal( pState, "p0" );

	return true;
}


bool CBattleScript::ExSetAbsorbDamage( unsigned int damage )	//减少吸收 需要结束absorb buff
{
	if( NULL == m_pTgtPlayer )
		return false;

	m_pTgtPlayer->SetAbsorbDamage( damage );
	return true;
}


//伤害反弹4个函数
bool CBattleScript::FnPvcDamageRebound( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtMonster = pTargetMonster;
	m_reactFunc = EFnPvcDamageRebound;
	return CallScriptOnFnPvcDamageRebound( damage, battleFlag, pVecRst);
}


bool CBattleScript::FnPvpDamageRebound( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtPlayer = pTargetPlayer;
	m_reactFunc = EFnPvpDamageRebound;
	return CallScriptOnFnPvpDamageRebound( damage, battleFlag, pVecRst );
}


bool CBattleScript::FnCvpDamageRebound( CMonster* pAtkMonster, CPlayer* pTargetPlayer, unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst )
{
	InitComponent();
	m_pTgtPlayer = pTargetPlayer;
	m_pAtkMonster = pAtkMonster;
	m_reactFunc = EFnCvpDamageRebound;
	return CallScriptOnFnCvpDamageRebound( damage, battleFlag, pVecRst );
}


bool CBattleScript::FnCvcDamageRebound( CMonster* pAtkMonster, CMonster* pTargetMonster, unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst )
{
	InitComponent();
	m_pTgtMonster = pTargetMonster;
	m_pAtkMonster = pAtkMonster;
	m_reactFunc = EFnCvcDamageRebound;
	return CallScriptOnFnCvcDamageRebound( damage, battleFlag, pVecRst );
}


//关于buff4个函数
bool CBattleScript::FnAddBuffPvc( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int buffID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtMonster = pTargetMonster;
	m_reactFunc = EFnAddBuffPvc;
	return CallScriptOnFnAddBuffPvc( buffID, pVecRst);
}


bool CBattleScript::FnAddBuffPvp( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int buffID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtPlayer = pTargetPlayer;
	m_reactFunc = EFnAddBuffPvp;
	return CallScriptOnFnAddBuffPvp( buffID, pVecRst );
}


bool CBattleScript::FnAddBuffCvp( CMonster* pAtkMonster, CPlayer* pTargetPlayer, unsigned int buffID, vector<int>* pVecRst )
{
	InitComponent();
	m_pTgtPlayer = pTargetPlayer;
	m_pAtkMonster = pAtkMonster;
	m_reactFunc = EFnAddBuffCvp;
	return CallScriptOnFnAddBuffCvp( buffID, pVecRst );
}


bool CBattleScript::FnAddBuffCvc( CMonster* pAtkMonster, CMonster* pTgtMonster, unsigned int buffID, vector<int>* pVecRst )
{
	InitComponent();
	m_pTgtMonster = pTgtMonster;
	m_pAtkMonster = pAtkMonster;
	m_reactFunc = EFnAddBuffCvc;
	return CallScriptOnFnAddBuffCvc( buffID, pVecRst );
}

//关于加血4个函数
bool CBattleScript::FnPvcHealNum( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int healID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtMonster = pTargetMonster;
	m_reactFunc = EFnPvcHealNum;
	return CallScriptOnFnPvcHealNum( healID, pVecRst );
}


bool CBattleScript::FnPvpHealNum( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int healID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtPlayer = pTargetPlayer;
	m_reactFunc = EFnPvpHealNum;
	return CallScriptOnFnPvpHealNum( healID, pVecRst );
}


bool CBattleScript::FnCvpHealNum( CMonster* pAtkMonster, CPlayer* pTargetPlayer, unsigned int healID, vector<int>* pVecRst )
{
	InitComponent();
	m_pTgtPlayer = pTargetPlayer;
	m_pAtkMonster = pAtkMonster;
	m_reactFunc = EFnCvpHealNum;
	return CallScriptOnFnCvpHealNum( healID, pVecRst );
}


bool CBattleScript::FnCvcHealNum( CMonster* pAtkMonster, CMonster* pTgtMonster, unsigned int healID, vector<int>* pVecRst )
{
	InitComponent();
	m_pTgtMonster = pTgtMonster;
	m_pAtkMonster = pAtkMonster;
	m_reactFunc = EFnCvcHealNum;

	return CallScriptOnFnCvcHealNum( healID, pVecRst );
}


//关于自爆的伤害值处理
bool CBattleScript::FnExlosionPvc( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int explosionArg, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtMonster = pTargetMonster;
	m_reactFunc = EFnExlosionPvc;
	return CallScriptOnFnExlosionPvc( explosionArg, pVecRst );
}


bool CBattleScript::FnExlosionPvp( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int exlosiongArg, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtPlayer = pTargetPlayer;
	m_reactFunc = EFnExlosionPvp;
	return CallScriptOnFnExlosionPvp( exlosiongArg, pVecRst );
}


bool CBattleScript::FnRelievePvc( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int relieveID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtMonster = pTargetMonster;
	m_reactFunc = EFnRelievePvc;
	return CallScriptOnFnRelievePvc( relieveID, pVecRst );
}

bool CBattleScript::FnRelievePvp( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int relieveID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtPlayer = pTargetPlayer;
	m_reactFunc = EFnRelievePvp;
	return CallScriptOnFnRelievePvp( relieveID, pVecRst );
}


bool CBattleScript::FnItemDamagePvc( CPlayer* pAtkPlayer, CMonster* pTargetMonster, unsigned int skillID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtMonster = pTargetMonster;
	m_reactFunc = EFnItemDamagePvc;
	return CallScriptOnFnItemDamagePvc( skillID, pVecRst );
}


bool CBattleScript::FnItemDamagePvp( CPlayer* pAtkPlayer, CPlayer* pTargetPlayer, unsigned int skillID, vector<int>* pVecRst )
{
	InitComponent();
	m_pAtkPlayer = pAtkPlayer;
	m_pTgtPlayer = pTargetPlayer;
	m_reactFunc = EFnItemDamagePvp;
	return CallScriptOnFnItemDamagePvp( skillID, pVecRst );
}


bool CBattleScript::InitSkillProcessArg( bool isUseItem, unsigned int skillID )
{
	m_useSkillID = skillID;

	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_BOOLEAN( m_pLuaState, BATTEL_SCRIPT::IsUseItem, isUseItem );
	LUATABLE_SET_NUMBER( m_pLuaState, BATTEL_SCRIPT::skillID, skillID );
#else
	LUAMETATABLE_SET_BOOLEAN( m_pLuaState, "IsUseItem", isUseItem );
	LUAMETATABLE_SET_NUMBER( m_pLuaState, "skillID", skillID );
#endif
	lua_setglobal( m_pLuaState, "args" );
	return true;
}


//伤害反弹4个函数
bool CBattleScript::CallScriptOnFnPvcDamageRebound( unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnPvcDamageRebound pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();


	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnPvcDamageRebound" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnPvcDamageRebound函数\n" );
		return false;
	}

	if ( NULL == m_pAtkPlayer )
	{
		D_ERROR("CBattleScript::CallScriptOnFnPvcDamageRebound m_pAtkPlayer = NULL");
		return false;
	}

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
	SetField( m_pLuaState, BATTEL_SCRIPT::Damage, damage );
	SetField( m_pLuaState, BATTEL_SCRIPT::BattleFlag, battleFlag);
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
	SetField( m_pLuaState, "Damage", damage );
	SetField( m_pLuaState, "BattleFlag", battleFlag);
#endif
	lua_setglobal( m_pLuaState, "args" );

	lua_getglobal( m_pLuaState, "p0" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( m_pLuaState, BATTEL_SCRIPT::absorbdamage, m_pAtkPlayer->GetAbsorbDamage() );
#else
	LUAMETATABLE_SET_NUMBER( m_pLuaState, "absorbdamage", m_pAtkPlayer->GetAbsorbDamage() );
#endif
	lua_setglobal( m_pLuaState, "p0" );

	//调用检测函数；FnPvcDamageRebound为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnPvcDamageRebound函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//2返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnPvpDamageRebound( unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnPvpDamageRebound pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnPvpDamageRebound" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnPvpDamageRebound函数\n" );
		return false;
	}

	if ( NULL == m_pAtkPlayer )
	{
		D_ERROR("CBattleScript::CallScriptOnFnPvpDamageRebound m_pAtkPlayer = NULL");
		return false;
	}

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
	SetField( m_pLuaState, BATTEL_SCRIPT::Damage, damage );
	SetField( m_pLuaState, BATTEL_SCRIPT::BattleFlag, battleFlag);
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
	SetField( m_pLuaState, "Damage", damage );
	SetField( m_pLuaState, "BattleFlag", battleFlag);
#endif
	lua_setglobal( m_pLuaState, "args" );

	lua_getglobal( m_pLuaState, "p0" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( m_pLuaState, BATTEL_SCRIPT::absorbdamage, m_pAtkPlayer->GetAbsorbDamage() );
#else
	LUAMETATABLE_SET_NUMBER( m_pLuaState, "absorbdamage", m_pAtkPlayer->GetAbsorbDamage() );
#endif
	lua_setglobal( m_pLuaState, "p0" );

	//调用检测函数；FnPvpDamageRebound为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnPvpDamageRebound函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//2返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnCvpDamageRebound( unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnPvpDamageRebound pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();


	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnCvpDamageRebound" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCvpDamageRebound函数\n" );
		return false;
	}

	if ( NULL == m_pAtkMonster)
	{
		D_ERROR("CBattleScript::CallScriptOnFnCvpDamageRebound m_pAtkMonster = NULL");
		return false;
	}

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
	SetField( m_pLuaState, BATTEL_SCRIPT::Damage, damage );
	SetField( m_pLuaState, BATTEL_SCRIPT::BattleFlag, battleFlag);
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
	SetField( m_pLuaState, "Damage", damage );
	SetField( m_pLuaState, "BattleFlag", battleFlag);
#endif
	lua_setglobal( m_pLuaState, "args" );

	lua_getglobal( m_pLuaState, "m0" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( m_pLuaState, BATTEL_SCRIPT::absorbdamage, m_pAtkMonster->GetAbsorbDamage() );
#else
	LUAMETATABLE_SET_NUMBER( m_pLuaState, "absorbdamage", m_pAtkMonster->GetAbsorbDamage() );
#endif
	lua_setglobal( m_pLuaState, "m0" );

	//调用检测函数；FnCvpDamageRebound为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCvpDamageRebound函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//2返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnCvcDamageRebound( unsigned int damage, EBattleFlag battleFlag, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnCvcDamageRebound pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();


	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnCvcDamageRebound" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCvcDamageRebound函数\n" );
		return false;
	}

	if ( NULL == m_pAtkMonster)
	{
		D_ERROR("CBattleScript::CallScriptOnFnCvcDamageRebound m_pAtkMonster = NULL");
		return false;
	}

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
	SetField( m_pLuaState, BATTEL_SCRIPT::Damage, damage );
	SetField( m_pLuaState, BATTEL_SCRIPT::BattleFlag, battleFlag);
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
	SetField( m_pLuaState, "Damage", damage );
	SetField( m_pLuaState, "BattleFlag", battleFlag);
#endif
	lua_setglobal( m_pLuaState, "args" );

	lua_getglobal( m_pLuaState, "m0" );
#ifdef LUAHASH_NUMKEY
	LUATABLE_SET_NUMBER( m_pLuaState, BATTEL_SCRIPT::absorbdamage, m_pAtkMonster->GetAbsorbDamage() );
#else
	LUAMETATABLE_SET_NUMBER( m_pLuaState, "absorbdamage", m_pAtkMonster->GetAbsorbDamage() );
#endif
	lua_setglobal( m_pLuaState, "m0" );

	//调用检测函数；FnCvcDamageRebound为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCvcDamageRebound函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//1返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}

//关于buff4个函数
bool CBattleScript::CallScriptOnFnAddBuffPvc( unsigned int buffID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnAddBuffPvc pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnAddBuffPvc" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnAddBuffPvc函数\n" );
		return false;
	}

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
	SetField( m_pLuaState, BATTEL_SCRIPT::BuffID, buffID );
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
	SetField( m_pLuaState, "BuffID", buffID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	FillBuffP0( m_pLuaState );
	FillBuffM1( m_pLuaState );

	//调用检测函数；FnAddBuffPvc为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 3, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnAddBuffPvc函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//3返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -3)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnAddBuffPvp( unsigned int buffID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnAddBuffPvp pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnAddBuffPvp" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnAddBuffPvp函数\n" );
		return false;
	}

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
	SetField( m_pLuaState, BATTEL_SCRIPT::BuffID, buffID );
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
	SetField( m_pLuaState, "BuffID", buffID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	//填充 
	FillBuffP0( m_pLuaState );
	FillBuffP1( m_pLuaState );

	//调用检测函数；FnAddBuffPvp为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 3, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnAddBuffPvp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//3返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -3)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnAddBuffCvp( unsigned int buffID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnAddBuffCvp pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();

	lua_getglobal( m_pLuaState, "FnAddBuffCvp" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnAddBuffCvp函数\n" );
		return false;
	}

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
	SetField( m_pLuaState, BATTEL_SCRIPT::BuffID, buffID );
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
	SetField( m_pLuaState, "BuffID", buffID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	FillBuffP1( m_pLuaState );

	//调用检测函数；FnAddBuffCvp为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 3, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnAddBuffCvp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//3返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -3)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnAddBuffCvc( unsigned int buffID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnAddBuffCvc pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnAddBuffCvc" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnAddBuffCvc函数\n" );
		return false;
	}

	//加入此次攻击的技能ID
	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::skillID, m_useSkillID );
	SetField( m_pLuaState, BATTEL_SCRIPT::BuffID, buffID );
#else
	SetField( m_pLuaState, "skillID", m_useSkillID );
	SetField( m_pLuaState, "BuffID", buffID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	//推入战斗脚本p1
	lua_getglobal( m_pLuaState, "m1" );
	FillBattleStagem1( m_pLuaState );
	lua_setglobal( m_pLuaState, "m1" );

	lua_getglobal( m_pLuaState, "m0" );
	FillBattleStagem0( m_pLuaState );
	lua_setglobal( m_pLuaState, "m0" );

	FillBuffM1( m_pLuaState );

	//调用检测函数；FnAddBuffCvc为1输入参数,3返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 3, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnAddBuffCvc函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//3返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -3)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


//关于加血4个函数
bool CBattleScript::CallScriptOnFnPvcHealNum( unsigned int healID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnPvcHealNum pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnPvcHealNum" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnPvcHealNum函数\n" );
		return false;
	}

	//推入战斗脚本p0
	lua_getglobal( m_pLuaState, "p0" );
	FillBattleStagep0( m_pLuaState );
	lua_setglobal( m_pLuaState, "p0" );

	//m1为被攻击怪物
	lua_getglobal( m_pLuaState, "m1" );
	FillBattleStagem1( m_pLuaState );
	lua_setglobal( m_pLuaState, "m1" );

	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::HealID, healID );
#else
	SetField( m_pLuaState, "HealID", healID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	//调用检测函数；FnPvcHealNum为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 4, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnPvcHealNum函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//4返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -4)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -3)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnPvpHealNum( unsigned int healID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnPvpHealNum pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnPvpHealNum" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnPvpHealNum函数\n" );
		return false;
	}
	
	//推入战斗脚本p0
	lua_getglobal( m_pLuaState, "p0" );
	FillBattleStagep0( m_pLuaState );
	lua_setglobal( m_pLuaState, "p0" );

	//m1为被攻击怪物
	lua_getglobal( m_pLuaState, "p1" );
	FillBattleStagep1( m_pLuaState );
	lua_setglobal( m_pLuaState, "p1" );

	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::HealID, healID );
#else
	SetField( m_pLuaState, "HealID", healID );
#endif
	lua_setglobal( m_pLuaState, "args" );


	//调用检测函数；FnPvpHealNum为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 4, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnPvpHealNum函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//4返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -4)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -3)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnCvpHealNum( unsigned int healID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnCvpHealNum pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnCvpHealNum" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCvpHealNum函数\n" );
		return false;
	}

	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::HealID, healID );
#else
	SetField( m_pLuaState, "HealID", healID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	//调用检测函数；FnCvpHealNum为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 4, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCvpHealNum函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//4返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -4)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -3)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnCvcHealNum( unsigned int healID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnCvcHealNum pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnCvcHealNum" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnCvcHealNum函数\n" );
		return false;
	}

	lua_getglobal( m_pLuaState, "args" );
#ifdef LUAHASH_NUMKEY
	SetField( m_pLuaState, BATTEL_SCRIPT::HealID, healID );
#else
	SetField( m_pLuaState, "HealID", healID );
#endif
	lua_setglobal( m_pLuaState, "args" );

	//调用检测函数；FnPvcHealNum为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 0, 4, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnCvcHealNum函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//4返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -4)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -3)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnExlosionPvc( unsigned int arg, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnExlosionPvc pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnExlosionPvc" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnExlosionPvc函数\n" );
		return false;
	}

	//压入自爆技能的参数
	lua_pushnumber( m_pLuaState, arg );

	FillExplosion( m_pLuaState );

	//调用检测函数；FnExlosionPvc为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 1, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnExlosionPvc函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//2返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


bool CBattleScript::CallScriptOnFnExlosionPvp( unsigned int arg, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnExlosionPvp pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnExlosionPvp" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnExlosionPvp函数\n" );
		return false;
	}

	//压入自爆技能的参数
	lua_pushnumber( m_pLuaState, arg );

	FillExplosion( m_pLuaState );

	//调用检测函数；FnExlosionPvp为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 1, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnExlosionPvp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//2返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	
	return true;
}


//关于relieve2个
bool CBattleScript::CallScriptOnFnRelievePvc( unsigned int relieveID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnRelievePvc pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnRelievePvc" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnRelievePvc函数\n" );
		return false;
	}

	//压入relieve参数
	lua_pushnumber( m_pLuaState, relieveID );

	//调用检测函数；FnRelievePvc为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 1, 0, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnRelievePvc函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	return true;
}


bool CBattleScript::CallScriptOnFnRelievePvp( unsigned int relieveID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
			
	lua_getglobal( m_pLuaState, "FnRelievePvp" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnRelievePvp函数\n" );
		return false;
	}

	//压入relieve参数
	lua_pushnumber( m_pLuaState, relieveID );

	//调用检测函数；FnRelievePvp为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 1, 0, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnRelievePvp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	return true;
}


bool CBattleScript::CallScriptOnFnItemDamagePvc(  unsigned int skillID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnItemDamagePvc pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnItemDamagePvc" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnItemDamagePvc函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, skillID );
	FillItemDamageM1( m_pLuaState );
	
	//调用检测函数；FnItemDamagePvc为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 1, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnItemDamagePvc函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//2返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	

	return true;
}



bool CBattleScript::CallScriptOnFnItemDamagePvp(  unsigned int skillID, vector<int>* pVecRst )
{
	CLuaRestoreStack rs( m_pLuaState );
	ACE_UNUSED_ARG( rs );//防止未使用参数警告；
	if( NULL == pVecRst )
	{
		D_ERROR("CBattleScript::CallScriptOnFnItemDamagePvp pVecRst = NULL;");
		return false;
	}
	pVecRst->clear();
			
	lua_getglobal( m_pLuaState, "FnItemDamagePvp" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "战斗脚本未定义FnItemDamagePvp函数\n" );
		return false;
	}

	lua_pushnumber( m_pLuaState, skillID );
	FillItemDamageP1( m_pLuaState );

	//调用检测函数；FnItemDamagePvp为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 1, 2, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行FnItemDamagePvp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	//2返回值
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -2)) );	
	pVecRst->push_back( (int)(lua_tonumber(m_pLuaState, -1)) );	

	return true;
}


bool CBattleScript::ExEndTgtPlayerAllDebuff()		//所有的debuff
{
	if( NULL == m_pTgtPlayer )
	{
		D_ERROR("CBattleScript::ExEndTgtPlayerAllDebuff m_pTgtPlayer = NULL\n");
		return false;
	}

	m_pTgtPlayer->EndAllDebuff();
	return true;
}



bool CBattleScript::ExEndTgtPlayerControlBuff()		//控制类的技能
{
	if( NULL == m_pTgtPlayer )
	{
		D_ERROR("CBattleScript::ExEndTgtPlayerControlBuff m_pTgtPlayer = NULL\n");
		return false;
	}

	m_pTgtPlayer->EndControlBuff();
	return true;
}

bool CBattleScript::ExRelieveTgtPlayerBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve )
{
	if( NULL == m_pTgtPlayer )
	{
		D_ERROR("CBattleScript::ExRelieveTgtBuff m_pTgtPlayer = NULL\n");
		return false;
	}
	
	return m_pTgtPlayer->RelieveBuff( relieveType, relieveLevel, relieveNum, isRealRelieve );	
}


bool CBattleScript::ExRelieveTgtMonsterBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve )			//真正清除
{
	if( NULL == m_pTgtMonster )
	{
		D_ERROR("CBattleScript::ExRelieveTgtBuff m_pTgtPlayer = NULL\n");
		return false;
	}
	
	return m_pTgtMonster->RelieveBuff( relieveType, relieveLevel, relieveNum, isRealRelieve );	
}

bool CBattleScript::ExAtkHaveItem(unsigned int itemID, unsigned int num )
{
	if ( NULL == m_pAtkPlayer )
	{
		D_ERROR("CBattleScript::ExAtkHaveItem m_pAtkPlayer = NULL\n");
		return false;
	}

	return ((unsigned int)m_pAtkPlayer->CounterItemByID(itemID) >= num);
}

bool CBattleScript::ExTgtHaveItem(unsigned int itemID, unsigned int num )
{
	if ( NULL == m_pTgtPlayer )
	{
		D_ERROR("CBattleScript::ExTgtHaveItem m_pTgtPlayer = NULL\n");
		return false;
	}

	return ((unsigned int)m_pTgtPlayer->CounterItemByID(itemID) >= num);
}

bool CBattleScript::ExAtkCreateWeakBuff(unsigned int time )
{
	if ( NULL == m_pAtkPlayer )
	{
		D_ERROR("CBattleScript::ExAtkCreateWeakBuff m_pAtkPlayer = NULL\n");
		return false;
	}
	m_pAtkPlayer->CreateWeakBuff( time );
	return true;
}






