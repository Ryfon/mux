﻿#include "LevelUpScript.h"

#ifdef LEVELUP_SCRIPT

#include "../Player/Player.h"
#include "ScriptExtent.h"

map<unsigned int,CLevelUpScript*> CLevelUpScriptManager::m_mapLevelUpScript;	//职业ID<---->脚本指针映射
map<lua_State*, CLevelUpScript*>  CLevelUpScriptManager::m_mapLuaStateFsms;		//lua状态<---->脚本指针映射;

CLevelUpScript::~CLevelUpScript()
{}

bool CLevelUpScript::Init( const char* luaScriptFile )
{
	return LoadFrom<CLevelUpScriptExtent>( luaScriptFile );
}

bool CLevelUpScript::OnPlayerLevelUp( CPlayer* pLevelUpPlayer )		//调用LUA脚本
{
	ResetPaticipant();
	m_pScriptUser = pLevelUpPlayer;
	if ( NULL == pLevelUpPlayer )
	{
		return false;
	}
	unsigned short level = pLevelUpPlayer->GetLevel();
	return CallScriptOnPlayerLevelUp( level );
}

bool CLevelUpScript::OnPlayerSetBaseProp( CPlayer* pLevelUpPlayer )	//调用LUA脚本
{
	ResetPaticipant();
	m_pScriptUser = pLevelUpPlayer;
	if ( NULL == pLevelUpPlayer )
	{
		return false;
	}
	unsigned short level = pLevelUpPlayer->GetLevel();
	return CallScriptOnPlayerSetBaseProp( level );
}


bool CLevelUpScript::CallScriptOnPlayerLevelUp( unsigned short level )
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnPlayerLevelUp" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "升级脚本未定义OnPlayerLevelUp函数\n" );
		return false;
	}

	//推入LUA脚本的参数 升级的等级
	lua_pushnumber( m_pLuaState, level );

	//调用检测函数；OnPlayerLevelUp为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 1, 0, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行OnPlayerLevelUp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	return true;
}


//调用设置基本属性脚本
bool CLevelUpScript::CallScriptOnPlayerSetBaseProp( unsigned short level )
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnPlayerSetBaseProp" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "升级脚本未定义OnPlayerSetBaseProp函数\n" );
		return false;
	}

	//推入LUA脚本的参数 升级的等级
	lua_pushnumber( m_pLuaState, level );

	//调用检测函数；OnPlayerSetBaseProp为1输入参数,0返回值的LUA函数
	if ( 0 != lua_pcall( m_pLuaState, 1, 0, 0 ) ) //检测函数调用失败;
	{
		const char* err = lua_tostring( m_pLuaState, -1 );
		if ( NULL != err )
		{
			D_ERROR( "执行OnPlayerSetBaseProp函数失败 错误信息:%s\n", err );
		}
		return false;
	}

	return true;
}


bool CLevelUpScript::ExtSendSystemMsg( const char* pMsg )
{
	if( NULL == pMsg || NULL == m_pScriptUser)
		return false;
	
	m_pScriptUser->SendSystemChat( pMsg );
	return true;
}


//设置最大基础hp
bool CLevelUpScript::ExtLevelSetBaseMaxHp( unsigned int maxHp )
{
	if( NULL == m_pScriptUser )
		return false;
	
	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBaseMaxHp( maxHp );
	return true;
}

//设置最大基础mp
bool CLevelUpScript::ExtLevelSetBaseMaxMp( unsigned int maxMp )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBaseMaxMp( maxMp );
	return true;
}


//设置力量
bool CLevelUpScript::ExtLevelSetStr( unsigned int baseStr )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBaseStr( baseStr );
	return true;
}


//设置敏捷
bool CLevelUpScript::ExtLevelSetAgi( unsigned int baseAgi )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBaseAgi( baseAgi );
	return true;
}


//设置智力
bool CLevelUpScript::ExtLevelSetInt( unsigned int baseInt )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBaseInt( baseInt );
	return true;
}


//设置基础物理攻击力
bool CLevelUpScript::ExtLevelSetBasePhyAtk( unsigned int basePhyAtk )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBasePhyAtk( basePhyAtk );
	return true;
}


//设置基础魔法攻击力
bool CLevelUpScript::ExtLevelSetBaseMagAtk( unsigned int baseMagAtk )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBaseMagAtk( baseMagAtk );
	return true;
}


//设置基础物理防御力
bool CLevelUpScript::ExtLevelSetBasePhyDef( unsigned int basePhyDef )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBasePhyDef( basePhyDef );
	return true;
}


//设置基础魔法防御力
bool CLevelUpScript::ExtLevelSetBaseMagDef( unsigned int baseMagDef )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBaseMagDef( baseMagDef );
	return true;
}


//设置基础物理命中
bool CLevelUpScript::ExtLevelSetBasePhyHit( unsigned int basePhyHit )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBasePhyHit( basePhyHit );
	return true;
}


//设置基础魔法命中
bool CLevelUpScript::ExtLevelSetBaseMagHit( unsigned int baseMagHit )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetBaseMagHit( baseMagHit );
	return true;
}


//设置下级经验
bool CLevelUpScript::ExtLevelSetNextLevelExp( unsigned int nextLevelExp )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->SetNextLevelExp( nextLevelExp );
	return true;
}


//添加技能
bool CLevelUpScript::ExtLevelAddSkill( unsigned int skillID )
{
	if( NULL == m_pScriptUser )
		return false;

	m_pScriptUser->AddSkillByID( skillID );
	return true;
}


//添加道具
bool CLevelUpScript::ExtLevelAddItem( unsigned int itemID, unsigned int itemNum )
{
	if( NULL == m_pScriptUser )
		return false;

	m_pScriptUser->GetNormalItem( itemID, itemNum );
	return true;
}

//添加一个buff
bool CLevelUpScript::ExtLevelAddBuff( unsigned int buffID, unsigned int buffTime )
{
	if( NULL == m_pScriptUser )
		return false;

	CBuffProp* pBuffProp = CManBuffProp::FindObj( buffID );
	PlayerID createID;
	createID.dwPID = 0;
	createID.wGID = 0;
	//return m_pScriptUser->CreateBuff( pBuffProp, buffTime, createID, CREATE_NULL );
	D_WARNING( "CLevelUpScript::ExtLevelAddBuff， 调用已被注释功能\n" );
	return true;
}

//添加sp豆
bool CLevelUpScript::ExtLevelAddSpBean( unsigned int spBeanNum )
{
	if( NULL == m_pScriptUser )
		return false;

	////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, m_pScriptUser->AddSpBean( spBeanNum );
	return true;
}


unsigned short CLevelUpScript::ExtLevelRace()
{
	if( NULL == m_pScriptUser )
		return false;

	return m_pScriptUser->GetRace();
}


unsigned char CLevelUpScript::ExtLevelSex()
{
	if( NULL == m_pScriptUser )
		return false;

	return m_pScriptUser->GetSex();
}

bool CLevelUpScript::ExtShowNotice(const char* content, char position, char isglobal, unsigned int maxshowcount, unsigned int distancetime )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CLevelUpScript::ExtShowNotice, NULL == m_pScriptUser\n" );
		return false;
	}

	if ( !content )
		return false;

	MGShowTmpNoticeByPosition tmpnotice;
	StructMemSet( tmpnotice, 0x0, sizeof(tmpnotice) );
	tmpnotice.isbroadcast = isglobal;
	tmpnotice.showtmpnotice.maxshowcount = maxshowcount;
	tmpnotice.showtmpnotice.distancetime = distancetime;
	tmpnotice.showtmpnotice.showposition = position;
	tmpnotice.showtmpnotice.contentlen = ACE_OS::snprintf( tmpnotice.showtmpnotice.content,
		sizeof(tmpnotice.showtmpnotice.content)-1,
		"%s",
		content);

	m_pScriptUser->SendPkg<MGShowTmpNoticeByPosition>( &tmpnotice );
	return true;
}

bool CLevelUpScript::ExtShowParamNotice(unsigned int msgid, char position, char isglobal,unsigned int maxshowcount, unsigned int distancetime,
									   unsigned int validparam,const char* param1,const char* param2, const char* param3,const char* param4,const char* param5 )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CLevelUpScript::ExtShowNotice, NULL == m_pScriptUser\n" );
		return false;
	}

	MGSendParamNotice paramnotice;
	StructMemSet( paramnotice, 0x0, sizeof(paramnotice) );
	paramnotice.parammsgnotice.msgid = msgid;
	paramnotice.parammsgnotice.showposition = position;
	paramnotice.parammsgnotice.validparamcount = validparam;
	paramnotice.parammsgnotice.distancetime = distancetime;
	paramnotice.parammsgnotice.maxshowcount = maxshowcount;
	paramnotice.isgloabl = isglobal;

	if ( validparam>0 && param1 )
	{
		paramnotice.parammsgnotice.param1len = ACE_OS::snprintf( paramnotice.parammsgnotice.param1,
			sizeof(paramnotice.parammsgnotice.param1)-1,
			"%s",
			param1 );

		if ( validparam>1 && param2 )
		{
			paramnotice.parammsgnotice.param2len = ACE_OS::snprintf( paramnotice.parammsgnotice.param2,
				sizeof(paramnotice.parammsgnotice.param2)-1,
				"%s",
				param2 );

			if ( validparam>2 && param3 )
			{
				paramnotice.parammsgnotice.param3len = ACE_OS::snprintf( paramnotice.parammsgnotice.param3,
					sizeof(paramnotice.parammsgnotice.param3)-1,
					"%s",
					param3 );

				if ( validparam>3 && param4 )
				{
					paramnotice.parammsgnotice.param4len = ACE_OS::snprintf( paramnotice.parammsgnotice.param4,
						sizeof(paramnotice.parammsgnotice.param4)-1,
						"%s",
						param4 );

					if ( validparam>4 && param5 )
					{
						paramnotice.parammsgnotice.param5len = ACE_OS::snprintf( paramnotice.parammsgnotice.param5,
							sizeof(paramnotice.parammsgnotice.param5)-1,
							"%s",
							param5 );
					}
				}
			}
		}
	}
	m_pScriptUser->SendPkg<MGSendParamNotice>( &paramnotice );
	return true;
}



bool CLevelUpScript::ExtSendMail(const char* pMailTitle, const char* pMailContent, unsigned int money, vector<unsigned int>& vecItemTypes, vector<int>& vecItemCounts )
{
	if( NULL == m_pScriptUser )
	{
		D_ERROR( "CLevelUpScript::ExtSendMail, NULL == m_pScriptUser\n" );
		return false;
	}

	if ( (NULL == pMailTitle) 
		|| (NULL == pMailContent) 
		)
	{
		D_ERROR( "CLevelUpScript::ExtSendMail，欲发送邮件的标题或内容空\n" );
		return false;
	}

	if ( vecItemTypes.size() != vecItemCounts.size() )
	{
		D_ERROR( "CLevelUpScript::ExtSendMail，欲发送邮件附件物品类型与数量数组大小不一致(%d:%d)\n", vecItemTypes.size(), vecItemCounts.size() );
		return false;
	}

	MGSendSysMailToPlayer sendSysMail;
	StructMemSet( sendSysMail, 0x0, sizeof(sendSysMail) );
	SafeStrCpy( sendSysMail.newSysMail.recvPlayerName, m_pScriptUser->GetNickName() );
	SafeStrCpy( sendSysMail.newSysMail.sendMailContent, pMailContent );
	int contentlen = sizeof(sendSysMail.newSysMail.sendMailContent) - 1;
	sendSysMail.newSysMail.sendMailContent[contentlen] = '\0';
	SafeStrCpy( sendSysMail.newSysMail.sendMailTitle,   pMailTitle );
	int titlelen = sizeof(sendSysMail.newSysMail.sendMailTitle) -1;
	sendSysMail.newSysMail.sendMailTitle[titlelen] = '\0';
	sendSysMail.newSysMail.silverMoney = money;//双币种判断
	sendSysMail.newSysMail.goldMoney = 0;//双币种判断

	unsigned int maxitemCount = MAX_MAIL_ATTACHITEM_COUNT;
	if ( money != 0 )
	{
		maxitemCount = MAX_MAIL_ATTACHITEM_COUNT - 1;
	}

	unsigned int itemIndex = 0 ;
	vector<int>::iterator iter1;
	vector<unsigned int>::iterator iter2;
	for ( iter1=vecItemCounts.begin(), iter2=vecItemTypes.begin(); 
		  iter1!=vecItemCounts.end(), iter2!=vecItemTypes.end(); ++iter1, ++iter2 )
	{
		if ( itemIndex >= ARRAY_SIZE(sendSysMail.newSysMail.attachItem) )
		{
			D_ERROR( "CNormalScript::ExtSendMail, itemIndex(%d)越界，item数组大小%d\n", itemIndex, vecItemCounts.size() );
			break;
		}
		sendSysMail.newSysMail.attachItem[itemIndex].usItemTypeID = *iter2;
		sendSysMail.newSysMail.attachItem[itemIndex].ucCount = *iter1;
		++itemIndex;
	}
	//for ( unsigned  int  i = 0 ; i < 12 && itemIndex < maxitemCount ; i+=2 )
	//{
	//	if ( ( i+1 >= ARRAY_SIZE(itemArr) )
	//		|| ( itemIndex >= ARRAY_SIZE(sendSysMail.newSysMail.attachItem) )
	//		)
	//	{
	//		D_ERROR( "CLevelUpScript::ExtSendMail, i(%d)或itemIndex(%d)越界\n", i, itemIndex );
	//		break;
	//	}
	//	if ( itemArr[i] == 0 || itemArr[i+1] == 0 )
	//	{
	//		break;
	//	}

	//	sendSysMail.newSysMail.attachItem[itemIndex].usItemTypeID = itemArr[i];
	//	sendSysMail.newSysMail.attachItem[itemIndex].ucCount = itemArr[i+1];
	//	itemIndex++;
	//}

	m_pScriptUser->SendPkg<MGSendSysMailToPlayer>( &sendSysMail );
	return true;
}

//一共就6个职业一个一个加载
bool CLevelUpScriptManager::Init()
{
	//战士的升级脚本
	CLevelUpScript* pWarriorScript = NEW CLevelUpScript;
	if( !pWarriorScript->Init("config/scripts/levelupscript/warriorlevelup.lua") )
	{
		D_ERROR("找不到战士的升级脚本\n");
		delete pWarriorScript; pWarriorScript = NULL;
		return false;
	}
	if( !AddScript( (unsigned int)CLASS_WARRIOR, pWarriorScript ) )
	{
		delete pWarriorScript; pWarriorScript = NULL;
		return false;
	}
		

	//刺客的升级脚本
	CLevelUpScript* pAssassinScript = NEW CLevelUpScript;
	if( !pAssassinScript->Init("config/scripts/levelupscript/assassinlevelup.lua") )
	{
		D_ERROR("找不到刺客的升级脚本");
		delete pAssassinScript; pAssassinScript = NULL;
		return false;
	}
	if( !AddScript( (unsigned int)CLASS_ASSASSIN, pAssassinScript ) )
	{
		delete pAssassinScript; pAssassinScript = NULL;
		return false;
	}

	//弓箭手的升级脚本
	CLevelUpScript* pHunterScript = NEW CLevelUpScript;
	if( !pHunterScript->Init("config/scripts/levelupscript/hunterlevelup.lua") )
	{
		D_ERROR("找不到弓箭手的升级脚本");
		delete pHunterScript; pHunterScript = NULL;
		return false;
	}
	if( !AddScript( (unsigned int)CLASS_HUNTER, pHunterScript ) )
	{
		delete pHunterScript; pHunterScript = NULL;
		return false;
	}

	//魔法师的升级脚本
	CLevelUpScript* pMageScript = NEW CLevelUpScript;
	if( !pMageScript->Init("config/scripts/levelupscript/magelevelup.lua") )
	{
		D_ERROR("找不到魔法师的升级脚本");
		delete pMageScript; pMageScript = NULL;
		return false;
	}
	if( !AddScript( (unsigned int)CLASS_MAGE, pMageScript ) )
	{
		delete pMageScript; pMageScript = NULL;
		return false;
	}
	return true;
}


bool CLevelUpScriptManager::AddScript( unsigned int jobID, CLevelUpScript*  pScript )
{
	if( NULL == pScript )
		return false;

	map<unsigned int,CLevelUpScript*>::iterator jobIter = m_mapLevelUpScript.find( jobID );
	if( jobIter != m_mapLevelUpScript.end() )
	{
		D_ERROR("插入的重复的脚本 jobID:%d\n", jobID );
		return false;
	}
	
	map<lua_State*, CLevelUpScript*>::iterator luaIter = m_mapLuaStateFsms.find( pScript->GetState() );
	if( luaIter != m_mapLuaStateFsms.end() )
	{
		D_ERROR("插入的重复的脚本 luaState:%d\n", pScript->GetState() );
		return false;
	}

	m_mapLevelUpScript.insert( make_pair( jobID, pScript ) );
	m_mapLuaStateFsms.insert( make_pair( pScript->GetState(), pScript ));
	return true;
}

#endif  //LEVELUP_SCRIPT


