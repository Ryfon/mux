﻿#pragma once

#include "Utility.h"
#include "LuaScript.h"

class CPlayer;

class CCalSecondPropertyScript : public CLuaScriptBase
{
public:
	CCalSecondPropertyScript(void);
	virtual ~CCalSecondPropertyScript(void);


	bool Init( const char* luaScriptFile );

	//调用lua脚本的函数
public:
	bool FnCalPhyAtk( CPlayer * player, int & ret );
	bool FnCalPhyDef( CPlayer * player, int & ret );
	bool FnCalPhyHit( CPlayer * player, int & ret );
	bool FnCalPhyJouk( CPlayer * player, int & ret );
	bool FnCalMagAtk( CPlayer * player, int & ret );
	bool FnCalMagDef( CPlayer * player, int & ret );
	bool FnCalMagHit( CPlayer * player, int & ret );
	bool FnCalMagJouk( CPlayer * player, int & ret );
	bool FnCalExcellent( CPlayer * player, int & ret );
	bool FnCalCritical( CPlayer * player, int & ret );
	bool FnCalHP( CPlayer * player, int & ret );
	bool FnCalHPrecovery( CPlayer * player, int & ret );
	bool FnCalMP( CPlayer * player, int & ret );
	bool FnCalMPrecovery( CPlayer * player, int & ret );


private:
	CCalSecondPropertyScript( const CCalSecondPropertyScript& );  //屏蔽这两个操作；
	CCalSecondPropertyScript& operator = ( const CCalSecondPropertyScript& );//屏蔽这两个操作；

private:
	bool CallScriptCalPhyAtk( int & ret );
	bool CallScriptCalPhyDef( int & ret );
	bool CallScriptCalPhyHit( int & ret );
	bool CallScriptCalPhyJouk( int & ret );
	bool CallScriptCalMagAtk( int & ret );
	bool CallScriptCalMagDef( int & ret );
	bool CallScriptCalMagHit( int & ret );
	bool CallScriptCalMagJouk( int & ret );
	bool CallScriptCalExcellent( int & ret );
	bool CallScriptCalCritical( int & ret );
	bool CallScriptCalHP( int & ret );
	bool CallScriptCalHPrecovery( int & ret );
	bool CallScriptCalMP( int & ret );
	bool CallScriptCalMPrecovery( int & ret );
private:
	CPlayer * m_pPlayer;
};


class CCalSecondPropertyScriptManager
{
private:
	CCalSecondPropertyScriptManager();//屏蔽此构造;
	~CCalSecondPropertyScriptManager();//屏蔽此析构;
public:
	///从配置中读入脚本;
	static bool Init()
	{
		CCalSecondPropertyScript* pScript = NEW CCalSecondPropertyScript;
		if ( NULL == pScript )
		{
			return false;
		}
		if ( pScript->Init( "config/scripts/CalSecondProp.lua" ) ) 
		{
			m_pCalSecondPropScript = pScript;
		} else {
			delete pScript; m_pCalSecondPropScript = NULL; pScript = NULL;
			return false;
		}
		return true;
	}

	//释放管理的脚本；
	static void Release()
	{ 
		if( NULL != m_pCalSecondPropScript )
		{
			delete m_pCalSecondPropScript; m_pCalSecondPropScript = NULL;
		}
	}


	///寻找对应职业,找到对应的脚本
	static CCalSecondPropertyScript* GetScript()
	{
		return m_pCalSecondPropScript;
	};

private:
	static CCalSecondPropertyScript* m_pCalSecondPropScript;	//战斗脚本
};
