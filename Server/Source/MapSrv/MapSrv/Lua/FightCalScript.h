﻿/**
* @file FightCalScript.h
* @brief 战斗计算公式脚本
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: FightCalScript.h
* 摘    要: 管理战斗计算公式脚本
* 作    者: dzj
* 完成日期: 2008.03.04
*
*/

#pragma once

#include "LuaScript.h"
#include <vector>
#include "Utility.h"
#include "../NpcSkill.h"
using namespace std;

extern const char* FCAL_SCRIPT_NAME;

class CPlayer;
class CMonster;
class CPlayerSkill;
///战斗判定脚本；
class CFightCalScript : public CLuaScriptBase
{

	friend class CFightCalScriptManager;

private:
	CFightCalScript( const CFightCalScript& );  //屏蔽这两个操作；
	CFightCalScript& operator = ( const CFightCalScript& );//屏蔽这两个操作；

private:
	CFightCalScript() {};
	virtual ~CFightCalScript (void);

	bool Init( const char* luaScriptFile );

public:
	///玩家直接攻击怪物计算;
	bool OnPvcCal( CPlayer* pPlayer/*attacker*/, CMonster* pTargetMonster/*target*/, CPlayerSkill* m_pPlayerSkill/*skill used*/, vector<int>* pRstVec/*return values*/  );
	///怪物直接攻击玩家计算
	bool OnCvpCal( CMonster* pMonster, CPlayer* pTargetPlayer, CNpcSkill* pNpcSkill, vector<int>* pRstVec);
	///玩家直接攻击玩家  １　攻击玩家　２被攻击玩家　３攻击玩家所用的技能　４返回值
	bool OnPvpCal( CPlayer* pAttackPlayer, CPlayer* pTargetPlayer, CPlayerSkill* pPlayerSkill, vector<int>* pRstVec);
	//NPC 攻击 NPC
	bool OnCvcCal( CMonster* pAtkMonster, CMonster* pTargetMonster, CNpcSkill* pNpcSkill, vector<int>* pRstVec );

public:
	///脚本扩展函数调用，返回主动攻击的玩家，如果主动攻击者确实为玩家的话；
	CPlayer* GetAttackPlayer() { return m_pAttackPlayer; }
	///返回本次攻击的目标怪物；
	CMonster* GetTargetMonster() { return m_pTargetMonster; }
	///返回本次攻击所使用的技能；
	CPlayerSkill* GetPlayerSkill() { return m_pPlayerSkill; }
	//返回本次攻击的攻击怪物
	CMonster* GetAttackMonster() { return m_pAttackMonster; }
	//返回这次攻击的目标玩家
	CPlayer* GetTargetPlayer() { return m_pTargetPlayer; }
	//返回这次攻击的怪物技能
	CNpcSkill* GetNpcSkill()  { return m_pNpcSkill; }

private:
	///重置参与者，由于主逻辑为单线程，因此各次战斗判断调用顺序进行，每次新的战斗判断都清之前的战斗判断参与者；
	void ResetPaticipant()
	{
		m_pAttackPlayer = NULL;
		m_pTargetPlayer = NULL;
		m_pAttackMonster = NULL;
		m_pTargetMonster = NULL;
		m_pPlayerSkill = NULL;
		m_pNpcSkill = NULL;
	}

	///调用脚本玩家攻击怪物计算公式;
	bool CallScriptOnPvcCal( vector<int>* pRstVec );
	///调用脚本怪物攻击玩家计算公式
	bool CallScriptOnCvpCal( vector<int>* pRstVec);
	///调用脚本玩家攻击玩家的计算公式
	bool CallScriptOnPvpCal( vector<int>* pRstVec);
	///调用脚本NPC攻击NPC的计算公式
	bool CallScriptOnCvcCal( vector<int>* pRstVec);

private:
	CPlayer*  m_pAttackPlayer;  //攻击玩家；
	CMonster* m_pTargetMonster; //被攻击的怪物；
	CMonster* m_pAttackMonster;	 //攻击玩家的怪物 04。08 hyn
	CPlayer* m_pTargetPlayer;	//被怪物攻击的玩家
	CPlayerSkill* m_pPlayerSkill;     //玩家攻击所使用的技能；
	CNpcSkill* m_pNpcSkill;		//怪物攻击所使用的技能	
};

class CFightCalScriptManager
{
private:
	CFightCalScriptManager();//屏蔽此构造;
	~CFightCalScriptManager();//屏蔽此析构;
public:
	///从配置中读入脚本;
	static bool Init()
	{
		CFightCalScript* pScript = NEW CFightCalScript;
		if ( pScript->Init( "config/scripts/fightcal.lua" ) ) //FCAL_SCRIPT_NAME
		{
			m_pFightCalScript = pScript;
			//AddScripts( FCAL_SCRIPT_NAME, pScript );
		} else {
			m_pFightCalScript = NULL;
			return false;
		}
		return true;
	}

	//释放管理的脚本；
	static void Release()
	{
		if ( NULL != m_pFightCalScript )
		{
			delete m_pFightCalScript; m_pFightCalScript = NULL;
		}
		return;
	};

	//static void AddScripts( const char* strName, CFightCalScript* pScript )
	//{
	//	m_NameToScripts.insert( pair<const char*, CFightCalScript*>( strName, pScript ) );
	//	m_StateToScripts.insert( pair<lua_State*, CFightCalScript*>( pScript->GetState(), pScript ) );
	//}

	///寻找对应名字的脚本，用于CNpc；
	static CFightCalScript* FindScripts( const char* strName ) { ACE_UNUSED_ARG(strName); return m_pFightCalScript; };

	///寻找对应luastate的脚本，用于luaCallBack；
	static CFightCalScript* FindScripts( lua_State* pState )
	{
		ACE_UNUSED_ARG( pState);
		return m_pFightCalScript;
		//map<lua_State*, CFightCalScript*>::iterator tmpIter = m_StateToScripts.find( pState );
		//if ( tmpIter == m_StateToScripts.end() )
		//{
		//	return NULL;
		//}
		//return tmpIter->second;
	}

private:
	static CFightCalScript* m_pFightCalScript;
	//static map<const char*, CFightCalScript*> m_NameToScripts;
	//static map<lua_State*, CFightCalScript*>  m_StateToScripts;
};
