﻿/**
* @file FightCalScript.cpp
* @brief 战斗计算公式脚本
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: FightCalScript.cpp
* 摘    要: 管理战斗计算公式脚本
* 作    者: dzj
* 完成日期: 2008.03.04
*
*/

#include "FightCalScript.h"
#include "../Player/Player.h"
#include "ScriptExtent.h"

const char* FCAL_SCRIPT_NAME = "fightcal.lua";

CFightCalScript* CFightCalScriptManager::m_pFightCalScript;
//map<const char*, CFightCalScript*> CFightCalScriptManager::m_NameToScripts;
//map<lua_State*, CFightCalScript*>  CFightCalScriptManager::m_StateToScripts;

CFightCalScript::~CFightCalScript (void) {};

bool CFightCalScript::Init( const char* luaScriptFile ) 
{ 
	return LoadFrom<CFightScriptExtent>( luaScriptFile );
};

///玩家直接攻击怪物计算;
bool CFightCalScript::OnPvcCal( CPlayer* pPlayer/*attacker*/, CMonster* pTargetMonster/*target*/, CPlayerSkill* pSkill/*skill used*/, vector<int>* pRstVec/*return values*/  )
{ 
	ResetPaticipant();
	m_pAttackPlayer = pPlayer;
	m_pTargetMonster = pTargetMonster;
	m_pPlayerSkill = pSkill;

	return CallScriptOnPvcCal( pRstVec );
}

///怪物直接攻击玩家计算
bool CFightCalScript::OnCvpCal( CMonster* pMonster, CPlayer* pTargetPlayer, CNpcSkill* pNpcSkill, vector<int>* pRstVec)
{
	ResetPaticipant();
	m_pAttackMonster = pMonster;
	m_pTargetPlayer = pTargetPlayer;
	m_pNpcSkill = pNpcSkill;
	return CallScriptOnCvpCal( pRstVec );
}


bool CFightCalScript::OnPvpCal( CPlayer* pAttackPlayer, CPlayer* pTargetPlayer, CPlayerSkill* pPlayerSkill, vector<int>* pRstVec)
{
	ResetPaticipant();
	m_pAttackPlayer = pAttackPlayer;
	m_pTargetPlayer = pTargetPlayer;
	m_pPlayerSkill = pPlayerSkill;
	return CallScriptOnPvpCal( pRstVec );
}


//NPC 攻击 NPC
bool CFightCalScript::OnCvcCal( CMonster* pAtkMonster, CMonster* pTargetMonster, CNpcSkill* pNpcSkill, vector<int>* pRstVec )
{
	ResetPaticipant();
	m_pAttackMonster = pAtkMonster;
	m_pTargetMonster = pTargetMonster;
	m_pNpcSkill = pNpcSkill;
	return CallScriptOnCvcCal( pRstVec );
}



///调用脚本玩家攻击怪物计算公式;
bool CFightCalScript::CallScriptOnPvcCal( vector<int>* pRstVec )
{
	g_debugPlayer = m_pAttackPlayer;

	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnPvcCal" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "脚本未定义PVC战斗计算函数:OnPvcCal\n" );
		return false;
	}
	//调用检测函数；
	int nRet = lua_pcall( m_pLuaState, 0, 3, 0 );
	if ( 0 != nRet ) //检测函数调用失败;
	{
		D_ERROR( "执行PVC战斗判断函数失败 error:%d\n", nRet );
		return false;
	}

	pRstVec->clear();
	for ( int i=0; i<3; ++i )
	{
		if ( !lua_isnumber( m_pLuaState, -1-i ) )
		{
			D_ERROR( "战斗计算脚本返回值个数错误，实际返回值只有%d个\n", i );
			return false;
		}
		pRstVec->push_back( (int)(lua_tonumber(m_pLuaState, -1-i)) );
	}

	g_debugPlayer = NULL;

	//取调用结果；
	return true;
}


bool CFightCalScript::CallScriptOnCvpCal( vector<int>* pRstVec)
{
	g_debugPlayer = m_pTargetPlayer;

	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnCvpCal" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "脚本未定义CVP战斗计算函数:OnCvpCal\n" );
		return false;
	}
	//调用检测函数；
	if ( 0 != lua_pcall( m_pLuaState, 0, 3, 0 ) ) //检测函数调用失败;
	{
		D_ERROR( "执行CVP战斗判断函数失败1\n" );
		return false;
	}

	pRstVec->clear();
	for ( int i=0; i<3; ++i )
	{
		if ( !lua_isnumber( m_pLuaState, -1-i ) )
		{
			D_ERROR( "战斗计算脚本返回值个数错误，实际返回值只有%d个\n", i );
			return false;
		}
		pRstVec->push_back( (int)(lua_tonumber(m_pLuaState, -1-i)) );
	}

	g_debugPlayer = NULL;

	//取调用结果；
	return true;
}


bool CFightCalScript::CallScriptOnPvpCal( vector<int>* pRstVec)
{
	g_debugPlayer = m_pAttackPlayer;

	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnPvpCal" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "脚本未定义PVP战斗计算函数:OnCPvpCal\n" );
		return false;
	}
	//调用检测函数；
	if ( 0 != lua_pcall( m_pLuaState, 0, 3, 0 ) ) //检测函数调用失败;
	{
		D_ERROR( "执行PVP战斗判断函数失败1\n" );
		return false;
	}

	pRstVec->clear();
	for ( int i=0; i<3; ++i )
	{
		if ( !lua_isnumber( m_pLuaState, -1-i ) )
		{
			D_ERROR( "战斗计算脚本返回值个数错误，实际返回值只有%d个\n", i );
			return false;
		}
		pRstVec->push_back( (int)(lua_tonumber(m_pLuaState, -1-i)) );
	}

	g_debugPlayer = NULL;

	//取调用结果；
	return true;	
}


bool CFightCalScript::CallScriptOnCvcCal( vector<int>* pRstVec)
{
	CLuaRestoreStack rs( m_pLuaState );

	ACE_UNUSED_ARG( rs );//防止未使用参数警告；

	lua_getglobal( m_pLuaState, "OnCvcCal" );
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义战斗判断函数；
		D_ERROR( "脚本未定义CVC战斗计算函数:OnCvpCal\n" );
		return false;
	}
	//调用检测函数；
	if ( 0 != lua_pcall( m_pLuaState, 0, 3, 0 ) ) //检测函数调用失败;
	{
		D_ERROR( "执行CVC战斗判断函数失败1\n" );
		const char* error = lua_tostring( m_pLuaState, -1 );
		D_ERROR("具体错误 %s\n", error);
		return false; 
	}

	pRstVec->clear();
	for ( int i=0; i<3; ++i )
	{
		if ( !lua_isnumber( m_pLuaState, -1-i ) )
		{
			D_ERROR( "战斗计算脚本返回值个数错误，实际返回值只有%d个\n", i );
			return false;
		}
		pRstVec->push_back( (int)(lua_tonumber(m_pLuaState, -1-i)) );
	}

	//取调用结果；
	return true;
}


////释放管理的脚本；
//void  CFightCalScriptManager::Release()
//{
//	D_WARNING( "释放战斗判断脚本管理器成功\n" );
//	map<const char*, CFightCalScript*>::iterator tmpIter	= m_NameToScripts.begin();
//	for ( tmpIter=m_NameToScripts.begin(); tmpIter!=m_NameToScripts.end(); ++tmpIter )
//	{
//		delete tmpIter->second; tmpIter->second = NULL;
//	}
//	m_NameToScripts.clear();
//	m_StateToScripts.clear();
//}
//
/////寻找对应名字的脚本；
//CFightCalScript* CFightCalScriptManager::FindScripts( const char* strName )
//{
//	D_WARNING( "取战斗判断脚本%s，当前管理的脚本数%d\n", strName, m_NameToScripts.size() );
//	if ( m_NameToScripts.size() > 0 )
//	{
//		D_WARNING( "当前管理的第一个战斗判断脚本为%s\n", m_NameToScripts.begin()->first );
//		D_WARNING( "取战斗判断脚本成功!\n" );
//		return m_NameToScripts.begin()->second;
//	} else {
//		return NULL;
//	}
//
//	//map<const char*, CFightPreScript*>::iterator tmpIter = m_NameToScripts.find( strName );
//	//if ( tmpIter == m_NameToScripts.end() )
//	//{
//	//	D_WARNING( "取战斗判断脚本失败!\n" );
//	//	return NULL;
//	//}
//	//D_WARNING( "取战斗判断脚本成功!\n" );
//	//return tmpIter->second;
//}
//
