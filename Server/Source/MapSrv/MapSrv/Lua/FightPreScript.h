﻿/**
* @file FightPreScript.h
* @brief 战斗判定脚本
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: FightPreScript.h
* 摘    要: 管理战斗判定脚本
* 作    者: dzj
* 完成日期: 2008.03.04
*
*/

#pragma once

#include "LuaScript.h"
#include "Utility.h"

extern const char* FPRE_SCRIPT_NAME;

enum FIGHTPRE_RST { FPRE_CHECK_OK = 0, FPRE_CHECK_FAILED = 1 };
class CPlayer;
class CMonster;
///战斗判定脚本；
class CFightPreScript : public CLuaScriptBase
{

	friend class CFightPreScriptManager;

private:
	CFightPreScript( const CFightPreScript& );  //屏蔽这两个操作；
	CFightPreScript& operator = ( const CFightPreScript& );//屏蔽这两个操作；

private:
	CFightPreScript() {};
	virtual ~CFightPreScript (void) {};

	bool Init( const char* luaScriptFile );

public:
	///玩家直接攻击怪物;
	FIGHTPRE_RST OnPvcDirectCheck( CPlayer* pPlayer/*attacker*/, CMonster* pTargetMonster );
	///玩家强制攻击怪物;
	FIGHTPRE_RST OnPvcForceCheck( CPlayer* pPlayer/*attacker*/, CMonster* pTargetMonster );
	///怪物直接攻击玩家;
	FIGHTPRE_RST OnCvpDirectCheck( CMonster* pAttackMonster/*attacker*/, CPlayer* pPlayer );
	///玩家直接攻击玩家;
	FIGHTPRE_RST OnPvpDirectCheck( CPlayer* pPlayer/*attacker*/, CPlayer* pTargetPlayer );

public:
	///脚本扩展函数调用，返回主动攻击的玩家，如果主动攻击者确实为玩家的话；
	CPlayer* GetAttackerPlayer() { return m_pAttackPlayer; }
	///脚本扩展函数调用，返回主动攻击的怪物，如果主动攻击者确实为怪物的话；
	CMonster* GetAttackerMonster() { return m_pAttackMonster; }
	///脚本扩展函数调用，返回作为攻击目标的玩家，如果被攻击者确实为玩家的话；
	CPlayer* GetTargetPlayer() { return m_pTargetPlayer; }
	///脚本扩展函数调用，返回作为攻击目标的怪物，如果被攻击者确实为怪物的话；
	CMonster* GetTargetMonster() { return m_pTargetMonster; }

	

private:
	///重置参与者，由于主逻辑为单线程，因此各次战斗判断调用顺序进行，每次新的战斗判断都清之前的战斗判断参与者；
	void ResetPaticipant()
	{
		m_pAttackPlayer = NULL;
		m_pAttackMonster = NULL;
		m_pTargetPlayer = NULL;
		m_pTargetMonster = NULL;
	}

	///调用脚本PVC战斗判断函数，玩家直接攻击怪物；
	FIGHTPRE_RST CallScriptOnPvcDirectCheck();
	///调用脚本PVC战斗判断函数，玩家强制攻击怪物；
	FIGHTPRE_RST CallScriptOnPvcForceCheck();
	///调用脚本PVC战斗判断函数，怪物直接攻击玩家；
	FIGHTPRE_RST CallScriptOnCvpDirectCheck();
	///调用脚本PVC战斗判断函数，玩家直接攻击玩家；
	FIGHTPRE_RST CallScriptOnPvpDirectCheck();

private:
	CPlayer*  m_pAttackPlayer; 
	CMonster* m_pAttackMonster;
	CPlayer*  m_pTargetPlayer; 
	CMonster* m_pTargetMonster; 
};

class CFightPreScriptManager
{
private:
	CFightPreScriptManager();//屏蔽此构造;
	~CFightPreScriptManager();//屏蔽此析构;
public:
	///从配置中读入所有NPC脚本;
	static bool Init()
	{
		CFightPreScript* pScript = NEW CFightPreScript;
		if ( pScript->Init( "config/scripts/fightprechk.lua" ) ) //FPRE_SCRIPT_NAME
		{
			m_pFightPreScript = pScript;
			//AddScripts( FPRE_SCRIPT_NAME, pScript );
		} else {
			m_pFightPreScript = NULL;
			return false;
		}
		return true;
	}

	//释放管理的脚本；
	static void Release()
	{
		if ( NULL != m_pFightPreScript )
		{
			delete m_pFightPreScript; m_pFightPreScript = NULL;
		}
		return;
	}

	//static void AddScripts( const char* strName, CFightPreScript* pScript )
	//{
	//	m_NameToScripts.insert( pair<const char*, CFightPreScript*>( strName, pScript ) );
	//	m_StateToScripts.insert( pair<lua_State*, CFightPreScript*>( pScript->GetState(), pScript ) );
	//}

	///寻找对应名字的脚本，用于CNpc；
	static CFightPreScript* FindScripts( const char* strName )
	{
		ACE_UNUSED_ARG( strName );
		return m_pFightPreScript;
	};

	///寻找对应luastate的脚本，用于luaCallBack；
	static CFightPreScript* FindScripts( lua_State* pState )
	{
		ACE_UNUSED_ARG( pState );
		return m_pFightPreScript;
		//map<lua_State*, CFightPreScript*>::iterator tmpIter = m_StateToScripts.find( pState );
		//if ( tmpIter == m_StateToScripts.end() )
		//{
		//	return NULL;
		//}
		//return tmpIter->second;
	}

private:
	static CFightPreScript* m_pFightPreScript;
	//static map<const char*, CFightPreScript*> m_NameToScripts;
	//static map<lua_State*, CFightPreScript*>  m_StateToScripts;
};
