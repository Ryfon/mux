﻿// ---------------------------------------------------------------------------
// FILE NAME            : LuaScript.h
// ---------------------------------------------------------------------------
// DESCRIPTION :
//
// Scripting base class
// 
// ---------------------------------------------------------------------------
// VERSION              : 1.00
// DATE                 : 1-Sep-2005
// AUTHOR               : Richard Shephard
// ---------------------------------------------------------------------------
// LIBRARY INCLUDE FILES

#ifndef __LUA_SCRIPT_BASE_H__
#define __LUA_SCRIPT_BASE_H__

#include "include/lua.hpp"
#include <map>
using namespace std;

#include "../../../Base/Utility.h"

///脚本类基类；
class CLuaScriptBase
{
private:
	CLuaScriptBase( const CLuaScriptBase& );  //屏蔽这两个操作；
	CLuaScriptBase& operator = ( const CLuaScriptBase& );//屏蔽这两个操作；

public:
   CLuaScriptBase ();
   virtual ~CLuaScriptBase (void);

public:
	lua_State* GetState() { return m_pLuaState; }

protected:
   template< typename T_Owner >
   void SetOwner( T_Owner* pOwner )
   {
		//建一个owner表，用于存放调用本脚本的脚本使用者指针；
		lua_pushnumber( m_pLuaState, (unsigned long) pOwner );
		lua_setglobal( m_pLuaState, OWNER_TB_NAME );
   }

protected:
	/// Compile script into Virtual Machine
	///装入脚本并初始化该脚本可调用的扩展函数列表；
	template <typename T_Extent/*扩展接口提供类*/>
	bool LoadFrom( const char *strFilename )
	{
		if ( NULL == strFilename )
		{
			return false;
		}
		T_Extent::Init( m_pLuaState );//将相关函数注册到新的lua_State;

		bool fSuccess = false;
		int iErr = luaL_loadfile(m_pLuaState, strFilename);

		if (iErr == 0)
		{
			// Call main...
			iErr = lua_pcall(m_pLuaState, 0, LUA_MULTRET, 0);
			if (iErr == 0)
			{
				fSuccess = true;
			}else if( iErr == LUA_ERRRUN )	{
				const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
				D_ERROR( "脚本%s语法错误%s\n", strFilename, szErrMessage );
			}
		} else {
			if ( iErr == LUA_ERRSYNTAX )
			{
				//语法错误；
				const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
				D_WARNING( "脚本%s语法错误%s\n", strFilename, szErrMessage );
			}else if ( iErr == LUA_ERRMEM ) {
				//内存相关错误;
				D_WARNING( "装载脚本%s时，发生内存错误\n", strFilename );
			} else if ( iErr == 6 ) {
				D_WARNING( "装载脚本%s时，找不到指定文件\n", strFilename );
			} else {
				//其它错误；
				D_WARNING( "装载脚本%s时，发生未知错误\n", strFilename );
			}
		}
		return fSuccess;
	}

protected:
   lua_State* m_pLuaState;
   int m_nArgs;
};

#endif // __LUA_SCRIPT_BASE_H__
