﻿/**
* @file NpcChatScript.h
* @brief 定义脚本描述的NPC对话
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: NpcChatScript.h
* 摘    要: 定义脚本描述的NPC对话
* 作    者: dzj
* 完成日期: 2007.04.14
*
*/

#include "NpcChatScript.h"


#include "../Lua/ScriptExtent.h"

//#include "../monster/manmonstercreator.h"
#include "../monster/monster.h"

#include "../GMCmdManager.h"

#include "../npcshop/npcshop.h"
#include "../Activity/PunishmentManager.h"
#include "../Activity/WarTaskManager.h"
#include "../Union/UnionManager.h"
#include "../AntiAddictionManager.h"
#include "../monster/manmonstercreator.h"
#include "../MuxMap/muxmapbase.h"
#include "../MuxMap/manmuxcopy.h"
#include "../RaceManager.h"
#include "../MuxMap/mannormalmap.h"
#include "../Item/LoadDropItemXML.h"

map<unsigned long, CNormalScript*> CNormalScriptManager::m_mapDwordFsms;
map<lua_State*, CNormalScript*> CNormalScriptManager::m_mapLuaStateFsms;

//09.04.28,取消工会任务新实现,map<unsigned long, CNormalScript*> CNormalScriptManager::m_mapDwordTaskFsms;//脚本ID号<---->脚本指针映射;
//map<lua_State*, CNormalScript*> CNormalScriptManager::m_mapLuaStateTaskFsms;//lua状态<---->脚本指针映射;

///从文件中装入脚本；
bool CNormalScript::InitNormalScript( const char* luaScriptFile )
{
	m_scriptType = SCRIPT_INVALIDTYPE;//初始没有类型，loadscript者知道真正的类型；
	m_ID = 0;//无效的脚本ID号；
	m_pScriptUser = NULL;//初始无玩家与本NPC对话；
	m_strUiInfo.str("");//清对话界面信息；

	if ( NULL == luaScriptFile )
	{
		return false;
	}
	if ( ! LoadFrom<CNormalScriptExtent>( luaScriptFile ) )
	{
		return false;
	}

	return ReadScriptID();
}

/////计数器条件满足时，调用计数器脚本;
/////   只有本脚本的确为计数器脚本时才有效；
/////   目前固定调用start stage中的OnMeetCounter，以后也可以考虑在道具上绑定与对话一样复杂的脚本，如果那样，则必须对计数器脚本也增加nstage;
//bool CNormalScript::OnMeetCounter( CPlayer* pOwner )
//{
//	//1、如果定义了OnMeetCounter函数，则执行之，否则直接返回；
//	if ( NULL == pOwner )
//	{
//		return false;
//	}
//
//	m_pScriptUser = pOwner;//保存当前与本NPC对话的玩家；
//
//	int nStage = 0;
//	if ( !GetStartStage( nStage ) ) 
//	{
//		return false;
//	}
//	pOwner->SetChatStage( nStage );
//	bool isok = CallScriptOnMeetCounter( pOwner );//回调脚本中的计数器对应函数；
//	if ( !isok ) 
//	{
//		return false;
//	}
//
//	return isok;
//}

///NPC死亡事件调用脚本(只适用于boss级怪物，只有一个阶段，交互对象为杀怪物者)；
bool CNormalScript::OnNpcDie( CPlayer* pKiller )
{
	TRY_BEGIN;

	//3、找到脚本中起始stage的OnEnter函数执行之
	if ( SCRIPT_NPC != m_scriptType )
	{
		D_WARNING( "非NPC脚本%d被作为死亡脚本调用，实际类型%d\n", GetID(), m_scriptType );
		return false;
	}

	if ( NULL == pKiller )
	{
		return false;
	}

	m_pScriptUser = pKiller;//保存脚本交互玩家；

	m_strUiInfo.str("");//清对话界面信息；

	//初始进入;
	int nStage = 0;
	if ( !GetStartStage( nStage ) ) //取初始stage;
	{
		D_WARNING( "NPC死亡脚本%d，取不到初始stage\n", GetID() );
		return false;
	}
	m_pScriptUser->SetNpcChatStage( nStage );

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		D_WARNING( "NPC死亡脚本%d，states非表\n", GetID() );
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		D_WARNING( "NPC死亡脚本%d，states[startstage]非表\n", GetID() );
		return false;
	}

	//当前-1为states[nStage]，找到其OnEnter函数并执行之；
	lua_pushstring( m_pLuaState, "OnEnter" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnEnter"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnEnter；
		D_WARNING( "NPC死亡脚本%d，没有OnEnter函数\n", GetID() );
		return false;
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnEnter;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

///玩家使用道具时，调用道具脚本；
///   只有本脚本的确为道具脚本时才有效；
///   目前固定调用start stage中的OnEnter，以后也可以考虑在道具上绑定与对话一样复杂的脚本，如果那样，则必须对道具脚本也增加nstage;
bool CNormalScript::OnItemChat( CPlayer* pOwner, int nOption )
{
	if ( SCRIPT_ITEM != m_scriptType )
	{
		D_WARNING( "非Item脚本%d被作为Item脚本调用，实际类型%d\n", GetID(), m_scriptType );
		return false;
	}

	//1、如果定义了OnEnter函数，则执行之，否则直接返回；
	if ( NULL == pOwner )
	{
		return false;
	}

	m_pScriptUser = pOwner;//保存当前与本Item对话的玩家；

	m_strUiInfo.str("");//清对话界面信息；

	int nStage = pOwner->GetItemChatStage();
	if ( nStage == 0 )
	{
		//初始进入;
		if ( !GetStartStage( nStage ) )
		{
			return false;
		}
		pOwner->SetItemChatStage( nStage );
		bool isok = CallScriptOnUseItem( pOwner );//回调脚本进入函数；
		return isok;//只是初次点击，非选项；
	}

	return CallScriptOnOption( pOwner, nOption );
}

///选项选择, nOption==0表示初始进入脚本
bool CNormalScript::OnAreaChat(CPlayer* pOwner, int nOption )
{
	if ( SCRIPT_AREA != m_scriptType )
	{
		D_WARNING( "非Area脚本%d被作为Area脚本调用，实际类型%d\n", GetID(), m_scriptType );
		return false;
	}

	if ( NULL == pOwner )
	{
		return false;
	}

	m_pScriptUser = pOwner;//保存当前聊天的玩家信息

	m_strUiInfo.str("");//清对话界面信息；

	int nStage = pOwner->GetAreaChatStage();
	if( nStage == 0 )
	{
		//初始进入;
		if( !GetStartStage(nStage) )
		{
			return false;
		}
		pOwner->SetAreaChatStage( nStage );
		bool isok = CallScriptOnAreaChat( pOwner );//回调脚本进入函数；
		return isok;//只是初次点击，非选项；
	}
	return CallScriptOnOption( pOwner, nOption );
}

//09.04.28,取消工会任务新实现,///选项选择，nOption==0表示初始选择任各对话；
//bool CNormalScript::OnTaskChat( CPlayer* pOwner, int nOption )
//{
//	//1、取出当前stage
//	//2、如果pOwner为初始进入，则取出脚本中的start，将pOwner的stage设为start，
//	//    接着调用state[stage]中的OnEnter，如果本次为初始选择NPC，则函数至此返回;
//	//3、找到脚本中相应的state[stage]
//	//4、执行其OnOption函数;
//	if ( SCRIPT_TASK != m_scriptType )
//	{
//		D_WARNING( "非任务脚本%d被作为任务脚本调用，实际类型%d\n", GetID(), m_scriptType );
//		return false;
//	}
//
//	if ( NULL == pOwner )
//	{
//		return false;
//	}
//
//	m_pScriptUser = pOwner;//保存当前与本NPC对话的玩家；
//
//	m_strUiInfo.str("");//清对话界面信息；
//
//	int nStage = pOwner->GetTaskChatStage();
//	if ( nStage == 0 )
//	{
//		//初始进入;
//		if ( !GetStartStage( nStage ) )
//		{
//			return false;
//		}
//		pOwner->SetTaskChatStage( nStage );
//		bool isok = CallScriptOnTaskChat( pOwner );//回调脚本进入函数；
//		if ( !isok ) 
//		{
//			return false;
//		}
//		return isok;//只是初次点击，非选项；
//	}
//
//	return CallScriptOnOption( pOwner, nOption );
//}

///选项选择，nOption==0表示初始选择NPC；
bool CNormalScript::OnNpcChat( CPlayer* pOwner, int nOption )
{
	//1、取出当前stage
	//2、如果pOwner为初始进入，则取出脚本中的start，将pOwner的stage设为start，
	//    接着调用state[stage]中的OnEnter，如果本次为初始选择NPC，则函数至此返回;
	//3、找到脚本中相应的state[stage]
	//4、执行其OnOption函数;
	if ( SCRIPT_NPC != m_scriptType )
	{
		D_WARNING( "非NPC脚本%d被作为NPC脚本调用，实际类型%d\n", GetID(), m_scriptType );
		return false;
	}

	if ( NULL == pOwner )
	{
		return false;
	}

	m_pScriptUser = pOwner;//保存当前与本NPC对话的玩家；

	m_strUiInfo.str("");//清对话界面信息；

	int nStage = pOwner->GetNpcChatStage();
	if ( nStage == 0 )
	{
		//初始进入;
		if ( !GetStartStage( nStage ) )
		{
			return false;
		}
		pOwner->SetNpcChatStage( nStage );
		bool isok = CallScriptOnNpcChat( pOwner );//回调脚本进入函数；
		if ( !isok ) 
		{
			return false;
		}
		return isok;//只是初次点击，非选项；
	}

	return CallScriptOnOption( pOwner, nOption );
}

///NPC或Item向对话玩家发送对话界面信息；
void CNormalScript::ExtNpcSendUiInfo()
{
	if ( NULL != m_pScriptUser )
	{
		if ( SCRIPT_NPC == m_scriptType )
		{
			m_pScriptUser->SendNpcUiInfo( m_strUiInfo.str().c_str() );
		} else if ( SCRIPT_ITEM == m_scriptType ) {
			m_pScriptUser->SendItemUiInfo( m_strUiInfo.str().c_str() );
		} else if ( SCRIPT_AREA == m_scriptType ){
			m_pScriptUser->SendAreaUiInfo( m_strUiInfo.str().c_str() );
		//09.04.28,取消工会任务新实现,} else if ( SCRIPT_TASK == m_scriptType ){
		//	m_pScriptUser->SendTaskUiInfo( m_strUiInfo.str().c_str() );
		} else {
			//非法的脚本类型；
		}		
		m_strUiInfo.str("");
	}
}

///NPC发送关闭对话界面指令
void CNormalScript::ExtNpcCloseUiInfo()
{
	if ( NULL != m_pScriptUser )
	{
		m_strUiInfo.str("@*"); //关闭字符串
		ExtNpcSendUiInfo();
	}
}

///取对话对象玩家的昵称;
const char* CNormalScript::ExtGetPlayerName()
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->GetNickName();
	} else {
		return "";
	}
}


bool CNormalScript::ExtDisappearChatNpc( bool isDropItem )
{
	if( NULL == m_pScriptUser )
	{
		D_ERROR("CNormalScript::ExtDisappearChatNpc m_pScriptUser为空\n");
		return false;
	}

	CMonster* pMonster = m_pScriptUser->GetNpcChatTarget();
	if( NULL == pMonster )
	{
		D_ERROR("CNormalScript::ExtDisappearChatNpc pMonster为空\n");
		return false;
	}

	pMonster->OnMonsterDie( m_pScriptUser, GCMonsterDisappear::M_DIE, isDropItem );
	return true;
}

/*任务相关*/
///查询：调用者等级是否小于特定值；
bool CNormalScript::ExtQueryPlayerLevelLowerThan( unsigned int checkLevel )
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}

	return (m_pScriptUser->GetLevel() > checkLevel);
}
///查询：调用者等级是否大于特定值；
bool CNormalScript::ExtQueryPlayerLevelMoreThan( unsigned int checkLevel )
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}

	return (m_pScriptUser->GetLevel() > checkLevel);
}

///查询：调用者是否从未接过特定任务；
bool CNormalScript::ExtQueryNeverAcceptTask( unsigned int taskID )
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}

	//判断任务是否在玩家的身上,
	if ( m_pScriptUser->GetTaskStatus( taskID ) == NTS_NOACCEPT )
	{
		//如果不在玩家的身上,判断他是否曾经做过这个任务
		return !m_pScriptUser->IsHasDoneTask( taskID );
	}
	else
		return false;
}

///查询：指定任务在玩家的任务列表中；
bool CNormalScript::ExtQueryOnMissionList( unsigned int taskID )
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}
	return (NTS_NOACCEPT != m_pScriptUser->GetTaskStatus( taskID ));
}

///查询：指定任务在任务列表但未达成目标
bool CNormalScript::ExtQueryNotAchieveMissionTarget( unsigned int taskID )
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}
	return (NTS_TGT_NOT_ACHIEVE == m_pScriptUser->GetTaskStatus( taskID ));
}

///查询：指定任务在任务列表达成目标
bool CNormalScript::ExtQueryAchieveMissionTarget( unsigned int taskID )
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}
	return (NTS_TGT_ACHIEVE == m_pScriptUser->GetTaskStatus( taskID ));
}

///查询：完成过任务
bool CNormalScript::ExtQueryIsFinishMissionBefore( unsigned int taskID )
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}

	//该任务是否存过盘
	return m_pScriptUser->IsHasDoneTask( taskID );
}

///操作：给玩家任务
int CNormalScript::ExtGiveTask( unsigned int taskID )
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}

	m_pScriptUser->AddTask( taskID );

	return 0;
}

///操作：执行任务(将任务属性设置为已达成目标但未交还)
int CNormalScript::ExtProcessTask( unsigned int taskID )
{
	if ( NULL == m_pScriptUser )
	{
		return -1;
	}

	m_pScriptUser->SetTaskStat( taskID, NTS_TGT_ACHIEVE );

	return 0;
}

///操作：完成任务
int CNormalScript::ExtFinishTask( unsigned int taskID )
{
	if ( NULL == m_pScriptUser )
	{
		return -1;
	}

#ifdef ANTI_ADDICTION
	if( m_pScriptUser->IsTriedOrAddictionState() )
	{
		//疲劳沉迷状态不能还任务
		return -1;
	}
#endif	//ANTI_ADDICTION

	if ( m_pScriptUser->SetTaskStat( taskID, NTS_FINISH ) )
	{
		return 0;
	} else {
		return -1;
	}
}

int CNormalScript::ExtGetPlayerSex()
{
	if ( NULL == m_pScriptUser )
	{
		return -1;
	}
	return m_pScriptUser->GetSex();
}

///操作：给玩家加经验
int CNormalScript::ExtAddExp( unsigned int expToAdd )
{
	if ( NULL == m_pScriptUser )
	{
		return -1;
	}

	m_pScriptUser->AddExp( expToAdd, GCPlayerLevelUp::IN_TASK, false );
	return 0;
}

bool CNormalScript::ExtCanGiveMoney(unsigned int addMoney )
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}

	if( m_pScriptUser->GetSilverMoney() + addMoney > MAX_MONEY ) //双币种判断
	{
		D_WARNING( "CNormalScript::ExtCanGiveMoney，脚本%d，给钱%d后银币数量超过上限\n", GetID(), addMoney );
		return false;
	}

	return true;
}


///操作：给玩家加金钱
int CNormalScript::ExtAddMoney( unsigned int moneyToAdd )
{
	if ( NULL == m_pScriptUser )
	{
		return -1;
	}

	m_pScriptUser->AddSilverMoney( moneyToAdd );//双币种判断

	return 0;
}

///操作：给玩家加属性
int CNormalScript::ExtAddProperty( unsigned int typeToAdd, int valToAdd )
{
	if ( NULL == m_pScriptUser )
	{
		return -1;
	}

	CGMCmdManager::_ChangeAttribute( m_pScriptUser, CGMCmdManager::TYPE_ADD, (EAttribute)typeToAdd, valToAdd );

	return 0;
}

///操作：给玩家设属性
int CNormalScript::ExtSetProperty( unsigned int typeToSet, int valToSet )
{
	if ( NULL == m_pScriptUser )
	{
		return -1;
	}

	CGMCmdManager::_ChangeAttribute( m_pScriptUser, CGMCmdManager::TYPE_SET, (EAttribute)typeToSet, valToSet );

	return 0;
}

///操作：传玩家进副本
int CNormalScript::ExtTransPlayerToCopy( unsigned short copyMapID, unsigned short scrollType )
{
	if ( NULL == m_pScriptUser )
	{
		return -1;
	}

	D_DEBUG( "NPC指令，真副本尝试，玩家%s，副本类型%d，副本券%d\n", m_pScriptUser->GetAccount(), copyMapID, scrollType );
	m_pScriptUser->TransPlayerToCopyQuest( copyMapID, scrollType );

	return 0;
}

///操作：传玩家进伪副本
int CNormalScript::ExtTransPlayerToPCopy( unsigned short copyMapID, unsigned short scrollType )
{
	if ( NULL == m_pScriptUser )
	{
		return -1;
	}

	D_DEBUG( "NPC指令，伪副本尝试，玩家%s，副本类型%d，副本券%d\n", m_pScriptUser->GetAccount(), copyMapID, scrollType );
	m_pScriptUser->TransPlayerToPCopyQuest( copyMapID, scrollType );

	return 0;
}

///NPC给玩家身上加计数器；
int CNormalScript::ExtAddPlayerCounter( unsigned int counterIdType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount, unsigned int taskID )
{ 
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->AddCounter( counterIdType, counterIdMin, counterIdMax, targetCount, taskID );
	}
	return 0; 
};

///NPC给玩家道具；
int CNormalScript::ExtGiveItem( unsigned int itemTypeID, unsigned int itemNum ) 
{ 
	if ( NULL != m_pScriptUser )
	{
		return (m_pScriptUser->OnNpcGiveItem( itemTypeID, itemNum ));
	}
	return 0;
};

int CNormalScript::ExtGiveItemRondomLevel(unsigned int itemTypeID, unsigned int itemNum, unsigned int levelBegin, unsigned int levelEnd )
{
	if ( NULL != m_pScriptUser )
	{
		if ( levelBegin > levelEnd )
			return 0;

		return m_pScriptUser->GetNormalItem( itemTypeID, itemNum, RandNumber( levelBegin, levelEnd ) );
	}
	return 0;
}

///NPC取走玩家道具；
int CNormalScript::ExtRemoveItem( unsigned int itemTypeID, unsigned int itemNum )
{
	if ( NULL != m_pScriptUser )
	{
		return (m_pScriptUser->OnNpcRemoveItem( itemTypeID, itemNum ) );
	}
	return 0;
}

int CNormalScript::ExtRemoveScriptItem()
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->OnNpcRemoveScriptItem();
	}
	return 0;
}


///NPC在特定位置内刷特定数量怪物；
int CNormalScript::ExtRefreshMonster( unsigned int monsterID, unsigned int monsterNum, unsigned short posX, unsigned short posY ) 
{
	D_ERROR( "未实现脚本接口：ExtRefreshMonster, %d:%d, (%d,%d)", monsterID, monsterNum, posX, posY );
	return 0;
};

///NPC指令玩家弹托盘；
int CNormalScript::ExtShowPlateCmd( int plateType )
{
	if ( NULL != m_pScriptUser )
	{
		m_pScriptUser->ShowPlateCmd( (EPlateType)plateType );
	}
	return 0;
}

///NPC指令探出玩家邮件界面
int CNormalScript::ExtShowEmailCmd()
{
	if ( NULL != m_pScriptUser )
	{
		m_pScriptUser->ShowMailBox();
	}
	return 0;
}

///NPC删去跟随玩家的NPC；
bool CNormalScript::ExtDelPlayerNpc( unsigned int npcType )
{
	if ( NULL != m_pScriptUser )
	{
		m_pScriptUser->DelFollowNpc( npcType );
		return true;
	} else {
		return false;
	}
}


bool CNormalScript::ExtNpcQueryPropertyEqual( unsigned int type, int value)
{
	if( NULL != m_pScriptUser )
	{
		return CGMCmdManager::NpcQueryPropertyEqual( m_pScriptUser, (EAttribute)type, value );
	}
	return false;
}


bool CNormalScript::ExtNpcQueryPropertyLowerThan( unsigned int type, int checkValue )
{
	if( NULL != m_pScriptUser )
	{
		return CGMCmdManager::NpcQueryPropertyLowerThan( m_pScriptUser, (EAttribute)type, checkValue );
	}
	return false;
}


bool CNormalScript::ExtNpcQueryPropertyMoreThan( unsigned int type, int checkValue )
{
	if( NULL != m_pScriptUser)
	{
		return CGMCmdManager::NpcQueryPropertyMoreThan( m_pScriptUser, (EAttribute)type, checkValue );
	}
	return false;
}

bool CNormalScript::ExtNpcShowProtectItemPlate(unsigned int type )
{
	if ( NULL!= m_pScriptUser )
	{
		m_pScriptUser->GetProtectItemPlate().ShowProtectItemPlate(  ( PROTECTITEM_PLATE_TYPE )type );
		return true;
	}
	return false;
}

///显示ＮＰＣ商店；
bool CNormalScript::ExtNpcShowShop( unsigned int shopID )
{
	if ( NULL != m_pScriptUser )
	{
		m_pScriptUser->CachedRepurchase().Clear();//每次打开时清除回购信息
		return CNpcShopManager::ShowNpcShop( m_pScriptUser, shopID, 0/*显示第一页*/ );
	}
	return false;
}

///玩家是否在骑乘状态
bool CNormalScript::ExtIsOnRideState()
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->IsRideState();
	}
	return false;
}

bool CNormalScript::ExtSendMail(const char* pMailTitle, const char* pMailContent, unsigned int money, vector<unsigned int>& vecItemTypes, vector<int>& vecItemCounts )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CNormalScript::ExtSendMail, NULL == m_pScriptUser\n" );
		return false;
	}

	if ( (NULL == pMailTitle) 
		|| (NULL == pMailContent) 
		)
	{
		D_ERROR( "CNormalScript::ExtSendMail，欲发送邮件的标题或内容空\n" );
		return false;
	}

	if ( vecItemTypes.size() != vecItemCounts.size() )
	{
		D_ERROR( "CNormalScript::ExtSendMail，欲发送邮件附件物品类型与数量数组大小不一致(%d:%d)\n", vecItemTypes.size(), vecItemCounts.size() );
		return false;
	}

	MGSendSysMailToPlayer sendSysMail;
	StructMemSet( sendSysMail, 0x0, sizeof(sendSysMail) );
	SafeStrCpy( sendSysMail.newSysMail.recvPlayerName, m_pScriptUser->GetNickName() );
	SafeStrCpy( sendSysMail.newSysMail.sendMailContent, pMailContent );
	int contentlen = sizeof(sendSysMail.newSysMail.sendMailContent) - 1;
	sendSysMail.newSysMail.sendMailContent[contentlen] = '\0';
	SafeStrCpy( sendSysMail.newSysMail.sendMailTitle,   pMailTitle );
	int titlelen = sizeof(sendSysMail.newSysMail.sendMailTitle) -1;
	sendSysMail.newSysMail.sendMailTitle[titlelen] = '\0';
	sendSysMail.newSysMail.silverMoney = money;//双币种判断
	sendSysMail.newSysMail.goldMoney = 0;

	unsigned int maxitemCount = MAX_MAIL_ATTACHITEM_COUNT;
	if ( money != 0 )
	{
		maxitemCount = MAX_MAIL_ATTACHITEM_COUNT - 1;
	}

	unsigned int itemIndex = 0 ;
	vector<int>::iterator iter1;
	vector<unsigned int>::iterator iter2;
	for ( iter1=vecItemCounts.begin(), iter2=vecItemTypes.begin(); 
		  iter1!=vecItemCounts.end(), iter2!=vecItemTypes.end(); ++iter1, ++iter2 )
	{
		if ( itemIndex >= ARRAY_SIZE(sendSysMail.newSysMail.attachItem) )
		{
			D_ERROR( "CNormalScript::ExtSendMail, itemIndex(%d)越界，item数组大小%d\n", itemIndex, vecItemCounts.size() );
			break;
		}
		sendSysMail.newSysMail.attachItem[itemIndex].usItemTypeID = *iter2;
		sendSysMail.newSysMail.attachItem[itemIndex].ucCount = *iter1;
		++itemIndex;
	}
	//for ( unsigned  int  i = 0 ; i < 12 && itemIndex < maxitemCount ; i+=2 )
	//{
	//	if ( ( i+1 >= ARRAY_SIZE(itemArr) )
	//		|| ( itemIndex >= ARRAY_SIZE(sendSysMail.newSysMail.attachItem) )
	//		)
	//	{
	//		D_ERROR( "CNormalScript::ExtSendMail, i(%d)或itemIndex(%d)越界\n", i, itemIndex );
	//		break;
	//	}

	//	if ( itemArr[i] == 0 || itemArr[i+1] == 0 )
	//	{
	//		break;
	//	}

	//	sendSysMail.newSysMail.attachItem[itemIndex].usItemTypeID = itemArr[i];
	//	sendSysMail.newSysMail.attachItem[itemIndex].ucCount = itemArr[i+1];
	//	itemIndex++;
	//}

	m_pScriptUser->SendPkg<MGSendSysMailToPlayer>( &sendSysMail );
	return true;
}

bool CNormalScript::ExtShowStorage()
{
	if ( !m_pScriptUser )
		return false;

	return m_pScriptUser->GetPlayerStorage().ShowStoragePlate();
}

bool CNormalScript::ExtBeginOLTest( unsigned int taskID )
{
	if ( !m_pScriptUser )
		return false;

	m_pScriptUser->GetPlayerOLTestPlate().ShowOLTestPlate( taskID , true );
	return true;
}

bool CNormalScript::ExtCanGetNpcFollowTask()
{
	if ( m_pScriptUser )
		return m_pScriptUser->GetFollowingMonster()==NULL;

	return false;
}

//09.04.28,取消工会任务新实现,///工会已发布任务
//bool CNormalScript::ExtDisplayUnionIssuedTask(void)
//{
//	if ( NULL == m_pScriptUser )
//	{
//		return false;
//	}
//
//	m_pScriptUser->SendUnionTaskInfo();
//	return true;
//}

bool CNormalScript::ExtSelectUnionCmd(unsigned int cmdID)
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}

	m_pScriptUser->SelectUnionCmd(cmdID);
	return true;
}

bool CNormalScript::ExtHaveIssuedUnionTask()
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}

	CUnion * pUnion = m_pScriptUser->Union();
	if(NULL == pUnion)
		return false;

	return pUnion->CheckTaskHaveIssue();
}

bool CNormalScript::ExtIsUnionLeader()
{
	if ( NULL == m_pScriptUser )
	{
		return false;
	}

	return m_pScriptUser->IsUnionLeader();
}

int CNormalScript::ExtQueryUnionLevel()
{
	if ( NULL == m_pScriptUser )
	{
		return 0;
	}

	CUnion * pUnion = m_pScriptUser->Union();
	if(NULL == pUnion)
		return 0;

	return pUnion->GetLevel();
}

void CNormalScript::ExtNpcChatSysDialog( const char* showMsg )
{
	if ( NULL == m_pScriptUser )
		return;

	if ( NULL == showMsg )
		return;

	MGChat chat;
	StructMemSet( chat, 0x00, sizeof(chat));

	chat.chatType = CT_SCRIPT_MSG;
	chat.extInfo = 0;//无额外信息；
	SafeStrCpy(chat.strChat, showMsg);

	m_pScriptUser->SendPkg<MGChat>(&chat);
	return;
}

bool CNormalScript::ExtNpcSwitchRaceMap(unsigned int mapid, unsigned short x, unsigned short y )
{
	if ( NULL == m_pScriptUser )
		return false;

	return true;
}


bool CNormalScript::ExtNpcIsEvil()
{
	if( NULL != m_pScriptUser )
	{
		return ( m_pScriptUser->GetGoodEvilPoint() < -5 );
	}
	return false;
}

//返回一个随即值
int CNormalScript::ExtNpcRandNumber( int a, int b)
{
	return RandNumber( a, b );
}


//增加种族声望
bool CNormalScript::ExtAddRacePrestige( unsigned int addVal )
{
	if( NULL != m_pScriptUser )
	{
		m_pScriptUser->AddRacePrestige( addVal );
		return true;
	}
	return false;
}


bool CNormalScript::ExtNpcCreateBuff( unsigned int buffId, unsigned int buffTime )
{
	CBuffProp* pBuffProp = CManBuffProp::FindObj( buffId );
	if( NULL == pBuffProp )
		return false;

	PlayerID createID;
	createID.wGID = 0;
	createID.dwPID = 0;

	if( NULL == m_pScriptUser )
	{
		return false;
	}

	//return m_pScriptUser->CreateBuff( pBuffProp, buffTime, createID, CREATE_NULL ); ;
	return true;
}

bool CNormalScript::ExtGetPlayerPosInfo(unsigned& mapid, unsigned& posX, unsigned& posY )
{
	if( NULL != m_pScriptUser )
	{
		mapid = (unsigned)m_pScriptUser->GetMapID();
		posX  = (unsigned)m_pScriptUser->GetPosX();
		posY  = (unsigned)m_pScriptUser->GetPosY();
		return true;
	}
	return false;
}

bool CNormalScript::ExtPlayerCallMonster( unsigned int num, unsigned int range )
{
	if ( NULL != m_pScriptUser )
	{
		m_pScriptUser->CallMonster(num,range);
		return true;
	}
	return false;
}

bool CNormalScript::ExtPlayerRandMove()
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->RandomMove();
	}
	return false;
}

bool CNormalScript::ExtShowAuctionPlate()
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->ShowAuctionPlate();
	}
	return false;
}

#ifdef OPEN_PUNISHMENT
bool CNormalScript::ExtNpcShowPunishPlayerInfo()
{
	if( NULL != m_pScriptUser )
	{
		CPunishmentManager::NotifyQueryPunishPlayerInfo( m_pScriptUser );
		return true;
	}
	return false;
}


//给予天谴任务
bool CNormalScript::ExtGivePunishTask()
{
	if( NULL != m_pScriptUser )
	{
		return m_pScriptUser->GivePunishTask();
	}
	return false;
}

//是否在天谴活动中,接天谴任务的先置条件
bool CNormalScript::ExtIsInPunishment()
{
	return (CPunishmentManager::GetPunishState() != 0 );
}



//是否接过天谴任务
bool CNormalScript::ExtIsHavePunishTask()
{
	if( NULL != m_pScriptUser )
	{
		return m_pScriptUser->IsHavePunishTask();
	}
	return false;
}


//天谴任务是否完成
bool CNormalScript::IsPunishTaskAchieve()
{
	if( NULL != m_pScriptUser )
	{
		return ( m_pScriptUser->GetKillCounter() > 0);
	}
	return false;
}


//获取天谴杀人计数器
int CNormalScript::GetPunishCounter()
{
	if( NULL != m_pScriptUser )
	{
		return m_pScriptUser->GetKillCounter();
	}
	return 0;
}


//获取天谴的目标对象
void CNormalScript::GetPunishTargetName( string& targetName )
{
	if( NULL != m_pScriptUser )
	{
		CPunishmentManager::GetPunishTargetName( m_pScriptUser, targetName );
	}
}


//结束天谴任务
int CNormalScript::ExtFinishPunishTask()
{
	if( NULL != m_pScriptUser )
	{
#ifdef ANTI_ADDICTION
		if( m_pScriptUser->IsTriedOrAddictionState() )
		{
			//疲劳沉迷状态不能还任务
			return 0;
		}
#endif	//ANTI_ADDICTION
		return m_pScriptUser->FinishPunishTask();
	}
	return 0;
}
#endif	/* OPEN_PUNISHMENT*/

#ifdef NEW_NPC_INTERFACE
//1 判断是否加入了战盟
bool CNormalScript::ExtNpcIsInUnion()
{
	if( NULL != m_pScriptUser )
	{
		return ( NULL != m_pScriptUser->Union() );
	}
	return 0;
}


bool CNormalScript::ExtNpcIsUnionLeader()
{
	if( NULL != m_pScriptUser )
	{
		return m_pScriptUser->IsUnionLeader();
	}
	return false;
}


//3 判断是否为组队状态
bool CNormalScript::ExtNpcIsInTeam()
{
	if( NULL != m_pScriptUser )
	{
		return m_pScriptUser->IsInTeam();
	}
	return 0;
}


//4 判断是否为队长
bool CNormalScript::ExtNpcIsTeamCaptain()
{
	if( NULL != m_pScriptUser )
	{
		return m_pScriptUser->IsTeamCaptain();
	}
	return 0;
}

//5 判断小队有几人

bool CNormalScript::ExtNpcTeamNumberMoreThan( unsigned int number )
{
	if( NULL != m_pScriptUser )
	{
		CGroupTeam* pTeam = m_pScriptUser->GetGroupTeam();
		if( NULL != pTeam )
		{
			unsigned int teamNumber = (unsigned int)(pTeam->GetTeamMemberVec().size());
			return ( teamNumber > number );
		}
	}
	return false;
}

bool CNormalScript::ExpNpcTeamNumberLowerThan( unsigned int number )
{
	if( NULL != m_pScriptUser )
	{
		CGroupTeam* pTeam = m_pScriptUser->GetGroupTeam();
		if( NULL != pTeam )
		{
			unsigned int teamNumber = (unsigned int)(pTeam->GetTeamMemberVec().size());
			return ( teamNumber < number );
		}
	}
	return false;
}

bool CNormalScript::ExtShowRaceMasterPlate()
{
	if( NULL != m_pScriptUser )
	{
		if( m_pScriptUser->IsRaceMaster() )
		{
			AllRaceMasterManagerSingle::instance()->ShowRaceMasterPlate( m_pScriptUser->GetRace() );
			return true;
		}
		else
		{
			ExtNpcChatSysDialog("你不是族长，操作失败!");
		}
	}
	return false;
}

//查询某伪副本是否可进
bool CNormalScript::ExtIsPCopyOpen( unsigned short pcopymapid )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CNormalScript::ExtIsPCopyOpen, NULL == m_pScriptUser\n" );
		return false;
	}

	return CManMuxCopy::IsPCopyOpen( pcopymapid );
}

bool CNormalScript::ExtCheckMapSwitchRace( long mapID )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CNormalScript::ExtCheckMapSwitchRace, NULL == m_pScriptUser\n" );
		return false;
	}

	CMuxMap *pMap = CManNormalMap::FindMap(mapID);
	if(NULL == pMap)
	{
		D_ERROR( "CNormalScript::ExtCheckMapSwitchRace, NULL == pMap\n" );
		return false;
	}

	//CityInfo *pCityInfo = pMap->GetCityInfo();
	//if(NULL == pCityInfo)
	//{
	//	D_ERROR( "CNormalScript::ExtCheckMapSwitchRace, NULL == pCityInfo\n" );
	//	return false;
	//}

	//if(m_pScriptUser->GetRace() != pCityInfo->ucRace)
	//{
	//	if(!CWarTaskManager::IsAttackWar())//攻城战未开启
	//	{
	//		D_DEBUG("攻城战还未开始，%s(Race:%d)无法进入不同种族(Race:%d)攻城战地图\n", m_pScriptUser->GetNickName(), m_pScriptUser->GetRace(), pCityInfo->ucRace );
	//		return false;
	//	}
	//	else
	//	{
	//		if( !AllRaceMasterManagerSingle::instance()->IsDecalreWar( m_pScriptUser->GetRace() , pCityInfo->ucRace ) )
	//		{
	//			D_DEBUG("本种族(Race:%d)未向目标地图种族(Race:%d)宣战，%s 无法进入\n",m_pScriptUser->GetRace(), pCityInfo->ucRace,m_pScriptUser->GetNickName() );
	//			return false;
	//		}
	//	}
	//}

	
	if ( !CWarTaskManager::IsAttackWar() )//攻城战未开始
	{
		CityInfo *pCityInfo = pMap->GetCityInfo();
		if(NULL == pCityInfo)
		{
			D_ERROR( "CNormalScript::ExtCheckMapSwitchRace, NULL == pCityInfo\n" );
			return false;
		}

		if(m_pScriptUser->GetRace() != pCityInfo->ucRace)
		{
			D_DEBUG("攻城战还未开始，%s(Race:%d)无法进入不同种族(Race:%d)攻城战地图\n", m_pScriptUser->GetNickName(), m_pScriptUser->GetRace(), pCityInfo->ucRace );
			return false;
		}
	}
	else
	{
		MapWarAdditionalInfo *pAdditonInfo = pMap->GetWarAdditionalInfo();//配了攻城战内城外城的关系
		if(NULL == pAdditonInfo)
		{
			D_ERROR( "CNormalScript::ExtCheckMapSwitchRace, NULL == pAdditonInfo\n" );
			return false;
		}

		if ( !pAdditonInfo->SwitchRaceEnabled( m_pScriptUser->GetRace() ) )
		{
			D_DEBUG("跳转玩家的种族(Race:%d)不在可跳转种族集合(Race:%d)内，%s 无法进入\n",m_pScriptUser->GetRace(), pAdditonInfo->warSwitchRace,m_pScriptUser->GetNickName() );
			return false;
		}
	}
		
	return true;
}

bool CNormalScript::ExtShowNotice(const char* content, char position, char isglobal, unsigned int maxshowcount, unsigned int distancetime )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CNormalScript::ExtShowNotice, NULL == m_pScriptUser\n" );
		return false;
	}
	
	if ( !content )
		return false;

	MGShowTmpNoticeByPosition tmpnotice;
	StructMemSet( tmpnotice, 0x0, sizeof(tmpnotice) );
	tmpnotice.isbroadcast = isglobal;
	tmpnotice.showtmpnotice.maxshowcount = maxshowcount;
	tmpnotice.showtmpnotice.distancetime = distancetime;
	tmpnotice.showtmpnotice.showposition = position;
	tmpnotice.showtmpnotice.contentlen = ACE_OS::snprintf( tmpnotice.showtmpnotice.content,
		sizeof(tmpnotice.showtmpnotice.content)-1,
		"%s",
		content);

	m_pScriptUser->SendPkg<MGShowTmpNoticeByPosition>( &tmpnotice );
	return true;
}


bool CNormalScript::ExtIsHaveSkill(unsigned int skillID )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CNormalScript::ExtIsHaveSkill, NULL == m_pScriptUser\n" );
		return false;
	}

	return m_pScriptUser->FindMuxSkill( skillID )!= NULL;
}

bool CNormalScript::ExtExecDropSetID(unsigned int dropsetID )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CNormalScript::ExtExecDropSetID, NULL == m_pScriptUser\n" );
		return false;
	}

	long money = 0;
	std::vector<CItemPkgEle> itemPkgVec;
	unsigned int dropItemArr[20] = {0};

	//运行对应的掉落集
	MonsterDropItemSetSingle::instance()->RunMonsterDropItemSet( m_pScriptUser, dropsetID, itemPkgVec, money );

	//如果有道具,
	if ( !itemPkgVec.empty() )
	{
		unsigned int itemIndex = 0;
		for( unsigned int i = 0 ; i<(unsigned int)itemPkgVec.size(); i++ )
		{
			if ( itemIndex+1 >= ARRAY_SIZE(dropItemArr) )
			{
				D_ERROR( "CNormalScript::ExtExecDropSetID, 越界, dropItemArr size %d, 索引位置%d, 玩家%s, 掉落集%d\n"
					, ARRAY_SIZE(dropItemArr), itemIndex+1, m_pScriptUser->GetAccount(), dropsetID );
				return false;
			}
			const ItemInfo_i& iteminfo = itemPkgVec[i].GetItemEleInfo(); 
			if ( iteminfo.usItemTypeID != 0 && iteminfo.ucCount != 0 )
			{
				dropItemArr[itemIndex]   = iteminfo.usItemTypeID;
				dropItemArr[itemIndex+1] = iteminfo.ucCount;
				itemIndex+=2;
			}
		}

		if ( itemIndex >0 )//如果本次随机了一些道具
		{
			if( !m_pScriptUser->NpcCanGiveItem( dropItemArr,itemIndex ) )//检查是否能放下随即的道具
			{
				m_pScriptUser->SendSystemChat("CNormalScript::ExtExecDropSetID, 包裹无法放下足够的道具");
				return false;
			}
			else//如果能够放下
			{
				for ( unsigned int i=0 ; i< itemIndex; i+=2 )
				{
					if ( i+1 >= ARRAY_SIZE(dropItemArr) )
					{
						D_ERROR( "CNormalScript::ExtExecDropSetID, i(%d)+1 >= ARRAY_SIZE(dropItemArr)，玩家%s, 掉落集%d\n"
							, i, m_pScriptUser->GetAccount(), dropsetID );
						return false;
					}
					unsigned int itemTypeID = dropItemArr[i];
					unsigned int itemCount  = dropItemArr[i+1];
					m_pScriptUser->OnNpcGiveItem( itemTypeID, itemCount);
				}
				
				//如果掉钱，增加玩家金钱
				if ( money>0 )
				{
					m_pScriptUser->AddSilverMoney( money );//双币种判断
				}
			}
		}
	}
	else//如果本次没有随机道具
	{
		//如果掉钱，增加玩家金钱
		if ( money>0 )
		{
			m_pScriptUser->AddSilverMoney( money );//双币种判断
		}
	}

	return true;
}

bool CNormalScript::ExtShowParamNotice(unsigned int msgid, char position, char isglobal,unsigned int maxshowcount, unsigned int distancetime,
									   unsigned int validparam,const char* param1,const char* param2, const char* param3,const char* param4,const char* param5 )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CNormalScript::ExtShowNotice, NULL == m_pScriptUser\n" );
		return false;
	}

	MGSendParamNotice paramnotice;
	StructMemSet( paramnotice, 0x0, sizeof(paramnotice) );
	paramnotice.parammsgnotice.msgid = msgid;
	paramnotice.parammsgnotice.showposition = position;
	paramnotice.parammsgnotice.validparamcount = validparam;
	paramnotice.parammsgnotice.distancetime = distancetime;
	paramnotice.parammsgnotice.maxshowcount = maxshowcount;
	paramnotice.isgloabl = isglobal;

	if ( validparam>0 && param1 )
	{
		paramnotice.parammsgnotice.param1len = ACE_OS::snprintf( paramnotice.parammsgnotice.param1,
			sizeof(paramnotice.parammsgnotice.param1)-1,
			"%s",
			param1 );
		
		if ( validparam>1 && param2 )
		{
			paramnotice.parammsgnotice.param2len = ACE_OS::snprintf( paramnotice.parammsgnotice.param2,
				sizeof(paramnotice.parammsgnotice.param2)-1,
				"%s",
				param2 );

			if ( validparam>2 && param3 )
			{
				paramnotice.parammsgnotice.param3len = ACE_OS::snprintf( paramnotice.parammsgnotice.param3,
					sizeof(paramnotice.parammsgnotice.param3)-1,
					"%s",
					param3 );
				
				if ( validparam>3 && param4 )
				{
					paramnotice.parammsgnotice.param4len = ACE_OS::snprintf( paramnotice.parammsgnotice.param4,
						sizeof(paramnotice.parammsgnotice.param4)-1,
						"%s",
						param4 );

					if ( validparam>4 && param5 )
					{
						paramnotice.parammsgnotice.param5len = ACE_OS::snprintf( paramnotice.parammsgnotice.param5,
							sizeof(paramnotice.parammsgnotice.param5)-1,
							"%s",
							param5 );
					}
				}
			}
		}
	}
	m_pScriptUser->SendPkg<MGSendParamNotice>( &paramnotice );
	return true;
}

bool CNormalScript::ExtNpcTeamNumberEqual( unsigned int number )
{
	if( NULL != m_pScriptUser )
	{
		CGroupTeam* pTeam = m_pScriptUser->GetGroupTeam();
		if( NULL != pTeam )
		{
			unsigned int teamNumber = (unsigned int)(pTeam->GetTeamMemberVec().size());
			return ( teamNumber == number );
		}
	}
	return false;
}



//6 判断好友有几人
unsigned int CNormalScript::ExtNpcGetFriendNumber()
{
	if( NULL != m_pScriptUser )
	{
		return m_pScriptUser->GetFriendNum();
	}
	return 0;
}


//7 判断仇人有几人
unsigned int CNormalScript::ExtNpcGetFoeNumber()
{
	if( NULL != m_pScriptUser )
	{
		return m_pScriptUser->GetEnemyNum();
	}
	return 0;
}
#endif //NEW_NPC_INTERFACE


#ifdef ANTI_ADDICTION
//是否在疲劳沉迷状态
bool CNormalScript::IsInTriedOrAddictionState()
{
	if( NULL != m_pScriptUser )
	{
		return m_pScriptUser->IsTriedOrAddictionState();
	}
	return false;
}
#endif	//ANTI_ADDICTION

bool CNormalScript::ExtNpcSaveRebirthPos(unsigned short leftOfRanage, unsigned short topOfRanage,unsigned short rightOfRanage, unsigned short bottomOfRanage)
{
	if( NULL != m_pScriptUser )
	{
		m_pScriptUser->SaveRebirthPt(leftOfRanage, topOfRanage, rightOfRanage, bottomOfRanage);
		return true;
	}
	return false;
}


bool CNormalScript::ExtNpcCreateNpcByChatTarget( unsigned int npcTypeId, unsigned int npcNum )
{
	if( NULL == m_pScriptUser )
	{
		return false;
	}

	CMuxMap* pMap = m_pScriptUser->GetCurMap();
	if( NULL == pMap )
		return false;
	
	unsigned short posX = m_pScriptUser->GetPosX();
	unsigned short posY = m_pScriptUser->GetPosY();
	CMonster* pMonsterCreated = NULL;
	for( unsigned int i = 0; i< npcNum; ++i )
	{
		CCreatorIns::SinglePtRefreshNoCreator( npcTypeId, pMap, posX, posY, 0,  pMonsterCreated, NULL );
	}
	return true;
}


//添加标记
bool CNormalScript::AddMark( unsigned int mark )
{
	if( NULL == m_pScriptUser )
	{
		return false;
	}

	m_pScriptUser->SetMark( mark );
	return true;
}


//检查标记
bool CNormalScript::CheckMark( unsigned int mark )
{
	if( NULL == m_pScriptUser )
	{
		return false;
	}

	return m_pScriptUser->CheckMark( mark );
}


bool CNormalScript::CheckExTime( unsigned int time )
{
	int month = time/100000;	//8位前2位为月
	int minute = time%100;		//最后2位分钟
	int hour = time/100; hour = hour % 100;   //3~4位为小时
	int day = time/10000; month = month %100;	//5~6位天数

	int curYear = 0, curMonth = 0, curDay = 0, curHour = 0, curMinute = 0, curSecond = 0, curWday =0, curMsec = 0;
	GetCurTime( curYear, curMonth, curDay, curHour, curMinute, curSecond, curWday, curMsec );

	if( month > curMonth )
		return true;

	if( month < curMonth )
		return false;

	if( day > curDay )
		return true;

	if( day < curDay )
		return false;

	if( hour > curHour )
		return true;

	if( hour < curHour )
		return false;

	if( minute > curMinute )
		return true;

	if( minute < curMinute )
		return false;

	return true;
}


bool CNormalScript::CheckTime( unsigned int time1, unsigned int time2 )
{
	int hour1 = time1/100, hour2 = time2/100;
	int minute1 = time1%100,  minute2 = time2%100;

	if( hour1 > hour2 )
		return false;

	if( hour1 == hour2 && minute1 > minute2 )
		return false;
	
	int curYear = 0, curMonth = 0, curDay = 0, curHour = 0, curMinute = 0, curSecond = 0, curWday =0, curMsec = 0;
	GetCurTime( curYear, curMonth, curDay, curHour, curMinute, curSecond, curWday, curMsec );
	
	bool bcheck1 = false;
	if ( hour1 < curHour )
	{
		bcheck1 = true;
	}else if( hour1 == curHour )
	{
		bcheck1 = ( curMinute >= minute1 );
	}

	if ( !bcheck1 )
		return false;

	bool bcheck2 =  false;
	if ( hour2 > curHour )
	{
		bcheck2 = true;
	}else if( hour2 == curHour )
	{
		bcheck2 = ( minute2 >= curMinute );
	}
	
	if ( !bcheck2 )
		return false;
		
	return true;
}


///NPC指令切换地图
bool CNormalScript::ExtNpcSwitchMap( unsigned int mapID, unsigned short posX, unsigned short posY )
{
	if ( NULL != m_pScriptUser )
	{
		//地图内跳转 
		if( m_pScriptUser->GetMapID() == mapID )
		{
			CMuxMap* pMap = m_pScriptUser->GetCurMap();
			if ( NULL == pMap )
			{
				m_pScriptUser->SendSystemChat( "GM命令执行错：当前不在地图内\n" );
				return false;
			}

			bool isMoveOk = pMap->OnPlayerInnerJump( m_pScriptUser, posX, posY );
			if ( !isMoveOk )
			{
				m_pScriptUser->SendSystemChat( "GM命令执行错：同地图跳转失败\n" );
				return false;
			} 
		}
		else//跳转到其他地图
		{
			m_pScriptUser->IssueSwitchMap( mapID, posX, posY );
		}
		return true;
	}
	return false;
}

///玩家学习宠物技能；
bool CNormalScript::ExtNpcPetSkillLearn( unsigned int petSkillID )
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->PetSkillLearn( petSkillID );
	}
	return false;
}

///玩家学习技能
bool CNormalScript::ExtNpcSkillLearn( unsigned int skillID )
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->LearnSkillByID( skillID );
	}
	return false;
}

///NPC检查是否有跟随玩家的NPC；
bool CNormalScript::ExtCheckPlayerNpc( unsigned int npcType )
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->CheckFollowNpc( npcType );
	}

	return false;
}

///NPC创建一个跟随玩家的NPC；
int CNormalScript::ExtCreatePlayerNpc( unsigned int npcType, unsigned int taskID )
{
	if ( NULL != m_pScriptUser )
	{
		m_pScriptUser->AddFollowingMonster( npcType, taskID );
		return 0;
	}

	return 0;
}

///查询：玩家包裹中是否有特定物品;
bool CNormalScript::ExtQueryPackageHave( unsigned int itemTypeID ,unsigned int itemNum )
{
	if ( NULL != m_pScriptUser )
	{
		unsigned int itemcount = m_pScriptUser->CounterItemByID( itemTypeID );
		return itemcount >= itemNum;
	}

	return false;
}

bool CNormalScript::ExtQueryPackageCanContain( unsigned int itemArr[], unsigned int ArrCount )
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->NpcCanGiveItem( itemArr,ArrCount );
	}

	return  false;
}

bool CNormalScript::ExtSendStandardUiInfo(unsigned int infoNo )
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->NoticeTaskUiInfo( infoNo );
	}
	return false;
}

///NPC检测玩家智能值，大于；
bool CNormalScript::ExtQueryIntelligenceMoreThan( unsigned int checkValue )
{
	if ( NULL != m_pScriptUser )
	{
		int intelValue = m_pScriptUser->GetScriptInt();
		return ( intelValue >= (int)checkValue );
	}

	return false;
}

///NPC检测玩家智能值，小于；
bool CNormalScript::ExtQueryIntelligenceLessThan( unsigned int checkValue )
{
	if ( NULL != m_pScriptUser )
	{
		int intelValue = m_pScriptUser->GetScriptInt();
		return ( intelValue < (int)checkValue );
	}

	return false;
}

///根据当前stage调用脚本OnOption；
bool CNormalScript::CallScriptOnOption( CPlayer* pOwner, int nOption )
{
	TRY_BEGIN;

	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 0;
	if ( SCRIPT_NPC == m_scriptType )
	{
		nStage = pOwner->GetNpcChatStage();
	} else if ( SCRIPT_ITEM == m_scriptType ) {
		nStage = pOwner->GetItemChatStage();
	} else if( SCRIPT_AREA == m_scriptType ) {
		nStage = pOwner->GetAreaChatStage();
	//09.04.28,取消工会任务新实现,} else if( SCRIPT_TASK == m_scriptType ) {
	//	nStage = pOwner->GetTaskChatStage();
	} else {
		//非法脚本
		return false;
	}

	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnOption函数并执行之；
	lua_pushstring( m_pLuaState, "OnOption" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, nOption );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

/////根据当前stage调用脚本OnMeetCounter;
//bool CNormalScript::CallScriptOnMeetCounter( CPlayer* pOwner )
//{
//	TRY_BEGIN;
//
//	CLuaRestoreStack rs( m_pLuaState );
//
//	//当前stage;
//	int nStage = pOwner->GetChatStage();
//	if ( nStage<=0 )
//	{
//		return false;//进到本函数时，不应该出现stage为0的情况；
//	}
//
//	//找到相应的stage;
//	lua_getglobal( m_pLuaState, "states" );
//	if ( ! lua_istable( m_pLuaState, -1 ) )
//	{
//		//非表，取到的内容错误;
//		return false;
//	}
//
//	//取states[nStage],即-2[nStage];
//	lua_pushnumber( m_pLuaState, nStage );
//	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
//	if ( ! lua_istable( m_pLuaState, -1 ) )
//	{
//		//非表，取到的内容错误；
//		return false;
//	}
//
//	//当前-1为states[nStage]，找到其OnMeetCounter函数并执行之；
//	lua_pushstring( m_pLuaState, "OnMeetCounter" );
//	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnEnter"]
//	if ( ! lua_isfunction( m_pLuaState, -1 ) )
//	{
//		//非函数，未定义OnMeetCounter；
//		return false;
//	}
//
//bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnMeetCounter;
//if ( !iscallok )
//{
//	const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
//	if ( NULL != szErrMessage )
//	{
//		D_ERROR( "LUA执行错：%s\n", szErrMessage );
//	} else {
//		D_ERROR( "LUA执行错，未知错误\n" );
//	}
//}
//return iscallok;
//	TRY_END;
//	return false;
//}


///根据当前stage调用脚本OnEnter;
bool CNormalScript::CallScriptOnUseItem( CPlayer* pOwner )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		return false;
	}
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = pOwner->GetItemChatStage();
	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnEnter函数并执行之；
	lua_pushstring( m_pLuaState, "OnEnter" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnEnter"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnEnter；
		return false;
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnEnter;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}


bool CNormalScript::CallScriptOnAreaChat(CPlayer* pOwner )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		return false;
	}

	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = pOwner->GetAreaChatStage();
	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnEnter函数并执行之；
	lua_pushstring( m_pLuaState, "OnEnter" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnEnter"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnEnter；
		return false;
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnEnter;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

//09.04.28,取消工会任务新实现,///根据当前stage调用脚本的OnEnter;
//bool CNormalScript::CallScriptOnTaskChat( CPlayer* pOwner )
//{
//	TRY_BEGIN;
//
//	if ( NULL == pOwner )
//	{
//		return false;
//	}
//
//	CLuaRestoreStack rs( m_pLuaState );
//
//	//当前stage;
//	int nStage = pOwner->GetTaskChatStage();
//	if ( nStage<=0 )
//	{
//		return false;//进到本函数时，不应该出现stage为0的情况；
//	}
//
//	//找到相应的stage;
//	lua_getglobal( m_pLuaState, "states" );
//	if ( ! lua_istable( m_pLuaState, -1 ) )
//	{
//		//非表，取到的内容错误;
//		return false;
//	}
//
//	//取states[nStage],即-2[nStage];
//	lua_pushnumber( m_pLuaState, nStage );
//	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
//	if ( ! lua_istable( m_pLuaState, -1 ) )
//	{
//		//非表，取到的内容错误；
//		return false;
//	}
//
//	//当前-1为states[nStage]，找到其OnEnter函数并执行之；
//	lua_pushstring( m_pLuaState, "OnEnter" );
//	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnEnter"]
//	if ( ! lua_isfunction( m_pLuaState, -1 ) )
//	{
//		//非函数，未定义OnEnter；
//		return false;
//	}
//
//	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnEnter;
//	if ( !iscallok )
//	{
//		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
//		if ( NULL != szErrMessage )
//		{
//			D_ERROR( "LUA执行错：%s\n", szErrMessage );
//		} else {
//			D_ERROR( "LUA执行错，未知错误\n" );
//		}
//	}
//
//	return iscallok;
//	TRY_END;
//	return false;
//}

///根据当前stage调用脚本OnEnter;
bool CNormalScript::CallScriptOnNpcChat( CPlayer* pOwner )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		return false;
	}

	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = pOwner->GetNpcChatStage();
	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnEnter函数并执行之；
	lua_pushstring( m_pLuaState, "OnEnter" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnEnter"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnEnter；
		return false;
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnEnter;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

///检测脚本当前stage的所有stage转换条件，如果需要转换，则执行转换；
bool CNormalScript::CallScriptCheckTransCon( CPlayer* pOwner )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		return false;
	}

	//依次找到各advance函数，只要某一函数返回真，则执行stage转换;
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 0;
	if ( SCRIPT_ITEM == m_scriptType )
	{
		nStage = pOwner->GetItemChatStage();
	} else if ( SCRIPT_NPC == m_scriptType ) {
		nStage = pOwner->GetNpcChatStage();
	//09.04.28,取消工会任务新实现,} else if ( SCRIPT_TASK == m_scriptType ) {
	//	nStage = pOwner->GetTaskChatStage();
	} else if( SCRIPT_AREA == m_scriptType ){
		nStage = pOwner->GetAreaChatStage();
	}

	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的states;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到links表；
	lua_pushstring( m_pLuaState, "links" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["links"]
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，未定义链接
		return false;
	}

	//依次遍历links表，执行检测条件，如果检测条件满足则执行转换;
	for ( int i=1; i<=32; ++i ) //目前认为单节点链接数不会超过此限额;
	{
		CLuaRestoreStack rsfor( m_pLuaState );

		//取links表第i个元素；
		lua_pushnumber( m_pLuaState, i );
		lua_rawget( m_pLuaState, -2 );
		if ( !lua_istable( m_pLuaState, -1 ) )
		{
			//已经到了表内最后一个元素，说明所有条件都不满足，无需执行转换；
			break;
		}

		if (true)
		{
			CLuaRestoreStack tmprs( m_pLuaState );
			//当前已取出links表的第i个元素，检查其advance函数；
			lua_pushstring( m_pLuaState, "advance" );
			lua_rawget( m_pLuaState, -2 );
			if( !lua_isfunction( m_pLuaState, -1 ) )
			{
				//取不到该表项的advance函数；
				break;
			}

			//调用检测函数；
			if ( ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
				|| ( ! lua_isboolean( m_pLuaState, -1 ) )   //或者返回值非bool值；
				)
			{
				break;
			}

			//取调用结果；
			int isTrans = (int) lua_toboolean( m_pLuaState, -1 );
			if ( isTrans )//转换条件满足；
			{
				//执行转换后退出，因为同一个stage只可能执行多个链接中的一个；
				//准备取目标状态；				
				lua_pop( m_pLuaState, 1 );//栈顶的两个元素，-1:检测函数返回值；
				//取目标stage；
				lua_pushstring( m_pLuaState, "target" );
				lua_rawget( m_pLuaState, -2 );
				if ( lua_isnil( m_pLuaState, -1 ) )
				{
					//目标stage为nil，表示退出与NPC对话；
					if ( SCRIPT_ITEM == m_scriptType )
					{
						pOwner->SetItemChatStage(0);
					} else if ( SCRIPT_NPC == m_scriptType ) {
						pOwner->SetNpcChatStage(0);
					//09.04.28,取消工会任务新实现,} else if ( SCRIPT_TASK == m_scriptType ) {
					//	pOwner->SetTaskChatStage(0);
					} else if( SCRIPT_AREA == m_scriptType ){
						pOwner->SetAreaChatStage(0);
					}
				} else if ( lua_isnumber( m_pLuaState, -1 ) ) {
					//取到了目标stage;
					int nTargetStage = (int) lua_tonumber( m_pLuaState, -1 );
					if ( SCRIPT_ITEM == m_scriptType )
					{
						pOwner->SetItemChatStage(nTargetStage);
						CallScriptOnUseItem( pOwner );//进入新状态回调脚本相应阶段的OnEnter函数；					
					} else if ( SCRIPT_NPC == m_scriptType ) {
						pOwner->SetNpcChatStage(nTargetStage);
						CallScriptOnNpcChat( pOwner );//进入新状态回调脚本相应阶段的OnEnter函数；					
					//09.04.28,取消工会任务新实现,} else if ( SCRIPT_TASK == m_scriptType ) {
					//	pOwner->SetTaskChatStage(nTargetStage);
					//	CallScriptOnTaskChat( pOwner );//进入新状态回调脚本相应阶段的OnEnter函数；					
					} else if( SCRIPT_AREA == m_scriptType ){
						pOwner->SetAreaChatStage( nTargetStage );
						CallScriptOnAreaChat( pOwner );//进入新状态回调脚本相应阶段的OnEnter函数；
					}
				} else {
					//error，错误日志；
				}
				break;
			}
			//转换条件不满足，继续检测下一条件；
		}		
	}

	return  true;
	TRY_END;
	return false;
}

///检测脚本链接函数，找到转换目标执行转换,新条件转移机制；
bool CNormalScript::NewCallScriptCheckTransCon( CPlayer* pOwner )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		return false;
	}

	//依次找到各advance函数，只要某一函数返回真，则执行stage转换;
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 0;
	if ( SCRIPT_ITEM == m_scriptType )
	{
		nStage = pOwner->GetItemChatStage();
	} else if ( SCRIPT_NPC == m_scriptType ) {
		nStage = pOwner->GetNpcChatStage();
	//09.04.28,取消工会任务新实现,} else if ( SCRIPT_TASK == m_scriptType ) {
	//	nStage = pOwner->GetTaskChatStage();
	} else if( SCRIPT_AREA == m_scriptType ){
		nStage = pOwner->GetAreaChatStage();
	}

	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的states;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到links函数；
	lua_pushstring( m_pLuaState, "links" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["links"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非表，未定义链接函数；
		return false;
	}

	//调用链接函数取链接目标；
	if ( ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) )//检测函数调用失败;
		 || ( ! lua_isnumber( m_pLuaState, -1 ) )   //或者返回值非数值；
		 )
	{
		return false;//执行链接函数失败，不执行转换；
	}

	//取到了目标stage;
	int nTargetStage = (int) lua_tonumber( m_pLuaState, -1 );
	if ( SCRIPT_ITEM == m_scriptType )
	{
		pOwner->SetItemChatStage(nTargetStage);
		CallScriptOnUseItem( pOwner );//进入新状态回调脚本相应阶段的OnEnter函数；					
	} else if ( SCRIPT_NPC == m_scriptType ) {
		pOwner->SetNpcChatStage(nTargetStage);
		CallScriptOnNpcChat( pOwner );//进入新状态回调脚本相应阶段的OnEnter函数；					
	//09.04.28,取消工会任务新实现,} else if ( SCRIPT_TASK == m_scriptType ) {
	//	pOwner->SetTaskChatStage(nTargetStage);
	//	CallScriptOnTaskChat( pOwner );//进入新状态回调脚本相应阶段的OnEnter函数；					
	} else if( SCRIPT_AREA == m_scriptType ){
		pOwner->SetAreaChatStage( nTargetStage );
		CallScriptOnAreaChat( pOwner );//进入新状态回调脚本相应阶段的OnEnter函数；
	}

	return  true;
	TRY_END;
	return false;
}




