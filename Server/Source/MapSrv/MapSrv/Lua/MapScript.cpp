﻿/**
* @file MapScript.cpp
* @brief 实现地图脚本
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MapScript.cpp
* 摘    要: 实现地图脚本
* 作    者: dzj
* 开始日期: 2009.03.31
*
*/

#include "MapScript.h"

#ifdef STAGE_MAP_SCRIPT

#include "../Player/Player.h"

#include "../MuxMap/muxmapbase.h"
#include "../Lua/ScriptExtent.h"
#include "../Activity/WarTaskManager.h"
#include "../monster/manmonstercreator.h"
#include "../CreatePtSet.h"
#include "../MuxMap/manmuxcopy.h"
#include "../MuxSkillConfig.h"
#include "../MuxMap/mannormalmap.h"

map<unsigned long, CMapScript*> CMapScriptManager::m_mapDwordFsms;
map<lua_State*, CMapScript*> CMapScriptManager::m_mapLuaStateFsms;

///从文件中装入脚本；
bool CMapScript::InitMapScript( const char* luaScriptFile )
{
	m_ID = 0;//无效的脚本ID号；
	m_pScriptUser = NULL;//初始无玩家与本NPC对话；
	m_strUiInfo.str("");//清对话界面信息；
	m_pScriptMap = NULL;

	if ( ! LoadFrom<CMapScriptExtent>( luaScriptFile ) ) 
	{
		return false;
	}

	return ReadScriptID();
}

///阶段初始化；
bool CMapScript::OnStageInit( CMuxMap* pMap )
{
	if ( NULL == pMap )
	{
		D_ERROR( "CMapScript::OnStageInit，输入地图指针空\n" );
		return false;
	}

	InitComponent();
	SetScriptUser( NULL );
	SetScriptMap( pMap );
	SetMapScriptState( On_Stage_Init );

	return CallScriptOnStageInit();
}

///事件：玩家进入关卡地图；
bool CMapScript::OnPlayerEnter( CMuxMap* pMap, CPlayer* pEnterPlayer )
{
	if ( ( NULL == pMap )
		|| ( NULL == pEnterPlayer )
		)
	{
		D_ERROR( "CMapScript::OnPlayerEnter，输入地图指针空或输入玩家指针空\n" );
		return false;
	}

	InitComponent();
	SetScriptUser( pEnterPlayer );
	SetScriptMap( pMap );
	SetMapScriptState( On_Player_Enter );

	return CallScriptOnPlayerEnter();
}

///事件：玩家离开关卡地图；
bool CMapScript::OnPlayerLeave( CMuxMap* pMap, CPlayer* pLeavePlayer )
{
	if ( ( NULL == pMap )
		|| ( NULL == pLeavePlayer )
		)
	{
		D_ERROR( "CMapScript::OnPlayerLeave，输入地图指针空或输入玩家指针空\n" );
		return false;
	}

	InitComponent();
	SetScriptUser( pLeavePlayer );
	SetScriptMap( pMap );
	SetMapScriptState( On_Player_Leave );

	return CallScriptOnPlayerLeave();
}

///事件：计数器到达；
bool CMapScript::OnCounterMeet( CMuxMap* pMap, unsigned int counterID )
{
	if ( NULL == pMap )
	{
		D_ERROR( "CMapScript::OnCounterMeet，输入地图指针空\n" );
		return false;
	}

	InitComponent();
	SetScriptUser( NULL );
	SetScriptMap( pMap );
	SetMapScriptState( On_Counter_Meet );

	return CallScriptOnCounterMeet( counterID );
}

///事件：计时器到达；
bool CMapScript::OnTimerMeet( CMuxMap* pMap, unsigned int timerID )
{
	if ( NULL == pMap )
	{
		D_ERROR( "CMapScript::OnTimerMeet，输入地图指针空\n" );
		return false;
	}

	InitComponent();
	SetScriptUser( NULL );
	SetScriptMap( pMap );
	SetMapScriptState( On_Timer_Meet );

	return CallScriptOnTimerMeet( timerID );
}


bool CMapScript::OnDetailTimerMeet( CMuxMap* pMap, unsigned int timerId )
{
	if ( NULL == pMap )
	{
		D_ERROR( "CMapScript::OnDetailTimerMeet，输入地图指针空\n" );
		return false;
	}

	InitComponent();
	SetScriptUser( NULL );
	SetScriptMap( pMap );
	SetMapScriptState( On_DetailTimer_Meet );

	return CallScriptOnDetailTimerMeet( timerId );
}


bool CMapScript::OnNpcDead( CMuxMap* pMap, CPlayer* pKiller, unsigned int npcTypeId )
{
	if ( NULL == pMap )
	{
		D_ERROR( "CMapScript::OnDNpcDead，输入地图指针空\n" );
		return false;
	}

	InitComponent();
	SetScriptUser( pKiller );
	SetScriptMap( pMap );
	SetMapScriptState( On_Npc_Dead );

	return CallScriptOnNpcDead( npcTypeId );
}


bool CMapScript::OnPlayerDie( CMuxMap* pMap, CPlayer* diePlayer )
{
	if ( NULL == pMap )
	{
		D_ERROR( "CMapScript::OnPlayerDie，输入地图指针空\n" );
		return false;
	}

	InitComponent();
	SetScriptUser( diePlayer );
	SetScriptMap( pMap );
	
	return CallScriptOnPlayerDie();
}


bool CMapScript::OnActivityStart( CMuxMap* pMap, unsigned int actID  )
{
	if ( NULL == pMap )
	{
		D_ERROR( "CMapScript::OnActivityStart，输入地图指针空\n" );
		return false;
	}

	InitComponent();
	SetScriptMap( pMap );
	return CallScriptOnActivityStart( actID );
}


bool CMapScript::OnActivityEnd( CMuxMap* pMap, unsigned int actID )		//活动结束
{
	if( NULL == pMap )
	{
		D_ERROR("CMapScript::OnActivityEnd，输入地图指针空\n");
		return false;
	}

	InitComponent();
	SetScriptMap( pMap );
	return CallScriptOnActivityEnd( actID );
}


//进入 离开区域
bool CMapScript::OnPlayerEnterArea( CMuxMap* pMap, CPlayer* pMovePlayer, unsigned int areaID )
{
	if( NULL == pMap )
	{
		D_ERROR("CMapScript::OnPlayerEnterArea，输入地图指针空\n");
		return false;
	}

	InitComponent();
	SetScriptMap( pMap );
	SetScriptUser( pMovePlayer );
	return CallScriptOnEnterArea( areaID );
}


bool CMapScript::OnPlayerLeaveArea( CMuxMap* pMap, CPlayer* pMovePlayer, unsigned int areaID )
{
	if( NULL == pMap )
	{
		D_ERROR("CMapScript::OnPlayerLeaveArea，输入地图指针空\n");
		return false;
	}

	InitComponent();
	SetScriptMap( pMap );
	SetScriptUser( pMovePlayer );
	return CallScriptOnLeaveArea( areaID );
}


//Npc首次被击
bool CMapScript::OnNpcFirstBeAttacked( CMuxMap* pMap, CPlayer* pAtker, unsigned int monsterID )
{
	if( NULL == pMap )
	{
		D_ERROR("CMapScript::OnNpcFirstBeAttacked，输入地图指针空\n");
		return false;
	}

	InitComponent();
	SetScriptMap( pMap );
	SetScriptUser( pAtker );
	return CallScriptOnNpcFirstBeAttacked( monsterID );
}


bool CMapScript::OnNotifyLevelMapInfo( CMuxMap* pMap, unsigned int count )
{
	if( NULL == pMap )
	{
		D_ERROR("CMapScript::NotifyLevelMapInfo，输入地图指针空\n");
		return false;
	}

	InitComponent();
	SetScriptMap( pMap );
	return CallScriptOnNotifyLevelMapInfo( count );
}


///地图内广播；
bool CMapScript::ExtBrocastMsg( const char* notimsg )
{
	if ( NULL != m_pScriptMap )
	{
		m_pScriptMap->MapBrocastChatMsg( notimsg );
	}
	return true; 
}

//取地图实例保存的脚本用数据
bool CMapScript::ExtGetMapScriptTarget( int& getVal )
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR("CMapScript::MSGetMapScriptTarget，m_pScriptMap空\n");
		return false;
	}

	m_pScriptMap->MapGetMapScriptTarget( getVal );
	return true;
}

//保存地图实例脚本用数据；
bool CMapScript::ExtSetMapScriptTarget( int tosetVal )
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR("CMapScript::MSSetMapScriptTarget，m_pScriptMap空\n");
		return false;
	}

	m_pScriptMap->MapSetMapScriptTarget( tosetVal );
	return true;
}

///添加地图计数器；
bool CMapScript::ExtAddCounterOfMap( unsigned int counterClsId
									, unsigned int counterType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount )
{
	if ( NULL != m_pScriptMap )
	{
		m_pScriptMap->MapAddCounterOfMap( counterClsId, counterType, counterIdMin, counterIdMax, targetCount );
	}
	return true; 
}

///添加地图计时器；
bool CMapScript::ExtAddTimerOfMap( unsigned int timerClsID, unsigned int resTime/*到期时间，单位:分钟*/ )
{
	if ( NULL != m_pScriptMap )
	{
		m_pScriptMap->MapInsertMapTimer( timerClsID, resTime );
	}
	return true; 
}

bool CMapScript::ExtIsHaveTimerOfMap( unsigned int timerClsID )
{
	if ( NULL != m_pScriptMap )
	{
		return m_pScriptMap->MapIsHaveMapTimer( timerClsID );
	}
	return false;
}


bool CMapScript::ExtAddDetailTimerOfMap( unsigned int timerId, unsigned hour, unsigned minute )
{
	if ( NULL != m_pScriptMap )
	{
		m_pScriptMap->MapInsertDetailTimer( timerId, hour, minute );
	}
	return true; 
}


void CMapScript::ExtAttackWarEnd( unsigned int taskId )		//给脚本回调攻城战结束
{
	ACE_UNUSED_ARG( taskId );
	CWarTaskManager::NotifyAttackCityEnd();
}



bool CMapScript::ExtIsAttackWar()		//是否是攻城时间
{
	return CWarTaskManager::IsAttackWar();
}


bool CMapScript::ExtCreateNpc( unsigned int npcTypeId, unsigned short posX, unsigned short posY )
{ 
	if ( NULL == m_pScriptMap )
	{
		return false;
	}

	CMonster* pCreatedMonster = NULL;
	CCreatorIns::SinglePtRefreshNoCreator( npcTypeId, m_pScriptMap, posX, posY, 0, pCreatedMonster, NULL );

	return true;
}


//删除指定Type的npc
bool CMapScript::ExtDelNpc( unsigned int npcTypeId )
{
	if( !m_pScriptMap )
		return false;

	m_pScriptMap->DelTypeMonster( npcTypeId );
	return true;
}


bool CMapScript::ExtDelNpcByMapID( unsigned int mapID, unsigned int npcTypeId )
{
	CMuxMap* pMap = CManNormalMap::FindMap( mapID );
	if( NULL == pMap )
	{
		D_WARNING("CMapScript::ExtDelNpcByMapID pMap = NULL\n");
		return false;
	}

	pMap->DelTypeMonster( npcTypeId );
	return true;
}


bool CMapScript::ExtNotifyWarTaskState( unsigned int taskId, ETaskStatType taskState )			//通知拥有这个攻城任务的玩家任务完成
{
	//需要通知所有的GateSrv
	MGNotifyWarTaskState gateMsg;
	gateMsg.warTaskInfo = WarTaskInfo( taskId, taskState );
	CManG_MProc::SendMsgToAllGate<MGNotifyWarTaskState>( &gateMsg );
	
	return true;
}


bool CMapScript::ExtDeleteTimer( unsigned int timerId )
{
	if ( NULL != m_pScriptMap )
	{
		m_pScriptMap->MapDeleteMapTimer( timerId );
	}
	return true; 
}


bool CMapScript::ExtDeleteDetailTimer( unsigned int detailTimerId )
{
	if ( NULL != m_pScriptMap )
	{
		m_pScriptMap->MapDeleteDetailTimer( detailTimerId );
	}
	return true; 
}

bool CMapScript::ExtNotifyWorldChat( const char* pMsg )
{
	MGChat gateMsg;
	gateMsg.chatType = CT_WORLD_CHAT;
	gateMsg.extInfo = CT_SYS_EVENT_NOTI;//世界聊天的系统消息；
	SafeStrCpy( gateMsg.strChat, pMsg );
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		return false;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( MGChat, pGateSrv, gateMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
	return true;
}


bool CMapScript::ExtCreateNpcBySet( unsigned int npcTypeId, unsigned int createSet, unsigned int npcNum )
{
	CCreatePtSet* pCreateSet = CCreatePtSetManager::FindCreateSet( createSet );
	if( NULL == pCreateSet )
	{
		D_WARNING( "ExtCreateNpcBySet， 找不到创建集%d\n", createSet );
		return false;
	}

	CMuxMap* pMap = GetScriptMap();
	if( NULL == pMap )
	{
		D_WARNING( "ExtCreateNpcBySet， 脚本所在地图空，createSet%d\n", createSet );
		return false;
	}

	if( pCreateSet->GetMapID() != pMap->GetMapNo() )
	{
		D_WARNING( "ExtCreateNpcBySet， createSet%d所属地图%d与脚本地图%d不符\n", createSet, pCreateSet->GetMapID(), pMap->GetMapNo() );
		return false;
	}

	MuxPoint pt;
	if( !pCreateSet->GetRandomPt( pt ) )
	{
		D_WARNING( "ExtCreateNpcBySet， createSet%d取随机点失败\n", createSet );
		return false;
	}
	
	CMonster* pCreatedMonster = NULL;
	for( unsigned int i=0; i<npcNum; ++i )
	{
		CCreatorIns::SinglePtRefreshNoCreator( npcTypeId, m_pScriptMap, pt.nPosX, pt.nPosY, 0, pCreatedMonster, NULL );
	}
	return true;
}


//获取怪物最后一刀玩家种族
unsigned int CMapScript::GetNpcKillerRace()
{
	if( NULL != m_pScriptUser && m_callScriptState == On_Npc_Dead )
	{
		return m_pScriptUser->GetRace();
	}
	return 1;
}

//修改地图归属种族
bool CMapScript::ModifyMapRace( unsigned int race )
{
	if( NULL == m_pScriptMap )
		return false;

	CityInfo* pCityInfo = m_pScriptMap->GetCityInfo();
	if( NULL == pCityInfo )
	{
		D_ERROR("ModifyMapRace Map:%d 没有配置城市信息\n", m_pScriptMap->GetMapNo() );
		return false;
	}

	pCityInfo->ucRace = race;
	m_pScriptMap->UpdateCityInfo();	//更改后更新到DB
	return true;
}

//Get MapRace
unsigned int CMapScript::GetMapRace()
{
	if( NULL == m_pScriptMap )
		return false;

	CityInfo* pCityInfo = m_pScriptMap->GetCityInfo();
	if( NULL == pCityInfo )
	{
		D_ERROR("GetMapRace Map:%d 没有配置城市信息\n", m_pScriptMap->GetMapNo() );
		return false;
	}

	return pCityInfo->ucRace;
}


bool CMapScript::OpenNpcCreator( unsigned int npcCreatorID )
{
	if( NULL == m_pScriptMap )
		return false;

	m_pScriptMap->SetNpcCreatorActiveFlag( npcCreatorID, true );
	return true;
}


bool CMapScript::CloseNpcCreator( unsigned int npcCreatorID )
{
	if( NULL == m_pScriptMap )
		return false;

	return m_pScriptMap->SetNpcCreatorActiveFlag( npcCreatorID, false );
}

//获取城市等级
unsigned int CMapScript::GetCityLevel()
{
	if( NULL == m_pScriptMap )
		return 0;

	CityInfo* pCityInfo = m_pScriptMap->GetCityInfo();
	if( NULL == pCityInfo )
	{
		D_ERROR("GetCityLevel Map:%d 并没有配置地图信息\n", m_pScriptMap->GetMapNo() );
		return 0;
	}

	return pCityInfo->cityLevel;
}

//设置该地图物件NPC的状态
bool CMapScript::SetItemNpcStatus( unsigned int itemSeq, unsigned int status )
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR( "CMapScript::SetItemNpcStatus，对应地图指针空\n" );
		return false;
	}

	return m_pScriptMap->SetMapItemNpcState( itemSeq, status );
}

//修改复活点种族归属
bool CMapScript::ModifyRebirthRace( unsigned int mapID, unsigned int race )
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR( "CMapScript::ModifyRebirthRace，对应地图指针空\n" );
		return false;
	}

	CMuxMap* pMap = CManNormalMap::FindMap( mapID );
	if( NULL == pMap )
	{
		D_ERROR("找不到Map ID为%d的地图\n", mapID );
		return false;
	}
		
	return pMap->ModityWarRebirthRace((unsigned short)race);	
}


//获取地图内计数器数字
unsigned int CMapScript::GetMapCounterNum()
{
	return 0;
}


//修改跳转点的种族
bool CMapScript::ModifyTelpotRace( unsigned int mapID, unsigned int race )
{
	//必须要该地图在这个地图上
	CMuxMap* pMap = CManNormalMap::FindMap( mapID );
	if( NULL == pMap )
	{
		D_ERROR("找不到ID为%d的地图\n", mapID );
		return false;
	}

	return pMap->ModityWarSwitchRace((unsigned short)race);
}


//创建buff	
bool CMapScript::CreateBuff( unsigned int buffID, unsigned int buffTime )
{
	if( NULL == m_pScriptUser )
	{
		D_ERROR("CMapScript::CreateBuff 对应buffID为NULL\n");
		return false;
	}

	MuxBuffProp* pBuffProp = BuffPropSingleton::instance()->FindTSkillProp( buffID );
	if( NULL == pBuffProp )
		return false;

	PlayerID createID;
	createID.dwPID = 0;
	createID.wGID = 0;
	m_pScriptUser->CreateMuxBuff( pBuffProp, buffTime, createID ); 
	return true;
}

//删除  
bool CMapScript::DeleteBuff( unsigned int buffID )
{
	if( NULL == m_pScriptUser )
	{
		D_ERROR("CMapScript::DeleteBuff 对应buffID为NULL\n");
		return false;
	}

	return m_pScriptUser->DeleteMuxBuff( buffID );
}


bool CMapScript::KillAllPlayerByRace( unsigned int race )
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR("CMapScript::KillAllPlayerByRace m_pScriptMap = NULL");
		return false;
	}

	m_pScriptMap->KillAllPlayerByRace( race );
	return true;
}


const char* CMapScript::GetScriptUserName()
{
	if( NULL == m_pScriptUser )
	{
		return "";//npc被打死时，脚本中可能会取杀怪物者名字，但怪物可能不是被人杀死，为防止脚本出错，因此返回一个空字符串;
	}

	return m_pScriptUser->GetNickName();
}


//删除玩家身上的道具
bool CMapScript::RemoveUserItem( unsigned int itemID, unsigned int num )
{
	if( NULL == m_pScriptUser )
	{
		D_ERROR("当前相应函数无脚本使用者\n");
		return false;
	}

	if( num != 0 )
	{
		m_pScriptUser->OnDropItemByType( itemID, num ); 
	}else
	{
		m_pScriptUser->OnDropAllByType( itemID );
	}
	return true;
}


bool CMapScript::SwitchMapPlayer( unsigned int race, unsigned int mapID, unsigned int posX1, unsigned int posY1, unsigned int posX2, unsigned int posY2 )
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR("找不到对应的map\n");
		return false;
	}
	return m_pScriptMap->SwitchMapPlayer( race, mapID, posX1, posY1, posX2, posY2 );
}


///调整对应玩家的副本阶段
bool CMapScript::ExtSetResPlayerCopyStage( unsigned short copyStage )
{
	if( NULL == m_pScriptUser )
	{
		D_ERROR( "CMapScript::ExtSetResPlayerCopyStage，对应玩家指针空\n" );
		return false;
	}

	return m_pScriptUser->SetStageInfoStageNum( copyStage );
}

///副本地图设置所有玩家的outmapinfo;
bool CMapScript::ExtSetAllPlayerOutPos( unsigned short mapid, unsigned short posX, unsigned short posY, unsigned short extent )
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR( "CMapScript::ExtSetAllPlayerOutPos，对应玩家指针空\n" );
		return false;
	}

	return m_pScriptMap->SetAllPlayerOutPos( mapid, posX, posY, extent );
}

///副本地图设置玩家的outmapinfo;
bool CMapScript::ExtSetPlayerOutPos( unsigned short mapid, unsigned short posX, unsigned short posY, unsigned short extent )
{
	if( NULL == m_pScriptUser )
	{
		D_ERROR( "CMapScript::ExtSetPlayerOutPos，对应玩家指针空\n" );
		return false;
	}

	return m_pScriptUser->SetPlayerOutPos( mapid, posX, posY, extent );
}

///副本地图踢相应玩家；
bool CMapScript::ExtKickResPlayer()
{
	if( NULL == m_pScriptUser )
	{
		D_ERROR( "CMapScript::ExtKickResPlayer，对应地图指针空\n" );
		return false;
	}

	m_pScriptUser->IssuePlayerLeaveCopyHF();

	return true;
}

///副本结束(同时踢所有玩家)；
bool CMapScript::ExtEndCopy()
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR( "CMapScript::ExtEndCopy，对应地图指针空\n" );
		return false;
	}

	m_pScriptMap->IssueDelCopyQuest();

	return true;
}

///设置某伪副本可进；
bool CMapScript::ExtPCopyOpen()
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR( "CMapScript::ExtPCopyOpen，对应地图指针空\n" );
		return false;
	}

	m_pScriptMap->OnPCopyOpen();

	return true;
}

///设置某伪副本不可进；
bool CMapScript::ExtPCopyClose()
{
	if( NULL == m_pScriptMap )
	{
		D_ERROR( "CMapScript::ExtPCopyClose，对应地图指针空\n" );
		return false;
	}

	m_pScriptMap->OnPCopyClose();

	return true;
}

///调整对应玩家位置至指定点(例如进入地图的玩家)
bool CMapScript::ExtSetResPlayerPos( unsigned short posX, unsigned short posY, unsigned short extent )
{
	if ( NULL == m_pScriptUser )
	{
		D_ERROR( "CMapScript::ExtSetResPlayerPos，对应玩家指针空\n" );
		return false;
	}

	CMuxMap* pMap = m_pScriptUser->GetCurMap();
	if ( NULL == pMap )
	{
		D_ERROR( "CMapScript::ExtSetResPlayerPos，玩家所在地图空\n" );
		return false;
	}

	bool isMoveOk = pMap->OnPlayerInnerJump( m_pScriptUser, posX, posY );
	if ( !isMoveOk )
	{
		D_ERROR( "CMapScript::ExtSetResPlayerPos，玩家地图内跳转失败\n" );
		return false;
	} 

	return true;
}

///设置副本TD标记；
bool CMapScript::ExtSetTDFlag()
{
	if ( NULL == m_pScriptMap )
	{
		return false;
	}
	return m_pScriptMap->SetTDFlag();
}

///地图脚本通知对应玩家消息(例如进入地图的玩家，离开地图的玩家等)
bool CMapScript::ExtNotiResPlayerMsg( const char* notimsg ) 
{ 
	if ( NULL != m_pScriptUser )
	{
		m_pScriptUser->SendSystemChat( notimsg );
	}
	return true; 
};

bool CMapScript::ExtSendStandardUiInfo(unsigned int infoNo )
{
	if ( NULL != m_pScriptUser )
	{
		return m_pScriptUser->NoticeTaskUiInfo( infoNo );
	}
	return false;
}

///根据当前stage调用脚本OnStageInit;
bool CMapScript::CallScriptOnStageInit()
{
	TRY_BEGIN;

	if ( NULL == m_pScriptMap )
	{
		return false;
	}

	CLuaRestoreStack rs( m_pLuaState );

	////当前stage;
	int nStage = 1;//pOwner->GetNpcChatStage();
	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnStageInit函数并执行之；
	lua_pushstring( m_pLuaState, "OnStageInit" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnStageInit"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnStageInit；
		return false;
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnEnter;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

///根据当前stage调用脚本OnPlayerEnter；
bool CMapScript::CallScriptOnPlayerEnter()
{
	TRY_BEGIN;

	if ( NULL == m_pScriptMap )
	{
		return false;
	}

	CLuaRestoreStack rs( m_pLuaState );

	////当前stage;
	int nStage = 1;//pOwner->GetNpcChatStage();
	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnPlayerEnter函数并执行之；
	lua_pushstring( m_pLuaState, "OnPlayerEnter" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnStageInit"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnStageInit；
		return false;
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnEnter;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

///根据当前stage调用脚本OnPlayerLeave；
bool CMapScript::CallScriptOnPlayerLeave()
{
	TRY_BEGIN;

	if ( NULL == m_pScriptMap )
	{
		return false;
	}

	CLuaRestoreStack rs( m_pLuaState );

	////当前stage;
	int nStage = 1;//pOwner->GetNpcChatStage();
	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnPlayerLeave函数并执行之；
	lua_pushstring( m_pLuaState, "OnPlayerLeave" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnStageInit"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnStageInit；
		return false;
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnEnter;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

///根据当前stage调用脚本OnCounterMeet；
bool CMapScript::CallScriptOnCounterMeet( unsigned int counterID )
{
	TRY_BEGIN;

	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnCounterMeet函数并执行之；
	lua_pushstring( m_pLuaState, "OnCounterMeet" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, counterID );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

///根据当前stage调用脚本OnTimerMeet；
bool CMapScript::CallScriptOnTimerMeet( unsigned int timerID )
{
	TRY_BEGIN;

	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnTimerMeet函数并执行之；
	lua_pushstring( m_pLuaState, "OnTimerMeet" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, timerID );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;

}


bool CMapScript::CallScriptOnDetailTimerMeet( unsigned int timerId )
{
	TRY_BEGIN;

	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnDetailTimerMeet函数并执行之；
	lua_pushstring( m_pLuaState, "OnDetailTimerMeet" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, timerId );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}


bool CMapScript::CallScriptOnNpcDead( unsigned int npcTypeId )
{
	TRY_BEGIN;

	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnNpcDead函数并执行之；
	lua_pushstring( m_pLuaState, "OnNpcDead" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, npcTypeId );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}


bool CMapScript::CallScriptOnPlayerDie()
{
	TRY_BEGIN;

	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnNpcDead函数并执行之；
	lua_pushstring( m_pLuaState, "OnPlayerDie" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

bool CMapScript::CallScriptOnActivityEnd( unsigned int actID )
{
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnDetailTimerMeet函数并执行之；
	lua_pushstring( m_pLuaState, "OnActivityEnd" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, actID );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}


bool CMapScript::CallScriptOnEnterArea( unsigned int areaID )
{
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnDetailTimerMeet函数并执行之；
	lua_pushstring( m_pLuaState, "OnEnterArea" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, areaID );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}


bool CMapScript::CallScriptOnLeaveArea( unsigned int areaID )
{
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	if ( nStage<=0 )
	{
		return false;//进到本函数时，不应该出现stage为0的情况；
	}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnDetailTimerMeet函数并执行之；
	lua_pushstring( m_pLuaState, "OnLeaveArea" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, areaID );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}


//NPC首次被击
bool CMapScript::CallScriptOnNpcFirstBeAttacked( unsigned int monsterClsID )
{
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnNpcFirstBeAttacked函数并执行之；
	lua_pushstring( m_pLuaState, "OnNpcFirstBeAttacked" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, monsterClsID );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}


bool CMapScript::CallScriptOnNotifyLevelMapInfo( unsigned int count )
{
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnNotifyLevelMapInfo函数并执行之；
	lua_pushstring( m_pLuaState, "OnNotifyLevelMapInfo" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, count );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}



bool CMapScript::CallScriptOnActivityStart( unsigned int actID )
{
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;

	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其OnDetailTimerMeet函数并执行之；
	lua_pushstring( m_pLuaState, "OnActivityStart" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["OnOption"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义OnOption；
		return false;
	}
	lua_pushnumber( m_pLuaState, actID );

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 1, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}



///检测脚本链接函数，找到转换目标执行转换,新条件转移机制；
bool CMapScript::NewCallScriptCheckTransCon( CPlayer* pOwner )
{
	TRY_BEGIN;

	ACE_UNUSED_ARG( pOwner );//地图脚本stage无变化；

	//依次找到各advance函数，只要某一函数返回真，则执行stage转换;
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nStage = 1;//GetStartStage();

	//if ( nStage<=0 )
	//{
	//	return false;//进到本函数时，不应该出现stage为0的情况；
	//}

	//找到相应的states;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nStage );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到links函数；
	lua_pushstring( m_pLuaState, "links" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["links"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非表，未定义链接函数；
		return false;
	}

	//调用链接函数取链接目标；
	if ( ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) )//检测函数调用失败;
		 || ( ! lua_isnumber( m_pLuaState, -1 ) )   //或者返回值非数值；
		 )
	{
		return false;//执行链接函数失败，不执行转换；
	}

	//取到了目标stage;
	int nTargetStage = (int) lua_tonumber( m_pLuaState, -1 ); ACE_UNUSED_ARG( nTargetStage );
	if ( true /*SCRIPT_ITEM == m_scriptType*/ )
	{
		CallScriptOnStageInit();//进入新状态回调脚本相应阶段的OnStageInit函数；					
	} else {
		D_ERROR( "错误的地图脚本类型\n" );
		return false;
	}

	return  true;
	TRY_END;
	return false;
}


void CMapScript::InitComponent()
{
	m_ID = 0;//无效的脚本ID号；
	m_pScriptUser = NULL;//初始无玩家与本NPC对话；
	m_strUiInfo.str("");//清对话界面信息；
	m_callScriptState = Map_Script_State_Max;  //调用脚本设置为无效
}


void CMapScript::SetMapScriptState( EMapScriptState scriptState )
{
	m_callScriptState = scriptState;
}


//创建WarTimer
bool CMapScript::AddWarTimer( unsigned int timerID, unsigned int leftSecond, unsigned int startInformTime, unsigned int clockWise, bool isInformClient, const char* timerWordInfo )
{
	if( NULL == m_pScriptMap )
		return false;

	CWarTimerManager& timerManager = m_pScriptMap->GetWarTimerManager();
	timerManager.AddWarTimer( timerID, leftSecond, startInformTime, clockWise, isInformClient, timerWordInfo );
	return true;
}


//删除WarTimer
bool CMapScript::DelWarTimer( unsigned int timerID )
{
	//D_INFO("Enter CMapScript::DelWarTimer timerID = %d\n",timerID);
	if( NULL == m_pScriptMap )
	{
		//D_INFO("m_pScriptMap == NULL\n");
		return false;
	}

	CWarTimerManager& timerManager = m_pScriptMap->GetWarTimerManager();
	timerManager.DeleteWarTimer( timerID );
	return true;
}


//创建WarCounter
bool CMapScript::AddWarCounter( unsigned int counterID, unsigned int counterNum, const char* szCounterInfo )
{
	if( NULL == m_pScriptMap )
		return false;

	CWarCounterManager& counterManager = m_pScriptMap->GetWarCounterManager();
	counterManager.AddWarCounter( counterID, counterNum, szCounterInfo );
	return true;
}


//删除warCounter
bool CMapScript::DelWarCounter( unsigned int counterID )
{
	if( NULL == m_pScriptMap )
		return false;

	CWarCounterManager& counterManager = m_pScriptMap->GetWarCounterManager();
	counterManager.DelWarCounter( counterID );
	return true;
}

//获得计数器的数字
unsigned int CMapScript::GetWarCounterNum( unsigned int counterID )
{
	if( NULL == m_pScriptMap )
		return 0;

	CWarCounterManager& counterManager = m_pScriptMap->GetWarCounterManager();
	return counterManager.GetCounterNum( counterID );
}


//全地图通知计数器信息
bool CMapScript::MapDisplayWarCounterInfo( unsigned int counterID )
{
	if( NULL == m_pScriptMap )
		return false;

	CWarCounterManager& counterManager = m_pScriptMap->GetWarCounterManager();
	counterManager.MapDisplayCounterInfo( counterID );
	return true;
}


//设置计数器的数字
bool CMapScript::SetWarCounterNum( unsigned int counterID, unsigned int counterNum )
{
	if( NULL == m_pScriptMap )
		return false;

	CWarCounterManager& counterManager = m_pScriptMap->GetWarCounterManager();
	counterManager.SetCounterNum( counterID, counterNum );
	return true;
}



//单玩家通知计数器的数字
bool CMapScript::PlayerDisplayWarCounterInfo( unsigned int counterID )
{
	if( NULL == m_pScriptMap )
		return false;
	
	CWarCounterManager& counterManager = m_pScriptMap->GetWarCounterManager();
	counterManager.OnPlayerDisplayCounterInfo( counterID, m_pScriptUser );
	return true;
}


bool CMapScript::ExtShowNotice(const char* content, char position, char isglobal, unsigned int maxshowcount, unsigned int distancetime )
{
	if ( NULL == m_pScriptMap )
	{
		D_ERROR( "CMapScript::ExtShowNotice, NULL == m_pScriptMap\n" );
		return false;
	}

	if ( !content )
		return false;

	MGShowTmpNoticeByPosition tmpnotice;
	StructMemSet( tmpnotice, 0x0, sizeof(tmpnotice) );
	tmpnotice.isbroadcast = isglobal;
	tmpnotice.showtmpnotice.distancetime = distancetime;
	tmpnotice.showtmpnotice.maxshowcount = maxshowcount;
	tmpnotice.showtmpnotice.showposition = position;
	tmpnotice.showtmpnotice.contentlen = ACE_OS::snprintf( tmpnotice.showtmpnotice.content,
		sizeof(tmpnotice.showtmpnotice.content)-1,
		"%s",
		content);

	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
		return false;

	MsgToPut* pNewMsg = CreateSrvPkg( MGShowTmpNoticeByPosition, pGateSrv, tmpnotice );
	if ( pNewMsg )
		pGateSrv->SendPkgToGateServer( pNewMsg );

	return true;
}

bool CMapScript::ExtShowParamNotice(unsigned int msgid, char position, char isglobal,unsigned int maxshowcount, unsigned int distancetime,
									unsigned int validparam,const char* param1,const char* param2, const char* param3,const char* param4,const char* param5 )
{
	if ( NULL == m_pScriptMap )
	{
		D_ERROR( "CMapScript::ExtShowNotice, NULL == m_pScriptMap\n" );
		return false;
	}

	MGSendParamNotice paramnotice;
	StructMemSet( paramnotice, 0x0, sizeof(paramnotice) );
	paramnotice.parammsgnotice.msgid = msgid;
	paramnotice.parammsgnotice.showposition = position;
	paramnotice.parammsgnotice.validparamcount = validparam;
	paramnotice.parammsgnotice.distancetime = distancetime;
	paramnotice.parammsgnotice.maxshowcount = maxshowcount;
	paramnotice.isgloabl = isglobal;

	if ( validparam>0 && param1 )
	{
		paramnotice.parammsgnotice.param1len = ACE_OS::snprintf( paramnotice.parammsgnotice.param1,
			sizeof(paramnotice.parammsgnotice.param1)-1,
			"%s",
			param1 );

		if ( validparam>1 && param2 )
		{
			paramnotice.parammsgnotice.param2len = ACE_OS::snprintf( paramnotice.parammsgnotice.param2,
				sizeof(paramnotice.parammsgnotice.param2)-1,
				"%s",
				param2 );

			if ( validparam>2 && param3 )
			{
				paramnotice.parammsgnotice.param3len = ACE_OS::snprintf( paramnotice.parammsgnotice.param3,
					sizeof(paramnotice.parammsgnotice.param3)-1,
					"%s",
					param3 );

				if ( validparam>3 && param4 )
				{
					paramnotice.parammsgnotice.param4len = ACE_OS::snprintf( paramnotice.parammsgnotice.param4,
						sizeof(paramnotice.parammsgnotice.param4)-1,
						"%s",
						param4 );

					if ( validparam>4 && param5 )
					{
						paramnotice.parammsgnotice.param5len = ACE_OS::snprintf( paramnotice.parammsgnotice.param5,
							sizeof(paramnotice.parammsgnotice.param5)-1,
							"%s",
							param5 );
					}
				}
			}
		}
	}
	
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
		return false;

	MsgToPut* pNewMsg = CreateSrvPkg( MGSendParamNotice, pGateSrv, paramnotice );
	if ( pNewMsg )
		pGateSrv->SendPkgToGateServer( pNewMsg );

	return true;
}

bool CMapScript::ExtSetBlockQuad( unsigned int regionID, unsigned int val )
{
#ifdef HAS_PATH_VERIFIER
	if( NULL != m_pScriptMap )
	{
		m_pScriptMap->SetBlockQuard(regionID, (val != 0) ? true : false);
		return true;
	}
#endif

	return false;
}


bool CMapScript::CreateMapBuff( unsigned int race, unsigned int buffID, unsigned int buffTime )
{
	if( NULL == m_pScriptMap )
		return false;

	m_pScriptMap->CreateMapBuff( race, buffID, buffTime );
	return true;
}

bool CMapScript::ExtWarRealStart(void)
{
	if( NULL == m_pScriptMap )
		return false;

	if(!CWarTaskManager::IsAttackWar())
		return false;

	m_pScriptMap->SetWarStart();
	return true;
}

#endif //STAGE_MAP_SCRIPT





