﻿/**
* @file NpcChatScript.h
* @brief 定义脚本描述的NPC对话
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: NpcChatScript.h
* 摘    要: 定义脚本描述的NPC对话
* 作    者: dzj
* 完成日期: 2007.04.14
*
*/

#pragma once

#include "../Lua/LuaScript.h"
#include "XmlManager.h"
#include "Utility.h"

#include <vector>
#include <string>

using namespace std;

class CPlayer;
///原来只用于NPC对话脚本，扩展用于挂item脚本，计数器脚本
class CNormalScript : public CLuaScriptBase
{
	friend class CNormalScriptManager;

	enum SCRIPT_TYPE
	{
		SCRIPT_NPC = 0,  //NPC对话脚本；
		SCRIPT_ITEM, //ITEM对话脚本；
		SCRIPT_AREA, //区域脚本
		//09.04.28,取消工会任务新实现,SCRIPT_TASK, //任务脚本
		SCRIPT_INVALIDTYPE //无效脚本；
	};

private:
	CNormalScript() {};
	virtual ~CNormalScript (void) {};

	//从文件中装入脚本；
	bool InitNormalScript( const char* luaScriptFile );

	void SetScriptTypeItem() { m_scriptType = SCRIPT_ITEM; };

	void SetScriptTypeNpc() { m_scriptType = SCRIPT_NPC; };

	void SetScriptTypeArea() { m_scriptType = SCRIPT_AREA; }

	//09.04.28,取消工会任务新实现,void SetScriptTypeTask() { m_scriptType = SCRIPT_TASK; };

public:
	bool IsAreaScript() { return (SCRIPT_AREA == m_scriptType); };

	SCRIPT_TYPE GetScriptType() { return m_scriptType; };

public:
	unsigned long GetID() { return m_ID; }

	CPlayer* GetScriptUser()
	{
		return m_pScriptUser;
	}

public:///以下为供玩家对话选择的接口;

	///NPC死亡事件调用脚本(只适用于boss级怪物，只有一个阶段，交互对象为杀怪物者)；
	bool OnNpcDie( CPlayer* pKiller );

	//09.04.28,取消工会任务新实现,///选项选择，nOption==0表示初始选择任各对话；
 //   bool OnTaskChat( CPlayer* pOwner, int nOption );

	///选项选择，nOption==0表示初始选择NPC;
	bool OnNpcChat( CPlayer* pOwner, int nOption );

	///玩家使用道具时，调用道具脚本;
	///   只有本脚本的确为道具脚本时才有效;
	///   目前固定调用start stage中的OnEnter，以后也可以考虑在道具上绑定与对话一样复杂的脚本，如果那样，则必须对道具脚本也增加nstage;
	bool OnItemChat( CPlayer* pOwner, int nOption );

	///玩家进入任务区域时,调用区域脚本
	bool OnAreaChat( CPlayer* pOwner, int nOption );

	/////计数器条件满足时，调用计数器脚本;
	/////   只有本脚本的确为计数器脚本时才有效；
	/////   目前固定调用start stage中的OnMeetCounter，以后也可以考虑在道具上绑定与对话一样复杂的脚本，如果那样，则必须对计数器脚本也增加nstage;
	//bool OnMeetCounter( CPlayer* pOwner );

public:///以下为提供对话脚本回调的接口；
	///NPC通知交互者显示信息；
	void ExtSetNpcShowMsg( const char* showMsg )
	{
		m_strUiInfo << "@0:";
		m_strUiInfo << showMsg; //例如:"::1001;王;李;"
	}

	//NPC通知自定义显示消息
	void ExtSetNpcShowCustomizeMsg( const char* showMsg )
	{
		m_strUiInfo <<";";
		m_strUiInfo << showMsg;
	}

	///NPC设置NPC选项信息
	void ExtSetNpcOption( int nOption, const char* optionMsg )
	{
		m_strUiInfo << "@" << nOption << ":" << optionMsg;
	}
	///NPC中Item向对话玩家发送对话界面信息；
	void ExtNpcSendUiInfo();
	///NPC发送关闭对话界面指令
	void ExtNpcCloseUiInfo();
	///NPC检测是否需要执行stage转换
	void ExtCheckTransCon()
	{
		if ( NULL != m_pScriptUser )
		{
			//CallScriptCheckTransCon( m_pScriptUser );
			NewCallScriptCheckTransCon( m_pScriptUser );
		}
		return;
	}

	/*任务相关*/
	///查询：调用者等级是否小于特定值；
	bool ExtQueryPlayerLevelLowerThan( unsigned int checkLevel );
	///查询：调用者等级是否大于特定值；
	bool ExtQueryPlayerLevelMoreThan( unsigned int checkLevel );
	///查询：调用者是否从未接过特定任务；
	bool ExtQueryNeverAcceptTask( unsigned int taskID );
	///查询：指定任务在玩家的任务列表中；
	bool ExtQueryOnMissionList( unsigned int taskID );
	///查询：指定任务在任务列表但未达成目标
	bool ExtQueryNotAchieveMissionTarget( unsigned int taskID );
	///查询：指定任务在任务列表达成目标
	bool ExtQueryAchieveMissionTarget( unsigned int taskID );
	///查询：完成过任务
	bool ExtQueryIsFinishMissionBefore( unsigned int taskID );
	///NPC检测玩家智能值，大于；
    bool ExtQueryIntelligenceMoreThan( unsigned int checkValue );
	///NPC检测玩家智能值，小于；
    bool ExtQueryIntelligenceLessThan( unsigned int checkValue );
	///查询：玩家包裹中是否有特定物品;
	bool ExtQueryPackageHave( unsigned int itemTypeID ,unsigned int itemNum );
	///检测道具能否被放入道具包
	bool ExtQueryPackageCanContain( unsigned int itemArr[], unsigned int ArrCount );
	///通知客户端任务失败信息
	bool ExtSendStandardUiInfo( unsigned int infoNo );
	///操作：给玩家任务
	int ExtGiveTask( unsigned int taskID );
	///操作：执行任务(将任务属性设置为已达成目标但未交还)
	int ExtProcessTask( unsigned int taskID );
	///操作：完成任务,0正常完成，-1非正常完成
	int ExtFinishTask( unsigned int taskID );
	///获得玩家性别
	int ExtGetPlayerSex();
	///操作：给玩家加经验
	int ExtAddExp( unsigned int expToAdd );
	///能否给与金钱
	bool ExtCanGiveMoney( unsigned int addMoney );
	///操作：给玩家加金钱
	int ExtAddMoney( unsigned int moneyToAdd );
	///操作：给玩家加属性
    int ExtAddProperty( unsigned int typeToAdd, int valToAdd );
	///操作：给玩家设属性
    int ExtSetProperty( unsigned int typeToSet, int valToSet );	

	///操作：传玩家进副本
	int ExtTransPlayerToCopy( unsigned short copyMapID, unsigned short scrollType );

	///操作：传玩家进伪副本
	int ExtTransPlayerToPCopy( unsigned short copyMapID, unsigned short scrollType );

	///NPC给玩家身上加计数器；
	int ExtAddPlayerCounter( unsigned int counterIdType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount, unsigned int taskID );
	///NPC给玩家道具；
	int ExtGiveItem( unsigned int itemTypeID, unsigned int itemNum );
	///NPC给玩家的道具附加随机等级
	int ExtGiveItemRondomLevel( unsigned int itemTypeID, unsigned int itemNum, unsigned int levelBegin, unsigned int levelEnd );
	///NPC取走玩家道具；
	int ExtRemoveItem( unsigned int itemTypeID, unsigned int itemNum );	
	///NPC取走scriptItem
	int ExtRemoveScriptItem();

	///NPC在特定位置内刷特定数量怪物；
	int ExtRefreshMonster( unsigned int monsterID, unsigned int monsterNum, unsigned short posX, unsigned short posY );
	///NPC指令玩家弹托盘；
	int ExtShowPlateCmd( int plateType );
	///NPC指令弹出玩家邮件界面
	int ExtShowEmailCmd();
	///NPC创建一个跟随玩家的NPC；
	int ExtCreatePlayerNpc( unsigned int npcType, unsigned int taskID );	
	///NPC检查是否有跟随玩家的NPC；
	bool ExtCheckPlayerNpc( unsigned int npcType );	
	///NPC删去跟随玩家的NPC；
	bool ExtDelPlayerNpc( unsigned int npcType );
	///属性=
	bool ExtNpcQueryPropertyEqual( unsigned int type,  int value);
	///属性<
	bool ExtNpcQueryPropertyLowerThan( unsigned int type,  int checkValue );
	///属性>
	bool ExtNpcQueryPropertyMoreThan( unsigned int type,  int checkValue );
	////显示保护托盘
	bool ExtNpcShowProtectItemPlate( unsigned int type );
	///显示ＮＰＣ商店；
	bool ExtNpcShowShop( unsigned int shopID );
	///玩家是否在骑乘状态
	bool ExtIsOnRideState();
	///给玩家发送系统邮件
	bool ExtSendMail( const char* pMailTitle, const char* pMailContent, unsigned int money, vector<unsigned int>& vecItemTypes, vector<int>& vecItemCounts );
	///弹出仓库界面
	bool ExtShowStorage();
	///弹出答题界面
	bool ExtBeginOLTest( unsigned int taskID );
	///是否能接取护送任务
	bool ExtCanGetNpcFollowTask();
#ifdef OPEN_PUNISHMENT
	//显示天谴玩家的位置
	bool ExtNpcShowPunishPlayerInfo();
	//给予天谴任务
	bool ExtGivePunishTask();	
	//是否在天谴活动中,接天谴任务的先置条件
	bool ExtIsInPunishment();
	//是否接过天谴任务
	bool ExtIsHavePunishTask();
	//天谴任务是否完成
	bool IsPunishTaskAchieve();
	//获取天谴杀人计数器
	int GetPunishCounter();
	//获取天谴的目标对象
	void GetPunishTargetName( string& targetName );
	//结束天谴任务
	int ExtFinishPunishTask();
#endif	//OPEN_PUNISHMENT
	///NPC指令切换地图
    bool ExtNpcSwitchMap( unsigned int mapID, unsigned short posX, unsigned short posY );
	///取对话对象玩家的昵称;
	const char* ExtGetPlayerName();
	//让该对话NPC消失(死亡，采集系用)
	bool ExtDisappearChatNpc( bool isDropItem );

	///玩家学习宠物技能；
    bool ExtNpcPetSkillLearn( unsigned int petSkillID );
	///玩家学习技能
	bool ExtNpcSkillLearn( unsigned int skillID );


	//09.04.28,取消工会任务新实现,//显示工会已发布任务
	//bool ExtDisplayUnionIssuedTask(void);
	//选择npc工会命令
	bool ExtSelectUnionCmd(unsigned int cmdID);
	//判断工会是否发布任务
	bool ExtHaveIssuedUnionTask();
	//是否公会会长
	bool ExtIsUnionLeader();
	//查询工会等级
	int ExtQueryUnionLevel();
	//显示NPC对话系统对话框
	void ExtNpcChatSysDialog( const char* showMsg );
	//查询是否是魔头
	bool ExtNpcIsEvil();
	//返回一个随即值
	int ExtNpcRandNumber( int a, int b);
	//增加种族声望
	bool ExtAddRacePrestige( unsigned int addVal );
	//创建BUFF
	bool ExtNpcCreateBuff( unsigned int buffId, unsigned int buffTime );
	//跳转到内城的npc
	bool ExtNpcSwitchRaceMap( unsigned int mapid, unsigned short x, unsigned short y );
#ifdef NEW_NPC_INTERFACE
	//1 判断是否加入了战盟
	bool ExtNpcIsInUnion();	

	//2 是否是战盟盟主
	bool ExtNpcIsUnionLeader();
	
	//3 判断是否为组队状态
	bool ExtNpcIsInTeam();

	//4 判断是否为队长
	bool ExtNpcIsTeamCaptain();

	//5 判断小队有几人
	bool ExtNpcTeamNumberMoreThan( unsigned );
	bool ExpNpcTeamNumberLowerThan( unsigned );
	bool ExtNpcTeamNumberEqual( unsigned );

	//6 判断好友有几人
	unsigned int ExtNpcGetFriendNumber();
	
	//7 判断仇人有几人
	unsigned int ExtNpcGetFoeNumber();

	bool ExtRandomMove();
#endif //NEW_NPC_INTERFACE

#ifdef ANTI_ADDICTION
	//是否在疲劳沉迷状态
	bool IsInTriedOrAddictionState();
#endif

	//保存复活点
	bool ExtNpcSaveRebirthPos(unsigned short leftOfRanage, unsigned short topOfRanage,unsigned short rightOfRanage, unsigned short bottomOfRanage);

	//从对话NPC处生成特定的怪物
	bool ExtNpcCreateNpcByChatTarget( unsigned int npcTypeId, unsigned int npcNum );
	
	bool AddMark( unsigned int mark );	
	bool CheckMark( unsigned int mark );

	bool CheckExTime( unsigned int time );
	bool CheckTime( unsigned int time1, unsigned int time2 );

	//弹出族长操作
	bool ExtShowRaceMasterPlate();

	//查询某伪副本是否可进
	bool ExtIsPCopyOpen( unsigned short pcopymapid );

	//地图跳转点种族归属是否和指定种族一致
	bool ExtCheckMapSwitchRace( long mapID );

	//发送在制定玩家制定屏幕位置公告
	bool ExtShowNotice( const char* content, char position, char isglobal, unsigned int maxshowcount, unsigned int distancetime );

	//在制定屏幕位置弹出可变参数公告
	bool ExtShowParamNotice( unsigned int msgid, char position, char isglobal,unsigned int maxshowcount, unsigned int distancetime, unsigned int validparam,const char* param1,const char* param2,
		const char* param3,const char* param4,const char* param5 );

	//玩家是否拥有这个技能
	bool ExtIsHaveSkill( unsigned int skillID );

	//运行对应的掉落集合
	bool ExtExecDropSetID( unsigned int dropsetID );

	//获取玩家的位置信息
	bool ExtGetPlayerPosInfo( unsigned& mapid, unsigned& posX, unsigned& posY );

	//吸引玩家周围怪
	bool ExtPlayerCallMonster( unsigned int num, unsigned int range );

	//玩家随机移动
	bool ExtPlayerRandMove();

	//显示玩家的拍卖行
	bool ExtShowAuctionPlate();

private:
	bool ReadScriptID()
	{
		CLuaRestoreStack rs( m_pLuaState );

		lua_getglobal( m_pLuaState, "nScriptID" );
		if ( ! lua_isnumber( m_pLuaState, -1 ) )
		{
			return false;
		}		

		m_ID = (int) lua_tonumber( m_pLuaState, -1 );

		return true;

	}

	//本函数以后可以改为同类型脚本只读一次；
	bool GetStartStage( int& nStage )
	{
		CLuaRestoreStack rs( m_pLuaState );

		lua_getglobal( m_pLuaState, "start" );
		if ( ! lua_isnumber( m_pLuaState, -1 ) )
		{
			return false;
		}		

		nStage = (int) lua_tonumber( m_pLuaState, -1 );

		return true;
	}

	///根据当前stage调用脚本OnOption；
	bool CallScriptOnOption( CPlayer* pOwner, int nOption );

	///根据当前stage调用脚本OnEnter;
	bool CallScriptOnNpcChat( CPlayer* pOwner );

	///根据当前stage调用脚本OnEnter;
    bool CallScriptOnUseItem( CPlayer* pOwner );

	//根据当前的stage调用脚本的OnEnter
	bool CallScriptOnAreaChat( CPlayer* pOwner );

	///根据当前stage调用脚本OnMeetCounter;
    bool CallScriptOnMeetCounter( CPlayer* pOwner );

	//09.04.28,取消工会任务新实现,///根据当前stage调用脚本的OnEnter;
	//bool CallScriptOnTaskChat( CPlayer* pOwner );

	///检测脚本当前stage的所有状态转换条件，如果需要转换，则执行转换；
	///本函数供脚本在适当时候调用；
	bool CallScriptCheckTransCon( CPlayer* pOwner );

	///检测脚本链接函数，找到转换目标执行转换,新条件转移机制；
	bool NewCallScriptCheckTransCon( CPlayer* pOwner );

private:
	unsigned long m_ID;//对话脚本ID号；
	CPlayer* m_pScriptUser;//当前正在与本脚本对话的玩家，或当前使用道具的玩家；
	stringstream m_strUiInfo;//当前脚本对话信息；
	SCRIPT_TYPE  m_scriptType;//脚本类型；
};


class CNormalScriptManager
{
private:
	//屏蔽这两个函数；
	CNormalScriptManager()
	{
	}
	~CNormalScriptManager();

public:
	static void Release()
	{
		for ( map<unsigned long, CNormalScript*>::iterator iter = m_mapDwordFsms.begin(); 
			iter != m_mapDwordFsms.end(); ++iter )
		{
			if ( NULL != iter->second )
			{
				delete iter->second; iter->second = NULL;
			}
		}
		m_mapDwordFsms.clear();
		m_mapLuaStateFsms.clear();

		//09.04.28,取消工会任务新实现,//释放任务脚本；
		//for ( map<unsigned long, CNormalScript*>::iterator iter = m_mapDwordTaskFsms.begin(); 
		//	iter != m_mapDwordTaskFsms.end(); ++iter )
		//{
		//	delete iter->second; iter->second = NULL;
		//}
		//m_mapDwordTaskFsms.clear();
		//m_mapLuaStateTaskFsms.clear();
	}

	///初始化管理器;
	static bool Init()
	{
		return true;
	}

public:
	///从指定路径装入脚本;
	static CNormalScript* LoadScript( const char* scriptPath/*09.04.28,取消工会任务新实现,, bool isTaskScript=false*/ )
	{
		if ( NULL == scriptPath )
		{
			return false;
		}
		CNormalScript* pNormalScript = NEW CNormalScript;
		if ( NULL == pNormalScript )
		{
			return false;
		}
		bool bRet = pNormalScript->InitNormalScript( scriptPath );
		if( !bRet )
		{
			delete pNormalScript; pNormalScript = NULL;
			return NULL;
		}
		//09.04.28,取消工会任务新实现,if ( isTaskScript )
		//{
		//	AddTaskScript( pNormalScript );
		//} else {
			AddScript( pNormalScript );
		//09.04.28,取消工会任务新实现,}
		return pNormalScript;
	}

private:
	///向管理器中添加新脚本；
	static bool AddScript( CNormalScript* pScript )
	{
		if ( NULL == pScript )
		{
			return false;
		}

		map<unsigned long, CNormalScript*>::iterator iter = m_mapDwordFsms.find( pScript->GetID() );
		if ( iter != m_mapDwordFsms.end() )
		{
			//原来就有该脚本；
			D_WARNING( "CNormalScriptManager::AddScript,重复装载脚本%d\n", pScript->GetID() );
			return true;
		} else {			
			m_mapDwordFsms.insert( pair<unsigned long, CNormalScript*>( pScript->GetID(), pScript ) );
			m_mapLuaStateFsms.insert( pair<lua_State*, CNormalScript*>( pScript->GetState(), pScript ) );
		}
		return true;
	}

	//09.04.28,取消工会任务新实现,static bool AddTaskScript( CNormalScript* pTaskScript )
	//{
	//	if ( NULL == pTaskScript )
	//	{
	//		return false;
	//	}

	//	map<unsigned long, CNormalScript*>::iterator iter = m_mapDwordTaskFsms.find( pTaskScript->GetID() );
	//	if ( iter != m_mapDwordTaskFsms.end() )
	//	{
	//		//原来就有该脚本；
	//		D_WARNING( "CNormalScriptManager::AddTaskScript,重复装载任务脚本%d\n", pTaskScript->GetID() );
	//		return true;
	//	} else {			
	//		m_mapDwordTaskFsms.insert( pair<unsigned long, CNormalScript*>( pTaskScript->GetID(), pTaskScript ) );
	//		m_mapLuaStateTaskFsms.insert( pair<lua_State*, CNormalScript*>( pTaskScript->GetState(), pTaskScript ) );
	//	}
	//	return true;
	//}

public:
	/*
            // NPC对话：1 + 5位NPCID + 3位流水号
            // NPC死亡：2 + 5位NPCID + 3位流水号
            // 怪物死亡：3 + 怪物ID后5位 + 3位流水号
            // 物品脚本：4 + ItemID后5位 + 3位流水号
            // 区域脚本：5 + 6位区域ID + 2位流水号
	*/
#define NPCSCRIPT_FIRSTNUM 1        //npc聊天脚本ID号首数字；
#define NPC_DIESCRIPT_FIRSTNUM 2     //npc死亡脚本ID号首数字；
#define MONSTER_DIESCRIPT_FIRSTNUM 3 //npc死亡脚本ID号首数字；
#define ITEMSCRIPT_FIRSTNUM 4    //挂item脚本ID号首数字；
#define REGIONSCRIPT_FIRSTNUM 5    //挂区域脚本ID号首数字；
#define COUNTERSCRIPT_FIRSTNUM 6 //挂计数器脚本ID号首数字；
	///根据脚本ID号寻找脚本；
	static CNormalScript* FindScripts( unsigned long scriptID )
	{
		TRY_BEGIN;

		map<unsigned long, CNormalScript*>::iterator iter = m_mapDwordFsms.find( scriptID );
		if ( iter != m_mapDwordFsms.end() )
		{
			return iter->second;
		} else {
			//根据第一位判断脚本类型，并找到相应的脚本目录，检查是否有相应脚本文件，如果有，则载入；
			if ( scriptID < 1000 )
			{
				return NULL;
			}
			stringstream strID;
			strID << scriptID;
			unsigned int idlen = (int) ( strID.str().length() );
			if ( 0 == idlen )
			{
				return NULL;
			}
			unsigned long firstNum = 0;
			if ( 1 == idlen )
			{
				firstNum = scriptID;
			} else {
				--idlen;
				firstNum = scriptID / (unsigned int) ( pow((float)10, (int)idlen) );
			}

			strID.str( "" );
			strID.clear();
			/*
			#define NPCSCRIPT_FIRSTNUM 1        //npc聊天脚本ID号首数字；
			#define NPC_DIESCRIPT_FIRSTNUM 2     //npc死亡脚本ID号首数字；
			#define MONSTER_DIESCRIPT_FIRSTNUM 3 //npc死亡脚本ID号首数字；
			#define ITEMSCRIPT_FIRSTNUM 4    //挂item脚本ID号首数字；
			#define REGIONSCRIPT_FIRSTNUM 5    //挂item脚本ID号首数字；
			#define COUNTERSCRIPT_FIRSTNUM 6 //挂计数器脚本ID号首数字；
			*/
			if ( NPCSCRIPT_FIRSTNUM == firstNum )
			{
				//npc相关脚本；
				strID << "config/scripts/chatscripts/npc" << scriptID << ".lua";
			} else if ( ( MONSTER_DIESCRIPT_FIRSTNUM == firstNum ) 
				        || ( NPC_DIESCRIPT_FIRSTNUM == firstNum ) ){
				//怪物死亡相关脚本；
				strID << "config/scripts/chatscripts/death" << scriptID << ".lua";
			} else if ( ITEMSCRIPT_FIRSTNUM == firstNum ) {
				//挂item脚本
				strID << "config/scripts/itemscripts/item" << scriptID << ".lua";
			} else if ( REGIONSCRIPT_FIRSTNUM == firstNum ) {
				strID << "config/scripts/areascripts/area" << scriptID << ".lua";
			} else if ( COUNTERSCRIPT_FIRSTNUM == firstNum ) {
				//挂计数器脚本
				strID << "config/scripts/counterscripts/counter" << scriptID << ".lua";
			} else {
				return NULL;
			}

			CNormalScript* pNormalScript = LoadScript( strID.str().c_str() );
			if ( NULL != pNormalScript )
			{
				if ( ( NPCSCRIPT_FIRSTNUM == firstNum )
					|| ( NPC_DIESCRIPT_FIRSTNUM == firstNum )
					|| ( MONSTER_DIESCRIPT_FIRSTNUM == firstNum )
					)
				{
					//npc聊天脚本；
					pNormalScript->SetScriptTypeNpc();
				} else if ( ITEMSCRIPT_FIRSTNUM == firstNum ) {
					//挂item脚本
					pNormalScript->SetScriptTypeItem();
				} else if ( REGIONSCRIPT_FIRSTNUM == firstNum ) {
					pNormalScript->SetScriptTypeArea();
				} else {
				}
			}
			strID.str( "" );
			return pNormalScript;
		}

		return NULL;
		TRY_END;
		return NULL;
	}

	//09.04.28,取消工会任务新实现,///根据任务ID号寻找任务脚本；
	//static CNormalScript* FindTaskScripts( unsigned long scriptID )
	//{
	//	TRY_BEGIN;

	//	map<unsigned long, CNormalScript*>::iterator iter = m_mapDwordTaskFsms.find( scriptID );
	//	if ( iter != m_mapDwordTaskFsms.end() )
	//	{
	//		return iter->second;
	//	} else {
	//		//根据第一位判断脚本类型，并找到相应的脚本目录，检查是否有相应脚本文件，如果有，则载入；
	//		if ( scriptID < 1000 )
	//		{
	//			return NULL;
	//		}

	//		stringstream strID;
	//		strID.str( "" );
	//		strID.clear();

	//		//固定装载任务脚本；
	//		strID << "config/scripts/taskscripts/task" << scriptID << ".lua";

	//		CNormalScript* pNormalScript = LoadScript( strID.str().c_str(), true );
	//		if ( NULL != pNormalScript )
	//		{
	//			//挂item脚本
	//			pNormalScript->SetScriptTypeTask();
	//		}
	//		strID.str( "" );
	//		return pNormalScript;
	//	}

	//	return NULL;
	//	TRY_END;
	//	return NULL;
	//}

	///根据lua state寻找脚本；
	static CNormalScript* FindScripts( lua_State* inState )
	{
		if ( NULL == inState )
		{
			return NULL;
		}

		map<lua_State*, CNormalScript*>::iterator iter = m_mapLuaStateFsms.find( inState );
		if ( iter != m_mapLuaStateFsms.end() )
		{
			return iter->second;
		} else {
			return NULL;
		}
	}

	//09.04.28,取消工会任务新实现,///根据lua state寻找任务脚本；
	//static CNormalScript* FindTaskScripts( lua_State* inState )
	//{
	//	if ( NULL == inState )
	//	{
	//		return NULL;
	//	}

	//	map<lua_State*, CNormalScript*>::iterator iter = m_mapLuaStateTaskFsms.find( inState );
	//	if ( iter != m_mapLuaStateTaskFsms.end() )
	//	{
	//		return iter->second;
	//	} else {
	//		return NULL;
	//	}
	//}

private:
	static map<unsigned long, CNormalScript*> m_mapDwordFsms;//脚本ID号<---->脚本指针映射;
	static map<lua_State*, CNormalScript*> m_mapLuaStateFsms;//lua状态<---->脚本指针映射;

//09.04.28,取消工会任务新实现,private:
//	static map<unsigned long, CNormalScript*> m_mapDwordTaskFsms;//脚本ID号<---->脚本指针映射;
//	static map<lua_State*, CNormalScript*> m_mapLuaStateTaskFsms;//lua状态<---->脚本指针映射;
};

