﻿/**
* @file FightCalScript.h
* @brief 升级脚本
* Copyright(c) 2009,上海第九城市游戏研发部
* All rights reserved
* 文件名称: LevelUpScript.h
* 摘    要: 升级脚本
* 作    者: Hyn
* 完成日期: 2009.08.05
*
*/


#ifndef LEVELUPSCRIPT_H
#define LEVELUPSCRIPT_H

#include "Utility.h"

#ifdef LEVELUP_SCRIPT

#include "LuaScript.h"
#include <vector>
#include <map>
using namespace std;

class CPlayer;
class CPlayerSkill;

class CLevelUpScript : public CLuaScriptBase
{
	friend class CLevelUpScriptManager;

	private:
	CLevelUpScript( const CLevelUpScript& );  //屏蔽这两个操作；
	CLevelUpScript& operator = ( const CLevelUpScript& );//屏蔽这两个操作；

private:
	CLevelUpScript():m_pScriptUser(NULL) {};
	virtual ~CLevelUpScript (void);

	bool Init( const char* luaScriptFile );

public:
	bool OnPlayerLevelUp( CPlayer* pLevelUpPlayer );		//调用LUA脚本

	bool OnPlayerSetBaseProp( CPlayer* pLevelUpPlayer );	//调用LUA脚本

private:
	///重置参与者，由于主逻辑为单线程，因此各次战斗判断调用顺序进行，每次新的战斗判断都清之前的战斗判断参与者；
	void ResetPaticipant()
	{
		m_pScriptUser = NULL;
	}

	///调用升级脚本
	bool CallScriptOnPlayerLevelUp( unsigned short level );

	//调用设置基本属性脚本
	bool CallScriptOnPlayerSetBaseProp( unsigned short level );

private:
	///脚本扩展函数调用，返回调用脚本的玩家；
	CPlayer* GetScriptUser() { return m_pScriptUser; }

//具体回调的操作,比如设血,魔,攻击力,防御力等等
public:
	//发送系统消息
	bool ExtSendSystemMsg( const char* pMsg );
	//设置最大基础hp
	bool ExtLevelSetBaseMaxHp( unsigned int maxHp );
	//设置最大基础mp
	bool ExtLevelSetBaseMaxMp( unsigned int maxMp );
	//设置力量
	bool ExtLevelSetStr( unsigned int baseStr );
	//设置敏捷
	bool ExtLevelSetAgi( unsigned int baseAgi );
	//设置智力
	bool ExtLevelSetInt( unsigned int baseInt );
	//设置基础物理攻击力
	bool ExtLevelSetBasePhyAtk( unsigned int basePhyAtk );
	//设置基础魔法攻击力
	bool ExtLevelSetBaseMagAtk( unsigned int baseMagAtk );
	//设置基础物理防御力
	bool ExtLevelSetBasePhyDef( unsigned int basePhyDef );
	//设置基础魔法防御力
	bool ExtLevelSetBaseMagDef( unsigned int baseMagDef );
	//设置基础物理命中
	bool ExtLevelSetBasePhyHit( unsigned int basePhyHit );
	//设置基础魔法命中
	bool ExtLevelSetBaseMagHit( unsigned int baseMagHit );
	//设置下级经验
	bool ExtLevelSetNextLevelExp( unsigned int nextLevelExp );
	//添加技能
	bool ExtLevelAddSkill( unsigned int skillID );
	//添加道具
	bool ExtLevelAddItem( unsigned int itemID, unsigned int itemNum );
	//添加一个buff
	bool ExtLevelAddBuff( unsigned int buffID, unsigned int buffTime );
	//添加sp豆
	bool ExtLevelAddSpBean( unsigned int spBeanNum );
	//获取种族
	unsigned short ExtLevelRace();
	//获取性别
	unsigned char ExtLevelSex();
	//赠送邮件
	bool ExtSendMail( const char* pMailTitle, const char* pMailContent, unsigned int money, vector<unsigned int>& vecItemTypes, vector<int>& vecItemCounts );

	bool ExtShowNotice( const char* content, char position, char isglobal, unsigned int maxshowcount, unsigned int distancetime );
	//在制定屏幕位置弹出可变参数公告
	bool ExtShowParamNotice( unsigned int msgid, char position, char isglobal,unsigned int maxshowcount, unsigned int distancetime,
		unsigned int validparam,const char* param1,const char* param2, const char* param3,const char* param4,const char* param5 );

private:
	CPlayer* m_pScriptUser;  //升级玩家；
};


class CLevelUpScriptManager
{
private:
	CLevelUpScriptManager();//屏蔽此构造;
	~CLevelUpScriptManager();//屏蔽此析构;
public:
	///从配置中读入脚本;
	static bool Init();

	static bool AddScript( unsigned int jobID, CLevelUpScript*  pScript );
	
	//释放管理的脚本；
	static void Release()
	{ 
		CLevelUpScript* pScript = NULL;
		for( map<unsigned int,CLevelUpScript*>::iterator iter = m_mapLevelUpScript.begin(); iter != m_mapLevelUpScript.end(); ++iter )
		{
			pScript = iter->second;
			if( NULL != pScript )
			{
				delete pScript; pScript = NULL;
			}
		}
		m_mapLevelUpScript.clear();
		m_mapLuaStateFsms.clear();
	}

	
	///寻找对应职业,找到对应的脚本
	static CLevelUpScript* FindScripts( unsigned int jobID )
	{
		map<unsigned int,CLevelUpScript*>::iterator iter = m_mapLevelUpScript.find( jobID );
		if( iter != m_mapLevelUpScript.end() )
		{
			return iter->second;
		}
		return NULL;
	};

	///寻找对应luastate的脚本，用于luaCallBack；
	static CLevelUpScript* FindScripts( lua_State* inState )
	{
		if ( NULL == inState )
		{
			return NULL;
		}

		map<lua_State*, CLevelUpScript*>::iterator iter = m_mapLuaStateFsms.find( inState );
		if ( iter != m_mapLuaStateFsms.end() )
		{
			return iter->second;
		}
		return NULL;
	}

private:
	static map<unsigned int,CLevelUpScript*> m_mapLevelUpScript;	//职业ID<---->脚本指针映射
	static map<lua_State*, CLevelUpScript*>  m_mapLuaStateFsms;		//lua状态<---->脚本指针映射;
	
};

#endif	//LEVELUP_SCRIPT 

#endif	//LEVELUPSCRIPT_H
