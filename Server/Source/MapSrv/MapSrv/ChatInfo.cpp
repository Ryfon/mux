﻿#include "ChatInfo.h"
#include "Player/Player.h"
#include "monster/monster.h"
#include "Item/CSpecialItemProp.h"

//09.04.28,取消工会任务新实现,void TaskChatInfo::ClearTaskChatTarget()
//{
//	m_taskID = 0;
//	m_CurStage = 0ul;
//	m_pScript = NULL;
//}
//
/////设置交谈目标；
//bool TaskChatInfo::SetTaskChatTarget( unsigned int taskid, int nOption, bool isSrvSet/*是否服务器端主动调用，还是客户端发来消息导致调用*/  )
//{
//	//如果原来没有交谈对象，或者原来的交谈对象不是pMonster，则重置交谈对象并置stage为0;
//	if ( ( 0 == taskid )
//		|| ( taskid != m_taskID )
//		|| ( nOption == 0 ) //0表示重新点了任务对话,因为有可能客户端在谈话途中关闭了对话,稍后再次激活同一任务对话,如果不重设,则选项0会导致对话再也无法进行;
//		)
//	{
//		if ( !isSrvSet )
//		{
//			///客户端不能主动开始新任务对话，只能服务器端主动发起；
//			return false;
//		}
//		ClearTaskChatTarget();
//		m_taskID = taskid;
//		SetTaskChatStage( 0 );
//		m_pScript = CNormalScriptManager::FindTaskScripts( taskid );
//	}
//
//	return false;
//}
//
/////选项选择，nOption==0表示初始选择NPC；
//bool TaskChatInfo::OnTaskChatOption( CPlayer* pOwner, int nOption )
//{
//	if ( ( 0 == m_taskID )
//		|| ( NULL == m_pScript )
//		)
//	{
//		//交谈对象空；
//		return false;
//	}
//	return m_pScript->OnTaskChat( pOwner, nOption );
//}

void NpcChatInfo::ClearNpcChatTarget()
{
	if ( NULL != pChatTarget )
	{
		//pChatTarget->NotifyRefDeled( this );
		pChatTarget = NULL;
	}
}

///设置交谈目标；
void NpcChatInfo::SetNpcChatTarget( CMonster* pMonster, int nOption )
{
	//如果原来没有交谈对象，或者原来的交谈对象不是pMonster，则重置交谈对象并置stage为0;
	if ( ( NULL == pChatTarget )
		|| ( pChatTarget != pMonster )
		|| ( nOption == 0 ) //0表示重新点了该NPC,因为有可能客户端在谈话途中关闭了对话,稍后再次点击同一NPC,如果不重设,则选项0会导致对话再也无法进行;
		)
	{
		ClearNpcChatTarget();
		pChatTarget = pMonster;//->GetRefPtr( this );
		SetNpcChatStage( 0 );
	}
}

///选项选择，nOption==0表示初始选择NPC；
bool NpcChatInfo::OnNpcChatOption( CPlayer* pOwner, int nOption )
{
	if ( NULL == pChatTarget )
	{
		//交谈对象空；
		return false;
	}
	return pChatTarget->OnChatOption( pOwner, nOption );
}

void ItemChatInfo::ClearItemChatTarget()
{
	if ( NULL != pChatTarget )
	{
		//pChatTarget->NotifyRefDeled( this );
		pChatTarget = NULL;
	}
}

///设置交谈目标；
void ItemChatInfo::SetItemChatTarget( CBaseItem* pItem, int nOption )
{
	//如果原来没有交谈对象，或者原来的交谈对象不是pMonster，则重置交谈对象并置stage为0;
	if ( ( NULL == pChatTarget )
		|| ( pChatTarget != pItem )
		|| ( nOption == 0 ) //0表示重新点了该NPC,因为有可能客户端在谈话途中关闭了对话,稍后再次点击同一NPC,如果不重设,则选项0会导致对话再也无法进行;
		)
	{
		ClearItemChatTarget();
		pChatTarget = pItem;
		SetItemChatStage( 0 );
	}
}

///选项选择，nOption==0表示初始选择NPC；
bool ItemChatInfo::OnItemChatOption( CPlayer* pOwner, int nOption )
{
	if ( NULL == pChatTarget )
	{
		//交谈对象空；
		return false;
	}
	return pChatTarget->OnChatOption( pOwner, nOption );
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//区域任务

void AreaTaskChatInfo::SetAreaChatTarget( int nOption)
{
	//如果原来没有交谈对象，或者原来的交谈对象不是pMonster，则重置交谈对象并置stage为0;
	if (  ( nOption == 0 ) //0表示重新点了该NPC,因为有可能客户端在谈话途中关闭了对话,稍后再次点击同一NPC,如果不重设,则选项0会导致对话再也无法进行;
	   )
	{
		SetAreaTaskStage( 0 );
	}

	//SetAreaTaskStage( nOption );
}

bool AreaTaskChatInfo::OnAreaTaskOption(CPlayer* pOwner, int nOption )
{
	if ( !pOwner )
		return false;

	CNormalScript* pScript = pOwner->GetAreaTaskScript();
	if ( pScript )
	{
		pScript->OnAreaChat( pOwner, nOption );
		return true;
	}

	return false;
}

