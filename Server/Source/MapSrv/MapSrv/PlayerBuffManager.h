﻿#pragma once

#include "MuxBuffEle.h"
#include "BattleUtility.h"
#include "PkgProc/SrvProtocol.h"


class CPlayerBuffManager
{
	friend class CPlayer;
	#define MAX_BUFF_SIZE 32 //最大BUFF数量
	
public:
	CPlayerBuffManager(void);
	~CPlayerBuffManager(void);

	void AttachOwner( CPlayer* pPlayer );	//设置宿主

	//是否有控制BUFF
	bool IsHaveControlBuff();
	
	void SetFaint( bool bFlag );	//设置晕旋
	bool IsFaint();					//是否晕旋
	
	bool IsBondage();	//是否束缚
	void SetBondage( bool bFlag );	//设置束缚
	
	void SetInvicible( bool bFlag );
	bool IsInvicible();	//是否无敌

	//是否在自爆状态
	bool IsSelfExplosion();	
	void SetSelfExplosionInfo( bool bFlag, unsigned int arg );
	unsigned int GetSelfExplosionArg();

	//设置减速标志
	void SetSpeedDown( bool bFlag );
	bool IsSpeedDown();

	//设置加速标志
	void SetSpeedUp( bool bFlag );
	bool IsSpeedUp();

	//增加SP速度
	bool IsSpPowerSpeedUp();
	void SetSpPowerSpeedUp( bool bFlag );

	//移动免疫
	bool IsMoveImmunity();
	void SetMoveImmunity( bool bFlag );

	void SetAbsorbDamage( unsigned int damage );	//设置吸收伤害量
	unsigned int GetAbsorbDamage()
	{
		return m_buffAffect.m_nAbsorbDamage;
	}

	bool AbsorbCheck( unsigned int& damage );
	
	bool IsHaveBuff();

	bool IsInAbsorbState();

	inline void AddBuffNum()
	{
		m_buffNum++;
	}

	inline void SubBuffNum()
	{
		m_buffNum--;
	}

	//是否能上骑乘
	bool IsCanOnMount();

	//死亡时的BUFF处理
	void OnBuffDie();

	bool CreateMuxBuff( MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID );
	bool DeleteMuxBuff( unsigned int buffID );		//结束一种类型的buff

	void BuffProcess();
	void UpdateBuffData( PlayerInfo_5_Buffers& data, unsigned int saveType  ); // -1 离线时处理 -0 平时的处理

	bool IsPosValid( unsigned int index )
	{
		if( index >= ARRAY_SIZE( m_pPlayerBuffs ) )
		{
			D_ERROR("IsBuffValid index超过m_pPlayerBuffs size");
			return false;
		}

		return (m_buffPosFlag>>index)&0x01;
	}

	void SetPosValid( unsigned int index )
	{
		if( index >= ARRAY_SIZE( m_pPlayerBuffs ) )
		{
			D_ERROR("SetPosValid index超过m_pPlayerBuffs size");
			return;
		}

		m_buffPosFlag |= 0x01<<index;
	}
	
	void SetPosInValid( unsigned int index )
	{
		if( index >= ARRAY_SIZE( m_pPlayerBuffs ) )
		{
			D_ERROR("SetPosInValid index超过m_pPlayerBuffs size");
			return;
		}

		m_buffPosFlag &= ~(0x01<<index);
	}

	bool FindInvalidIndex( unsigned int& index)
	{
		for( unsigned int i=0; i < ARRAY_SIZE(m_pPlayerBuffs); ++i )
		{
			if( !IsPosValid(i) )
			{
				index = i;
				return true;
			}
		}
		return false;
	}

	bool ActiveEndBuff( unsigned int buffIndex );


	//封装下开始结束的处理
	bool PlayerBuffStartWrapper( CMuxBuffEle* pBuffEle, unsigned int buffIndex, MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID );
	bool PlayerBuffEndWrapper( CMuxBuffEle* pBuffEle, unsigned int buffIndex );
	
	//新版buff
	void AddMuxBuffEffect( MuxBuffProp* pBuffProp );
	void AddSingleMuxBuffEffect( MuxBuffProp* pBuffProp );
	void DelMuxBuffEffect( MuxBuffProp* pBuffPro, unsigned int buffLevel );
	void DelSingleMuxBuffEfect( MuxBuffProp* pBuffProp, unsigned int buffLevel );


	//获得属性
	int GetBuffAgi();
	int GetBuffAoeRange();
	int GetBuffInt();
	int GetBuffStr();
	int GetBuffPhyAtk();
	int GetBuffMagAtk();
	int GetBuffPhyDef();
	int GetBuffMagDef();
	int GetBuffPhyHit();
	int GetBuffMagHit();
	float GetBuffSpeedAffect();
	float GetBuffSpPowerUpRate();
	int	  GetBuffMaxHpUp();			
	float GetBuffBaseMaxHpUp();
	int GetBuffPhyCriLevel();
	int GetBuffMagCriLevel();
	int GetBuffPhyCriDamAdd();
	int GetBuffMagCriDamAdd();
	float GetBuffBaseHpUp();
	unsigned int GetDrArg();

	int GetBuffAtkDistance()
	{
		return m_buffAffect.nAddAtkDistance;
	}

	float GetDotDmgUp()
	{
		return m_buffAffect.fDotDmgUp;
	}

	AttributeEffect* GetBuffEffect()
	{
		return &m_buffAffect;
	}

	float GetSkillAddBondageRate()	//获得技能增加束缚几率
	{
		return m_buffAffect.fAddBondageRate;
	}


	float GetSkillAntiBondageRate()		//获得技能抗束缚几率
	{
		return m_buffAffect.fResistBondageRate;
	}

	float GetSkillAddFaintRate()   //获得增加的击晕几率
	{
		return m_buffAffect.fAddFaintRate;
	}

	float GetSkillAntiFaintRate()
	{
		return m_buffAffect.fResistFaintRate;
	}

	unsigned int GetBuffTimesExp()
	{
		return m_buffAffect.timesExp;
	}

	unsigned int GetBuffTimesPhyMagExp()
	{
		return m_buffAffect.timesPhyMagExp;
	}

	bool IsDamageRebound()
	{
		return m_buffAffect.m_bDamageRebound;
	}

	//设置伤害反弹属性
	void SetDamageReboundInfo( bool bFlag, unsigned int buffID );

	//是否变羊
	bool IsSheep()
	{
		return m_buffAffect.isSheep;
	}

	//是否睡眠
	bool IsInSleep()
	{
		return m_buffAffect.isSleep;
	}

	//删除加速buff
	bool EndSpeedUpBuff();

	//删除控制buff
	void EndControlBuff();

	//删除所有debuff
	void EndAllDebuff();

	//删除某个type的buff
	bool EndBuffByType( unsigned int buffType );
	//删除某个特定ID的buff
	bool EndBuffByID( unsigned int buffID );


	bool EndAbsorbBuff();
	//结束变羊debuff
	bool EndSheepDebuff();
	//结束休息buff
	bool ActiveEndRestBuff();

	//获得经验值倍数
	unsigned int GetTimesExp()
	{
		return m_buffAffect.timesExp;
	}

	//获得熟练度倍数
	unsigned int GetTimesPhyMagExp()
	{
		return m_buffAffect.timesPhyMagExp;
	}
	
	//虚弱buff
	bool EndWeakBuff();

	//解除性BUFF
	bool RelieveBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve/*是否只是探测可能性*/ );

	void GetSwitchMapBuffData( MGTempBuffData& bfData );

private:
	CMuxBuffEle m_pPlayerBuffs[MAX_BUFF_SIZE];		//最大的ELE数组
	unsigned int m_buffPosFlag;						//记录buff中的有效栏位
	CPlayer* m_pOwner;								//管理器的寄主
	unsigned int m_buffNum;							//BUFF数量
	
	AttributeEffect m_buffAffect;			//buff属性
};

