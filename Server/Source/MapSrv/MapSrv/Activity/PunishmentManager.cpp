﻿#include "PunishmentManager.h"
#include <ctime>
#include "LoadConfig/LoadSrvConfig.h"
#include "../PlayerManager.h"
#include "../Player/Player.h"
#include "../MuxMap/muxmapbase.h"
#include "PunishTask.h"

bool CPunishmentManager::m_bIsInPunishTime = false;  //表明是否在天谴时间之内
bool CPunishmentManager::m_bIsPunishNotified = false; //表明是否已经通知天谴开始
std::vector<CPunishmentManager::PunishTime> CPunishmentManager::m_vecPunishTime;	//每天的天谴时间保留在内
ACE_Time_Value CPunishmentManager::m_punishStartTime = ACE_Time_Value::zero;	//天谴的启动时间，用来检查可以在何时结束天谴
ACE_Time_Value CPunishmentManager::m_lastUpdateInfoTime = ACE_Time_Value::zero;	//定时发送位置信息的定时器
std::vector<CPunishmentManager::PunishPlayer> CPunishmentManager::m_vecHumanPunishPlayer;	//该次天谴中人类遭到天谴的玩家
std::vector<CPunishmentManager::PunishPlayer> CPunishmentManager::m_vecElfPunishPlayer;		//该次天谴中精灵遭到天谴的玩家
std::vector<CPunishmentManager::PunishPlayer> CPunishmentManager::m_vecEkkaPunishPlayer;	//该次天谴中艾卡遭到天谴的玩家
int CPunishmentManager::m_deadRate = 0;			//遭受天谴时死亡的概率
int CPunishmentManager::m_safeRate = 0;			//遭受天谴时安全的概率
int CPunishmentManager::m_upgradeRate = 0;		//遭受天谴时升级装备的概率

CPunishmentManager::CPunishmentManager(void)
{
}

CPunishmentManager::~CPunishmentManager(void)
{
}

void CPunishmentManager::EnablePunishState()	//设置天谴状态
{
	m_bIsInPunishTime = true;
}

void CPunishmentManager::DisablePunishState()	//取消天谴状态
{
	m_bIsInPunishTime = false;
}

bool CPunishmentManager::GetPunishState()
{
	return m_bIsInPunishTime;
}

bool CPunishmentManager::IsPunishmentStart()	//查询天谴是否开始，只有mapsrv0又权利执行再由gatesrv0来通知各个mapsrv天谴开始
{
 	if( GetPunishState() )
	{
		return false;	//在天谴状态中不需要判定 
	}

	if( m_vecPunishTime.empty() )
	{
		return false;	//没有配置天谴时间也不需要执行
	}

	int cyear=0, cmon=0, cday=0, chour=0, cmin=0, csec=0, cmsec=0, cwday=0;
	GetCurTime( cyear, cmon, cday, chour, cmin, csec, cwday, cmsec );
	//time_t currentTime = time( 0 );
	//tm* pSystemTime = localtime( &currentTime );
	for( std::vector<PunishTime>::iterator iter = m_vecPunishTime.begin(); iter != m_vecPunishTime.end(); ++iter )
	{
		if( chour == iter->hour && cmin == iter->minute )
		{
			return true;
		}
	}
	return false;
}


void CPunishmentManager::StartPunishment( const GMPunishmentStart* pStartInfo )		//开始天谴，收到GateSrv消息执行
{
	if ( NULL == pStartInfo )
	{
		return;
	}
	EnablePunishState();
	m_punishStartTime = ACE_OS::gettimeofday();		//记录下开始时间

	for( int i=0; i<pStartInfo->humanRace.playerNum && i<ARRAY_SIZE(pStartInfo->humanRace.player); ++i)
	{
		PunishPlayer victim;
		victim.playerUID = pStartInfo->humanRace.player[i].playerUID;
		victim.mapID = pStartInfo->humanRace.player[i].mapID;
		victim.posX = pStartInfo->humanRace.player[i].posX;
		victim.posY = pStartInfo->humanRace.player[i].posY;
		victim.playerName = pStartInfo->humanRace.player[i].name;
		victim.punishTime = pStartInfo->humanRace.player[i].punishTime;
		victim.bPunished = pStartInfo->humanRace.player[i].bPunished;
		m_vecHumanPunishPlayer.push_back( victim );
		D_DEBUG("%s玩家将在天谴开始后%d分钟受到天谴\n", victim.playerName.c_str(),  victim.punishTime );
	}

	for( int i=0; i<pStartInfo->elfRace.playerNum && i<ARRAY_SIZE(pStartInfo->elfRace.player); ++i)
	{
		PunishPlayer victim;
		victim.playerUID = pStartInfo->elfRace.player[i].playerUID;
		victim.mapID = pStartInfo->elfRace.player[i].mapID;
		victim.posX = pStartInfo->elfRace.player[i].posX;
		victim.posY = pStartInfo->elfRace.player[i].posY;
		victim.playerName = pStartInfo->elfRace.player[i].name;
		victim.punishTime = pStartInfo->elfRace.player[i].punishTime;
		victim.bPunished = pStartInfo->elfRace.player[i].bPunished;
		m_vecElfPunishPlayer.push_back( victim );
		D_DEBUG("%s玩家将在天谴开始后%d分钟受到天谴\n", victim.playerName.c_str(),  victim.punishTime );
	}

	for( int i=0; i<pStartInfo->ekkaRace.playerNum && i<ARRAY_SIZE(pStartInfo->ekkaRace.player); ++i)
	{
		PunishPlayer victim;
		victim.playerUID = pStartInfo->ekkaRace.player[i].playerUID;
		victim.mapID = pStartInfo->ekkaRace.player[i].mapID;
		victim.posX = pStartInfo->ekkaRace.player[i].posX;
		victim.posY = pStartInfo->ekkaRace.player[i].posY;
		victim.playerName = pStartInfo->ekkaRace.player[i].name;
		victim.punishTime = pStartInfo->ekkaRace.player[i].punishTime;
		victim.bPunished = pStartInfo->ekkaRace.player[i].bPunished;
		m_vecEkkaPunishPlayer.push_back( victim );
		D_DEBUG("%s玩家将在天谴开始后%d分钟受到天谴\n", victim.playerName.c_str(),  victim.punishTime );
	}

	UpdateAllPunishPlayerInfo();		//对该mapsrv上的玩家进行信息的更新并通知其他mapsrv
}


void CPunishmentManager::NotifyPunishPlayerInfo( const PunishPlayer& player )
{
	MGBroadcastPunishInfo srvmsg;
	srvmsg.punishPlayer.playerUID = player.playerUID;
	SafeStrCpy( srvmsg.punishPlayer.name, player.playerName.c_str() );
	srvmsg.punishPlayer.bPunished = player.bPunished;
	srvmsg.punishPlayer.posX = player.posX;
	srvmsg.punishPlayer.posY = player.posY;
	srvmsg.punishPlayer.mapID = player.mapID;
	srvmsg.punishPlayer.punishTime = player.punishTime;
	//群发消息只发给GateSrv0
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("CPunishmentManager::NotifyPunishPlayerInfo 找不到GateSrv0 不能发放消息\n");
		return;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( MGBroadcastPunishInfo, pGateSrv, srvmsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
}


void CPunishmentManager::UpdatePunishPlayerInfo( const PunishPlayer& player )		//更新此mapsrv上在线天谴玩家的信息至其他mapsrv
{
	CPlayer* pPunishPlayer = CPlayerManager::FindPlayerByRoleName( player.playerName );
	if( NULL == pPunishPlayer )
	{
		return;
	}

	TYPE_POS x = 0, y=0;
	pPunishPlayer->GetPos( x, y );
	MGBroadcastPunishInfo srvmsg;
	srvmsg.punishPlayer.playerUID = pPunishPlayer->GetDBUID();
	SafeStrCpy( srvmsg.punishPlayer.name, player.playerName.c_str() );
	srvmsg.punishPlayer.bPunished = player.bPunished;
	srvmsg.punishPlayer.posX = x;
	srvmsg.punishPlayer.posY = y;
	srvmsg.punishPlayer.mapID = pPunishPlayer->GetMapID();
	srvmsg.punishPlayer.punishTime = player.punishTime;
	//群发消息只发给GateSrv0
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("CPunishmentManager::NotifyPunishPlayerInfo 找不到GateSrv0 不能发放消息\n");
		return;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( MGBroadcastPunishInfo, pGateSrv, srvmsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
}


void CPunishmentManager::UpdateAllPunishPlayerInfo()	//对所有mapsrv上的玩家进行信息的更新
{
	//如果天谴玩家在本mapsrv上，通知其他mapsrv他的信息
	for( size_t i = 0; i< m_vecHumanPunishPlayer.size(); ++i )
	{
		UpdatePunishPlayerInfo( m_vecHumanPunishPlayer[i] );
	}

	for( size_t i = 0; i< m_vecElfPunishPlayer.size(); ++i )
	{
		UpdatePunishPlayerInfo( m_vecElfPunishPlayer[i] );
	}

	for( size_t i = 0; i< m_vecEkkaPunishPlayer.size(); ++i )
	{
		UpdatePunishPlayerInfo( m_vecEkkaPunishPlayer[i] );
	}

	m_lastUpdateInfoTime = ACE_OS::gettimeofday();
}


void CPunishmentManager::NotifyPunishStart()	//通知所有mapsrv天谴开始
{
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("CPunishmentManager::NotifyPunishStart 找不到GateSrv0 不能发放消息\n");
		return;
	}

	MGPunishmentStart gateMsg;
	MsgToPut* pNewMsg = CreateSrvPkg( MGPunishmentStart, pGateSrv, gateMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
}


void CPunishmentManager::NotifyPunishEnd()	//通知所有mapsrv天谴结束
{
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("CPunishmentManager::NotifyPunishStart 找不到GateSrv0 不能发放消息\n");
		return;
	}

	MGPunishmentEnd gateMsg;
	MsgToPut* pNewMsg = CreateSrvPkg( MGPunishmentEnd, pGateSrv, gateMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
}


void CPunishmentManager::NotifySurroundingPunishment( CPlayer* pPlayer, UINT damage)
{
	if( NULL == pPlayer )
	{
		D_ERROR("CPunishmentManager::NotifySurroundingPunishment pPlayer为空\n");
		return;
	}

	if( pPlayer->GetCurrentHP() == 0 )
	{
		D_ERROR("玩家已死亡不触发天谴\n");
		return;
	}


	pPlayer->GetSelfStateManager().OnPlayerBeGodPunished();
	

	UINT playerHp = pPlayer->GetCurrentHP();
	MGPlayerAttackTarget gateMsg;
	StructMemSet( gateMsg, 0, sizeof(MGPlayerAttackTarget) );
	gateMsg.targetID = pPlayer->GetFullID();
	gateMsg.nAtkPlayerHP = pPlayer->GetCurrentHP();
	gateMsg.nAtkPlayerMP = pPlayer->GetCurrentMP();
	gateMsg.nErrCode = BATTLE_SUCCESS;
	gateMsg.skillID = PUNISHMENT_SKILL_ID;
	gateMsg.battleFlag = ( damage == 0)? PUNISH_MISS : HIT;
	gateMsg.lHpSubed = damage;
	if ( (damage <= 0) && (gateMsg.battleFlag != PUNISH_MISS) )
	{
		//当然，实际不可能到这里，但为了统一起见，也加上；
		D_WARNING( "CPunishmentManager::NotifySurroundingPunishment，伤血为0但返回为非MISS\n" );
	}
	gateMsg.lHpLeft = ( damage >= playerHp ) ? 0: playerHp - damage;
	CMuxMap* pMap = pPlayer->GetCurMap();
	if( NULL == pMap )
	{
		D_ERROR("CPunishmentManager::NotifySurroundingPunishment pMap为空\n");
		return;
	}
	pMap->NotifySurrounding<MGPlayerAttackTarget>( &gateMsg, pPlayer->GetPosX(), pPlayer->GetPosY() );
}

void CPunishmentManager::NotifyQueryPunishPlayerInfo( CPlayer* pPlayer )		//根据查询的种族来返回天谴玩家的相关信息 
{
	if( NULL == pPlayer )
	{
		D_ERROR("CPunishmentManager::NotifyQueryPunishPlayerInfo pPlayer为NULL");
		return;
	}

	if( !GetPunishState() )			//不在天谴状态返回人数0
	{
		MGQueryPunishPlayerInfo gateMsg;
		gateMsg.memberSize = 0;
		pPlayer->SendPkg<MGQueryPunishPlayerInfo>( &gateMsg );
		return;
	}

	unsigned short race = pPlayer->GetRace();
	if( race == RACE_HUMAN )
	{
		MGQueryPunishPlayerInfo gateMsg;
		for( size_t i = 0; i<m_vecHumanPunishPlayer.size(); ++i )
		{
			gateMsg.members[i].mapID = 	m_vecHumanPunishPlayer[i].mapID;
			gateMsg.members[i].posX = m_vecHumanPunishPlayer[i].posX;
			gateMsg.members[i].posY = m_vecHumanPunishPlayer[i].posY;
			SafeStrCpy( gateMsg.members[i].punishPlayerName, m_vecHumanPunishPlayer[i].playerName.c_str() );
			gateMsg.members[i].nameLen = (UINT)strlen( gateMsg.members[i].punishPlayerName ) + 1;
		}
		gateMsg.memberSize = (UINT)m_vecHumanPunishPlayer.size();
		pPlayer->SendPkg<MGQueryPunishPlayerInfo>( &gateMsg );
	}else if( race == RACE_ARKA ){
		MGQueryPunishPlayerInfo gateMsg;
		for( size_t i = 0; i<m_vecEkkaPunishPlayer.size(); ++i )
		{
			gateMsg.members[i].mapID = m_vecEkkaPunishPlayer[i].mapID;
			gateMsg.members[i].posX = m_vecEkkaPunishPlayer[i].posX;
			gateMsg.members[i].posY = m_vecEkkaPunishPlayer[i].posY;
			SafeStrCpy( gateMsg.members[i].punishPlayerName, m_vecEkkaPunishPlayer[i].playerName.c_str() );
			gateMsg.members[i].nameLen = (UINT)strlen( gateMsg.members[i].punishPlayerName ) + 1;
		}
		gateMsg.memberSize = (UINT)m_vecEkkaPunishPlayer.size();
		pPlayer->SendPkg<MGQueryPunishPlayerInfo>( &gateMsg );

	}else{
		MGQueryPunishPlayerInfo gateMsg;
		for( size_t i = 0; i<m_vecElfPunishPlayer.size(); ++i )
		{
			gateMsg.members[i].mapID = m_vecElfPunishPlayer[i].mapID;
			gateMsg.members[i].posX = m_vecElfPunishPlayer[i].posX;
			gateMsg.members[i].posY = m_vecElfPunishPlayer[i].posY;
			SafeStrCpy( gateMsg.members[i].punishPlayerName, m_vecElfPunishPlayer[i].playerName.c_str() );
			gateMsg.members[i].nameLen = (UINT)strlen( gateMsg.members[i].punishPlayerName ) + 1;
		}
		gateMsg.memberSize = (UINT)m_vecElfPunishPlayer.size();
		pPlayer->SendPkg<MGQueryPunishPlayerInfo>( &gateMsg );	
	}
}


void CPunishmentManager::GetPunishTargetName( CPlayer* pPlayer, string& targetName )
{
	if( NULL == pPlayer )
		return;

	unsigned short ucRace = pPlayer->GetRace();
	stringstream ss;
	if( ucRace == RACE_HUMAN )
	{
		for( size_t i = 0; i<m_vecHumanPunishPlayer.size(); ++i )
		{
			ss << m_vecHumanPunishPlayer[i].playerName.c_str() <<",";
		}
	}else if( ucRace == RACE_ARKA )
	{
		for( size_t i = 0; i<m_vecEkkaPunishPlayer.size(); ++i )
		{
			ss << m_vecEkkaPunishPlayer[i].playerName.c_str() <<",";
		}
	}else if( ucRace == RACE_ELF )
	{
		for( size_t i = 0; i<m_vecElfPunishPlayer.size(); ++i )
		{
			ss << m_vecElfPunishPlayer[i].playerName.c_str() <<",";
		}
	}
	targetName = ss.str();
}


bool CPunishmentManager::IsPunishmentEnd()		//查询天谴是否结束
{
	if( !GetPunishState() )
	{
		return false;	//不在天谴状态中不需要判定 
	}

	ACE_Time_Value lastTime = ACE_OS::gettimeofday() - m_punishStartTime;
	if( lastTime.sec() < 3600 )
	{
		return false;
	}

	return true;
}

void CPunishmentManager::SetPunishmentNotified(bool bIsNotified) //设置是否已经通知天谴开始
{
	m_bIsPunishNotified = bIsNotified;
}

bool CPunishmentManager::IsPunishmentNotified()		//表明是否已经通知天谴开始
{
	return m_bIsPunishNotified;
}


void CPunishmentManager::EndPunishment()		//结束天谴, 收到GateSrv消息执行,清空信息
{
	m_punishStartTime = ACE_Time_Value::zero;
	DisablePunishState();
	m_vecHumanPunishPlayer.clear();
	m_vecElfPunishPlayer.clear();
	m_vecEkkaPunishPlayer.clear();
}


bool CPunishmentManager::IsPunishTarget( CPlayer* pTarget )
{
	if( NULL == pTarget )
		return false;

	unsigned short race = pTarget->GetRace();
	UINT playerUID = pTarget->GetDBUID();
	if( race == RACE_HUMAN )
	{
		for( size_t i =0; i< m_vecHumanPunishPlayer.size(); i++ )
		{
			if( m_vecHumanPunishPlayer[i].playerUID == playerUID )
			{
				return true; 
			}
		}
		return false;
	}else if( race == RACE_ARKA ){
		for( size_t i =0; i< m_vecEkkaPunishPlayer.size(); i++ )
		{
			if( m_vecEkkaPunishPlayer[i].playerUID == playerUID )
			{
				return true;
			}
		}
		return false;
	}else{
		for( size_t i =0; i< m_vecElfPunishPlayer.size(); i++ )
		{
			if( m_vecElfPunishPlayer[i].playerUID == playerUID )
			{
				return true;
			}
		}
		return false;
	}
}


bool CPunishmentManager::CheckPunishPlayer( const PunishPlayer& victim )				//天谴时间到惩罚玩家
{
	if( victim.bPunished )
	{
		return false;
	}

	ACE_Time_Value lastTime = ACE_OS::gettimeofday() - m_punishStartTime;
	if( lastTime.sec() /60 == victim.punishTime )
	{
		CPlayer* pPlayer = CPlayerManager::FindPlayerByRoleName( victim.playerName );
		if( NULL == pPlayer || pPlayer->IsLeaveDelay() )
		{
			D_INFO("天谴触发，但是玩家%s不在mapsrv上\n", victim.playerName.c_str() );
			return false;
		}

		//算惩罚结果 死亡 无事 升级装备
		PunishingPlayer( pPlayer );
	  	return true;
	} //if( lastTime.sec() /60 == minute )
	return false;
}


void CPunishmentManager::PunishingPlayer( CPlayer* pPlayer )				//天谴某个玩家
{
	if( NULL == pPlayer )
	{
		D_ERROR("CPunishmentManager::PunishPlayer 惩罚的玩家为空\n");
		return;
	}
	
	MGChat gateMsg;
	StructMemSet( gateMsg, 0, sizeof(gateMsg) );
	gateMsg.chatType = CT_RACE_SYS_EVENT;
	gateMsg.extInfo = pPlayer->GetRace();

	stringstream sysString;


	//算惩罚结果 死亡 无事 升级装备
	int randNumber = RandNumber( 1, 100);
	int result = 0;
	if( randNumber <= m_deadRate )
	{	
		PlayerID attackID;
		attackID.dwPID = 0;
		attackID.wGID = MONSTER_GID;
		NotifySurroundingPunishment( pPlayer, 999999 );
		pPlayer->SubPlayerHp( pPlayer->GetCurrentHP(), NULL, attackID );
		sysString << pPlayer->GetNickName() << "已遭受天谴，请弃恶从善回头是岸";
		SafeStrCpy( gateMsg.strChat, sysString.str().c_str() );
		result = 0;
	}else if( randNumber > 100 - m_upgradeRate ){
		pPlayer->RandUpdateOneEquipItem();	//提升身上装备
		NotifySurroundingPunishment( pPlayer, 0 );
		sysString << pPlayer->GetNickName() << "已逃过了天谴，他的某件装备已经幸运的升了一级";
		SafeStrCpy( gateMsg.strChat, sysString.str().c_str() );
		result = 1;		//身上装备升级
	}else{
		sysString << pPlayer->GetNickName() <<"已遭受天谴,但是幸运女神保佑其毫发无伤";
		SafeStrCpy( gateMsg.strChat, sysString.str().c_str() );
		NotifySurroundingPunishment( pPlayer, 0 );
		result = 2;		//相安无事
	}

	pPlayer->SendPkg<MGChat>( &gateMsg );

	return;
}


bool CPunishmentManager::CheckInfoUpdate()		//检查是否到了时间进行通知
{
	//超过十分钟会进行一次整体更新
	ACE_Time_Value lastTime = ACE_OS::gettimeofday() - m_lastUpdateInfoTime;
	if( lastTime.sec() >= 10*60 )	//每10分钟通知一次
	{
		UpdateAllPunishPlayerInfo();
		return true;
	}
	return false;
}


void CPunishmentManager::UpdatePunishPlayerinfo( const PunishPlayerInfo& playerInfo )	//更新惩罚信息 地图位置信息
{
	for( size_t i = 0; i< m_vecHumanPunishPlayer.size(); ++i )
	{
		if( strcmp( m_vecHumanPunishPlayer[i].playerName.c_str(), playerInfo.name ) == 0 )
		{
			m_vecHumanPunishPlayer[i].playerUID = playerInfo.playerUID;
			m_vecHumanPunishPlayer[i].mapID = playerInfo.mapID;
			m_vecHumanPunishPlayer[i].posX = playerInfo.posX;
			m_vecHumanPunishPlayer[i].posY = playerInfo.posY;
			m_vecHumanPunishPlayer[i].punishTime = playerInfo.punishTime;
			m_vecHumanPunishPlayer[i].bPunished = playerInfo.bPunished;
		}
	}

	for( size_t i = 0; i< m_vecElfPunishPlayer.size(); ++i )
	{
		if( strcmp( m_vecElfPunishPlayer[i].playerName.c_str(), playerInfo.name ) == 0 )
		{
			m_vecElfPunishPlayer[i].playerUID = playerInfo.playerUID;
			m_vecElfPunishPlayer[i].mapID = playerInfo.mapID;
			m_vecElfPunishPlayer[i].posX = playerInfo.posX;
			m_vecElfPunishPlayer[i].posY = playerInfo.posY;
			m_vecElfPunishPlayer[i].punishTime = playerInfo.punishTime;
			m_vecElfPunishPlayer[i].bPunished = playerInfo.bPunished;
		}
	}

	for( size_t i = 0; i< m_vecEkkaPunishPlayer.size(); ++i )
	{
		if( strcmp( m_vecEkkaPunishPlayer[i].playerName.c_str(), playerInfo.name ) == 0 )
		{
			m_vecEkkaPunishPlayer[i].playerUID = playerInfo.playerUID;
			m_vecEkkaPunishPlayer[i].mapID = playerInfo.mapID;
			m_vecEkkaPunishPlayer[i].posX = playerInfo.posX;
			m_vecEkkaPunishPlayer[i].posY = playerInfo.posY;
			m_vecEkkaPunishPlayer[i].punishTime = playerInfo.punishTime;
			m_vecEkkaPunishPlayer[i].bPunished = playerInfo.bPunished;
		}
	}
}


void CPunishmentManager::CheckAllPunishPlayer()
{
	//检查在该地图中这些玩家是否要遭受天谴
	for( size_t i = 0; i< m_vecHumanPunishPlayer.size(); ++i )
	{
		if( CheckPunishPlayer( m_vecHumanPunishPlayer[i] ) )
		{
			m_vecHumanPunishPlayer[i].bPunished = true;
			NotifyPunishPlayerInfo( m_vecHumanPunishPlayer[i] );
		}
	}

	for( size_t i = 0; i< m_vecElfPunishPlayer.size(); ++i )
	{
		if( CheckPunishPlayer( m_vecElfPunishPlayer[i] ) )
		{
			m_vecElfPunishPlayer[i].bPunished = true;
			//在这里通知其他mapsrv该玩家已经遭受天谴
			NotifyPunishPlayerInfo( m_vecElfPunishPlayer[i] );
		}
	}

	for( size_t i = 0; i< m_vecEkkaPunishPlayer.size(); ++i )
	{
		if( CheckPunishPlayer( m_vecEkkaPunishPlayer[i] ) )
		{
			m_vecEkkaPunishPlayer[i].bPunished = true;
			//在这里通知其他mapsrv该玩家已经遭受天谴
			NotifyPunishPlayerInfo( m_vecEkkaPunishPlayer[i] );
		}
	}
}


void CPunishmentManager::TimeProc()
{
	//轮询，如果不在天谴状态中,检查是否启动天谴
	if( !GetPunishState() )
	{
		if( CLoadSrvConfig::GetSelfID() == 0 )  //如果mapsrv0才去判断
		{
			if ( IsPunishmentStart() )
			{
				if ( !IsPunishmentNotified() )
				{
					//通知GateSrv0告诉所有mapsrv开始
					D_DEBUG("天谴时间已到触发天谴\n");
					NotifyPunishStart();	
					SetPunishmentNotified(true);
				}
			}
			else
			{
				SetPunishmentNotified(false);
			}
		}
	}else{
		if( CLoadSrvConfig::GetSelfID() == 0 && IsPunishmentEnd() )
		{
			NotifyPunishEnd();
			SetPunishmentNotified(false);
			return;
		}

		//检查玩家是否遭到天谴
	    CheckAllPunishPlayer();

		//检查是否到了时间进行整体的通知
		CheckInfoUpdate();
	}
}


bool CPunishmentManager::LoadPunishmentXml( const char* szPath )	//读取天谴的XML配置
{
	TRY_BEGIN;

	if ( NULL == szPath )
	{
		D_ERROR( "CPunishmentManager::LoadPunishmentXml，input file name NULL\n" );
		return false;
	}

	CXmlManager xmlLoader;
	if( !xmlLoader.Load(szPath) )
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	//取该XML所有第二层的ELEMNET
	vector<CElement*> elementSet;
	xmlLoader.FindChildElements( pRoot, elementSet );
	
	if ( elementSet.empty() )
	{
		return false;
	}

	CElement* tmpEle = NULL;
	int curpos = 0;
	//编译警告去除，by dzj, 09.07.08, unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( strcmp(tmpEle->Name(), "PunishmentTime" ) == 0 ) //天谴时间元素	
		{
			PunishTime time;
	   		if ( !IsXMLValValid( tmpEle, "Hour", curpos ) )
			{
				return false;
			}		
			time.hour = atoi( tmpEle->GetAttr("Hour") );

			if ( !IsXMLValValid( tmpEle, "Minute", curpos ) )
			{
				return false;
			}		
			time.minute= atoi( tmpEle->GetAttr("Minute") );
			m_vecPunishTime.push_back( time );
		}

		if( strcmp( tmpEle->Name(),"PunishmentRate") == 0)
		{
			if ( !IsXMLValValid( tmpEle, "DeadRate", curpos ) )
			{
				return false;
			}		
			m_deadRate = atoi( tmpEle->GetAttr("DeadRate") );

			if ( !IsXMLValValid( tmpEle, "SafeRate", curpos ) )
			{
				return false;
			}		
			m_safeRate = atoi( tmpEle->GetAttr("SafeRate") );

			if ( !IsXMLValValid( tmpEle, "UpgradeRate", curpos ) )
			{
				return false;
			}		
			m_upgradeRate = atoi( tmpEle->GetAttr("UpgradeRate") );
		}

		curpos++;
	}

	return true;
	TRY_END;
	return false;
}


bool CPunishmentManager::IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
{
	if ( NULL == pEle->GetAttr( attName ) )
	{
		D_ERROR( "玩家技能属性XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
		return false;
	}
	return true;
}


bool CPunishmentManager::Init()		//读取配置文件
{
	return LoadPunishmentXml("config/activity/punishment.xml");
}


void CPunishmentManager::PunishPlayerOffline( CPlayer* pPlayer )
{
	if( NULL == pPlayer )
	{
		D_ERROR("CPunishmentManager::FindPunishPlayer为空\n");
		return;
	}



	unsigned short race = pPlayer->GetRace();
	UINT playerUID = pPlayer->GetDBUID();

	//发现自己是天谴目标的话.... 告之其他mapsrv
	if( race == RACE_HUMAN )
	{
		for( size_t i =0; i< m_vecHumanPunishPlayer.size(); i++ )
		{
			if( m_vecHumanPunishPlayer[i].playerUID == playerUID )
			{
				m_vecHumanPunishPlayer[i].mapID = 0;  //当mapID等于0时当下线处理
				m_vecHumanPunishPlayer[i].posX = 0;
				m_vecHumanPunishPlayer[i].posY = 0;
				NotifyPunishPlayerInfo( m_vecHumanPunishPlayer[i] );
				return;
			}
		}
	}else if( race == RACE_ARKA ){
		for( size_t i =0; i< m_vecEkkaPunishPlayer.size(); i++ )
		{
			if( m_vecEkkaPunishPlayer[i].playerUID == playerUID )
			{
				m_vecEkkaPunishPlayer[i].mapID = 0;  //当mapID等于0时当下线处理
				m_vecEkkaPunishPlayer[i].posX = 0;
				m_vecEkkaPunishPlayer[i].posY = 0;
				NotifyPunishPlayerInfo( m_vecEkkaPunishPlayer[i] );
				return;
			}
		}
	}else{
		for( size_t i =0; i< m_vecElfPunishPlayer.size(); i++ )
		{
			if( m_vecElfPunishPlayer[i].playerUID == playerUID )
			{
				m_vecElfPunishPlayer[i].mapID = 0;  //当mapID等于0时当下线处理
				m_vecElfPunishPlayer[i].posX = 0;
				m_vecElfPunishPlayer[i].posY = 0;
				NotifyPunishPlayerInfo( m_vecElfPunishPlayer[i] );
				return;
			}
		}
	}
}

