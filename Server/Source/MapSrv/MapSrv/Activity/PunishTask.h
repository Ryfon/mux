﻿

#ifndef PUNISHCOUNTER_H
#define PUNISHCOUNTER_H


#include <string>
#include <map>
#include "PkgProc/CliProtocol_T.h"
using namespace std;
using namespace MUX_PROTO;

class CPlayer;

//生成 只在GivePunishTask中  DELETE只在CPunishTaskManager中 
class CPunishTask
{
public:
	CPunishTask():m_killCounter( 0 ),m_bAcceptPunishTask(false),m_pOwner( NULL )
	{}

	CPunishTask( int killCounter):m_killCounter( killCounter ),m_bAcceptPunishTask(false),m_pOwner( NULL )
	{
	}

public:
	//当杀死一个玩家
	bool OnKillPlayer( CPlayer* pTarget );
	//获得宿主
	void AttachOwner( CPlayer* pPlayer );
	//获得天谴的杀人计数
	int GetKillCounter();
	//是否接过天谴任务
	bool IsAcceptPunishTask();
	//清除任务状态
	void Clear();
	//设置接受任务
	void SetAcceptPunishTask( bool bFlag );
	//设置杀人计数器
	void SetKillCounter( unsigned int killCounter );

private:
	int m_killCounter;					//杀人的次数
	bool m_bAcceptPunishTask;			//是否接过天谴任务
	CPlayer* m_pOwner;					//寄主
};

#endif /*PUNISHCOUNTER_H*/

