﻿
#include <vector>
#include <string>
#include "Utility.h"
#include "PkgProc/SrvProtocol.h"

class CElement;
class CPunishmentManager
{
	typedef struct StrPunishTime
	{
		int hour;
		int minute;
	}PunishTime;	//用来记录每天天谴的时间

	typedef struct StrPunishPlayer
	{
		unsigned int playerUID;
	    std::string playerName;	//天谴玩家的姓名
		int punishTime;		//在1小时内的第几分钟会受到天谴
		bool bPunished;		//是否受过惩罚，受过惩罚的玩家不会受到第2次的惩罚
		unsigned long mapID;		//该玩家的所在地图ID
		unsigned short posX;		//该玩家在地图上的位置X
		unsigned short posY;		//该玩家在地狱上的位置Y
	}PunishPlayer;		//用来记录该次天谴的玩家


private:
	CPunishmentManager(void);		//静态类屏蔽构造析构函数
	~CPunishmentManager(void);

public:
	static bool Init();		//读取配置文件
	static void TimeProc();		//管理器的更新
	static void EnablePunishState();	//设置天谴状态
	static void DisablePunishState();	//取消天谴状态
	static bool GetPunishState();		//获取天谴状态
	
public:
	static bool IsPunishmentStart();	//查询天谴是否开始，只有mapsrv0又权利执行再由gatesrv0来通知各个mapsrv天谴开始
	static bool IsPunishmentEnd();		//查询天谴是否结束
	static void	SetPunishmentNotified(bool b);	//设置是否已经通知天谴开始
	static bool IsPunishmentNotified();	//查询是否已经通知天谴开始
	static void StartPunishment( const GMPunishmentStart* pStartInfo);		//开始天谴，收到GateSrv消息执行
	static void EndPunishment();		//结束天谴, 收到GateSrv消息执行
	static bool IsPunishTarget( CPlayer* pTarget );
	
	static void CheckAllPunishPlayer();	//对所有玩家进行检查是否到天谴时间 
	static bool CheckPunishPlayer( const PunishPlayer& victim );				//对单个玩家进行检查是否到天谴时间 
	static void PunishingPlayer( CPlayer* pPlayer );				//天谴某个玩家
	static bool CheckInfoUpdate();		//检查是否到了时间进行通知
	static void UpdatePunishPlayerinfo( const PunishPlayerInfo& playerInfo );	//更新惩罚信息 地图位置信息
	static void PunishPlayerOffline( CPlayer* pPlayer);

	static void UpdateAllPunishPlayerInfo();	//对所有mapsrv上的玩家进行信息的更新
	static void UpdatePunishPlayerInfo( const PunishPlayer& player );		//更新此mapsrv上在线天谴玩家的信息至其他mapsrv
	static void NotifyPunishPlayerInfo( const PunishPlayer& player );		//更新此mapsrv上在线天谴玩家的信息至其他mapsrv
	static void NotifyPunishStart();	//通知所有mapsrv天谴开始
	static void NotifyPunishEnd();		//通知所有mapsrv天谴结束
	static void NotifySurroundingPunishment( CPlayer* pPlayer, UINT damage );
	static void NotifyQueryPunishPlayerInfo( CPlayer* pPlayer );		//根据查询的种族来返回天谴玩家的相关信息 

	static void GetPunishTargetName( CPlayer* pPlayer, string& targetName );

private:
	static bool LoadPunishmentXml( const char* szPath );	//读取天谴的XML配置
	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord );
	
private:
	static bool m_bIsInPunishTime;  //表明是否在天谴时间之内
	static bool m_bIsPunishNotified;	//表明是否已经通知天谴开始
	static std::vector<PunishTime> m_vecPunishTime;	//每天的天谴时间保留在内
	static ACE_Time_Value m_punishStartTime;	//天谴的启动时间，用来检查可以在何时结束天谴
	static ACE_Time_Value m_lastUpdateInfoTime;		//天谴的时候每隔10分钟需要更新天谴玩家的位置信息
	static std::vector<PunishPlayer> m_vecHumanPunishPlayer;	//该次天谴中人类遭到天谴的玩家
	static std::vector<PunishPlayer> m_vecElfPunishPlayer;		//该次天谴中精灵遭到天谴的玩家
	static std::vector<PunishPlayer> m_vecEkkaPunishPlayer;		//该次天谴中艾卡遭到天谴的玩家
	static int m_deadRate;			//遭受天谴时死亡的概率 百分比
	static int m_safeRate;			//遭受天谴时安全的概率 百分比
	static int m_upgradeRate;		//遭受天谴时升级装备的概率 百分比

};

