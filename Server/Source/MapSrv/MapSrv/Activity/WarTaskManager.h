﻿

#ifndef WARMANAGER_H
#define WARMANAGER_H

#include "Utility.h"
class CElement;

class CWarTaskManager
{
private:
	CWarTaskManager(void);
	~CWarTaskManager(void);

public:
	static bool InitWarTaskManager();

public:
	static void NotifyAttackCityBegin();	//通知攻城战开始
	static void NotifyAttackCityEnd();	//

//新接口 华丽的分界线
	static void OnAttackCityBegin( unsigned int actID );
	static void OnAttackCityEnd( unsigned int taskId );
	static void Release();	

	static void GetWarStartTime( unsigned int& dayPerWeek, unsigned int& startHour, unsigned int& startMinute )
	{
		dayPerWeek = m_dayPerWeek;
		startHour = m_startHour;
		startMinute = m_startMinute;
	}

	static void CityWarTimer();

	static void SetActivateFlag( bool bFlag )
	{
		m_bIsActivate = bFlag;
	}

	static bool IsAttackWar();

private:
	static bool LoadWarXML( const char* szPath );

private:
	static bool m_bIsActivate;			//是否被激活该功能	
	static bool m_bIsAttackWar;			//是否在攻城战时间内
	static unsigned int m_warTime;		//攻城的持续秒数
	static ACE_Time_Value m_citywarStartTime;	//攻城战开始的时间
	static unsigned int m_dayPerWeek;		//每周的第几天
	static unsigned int m_startHour;		//开始的小事
	static unsigned int m_startMinute;		//开始的分钟
	static unsigned int m_actID;			
};



#endif //WARTASKMANAGER_H
