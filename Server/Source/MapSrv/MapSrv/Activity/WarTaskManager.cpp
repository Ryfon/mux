﻿#include "WarTaskManager.h"
#include "../PlayerManager.h"
#include "Utility.h"
#include "../G_MProc.h"
#include "../AISys/RuleAIEngine.h"
#include "../Player/Player.h"
#include "../RaceManager.h"
#include "../MuxMap/mannormalmap.h"
#include "XmlManager.h"

bool CWarTaskManager::m_bIsActivate = false;		//是否被激活
bool CWarTaskManager::m_bIsAttackWar = false;			//是否在攻城战时间内
unsigned int CWarTaskManager::m_warTime = 0;		//攻城的持续秒数
ACE_Time_Value CWarTaskManager::m_citywarStartTime = ACE_Time_Value::zero;	//攻城战开始的时间
unsigned int CWarTaskManager::m_dayPerWeek = 0;		//每周的第几天
unsigned int CWarTaskManager::m_startHour = 0;		//开始的小事
unsigned int CWarTaskManager::m_startMinute = 0;		//开始的分钟
unsigned int CWarTaskManager::m_actID = 0;


CWarTaskManager::CWarTaskManager(void)
{
}

CWarTaskManager::~CWarTaskManager(void)
{
}


bool CWarTaskManager::InitWarTaskManager()
{
	m_actID = 1000;
	m_bIsActivate = true;
	m_bIsAttackWar = false;
	return LoadWarXML("config/activity/warconfig.xml");
}

void CWarTaskManager::NotifyAttackCityBegin()	//通知攻城战开始
{
	//只有0号mapsrv 可以开启
	if( CLoadSrvConfig::GetSelfID() != 0 )
		return;

	MGAttackCityBegin gateMsg;
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("CWarTaskManager::AttackCityBegin 找不到pGateSrv\n");
		return;
	}
	gateMsg.actID = m_actID;
	MsgToPut* pNewMsg = CreateSrvPkg( MGAttackCityBegin, pGateSrv, gateMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
}


void CWarTaskManager::CityWarTimer()
{
	if( !m_bIsActivate )			//是否被激活该功能
		return;

	if( m_bIsAttackWar )
	{
		ACE_Time_Value currentTime = ACE_OS::gettimeofday();
		if( (currentTime - m_citywarStartTime).sec() >= m_warTime )
		{
			NotifyAttackCityEnd();
			m_bIsAttackWar = false;
		}
	}else
	{
		int cyear = 0, cmon = 0, cmday = 0, chour = 0, cmin = 0, csec = 0, cwday = 0, cmsec = 0;
		GetCurTime( cyear, cmon, cmday, chour,cmin,csec,cwday, cmsec);
		if( cwday == m_dayPerWeek && chour == m_startHour && cmin == m_startMinute )
		{
			NotifyAttackCityBegin();
		}
	}
}


bool CWarTaskManager::IsAttackWar()
{
	return m_bIsAttackWar;
}


void CWarTaskManager::NotifyAttackCityEnd(  )	//攻城结束
{
	//通知玩家攻城战结束
	MGAttackCityEnd gateMsg;
	gateMsg.warTaskId = m_actID;
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("CWarTaskManager::AttackCityEnd 找不到pGateSrv\n");
		return;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( MGAttackCityEnd, pGateSrv, gateMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
	D_DEBUG("攻城战结束\n");
}


void CWarTaskManager::OnAttackCityBegin( unsigned int actID )
{
	CManNormalMap::OnActivityStart( actID );
	m_bIsAttackWar = true;
	m_citywarStartTime = ACE_OS::gettimeofday();
}


void CWarTaskManager::OnAttackCityEnd( unsigned int actID )
{
	AllRaceMasterManagerSingle::instance()->ResetAllRaceDeclareWarInfo();
	CManNormalMap::OnActivityEnd( actID );
}


void CWarTaskManager::Release()	
{

}


bool CWarTaskManager::LoadWarXML( const char* szPath )
{
	CXmlManager xmlManager;
	if(!xmlManager.Load(szPath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szPath );
		return false;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szPath);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements( pRoot, childElements );
	CElement* tmpEle = NULL;
	if( childElements.empty() )
	{
		D_WARNING("warconfig.xml属性为空\n");
		return true;
	}

	const char* pAttr = NULL;
	unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = childElements.begin();
		tmpiter != childElements.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( NULL == tmpEle )
			continue;

		int curpos = (int)(tmpiter - childElements.begin());
		if( strcmp(tmpEle->Name(), "citywar_time" ) == 0 )
		{
			pAttr = tmpEle->GetAttr("weekday");
			if( pAttr )
			{
				m_dayPerWeek = (unsigned int)atoi(pAttr);
			}

			pAttr = tmpEle->GetAttr("hour");
			if( pAttr )
			{
				m_startHour = (unsigned int)atoi(pAttr);
			}

			pAttr = tmpEle->GetAttr("minute");
			if( pAttr )
			{
				m_startMinute = (unsigned int)atoi( pAttr );
			}

			pAttr = tmpEle->GetAttr("wartime");
			if( pAttr )
			{
				m_warTime = (unsigned int)atoi( pAttr );
			}
		}
	}
	return true;
}
