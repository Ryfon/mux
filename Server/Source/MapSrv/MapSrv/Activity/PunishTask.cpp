﻿
#include "PunishTask.h"
#include "PunishmentManager.h"
#include "../Player/Player.h"

void CPunishTask::AttachOwner( CPlayer* pPlayer )
{
	m_pOwner = pPlayer;
}


bool CPunishTask::OnKillPlayer( CPlayer* pTarget )
{
	if( NULL == m_pOwner )
		return false;

	if( NULL == pTarget )
		return false;

	if( m_pOwner->GetRace() != pTarget->GetRace() )
		return false;

	if( !CPunishmentManager::IsPunishTarget( pTarget ) )
		return false;

	m_killCounter++;
	//打死一个就算完成任务
	if( m_killCounter >= 1)
	{
		MGPlayerTaskStatus gateMsg;
		gateMsg.taskID = PUNISH_TASK_ID;
		gateMsg.taskStat = NTS_TGT_ACHIEVE;
		gateMsg.failTime = 0;
		m_pOwner->SendPkg<MGPlayerTaskStatus>( &gateMsg );
	}
	MGPlayerTaskRecordUpdate updateCounter;
	StructMemSet( updateCounter, 0, sizeof(MGPlayerTaskRecordUpdate) );
	updateCounter.taskRdInfo.taskID = PUNISH_TASK_ID;
	updateCounter.taskRdInfo.processData = m_killCounter;
	m_pOwner->SendPkg<MGPlayerTaskRecordUpdate>( &updateCounter );
	return true;
}


//获得天谴的杀人计数
int CPunishTask::GetKillCounter()
{
	return m_killCounter;
}


//是否接过天谴任务
bool CPunishTask::IsAcceptPunishTask()
{
	return m_bAcceptPunishTask;
}


//清除
void CPunishTask::Clear()
{
	m_bAcceptPunishTask = false;
	m_killCounter = 0;
}


void CPunishTask::SetKillCounter( unsigned int killCounter )
{
	m_killCounter = killCounter;
}


void CPunishTask::SetAcceptPunishTask( bool bFlag )
{
	m_bAcceptPunishTask = bFlag;
}
