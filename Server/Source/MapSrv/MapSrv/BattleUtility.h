﻿
#ifndef BATTLEUTILITY_H
#define BATTLEUTILITY_H

#include <vector>
#include "PkgProc/CliProtocol.h"


class CItemSkill;
class CPlayerSkill;
class CPlayer;
class CMonster;
class CBuffProp;

enum EBattle_Type
{
	COMMON_SKILL_PVC,
	WEAPON_SKILL_PVC,
	PARTION_SKILL_PVC,
	ASSIST_SKILL_PVC,
	COMMON_SKILL_PVP,
	WEAPON_SKILL_PVP,
	PARTION_SKILL_PVP,
	ASSIST_SKILL_PVP,
	ITEM_SKILL_AOE,
	BATTLE_MAX,
};

enum EBattleType
{
	PVP = 0,
	PVC = 1,
	CVP = 2,
	CVC = 3,
	BATTLE_INVALIDE = 4
};


enum ESub_Type
{
	SUB_BATTLE = 0,
	SUB_INVALID = 1
};

#define CREATE_BUFFARG( pSkill, name )\
	BuffArg name;\
	name.buffProp = pSkill->GetBuffProp();\
	name.buffTime = pSkill->GetPeriodTime();\
	name.assistObject = pSkill->GetAssistObject();\
	name.linkRate = pSkill->GetLinkRate();\
	name.isTeamBuff = ( pSkill->GetTargetType() == 2 );\
	name.teamRange = pSkill->GetAttDistMax();\
	name.skillID = pSkill->GetSkillID();\


typedef struct StrAttributeEffect
{
public:
	int nPhyAttack;			//改变的物理攻击
	int nPhyDefend;			//改变的物理防御
	int nMagAttack;			//改变的魔法攻击
	int nMagDefend;        //改变的魔法DEFEND
	int nPhyHitLevel;      //改变的物理HIT
	int nMagHitLevel;      //改变的魔法HIT
	int nPhyCriLevel;		//改变的物理CRI
	int nMagCriLevel;		//改变的魔法CRI
	int nStr;			   //改变的STR
	int nInt;             //改变的INT
	int nAgi;			   //改变的AGI
	std::vector<int> nSpeedAffect;    //改变的速度系数
	int nMaxHpUp;			//MAXHP的上升量,常量
	float fBaseHpUp;		//增加基本HP
	int nPhyCriDamAddition;		//物理暴击的追加伤害
	int nMagCriDmgAddition;		//魔法暴击的追加伤害
	float nAddPhyDamageRate;	//物理攻击的伤害提升百分比
	float nAddMagDamageRate;	//魔法攻击的伤害提升百分比
	float fResistFaintRate;		//抵抗晕眩等级
	float fResistBondageRate;	//抵抗束缚等级
	float fAddFaintRate;			//提高击晕几率等级
	float fAddBondageRate;		//提高束缚几率等级
	int nAddAoeRange;			//提高范围技能的攻击范围
	int nAddAtkDistance;		//提高技能的释放距离
	float fDotDmgUp;			//提高dot伤害的百分比
	int nDecreaseCdTime;		//减少每个技能cｄ的时间
	float fAddPhyCriRate;		//改变的物暴率
	float fAddMagCriRate;		//改变的魔暴率
	float fAddPhyHitRate;		//改变的物命（rate ）
	float fAddMagHitRate;		//改变的魔命( rate )
	float fAddPhyCriDmgRate;	//增加物理暴击伤害值
	float fAddMagCriDmgRate;	//增加魔法暴击伤害值
	float fPhyJoukRate;			//物理躲闪几率
	float fMagJoukRate;			//魔法躲闪几率
	float fSpPowerUpRate;		//SP加速豆增加的百分比
	unsigned int timesExp;		//n倍经验
	unsigned int timesPhyMagExp;	//n倍熟练度
	bool m_bSelfExplosion;							//是否有自爆
	unsigned int selfExplosionArg;					//自爆参数
	bool m_bFaint;									//设置晕旋标记
	bool m_bBondage;								//设置束缚标记
	bool m_bInvicible;								//无敌标志
	unsigned m_nAbsorbDamage;						//寄主能吸收的伤害 
	bool m_bSpeedDown;								//是否被减速
	bool m_bSpPowerSpeedUp;							//sp豆是否加豆
	bool m_bMoveImmunity;							//是否移动免疫
	bool m_bSpeedUp;								//是否有加速BUFF
	bool m_bDamageRebound;							//是否伤害反弹
	unsigned int DrArg;								//伤害反弹的参数
	bool isSleep;									//是否睡眠
	bool isSheep;									//是否变羊

public:
	void Reset()
	{
		nPhyAttack = 0;
		nPhyDefend = 0;
		nMagAttack = 0;
		nMagDefend = 0; 
		nPhyHitLevel = 0;
		nMagHitLevel = 0;
		nPhyCriLevel = 0;
		nMagCriLevel = 0;
		nStr = 0;
		nInt = 0;
		nAgi = 0;
		nSpeedAffect.clear();
		nMaxHpUp = 0;			//MAXHP的上升量
		fBaseHpUp = 0.0f;
		nPhyCriDamAddition = 0;		//物理暴击的追加伤害
		nMagCriDmgAddition = 0;		//魔法暴击的追加伤害
		nAddPhyDamageRate = 0.0f;	//物理攻击的伤害提升百分比
		nAddMagDamageRate = 0.0f;	//魔法攻击的伤害提升百分比
		fResistFaintRate = 0.0f;	//抵抗晕眩几率
		fResistBondageRate= 0.0f;	//抵抗束缚几率
		fAddFaintRate = 0.0f;		//提高击晕几率
		fAddBondageRate = 0.0f;		//提高束缚几率
		nAddAoeRange = 0;			//提高范围技能的攻击范围
		nAddAtkDistance = 0;		//提高技能的释放距离
		fDotDmgUp = 0.0f;		//提高dot伤害的百分比
		nDecreaseCdTime = 0;		//减少每个技能cｄ的时间
		fAddPhyCriRate = 0.0f;		//改变的物暴率
		fAddMagCriRate = 0.0f;		//改变的魔暴率
		fAddPhyHitRate = 0.0f;		//改变的物命（rate ）
		fAddMagHitRate = 0.0f;		//改变的魔命( rate )
		fAddPhyCriDmgRate = 0.0f;	//增加物理暴击伤害值
		fAddMagCriDmgRate = 0.0f;	//增加魔法暴击伤害值
		fPhyJoukRate = 0.0f;		//物理躲闪几率
		fMagJoukRate = 0.0f;		//魔法躲闪几率
		fSpPowerUpRate = 1.0f;		//SP豆的加速率
		timesExp = 1;		//n倍经验
		timesPhyMagExp = 1;	//n倍熟练度
		m_bSelfExplosion = false;							//是否有自爆
		selfExplosionArg = 0;
		m_bFaint = false;									//设置晕旋标记
		m_bBondage = false;								//设置束缚标记
		m_bInvicible = false;								//无敌标志
		m_nAbsorbDamage = 0;						//寄主能吸收的伤害 
		m_bSpeedDown = false;								//是否被减速
		m_bSpPowerSpeedUp = false;							//sp豆是否加豆
		m_bMoveImmunity = false;							//是否移动免疫
		m_bSpeedUp = false;								//是否有加速BUFF
		m_bDamageRebound = false;							//是否伤害反弹
		DrArg = 0;						//伤害反弹的参数 
		isSheep = false;
		isSleep = false;
	}

	StrAttributeEffect()
	{
		Reset();
	}
}AttributeEffect;

//////////////////////////新技能参数/////////////////////////////

struct BuffArg
{
	CBuffProp* buffProp;
	int assistObject;
	float linkRate;
	TYPE_TIME buffTime;
	bool isTeamBuff;
	int teamRange;		//小队技能的距离
	unsigned int skillID;

	BuffArg():buffProp(NULL),assistObject(0),linkRate(0.0f),buffTime( 0 ), isTeamBuff(false),teamRange(0),skillID(0)
	{}
};


const double propertyKArray[6][6] = {{ 1.5, 0.4, 0.5, 0.2, 2, 1 },{ 1.2, 0.5, 0.5, 0.2, 3, 1 },{ 0.8, 0.6, 1, 0.8, 1.5, 1.5 },{ 0.75, 0.9, 0.8, 1, 1.5, 1.5 },{ 0.4, 1.6, 0.2, 0.5, 1, 2 },{ 0.5, 1.25, 0.2, 0.5, 1, 3 }};

//double GetPropertyK( unsigned short job, unsigned int index );

bool GetPosDistanceMax( TYPE_POS posX1, TYPE_POS posY1, TYPE_POS posX2, TYPE_POS posY2, unsigned int range );

//测算2点之间距离
bool IsInRange( TYPE_POS posX1, TYPE_POS posY1, TYPE_POS posX2, TYPE_POS posY2, unsigned int range );

//2点是否在距离内，精确值
bool IsInAccurateDistance( TYPE_POS x1,TYPE_POS y1, TYPE_POS x2, TYPE_POS y2,  TYPE_POS distance );

//计算是否在攻击范围之内  宽松值
bool IsInVagueAttDistance(TYPE_POS x1, TYPE_POS y1, TYPE_POS x2, TYPE_POS y2, int minRange, int maxRange, bool bPlayerUse = false )	; 


#endif
