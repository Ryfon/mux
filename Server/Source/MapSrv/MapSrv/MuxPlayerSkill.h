﻿#pragma once

#include "MuxSkillProp.h"
#include "Utility.h"
#include "tcache/tcache.h"

class CPlayer;
class CUnion;

class CMuxPlayerSkill
{
public:
	CMuxPlayerSkill();
	~CMuxPlayerSkill();

	PoolFlagDefine()
	{
		m_pSkillProp = NULL;		//数据
		m_dbIndex = 0;			//数据库数组中的索引
		m_pOwner = NULL;				//拥有该玩家的
		m_pUnionOwner = NULL;			//拥有该技能的工会
		m_bIsGuildSkill = false;			//是否是工会技能 
	}

	bool InitPlayerSkill( const SkillInfo_i& skillInfo, unsigned int dbIndex, CPlayer* pPlayer );		//初始化

public:
	//是否可空放技能；
	inline bool IsNoTargetValid() { if( NULL == m_pSkillProp ) {return false;} return m_pSkillProp->m_target != 3; }
	//击退距离(单位0.1米)
	inline unsigned int GetKickBackRange() { if( NULL == m_pSkillProp ) {return 0;} return m_pSkillProp->m_kickBackRange+50/*校验稍为放宽些*/; }
	//死亡时是否仍击退
	inline bool IsDeadKickBack() { if ( NULL == m_pSkillProp ) {return false;} return m_pSkillProp->m_deadKickBack; }

	//是否是SP技能
	bool IsSpSkill();
	//增加SP经验
	void AddSpExp( unsigned int spExp );
	unsigned int GetSpExp();

	//获取下一级的SP经验
	unsigned int GetNextSkillID();

	unsigned int GetSkillID();

	unsigned int GetEffectiveSkillID()
	{
		return m_dbData.effectiveSkillID;
	}

	CMuxSkillProp* GetSkillProp()
	{
		return m_pSkillProp;
	}

	bool IsAssistSkill();

	void SetMuxSkillProp( CMuxSkillProp* pSkillProp, bool changeSkillID = true )
	{
		m_pSkillProp = pSkillProp;
		if ( NULL == pSkillProp )
		{
			return;
		}
		if(changeSkillID)
			m_dbData.skillID = pSkillProp->m_skillID;
		
		m_dbData.effectiveSkillID = pSkillProp->m_skillID;
		UpdateDbData();
	}

	void UpdateDbData();

	void SetDbIndex( unsigned int dbIndex );

	unsigned int GetSkillType();

	unsigned int GetUpgradePoints();

	bool IsNormalAttack()
	{
		if( NULL == m_pSkillProp )
			return false;
		return m_pSkillProp->IsNormalSkill();
	}

	bool IsGoodSkill()
	{
		if( NULL == m_pSkillProp )
			return false;
		return m_pSkillProp->IsGoodSkill();
	}


	unsigned int GetPublicCdType()
	{
		if( NULL == m_pSkillProp )
			return 0;
		return m_pSkillProp->m_publicCDType;
	}

	unsigned int GetUllageMp()
	{
		if( NULL == m_pSkillProp )
			return 0;

		return m_pSkillProp->m_nUllageMp;
	}

	unsigned int GetAttDistMin()
	{
		if( NULL == m_pSkillProp )
			return 0;

		return m_pSkillProp->m_nAttDistMin;
	}
	
	unsigned int GetAttDistMax()
	{
		if( NULL == m_pSkillProp )
			return 0;

		return m_pSkillProp->m_nAttDistMax;
	}

	unsigned int GetCoolDown()
	{
		if( NULL == m_pSkillProp )
			return 0;

		return m_pSkillProp->m_nCoolDown;
	}

	unsigned int GetUllageSp()
	{
		if( NULL == m_pSkillProp )
			return 0;

		return m_pSkillProp->m_nUllageSp;
	}


	unsigned int GetCurseTime()
	{
		if( NULL == m_pSkillProp )
			return 0;

		return m_pSkillProp->m_nCurseTime;
	}


	unsigned int GetType()
	{
		if( NULL == m_pSkillProp )
			return 0;
		return m_pSkillProp->m_skillType;
	}


	unsigned int GetAddExp()
	{
		if( NULL == m_pSkillProp )
			return 0;
		return m_pSkillProp->m_nAddExp;
	}

	bool IsSpiltSkill()
	{
		if( NULL == m_pSkillProp )
			return false;
		return ( NULL != m_pSkillProp->m_pMuxSpiltProp );
	}

	bool IsAoeSkill()
	{
		if( NULL == m_pSkillProp )
			return false;
		return m_pSkillProp->IsAoeSkill();
	}

	bool IsTeamSkill()
	{
		if( NULL == m_pSkillProp )
			return false;
		return ( m_pSkillProp->m_target == 2 );
	}

	unsigned int GetAoeShape()
	{
		if( NULL == m_pSkillProp )
			return 0;
		return m_pSkillProp->m_shape;
	}

	unsigned int GetAoeRange()
	{
		if( NULL == m_pSkillProp )
			return 0;
		return m_pSkillProp->GetAoeRange();
	}

	MuxPlayerDamageProp* GetDamageProp()
	{
		if( NULL == m_pSkillProp )
			return NULL;

		return m_pSkillProp->m_pDamageProp;
	}

	MuxBuffProp* GetBuffProp()
	{
		if( NULL == m_pSkillProp )
			return NULL;

		return m_pSkillProp->m_pBuffProp;
	}

	MuxSpiltProp* GetSpiltProp()
	{
		if( NULL == m_pSkillProp )
			return NULL;

		return m_pSkillProp->m_pMuxSpiltProp;
	}

	MuxItemSkillProp* GetItemSkillProp()
	{
		if( NULL == m_pSkillProp )
			return NULL;

		return m_pSkillProp->m_pMuxItemSkillProp;
	}

	unsigned int GetBasicSkillID()
	{
		if( NULL == m_pSkillProp )
			return 0;

		return m_pSkillProp->m_BasicSkill;
	}

	bool UpdateSkill( unsigned int skillID);

	unsigned int GetRelieveID()
	{
		if( NULL == m_pSkillProp )
			return 0;

		return m_pSkillProp->m_relieveID;
	}

	void InitGuildSkill( CMuxSkillProp* pSkillProp, CUnion* pOwner )
	{
		m_pSkillProp = pSkillProp;
		m_pUnionOwner = pOwner;
		m_bIsGuildSkill = true;
	}

	//是否是工会技能
	bool IsGuildSkill()
	{
		return m_bIsGuildSkill;
	}
	

private:
	CMuxSkillProp* m_pSkillProp;		//数据
	SkillInfo_i  m_dbData;			//数据库的数据
	unsigned int m_dbIndex;			//数据库数组中的索引
	CPlayer* m_pOwner;				//拥有该玩家的
	CUnion* m_pUnionOwner;			//拥有该技能的工会
	bool m_bIsGuildSkill;			//是否是工会技能 
};
