﻿
#ifndef NPCSTANDRULE_H
#define NPCSTANDRULE_H

class CPlayer;
class CMonster;

enum ENpcRaceAttribute
{
	NPC_RACE_HUMAN = 1,		//人类
	NPC_RACE_ERKA = 2,		//艾卡
	NPC_RACE_ELF = 3,		//精灵
	NPC_RACE_MONSTER = 20,	//怪物
	NPC_RACE_NEUTRAL = 99  //中立
};

enum ENpcType
{
	NPC_TYPE_TASK = 11,			//任务NPC
	NPC_TYPE_FUNCTION = 12,		//功能NPC 
	NPC_TYPE_DIANZHUI = 13,		//点缀NPC
	NPC_TYPE_INVICIBLE = 14,	//无敌NPC
	NPC_TYPE_GUARD = 15,		//护卫NPC
	NPC_TYPE_FOLLOW = 16,		//护送NPC
	NPC_TYPE_COLLECT = 17,		//采集NPC
	NPC_TYPE_MARK = 18,			//城战标志NPC
	NPC_TYPE_MONSTER = 20		//怪物NPC
};

class CNpcStandRule
{
private:
	CNpcStandRule(void);
	~CNpcStandRule(void);

public:
	//在NPC对话时的判断
	static bool OnTalkingToNpc( CPlayer* pPlayer, CMonster* pMonster );

	//当攻击NPC时,类似于坏技能
	static bool OnAttackNpc( CPlayer* pAttackPlayer, CMonster* pTargetMonster );

	//当给NPC使用辅助技能时,类似于好技能
	static bool OnUseAssistSkillToNpc( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster );
};

#endif  /*NPCSTANDRULE_H*/
