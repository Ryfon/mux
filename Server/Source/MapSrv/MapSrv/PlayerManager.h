﻿/* @file PoolManager.h 
@brief 玩家的名字查询类，所以此mapserver上的玩家都会集中在此
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：hyn
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/



#pragma once
#include <map>
#include <string>
#include <queue>
using namespace std;

#include "../../Base/aceall.h"
#include "../../Base/PkgProc/CliProtocol_T.h"
using namespace MUX_PROTO;

class CPlayer;


class CPlayerManager
{
protected:
	CPlayerManager(void);
public:
	~CPlayerManager(void);

	///初始操作；
	static bool Init();

	///释放处理(对于m_deferredOfflinePlayers中还未清理的pPlayer作清理操作);
	static void Release();

	///插入玩家
	static bool InsertAccountPlayer( const char* playerAccount, CPlayer* pPlayer, unsigned long playerPID/*用于调试, 05.19*/);

	///添加角色名<-->玩家映射；
	static bool InsertRolePlayer( const char* roleName, CPlayer* pPlayer );
	
	///通过名字寻找所有mapsrv上的玩家
	static CPlayer* FindPlayerByAccount(const string& accountName);

	///通过角色名寻找所有mapsrv上的玩家
	static CPlayer* FindPlayerByRoleName(const string& roleName);

	///删除玩家
	static bool DeletePlayer(CPlayer* pPlayer);

	///慢时钟
	static void PlayerSlowTimer();

	///延迟离线慢时钟
	static void RegisterDeferredOffline(CPlayer *pPlayer);

	///延迟离线慢时钟
	static void ProcessDeferredOffline();

	static unsigned int GetPlayerSize();

private:
	static map<string, CPlayer*> m_mapAccountPlayer;

	static map<string, CPlayer*> m_mapRolePlayer;

	static ACE_Time_Value m_lastSlowTimeExecTime;

	static std::queue<CPlayer*> m_deferredOfflinePlayers;

	static vector<CPlayer*>     m_finalDelPlayers;//已做完所有删除逻辑，只等最后释放回池的玩家；
};
