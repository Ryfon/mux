﻿#include "GroupTeam.h"
#include "GroupTeamManager.h"
#include "MuxMap/muxmapbase.h"

void CGroupTeam::ExitGroupTeam( const PlayerID& playerID )
{
	TRY_BEGIN;

	if( mGroupMemberVec.empty() )
		return;

	//玩家是否存在在该组队中
	bool beRemove = false;
	std::vector<TeamPlayer>::iterator lter = mGroupMemberVec.begin();
	for ( ;lter!= mGroupMemberVec.end(); ++lter )
	{
		TeamPlayer& tp = *lter;
		if ( tp.playerID == playerID )
		{
			lter = mGroupMemberVec.erase(lter);
			
			//通知组队玩家的状态改变了
			NotifyChangeTeamState( playerID, 0x00 );
			beRemove = true;
			break;
		}
	}

	if ( beRemove )
	{
		//将玩家从该组队对应地图编号的管理中删除
		std::vector<TeamMemberControl *>::iterator lter = mGroupTeamMemberControlVec.begin(),
			                                    lterEnd = mGroupTeamMemberControlVec.end();
		for (; lter != lterEnd; ++lter )
		{
			TeamMemberControl* tmpPickCor = *lter;
			if ( tmpPickCor && tmpPickCor->RemoveTurnPlayer( playerID) )
				break;
		}
	}

	TRY_END;
}

//因为切换地图,离开组队
bool CGroupTeam::OnSwitchMapExitGroupTeam(CPlayer* pPlayer, unsigned int mapID )
{
	if ( !pPlayer )
		return false;

	TRY_BEGIN;

	//切换地图，从对应的管理器中移出改玩家
	TeamMemberControl* pControl = GetTeamMemberControl( mapID );
	if ( pControl && pControl->IsPlayerExist( pPlayer ) )
	{
		pControl->RemoveTurnPlayer( pPlayer->GetFullID() );
		return IsMemberEmpty();
	}
	TRY_END;

	return false;
}


//是否含有组队队长,返回队长在服务器的指针
bool CGroupTeam::GetGroupTeamLeaderID(PlayerID& playerID )
{
	TRY_BEGIN;

	playerID = mLeaderPlayerID;
	return true;
	TRY_END;

	return false;
}


void CGroupTeam::SendTeamMemberDetailInfo( MGTeamMemberDetailInfoUpdate* pDetailMsg, CPlayer* exceptPlayer )
{
	TRY_BEGIN;

	if ( !exceptPlayer )
		return;

	if ( !pDetailMsg )
		return;

	for( std::vector<TeamPlayer>::iterator iter = mGroupMemberVec.begin(); iter != mGroupMemberVec.end(); ++iter )
	{
		   TeamPlayer& tm = *iter;

			CGateSrv* pProc = CManG_MProc::FindProc( tm.playerID.wGID );
			if( NULL == pProc )
			{
				D_ERROR("CGroupTeam::SendTeamMemberDetailInfo 找不到GATESRV %d\n",tm.playerID.wGID );
				continue;
			}
			
			pDetailMsg->lNotiPlayerID = tm.playerID;
#ifdef GATE_SENDV
			bool isNewPkg = false;
			MsgToPut* pNewMsg = CreateGateSrvPkg( MGTeamMemberDetailInfoUpdate, pProc, *pDetailMsg, isNewPkg );
			if ( isNewPkg )
			{
				pProc->SendOldRsvNew( pNewMsg );
			}
#else  //GATE_SENDV
			MsgToPut* pNewMsg = CreateSrvPkg( MGTeamMemberDetailInfoUpdate, pProc, *pDetailMsg );
			pProc->SendPkgToGateServer( pNewMsg );
#endif //GATE_SENDV
	}

	TRY_END;
}

void CGroupTeam::GetNewItemNoticeOtherMembers( CPlayer* pMember,const ItemInfo_i& itemEle )
{
	TRY_BEGIN;

	if ( !pMember )
		return;

	MGNoticeTeamMemberGetNewItem noticePickItem;
	noticePickItem.itemTypeID = itemEle.usItemTypeID;
	noticePickItem.pickPlayerID = pMember->GetFullID();
	noticePickItem.teamID  =  GetGroupTeamID();
	pMember->SendPkg<MGNoticeTeamMemberGetNewItem>( &noticePickItem );

	//TeamMemberControl* pControl = GetTeamMemberControl( pMember->GetMapID() );
	//if ( !pControl )
	//	return;

	//const std::vector<CPlayer *>& teamPlayerVec = pControl->GetTurnPickerVec();
	//if ( teamPlayerVec.empty() )
	//	return;

	////只通知在同一个地图上的玩家拾取道具的情况
	//MGTeamMemberPickItem memberPickItem;
	//memberPickItem.itemTypeID  =  itemEle.usItemTypeID;
	//memberPickItem.pickPlayerID = pMember->GetFullID();
	//for ( std::vector<CPlayer *>::const_iterator iter = teamPlayerVec.begin(); iter != teamPlayerVec.end(); ++iter )
	//{
	//	CPlayer* pPlayer = *iter;
	//	if ( pPlayer != NULL )
	//	{
	//		pPlayer->SendPkg<MGTeamMemberPickItem>( &memberPickItem );
	//	}
	//}

	TRY_END;
}


void CGroupTeam::RecvGroupMemberInfo(const PlayerID * playerArr, const int* memberSeqArr, int count, int leaderIndex )
{
	TRY_BEGIN;

	if ( !playerArr || !memberSeqArr )
		return;

	if ( count <= 0 )
		return;

	if ( leaderIndex >= count  )
		return;

	//设置队长的ID号
	mLeaderPlayerID = playerArr[leaderIndex];

	//清空地图保存的组队信息
	mGroupMemberVec.clear();
	ClearTeamMemberControl();

	//根据传来的组队信息，重新组织组队成员管理器
	MGChangeTeamState srvmsg;
	for ( unsigned int i = 0 ; i< (unsigned int)count; ++i )
	{
		CGroupTeam::TeamPlayer tp;
		tp.playerID =  playerArr[i];
		mGroupMemberVec.push_back( tp );
		CGateSrv* pGateSrv = CManG_MProc::FindProc( tp.playerID.wGID );
		if ( pGateSrv )
		{
			CPlayer* pGroupPlayer = pGateSrv->FindPlayer( tp.playerID.dwPID );
			if ( pGroupPlayer != NULL )
			{
				if ( !pGroupPlayer->GetSelfStateManager().IsGroupState() )
				{
					CGroupState& groupState = pGroupPlayer->GetSelfStateManager().GetGroupState();
					groupState.OnBegin();
					groupState.SetOwnerGroupTeam( this );
				}

				if ( i == (unsigned int)leaderIndex )//如果该玩家是队长
				{
					//[玩家组队队长状态改变时 simpleinfochange]
					pGroupPlayer->OnSimpleInfoChange();
				}


				//获取玩家所属的地图编号
				unsigned mapID = pGroupPlayer->GetMapID();
				//根据玩家的地图编号，插入组队对应地图的管理器
				TeamMemberControl* pControl = GetTeamMemberControl( mapID );
				if ( !pControl )
					pControl = NewTeamMemberControl( mapID );

				if ( pControl != NULL )
					pControl->PushTurnPlayer( pGroupPlayer );

				srvmsg.changePlayerID = pGroupPlayer->GetFullID();
				srvmsg.teamState = ( i == (unsigned int)leaderIndex )?0x02:0x01;
				CMuxMap* pMap = pGroupPlayer->GetCurMap();
				if( pMap )
					pMap->NotifySurrounding<MGChangeTeamState>( &srvmsg, pGroupPlayer->GetPosX(), pGroupPlayer->GetPosY() );
			}
			else
			{
				D_ERROR("据GateSrv传来的组队玩家的PlayerID,找不到组队的玩家 PID:%d\n", tp.playerID.dwPID  );
			}
		}
		else
		{
			D_ERROR("依据GateSrv传来的组队玩家的PlayerID,找不到组队的玩家的GateSrv:%d\n",tp.playerID.wGID );
		}

	}

	TRY_END;
}

//更新组队信息
void CGroupTeam::UpdateGroupTeamInfo(ITEM_SHAREMODE itemShareMode, 
									 EXPERIENCE_SHAREMODE expreShareMode, 
									 ROLLITEM_MODE rollItemLevel )
{
	TRY_BEGIN;

	mItemShareMode = itemShareMode;
	mExpreShareMode = expreShareMode;

	switch( rollItemLevel )
	{
	case E_GREEN_LEVEL:
		mRollItemLevel = E_GREEN;
		break;
	case E_BLUE_LEVEL:
		mRollItemLevel = E_BLUE;
		break;
	/*去掉物品质中紫色红色相关(zsw/2010.8.13)
	case E_PURPLE_LEVEL:
		mRollItemLevel = E_PURPLE;
		break;*/
	case E_ORANGE_LEVEL:
		mRollItemLevel = E_ORANGE;
		break;
	default:
		mRollItemLevel = E_ORANGE;
		break;
	}

	TRY_END;
}

void CGroupTeam::NotifyChangeTeamState( const PlayerID& changePlayerID, char state )
{
	CGateSrv* pGateSrv = CManG_MProc::FindProc( changePlayerID.wGID );
	if( pGateSrv )
	{
		CPlayer* oldLeader = pGateSrv->FindPlayer( changePlayerID.dwPID );
		if( oldLeader )
		{
			//[玩家组队状态发生改变了，现在为队长发生变化时的通知 simpleinfochange]
			oldLeader->OnSimpleInfoChange();

			CMuxMap* pMap = oldLeader->GetCurMap();
			if( pMap )
			{
				MGChangeTeamState srvmsg;
				srvmsg.changePlayerID = oldLeader->GetFullID();
				srvmsg.teamState = state;
				pMap->NotifySurrounding<MGChangeTeamState>( &srvmsg, oldLeader->GetPosX(), oldLeader->GetPosY() );
			}
		}
	}
}

void CGroupTeam::OnDisBandGroupTeam()
{
	TRY_BEGIN;

	//结束组队中属于该MapSrv的玩家的组队状态
	std::vector<TeamMemberControl *>::iterator lter = mGroupTeamMemberControlVec.begin();
	for (; lter != mGroupTeamMemberControlVec.end(); ++lter )
	{
		TeamMemberControl* pControl = *lter;
		if ( pControl )
		{
			pControl->TeamDisBandNotify();
			pControl->ReleaseAllPlayerGroupState();
		}
	}

	//然后回收此组队对象
	GroupTeamManagerSingle::instance()->RecoverGroupTeam( this );

	TRY_END;
}

bool CGroupTeam::IsPlayerExists(const PlayerID& playerID )
{
	TRY_BEGIN;

	if ( mGroupMemberVec.empty() )
		return false;

	std::vector<TeamPlayer>::iterator lter = mGroupMemberVec.begin();
	for ( ;lter != mGroupMemberVec.end(); ++lter )
	{
		TeamPlayer& tm = (*lter);
		if ( tm.playerID == playerID )
			return true;
	}

	TRY_END;

	return false;
}

bool CGroupTeam::IsMemberEmpty()
{
	TRY_BEGIN;

	if ( mGroupMemberVec.empty() )
		return true;

	std::vector<TeamMemberControl *>::iterator lter = mGroupTeamMemberControlVec.begin();
	for (; lter != mGroupTeamMemberControlVec.end(); ++lter )
	{
		TeamMemberControl* pControl = *lter;
		if ( pControl && pControl->HaveExistPlayer() )//只要有一个控制器不为空，则代表在该MapSrv上还有属于该组队的玩家
			return false;
	}

	return true;
	TRY_END;

	return false;
}

//@param1 经验的来源者
//@param2 初始经验的值
//@param3 打死怪物的等级
void CGroupTeam::ShareExpreOnGroupTeam( CPlayer* expreMainPlayer, long expre ,int monsterLevel, bool isExpCritical, bool isBossMonster )
{
	if ( !expreMainPlayer )
		return;

	TRY_BEGIN;

	////组队中获取处于同一地图上的玩家
	TeamMemberControl* pControl =  GetTeamMemberControl( expreMainPlayer->GetMapID() );
	if ( !pControl )
		return;

	const std::vector<CPlayer *>& sameMapGroupMemberVec = pControl->GetTurnPickerVec();
	if ( sameMapGroupMemberVec.empty() )
		return;

	//获取地图玩家的总等级 
	unsigned int totalLevel = 0;
	std::vector<CPlayer *>::const_iterator  lter     = sameMapGroupMemberVec.begin(),
		                                    lterEnd  = sameMapGroupMemberVec.end();
	for ( ; lter!= lterEnd; ++lter )
	{
		CPlayer* pplayer = *lter;
		if ( pplayer && !pplayer->GetSelfStateManager().IsInSafeArea() )//如果不在安全区才能分享经验
		{
			totalLevel += pplayer->GetLevel();
		}
	}

	int k1 = 1;
	switch( mExpreShareMode )
	{
	//独享模式
	case E_EXCLUSIVE_MODE:
		{
			//分享打怪经验的%2
			float shareExpre = (float)(k1* expre * 0.02);
			for ( std::vector<CPlayer *>::const_iterator lter = sameMapGroupMemberVec.begin();
				lter!=sameMapGroupMemberVec.end();
				++lter )
			{
				CPlayer* player = *lter;
				if ( !player )
					continue;

				if ( player->GetSelfStateManager().IsInSafeArea() )
				{
					D_DEBUG("玩家%s在安全区，不能分享组队经验\n", player->GetNickName() );
					continue;
				}

				//是否是死亡状态
				if ( player->IsDying() )
				{
					continue;
				}

				//新加经验公式后，5级经验0去除，by dzj, 10.06.21;//如果玩家的等级比怪物大5级
				//int level = (int)player->GetLevel();
				//if ( ( level>monsterLevel) && 
				//	 ( ( level - monsterLevel ) > 5 ) )
				//{
				//	continue;
				//}

				if ( player->GetFullID() !=  expreMainPlayer->GetFullID() )
				{
					player->AddExp( (int)shareExpre, GCPlayerLevelUp::IN_BATTLE, isExpCritical );

					//增加经验会影响好友收益及好友度
					player->AddExpAffectFriendGain(shareExpre, isBossMonster);
				}
				else
				{
					expreMainPlayer->AddExp( (int)expre, GCPlayerLevelUp::IN_BATTLE, isExpCritical );

					//增加经验会影响好友收益及好友度
					expreMainPlayer->AddExpAffectFriendGain(expre, isBossMonster);
				}
			}
		}
		break;
	//共享模式
	case E_SHARE_MODE:
		{
			int n  = (int)sameMapGroupMemberVec.size();
			if ( n <= 0 )
				return;

			for ( std::vector<CPlayer *>::const_iterator lter = sameMapGroupMemberVec.begin();
				lter!=sameMapGroupMemberVec.end();
				++lter )
			{
				CPlayer* player = *lter;
				if ( !player )
					continue;

				if ( player->GetSelfStateManager().IsInSafeArea() )
				{
					D_DEBUG("玩家%s在安全区，不能分享组队经验\n", player->GetNickName() );
					continue;
				}


				//是否是死亡状态
				if ( player->IsDying() )
				{
					continue;
				}

				int level = (int)player->GetLevel();
				//新加经验公式后，5级经验0去除，by dzj, 10.06.21;//如果玩家的等级比怪物大5级
				//if ( ( level>monsterLevel) && 
				//	( ( level - monsterLevel ) > 5 ) )
				//{
				//	continue;
				//}


				//公式
				float k2 = (float)(( 1+0.1*(n-1))*level/totalLevel);
				if( k2 >= 1.5f )
					k2 = 1.5f;

				int oldExp = player->GetExp();
				int shareExpre = (int)( k1*k2*expre ); 
				player->AddExp( (int)shareExpre, GCPlayerLevelUp::IN_BATTLE, isExpCritical );

				//增加经验会影响好友收益及好友度
				player->AddExpAffectFriendGain(shareExpre, isBossMonster);
			}
		}
		break;
	default:
		break;
	}

	TRY_END;
}

//设置组队的队长
void CGroupTeam::SetGroupTeamLeaderIndex(const MUX_PROTO::PlayerID &playerID)
{
	TRY_BEGIN;

	if( mLeaderIndex >= mGroupMemberVec.size() )
		return;

	TeamPlayer oldLeaderPlayer = mGroupMemberVec[mLeaderIndex];
	if( playerID == oldLeaderPlayer.playerID ){
		return;			//更改的队长相同无需改动
	}

	NotifyChangeTeamState( oldLeaderPlayer.playerID, 0x01 );
	NotifyChangeTeamState( playerID,  0x02 );

	std::vector<TeamPlayer>::iterator lter = mGroupMemberVec.begin();
	for ( ; lter!= mGroupMemberVec.end(); ++lter )
	{
		TeamPlayer& tp = *lter;
		if ( tp.playerID == playerID ){
			break;
		}
	}

	if ( lter!= mGroupMemberVec.end() )
	{
		mLeaderIndex = (TYPE_UI8)( lter - mGroupMemberVec.begin() );
		mLeaderPlayerID = playerID;
	}
	else
	{
		D_ERROR("CGroupTeam::SetGroupTeamLeaderIndex 设置组队队长出错\n");
	}

	TRY_END;
}

void CGroupTeam::KickPlayerOnTeam(const PlayerID& playerID )
{
	TRY_BEGIN;

	ExitGroupTeam( playerID );

	TRY_END;

}


unsigned int CGroupTeam::GetTeamMemberInUnSafeArea(unsigned int mapID, std::vector<CPlayer *>& playerVec )
{
	TeamMemberControl* pControl = GetTeamMemberControl( mapID );
	if ( !pControl )
		return 0;

	const std::vector<CPlayer * >& turnPlayerVec = pControl->GetTurnPickerVec();
	if ( turnPlayerVec.empty() )
		return 0;

	for ( std::vector<CPlayer *>::const_iterator lter = turnPlayerVec.begin(); lter != turnPlayerVec.end(); ++lter)
	{
		CPlayer* pPlayer = *lter;
		if ( pPlayer && !pPlayer->GetSelfStateManager().IsInSafeArea() )
		{
			playerVec.push_back( pPlayer );
		}
	}
	//std::copy( turnPlayerVec.begin(),turnPlayerVec.end(), back_inserter(playerVec) );
	
	return (unsigned int)playerVec.size();
}


unsigned int CGroupTeam::GetTeamMemberOnTheMap(unsigned int mapID, std::vector<CPlayer *>& playerVec )
{
	TeamMemberControl* pControl = GetTeamMemberControl( mapID );
	if ( !pControl )
		return 0;

	const std::vector<CPlayer * >& turnPlayerVec = pControl->GetTurnPickerVec();
	if ( turnPlayerVec.empty() )
		return 0;

	std::copy( turnPlayerVec.begin(),turnPlayerVec.end(), back_inserter(playerVec) );

	return (unsigned int)playerVec.size();
}


TeamMemberControl* CGroupTeam::GetTeamMemberControl(unsigned int mapID )
{
	TRY_BEGIN;

	if ( mGroupTeamMemberControlVec.empty() )
		return NULL;

	std::vector<TeamMemberControl *>::iterator lter = mGroupTeamMemberControlVec.begin();
	for ( ; lter !=mGroupTeamMemberControlVec.end(); ++lter )
	{
		TeamMemberControl* pControl = *lter;
		if ( NULL == pControl )
		{
			D_ERROR( "GetTeamMemberControl,不可能取址错误\n" );
			continue;
		}
		if ( pControl->GetOwnerMap() == mapID )
		{
			return pControl;
		}
	}

	return NULL;
	TRY_END;
	return NULL;
}

void CGroupTeam::ClearTeamMemberControl()
{
	TRY_BEGIN;

	if ( mGroupTeamMemberControlVec.empty() )
		return;

	std::vector<TeamMemberControl *>::iterator lter = mGroupTeamMemberControlVec.begin();
	for ( ; lter !=mGroupTeamMemberControlVec.end(); ++lter )
	{
		TeamMemberControl* control = *lter;
		if ( control )
			control->ClearTurnPickPlayer();
	}

	TRY_END;
}

bool CGroupTeam::CheckTeamMemberHaveTask(unsigned int taskID ,unsigned int mapID )
{
	TeamMemberControl* pTurnPicker = GetTeamMemberControl( mapID );
	if ( pTurnPicker )
		return pTurnPicker->CheckHaveTask( taskID );

	return false;
}

void CGroupTeam::NoticeTeamMemberGetNewItem(const PlayerID& pickPlayerID, unsigned int itemTypeID )
{
	if ( mGroupTeamMemberControlVec.empty() )
		return;

	std::vector<TeamMemberControl *>::iterator lter = mGroupTeamMemberControlVec.begin();
	for ( ; lter !=mGroupTeamMemberControlVec.end(); ++lter )
	{
		TeamMemberControl* control = *lter;
		if ( control )
			control->TeamMemberGetNewItem(  pickPlayerID, itemTypeID );
	}
}

void CGroupTeam::TeamPlayerKillNPCEvent( const PlayerID& killNPCPlayer, unsigned int mapID, unsigned int reasonObjID, unsigned int reasonObjNum )
{
	TRY_BEGIN;

	//获取该地图的玩家
	TeamMemberControl* pTurnPicker = GetTeamMemberControl( mapID );

	if ( pTurnPicker )
	{
		//获取在该地图上的所有玩家
		const std::vector<CPlayer *>& playerVec= pTurnPicker->GetTurnPickerVec();
		for ( std::vector<CPlayer *>::const_iterator lter = playerVec.begin(); lter!= playerVec.end(); ++lter )
		{
			CPlayer* pPlayer = *lter;
			if ( pPlayer && pPlayer->GetFullID() != killNPCPlayer && !pPlayer->GetSelfStateManager().IsInSafeArea() )//玩家不能在安全区
			{
				pPlayer->TeamMemberKillNpcEvent( reasonObjID, reasonObjNum );
			}
		}
	}

	TRY_END;
}

void CGroupTeam::OnChangePickItemMode()
{
	TRY_BEGIN;

	if ( mGroupTeamMemberControlVec.empty() )
		return;

	std::vector<TeamMemberControl *>::iterator lter = mGroupTeamMemberControlVec.begin();
	for ( ; lter !=mGroupTeamMemberControlVec.end(); ++lter )
	{
		TeamMemberControl* control = *lter;
		if ( control )
			control->ResetPickItemIndex();
	}

	TRY_END;
}

TeamMemberControl* CGroupTeam::NewTeamMemberControl(unsigned int mapID)
{
	TRY_BEGIN;

	TeamMemberControl* newMapControl = NEW TeamMemberControl;
	if ( newMapControl )
	{
		newMapControl->Init( mapID );
		mGroupTeamMemberControlVec.push_back( newMapControl );
		return newMapControl;
	}

	TRY_END;

	return NULL;
}

CPlayer* CGroupTeam::IncPickItemIndexOnTurnMode( unsigned mapID )
{
	TRY_BEGIN;

	//轮流模式和队伍模式才能调用此函数
	if ( mItemShareMode != E_TURN_MODE && mItemShareMode != E_TEAM_MODE )
	{
		D_ERROR("IncPickItemIndexOnTurnMode 在非法状态调用 %d\n", mItemShareMode );
		return NULL;
	}

	CPlayer* pPlayer = NULL;
	TeamMemberControl* pControl = GetTeamMemberControl( mapID );
	if ( pControl )
		pPlayer = pControl->NowTurnPlayer();
	
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGroupTeam::IncPickItemIndexOnTurnMode, NULL == pPlayer\n" );
		return NULL;
	}
	return pPlayer;

	TRY_END;

	return NULL;
}

void CGroupTeam::UpdateGroupTeamItemShareMode( ITEM_SHAREMODE itemShareMode )
{
	TRY_BEGIN;

	mItemShareMode = itemShareMode;
	OnChangePickItemMode();

	TRY_END;
}

void TeamMemberControl::PushTurnPlayer(CPlayer *pPlayer)
{
	if( !pPlayer )
	{
		D_ERROR("TeamMemberControl::PushTurnPlayer 插入空指针\n");
		return;
	}

	if ( pPlayer && pPlayer->GetMapID() == mMapID )
		mTurnPickVec.push_back( pPlayer );
}

//本次轮流拾取轮到的玩家
CPlayer* TeamMemberControl::NowTurnPlayer()
{
	TRY_BEGIN;

	if ( mPickItemIndex >= mTurnPickVec.size() )
		mPickItemIndex = 0;

	CPlayer* pPlayer = NULL;
	if ( mPickItemIndex < mTurnPickVec.size() )
	{
		pPlayer = mTurnPickVec[mPickItemIndex];
		mPickItemIndex++;
	}

	//assert( pPlayer!= NULL );
	if ( NULL == pPlayer )
	{
		D_ERROR( "TeamMemberControl::NowTurnPlayer, NULL==pPlayer\n" );
		return NULL;
	}
	return pPlayer;

	TRY_END;
	
	return NULL;
}

//释放单个玩家的组队状态
void TeamMemberControl::ReleasePlayerGroupState(CPlayer* pPlayer )
{
	TRY_BEGIN;

	if ( !pPlayer )
		return;

	pPlayer->GetSelfStateManager().GetGroupState().OnEnd();

	TRY_END;
}

//释放该控制器控制的所有玩家的组队状态
void TeamMemberControl::ReleaseAllPlayerGroupState()
{
	TRY_BEGIN;

	std::vector<CPlayer *>::iterator lter = mTurnPickVec.begin();
	for ( ;lter!= mTurnPickVec.end(); ++lter )
	{
		CPlayer* tmpPlayer = *lter;
		if ( tmpPlayer )
			ReleasePlayerGroupState( tmpPlayer );
	}
	TRY_END;
}

void TeamMemberControl::TeamDisBandNotify()
{
	TRY_BEGIN;

	std::vector<CPlayer *>::iterator lter = mTurnPickVec.begin();
	for ( ;lter!= mTurnPickVec.end(); ++lter )
	{
		CPlayer* tmpPlayer = *lter;
		if ( tmpPlayer )
		{
			CMuxMap* pMap = tmpPlayer->GetCurMap();
			if( pMap )
			{
				MGChangeTeamState srvmsg;
				srvmsg.changePlayerID = tmpPlayer->GetFullID();
				srvmsg.teamState = 0x00;
				pMap->NotifySurrounding<MGChangeTeamState>( &srvmsg, tmpPlayer->GetPosX(), tmpPlayer->GetPosY() );
			}
		}
	}

	TRY_END;
}


//该玩家在控制器中是否存在
bool TeamMemberControl::IsPlayerExist(CPlayer *pPlayer)
{
	TRY_BEGIN;

	if ( !pPlayer )
		return false;

	for ( std::vector<CPlayer *>::iterator lter = mTurnPickVec.begin();lter!= mTurnPickVec.end(); ++lter )
	{
		CPlayer* pTmp =*lter;
		if ( pTmp && pTmp->GetFullID() == pPlayer->GetFullID() )
			return true;
	}

	TRY_END;

	return false;
}

bool TeamMemberControl::CheckHaveTask(unsigned int taskID )
{
	TRY_BEGIN;

	for ( std::vector<CPlayer *>::iterator lter = mTurnPickVec.begin();lter!= mTurnPickVec.end(); ++lter )
	{
		//不在安全区,且玩家身上有任务时 
		CPlayer* pTmp =*lter;
		if ( pTmp &&
			 !pTmp->GetSelfStateManager().IsInSafeArea() &&
			 pTmp->FindPlayerTask(taskID) != NULL )
		{
			return true;
		}
	}

	TRY_END;

	return false;
}

void TeamMemberControl::TeamMemberGetNewItem(const PlayerID& pickPlayerID, unsigned int itemTypeID )
{
	TRY_BEGIN;

	if ( mTurnPickVec.empty() )
		return;

	for ( std::vector<CPlayer *>::iterator lter = mTurnPickVec.begin();lter!= mTurnPickVec.end(); ++lter )
	{
		CPlayer* pTmp =*lter;
		if ( pTmp )
		{
			MGTeamMemberPickItem memberPickItem;
			memberPickItem.itemTypeID = itemTypeID;
			memberPickItem.pickPlayerID = pickPlayerID;
			pTmp->SendPkg<MGTeamMemberPickItem>( &memberPickItem );
		}
	}

	TRY_END;
}

bool TeamMemberControl::RemoveTurnPlayer(const PlayerID& playerID )
{
	TRY_BEGIN;

	if ( mTurnPickVec.empty() )
		return false;

	bool isRemove = false;
	std::vector<CPlayer *>::iterator lter = mTurnPickVec.begin();
	for ( ;lter!= mTurnPickVec.end(); ++lter )
	{
		CPlayer* pTmp = *lter;
		if ( pTmp->GetFullID() == playerID )
		{
			int index = (int)(lter -  mTurnPickVec.begin());
			if ( index >= 0 )
			{
				mTurnPickVec.erase(lter);
				if ( (int)mPickItemIndex > index )
					mPickItemIndex--;

				if ( mPickItemIndex == mTurnPickVec.size() )
					mPickItemIndex--;
		
				isRemove = true;
				//释放该玩家的组队状态
				ReleasePlayerGroupState( pTmp );
			}
			break;
		}
	}

	if ( mTurnPickVec.empty() )
		mPickItemIndex = 0;

	return isRemove;

	TRY_END;

	return false;
}
