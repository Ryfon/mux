﻿#pragma once
#include <string>
#include <map>
#include "BuffProp.h"
#include "AttackSkillProp.h"
#include "ManObject.h"
#include "PkgProc/CliProtocol.h"
#include "XmlEventHandler.h"
#include "XmlManager.h"

using namespace std;
using namespace MUX_PROTO; 


//技能等级经验值
typedef struct StrSkillLevelExp
{
	unsigned long phyNextLevelExp;
	unsigned long phyLastLevelExp;
	unsigned long magNextLevelExp;
	unsigned long magLastLevelExp;
	char szSkillTitle[32];   //称号
	unsigned short usMaxGetPhyPoint;	//该等级最多获得的物理技能点数
	unsigned short usMaxGetMagPoint;	//该等级最多获得的魔法技能点数
}SkillLevelExp;

//技能点经验值
typedef struct StrSkillPointExp
{
	unsigned long phyNextPointExp;
	unsigned long phyLastPointExp;
	unsigned long magNextPointExp;
	unsigned long magLastPointExp;
}SkillPointExp;	


//下面是5个职业具体的职业技能升级方案
class CSkillExp
{
public:
	bool FindLevelExp(int nLevel,SkillLevelExp& outSkillLevelExp)
	{
		map<int, SkillLevelExp>::iterator iter = m_mapSkillLevelExp.find( nLevel );
		if( iter != m_mapSkillLevelExp.end() )
		{
			outSkillLevelExp = iter->second;
			return true;
		}
		return false;
	}

	bool FindPointExp(int nPointNum, SkillPointExp& outSkillPointExp)
	{
		map<int, SkillPointExp>::iterator iter = m_mapSkillPointExp.find( nPointNum );
		if( iter != m_mapSkillPointExp.end() )
		{
			outSkillPointExp = iter->second;
			return true;	
		}
		return false;
	}

	//设置对象的标识号
	void SetObjID(int nClass)
	{
		m_lClass = nClass;
	}

	unsigned long GetObjID()	//返回该对象的标识号
	{
		return m_lClass;
	}

	
	//读取技能等级配置
	bool LoadFromSkillLevelXML(const char* szFileName);


	//读取技能点数配置
	bool LoadFromSkillPointXML(const char* szFileName);

	unsigned short GetMaxPhyPoint()
	{
		return m_usMaxPhyPoint;
	}

	unsigned short GetMaxMagPoint()
	{
		return m_usMaxMagPoint;
	}


	unsigned short GetMaxSkillLevel()
	{
		return m_usMaxSkillLevel;
	}




	CSkillExp():m_lClass(0),m_usMaxPhyPoint(0),m_usMaxMagPoint(0),m_usMaxSkillLevel(0){}  //constructor
	~CSkillExp(){}   //destrucotor

private:
	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
	{
		if ( NULL == pEle->GetAttr( attName ) )
		{
			D_ERROR( "玩家技能经验XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
			return false;
		}
		return true;
	}

private:
	unsigned long m_lClass;		//对应的职业ID号
	map<int, SkillLevelExp> m_mapSkillLevelExp;		//技能等级
	map<int, SkillPointExp> m_mapSkillPointExp;		//技能点数

private:
	unsigned short m_usMaxPhyPoint;
	unsigned short m_usMaxMagPoint;
	unsigned short m_usMaxSkillLevel;	
};


class CManSkillExp : public CObjectManager<CSkillExp>
{
public:
	static bool Init();

protected:
	CManSkillExp(){}
};

