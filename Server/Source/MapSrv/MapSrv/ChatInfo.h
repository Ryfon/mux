﻿/********************************************************************
	created:	2008/11/04
	created:	4:11:2008   9:34
	file:		ChatInfo.h
	author:		管传淇
	
	purpose:	将子建写的类从CPlayer.h中移出,使用一个单独的文件存放.
*********************************************************************/
#pragma once

#include <stdlib.h>

class CPlayer;
class CMonster;
class CBaseItem;

///NPC聊天相关信息，保存聊天的对象以及当前聊天所进行到的阶段；
struct NpcChatInfo 
{
public:
	NpcChatInfo() 
	{
		Init();
	}

	~NpcChatInfo()
	{
		ClearNpcChatTarget();
	}

public:
	void Init()
	{
		pChatTarget = NULL;
		dwCurStage = 0ul;
	}

	void ClearNpcChatTarget();

public:
	///设置交谈目标；
	void SetNpcChatTarget( CMonster* pMonster, int nOption );
	///取交谈目标；
	CMonster* GetNpcChatTarget() { return pChatTarget; }
	///设置当前交谈阶段；
	void SetNpcChatStage( unsigned long chatStage ) { dwCurStage = chatStage; }
	///得到当前交谈阶段；
	unsigned long GetNpcChatStage() { return dwCurStage; }

public:
	///选项选择，nOption==0表示初始选择NPC；
	bool OnNpcChatOption( CPlayer* pOwner, int nOption );

private:
	CMonster* pChatTarget;//交谈对象；
	unsigned long dwCurStage;//当前的所处的交谈阶段
};

//09.04.28,取消工会任务新实现,class CNormalScript;
/////任务聊天相关信息，保存聊天的任务ID以及当前聊天所进行到的阶段；
//struct TaskChatInfo 
//{
//public:
//	TaskChatInfo() 
//	{
//		Init();
//	}
//
//	~TaskChatInfo()
//	{
//		ClearTaskChatTarget();
//	}
//
//public:
//	void Init()
//	{
//		m_taskID = 0ul;
//		m_CurStage = 0ul;
//		m_pScript = NULL;
//	}
//
//	void ClearTaskChatTarget();
//
//public:
//	///设置交谈目标；
//	bool SetTaskChatTarget( unsigned int taskID, int nOption, bool isSrvSet/*是否服务器端主动调用，还是客户端发来消息导致调用*/ );
//	///设置当前交谈阶段；
//	void SetTaskChatStage( unsigned long chatStage ) { m_CurStage = chatStage; }
//	///得到当前交谈阶段；
//	unsigned long GetTaskChatStage() { return m_CurStage; }
//
//public:
//	///选项选择，nOption==0表示初始选择NPC；
//	bool OnTaskChatOption( CPlayer* pOwner, int nOption );
//
//private:
//	unsigned int   m_taskID;//交谈任务ID号；
//	unsigned long  m_CurStage;//当前的所处的交谈阶段
//	CNormalScript* m_pScript;//对应的任务脚本；
//};

///Item聊天相关信息，保存聊天的对象以及当前聊天所进行到的阶段；
struct ItemChatInfo 
{
public:
	ItemChatInfo() 
	{
		Init();
	}

	~ItemChatInfo()
	{
		ClearItemChatTarget();
	}

public:
	void Init()
	{
		pChatTarget = NULL;
		dwCurStage = 0ul;
	}

	void ClearItemChatTarget();

public:
	///设置交谈目标；
	void SetItemChatTarget( CBaseItem* pItem, int nOption );
	///取交谈目标；
	CBaseItem* GetItemChatTarget() { return pChatTarget; }
	///设置当前交谈阶段；
	void SetItemChatStage( unsigned long chatStage ) { dwCurStage = chatStage; }
	///得到当前交谈阶段；
	unsigned long GetItemChatStage() { return dwCurStage; }

public:
	///选项选择，nOption==0表示初始使用Item；
	bool OnItemChatOption( CPlayer* pOwner, int nOption );

private:
	CBaseItem* pChatTarget;//交谈对象；
	unsigned long dwCurStage;//当前的所处的交谈阶段
};

///AreaTask的相关信息,保存进入任务区域后,任务所进行到的阶段
struct AreaTaskChatInfo
{
public:
	AreaTaskChatInfo()
	{

	}

	~AreaTaskChatInfo()
	{

	}

public:
	void Init()
	{
		dwCurStage = 0ul;
	}

public:
	///设置交谈目标；
	void SetAreaChatTarget(  int nOption );

	//设置区域任务进行到的状态
	void SetAreaTaskStage( unsigned long taskState ) 
	{
		dwCurStage = taskState;
	}

	//获得当前的任务进行状态
	unsigned long GetAreaTaskState() { return dwCurStage; }

public:
	///选项选择
	bool OnAreaTaskOption(  CPlayer* pOwner, int nOption );

private:
	unsigned long dwCurStage;//当前所处的任务阶段
};
