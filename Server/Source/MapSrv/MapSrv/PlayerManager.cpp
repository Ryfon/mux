﻿#include "PlayerManager.h"
#include "Player/Player.h"

#include "WaitQueue.h"
#include "AISys/RuleAIEngine.h"


//静态声明
map<string, CPlayer*> CPlayerManager::m_mapAccountPlayer;

map<string, CPlayer*> CPlayerManager::m_mapRolePlayer;

ACE_Time_Value CPlayerManager::m_lastSlowTimeExecTime;

std::queue<CPlayer*> CPlayerManager::m_deferredOfflinePlayers;

vector<CPlayer*>     CPlayerManager::m_finalDelPlayers;//已做完所有删除逻辑，只等最后释放回池的玩家；

CPlayerManager::CPlayerManager(void)
{
}

CPlayerManager::~CPlayerManager(void)
{
}

///初始操作；
bool CPlayerManager::Init()
{
	TRY_BEGIN;

	m_lastSlowTimeExecTime = ACE_OS::gettimeofday();
	m_finalDelPlayers.clear();

	return true;
	TRY_END;
	return false;
}


///插入玩家
bool CPlayerManager::InsertAccountPlayer( const char* playerAccount, CPlayer* pPlayer, unsigned long playerPID/*用于调试, 05.19*/ )
{
	TRY_BEGIN;

	if ( NULL == playerAccount )
	{
		D_ERROR( "CPlayerManager::InsertAccountPlayer, NULL == playerAccount\n" );
		return false;
	}

	if ( NULL == pPlayer )
	{
		D_ERROR( "CPlayerManager::InsertAccountPlayer, NULL == pPlayer\n" );
		return false;
	}

	if( FindPlayerByAccount( playerAccount ) )
	{
		D_ERROR( "插入名字管理器的时候发现已有该玩家%s\n", playerAccount );
		return false;
	}

	m_mapAccountPlayer.insert( make_pair( playerAccount, pPlayer) );
	D_DEBUG( "将玩家%s加入全局名字管理器，其ID号:%x\n", playerAccount, playerPID );
	D_WARNING_DAY( "1:当前mapsrv管理人数%d\n\n", m_mapAccountPlayer.size() );

	return true;
	TRY_END;
	return false;
}

///添加角色名<-->玩家映射；
bool CPlayerManager::InsertRolePlayer( const char* roleName, CPlayer* pPlayer )
{
	TRY_BEGIN;

	if ( NULL == roleName )
	{
		D_ERROR( "CPlayerManager::InsertRolePlayer, NULL == roleName\n" );
		return false;
	}

	if ( NULL == pPlayer )
	{
		D_ERROR( "CPlayerManager::InsertRolePlayer, NULL == pPlayer\n" );
		return false;
	}

	if( FindPlayerByRoleName( roleName ) )
	{
		D_ERROR( "插入昵称管理器的时候发现已有该玩家%s\n", roleName );
		return false;
	}

	D_DEBUG( "将玩家%s加入昵称名字管理器\n", roleName );
	m_mapRolePlayer.insert( make_pair( roleName, pPlayer) );

	return true;
	TRY_END;
	return false;
}
	
///删除玩家
bool CPlayerManager::DeletePlayer(CPlayer* pPlayer)
{
	TRY_BEGIN;

	if ( NULL == pPlayer )
	{
		return false;
	}

	//CRuleAIEngine::Instance()->OnPlayerDie( pPlayer->GetFullID() );

	//从帐号映射中删去，同时释放指针；
	string accountName = pPlayer->GetAccount();
	if( ! FindPlayerByAccount( accountName ) )
	{
		D_ERROR( "从名字管理器删除时发现玩家%s已不在\n", accountName.c_str() );
		return false;
	}
	map<string, CPlayer*>::iterator iter = m_mapAccountPlayer.find( accountName );
	m_mapAccountPlayer.erase( iter );

	//从昵称映射中删去；
	string roleName = pPlayer->GetNickName();
	if( ! FindPlayerByRoleName( roleName ) )
	{
		D_WARNING( "从昵称管理器删除时发现该玩家%s已不在，可能玩家fullinfo尚未完整时即已下线!\n", roleName.c_str() );
	} else {
		map<string, CPlayer*>::iterator iter = m_mapRolePlayer.find( roleName );
		m_mapRolePlayer.erase( iter );
		D_DEBUG( "把玩家%s从昵称名字管理器删除\n", roleName.c_str() );
	}

	D_DEBUG( "把玩家%s从全局名字管理器删除\n", accountName.c_str() );
	D_WARNING_DAY( "2:当前mapsrv管理人数%d\n\n", m_mapAccountPlayer.size() );

	m_finalDelPlayers.push_back( pPlayer );//压入最后删除队列，准备池回收；

	CWaitQueue::OnSomeOneLeave();


	return true;
	TRY_END;
	return false;
}

///通过名字寻找所有mapsrv上的玩家
CPlayer* CPlayerManager::FindPlayerByAccount(const string& accountName)
{
	TRY_BEGIN;

	map<string, CPlayer*>::iterator iter = m_mapAccountPlayer.find( accountName );
	if( iter != m_mapAccountPlayer.end() )
	{
		return iter->second;
	}

	return NULL;
	TRY_END;
	return NULL;
}

///通过角色名寻找所有mapsrv上的玩家
CPlayer* CPlayerManager::FindPlayerByRoleName(const string& roleName) 
{ 
	TRY_BEGIN;

	map<string, CPlayer*>::iterator iter = m_mapRolePlayer.find( roleName );
	if( iter != m_mapRolePlayer.end() )
	{
		return iter->second;
	}

	return NULL;
	TRY_END;
	return NULL;
};

///慢时钟
void CPlayerManager::PlayerSlowTimer()
{
	TRY_BEGIN;

	//!!!!mapserver.cpp锁定,暂时放在这里,等以后转移到mapserver.cpp
	ProcessDeferredOffline();

	if ( NULL != g_poolManager )
	{
		for ( vector<CPlayer*>::iterator iter=m_finalDelPlayers.begin(); iter!=m_finalDelPlayers.end(); ++iter )
		{
			if ( NULL != *iter )
			{
				g_poolManager->Release( *iter );
			}
		}
	}
	m_finalDelPlayers.clear();

	/*ACE_Time_Value pastTime = ACE_OS::gettimeofday() - m_lastSlowTimeExecTime;
	if ( pastTime.sec() < 2 ) 
	{
		return;
	} */

	//调试
	//D_DEBUG("本mapserver管理的玩家个数%d\n", m_mapName.size());

	//测下时间...

	m_lastSlowTimeExecTime = ACE_OS::gettimeofday();

	for ( map<string, CPlayer*>::iterator iter = m_mapAccountPlayer.begin();
		iter != m_mapAccountPlayer.end(); ++ iter )
	{
		if ( NULL != iter->second )
		{
			iter->second->PlayerTimerProc();
		}
	}
	
	return;
	TRY_END;
	return;
}

void CPlayerManager::RegisterDeferredOffline(CPlayer* pPlayer)
{
	if ( NULL == pPlayer )
	{
		return;
	}

	D_DEBUG( "CPlayerManager::RegisterDeferredOffline,%s加入延迟删除队列\n", pPlayer->GetAccount() );

	m_deferredOfflinePlayers.push(pPlayer);

	return;
}

///释放处理(对于m_deferredOfflinePlayers中还未清理的pPlayer作清理操作);
void CPlayerManager::Release()
{
	TRY_BEGIN;

	ACE_Time_Value curTime = ACE_OS::gettimeofday();

	while(! m_deferredOfflinePlayers.empty())
	{
		CPlayer* pPlayer = m_deferredOfflinePlayers.front();
		if(pPlayer == NULL)
		{	
			m_deferredOfflinePlayers.pop();
			continue;
		}

		D_DEBUG( "CPlayerManager::Release,延迟队列中%s被强制遍历删除\n", pPlayer->GetAccount() );

		pPlayer->ProcessDeferredOffline( true, curTime );

		m_deferredOfflinePlayers.pop();
	}

	if ( NULL != g_poolManager )
	{
		for ( vector<CPlayer*>::iterator iter=m_finalDelPlayers.begin(); iter!=m_finalDelPlayers.end(); ++iter )
		{
			if ( NULL != *iter )
			{
				g_poolManager->Release( *iter );
			}
		}
	}
	m_finalDelPlayers.clear();

	return;
	TRY_END;
	return;
}

void CPlayerManager::ProcessDeferredOffline()
{
	TRY_BEGIN;

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	while(! m_deferredOfflinePlayers.empty())
	{
		CPlayer *&pPlayer = m_deferredOfflinePlayers.front();
		if(pPlayer == NULL)
		{	
			m_deferredOfflinePlayers.pop();
			continue;
		}		

		if(!pPlayer->ProcessDeferredOffline( false, currentTime))
			break;

		m_deferredOfflinePlayers.pop();
	}

	return;
	TRY_END;
	return;
}


unsigned int CPlayerManager::GetPlayerSize()
{
	return (unsigned int) m_mapAccountPlayer.size();
}



