﻿#include "NpcSkill.h"
#include "XmlManager.h"
#include "BuffProp.h"

CNpcSkill::CNpcSkill(void):m_lNpcSkillID(0),m_nTarget(0),m_nType(0),m_fHit(0.0f),m_fAddHit(0.0f),m_fCritical(0.0f)
,m_nAddAtt(0),m_nAttMin(0),m_nAttMax(0),m_nAttDistMin(0),m_nAttDistMax(0),m_nShape(0),m_nRange(0),m_nCurseTime(0)
,m_nCoolDown(0),m_nLinkID(0),m_fLinkRate(0.0f),m_nPeriodTime(0),m_nAssistObject(0),m_pBuffProp(NULL)
{
}


CNpcSkill::~CNpcSkill(void)
{
}


bool CManNpcSkill::Init()
{
	return LoadFromXML("config/npc/npc_skill.xml");
}

bool CManNpcSkill::IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
{
	if ( NULL == pEle || NULL == attName )
	{
		return false;
	}
	if ( NULL == pEle->GetAttr( attName ) )
	{
		D_ERROR( "服务器配置XML文件第%d条记录属性%s失败\n", nRecord, attName );
		return false;
	}
	return true;
}


bool CManNpcSkill::LoadFromXML(const char* szFileName)
{
	TRY_BEGIN;

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(szFileName))
	{
		return false;
	}

	D_INFO("Enter Loading:%s\n", szFileName );
	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;
	CNpcSkill* pNpcSkill = NULL;
	
	int nCurPos = 0;
	unsigned int rootLevel =  pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			nCurPos++;
			pNpcSkill = NEW CNpcSkill;
	   	 	if( ! IsXMLValValid(tmpEle, "nSkillID", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_lNpcSkillID = atoi( tmpEle->GetAttr("nSkillID") );
			if( ! IsXMLValValid(tmpEle, "nTarget", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nTarget = atoi( tmpEle->GetAttr("nTarget"));
			if( ! IsXMLValValid(tmpEle, "nType", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nType = atoi( tmpEle->GetAttr("nType") );
			if( ! IsXMLValValid(tmpEle, "nHit", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_fHit = (float)atof( tmpEle->GetAttr("nHit"));
			if( ! IsXMLValValid(tmpEle, "nAddHit", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_fAddHit = (float)atof( tmpEle->GetAttr("nAddHit"));
			if( ! IsXMLValValid(tmpEle, "nCritical", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_fCritical = (float)atof( tmpEle->GetAttr("nCritical"));
			if( ! IsXMLValValid(tmpEle, "nAddAtt", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nAddAtt = atoi( tmpEle->GetAttr("nAddAtt"));
			if( ! IsXMLValValid(tmpEle, "nAttMin", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nAttMin = atoi( tmpEle->GetAttr("nAttMin"));
			if( ! IsXMLValValid(tmpEle, "nAttMax", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nAttMax = atoi( tmpEle->GetAttr("nAttMax"));
			
			if( ! IsXMLValValid(tmpEle, "nAttDistMin", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nAttDistMin = atoi( tmpEle->GetAttr("nAttDistMin"));
			if( ! IsXMLValValid(tmpEle, "nAttDistMax", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nAttDistMax = atoi( tmpEle->GetAttr("nAttDistMax"));
			if( ! IsXMLValValid(tmpEle, "nShape", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nShape = atoi( tmpEle->GetAttr("nShape"));
			if( ! IsXMLValValid(tmpEle, "nRange", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nRange = atoi( tmpEle->GetAttr("nRange"));
			if( ! IsXMLValValid(tmpEle, "nCurseTime", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nCurseTime = atoi( tmpEle->GetAttr("nCurseTime"));

			if( ! IsXMLValValid(tmpEle, "nCoolDown", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nCoolDown = atoi( tmpEle->GetAttr("nCoolDown"));

			if( ! IsXMLValValid(tmpEle, "nLinkID", nCurPos) )
			{
				return false;
			}
			pNpcSkill->m_nLinkID = atoi( tmpEle->GetAttr("nLinkID"));
			if( pNpcSkill->m_nLinkID != 0)
			{
				CBuffProp* pTempProp = CManBuffProp::FindObj( pNpcSkill->m_nLinkID );
				if( NULL == pTempProp)
				{
					D_ERROR("NpcSkill Link的ID找不到 ID:%d\n", pNpcSkill->m_nLinkID);
				}
				pNpcSkill->m_pBuffProp = pTempProp;

				if( ! IsXMLValValid(tmpEle, "nLinkRate", nCurPos) )
				{
					return false;
				}
				pNpcSkill->m_fLinkRate = (float)atof( tmpEle->GetAttr("nLinkRate"));
		
				if( ! IsXMLValValid(tmpEle, "AssistObject", nCurPos) )
				{
					return false;
				}
				pNpcSkill->m_nAssistObject = atoi( tmpEle->GetAttr("AssistObject"));

				if( ! IsXMLValValid(tmpEle, "nPeriodOfTime", nCurPos) )
				{
					return false;
				}
				pNpcSkill->m_nPeriodTime = atoi( tmpEle->GetAttr("nPeriodOfTime"));
			}
		}
		AddObject(pNpcSkill); //加入配置类
	}

	D_INFO("Leaving Loading:%s\n", szFileName );
	return true;
	TRY_END;
	return false;	
}
