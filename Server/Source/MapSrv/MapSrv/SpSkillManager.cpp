﻿#include "SpSkillManager.h"
#include "XmlManager.h"
#include "XmlEventHandler.h"
#include "../../Base/Utility.h"

map<unsigned short, unsigned int> CSpSkillManager::m_spLevelExp;
unsigned short CSpSkillManager::m_spPowerPerBean = 0;
unsigned int CSpSkillManager::m_proLuck = 0;									//幸运系统，发生幸运暴击的概率
unsigned int CSpSkillManager::m_coeLuck = 0;									//增加经验的比例
unsigned char CSpSkillManager::m_deadWearRate = 0;

CSpSkillManager::CSpSkillManager(void)
{
}

CSpSkillManager::~CSpSkillManager(void)
{
}


void CSpSkillManager::AddLevelSpExp(unsigned short usLevel, unsigned int uiExp)
{
	m_spLevelExp.insert( make_pair(usLevel,uiExp ) );
}


void CSpSkillManager::SetSpPowerPerBean(unsigned short usPowerPerBean)
{
	m_spPowerPerBean = usPowerPerBean;
}


bool CSpSkillManager::LoadFromXML(const char* szFileName)		//读取XML
{
	TRY_BEGIN;

	if ( NULL == szFileName )
	{
		return false;
	}
	CXmlManager xmlLoader;
	if(!xmlLoader.Load(szFileName))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;
		
	//编译警告去除，by dzj, 09.07.07, int nCurPos = 0;
	unsigned int rootLevel =  pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			if( strcmp( tmpEle->Name(), "splevel" ) == 0 )
			{
				unsigned short usLevel = atoi( tmpEle->GetAttr("SpLevel") );
				unsigned int   uiExp = atoi( tmpEle->GetAttr("LevelDegree")  );
				AddLevelSpExp( usLevel, uiExp );
			}

			if( strcmp( tmpEle->Name(), "spperbean") == 0)
			{
				unsigned short usSpPowerPerBean = atoi( tmpEle->GetAttr("SpPowers") );
				SetSpPowerPerBean( usSpPowerPerBean );
			}

			if( strcmp( tmpEle->Name(), "LuckySystem") == 0)
			{
				m_proLuck = (unsigned int)atoi( tmpEle->GetAttr("ProLuck") );
				m_coeLuck = (unsigned int)atoi( tmpEle->GetAttr("CoeLuck") );
			}

			if( strcmp( tmpEle->Name(), "Wear") == 0)
			{
				m_deadWearRate = (BYTE)atoi(tmpEle->GetAttr("DeadWear"));
			}
		}
	}
	return true;
	TRY_END;
	return false;
}
