﻿
#include "BattleUtility.h"
#include "../../Base/Utility.h"

//测算2点之间距离
bool IsInRange( TYPE_POS posX1, TYPE_POS posY1, TYPE_POS posX2, TYPE_POS posY2, unsigned int range )
{
	unsigned int dx = abs(posX1 - posX2);
	unsigned int dy = abs(posY1 - posY2);
	unsigned int max = dx>dy?dx:dy;
	return (range >= max); 
}


//2点是否在距离内，精确值
bool IsInAccurateDistance( TYPE_POS x1,TYPE_POS y1, TYPE_POS x2, TYPE_POS y2,  TYPE_POS distance )
{
	int dx = x1 - x2;
	int dy = y1 - y2;
	int powDistance = distance * distance;
	return (powDistance >= (dx*dx + dy*dy) );
}


//计算是否在攻击范围之内  宽松值
bool IsInVagueAttDistance(TYPE_POS x1, TYPE_POS y1, TYPE_POS x2, TYPE_POS y2, int minRange, int maxRange, bool bPlayerUse )	   ///贺轶男检查;
{
	//测算距离
	unsigned int dx = abs(x1-x2);
	unsigned int dy = abs(y1-y2);
	unsigned int uiDistance = dx>dy?dx:dy;
	if( uiDistance < (unsigned int)minRange )
	{
		if( bPlayerUse )
		{
			D_WARNING("人怪距离小于最小攻击距离 人和怪的距离%d  最小攻击距离%d  人位置(%d,%d) 怪位置( %d,%d)\n", uiDistance, minRange, x1,y1,x2,y2 );
		}
		return false;
	}

	if( uiDistance > (unsigned int)(maxRange + ATTACK_ADJUST) )
	{
		if( bPlayerUse )
		{
			//D_WARNING("人怪距离大于最大攻击距离和修正值之和 人和怪的距离%d 最大攻击距离修正%d 人位置(%d,%d) 怪位置( %d,%d) \n", uiDistance, maxRange, x1,y1,x2,y2 );
		}
		return false;
	}
	return true;
}


//double GetPropertyK( unsigned short job, unsigned int index )
//{
//	if( job >= 6 || index >= 6 )
//	{
//		return 0;
//	}
//	return propertyKArray[job][index];
//}

