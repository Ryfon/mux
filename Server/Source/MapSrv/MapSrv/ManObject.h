﻿/**
* @file ManObject.h
* @brief 对象管理模板类
* Copyright(c) 2008,上海第九城市游戏研发部
* All rights reserved
* 文件名称: ManObject.h
* 摘    要: 管理配置类
* 作    者: hyn
* 完成日期: 2008.3.4
*
*/

#pragma once
#include <map>
#include <vector>
#include "Utility.h"
using namespace std;


//对象的管理摸板类
//OBJECT必须包含方法 GetObjID()来取出标识符
template <class T>
class CObjectManager
{
public:
	CObjectManager(void)
	{}

	~CObjectManager(void)
	{}

public:
	static bool ReleaseManedObj()
	{
		TRY_BEGIN;

		for (typename map<unsigned long, T*>::iterator tmpiter = m_mapObjs.begin();
			tmpiter != m_mapObjs.end(); ++tmpiter )
		{
			if ( NULL != tmpiter->second )
			{
				delete tmpiter->second; tmpiter->second = NULL;
			}
		}
		m_mapObjs.clear();
		return true;

		TRY_END;
		return false;
	};

	///寻找对象，如果找不到则返回NULL;
	static T* FindObj( unsigned long lObjectID )
	{
		TRY_BEGIN;

		typename map<unsigned long, T*>::iterator tmpiter = m_mapObjs.find( lObjectID );
		if ( tmpiter != m_mapObjs.end() )
		{
			return tmpiter->second;
		} else {
			return NULL;
		}

		TRY_END;
		return NULL;
   	 }

public:
	///添加一个对象到管理器；
	static bool AddObject( T* pObject )
	{
		TRY_BEGIN;

		if( !pObject )
			return false;

		unsigned long lObjectID = pObject->GetObjID();
		typename map<unsigned long, T*>::iterator tmpiter = m_mapObjs.find( lObjectID );
		if ( tmpiter != m_mapObjs.end() )
		{
			D_WARNING( "类别ID号%d重复\n", lObjectID );
			delete pObject;  //插不进去delete
			return false;
		}

		m_mapObjs.insert( pair<unsigned long, T*>( lObjectID, pObject ) );
		return true;

		TRY_END;
		return false;
	}

	static void GetAllObjectID( vector<unsigned long>& vecID)
	{
		vecID.clear();
		for( typename map<unsigned long,T*>::iterator iter = m_mapObjs.begin(); iter != m_mapObjs.end(); ++iter)
		{
			vecID.push_back( iter->first );
		}
	}

protected:
	static map<unsigned long, T*>& GetMapObject()
	{
		return m_mapObjs;
	}

private:
	///本管理器管理的所有怪物技能属性；
	static map<unsigned long,T*> m_mapObjs;//本管理器管理的所有怪物技能属性；
};

template<class T>
map<unsigned long,T*> CObjectManager<T>::m_mapObjs;	//声明

