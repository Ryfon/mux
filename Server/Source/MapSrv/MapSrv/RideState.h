﻿/** @file RideState.h
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-6-24
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef RIDESTATE_H
#define RIDESTATE_H

#include <map>
#include "MuxMap/muxmapbase.h"

class CBaseItem;

//骑乘队伍
class RideTeam
{
public:
	RideTeam( CPlayer* pLeader,CBaseItem* pRideItem,float speed )
		: m_pRideItem(pRideItem), m_pLeader(pLeader), m_pMember(NULL), m_speed(speed), mItemTypeID(0)
	{
		if ( NULL != pLeader )
		{
			SetRideTeamID( pLeader->GetDBID() );
		}
	}

	RideTeam( CPlayer* pLeader,unsigned long itemTypeID,float speed )
		: m_pRideItem(NULL), m_pLeader(pLeader), m_pMember(NULL), m_speed(speed), mItemTypeID(itemTypeID)
	{
		if ( NULL != pLeader )
		{
			SetRideTeamID( pLeader->GetDBID() );
		}
	}

public:

	////设置队伍编号
	void SetRideTeamID( unsigned long teamID ) { m_rideTeamID = teamID; } 

	//获取队伍编号
	unsigned long GetRideTeamID() { return m_rideTeamID; }

	//判断是否为队长
	bool IsTeamLeader( CPlayer* pPlayer );

	//骑乘队伍是否人员满了
	bool IsTeamFull() { return  m_pMember!= NULL;  }

	//进入队员时
	void EnterRideTeam( CPlayer* pPlayer );

	//队员离开时
	void ExitRideTeam( CPlayer* pPlayer );

	//解散骑乘队伍
	void DissolveRideTeam();

	//当队伍移动时
	void OnRideTeamMove();

	//通知队伍中的人
	//@isSend 是否通知骑乘队伍中的队员
	template<class T>
	void NoticeMsgToAll( T* msg ,bool isSend  )
	{
		if ( m_pLeader )
		{
			CMuxMap* pPlayerMap = m_pLeader->GetCurMap();
			if ( NULL != pPlayerMap )
			{
				pPlayerMap->NotifySurrounding<T>( msg, m_pLeader->GetPosX(), m_pLeader->GetPosY() );
			} 
		}

		if ( m_pMember && isSend )
			m_pMember->SendPkg<T>(msg);
	}

	//通知周围的人，骑乘队伍出现了
	void NoticeRideTeamAppear();

	//当骑乘队伍移动进入新的格子时
	void OnRideTeamMoveToNewBlock();

	//将骑乘信息通知给其他玩家
	void NoticeRideTeamInfoToOtherPlayer( CPlayer* pPlayer );

	////获取玩家所在的节点
	//MuxMapBlock* GetPlayerOwnerMapNode( CPlayer* pPlayer );

	//装备坐骑中,增加玩家的速度时
	void OnAddPlayerSpeed( float fspeed );

	//骑乘结束，减少玩家速度
	void OnSubPlayerSpeed( float fspeed );

	//获取抗击晕等级
	int  GetStunRank();

	////修改队伍成员的其他人所在的节点
	//void ChangeTeamMemberPos();

	//获取骑乘的道具指针
	CBaseItem* GetRideItem() { return m_pRideItem; }

	//获得骑乘的队员
	CPlayer* GetRideMember() { return m_pMember; }

	//获得骑乘的队长
	CPlayer* GetRideLeader() { return m_pLeader; }

	//获取骑乘的队伍名单
	void GetRideTeamMember( PlayerID& leaderID, PlayerID& memberID );

private:
	//获取队伍的位置
	void GetRideTeamPos( int& x, int& y);

	//获取骑乘道具的信息
	void GetRideItemInfo( ItemInfo_i& rideItemInfo );

	//当改变玩家的骑乘速度时
	void OnChangePlayerSpeed( /*float speed*/ );

	void GetMountItemType( char& mountItemType );

private:
	RideTeam& operator=( const RideTeam& );
	RideTeam( const RideTeam& );

private:
	unsigned long m_rideTeamID;//骑乘队伍在服务器的编号
	CBaseItem* m_pRideItem;//骑乘的坐骑
	CPlayer* m_pLeader;//队长
	CPlayer* m_pMember;//队员
	float  m_speed;//速度
	unsigned long mItemTypeID;//使用的坐骑的XML类型,只针对骑乘道具的情况
};



class RideTeamManager
{
	friend class ACE_Singleton<RideTeamManager, ACE_Null_Mutex>; 

public:
	//队伍默认编号从1开始
	RideTeamManager(){}

	~RideTeamManager(){}

private:
	///after RideTeam created
	bool AfterRideTeamCreated( RideTeam* pTeam, CPlayer* pLeader, float fSpeed );

public:
	//在服务器端创建一个新的骑乘队伍
	RideTeam* NewRideTeam( CPlayer* pLeader, CBaseItem* pRideItem,float speed );

	RideTeam* NewRideTeam( CPlayer* pLeader, unsigned long itemTypeID, float speed );

	//依据队伍编号，获取队伍
	RideTeam* GetRideTeamByID( unsigned long teamID );

	//回收一个骑乘队伍内存,既队伍解散
	void RecoverRideTeam( RideTeam* pRideTeam );

	//当玩家退出服务器时
	void OnPlayerLeaveSrv( CPlayer* pPlayer );

	//当玩家切换地图时
	void OnPlayerSwitchMap( CPlayer* pPlayer , unsigned int newMapID, unsigned int targetPosX, unsigned int targetPosY );

	void OnPlayerSwitchMap( CPlayer* pPlayer );

private:
	////获取新的队伍编号
	//long NewRideTeamIndex(){ return m_TeamIndex; }

	//void IncRideTeamIndex() { m_TeamIndex++; }

private:
	std::vector<RideTeam *> m_RideTeamVec;
	//std::map<long, RideTeam* > m_RideTeamMap;
};

typedef ACE_Singleton<RideTeamManager, ACE_Null_Mutex> RideTeamManagerSingle;

#endif

