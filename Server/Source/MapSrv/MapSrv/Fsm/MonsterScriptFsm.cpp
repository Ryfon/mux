﻿/**
* @file MonsterScriptFsm.cpp
* @brief 实现脚本描述的怪物有限状态机
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MonsterScriptFsm.cpp
* 摘    要: 实现脚本描述的怪物有限状态机
* 作    者: dzj
* 完成日期: 2007.03.19
*
*/

#include "MonsterScriptFsm.h"
#include "../monster/monster.h"
#include "../Lua/ScriptExtent.h"

map<lua_State*, CMonsterScriptFsm*> CMonsterScriptFsm::m_allFsms;

CMonsterScriptFsm::CMonsterScriptFsm() 
{
	for ( int i=0; i<AI_STAT_NUM; ++i )
	{
		if ( i >= ARRAY_SIZE(m_arrStateEvent) )
		{
			D_ERROR( "CMonsterScriptFsm::CMonsterScriptFsm, i(%d)越界\n", i );
			return;
		}
		for ( int j=0; j<AI_EVENT_NUM; ++j )
		{
			if ( j >= ARRAY_SIZE(m_arrStateEvent[i]) )
			{
				D_ERROR( "CMonsterScriptFsm::CMonsterScriptFsm, j(%d)越界\n", j );
				return;
			}
			m_arrStateEvent[i][j] = false;
		}
	}
};

///根据脚本初始化；
bool CMonsterScriptFsm::Init( const char* luaScriptFile ) 
{ 
	if ( ! LoadFrom<CAIScriptExtent>( luaScriptFile ) )
	{
		return false;
	}

	m_allFsms.insert( pair<lua_State*, CMonsterScriptFsm*>( this->GetState(), this ) );//在此map中保存所有的脚本FSM，主要供扩展函数回调时查询；

    CLuaRestoreStack rs( m_pLuaState );

	//找到stage表;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//以下初始化state--event有效性数组；
	//对于每一个状态，检查该状态的各个事件函数是否已定义；
	for ( int st=0; st<AI_STAT_NUM; ++st )
	{
		//取states[nStage],即-2[nStage];
		lua_pushnumber( m_pLuaState, st );
		lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
		if ( ! lua_istable( m_pLuaState, -1 ) )
		{
			//非表，脚本没有定义对应该状态的任何事件处理函数;
			lua_settop( m_pLuaState, -2 );//恢复栈，细查， int jjj；
			continue;
		}

		for ( int ev=0; ev<AI_EVENT_NUM; ++ev )
		{
			//当前-1为states[nStage]，找到其对应名字函数并执行之；
			lua_pushstring( m_pLuaState, AI_Event_FuncName_Arr[ev] );
			lua_rawget( m_pLuaState, -2 );//取states[nStage]["funName"]
			if ( lua_isfunction( m_pLuaState, -1 ) )
			{
				if ( ( st >= ARRAY_SIZE(m_arrStateEvent) )
					|| ( ev >= ARRAY_SIZE(m_arrStateEvent[st]) )
					)
				{
					D_ERROR( "CMonsterScriptFsm::Init，st(%d)或ev(%d)越界\n", st, ev );
					return false;
				}
				m_arrStateEvent[st][ev] = true;//定义了对应函数；
			}
			lua_settop( m_pLuaState, -2 );//恢复栈，细查，int jjj；
		}
		lua_settop( m_pLuaState, -2 );
	}

	return true;
};

///检查是否满足转换条件
bool CMonsterScriptFsm::CheckTransCon( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
	//return CallScriptCheckTransCon( pMonster );
}

///发现玩家；
bool CMonsterScriptFsm::OnPlayerDetected( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
	//return CallScriptOnPlayerDetected( pMonster );
}

///玩家离开视野；
bool CMonsterScriptFsm::OnPlayerLeaveView( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
	//return CallScriptOnPlayerLeaveView( pMonster );
}

///攻击目标离开视野；
bool CMonsterScriptFsm::OnAttackTargetLeave( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
	//return CallScriptOnAttackTargetLeave( pMonster );
}

///自身离开出生点太远；
bool CMonsterScriptFsm::OnTooFarFromBornPt( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
	//return CallScriptOnTooFarFromBornPt( pMonster );
}

///自身HP太小；
bool CMonsterScriptFsm::OnSelfHpTooLow( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
	//return CallScriptOnSelfHpTooLow( pMonster );
}

///自身被打；
bool CMonsterScriptFsm::OnBeAttacked( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
	//return CallScriptOnBeAttacked( pMonster );
}

//被放debuff
bool CMonsterScriptFsm::OnDebuff( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
}

///发现NPC;
bool CMonsterScriptFsm::OnNpcDetected( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
	//return CallScriptOnNpcDetected( pMonster );
}

///NPC离开;
bool CMonsterScriptFsm::OnNpcLeave( unsigned long monsterID )
{
	ACE_UNUSED_ARG(monsterID); 
	return true;
	//return CallScriptOnNpcLeave( pMonster );
}


///怪物自身出生（注：本事件发生在怪物从地图上出现之前，也就是说，事件发生时地图上还没有本怪物）
bool CMonsterScriptFsm::OnSelfBorn( unsigned long monsterID, const AIMapInfo& mapInfo )
{
	//脚本实现目前不处理该事件；
	ACE_UNUSED_ARG( monsterID ); 
	ACE_UNUSED_ARG( mapInfo ); 
	return true;
}

///怪物自身死亡（注：本事件发生在怪物从地图上删去之后，也就是说，事件发生时地图上已经没有本怪物）
bool CMonsterScriptFsm::OnSelfDie( unsigned long monsterID )
{
	//脚本实现目前不处理该事件；
	ACE_UNUSED_ARG( monsterID ); 
	return true;
}

bool CMonsterScriptFsm::OnPlayerDropTask(unsigned long monsterID )
{
	//脚本实现目前不处理该事件；
	ACE_UNUSED_ARG( monsterID ); 
	return true;
}

///目前都不带参数，以后如果需要传参数给脚本，则再添加一个vector<int>与vector<const char*>;
bool CMonsterScriptFsm::CallScriptByFunName( CMonster* pMonster, const char* funName )
{
	TRY_BEGIN;

	if ( NULL == pMonster )
	{
		D_ERROR("CMonsterScriptFsm::CallScriptByFunName, NULL == pMonster\n");
		return false;
	}

	if ( NULL == funName )
	{
		D_ERROR("CMonsterScriptFsm::CallScriptByFunName, NULL == funName\n");
		return false;
	}

	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	MONSTER_STAT curState = pMonster->GetState();
	if ( ! IsStageValid( curState) )
	{
		return false;
	}

	//找到相应的stage;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, curState );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到其对应名字函数并执行之；
	lua_pushstring( m_pLuaState, funName );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["funName"]
	if ( ! lua_isfunction( m_pLuaState, -1 ) )
	{
		//非函数，未定义该函数；
		return false;
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnOption;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;

}

///调用脚本进入处理函数，私有，由本解释框架调用
bool CMonsterScriptFsm::CallScriptOnEnter( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnEnter，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_ENTER ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_ENTER] );
}

///调用脚本退出处理函数，私有，由本解释框架调用
bool CMonsterScriptFsm::CallScriptOnExit( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnExit，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_EXIT ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_EXIT] );
}

///调用脚本发现玩家处理函数
bool CMonsterScriptFsm::CallScriptOnPlayerDetected( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnPlayerDetected，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_PLAYER_DETECTED ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_PLAYER_DETECTED] );
}

///调用脚本玩家离开处理函数
bool CMonsterScriptFsm::CallScriptOnPlayerLeaveView( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnPlayerLeaveView，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_PLAYER_LEAVEVIEW ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_PLAYER_LEAVEVIEW] );
}

///调用脚本攻击目标离开处理函数
bool CMonsterScriptFsm::CallScriptOnAttackTargetLeave( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnAttackTargetLeave，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_ATTACKTARGET_LEAVE ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_ATTACKTARGET_LEAVE] );
}

///调用脚本离开出生点太远处理函数
bool CMonsterScriptFsm::CallScriptOnTooFarFromBornPt( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnTooFarFromBornPt，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_TOOFARFROM_BPT ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_TOOFARFROM_BPT] );
}

///自身HP太小；
bool CMonsterScriptFsm::CallScriptOnSelfHpTooLow( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnSelfHpTooLow，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_SELFHP_TOOLOW ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_SELFHP_TOOLOW] );
}

///调用脚本被打处理函数
bool CMonsterScriptFsm::CallScriptOnBeAttacked( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnBeAttacked，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_BEATTACKED ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_BEATTACKED] );
}

///调用脚本发现NPC函数;
bool CMonsterScriptFsm::CallScriptOnNpcDetected( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnNpcDetected，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_NPC_DETECTED ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_NPC_DETECTED] );
}

///调用脚本NPC离开函数;
bool CMonsterScriptFsm::CallScriptOnNpcLeave( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnNpcLeave，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_NPC_LEAVE ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_NPC_LEAVE] );
}

///调用脚本//MEVENT_FOLLOWPLAYER_BEATTACK, 玩家被攻击
bool CMonsterScriptFsm::CallScriptOnFollowPlayerBeAttack( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnFollowPlayerBeAttack，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_FOLLOWPLAYER_BEATTACK ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_FOLLOWPLAYER_BEATTACK] );
}

///调用脚本//MEVENT_PLAYER_ADDHP, 玩家加HP
bool CMonsterScriptFsm::CallScriptOnPlayerAddHP( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnPlayerAddHP，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_PLAYER_ADDHP ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_PLAYER_ADDHP] );
}

///调用脚本//MEVENT_PLAYER_DIE, 玩家死亡
bool CMonsterScriptFsm::CallScriptOnPlayerDie( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptOnPlayerDie，输入参数空\n" );
		return false;
	}
	if ( !IsResEvent( pMonster->GetState(), MEVENT_PLAYER_DIE ) )
	{
		//未定义对该事件的响应函数，直接返回；
		return true;
	}
	return CallScriptByFunName( pMonster, AI_Event_FuncName_Arr[MEVENT_PLAYER_DIE] );
}

///调用脚本状态转换检测处理函数
bool CMonsterScriptFsm::CallScriptCheckTransCon( CMonster* pMonster )
{
	TRY_BEGIN;
	
	if ( NULL == pMonster )
	{
		D_ERROR( "CMonsterScriptFsm::CallScriptCheckTransCon，输入参数空\n" );
		return false;
	}

	//依次找到各advance函数，只要某一函数返回真，则执行stage转换;
	CLuaRestoreStack rs( m_pLuaState );

	//当前stage;
	int nState = pMonster->GetState();
	if ( nState<0 )
	{
		return false;
	}

	//找到相应的states;
	lua_getglobal( m_pLuaState, "states" );
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误;
		return false;
	}

	//取states[nStage],即-2[nStage];
	lua_pushnumber( m_pLuaState, nState );
	lua_rawget( m_pLuaState, -2 );//取states[nStage],即-2[nStage];
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，取到的内容错误；
		return false;
	}

	//当前-1为states[nStage]，找到links表；
	lua_pushstring( m_pLuaState, "links" );
	lua_rawget( m_pLuaState, -2 );//取states[nStage]["links"]
	if ( ! lua_istable( m_pLuaState, -1 ) )
	{
		//非表，未定义链接
		return false;
	}

	//依次遍历links表，执行检测条件，如果检测条件满足则执行转换;
	for ( int i=0; i<AI_STAT_NUM; ++i ) //目前认为单节点链接数不会超过此限额;
	{
		CLuaRestoreStack rs( m_pLuaState );

		//取links表第i个元素；
		lua_pushnumber( m_pLuaState, i );
		lua_rawget( m_pLuaState, -2 );
		if ( !lua_istable( m_pLuaState, -1 ) )
		{
			//已经到了表内最后一个元素，说明所有条件都不满足，无需执行转换；
			break;
		}

		if (true)
		{
			CLuaRestoreStack tmprs( m_pLuaState );
			//当前已取出links表的第i个元素，检查其advance函数；
			lua_pushstring( m_pLuaState, "advance" );
			lua_rawget( m_pLuaState, -2 );
			if( !lua_isfunction( m_pLuaState, -1 ) )
			{
				//取不到该表项的advance函数；
				D_ERROR( "AI脚本错误，advance函数未定义\n" );
				break;
			}

			//调用检测函数；
			if ( ( 0 != lua_pcall( m_pLuaState, 0, 1, 0 ) ) //检测函数调用失败;
				|| ( ! lua_isboolean( m_pLuaState, -1 ) )   //或者返回值非bool值；
				)
			{
				break;
			}

			//取调用结果；
			int isTrans = (int) lua_toboolean( m_pLuaState, -1 );
			if ( isTrans )//转换条件满足；
			{
				//执行转换后退出，因为同一个stage只可能执行多个链接中的一个；
				//准备取目标状态；				
				lua_pop( m_pLuaState, 1 );//栈顶的两个元素，-1:检测函数返回值；
				//取目标stage；
				lua_pushstring( m_pLuaState, "target" );
				lua_rawget( m_pLuaState, -2 );
				if ( lua_isnil( m_pLuaState, -1 ) )
				{
					D_ERROR( "AI脚本错误，advance to 空状态\n" );
				} else if ( lua_isnumber( m_pLuaState, -1 ) ) {
					//取到了目标stage;
					MONSTER_STAT targetStage = ( MONSTER_STAT) ( (int) lua_tonumber( m_pLuaState, -1 ) );
					if ( !IsStageValid( targetStage ) )
					{
						D_ERROR( "AI脚本错误，返回的stage号无效" );
						break;
					}
					CallScriptOnExit( pMonster );//退出旧状态时回调相应阶段的OnExit函数；
					pMonster->SetState( targetStage );
					CallScriptOnEnter( pMonster );//进入新状态回调脚本相应阶段的OnEnter函数；					
				} else {
					D_ERROR( "AI脚本错误，返回的stage号类型错误" );
				}
				break;
			}
			//转换条件不满足，继续检测下一条件；
		}		
	}

	bool iscallok = ( 0 == lua_pcall( m_pLuaState, 0, 0, 0 ) );//调用OnEnter;
	if ( !iscallok )
	{
		const char* szErrMessage = lua_tostring( m_pLuaState, -1 );
		if ( NULL != szErrMessage )
		{
			D_ERROR( "LUA执行错：%s\n", szErrMessage );
		} else {
			D_ERROR( "LUA执行错，未知错误\n" );
		}
	}

	return iscallok;
	TRY_END;
	return false;
}

