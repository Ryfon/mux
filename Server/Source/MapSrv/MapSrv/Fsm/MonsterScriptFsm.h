﻿/**
* @file MonsterScriptFsm.h
* @brief 定义脚本描述的怪物有限状态机
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MonsterScriptFsm.h
* 摘    要: 定义脚本描述的怪物有限状态机
* 作    者: dzj
* 完成日期: 2007.03.19
*
*/

#pragma once

#include "IFsm.h"
#include "../Lua/LuaScript.h"
#include "Utility.h"

class CMonsterScriptFsm : public IMonsterFsm, public CLuaScriptBase
{
	friend class CManMonsterFsm;
private:
	CMonsterScriptFsm();

	virtual ~CMonsterScriptFsm() {};

public:
	///根据脚本初始化；
	bool Init( const char* luaScriptFile );

public:
	///////////////////////////////////////////////////////////////////////
	//必须实现的FSM接口函数...
	/////告诉IMonsterFsm,有一个新的本实例使用者pMonster，由pMonster在第一次使用IFsm之前调用:pMonster->m_pIFsm = CManMonsterFsm::FindFsm(...); pMonster->m_pIFsm->RegisterUser(pMonster);
	/////脚本实现不理会本接口，直接返回真；
	//virtual bool RegisterUser( CMonster* pMonster ) { ACE_UNUSED_ARG( pMonster ); return true; };
	/////告诉IMonsterFsm,pMonster已不再使用本实例，由pMonster在回收之前调用:pMonster->m_pIFsm->UnRegisterUser(pMonster); pMonster->m_pIFsm = NULL;
	/////脚本实现不理会本接口，直接返回真；
	//virtual bool UnRegisterUser(CMonster* pMonster ) { ACE_UNUSED_ARG( pMonster ); return true; };

	///取该AI状态机的AI编号；
	virtual int GetAIID() { return m_nAIID; };

	///状态机是否响应特定状态下的特定事件；
	virtual bool IsResEvent( const MONSTER_STAT& curStat, const MONSTER_EVENT& curEvent ) { return m_arrStateEvent[curStat][curEvent]; };

	///检查是否满足转换条件
	virtual bool CheckTransCon( unsigned long monsterID );

	///进入状态，目前不会被脚本框架调用；
	virtual bool OnEnter( unsigned long monsterID ) { ACE_UNUSED_ARG( monsterID ); return false; };
	///退出状态，目前不会被脚本框架调用；
	virtual bool OnExit( unsigned long monsterID ) { ACE_UNUSED_ARG( monsterID ); return false; };

	///定时执行的更新函数(由程序框架定时调用，调用间隔目前初定约100毫秒)，目前脚本中不设此函数，但C++接口可能需要；
	virtual bool OnUpdate( unsigned long monsterID ) { ACE_UNUSED_ARG( monsterID ); return false; };

	///发现玩家；
	virtual bool OnPlayerDetected( unsigned long monsterID );
	///玩家离开视野；
	virtual bool OnPlayerLeaveView( unsigned long monsterID );
	///攻击目标离开视野；
	virtual bool OnAttackTargetLeave( unsigned long monsterID );
	///自身离开出生点太远；
	virtual bool OnTooFarFromBornPt( unsigned long monsterID );
	///自身HP太小；
	virtual bool OnSelfHpTooLow( unsigned long monsterID );
	///自身被打；
	virtual bool OnBeAttacked( unsigned long monsterID );
	//自身被放debuff
	virtual bool OnDebuff( unsigned long monsterID );
    ///发现NPC;
	virtual bool OnNpcDetected( unsigned long monsterID );
    ///NPC离开;
	virtual bool OnNpcLeave( unsigned long monsterID );
	///玩家丢弃任务
	virtual bool OnPlayerDropTask( unsigned long monsterID );


	virtual bool OnFollowPlayerBeAttack( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

	///调用脚本//MEVENT_PLAYER_ADDHP, 玩家加HP
	virtual bool OnPlayerAddHP( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

	///调用脚本//MEVENT_PLAYER_DIE, 玩家死亡
	virtual bool OnPlayerDie( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

    ///怪物自身出生（注：本事件发生在怪物从地图上出现之前，也就是说，事件发生时地图上还没有本怪物,08.07.31修改，时机改为加入地图之后）
	virtual bool OnSelfBorn( unsigned long monsterID, const AIMapInfo& mapInfo );
    ///怪物自身死亡（注：本事件发生在怪物从地图上删去之后，也就是说，事件发生时地图上已经没有本怪物,08.07.31修改，时机改为加入地图之后）
	virtual bool OnSelfDie( unsigned long monsterID );
	//...必须实现的FSM接口函数;
	virtual bool OnFaintStart( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; }  //8.21 添加新接口
	virtual bool OnFaintEnd( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; }  //8.21 添加新接口

	virtual bool OnBondageStart( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; }  //8.21 添加新接口
	virtual bool OnBondageEnd( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; }  //8.21 添加新接口

	///怪物开始跟随；
	virtual bool OnStartFollowing( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

	///通知跟随怪物，其所跟随的玩家开始移动；
	virtual bool OnFollowPlayerStartMoving( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

	virtual bool OnDebugThreatListStart( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; }  //8.21 添加新接口
	virtual bool OnDebugThreatListEnd( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; }  //8.21 添加新接口

	///怪物进入视野，收到此事件后，通过GetAIEventInfo取得具体的进入视野怪物信息；
	virtual bool OnMonsterEnterView( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };
	///怪物离开视野，收到此事件后，通过GetAIEventInfo取得具体的离开视野怪物信息；
	virtual bool OnMonsterLeaveView( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

	///有人与怪物对话；
	virtual bool OnMonsterBeChat( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

	///周围玩家移动状态改变(原因：新移动或进入怪物所在块)；
	virtual bool OnSurPlayerMovChg( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

	///所召唤的怪物出生，收到此事件后，通过GetAIEventInfo取得具体的出生怪物信息；
	virtual bool OnCalledMonsterBorn( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

	virtual bool OnMonsterBuffStart( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; };

	virtual bool OnMonsterBuffEnd( unsigned long monsterID )  { ACE_UNUSED_ARG(monsterID); return false; };
	virtual bool OnAttackCityStart( unsigned long monsterID ) { ACE_UNUSED_ARG( monsterID );  return false; }
	virtual bool OnAttackCityEnd( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID );  return false;  }
	virtual bool OnFollowingPlayerBeAttack( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false;  }
	virtual bool OnPlayerAddHp( unsigned long monsterID ) { ACE_UNUSED_ARG(monsterID); return false; }

		//怪物被击退
	virtual bool OnBeKickBack( unsigned long monsterID )  { ACE_UNUSED_ARG(monsterID); return false;};

	///////////////////////////////////////////////////////////////////////

private:
	///调用脚本进入处理函数，私有，由本解释框架调用
	bool CallScriptOnEnter( CMonster* pMonster );
	///调用脚本退出处理函数，私有，由本解释框架调用
	bool CallScriptOnExit( CMonster* pMonster );
	///调用脚本状态转换检测处理函数
	bool CallScriptCheckTransCon( CMonster* pMonster );
	///调用脚本发现玩家处理函数
	bool CallScriptOnPlayerDetected( CMonster* pMonster );
	///调用脚本玩家离开处理函数
	bool CallScriptOnPlayerLeaveView( CMonster* pMonster );
	///调用脚本攻击目标离开处理函数
	bool CallScriptOnAttackTargetLeave( CMonster* pMonster );
	///调用脚本自身Hp过小时的处理函数
	bool CallScriptOnSelfHpTooLow( CMonster* pMonster );
	///调用脚本离开出生点太远处理函数
	bool CallScriptOnTooFarFromBornPt( CMonster* pMonster );
	///调用脚本被打处理函数
	bool CallScriptOnBeAttacked( CMonster* pMonster );
	//调用脚本被放debuff
	//bool CallScriptOnDebuff( CMonster* pMonster );
    ///调用脚本发现NPC函数;
	bool CallScriptOnNpcDetected( CMonster* pMonster );
    ///调用脚本NPC离开函数;
	bool CallScriptOnNpcLeave( CMonster* pMonster );

	///调用脚本//MEVENT_FOLLOWPLAYER_BEATTACK, 玩家被攻击
	bool CallScriptOnFollowPlayerBeAttack( CMonster* pMonster );

	///调用脚本//MEVENT_PLAYER_ADDHP, 玩家加HP
	bool CallScriptOnPlayerAddHP( CMonster* pMonster );

	///调用脚本//MEVENT_PLAYER_DIE, 玩家死亡
	bool CallScriptOnPlayerDie( CMonster* pMonster );

	///根据调用者的当前状态调用脚本中指定名字函数；
    bool CallScriptByFunName( CMonster* pMonster, const char* funName );

private:
	bool IsStageValid( const MONSTER_STAT& inStage ) 
	{
		return ( ( inStage>0 ) && ( inStage < AI_STAT_NUM ) );
	}

private:
	bool m_arrStateEvent[AI_STAT_NUM][AI_EVENT_NUM];
	int    m_nAIID;//本AI的编号；

public:
	static CMonsterScriptFsm* FindScripts( lua_State* inState )
	{
		map<lua_State*, CMonsterScriptFsm*>::iterator iter = m_allFsms.find( inState );
		if ( iter != m_allFsms.end() )
		{
			return iter->second;
		} else {
			return NULL;
		}
	}

private:
	static map<lua_State*, CMonsterScriptFsm*> m_allFsms;
};
