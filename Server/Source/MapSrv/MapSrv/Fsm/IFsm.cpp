﻿/**
* @file IFsm.cpp
* @brief 定义怪物有限状态机C++接口
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: IFsm.cpp
* 摘    要: 定义怪物有限状态机C++接口
* 作    者: dzj
* 完成日期: 2007.03.19
*
*/

#include "IFsm.h"
#include "MonsterScriptFsm.h"
#include "../AISys/MonsterSkillSet.h"
#include "../AISys/RuleAIEngine.h"

map<int, IMonsterFsm*> CManMonsterFsm::m_mapMonsterFsms;

const char* AI_Event_FuncName_Arr[] =  //对应各事件，脚本中的函数名，其排列顺序应与MONSTER_EVENT中的元素定义一一对应;
{
	/*
enum MONSTER_EVENT //怪物可能响应的事件
{
	  MEVENT_UPDATE=0 //定时执行的更新函数，目前脚本中不设此函数，但C++接口可能需要；
	, MEVENT_ENTER=1  //进入状态；
	, MEVENT_EXIT=2   //退出状态；
	, MEVENT_PLAYER_DETECTED=3    //发现玩家（玩家进入视野）；
	, MEVENT_PLAYER_LEAVEVIEW=4       //玩家离开视野；
	, MEVENT_ATTACKTARGET_LEAVE=5 //攻击目标离开视野；
	, MEVENT_TOOFARFROM_BPT=6     //离开出生点过远；
	, MEVENT_SELFHP_TOOLOW=7      //自身HP小于一定百分比；
	, MEVENT_BEATTACKED=8         //自身被打；
	, MEVENT_NPC_DETECTED=9      //发现NPC
	, MEVENT_NPC_LEAVE=10         //NPC离开
	, MEVENT_SELF_BORN=11         //怪物自身出生（注：本事件发生在怪物从地图上出现之前，也就是说，事件发生时地图上还没有本怪物）
	, MEVENT_SELF_DIE=12          //怪物自身死亡（注：本事件发生在怪物从地图上删去之后，也就是说，事件发生时地图上已经没有本怪物）
	, MEVENT_ON_DEBUFF =13,        //怪物被放DEBUFF
	, MEVENT_ENDOF_RETURN = 14,    //怪物完成回归过程
	, MEVENT_ENDOF_FLEE = 15,      //怪物完成逃跑
	, MEVENT_MONSTER_ENTERVIEW = 16,//怪物进入视野
	, MEVENT_MONSTER_LEAVEVIEW = 17, //怪物离开视野
	, MEVENT_MONSTER_BECHAT = 18,     //有人与本怪物对话；
	, MEVENT_SURPLAYER_MOVCHG  = 19,  //周围玩家移动状态改变；
	, MEVENT_CALLEDMONSTER_BORN = 20,  //所召唤的怪物出生；
	, MEVENT_ATTACK_CITY_START = 21,   //攻城开始
	, MEVENT_ATTACK_CITY_END = 22		//攻城结束
	, MEVENT_FOLLOWPLAYER_BEATTACK = 23,	//跟随的玩家受到攻击
	, MEVENT_PLAYER_ADDHP = 24,			//当玩家加血的时候....
    , MEVENT_PLAYER_DIE  = 25,            //视野范围内某玩家死亡
};
	*/
	  "OnUpdate"            //定时执行的更新函数，目前脚本中不设此函数，但C++接口可能需要；
	, "OnEnter"             //MEVENT_ENTER
	, "OnExit"              //MEVENT_EXIT
	, "OnPlayerDetected"    //MEVENT_PLAYER_DETECTED
	, "OnPlayerLeaveView"   //MEVENT_PLAYER_LEAVEVIEW
	, "OnAttackTargetLeave" //MEVENT_ATTACKTARGET_LEAVE
	, "OnTooFarFromBornPt"  //MEVENT_TOOFARFROM_BPT
	, "OnSelfHpTooLow"      //MEVENT_SELFHP_TOOLOW
	, "OnBeAttacked"        //MEVENT_BEATTACKED  
	, "OnNpcDetected"       //MEVENT_NPC_DETECTED
	, "OnNpcLeave"          //MEVENT_NPC_LEAVE
	, "OnSelfBorn"          //MEVENT_SELF_BORN=12         //怪物自身出生（注：本事件发生在怪物从地图上出现之前，也就是说，事件发生时地图上还没有本怪物）
	, "OnSelfDie"           //MEVENT_SELF_DIE=13          //怪物自身死亡（注：本事件发生在怪物从地图上删去之后，也就是说，事件发生时地图上已经没有本怪物）
	, "OnOnDebuff"          //MEVENT_ON_DEBUFF =13,        //怪物被放DEBUFF
	, "OnEndofReturn"       //MEVENT_ENDOF_RETURN = 14,    //怪物完成回归过程
	, "OnEndofFlee"         //MEVENT_ENDOF_FLEE = 15,      //怪物完成逃跑
	, "OnMonsterEnterView"  //MEVENT_MONSTER_ENTERVIEW = 16,//怪物进入视野
	, "OnMonsterLeaveView"  //MEVENT_MONSTER_LEAVEVIEW = 17 //怪物离开视野
	, "OnBeChat"            //MEVENT_MONSTER_BECHAT = 18 //有人与本怪物对话
	, "OnSurPlayerMovChg"   //MEVENT_SURPLAYER_MOVCHG  = 19, //周围玩家移动状态改变(原因：新移动或进入怪物所在块)；
	, "OnCalledMonsterBorn" //MEVENT_CALLEDMONSTER_BORN = 20, //所召唤的怪物出生；
	, "OnAttackCityStart"	//MEVENT_ATTACK_CITY_START = 21，  //攻城开始
	, "OnAttackCityEnd"		//MEVENT_ATTACK_CITY_END = 22,	//攻城结束
	, "OnFollowPlayerBeAttack" //MEVENT_FOLLOWPLAYER_BEATTACK = 23, 玩家被攻击
	, "OnPlayerAddHP"          //MEVENT_PLAYER_ADDHP = 24, 玩家加HP
	, "OnPlayerDie"            //MEVENT_PLAYER_DIE = 25, 玩家死亡
	, "OnPlayerDropTask"       //MEVENT_PLAYER_DROP_TASK = 26,       //玩家放弃任务
	, "OnBeKickBack"           //MEVENT_BE_KICK_BACK = 27              //被击退
};

IMonsterFsm::~IMonsterFsm(){};//析构


///从配置中读入所有类型的怪物状态机；;
bool CManMonsterFsm::Init()
{
	//或读入怪物FSM脚本，或初始化C++代码Fsm，每次初始化新的Fsm之前应检查之前是否有同Id Fsm已被初始化；
	if ( true )
	{
		CMonsterScriptFsm* pTmpFsm = NEW CMonsterScriptFsm;
		if ( NULL == pTmpFsm )
		{
			D_ERROR( "CManMonsterFsm::Init，内存分配失败\n" );
			return false;
		}
		pTmpFsm->Init( "testai.lua" );		
		AddFsm( pTmpFsm->GetAIID(), pTmpFsm );
	} 
	else 
	{
		//IMonsterFsm* pTmpFsm = new CPlusPlusMonsteFsm;
	}	

	return true;
}

///寻找对应Id的脚本，用于CNpc；
IMonsterFsm* CManMonsterFsm::FindFsm( int fsmId )
{
	ACE_UNUSED_ARG( fsmId );
	return CRuleAIEngine::Instance();
};

//玩家离开当前地图(应从AI中删去)
bool CManMonsterFsm::OnPlayerLeaveMap( unsigned short wGID, unsigned int dwPID, bool isSwitchMap )
{
	PlayerID leavePlayerID;
	leavePlayerID.wGID = wGID;
	leavePlayerID.dwPID = dwPID;
	return CRuleAIEngine::OnPlayerLeaveMap( leavePlayerID, isSwitchMap );
}


