﻿/**
* @file IFsm.h
* @brief 定义怪物有限状态机C++接口
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: IFsm.h
* 摘    要: 定义怪物有限状态机C++接口
* 作    者: dzj
* 完成日期: 2007.03.19
*
*/

#pragma once

#include <map>
using namespace std;

#include "Utility.h"

///与AI交流用怪物所属地图信息；
struct AIMapInfo{
	bool isNormalOrCopy;//普通地图还是副本地图;
	unsigned int mapID;//对于普通地图为地图号，副本地图为副本号；
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///注意！！！以下定义必须保持一致，修改其中一个时记得对其它定义作相应修改
#define AI_STAT_NAME_SIZE 32
#define AI_STAT_NUM 8 //!!!, AI可能有的状态数目,必须与MONSTER_STAT中的元素数目一致；
#define AI_EVENT_NUM 18 //AI可能响应的事件数目,必须与MONSTER_EVENT中的元素数目一致；
enum MONSTER_STAT  //怪物状态;
{ 
	MSTAT_MINVALUE = -1 //下界，用于解析时的检查
  //---------------------------------------------
  ,	MSTAT_IDLE=0       //休闲状态；
  , MSTAT_PATROL=1     //巡逻状态；
  , MSTAT_FIGHTING=2   //交战状态；
  , MSTAT_ESCAPE=3     //逃跑状态；
  , MSTAT_CHASE=4      //追逐状态；
  , MSTAT_RETURN=5     //回归状态；
  , MSTAT_DYING=6      //死亡状态；
  , MSTAT_FOLLOW = 7   //跟随状态
  //--------------------------------------------
  ,	MSTAT_MAXVALUE     //上界，用于解析时的检查
};


enum MONSTER_EVENT //怪物可能响应的事件
{
	 MEVENT_MINVALUE = -1, //for check 
	 //----------------------------------------

	 MEVENT_UPDATE=0, //定时执行的更新函数，目前脚本中不设此函数，但C++接口可能需要；
	 MEVENT_ENTER=1,  //进入状态；
	 MEVENT_EXIT=2,   //退出状态；
	 MEVENT_PLAYER_DETECTED=3,    //发现玩家（玩家进入视野）；
	 MEVENT_PLAYER_LEAVEVIEW=4,       //玩家离开视野；
	 MEVENT_ATTACKTARGET_LEAVE=5, //攻击目标离开视野；
	 MEVENT_TOOFARFROM_BPT=6,     //离开出生点过远；
	 MEVENT_SELFHP_TOOLOW=7,      //自身HP小于一定百分比；
	 MEVENT_BEATTACKED=8,         //自身被打；
	 MEVENT_NPC_DETECTED=9,      //发现NPC
	 MEVENT_NPC_LEAVE=10,         //NPC离开
	 MEVENT_SELF_BORN=11,         //怪物自身出生（注：本事件发生在怪物从地图上出现之前，也就是说，事件发生时地图上还没有本怪物）
	 MEVENT_SELF_DIE=12,          //怪物自身死亡（注：本事件发生在怪物从地图上删去之后，也就是说，事件发生时地图上已经没有本怪物）
	 MEVENT_ON_DEBUFF =13,        //怪物被放DEBUFF
	 MEVENT_ENDOF_RETURN = 14,    //怪物完成回归过程
	 MEVENT_ENDOF_FLEE = 15,      //怪物完成逃跑
	 MEVENT_MONSTER_ENTERVIEW = 16,//怪物进入视野
	 MEVENT_MONSTER_LEAVEVIEW = 17, //怪物离开视野
	 MEVENT_MONSTER_BECHAT    = 18, //有人与本怪物对话；
	 MEVENT_SURPLAYER_MOVCHG  = 19, //周围玩家移动状态改变；
	 MEVENT_CALLEDMONSTER_BORN = 20, //所召唤的怪物出生；
	 MEVENT_BUFF_START = 21,  //减速BUFF开始
	 MEVENT_BUFF_END = 22, //减速BUFF结束
	 MEVENT_FOLLOWPLAYER_BEATTACK = 23,	//跟随的玩家受到攻击
	 MEVENT_PLAYER_ADDHP = 24,			//当玩家加血的时候....
	 MEVENT_PLAYER_DIE  = 25,            //视野范围内某玩家死亡
	 MEVENT_PLAYER_DROP_TASK = 26,       //玩家放弃任务
	 MEVENT_BE_KICK_BACK = 27,              //被击退
	 //----------------------------------------
	 MEVENT_MAXVALUE //only for check
};

extern const char* AI_Event_FuncName_Arr[];//对应各事件，脚本中的函数名，其排列顺序应与MONSTER_EVENT中的元素定义一一对应;
///注意！！！以上定义必须保持一致，修改其中一个时请记得对其它定义作相应修改
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


class CMonster;

class IMonsterFsm
{
	friend class CManMonsterFsm;

protected:
	IMonsterFsm(){};//构造
	virtual ~IMonsterFsm();//析构

public:
	///取该AI状态机的AI编号；
	virtual int GetAIID() = 0;
	///状态机是否响应特定状态下的特定事件；
	virtual bool IsResEvent( const MONSTER_STAT& curStat, const MONSTER_EVENT& curEvent ) = 0;

	///检查是否满足转换条件，如果满足则执行转换；
	virtual bool CheckTransCon( unsigned long monsterID ) = 0;

	/////告诉IMonsterFsm,有一个新的本实例使用者pMonster，由pMonster在第一次使用IFsm之前调用:pMonster->m_pIFsm = CManMonsterFsm::FindFsm(...); pMonster->m_pIFsm->RegisterUser(pMonster);
	//virtual bool RegisterUser( CMonster* pMonster ) = 0;
	/////告诉IMonsterFsm,pMonster已不再使用本实例，由pMonster在回收之前调用:pMonster->m_pIFsm->UnRegisterUser(pMonster); pMonster->m_pIFsm = NULL;
	//virtual bool UnRegisterUser(CMonster* pMonster ) = 0;

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	//以下为事件响应函数，响应特定事件...
	///进入状态；
	virtual bool OnEnter( unsigned long monsterID ) = 0;
	///退出状态；
	virtual bool OnExit( unsigned long monsterID ) = 0;

	///定时执行的更新函数(由程序框架定时调用，调用间隔目前初定约100毫秒)，目前脚本中不设此函数，但C++接口可能需要；
	virtual bool OnUpdate( unsigned long monsterID ) = 0;

	///发现玩家；
	virtual bool OnPlayerDetected( unsigned long monsterID ) = 0;
	///玩家离开视野；
	virtual bool OnPlayerLeaveView( unsigned long monsterID ) = 0;
	///攻击目标离开视野；
	virtual bool OnAttackTargetLeave( unsigned long monsterID ) = 0;
	///自身离开出生点太远；
	virtual bool OnTooFarFromBornPt( unsigned long monsterID ) = 0;
	///自身HP太小；
	virtual bool OnSelfHpTooLow( unsigned long monsterID ) = 0;
	///自身被打；
	virtual bool OnBeAttacked( unsigned long monsterID ) = 0;
	//自身被放debuff
	virtual bool OnDebuff( unsigned long monsterID ) = 0;
    ///发现NPC;
	virtual bool OnNpcDetected( unsigned long monsterID ) = 0;
    ///NPC离开;
	virtual bool OnNpcLeave( unsigned long monsterID ) = 0;
    ///怪物自身出生（注：本事件发生在怪物从地图上出现之前，也就是说，事件发生时地图上还没有本怪物,08.07.31修改，时机改为加入地图之后）
	virtual bool OnSelfBorn( unsigned long monsterID, const AIMapInfo& mapInfo ) = 0;
    ///怪物自身死亡（注：本事件发生在怪物从地图上删去之后，也就是说，事件发生时地图上已经没有本怪物，08.07.31修改，时机改为地图上删去之前）
	virtual bool OnSelfDie( unsigned long monsterID ) = 0;
	//怪物晕旋开始
	virtual bool OnFaintStart( unsigned long monsterID ) = 0;
	//怪物晕眩结束
	virtual bool OnFaintEnd( unsigned long monsterID ) = 0;
	//怪物束缚开始
	virtual bool OnBondageStart( unsigned long monsterID ) = 0;
	//怪物束缚结束
	virtual bool OnBondageEnd( unsigned long monsterID ) = 0;

	///怪物开始跟随；
	virtual bool OnStartFollowing( unsigned long monsterID ) = 0;

	///通知跟随怪物，其所跟随的玩家开始移动；
	virtual bool OnFollowPlayerStartMoving( unsigned long monsterID ) = 0;

	//调试怪物威胁列表开始
	virtual bool OnDebugThreatListStart( unsigned long monsterID ) = 0;
	//调试怪物威胁列表结束
	virtual bool OnDebugThreatListEnd( unsigned long monsterID ) = 0;

	///怪物进入视野，收到此事件后，通过GetAIEventInfo取得具体的进入视野怪物信息；
	virtual bool OnMonsterEnterView( unsigned long monsterID ) = 0;
	///怪物离开视野，收到此事件后，通过GetAIEventInfo取得具体的离开视野怪物信息；
	virtual bool OnMonsterLeaveView( unsigned long monsterID ) = 0;

	///有人与怪物对话；
	virtual bool OnMonsterBeChat( unsigned long monsterID ) = 0;

	///周围玩家移动状态改变(原因：新移动或进入怪物所在块)；
	virtual bool OnSurPlayerMovChg( unsigned long monsterID ) = 0;

	///所召唤的怪物出生，收到此事件后，通过GetAIEventInfo取得具体的出生怪物信息；
    virtual bool OnCalledMonsterBorn( unsigned long monsterID ) = 0;

	///通知怪物BUFF开始，现在只用于减速BUFF
	virtual bool OnMonsterBuffStart( unsigned long monsterID ) = 0;

	///通知怪物BUFF结束，现在只用于减缓BUFF
	virtual bool OnMonsterBuffEnd( unsigned long monsterID ) = 0;

	//通知AI攻城开始，monsterID无意义
	virtual bool OnAttackCityStart( unsigned long monsterID ) = 0;
	//通知AI攻城结束,monsterID无意义
	virtual bool OnAttackCityEnd( unsigned long monsterID ) = 0;
	//跟随的玩家受到攻击
	virtual bool OnFollowingPlayerBeAttack( unsigned long monsterID ) = 0;
	//当玩家加血的时候
	virtual bool OnPlayerAddHp( unsigned long monsterID ) = 0;

	//视野内的某玩家死亡时
	virtual bool OnPlayerDie( unsigned long monsterID ) = 0;

	//当玩家丢弃任务
	virtual bool OnPlayerDropTask( unsigned long monsterID ) = 0;

	//怪物被击退
	virtual bool OnBeKickBack( unsigned long monsterID )  = 0;

	//...以上为事件响应函数，响应特定事件；
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
};

class CMonsterScriptFsm;

class CManMonsterFsm
{
private:
	CManMonsterFsm();//屏蔽此构造;
	~CManMonsterFsm();//屏蔽此析构;

private:
	CManMonsterFsm( const CManMonsterFsm& );  //屏蔽这两个操作；
	CManMonsterFsm& operator = ( const CManMonsterFsm& );//屏蔽这两个操作；

public:
	///从配置中读入所有类型的怪物状态机；;
	static bool Init();

	//释放管理的脚本；
	static void Release()
	{

		for ( map<int, IMonsterFsm*>::iterator iter=m_mapMonsterFsms.begin();
			iter!=m_mapMonsterFsms.end(); ++iter )
		{
			if ( NULL != iter->second )
			{
				delete iter->second;
			} else {
				//异常；
				D_ERROR("CManMonsterFsm::Release异常\n");
			}
		}
		return;
	}

	static void AddFsm( int fsmId, IMonsterFsm* pFsm )
	{
		if ( NULL == pFsm )
		{
			D_ERROR("CManMonsterFsm::AddFsm异常，输入%d的Fsm空\n", fsmId);
			return;
		}
		map<int, IMonsterFsm*>::iterator iter = m_mapMonsterFsms.find( fsmId );
		if ( iter != m_mapMonsterFsms.end() )
		{
			D_ERROR("CManMonsterFsm::AddFsm异常，重复加入%d的Fsm\n", fsmId);
			return;
		}

		m_mapMonsterFsms.insert( pair<int, IMonsterFsm*>( fsmId, pFsm ) );
		return;
	}

	///寻找对应Id的脚本，用于CNpc；
	static IMonsterFsm* FindFsm( int fsmId );

	//玩家离开当前地图(应从AI中删去)
	static bool OnPlayerLeaveMap( unsigned short wGID, unsigned int dwPID, bool isSwitchMap );

private:
	static map<int, IMonsterFsm*> m_mapMonsterFsms;
};
