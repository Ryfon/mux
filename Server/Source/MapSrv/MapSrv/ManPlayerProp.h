﻿#pragma once
#include "ManObject.h"
#include "PlayerLevelConfig.h"
#include "XmlManager.h"

//根据不同的职业来确定的配置
class CClassLevelProp
{
private:
	CClassLevelProp( const CClassLevelProp& map);
	CClassLevelProp& operator = ( const CClassLevelProp& map ); //屏蔽这两个操作；

public:
	virtual ~CClassLevelProp(void);
	CClassLevelProp(void);

public:
	unsigned long GetObjID()
	{
		return m_lClassID;
	}

	//读取XML
	bool LoadFromXML(const char* szFileName);


	bool ReleaseManedObj()
	{
		TRY_BEGIN;

		for (map<unsigned long, CLevelProp*>::iterator tmpiter = m_mapObjs.begin();
			tmpiter != m_mapObjs.end(); ++tmpiter )
		{
			if ( NULL != tmpiter->second )
			{
				delete tmpiter->second; tmpiter->second = NULL;
			}
		}
		m_mapObjs.clear();
		return true;

		TRY_END;
		return false;
	};

	///寻找对象，如果找不到则返回NULL;
	CLevelProp* FindObj( unsigned long lObjectID )
	{
		TRY_BEGIN;

		map<unsigned long, CLevelProp*>::iterator tmpiter = m_mapObjs.find( lObjectID );
		if ( tmpiter != m_mapObjs.end() )
		{
			return tmpiter->second;
		} else {
			return NULL;
		}

		TRY_END;
		return NULL;
   	 }

public:
	///添加一个对象到管理器；
	bool AddObject( CLevelProp& object )
	{
		TRY_BEGIN;

		unsigned long lObjectID = object.GetObjID();
		map<unsigned long, CLevelProp*>::iterator tmpiter = m_mapObjs.find( lObjectID );
		if ( tmpiter != m_mapObjs.end() )
		{
			D_WARNING( "CLevelProp 类别ID号%d重复\n", lObjectID );
			return false;
		}

		CLevelProp* pObj = NEW CLevelProp;//保存对象
		*pObj = object;
		m_mapObjs.insert( pair<unsigned long, CLevelProp*>( lObjectID, pObj ) );
		return true;

		TRY_END;
		return false;
	}


	unsigned short GetMaxLevel()
	{
		return m_usMaxLevel;
	}

	const map<unsigned long,CLevelProp*>& PropSet(void)
	{
		return m_mapObjs;
	}

private:
	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
	{
		if ( NULL == pEle->GetAttr( attName ) )
		{
			D_ERROR( "玩家职业属性XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
			return false;
		}
		return true;
	}

private:
	///本管理器管理的所有怪物技能属性；
	map<unsigned long,CLevelProp*> m_mapObjs;//本管理器管理的所有怪物技能属性；

private:
	unsigned long m_lClassID;
	unsigned short m_usMaxLevel;	//该职业的最大等级
};


class CManPlayerProp : public CObjectManager<CClassLevelProp>
{
public:
	~CManPlayerProp()
	{}


	CManPlayerProp()
	{}

//初始化
public:
	static bool Init();

	static const std::map<unsigned long, CClassLevelProp*>& GetAllClassLevelProp();
};


