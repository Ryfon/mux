﻿#include "G_MProc.h"
#include "MuxMap/mannormalmap.h"
#include "PoolManager.h"
#include "Item/CItemIDGenerator.h"
#include "Item/ItemManager.h"
#include "RideState.h"
#include "GroupTeamManager.h"
#include "Rank/rankeventmanager.h"
#include "WaitQueue.h"

map<int,CGateSrv*> CManG_MProc::m_mapGateSrv;

#ifdef GATE_SENDV //玩家层sendv;
DsFerd< CGateSrv, 32 > CManG_MProc::m_arrGateHandle;//存放所有gates的ferd;
#else  //GATE_SENDV
map<int,CGateSrv*> CManG_MProc::m_mapHandle;
#endif //GATE_SENDV

unsigned int CManG_MProc::m_curMaxGateFlag = 0u;//用于存放下一个连进gatesrv的序号标识，每连进一个新的gatesrv，该值++;

extern CPoolManager* g_poolManager;
extern unsigned int g_maxMapSrvPlayerNum;

CGateSrv::CGateSrv() 
{
#ifdef USE_SELF_HASH
	m_pHashPlayers = NULL;
#else //USE_SELF_HASH
	m_mapProcPlayers.clear();  ///单个gs处理器上的玩家
#endif //USE_SELF_HASH

	m_lGID = 0x99999ul;	//gatsrv上的ID
	m_uniqueFlag = 0u;//初始无效标记；

#ifdef GATE_SENDV
	m_ownerFerd = NULL;//本实例所属的ferd数组；
	m_posInFerd = 0;//当前在ferd数组中的位置；
	m_lastSendTime = ACE_OS::gettimeofday();
	m_ToSendMsg = NULL;	
#endif //GATE_SENDV

	m_pConn = NULL;
	m_lGID = INVALID_GID;

	PoolObjInit();
};


void CGateSrv::PoolObjInit()
{
	TRY_BEGIN;

	m_bIsContainAdd = false;
	m_bIsRcvValid = false;//初始无效，连接建立后有效，发起断开之后无效，有效时可收包，无效时不收包；

	m_uniqueFlag = 0u;//初始无效标记；
	m_pConn = NULL;

	m_lGID = INVALID_GID;

	SetDisConnected();

    OnDestory( false/*只是初始化，而不是从有效状态销毁*/ );

	TRY_END;
}

void CGateSrv::InitConn( CSrvConnection* pConn )
{
	TRY_BEGIN;

	m_bIsRcvValid = true;

	m_pConn = pConn;

	SetConnected();

	m_uniqueFlag = 0u;//初始无效标记；

#ifdef GATE_SENDV //gatesrv层sendv;
	///////////////////////////////////////////////////
	//这些不要放到PoolObjInit中去...
	m_ToSendMsg = NULL;	
	m_lastSendTime = ACE_OS::gettimeofday();
	SetFerdPos( NULL, -1 );//这一句不要移到PoolObjInit中去；
	//...这些不要放到PoolObjInit中去
	///////////////////////////////////////////////////
#endif //GATE_SENDV 

#ifdef USE_SELF_HASH
	m_pHashPlayers = NEW DsHashTable< HashTbPtrEle<CPlayer>, 6000, 2 >;
#endif //USE_SELF_HASH

	m_lGID = INVALID_GID;
	CManG_MProc::AddHandle( GetHandleID(), this );

	if ( !ItemIDGeneratorSingle::instance()->IsRequest() )
	{
		ItemIDGeneratorSingle::instance()->RequestNewIDRange();
	}
	
	TRY_END;
}

///收包处理；
void CGateSrv::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )  ///这个根据不同的server可重写
{ 
	TRY_BEGIN;

	if ( !IsRcvValid() )
	{
		///已断开或已发起断连，不再从此连接收包；
		return;
	}

	if( ! CDealG_MPkg::DealPkg(this,wCmd,pBuf,wPkgLen) )
	{
		//D_WARNING("处理消息编号:%x 失败",wCmd);
	}
	return; 

	TRY_END;
}

//CGateSrv::CGateSrv(CSrvConnection* pConn) : CSrv(pConn)
//{
//	TRY_BEGIN;
//
//	m_uniqueFlag = 0;//初始无效标记；
//
//#ifdef GATE_SENDV //gatesrv层sendv;
//	///////////////////////////////////////////////////
//	//这些不要放到PoolObjInit中去...
//	m_ToSendMsg = NULL;	
//	m_lastSendTime = ACE_OS::gettimeofday();
//	SetFerdPos( NULL, -1 );//这一句不要移到PoolObjInit中去；
//	//...这些不要放到PoolObjInit中去
//	///////////////////////////////////////////////////
//#endif //GATE_SENDV 
//
//#ifdef USE_SELF_HASH
//	m_pHashPlayers = NEW DsHashTable< HashTbPtrEle<CPlayer>, 6000, 2 >;
//#endif //USE_SELF_HASH
//
//	m_lGID = INVALID_GID;
//	CManG_MProc::AddHandle( GetHandleID(), this );
//
//	if ( !ItemIDGeneratorSingle::instance()->IsRequest() )
//	{
//		ItemIDGeneratorSingle::instance()->RequestNewIDRange();
//	}
//	
//	TRY_END;
//}

//自身销毁时把管理器中的自己删除
CGateSrv::~CGateSrv() 
{
}

void CGateSrv::OnDestory( bool isTrueDesOrInit /*是真正从有效状态销毁还是初始化,真正销毁为true，初始化为false*/ )
{
	TRY_BEGIN;

	SetDisConnected();

	ClearAllCPlayer();

	//需要把在这等待队列上的GateSrv玩家也通通移除
	CWaitQueue::RemoveWaitPlayerOnGateSrv( this->GetGateSrvID() );

	if ( isTrueDesOrInit )
	{
		//真正从有效状态销毁；
		D_INFO( "CGateSrv断开, gateID:%d, handleID:%d\n", GetGateSrvID(), GetHandleID() );
		m_bIsRcvValid = false;
		if ( IsContainAdd() )//如果曾加入管理器，则从管理器删去；
		{
			CManG_MProc::RemoveGateSrv( GetGateSrvID() );
			CManG_MProc::RemoveHandle( GetHandleID(), this );
		} else {
			D_INFO( "该GateSrv之前未加入管理器，可能重复ID号Gate\n" );
		}
	}

#ifdef USE_SELF_HASH
	if ( NULL != m_pHashPlayers )
	{
		delete m_pHashPlayers; m_pHashPlayers = NULL;
	}
#endif //USE_SELF_HASH

#ifdef GATE_SENDV //gatesrv层sendv;
	if ( NULL != m_ToSendMsg )
	{
		if ( NULL != g_poolMsgToPut )
		{
			g_poolMsgToPut->Release( m_ToSendMsg );
			m_ToSendMsg = NULL;
		}
	}
	m_ToSendMsg = NULL;	
	m_lastSendTime = ACE_OS::gettimeofday();
	DelSelfFromFerd();
#endif //GATE_SENDV 

	CSrv::OnDestory( isTrueDesOrInit );

	return;
	TRY_END;
	return;
}

#ifdef GATE_SENDV
//检测所有gate句柄上有无延时数据需要发送；
void CManG_MProc::CheckSend( const ACE_Time_Value& curTime )
{
	TRY_BEGIN;

	FERDFOR( CGateSrv, (&m_arrGateHandle), tmpPtr )
	{
		if ( NULL == tmpPtr )
		{
			break;
		}
		tmpPtr->GateCheckSend( curTime );//检查数据发送；
	}
	ENDFERDFOR;

	return;
	TRY_END;
}

//检查是否有足够数据，或是否时间已太久不得不发送数据；
void CGateSrv::GateCheckSend( const ACE_Time_Value& curTime )
{
	TRY_BEGIN;
	if ( NULL == m_ToSendMsg )
	{
		//没有待发消息；
		return;
	}

	ACE_Time_Value passed = curTime - m_lastSendTime;
	if ( passed.msec() > 10 )
	{
		if ( NULL != m_ToSendMsg )
		{
			//最迟时间已到，发送出去；			
			//D_DEBUG( "延迟发包！\n" );
			RealSendPkgToGate( m_ToSendMsg );
			m_ToSendMsg = NULL;
		}
		m_lastSendTime = ACE_OS::gettimeofday();//记录消息发送时刻；
	}

	return;
	TRY_END;
	return;
}

//发送旧消息，暂存新消息；
void CGateSrv::SendOldRsvNew( MsgToPut* pNewMsg )
{
	//以下，消息足够大，立刻发送出去；
	RealSendPkgToGate( m_ToSendMsg );
	m_ToSendMsg = pNewMsg;
	return;
}

/////向外发包
//void CGateSrv::SendPkgToGateServer( MsgToPut* pPkg )
//{
//	if ( NULL == pPkg )
//	{
//		return;
//	}
//	if ( NULL == m_ToSendMsg )
//	{
//		m_ToSendMsg = pPkg;//原来无待发消息，暂存此待发消息；
//	} else {
//		//原来有待发消息，尝试将新待发消息附至原消息尾部；
//		//如果放不进去（原消息已较满），则将原消息与新消息立刻依次发送出去
//		//如果可以放进去，则将新消息附至原消息尾部
//		if ( m_ToSendMsg->nMsgLen + pPkg->nMsgLen >= sizeof( m_ToSendMsg->pMsg ) )
//		{
//			//新旧消息加起来超过了单个MsgToPut的容量，直接将这两个消息一起发出去；
//			RealSendPkgToGate( m_ToSendMsg );
//			RealSendPkgToGate( pPkg );
//			return;
//		} else {
//			//新消息可以附至旧消息尾；
//			memcpy( m_ToSendMsg->pMsg+m_ToSendMsg->nMsgLen/*拷贝目标开始位置*/, pPkg->pMsg/*拷贝源开始位置*/, pPkg->nMsgLen/*拷贝长度*/ );
//			m_ToSendMsg->nMsgLen += pPkg->nMsgLen;//旧消息长度刷新；
//			//新消息内容已附于旧消息尾待发，释放本新消息；
//			if ( NULL != g_poolMsgToPut )
//			{
//				g_poolMsgToPut->Release( pPkg );
//				pPkg = NULL;
//			} else {
//				D_ERROR( "SendPkgToPlayer, g_poolMsgToPut空\n" );
//			}
//		}		
//	}//新消息附至旧消息尾部；
//	if ( m_ToSendMsg->nMsgLen < 400 )
//	{
//		//消息还不够大，暂时不发送；
//		return;
//	}
//	//以下，消息足够大，立刻发送出去；
//	RealSendPkgToGate( m_ToSendMsg );
//
//	return;
//}

#else //GATE_SENDV

void CGateSrv::SendPkgToGateServer( MsgToPut* pPkg )
{
	RealSendPkgToGate( pPkg );
}

#endif //GATE_SENDV

void CGateSrv::RealSendPkgToGate( MsgToPut* pPkg )
{
	TRY_BEGIN;

	if ( NULL == pPkg )
	{
		return;
	}

#ifdef GATE_SENDV
	if ( 0 == RAND % 20 )
	{
		D_DEBUG( "!!!,发包(%x)大小%d\n", (DWORD)pPkg, pPkg->nMsgLen );
	}
#endif //GATE_SENDV

	if ( !IsConnected() )
	{
		//连接已断开，不再向此连接发包，用于对应gate突然关闭，此种情形下避免大量发包；
		return;
	}

	if ( NULL != g_pPkgSender )
	{
		g_pPkgSender->PushMsg( pPkg );
	}

	return;
	TRY_END;
	return;
}


void CGateSrv::ClearAllCPlayer()
{
	TRY_BEGIN;

	CPlayer* pTmpPlayer = NULL;
	vector<CPlayer*> vecPlayer;

#ifdef USE_SELF_HASH
	if ( NULL != m_pHashPlayers )
	{
		HashTbPtrEle<CPlayer>* pFound = NULL;
		m_pHashPlayers->InitExplore();
		while ( NULL != (pFound=m_pHashPlayers->ExploreNext()) )
		{
			pTmpPlayer = pFound->GetInnerPtr();
			if ( NULL == pTmpPlayer )
			{
				continue;
			}
			vecPlayer.push_back( pTmpPlayer );
		}
	}
#else //USE_SELF_HASH
	for( map<long,CPlayer*>::iterator iter = m_mapProcPlayers.begin(); iter != m_mapProcPlayers.end(); ++ iter)
	{
		pTmpPlayer = iter->second;
		if ( NULL == pTmpPlayer )
		{
			continue;
		}
		//注意,DeletePlayer方法会修改迭代器
		vecPlayer.push_back( pTmpPlayer );
	}
#endif //USE_SELF_HASH

	for( vector<CPlayer*>::iterator iter = vecPlayer.begin(); iter != vecPlayer.end(); ++iter)
	{
		IssueGateDeletePlayer( (*iter)->GetPID(), GMPlayerLeave::NO_SPECIAL );
	}

	TRY_END;
}


//bool CGateSrv::AddPlayerID(const PlayerID playerID)
//{
//	return m_playerIDBuff.AddPlayer( playerID );
//}


//void CGateSrv::ResetBuff()
//{
//	m_playerIDBuff.Reset();
//}
//
//
//int CGateSrv::GetPlayerIDNum()
//{
//	return m_playerIDBuff.GetPlayerIDNum();
//	return 0;
//}

///通知gatesrv发起踢此玩家
void CGateSrv::NotifyGateSrvKickPlayer( const PlayerID& playerID )
{
	if ( GetGateSrvID() != playerID.wGID )
	{
		D_ERROR( "NotifyGateSrvKickPlayer, 传入欲踢玩家(%d,%d)，gatesrv自身ID%d，GID不符\n", playerID.wGID, playerID.dwPID, GetGateSrvID() );
		return;
	}

	MGKickPlayer kickMsg;
	kickMsg.targetPlayerID = playerID;
#ifdef GATE_SENDV
	bool isNewPkg = false;
	MsgToPut* pNewMsg = CreateGateSrvPkg( MGKickPlayer, this, kickMsg, isNewPkg );
	if ( isNewPkg )
	{
		SendOldRsvNew( pNewMsg );
	}
#else  //GATE_SENDV
	MsgToPut* pNewMsg = CreateSrvPkg( MGKickPlayer, this, kickMsg ); 
	SendPkgToGateServer(  pNewMsg );
#endif //GATE_SENDV

	return;
}

///发起删去player
void CGateSrv::IssueGateDeletePlayer( long lPlayerPID, int nReason, bool isUpdatePlayerInfo )
{
	TRY_BEGIN;

	CPlayer* pTempPlayer = FindPlayer(lPlayerPID);
	if ( pTempPlayer == NULL )
	{
		D_ERROR("CGateSrv::IssueGateDeletePlayer,在本GateSrv实例%d上找不到玩家(%x)\n", GetGateSrvID(), lPlayerPID );
		return;
	}

	if ( pTempPlayer->IsInDelDelQue() )
	{
		D_ERROR( "IssueGateDeletePlayer,试图重复删玩家%s，忽略后一次删除\n", pTempPlayer->GetAccount() );
		return;
	}

	pTempPlayer->SetLeaveUpdateInfoToDB( isUpdatePlayerInfo );
	if ( GMPlayerLeave::RETURN_SEL_ROLE == nReason )
	{
		pTempPlayer->SetLeaveUpdateInfoToDB( true );
	}

	D_DEBUG( "IssueGateDeletePlayer，玩家%s(%d:%d,%d)离开mapsrv，原因:%d\n"
		, pTempPlayer->GetAccount(), pTempPlayer->GetMapID(), pTempPlayer->GetPosX(), pTempPlayer->GetPosY(), nReason );

	//不是跳地图,就是玩家下线,玩家下线要保留一段时间;
	if ( ( GMPlayerLeave::MAP_SWITCH_NORMAL != nReason )
		&& ( GMPlayerLeave::MAP_SWITCH_COPY != nReason )
		)
	{
		//不是跳地图，加入在线保留队列后直接返回；
		if ( GMPlayerLeave::RETURN_SEL_ROLE == nReason )
		{
			//换角色立刻del;
			pTempPlayer->RegisterDeferredOffline( true );
		} else {
			pTempPlayer->RegisterDeferredOffline( !isUpdatePlayerInfo/*若无需更新玩家信息，则必定为快速删除*/ );
		}
		return;
	}

	//以下跳地图处理--直接离开地图，没有保留时间，且进行跳地图特殊处理；
	if ( true )
	{
		//跳地图影响交易
		pTempPlayer->LeaveAffectTrade();
		//跳地图影响Buff
		pTempPlayer->OnBuffLeave();  
		//跳地图，影响玩家的答题
		pTempPlayer->GetPlayerOLTestPlate().RefreshOLTest();
		//跳地图影响善恶值的监视
		CEvilTimeControlManagerSingle::instance()->RemoveGoodEvilControl( pTempPlayer->GetDBUID() );
		//跳地图影响骑乘
		RideTeamManagerSingle::instance()->OnPlayerSwitchMap( pTempPlayer );
		//跳地图影响组队
		GroupTeamManagerSingle::instance()->OnGroupPlayerSwichMap( pTempPlayer );
		//跳地图影响道具
		ItemManagerSingle::instance()->RemovePlayerItem( pTempPlayer );
		//跳地图影响限时任务
		CLimitTimeTaskManagerSingle::instance()->OnPlayerLeaveSrv( pTempPlayer );
		//跳地图影响排行榜
//#ifdef RANKCHART_OPEN
		//RankEventManagerSingle::instance()->OnPlayerLeaveMap( pTempPlayer );
		RankEventManager::OnPlayerLeaveMap( pTempPlayer );
//#endif //RANKCHART_OPEN
		CUnion *pUnion = pTempPlayer->Union(); //copy...,
		if(NULL != pUnion)
		{
			pUnion->UnregisterPlayer(pTempPlayer);//取消工会注册
		}
		//跳地图影响宠物
		CMuxPet * pPet = pTempPlayer->GetPet();
		if (NULL != pPet)
		{
			pPet->OnPlayerSwitchMap();
		}
	}

	if ( GMPlayerLeave::MAP_SWITCH_COPY == nReason )
	{
		pTempPlayer->OfflineProc( INFO_SWITCH_UPDATE, false/*进副本离开当前地图，不清副本信息*/ );
	} else {
		pTempPlayer->OfflineProc( INFO_SWITCH_UPDATE, true/*进普通地图离开当前地图，清副本信息*/ );
	}
	
	return;
	TRY_END;
	return;
}

///每个gatesrv定时执行的时钟(定时通知各gate，本mapsrv是否可进入)；
void CManG_MProc::ManGateSrvTimer()
{
	TRY_BEGIN;

	static unsigned int MAX_GATE_TIMER = 0;
	++ MAX_GATE_TIMER;
	if ( 0 != (MAX_GATE_TIMER%2) )
	{
		//每秒钟通知各gate一次自身是否可进；
		return;
	}

	MGMapIsFull mapIsFull;
	mapIsFull.isFull = ((CPlayerManager::GetPlayerSize() + 500 ) >= g_maxMapSrvPlayerNum);

	CGateSrv* pGateSrv = NULL;
	MsgToPut* pNewMsg = NULL;

	for ( map<int, CGateSrv*>::iterator iter = m_mapGateSrv.begin(); iter != m_mapGateSrv.end(); ++iter )
	{
		pGateSrv = iter->second;
		if ( NULL != pGateSrv )
		{
			pNewMsg = CreateSrvPkg( MGMapIsFull, pGateSrv, mapIsFull );
			if ( NULL != pNewMsg )
			{
				pGateSrv->SendPkgToGateServer(pNewMsg);
			}
		}
	}

	return;
	TRY_END;
	return;
}


