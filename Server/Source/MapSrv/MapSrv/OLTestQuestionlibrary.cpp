﻿#include "OLTestQuestionlibrary.h"
#include "XmlManager.h"
#include "../../Base/Utility.h"

std::map<unsigned int, Question > OLTestQuestionLibrary::mQuestionsLibrary;

void OLTestQuestionLibrary::ReadQuestionLib()
{
	const char xmlFilePath[] = {"config/question/question_s.xml"};
	
	CXmlManager xmlManager;
	if(!xmlManager.Load(xmlFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", xmlFilePath );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",xmlFilePath );
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	for ( std::vector<CElement *>::iterator lter = childElements.begin(); 
		lter != childElements.end();
		++lter )
	{
		CElement* pElement = *lter;
		if ( pElement )
		{
			if ( pElement->GetAttr("solution") && pElement->GetAttr("timelimit") && pElement->GetAttr("id") )
			{
				Question question;
				question.answerID   = ACE_OS::atoi( pElement->GetAttr("solution") );
				question.limittime  = ACE_OS::atoi( pElement->GetAttr("timelimit") );
				question.questionID = ACE_OS::atoi( pElement->GetAttr("id") );
				mQuestionsLibrary.insert( std::pair<unsigned int, Question>(question.questionID, question) );
			}
		}
	}

}