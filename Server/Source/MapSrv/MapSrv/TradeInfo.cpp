﻿#include "TradeInfo.h"
#include "Utility.h"


CTradeInfo::CTradeInfo(void)
{
	StructMemSet( m_invitePlayerID, 0x00, sizeof(m_invitePlayerID));
	StructMemSet( m_targetPlayerID, 0x00, sizeof(m_targetPlayerID));
	StructMemSet(m_data, 0x00, sizeof(m_data));

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < MAX_TRADE_NUM; j++)
			m_data[i].iItemIndexs[j] = INVALAD_INDEX;
	}
}

CTradeInfo::CTradeInfo(PlayerID invitePlayerID, PlayerID targetPlayerID)
{
	m_invitePlayerID = invitePlayerID;
	m_targetPlayerID = targetPlayerID;
	StructMemSet(m_data, 0x00, sizeof(m_data));
	
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < MAX_TRADE_NUM; j++)
			m_data[i].iItemIndexs[j] = INVALAD_INDEX;
	}
}

CTradeInfo::~CTradeInfo(void)
{

}

void CTradeInfo::InvitePlayerID(PlayerID &uiPlayerID)
{
	m_invitePlayerID = uiPlayerID;
	return;
}

PlayerID CTradeInfo::InvitePlayerID(void)
{
	return m_invitePlayerID;
}

void CTradeInfo::TargetPlayerID(PlayerID &uiPlayerID)
{
	m_targetPlayerID = uiPlayerID;
	return ;
}

PlayerID CTradeInfo::TargetPlayerID(void)
{
	return m_targetPlayerID;
}

void CTradeInfo::State(eTradePlayerIndex index, TradeData::eState newState)
{
	if((INVITE_PLAYER != index) && (TARGET_PLAYER != index))
		return;

	m_data[index].state = newState;
	return;
}

CTradeInfo::TradeData::eState CTradeInfo::State(eTradePlayerIndex index)
{
	if((INVITE_PLAYER != index) && (TARGET_PLAYER != index))
		return TradeData::TRADE_INIT;
		
	return m_data[index].state;
}

bool CTradeInfo::AddItem(CTradeInfo::eTradePlayerIndex index, int iItemIndex)
{
	if((INVITE_PLAYER != index) && (TARGET_PLAYER != index))
		return false;

	TradeData &tradeData = m_data[index];
	unsigned short usMaxCount = sizeof(tradeData.iItemIndexs)/sizeof(int);
	
	if(tradeData.usCount >= usMaxCount)
	{
		D_ERROR("CTradeInfo::AddItem, tradeData.usCount=%d, usMaxCount=%d\n", tradeData.usCount, usMaxCount);
		return false;
	}

	// 检查是否重复
	for(int i = 0; i < usMaxCount; i++)
	{
		if(tradeData.iItemIndexs[i] == iItemIndex)
			return false;
	}

	// 插入
	for(unsigned short i = 0; i < usMaxCount; i++)
	{
		if(tradeData.iItemIndexs[i] == INVALAD_INDEX)
		{
			tradeData.iItemIndexs[i] = iItemIndex;
			tradeData.usCount++;

			break;
		}
	}

	return true;
}

void CTradeInfo::DeleteItem(CTradeInfo::eTradePlayerIndex index, int iItemIndex)
{
	if((INVITE_PLAYER != index) && (TARGET_PLAYER != index))
		return;

	TradeData &tradeData = m_data[index];
	unsigned short usMaxCount = sizeof(tradeData.iItemIndexs)/sizeof(int);

	for(unsigned short i = 0; i < usMaxCount; i++)
	{
		if(tradeData.iItemIndexs[i] == iItemIndex)
		{
			tradeData.iItemIndexs[i] = INVALAD_INDEX;
			tradeData.usCount--;

			break;
		}
	}

	return;
}

void CTradeInfo::TradeItemInfo(eTradePlayerIndex index, int *&pItemIndexs, unsigned short &usCount)
{
	if ( (INVITE_PLAYER != index) && (TARGET_PLAYER != index) )
	{
		return;
	}

	pItemIndexs = m_data[index].iItemIndexs;
	usCount = m_data[index].usCount;

	return;
}

void CTradeInfo::Money(eTradePlayerIndex index, unsigned int uiMoney)
{
	if((INVITE_PLAYER != index) && (TARGET_PLAYER != index))
		return;

	m_data[index].uiMoney = uiMoney;
}

unsigned int CTradeInfo::Money(eTradePlayerIndex index)
{
	if((INVITE_PLAYER != index) && (TARGET_PLAYER != index))
		return 0;

	return m_data[index].uiMoney;
}

bool CTradeInfo::BothConfirmTrade(void)
{
	return (m_data[0].state == TradeData::TRADE_CONFIRM) && (m_data[1].state == TradeData::TRADE_CONFIRM);
}

std::map<unsigned int,CTradeInfo*> CTradeInfoManager::m_tradeInfoMap;
unsigned int CTradeInfoManager::m_count = 0;

CTradeInfo * CTradeInfoManager::GetTradeInfo(unsigned int tradeID)
{
	std::map<unsigned int,CTradeInfo*>::iterator itr = m_tradeInfoMap.find(tradeID);
	if (itr != m_tradeInfoMap.end())
	{
		return itr->second;
	}
	return NULL;
}

CTradeInfo * CTradeInfoManager::NewTradeInfo(unsigned int& outtradeID)
{
	outtradeID = ++m_count;
	CTradeInfo * pTradeInfo = NEW CTradeInfo();
	m_tradeInfoMap.insert(std::make_pair(m_count,pTradeInfo));
	return pTradeInfo;
}

CTradeInfo * CTradeInfoManager::NewTradeInfo(PlayerID invitePlayerID, PlayerID targetPlayerID,unsigned int& outtradeID)
{
	outtradeID = ++m_count;
	CTradeInfo * pTradeInfo = NEW CTradeInfo(invitePlayerID,targetPlayerID);
	m_tradeInfoMap.insert(std::make_pair(m_count,pTradeInfo));
	return pTradeInfo;
}

void CTradeInfoManager::DelTradeInfo(unsigned int tradeID)
{
	std::map<unsigned int,CTradeInfo*>::iterator itr = m_tradeInfoMap.find(tradeID);
	if (itr != m_tradeInfoMap.end())
	{
		delete itr->second;
		itr->second = NULL;
		m_tradeInfoMap.erase(itr);
	}
}

