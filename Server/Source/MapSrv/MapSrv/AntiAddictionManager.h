﻿
#ifndef ANTIADDICTIONMANAGER_H
#define ANTIADDICTIONMANAGER_H


//防沉迷的管理器

enum AddictiontState
{
	NORMAL,		//正常状态
	TRIED,		//疲劳状态
	ADDICTION,	//沉迷状态
};

//关于收益的类型
enum ProfitType  
{
	TYPE_EXP,						//经验值的收益		
	TYPE_MONEY,						//金钱的收益,单指从任务中获得的,现在组队分到的钱也减半
	TYPE_RACE_PRESTIGE,				//种族声望的收益
	TYPE_GOODEVILPOINT,				//善恶值的收益
	SKILL_SPEXP,					//技能的SP经验
	PHYMAG_EXP,						//物理魔法熟练度
};

class CPlayer;


class CAntiAddictionManager
{
public:
	CAntiAddictionManager(void);
	~CAntiAddictionManager(void);

	void AttachOwner( CPlayer* pPlayer );
	unsigned int GetState();			//获取状态
	bool IsTriedState();	//是否在疲劳状态
	void TriedStateProfitCheck( unsigned int& addVal, ProfitType addType );	//如果是未成年人且在疲劳状态，增加的值可能要减少一半 
	bool TriedStatePKCheck();			//在疲劳状态下的PK判定
	void SetState( unsigned int state );
	bool IsTriedOrAddictionState();

private:
	AddictiontState m_state;		//玩家当前在什么状态	
	CPlayer* m_pOwner;			//寄主
};



#endif /*ANTIADDICTIONMANAGER_H*/
