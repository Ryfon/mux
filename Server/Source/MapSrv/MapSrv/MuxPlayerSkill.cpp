﻿#include "MuxPlayerSkill.h"
#include "MuxSkillConfig.h"
#include "Player/Player.h"


CMuxPlayerSkill::CMuxPlayerSkill():m_pSkillProp( NULL), m_dbIndex( 0 ), m_pOwner( NULL )
{
}

CMuxPlayerSkill::~CMuxPlayerSkill()
{
}


bool CMuxPlayerSkill::InitPlayerSkill( const SkillInfo_i& skillInfo, unsigned int dbIndex, CPlayer* pPlayer )		//初始化
{
	if( NULL == pPlayer )
		return false;

	m_pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( skillInfo.effectiveSkillID/*skillID使用有效技能编号*/ );
	if( NULL == m_pSkillProp )
		return false;

	pPlayer->RestSkillCheck( m_pSkillProp );

	m_pOwner = pPlayer;
	m_dbData = skillInfo;
	m_dbIndex = dbIndex;
	m_bIsGuildSkill = false;
	return true;
}


bool CMuxPlayerSkill::IsSpSkill()
{
	if( NULL == m_pSkillProp )
	{
		D_ERROR("CMuxPlayerSkill::IsSpSkill() m_pSkillProp = NULL\n");
		return false;
	}

	return m_pSkillProp->IsSpSkill();
}


void CMuxPlayerSkill::AddSpExp( unsigned int spExp )
{
	if( NULL == m_pOwner || NULL == m_pSkillProp )
		return;

	unsigned nextSkillID = GetNextSkillID();
	if( nextSkillID == 0 )
		return;

#ifdef ANTI_ADDICTION
	m_pOwner->TriedStateProfitCheck( spExp, SKILL_SPEXP );
#endif	//ANTI_ADDICTION

	/* zsw注释掉skillExp相关的代码
	m_dbData.skillExp += spExp;
	if( m_dbData.skillExp >= m_pSkillProp->m_skillDegree )
	{
		CMuxSkillProp* pNextSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( nextSkillID );
		if( NULL == pNextSkillProp )
		{
			D_ERROR("SP技能升级时找不到下一级的SP技能\n");
			return;
		}

#ifdef NEW_SKILL_UPDATE
		m_pOwner->UpdateSkillInfoToDB();
#endif

		//因为是一个新技能了
		m_pSkillProp = pNextSkillProp;
		m_dbData.skillExp = 0; 
		MGSpSkillUpgrade upgradeMsg;
		upgradeMsg.oldSpSkillID = m_dbData.skillID;
		upgradeMsg.newSpSkillID = m_pSkillProp->m_skillID;
		m_pOwner->SendPkg<MGSpSkillUpgrade>( &upgradeMsg );
		CLogManager::DoSkillLevelUp(m_pOwner, LOG_SVC_NS::ST_SP, m_dbData.skillID, m_dbData.skillID, m_pSkillProp->m_skillID);//记日至
		m_dbData.skillID = m_pSkillProp->m_skillID;
	}*/
	UpdateDbData();
}


unsigned int CMuxPlayerSkill::GetSpExp()
{
	//return m_dbData.skillExp;注释掉skillExp相关的代码
	return 0;
}


unsigned int CMuxPlayerSkill::GetNextSkillID()
{
	/*if( NULL == m_pSkillProp )
	{
		D_ERROR("CMuxPlayerSkill::GetNextSkillID() m_pSkillProp = NULL\n");
		return false;
	}

	return m_pSkillProp->GetNextSkillID();*/
	
	CMuxSkillProp* pProp = MuxSkillPropSingleton::instance()->FindTSkillProp(m_dbData.skillID);
	if(NULL != pProp)
		return pProp->GetNextSkillID();

	return 0;
}


unsigned int CMuxPlayerSkill::GetSkillID() 
{
	/*if( NULL == m_pSkillProp )
	{
		D_ERROR("CMuxPlayerSkill::GetSkillID() m_pSkillProp = NULL\n");
		return false;
	}

	return m_pSkillProp->m_skillID;*/
	return m_dbData.skillID;
}



bool CMuxPlayerSkill::IsAssistSkill()
{
	if( NULL == m_pSkillProp )
	{
		D_ERROR("CMuxPlayerSkill::IsAssistSkill() m_pSkillProp = NULL\n");
		return false;
	}

	return m_pSkillProp->IsAssistSkill();
}


void CMuxPlayerSkill::UpdateDbData()
{
	if( m_pOwner )
	{	
		//m_pOwner->SetSkillData( m_dbIndex, m_dbData.skillID, m_dbData.skillExp );注释掉skillExp相关的代码
		m_pOwner->SetSkillData( m_dbIndex, m_dbData.skillID, m_dbData.effectiveSkillID );
	}
}


void CMuxPlayerSkill::SetDbIndex( unsigned int dbIndex )
{
	m_dbIndex = dbIndex;
}


unsigned int CMuxPlayerSkill::GetSkillType()
{
	if( NULL == m_pSkillProp )
	{
		D_ERROR("CMuxPlayerSkill::GetSkillType() m_pSkillProp = NULL\n");
		return 0;
	}

	return ( m_pSkillProp->m_skillType );
}


unsigned int CMuxPlayerSkill::GetUpgradePoints()
{
	if( NULL == m_pSkillProp )
	{
		D_ERROR("CMuxPlayerSkill::GetUpgradePoints() m_pSkillProp = NULL\n");
		return 0;
	}

	return ( m_pSkillProp->m_upgradePoint );
}


bool CMuxPlayerSkill::UpdateSkill( unsigned int skillID)
{
	CMuxSkillProp* pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( skillID );
	if( NULL == pSkillProp )
		return false;

	m_dbData.skillID = skillID;
	//m_dbData.skillExp = 0;//注释掉skillExp相关的代码
	m_dbData.effectiveSkillID = skillID;
	m_pSkillProp = pSkillProp;
	UpdateDbData();
	return true;
}