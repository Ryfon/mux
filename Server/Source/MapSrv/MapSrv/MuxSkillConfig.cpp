﻿#include "MuxSkillConfig.h"
#include "MuxSkillProp.h"
#include "Utility.h"






template<typename T>
bool MuxSkillConfig<T>::IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
{
	if ( NULL == pEle || NULL == attName )
	{
		return false;
	}
	if ( NULL == pEle->GetAttr( attName ) )
	{
		D_ERROR( "玩家技能属性XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
		return false;
	}
	return true;
}


template <>
bool MuxSkillConfig<MuxPlayerDamageProp>::LoadSkillPropT( const char* szFilePath )
{
	CXmlManager xmlManager;
	if(!xmlManager.Load(szFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szFilePath );
		return false;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szFilePath);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements( pRoot, childElements );
	CElement* tmpEle = NULL;
	if( childElements.empty() )
	{
		D_WARNING("MuxPlayerDamageProp属性为空\n");
		return true;
	}

	unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = childElements.begin();
		tmpiter != childElements.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( NULL == tmpEle )
			continue;

		int curpos = (int)(tmpiter - childElements.begin());
		bool isSuccess = false;
		MuxPlayerDamageProp* pPlayerDam = NEW MuxPlayerDamageProp;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			do{
				if ( !IsXMLValValid( tmpEle, "nID", curpos ) )
				{
					break;
				}		
				pPlayerDam->mID = (unsigned int)(atoi)( tmpEle->GetAttr("nID") );

				if ( !IsXMLValValid( tmpEle, "Attack", curpos ) )
				{
					break;
				}		
				pPlayerDam->fAttack = (float)(atoi)( tmpEle->GetAttr("Attack") );
		
				if ( !IsXMLValValid( tmpEle, "NumAffix", curpos ) )
				{
					break;
				}		
				pPlayerDam->mNumAffix = (unsigned int)(atoi)( tmpEle->GetAttr("NumAffix") );

   				if ( !IsXMLValValid( tmpEle, "Num", curpos ) )
				{
					break;
				}		
				pPlayerDam->mNum = (unsigned int)(atoi)( tmpEle->GetAttr("Num") );
				//NumDamage
				if ( !IsXMLValValid( tmpEle, "NumDamage", curpos ) )
				{
					break;
				}		
				pPlayerDam->mNumDamage = (unsigned int)(atoi)( tmpEle->GetAttr("NumDamage") );

				if ( !IsXMLValValid( tmpEle, "CriAddDamage", curpos ) )
				{
					break;
				}		
				pPlayerDam->mCriAddDamage = (unsigned int)(atoi)( tmpEle->GetAttr("CriAddDamage") );

				if ( !IsXMLValValid( tmpEle, "Num", curpos ) )
				{
					break;
				}		
				pPlayerDam->mNum = (unsigned int)(atoi)( tmpEle->GetAttr("Num") );

				if ( !IsXMLValValid( tmpEle, "FactTime", curpos ) )
				{
					break;
				}		
				pPlayerDam->mFactTime = (unsigned int)(atoi)( tmpEle->GetAttr("FactTime") );

				if ( !IsXMLValValid( tmpEle, "EffectMoveSpeed", curpos ) )
				{
					break;
				}		
				pPlayerDam->mEffectMoveSpeed = (unsigned int)(atoi)( tmpEle->GetAttr("EffectMoveSpeed") );

				if( m_mapMuxSkillPropT.find( pPlayerDam->mID ) !=  m_mapMuxSkillPropT.end() )
				{
					D_ERROR("重复的ID %d\n", pPlayerDam->mID );
					break;
				}
				isSuccess = true;
			}while( false );

			if( !isSuccess )
			{
				delete pPlayerDam; pPlayerDam = NULL;
				continue;
			}
		
			m_mapMuxSkillPropT.insert( make_pair( pPlayerDam->mID, pPlayerDam ) );
		}
	}
	return true;
}


template <>
bool MuxSkillConfig<MuxNpcDamageProp>::LoadSkillPropT( const char* szFilePath )
{
	CXmlManager xmlManager;
	if(!xmlManager.Load(szFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szFilePath );
		return false;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szFilePath);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements( pRoot, childElements );
	CElement* tmpEle = NULL;
	if( childElements.empty() )
	{
		D_WARNING("MuxPlayerDamageProp属性为空\n");
		return true;
	}

	unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = childElements.begin();
		tmpiter != childElements.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( NULL == tmpEle )
			continue;

		int curpos = (int)(tmpiter - childElements.begin());
		MuxNpcDamageProp* pNpcDam = NEW MuxNpcDamageProp;
		bool isSuccess = false;
	
		if ( tmpEle->Level() == rootLevel+1 )
		{
			do{
				if ( !IsXMLValValid( tmpEle, "nID", curpos ) )
				{
					break;
				}		
				pNpcDam->mID = (unsigned int)(atoi)( tmpEle->GetAttr("nID") );

				if ( !IsXMLValValid( tmpEle, "nHit", curpos ) )
				{
					break;
				}		
				pNpcDam->fHit = (float)(atoi)( tmpEle->GetAttr("nHit") );
		
				if ( !IsXMLValValid( tmpEle, "nAddHit", curpos ) )
				{
					break;
				}		
				pNpcDam->addHit = (float)(atoi)( tmpEle->GetAttr("nAddHit") );

				if ( !IsXMLValValid( tmpEle, "nCritical", curpos ) )
				{
					break;
				}		
				pNpcDam->nCritical = (float)(atoi)( tmpEle->GetAttr("nCritical") );
				//nAddAtt
				if ( !IsXMLValValid( tmpEle, "nAddAtt", curpos ) )
				{
					break;
				}		
				pNpcDam->nAddAtt = (unsigned int)(atoi)( tmpEle->GetAttr("nAddAtt") );

				if ( !IsXMLValValid( tmpEle, "nAttMin", curpos ) )
				{
					break;
				}		
				pNpcDam->attMin = (unsigned int)(atoi)( tmpEle->GetAttr("nAttMin") );

				if ( !IsXMLValValid( tmpEle, "nAttMax", curpos ) )
				{
					break;
				}		
				pNpcDam->attMax = (unsigned int)(atoi)( tmpEle->GetAttr("nAttMax") );

				if( m_mapMuxSkillPropT.find( pNpcDam->mID ) !=  m_mapMuxSkillPropT.end() )
				{
					D_ERROR("重复的ID %d\n", pNpcDam->mID );
					break;
				}
				isSuccess = true;
			}while( false );

			if( !isSuccess )
			{
				delete pNpcDam; pNpcDam = NULL;
				continue;
			}
			m_mapMuxSkillPropT.insert( make_pair( pNpcDam->mID, pNpcDam ) );
		}
	}
	return true;
}


template <>
bool MuxSkillConfig<MuxItemSkillProp>::LoadSkillPropT( const char* szFilePath )
{
	CXmlManager xmlManager;
	if(!xmlManager.Load(szFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szFilePath );
		return false;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szFilePath);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements( pRoot, childElements );
	CElement* tmpEle = NULL;
	if( childElements.empty() )
	{
		D_WARNING("MuxItemSkillProp属性为空\n");
		return true;
	}

	unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = childElements.begin();
		tmpiter != childElements.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( NULL == tmpEle )
			continue;

		int curpos = (int)(tmpiter - childElements.begin());
		MuxItemSkillProp* pItemSkillProp = NEW MuxItemSkillProp;
		bool isSuccess = false;
	
		if ( tmpEle->Level() == rootLevel+1 )
		{
			do{
				if ( !IsXMLValValid( tmpEle, "nID", curpos ) )
				{
					break;
				}		
				pItemSkillProp->mID = (unsigned int)(atoi)( tmpEle->GetAttr("nID") );

				if ( !IsXMLValValid( tmpEle, "HealType", curpos ) )
				{
					break;
				}		
				pItemSkillProp->HealType = (unsigned int)(atoi)( tmpEle->GetAttr("HealType") );
		
				if ( !IsXMLValValid( tmpEle, "DamageMethod", curpos ) )
				{
					break;
				}		
				pItemSkillProp->DamageMethod = (unsigned int)(atoi)( tmpEle->GetAttr("DamageMethod") );

				if ( !IsXMLValValid( tmpEle, "MinHeal", curpos ) )
				{
					break;
				}		
				pItemSkillProp->MinHeal = (unsigned int)(atoi)( tmpEle->GetAttr("MinHeal") );
				//nAddAtt
				if ( !IsXMLValValid( tmpEle, "MaxHeal", curpos ) )
				{
					break;
				}		
				pItemSkillProp->MaxHeal = (unsigned int)(atoi)( tmpEle->GetAttr("MaxHeal") );

				if( m_mapMuxSkillPropT.find( pItemSkillProp->mID ) !=  m_mapMuxSkillPropT.end() )
				{
					D_ERROR("重复的ID %d\n", pItemSkillProp->mID );
					break;
				}
				isSuccess = true;
			}while( false );

			if( !isSuccess )
			{
				delete pItemSkillProp; pItemSkillProp = NULL;
				continue;
			}

			m_mapMuxSkillPropT.insert( make_pair( pItemSkillProp->mID, pItemSkillProp ) );
		}
	}
	return true;
}


template <>
bool MuxSkillConfig<MuxSpiltProp>::LoadSkillPropT( const char* szFilePath )
{
	CXmlManager xmlManager;
	if(!xmlManager.Load(szFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szFilePath );
		return false;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szFilePath);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements( pRoot, childElements );
	CElement* tmpEle = NULL;
	if( childElements.empty() )
	{
		D_WARNING("MuxSpiltProp属性为空\n");
		return true;
	}

	unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = childElements.begin();
		tmpiter != childElements.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( NULL == tmpEle )
			continue;

		int curpos = (int)(tmpiter - childElements.begin());
		MuxSpiltProp* pSpiltProp = NEW MuxSpiltProp;
		bool isSuccess = false;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			do{
				if ( !IsXMLValValid( tmpEle, "nID", curpos ) )
				{
					break;
				}		
				pSpiltProp->mID = (unsigned int)(atoi)( tmpEle->GetAttr("nID") );

				if ( !IsXMLValValid( tmpEle, "StartTime", curpos ) )
				{
					break;
				}		
				pSpiltProp->StartTime = (unsigned int)(atoi)( tmpEle->GetAttr("StartTime") );
		
				if ( !IsXMLValValid( tmpEle, "MidwaySpeed", curpos ) )
				{
					break;
				}		
				pSpiltProp->MidwaySpeed = (unsigned int)(atoi)( tmpEle->GetAttr("MidwaySpeed") );

   				if ( !IsXMLValValid( tmpEle, "EndTime", curpos ) )
				{
					break;
				}		
				pSpiltProp->EndTime = (unsigned int)(atoi)( tmpEle->GetAttr("EndTime") );
				//DamageSplit
				if ( !IsXMLValValid( tmpEle, "DamageSplit", curpos ) )
				{
					break;
				}		
				pSpiltProp->DamageSplit = (unsigned int)(atoi)( tmpEle->GetAttr("DamageSplit") );

				if ( !IsXMLValValid( tmpEle, "MinMidwayTime", curpos ) )
				{
					break;
				}		
				pSpiltProp->MinMidwayTime = (unsigned int)(atoi)( tmpEle->GetAttr("MinMidwayTime") );

				if( m_mapMuxSkillPropT.find( pSpiltProp->mID ) !=  m_mapMuxSkillPropT.end() )
				{
					D_ERROR("重复的ID %d\n", pSpiltProp->mID );
					break;
				}
				isSuccess = true;
			}while( false );

			if( !isSuccess )
			{
				delete pSpiltProp; pSpiltProp = NULL;
				continue;
			}

			m_mapMuxSkillPropT.insert( make_pair( pSpiltProp->mID, pSpiltProp ) );
		}
	}
	return true;
}


template <>
bool MuxSkillConfig<MuxBuffProp>::LoadSkillPropT( const char* szFilePath )
{
	CXmlManager xmlManager;
	if(!xmlManager.Load(szFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szFilePath );
		return false;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szFilePath);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements( pRoot, childElements );
	CElement* tmpEle = NULL;
	if( childElements.empty() )
	{
		D_WARNING("MuxBuffProp属性为空\n");
		return true;
	}

	unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = childElements.begin();
		tmpiter != childElements.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( NULL == tmpEle )
			continue;

		int curpos = (int)(tmpiter - childElements.begin());
		MuxBuffProp* pMuxBuffProp = NEW MuxBuffProp;
		bool isSuccess = false;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			do{
				if ( !IsXMLValValid( tmpEle, "BuffID", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->mID = (unsigned int)(atoi)( tmpEle->GetAttr("BuffID") );

				if ( !IsXMLValValid( tmpEle, "BuffLevel", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->BuffLevel = (unsigned int)(atoi)( tmpEle->GetAttr("BuffLevel") );
		
				if ( !IsXMLValValid( tmpEle, "SubType", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->SubType = (unsigned int)(atoi)( tmpEle->GetAttr("SubType") );

   				if ( !IsXMLValValid( tmpEle, "AssistChoice", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->AssistChoice = (unsigned int)(atoi)( tmpEle->GetAttr("AssistChoice") );
				//AssistType
				if ( !IsXMLValValid( tmpEle, "AssistType", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->AssistType = (unsigned int)(atoi)( tmpEle->GetAttr("AssistType") );

				if ( !IsXMLValValid( tmpEle, "Type", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->bgType = (unsigned int)(atoi)( tmpEle->GetAttr("Type") );

				if ( !IsXMLValValid( tmpEle, "PushIndex", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->PushIndex = (unsigned int)(atoi)( tmpEle->GetAttr("PushIndex") );

				if ( !IsXMLValValid( tmpEle, "PushRule", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->PushRule = (unsigned int)(atoi)( tmpEle->GetAttr("PushRule") );

				if ( !IsXMLValValid( tmpEle, "nPeriodOfTime", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->nPeriodOfTime = (unsigned int)(atoi)( tmpEle->GetAttr("nPeriodOfTime") );

				if ( !IsXMLValValid( tmpEle, "nLinkRate", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->LinkRate = (float)(atoi)( tmpEle->GetAttr("nLinkRate") );

				if ( !IsXMLValValid( tmpEle, "AssistObject", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->AssistObject = (unsigned int)(atoi)( tmpEle->GetAttr("AssistObject") );

				if ( !IsXMLValValid( tmpEle, "nActionSpace", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->nActionSpace = (unsigned int)(atoi)( tmpEle->GetAttr("nActionSpace") );

				if ( !IsXMLValValid( tmpEle, "Value", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->Value = (unsigned int)(atoi)( tmpEle->GetAttr("Value") );

				if ( !IsXMLValValid( tmpEle, "DeathClear", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->isDeathClear = ( (atoi)( tmpEle->GetAttr("DeathClear") ) != 0 );

				if ( !IsXMLValValid( tmpEle, "OffLineSave", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->isOffLineSave = ( (atoi)( tmpEle->GetAttr("OffLineSave") ) != 0 );

				if ( !IsXMLValValid( tmpEle, "RightClickClear", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->isRightClickClear = ( (atoi)( tmpEle->GetAttr("RightClickClear") ) != 0 );
		
   				if ( !IsXMLValValid( tmpEle, "AffectLowestLevel", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->AffectLowestLevel = (atoi)( tmpEle->GetAttr("AffectLowestLevel") );

				if ( !IsXMLValValid( tmpEle, "TierNum", curpos ) )
				{
					return false;
				}		
				pMuxBuffProp->TierNum = (atoi)( tmpEle->GetAttr("TierNum") );

				if( m_mapMuxSkillPropT.find( pMuxBuffProp->mID ) !=  m_mapMuxSkillPropT.end() )
				{
					D_ERROR("重复的BuffProp ID %d\n", pMuxBuffProp->mID );
					break;
				}
				isSuccess = true;
			}while( false );

			if( !isSuccess )
			{
				delete pMuxBuffProp; pMuxBuffProp = NULL;
				continue;
			}

			m_mapMuxSkillPropT.insert( make_pair( pMuxBuffProp->mID, pMuxBuffProp ) );
		}
	}

	//读取复合型BUFF	
	MuxBuffProp* pLinkBuffProp = NULL;
	MuxBuffProp* pMainBuffProp = NULL;
	unsigned int mainBuffID = 0, linkBuffID = 0;
	char szToken[128];
	for ( vector<CElement*>::iterator tmpiter = childElements.begin();
		tmpiter != childElements.end(); ++tmpiter )
	{
		if ( tmpEle->Level() == rootLevel+1 )
		{
			int curpos = (int)(tmpiter - childElements.begin());
			tmpEle = *tmpiter;
			if( NULL == tmpEle )
				continue;

			if ( !IsXMLValValid( tmpEle, "BuffID", curpos ) )
			{
				return false;
			}
			mainBuffID = (unsigned int)(atoi)(tmpEle->GetAttr("BuffID"));

			pMainBuffProp = BuffPropSingleton::instance()->FindTSkillProp( mainBuffID );
			if( NULL == pMainBuffProp )
			{
				D_ERROR("找不到ID:%d的BUFFPROP\n", mainBuffID );
				return false;
			}

			if ( !IsXMLValValid( tmpEle, "LinkBuffID", curpos ) )
			{
				return false;
			}
						
			SafeStrCpy( szToken, tmpEle->GetAttr("LinkBuffID") );
			char* token = strtok( szToken, "," );
			while( NULL != token )
			{
				linkBuffID = (atoi)(token);
				if( linkBuffID != 0 )
				{
					pLinkBuffProp = BuffPropSingleton::instance()->FindTSkillProp( linkBuffID );
					if( NULL == pLinkBuffProp )
					{
						D_ERROR("查找ID:%d的复合属性时, 找不到ID为%d的BUFFPROP\n", mainBuffID, linkBuffID );
						return false;
					}
					pMainBuffProp->vecBuffProp.push_back( pLinkBuffProp );
				}
				token = strtok( NULL, "," );
			}
		}
	}
	return true;
}


template <>
bool MuxSkillConfig<CMuxSkillProp>::LoadSkillPropT( const char* szFilePath )
{
	CXmlManager xmlManager;
	if(!xmlManager.Load(szFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szFilePath );
		return false;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szFilePath);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements( pRoot, childElements );
	CElement* tmpEle = NULL;
	if( childElements.empty() )
	{
		D_WARNING("CMuxSkillProp属性为空\n");
		return true;
	}

	unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = childElements.begin();
		tmpiter != childElements.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( NULL == tmpEle )
			continue;

		int curpos = (int)(tmpiter - childElements.begin());
		CMuxSkillProp* pMuxSkillProp = NEW CMuxSkillProp;
		bool isSuccess = false;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			do{
					if ( !IsXMLValValid( tmpEle, "nID", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_skillID = (unsigned int)(atoi)( tmpEle->GetAttr("nID") );

					if ( !IsXMLValValid( tmpEle, "nType", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_skillType = (unsigned int)(atoi)( tmpEle->GetAttr("nType") );
			
					if ( !IsXMLValValid( tmpEle, "nUpgradeLevel", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_upgradeLevel = (unsigned int)(atoi)( tmpEle->GetAttr("nUpgradeLevel") );

	   				if ( !IsXMLValValid( tmpEle, "nTarget", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_target = (unsigned int)(atoi)( tmpEle->GetAttr("nTarget") );

					if ( !IsXMLValValid( tmpEle, "nShape", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_shape = (unsigned int)(atoi)( tmpEle->GetAttr("nShape") );

					if ( !IsXMLValValid( tmpEle, "nRange", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_range = (unsigned int)(atoi)( tmpEle->GetAttr("nRange") );

					if ( !IsXMLValValid( tmpEle, "nArea", curpos ) )
					{
						D_ERROR( "LoadSkillPropT, 技能%d的nArea属性空\n", pMuxSkillProp->m_skillID  );
					}
					else
					{
						pMuxSkillProp->m_area = (unsigned int)(atoi)( tmpEle->GetAttr("nArea") );
					}

					if ( !IsXMLValValid( tmpEle, "KickBackRange", curpos ) )
					{
						D_ERROR( "LoadSkillPropT, 技能%d的KickBackRange属性空\n", pMuxSkillProp->m_skillID );
					}
					else
					{
						pMuxSkillProp->m_kickBackRange = (unsigned int)((atof)( tmpEle->GetAttr("KickBackRange") ) * 10);
					}

					if ( !IsXMLValValid( tmpEle, "DeadKickBack", curpos ) )
					{
						D_ERROR( "LoadSkillPropT, 技能%d的DeadKickBack属性空\n", pMuxSkillProp->m_skillID );
					}
					else
					{
						pMuxSkillProp->m_deadKickBack = (bool)(atoi(tmpEle->GetAttr("DeadKickBack")) );
					}

					//if ( !IsXMLValValid( tmpEle, "nUpgradePoint", curpos ) )
					//{
					//	break;
					//}		
					pMuxSkillProp->m_upgradePoint = 0/*(unsigned int)(atoi)( tmpEle->GetAttr("nUpgradePoint") )*/;

					if ( !IsXMLValValid( tmpEle, "PublicCDType", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_publicCDType = (unsigned int)(atoi)( tmpEle->GetAttr("PublicCDType") );

					//if ( !IsXMLValValid( tmpEle, "SpLevel", curpos ) )
					//{
					//	break;
					//}		
					pMuxSkillProp->m_spLevel = 0/*(unsigned int)(atoi)( tmpEle->GetAttr("SpLevel") )*/;

					//if ( !IsXMLValValid( tmpEle, "SkillDegree", curpos ) )
					//{
					//	break;
					//}		
					pMuxSkillProp->m_skillDegree = 0/*(unsigned int)(atoi)( tmpEle->GetAttr("SkillDegree") )*/;

					//if ( !IsXMLValValid( tmpEle, "nUllageSp", curpos ) )
					//{
					//	break;
					//}		
					pMuxSkillProp->m_nUllageSp = 0/*(unsigned int)(atoi)( tmpEle->GetAttr("nUllageSp") )*/;

					if ( !IsXMLValValid( tmpEle, "nUllageMpPer", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_nUllageMpPer = (float)(atoi)( tmpEle->GetAttr("nUllageMpPer") );

					if ( !IsXMLValValid( tmpEle, "nUllageMp", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_nUllageMp = (unsigned int)(atoi)( tmpEle->GetAttr("nUllageMp") );

					if ( !IsXMLValValid( tmpEle, "nUllageItem", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_nUllageItem = (unsigned int)(atoi)( tmpEle->GetAttr("nUllageItem") );

					if ( !IsXMLValValid( tmpEle, "nItemNum", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_nItemNum = (unsigned int)(atoi)( tmpEle->GetAttr("nItemNum") );

					if ( !IsXMLValValid( tmpEle, "nCurseTime", curpos ) )
					{
						break;
					}
					pMuxSkillProp->m_nCurseTime = (unsigned int)(atoi)( tmpEle->GetAttr("nCurseTime") );
					
					
					if ( !IsXMLValValid( tmpEle, "nCoolDown", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_nCoolDown = (atoi)( tmpEle->GetAttr("nCoolDown") );

					if ( !IsXMLValValid( tmpEle, "nClass", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_nClass = (atoi)( tmpEle->GetAttr("nClass") );

					//if ( !IsXMLValValid( tmpEle, "nExp", curpos ) )
					//{
					//	break;
					//}		
					pMuxSkillProp->m_nAddExp = 0/*( (atoi)( tmpEle->GetAttr("nExp") ) )*/;
			
	   				if ( !IsXMLValValid( tmpEle, "nNextSkillID", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_nNextSkillID = (atoi)( tmpEle->GetAttr("nNextSkillID"));

					if ( !IsXMLValValid( tmpEle, "nLevel", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_level = ( (atoi)( tmpEle->GetAttr("nLevel") ) );

					if ( !IsXMLValValid( tmpEle, "nAttDistMin", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_nAttDistMin = (atoi)( tmpEle->GetAttr("nAttDistMin") );

					if ( !IsXMLValValid( tmpEle, "nAttDistMax", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_nAttDistMax = (atoi)( tmpEle->GetAttr("nAttDistMax") );

					//if ( !IsXMLValValid( tmpEle, "NpcRank", curpos ) )
					//{
					//	break;
					//}		
					pMuxSkillProp->m_NpcRank = 0/*(atoi)( tmpEle->GetAttr("NpcRank") )*/;

					if ( !IsXMLValValid( tmpEle, "MaxTargetLevel", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_MaxTargetLevel = (atoi)( tmpEle->GetAttr("MaxTargetLevel") );

					if ( !IsXMLValValid( tmpEle, "MaxTargetLevel", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_MaxTargetLevel = (atoi)( tmpEle->GetAttr("MaxTargetLevel") );

					if ( !IsXMLValValid( tmpEle, "MinTargetLevel", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_MinTargetLevel = (atoi)( tmpEle->GetAttr("MinTargetLevel") );

					if ( !IsXMLValValid( tmpEle, "BasicSkill", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_BasicSkill = (atoi)( tmpEle->GetAttr("BasicSkill") );

					if ( !IsXMLValValid( tmpEle, "BasicSkill", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_BasicSkill = (atoi)( tmpEle->GetAttr("BasicSkill") );

					if ( !IsXMLValValid( tmpEle, "GoodorBad", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_isGoodSkill = ( (atoi)( tmpEle->GetAttr("GoodorBad") ) == 1 );

					if ( !IsXMLValValid( tmpEle, "nUpgradeExp", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_upgradeExp = (atoi)( tmpEle->GetAttr("nUpgradeExp") );
					}

					if ( !IsXMLValValid( tmpEle, "nUpgradeMoney", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_upgradeMoney = (atoi)( tmpEle->GetAttr("nUpgradeMoney") );
					}

					if ( !IsXMLValValid( tmpEle, "nUpgradeStr", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_upgradeStr = (atoi)( tmpEle->GetAttr("nUpgradeStr") );
					}

					if ( !IsXMLValValid( tmpEle, "nUpgradeVit", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_upgradeVit = (atoi)( tmpEle->GetAttr("nUpgradeVit") );
					}

					if ( !IsXMLValValid( tmpEle, "nUpgradeAgi", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_upgradeAgi = (atoi)( tmpEle->GetAttr("nUpgradeAgi") );
					}

					if ( !IsXMLValValid( tmpEle, "nUpgradeSpi", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_upgradeSpi = (atoi)( tmpEle->GetAttr("nUpgradeSpi") );
					}

					if ( !IsXMLValValid( tmpEle, "nUpgradeInt", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_upgradeInt = (atoi)( tmpEle->GetAttr("nUpgradeInt") );
					}

					if ( !IsXMLValValid( tmpEle, "nSubExp", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_subExp = (atoi)( tmpEle->GetAttr("nSubExp") );
					}

					if ( !IsXMLValValid( tmpEle, "nSubMoney", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_subMoney = (atoi)( tmpEle->GetAttr("nSubMoney") );
					}

					if ( !IsXMLValValid( tmpEle, "WeaponSkill", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_weaponSkill = (atoi)( tmpEle->GetAttr("WeaponSkill") );
					}

					if ( !IsXMLValValid( tmpEle, "PlayerDam", curpos ) )
					{
						break;
					}	

					if ( !IsXMLValValid( tmpEle, "CastTime", curpos ) )
					{

					}
					else
					{
						pMuxSkillProp->m_castTime = (atoi)( tmpEle->GetAttr("CastTime") );
					}

					unsigned int playerDamID = (unsigned int)(atoi)( tmpEle->GetAttr("PlayerDam") );
					if( playerDamID != 0 )
					{
						MuxPlayerDamageProp* pPlayerDamProp = PlayerDamPropSingleton::instance()->FindTSkillProp( playerDamID );
						if( NULL == pPlayerDamProp )
						{
							D_ERROR("母表中找不到MuxPlayerDamageProp的id:%d\n", playerDamID );
							//return false;
						}
						pMuxSkillProp->m_pDamageProp = pPlayerDamProp;
					}

					if ( !IsXMLValValid( tmpEle, "MonsterDam", curpos ) )
					{
						break;
					}		
					unsigned int monsterDamID = (unsigned int)(atoi)( tmpEle->GetAttr("MonsterDam") );
					if( monsterDamID != 0 )
					{
						MuxNpcDamageProp* pMonsterDamProp =  NpcDamPropSingleton::instance()->FindTSkillProp( monsterDamID );
						if( NULL == pMonsterDamProp )
						{
							D_ERROR("母表中找不到MuxNpcDamageProp的id:%d\n", monsterDamID );
							//return false;
						}
						pMuxSkillProp->m_pNpcDamageProp = pMonsterDamProp;
					}

					if ( !IsXMLValValid( tmpEle, "Heal", curpos ) )
					{
						break;
					}		
					unsigned int healID = (unsigned int)(atoi)( tmpEle->GetAttr("Heal") );
					if( healID != 0 )
					{
						MuxItemSkillProp* pItemSkillProp =  ItemSkillPropSingleton::instance()->FindTSkillProp( healID );
						if( NULL == pItemSkillProp )
						{
							D_ERROR("母表中找不到MuxItemSkillProp的id:%d\n", healID );
							//return false;
						}
						pMuxSkillProp->m_pMuxItemSkillProp = pItemSkillProp;
					}

					if ( !IsXMLValValid( tmpEle, "Split", curpos ) )
					{
						break;
					}		
					unsigned int spiltID = (unsigned int)(atoi)( tmpEle->GetAttr("Split") );
					if( spiltID != 0 )
					{
						MuxSpiltProp* pSpiltProp =  SpiltPropSingleton::instance()->FindTSkillProp( spiltID );
						if( NULL == pSpiltProp )
						{
							D_ERROR("母表中找不到MuxSpiltProp的id:%d\n", spiltID );
							//return false;
						}
						pMuxSkillProp->m_pMuxSpiltProp = pSpiltProp;
					}

					if ( !IsXMLValValid( tmpEle, "Buff", curpos ) )
					{
						break;
					}		
					unsigned int buffID = (unsigned int)(atoi)( tmpEle->GetAttr("Buff"));
					if( buffID != 0 )
					{
						MuxBuffProp* pMuxBuffProp =  BuffPropSingleton::instance()->FindTSkillProp( buffID );
						if( NULL == pMuxBuffProp )
						{
							D_ERROR("母表中找不到MuxBuffProp的id:%d\n", buffID );
							//return false;
						}
						pMuxSkillProp->m_pBuffProp = pMuxBuffProp;
					}

					if ( !IsXMLValValid( tmpEle, "Relieve", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_relieveID = (unsigned int)atoi(tmpEle->GetAttr("Relieve"));

					if ( !IsXMLValValid( tmpEle, "NormalAttackSkill", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_isNormalSkill = ( (atoi)(tmpEle->GetAttr("NormalAttackSkill")) != 0 );

					if ( !IsXMLValValid( tmpEle, "GuildSkill", curpos ) )
					{
						break;
					}		
					pMuxSkillProp->m_isGuildSkill = ( (atoi)(tmpEle->GetAttr("GuildSkill")) != 0 );

					
					if( m_mapMuxSkillPropT.find(pMuxSkillProp->m_skillID) != m_mapMuxSkillPropT.end() )
					{
						D_ERROR("重复的Skill ID:%d\n", pMuxSkillProp->m_skillID );
						break;
					}					
					isSuccess = true;
				}while( false );

			if(!isSuccess )
			{
				delete pMuxSkillProp; pMuxSkillProp = NULL;
				continue;
			}

			m_mapMuxSkillPropT.insert( make_pair( pMuxSkillProp->m_skillID, pMuxSkillProp ) );
		}
	}
	return true;
}

template <>
int MuxSkillConfig<CMuxSkillProp>::FindSpSkill( unsigned short spLevel, unsigned int job, vector<CMuxSkillProp*>& vecSkillProp )
{
	int count = 0;
	CMuxSkillProp* pSkillProp = NULL;
	for( map<unsigned int, CMuxSkillProp*>::iterator iter = m_mapMuxSkillPropT.begin(); iter != m_mapMuxSkillPropT.end(); iter++ )
	{
		pSkillProp = iter->second;
		if( NULL == pSkillProp )
			continue;
		
		if( spLevel == pSkillProp->m_spLevel && pSkillProp->IsClassSkill( job ) && pSkillProp->m_level == 1 )
		{
			vecSkillProp.push_back( pSkillProp );
			count++;
		}
	}
	return count;
}

