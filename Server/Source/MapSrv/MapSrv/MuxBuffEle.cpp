﻿#include "MuxBuffEle.h"
#include "Player/Player.h"
#include "monster/monster.h"
#include "MuxMap/muxmapbase.h"

#define FIND_CREATE_PLAYER\
	CPlayer* pPlayer = NULL;\
	CGateSrv* pGateSrv = CManG_MProc::FindProc( m_createID.wGID );\
	if( pGateSrv )\
	{\
		pPlayer = pGateSrv->FindPlayer( m_createID.dwPID );\
	}\


CMuxBuffEle::CMuxBuffEle()
{
	m_bValid = false;						//该BUFF是否生效
	m_buffLevel = 0;				//关于buff的等级
	m_pBuffProp = NULL;
	m_createID.dwPID = 0; m_createID.wGID = 0;				//生成者的ID 
	m_leftTime = ACE_Time_Value::zero;			//BUFF剩余的时间
	m_lastUpdateTime = ACE_Time_Value::zero;	//上次更新的时间
	m_hpmpUpdateTimer = ACE_Time_Value::zero;	//用于dot，hot的timer
	m_bEternal = false;					//是否是永久buff 永久buff剩
}

CMuxBuffEle::~CMuxBuffEle()
{
}


bool CMuxBuffEle::OnPlayerBuffUpdate( CPlayer* pPlayer, unsigned int buffIndex )
{
	if( NULL == pPlayer )
		return false;

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	
	if( IsHpMpBuff() )
	{
		unsigned int hpmpInterval = GetHpMpUpdateInterval();
		if( currentTime >= m_hpmpUpdateTimer )
		{
			OnPlayerHpMpUpdate( pPlayer, buffIndex );
			m_hpmpUpdateTimer += hpmpInterval;
		}
	}

	//非永久buff需要减时间
	if( !m_bEternal )
	{
		ACE_Time_Value pastTime = currentTime - m_lastUpdateTime;
		if( pastTime >= m_leftTime )
		{
			return false;
		}

		m_leftTime -= pastTime;
		m_lastUpdateTime = currentTime;
	}
	return true;
}


bool CMuxBuffEle::IsHpMpBuff()
{
	return m_hpmpUpdateInfo.isHpMpBuff;
}


unsigned int CMuxBuffEle::GetHpMpUpdateInterval()
{
	return m_hpmpUpdateInfo.updateInterval;
}


bool CMuxBuffEle::BuffInit( MuxBuffProp* pMuxBuffProp, unsigned int buffTime, const PlayerID& createID )
{
	if( NULL == pMuxBuffProp )
		return false;

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();

	m_pBuffProp = pMuxBuffProp;
	m_leftTime = buffTime;
	m_buffLevel = pMuxBuffProp->BuffLevel;
	m_lastUpdateTime = ACE_OS::gettimeofday();
	m_buffLevel = 1;
	m_createID = createID;

	//永久buff的检测
	if( buffTime == ETERNAL_BUFF_TIME )
	{
		m_bEternal = true;	
	}
	InitHpMpUpdateInfo( pMuxBuffProp, currentTime );	
	return true;
}


void CMuxBuffEle::InitHpMpUpdateInfo( MuxBuffProp* pMuxBuffProp, const ACE_Time_Value& curTime )
{
	if( NULL == pMuxBuffProp )
		return;

	if( pMuxBuffProp->AssistType == BUFF_SP_COMPLEX )
	{
		for( unsigned int i=0; i<pMuxBuffProp->vecBuffProp.size(); ++i )
		{
			SingleHpMpBuffCheck( pMuxBuffProp->vecBuffProp[i] );
		}
	}else{
		SingleHpMpBuffCheck( pMuxBuffProp );
	}
	m_hpmpUpdateInfo.updateInterval = pMuxBuffProp->nActionSpace;
	m_hpmpUpdateTimer = curTime;
	m_hpmpUpdateTimer += m_hpmpUpdateInfo.updateInterval;
	D_INFO("下次hpmpUpdate时间%u\n", m_hpmpUpdateTimer.msec() );
}



void CMuxBuffEle::OnPlayerHpMpUpdate( CPlayer* pTargetPlayer, unsigned int buffIndex )
{
	if( NULL == pTargetPlayer )
		return;

	unsigned int maxHp = pTargetPlayer->GetMaxHP();
	unsigned int maxMp = pTargetPlayer->GetMaxMP();

	int finalHp = (int)( m_hpmpUpdateInfo.perHp * maxHp );
	finalHp += m_hpmpUpdateInfo.hpUpdateVal;

	int finalMp = (int)( m_hpmpUpdateInfo.perMp * maxMp );
	finalMp += m_hpmpUpdateInfo.mpUpdateVal;
	FIND_CREATE_PLAYER;

	EBattleFlag battleFlag = HIT;
	if( finalHp >= 0 )
	{
		finalHp = pTargetPlayer->AddPlayerHp( finalHp );
	}else{
		pTargetPlayer->SubPlayerHp( (unsigned int)(finalHp*-1), pPlayer, m_createID );
	}
	
	if( finalMp >= 0 )
	{
		finalMp = pTargetPlayer->AddPlayerMp( finalMp );
	}else{
		pTargetPlayer->SubPlayerMp( (unsigned int)(finalMp * -1), 0 );
		//pTargetPlayer->SubMP( (unsigned int)(finalMp * -1) );
	}

	if( !IsRestBuff() )
	{
		//珍爱生命，判断里不要取反
		//if( !(finalHp > 0 && pTargetPlayer->GetMaxHP() == pTargetPlayer->GetCurrentHP()) )
		if ( finalHp != 0 || finalMp != 0 )
		{
			MGBuffHpMpUpdate hpmpUpdate;
			hpmpUpdate.hpmpUpdate.hp = finalHp;
			hpmpUpdate.hpmpUpdate.mp = finalMp;
			hpmpUpdate.hpmpUpdate.tgtHp = pTargetPlayer->GetCurrentHP();
			hpmpUpdate.hpmpUpdate.tgtMp = pTargetPlayer->GetCurrentMP();
			hpmpUpdate.hpmpUpdate.tgtCharacterID = pTargetPlayer->GetFullID();
			hpmpUpdate.hpmpUpdate.createID = m_createID;
			hpmpUpdate.hpmpUpdate.buffID = GetBuffID();
			hpmpUpdate.hpmpUpdate.buffIndex = buffIndex;
			hpmpUpdate.hpmpUpdate.battleFlag = battleFlag;
			
			CMuxMap* pMap = pTargetPlayer->GetCurMap();
			if( NULL != pMap )
			{
				pMap->NotifySurrounding<MGBuffHpMpUpdate>(&hpmpUpdate, pTargetPlayer->GetPosX(), pTargetPlayer->GetPosY() );
			}
		}
	}else{
		pTargetPlayer->NotifyHpChange();
		pTargetPlayer->NotifyMpChange();
	}
	D_INFO("更新buff的血消息 PlayerPID:%d BuffIndex:%d\n", pTargetPlayer->GetPID(), buffIndex );
}

void CMuxBuffEle::OnMonsterHpMpUpdate( CMonster* pTargetMonster, unsigned int buffIndex )
{
	if( NULL == pTargetMonster )
		return;

	unsigned int maxHp = pTargetMonster->GetMaxHp();

	int finalHp = (int)( m_hpmpUpdateInfo.perHp * maxHp );
	finalHp += m_hpmpUpdateInfo.hpUpdateVal;

	FIND_CREATE_PLAYER;

	EBattleFlag battleFlag = HIT;
	if( finalHp >= 0 )
	{
		pTargetMonster->AddHp( finalHp );
	}else{
		pTargetMonster->SubMonsterHp( (unsigned int)(finalHp*-1), pPlayer, true/*debuff引起的伤害*/, 0 );
	}
	
	MGBuffHpMpUpdate hpmpUpdate;
	hpmpUpdate.hpmpUpdate.hp = finalHp;
	hpmpUpdate.hpmpUpdate.mp = 0;
	hpmpUpdate.hpmpUpdate.tgtHp = pTargetMonster->GetHp();
	hpmpUpdate.hpmpUpdate.tgtMp = 0;
	hpmpUpdate.hpmpUpdate.tgtCharacterID = pTargetMonster->GetCreatureID();
	hpmpUpdate.hpmpUpdate.createID = m_createID;
	hpmpUpdate.hpmpUpdate.buffID = GetBuffID();
	hpmpUpdate.hpmpUpdate.buffIndex = buffIndex;
	hpmpUpdate.hpmpUpdate.battleFlag = battleFlag;
	
	CMuxMap* pMap = pTargetMonster->GetMap();
	if( NULL != pMap )
	{
		pMap->NotifySurrounding<MGBuffHpMpUpdate>(&hpmpUpdate, pTargetMonster->GetPosX(), pTargetMonster->GetPosY() );
	}
}


void CMuxBuffEle::SingleHpMpBuffCheck( MuxBuffProp* pBuffProp )
{
	if( NULL == pBuffProp )
		return;

	switch( pBuffProp->AssistType )
	{
	case BUFF_HP_HOT:
		m_hpmpUpdateInfo.isHpMpBuff = true;
		m_hpmpUpdateInfo.hpUpdateVal += pBuffProp->Value;
		break;

	case BUFF_HP_DOT:
		m_hpmpUpdateInfo.isHpMpBuff = true;
		m_hpmpUpdateInfo.hpUpdateVal -= pBuffProp->Value;
		break;

	case MAG_HOT:
		m_hpmpUpdateInfo.isHpMpBuff = true;
		m_hpmpUpdateInfo.mpUpdateVal += pBuffProp->Value;
		break;

	case MAG_PER_HOT:
		m_hpmpUpdateInfo.isHpMpBuff = true;
		m_hpmpUpdateInfo.perMp += ( pBuffProp->Value / 100.f );
		break;

	case BUFF_PERCENT_HOT:
		m_hpmpUpdateInfo.isHpMpBuff = true;
		m_hpmpUpdateInfo.perHp += pBuffProp->Value / 100.f;
		break;

	default:
		break;
	}
}


bool CMuxBuffEle::OnPlayerBuffStart( CPlayer* pPlayer, unsigned int buffIndex,  MuxBuffProp* pMuxBuffProp, unsigned int buffTime, const PlayerID& createID, bool isNotiPlayer )
{
	if( !BuffInit( pMuxBuffProp, buffTime, createID ) )
		return false;

	if( NULL == pPlayer || NULL == m_pBuffProp)
		return false;

	pPlayer->AddMuxBuffEffect( m_pBuffProp );
	if( IsRestBuff() )
	{
		//设置玩家休息状态
		pPlayer->SetRestFlag( true );	
	}

	if( isNotiPlayer )
	{
		MGBuffStart bs;
		bs.buffStart.buffID = m_pBuffProp->mID;
		bs.buffStart.buffLevel = m_buffLevel;
		bs.buffStart.buffIndex = buffIndex;
		bs.buffStart.buffTime = (unsigned int)m_leftTime.sec();
		bs.buffStart.skillID = 0;
		bs.buffStart.tgtCharacterID = pPlayer->GetFullID();
		bs.buffStart.useCharacterID = m_createID;
		CMuxMap* pMap = pPlayer->GetCurMap();
		if( pMap )
		{
			pMap->NotifySurrounding<MGBuffStart>( &bs, pPlayer->GetPosX(), pPlayer->GetPosY() );
		}
	}
	D_INFO("Player PID:%d BuffIndex:%d的buff开始\n", pPlayer->GetPID(), buffIndex );
	return true;
}


void CMuxBuffEle::OnPlayerBuffEnd( CPlayer* pPlayer, unsigned int buffIndex, bool isNotiPlayer )
{
	if( NULL == pPlayer || NULL == m_pBuffProp)
		return;

	pPlayer->DelMuxBuffEffect( m_pBuffProp, m_buffLevel );
	if( IsRestBuff() )
	{
		pPlayer->SetRestFlag( false );
	}

	m_bValid = false;
	m_hpmpUpdateInfo.Reset();	//重置信息
	m_bEternal = false;		//永久buff要设回原来的值

	//WeakBuffCheck
	if( GetBuffID() == WEAK_BUFF_ID )
	{
		pPlayer->SetAlive();
	}

	if( isNotiPlayer )
	{
		MGBuffEnd be;
		be.buffEnd.buffID = m_pBuffProp->mID;
		be.buffEnd.tgtCharacterID = pPlayer->GetFullID();
		be.buffEnd.buffIndex = buffIndex;
		CMuxMap* pMap = pPlayer->GetCurMap();
		if( pMap )
		{
			pMap->NotifySurrounding<MGBuffEnd>( &be, pPlayer->GetPosX(), pPlayer->GetPosY() );
		}
	}
	D_INFO("Player PID:%d BuffIndex:%d的buff结束\n", pPlayer->GetPID(), buffIndex );
}


void CMuxBuffEle::OnPlayerBuffChangeStatus( CPlayer* pPlayer, unsigned int buffIndex, MuxBuffProp* pBuffProp,  unsigned int buffLevel, bool isBuffUpgrade, const PlayerID& createID )	//改变buff的时间 等级会调用
{
	if( NULL == pPlayer || NULL == pBuffProp )
		return;

	
	if( isBuffUpgrade )
	{
		pPlayer->AddMuxBuffEffect( pBuffProp );
	}else
	{
		//替换流程,结束原来的
		OnPlayerBuffEnd( pPlayer, buffIndex, false );
		OnPlayerBuffStart( pPlayer, buffIndex, pBuffProp, pBuffProp->nPeriodOfTime, createID, false );
	}

	m_buffLevel = buffLevel;

	MGBuffStatusUpdate update;
	update.buffChange.tgtCharacterID = pPlayer->GetFullID();
	update.buffChange.buffTime = pBuffProp->nPeriodOfTime;
	update.buffChange.buffID = pBuffProp->mID;
	update.buffChange.buffIndex = buffIndex;
	update.buffChange.buffLevel = buffLevel;
	CMuxMap* pMap = pPlayer->GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGBuffStatusUpdate>( &update, pPlayer->GetPosX(), pPlayer->GetPosY() );
	}
}


bool CMuxBuffEle::OnMonsterBuffUpdate( CMonster* pMonster, unsigned int buffIndex )
{
	if( NULL == pMonster )
		return false;

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	
	if( IsHpMpBuff() )
	{
		unsigned int hpmpInterval = GetHpMpUpdateInterval();
		if( currentTime >= m_hpmpUpdateTimer )
		{
			OnMonsterHpMpUpdate( pMonster, buffIndex );
			m_hpmpUpdateTimer = currentTime;
			m_hpmpUpdateTimer += hpmpInterval;
		}
	}

	//非永久buff需要减时间
	if( !m_bEternal )
	{
		ACE_Time_Value pastTime = currentTime - m_lastUpdateTime;
		if( pastTime >= m_leftTime )
		{
			return false;
		}

		m_leftTime -= pastTime;
		m_lastUpdateTime = currentTime;
	}
	return true;
}
bool CMuxBuffEle::OnMonsterBuffStart( CMonster* pMonster, unsigned int buffIndex,  MuxBuffProp* pMuxBuffProp, unsigned int buffTime, const PlayerID& createID, bool isNotiPlayer )
{
	if( !BuffInit( pMuxBuffProp, buffTime, createID ) )
		return false;

	if( NULL == pMonster || NULL == m_pBuffProp)
		return false;

	pMonster->AddMuxBuffEffect( m_pBuffProp );

	//替换的时候不需要通知
	if( isNotiPlayer )
	{
		MGBuffStart bs;
		bs.buffStart.buffID = m_pBuffProp->mID;
		bs.buffStart.buffLevel = m_buffLevel;
		bs.buffStart.buffIndex = buffIndex;
		bs.buffStart.buffTime = (unsigned int)m_leftTime.sec();
		bs.buffStart.skillID = 0;
		bs.buffStart.tgtCharacterID = pMonster->GetCreatureID();
		bs.buffStart.useCharacterID = m_createID;
		CMuxMap* pMap = pMonster->GetMap();
		if( pMap )
		{
			pMap->NotifySurrounding<MGBuffStart>( &bs, pMonster->GetPosX(), pMonster->GetPosY() );
		}
	}
	D_DEBUG("怪物buff开始 ID:%d 时间:%u \n", m_pBuffProp->mID, buffTime );
	return true;
}


void CMuxBuffEle::OnMonsterBuffEnd( CMonster* pMonster, unsigned int buffIndex, bool isNotiPlayer )
{
	if( NULL == pMonster || NULL == m_pBuffProp)
		return;

	pMonster->DelMuxBuffEffect( m_pBuffProp, m_buffLevel );
	m_bValid = false;
	m_hpmpUpdateInfo.Reset();	//重置信息

	if( isNotiPlayer )
	{
		MGBuffEnd be;
		be.buffEnd.buffID = m_pBuffProp->mID;
		be.buffEnd.tgtCharacterID = pMonster->GetCreatureID();
		be.buffEnd.buffIndex = buffIndex;
		CMuxMap* pMap = pMonster->GetMap();
		if( pMap )
		{
			pMap->NotifySurrounding<MGBuffEnd>( &be, pMonster->GetPosX(), pMonster->GetPosY() );
		}
	}
	D_DEBUG("怪物buff结束 ID:%d \n", m_pBuffProp->mID );
}


void CMuxBuffEle::OnMonsterBuffChangeStatus( CMonster* pMonster, unsigned int buffIndex, MuxBuffProp* pBuffProp, unsigned int buffLevel, bool isBuffUpgrade, const PlayerID& createID )
{
	if( NULL == pMonster || NULL == pBuffProp )
		return;

	if( isBuffUpgrade )
	{
		pMonster->AddMuxBuffEffect( pBuffProp );
	}else
	{
		//删除原来的影响给予新的影响
		OnMonsterBuffEnd( pMonster, buffIndex, false );
		OnMonsterBuffStart( pMonster, buffIndex, pBuffProp, pBuffProp->nPeriodOfTime, createID, false );
	}

	m_buffLevel = buffLevel;

	MGBuffStatusUpdate update;
	update.buffChange.tgtCharacterID = pMonster->GetCreatureID();
	update.buffChange.buffTime = pBuffProp->nPeriodOfTime;
	update.buffChange.buffID = pBuffProp->mID;
	update.buffChange.buffIndex = buffIndex;
	update.buffChange.buffLevel = buffLevel;
	CMuxMap* pMap = pMonster->GetMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGBuffStatusUpdate>( &update, pMonster->GetPosX(), pMonster->GetPosY() );
	}
}

