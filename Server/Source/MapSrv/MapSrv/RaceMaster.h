﻿/********************************************************************
	created:	2009/11/05
	created:	5:11:2009   13:56
	file:		RaceMaster.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef RACE_MASTER_H
#define RACE_MASTER_H

#include "../../Base/PkgProc/CliProtocol_T.h"
#include <ctime>
#include "Utility.h"

class CPlayer;
class CRaceManager;

class CRaceMaster
{
	friend class CRaceManager;

public:

	CRaceMaster():m_masterUID(0),m_todayNoticeCount(5),m_lastupdatetime(0),m_DeclareWarInfo(0),m_pRaceMaster(NULL),m_daysalary(100000)
	{
		StructMemSet( m_masterNoticeArr, '\0', sizeof(m_masterNoticeArr) );
	}

public:
	bool SetRaceMasterPlayer( CPlayer* pPlayer );

	void RaceMasterLeave(){ m_pRaceMaster = NULL; }

	void SetRaceMasterInfo( unsigned int masterUID, unsigned int noticeCount,  unsigned int lastupdatetime , char declareWarInfo )
	{
		m_masterUID = masterUID;
		m_todayNoticeCount = noticeCount;
		m_lastupdatetime = lastupdatetime;
		m_DeclareWarInfo = declareWarInfo;
	}

	unsigned int GetMasterUID() { return m_masterUID; }

	char GetDeclareWarInfo() { return m_DeclareWarInfo; }

	char* GetMasterNoticeMsgArr() { return m_masterNoticeArr; }

    bool SetMasterNoticeMsgArr( const char* szNewNotice )
	{
		if ( NULL == szNewNotice )
		{
			return false;
		}

		StructMemSet( m_masterNoticeArr, 0, sizeof(m_masterNoticeArr) );
		SafeStrCpy( m_masterNoticeArr, szNewNotice );
		return (strlen(m_masterNoticeArr) > 0);

	}

	unsigned GetTodayNoticeCount() { return m_todayNoticeCount; }

	void DecTodayNoticeMsgCount() { m_todayNoticeCount--; }

	const char* GetRaceMasterName();
	
	bool IsReadyDeclareWar( unsigned short racetype )
	{
		return ( m_DeclareWarInfo & 1<< racetype ) != 0;
	}

	bool IsReadyBeDeclareWar( unsigned short racetype )
	{
		return ( ( m_DeclareWarInfo>> 3 ) & ( 1<<racetype ) ) != 0;
	}

	void MaskDeclareWarSign( unsigned short racetype )
	{
		m_DeclareWarInfo |= 1 << racetype;
	}

	void MaskBeDeclareWarSign( unsigned short racetype )
	{
		m_DeclareWarInfo |= ( ( 1<<racetype ) << 3 );
	}
	
	CPlayer* GetRaceMasterPlayer() { return m_pRaceMaster; };

	bool IsGotSalary() { return m_daysalary == 0 ;}

	unsigned int GetLastUpdateTime() { return m_lastupdatetime; }

	void RefreshRaceMasterInfo() 
	{ 
		m_todayNoticeCount =  5;
		m_lastupdatetime = (unsigned int)time(NULL);
		m_daysalary = 100000;
	}

	void ResetDeclareWar() { m_DeclareWarInfo = 0; }

	//族长的公告是否为空
	bool IsMsgEmpty() { return m_masterNoticeArr[0] == '\0'; }

	//玩家领取每天的工资
	int MasterGetSalary();

	//设置族长的工资
	void SetRaceMasterSalary( int money ) { m_daysalary = money; }

	//族长状态变化
	void NoticeRaceMasterStateChange( bool isMaster );

private://组长每天的通知信息
	unsigned int m_masterUID;//族长唯一编号
	unsigned int m_todayNoticeCount;//每天能通知的次数
	int m_daysalary;//玩家的工资
	unsigned int m_lastupdatetime;//上次更新的时间,用于记录每天的凌晨的刷新
	char  m_DeclareWarInfo;//宣战信息( 0 000 000 0)
	char  m_masterNoticeArr[MAX_RACE_NOTICE_LEN];//族长的通知信息

private:
	CPlayer* m_pRaceMaster;
};


#endif
