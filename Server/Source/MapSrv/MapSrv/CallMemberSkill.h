﻿#ifndef CALL_MEMBER_SKILL_H_
#define CALL_MEMBER_SKILL_H_

#include "Utility.h"
#include "../../Base/PkgProc/CliProtocol.h"
#include "../../Base/aceall.h"
#include <set>
#include <map>
using namespace std;

#define CALL_OVER_TIME	10000		//召唤术的过期时间(时间稍微放宽点)
class CallMemberManager;
class CPlayer;
class CallMember
{
	friend class CallMemberManager;
public:
	void		AddCalledMember(const PlayerID& playerID );
	void		SetMapInfo(TYPE_POS posX, TYPE_POS posY, unsigned short mapID,unsigned int teamID);
	void		OnReplyCall(CPlayer * pPlayer, int isAgree);
	bool		IsMemberEmpty(){return m_calledMember.empty();}
	unsigned int GetCallMemberID(){return m_callMemberID;}
private:
	CallMember();
	~CallMember(){}
	void		SetID(unsigned int id);
	void		SetTime(unsigned int time);
private:
	set<PlayerID>	m_calledMember;	//被召唤的玩家
	TYPE_POS		m_posX;			//召唤集结地x坐标
	TYPE_POS		m_posY;			//召唤集结地y坐标
	unsigned short	m_mapID;		//召唤集结地的mapID
	unsigned int	m_teamID;		//队伍id
	unsigned int	m_callMemberID;
	unsigned int	m_time;			//召唤时的时间
};

class CallMemberManager
{
	friend class ACE_Singleton<CallMemberManager, ACE_Null_Mutex>;
public:
	bool		 StartCallMember(CPlayer* pPlayer);
	void		 OnReplyCallMember(CPlayer * pPlayer,int isAgree, unsigned int callMemberID);
private:
	CallMember * GetCallMember(unsigned int id);
	CallMember * CreateCallMember();
	void		 AddCallMember(CallMember* callMember);
	void		 DelCallMember(unsigned int id);
private:
	map<unsigned int,CallMember*>	m_callMemberMap;
	map<pair<unsigned int,unsigned int>,unsigned int>  m_callMemberIndex;//map<pair<mapID,teamID>,callMemberID>
	unsigned int			m_count;
private:
	CallMemberManager():m_count(0){}
	~CallMemberManager(){
		map<unsigned int, CallMember*>::iterator it = m_callMemberMap.begin();
		for (; it!=m_callMemberMap.end(); ++it)
		{
			delete it->second;
			it->second = NULL;
		}
		m_callMemberMap.clear();
		m_callMemberIndex.clear();
	}
};

typedef ACE_Singleton<CallMemberManager, ACE_Null_Mutex> CallMemberManagerSingle;

#endif
