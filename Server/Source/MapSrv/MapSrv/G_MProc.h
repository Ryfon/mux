﻿/*
*
* @file G_MProc.h
* @brief 对gatesrv的处理
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: G_MProc.h
* 摘    要: gatesrv的包处理器
* 作    者: hyn
* 完成日期: 
*/

#ifndef G_MPROC_H
#define G_MPROC_H


#include <map>
#include "OtherServer.h"
#include "PlayerManager.h"
#include "../../Base/PlayerIDBuffer.h"

#include "../../Base/dscontainer/dshashtb.h"
#include "../../Base/dscontainer/dsferd.h"

class CManG_MProc;
class CManNormalMap;
class CPlayer;

extern IBufQueue* g_pPkgSender;		///发送的队列
#define INVALID_GID 0

//#define GATE_SENDV //向gatesrv的集中发包 

class CGateSrv : public CSrv
{
private:
	CGateSrv (const CGateSrv& );
	CGateSrv& operator = (const CGateSrv& );

public:
	CGateSrv();

public:
	//explicit CGateSrv(CSrvConnection* pConn);
	virtual ~CGateSrv();

	PoolFlagDefine();

	virtual void InitConn( CSrvConnection* pConn );

public:
	void SetUniqueFlag( unsigned int uniqueFlag ) { m_uniqueFlag = uniqueFlag; };
	unsigned int GetUniqueFlag() { return m_uniqueFlag; };

private:
	unsigned int m_uniqueFlag;

#ifdef GATE_SENDV //gatesrv层sendv;
	///////////////////////////////////////////////////////
	//以下信息用于dsferd...;
public:
	void SetFerdPos( DsFerd< CGateSrv, 32 >* ownerFerd, int posInFerd )
	{
		m_posInFerd = posInFerd;
		m_ownerFerd = ownerFerd;//记录所属的ferd数组以及自身在该数组中的位置；
	}
	void DelSelfFromFerd()
	{
		if ( NULL != m_ownerFerd )
		{
			if ( m_posInFerd >= 0 )
			{
				m_ownerFerd->DeleteEleAtPos( m_posInFerd );
				SetFerdPos( NULL, -1 );
			}
		}
	}
private:
	DsFerd< CGateSrv, 32 >* m_ownerFerd;//本实例所属的ferd数组；
	int m_posInFerd;//当前在ferd数组中的位置；
	//...以上信息用于dsferd;
	///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////
	//以下集中发送相关...
public:
	void GateCheckSend( const ACE_Time_Value& curTime );//检查是否有足够数据，或是否时间已太久不得不发送数据；
	MsgToPut* GetOldToSend()
	{
		return m_ToSendMsg;
	}

private:
	ACE_Time_Value m_lastSendTime;
	MsgToPut*      m_ToSendMsg;	
	//...以上集中发送相关;
	///////////////////////////////////////////////////////
#endif //GATE_SENDV

	void RealSendPkgToGate( MsgToPut* pPkg );
	
public:
	//新建立时的处理(连接建立)
	void OnCreated()
	{
		return;
	}

public:
	///请求销毁自身(断开连接)
	void ReqDestorySelf()
	{
		TRY_BEGIN;

		D_INFO( "主动请求断开GateSrv，HandleID:%d\n", GetHandleID() );

		m_bIsRcvValid = false;
		if ( NULL != m_pConn )
		{
			m_pConn->ReqDestorySelf();
		}

		return;
		TRY_END;
		return;
	}

public:
	///设已加入管理器标记，以便在断开时进行适当删除(在同ID号gate重复登录时会发生无需删除的情况)；
	inline void SetContainAdd() { m_bIsContainAdd = true; };

private:
	///初始无效，连接建立后有效，发起断开之后无效，有效时可收包，无效时不收包；
	inline bool IsRcvValid() { return m_bIsRcvValid; };
	bool m_bIsRcvValid;//接收是否有效//初始无效，连接建立后有效，发起断开之后无效，有效时可收包，无效时不收包；
	inline bool IsContainAdd() { return m_bIsContainAdd; };
	bool m_bIsContainAdd;//是否已加入管理器；


public:
	virtual void OnDestory( bool isTrueDesOrInit /*是真正从有效状态销毁还是初始化,真正销毁为true，初始化为false*/ );

	void OnPkgError(void)
	{
		D_ERROR( "来自于GsrvID:%d 的错误消息\n", GetHandleID() );
	}


	/*寻找该处理器上编号ID的玩家*/
	CPlayer* FindPlayer( long dwPID )
	{
		TRY_BEGIN;

#ifdef USE_SELF_HASH
	    HashTbPtrEle<CPlayer>* pFound = NULL;
		if ( NULL == m_pHashPlayers )
		{
			return NULL;
		}
		if ( m_pHashPlayers->FindEle( dwPID, pFound ) )
		{
			if ( NULL != pFound )
			{
				return pFound->GetInnerPtr();
			}
		}
		return NULL;
#else //USE_SELF_HASH
		map<long,CPlayer*>::iterator iter = m_mapProcPlayers.find( dwPID );
		if ( iter != m_mapProcPlayers.end() )
		{
			return iter->second;
		}
#endif //USE_SELF_HASH

		return NULL;
		TRY_END;
		return NULL;
	}


	void AddAccountPlayer( long dwPID, const char* playerAccount, CPlayer* pPlayer )
	{
		TRY_BEGIN;

#ifdef USE_SELF_HASH
		if ( NULL == m_pHashPlayers )
		{
			D_ERROR( "AddAccountPlayer， NULL == m_pHashPlayers\n" );
			return;
		}
		m_pHashPlayers->PushEle( HashTbPtrEle<CPlayer>( dwPID, pPlayer ) );
#else //USE_SELF_HASH
		m_mapProcPlayers.insert( make_pair(dwPID, pPlayer) );
#endif //USE_SELF_HASH

    	//因为此时玩家实例身上还没有名字等信息，不能通过pPlayer取玩家帐号，by dzj, 08.04.26;
	    CPlayerManager::InsertAccountPlayer( playerAccount, pPlayer, dwPID );

		TRY_END;
	}

	///通知gatesrv发起踢此玩家
	void NotifyGateSrvKickPlayer( const PlayerID& playerID );

	///发起删去player
	void IssueGateDeletePlayer( long lPlayerPID, int nReason, bool isUpdatePlayerInfo=true);

	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );///这个根据不同的server可重写


private:
	void SetConnected()
	{
		m_bConnected = true;
	}
	void SetDisConnected()
	{
		m_bConnected = false;
	}
	inline bool IsConnected()
	{
		return m_bConnected;
	}
	
private:
	bool m_bConnected;//连接是否已断开

public:
	///发送旧消息，暂存新消息；
	void SendOldRsvNew( MsgToPut* pNewMsg );
#ifndef GATE_SENDV
	///向外发包
	void SendPkgToGateServer( MsgToPut* pPkg );
#endif //GATE_SENDV

	//template <typename T_Msg>
	//void SendBroadcastPkg( T_Msg* pMsg )
	//{
	//	MGBroadCast broadcastMsg;
	//	//获取ID
	//	if( !m_playerIDBuff.CopyBuff( broadcastMsg.broadcastPlayerID, ARRAY_SIZE(broadcastMsg.broadcastPlayerID), broadcastMsg.nNum ) )
	//	{
	//		return;
	//	}
	//	MsgToPut* pBroadcast = CreateSrvPkg( MGBroadCast, this, broadcastMsg );
	//	SendPkgToGateServer( pBroadcast );
	//	pMsg->lNotiPlayerID.wGID = (unsigned short)GetGateSrvID();
	//	MsgToPut* pMsgToPut = CreateSrvPkg( T_Msg, this, *pMsg );
	//	SendPkgToGateServer( pMsgToPut );
	//	m_playerIDBuff.Reset();	//发完清空
	//}


	void SetPkgSender( IBufQueue* pSendNetMsgQueue )
	{
		g_pPkgSender = pSendNetMsgQueue;
	}


	//清除所有玩家
	void ClearAllCPlayer();

	//获取GATESRVID
	inline unsigned long GetGateSrvID(void)
	{
		return m_lGID;
	}

	//设置GATESRVID
	void SetGateSrvID(unsigned long lGID)
	{
		m_lGID = lGID;
	}

	//增加一个玩家ID
	bool AddPlayerID(const PlayerID playerID);

	//清空保存reset Buff
	void ResetBuff();

	//获取缓冲里
	int GetPlayerIDNum();

	inline void GateDelPlayerFromContainer( long dwPID )
	{
#ifdef USE_SELF_HASH
		if ( NULL == m_pHashPlayers )
		{
			D_ERROR( "CGateSrv::DeletePlayer， NULL == m_pHashPlayers\n" );
			return;
		}
		m_pHashPlayers->DelEle( dwPID );
#else //USE_SELF_HASH
		map<long,CPlayer*>::iterator iter = m_mapProcPlayers.find( dwPID );
		if( iter != m_mapProcPlayers.end() )
		{
			m_mapProcPlayers.erase(iter);
		}
#endif //USE_SELF_HASH
		return;
	}

private:

#ifdef USE_SELF_HASH
	DsHashTable< HashTbPtrEle<CPlayer>, 6000, 2 >* m_pHashPlayers;
#else //USE_SELF_HASH
	map<long,CPlayer*> m_mapProcPlayers;  ///单个gs处理器上的玩家
#endif //USE_SELF_HASH

	unsigned long m_lGID;	//gatsrv上的ID
	//CPlayerIDBuff m_playerIDBuff;	//用于范围通知的玩家ID缓存
};


class CManG_MProc
{
private:
	CManG_MProc (const CManG_MProc& );
	CManG_MProc& operator = (const CManG_MProc& );

public:
	///每个gatesrv定时执行的时钟(定时通知各gate，本mapsrv是否可进入)；
	static void ManGateSrvTimer();

public:
//  因为gatesrv与connection绑定，连接删除时将gatesrv删去，因此这里不需要释放；	static void ReleaseManedGateSrv()
//	{
//#ifdef GATE_SENDV //玩家层sendv;
//		FERDFOR( CGateSrv, (&m_arrGateHandle), tmpPtr )
//		{
//			if ( NULL != tmpPtr )
//			{
//				g_poolManager->ReleaseGateSrv( tmpPtr );
//			}
//		}
//		ENDFERDFOR;
//#else  //GATE_SENDV
//		for ( map<int, CGateSrv*>::iterator tmpIter=m_mapHandle.begin(); tmpIter!=m_mapHandle.end(); ++tmpIter )
//		{
//			if ( NULL != tmpIter->second )
//			{
//				g_poolManager->ReleaseGateSrv( tmpIter->second );
//			}
//		}
//		m_mapHandle.clear();
//#endif //GATE_SENDV
//	}

	//通过GID来添加gatesrv
	static void AddGateSrv(int wGID, CGateSrv* pProc )
	{
		TRY_BEGIN;

		if ( NULL == pProc )
		{
			return;
		}

		map<int, CGateSrv*>::iterator tmpIter = m_mapGateSrv.find( wGID );
		if ( tmpIter != m_mapGateSrv.end() )
		{
			D_WARNING( "将GateServer加入全局列表时,发现对应的wGID:%d已在列表中，断开第二个gatesrv\n", wGID );
			pProc->ReqDestorySelf();
			return;
		}
		pProc->SetContainAdd();
	    m_mapGateSrv.insert( pair<int, CGateSrv*>( wGID, pProc ) );

		TRY_END;
	}

	static void AddHandle(int nHandleID, CGateSrv* pProc)
	{
		TRY_BEGIN;

		++m_curMaxGateFlag;
		if ( m_curMaxGateFlag <= 0 )
		{
			//因为0用作无效gatesrv标识,在gatesrv收回对象池时，其uniqueflag被置为0；
			m_curMaxGateFlag = 1u;
		}
		pProc->SetUniqueFlag( m_curMaxGateFlag );//设置该gatesrv的唯一标识号；

#ifdef GATE_SENDV //玩家层sendv;
		m_arrGateHandle.InsertEle( pProc );
#else  //GATE_SENDV
		map<int, CGateSrv*>::iterator tmpIter = m_mapHandle.find( nHandleID );
		if ( tmpIter != m_mapHandle.end() )
		{
			D_WARNING( "将GateServer加入全局列表时,发现对应的nHandleID:%d已在列表中\n", nHandleID );
			return;
		}
	    m_mapHandle.insert( pair<int, CGateSrv*>( nHandleID, pProc ) );
#endif //GATE_SENDV
		TRY_END;
	}

	static void CheckSend( const ACE_Time_Value& curTime );//检测所有gate句柄上有无延时数据需要发送；

	//通过GID来删除gatesrv
	static bool RemoveGateSrv( int wGID )
	{
		TRY_BEGIN;

   	 	map<int, CGateSrv*>::iterator tmpIter = m_mapGateSrv.find( wGID );
		if ( tmpIter != m_mapGateSrv.end() )
		{
			m_mapGateSrv.erase( tmpIter );
			return true;
		}

		TRY_END;
		return false;
	}


	static bool RemoveHandle( int nHandle, CGateSrv* pResHandle )
	{
		TRY_BEGIN;

#ifdef GATE_SENDV //玩家层sendv;
		pResHandle->DelSelfFromFerd();
#else  //GATE_SENDV
		ACE_UNUSED_ARG( pResHandle );
   	 	map<int, CGateSrv*>::iterator tmpIter = m_mapHandle.find( nHandle );
		if ( tmpIter != m_mapHandle.end() )
		{
			m_mapHandle.erase( tmpIter );
			return true;
		}
#endif //GATE_SENDV

		return false;
		TRY_END;
		return false;
	}

	//通过GID来寻找gatesrv
	static CGateSrv* FindProc( int wGID )
	{
		TRY_BEGIN;

		map<int, CGateSrv*>::iterator tmpIter = m_mapGateSrv.find(wGID);
		if ( tmpIter != m_mapGateSrv.end() )
		{
			return tmpIter->second;
		}
		else 
		{
			return NULL;
		}

		TRY_END;
		return NULL;
	}

	//随机寻找第一个gatesrv可用gatesrv来发消息
	static CGateSrv* FindRandGateSrv()
	{
		if( !(m_mapGateSrv.empty()) )
		{
			map<int,CGateSrv*>::iterator iter = m_mapGateSrv.begin();
			return iter->second;
		}
		return NULL;
	}

	///发送广播消息给各gate，每个gate一条；
	template< typename T_Msg >
	static void SendMsgToAllGate( T_Msg* pMsg )
	{
		CGateSrv* pGateSrv = NULL;
		for( map<int, CGateSrv*>::iterator iter = m_mapHandle.begin(); iter != m_mapHandle.end(); ++iter )
		{
			pGateSrv = iter->second;
			if( NULL == pGateSrv )
				continue;
			MsgToPut* pNewMsg = CreateSrvPkg( T_Msg, pGateSrv, *pMsg );
			pGateSrv->SendPkgToGateServer( pNewMsg );
		}
	}

	///发送广播消息给服务器上所有玩家，每个gate一条，到gate后，会由gate转发给各gate上所有玩家;
	template< typename T_Msg >
	static void SendToAllSrvAllPlayer( T_Msg* pMsg )
	{
		CGateSrv* pGateSrv = NULL;
        MGBroadCast preBroad;//全服广播前导消息；
		StructMemSet( preBroad, 0x00, sizeof(preBroad) );
		preBroad.nCurNum = MGBroadCast::NOTI_EVERYONE;
		for( map<int, CGateSrv*>::iterator iter = m_mapHandle.begin(); iter != m_mapHandle.end(); ++iter )
		{
			pGateSrv = iter->second;
			if( NULL == pGateSrv )
				continue;

			MsgToPut* pNewMsg = CreateSrvPkg( MGBroadCast, pGateSrv, preBroad );//先发送前导消息(这里不使用CreateMGTPkg组包，因为nCurNum为特殊值，而不是表明数组大小)；
			pGateSrv->SendPkgToGateServer( pNewMsg );

			pMsg->lNotiPlayerID.wGID = (unsigned short)pGateSrv->GetGateSrvID();//防止被gatesrv忽略;
			pMsg->lNotiPlayerID.dwPID = 0;//防止被gatesrv忽略;
			pNewMsg = CreateSrvPkg( T_Msg, pGateSrv, *pMsg );
			pGateSrv->SendPkgToGateServer( pNewMsg );
		}
	}

private:
	CManG_MProc(){}  //constructor
	~CManG_MProc(){}  //destructor
	
private:
	static map<int, CGateSrv*> m_mapGateSrv;
#ifdef GATE_SENDV //玩家层sendv;
	static DsFerd< CGateSrv, 32 > m_arrGateHandle;//存放所有gates的ferd;
#else  //GATE_SENDV
	static map<int, CGateSrv*> m_mapHandle;
#endif //GATE_SENDV

private:
	static unsigned int m_curMaxGateFlag;//用于存放下一个连进gatesrv的序号标识，每连进一个新的gatesrv，该值++;
};

#endif /*G_MPROC_H*/
