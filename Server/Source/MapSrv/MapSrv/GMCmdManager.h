﻿/** @file GMCmdManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-3-12
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef GM_CMD_MANAGER_H
#define GM_CMD_MANAGER_H

#include "Player/Player.h"
#include <map>
#include <string>
#include <sstream>
#include <vector>


/**
* @class CGMCmdManager
*
* @brief 管理GM指令的注册、查找、分派
* 
*/


//增加修改的人物属性的枚举
enum EAttribute { INVALID_ATTR = 0,  //无效的ATTR
				  ATTR_LEVEL,  //等级
				  ATTR_TITLE,   //称号
				  ATTR_EXP,	//经验
				  ATTR_SPEED, //速度
				  ATTR_SPMAX, //最大SP(序号5)
				  ATTR_SPNUMMAX,  //最大SP槽
				  ATTR_HP,	//当前HP
				  ATTR_MP,  //当前MP
				  ATTR_SP,	//当前SP
				  ATTR_SPNUM,	//当前SP槽(序号10)
	   	   		  ATTR_PHYLEVEL,  //物理等级
				  ATTR_MAGLEVEL,  //魔法等级
				  ATTR_PHYLEVEL_MAX,  //物理技能上限
	   	   		  ATTR_MAGLEVEL_MAX,  //魔法技能上限
				  ATTR_PHYEXP,	//物理技能熟练度(序号15)
				  ATTR_MAGEXP,  //魔法技能熟练度
				  ATTR_PHY_ALLOPT,  //物理可分配点数
				  ATTR_MAG_ALLOPT,  //魔法可分配点数
				  ATTR_PHY_TOTALPT, //物理总获得点数
				  ATTR_MAG_TOTALPT, //魔法总获得点数(序号20)
				  ATTR_MONEY, //钱币(银币)
				  ATTR_BUFF,  //BUFF效果
				  ATTR_PHYATTACK,  //物理攻击
				  ATTR_STR,	//力量
				  ATTR_INT,	//智力(序号25)
				  ATTR_AGI,	//敏捷
				  ATTR_HPMAX,	//最大HP
				  ATTR_MPMAX,	//最大MP
				  ATTR_MAGATTACK,	//魔法攻击力
				  ATTR_PHYDEFEND,	//物理防御力(序号30)
				  ATTR_MAGDEFEND,	//魔法防御力
				  ATTR_PHYHIT,	//物理命中
				  ATTR_MAGHIT,	//魔法命中
				  ATTR_RACE_PRESTIGE, //种族声望
				  ATTR_GOODEVILPOINT,	//善恶值(序号35)
				  ATTR_GOLD_MONEY, //钱币(金币)
				  ATTR_MAX
				};


class CGMCmdManager
{
	typedef int (*PFN_Fun)( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	typedef int (*PREQ_Fun)( const std::vector<std::string> &cmdArgs, const PlayerID& requestID );

public:
	enum ChangeType
	{
		TYPE_SET,
		TYPE_ADD,
	};

#define GM_CMD_SUCCESS 0	//GM命令使用成功的宏
#define GM_CMD_ERROR -1		//GM命令错误的宏
#define GM_TRANSFER -2		//需要转发到CENTER去的GM命令

	
public:
	/// 初始化
	static int GMCmdInit(void);
	static int InitReqGmCmd();		//对转发来的GM命初始化

	// 是否是GM指令？
	static bool IsGmCmd(const char* pszString, std::string &strCmd, std::vector<std::string> &cmdArgs);

	/// 执行
	static int DealGMCmd(const char *pszCmd, std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	static int DealReqGmCmd(const char *pszCmd, std::vector<std::string> &cmdArgs, const PlayerID& requestPlayerID );

private:
	/// 注册
	static void RegisterGMCmd(const char* pszCmd, PFN_Fun CmdFun);
	static void RegisterReqGmCmd(const char* pszCmd, PREQ_Fun CmdFun);

	/// 查找
	static PFN_Fun FindGMCmd(const char *pszCmd);
	static PREQ_Fun FindReqGmCmd(const char *pszCmd);

private:
	/// GM命令处理函数...
	/// 设定玩家属性
	static int SetAttribute( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//增加属性
	static int AddAttribute( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	/// 技能
	static int AddSkill( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	/// 无敌
	static int King( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	/// 解除无敌
	static int Unking( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	/// 创建NPC
	static int CreateNPC( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	/// 删除NPC
	static int DestoryNPC( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	///GM命令，同地图内跳转；
	static int MoveTo( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	/// 移动到他人（或NPC）身边
	static int Go( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	/// 把他人(或NPC)拉到自己身边
	static int Drag( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	/// 隐身
	static int Hide( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	/// 解除隐身
	static int Unhide( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//查询属性
	static int GetAttribute( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//创建道具
	static int CreateItem( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//版本信息
	static int GetVersion( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//怪物STATE
	static int GetMonsterState( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//设置玩家已完成任务但尚未完成提交；
	static int GMFinishTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//添加一个玩家 BUFF
	static int AddPlayerBuff( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//添加一个怪物的BUFF
	static int AddMonsterBuff( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//升级道具
	static int LevelUpItem( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//改造道具
	static int ChangeItem( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//改变游戏中的道具等级
	static int ChangeItemLevel( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );
	//改变游戏中道具的追加编号
	static int ChangeItemAddID( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );

    //设置或取消查看地图调试信息开关, for debug, by dzj, 08.10.30；
	static int ViewMapDebugInfo( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//查询服务器端的移动玩家信息, for debug, by dzj, 08.10.22；
	static int GetMapMovingPlayer( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//查询服务器端玩家自身位置, for debug, by dzj, 08.10.22；
	static int GetSelfPos( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//调试威胁列表开始
	static int ThreatListStart( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//调试威胁列表结束
	static int ThreatListEnd( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//获取物品信息
	static int GetItemInfo( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//在游戏中更新伤害脚本
	static int UpdateScript( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//清除游戏中的任务存盘信息
	static int ClearTaskHistory( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//查询玩家的善恶值
	static int QueryGoodEvilPoint( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//设置玩家的善恶值
	static int SetGoodEvilPoint( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//查询玩家处于恶魔状态的时间
	static int ShowGoodEvilPointTime( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//重载AI脚本；
	static int ReloadAIScript( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	////查找指定帐号玩家位置；
	//static int GetPlayerSur( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//打印函数的性能的分析报告
	static int ShowProfileAnalyze( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//打开性能分析的开关
	static int OpenProfileSwitch( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//关闭性能分析开关
	static int TurnOffProfileSwitch( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );
	//跳转地图的实现
	static int SwichMapReq( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );
	//设置幸运系统
	static int SetLuckySystem( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );

	static int SendSpecialString( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );

#ifdef ITEM_NPC
	static int ChangeItemSeqStatus( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );
#endif //ITEM_NPC

#ifdef OPEN_PUNISHMENT
	//启动天谴
	static int StartPunishment( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );
	//关闭天谴
	static int EndPunishment( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );
	//自身遭受天谴
	static int PunishPlayer( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );
#endif

	//增加工会活跃度
	static int AddUnionActive(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	static int AddUnionPrestige(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	static int AddPetExp(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	static int SetPetHunger(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	static int ShowPetHunger(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	static int ShowProtectPlate( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );//显示保护道具的托盘
	static int AddCityExp( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );	//增加关卡城市的经验
	static int GetCityInfo( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );	//获取关卡地图的信息
	static int GiveTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );		//添加任务
	static int SubmitTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );		//提交任务

	static int GivePunishTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );	//添加天谴任务
	static int FinishPunishTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );	//完成天谴任务
	static int GiveUpPunishTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );	//放弃天谴任务
	//设置玩家种族(临时)
	static int SetPlayerRace( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	static int OnShowStoragePlate( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );//显示仓库托盘

	static int KickAllPlayer( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );  //踢去所有玩家

	//创建一个指定副本，并将该玩家加入此副本;
	static int OnTryCopy( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//创建一个指定副本，并将该玩家加入此副本;
	static int OnTryPCopy( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//玩家离开当前副本;
	static int OnLeaveCopy( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );//玩家离开当前副本;

	static int OnGetAllPetSkill( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	static int ReloadBattleScript( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	static int RunTestBattleScript( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	static int BeRaceMaster( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );

	static int ShowRaceMasterPlate( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	static int RaceMasterNewPublicMsg( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	static int RaceMasterDeclareWar( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	static int TestWarRebirthPt( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//开启攻城战
	static int StartAttackWar( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//设置攻城战的激活与否
	static int ActivateCityWar( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//更改姓名
	static int ChangeSelfName( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	//设置当前地图的生成器
	static int SetCreator( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager ); 
	//设置warTimer
	static int AddWarTimer( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager ); 
	//创建地图buff
	static int CreateMapBuff( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  );

	static int TestBlockQuad( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager ); 
	//设置被动休息技能的回血间隔
	static int SetRsInterval( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager ); 
	//开始新手闪烁
	static int CreateRookieBreak( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager ); 

	//开始选族长
	static int SelectRaceMaster( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//踢玩家,角色名请求，按照角色名的GM流程处理
	static int OnKickPlayerRequest( const std::vector<std::string> &cmdArgs, const PlayerID& requestPlayerID  );	

	//返回角色选择
	static int ReturnSelRole(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );
	
	static void SetGmShield(bool bGmShield)
	{
		m_bGmShield = bGmShield;
	}

	//修改伪线状态
	static int ModifyPWState( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//修改伪线地图状态
	static int ModifyPWMapState( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//切线
	static int ShiftPW( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );

	//踢线
	static int KickOffPW( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager );


private:
	CGMCmdManager(){}  //constructor
	~CGMCmdManager(){}  //destructor

public:
	//更改属性 type 0代表set 1代表增加
	static int _ChangeAttribute(CPlayer* pPlayer, ChangeType nType, EAttribute attr,unsigned int nUpdateVal);//public,供NPC脚本扩展调用

	static bool NpcQueryPropertyEqual (CPlayer* pPlayer, EAttribute attr, int nUpdateVal);

	static bool NpcQueryPropertyLowerThan(CPlayer* pPlayer, EAttribute attr, int nUpdateVal);

	static bool NpcQueryPropertyMoreThan(CPlayer* pPlayer, EAttribute attr, int nUpdateVal);

	static bool NpcAddSkill( TYPE_ID skillID, CPlayer* pPlayer);

	static unsigned int GetRsInterval()
	{
		return m_rsInterval;
	}

private:
	/// GM指令列表
	static std::map<std::string, PFN_Fun> m_GMCmdList;
	static std::map<std::string, PREQ_Fun> m_mapReqGmCmd;	//申请GM列表

	static bool m_bGmShield;	//是否需要屏蔽GM命令
	static unsigned int m_rsInterval;

	//更改属性
	static int _ChangeLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);

	static int _ChangeExp( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal);

	static int _ChangeSpeed( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal);

	static int _ChangePhyAttack( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal);

	static int _ChangeMagAttack( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal);

	static int _ChangePhyDefend( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal);

	static int _ChangeMagDefend( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal);

	static int _ChangePhyHit( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal);

	static int _ChangeMagHit( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal);

	static int _ChangeHp( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal);

	static int _ChangeMp( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);

	static int _ChangePhyExp( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);

	static int _ChangeMagExp( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);

	static int _ChangeInt( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);

	static int _ChangeStr( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);

	static int _ChangeMoney( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);
	static int _ChangeGoldMoney( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);

	static int _ChangePhyLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);	//变更物理技能等级

	static int _ChangeMagLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);	//变更魔法技能等级

	static int _ChangeMaxPhyLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);

	static int _ChangeMaxMagLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal);

	static int _ChangeSpPower( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal );

	static int _ChangeRacePrestige( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal );

};

#endif/*GM_CMD_MANAGER_H*/


