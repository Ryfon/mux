﻿#pragma once
#include "ManObject.h"

class CBuffProp;
class CElement;

class CNpcSkill
{
private:
	CNpcSkill( const CNpcSkill& map);
	CNpcSkill& operator = ( const CNpcSkill& map ); //屏蔽这两个操作；

public:
	CNpcSkill(void);
	virtual ~CNpcSkill(void);

public:
	unsigned long GetObjID()
	{
		return m_lNpcSkillID;
	}

public:
	unsigned long m_lNpcSkillID;  //技能ID 
	int m_nTarget;   //目标
	int m_nType;	 //技能的类型
	float m_fHit;		 //技能的命中
	float m_fAddHit;	 //技能的附加命中
	float m_fCritical;	//技能的暴击命中
	int m_nAddAtt;
	int m_nAttMin;
	int m_nAttMax;
	int m_nAttDistMin;
	int m_nAttDistMax;
	int m_nShape;
	int m_nRange;
	int m_nCurseTime;
	int m_nCoolDown;		//冷却时间
	int m_nLinkID;			//随即的BUFFID
	float m_fLinkRate;		//出现几率
	int m_nPeriodTime;		//BUFF出现的时间
	int m_nAssistObject;	//选中对象
	

	CBuffProp* m_pBuffProp;	//如果LINK BUFF话的属性
};


class CManNpcSkill : public CObjectManager<CNpcSkill>
{
public:
	static bool Init();

	~CManNpcSkill(){}

protected:
	CManNpcSkill(){}

private:
	static bool LoadFromXML(const char* szFileName);

	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord );
};
