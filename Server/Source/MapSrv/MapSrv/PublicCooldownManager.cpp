﻿#include "PublicCooldownManager.h"

map< int,vector<unsigned int> > CPublicCooldownManager::m_mapTypeSkill;

CPublicCooldownManager::CPublicCooldownManager(void)
{
}

CPublicCooldownManager::~CPublicCooldownManager(void)
{
}


void CPublicCooldownManager::InsertPublicCooldownInfo( int cooldownID, unsigned int skillID)
{
	map< int, vector< unsigned int> >::iterator iter = m_mapTypeSkill.find( cooldownID );
	if( iter == m_mapTypeSkill.end() )
	{
		vector< unsigned int> vecSkillID;
		vecSkillID.push_back( skillID );
		m_mapTypeSkill.insert( make_pair(cooldownID, vecSkillID ) ); 
	}else{
		iter->second.push_back( skillID );
	}
}


vector<unsigned int>* CPublicCooldownManager::GetCoolDownSkills( int cooldownID )
{
	map< int, vector< unsigned int> >::iterator iter = m_mapTypeSkill.find( cooldownID );
	if( iter == m_mapTypeSkill.end() )
	{
		return NULL;
	}else{
		return &iter->second;
	}
}
