﻿/********************************************************************
	created:	2009/06/09
	created:	9:6:2009   17:20
	file:		TaskIndexManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef _TASK_INDEX_MANAGER_H
#define _TASK_INDEX_MANAGER_H

#include <map>
#include <vector>
#include <aceall.h>
#include "Utility.h"


class CElement;
class CPlayer;

struct TaskIndexInfo
{
	unsigned int sex;
	unsigned int nclass;
	unsigned int nrace;
	std::map<unsigned int, unsigned int> mTaskIndexMap;
};


class TaskIndexInfoManager
{
	friend class ACE_Singleton<TaskIndexInfoManager, ACE_Null_Mutex>;	
public:
	TaskIndexInfoManager() {};

	~TaskIndexInfoManager()
	{
		std::vector<TaskIndexInfo *>::iterator lter = mTaskIndexInfoVec.begin();
		for ( ; lter != mTaskIndexInfoVec.end(); ++lter )
		{
			delete *lter;
		}
		mTaskIndexInfoVec.clear();
	}


public:
	void LoadAllTaskXML();

	void LoadXML( unsigned int nsex ,unsigned int nrace, unsigned int nclass );

	void PushTaskIndexEle( CElement* pxmlElement, TaskIndexInfo* pTaskIndexInfo  );

	int FindTaskIndex( unsigned int sex,  unsigned int nrace ,unsigned int nclass, unsigned int taskID );

private:
	std::vector<TaskIndexInfo *> mTaskIndexInfoVec;
};
typedef ACE_Singleton<TaskIndexInfoManager,ACE_Null_Mutex> TaskIndexInfoManagerSingle;



class NewTaskRecordDelegate
{
public:
	NewTaskRecordDelegate():m_pAttachPlayer(NULL){ StructMemSet(m_TaskRdArr,0,sizeof(m_TaskRdArr)); }

public:
	void AttachPlayer( CPlayer* pPlayer )
	{
		m_pAttachPlayer = pPlayer;
	}

	void RecvPlayerTaskRecordArr( unsigned int* taskRdArr, unsigned int pageIndex );

	//是否做过这个任务
	bool IsHaveDoneTask( char sex, unsigned int race, unsigned int nclass , unsigned int taskID );

	//将做过的这个任务记录下来
	void RecordDoneTask( char sex, unsigned int race, unsigned int nclass , unsigned int taskID );

	//获取任务记录的的状态
	unsigned int GetTaskRdState( unsigned int taskMask );

	//设置任务记录的状态
	void SetTaskRdState( unsigned int taskMask ,unsigned int taskState );

	//更新最新的任务存盘信息到DB
	void UpdateTaskRdInfoToDB();

private:
	CPlayer* m_pAttachPlayer;
	unsigned int m_TaskRdArr[100];
};

#endif

