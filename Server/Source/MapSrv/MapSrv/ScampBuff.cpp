﻿#include "ScampBuff.h"
#include "Player/Player.h"

CScampBuff::CScampBuff(void)
{
}

CScampBuff::~CScampBuff(void)
{
}


void CScampBuff::BuffStart()
{ 
	if( NULL == m_pOwner )
	{
		D_ERROR("CScampBuff::BuffStart m_pOwner为NULL\n");
		return;
	}

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	SetUpdateTime( currentTime );

	m_pOwner->StartRogueState();
}

void CScampBuff::Update()
{
	ACE_Time_Value currentTime = ACE_OS::gettimeofday();

	if( IsBuffEnd( currentTime ) ) 
	{
		BuffEnd();  //这个函数对到期的BUFF进行处理
		return;
	}

	SetLeftTime(  GetLeftTime() - (currentTime - GetUpdateTime()) );	//对剩余的时间作修改
	SetUpdateTime( currentTime );  //此次轮询结束
}


void CScampBuff::BuffEnd()
{
	if( NULL == m_pOwner )
	{
		D_ERROR("CScampBuff::BuffEnd m_pOwner为NULL\n");
		return;
	}

	SetDelFlag();  //结束了就要删除
	m_pOwner->EndRogueState();
	m_pOwner->ClearRogueState();
}


void CScampBuff::Initialize( CPlayer* pOwner, int seconds )
{
	m_pOwner = pOwner;
	ACE_Time_Value leftTime( seconds,0);
	SetLeftTime( leftTime );
	CBuffManager::InsertBuff( this );
}


void CScampBuff::Release()
{
	if ( NULL != g_poolManager )
	{
		g_poolManager->ReleaseRogueBuff( this );
	}
}
