﻿#include "AntiAddictionManager.h"
#include "Player/Player.h"

CAntiAddictionManager::CAntiAddictionManager(void):m_state( NORMAL ),m_pOwner( NULL )
{
}

CAntiAddictionManager::~CAntiAddictionManager(void)
{
}


bool CAntiAddictionManager::IsTriedState()  	//是否在疲劳状态
{
	return ( m_state == TRIED );
}


bool CAntiAddictionManager::IsTriedOrAddictionState()
{
	return ( (m_state == TRIED) || (m_state == ADDICTION ) ); 
}


void CAntiAddictionManager::AttachOwner( CPlayer* pPlayer )
{
	m_pOwner = pPlayer;
}


unsigned int CAntiAddictionManager::GetState()			//获取状态
{
	return (UINT)m_state;
}


//现在并没有根据不同的收益类型来获得不同的收益
void CAntiAddictionManager::TriedStateProfitCheck( unsigned int& addVal, ProfitType addType )	//如果是未成年人且在疲劳状态，增加的值可能要减少一半( 金钱,经验
{
	ACE_UNUSED_ARG( addType );
	if( IsTriedState() )
	{
		addVal = addVal / 2;
		return;
	}

	//新需求
	if( m_state == ADDICTION )
	{
		addVal = 0;
	}
}


bool CAntiAddictionManager::TriedStatePKCheck()			//在疲劳状态下的PK判定
{
	if( NULL == m_pOwner )
		return false;
	
	if( m_pOwner->GetGoodEvilPoint() < 0 && IsTriedState() )
	{
		m_pOwner->SendSystemChat("疲劳状态下魔头是不能进行PK的");
		return false;
	}

	return true;
}


void CAntiAddictionManager::SetState( unsigned int state )
{
	m_state = (AddictiontState)(state);
}
