﻿#include "CooldownManager.h"

bool CCooldownManager::CheckCooldown( TYPE_ID cdType, const ACE_Time_Value& cooldownTime )
{
	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	map<TYPE_ID, ACE_Time_Value>::iterator iter = m_mapCdTimer.find( cdType );
	if( iter != m_mapCdTimer.end() )
	{
		if( currentTime >= iter->second )
		{
			currentTime += cooldownTime;
			iter->second = currentTime;		//重置时间
			return true;
		}
		return false;
	}

	currentTime += cooldownTime;
	m_mapCdTimer.insert( make_pair(cdType, currentTime) );	
	return true;
}
