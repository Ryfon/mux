﻿
#include "rankeventmanager.h"

#include "../Player/Player.h"

bool         RankEventManager::m_isLoadCmdIssued = false;//是否已发装载命令；
bool         RankEventManager::m_isReady = false;//是否准备好(已收到functionsrv相关信息)
int          RankEventManager::m_arrBias[RANK_TYPE_NUM];
unsigned int RankEventManager::m_arrRankSeq[RANK_TYPE_NUM];//各排行榜当前序号；

//加载排行榜的信息
void RankEventManager::LoadAllRankChartInfo()
{
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("RankEventManager::LoadAllRankChartInfo, 找不到一个用于发送消息的GateSrv\n");
		return;
	}

	MGLoadRankInfo loadRankInfo;
	MsgToPut* pNewMsg = CreateSrvPkg( MGLoadRankInfo, pGateSrv, loadRankInfo );
	pGateSrv->SendPkgToGateServer( pNewMsg );

	m_isLoadCmdIssued = true;

	return;
}

//收到新的阈值或排行榜序号
void RankEventManager::OnBiasInfo( const GMBiasInfo* pGMBiasInfo )
{
	if ( NULL == pGMBiasInfo )
	{
		D_ERROR( "RankEventManager::OnBiasInfo, NULL == pGMBiasInfo\n" );
		return;
	}

	if ( ( ARRAY_SIZE(pGMBiasInfo->arrBias) != ARRAY_SIZE(m_arrBias) )
		|| ( ARRAY_SIZE(pGMBiasInfo->arrRankSeq) != ARRAY_SIZE(m_arrRankSeq) )
		)
	{
		D_ERROR( "RankEventManager::OnBiasInfo, GMBiasInfo协议中的数组大小不正确\n" );
		return;
	}

	for ( int i=0; i<ARRAY_SIZE(m_arrBias); ++i )
	{
		if ( ( i >= ARRAY_SIZE(m_arrRankSeq) ) 
			|| ( i >= ARRAY_SIZE(pGMBiasInfo->arrBias) )
			|| ( i >= ARRAY_SIZE(pGMBiasInfo->arrRankSeq) )
			)
		{
			D_ERROR( "RankEventManager::OnBiasInfo， i(%d)越界\n", i );
			return;
		}
		m_arrBias[i] = pGMBiasInfo->arrBias[i];
		m_arrRankSeq[i] = pGMBiasInfo->arrRankSeq[i];
	}

	if ( !m_isReady )
	{
		m_isReady = true;

		//告诉gate自身准备好;		
		CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
		if( NULL == pGateSrv )
		{
			D_ERROR("RankEventManager::OnBiasInfo, 找不到一个用于发送消息的GateSrv\n");
			return;
		}
		MGMapSrvReady mapSrvReady;
		MsgToPut* pNewMsg = CreateSrvPkg( MGMapSrvReady, pGateSrv, mapSrvReady );
		pGateSrv->SendPkgToGateServer( pNewMsg );
	}

	return;
}


//通知functionsrv相关缓存信息改变；
bool RankEventManager::NotifyCacheInfo( ERankType rankType, CPlayer* pPlayer, int newVal )
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "RankEventManager::NotifyCacheInfo, NULL == pPlayer\n" );
		return false;
	}

	if ( rankType >= ARRAY_SIZE(m_arrRankSeq) )
	{
		D_ERROR( "RankEventManager::NotifyCacheInfo, rankType >= ARRAY_SIZE(m_arrBias)\n" );
		return false;
	}

	MGRankCacheInfo cacheInfo;
	cacheInfo.rankType = rankType;
	//使用extrainfo最低一位存储性别信息；
	cacheInfo.cacheRcd.SetIsFemale( pPlayer->GetSex() != 1 );
	//cacheInfo.cacheRcd.shFlag = pPlayer->GetSHFlag();
	cacheInfo.cacheRcd.rankSeqID = m_arrRankSeq[rankType];
	cacheInfo.cacheRcd.enterMapSeq = pPlayer->GetEnterMapSeq();
	cacheInfo.cacheRcd.rankItemInfo.playerClass = pPlayer->GetClass();
	cacheInfo.cacheRcd.rankItemInfo.playerLevel = pPlayer->GetLevel();
	cacheInfo.cacheRcd.rankItemInfo.rankInfo = newVal;
	SafeStrCpy( cacheInfo.cacheRcd.rankItemInfo.playerName, pPlayer->GetNickName() );
	cacheInfo.cacheRcd.playerUID = pPlayer->GetPlayerUID();

	pPlayer->ForceGateSend<MGRankCacheInfo>(  &cacheInfo  );

	return true; 
}

//当玩家的善恶值发生改变时
bool RankEventManager::MonitorPlayerGoodEvilPointChange( CPlayer* pPlayer, int oldPoint/*旧值*/ )
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "RankEventManager::MonitorPlayerGoodEvilPointChange，NULL == pPlayer\n" );
		return false;
	}

	int newPoint = pPlayer->GetGoodEvilPoint();

	if ( (newPoint >= m_arrBias[RANK_GOODEVIL]) || (oldPoint >= (int)(m_arrBias[RANK_GOODEVIL])) )
	{
		NotifyCacheInfo( RANK_GOODEVIL, pPlayer, newPoint );
	}

	return true;
};

//监视玩家伤害
bool RankEventManager::MonitorPlayerDamage( CPlayer* pPlayer, unsigned int damage ) 
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "RankEventManager::MonitorPlayerDamage，NULL == pPlayer\n" );
		return false;
	}

	pPlayer->NewOutDamage( m_arrRankSeq[RANK_DAMAGE], damage );

	if ( damage > (unsigned int)m_arrBias[RANK_DAMAGE] )
	{
		NotifyCacheInfo( RANK_DAMAGE, pPlayer, damage );
	}

	return true;
};

//监视玩家死亡
bool RankEventManager::MonitorPlayerDie( CPlayer* pPlayer ) 
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "RankEventManager::MonitorPlayerDie，NULL == pPlayer\n" );
		return false;
	}

	pPlayer->IncBeKillTimes( m_arrRankSeq[RANK_BEKILL] );
	unsigned int beKillTimes = pPlayer->GetBeKillTimes( m_arrRankSeq[RANK_BEKILL] );

	if ( beKillTimes > (unsigned int)m_arrBias[RANK_BEKILL] )
	{
		NotifyCacheInfo( RANK_BEKILL, pPlayer, beKillTimes );
	}

	return true;
};

//监视玩家杀人次数
bool RankEventManager::MonitorPlayerKill( CPlayer* pPlayer ) 
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "RankEventManager::MonitorPlayerKill，NULL == pPlayer\n" );
		return false;
	}

	pPlayer->IncKillTimes( m_arrRankSeq[RANK_KILL] );
	unsigned int killTimes = pPlayer->GetKillTimes( m_arrRankSeq[RANK_KILL] );

	if ( killTimes > (unsigned int)m_arrBias[RANK_KILL] )
	{
		NotifyCacheInfo( RANK_KILL, pPlayer, killTimes );
	}

	return true;
};

//监视玩家金币改变
bool RankEventManager::MonitorPlayerGoldMoney( CPlayer* pPlayer, unsigned int oldGold/*旧值*/ ) 
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "RankEventManager::MonitorPlayerGoldMoney，NULL == pPlayer\n" );
		return false;
	}

	unsigned int newGold = pPlayer->GetGoldMoney();

	if ( (newGold > (unsigned int)m_arrBias[RANK_GOLD]) || (oldGold >= (unsigned int)m_arrBias[RANK_GOLD]) )
	{
		NotifyCacheInfo( RANK_GOLD, pPlayer, newGold );
	}

	return true;
};

//监视玩家银币改变
bool RankEventManager::MonitorPlayerSilverMoney( CPlayer* pPlayer, unsigned int oldSilver/*新值*/ ) 
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "RankEventManager::MonitorPlayerSilverMoney，NULL == pPlayer\n" );
		return false;
	}

	unsigned int newSilver = pPlayer->GetSilverMoney();

	if ( (newSilver > (unsigned int)m_arrBias[RANK_SILVER]) || (oldSilver >= (unsigned int)m_arrBias[RANK_SILVER]) )
	{
		NotifyCacheInfo( RANK_SILVER, pPlayer, newSilver );
	}

	return true;
};

//当玩家的等级发生改变时
bool RankEventManager::OnPlayerLevelChange( CPlayer* pPlayer ) 
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "RankEventManager::OnPlayerLevelChange，NULL == pPlayer\n" );
		return false;
	}

	unsigned int newLevel = pPlayer->GetLevel();

	if ( newLevel > (unsigned int)m_arrBias[RANK_LEVEL] )
	{
		NotifyCacheInfo( RANK_LEVEL, pPlayer, newLevel );
	}

	return true;
};