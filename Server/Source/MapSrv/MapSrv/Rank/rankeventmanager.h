﻿
/**
* @file rankeventmanager.h
* @brief rankeventmanager
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: rankeventmanager.h
* 摘    要: 排行榜相关事件管理器
        MapSrv维持一个阈值，若变化前后的某个值在此阈值内，则相关信息通知functionsrv
		FunctionSrv根据收到的信息更新自身缓存，若阈值发生变化，则广播给mapsrv更新
		为减少阈值下降带来的进榜人员遗漏可能，functionsrv的缓存数量按实际进榜人数的2倍计算。
* 作    者: dzj
* 完成日期: 2010.07.30
*/

#include "Utility.h"
#include "../../../Base/PkgProc/SrvProtocol.h"

class CPlayer;

class RankEventManager
{
private:
	//通知排行榜服务器,排行榜上的一玩家上线
	static void NoticeRankPlayerOnLine( CPlayer* pOnLinePlayer ) {};

	//通知排行榜服务器,排行榜上的一玩家离线
	static void NoticeRankPlayerOffLine( CPlayer* pOffLinePlayer ) {};

	//是否加载排行信息完毕
	static bool IsLoadRankOK() { return m_isReady; }

public:
	//加载排行榜的信息
	static void LoadAllRankChartInfo();
	//收到新的阈值或排行榜序号
	static void OnBiasInfo( const GMBiasInfo* pGMBiasInfo );
	//是否已发送过装载排行榜信息指令
	static bool IsLoadCmdIssued() { return m_isLoadCmdIssued; }

public:
	//当玩家进入地图
	static void OnPlayerEnterMap( CPlayer* pPlayer ) {};

	//当玩家离开地图时
	static void OnPlayerLeaveMap( CPlayer* pPlayer ) {};

	//当接到排行榜更新的消息时
	static void RecvRankPlayerUpdateInfo( const GMRankPlayerInfoUpdate*  pUpdateRankInfo ) {};

private:
	static bool NotifyCacheInfo( ERankType rankType, CPlayer* pPlayer, int newVal );

public: //可升降排行榜，需要同时看旧值与新值是否超过阈值，只升排行榜，只要看新值是否在排行榜内；

	//当玩家的善恶值发生改变时
	static bool MonitorPlayerGoodEvilPointChange( CPlayer* pPlayer, int oldPoint/*旧值*/ );

	//玩家的种族声望
	static bool MonitorPlayerPrestige( CPlayer* pPlayer ) { return true;/*种族已去除，by dzj, 10.08.02*/};

	//监视玩家伤害
	static bool MonitorPlayerDamage( CPlayer* pPlayer, unsigned int damage );

	////监视玩家被伤害
	//static bool MontiorPlayerBeDemage( CPlayer* pPlayer, unsigned int beDemage ) { return true;/*被伤害排行榜去除*/};

	//监视玩家死亡
	static bool MonitorPlayerDie( CPlayer* pPlayer );

	//监视玩家杀人次数
	static bool MonitorPlayerKill( CPlayer* pPlayer );

	//监视玩家金币改变
	static bool MonitorPlayerGoldMoney( CPlayer* pPlayer, unsigned int oldGold/*旧值*/ );

	//监视玩家银币改变
	static bool MonitorPlayerSilverMoney( CPlayer* pPlayer, unsigned int oldSilver/*新值*/ );

	//当玩家的等级发生改变时
	static bool OnPlayerLevelChange( CPlayer* pPlayer );

private:
	static bool         m_isLoadCmdIssued;//是否已发装载命令；
	static bool         m_isReady;//是否准备好(已收到functionsrv相关信息)
	static int          m_arrBias[RANK_TYPE_NUM];
	static unsigned int m_arrRankSeq[ARRAY_SIZE(m_arrBias)];//各排行榜当前序号；
}; // class RankEventManager

