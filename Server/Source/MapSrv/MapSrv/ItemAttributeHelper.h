﻿/********************************************************************
	created:	2010/05/06
	created:	6:5:2010   16:38
	file:		ItemAttributeHelper.h
	author:		管传淇
	
	purpose:	
*********************************************************************/


#ifndef ITEMATTRIBUTE_HELPER_H
#define ITEMATTRIBUTE_HELPER_H

#include "../../Base/PkgProc/SrvProtocol.h"
#include "ItemAttribute.h"

class CBaseItem;
class CPlayer;
class ItemAttributeHelper
{
public:
	static void OnEquipItemAffectPlayer( CPlayer* pPlayer, const ItemInfo_i&  equipItem );

	static void OnUnEquipItemAffectPlayer( CPlayer* pPlayer, const ItemInfo_i& unequipItem );

	static void OnWearPointEmptyAffectPlayer( CPlayer* pPlayer, const ItemInfo_i& wearEmptyitem );

	static void OnRepairWearPointEmtpyItem( CPlayer* pPlayer, const ItemInfo_i& repaireItem );

private:
	static int  GetItemLevelRank( int level ) { return level/10+1; }

	static int  ExecRandItemAttribute( ItemRandAttribute& randAttribute );//, unsigned int& seed);// unsigned int (*pRandFunc)(void)  )

	//装备卓越或幸运属性影响
	static bool _item_lucky_excellent_affect_player( CPlayer* pPlayer, CBaseItem* pItem, const ItemInfo_i& equipItem, unsigned int luckyorexcel/*lucky1或excel2*/, bool isequipitem );

	//static void  _item_luckyattribute_affect_player( CPlayer* pPlayer, CBaseItem* pItem, unsigned int luckseed , bool isequipitem );

	//static void  _item_excellentattribute_affect_player( CPlayer* pPlayer,CBaseItem* pItem , unsigned int excellentseed , bool isequipitem );
};

#endif