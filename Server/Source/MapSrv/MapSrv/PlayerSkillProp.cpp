﻿#include "PlayerSkillProp.h"

#include "PkgProc/CliProtocol.h"
#include "Utility.h"
using namespace MUX_PROTO;



///////////////////////////////////////////////////////////////////////////////////////////////////////////

//读取技能等级点数的配置
bool CSkillExp::LoadFromSkillLevelXML(const char* szFileName)
{
	TRY_BEGIN;

	if ( NULL == szFileName )
	{
		D_ERROR( "CSkillExp::LoadFromSkillLevelXML，input file name NULL\n" );
		return false;
	}

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(szFileName))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;
	//CElement* tmpPropEle = NULL;
	SkillLevelExp tmpSkillLevelExp;	//技能等级经验
	int nSkillLevel = 0;
	unsigned long tempPhyLastLevelExp = 0;
	unsigned long tempMagLastLevelExp = 0;

	unsigned int rootLevel =  pRoot->Level();
	int lastLevel = 0;
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		int curpos = (int) (tmpiter-elementSet.begin());
		if ( tmpEle->Level() == rootLevel+1 )
		{
			if ( !IsXMLValValid( tmpEle, "nLevel", curpos ) )
			{
				return false;
			}		
			nSkillLevel = atoi( tmpEle->GetAttr("nLevel") );

			tmpSkillLevelExp.phyLastLevelExp = tempPhyLastLevelExp;
			if ( !IsXMLValValid( tmpEle, "nPhysicsExp", curpos ) )
			{
				return false;
			}		
			tmpSkillLevelExp.phyNextLevelExp = atoi( tmpEle->GetAttr("nPhysicsExp") );
		

			tmpSkillLevelExp.magLastLevelExp = tempMagLastLevelExp;
			if ( !IsXMLValValid( tmpEle, "nMagicExp", curpos ) )
			{
				return false;
			}		
			tmpSkillLevelExp.magNextLevelExp = atoi( tmpEle->GetAttr("nMagicExp") )	;
			

			if ( !IsXMLValValid( tmpEle, "nSkillTitle", curpos ) )
			{
				return false;
			}
			SafeStrCpy( tmpSkillLevelExp.szSkillTitle, tmpEle->GetAttr("nSkillTitle"));

			if ( !IsXMLValValid( tmpEle, "nPhysicsPoint", curpos ) )
			{
				return false;
			}	

			tmpSkillLevelExp.usMaxGetPhyPoint = atoi( tmpEle->GetAttr("nPhysicsPoint") );

			if ( !IsXMLValValid( tmpEle, "nMagicPoint", curpos ) )
			{
				return false;
			}	

			tmpSkillLevelExp.usMaxGetMagPoint = atoi( tmpEle->GetAttr("nMagicPoint") );
		}
		m_mapSkillLevelExp.insert( make_pair( nSkillLevel,tmpSkillLevelExp) ); //加入配置类
		lastLevel = nSkillLevel;   //记录最后的等级
		//为下一级做准备
		tempPhyLastLevelExp = tmpSkillLevelExp.magNextLevelExp;  
		tempMagLastLevelExp = tmpSkillLevelExp.magNextLevelExp;
	}
	m_usMaxSkillLevel = lastLevel;
	return true;
	TRY_END;
	return false;
}


bool CSkillExp::LoadFromSkillPointXML(const char* szFileName)
{
	TRY_BEGIN;

	if ( NULL == szFileName )
	{
		D_ERROR( "CSkillExp::LoadFromSkillPointXML，input file name NULL\n" );
		return false;
	}

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(szFileName))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;
	//CElement* tmpPropEle = NULL;
	SkillPointExp tmpSkillPointExp;	//技能等级经验
	int nSkillPointNum = 0;
	unsigned long tempLastPhyPoint = 0;
	unsigned long tempLastMagPoint = 0;
	bool bFirstPhy = true;
	bool bFirstMag = true;

	unsigned int rootLevel =  pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		int curpos = (int)(tmpiter-elementSet.begin());
		if ( tmpEle->Level() == rootLevel+1 )
		{
			if ( !IsXMLValValid( tmpEle, "Allocpoint", curpos ) )
			{
				return false;
			}		
			nSkillPointNum = atoi( tmpEle->GetAttr("Allocpoint") );

			tmpSkillPointExp.phyLastPointExp = tempLastPhyPoint;
			if ( !IsXMLValValid( tmpEle, "PhysicalExp", curpos ) )
			{
				return false;
			}		
			tmpSkillPointExp.phyNextPointExp = atoi( tmpEle->GetAttr("PhysicalExp") );
			if( tmpSkillPointExp.phyNextPointExp == 0 && bFirstPhy )
			{
				m_usMaxPhyPoint = nSkillPointNum;
				bFirstPhy = false;
			}

			tmpSkillPointExp.magLastPointExp = tempLastMagPoint;
			if ( !IsXMLValValid( tmpEle, "MagicalExp", curpos ) )
			{
				return false;
			}		
			tmpSkillPointExp.magNextPointExp = atoi( tmpEle->GetAttr("MagicalExp") );
			if( tmpSkillPointExp.magNextPointExp == 0 && bFirstMag )
			{
				m_usMaxMagPoint = nSkillPointNum;
				bFirstMag = false;
			}
	   	}
		m_mapSkillPointExp.insert( make_pair( nSkillPointNum, tmpSkillPointExp )); //加入配置类
		//为下一级做准备
		tempLastPhyPoint = tmpSkillPointExp.phyNextPointExp;
		tempLastMagPoint = tmpSkillPointExp.magNextPointExp;
	}

	return true;
	TRY_END;
	return false;
}


bool CManSkillExp::Init(void)
{
	//职业：剑士的配置
	CSkillExp* pSkillExp = NEW CSkillExp;
	pSkillExp->SetObjID( CLASS_WARRIOR );	
	if( !pSkillExp->LoadFromSkillLevelXML("config/character/warrior/warrior_skill_level.xml") )
	{
		D_ERROR("读取剑士技能等级配置文件出错或者找不到配置文件\n");
		return false;
	}
	D_DEBUG("读取剑士技能等级配置文件成功\n");
	if( !pSkillExp->LoadFromSkillPointXML("config/character/warrior/warrior_skill_point.xml") )
	{
		D_ERROR("读取剑士技能点配置文件出错或者找不到配置文件\n");
		return false;
	}
	AddObject( pSkillExp );
	D_DEBUG("读取剑士技能点配置文件成功\n");

	
	//职业：刺客的配置
	pSkillExp = NEW CSkillExp;	
	pSkillExp->SetObjID( CLASS_ASSASSIN );
	if( !pSkillExp->LoadFromSkillLevelXML("config/character/assassin/assassin_skill_level.xml") )
	{
		D_ERROR("读取刺客技能等级配置文件出错或者找不到配置文件\n");
		return false;
	}
	D_DEBUG("读取刺客技能等级配置文件成功\n");
	if( !pSkillExp->LoadFromSkillPointXML("config/character/assassin/assassin_skill_point.xml") )
	{
		D_ERROR("读取刺客技能点配置文件出错或者找不到配置文件\n");
		return false;
	}
	AddObject( pSkillExp );
	D_DEBUG("读取刺客技能点配置文件成功\n");

		
	//职业：圣骑士的配置
	pSkillExp = NEW CSkillExp;	
	pSkillExp->SetObjID( CLASS_PALADIN );
	if( !pSkillExp->LoadFromSkillLevelXML("config/character/paladin/paladin_skill_level.xml") )
	{
		D_ERROR("读取圣骑士技能等级配置文件出错或者找不到配置文件\n");
		return false;
	}
	D_DEBUG("读取圣骑士技能等级配置文件成功\n");
	if( !pSkillExp->LoadFromSkillPointXML("config/character/paladin/paladin_skill_point.xml") )
	{
		D_ERROR("读取圣骑士配置文件出错或者找不到配置文件\n");
		return false;
	}
	AddObject( pSkillExp );
	D_DEBUG("读取圣骑士技能点配置文件成功\n");

	//职业： 魔剑士的配置
	pSkillExp = NEW CSkillExp;	
	pSkillExp->SetObjID( CLASS_BLADER );
	if( !pSkillExp->LoadFromSkillLevelXML("config/character/blader/blader_skill_level.xml") )
	{
		D_ERROR("读取魔剑士技能等级配置文件出错或者找不到配置文件\n");
		return false;
	}
	D_DEBUG("读取魔剑士技能等级配置文件成功\n");
	if( !pSkillExp->LoadFromSkillPointXML("config/character/blader/blader_skill_point.xml") )
	{
		D_ERROR("读取魔剑士技能点配置文件出错或者找不到配置文件\n");
		return false;
	}
	D_DEBUG("读取魔剑士技能点配置文件成功\n");
	AddObject( pSkillExp );

	//职业：魔法师的配置
	pSkillExp = NEW CSkillExp;	
	pSkillExp->SetObjID( CLASS_MAGE );
	if( !pSkillExp->LoadFromSkillLevelXML("config/character/mage/mage_skill_level.xml") )
	{
		D_ERROR("读取魔法师技能等级配置文件出错或者找不到配置文件\n");
		return false;
	}
	D_DEBUG("读取魔法师技能等级配置文件成功\n");
	if( !pSkillExp->LoadFromSkillPointXML("config/character/mage/mage_skill_point.xml") )
	{
		D_ERROR("读取魔法师技能点配置文件出错或者找不到配置文件\n");
		return false;
	}
	D_DEBUG("读取魔法师技能点配置文件成功\n");
	AddObject( pSkillExp );

	pSkillExp = NEW CSkillExp;	
	pSkillExp->SetObjID( CLASS_HUNTER );
	if( !pSkillExp->LoadFromSkillLevelXML("config/character/hunter/hunter_skill_level.xml") )
	{
		D_ERROR("读取弓箭手技能等级配置文件出错或者找不到配置文件\n");
		return false;
	}
	D_DEBUG("读取弓箭手技能等级配置文件成功\n");
	if( !pSkillExp->LoadFromSkillPointXML("config/character/hunter/hunter_skill_point.xml") )
	{
		D_ERROR("读取弓箭手技能点配置文件出错或者找不到配置文件\n");
		return false;
	}
	D_DEBUG("读取弓箭手技能点配置文件成功\n");
	AddObject( pSkillExp );
	return true;	
}
