﻿/**
* @file otherserver.h
* @brief 定义mapserver上服务器端的其它服务
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: otherserver.h
* 摘    要: 定义mapserver服务器端的其它服务
* 作    者: dzj
* 完成日期: 2007.12.10
*
*/

#include <map>
#include "Utility.h"
#include "BufQueue/BufQueue.h"
#include "PkgProc/PacketBuild.h"
#include "DealG_MPkg.h"

#pragma once
using namespace std;

extern IBufQueue* g_pPkgSender;

#ifdef USE_DSIOCP
extern TCache< UniqueObj< CDsSocket > >* g_poolUniDssocket;//保存socket唯一对象的对象池；
#endif //USE_DSIOCP

#ifdef USE_DSIOCP
    #define CreateServerPkg( PKGTYPE, pPlayer, pPkg ) CPacketBuild::CreatePkg<PKGTYPE>( m_nHandleID, m_nSessionID, GetUniqueDsSocket(), &pPkg );
#else //USE_DSIOCP
    #define CreateServerPkg( PKGTYPE, pPkg ) CPacketBuild::CreatePkg<PKGTYPE>( m_nHandleID, m_nSessionID, &pPkg );
#endif //USE_DSIOCP


class CSrv;
class CSrvConnection
{
public:
	CSrvConnection() : m_nHandleID(0), m_nSessionID(0), m_bySrvType(0x0f)/*0x0ff目前无效的服务标识号*/, m_pSrvBase(NULL) 
	{
#ifdef USE_DSIOCP
		m_pUniqueSocket = NULL;
#endif //USE_DSIOCP
	};
	~CSrvConnection() {};

#ifdef USE_DSIOCP
public:
	void SetUniqueDsSocket( UniqueObj< CDsSocket >* pUniqueSocket )
	{
		if ( NULL == pUniqueSocket )
		{
			return;
		}
		if ( ( NULL == m_pUniqueSocket )
			&& ( NULL != pUniqueSocket->GetUniqueObj() )
			)
		{
			//自己保存一份，因为MsgToPut中的uniqueSocket在回收时会被删去；
			m_pUniqueSocket = g_poolUniDssocket->RetrieveOrCreate();
			m_pUniqueSocket->Init( pUniqueSocket->GetUniqueObj() );
		}		
	}
	UniqueObj< CDsSocket>* GetUniqueDsSocket()
	{
		if ( ( NULL != m_pUniqueSocket ) //非空
			&& ( NULL != m_pUniqueSocket->GetUniqueObj() ) //且对应的句柄仍然有效；
			)
		{
			//复制一份往外传，因为MsgToPut中的uniqueSocket在回收时会被删去；
			UniqueObj< CDsSocket >* pOutUniqueObj = g_poolUniDssocket->RetrieveOrCreate();
			pOutUniqueObj->Init( m_pUniqueSocket->GetUniqueObj() );
			return pOutUniqueObj;
		} else {
			return NULL;
		}
	}
private:
	UniqueObj< CDsSocket >* m_pUniqueSocket;
#endif //USE_DSIOCP

public:
	///置玩家的句柄信息，每次新建时必须调用;
	void SetHandleInfo(/*T_PkgSender* pPkgSender,*/ int nHandleID, int nSessionID );

//private:
//	friend class CSrv;
//	void SetSrvBase( CSrv* pSrvBase )
//	{
//		m_pSrvBase = pSrvBase;
//	}

public:
	//新建立时的处理(连接建立)
	void OnCreated()
	{
		ResetInfo();
		return;
	}

	///销毁时的处理(连接断开)
	///注意：在新建连接之前也会调用这里；
	void OnDestoryed();

	///请求销毁自身(断开连接)
	void ReqDestorySelf();
	

	///如果某连接上解析包错误，则断开此连接；
	void OnPkgError()
	{
		//解析包错误时断开连接；
		ReqDestorySelf();
	}

	///收包处理；
	void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

	int GetHandleID()
	{
		return m_nHandleID;
	}

	int GetSessionID()
	{
		return m_nSessionID;
	}

	///向外发包；
	void SendPkgToSrv( MsgToPut* pPkg )
	{
		if ( NULL != g_pPkgSender )
		{
			g_pPkgSender->PushMsg( pPkg );
			//g_pPkgSender->SetReadEvent();//通知网络模块立即发送出去；
		}
		return;
	}

private:
	///重置自身信息；
	void ResetInfo()
	{
		m_nHandleID = 0;
		m_nSessionID = 0;
	}

private:
	///通信句柄标识，玩家通过该句柄向发包者标识自己的发包对象；
	int m_nHandleID;
	///通信句柄校验号，玩家通过该校验号向发包者校验自己的发包对象；
	int m_nSessionID; 

	///该服务对应的标识号,例如：C、A等，见SrvProtocol.h;
	int m_bySrvType;

	CGateSrv* m_pSrvBase;
	bool  m_bIsDesSelf;//是否发过了删自身请求;
};

class CSrv
{
public:
	CSrv() {};
	//explicit CSrv( CSrvConnection* pConn ) : m_pConn( pConn ) 
	//{
	//	if ( NULL != m_pConn )
	//	{
	//		m_pConn->SetSrvBase( this );
	//	}
	//};
	virtual ~CSrv(){};

	virtual void InitConn( CSrvConnection* pConn ) = 0;

public:
#ifdef USE_DSIOCP
	UniqueObj< CDsSocket>* GetUniqueDsSocket()
	{
		if ( NULL != m_pConn )
		{
		  return m_pConn->GetUniqueDsSocket();//返回连接对应属性；
		} else {
		  return NULL;
		}
	}
#endif //USE_DSIOCP

	int GetHandleID()
	{
		if ( NULL != m_pConn )
		{
		  return m_pConn->GetHandleID();//返回连接对应属性；
		} else {
		  return -1;
		}
	}

	int GetSessionID()
	{
		if ( NULL != m_pConn )
		{
		  return m_pConn->GetSessionID();//返回连接对应属性；
		} else {
		  return -1;
		}
	}

	/////向外发包；
	//void SendPkgToSrv( MsgToPut* pPkg )
	//{
	//	if ( NULL != m_pConn )
	//	{
	//		m_pConn->SendPkgToSrv( pPkg );//转连接处理；
	//	}
	//	return;
	//}


public:
	///断开处理（销毁 ）
	virtual void OnDestory( bool isTrueDesOrInit /*是真正从有效状态销毁还是初始化,真正销毁为true，初始化为false*/ );
	///收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen ) = 0;

protected:
	CSrvConnection* m_pConn;//该服务对应的连接；
};
