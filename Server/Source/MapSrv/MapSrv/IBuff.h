﻿
#pragma once

#include "BuffProp.h"
#include "Utility.h"


class CPlayer;
class CMonster;

enum ECreateType
{
	CREATE_PLAYER,
	CREATE_MONSTER,
	CREATE_NULL,	//新上线的人如果有BUFF也肯定找不到以前释放的人,就用这个TYPE
};

typedef struct StrCreateInfo
{
	StrCreateInfo()
	{
		ResetCreateInfo();
	}

	void ResetCreateInfo();
	
	void SetCreateInfo( ECreateType type, const PlayerID& ID );
	
	ECreateType GetCreateType();
	
	void UpdateCreateInfo();
	
	CPlayer* GetCreatePlayer();
	
	const PlayerID& GetCreateID();
	

private:
	ECreateType createType;	
	PlayerID createID;			//保存人的ID或者怪物的ID,需要每次去寻找,用指针有可能会失效
	CPlayer* pCreatePlayer;
	CMonster* pCreateMonster;
}CreateInfo;

enum BUFF_END_TYPE
{
	NORMAL_END = 0,			//正常时间结束
	BUFF_EXCHANGE = 1,		//BUFF替换导致BUFF结束 	 
};


class IBuff
{
private:
	IBuff( const IBuff& );  //屏蔽这两个操作；
	IBuff& operator = ( const IBuff& );//屏蔽这两个操作；

public:
	virtual void Update() = 0;		//BUFF的更新

	virtual void BuffStart() = 0;	//BUFF的开始

	virtual void BuffEnd() = 0;		//BUFF的结束

	virtual void DotHotProcess() = 0;	//关于DOT HOT的处理

	virtual void Release() = 0;

	IBuff();

	virtual ~IBuff(){}

	void PoolInit()
	{
		m_buffID.uiBuffID = 0;	 //mapsrv下产生BUFF的唯一ID
		m_buffID.uiBuffTypeID = 0;
		m_buffID.uiMapSrvID = 0;
		m_leftTime = ACE_Time_Value::zero;	//BUFF剩余的时间
		m_lastUpdateTime = ACE_Time_Value::zero;	//上次更新的时间
		m_nextDotTime = ACE_Time_Value::zero;	//上次DOT或者HOT的时间,每超过3秒需要做下血值上的处理
		m_pBuffProp = NULL;  //该BUFF影响的属性
		m_bDelFlag = false;	//当此标志被至TRUE，下次轮询会回收
		m_createInfo.ResetCreateInfo();	//创建者的信息
	}


public:
	//设置删除标志量
	void SetDelFlag();

	//获取删除标志量
	bool GetDelFlag();

	//设置BUFF上次的更新时间
	void SetUpdateTime(const ACE_Time_Value& curTime);

	//获取BUFF上次的更新时间
	const ACE_Time_Value& GetUpdateTime();

	void SetCreateInfo( const PlayerID& createID, ECreateType type );		//设置这个BUFF创建者的类型，分无，玩家，或者怪物	

	bool IsDotType();	//是否是扣血类型，扣血类型需要定时发送扣血消息

	const ACE_Time_Value& GetNextDotTime();

	//设置下一次执行dot的时间
	void SetNextDotTime( const ACE_Time_Value& time );

	 //根据现有的时间检查buff是否过期
	bool IsBuffEnd( const ACE_Time_Value& currentTime );

	void SetLeftTime( const ACE_Time_Value& leftTime );

	const ACE_Time_Value& GetLeftTime();

	int GetLeftSecond();	//返回BUFF剩余的秒数

	TYPE_ID GetTypeID();

	TYPE_ID GetMapSrvCreateID();

	void SetBuffProp( CBuffProp* pBuffProp );	//设置BUFF的属性

	ECreateType GetCreateType();

	BUFF_TYPE GetBuffType();

	CBuffProp* GetBuffProp();

	const BuffID& GetBuffID();

	void SetBuffID( const BuffID& buffID );

	int GetBuffVal();

	void UpdateCreateInfo();

	CPlayer* GetCreatePlayer();

	const PlayerID& GetCreateID();

	CBuffProp::PUSH_RULE GetPushRule()
	{
		if( m_pBuffProp )
		{
			return m_pBuffProp->GetPushRule();
		}
		return CBuffProp::RULE_INVALID;
	}

	bool IsDebuff()
	{
		if( m_pBuffProp )
		{
			return m_pBuffProp->IsDebuff();
		}
		return false;
	}

protected:
	BuffID m_buffID;	 //mapsrv下产生BUFF的唯一ID

	ACE_Time_Value m_leftTime;	//BUFF剩余的时间

	ACE_Time_Value m_lastUpdateTime;	//上次更新的时间

	ACE_Time_Value m_nextDotTime;	//上次DOT或者HOT的时间,每超过3秒需要做下血值上的处理

	CBuffProp* m_pBuffProp;  //该BUFF影响的属性

	bool m_bDelFlag;	//当此标志被至TRUE，下次轮询会回收

	CreateInfo m_createInfo;	//创建者的信息
};


class CBuffManager
{
public:
	static void TimeProc();   

	static void InsertBuff(IBuff* pBuff);

	static IBuff* FindBuff( unsigned long lBuffID);

	static unsigned long GenerateBuffID();

		
private:
	static unsigned long m_lBuffID;		//全局的BUFFID,只会递增
	static vector<IBuff*> m_vecBuffs;  //BUFF
};
