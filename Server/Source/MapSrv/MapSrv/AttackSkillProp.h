﻿#pragma once

struct CAttackSkillProp
{
//攻击技能所包含的属性
	float m_fAttackChange;	//攻击的提升或者降低
	int m_nDamageAffix;	//攻击的追加伤害
	float m_fCollapse;	//击倒产生的几率
	float m_fFaint;	//晕眩产生的几率
	float m_fBondage;	//束缚产生的几率
	float m_fCriticalHit;	//暴击产生的几率
	float m_fCriticalDamage;	//攻击暴击的提升系数
	int m_nCriAddDamage;	//暴击附加的伤害
};
