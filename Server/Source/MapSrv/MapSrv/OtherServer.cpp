﻿/**
* @file otherserver.cpp
* @brief 定义服务器端的其它服务
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: otherserver.h
* 摘    要: 定义服务器端的其它服务
* 作    者: dzj
* 完成日期: 2007.12.10
*
*/

#include "G_MProc.h"
#include "PoolManager.h"

extern CPoolManager* g_poolManager;

///置玩家的句柄信息，每次新建时必须调用;
void CSrvConnection::SetHandleInfo( int nHandleID, int nSessionID )
{
	if ( ( nSessionID <=0 )
		|| ( nSessionID > 1000 )
		)
	{
		//错误，srv的nSessionID不应该大于1000;
		//D_WARNING( "错误的nSessionID%d, srv的sessionID应该小于等于1000\n\n", nSessionID );
	}

	m_bIsDesSelf = false;

	m_nHandleID = nHandleID;
	m_nSessionID = nSessionID;		
	m_bySrvType = 0x0f;//注意，此处设为对应的srvType;

	D_INFO("\n*****************设置连接句柄：HandleID-%d,SessionID-%d,SrvType-%d\n",m_nHandleID,m_nSessionID,m_bySrvType);

//	////////////////////////////////////////////////////////////
//	//COMM_BUG
//#include "PkgProc/SrvProtocol.h"
//	using namespace MUX_PROTO;
	///*
	//typedef struct StrMGRun
	//{
	//	static const unsigned char  byPkgType = M_G;
	//	static const unsigned short wCmd = M_G_RUN;
	//	PlayerID lNotiPlayerID;//通知对象ID号；
	//	PlayerID lPlayerID;//玩家ID号；
	//	enum {
	//		RUNRST_OK, //跑动OK
	//		RUNRST_TARGETINVALID //目标点无效；
	//	};
	//	int nErrCode;//RUNRST_OK:跑动正常，RUNRST_TARGETINVALID:目标点无效；
	//	unsigned short nOrgMapPosX;//移动出发点X坐标；
	//	unsigned short nOrgMapPosY;//移动出发点Y坐标；
	//	float fOrgWorldPosX;//移动出发点X客户端坐标；
	//	float fOrgWorldPosY;//移动出发点Y客户端坐标；
	//	unsigned short nNewMapPosX;//移动目标点X坐标；
	//	unsigned short nNewMapPosY;//移动目标点Y坐标；
	//	float fSpeed;//移动速度；
	//	float fNewWorldPosX;//移动目标点X客户端坐标；
	//	float fNewWorldPosY;//移动目标点Y客户端坐标；
	//}MGRun; //mapsrv发往gatesrv的跑动请求消息
	//*/
//	MGRun mgRun;
//	mgRun.lNotiPlayerID.wGID = 0;
//	mgRun.lNotiPlayerID.dwPID = 0x12345;
//	mgRun.lPlayerID.wGID = 0;
//	mgRun.lPlayerID.dwPID = 0x54321;
//	mgRun.nErrCode = NULL;
//	mgRun.nOrgMapPosX = 1;
//	mgRun.nOrgMapPosY = 2;
//	mgRun.fOrgWorldPosX = (float)1.1;
//	mgRun.fOrgWorldPosY = (float)2.2;
//	mgRun.nNewMapPosX = 3;
//	mgRun.nNewMapPosY = 4;
//	mgRun.fSpeed = (float)5.5;
//	mgRun.fNewWorldPosX = 6;
//	mgRun.fNewWorldPosY = 7;
//	for ( int i=0; i<1000; ++i )
//	{
//		MsgToPut* msgToPut = CreateSrvPkg( MGRun, this, mgRun );
//		if ( NULL == msgToPut )
//		{
//			D_WARNING( "创建包失败\n" );
//			break;
//		}
//		SendPkgToSrv( msgToPut );
//		static int nSendNum = 0;
//		++nSendNum;
//		if ( nSendNum % 50 == 0 )
//		{
//			D_WARNING( "发送第%d个移动包成功\n", nSendNum );
//		}
//		
//	}
//	//COMM_BUG
//	////////////////////////////////////////////////////////////

	return;
}

///销毁时的处理(连接断开)
///注意：在新建连接之前也会调用这里；
void CSrvConnection::OnDestoryed()
{
#ifdef USE_DSIOCP
	if ( NULL != m_pUniqueSocket )
	{
		if ( NULL != g_poolUniDssocket )
		{
			g_poolUniDssocket->Release( m_pUniqueSocket );
			m_pUniqueSocket = NULL;
		}
	}
#endif //USE_DSIOCP

	if ( m_nHandleID != 0 )
	{
		//确实为断开，区别于新建连接时的回调；
		//以下断开处理；
		if(m_pSrvBase != NULL)
		{
			m_pSrvBase->OnDestory( true );
			if ( NULL != g_poolManager )
			{
				g_poolManager->ReleaseGateSrv( m_pSrvBase );
			} else {
				D_ERROR( "CSrvConnection::OnDestoryed, g_poolManager空！" );
			}
			m_pSrvBase = NULL;
		}
	}
	ResetInfo();
	return;
}

///收包处理；
void CSrvConnection::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )
{
	///看收的包new东西
	if(m_pSrvBase == NULL)
	{
		//m_pSrvBase = NEW CGateSrv();
		if ( NULL != g_poolManager )
		{
			m_pSrvBase = g_poolManager->RetriveGateSrv();
		} else {
			D_ERROR( "CSrvConnection::OnPkgRcved, g_poolManager空！" );
			return;
		}

		if ( NULL == m_pSrvBase )
		{
			D_ERROR( "CSrvConnection::OnPkgRcved, 取不到新的gatesrv实例，连接的gatesrv数超限！" );
			return;
		}
		
		m_pSrvBase->InitConn( this );
		m_bySrvType = 0x01;
	}

	m_pSrvBase->OnPkgRcved(wCmd,pBuf,wPkgLen);

	return; 
};

void CSrvConnection::ReqDestorySelf()
{
	if ( m_bIsDesSelf )
	{
		return;//已经发过断开请求，就不再请求断开了；
	}
	//发送长度为0消息断开自身；
	if ( NULL == g_poolMsgToPut )
	{
		D_ERROR( "CSrvConnection::ReqDestorySelf, NULL == g_poolMsgToPut\n" );
		return;
	}
	MsgToPut* disconnectMsg = g_poolMsgToPut->RetrieveOrCreate();
	if ( NULL == disconnectMsg )
	{
		D_ERROR( "CSrvConnection::ReqDestorySelf, NULL == disconnectMsg\n" );
		return;
	}
#ifdef USE_DSIOCP
	disconnectMsg->pUniqueSocket = GetUniqueDsSocket();//唯一需要主动设置unisocket者，其余都通过createsrvpkg的方式来生成；
#endif //USE_DSIOCP
	disconnectMsg->nHandleID = m_nHandleID;//向断开执行者传递自身标识；
	disconnectMsg->nSessionID = m_nSessionID;//向断开执行者传递自身标识；
	disconnectMsg->nMsgLen = 0;//表明断开连接请求;
	SendPkgToSrv( disconnectMsg );
	m_bIsDesSelf = true; //标识已经发过断开请求；
	D_WARNING( "本MS主动请求断开SRV(其sessionID:%d)\n", m_nSessionID );
	/*if(m_bySrvType == 0x01)
	{
		if(m_pSrvBase != NULL)
		{
			CGateSrv* pProc = (CGateSrv*)m_pSrvBase;
			pProc->ClearAllCPlayer();
		}
	}*/
	return;
}

///断开处理（销毁 ）
void CSrv::OnDestory( bool isTrueDesOrInit /*是真正从有效状态销毁还是初始化,真正销毁为true，初始化为false*/ )
{
	ACE_UNUSED_ARG( isTrueDesOrInit );
	m_pConn = NULL;
}



