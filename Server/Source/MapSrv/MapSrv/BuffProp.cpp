﻿#include "BuffProp.h"
#include "Utility.h"

CBuffProp::CBuffProp(void)
{
	m_nBuffID = 0;			//技能的ID,通过ID来寻找
	m_nAssistChoice = 0;	//这个BUFF属于人还是怪物
	m_buffIndex = 0;			//这个BUFF规定放在人中的栏位
	m_iVal = 0;					//更新的值int型，如果需要浮点型就用百分比
	m_bDebuff = false;					//是否是有利BUFF,怪物的时候需要用
}

CBuffProp::~CBuffProp(void)
{
}


bool CManBuffProp::Init()
{
	return LoadBuffXML( "config/common/buff.xml" );
}


bool CManBuffProp::LoadBuffXML(const char* pszFileName)
{
	if ( NULL == pszFileName )
	{
		D_ERROR( "CManBuffProp::LoadBuffXML，input file name NULL\n" );
		return false;
	}

	TRY_BEGIN;

	CXmlManager xmlLoader;
	if(!xmlLoader.Load( pszFileName ))
	{
		return false;
	}

	D_INFO("Enter LoadingXml:%s \n", pszFileName );

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}

	CElement* tmpEle = NULL;
	unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		int curpos = (int)(tmpiter - elementSet.begin());
		if ( tmpEle->Level() == rootLevel+1 )
		{
			CBuffProp* pBuffProp = NEW CBuffProp;
	
			if ( !IsXMLValValid( tmpEle, "BuffID", curpos ) )
			{
				delete pBuffProp; pBuffProp = NULL;
				return false;
			}		
			pBuffProp->m_nBuffID = atoi( tmpEle->GetAttr("BuffID") );

			if ( !IsXMLValValid( tmpEle, "AssistChoice", curpos ) )
			{
				delete pBuffProp; pBuffProp = NULL;
				return false;
			}		
			pBuffProp->m_nAssistChoice = atoi( tmpEle->GetAttr("AssistChoice") );

			if ( !IsXMLValValid( tmpEle, "AssistType", curpos ) )
			{
				delete pBuffProp; pBuffProp = NULL;
				return false;
			}		
			pBuffProp->m_buffType = (BUFF_TYPE)atoi( tmpEle->GetAttr("AssistType"));
			switch(pBuffProp->m_buffType)
			{
				case BUFF_STR:
				case BUFF_AGI:
				case BUFF_INT:
					D_ERROR("无效的BuffType，buffID=%d,buffType=%d\n",pBuffProp->m_nBuffID, pBuffProp->m_buffType );
					break;
			}

			if ( !IsXMLValValid( tmpEle, "PushRule", curpos ) )
			{
				delete pBuffProp; pBuffProp = NULL;
				return false;
			}		
			pBuffProp->m_pushRule = (CBuffProp::PUSH_RULE)atoi( tmpEle->GetAttr("PushRule"));

			if ( !IsXMLValValid( tmpEle, "BuffIndex", curpos ) )
			{
				delete pBuffProp; pBuffProp = NULL;
				return false;
			}		
			pBuffProp->m_buffIndex = atoi( tmpEle->GetAttr("BuffIndex"));
		
			if ( !IsXMLValValid( tmpEle, "Value", curpos ) )
			{
				delete pBuffProp; pBuffProp = NULL;
				return false;
			}		
			pBuffProp->m_iVal = atoi( tmpEle->GetAttr("Value"));

			if ( !IsXMLValValid( tmpEle, "nActionSpace", curpos ) )
			{
				delete pBuffProp; pBuffProp = NULL;
				return false;
			}		
			pBuffProp->m_nSpaceTime = atoi( tmpEle->GetAttr("nActionSpace"));

			if ( !IsXMLValValid( tmpEle, "Type", curpos ) )
			{
				delete pBuffProp; pBuffProp = NULL;
				return false;
			}		 
			pBuffProp->m_bDebuff = (atoi( tmpEle->GetAttr("Type")) != 0);


			//最后如果是复合BUFF则再读LINKID
			if( pBuffProp->m_buffType == BUFF_SP_COMPLEX )
			{
				int nLinkID;
				//ID 1
				if ( !IsXMLValValid( tmpEle, "LinkBuffID1", curpos ) )
				{
					delete pBuffProp; pBuffProp = NULL;
					return false;
				}		
				nLinkID = atoi( tmpEle->GetAttr("LinkBuffID1"));
				if( nLinkID != 0 )
				{
					CBuffProp* pTmpProp = CManBuffProp::FindObj( nLinkID );
					if( NULL == pTmpProp )
					{
						D_WARNING("复合ID出错,找不到复合的ID1，出错的BUFFID:%d, link的ID:%d \n", pBuffProp->m_nBuffID, nLinkID );
					}else{
						pBuffProp->PushLinkBuffProp( pTmpProp );
					}
				}

				//ID2
				if ( !IsXMLValValid( tmpEle, "LinkBuffID2", curpos ) )
				{
					delete pBuffProp; pBuffProp = NULL;
					return false;
				}		
				nLinkID = atoi( tmpEle->GetAttr("LinkBuffID2"));
				if( nLinkID != 0)
				{
					CBuffProp* pTmpProp = CManBuffProp::FindObj( nLinkID );
					if( NULL == pTmpProp )
					{
						D_WARNING("复合ID出错,找不到复合的ID2，出错的BUFFID:%d, link的ID:%d \n", pBuffProp->m_nBuffID, nLinkID );
					}else{
						pBuffProp->PushLinkBuffProp( pTmpProp );
					}
				}

				//ID3
				if ( !IsXMLValValid( tmpEle, "LinkBuffID3", curpos ) )
				{
					delete pBuffProp; pBuffProp = NULL;
					return false;
				}		
				nLinkID = atoi( tmpEle->GetAttr("LinkBuffID3"));
				if( nLinkID != 0)
				{
					CBuffProp* pTmpProp = CManBuffProp::FindObj( nLinkID );
					if( NULL == pTmpProp )
					{
						D_WARNING("复合ID出错,找不到复合的ID3，出错的BUFFID:%d, link的ID:%d \n", pBuffProp->m_nBuffID, nLinkID );
					}else{
						pBuffProp->PushLinkBuffProp( pTmpProp );
					}
				}

				//ID4
				if ( !IsXMLValValid( tmpEle, "LinkBuffID4", curpos ) )
				{
					delete pBuffProp; pBuffProp = NULL;
					return false;
				}		
				nLinkID = atoi( tmpEle->GetAttr("LinkBuffID4"));
				if( nLinkID != 0)
				{
					CBuffProp* pTmpProp = CManBuffProp::FindObj( nLinkID );
					if( NULL == pTmpProp )
					{
						D_WARNING("复合ID出错,找不到复合的ID4，出错的BUFFID:%d, link的ID:%d \n", pBuffProp->m_nBuffID, nLinkID );
					}else{
						pBuffProp->PushLinkBuffProp( pTmpProp );
					}
				}

				//ID5
				if ( !IsXMLValValid( tmpEle, "LinkBuffID5", curpos ) )
				{
					delete pBuffProp; pBuffProp = NULL;
					return false;
				}		
				nLinkID = atoi( tmpEle->GetAttr("LinkBuffID5"));
				if( nLinkID != 0)
				{
					CBuffProp* pTmpProp = CManBuffProp::FindObj( nLinkID );
					if( NULL == pTmpProp )
					{
						D_WARNING("复合ID出错,找不到复合的ID5，出错的BUFFID:%d, link的ID:%d \n", pBuffProp->m_nBuffID, nLinkID );
					}else{
						pBuffProp->PushLinkBuffProp( pTmpProp );
					}
				}

				//ID6
				if ( !IsXMLValValid( tmpEle, "LinkBuffID6", curpos ) )
				{
					delete pBuffProp; pBuffProp = NULL;
					return false;
				}		
				nLinkID = atoi( tmpEle->GetAttr("LinkBuffID6"));
				if( nLinkID != 0)
				{
					CBuffProp* pTmpProp = CManBuffProp::FindObj( nLinkID );
					if( NULL == pTmpProp )
					{
						D_WARNING("复合ID出错,找不到复合的ID6，出错的BUFFID:%d, link的ID:%d \n", pBuffProp->m_nBuffID, nLinkID );
					}else{
						pBuffProp->PushLinkBuffProp( pTmpProp );
					}
				}

				//ID7
				if ( !IsXMLValValid( tmpEle, "LinkBuffID7", curpos ) )
				{
					delete pBuffProp; pBuffProp = NULL;
					return false;
				}		
				nLinkID = atoi( tmpEle->GetAttr("LinkBuffID7"));
				if( nLinkID != 0)
				{
					CBuffProp* pTmpProp = CManBuffProp::FindObj( nLinkID );
					if( NULL == pTmpProp )
					{
						D_WARNING("复合ID出错,找不到复合的ID7，出错的BUFFID:%d, link的ID:%d \n", pBuffProp->m_nBuffID, nLinkID );
					}else{
						pBuffProp->PushLinkBuffProp( pTmpProp );
					}
				}

				//ID8
				if ( !IsXMLValValid( tmpEle, "LinkBuffID8", curpos ) )
				{
					delete pBuffProp; pBuffProp = NULL;
					return false;
				}		
				nLinkID = atoi( tmpEle->GetAttr("LinkBuffID8"));
				if( nLinkID != 0)
				{
					CBuffProp* pTmpProp = CManBuffProp::FindObj( nLinkID );
					if( NULL == pTmpProp )
					{
						D_WARNING("复合ID出错,找不到复合的ID8，出错的BUFFID:%d, link的ID:%d \n", pBuffProp->m_nBuffID, nLinkID );
					}else{
						pBuffProp->PushLinkBuffProp( pTmpProp );
					}
				}
			}
			AddObject( pBuffProp );//加入BUFF管理器
		}
	}
	D_INFO("Leaving LoadingXml:%s \n", pszFileName );
	return true;
	TRY_END;
	return false;
}


bool CBuffProp::IsDotType()
{
	if( m_buffType == BUFF_HP_DOT || m_buffType == BUFF_HP_HOT || m_buffType == BUFF_PERCENT_HOT )
	{
		return true;
	}

	if( m_buffType == BUFF_SP_COMPLEX )
	{
		CBuffProp* pTmpProp = NULL;
		for( int i=0; i < (int)m_vecLinkBuffProp.size(); ++i )
		{
			pTmpProp = m_vecLinkBuffProp[i];
			if( NULL == pTmpProp )
				continue;

			if( pTmpProp->m_buffType == BUFF_HP_DOT || pTmpProp->m_buffType == BUFF_HP_HOT
				|| pTmpProp->m_buffType == BUFF_PERCENT_HOT )
			{
				return true;
			}
		}
	}
	return false;
}
