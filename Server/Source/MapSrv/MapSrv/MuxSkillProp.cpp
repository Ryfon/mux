﻿#include "MuxSkillProp.h"
#include "Utility.h"

CMuxSkillProp::CMuxSkillProp():m_skillID(0),
m_skillType(0),
m_upgradeLevel(0),
m_target(0),
m_shape(0),
m_range(0),
m_kickBackRange(0),
m_deadKickBack(false),
m_area(0),
m_upgradePoint(0),
m_publicCDType(0),
m_spLevel(0),
m_skillDegree(0),
m_nUllageSp(0),
m_nUllageMp(0),
m_nUllageMpPer(0),
m_nUllageItem(0),
m_nItemNum(0),
m_nCurseTime(0),
m_nCoolDown(0),
m_nClass(0),
m_nAddExp(0),
m_nNextSkillID(0),
m_nAttDistMin(0),
m_nAttDistMax(0),
m_NpcRank(0),
m_MaxTargetLevel(0),
m_MinTargetLevel(0),
m_BasicSkill(0),
m_upgradeExp(0),
m_upgradeMoney(0),
m_upgradeStr(0),
m_upgradeVit(0),
m_upgradeAgi(0),
m_upgradeSpi(0),
m_upgradeInt(0),
m_subExp(0),
m_subMoney(0),
m_weaponSkill(0),
m_castTime(0),
m_isGoodSkill(false),
m_isNormalSkill(false),
m_isGuildSkill(false),
m_pDamageProp(NULL),
m_pNpcDamageProp(NULL),
m_pMuxItemSkillProp(NULL),
m_pBuffProp(NULL),
m_pMuxSpiltProp(NULL),
m_relieveID(0)
{

}

CMuxSkillProp::~CMuxSkillProp()
{}


bool CMuxSkillProp::IsAoeSkill()
{
	return ( m_shape != 0 );
}


bool CMuxSkillProp::IsGoodSkill()
{
	return m_isGoodSkill;
}


bool CMuxSkillProp::IsGuildSkill()
{
	return m_isGuildSkill;
}


bool CMuxSkillProp::IsNormalSkill()
{
	return m_isNormalSkill;
}


unsigned int CMuxSkillProp::GetAoeRange()
{
	return m_range;
}


bool CMuxSkillProp::IsSpSkill()
{
	return ( m_spLevel != 0);
}


unsigned int CMuxSkillProp::GetNextSkillID()
{
	return m_nNextSkillID;
}


