﻿#include "LogManager.h"
#include "Player/Player.h"
#include "monster/monster.h"
#include "monster/manmonstercreator.h"
#include "../../Base/PkgProc/LogSvcProtocol.h"
#include "G_MProc.h"

unsigned int CLogManager::lcfMoeny = 10000;//金钱
unsigned int CLogManager::lcfSpPowerBean = 1;//sp豆



void CLogManager::DoPlayerLevelUp(CPlayer *pPlayer, unsigned short oldLevel)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_LEVEL_UP;
		logInfo.logInfo.levelUp.Time = (UINT)ACE_OS::time(NULL);
		logInfo.logInfo.levelUp.OldLevel = oldLevel;
		logInfo.logInfo.levelUp.NewLevel = pPlayer->GetLevel();
		logInfo.logInfo.levelUp.RoleID = pPlayer->GetPlayerUID();

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
}

void CLogManager::DoSkillLevelUp(CPlayer *pPlayer, unsigned char skillType, unsigned int skillID, unsigned int oldLevel, unsigned int newLevel)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::SKILL_LEVEL_UP;
		logInfo.logInfo.skillLevelUp.Time = (UINT)ACE_OS::time(NULL);
		logInfo.logInfo.skillLevelUp.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.skillLevelUp.RoleLevel = (BYTE)pPlayer->GetLevel();
		logInfo.logInfo.skillLevelUp.SkillType = skillType;
		logInfo.logInfo.skillLevelUp.OldLevel = oldLevel;
		logInfo.logInfo.skillLevelUp.NewLevel = newLevel;

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
}

void CLogManager::DoGetItem(CPlayer *pPlayer, unsigned char ownType, ItemInfo_i &item)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_GET_ITEM;
		logInfo.logInfo.getItem.Time = (UINT)ACE_OS::time(NULL);
		logInfo.logInfo.getItem.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.getItem.RoleLevel = (BYTE)pPlayer->GetLevel();
		logInfo.logInfo.getItem.OwnType = ownType;
		CopyItemInfo(logInfo.logInfo.getItem.Item, item);

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
}

void CLogManager::DoUseItem(CPlayer *pPlayer, ItemInfo_i &item)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_USE_ITEM;
		logInfo.logInfo.useItem.Time = (UINT)ACE_OS::time(NULL);
		logInfo.logInfo.useItem.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.useItem.RoleLevel = (BYTE)pPlayer->GetLevel();
		CopyItemInfo(logInfo.logInfo.useItem.Item, item);

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
}

void CLogManager::DoDropItem(CPlayer *pPlayer, ItemInfo_i &item)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_DROP_ITEM;
		logInfo.logInfo.dropItem.Time = (UINT)ACE_OS::time(NULL);
		logInfo.logInfo.dropItem.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.dropItem.RoleLevel = (BYTE)pPlayer->GetLevel();
		CopyItemInfo(logInfo.logInfo.dropItem.Item, item);

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
}

void CLogManager::DoTaskChanage(CPlayer *pPlayer, unsigned int taskId, unsigned char state)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID  = MGLogPlayerInfo::ROLE_TASK_CHANAGE;
		logInfo.logInfo.taskChanage.Time = (UINT)ACE_OS::time(NULL);
		logInfo.logInfo.taskChanage.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.taskChanage.RoleLevel = (BYTE)pPlayer->GetLevel();
		logInfo.logInfo.taskChanage.QuestID = taskId;
		logInfo.logInfo.taskChanage.ChangeType = state;

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
}

void CLogManager::DoPlayerDead(CPlayer *pPlayer, unsigned char killerType, unsigned int killerID, unsigned int dropExp, unsigned int dropCount, ItemInfo_i items[])
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_DEAD;
		logInfo.logInfo.playerDead.Time = (UINT)ACE_OS::time(NULL);
		logInfo.logInfo.playerDead.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.playerDead.RoleLevel = (BYTE)pPlayer->GetPhyLevel();
		logInfo.logInfo.playerDead.MapID = pPlayer->GetMapID();
		logInfo.logInfo.playerDead.MapX = pPlayer->GetPosX();
		logInfo.logInfo.playerDead.MapY = pPlayer->GetPosY();
		logInfo.logInfo.playerDead.KillerType = killerType;
		logInfo.logInfo.playerDead.KillerID = killerID;
		logInfo.logInfo.playerDead.DropExp = dropExp;

		logInfo.logInfo.playerDead.DropItemCount = 0;
		for(unsigned int i = 0; i < dropCount; i++ )
		{
			CopyItemInfo(logInfo.logInfo.playerDead.DropItem[logInfo.logInfo.playerDead.DropItemCount], items[i]);
			logInfo.logInfo.playerDead.DropItemCount++ ;		
			if(logInfo.logInfo.playerDead.DropItemCount == sizeof(logInfo.logInfo.playerDead.DropItem)/sizeof(logInfo.logInfo.playerDead.DropItem[0]))
			{
				pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
				logInfo.logInfo.playerDead.DropItemCount = 0 ;
			}
		}

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
}

void CLogManager::DoSwitchMap(CPlayer *pPlayer, unsigned int oldMapID, unsigned int newMapID)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_MAP_CHANAGE;
		logInfo.logInfo.mapChanage.Time = (UINT)ACE_OS::time(NULL);
		logInfo.logInfo.mapChanage.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.mapChanage.RoleLevel = (BYTE)pPlayer->GetLevel();
		logInfo.logInfo.mapChanage.OldMapID = oldMapID;
		logInfo.logInfo.mapChanage.NewMapID = newMapID;

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
}

void CLogManager::DoTradeItem(CPlayer *pPlayer, CPlayer *pPeerPlayer, ItemInfo_i ownerItems[], unsigned int ownItemCount, ItemInfo_i peerItems[], unsigned int peerItemCount, unsigned int ownerMoney, unsigned int peerMoney)
{
	if((NULL != pPlayer) && (NULL != pPeerPlayer))
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_TRADE;
		logInfo.logInfo.trade.Time = (UINT)ACE_OS::time(NULL);
		logInfo.logInfo.trade.UnitA.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.trade.UnitA.RoleLevel = (BYTE)pPlayer->GetLevel();
		logInfo.logInfo.trade.UnitA.Money = ownerMoney;
		logInfo.logInfo.trade.UnitB.RoleID = pPeerPlayer->GetPlayerUID();
		logInfo.logInfo.trade.UnitB.RoleLevel = (BYTE)pPeerPlayer->GetLevel();
		logInfo.logInfo.trade.UnitB.Money = peerMoney;
		
		if((0 == ownItemCount) && (0 == peerItemCount))//交易没有物品
		{
			if((0 != ownerMoney) || (0 != peerMoney))
				pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);//只有金钱
		}
		else//交易有物品
		{
			unsigned int maxCountPerUnit = sizeof(logInfo.logInfo.trade.UnitA.Item)/sizeof(logInfo.logInfo.trade.UnitA.Item[0]);
			unsigned int ownIndex = 0;
			unsigned int peerIndex = 0;
			while((ownIndex < ownItemCount) || (peerIndex < peerItemCount))
			{
				logInfo.logInfo.trade.UnitA.ItemCount = 0;
				logInfo.logInfo.trade.UnitB.ItemCount = 0;
				
				if(ownIndex < ownItemCount)//未结束
				{
					for(unsigned int i = 0; i < maxCountPerUnit; i++)
					{
						CopyItemInfo(logInfo.logInfo.trade.UnitA.Item[i], ownerItems[ownIndex]);
						logInfo.logInfo.trade.UnitA.ItemCount++;
						ownIndex++;
						if(ownIndex == ownItemCount)
							break;
					}
				}
				
				if(peerIndex < peerItemCount)//未结束
				{
					for(unsigned int i = 0; i < maxCountPerUnit; i++)
					{
						CopyItemInfo(logInfo.logInfo.trade.UnitB.Item[i], peerItems[peerIndex]);
						logInfo.logInfo.trade.UnitB.ItemCount++;
						peerIndex++;
						if(peerItemCount == peerItemCount)
							break;
					}
				}

				if((0 != logInfo.logInfo.trade.UnitA.ItemCount) || (0 != logInfo.logInfo.trade.UnitB.ItemCount))
					pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
			}
		}
	}

	return;
}

void CLogManager::CopyItemInfo(LOG_SVC_NS::ItemInfo_i &dst, MUX_PROTO::ItemInfo_i &src)
{
	dst.UUID = src.uiID;
	dst.TypeID = src.usItemTypeID;
	dst.WearPoint = src.usWearPoint;
	//去除追加，原位置用于随机集 dst.AddId = src.randSet/*usAddId*/;
	dst.AddId = 0;
	dst.Count = src.ucCount;
	dst.LevelUp = src.ucLevelUp;

	return;
}

void CLogManager::DoSpPowerChange(CPlayer *pPlayer, unsigned char changeType, unsigned char changeNum)
{
	if(changeNum < lcfSpPowerBean)
		return;

	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_SP_POWER_CHANGE;
		logInfo.logInfo.spChange.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.spChange.RoleLevel = (BYTE)pPlayer->GetLevel();
		logInfo.logInfo.spChange.ChangeNum = changeNum;
		logInfo.logInfo.spChange.ChangeType = changeType;
		logInfo.logInfo.spChange.Time = Now();

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
	return ;
}

void CLogManager::DoItemPropertyChange(CPlayer *pPlayer, unsigned char propertyType,  unsigned int itemUId, unsigned itemTypeID, unsigned int oldValue, unsigned newValue)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_ITEM_PROP_CHANGE;
		logInfo.logInfo.itemChange.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.itemChange.RoleLevel = (BYTE)pPlayer->GetLevel();
		logInfo.logInfo.itemChange.Time = Now();
		logInfo.logInfo.itemChange.PropertyType = propertyType;
		logInfo.logInfo.itemChange.ItemUUID = itemUId;
		logInfo.logInfo.itemChange.ItemTypeID = itemTypeID;
		logInfo.logInfo.itemChange.OldValue = oldValue;
		logInfo.logInfo.itemChange.NewValue = newValue;

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
	return ;
}

void CLogManager::DoMoneyChange(CPlayer *pPlayer, unsigned char changeType, unsigned char reasonType, unsigned char moneyType, unsigned int moneyCount)
{
	if(moneyCount < lcfMoeny)
		return;

	if(NULL != pPlayer)
	{
		MGLogPlayerInfo logInfo;
		logInfo.logID = MGLogPlayerInfo::ROLE_MONEY_CHANGE;
		logInfo.logInfo.moneyChange.RoleID = pPlayer->GetPlayerUID();
		logInfo.logInfo.moneyChange.RoleLevel = (BYTE)pPlayer->GetLevel();
		logInfo.logInfo.moneyChange.Time = Now();
		logInfo.logInfo.moneyChange.ChangeType = changeType;
		logInfo.logInfo.moneyChange.ReasonType = reasonType;
		logInfo.logInfo.moneyChange.MoneyType = moneyType;
		logInfo.logInfo.moneyChange.MoneyCount = moneyCount;

		pPlayer->SendPkg<MGLogPlayerInfo>(&logInfo);
	}
	return ;

}

void CLogManager::DoPetAdd(CPlayer *pPlayer, unsigned int petID)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::ROLE_PET_ADD;
		loginfo.logInfo.petAdd.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.petAdd.RoleLevel = (BYTE)pPlayer->GetLevel();
		loginfo.logInfo.petAdd.PetID = petID;
		loginfo.logInfo.petAdd.Time = Now();

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}
}

void CLogManager::DoPetLevelUp(CPlayer *pPlayer, unsigned int petID, unsigned int oldValue, unsigned int newValue)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::ROLE_PET_LEVEL_UP;
		loginfo.logInfo.petLevelUp.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.petLevelUp.RoleLevel = (BYTE)pPlayer->GetLevel();
		loginfo.logInfo.petLevelUp.PetID = petID;
		loginfo.logInfo.petLevelUp.Time = Now();
		loginfo.logInfo.petLevelUp.OldLevel = oldValue;
		loginfo.logInfo.petLevelUp.NewLevel = newValue;

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}

void CLogManager::DoPetGainSkill(CPlayer *pPlayer, unsigned int petID, unsigned int skillID)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::ROLE_PET_GAIN_SKILL;
		loginfo.logInfo.petAddSkill.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.petAddSkill.RoleLevel = (BYTE)pPlayer->GetLevel();
		loginfo.logInfo.petAddSkill.PetID = petID;
		loginfo.logInfo.petAddSkill.FunctionID = skillID;
		loginfo.logInfo.petAddSkill.Time = Now();

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}

void CLogManager::DoPetStateChange(CPlayer *pPlayer, unsigned int petID, unsigned char stateType)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::ROLE_PET_STATE_CHANGE;
		loginfo.logInfo.petStateChange.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.petStateChange.RoleLevel = (BYTE)pPlayer->GetLevel();
		loginfo.logInfo.petStateChange.PetID = petID;
		loginfo.logInfo.petStateChange.Time = Now();
		loginfo.logInfo.petStateChange.PetID = petID;
		loginfo.logInfo.petStateChange.StateType = stateType;

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}

void CLogManager::DoUseSkill(CPlayer *pPlayer, unsigned char skillType, unsigned int skillID, bool isSpSkill)
{
	if(NULL != pPlayer)
	{
		//只记录sp,物理，魔法技能
		if(isSpSkill || (TYPE_PHYSICS == skillType) || (TYPE_MAGIC == skillType))
		{
			MGLogPlayerInfo loginfo;
			loginfo.logID = MGLogPlayerInfo::ROLE_USE_SKILL;
			loginfo.logInfo.useskill.RoleID = pPlayer->GetPlayerUID();
			loginfo.logInfo.useskill.RoleLevel = (BYTE)pPlayer->GetLevel();
			loginfo.logInfo.useskill.Time = Now();
			loginfo.logInfo.useskill.SkillID = skillID;
			if(isSpSkill)
				loginfo.logInfo.useskill.SkillType = LOG_SVC_NS::ST_SP;
			else if(TYPE_PHYSICS == skillType)
				loginfo.logInfo.useskill.SkillType = LOG_SVC_NS::ST_PHYSICS; 
			else if(TYPE_MAGIC == skillType)
				loginfo.logInfo.useskill.SkillType = LOG_SVC_NS::ST_MAGIC;

			pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
		}
	}

	return;
}

void CLogManager::DoKillPlayer(CPlayer *pPlayer, CPlayer *pBeKilledPlayer)
{
	if((NULL != pPlayer) &&(NULL != pBeKilledPlayer))
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::ROLE_KILL_PLAYER;
		loginfo.logInfo.killPlayer.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.killPlayer.RoleLevel = (BYTE)pPlayer->GetLevel();
		loginfo.logInfo.killPlayer.Time = Now();
		loginfo.logInfo.killPlayer.PlayerID = pBeKilledPlayer->GetPlayerUID();
		loginfo.logInfo.killPlayer.PlayerLevel = (BYTE)pBeKilledPlayer->GetLevel();

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}

void CLogManager::DoPlayerRebirth(CPlayer *pPlayer, unsigned int mapID, unsigned int posX, unsigned int posY)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::ROLE_REBIRTH;
		loginfo.logInfo.rebirth.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.rebirth.RoleLevel = (BYTE)pPlayer->GetLevel();
		loginfo.logInfo.rebirth.Time = Now();
		loginfo.logInfo.rebirth.MapID = mapID;
		loginfo.logInfo.rebirth.MapX = posX;
		loginfo.logInfo.rebirth.MapY = posY;

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}

void CLogManager::DoNpcShopPurchase(CPlayer *pPlayer,  unsigned int shopID, unsigned char purchaseType, unsigned int money, unsigned int itemType, unsigned int itemNum)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::NPC_SHOP_TRADE;
		loginfo.logInfo.npcShop.Time = Now();
		loginfo.logInfo.npcShop.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.npcShop.ShopID = shopID;
		loginfo.logInfo.npcShop.SaleBuy = purchaseType;
		loginfo.logInfo.npcShop.Money = money;
		loginfo.logInfo.npcShop.ItemTypeID = itemType;
		loginfo.logInfo.npcShop.ItemNum = itemNum;

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}

void CLogManager::DoRepairItem(CPlayer *pPlayer, unsigned int shopID, unsigned int money, unsigned int itemUid, unsigned int itemTypeID)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::ROLE_REPAIR_ITEM;
		loginfo.logInfo.repairItem.RoleID = pPlayer->GetPlayerUID();
		//loginfo.logInfo.repairItem.RoleLevel = (BYTE)pPlayer->GetLevel();
		loginfo.logInfo.repairItem.Time = Now();
		loginfo.logInfo.repairItem.ShopID = shopID;
		loginfo.logInfo.repairItem.Money = money;
		loginfo.logInfo.repairItem.ItemUUID = itemUid;
		loginfo.logInfo.repairItem.ItemTypeID = itemTypeID;

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}

void CLogManager::DoMonsterDead(CPlayer *pPlayer, CMonster *pMonster)
{
	if((NULL != pPlayer) &&(NULL != pMonster))
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::MONSTER_DIE;
		loginfo.logInfo.monsterDead.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.monsterDead.Time = Now();
		loginfo.logInfo.monsterDead.MapID = pPlayer->GetMapID();
		CCreatorIns *pCreator = pMonster->GetOwnCreator();
		if(NULL != pCreator)
			loginfo.logInfo.monsterDead.CreaterID = pCreator->GetCreatorID();
		loginfo.logInfo.monsterDead.MonsterID = pMonster->GetClsID();
		loginfo.logInfo.monsterDead.KillerID = pPlayer->GetPlayerUID();
		loginfo.logInfo.monsterDead.KillerLevel = pPlayer->GetLevel();

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}

void CLogManager::DoInstanceChange(unsigned int instanceID, unsigned int worldID, unsigned char changeType)
{
	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL != pGateSrv )
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::INSTANCE_CHANGE;
		loginfo.logInfo.instanceChange.Time = Now();
		loginfo.logInfo.instanceChange.InstanceID = instanceID;
		loginfo.logInfo.instanceChange.WorldID = worldID;
		loginfo.logInfo.instanceChange.CyclePoint = 0;
		loginfo.lNotiPlayerID.dwPID = 0;
		loginfo.lNotiPlayerID.wGID = 0;
		
		MsgToPut* pNewMsg = CreateSrvPkg( MGLogPlayerInfo, pGateSrv, loginfo );
		pGateSrv->SendPkgToGateServer( pNewMsg );
	}

	return;

}

//仓库打开
void CLogManager::DoWarehouseOpen(CPlayer *pPlayer, unsigned int warehouseID, unsigned char hasPasswd)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::WAREHOUSE_OPEN;
		loginfo.logInfo.warehouseOpen.Time = Now();
		loginfo.logInfo.warehouseOpen.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.warehouseOpen.WarehouseID = warehouseID;
		loginfo.logInfo.warehouseOpen.HasPassword = hasPasswd;
		loginfo.logInfo.warehouseOpen.MapID = pPlayer->GetMapID();
		loginfo.logInfo.warehouseOpen.MapX = pPlayer->GetPosX();
		loginfo.logInfo.warehouseOpen.MapY = pPlayer->GetPosY();

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}


//从仓库中存取物品
void CLogManager::DoWarehouseOperateItem(CPlayer *pPlayer, unsigned int warehouseID, unsigned char operateType, unsigned int itemUid, unsigned int itemTypeId, unsigned itemCount)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::WAREHOUSE_ITEM_OPERATE;
		loginfo.logInfo.warehouseItemOperate.Time = Now();
		loginfo.logInfo.warehouseItemOperate.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.warehouseItemOperate.WarehouseID = warehouseID;
		loginfo.logInfo.warehouseItemOperate.TransportType = operateType;
		loginfo.logInfo.warehouseItemOperate.ItemUUID = itemUid;
		loginfo.logInfo.warehouseItemOperate.ItemTypeID = itemTypeId;
		loginfo.logInfo.warehouseItemOperate.ItemNum = itemCount;

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}


//从仓库中存取金钱
void CLogManager::DoWarehouseOperateMoney(CPlayer *pPlayer, unsigned int warehouseID, unsigned char operateType, unsigned int money)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::WAREHOUSE_MONEY_OPERATE;
		loginfo.logInfo.warehouseMoneyOperate.Time = Now();
		loginfo.logInfo.warehouseMoneyOperate.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.warehouseMoneyOperate.WarehouseID = warehouseID;
		loginfo.logInfo.warehouseMoneyOperate.TransportType = operateType;
		loginfo.logInfo.warehouseMoneyOperate.Money = money;

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}


//仓库扩展
void CLogManager::DoWarehouseExtend(CPlayer *pPlayer, unsigned int warehouseID, unsigned int num)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::WAREHOUSE_EXTEND;
		loginfo.logInfo.warehouseExtend.Time = Now();
		loginfo.logInfo.warehouseExtend.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.warehouseExtend.WarehouseID = warehouseID;
		loginfo.logInfo.warehouseExtend.SlotNum = num;

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}


//仓库保护
void CLogManager::DoWareHousePrivate(CPlayer *pPlayer, unsigned int warehouseID, unsigned char operateType, unsigned char result)
{
	if(NULL != pPlayer)
	{
		MGLogPlayerInfo loginfo;
		loginfo.logID = MGLogPlayerInfo::WAREHOUSE_PRIVATE;
		loginfo.logInfo.warehousePrivate.Time = Now();
		loginfo.logInfo.warehousePrivate.RoleID = pPlayer->GetPlayerUID();
		loginfo.logInfo.warehousePrivate.WarehouseID = warehouseID;
		loginfo.logInfo.warehousePrivate.PrivateType = operateType;
		loginfo.logInfo.warehousePrivate.Result = result;

		pPlayer->SendPkg<MGLogPlayerInfo>(&loginfo);
	}

	return;
}
