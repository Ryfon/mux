﻿/**
* @file npcshop.cpp
* @brief npc商店
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: npcshop.cpp
* 摘    要: npc商店
* 作    者: dzj
* 完成日期: 2008.11.24
*
*/

#include "npcshop.h"

#include "XmlManager.h"

#include "../Player/Player.h"
#include "../LogManager.h"

map<unsigned long, CNpcShop*> CNpcShopManager::m_arrNpcShop;//NPC商店集合；
ACE_Time_Value CNpcShopManager::m_lastSupplyTime;

bool CNpcShopManager::IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
{
	if ( NULL == pEle || NULL == attName )
	{
		return false;
	}
	if ( NULL == pEle->GetAttr( attName ) )
	{
		D_ERROR( "读NPC商店XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
		return false;
	}
	return true;
}

///NPC技能商店配置文件读取；
bool CNpcShopManager::LoadSkillShopFromXML( const char* skillShopConfigFile )
{
	TRY_BEGIN;

	CXmlManager xmlLoader;
	if( !xmlLoader.Load(skillShopConfigFile) )
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;
	unsigned long shopID = 0;

	unsigned int rootLevel = pRoot->Level();
	CNpcShop* pSkillShop = NULL;
	unsigned long shopNum = 0;//当前读取的为第几个技能商店；

	unsigned long curShopID = 0;//当前技能商店ID号；
	float         curPriceRate = 1;//当前技能商店折扣率；
	unsigned long curCurrencyType = 0;//当前技能商店使用的货币；
	unsigned long curTradeItemID = 0;//当前技能商店使用的交易物；

	unsigned long curLabel  = 0;//当前标签序号；
	unsigned long curLabelPos = 0;//当前标签的数组访问位置(curLabel-1)
	unsigned long curItemPos    = 0;//当前标签中的商品位置；
/*
	unsigned long m_itemID;//商品ID；
*/

	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			if ( NULL != pSkillShop )
			{
				pSkillShop->InitShop();//初始化待发送信息（信息中包含各标签的各商品数量）
				pSkillShop->SetSkillShop();
			}

			//新shop层;
			if ( !IsXMLValValid( tmpEle, "ID", shopNum ) )
			{
				return false;
			}
			++shopNum;
			curShopID = atoi( tmpEle->GetAttr( "ID" ) );//准备读一个新的商店；
			/*
			unsigned long curPriceRate = 1;//当前商店折扣率；
			unsigned long curCurrencyType = 0;//当前商店使用的货币；
			unsigned long curTradeItemID = 0;//当前商店使用的交易物；
			*/
			if ( !IsXMLValValid( tmpEle, "PriceRate", shopNum ) )
			{
				return false;
			}
			curPriceRate = (float)atof( tmpEle->GetAttr( "PriceRate" ) );//商店折扣率；
			if ( !IsXMLValValid( tmpEle, "CurrencyType", shopNum ) )
			{
				return false;
			}
			curCurrencyType = atoi( tmpEle->GetAttr( "CurrencyType" ) );//商店货币类型；
			if ( !IsXMLValValid( tmpEle, "TradeItemID", shopNum ) )
			{
				return false;
			}
			curTradeItemID = atoi( tmpEle->GetAttr( "TradeItemID" ) );//商店货币物品；

			curLabel  = 0;//准备读一个新的商店；
			curItemPos    = 0;
			if ( m_arrNpcShop.find( shopID ) != m_arrNpcShop.end() )
			{
				//商店ID号重复；
				D_ERROR( "技能商店XML配置文件中的商店ID号%d重复配置\n", shopID );
				return false;
			}

			pSkillShop = NEW CNpcShop;//建一个新的shop实例并加入管理器；
			pSkillShop->SetSkillShop();
			pSkillShop->SetShopID( curShopID );
			pSkillShop->SetPriceRate( curPriceRate );
			pSkillShop->SetCurrencyType( curCurrencyType );
			pSkillShop->SetTradeItemID( curTradeItemID );
			m_arrNpcShop.insert( pair<unsigned long, CNpcShop*>( pSkillShop->GetShopID(), pSkillShop ) );
			//以下继续读入该商店的各个标签；
			continue;
		}

		if ( tmpEle->Level() == rootLevel+2 )
		{
			//标签层；
			++curLabel;//当前为第几个标签；
			curLabelPos = curLabel - 1;
			if ( curLabelPos >= ARRAY_SIZE(pSkillShop->m_arrShopLabels) )
			{
				D_WARNING( "2读第%d个技能商店配置时，该商店的标签数超出了预定的限制%d\n", shopNum, LABELNUM_PER_SHOP );
				return false;
			}
			pSkillShop->m_arrShopLabels[curLabelPos].SetSelfInfo( pSkillShop->GetShopID(), curLabelPos );//设置本标签的所属商店信息以及自身在商店中的标签序号信息；
			curItemPos = 0;//起始位置；
			continue;
		}

		if ( tmpEle->Level() == rootLevel+3 )
		{			
			if ( curItemPos >= ARRAY_SIZE(pSkillShop->m_arrShopLabels[curLabelPos].m_arrShopItems) )
			{
				D_WARNING( "第%d个技能商店的第%d个标签的商品数量超出限额%d\n", shopNum, curLabel, ITEMNUM_PER_SHOPLABEL );
				return false;
			}

			//商店商品层,当前商店为pShop，当前标签为curLabel(数组标签为curLabel-1)；
			/*<Skill SkillID="3111030" Level="1" TradePrice="200" Profession="0" />*/
			if ( !IsXMLValValid( tmpEle, "SkillID", shopNum ) )
			{
				D_WARNING( "第%d个技能商店的SkillID无效\n", shopNum );
				return false;
			}
			pSkillShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_itemID = atoi( tmpEle->GetAttr( "SkillID" ) );
			if ( !IsXMLValValid( tmpEle, "TradePrice", shopNum ) )
			{
				D_WARNING( "第%d个技能商店的TradePrice无效\n", shopNum );
				return false;
			}
			pSkillShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_tradePrice = atoi( tmpEle->GetAttr( "TradePrice" ) );
			pSkillShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].SetSkillItem();
#ifdef PET_SKILL_SHOP
			if ( !IsXMLValValid( tmpEle, "SkillType", shopNum ) )
			{
				D_WARNING( "第%d个技能商店的SkillType无效\n", shopNum );
				return false;
			}
			int tmpSkillType = atoi( tmpEle->GetAttr( "SkillType" ) );
			if ( 1 == tmpSkillType )
			{
				pSkillShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].SetPetSkillFlag();
			} else if ( 2 == tmpSkillType ) {
				//默认即为玩家技能，无需重设；
			} else {
				D_WARNING( "第%d个技能商店的SkillType%d非法(有效值1:宠物技能,2:玩家技能)\n", shopNum, tmpSkillType );
				return false;
			}
#endif //PET_SKILL_SHOP
			++curItemPos;//准备读下一个商品；
		}
	}

	if ( NULL != pSkillShop )
	{
		pSkillShop->InitShop();//初始化待发送信息（信息中包含各标签的各商品数量）
	}

	return true;
	TRY_END;
	return false;
}

///NPC商店配置文件读取；
bool CNpcShopManager::LoadNormalShopFromXML( const char* shopConfigFile )
{
	TRY_BEGIN;

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(shopConfigFile))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;
	unsigned long shopID = 0;

	unsigned int rootLevel = pRoot->Level();
	CNpcShop* pShop = NULL;
	unsigned long shopNum = 0;//当前读取的为第几个商店；

	unsigned long curShopID = 0;//当前商店ID号；
	float         curPriceRate = 1;//当前商店折扣率；
	unsigned long curCurrencyType = 0;//当前商店使用的货币；
	unsigned long curTradeItemID = 0;//当前商店使用的交易物；

	unsigned long curLabel  = 0;//当前标签序号；
	unsigned long curLabelPos = 0;//当前标签的数组访问位置(curLabel-1)
	unsigned long curItemPos    = 0;//当前标签中的商品位置；
/*
	unsigned long m_itemID;//商品ID；
	unsigned long m_perPurchaseNum;//单次购买数量；
	unsigned long m_tradePrice;//交易价格；
	unsigned long m_produceType;//商品产出方式类型；
	unsigned long m_dailyProduceHour;//天产出时的时间点，小时；
	unsigned long m_dailyProduceMin;//天产出时的时间点，分钟；
	unsigned long m_produceNum;//每次产出数量；
	unsigned long m_capabilityNum;//商店中该商品的最大仓储量；
*/

	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			if ( NULL != pShop )
			{
				pShop->InitShop();//初始化待发送信息（信息中包含各标签的各商品数量）
			}

			//新shop层;
			if ( !IsXMLValValid( tmpEle, "ID", shopNum ) )
			{
				return false;
			}
			++shopNum;
			curShopID = atoi( tmpEle->GetAttr( "ID" ) );//准备读一个新的商店；
			/*
			unsigned long curPriceRate = 1;//当前商店折扣率；
			unsigned long curCurrencyType = 0;//当前商店使用的货币；
			unsigned long curTradeItemID = 0;//当前商店使用的交易物；
			*/
			if ( !IsXMLValValid( tmpEle, "PriceRate", shopNum ) )
			{
				return false;
			}
			curPriceRate = (float)atof( tmpEle->GetAttr( "PriceRate" ) );//商店折扣率；
			if ( !IsXMLValValid( tmpEle, "CurrencyType", shopNum ) )
			{
				return false;
			}
			curCurrencyType = atoi( tmpEle->GetAttr( "CurrencyType" ) );//商店货币类型；
			if ( !IsXMLValValid( tmpEle, "TradeItemID", shopNum ) )
			{
				return false;
			}
			curTradeItemID = atoi( tmpEle->GetAttr( "TradeItemID" ) );//商店货币物品；

			curLabel  = 0;//准备读一个新的商店；
			curItemPos    = 0;
			if ( m_arrNpcShop.find( shopID ) != m_arrNpcShop.end() )
			{
				//商店ID号重复；
				D_ERROR( "商店XML配置文件中的商店ID号%d重复配置\n", shopID );
				return false;
			}

			pShop = NEW CNpcShop;//建一个新的shop实例并加入管理器；
			pShop->SetShopID( curShopID );
			pShop->SetPriceRate( curPriceRate );
			pShop->SetCurrencyType( curCurrencyType );
			pShop->SetTradeItemID( curTradeItemID );
			m_arrNpcShop.insert( pair<unsigned long, CNpcShop*>( pShop->GetShopID(), pShop ) );
			//以下继续读入该商店的各个标签；
			continue;
		}

		if ( tmpEle->Level() == rootLevel+2 )
		{
			//标签层；
			++curLabel;//当前为第几个标签；
			curLabelPos = curLabel - 1;
			if ( curLabelPos >= ARRAY_SIZE(pShop->m_arrShopLabels) )
			{
				D_WARNING( "2读第%d个商店配置时，该商店的标签数超出了预定的限制%d\n", shopNum, LABELNUM_PER_SHOP );
				return false;
			}
			pShop->m_arrShopLabels[curLabelPos].SetSelfInfo( pShop->GetShopID(), curLabelPos );//设置本标签的所属商店信息以及自身在商店中的标签序号信息；
			curItemPos = 0;//起始位置；
			continue;
		}

		if ( tmpEle->Level() == rootLevel+3 )
		{			
			if ( curItemPos >= ITEMNUM_PER_SHOPLABEL )
			{
				D_WARNING( "第%d个商店的第%d个标签的商品数量超出限额%d\n", shopNum, curLabel, ITEMNUM_PER_SHOPLABEL );
				return false;
			}
			if ( curLabelPos >= ARRAY_SIZE(pShop->m_arrShopLabels) ) 
			{
				D_WARNING( "第%d个商店的第%d个标签，curLabelPos(%) >= ARRAY_SIZE(pShop->m_arrShopLabels)\n", shopNum, curLabel, curLabelPos );
				return false;
			}
			if ( curItemPos >= ARRAY_SIZE(pShop->m_arrShopLabels[curLabelPos].m_arrShopItems) )
			{
				D_WARNING( "第%d个商店的第%d个标签，curItemPos(%) >= ARRYA_SIZE(pShop->m_arrShopLabels[curLabelPos].m_arrShopItems)\n", shopNum, curLabel, curItemPos );
				return false;
			}

			//商店商品层,当前商店为pShop，当前标签为curLabel(数组标签为curLabel-1)；
			/*<ShopItem ItemID="220001030" TradePrice="500" ProduceType="1" ProduceNum="10" Capability="50" />*/
			if ( !IsXMLValValid( tmpEle, "ItemID", shopNum ) )
			{
				return false;
			}
			pShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_itemID = atoi( tmpEle->GetAttr( "ItemID" ) );
			if ( !IsXMLValValid( tmpEle, "PerPurchaseNum", shopNum ) )
			{
				return false;
			}
			pShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_perPurchaseNum = atoi( tmpEle->GetAttr( "PerPurchaseNum" ) );
			if ( !IsXMLValValid( tmpEle, "TradePrice", shopNum ) )
			{
				return false;
			}
			pShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_tradePrice = atoi( tmpEle->GetAttr( "TradePrice" ) );
			if ( !IsXMLValValid( tmpEle, "ProduceType", shopNum ) )
			{
				return false;
			}
			pShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_produceType = atoi( tmpEle->GetAttr( "ProduceType" ) );

			if ( !IsXMLValValid( tmpEle, "DailyProduceTime", shopNum ) )
			{
				return false;
			}
			int dailyTime = atoi( tmpEle->GetAttr( "DailyProduceTime" ) );
			pShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_dailyProduceHour = dailyTime / 100;//产出时刻(小时)
			pShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_dailyProduceMin  = dailyTime % 100;//产出时刻(分钟)
			if ( !IsXMLValValid( tmpEle, "ProduceNum", shopNum ) )
			{
				return false;
			}
			pShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_produceNum = atoi( tmpEle->GetAttr( "ProduceNum" ) );
			if ( !IsXMLValValid( tmpEle, "Capability", shopNum ) )
			{
				return false;
			}	
			pShop->m_arrShopLabels[curLabelPos].m_arrShopItems[curItemPos].m_capabilityNum = atoi( tmpEle->GetAttr( "Capability" ) );
			++curItemPos;//准备读下一个商品；
		}
	}

	if ( NULL != pShop )
	{
		pShop->InitShop();//初始化待发送信息（信息中包含各标签的各商品数量）
	}

	return true;
	TRY_END;
	return false;
}

///取指定ID号NPC商店；
CNpcShop* CNpcShopManager::FindNpcShop( unsigned long shopID )
{
	map<unsigned long, CNpcShop*>::iterator iter = m_arrNpcShop.find( shopID );
	if ( iter != m_arrNpcShop.end() )
	{
		return iter->second;
	}

	//没有指定ＩＤ号商店；
	D_WARNING( "FindNpcShop，没有指定ＩＤ为%d的ＮＰＣ商店\n", shopID );
	return NULL;
}

///显示指定ＩＤ号ＮＰＣ商店；
bool CNpcShopManager::ShowNpcShop( CPlayer* pPlayer, unsigned long shopID, unsigned long shopPageID )
{
	CNpcShop* pNpcShop = FindNpcShop( shopID );
	if ( NULL == pNpcShop )
	{
		return false;
	}

	return pNpcShop->PlayerAskForShopLabel( pPlayer, shopID, shopPageID );//显示npc商店时，直接给玩家显示第一页的商品；
}

///从NPC商店中购买物品请求
bool CNpcShopManager::NpcShopBuy( CPlayer* pPlayer, const GMShopBuyReq& shopBuyReq )
{
	if ( NULL == pPlayer )
	{
		D_WARNING( "NpcShopBuy, 输入玩家指针空" );
		return false;
	}

	CNpcShop* pNpcShop = FindNpcShop( shopBuyReq.shopID );
	if ( NULL == pNpcShop )
	{
		D_WARNING( "NpcShopBuy,找不到指定ID号为%d的npc商店\n", shopBuyReq.shopID );
		return false;
	}

	if ( pNpcShop->IsNormalShop() )
	{
		return pNpcShop->PlayerBuyShopItem( pPlayer, shopBuyReq.shopID, shopBuyReq.shopPageID/*商品页码*/, shopBuyReq.shopPagePos/*商品在页中位置*/
			, shopBuyReq.itemID/*商品类型ID号*/, shopBuyReq.itemNum/*一次购买的商品数量*/, shopBuyReq.moneyType/*货币类型ID号*/, shopBuyReq.moneyID, shopBuyReq.moneyNum/*购买使用的货币数量*/ );
	} else {
		return pNpcShop->PlayerBuySkillShopItem( pPlayer, shopBuyReq.shopID, shopBuyReq.shopPageID/*商品页码*/, shopBuyReq.shopPagePos/*商品在页中位置*/
			, shopBuyReq.itemID/*商品类型ID号*/, shopBuyReq.moneyType/*货币类型*/, shopBuyReq.moneyID/*货币ID号*/, shopBuyReq.moneyNum/*购买使用的货币数量*/ );
	}

}

///每隔15分钟检测一次是否需要补充，检测三种类型：每小时，每天，每N天；
void CShopItem::QuarterSupplyCheck()
{
	if ( PRODUCE_INFINITE == m_produceType )
	{
		//无限产出不需要补充；
		m_bIsNeedSupply = false;
		return;
	}
	if ( m_curNum >= m_capabilityNum )
	{
		//当前数量已达容量上限；
		m_bIsNeedSupply = false;
		return;
	}

	unsigned int supplyunit = 0;//补充单位数（大部分情况下应为1，否则说明检测时间精度不够）
	time_t curTimeT = ACE_OS::time();

	if ( PRODUCE_PERHOUR == m_produceType )
	{
		//每小时产出；			
		unsigned int diffTime = (unsigned int)ACE_OS::difftime( curTimeT, m_lastSupplyTime );
		if ( diffTime < 3600 )
		{
			//小于1小时，还未到补充时刻；
			m_bIsNeedSupply = true;
			return;
		}
		//超过了1小时，进行补充;
		unsigned int passedhour = diffTime / 3600;
		supplyunit = passedhour;
		m_lastSupplyTime = curTimeT;

	} else if ( PRODUCE_PERDAY == m_produceType ) {

		//每天产出；
		tm curTm = *(ACE_OS::localtime( &curTimeT ));
		tm lastTm = *(ACE_OS::localtime( &m_lastSupplyTime ));

		if ( curTm.tm_mday < lastTm.tm_mday ) 
		{
			//当天日期比上次更新日期小，如果月份不一样，则说明已跨月，否则为不可能错误；
			if ( curTm.tm_mon == lastTm.tm_mon )
			{
				m_bIsNeedSupply = true;
				return;
			}
		} else if ( curTm.tm_mday == lastTm.tm_mday ) {
			//上次更新日期就是今天；
			//首先检测今天是否更新过，如果已经更新过则直接返回；
			if ( m_bIsDaySupplyed )
			{
				m_bIsNeedSupply = true;
				return;//当天已更新过，无需再更新；
			}
		}

		//新的一天，或者今天还未补充过；			
		//检测是否已达到指定时刻；
		if ( ( curTm.tm_hour < m_dailyProduceHour ) //还没到指定的小时；
			|| 
			( curTm.tm_hour == m_dailyProduceHour ) && ( curTm.tm_min < m_dailyProduceMin ) //已到指定小时，但还没到指定分钟；
			)
		{
			//还没到当天的补充时刻
			m_bIsNeedSupply = true;
			return;
		}
		//已到指定时刻；
		supplyunit = 1;//一天只补充一个单位；
		m_lastSupplyTime = curTimeT;//保存最后更新时间；
		m_bIsDaySupplyed = true;//今天已经更新过了；

	} else if ( PRODUCE_NDAY <= m_produceType ) {

		//每N天产出；
		tm curTm = *(ACE_OS::localtime( &curTimeT ));
		tm lastTm = *(ACE_OS::localtime( &m_lastSupplyTime ));

		//如果产出，应该每隔N天产出一次；
		unsigned int nDaySec = ( m_produceType-(PRODUCE_NDAY-2) ) * 24 * 3600;
		unsigned long passedsec = (unsigned long)ACE_OS::difftime( curTimeT, m_lastSupplyTime );
		if ( passedsec < nDaySec )
		{
			m_bIsNeedSupply = true;
			return;
		}

		//检测是否已达到指定时刻；
		if ( ( curTm.tm_hour < m_dailyProduceHour ) //还没到指定的小时；
			|| 
			( curTm.tm_hour == m_dailyProduceHour ) && ( curTm.tm_min < m_dailyProduceMin ) //已到指定小时，但还没到指定分钟；
			)
		{
			//还没到补充时刻
			m_bIsNeedSupply = true;
			return;
		}
		//已到指定时刻；
		supplyunit = 1;//一天只补充一个单位；
		m_lastSupplyTime = curTimeT;//保存最后更新时间；
		m_bIsDaySupplyed = true;//今天已经更新过了；
	} else {
		//未定义类型；
		m_bIsNeedSupply = false;
		return;
	}

	unsigned int supplynum = supplyunit * m_produceNum;

	m_curNum = m_curNum+supplynum;
	if ( m_curNum >= m_capabilityNum )
	{
		m_curNum = m_capabilityNum;
		m_bIsNeedSupply = false;//已经补满；
	} else {
		m_bIsNeedSupply = true;//尚未补满，仍需补充；
	}

	return;
}

///每隔1小时检测一次是否需要补充，检测一种类型：每N个小时；
void CShopItem::HourSupplyCheck()
{
	if ( PRODUCE_INFINITE == m_produceType )
	{
		//无限产出不需要补充；
		m_bIsNeedSupply = false;
		return;
	}
	if ( m_curNum >= m_capabilityNum )
	{
		//当前数量已达容量上限；
		m_bIsNeedSupply = false;
		return;
	}

	unsigned int supplyunit = 0;//补充单位数（大部分情况下应为1，否则说明检测时间精度不够）
	time_t curTimeT = ACE_OS::time();

	if ( (PRODUCE_NHOUR) <= m_produceType ) 
	{

		//如果产出，应该每隔N小时产出一次；
		unsigned int nHourSec = ( m_produceType-(PRODUCE_NHOUR-2) ) * 3600;
		unsigned int passedsec = (unsigned int)ACE_OS::difftime( curTimeT, m_lastSupplyTime );
		if ( passedsec < nHourSec ) 
		{
			//小于N小时，还未到补充时刻；
			m_bIsNeedSupply = true;
			return;
		}
		//超过了N小时，进行补充;
		supplyunit = passedsec / nHourSec;
		m_lastSupplyTime = curTimeT;//保存最后更新时间；

	} else {
		//未定义类型；
		m_bIsNeedSupply = false;
		return;
	}

	unsigned int supplynum = supplyunit * m_produceNum;

	m_curNum = m_curNum+supplynum;
	if ( m_curNum >= m_capabilityNum )
	{
		m_curNum = m_capabilityNum;
		m_bIsNeedSupply = false;//已经补满；
	} else {
		m_bIsNeedSupply = true;//尚未补满，仍需补充；
	}

	return;
}

//检查输入商品各属性，如果与本商品相符，则返回真并将商品数-1，否则返回假；
GCShopBuyRst::BuyRstType CShopItem::CheckAndBuyItem( unsigned long shopPagePos/*商品在页中位置*/, unsigned long itemID/*商品类型ID号*/
					 , unsigned long itemNum/*一次购买的商品数量*/, unsigned long moneyNum/*购买使用的货币数量*/ )
{
	TRY_BEGIN;

	if ( 0 == m_itemID )
	{
		D_WARNING( "CheckAndBuyItem,指定的购买位置%d无商品\n", shopPagePos );
		return GCShopBuyRst::ERROR_ITEM;	
	}

	if ( itemID != m_itemID )
	{
		D_WARNING( "CheckAndBuyItem,所购买的商品ID号%d与本item的相应信息不符\n", itemID );
		return GCShopBuyRst::ERROR_ITEM;	
	}

	if ( IsNormalItem() )
	{
		if ( itemNum != m_perPurchaseNum )
		{
			D_WARNING( "CheckAndBuyItem,所购买的商品一次性数量%d与本item的相应信息不符\n", itemNum );
			return GCShopBuyRst::ERROR_ITEM;	
		}
	} else if ( 1 != itemNum ) {
		D_WARNING( "CheckAndBuyItem,技能类商品%d一次只能购买一个\n", itemID );
		return GCShopBuyRst::ERROR_ITEM;
	}

	if ( moneyNum != m_tradePrice )
	{
		D_WARNING( "CheckAndBuyItem,所购买的商品代价%d与本item的相应信息不符\n", moneyNum );
		return GCShopBuyRst::ERROR_ITEM;	
	}

	if ( IsNormalItem() ) 
	{
		//普通商品需要检查店中物品数量是否足够；
		//所有检查都过，检查是否有足够数量item；
		if ( PRODUCE_INFINITE == m_produceType )
		{
			//无限产出不需要检查数量；
			return GCShopBuyRst::BUY_SUCCESS;
		} else {
			if ( m_curNum <=0 )
			{
				D_WARNING( "CheckAndBuyItem,所购买的商品%d目前没有仓储\n", itemID );
				return GCShopBuyRst::NOT_ENOUGH_ITEM;//数量不够；
			} else {
				-- m_curNum;
				m_bIsNeedSupply = true;//有物品被买走，置补充标记；
				return GCShopBuyRst::BUY_SUCCESS;//购买成功，已从仓储中减去了购买物品的数量；
			}
		}
	} else {
		//技能商品无需检查数量，直接返回购买成功；
		return GCShopBuyRst::BUY_SUCCESS;
	}

	TRY_END;
	return GCShopBuyRst::OTHER_ERROR;
}

///刷新页showShop消息中特定位置的item数目；
void CShopLabel::UpdateShowShopMsg( unsigned long itemPos )
{
	TRY_BEGIN;

	if ( ( itemPos >= ARRAY_SIZE(m_showShop.itemNum) )
		|| ( itemPos >= ARRAY_SIZE(m_arrShopItems) )
		)
	{
		D_WARNING( "CShopLabel::UpdateShowShopMsg, ITEM位置%d超限\n", itemPos );
		return;
	}

	m_showShop.itemNum[itemPos] = m_arrShopItems[itemPos].GetCurNum();

	return;
	TRY_END;
	return;
}

///取特定位置物品的价格;
bool CShopLabel::GetShopItemPrice( unsigned long shopPos, unsigned long& outPrice )
{
	if ( shopPos >= ARRAY_SIZE(m_arrShopItems) )
	{
		D_WARNING( "GetShopItemPrice，shopPos%d越界", shopPos );
		outPrice = 999999;
		return false;
	}

	outPrice = m_arrShopItems[shopPos].GetShopItemPrice();
	return true;
}

#ifdef PET_SKILL_SHOP
///指定位置是否为宠物技能；
bool CShopLabel::IsShopItemPetSkill( unsigned long shopPos )
{
	if ( shopPos >= ARRAY_SIZE(m_arrShopItems) )
	{
		D_WARNING( "IsShopItemPetSkill，shopPos%d越界", shopPos );
		return false;
	}

	return m_arrShopItems[shopPos].IsPetSkillFlagSet();
}
#endif //PET_SKILL_SHOP

///刷新MGShowShop消息
void CShopLabel::InitShowLabelInfo()
{
	TRY_BEGIN;

	m_showShop.shopID = GetSelfShopID();
	m_showShop.shopPageID = GetSelfLabelID();
	for ( int i=0; i<ITEMNUM_PER_SHOPLABEL; ++i )
	{
		if ( ( i >= ARRAY_SIZE(m_showShop.itemNum) )
			|| ( i >= ARRAY_SIZE(m_arrShopItems) )
			)
		{
			D_ERROR( "CShopLabel::InitShowLabelInfo, i(%d)越界\n", i );
			return;
		}
		m_showShop.itemNum[i] = m_arrShopItems[i].GetCurNum();
	}

	return;
	TRY_END;
	return;
}

///初始化补充标记, 输出值：初始化后，标签中是否有物品要补充；
void CShopLabel::InitLabelSupplyFlag()
{
	if ( !m_bIsNeedSupply )
	{
		return;//无需补充;
	}
	m_bIsNeedSupply = false;
	for ( int i=0; i<ARRAY_SIZE(m_arrShopItems); ++i )
	{
		m_arrShopItems[i].InitItemSupplyFlag();
		if ( m_arrShopItems[i].IsItemNeedSupply() )
		{
			m_bIsNeedSupply = true;//只要有任何一个商品需要补充，则本标签需要补充；
		}
	}
	return;
}

///补充标签中物品；
void CShopLabel::TrySupplyLabelItem( unsigned long timerInterval/*单位秒*/ ) 
{
	if ( !m_bIsNeedSupply )
	{
		return;//无需补充;
	}
	m_bIsNeedSupply = false;
	for ( int i=0; i<ITEMNUM_PER_SHOPLABEL; ++i )
	{
		if ( ( i >= ARRAY_SIZE(m_arrShopItems) )
			|| ( i >= ARRAY_SIZE(m_showShop.itemNum) )
			)
		{
			D_ERROR( "CShopLabel::TrySupplyLabelItem, i(%d)越界\n", i );
			return;
		}
		m_arrShopItems[i].TrySupplyItemItem( timerInterval );
		m_showShop.itemNum[i] = m_arrShopItems[i].GetCurNum();//试补充后即时更新通知消息；
		if ( m_arrShopItems[i].IsItemNeedSupply() )
		{
			m_bIsNeedSupply = true;
		}
	}

	return; 
}

///商店标签购买；
GCShopBuyRst::BuyRstType CShopLabel::ShopLabelCheckAndBuy( unsigned long shopPageID/*商品页码*/
						  , unsigned long shopPagePos/*商品在页中位置*/, unsigned long itemID/*商品类型ID号*/
						  , unsigned long itemNum/*一次购买的商品数量*/, unsigned long moneyNum/*购买使用的货币数量*/ )
{
	TRY_BEGIN;

	if ( shopPageID != m_selfLabelID )
	{
		//请求的标签号与本标签不符，此购买请求不应到本页；
		D_WARNING( "ShopLabelCheckAndBuy，请求的标签号%d与本标签不符，购买失败!\n", shopPageID );
		return GCShopBuyRst::OTHER_ERROR;
	}

	if ( shopPagePos >= ARRAY_SIZE(m_arrShopItems) )
	{
		//商品在页中位置不正确，购买请求错；
		D_WARNING( "ShopLabelCheckAndBuy，请求的商品页中位置%d越界，购买失败!\n", shopPagePos );
		return GCShopBuyRst::OTHER_ERROR;
	}

	return m_arrShopItems[shopPagePos].CheckAndBuyItem( shopPagePos, itemID, itemNum, moneyNum );
	TRY_END;
	return GCShopBuyRst::OTHER_ERROR;
}

//向玩家发送本页的物品数量列表；
bool CShopLabel::SendShopItemsToPlayer( CPlayer* pPlayer )
{
	TRY_BEGIN;

	pPlayer->SendPkg< MGShowShop > ( &m_showShop );

	return true;
	TRY_END;
	return false;
}

///刷新本商店中每一标签的MGShowShop信息，以备玩家查询时返回；
void CNpcShop::InitShowShopInfo()
{
	for( int i=0; i<ARRAY_SIZE(m_arrShopLabels); ++i )
	{
		m_arrShopLabels[i].InitShowLabelInfo();
	}

	return;
}

///初始化补充标记, 输出值：初始化后，商店中是否有物品要补充；
void CNpcShop::InitShopSupplyFlag()
{
	if ( !IsNormalShop() )
	{
		///只有普通商品商店才需要补充，技能商店无需补充也不需要商店通知消息；
		m_bIsNeedSupply = false;
		return;
	}

	if ( !m_bIsNeedSupply )
	{
		return;//无需补充;
	}
	m_bIsNeedSupply = false;
	for( int i=0; i<ARRAY_SIZE(m_arrShopLabels); ++i )
	{
		m_arrShopLabels[i].InitLabelSupplyFlag();
		if ( m_arrShopLabels[i].IsLabelNeedSupply() )
		{
			m_bIsNeedSupply = true;//只要有任何一个标签需要补充，则整个商店需要补充；
		}
	}

	return;
}

///补充商店中物品；
void CNpcShop::TrySupplyShopItem( unsigned long passedsec )
{
	if ( !IsNormalShop() )
	{
		///只有普通商品商店才需要补充，技能商店无需补充也不需要商店通知消息；
		return;
	}

	if ( !m_bIsNeedSupply )
	{
		return;//无需补充;
	}
	m_bIsNeedSupply = false;
	for( int i=0; i<ARRAY_SIZE(m_arrShopLabels); ++i )
	{
		m_arrShopLabels[i].TrySupplyLabelItem( passedsec );
		if ( m_arrShopLabels[i].IsLabelNeedSupply() )
		{
			m_bIsNeedSupply = true;//只要有任何一个标签需要补充，则整个商店需要补充；
		}
	}

	return;
}

///向玩家发送第n页的物品数量列表；
bool CNpcShop::SendShopLabelToPlayer( CPlayer* pPlayer, unsigned long labelID )
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CNpcShop::SendShopLabelToPlayer, NULL == pPlayer\n" );
		return false;
	}

	if ( labelID >= ARRAY_SIZE(m_arrShopLabels) )
	{
		D_ERROR( "CNpcShop::SendShopLabelToPlayer, labelID(%d) >= ARRAY_SIZE(m_arrShopLabels)\n", labelID );
		return false;
	}

	m_arrShopLabels[labelID].SendShopItemsToPlayer( pPlayer );
	return true;
}

///玩家请求第shopPageID页物品数量列表；
bool CNpcShop::PlayerAskForShopLabel( CPlayer* pPlayer, unsigned long shopID, unsigned long shopPageID )
{
	if ( NULL == pPlayer )
	{
		D_WARNING( "PlayerAskForShopLabel, 玩家指针空\n" );
		return false;
	}

	if ( shopID != m_shopID )
	{
		D_WARNING( "PlayerAskForShopLabel,输入的商店ＩＤ号与本商店不符!\n" );
		return false;
	}

	if ( shopPageID >= LABELNUM_PER_SHOP )
	{
		D_WARNING( "PlayerAskForShopLabel, 输入的商品页号%d超限\n", shopPageID );
		return false;
	}

	return SendShopLabelToPlayer( pPlayer, shopPageID );
}

GCShopBuyRst::BuyRstType CNpcShop::PlayerBuyCheckBuy( CPlayer* pPlayer, unsigned long shopID/*商店ID*/, unsigned long shopPageID/*商品页码*/
					   , unsigned long shopPagePos/*商品在页中位置*/, unsigned long itemID/*商品类型ID号*/
					   , unsigned long itemNum/*一次购买的商品数量*/, unsigned long moneyType/*货币类型*/, unsigned long moneyID/*货币ID号*/
					   , unsigned long useMoneyNum/*准备使用的货币数量*/ 
#ifdef PET_SKILL_SHOP
					   , bool& isPetSkill/*所购是否为宠物技能*/ 
#endif //PET_SKILL_SHOP
					   )
{
	TRY_BEGIN;

	GCShopBuyRst::BuyRstType pageBuyRst = GCShopBuyRst::OTHER_ERROR;

	if ( NULL == pPlayer )
	{
		D_WARNING( "PlayerBuyShopItem, pPlayer指针空\n" );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return pageBuyRst;
	}

	if ( shopID != m_shopID )
	{
		//请求的商店号与本商店不符，此购买请求不应到本商店
		D_WARNING( "PlayerBuyShopItem, 请求的商店号%d与本商店不符\n", shopID );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return pageBuyRst;
	}

	if ( shopPageID >= LABELNUM_PER_SHOP )
	{
		D_WARNING( "PlayerBuyShopItem, 请求的页号%d超限\n", shopPageID );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return pageBuyRst;
	}

	if ( shopPagePos >= ITEMNUM_PER_SHOPLABEL )
	{
		D_WARNING( "PlayerBuyShopItem, 请求的位置号%d超限\n", shopPagePos );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return pageBuyRst;
	}

	/*
	１、检查购买请求中的货币类型是否与本商店相符，检查玩家身上的货币类型与数量是否足够；
	２、检查玩家身上包裹位置是否足够；
	３、进入商店页，商店页决定玩家指定页面是否有对应物品，对应物品的相应属性是否与玩家传来信息一致，如果检查成功，还要从可售物品中扣去相应的物品数目；
	*/
	if ( shopPageID >= ARRAY_SIZE(m_arrShopLabels) )
	{
		D_WARNING( "PlayerBuyShopItem, shopPageID(%d) >= ARRAY_SIZE(m_arrShopLabels)\n", shopPagePos );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return pageBuyRst;
	}

#ifdef PET_SKILL_SHOP
	isPetSkill = m_arrShopLabels[shopPageID].IsShopItemPetSkill( shopPagePos );
#endif //PET_SKILL_SHOP

	unsigned long itemPrice = 999999;
	if ( ! m_arrShopLabels[shopPageID].GetShopItemPrice( shopPagePos, itemPrice ) )
	{
		D_WARNING( "PlayerBuyShopItem, 取不到指定位置%d的物品价格\n", shopPagePos );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return pageBuyRst;
	}

	float fNeedPrice = itemPrice * m_priceRate;
	unsigned long ulNeedPrice = (unsigned long) fNeedPrice;
	if ( fabs( fNeedPrice - ulNeedPrice*1.0 ) > 0.00001 )
	{
		++ulNeedPrice; 
	}

	if ( ulNeedPrice != useMoneyNum )
	{
		//准备用于购买的货币数目不正确；
		D_WARNING( "准备用于购买的货币数目不正确%d\n", useMoneyNum );
		pageBuyRst = GCShopBuyRst::ERROR_ITEM;
		return pageBuyRst;
	}

	//１、检查购买请求中的货币类型是否与本商店相符，检查玩家身上的货币类型与数量是否足够；
	if ( moneyType != 2 )
	{
		if ( moneyType != m_currencyType )
		{
			//货币类型不正确；
			D_WARNING( "购买请求中的货币类型%d与本商店不符1\n", moneyType );
			pageBuyRst = GCShopBuyRst::ERROR_ITEM;
			return pageBuyRst;
		}
		if ( moneyType == 0 )
		{
			//货币类型游戏币；
			if ( pPlayer->GetSilverMoney() < useMoneyNum ) //双币种购买判断
			{
				//金钱数量不够；
				pageBuyRst = GCShopBuyRst::NOT_ENOUGH_MONEY;
				return pageBuyRst;
			}
		}else if( moneyType == 4 )	//货币种类为4的时候使用种族声望
		{
			if( pPlayer->GetRacePrestige() < useMoneyNum )
			{
				//金钱数量不够；
				pageBuyRst = GCShopBuyRst::NOT_ENOUGH_MONEY;
				return pageBuyRst;
			}
		}else {
			D_WARNING( "PlayerBuyShopItem,目前没有%d类型货币，请检查配置\n", moneyType );
			pageBuyRst = GCShopBuyRst::ERROR_ITEM;
			return pageBuyRst;
		}
	} else {
		//以物易物；
		if ( 2 != m_currencyType )
		{
			//本商店非以物易物商店；
			D_WARNING( "非以物易物商店中，购买请求中的货币类型%d与本商店不符2\n", moneyType );
			pageBuyRst = GCShopBuyRst::ERROR_ITEM;
			return pageBuyRst;
		}
		if ( moneyID != m_tradeItemID ) 
		{
			//以物易物货币类型不正确；
			D_WARNING( "购买请求中的货币类型%d与本商店不符2\n", moneyType );
			pageBuyRst = GCShopBuyRst::ERROR_ITEM;
			return pageBuyRst;
		}
		if ( !(pPlayer->ItemNumReachTargetNum( m_tradeItemID, useMoneyNum )) )
		{
			//货币数量不够；
			D_WARNING( "玩家%s购买物品%d时，货币%d数量不够\n", pPlayer->GetAccount(), itemID, m_tradeItemID );
			pageBuyRst = GCShopBuyRst::NOT_ENOUGH_MONEY;
			return pageBuyRst;
		}
	}

	if ( IsNormalShop() ) //购买普通物品时检查玩家身上的包裹空间是否足够；
	{		
		//２、检查玩家身上包裹位置是否足够；
		if ( !(pPlayer->CanGetItem( itemID, itemNum )) )
		{
			D_WARNING( "玩家%s购买物品%d时，包裹空间不够\n", pPlayer->GetAccount(), itemID );
			pageBuyRst = GCShopBuyRst::NOT_ENOUGH_SPACE;
			return pageBuyRst;
		}
	}

	//３、进入商店页，商店页决定玩家指定页面是否有对应物品，对应物品的相应属性是否与玩家传来信息一致，如果检查成功，还要从可售物品中扣去相应的物品数目；
	pageBuyRst = m_arrShopLabels[shopPageID].ShopLabelCheckAndBuy( shopPageID/*商品页码*/, shopPagePos/*商品在页中位置*/
		, itemID/*商品类型ID号*/, itemNum/*一次购买的商品数量*/,  itemPrice/*购买使用的货币数量(等价于按此价钱购买，即前面价格比较时用于计算的商品价格)*/ );
	
	return pageBuyRst;
	TRY_END;
	return GCShopBuyRst::OTHER_ERROR;
}

///玩家请求购买第shopPageID页的itempos位置技能；
bool CNpcShop::PlayerBuySkillShopItem( CPlayer* pPlayer, unsigned long shopID/*商店ID*/, unsigned long shopPageID/*商品页码*/
									  , unsigned long shopPagePos/*商品在页中位置*/, unsigned long skillID/*商品类型ID号*/
									  , unsigned long moneyType/*货币类型ID号*/, unsigned long moneyID/*货币类型ID号*/
									  , unsigned long moneyNum/*购买使用的货币数量*/ )
{
	TRY_BEGIN;

    GCShopBuyRst::BuyRstType pageBuyRst = GCShopBuyRst::OTHER_ERROR;

	if ( NULL == pPlayer )
	{
		D_WARNING( "PlayerBuySkillShopItem, pPlayer指针空\n" );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return false;
	}

	if ( ( shopPageID >= LABELNUM_PER_SHOP )
		|| ( shopPagePos >= ITEMNUM_PER_SHOPLABEL )
		)
	{
		D_WARNING( "PlayerBuySkillShopItem, 玩家%s指定的购买标签%d或位置%d超限\n", pPlayer->GetNickName(), shopPageID, shopPagePos );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return false;
	}

#ifdef PET_SKILL_SHOP
	bool isPetSkill = false;
	pageBuyRst = PlayerBuyCheckBuy( pPlayer, shopID, shopPageID, shopPagePos, skillID, 1, moneyType, moneyID, moneyNum, isPetSkill );
#else //PET_SKILL_SHOP
	pageBuyRst = PlayerBuyCheckBuy( pPlayer, shopID, shopPageID, shopPagePos, skillID, 1, moneyType, moneyID, moneyNum );
#endif //PET_SKILL_SHOP

	bool isBuySkillOK = false;
	if ( GCShopBuyRst::BUY_SUCCESS == pageBuyRst )
	{
		//商店检测条件已过，与普通商品不一样的是，此时需要检查是否可以给玩家加指定技能，为此直接为玩家加技能，加技能结果作为购买结果；
#ifdef PET_SKILL_SHOP
		if ( isPetSkill )
		{
			isBuySkillOK = pPlayer->PetSkillLearn( skillID );
		} else {
			isBuySkillOK = pPlayer->AddSkillByID( skillID );
		}
#else //PET_SKILL_SHOP
		isBuySkillOK = pPlayer->AddSkillByID( skillID );
#endif //PET_SKILL_SHOP
		if ( !isBuySkillOK )
		{
			D_INFO( "玩家%s购买技能，商店条件过，但自身条件不符，购买失败\n", pPlayer->GetAccount() );
			pageBuyRst = GCShopBuyRst::ERROR_PLAYER_COND;
		}
	}

	if ( GCShopBuyRst::BUY_SUCCESS == pageBuyRst )
	{
		CLogManager::DoNpcShopPurchase(pPlayer, shopID, LOG_SVC_NS::TT_BUY, moneyNum, skillID, 1);//记日至

		bool takemoneyOK = false;
		//取走玩家身上货币；
		if ( moneyType < 2 )
		{
			//扣钱；
			if ( pPlayer->IsUseGoldMoney() )
			{
				//玩家使用金币
				takemoneyOK = pPlayer->SubGoldMoney( moneyNum );
			} else {
				//玩家使用银币
				takemoneyOK = pPlayer->SubSilverMoney( moneyNum );
			}			
		} else {
			if ( moneyID != m_tradeItemID )
			{
				D_ERROR( "不可能错误，PlayerBuySkillShopItem，货币类型不符\n" );
				pageBuyRst = GCShopBuyRst::ERROR_ITEM;
			} else {
				//扣货币物品；
				takemoneyOK = pPlayer->OnDropItemByConditionID( m_tradeItemID, moneyNum );
			}
		}
		
		if ( !takemoneyOK )
		{
			//购买成功的情况下，不可能出现取不走钱的错误，因为购买过程中已经对这些条件进行了检查；
			D_ERROR( "不可能错误，PlayerBuySkillShopItem，但取不走玩家身上物品%d数量%d\n", moneyType, moneyNum );
			return false;
		}
		//与普通商品不同，在前面检查玩家自身条件时已将技能加到玩家身上了，因此这里不需要再次添加；
	}

	MGShopBuyRst shopBuyRst;
	shopBuyRst.shopID = shopID;
	shopBuyRst.shopPageID = shopPageID;
	shopBuyRst.shopPagePos = shopPagePos;
	shopBuyRst.itemID = skillID;
	shopBuyRst.itemNum = 1;
	shopBuyRst.moneyType = moneyType;
	shopBuyRst.moneyNum = moneyNum;
    //通知玩家购买结果；
	shopBuyRst.buyRst = pageBuyRst;

	pPlayer->SendPkg< MGShopBuyRst >( &shopBuyRst );

	return true;
	TRY_END;
	return false;
}


///玩家请求购买第shopPageID页的itempos位置物品；
bool CNpcShop::PlayerBuyShopItem( CPlayer* pPlayer, unsigned long shopID/*商店ID*/, unsigned long shopPageID/*商品页码*/
					   , unsigned long shopPagePos/*商品在页中位置*/, unsigned long itemID/*商品类型ID号*/
					   , unsigned long itemNum/*一次购买的商品数量*/, unsigned long moneyType/*货币类型ID号*/, unsigned long moneyID/*货币ID号*/
					   , unsigned long moneyNum/*购买时准备使用的货币数量*/ )
{
	TRY_BEGIN;

    GCShopBuyRst::BuyRstType pageBuyRst = GCShopBuyRst::OTHER_ERROR;

	if ( NULL == pPlayer )
	{
		D_WARNING( "PlayerBuyShopItem, pPlayer指针空\n" );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return false;
	}

	if ( ( shopPageID >= LABELNUM_PER_SHOP )
		|| ( shopPagePos >= ITEMNUM_PER_SHOPLABEL )
		)
	{
		D_WARNING( "PlayerBuyShopItem, 玩家%s指定的购买标签%d或位置%d超限\n", pPlayer->GetAccount(), shopPageID, shopPagePos );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return false;
	}

	if ( shopPageID >= ARRAY_SIZE(m_arrShopLabels) )
	{
		D_WARNING( "PlayerBuyShopItem, shopPageID(%d) >= ARRAY_SIZE(m_arrShopLabels)\n", shopPageID );
		pageBuyRst = GCShopBuyRst::OTHER_ERROR;
		return false;
	}

#ifdef PET_SKILL_SHOP
	bool isPetSkill = false;
	pageBuyRst = PlayerBuyCheckBuy( pPlayer, shopID, shopPageID, shopPagePos, itemID, itemNum, moneyType, moneyID, moneyNum, isPetSkill );
#else //PET_SKILL_SHOP
	pageBuyRst = PlayerBuyCheckBuy( pPlayer, shopID, shopPageID, shopPagePos, itemID, itemNum, moneyType, moneyID, moneyNum );
#endif //PET_SKILL_SHOP

	MGShopBuyRst shopBuyRst;
	shopBuyRst.shopID = shopID;
	shopBuyRst.shopPageID = shopPageID;
	shopBuyRst.shopPagePos = shopPagePos;
	shopBuyRst.itemID = itemID;
	shopBuyRst.itemNum = itemNum;
	shopBuyRst.moneyType = moneyType;
	shopBuyRst.moneyNum = moneyNum;
    //通知玩家购买结果；
	shopBuyRst.buyRst = pageBuyRst;

	pPlayer->SendPkg< MGShopBuyRst >( &shopBuyRst );

	if ( GCShopBuyRst::BUY_SUCCESS == pageBuyRst )
	{
		bool takemoneyOK = false;
		//取走玩家身上货币；
		if ( moneyType < 2 )
		{
			//扣钱；
			if ( pPlayer->IsUseGoldMoney() )
			{
				//玩家使用金币
				takemoneyOK = pPlayer->SubGoldMoney( moneyNum );
			} else {
				//玩家使用银币
				takemoneyOK = pPlayer->SubSilverMoney( moneyNum );
			}			
		} else if( moneyType == 4 ){
			takemoneyOK = pPlayer->SubRacePrestige( moneyNum );
		} else {
			//扣货币物品；
			takemoneyOK = pPlayer->OnDropItemByConditionID( m_tradeItemID, moneyNum );
		}
		
		if ( !takemoneyOK )
		{
			//购买成功的情况下，不可能出现取不走钱的错误，因为购买过程中已经对这些条件进行了检查；
			D_WARNING( "不可能错误，PlayerBuyShopItem，但取不走玩家身上物品%d数量%d\n", moneyType, moneyNum );
			return false;
		} else {
			//扣钱成功，给玩家商品；		
			if ( !(pPlayer->GetNormalItem( itemID, itemNum )) )
			{
				//购买成功的情况下，不可能出现此错误，因为购买过程中已经对这些条件进行了检查；
				D_WARNING( "不可能错误，PlayerBuyShopItem，无法发给玩家物品%d数量%d\n", itemID, itemNum );
				return false;
			}
			//刷新showshop消息中购买页面购买位置的物品数目
			m_arrShopLabels[shopPageID].UpdateShowShopMsg( shopPagePos );
			CLogManager::DoNpcShopPurchase(pPlayer, shopID, LOG_SVC_NS::TT_BUY, moneyNum,itemID, itemNum);//记日至
		}
	}

	return true;
	TRY_END;
	return false;
}


