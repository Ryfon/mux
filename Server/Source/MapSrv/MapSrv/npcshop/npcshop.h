﻿/**
* @file npcshop.h
* @brief npc商店
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: npcshop.h
* 摘    要: npc商店
* 作    者: dzj
* 完成日期: 2008.11.24
*
*/
#include "PkgProc/SrvProtocol.h"

#include "../../../Base/Utility.h"

#include <map>

class CElement;
class CPlayer;

using namespace std;
using namespace MUX_PROTO;

/*
0：无限产出
1：每隔一小时产出ProduceNum个该商品
2：每隔一天产出ProduceNum个该商品
1002～1999：每隔N小时产出ProduceNum个该商品，N为2～999（相当于ProduceType值－1000的值）
2002～2999：每隔N天产出ProduceNum个该商品，N为2～999（相当于ProduceType值－2000的值）
*/
#define PRODUCE_INFINITE 0 //无限产出；
#define PRODUCE_PERHOUR  1 //每隔一小时产出ProduceNum个
#define PRODUCE_PERDAY   2 //每隔一天产出ProduceNum个该商品
#define PRODUCE_NHOUR    1002 //每隔N小时产出ProduceNum个该商品，N为2～999（相当于ProduceType值－1000的值）
#define PRODUCE_NDAY     2002 //2002～2999：每隔N天产出ProduceNum个该商品，N为2～999（相当于ProduceType值－2000的值）

//商店商品属性；
class CShopItem
{
	friend class CNpcShopManager;

public:
	enum ShopItemType
	{
		NormalShopItem = 0, //普通商品；
		SkillShopItem  = 1  //技能商品；
	};

public:
	///设置本商品为技能商品；
	inline void SetSkillItem()
	{
		m_ShopItemType = SkillShopItem;
	}

#ifdef PET_SKILL_SHOP
	inline void SetPetSkillFlag()
	{
		m_bIsPetSkill = true;
	}
	inline bool IsPetSkillFlagSet()
	{
		return m_bIsPetSkill;
	}
#endif //PET_SKILL_SHOP

private:
	///是否普通商品；
	inline bool IsNormalItem()
	{
		return (NormalShopItem == m_ShopItemType);
	}

private:
	ShopItemType m_ShopItemType;
#ifdef PET_SKILL_SHOP
	bool         m_bIsPetSkill;//是否为宠物技能;
#endif //PET_SKILL_SHOP

public:
	CShopItem() 
	{
#ifdef PET_SKILL_SHOP
	    m_bIsPetSkill = false;//是否为宠物技能;
#endif //PET_SKILL_SHOP
		m_ShopItemType = NormalShopItem;
		m_itemID = 0;//商品ID；
		m_perPurchaseNum = 0;//单次购买数量；
		m_tradePrice = 0;//交易价格；
		m_produceType = 0;//商品产出方式类型；
		m_dailyProduceHour = 0;//天产出时的时间点，小时；
		m_dailyProduceMin = 0;//天产出时的时间点，分钟；
		m_produceNum = 0;//每次产出数量；
		m_capabilityNum = 0;//商店中该商品的最大仓储量；

		//每item属性;
		m_curNum = 0;//当前可售商品数量；
        m_bIsNeedSupply = true;//初始总是检查；
		m_lastSupplyTime = ACE_OS::time();//上次更新时间；
		m_bIsDaySupplyed = false;
	};
	~CShopItem() {};

public:
    ///取当前剩余单位数；
	unsigned long GetCurNum() 
	{
		if ( !IsNormalItem() )
		{
			///非普通商品（技能商品），直接返回1； 
			return 1;
		}

		if ( 0 == m_itemID )
		{
			return 0;//本位置空；
		}
		//如果是无限产出，则直接返回1，否则返回当前剩余数目
		if ( PRODUCE_INFINITE == m_produceType )
		{
			return 1;
		}
		return m_curNum; 
	};

	inline unsigned long GetShopItemPrice() 
	{ 
		return m_tradePrice; 
	};

public:
	//检查输入商品各属性，如果与本商品相符，则返回真并将商品数-1，否则返回假；
	GCShopBuyRst::BuyRstType CheckAndBuyItem( unsigned long shopPagePos/*商品在页中位置*/, unsigned long itemID/*商品类型ID号*/
		, unsigned long itemNum/*一次购买的商品数量*/, unsigned long moneyNum/*购买使用的货币数量*/ );

public:
	/*
	#define PRODUCE_INFINITE 0 //无限产出；
	#define PRODUCE_PERHOUR  1 //每隔一小时产出ProduceNum个
	#define PRODUCE_PERDAY   2 //每隔一天产出ProduceNum个该商品
	#define PRODUCE_NHOUR    1002 //每隔N小时产出ProduceNum个该商品，N为2～999（相当于ProduceType值－1000的值）
	#define PRODUCE_NDAY     2002 //2002～2999：每隔N天产出ProduceNum个该商品，N为2～999（相当于ProduceType值－2000的值）
	*/
	///初始化补充标记, 输出值：初始化后，商店中是否有物品要补充；
	void InitItemSupplyFlag()
	{
		if ( !IsNormalItem() )
		{
			//非普通商品（技能商品），直接返回1； 
			m_bIsNeedSupply = false;
			return;
		}

		if ( PRODUCE_INFINITE == m_produceType )
		{
			//无限产出不需要补充；
			m_bIsNeedSupply = false;
			return;
		}
		if ( m_curNum >= m_capabilityNum )
		{
			//当前数量已达容量上限；
			m_bIsNeedSupply = false;
			return;
		}

		m_bIsNeedSupply = false;//重置初值；

		if ( PRODUCE_NHOUR == m_produceType )
		{
			//精度1小时；
			HourSupplyCheck();
		} else {
			//精度15分钟；
			QuarterSupplyCheck();
		}

		return;
	}

	///尝试补充店中商品；补充检测时钟，输入本次检测时钟的间隔
	//如果时钟间隔与本Item的产出类型不匹配，则直接返回否，表示本item不关心此时钟，此时isItemNeedSupplyFlag无效；
	//如果时钟间隔与本Item的产出类型匹配，则返回真，表示本item关心此时钟，此时isItemNeedSupplyFlag有效，标明本item是否还需要补充检测；
	bool TrySupplyItemItem( unsigned long timerInterval/*单位秒*/ )
	{
		if ( !IsNormalItem() )
		{
			//非普通商品（技能商品），直接返回1； 
			return false;
		}

		if ( !m_bIsNeedSupply )
		{
			return false;
		}

		if ( timerInterval < 15*60 )
		{
			//15分钟精度时钟；
			if ( PRODUCE_NHOUR == m_produceType )
			{
				//15分钟精度时钟不处理PRODUCE_NHOUR型产出;
				return false;//不关心此时钟；
			}
			m_bIsNeedSupply = false;//重置初值；
			QuarterSupplyCheck();
			return true;//关心此时钟；
		} else {
			//１小时精度时钟；
			if ( PRODUCE_NHOUR != m_produceType )
			{
				//1小时精度时钟只处理PRODUCE_NHOUR型产出;
				return false;//不关心此时钟；
			}
			m_bIsNeedSupply = false;//重置初值；
			HourSupplyCheck();
			return true;
		}
	}

	///每隔15分钟检测一次是否需要补充，检测三种类型：每小时，每天，每N天；
	void QuarterSupplyCheck();
	///每隔1小时检测一次是否需要补充，检测一种类型：每N个小时；
	void HourSupplyCheck();

public:
	bool IsItemNeedSupply() { return m_bIsNeedSupply; }

private:
	///是否有商品被买掉了需要补充；
	bool    m_bIsNeedSupply;
	time_t  m_lastSupplyTime;//上次补充时间；
	bool    m_bIsDaySupplyed;//今天是否已补充过,当ProduceType==PRODUCE_NDAY或PRODUCE_DAY时使用；

private:
	/*
	ItemID	商品ID
	对应游戏中的道具ID
	一个商店中陈列着该店铺出售的所有道具，及道具相关的销售属性
	PerPurchaseNum	单次购买数量
	每次购买单件该商品时在背包里获得的实际数量
	例如玩家购买30个一捆的回城卷，一次性花费购买后背包增加30个回城卷
	TradePrice	交易价格
	当商店的货币类型为0或1时，该属性代表交易所需的金额多少
	当商店的货币类型为2时，该属性表示交易所需的道具数量
	注：
	当TradePrice=0或者CurrencyType=2&&TradeItemID=0时，编辑器报错，服务器不允许该商品上架
	ProduceType	商品产出方式类型
	决定商品采用什么样的方式产出：
	0：无限产出
	1：每隔一小时产出ProduceNum个该商品
	2：每隔一天产出ProduceNum个该商品
	1002～1999：每隔N小时产出ProduceNum个该商品，N为2～999（相当于ProduceType值－1000的值）
	2002～2999：每隔N天产出ProduceNum个该商品，N为2～999（相当于ProduceType值－2000的值）
	当ProduceType=0时，后两个属性无效

	对于限量产出型（ProduceType≠0）的商品，当库存为0时，该商品图标不是删掉，而是变成灰色――相当于暗示该店有该类物品出产，只是现在库存为0而已。
	DailyProduceTime	当采用“天”的单位产出时，当天产出商品的时间点
	时间格式以24小时制的4位数字表示，例如“930”为上午九点半，1850为晚上6点50分
	ProduceNum	商品每时刻产出数量
	决定商品每小时/天产出多少个
	Capability	商店中该类商品的最大仓储量
	当ProduceType不为0，且当前商店中该商品的存储到达Capability的数值时，该商店不再产出新的该类商品
	*/
	unsigned long m_itemID;//商品ID；
	unsigned long m_perPurchaseNum;//单次购买数量；
	unsigned long m_tradePrice;//交易价格；
	unsigned long m_produceType;//商品产出方式类型；
	int m_dailyProduceHour;//天产出时的时间点，小时；
	int m_dailyProduceMin;//天产出时的时间点，分钟；
	unsigned long m_produceNum;//每次产出数量；
	unsigned long m_capabilityNum;//商店中该商品的最大仓储量；

private:
	//每item属性;
	unsigned long m_curNum;//当前可售商品数量；
};

//商店标签页；
class CShopLabel
{
	friend class CNpcShopManager;
public:
	CShopLabel() 
	{
		m_selfShopID = 0;
		m_selfLabelID = 0;
		m_bIsNeedSupply = true;//初始总是检查；
	};
	~CShopLabel() {};

public:
	///设置本标签所属的商店号以及在此商店中的标签号；
	void SetSelfInfo( unsigned long selfShopID, unsigned long selfLabelID )
	{
		m_selfShopID = selfShopID;
		m_selfLabelID = selfLabelID;
	}

	///刷新MGShowShop消息；
	void InitShowLabelInfo();
	///刷新页showShop消息中特定位置的item数目；
	void UpdateShowShopMsg( unsigned long itemPos );

	///初始化补充标记, 输出值：初始化后，标签中是否有物品要补充；
	void InitLabelSupplyFlag();

	unsigned long GetSelfShopID() { return m_selfShopID; };
	unsigned long GetSelfLabelID() { return m_selfLabelID; };

public:
	///取特定位置物品的价格;
    bool GetShopItemPrice( unsigned long shopPos, unsigned long& outPrice );

#ifdef PET_SKILL_SHOP
	///指定位置是否为宠物技能；
	bool IsShopItemPetSkill( unsigned long shopPos );
#endif //PET_SKILL_SHOP

public:
	///补充标签中物品；
	void TrySupplyLabelItem( unsigned long timerInterval/*单位秒*/ );

public:
	///商店标签购买；
	GCShopBuyRst::BuyRstType ShopLabelCheckAndBuy( unsigned long shopPageID/*商品页码*/
		, unsigned long shopPagePos/*商品在页中位置*/, unsigned long itemID/*商品类型ID号*/
		, unsigned long itemNum/*一次购买的商品数量*/, unsigned long moneyNum/*购买使用的货币数量*/ );

public:
	bool IsLabelNeedSupply() { return m_bIsNeedSupply; }

private:
	///是否有商品被买掉了需要补充；
	bool m_bIsNeedSupply;

public:
	//向玩家发送本页的物品数量列表；
	bool SendShopItemsToPlayer( CPlayer* pPlayer );

private:
	CShopItem m_arrShopItems[ITEMNUM_PER_SHOPLABEL];
	unsigned long m_selfShopID;//本页所属的shopID;
	unsigned long m_selfLabelID;//本页的页号；

private:
    MGShowShop m_showShop;//存放用于通知玩家本标签商品信息的消息结构，每次商品数量变化时修改，而不是每次有查询时都重新生成一次；
};

class CNpcShop
{
	friend class CNpcShopManager;

public:
	enum ShopType
	{
		NormalShop = 0, //普通商品商店；
		SkillShop  = 1  //技能商品商店；
	};

public:
	///设置本商店为技能商店；
	inline void SetSkillShop()
	{
		m_ShopType = SkillShop;
	}

private:
	///是否普通商品；
	inline bool IsNormalShop()
	{
		return (NormalShop == m_ShopType);
	}

private:
	ShopType m_ShopType;

public:
	CNpcShop() 
	{
		m_ShopType = NormalShop;
		m_shopID = 0;//商店ID；
		m_priceRate = 0;//出售价格率；
		m_currencyType = 0;//货币类型；
		m_tradeItemID = 0;//以物易物的道具ID；
		m_bIsNeedSupply = true;//初始总是检查；
	};
	~CNpcShop() {};

public:
	///初始化商店；
	void InitShop()
	{
 		InitShopSupplyFlag();//初始化商店、各标签以及各物品的补充标记；
		InitShowShopInfo();//初始化各标签的MG通知消息；
	}

	///初始化本商店中每一标签的MGShowShop信息，以备玩家查询时返回；
	void InitShowShopInfo();

	///初始化补充标记, 输出值：初始化后，商店中是否有物品要补充；
	void InitShopSupplyFlag();

	///补充商店中物品；
	void TrySupplyShopItem( unsigned long passedsec );

private:
	bool IsShopNeedSupply() { return m_bIsNeedSupply; }

private:
	///是否有商品被买掉了需要补充；
	bool m_bIsNeedSupply;

private:
	GCShopBuyRst::BuyRstType PlayerBuyCheckBuy( CPlayer* pPlayer, unsigned long shopID/*商店ID*/, unsigned long shopPageID/*商品页码*/
					   , unsigned long shopPagePos/*商品在页中位置*/, unsigned long itemID/*商品类型ID号*/
					   , unsigned long itemNum/*一次购买的商品数量*/, unsigned long moneyType/*货币类型ID号*/, unsigned long moneyID/*货币ID号*/
					   , unsigned long useMoneyNum/*准备使用的货币数量*/
#ifdef PET_SKILL_SHOP
					   , bool& isPetSkill/*所购是否为宠物技能*/ 
#endif //PET_SKILL_SHOP
					   );

public:
	///向玩家发送第n页的物品数量列表；
	bool SendShopLabelToPlayer( CPlayer* pPlayer, unsigned long labelID );

	///玩家请求第shopPageID页物品数量列表；
	bool PlayerAskForShopLabel( CPlayer* pPlayer, unsigned long shopID, unsigned long shopPageID );

	///玩家请求购买第shopPageID页的itempos位置物品；
	bool PlayerBuyShopItem( CPlayer* pPlayer, unsigned long shopID/*商店ID*/, unsigned long shopPageID/*商品页码*/
		, unsigned long shopPagePos/*商品在页中位置*/, unsigned long itemID/*商品类型ID号*/
		, unsigned long itemNum/*一次购买的商品数量*/, unsigned long moneyType/*货币类型ID号*/,unsigned long moneyID/*货币ID号*/
		, unsigned long moneyNum/*购买使用的货币数量*/ );

	///玩家请求购买第shopPageID页的itempos位置技能；
	bool PlayerBuySkillShopItem( CPlayer* pPlayer, unsigned long shopID/*商店ID*/, unsigned long shopPageID/*商品页码*/
		, unsigned long shopPagePos/*商品在页中位置*/, unsigned long skillID/*商品类型ID号*/
		, unsigned long moneyType/*货币类型ID号*/, unsigned long moneyID/*货币类型ID号*/ 
		, unsigned long moneyNum/*购买使用的货币数量*/ );

	///玩家请求出售包裹中pos位置物品；
	bool PlayerSellPkgItem( CPlayer* pPlayer, unsigned long itemID/*物品类型ID号*/
		, unsigned long itemPos/*售出物品在包裹中的位置*/, unsigned long itemNum/*售出数量*/, unsigned long moneyNum/*本次售出希望获得的金钱数*/ );

public:
	void SetShopID( unsigned long shopID ) { m_shopID = shopID; };
	void SetPriceRate( float priceRate ) { m_priceRate = priceRate; };
	void SetCurrencyType( unsigned long currencyType ) { m_currencyType = currencyType; };
	void SetTradeItemID( unsigned long tradeItemID ) { m_tradeItemID = tradeItemID; };

	unsigned long GetShopID() { return m_shopID; };
	float GetPriceRate() { return m_priceRate; };
	unsigned long GetCurrencyType() { return m_currencyType; };
	unsigned long GetTradeItemID() { return m_tradeItemID; };

private:
	/*
	ID	商店ID
	游戏中所有商店（包括道具商店和技能商店）的ID都是全局唯一的
	该商店ID供NPC对话脚本中用接口调用
	Desc	商店描述
	不显示在游戏中，只是便于配置人员识别的一个描述字符串
	PriceRate	出售价格率
	该商店中所有商品的实际价格均为（道具原有价格×出售价格率）后的数值
	该数值支持公式（以字符串格式存放）
	例如实际出售价格率是根据玩家声望进行折算的，玩家声望越高，店里物品的价格越低
	CurrencyType	商店使用的货币类型
	决定店里物品采用什么样的货币购买方式
	0：游戏币购买
	1：RMB货币购买
	（当使用前两种货币类型时，商品属性中的TradeItemID属性无效）
	2：以物易物（例如各个地区出产的牌子）
	TradeItemID	以物易物的道具ID
	游戏中除了用金币交易商品以外，还可以用其他数量的物品来进行交易（例如各个地图出产的牌子）
	当商店的货币类型为0和1时，该属性无效
	*/
	unsigned long  m_shopID;//商店ID；
	float          m_priceRate;//出售价格率；
	unsigned long  m_currencyType;//货币类型；
	unsigned long  m_tradeItemID;//以物易物的道具ID；

private:
	CShopLabel m_arrShopLabels[LABELNUM_PER_SHOP];
};

//NPC商店管理器
class CNpcShopManager
{
private:
	CNpcShopManager();
	~CNpcShopManager();

public:
	///NPC商店管理器，读取配置文件以及初始化；
	static bool NpcShopManagerInit( const char* shopConfigFile )
	{
		m_lastSupplyTime = ACE_OS::gettimeofday();
		m_arrNpcShop.clear();
		return LoadNormalShopFromXML( shopConfigFile );		
	}

	static bool NpcSkillShopManagerInit( const char* skillShopConfigFile )
	{
		return LoadSkillShopFromXML( skillShopConfigFile );
	}

	///NPC商店管理器，释放内部内存；
	static void NpcShopManagerRelease()
	{
		for( map<unsigned long, CNpcShop*>::iterator iter=m_arrNpcShop.begin(); iter!=m_arrNpcShop.end(); ++iter )
		{
			if ( NULL != iter->second )
			{
				delete iter->second; iter->second = NULL; 
			}
		}
		m_arrNpcShop.clear();
	}

public:
	static void NpcShopManTimerProc( const ACE_Time_Value& curTime )
	{
		if ( ( curTime-m_lastSupplyTime ).msec() < 5*60*1000/*5分钟*/ )
		{
			return;
		}
		unsigned int passedsec = (curTime-m_lastSupplyTime).msec() / 1000;
		m_lastSupplyTime = curTime;
		for( map<unsigned long, CNpcShop*>::iterator iter=m_arrNpcShop.begin(); iter!=m_arrNpcShop.end(); ++iter )
		{
			if ( NULL != iter->second )
			{
				iter->second->TrySupplyShopItem( passedsec );
			}
		}

		return;
	}

private:
	static ACE_Time_Value m_lastSupplyTime;

public:
	///NPC商店配置文件读取；
	static bool LoadNormalShopFromXML( const char* shopConfigFile );
	///NPC技能商店配置文件读取；
	static bool LoadSkillShopFromXML( const char* shopConfigFile );

public:
	///显示指定ＩＤ号ＮＰＣ商店；
	static bool ShowNpcShop( CPlayer* pPlayer, unsigned long shopID, unsigned long shopPageID );
	///从NPC商店中购买物品请求
	static bool NpcShopBuy( CPlayer* pPlayer, const GMShopBuyReq& shopBuyReq );

private:
	///取指定ID号NPC商店；
	static CNpcShop* FindNpcShop( unsigned long shopID );

private:
	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord );

private:
	static map<unsigned long, CNpcShop*> m_arrNpcShop;//NPC商店集合；

};
