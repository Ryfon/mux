﻿#include "MapPublicNoticeManager.h"
#include "XmlManager.h"
#include "../../Base/Utility.h"
#include "MuxMap/mannormalmap.h"

std::map<unsigned int, MapNoticeVec> MapPublicNoticeManager::mMapNoticeMap;
time_t MapPublicNoticeManager::mLastUpdateTime = time(NULL);

void  MapPublicNoticeManager::LoadNoticeConfig()
{
	const char pszXMLPath[] = {"config/sysmsg.xml"};
	CXmlManager xmlManager;
	if(!xmlManager.Load(pszXMLPath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszXMLPath );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszXMLPath);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	for ( std::vector<CElement *>::iterator lter = childElements.begin(); lter != childElements.end(); ++lter )
	{
		CElement* pElement = *lter;
		if ( pElement )
		{
			std::vector<CElement *> secondChildElement;
			xmlManager.FindChildElements( pElement,secondChildElement );

			if ( secondChildElement.empty() )
				continue;

			unsigned int mapID = (unsigned int)ACE_OS::atoi( pElement->GetAttr("id") );//这个地图的公告

			MapNoticeVec newMapNoticeVec;
			newMapNoticeVec.mapID = mapID;

			for ( std::vector<CElement *>::iterator iter = secondChildElement.begin(); iter != secondChildElement.end(); ++iter )
			{
				CElement* pMsgElement = *iter;
				if ( pMsgElement )
				{
					char * starttime = (char *)pMsgElement->GetAttr("starttime");
					char * endtime   = (char *)pMsgElement->GetAttr("endtime");
					char * interval  = (char *)pMsgElement->GetAttr("intervaltime");
					char * content   = (char *)pMsgElement->GetAttr("content");

					if ( !starttime ||  !endtime || !interval || !content )
						continue;

					char* min1 = ACE_OS::strchr( starttime,':');
					char* min2 = ACE_OS::strchr( endtime,':');
					min1++;
					min2++;
				

					MapNotices mapNotice;
					mapNotice.hour1 = ACE_OS::atoi( starttime );
					mapNotice.hour2 = ACE_OS::atoi( endtime );
					mapNotice.min1  = ACE_OS::atoi( min1 );
					mapNotice.min2  = ACE_OS::atoi( min2 );

					mapNotice.intervaltime = ACE_OS::atoi( interval );
					mapNotice.noticeMsg = std::string( content );

					newMapNoticeVec.noticeVec.push_back( mapNotice );
				}
			}

			std::map<unsigned int, MapNoticeVec>::iterator tmpmapiter = mMapNoticeMap.find(mapID);
			if ( mMapNoticeMap.end() != tmpmapiter )
			{
				D_ERROR( "sysmsg.xml中重复的mapID:%d\n", mapID );
				return;
			}
			mMapNoticeMap.insert( pair<unsigned int, MapNoticeVec>(mapID, newMapNoticeVec) );
			//mMapNoticeMap[mapID] = newMapNoticeVec;
		}
	}
}

MapNoticeVec* MapPublicNoticeManager::GetMapNoticeVec(unsigned int mapID )
{
	std::map<unsigned int, MapNoticeVec>::iterator lter = mMapNoticeMap.find( mapID );
	if ( lter != mMapNoticeMap.end() )
	{
		return &lter->second;
	}
	
	return NULL;
}

bool MapNoticeVec::CheckNeedPublicNotice(time_t curtime )
{
	struct tm*  cur_tm = ACE_OS::localtime( &curtime );
	unsigned int  hour = cur_tm->tm_hour;
	unsigned int  min  = cur_tm->tm_min;

	for ( std::vector<MapNotices>::iterator lter = noticeVec.begin() ;lter != noticeVec.end(); ++lter )
	{
		MapNotices& mapnotice = *lter;
		bool isCheck1 = false;
		if ( mapnotice.hour1 == hour && mapnotice.min1 <= min )
		{
			isCheck1 = true;
		}
		else if ( mapnotice.hour1 < hour )
		{
			isCheck1 = true;
		}

		if ( !isCheck1 )
			continue;
		
		bool isCheck2 = false;
		if ( mapnotice.hour2 == hour && mapnotice.min2 >= min )
		{
			isCheck2 = true;
		}
		else if( mapnotice.hour2 > hour )
		{
			isCheck2 = true;
		}

		if ( !isCheck2 )
			continue;
		
		if ( curtime >= mapnotice.lastupdatetime )
		{
			MapPublicNoticeManager::SendPublicNotice( mapID, mapnotice.noticeMsg.c_str() );

			mapnotice.lastupdatetime = curtime + mapnotice.intervaltime * 60;
		}
		
	}

	return true;
}


void MapPublicNoticeManager::SendMapPublicNotice()
{
	//获取当前时间
	time_t curtime = time(NULL);
	if ( difftime( curtime ,mLastUpdateTime ) >= 60 )
	{
		std::vector<long> mapIDVec;
		CManNormalMap::GetMannedMap( mapIDVec );

		if ( mapIDVec.empty() )
			return;

		//遍历所管理的每个地图，看是否需要发送公告
		for ( std::vector<long>::iterator lter = mapIDVec.begin(); lter != mapIDVec.end(); ++lter )
		{
			unsigned int mapID = *lter;
			MapNoticeVec* pMapNoticeVec = GetMapNoticeVec( mapID );

			if ( pMapNoticeVec )//看是否需要发公告
				pMapNoticeVec->CheckNeedPublicNotice( curtime );
		}

		mLastUpdateTime = curtime;
	}
}

void MapPublicNoticeManager::SendPublicNotice(unsigned int mapID, const char* notice )
{
	CNewMap* muxmap = CManNormalMap::FindMap( mapID );
	if ( muxmap )
	{
		muxmap->MapBrocastChatMsg( notice );
	}
}