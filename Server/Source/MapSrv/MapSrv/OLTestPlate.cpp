﻿#include "OLTestPlate.h"
#include "Player/Player.h"
#include "Item/NormalTask.h"
#include "OLTestQuestionlibrary.h"


void OLTestPlate::ShowOLTestPlate(unsigned int taskID ,bool isNeedPreDialog )
{
	if ( !mpAttachPlayer )
		return;

	ClearPlate();

	if ( isNeedPreDialog )//是否需要弹出预告框,弹出预告框后，不能询问题目
	{
		ShowPrevueDialog();

		SetOLTestSessionInfo( taskID, 0 , 0, 0, 0 );
	}
	else//如果不需要弹预告框，则开始询问题目
	{
		CNormalCounter* pAnswerCounter = mpAttachPlayer->GetTaskCounter(  taskID,  CT_ANSWER );
		if ( pAnswerCounter )
		{
			//获取计数器的题目范围
			unsigned int rangestart  = 0 ,  rangeend = 0;
			pAnswerCounter->GetCounterRange( rangestart, rangeend );

			//获取计数器的当前数目
			unsigned int curcount    = pAnswerCounter->GetCurCounterCount();

			//获取计数器的目标数目
			unsigned int targetcount = pAnswerCounter->GetCounterTargetCount(); 

			//将获取的信息,设置给当前的答题session
			SetOLTestSessionInfo( taskID, rangestart, rangeend , curcount , targetcount );	

			AskQuestion();
		}
	}
}

void OLTestPlate::ShowPrevueDialog()
{
	if ( !mpAttachPlayer )
		return;

	MGShowProvueTestDlg showPrevueDlg;
	mpAttachPlayer->SendPkg<MGShowProvueTestDlg>( &showPrevueDlg );
}

void OLTestPlate::CloseOLTestPlate()
{
	ClearPlate();

	if ( !mpAttachPlayer )
		return;

	MGCloseOLTestPlate closeOLTestDlg;
	mpAttachPlayer->SendPkg<MGCloseOLTestPlate>( &closeOLTestDlg );
}

void OLTestPlate::CloseAnswerQuestionWaitDlg()
{
	if ( !mpAttachPlayer )
		return;

	//开始新一轮的答题,以前的全部作废
	ShowOLTestPlate(  mTestSession.GetTestSessionTaskID() , false );
}

void OLTestPlate::ShowAnswerAllQuestionDlg()
{
	if ( !mpAttachPlayer )
		return;

	MGShowAnswerAllQuestionsDlg answerAllQuestions;
	mpAttachPlayer->SendPkg<MGShowAnswerAllQuestionsDlg>( &answerAllQuestions );
}

void OLTestPlate::AskQuestion()
{
	unsigned int curquestion = SelectQuestion();
	if ( curquestion > 0 )
	{
		Question* pQuestion = OLTestQuestionLibrary::GetQuestionInfo( curquestion );
		if ( pQuestion )
		{
			//设置当前的答题信息,让client显示问题
			ShowQuestion( pQuestion->questionID, pQuestion->answerID, pQuestion->limittime );

			//如果当前答题限时间
			if ( pQuestion->limittime > 0 ) 
			{
				unsigned int expiredtime = (unsigned int)time(NULL) + pQuestion->limittime;
				mTestSession.SetTestSessionExpireTime( expiredtime + 2 );
			}
		}
	}
}

void OLTestPlate::CheckIfOutExpireTime()
{
	if( (unsigned int)time(NULL) > mTestSession.curexpiretime )
	{
		TimeOutAnswerQuestionFail();
	}
}

void OLTestPlate::TimeOutAnswerQuestionFail()
{
	if ( !mpAttachPlayer )
		return;

	AnswerQuestionWrong();
}

void OLTestPlate::DropOLTest()
{
	if ( !mpAttachPlayer )
		return;

	CNormalCounter* pAnswerCounter = mpAttachPlayer->GetTaskCounter(  mTestSession.GetTestSessionTaskID(),  CT_ANSWER );
	if ( pAnswerCounter  )
	{
		pAnswerCounter->RefreshCounterNum( mpAttachPlayer , mTestSession.GetTestSessionTaskID()  );
	}

	//关闭答题托盘
	CloseOLTestPlate();
}

void OLTestPlate::RefreshOLTest()
{
	if ( !mpAttachPlayer )
		return;

	ETaskStatType taskstate = mpAttachPlayer->GetTaskStatus( mTestSession.GetTestSessionTaskID() );
	if ( taskstate == NTS_TGT_NOT_ACHIEVE)//如果任务正在进行中
	{
		//重刷计数器的值
		CNormalCounter* pAnswerCounter = mpAttachPlayer->GetTaskCounter(  mTestSession.GetTestSessionTaskID(),  CT_ANSWER );
		if ( pAnswerCounter  )
		{
			pAnswerCounter->RefreshCounterNum( mpAttachPlayer , mTestSession.GetTestSessionTaskID()  );
		}
	}
}

void OLTestPlate::AnswerQuestionWrong()
{
	if ( !mpAttachPlayer )
		return;

	//通知失败
	AswerQuestionRst( false );

	//重刷计数器的值
	CNormalCounter* pAnswerCounter = mpAttachPlayer->GetTaskCounter(  mTestSession.GetTestSessionTaskID(),  CT_ANSWER );
	if ( pAnswerCounter )
	{
		pAnswerCounter->RefreshCounterNum( mpAttachPlayer , mTestSession.GetTestSessionTaskID()  );
	}

	//清除当前的问题
	ClearOLTestCurQuestion();
}


void OLTestPlate::AnswerQuestionRight()
{
	if ( mpAttachPlayer )
	{
		//通知成功
		AswerQuestionRst( true );

		//记数增加
		mpAttachPlayer->TryCounterEvent( CT_ANSWER, mTestSession.curquestionID, 1 );

		//当前答题数目+1
		mTestSession.IncAnsweredCount();

		if ( mTestSession.IsReachTargetCount() )
		{
			CloseOLTestPlate();//关闭托盘

			//告诉client已经完成了所有答题
			ShowAnswerAllQuestionDlg();
		}
		else
		{
			AskQuestion();//开始下次询问
		}
	}
}

void OLTestPlate::AnswerTestQuestion(unsigned int selectID )
{
	if ( !mpAttachPlayer )
		return;

	if ( IsQuestioning() )//当前正在询问题目
	{
		if ( IsAnswerRight( selectID ) )//如果答案正确
		{
			AnswerQuestionRight();
		}
		else//如果回答错误
		{
			AnswerQuestionWrong();
		}
	}
	else
	{
		D_ERROR("%s 没有进入答题状态,但接到了回复答题的消息\n", mpAttachPlayer->GetNickName() );
	}
}

void OLTestPlate::AswerQuestionRst(bool isRight )
{
	if ( !mpAttachPlayer )
		return;

	MGAnswerQuestionRst questionRst;
	questionRst.answerQuestionRst.rst = isRight ? 0:1;
	mpAttachPlayer->SendPkg<MGAnswerQuestionRst>( &questionRst );
}


void OLTestPlate::ShowQuestion(unsigned int questionID, unsigned int answerid, unsigned int limittime )
{
	if ( !mpAttachPlayer )
		return;

	mTestSession.SetCurQuestionInfo( questionID, answerid );

	unsigned int a[] = {1, 2, 3, 4 };
	std::vector<unsigned int> vec(a, a + 4);
	std::random_shuffle( vec.begin(), vec.end() );//打乱选项

	MGShowOLTestQuestion question;
	question.showQuestion.curcount    = mTestSession.GetCurAnsweredCount();
	question.showQuestion.targetcount = mTestSession.GetTargetCount();
	question.showQuestion.limittime   = limittime;
	question.showQuestion.questionID  = questionID;
	question.showQuestion.s1 = vec[0];
	question.showQuestion.s2 = vec[1];
	question.showQuestion.s3 = vec[2];
	question.showQuestion.s4 = vec[3];
	mpAttachPlayer->SendPkg<MGShowOLTestQuestion>( &question );
}


void OLTestPlate::SetOLTestSessionInfo(unsigned int curtaskid, unsigned int questionstart, unsigned int questionend,
									   unsigned int alreadyansweredcount , unsigned int targetcount )
{
	if ( questionstart > questionend )
	{
		D_ERROR("答题范围集合错误 begin:%d end:%d\n", questionstart, questionend );
		return ;
	}
	else
	{
		D_DEBUG("生成答题集合 范围 ( %d  %d )\n", questionstart, questionend );
	}

	mTestSession.curansweredcount =  alreadyansweredcount;
	mTestSession.curtargetcount   =  targetcount;
	mTestSession.SetTestSessionTaskID( curtaskid );

	questionVec.clear();
	for ( unsigned int i = questionstart; i <= questionend; i++ )
		questionVec.push_back( i );

	if ( questionVec.size() )
		std::random_shuffle( questionVec.begin(), questionVec.end() );
}

unsigned int OLTestPlate::SelectQuestion()
{
	if ( questionVec.size() > 0 )
	{
		unsigned int questionID = questionVec[0];
		questionVec.erase( questionVec.begin() );
		return questionID;
	}

	return 0;
}