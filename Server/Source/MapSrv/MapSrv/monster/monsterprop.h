﻿/**
* @file monsterprop.h
* @brief 定义怪物配置属性
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: monsterprop.h
* 摘    要: 定义怪物配置属性
* 作    者: dzj
* 完成日期: 2007.12.24
*
*/

#pragma once

class CMonsterProp
{
public:
	CMonsterProp() {};
	~CMonsterProp() {};

public:
	unsigned long GetClsID() { return m_dwClsID; }

public:
	unsigned long m_dwClsID;//MONSTER类型号；NO="33554433" 
	char m_strName[32];//Name="狼人" 
	char m_strDesc[32];//Desc="狼人" 
	unsigned long m_lLevel;//Level="1" 
	unsigned long m_lType;//Type="8" 
	unsigned long m_lRaceType;//RaceType="1" 
	unsigned long m_lSize;//Size="3" 
	unsigned long m_lExp;//Exp="0" 
	unsigned long m_lExpLimitLevel;//ExpLimitLevel="0" 
	unsigned long m_lMaxHp;//MaxHp="200" 
	unsigned long m_lHpUp;//HpUp="0" 
	unsigned long m_lSpeedLevel;//SpeedLevel="2" 
	unsigned long m_lViewSpan;//ViewSpan="0" 
	unsigned long m_lChaseSpan;//ChaseSpan="0" 
	unsigned long m_lChaseTime;//ChaseTime="0" 
	unsigned long m_lPatrolSpan;//PatrolSpan="5" 
	unsigned long m_lAttType;//AttType="1" 
	unsigned long m_lAttMin;//AttMin="0" 
	unsigned long m_lAttMax;//AttMax="60" 
	unsigned long m_lActionName;//ActionName="" 
	unsigned long m_lHeartType;//HeartType="1" 
	unsigned long m_lAttDistMin;//AttDistMin="0" 
	unsigned long m_lAttDistMax;//AttDistMax="0" 
	unsigned long m_lAttRadius;//AttRadius="0" 
	unsigned long m_lAttFreq;//AttFreq="2" 
	unsigned long m_lSpaceTime;//SpaceTime="5" 
	unsigned long m_lAttProb;//AttProb="0" 
	unsigned long m_lFinalKill;//FinalKill="120" 
	unsigned long m_lHit;//Hit="0" 
	unsigned long m_lDefMin;//DefMin="0" 
	unsigned long m_lDefMax;//DefMax="0" 
	unsigned long m_lAttSpeed;//AttSpeed="0" 
	unsigned long m_lMoveSpeed;//MoveSpeed="0" 
	unsigned long m_lCritical;//Critical="2" 
	unsigned long m_lAttProp;//AttProp="0" 
	unsigned long m_lAttObjSel;//AttObjSel="0" 
	unsigned long m_lAttObjSelData;//AttObjSelData="" 
	unsigned long m_lChaseTimes;//ChaseTimes="0" 
	unsigned long m_lAIFireCond;//AIFireCond="0" 
	unsigned long m_lAIFireData;//AIFireData="0" 
	unsigned long m_lEscapeRatio;//EscapeRatio="0" 
	unsigned long m_lEscapeDist;//EscapeDist="0" 
	unsigned long m_lEscapeTime;//EscapeTime="0" 
	unsigned long m_lDist;//Dist="0" 
	unsigned long m_lAcceptAID;//AcceptAID="0" 
	unsigned long m_lAddBloodRatio;//AddBloodRatio="0" 
	unsigned long m_lAidBloodRatio;//AidBloodRatio="0" 
	unsigned long m_lAidBoostRatio;//AidBoostRatio="0" 
	unsigned long m_lUnchainStateRatio;//UnchainStateRatio="0" 
	unsigned long m_lSkillRatio;//SkillRatio="0" 
	unsigned long m_lSummonRatio;//SummonRatio="0" 
	unsigned long m_lSummonNum;//SummonNum="0" 
	unsigned long m_lSummonNPCNO;//SummonNPCNO="0" 
	unsigned long m_lCarryRatio;//CarryRatio="0" 
	unsigned long m_lTransRatio;//TransRatio="0" 
	unsigned long m_lReturnBorn;//ReturnBorn="0" 
	unsigned long m_lForeverState;//ForeverState="" 
	unsigned long m_lAddBloodId;//AddBloodId="" 
	unsigned long m_lPowerRise;//PowerRise="0" 
	unsigned long m_lPlusID;//PlusID="" 
	unsigned long m_lDamnifyID;//DamnifyID="" 
	unsigned long m_lModalNO;//ModalNO="0" 
	unsigned long m_lHeadImg;//HeadImg="0" 
	unsigned long m_lLogo;//Logo="0" 
	unsigned long m_lText;//Text="0" 
	unsigned long m_lCadre;//Cadre="0" 
	unsigned long m_lMovie;//Movie="0"
	unsigned long m_lSkill;//<Skill>1</Skill> 
};
