﻿/**
* @file monster.cpp
* @brief 定义怪物
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: monster.cpp
* 摘    要: 定义怪物
* 作    者: dzj
* 完成日期: 2007.12.24
*
*/

#include "monster.h"
#include "manmonstercreator.h"
#include "../MuxMap/muxmapbase.h"
#include "../Item/CItemPackage.h"
#include "../PoolManager.h"
#include "../MuxSkillConfig.h"
#include "../Lua/BattleScript.h"


///当前服务器上所有的NPC，用于遍历NPC；
vector<CMonster*> CManMonster::m_vecMonsters;
///当前服务器上所有的NPC，用于快速寻找NPC；
map<unsigned long, CMonster*> CManMonster::m_mapMonsters;

TCache<CMonster>* CManMonster::m_poolMonsters;

int CManMonster::m_LastEPos = 0;//上次遍历的结束位置，每次遍历怪物总数的1/4;
unsigned int CMonster::s_lMonsterIDAllocer = 1;//全局怪物ID号分配；

AIEventInfo  CMonster::m_AIEventInfo;//每次调用IFsm中AI有关的事件接口时，在本信息结构中保存调用相关的信息，以备AI扩展接口查询；

void CManMonster::TimerDelMonster( CMonster* pMonsterToDel )
{
	if ( NULL == pMonsterToDel )
	{
		return;
	}

	//无效的monster对象，应该删去；
	CNewMap* pMap = pMonsterToDel->GetMap();
	if ( NULL == pMap )
	{
		D_ERROR( "TimerDelMonster, pMonster所属的地图空!\n" );
		return;
	}

	pMap->FinalDelMonster( pMonsterToDel );

	unsigned long monsterid = pMonsterToDel->GetMonsterID();
	if ( NULL != m_poolMonsters )
	{
		m_poolMonsters->Release( pMonsterToDel );
	} 
	else 
	{
		D_ERROR( "TimerDelMonster m_poolMonsters空!\n" );
		return;
	}

	//从自身管理的有效monster列表和map中删去；
	map<unsigned long, CMonster*>::iterator mapiter = m_mapMonsters.find( monsterid );
	if ( mapiter != m_mapMonsters.end() )
	{
		m_mapMonsters.erase( mapiter );
	} 
	else 
	{
		D_ERROR( "TimerDelMonster,不该到此处，monster管理器中vector与map不一致ReleaseManedMonster" );
	}

	return;
}


CMonster::CMonster()
{
	PoolObjInit();
};


CMonster::~CMonster() 
{

};

unsigned long CMonster::GetMapIDOfMonster()
{
	if ( NULL == m_pMap )
	{
		return 0xffffffff;
	} 
	else 
	{
		return m_pMap->GetMapNo();
	}
}

void CMonster::InitTempInfo()
{
	SetSpeed( m_pNPCProp->m_fSpeed );
	m_tempInfo.uiMaxHP = m_pNPCProp->m_uiMaxHP;
	m_tempInfo.usInherenStand = (unsigned short)m_pNPCProp->m_inherentStand;
	m_tempInfo.nHit = m_pNPCProp->m_nHit;
	m_tempInfo.nPhyDef = m_pNPCProp->m_nPhysicsDef;
	m_tempInfo.fPhyDodge = m_pNPCProp->m_fPhysicDodge;
	m_tempInfo.nMagDef = m_pNPCProp->m_nMagicDef;
	m_tempInfo.fMagDodge = m_pNPCProp->m_fMagicDodge;
}


bool CMonster::InitMonster( unsigned long dwMonsterClsID )
{
	TRY_BEGIN;

	ClearMonsterDieFlag();//具体初始化后设置为活；

	m_dwMonsterClsID = dwMonsterClsID;
    
	m_pNPCProp = CManNPCProp::FindObj( dwMonsterClsID );
	if( NULL == m_pNPCProp )
	{
		return false;
	}

	m_pChatScript = CNormalScriptManager::FindScripts( m_pNPCProp->m_usScriptID );

	if ( m_pNPCProp->m_DeathScriptID > 0 )
	{
		m_pDeathScript = CNormalScriptManager::FindScripts( m_pNPCProp->m_DeathScriptID );
	}

	m_buffManager.AttachOwner( this );

	//设初始属性...
	//怪物类别号；
	//m_dwMonsterClsID = m_pMonsterProp->m_dwClsID;
	m_dwMonsterClsID = m_pNPCProp->m_lNPCClsID;
	//怪物ID，地图内唯一；
	++s_lMonsterIDAllocer;
	if ( s_lMonsterIDAllocer <= 0 )
	{
		s_lMonsterIDAllocer = 1;
	}
	m_lMonsterID = s_lMonsterIDAllocer;//怪物ID，地图内唯一；

	//怪物当前状态(巡逻，被惹怒，攻击中之类)
	//m_lMonsterHp = m_pMonsterProp->m_lMaxHp;
	m_lMonsterHp = m_pNPCProp->m_uiMaxHP; 
	m_bIsCanBeAttack = m_pNPCProp->IsCanBeAttacked();
	m_bIsCanBeAttackPvc = m_pNPCProp->IsCanBeAttackedPvc();
	m_bDropItem = true;
	m_bNotiOtherMonster = m_pNPCProp->isNeedNotiOtherMonster;
	ClearDelFlag();
	SetDelAllFlag(false);

#ifdef AI_SUP_CODE
	InitTempInfo();  //初始化tempinfo
#endif //AI_SUP_CODE

#ifdef AI_SUP_CODE
    m_curState = MSTAT_IDLE;//怪物当前所处状态； //NEWFSM
    //m_AIEventInfo = MEVENT_UPDATE;//每次调用IFsm中AI有关的事件接口时，在本信息结构中保存调用相关的信息，以备AI扩展接口查询；
	m_pIFsm = CManMonsterFsm::FindFsm( GetAIID() );//本monster使用的AI状态机接口指针
#endif //AI_SUP_CODE

#ifdef NEW_MONSTER_INFO
	m_monsterInfo.gcmonsterInfo.lMonsterID =  GetMonsterID();
	m_monsterInfo.gcmonsterInfo.lMonsterType = GetClsID();
	m_monsterInfo.gcmonsterInfo.nMapPosX = GetPosX();
	m_monsterInfo.gcmonsterInfo.nMapPosY = GetPosY();
	m_monsterInfo.gcmonsterInfo.fSpeed   = GetSpeed();
	m_monsterInfo.gcmonsterInfo.lMonsterHp =  GetHp();
	m_monsterInfo.gcmonsterInfo.MonsterDir = GetDir();
	m_monsterInfo.gcmonsterInfo.IsDirValid = true;

	m_monsterMove.gcMonsterMove.lMonsterID = GetMonsterID();
	m_monsterMove.gcMonsterMove.fSpeed  = GetSpeed();
	m_monsterMove.gcMonsterMove.nOrgMapPosX = GetPosX();
	m_monsterMove.gcMonsterMove.nOrgMapPosY = GetPosY();
#endif //NEW_MONSTER_INFO
	
	return true;	
	TRY_END;
	return false;
}


///怪物减血；注!!!!OnMonsterDie会让自身失效！！！！！！！
unsigned long CMonster::SubMonsterHp( int inHp, CPlayer* pKiller/*杀NPC者*/, bool bCreatedByDebuff /*是否为DEBUFF引起的伤害*/, int reactTime )
{
	if ( IsMonsterDie() )
	{
		D_WARNING( "CMonster::SubMonsterHp， 怪物%d已死亡，忽略再次减血\n", GetCreatureID().dwPID );
		return 0;
	}

	if( pKiller )
	{
		OnBeAttackedByPlayer( pKiller, inHp, bCreatedByDebuff, reactTime );
	}

	if( inHp == 0 )
	{
		return m_lMonsterHp;
	}

	if( NULL != pKiller )
	{
		if( !GetRewardInfo().isLocked )
		{
			CMuxMap* pMap = GetMap();
			if( pMap )
			{
				pMap->OnHandleScriptNpcFirstBeAttacked( GetClsID(), pKiller );
			}
			SetRewardInfo( pKiller->GetFullID() );
		}
	}

	//因为m_lMonsterHp无符号；
	if ( (int)m_lMonsterHp > inHp )
	{
		 m_lMonsterHp -= inHp;
#ifdef NEW_MONSTER_INFO
		 m_monsterInfo.gcmonsterInfo.lMonsterHp = m_lMonsterHp;
#endif //NEW_MONSTER_INFO
		 return m_lMonsterHp;
	} 
	else  
	{
		OnMonsterDie( pKiller );//调用死亡处理；
		return 0;
	}
}


#ifdef AI_SUP_CODE
int CMonster::ChatSurrounding( const char* pMsg, CHAT_TYPE chatType ) 
{
	//周围聊天；
	if ( NULL == pMsg )
	{
		return 0;
	}
	CMuxMap* pMap = GetMap();
	if ( NULL == pMap )
	{
		D_ERROR( "CMonster::ChatSurrounding, NULL == pMap\n" );
		return 0;
	}

	MGChat chatMsg;
	chatMsg.chatType = chatType;
	chatMsg.extInfo = 0;
	chatMsg.sourcePlayerID.wGID = 1000;
	chatMsg.sourcePlayerID.dwPID = GetMonsterID();
	SafeStrCpy( chatMsg.strChat, pMsg );

	if ( CT_SCOPE_CHAT == chatType )
	{
		pMap->NotifySurrounding<MGChat>( &chatMsg, GetPosX(), GetPosY() );	
	} 
	else if ( CT_MAP_BRO_CHAT == chatType ) 
	{
		//地图内消息；
		pMap->NotifyAllUseBrocast<MGChat>( &chatMsg );
	} 
	else if ( CT_WORLD_CHAT == chatType ) 
	{
		//世界消息发送
		D_WARNING( "暂不实现怪物世界喊话，有必要时添加\n" );		
	} 
	else 
	{
		D_ERROR( "怪物使用不可识别的聊天类型%d", chatType );
		return 0;
	}

	return 1;
};	
#endif //AI_SUP_CODE

///怪物死亡处理
void CMonster::OnMonsterDie( CPlayer* pKiller/*杀NPC者*/, GCMonsterDisappear::E_DIS_TYPE disType, bool isDropItem )
{
	if ( IsMonsterDie() )
	{
		D_ERROR( "怪物%x(类型%d)重复死亡\n", GetMonsterID(), GetClsID() );
		return;
	}
	SetMonsterDieFlag();//设置怪物已死标记；

	if ( IsToDel() )
	{
		D_ERROR( "重复OnMonsterDie, 怪物类型%d,pKiller(%s),disType:%d\n"
			, (unsigned int)GetClsID(), (NULL!=pKiller)?pKiller->GetAccount():"", disType );
		return;
	}

	m_lMonsterHp = 0;
////////////////////
#ifdef STAGE_MAP_SCRIPT
	CMuxMap* pMap = GetMap();
	if( NULL == pMap )
	{
		D_ERROR("CMonster::OnMonsterDie pMap为NULL\n");
		return;
	}
	if (GCMonsterDisappear::M_DIE == disType)
	{
		pMap->OnNpcDeadScriptHandle( pKiller, GetClsID() );
	}
#endif /*增加怪物死亡调用*/
//////////////////////

	if ( NULL != pKiller )
	{
		pKiller->OnKillNpc( this );
	}

	//怪物死亡给第一刀者经验值
	if( GetRewardInfo().isLocked )
	{
		CGateSrv* pGateSrv = CManG_MProc::FindProc( GetRewardInfo().rewardPlayerID.wGID );
		if( pGateSrv )
		{	
			CPlayer* pFirstAttackPlayer = pGateSrv->FindPlayer( GetRewardInfo().rewardPlayerID.dwPID );
			if( pFirstAttackPlayer )
			{
				pFirstAttackPlayer->OnKillMonsterAddExp( GetExp(), GetLevel(), IsBossMonster(), GetClsID());
				pFirstAttackPlayer->TryCounterEvent( CT_NPC_KILL, GetClsID(), 1 );//检查玩家身上的杀怪计数器； 
			}
		}
	}	

	if ( IsFollowing() )//是否为跟随NPC；
	{
		CPlayer* pFollowingPlayer = GetFollowingPlayer();
		if ( NULL != pFollowingPlayer )
		{
			pFollowingPlayer->ClearFollowingMonster();//清相应玩家的跟随信息；
		}
	}

	if ( NULL != m_pDeathScript )
	{ 
		//有死亡调用脚本，且有杀怪物者
		if ( NULL != pKiller )
		{
			pKiller->OnChatToNpc( this, 0 );//置对话对象为死亡NPC；
			m_pDeathScript->OnNpcDie( pKiller );//交互对象为杀怪物者，且死亡脚本只调用一个起始阶段，没有多阶段交互；
		}
	}

#ifdef ITEM_NPC
	if( IsItemNpc() )
	{	
		CMuxMap* pMap = GetMap();
		if( pMap )
		{
			pMap->SetMapItemNpcState( GetItemNpcSeq(), 100 );		//暂定100为死亡状态
			pMap->SetMapItemNpcMonsterId( GetItemNpcSeq(), 0 );		//重置为0
		}
	}
#endif //ITEM_NPC

	//死亡要清除BUFF
	//ClearAllBuff();		

	if( isDropItem )
	{
		//???,管传淇检查；
		if( GetNpcType() != NPC_TYPE_COLLECT )		//采集系的怪物不掉包裹
		{
			if( IsDropItem() )
			{
				CItemPackageManagerSingle::instance()->CreateItemPackageEx( this );
			}
		}else{
			//走采集的路线
			if( NULL != pKiller )
			{
				pKiller->OnKillCollectNpc( this );
			}
		}
		//???,管传淇检查；
	}

#ifdef AI_SUP_CODE  //AI触发
	OnSelfDie( disType );
#else  //AI_SUP_CODE;
	if( m_pActionState->OnEvent( EVENT_DIE, ACE_OS::gettimeofday() ) )
	{
		m_pMap->HandleMonsterDie( this );
	}
#endif //AI_SUP_CODE

	if ( NULL != m_pMap )
	{
		m_pMap->OnMonsterDisappear( this, disType );
	}

	return;
}


void CMonster::SetAttackFlag(bool bEnableAttack)
{
	m_bEnableAttack = bEnableAttack;
}


void CMonster::AddMuxBuffEffect( MuxBuffProp* pBuffProp )
{
	if( NULL == pBuffProp )
		return;

	if( pBuffProp->AssistType == BUFF_SP_COMPLEX )
	{
		for( unsigned int i=0; i< pBuffProp->vecBuffProp.size(); ++i )
		{
			AddMuxSingleBuffEffect( pBuffProp->vecBuffProp[i] );
		}
	}
	else
	{
		AddMuxSingleBuffEffect( pBuffProp );
	}
}


void CMonster::AddMuxSingleBuffEffect( MuxBuffProp* pBuffProp )
{
	if( NULL == pBuffProp )
		return;

	TRY_BEGIN;

	int nBuffVal = pBuffProp->Value;
	switch( pBuffProp->AssistType )
	{
	case BUFF_PHY_ATTACK_UP:
		m_buffAffect.nPhyAttack += nBuffVal;
		break;

	case BUFF_MAG_ATTACK_UP:
		m_buffAffect.nMagAttack += nBuffVal;
		break;

	case BUFF_PHY_DEFEND_UP:
		m_buffAffect.nPhyDefend += nBuffVal;
		break;

	case BUFF_MAG_DEFNED_UP:
		m_buffAffect.nMagDefend += nBuffVal;
		break;


	case BUFF_PHY_HIT_UP:
		m_buffAffect.nPhyHitLevel += nBuffVal;
		break;

	case BUFF_MAG_HIT_UP:
		m_buffAffect.nMagHitLevel += nBuffVal;
		break;

	case DEBUFF_SPEED_DOWN:
		{
			m_buffAffect.fSpeedAffect -= nBuffVal / (float)100;
			OnMonsterBuffStart( DEBUFF_SPEED_DOWN, nBuffVal );
		}
		break;

	case BUFF_ADD_FAINT_RATE:
		m_buffAffect.fAddFaintRate += nBuffVal / (float)100;
		break;

	case BUFF_ADD_FAINT_TIME:
		m_buffAffect.nAddFaintTime += nBuffVal;
		break;
	
	case BUFF_HP_HOT:
		break;

	case BUFF_HP_DOT:
		break;

	case DEBUFF_FAINT:
		{
			SetFaintFlag( true );
			D_DEBUG("monsterID:%d 调用晕眩 通知AI\n", GetMonsterID() );
			OnFaintStart();	//通知AI
		}
		break;

	case DEBUFF_BONDAGE:
		{
			SetBondageFlag( true );
			OnBondageStart();	//通知AI
		}
		break;

	case DAMAGE_REBOUND:
		{
			SetDamageReboundInfo( true, pBuffProp->mID );
		}
		break;

	case DEBUFF_PHY_DEFEND_DOWN:
		m_buffAffect.nPhyDefend -= nBuffVal;
		break;

	case DAMAGE_ABSORB:
		m_buffAffect.nAbsorbDamage = nBuffVal;
		break;

	case CLIENT_BUFF:
		break;

	case BUFF_ADD_AOE_RANGE:
		m_buffAffect.nAddAoeRange += nBuffVal;
		break;

	case BUFF_ADD_PHYCRI_DAMAGE:
		m_buffAffect.nPhyCriDamAddition += nBuffVal;
		break;

	case BUFF_ADD_MAGCRI_DAMAGE:
		m_buffAffect.nMagCriDmgAddition += nBuffVal;
		break;

	case BUFF_RATE_PHYDAMAGE_UP:        	//提高物理伤害的百分比,float型
		m_buffAffect.nAddPhyDamageRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGDAMAGE_UP:      	//提高魔法伤害的百分比
		m_buffAffect.nAddMagDamageRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_PHYCRI_DAMAGE_UP:         	//提高物理暴击攻击的伤害比率
		m_buffAffect.fAddPhyCriDmgRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGCRI_DAMAGE_UP:          	//提高魔法暴击攻击的伤害比率
		m_buffAffect.fAddMagCriDmgRate += nBuffVal / (float)100;
		break;

	case BUFF_PHYJOUK_LEVEL:        	//提高物理攻击闪避等级
		m_buffAffect.fPhyJoukRate += nBuffVal / (float)100;
		break;

	case BUFF_MAGJOUK_LEVEL:          	//提高魔法攻击闪避等级
		m_buffAffect.fMagJoukRate += nBuffVal / (float)100;
		break;

	case DEBUFF_SHEEP:         		//变羊
		m_buffAffect.isSheep = true;
		break;

	case DEBUFF_SLEEP:       	//睡眠
		m_buffAffect.isSleep = true;
		break;

	default:
		D_ERROR("1、错误的怪物BUFF类型 %d\n", pBuffProp->AssistType );
		break;
	}
	
	TRY_END;
}


bool CMonster::CreateMuxBuff( MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID )
{
	return m_buffManager.CreateMuxBuff( pBuffProp, buffTime, createID );
}


bool CMonster::CreateMuxBuffByID( unsigned int buffID, unsigned int buffTime, const PlayerID& createID )
{
	if ( IsMonsterDie() )
	{
		D_WARNING( "CMonster::CreateMuxBuffByID, 怪物%d已死亡，忽略附加buff\n", GetCreatureID().dwPID );
		return true;
	}

	MuxBuffProp* pBuffProp = BuffPropSingleton::instance()->FindTSkillProp( buffID );
	if( NULL == pBuffProp )
	{
		D_ERROR( "CMonster::CreateMuxBuffByID, 给怪物%d加buff，找不到buff%d\n", GetCreatureID().dwPID, buffID );
		return false;
	}

	return m_buffManager.CreateMuxBuff( pBuffProp, buffTime, createID );
}

void CMonster::DelMuxBuffEffect( MuxBuffProp* pBuffProp, unsigned int level )
{
	if( NULL == pBuffProp )
		return;
	
	if( pBuffProp->AssistType == BUFF_SP_COMPLEX )
	{
		for( unsigned int i = 0; i < pBuffProp->vecBuffProp.size(); ++i )
		{
			DelMuxSingleBuffEffect( pBuffProp->vecBuffProp[i], level );
		}
	}else{
		DelMuxSingleBuffEffect( pBuffProp, level );
	}

}

void CMonster::DelMuxSingleBuffEffect( MuxBuffProp* pBuffProp, unsigned int buffLevel )
{
	TRY_BEGIN;

	if ( NULL == pBuffProp )
	{
		D_ERROR( "CMonster::DelMuxSingleBuffEffect，输入buff空\n" );
		return;
	}

	int nBuffVal = pBuffProp->Value * buffLevel;
	switch( pBuffProp->AssistType )
	{
	case BUFF_PHY_ATTACK_UP:
		m_buffAffect.nPhyAttack -= nBuffVal;
		break;

	case BUFF_MAG_ATTACK_UP:
		m_buffAffect.nMagAttack -= nBuffVal;
		break;

	case BUFF_PHY_DEFEND_UP:
		m_buffAffect.nPhyDefend -= nBuffVal;
		break;

	case BUFF_MAG_DEFNED_UP:
		m_buffAffect.nMagDefend -= nBuffVal;
		break;


	case BUFF_PHY_HIT_UP:
		m_buffAffect.nPhyHitLevel -= nBuffVal;
		break;

	case BUFF_MAG_HIT_UP:
		m_buffAffect.nMagHitLevel -= nBuffVal;
		break;

	case DEBUFF_SPEED_DOWN:
		{
			m_buffAffect.fSpeedAffect += nBuffVal / (float)100;
			OnMonsterBuffEnd( DEBUFF_SPEED_DOWN, nBuffVal );
		}
		break;

	case BUFF_ADD_FAINT_RATE:
		m_buffAffect.fAddFaintRate -= nBuffVal / (float)100;
		break;

	case BUFF_ADD_FAINT_TIME:
		m_buffAffect.nAddFaintTime -= nBuffVal;
		break;
	
	case BUFF_HP_HOT:
		break;

	case BUFF_HP_DOT:
		break;

	case DEBUFF_FAINT:
		SetFaintFlag( false );
		OnFaintEnd();
		break;

	case DEBUFF_BONDAGE:
		SetBondageFlag( false );
		OnBondageEnd();
		break;

	case DAMAGE_REBOUND:
		SetDamageReboundInfo( false, 0 );
		break;

	case DEBUFF_PHY_DEFEND_DOWN:
		m_buffAffect.nPhyDefend += nBuffVal;
		break;

	case DAMAGE_ABSORB:
		m_buffAffect.nAbsorbDamage = 0;
		break;

	case CLIENT_BUFF:
		break;

	case BUFF_ADD_AOE_RANGE:
		m_buffAffect.nAddAoeRange -= nBuffVal;
		break;

	case BUFF_ADD_PHYCRI_DAMAGE:
		m_buffAffect.nPhyCriDamAddition -= nBuffVal;
		break;

	case BUFF_ADD_MAGCRI_DAMAGE:
		m_buffAffect.nMagCriDmgAddition -= nBuffVal;
		break;

	case BUFF_RATE_PHYDAMAGE_UP:        	//提高物理伤害的百分比,float型
		m_buffAffect.nAddPhyDamageRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGDAMAGE_UP:      	//提高魔法伤害的百分比
		m_buffAffect.nAddMagDamageRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_PHYCRI_DAMAGE_UP:         	//提高物理暴击攻击的伤害比率
		m_buffAffect.fAddPhyCriDmgRate -= nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGCRI_DAMAGE_UP:          	//提高魔法暴击攻击的伤害比率
		m_buffAffect.fAddMagCriDmgRate -= nBuffVal / (float)100;
		break;

	case BUFF_PHYJOUK_LEVEL:        	//提高物理攻击闪避等级
		m_buffAffect.fPhyJoukRate -= nBuffVal / (float)100;
		break;

	case BUFF_MAGJOUK_LEVEL:          	//提高魔法攻击闪避等级
		m_buffAffect.fMagJoukRate -= nBuffVal / (float)100;
		break;

	case DEBUFF_SHEEP:         		//变羊
		m_buffAffect.isSheep = false;
		break;

	case DEBUFF_SLEEP:       	//睡眠
		m_buffAffect.isSleep = false;
		break;

	default:
		D_ERROR( "错误的怪物BUFF类型:%d\n", pBuffProp->AssistType );
		break;
	}
	TRY_END;
}

#ifdef AI_SUP_CODE
void CMonster::StateUpdate()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::StateUpdate,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		return;//不执行AI；
	}

	TRY_BEGIN;
    m_AIEventInfo.Reset( GetAIFlag() );
	m_pIFsm->OnUpdate( GetMonsterID() );  // 这个是所有状态的更新函数
	return;
	TRY_END;
	return;
}

void CMonster::OnPlayerDetected(CPlayer* pDetectedPlayer)
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnPlayerDetected,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	if ( NULL == pDetectedPlayer )
	{
		D_ERROR( "CMonster::OnPlayerDetected, pDetectedPlayer == NULL\n" );
		return;
	}

	//if ( notifiedAIMonster.find( pDetectedPlayer->GetFullID().dwPID ) != notifiedAIMonster.end() )
	//{
	//	D_ERROR( "通知AI%d出现，之前已通知其该玩家%d进入\n", GetMonsterID(), pDetectedPlayer->GetFullID().dwPID );
	//	return;
	//}

	//notifiedAIMonster.insert( pDetectedPlayer->GetFullID().dwPID );
	//D_DEBUG( "通知怪物%d，玩家%d出现\n", GetMonsterID(), pDetectedPlayer->GetFullID().dwPID );

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_PLAYER_DETECTED );
	m_AIEventInfo.detectedPlayerID = pDetectedPlayer->GetFullID();
	m_AIEventInfo.detectedPlayerPosX = pDetectedPlayer->GetPosX();
	m_AIEventInfo.detectedPlayerPosY = pDetectedPlayer->GetPosY();
	m_AIEventInfo.detectedPlayerLevel = pDetectedPlayer->GetLevel();
	m_AIEventInfo.detectedPlayerHP = pDetectedPlayer->GetCurrentHP();
	//D_WARNING( "CMonster::OnPlayerDetected, call IFsm::OnPlayerDetected, player name : %s \n", pDetectedPlayer->GetAccount() );
	TRY_BEGIN;
	m_pIFsm->OnPlayerDetected( GetMonsterID() );
	return;
	TRY_END;
	return;
}

void CMonster::OnPlayerLeaveView( CPlayer* pLeavedPlayer, AILeaveReason leaveReason )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnPlayerLeaveView,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//执行AI；
		return;
	}

	if ( NULL == pLeavedPlayer )
	{
		D_ERROR( "CMonster::OnPlayerLeaveView, pLeavedPlayer == NULL\n" );
		return;
	}

	//if ( notifiedAIMonster.find( pLeavedPlayer->GetFullID().dwPID ) == notifiedAIMonster.end() )
	//{
	//	D_ERROR( "通知AI%d离开，之前没有通知其该玩家%d进入\n", GetMonsterID(), pLeavedPlayer->GetFullID().dwPID );
	//	return;
	//}

	//notifiedAIMonster.erase( pLeavedPlayer->GetFullID().dwPID );
	//D_DEBUG( "通知怪物%d，玩家%d离开\n", GetMonsterID(), pLeavedPlayer->GetFullID().dwPID );


	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_PLAYER_LEAVEVIEW );
	m_AIEventInfo.leaveReason = leaveReason;
	m_AIEventInfo.leavePlayerID = pLeavedPlayer->GetFullID();

	//D_WARNING( "CMonster::OnPlayerLeaveView, call IFsm::OnPlayerLeaveView, player name : %s \n", pLeavedPlayer->GetAccount() );
	TRY_BEGIN;
	m_pIFsm->OnPlayerLeaveView( GetMonsterID() );
	return;
	TRY_END;
	return;
}

void CMonster::OnBeAttackedByPlayer(CPlayer* pPlayer, int nDamage, bool bCreatedByDebuff, int reactTime )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnBeAttackedByPlayer,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	if ( NULL == pPlayer )
	{
		D_ERROR( "CMonster::OnBeAttackedByPlayer, pPlayer == NULL\n" );
		return;
	}

	unsigned int npcType = GetNpcType();
	if( npcType != 20 )
	{
		pPlayer->CreateRookieBreak();
	}

	m_AIEventInfo.Reset( GetAIFlag() );
	
	m_AIEventInfo.SetEvent( MEVENT_BEATTACKED );
	m_AIEventInfo.attackPlayerID = pPlayer->GetFullID();
	m_AIEventInfo.nDamage = nDamage;
	m_AIEventInfo.bCreateByDebuff = bCreatedByDebuff;	//是否为DEBUFF造成的伤害
	m_AIEventInfo.reactTime = reactTime;
	//D_WARNING( "CMonster::OnBeAttackedByPlayer, call IFsm::OnBeAttacked, player name : %s \n", pPlayer->GetAccount() );
	TRY_BEGIN;
	m_pIFsm->OnBeAttacked( GetMonsterID() );
	return;
	TRY_END;
	return;
}

void CMonster::OnBeAttackedByNpc( CMonster* pMonster, int nDamage, bool bCreatedByDebuff, int reactTime )
{
if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnBeAttackedByPlayer,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	if ( NULL == pMonster )
	{
		D_ERROR( "CMonster::OnBeAttackedByNpc, pMonster == NULL\n" );
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );
	
	m_AIEventInfo.SetEvent( MEVENT_BEATTACKED );
	m_AIEventInfo.attackPlayerID = pMonster->GetCreatureID();
	m_AIEventInfo.nDamage = nDamage;
	m_AIEventInfo.bCreateByDebuff = bCreatedByDebuff;	//是否为DEBUFF造成的伤害
	m_AIEventInfo.reactTime = reactTime;
	//D_WARNING( "CMonster::OnBeAttackedByPlayer, call IFsm::OnBeAttacked, player name : %s \n", pPlayer->GetAccount() );
	TRY_BEGIN;
	m_pIFsm->OnBeAttacked( GetMonsterID() );
	return;
	TRY_END;
	return;
}


void CMonster::OnPlayerDie( CPlayer* pDiePlayer )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnBeAttackedByPlayer,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	if ( NULL == pDiePlayer )
	{
		D_ERROR( "CMonster::OnPlayerDie, pDiePlayer == NULL\n" );
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_PLAYER_DIE );
	m_AIEventInfo.diePlayerID = pDiePlayer->GetFullID();
	TRY_BEGIN;
	m_pIFsm->OnPlayerDie( GetMonsterID() );
	return;
	TRY_END;
	return;
}

void CMonster::OnBeAttacckByNpc( CMonster* pAtkMonster, unsigned int damage )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnBeAttackedByPlayer,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	if ( NULL == pAtkMonster )
	{
		D_ERROR( "CMonster::OnBeAttackByNpc, pAtkMonster == NULL\n" );
		return;
	}

	if ( pAtkMonster->IsDisableAI() )
	{
		D_ERROR( "CMonster::OnBeAttackByNpc, disableAI NPC(clsid:%d) attack monster\n", pAtkMonster->GetClsID() );
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_BEATTACKED );
	m_AIEventInfo.attackPlayerID = pAtkMonster->GetCreatureID();
	m_AIEventInfo.nDamage = damage;
	m_AIEventInfo.bCreateByDebuff = false;	//是否为DEBUFF造成的伤害
	m_AIEventInfo.reactTime = 0;
	//D_WARNING( "CMonster::OnBeAttackedByPlayer, call IFsm::OnBeAttacked, player name : %s \n", pPlayer->GetAccount() );
	TRY_BEGIN;
	m_pIFsm->OnBeAttacked( GetMonsterID() );
	return;
	TRY_END;
	return;
}

void CMonster::OnAttackTargetLeave()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnAttackTargetLeave,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_ATTACKTARGET_LEAVE );
	//D_WARNING( "CMonster::OnBeAttackedByPlayer, call IFsm::OnBeAttacked, monster ID : %d \n", GetMonsterID() );
	TRY_BEGIN;	
	m_pIFsm->OnAttackTargetLeave( GetMonsterID() );
	return;
	TRY_END;
	return;
}


////触发发现NPC事件
//void CMonster::OnNpcDetected()
//{
//
//	if( NULL == m_pIFsm )
//	{
//		//D_ERROR("CMonster::OnNpcDetected,状态机为空\n");
//		return;
//	}
//
//	if ( IsDisableAI() )
//	{
//		//不执行AI；
//		return;
//	}
//
//	m_AIEventInfo.Reset( GetAIFlag() );
//
//	m_AIEventInfo.SetEvent( MEVENT_NPC_DETECTED );
//
//	TRY_BEGIN;
//	m_pIFsm->OnNpcDetected( GetMonsterID() );
//	return;
//	TRY_END;
//	return;
//}

////触发NPC离开事件
//void CMonster::OnNpcLeave()
//{
//	if( NULL == m_pIFsm )
//	{
//		//D_ERROR("CMonster::OnNpcLeave,状态机为空\n");
//		return;
//	}
//
//	if ( IsDisableAI() )
//	{
//		//不执行AI；
//		return;
//	}
//
//	m_AIEventInfo.Reset( GetAIFlag() );
//
//	TRY_BEGIN;
//	m_pIFsm->OnNpcLeave( GetMonsterID() );
//	return;
//	TRY_END;
//	return;
//}

void CMonster::OnSelfBorn()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfBorn,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_SELF_BORN );	
	//D_WARNING( "CMonster::OnSelfBorn, call IFsm::OnSelfBorn, monsterID : %d\n ", GetMonsterID() );
	TRY_BEGIN;
	CMuxMap* pCurMap = GetMap();
	if ( NULL != pCurMap )
	{
		m_pIFsm->OnSelfBorn( GetMonsterID(), pCurMap->GetMapInfoID() );
	} else {
		D_ERROR( "CMonster::OnSelfBorn，怪物出生地图空\n" );
	}
	return;
	TRY_END;
	return;
}

void CMonster::OnSelfDie( GCMonsterDisappear::E_DIS_TYPE disType )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_SELF_DIE );
	m_AIEventInfo.disType = disType;
	//D_WARNING( "CMonster::OnSelfDie, call IFsm::OnSelfDie, monsterID : %d\n ", GetMonsterID() );
	TRY_BEGIN;
	m_pIFsm->OnSelfDie( GetMonsterID() );
	return;
	TRY_END;
	return;
}


//跟随的玩家被打
void CMonster::OnFollowPlayerBeAttack( const PlayerID& attackID, int damage )
{
		if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_FOLLOWPLAYER_BEATTACK );
	m_AIEventInfo.attackFollowingID = attackID;
	m_AIEventInfo.nDamage = damage;
	
	TRY_BEGIN;
	m_pIFsm->OnFollowingPlayerBeAttack( GetMonsterID() );
	TRY_END;
	return;
}

///怪物进入视野，收到此事件后，通过GetAIEventInfo取得具体的进入视野怪物信息；
bool CMonster::OnMonsterEnterView( CMonster* pEnterViewMonster )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnMonsterEnterView,状态机为空\n");
		return false;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return false;
	}

	if( NULL == pEnterViewMonster )
	{
		return false;
	}

	if ( pEnterViewMonster->IsDisableAI() )
	{
		return false;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_MONSTER_ENTERVIEW );
	m_AIEventInfo.enterViewMonsterID = pEnterViewMonster->GetMonsterID();

	TRY_BEGIN;
	m_pIFsm->OnMonsterEnterView( GetMonsterID() );
	return true;
	TRY_END;
	return false;
};

///怪物离开视野，收到此事件后，通过GetAIEventInfo取得具体的离开视野怪物信息；
bool CMonster::OnMonsterLeaveView( CMonster* pLeaveViewMonster )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnMonsterLeaveView,状态机为空\n");
		return false;
	}

	if( NULL == pLeaveViewMonster )
	{
		return false;
	}

	if ( pLeaveViewMonster->IsDisableAI() )
	{
		return false;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return false;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_MONSTER_LEAVEVIEW );
	m_AIEventInfo.leaveViewMonsterID = pLeaveViewMonster->GetMonsterID();

	TRY_BEGIN;
	m_pIFsm->OnMonsterLeaveView( GetMonsterID() );
	return true;
	TRY_END;
	return false;
};

///有人与怪物对话；
bool CMonster::OnMonsterBeChat()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnMonsterBeChat,状态机为空\n");
		return false;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return false;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_MONSTER_BECHAT );

	TRY_BEGIN;
	m_pIFsm->OnMonsterBeChat( GetMonsterID() );
	return true;
	TRY_END;
	return false;
}

///本怪物召唤的怪物出生，通过GetAIEventInfo取新出生怪物ID号；
bool CMonster::OnCalledMonsterBorn( unsigned long calledMonsterID, unsigned int aiFlag )
{
	if( NULL == m_pIFsm )
	{
		D_ERROR("CMonster::OnCalledMonsterBorn,状态机为空\n");
		return false;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return false;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_CALLEDMONSTER_BORN );
	m_AIEventInfo.calledMonsterID = calledMonsterID;
	m_AIEventInfo.bornAiFlag = aiFlag;

	TRY_BEGIN;
	m_pIFsm->OnCalledMonsterBorn( GetMonsterID() );
	return true;
	TRY_END;
	return false;
}

///周围玩家移动状态改变(原因：新移动或进入怪物所在块)，通过GetAIEventInfo取得具体玩家的ID号；
bool CMonster::OnSurPlayerMovChg( CPlayer* pMovChgPlayer )
{
	if ( NULL == pMovChgPlayer )
	{
		D_ERROR( "CMonster::OnSurPlayerMovChg, 输入移动状态改变玩家指针为空\n" );
		return false;
	}

	if( NULL == m_pIFsm )
	{
		D_ERROR("CMonster::OnSurPlayerMovChg,状态机为空\n");
		return false;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return false;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_SURPLAYER_MOVCHG );
	m_AIEventInfo.movchgPlayerID = pMovChgPlayer->GetFullID();
	m_AIEventInfo.movechgPlayerPosX = pMovChgPlayer->GetPosX();
	m_AIEventInfo.movechgPlayerPosY = pMovChgPlayer->GetPosY();
	m_AIEventInfo.movchgPlayerLevel = pMovChgPlayer->GetLevel();

	TRY_BEGIN;
	m_pIFsm->OnSurPlayerMovChg( GetMonsterID() );
	return true;
	TRY_END;
	return false;
}

void CMonster::OnDebuff( CPlayer* pPlayer, int nDamage)
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( NULL == pPlayer )
	{
		return;
	}

	if ( IsDisableAI() )
	{
		//不执行AI；
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_ON_DEBUFF );
	m_AIEventInfo.attackPlayerID = pPlayer->GetFullID();
	m_AIEventInfo.nDamage = nDamage;
	TRY_BEGIN;
	m_pIFsm->OnDebuff( GetMonsterID() );
	return;
	TRY_END;
	return;
}

//怪物晕旋开始
void CMonster::OnFaintStart()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	TRY_BEGIN;
	m_pIFsm->OnFaintStart( GetMonsterID() );
	TRY_END;
}

//怪物晕旋结束
void CMonster::OnFaintEnd()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	TRY_BEGIN;
	m_pIFsm->OnFaintEnd( GetMonsterID() );
	TRY_END;
}

//怪物束缚开始
void CMonster::OnBondageStart()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	TRY_BEGIN;
	m_pIFsm->OnBondageStart( GetMonsterID() );
	TRY_END;
}


//怪物束缚结束
void CMonster::OnBondageEnd()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	TRY_BEGIN;
	m_pIFsm->OnBondageEnd( GetMonsterID() );
	TRY_END;
}

void CMonster::OnMonsterBuffStart( BUFF_TYPE buffType, int buffVal )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	TRY_BEGIN;
	m_AIEventInfo.SetEvent( MEVENT_BUFF_START );
	m_AIEventInfo.buffType = buffType;
	m_AIEventInfo.buffVal = buffVal;
	m_pIFsm->OnMonsterBuffStart( GetMonsterID() );
	TRY_END;
}


void CMonster::OnMonsterBuffEnd( BUFF_TYPE buffType, int nBuffVal )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	TRY_BEGIN;
	m_AIEventInfo.SetEvent( MEVENT_BUFF_END );
	m_AIEventInfo.buffType = buffType;
	m_AIEventInfo.buffVal = nBuffVal;
	m_pIFsm->OnMonsterBuffEnd( GetMonsterID() );
	TRY_END;
}


///怪物开始跟随；
void CMonster::OnStartFollowing( unsigned int taskID ) 
{ 
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.followingTaskID = taskID; 

	TRY_BEGIN;
	m_pIFsm->OnStartFollowing( GetMonsterID() );
	return;
	TRY_END;
	return; 
}

//怪物受到自爆
void CMonster::OnBeExplosion( CPlayer *pExplosionPlayer, CMuxSkillProp* pSkillProp, unsigned int exArg )
{
	if( NULL == pExplosionPlayer || NULL == pSkillProp )
		return;

	CMuxMap* pMap = pExplosionPlayer->GetCurMap();
	if( NULL == pMap )
		return;

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		return;
	}

	vector<int> vecRst;
	if( !pScript->FnExlosionPvc( pExplosionPlayer, this, exArg, &vecRst ) )
	{
		return;
	}

	if( vecRst.size() != 2 )
	{
		D_ERROR("FnExlosionPvc 返回值数量错误\n");
		return;
	}

	unsigned int damage = vecRst[0];
	EBattleFlag exFlag = (EBattleFlag)vecRst[1];

	if ( (damage <= 0) && (exFlag != MISS) )
	{
		D_WARNING( "CPlayer::OnBeExplosion, FnExlosionPvc, 战斗脚本伤血为0但返回为非MISS\n" );
	}

	SubMonsterHp( damage, pExplosionPlayer, false, pSkillProp->m_nCurseTime );

	MGUseSkillRst gcSkill;
	gcSkill.skillRst.attackHP = pExplosionPlayer->GetCurrentHP();
	gcSkill.skillRst.attackID = pExplosionPlayer->GetFullID();
	gcSkill.skillRst.attackMP = pExplosionPlayer->GetCurrentMP();
	gcSkill.skillRst.battleFlag = exFlag;
	gcSkill.skillRst.causeBuffID = 0;
	gcSkill.skillRst.lCooldownTime = pSkillProp->m_nCoolDown;
	gcSkill.skillRst.lCurseTime = pSkillProp->m_nCurseTime;
	gcSkill.skillRst.lHpLeft = GetHp();
	gcSkill.skillRst.lHpSubed = damage;
	gcSkill.skillRst.nErrCode = BATTLE_SUCCESS;
	gcSkill.skillRst.scopeFlag = SCOPE_ATTACK;
	gcSkill.skillRst.skillType = EXPLOSION_SKILL;
	gcSkill.skillRst.targetID = GetCreatureID();
	gcSkill.skillRst.targetRewardID = GetRewardInfo().rewardPlayerID;
	gcSkill.skillRst.sequenceID = 0;

	pMap->NotifySurrounding<MGUseSkillRst>( &gcSkill, GetPosX(), GetPosY() );
}


///通知跟随怪物，其所跟随的玩家开始移动；
void CMonster::OnFollowPlayerStartMoving()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	TRY_BEGIN;
	m_pIFsm->OnFollowPlayerStartMoving( GetMonsterID() );
	return;
	TRY_END;
	return; 
}


void CMonster::OnPlayerAddHp( const PlayerID& addStarter, const PlayerID& addTarget, unsigned int addVal )
{
		if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_PLAYER_ADDHP );
	m_AIEventInfo.addVal = addVal;
	m_AIEventInfo.addHpStarter = addStarter;
	m_AIEventInfo.addHpTarget = addTarget;

	TRY_BEGIN;
	m_pIFsm->OnPlayerAddHp( GetMonsterID() );
	return;
	TRY_END;
	return; 

}


//怪物调试威胁列表开始
void CMonster::OnDebugThreatListStart()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	TRY_BEGIN;
	m_pIFsm->OnDebugThreatListStart( GetMonsterID() );
	return;
	TRY_END;
	return;
}

//怪物调试威胁列表结束
void CMonster::OnDebugThreatListEnd()
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnSelfDie,状态机为空\n");
		return;
	}

	if( IsDisableAI() )
	{
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	TRY_BEGIN;
	m_pIFsm->OnDebugThreatListEnd( GetMonsterID() );
	return;
	TRY_END;
	return;
}

void CMonster::OnPlayerDropTask(unsigned int taskid )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnPlayerDropTask,状态机为空\n");
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_PLAYER_DROP_TASK );
	m_AIEventInfo.dropTaskID = taskid;

	TRY_BEGIN;
	m_pIFsm->OnPlayerDropTask( GetMonsterID() );
	return;
	TRY_END;
	return ;
}

//怪物被击退到新位置(单位0.1格)
void CMonster::OnBeKickBack( TYPE_POS posX10, TYPE_POS posY10 )
{
	if( NULL == m_pIFsm )
	{
		//D_ERROR("CMonster::OnPlayerDropTask,状态机为空\n");
		return;
	}

	m_AIEventInfo.Reset( GetAIFlag() );

	m_AIEventInfo.SetEvent( MEVENT_BE_KICK_BACK );
	m_AIEventInfo.kickBackPosX = posX10;
	m_AIEventInfo.kickBackPosY = posY10;

	TRY_BEGIN;
	m_pIFsm->OnBeKickBack( GetMonsterID() );
	return;
	TRY_END;
	return ;
}

unsigned int CMonster::GetInitHp()
{
	if( NULL == m_pNPCProp)
	{
		D_ERROR("怪物属性不存在\n");
		return 0;
	}
	return m_pNPCProp->m_uiMaxHP;
}


bool CMonster::ReturnStarted()
{
	if( !m_pNPCProp )
		return false;

	//D_DEBUG("调用怪物回归\n");
	ClearRewardInfo();
	SetHp( m_pNPCProp->m_uiMaxHP );
	SetAttackFlag( false );

	//位置调试，D_DEBUG( "怪物回归开始：(%d,%d)\n", GetPosX(), GetPosY() );
	CMuxMap* pMap = GetMap();
	if ( NULL == pMap )
	{
		return false;
	}
	pMap->NotifySurrounding<MGMonsterInfo>( GetMonsterInfo() , GetPosX(), GetPosY() );
	
	return true;
}

void CMonster::ReturnEnded()
{
	//位置调试，D_DEBUG( "怪物回归结束：(%d,%d)\n", GetPosX(), GetPosY() );
	SetAttackFlag( true );
	//SetRewardPlayer( NULL );
}

//void CMonster::Stop()
//{
//	if( NULL ==  m_pMap )
//	{
//		D_ERROR("CMonster::Stop()  m_pMap == NULL\n");
//		return;
//	}
//	m_pMap->HandleMonsterStop( this );
//}

#endif //AI_SUP_CODE


void CMonster::BuffProcess()
{
	m_buffManager.BuffProcess();
}


bool CMonster::RelieveBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve/*是否只是探测可能性*/ )
{
	return m_buffManager.RelieveBuff( relieveType, relieveLevel, relieveNum, isRealRelieve );
}


void CMonster::NotifyDamageRebound( unsigned int skillID, unsigned int reboundDamage, EBattleFlag btFlag )
{
	MGCharacterDamageRebound damReb;
	damReb.gcRebound.characterID = GetCreatureID();
	damReb.gcRebound.skillID = skillID;
	damReb.gcRebound.reboundDamage = reboundDamage;
	damReb.gcRebound.battleFlag = btFlag;
	if ( (reboundDamage <= 0) && (btFlag != MISS) )
	{
		D_WARNING( "CMonster::NotifyDamageRebound，伤血为0但返回为非MISS, skillID:%d\n", skillID );
	}
	CMuxMap* pMap = GetMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGCharacterDamageRebound>( &damReb, GetPosX(), GetPosY() );
	}
}
