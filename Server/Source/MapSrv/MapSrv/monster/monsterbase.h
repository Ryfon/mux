﻿/**
* @file monsterbase.h
* @brief 定义NPC
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: monsterbase.h
* 摘    要: 增加战斗宠后新加，准备统一战斗宠monster的共同部分；
* 作    者: dzj
* 完成日期: 2010.09.17
* 
*/


#pragma once

#include "tcache/tcache.h"
#include <vector>
#include <map>
#include "../Player/Player.h"
#include "../NPCProp.h"
#include "../Fsm/IFsm.h" //NEWFSM
#include "../Lua/NpcChatScript.h"
#include "../PoolManager.h"
#include "../MuxMap/MapCommonHead.h"
#include "../NpcStandRule.h"
#include "../MonsterBuffs.h"

using namespace std;

extern CPoolManager* g_poolManager;
class CCreatorIns;

/**
* NPC类，用于定义NPC实例
* 每次定义NPC实例时需要指定该NPC所属类别的ID号；
*/

//NPC需要修改的或者需要通过buff计算的属性

typedef struct StrNPCTmpInfo
{
	unsigned int uiMaxHP;
	unsigned short usInherenStand;
	float fPhyDodge;				//物理回避
	float fMagDodge;				//魔法回避
	int nHit;					//命中
	int nMagDef;				//魔法防御力
	int nPhyDef;				//物理防御力
	float fSpeed;				//怪物的当前速度
}NPCTmpInfo;


typedef struct StrScopeMonsterInfo
{
	TYPE_ID monsterID;
	TYPE_POS posX;
	TYPE_POS posY;
}ScopeMonsterInfo;

typedef struct StrScopePlayerInfo
{
	PlayerID playerID;
	TYPE_POS posX;
	TYPE_POS posY;
	unsigned short playerLevel;
}ScopePlayerInfo;

typedef struct StrRewardInfo
{
	StrRewardInfo()
	{
		ClearRewardInfo();
	}

	void ClearRewardInfo()
	{
		rewardPlayerID.dwPID = 0;
		rewardPlayerID.wGID  = 0;
		isLocked = false;
	}

	PlayerID rewardPlayerID;
	bool isLocked;

}RewardInfo;


typedef struct StrMonsterBuffAffect
{
	int nPhyAttack;			//改变的物理攻击
	int nPhyDefend;			//改变的物理防御
	int nMagAttack;			//改变的魔法攻击
	int nMagDefend;        //改变的魔法DEFEND
	int nPhyHitLevel;          //改变的物理HIT
	int nMagHitLevel;          //改变的魔法HIT
	int nPhyCriLevel;    //改变的物理CRI
	int nMagCriLevel;    //改变的魔法CRI
	float fSpeedAffect;		   //影响的速度
	int	  nAddFaintTime;		//增加的晕眩时间
	float fAddFaintRate;	//增加的晕眩几率
	bool  isDamageRebound;		//伤害反弹
	unsigned int DrArg;			//反弹参数
	unsigned int nAbsorbDamage;	//寄主能吸收的伤害 
	int		nAddAoeRange;		//提高范围技能的攻击范围
	int		nPhyCriDamAddition; //物理暴击的追加伤害
	int		nMagCriDmgAddition;	//魔法暴击的追加伤害
	float	nAddPhyDamageRate;	//物理攻击的伤害提升百分比
	float	nAddMagDamageRate;	//魔法攻击的伤害提升百分比
	float	fAddPhyCriDmgRate;	//增加物理暴击伤害值
	float	fAddMagCriDmgRate;	//增加魔法暴击伤害值
	float	fPhyJoukRate;		//物理躲闪几率
	float	fMagJoukRate;		//魔法躲闪几率
	bool	isSleep;			//是否睡眠
	bool	isSheep;			//是否变羊

	void Reset()
	{
		nPhyAttack = 0;
		nPhyDefend = 0;
		nMagAttack = 0;
		nMagDefend = 0;
		nPhyHitLevel = 0;
		nMagHitLevel = 0;
		nPhyCriLevel = 0;
		nMagCriLevel = 0;
		fSpeedAffect = 0;
		nAddFaintTime = 0;
		fAddFaintRate = 0;
		isDamageRebound = false;		//伤害反弹
		DrArg = 0;						//反弹参数
		nAbsorbDamage = 0;
		nAddAoeRange = 0;
		nPhyCriDamAddition = 0;
		nMagCriDmgAddition = 0;
		nAddPhyDamageRate = 0.0f;
		nAddMagDamageRate = 0.0f;
		fAddPhyCriDmgRate = 0.0f;
		fAddMagCriDmgRate = 0.0f;
		fPhyJoukRate = 0.0f;
		fMagJoukRate = 0.0f;
		isSleep = false;
		isSheep = false;
	}

	StrMonsterBuffAffect()
	{
		Reset();
	}
}MonsterBuffAffect;

//怪物移动信息；
struct MonsterMoveInfo
{
public:
	MonsterMoveInfo() : isMoving(false) {};
    ~MonsterMoveInfo() {};

public:
	void SetMonsterMovInfo( unsigned short inTargetGridX, unsigned short inTargetGridY )
	{
		isMoving = true;
		targetGridX = inTargetGridX;
		targetGridY = inTargetGridY;		
	}

	void ResetMonsterMovInfo()
	{
		isMoving = false;
	}

public:
	///当前在否在移动；
	bool IsMonsterMoving()	{ return isMoving; }

	unsigned short GetTargetGridX() { return targetGridX; }
	unsigned short GetTargetGridY() { return targetGridY; }

private:
	bool  isMoving;//怪物是否处于移动状态；
	unsigned short targetGridX;//目标点浮点X坐标；
	unsigned short targetGridY;//目标点浮点Y坐标；
};

//战斗宠与monster的共同基类；
class CMonsterBase : public CMuxCreature
{

};
