﻿/**
* @file monster.h
* @brief 定义NPC
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: monster.h
* 摘    要: 定义NPC
* 作    者: dzj
* 完成日期: 2007.12.24
* 
*/

#pragma once

//10.09.17//#include "manmonsterprop.h"
//#include "tcache/tcache.h"
//#include <vector>
//#include <map>
//#include "../Player/Player.h"
//#include "../NPCProp.h"
//#include "../Fsm/IFsm.h" //NEWFSM
//#include "../Lua/NpcChatScript.h"
//#include "../PoolManager.h"
//#include "../MuxMap/MapCommonHead.h"
//#include "../NpcStandRule.h"
//#include "../MonsterBuffs.h"
//
//using namespace std;
//
//extern CPoolManager* g_poolManager;
//class CCreatorIns;
//10.09.17//

#include "monsterbase.h"

/**
* NPC类，用于定义NPC实例
* 每次定义NPC实例时需要指定该NPC所属类别的ID号；
*/
 
///*怪物的移动方向为8个方向*/
//enum MONSTER_DIR { NORTH = 0, NORTHEAST, EAST, SOUTHEAST, SOUTH, SOUTHWEST, WEST, NORTHWEST};
/*怪物出边界或者遇到障碍物的转向方案*/
enum DIR_CHANGE { TURN_LEFT = 1, TURN_RIGHT};  
///通知AI玩家离开怪物视野的原因；
enum AILeaveReason { AILR_DIE = 0/*玩家死亡*/, AILR_LEAVEVIEW/*玩家离开视野范围*/, AILR_LEAVEMAP/*玩家离开地图*/, AILR_INVALID/*无效原因*/ };

#ifdef AI_SUP_CODE
//每次调用IFsm中AI有关的事件接口时，在本信息结构中保存调用相关的信息，以备AI扩展接口查询；
struct AIEventInfo
{
	void* selfAIFlag;//怪物AI标记；
	MONSTER_EVENT eventType;//事件类型

	TYPE_POS    kickBackPosX;//击退事件，被击怪物的新位置X(单位0.1格)
	TYPE_POS    kickBackPosY;//击退事件，被击怪物的新位置Y(单位0.1格)

	PlayerID    attackPlayerID;		//BeAttacked事件攻击的玩家
	int  nDamage;	//BeAttacked事件受到的伤害值
	bool bCreateByDebuff;		//是否由DEBUFF造成的
	int  reactTime;				//被攻击后转换状态的响应时间 

	AILeaveReason leaveReason;//PLAYER LEAVE事件中的玩家离开原因
	PlayerID      leavePlayerID;		//PLAYER LEAVE事件中的离开玩家

	GCMonsterDisappear::E_DIS_TYPE disType;//死亡事件时的额外信息；
	PlayerID      diePlayerID; //PLAYER_DIE事件中的死亡玩家

	PlayerID      detectedPlayerID;	//PLAYER_DETECTED事件中的出现玩家
	TYPE_POS      detectedPlayerPosX;	//探测到的玩家X
	TYPE_POS      detectedPlayerPosY;	//探测到的玩家Y
	unsigned int  detectedPlayerHP;//detected player的HP值
	unsigned short detectedPlayerLevel;	//探测到的玩家等级
	unsigned int   bornAiFlag;

	unsigned long enterViewMonsterID;//MEVENT_MONSTER_ENTERVIEW中的进入视野怪物；
	unsigned long leaveViewMonsterID;//MEVENT_MONSTER_LEAVEVIEW中的离开视野怪物；
	PlayerID      movchgPlayerID;//MEVENT_SURPLAYER_MOVCHG中移动状态发生改变的玩家；
	unsigned long calledMonsterID;//MEVENT_CALLEDMONSTER_BORN所召唤的怪物；
	unsigned short movchgPlayerLevel;
	TYPE_POS movechgPlayerPosX;
	TYPE_POS movechgPlayerPosY;
	PlayerID attackFollowingID;		//人或怪物  怪物GID为MONSTER_GID 

	PlayerID addHpStarter;	//使用加血技能的人
	PlayerID addHpTarget;	//使用加血的人
	unsigned int addVal;	//加血的量

	BUFF_TYPE buffType;		//针对MONSTERBUFFSTART和MONSTERBUFFEND 
	int buffVal;			//该BUFF影响的值
	unsigned int followingTaskID;
	unsigned int dropTaskID;


	void SetEvent( MONSTER_EVENT monsterEvent)
	{
		eventType = monsterEvent;
	}

	void Reset( void* inAIFlag )
	{
		selfAIFlag = inAIFlag;
		eventType = MEVENT_UPDATE;
		attackPlayerID.wGID  = 0;
		attackPlayerID.dwPID = 0;
		nDamage = 0;

		leaveReason = AILR_INVALID;//重置离开原因；
		leavePlayerID.wGID  = 0;
		leavePlayerID.dwPID = 0;
		

		detectedPlayerID.wGID  = 0;
		detectedPlayerID.dwPID = 0;
		detectedPlayerPosX = 0;	//探测到的玩家X
		detectedPlayerPosY = 0;	//探测到的玩家Y
		detectedPlayerHP = 0;

		diePlayerID.wGID = 0;
		diePlayerID.dwPID = 0;

		enterViewMonsterID = 0;
		leaveViewMonsterID = 0;

		dropTaskID = 0;

	    kickBackPosX = 0;//击退事件，被击怪物的新位置X
	    kickBackPosY = 0;//击退事件，被击怪物的新位置Y
	}
};
#endif //AI_SUP_CODE


class CMonster : public CMuxCreature
{
public:
	CREATURE_TYPE GetType() { return CREA_TYPE_MONSTER; };//应晓强要求添加，08.09.01

private:
	CMonster( const CMonster& inMonster );  //屏蔽这两个操作；
	CMonster& operator = ( const CMonster& inMonster );//屏蔽这两个操作；

///////////////////初始化/////////////////////////////////////////
public:
	//构造函数
	CMonster();
	//析构函数
	virtual ~CMonster(); 
	
	//对象池的初始化
	PoolFlagDefine()
	{
		SetMonsterDieFlag();//池中或刚从池中取出时设置为死；
		/*初始设置为0*/
		ClearFollowing();//所有NPC，除非显式设置，否则全都不是跟随NPC；

		m_aiFlag = NULL;//怪物的AI标记；

		m_pCreator = NULL;   //由管理器管理
		m_pMap = NULL;	//管理器管理
		m_pTargetPlayer = NULL;  //管理器管理
		//m_pRewardPlayer = NULL; //第一个打怪的人

#ifdef AI_SUP_CODE
		m_curState = MSTAT_IDLE;//怪物当前所处状态； //NEWFSM
		// m_AIEventInfo = MEVENT_UPDATE;//每次调用IFsm中AI有关的事件接口时，在本信息结构中保存调用相关的信息，以备AI扩展接口查询；
		m_pIFsm = NULL;//本monster使用的AI状态机接口指针；
		m_bEnableAttack = true;
#endif  //AI_SUP_CODE

//		m_PlayerNearby.Clear();//清附近玩家；
		ResetMonsterMovInfo();
#ifdef AI_SUP_CODE
		m_curState = MSTAT_IDLE;//NEWFSM
#endif //AI_SUP_CODE

		m_pChatScript = NULL;//聊天脚本；
		m_pDeathScript = NULL;//死亡事件调用脚本；
		ClearDelFlag();
        //m_vecSelfUsers.clear();//清本对象保存的所有引用本对象者；

		m_dwMonsterClsID = 0;
		m_bEnableAttack = true;
		m_pTargetPlayer = NULL;
		m_nPosX = 0;
		m_nPosY = 0;
		m_BornPt.nPosX = 0;
		m_BornPt.nPosY = 0;
		m_bEnableAttack = true;
		m_lMonsterHp = 0;
		m_wMonsterDir = 0;
		//10.09.19已废弃//m_lMonsterStat = 0;
		m_bEnableAttack = true;
		StructMemSet( m_monsterInfo, 0, sizeof( m_monsterInfo) );
		StructMemSet( m_monsterMove, 0, sizeof( m_monsterMove) );
		m_buffNum = 0;
		ClearRewardInfo();
#ifdef ITEM_NPC
		m_isItemNpc = false;
		m_itemNpcSeq = 0;
#endif //ITEM_NPC
		m_bNotiOtherMonster = true;
		m_bIsBeAttacked = false;
		m_bIsToAllDel = false;
	}

	//初始化怪物的战斗信息
	void InitTempInfo();

public:
	//生成怪物的时候的初始化！
	bool InitMonster( unsigned long dwMonsterClsID );
///////////////////初始化////////////////////////////////////////

//////////////////////////////获取设置属性////////////////////////
public:
	CCreatorIns* GetOwnCreator()
	{
		return m_pCreator;
	}


	void SetOwnCreator( CNewMap* pMap, CCreatorIns* pCreator/*所属生成器*/ )
	{
		m_pMap = pMap; //所在地图
		m_pCreator = pCreator;//所属生成器；
	}

	///取怪物类别号；
	unsigned long GetClsID() { return m_dwMonsterClsID; }
    ///取怪物实例标识号；
	unsigned int GetMonsterID() { return m_lMonsterID; }

	CPlayer* GetTargetPlayer() { return m_pTargetPlayer; }

	bool GetAttackFlag() {	return m_bEnableAttack; }

	int GetBornPtX() { return m_BornPt.nPosX; }
	int GetBornPtY() { return m_BornPt.nPosY; }

	void SetPos( int posX, int posY )
	{
		m_nPosX = posX;
		m_nPosY = posY;

#ifdef NEW_MONSTER_INFO
		m_monsterInfo.gcmonsterInfo.nMapPosX = m_nPosX;
		m_monsterInfo.gcmonsterInfo.nMapPosY = m_nPosY;
		m_monsterMove.gcMonsterMove.nOrgMapPosX = m_nPosX;
		m_monsterMove.gcMonsterMove.nOrgMapPosY = m_nPosY;
#endif //NEW_MONSTER_INFO

		return;
	}

	const PlayerID GetCreatureID() 
	{
		PlayerID targetID;
		targetID.dwPID = m_lMonsterID;
		targetID.wGID = MONSTER_GID;
		return targetID;
	}

	bool GetPos( TYPE_POS& posX, TYPE_POS& posY )
	{
		TRY_BEGIN;
		posX = m_nPosX;
		posY = m_nPosY;
		return true;
		TRY_END;
		return false;
	}

	unsigned short GetPosX() { return m_nPosX; };
	unsigned short GetPosY() { return m_nPosY; };

	///取怪物HP；
	unsigned long GetHp() { return m_lMonsterHp; }

	void SetHp( unsigned long monsterHp )
	{
		m_lMonsterHp = monsterHp;

#ifdef NEW_MONSTER_INFO
		m_monsterInfo.gcmonsterInfo.lMonsterHp = m_lMonsterHp;
#endif //NEW_MONSTER_INFO
	}

	void SetBornPt( int nX, int nY )
	{
		m_BornPt.nPosX = nX;
		m_BornPt.nPosY = nY;
		return;
	}

	inline CNewMap* GetMap() { return m_pMap; };

	unsigned long GetMapIDOfMonster();

	//void SetTargetPlayer(CPlayer* pPlayer) { m_pTargetPlayer = pPlayer; 	}

	//引用自身的对象管理
	void SetDelFlag() 
	{
		TRY_BEGIN;

		m_bIsToDel = true; 

		if ( NULL != m_pIFsm )
		{
			m_pIFsm = NULL;
		}

		TRY_END;
	};
	void ClearDelFlag() { m_bIsToDel = false; };
	bool IsToDel() { return m_bIsToDel; };

	bool IsToDelAll() { return m_bIsToAllDel; }
	void SetDelAllFlag(bool m_b) { m_bIsToAllDel = m_b; }


//////////////////怪物逻辑////////////////////
public:
	///怪物减血；注!!!!OnMonsterDie会让自身失效！！！！！！！
	unsigned long SubMonsterHp( int inHp, CPlayer* pKiller/*杀NPC者*/, bool bCreatedByDebuff /*是否为DEBUFF引起的伤害*/, int reactTime );

	unsigned long AddHp(unsigned int addHp)
	{
		if( !m_pNPCProp )
			return m_lMonsterHp;

		if( addHp <= 0)
			return m_lMonsterHp;

		if( m_lMonsterHp + addHp > m_pNPCProp->m_uiMaxHP )
			m_lMonsterHp = m_pNPCProp->m_uiMaxHP;

		m_lMonsterHp += addHp;

#ifdef NEW_MONSTER_INFO
		m_monsterInfo.gcmonsterInfo.lMonsterHp = m_lMonsterHp;
#endif //NEW_MONSTER_INFO

		return m_lMonsterHp;
	}


#ifdef AI_SUP_CODE  //04.14
	int ChatSurrounding( const char* pMsg, CHAT_TYPE chatType );
#endif //AI_SUP_CODE	

public:
	///快时钟到达时执行的操作；
	void FastTimerProc()
	{

	}

	///慢时钟到达时执行的操作；
	void SlowTimerProc()
	{
		TRY_BEGIN;

#ifdef AI_SUP_CODE
		StateUpdate();
		BuffProcess();
#else  //#ifdef AI_SUP_CODE

		/*ACE_Time_Value curTime = ACE_OS::gettimeofday();
		//当没在做动作才执行状态更新
		if( curTime - m_lastActionStartTime >= m_lastActionNeedTime)
			m_pActionState->TimerProc( curTime );*/
#endif
		TRY_END;
	}

	void SetDir( unsigned short curDir )
	{
		m_wMonsterDir = curDir;
		m_monsterInfo.gcmonsterInfo.MonsterDir = curDir;
	}

	unsigned short GetDir()
	{
		return m_wMonsterDir;
	}

public:
	bool IsMonsterMoving()
	{
		return m_MonsterMoveInfo.IsMonsterMoving();
	}
	///取目标点浮点坐标X；
    float GetFloatTargetX()
	{
		return (float) ( 1.0*GetTargetX() + 0.5 );
	}

	///取目标点浮点坐标Y；
	float GetFloatTargetY()
	{
		return (float) ( 1.0*GetTargetY() + 0.5 );
	}

	///取怪物移动目标点X，如果怪物静止，则返回怪物当前点位置；
	unsigned short GetTargetX()
	{
		return ( m_MonsterMoveInfo.IsMonsterMoving() ? m_MonsterMoveInfo.GetTargetGridX() : GetPosX() );
	}

	///取怪物移动目标点Y，如果怪物静止，则返回怪物当前点位置；
	unsigned short GetTargetY()
	{
		return ( m_MonsterMoveInfo.IsMonsterMoving() ? m_MonsterMoveInfo.GetTargetGridY() : GetPosY() );
	}

	void SetSpeed( float inSpeed )
	{
		m_tempInfo.fSpeed = inSpeed;

#ifdef NEW_MONSTER_INFO
		m_monsterInfo.gcmonsterInfo.fSpeed = m_tempInfo.fSpeed;
		m_monsterMove.gcMonsterMove.fSpeed = m_tempInfo.fSpeed;
#endif //NEW_MONSTER_INFO
	}


	///取怪物移动速度，如果怪物静止，则返回0；
	float GetSpeed()
	{
		return m_tempInfo.fSpeed;
	}

	void SetMonsterMovInfo( unsigned short inTargetGridX, unsigned short inTargetGridY )
	{
		m_MonsterMoveInfo.SetMonsterMovInfo( inTargetGridX, inTargetGridY );
		m_monsterInfo.gcmonsterInfo.IsDirValid = false;
	}

	void ResetMonsterMovInfo()
	{
		m_MonsterMoveInfo.ResetMonsterMovInfo();
	}

private:
	MonsterMoveInfo m_MonsterMoveInfo;//怪物移动状态；

public:
	inline void SetAIFlag( void* aiFlag )
	{
		m_aiFlag = aiFlag;
	}
private:
	inline void* GetAIFlag() { return m_aiFlag; }

private:
	void* m_aiFlag;//怪物的AI标记，在怪物出生后的第一时间由AI设置，发生AI事件时，通知给AI；

public:

	///////////////////////////////////////////////////////////////////
	///取NPC相关的一些属性，by dzj, 09.03.27,代码规范，公有成员去除...
	inline const char* GetNpcName()
	{
		if ( NULL == m_pNPCProp )
		{
			D_ERROR( "GetNpcName，NPC属性空\n" );
			return "无效怪物名";
		}

		return m_pNPCProp->m_npcName;
	}

	inline int GetAttackInterval()
	{
		if ( NULL == m_pNPCProp )
		{
			D_ERROR( "GetAttackInterval，NPC属性空\n" );
			return 999999;
		}

		return m_pNPCProp->m_nAttackInterval;
	}

	inline float GetNpcResistFaint()
	{
		if ( NULL == m_pNPCProp )
		{
			D_ERROR( "GetNpcResistFaint，NPC属性空\n" );
			return 0;
		}

		return m_pNPCProp->m_fResistFaint;
	}

	inline float GetResistBondage()
	{
		if ( NULL == m_pNPCProp )
		{
			D_ERROR( "GetResistBondage，NPC属性空\n" );
			return 0;
		}

		return m_pNPCProp->m_fResistBondage;
	}
	///...取NPC相关的一些属性，by dzj, 09.03.27,代码规范，公有成员去除；
	///////////////////////////////////////////////////////////////////


	 //获取怪物等级 08.3.09 hyn
	 unsigned long GetLevel(void)
	 {
		 if( NULL == m_pNPCProp)
			 return 0;
		 return m_pNPCProp->m_usLevel;
	 }

	 //获取目标集
	 unsigned int GetTargetSet()
	 {
		if( NULL == m_pNPCProp )
			return 0;
		return m_pNPCProp->m_targetSet;
	 }

	
	 //获得怪物的经验值 03.20 hyn
	 unsigned long GetExp(void)
	 {
		if( NULL == m_pNPCProp)
			return 0;
		return m_pNPCProp->m_lExp;
	 }

	 unsigned short GetNpcType()
	 {
		if( NULL == m_pNPCProp )
			return 0;
		return m_pNPCProp->m_npcType;
	 }


	  //获取怪物的物理防御
	 int GetPhyDef( void )
	 {
		if( NULL == m_pNPCProp)
			return 0;
		return ( m_tempInfo.nPhyDef + m_buffAffect.nPhyDefend ) >= 0 ? m_tempInfo.nPhyDef + m_buffAffect.nPhyDefend : 0;
	 }


	 //获取怪物的魔法防御
	 int GetMagDef( void )
	 {
		if( NULL == m_pNPCProp )
			return 0;
		return ( m_tempInfo.nMagDef + m_buffAffect.nMagDefend ) >= 0 ? m_tempInfo.nMagDef + m_buffAffect.nMagDefend : 0;
	 }


	 unsigned short GetRank(void)
	 {
		if ( NULL == m_pNPCProp )
		{
			return 0; //默认返回0;
		}
		return m_pNPCProp->m_usMonsterRank;
	 }

	 //是否BOSS怪物
	 bool IsBossMonster()
	 {
		 if ( NULL == m_pNPCProp )
		 {
			 return false;
		 }
		 return (3 == m_pNPCProp->m_usMonsterRank);//3:BOSS;
	 }

	 //获取怪物的物理回避
	 float GetPhyDodge( void )
	 {
		if( NULL == m_pNPCProp)
			return 0;
		return m_tempInfo.fPhyDodge;
	 }

	 //获取怪物的魔法回避
	 float GetMagDodge( void)
	 {
		if( NULL == m_pNPCProp)
			return 0;
		return m_tempInfo.fMagDodge;
	 }

	 MONSTER_STAT GetState() {return m_curState;} //NEWFSM
	 void SetState( const MONSTER_STAT& inState) { m_curState = inState; } //NEWFSM
	 
#ifdef AI_SUP_CODE

public:	
	unsigned short GetAIType(void)
	{
		if( m_pNPCProp == NULL )
			return 0;
		return m_pNPCProp->m_usAIType;
	}


	unsigned long GetAIID(void)
	{
		if( m_pNPCProp == NULL )
			return 0;
		return m_pNPCProp->m_lAIID;
	}


	//获取视野范围
	int GetSight(void)
	{
		if( NULL == m_pNPCProp)
			return 0;
		return m_pNPCProp->m_usSightScope;
	}

	unsigned long GetMonsterDropSetID(void)
	{
		if ( NULL == m_pNPCProp )
			return 0;

		return m_pNPCProp->m_dropSetID;
	}

	void SetFollowing( CPlayer* pFollowingPlayer )
	{
		m_isFollowing = true;
		m_followingPlayer = pFollowingPlayer;
	}

	void ClearFollowing()
	{
		m_isFollowing = false;
		m_followingPlayer = NULL;
	}

	CPlayer* GetFollowingPlayer()
	{
		return m_followingPlayer;
	}

	bool IsFollowing()
	{
		return m_isFollowing;
	}

	bool IsDisableAI()
	{
		if( !m_pNPCProp )
			return false;

		return (!m_pNPCProp->IsCallAI());// 专门的IsCallAI属性来决定
	}

	//判断当前的状态是否能发生该事件
	bool IsResEvent(MONSTER_EVENT monsterEvent )
	{
		if( NULL == m_pIFsm)
		{
			D_ERROR("状态机指针为空\n");
			return false;
		}
		return m_pIFsm->IsResEvent( m_curState, monsterEvent);
	}

	bool IsCanBeAttacked()
	{
		return m_bIsCanBeAttack;
	}

	void SetCanBeAttacked( bool bAttackable )
	{
		m_bIsCanBeAttack = bAttackable;
	}

	bool IsCanBeAttackedPvc()
	{
		return m_bIsCanBeAttackPvc;
	}

	void SetCanBeAttackedPvc( bool bAttackable )
	{
		m_bIsCanBeAttackPvc = bAttackable;
	}

	static AIEventInfo* GetAIEventInfo()
	{
		return &m_AIEventInfo;
	}

	void SetRewardInfo( const PlayerID& rewardID )
	{
		m_rewardInfo.rewardPlayerID = rewardID;
		m_rewardInfo.isLocked = true;

#ifdef NEW_MONSTER_INFO
		m_monsterInfo.gcmonsterInfo.rewardID = m_rewardInfo.rewardPlayerID;
#endif //NEW_MONSTER_INFO
	}

	void ClearRewardInfo()
	{
		m_rewardInfo.ClearRewardInfo();
		//memset( &m_rewardInfo, 0, sizeof(m_rewardInfo) );
#ifdef NEW_MONSTER_INFO
		m_monsterInfo.gcmonsterInfo.rewardID.dwPID = m_monsterInfo.gcmonsterInfo.rewardID.wGID = 0;
#endif //NEW_MONSTER_INFO
	}

	const RewardInfo& GetRewardInfo(void)
	{
		return m_rewardInfo;
	}

	unsigned short GetRaceAttribute()
	{
		if( m_pNPCProp )
			return m_pNPCProp->m_usRaceAttribute;
		return 0;
	}

	unsigned long GetInherentStand()
	{
		if( m_pNPCProp )
			return m_pNPCProp->m_inherentStand;
		return 0;
	}

	//根据状态的更新  
	void StateUpdate();
	//状态机触发事件dected事件
	void OnPlayerDetected(CPlayer* pDetectedPlayer);
	//状态机触发Leave事件
	void OnPlayerLeaveView( CPlayer* pLeavedPlayer, AILeaveReason leaveReason );
	//状态机触发beAttacked事件
	void OnBeAttackedByPlayer(CPlayer* pPlayer, int nDamage, bool bCreatedByDebuff, int reactTime );
	void OnBeAttackedByNpc( CMonster* pMonster, int nDamage, bool bCreatedByDebuff, int reactTime );


	//状态机触发玩家死亡事件
	void OnPlayerDie( CPlayer* pDiePlayer );

	void OnBeAttacckByNpc( CMonster* pAtkMonster, unsigned int damage );

	//触发目标离开视野事件
	void OnAttackTargetLeave();

	//怪物死亡
	void OnMonsterDie( CPlayer* pKiller/*杀NPC者*/, GCMonsterDisappear::E_DIS_TYPE disType=GCMonsterDisappear::M_DIE, bool isDropItem = true );

private:
	inline void SetMonsterDieFlag() { m_isMonsterDie = true; }
	inline void ClearMonsterDieFlag() { m_isMonsterDie = false; }
public:
	inline bool IsMonsterDie() { return m_isMonsterDie; }

private:
	bool m_isMonsterDie;

public:
	//触发BORN 事件
	void OnSelfBorn();
	//触发DIE 事件
	void OnSelfDie( GCMonsterDisappear::E_DIS_TYPE disType );
	//触发跟随玩家被攻击
	void OnFollowPlayerBeAttack( const PlayerID& attackID, int damage );
	///怪物进入视野，收到此事件后，通过GetAIEventInfo取得具体的进入视野怪物信息；
	bool OnMonsterEnterView( CMonster* pEnterViewMonster );
	///怪物离开视野，收到此事件后，通过GetAIEventInfo取得具体的离开视野怪物信息；
	bool OnMonsterLeaveView( CMonster* pLeaveViewMonster );

	///有人与怪物对话；
	bool OnMonsterBeChat();
	///怪物周围某玩家移动状态改变，通过GetAIEventInfo取得具体玩家的ID号；
	bool OnSurPlayerMovChg( CPlayer* pMovChgPlayer );

	///本怪物召唤的怪物出生，通过GetAIEventInfo取新出生怪物ID号；
	bool OnCalledMonsterBorn( unsigned long calledMonsterID, unsigned int aiFlag );

	//获取初始的怪物HP
	unsigned int GetInitHp();
	//怪物回归开始,补血，给客户端发消息
	bool ReturnStarted();
	//怪物回归结束
	void ReturnEnded();
	//怪物被放debuff();
	void OnDebuff( CPlayer* pPlayer, int nDamage);
	//怪物晕旋开始
	void OnFaintStart();
	//怪物晕旋结束
	void OnFaintEnd();
	//怪物束缚开始
	void OnBondageStart();
	//怪物束缚结束
	void OnBondageEnd();

	void OnMonsterBuffStart( BUFF_TYPE buffType, int buffVal );

	void OnMonsterBuffEnd( BUFF_TYPE buffType, int buffVal );

	///怪物开始跟随；
	void OnStartFollowing( unsigned int taskID ); 

	//怪物受到自爆伤害
	void OnBeExplosion( CPlayer *pExplosionPlayer, CMuxSkillProp* pSkillProp, unsigned int exArg );

	///通知跟随怪物，其所跟随的玩家开始移动；
	void OnFollowPlayerStartMoving();
	//周边人物加血
	void OnPlayerAddHp( const PlayerID& addStarter, const PlayerID& addTarget, unsigned int addVal );

	//怪物调试威胁列表开始
	void OnDebugThreatListStart();
	//调试怪物威胁列表结束
	void OnDebugThreatListEnd();
	////怪物移动停止
	//void Stop();
	//当玩家丢弃任务时
	void OnPlayerDropTask( unsigned int taskid );

	//怪物被击退到新位置(单位0.1格)
	void OnBeKickBack( TYPE_POS posX10, TYPE_POS posY10 );

	///玩家与NPC对话，nOption:玩家所选择的选项，==0表示初始选择NPC；
	bool OnChatOption( CPlayer* pOwner, int nOption )
	{
		if ( NULL == m_pChatScript )
		{
			//该NPC没有对话脚本；
			return false;
		}
		return m_pChatScript->OnNpcChat( pOwner, nOption );
	}


	//设置怪物是否能被攻击
	void SetAttackFlag(bool bCanBeAttacked);

	unsigned int GetMaxHp()
	{
		if( m_pNPCProp )
			return m_pNPCProp->m_uiMaxHP;
		return 0;
	}

private:
	static AIEventInfo  m_AIEventInfo;//每次调用IFsm中AI有关的事件接口时，在本信息结构中保存调用相关的信息，以备AI扩展接口查询；
	IMonsterFsm* m_pIFsm;//本monster使用的AI状态机接口指针； 

#endif //AI_SUP_CODE

public:
	/////////////////////////////////////BUFF/////////////////////////////////
	bool CreateMuxBuff( MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID );
	bool CreateMuxBuffByID( unsigned int buffID, unsigned int buffTime, const PlayerID& createID );


	//新buff
	void AddMuxBuffEffect( MuxBuffProp* pBuffProp );
	void AddMuxSingleBuffEffect( MuxBuffProp* pBuffProp );
	void DelMuxBuffEffect( MuxBuffProp* pBuffProp, unsigned int buffLevel );
	void DelMuxSingleBuffEffect( MuxBuffProp* pBuffProp, unsigned int buffLevel );

	bool IsDamageRebound()
	{
		return m_buffAffect.isDamageRebound;
	}

	MonsterBuffAffect* GetMonsterBuffAffect()
	{
		return &m_buffAffect;
	}
	
	//关于晕眩和束缚状态的查询
	bool IsFaint()
	{
		return m_bFaint;
	}

	void SetFaintFlag(bool bFlag)
	{
		m_bFaint = bFlag;
	}

	bool IsBondage()
	{
		return m_bBondage;
	}

	void SetBondageFlag(bool bFlag)
	{
		m_bBondage = bFlag;
	}

	//伤害反弹
	void SetDamageReboundInfo( bool bFlag, unsigned int buffID )
	{
		m_buffAffect.isDamageRebound = bFlag;
		m_buffAffect.DrArg = buffID;
	}

	unsigned int GetDrArg()
	{
		return m_buffAffect.DrArg;
	}

	unsigned int GetAbsorbDamage()
	{
		return m_buffAffect.nAbsorbDamage;
	}

	int GetBuffAoeRange()
	{
		return m_buffAffect.nAddAoeRange;
	}

	int GetBuffPhyCriDamAdd()
	{
		return m_buffAffect.nPhyCriDamAddition;
	}

	int GetBuffMagCriDamAdd()
	{
		return m_buffAffect.nMagCriDmgAddition;
	}

	float GetAddPhyDamageRate()
	{
		return m_buffAffect.nAddPhyDamageRate;
	}

	float GetAddMagDamageRate()
	{
		return m_buffAffect.nAddMagDamageRate;
	}

	float GetAddPhyCriDmgRate()
	{
		return m_buffAffect.fAddPhyCriDmgRate;
	}

	float GetAddMagCriDmgRate()
	{
		return m_buffAffect.fAddMagCriDmgRate;
	}

	float GetPhyJoukRate()
	{
		return m_buffAffect.fPhyJoukRate;
	}

	float GetMagJoukRate()
	{
		return m_buffAffect.fMagJoukRate;
	}

	//是否变羊
	bool IsSheep()
	{
		return m_buffAffect.isSheep;
	}

	//是否睡眠
	bool IsInSleep()
	{
		return m_buffAffect.isSleep;
	}

	int GetMonsterSize()
	{
		if( !m_pNPCProp )
			return 0;

		return m_pNPCProp->m_npcSize;
	}

	void AddBuffNum()
	{
		m_buffNum++;
	}

	void SubBuffNum()
	{
		m_buffNum--;
	}
	
	bool IsHaveBuff()
	{
		return (m_buffNum != 0);
	}

	MGMonsterInfo* GetMonsterInfo() 
	{
		m_monsterInfo.gcmonsterInfo.fTargetX = GetFloatTargetX();
		m_monsterInfo.gcmonsterInfo.fTargetY = GetFloatTargetY();
		return &m_monsterInfo;
	}

	MGMonsterMove* GetMonsterMove()
	{
		m_monsterMove.gcMonsterMove.nNewMapPosX = GetTargetX();
		m_monsterMove.gcMonsterMove.nNewMapPosY = GetTargetY();
		return &m_monsterMove;
	}

private:
	////////////////////////////////////////////////////////////////////////////////////////////////////
	//本对象的生命管理相关；
	bool m_bIsToDel;//本对象的删除标记；
	//vector< IRefPtrUser<CMonster>* > m_vecSelfUsers;//保存了本对象引用的所有其它对象；
	//本对象的生命管理相关；
	////////////////////////////////////////////////////////////////////////////////////////////////////

	//MonsterNearbyPlayer m_PlayerNearby;//附近玩家；
	bool m_bFaint;		//是否晕眩
	bool m_bBondage;	//是否束缚

	MuxPoint     m_BornPt;
	unsigned long m_dwMonsterClsID;  /*怪物的种类ID*/
	CCreatorIns* m_pCreator;   /*怪物生成器，包含怪物出生的属性*/
	bool m_bEnableAttack;   //是否能被攻击的标志量
	MONSTER_STAT m_curState;//怪物当前所处状态； //NEWFSM
	RewardInfo m_rewardInfo;	//怪物死亡能拾取的限制
	MuxPoint m_TargetPoint;	   //怪物移动的目标位置
		
	///怪物私有属性
	CNewMap*  m_pMap;	//所在地图的指针
	bool      m_isFollowing;//本NPC是否为跟随NPC；
	CPlayer*  m_followingPlayer;//本NPC跟随的玩家；
	CNPCProp* m_pNPCProp;	//NPC的公用属性
	CNormalScript* m_pChatScript;//该NPC对应的聊天脚本；
	CNormalScript* m_pDeathScript;//死亡事件调用脚本，只有BOSS级怪物才会有；
	int m_buffNum;	//BUFF的数量

	static unsigned int s_lMonsterIDAllocer;//全局怪物ID号分配；
	///怪物ID，地图内唯一；
	unsigned int m_lMonsterID;//怪物ID，地图内唯一；
	///怪物HP；
	unsigned long m_lMonsterHp;//怪物HP；

	unsigned short m_wMonsterDir;//怪物当前面对方向；

	NPCTmpInfo m_tempInfo; //怪物的战斗属性
	MonsterBuffAffect m_buffAffect;	  //怪物受BUFF影响的数值
	bool m_bIsCanBeAttack;			//是否能被攻击
	bool m_bIsCanBeAttackPvc;		//是否能被玩家攻击
	

	TYPE_POS m_nPosX;
	TYPE_POS m_nPosY;
	CPlayer* m_pTargetPlayer;  //怪物相联系人的目标

	MGMonsterInfo m_monsterInfo;
	MGMonsterMove m_monsterMove;
	bool	m_bIsToAllDel;		//是否处于批量删除状态，防止批量删除怪物时后生成的怪物也被一并删除
public:
	void SetDropItemFlag( bool bFlag )
	{
		m_bDropItem = bFlag;
	}

	bool IsDropItem()
	{
		return m_bDropItem;
	}

private:
	bool m_bDropItem;			//是否掉落道具

#ifdef ITEM_NPC
	private:
		bool m_isItemNpc;
		unsigned int m_itemNpcSeq;

	public:
		bool IsItemNpc()
		{
			return m_isItemNpc;
		}

		unsigned int GetItemNpcSeq()
		{
			return m_itemNpcSeq;
		}

		void SetItemNpcFlag( bool isItemNpc )
		{
			m_isItemNpc = isItemNpc;
		}

		void SetItemNpcSeq( unsigned int itemNpcSeq )
		{
			m_itemNpcSeq = itemNpcSeq;
		}
#endif //ITEM_NPC

public:
	bool NeedNotiOtherMonster()
	{
		return m_bNotiOtherMonster;
	}

private:
	bool m_bNotiOtherMonster;	//是否向该怪物通知其他怪物的出现消失
	CMonsterBuffs m_buffManager;

public:
	unsigned int GetIsAggressive()
	{
		if( NULL == m_pNPCProp )
			return 0;
		return m_pNPCProp->m_nIsAggressive;
	}

	void BuffProcess();

	const PlayerInfo_5_Buffers& GetBuffData()
	{
		return m_buffManager.GetBuffData();
	}

	bool RelieveBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve/*是否只是探测可能性*/ );

private:
	PlayerInfo_5_Buffers m_buffData;

private:
	bool m_bIsBeAttacked;		//是否被攻击过

public:
	void NotifyDamageRebound( unsigned int skillID, unsigned int reboundDamage, EBattleFlag btFlag );

private:
	//set<unsigned int> notifiedAIMonster;//调试AI通知匹配;
};

///保存唯一怪物,通过构造赋初值，通过GetUniqueMonster()得到真正玩家，一旦玩家被回收，GetUniqueMonster()将返回空；
#define UniqueMonster UniqueObj< CMonster > ;

/**
* 全局NPC实例管理器;
* 同时使用vector和map管理NPC实例
* vector主要用于对全局NPC实例进行遍历
* map主要用于对全局NPC实例进行查找
* 鉴于删除NPC操作需要遍历vector，因此一般情况下游戏进行中不进行NPC的删除操作，
*     若NPC死亡，则将NPC置一个无效状态，且随后新刷的NPC将重用之前已死亡的NPC实例指针(作必要初始化以及属性重置)
*/
class CManMonster
{
public:
	///初始化管理器;
	static void Init()
	{
		m_LastEPos = 0;
		m_vecMonsters.clear();
		m_mapMonsters.clear();
	    m_poolMonsters = NEW TCache< CMonster >( 30000 );//monster对象池，必需分配足够，以便在monster身上保存一个任何时候都可以访问的唯一ID号；
	}

	///释放管理器管理的一个NPC;
	static void ReleaseManedMonster( CMonster* pMonster )
	{
		TRY_BEGIN;

		if ( NULL == pMonster )
		{
			return;
		}

		if ( pMonster->IsToDel() )
		{
			D_ERROR( "CManMonster::ReleaseManedMonster, 重复删除怪物%x\n", (unsigned int)pMonster );
			return;
		}
		pMonster->SetDelFlag();//置删除标记；

		TRY_END;
	}

	///释放管理器管理的所有NPC;
	static void ReleaseAllManedMonster()
	{
		TRY_BEGIN;

		CMonster* pMonster = NULL;
		for ( vector<CMonster*>::iterator tmpiter = m_vecMonsters.begin();
			tmpiter != m_vecMonsters.end(); ++tmpiter )
		{
			pMonster = *tmpiter;
			if ( NULL != pMonster )
			{
				if ( NULL != m_poolMonsters )
				{
					pMonster->PoolObjInit();//递增唯一计数；
					m_poolMonsters->Release( pMonster );
				} else {
					D_ERROR( "ReleaseAllManedMonster m_poolMonsters空!\n" );
				}
			}
		}
		m_vecMonsters.clear();
		m_mapMonsters.clear();

		delete m_poolMonsters; m_poolMonsters = NULL;

		TRY_END;
	}


public:
	///向管理器中添加一个新的指定类型NPC；
	static bool AddNewMonster( unsigned long dwMonsterClsID, CMonster*& pOutMonster )
	{
		TRY_BEGIN;

		if ( NULL == CManNPCProp::FindObj( dwMonsterClsID ) )
		{
			D_WARNING( "找不到怪物类型%d的对应属性\n", dwMonsterClsID );
			pOutMonster = NULL;
			return false;
		}

		CMonster* pNewMonster = m_poolMonsters->Retrieve();//只从池中分配而不新NEW，以便任何时候都可以访问pMonster的唯一ID号;
		if ( NULL == pNewMonster )
		{
			D_ERROR( "CManMonster::AddNewMonster，从m_poolMonster中分配CMonster实例失败\n" );
			pOutMonster = NULL;
			return false;
		}
		if ( !(pNewMonster->InitMonster( dwMonsterClsID )) )
		{
			D_WARNING( "怪物类型%d属性初始化失败\n", dwMonsterClsID );
			m_poolMonsters->Release( pNewMonster );
			return false;
		}
		m_vecMonsters.push_back( pNewMonster );

		m_mapMonsters.insert( pair<unsigned long, CMonster*>( pNewMonster->GetMonsterID(), pNewMonster ) );
		pOutMonster = pNewMonster;

		return true;
		TRY_END;
		return false;
	}


	static void DelTypeMonster( unsigned int npcTypeId, unsigned int mapID )
	{
		CMonster* pMonster = NULL;
		for( size_t i = 0; i<m_vecMonsters.size(); i++ )
		{
			pMonster = m_vecMonsters[i];
			if( NULL == pMonster )
				continue;
			if( pMonster->GetClsID() == npcTypeId && pMonster->GetMapIDOfMonster() == mapID )
			{
				pMonster->OnMonsterDie( NULL, GCMonsterDisappear::M_VANISH, false );			//没人杀死,不掉包
			}
		}
	}

	static CMonster* FindMonster( unsigned long lMonsterID)
	{
		CMonster* pMonster = NULL;
		map<unsigned long, CMonster*>::iterator iter = m_mapMonsters.find( lMonsterID );
		if( iter != m_mapMonsters.end() )
		{
			pMonster = iter->second;
			if ( NULL != pMonster )
			{
				if ( !( pMonster->IsToDel() ) )
				{
					return pMonster;
				}
			}
		}
		return NULL;
	}

	static void TimerDelMonster( CMonster* pMonsterToDel );

	///NPC慢时钟，遍历各NPC，执行各自的慢时钟操作；
	static void MonsterSlowTimer()
	{
		TRY_BEGIN;

		//刷NPC也应放在此处；
		CMonster* pMonster = NULL;

	    //DWORD st = GetTickCount();

		if ( m_vecMonsters.empty() )
		{
			//没有怪物；
			return;
		}
		if ( m_LastEPos >= (int)(m_vecMonsters.size()) )
		{
			m_LastEPos = 0;
		}
#ifdef AUTO_EXP_UNIT
		static ACE_Time_Value lastBigLoopTime = ACE_OS::gettimeofday();
		static int            smallLoopNum = 1;//各小循环的每次遍历数，自适应调整，以小值开始启动；
		static int            bigLoopExped = 0;//大循环中已逝去小循环的已遍历数；
		int passedmsec = (ACE_OS::gettimeofday() - lastBigLoopTime).msec();//大循环已逝去毫秒数；
		int numpere = smallLoopNum;//本次遍历数，将在AutoExpNumCal中被修改；
		bool isUpdateLastBigLoopTime = false;//是否更新大循环起始时刻；
		AutoExpNumCal( passedmsec, (const int)m_vecMonsters.size(), smallLoopNum, bigLoopExped, numpere, isUpdateLastBigLoopTime );		
		//static int            smallLoopTimes = 0;
		//++ smallLoopTimes;
		if ( isUpdateLastBigLoopTime )
		{
//			D_DEBUG( "container_size:%d, smallLoopTimes:%d, smallLoopNum:%d, numpere:%d\n", m_vecMonsters.size(), smallLoopTimes, smallLoopNum, numpere );
			lastBigLoopTime = ACE_OS::gettimeofday();
//			smallLoopTimes = 0;
		}
#else //AUTO_EXP_UNIT
		int numpere = (int)(m_vecMonsters.size()) / EXPLORE_UNIT_PROP + 1;//每次遍历怪物总数的1/4;
#endif //AUTO_EXP_UNIT
		int numcure = 0;//当前遍历的怪物数；

		vector<CMonster*>::iterator tmpiter = m_vecMonsters.begin();
		advance( tmpiter, m_LastEPos );
		if ( tmpiter == m_vecMonsters.end() )
		{
			tmpiter = m_vecMonsters.begin();
			m_LastEPos = 0;
		}

		for ( ; tmpiter != m_vecMonsters.end(); )
		{
			if ( numcure >= numpere )
			{
				break;//已遍历超过1/4
			}

			++numcure;//本次已遍历的怪物数；
			++m_LastEPos;//最后遍历的元素位置；
			pMonster = *tmpiter;
			if ( NULL == pMonster )
			{
				tmpiter = m_vecMonsters.erase( tmpiter );
				--m_LastEPos;
				if ( m_LastEPos <= 0 )
				{
					m_LastEPos = 0;
				}
				continue;
			}

			if ( !(pMonster->IsToDel()) )//遍历过程中防止迭代器变化
			{
				pMonster->SlowTimerProc();		
				++tmpiter;
			} else {

				TimerDelMonster( pMonster );
				tmpiter = m_vecMonsters.erase( tmpiter );
				--m_LastEPos;
				if ( m_LastEPos <= 0 )
				{
					m_LastEPos = 0;
				}

			}
		}

		return;
		TRY_END;
		return;
	}

private:
	CManMonster(){}   //constructor
	~CManMonster(){}  //destructor

private:
	///当前服务器上所有的NPC，用于遍历NPC；
	static vector<CMonster*> m_vecMonsters;
	///当前服务器上所有的NPC，用于快速寻找NPC；
	static map<unsigned long, CMonster*> m_mapMonsters;

	static TCache<CMonster>* m_poolMonsters;

	static int m_LastEPos;//上次遍历的结束位置，每次遍历怪物总数的1/4;
};
