﻿/**
* @file monsterwrapper.cpp
* @brief 定义怪物AI用扩展接口
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: monsterwrapper.cpp
* 摘    要: 定义怪物AI用扩展接口
* 作    者: dzj
* 完成日期: 2007.04.01
*
*/

#include "monster.h"
#include "monsterwrapper.h"
#include "../NpcSkill.h"
#include "../Player/Player.h"
#include "../MuxMap/MapCommonHead.h"
#include "../MuxMap/mannormalmap.h"
#include "../MuxMap/manmapproperty.h"
#include "../MuxMap/manmuxcopy.h"
#include "../MuxSkillConfig.h"


#ifdef AI_SUP_CODE 

ScopeMonsterInfo CMonsterWrapper::m_scopeMonsters[MAX_SCOPE_MONSTER];

ScopePlayerInfo CMonsterWrapper::m_scopePlayers[MAX_SCOPE_MONSTER];	//用于返回范围BLOCK中的玩家

/////注册怪物接收玩家事件；
//int CMonsterWrapper::RegisterMonster( CMonster* pMonster, CPlayer* pPlayer )
//{
//	TRY_BEGIN;
//
//	if ( ( NULL == pMonster ) || ( NULL == pPlayer ) )
//	{
//		D_WARNING( "CMonsterWrapper::RegisterMonster, pointer == NULL\n" );
//		return 0;
//	}
//
//	return pPlayer->RegisterMonster( pMonster );
//	TRY_END;
//	return -1;
//}
//
/////反注册怪物接收玩家事件；
//int CMonsterWrapper::UnRegisterMonster( CMonster* pMonster, CPlayer* pPlayer )
//{
//	TRY_BEGIN;
//
//	if ( ( NULL == pMonster ) || ( NULL == pPlayer ) )
//	{
//		D_WARNING( "CMonsterWrapper::UnRegisterMonster, pointer == NULL\n" );
//		return 0;
//	}
//
//	return pPlayer->UnRegisterMonster( pMonster );
//	TRY_END;
//	return -1;
//}

///根据AIMapInfo寻找相应地图对象指针；
CMuxMap* CMonsterWrapper::FindMapByAiMapInfo( const AIMapInfo& mapInfo )
{
	CMuxMap* pMap = NULL;
	if ( mapInfo.isNormalOrCopy )
	{
		pMap = CManNormalMap::FindMap( mapInfo.mapID );
		if ( NULL == pMap )
		{
			D_ERROR( "FindMapByAiMapInfo，找不到指定普通地图(%d,%d)\n", (mapInfo.isNormalOrCopy)?1:0, mapInfo.mapID );
			return NULL;
		}
	} else {
		if ( ! CManMuxCopy::FindCopyByCopyID( mapInfo.mapID, pMap ) )
		{
			D_ERROR( "FindMapByAiMapInfo，找不到指定副本地图(%d,%d)\n", (mapInfo.isNormalOrCopy)?1:0, mapInfo.mapID );
			return NULL;
		}
	}

	return pMap;
}

///根据ID号找怪物指针，找不到返回NULL
CMonster* CMonsterWrapper::GetMonsterPtrByID( const AIMapInfo& mapInfo, unsigned long monsterID )
{
	CMuxMap* pMap = FindMapByAiMapInfo( mapInfo );

	if ( NULL == pMap )
	{
		//肯定是普通地图找不到；
		D_ERROR( "GetMonsterPtrByID，找不到地图(%d,%d)\n", (mapInfo.isNormalOrCopy)?1:0, mapInfo.mapID );
		return NULL;
	}

	return pMap->GetMonsterPtrInMap( monsterID );
}

///根据ID号找玩家指针，找不到返回NULL；	
CPlayer* CMonsterWrapper::GetPlayerPtrByID( const PlayerID& playerID )
{
	CGateSrv* pGateSrv = CManG_MProc::FindProc( playerID.wGID );
	if ( NULL == pGateSrv )
	{
		return NULL;
	}

	return pGateSrv->FindPlayer( playerID.dwPID );
}

///怪物向周围人说话；
int CMonsterWrapper::MonsterSpeak( const AIMapInfo& mapInfo, unsigned long monsterID, const char* pMsg, CHAT_TYPE chatType )
{
	TRY_BEGIN;

	//return 0;//因为傅晓强暂时没时间修改，现暂时屏蔽傅晓强的调试语句，by dzj, 08.07,???,邓子建检查;

    CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{ 
		D_WARNING( "CMonsterWrapper::MonsterSpeak, pMonster == NULL\n" );
		return 0;	
	}

	return pMonster->ChatSurrounding( pMsg, chatType );	
	TRY_END;
	return -1;
}

///设置怪物的AI标记；
int CMonsterWrapper::SetMonsterAIFlag( const AIMapInfo& mapInfo, unsigned long monsterID, void* aiFlag )
{
	TRY_BEGIN;

	if ( NULL == aiFlag )
	{
		D_ERROR( "CMonsterWrapper::SetMonsterAIFlag, NULL == aiFlag\n" );//但仍继续执行；
	}

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{	
		D_WARNING( "CMonsterWrapper::SetMonsterAIFlag, pMonster == NULL\n" );
		return 0; 
	}

	pMonster->SetAIFlag( aiFlag );
	return 0;
	TRY_END;
	return -1;
}

int CMonsterWrapper::GetMonsterAIType( const AIMapInfo& mapInfo, unsigned long monsterID )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{	
		D_WARNING( "CMonsterWrapper::GetMonsterAIType, pMonster == NULL\n" );
		return 0; 
	}

	return pMonster->GetAIType();
	TRY_END;
	return -1;
}

unsigned long CMonsterWrapper::GetMonsterAIID( const AIMapInfo& mapInfo, unsigned long monsterID )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{ 
		D_WARNING( "CMonsterWrapper::GetMonsterAIID, pMonster == NULL\n" );
		return 0; 
	}

	return pMonster->GetAIID();
	TRY_END;
	return 0xffffffff;
}


int CMonsterWrapper::TransState( const AIMapInfo& mapInfo, unsigned long monsterID, MONSTER_STAT newState )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{ 
		D_WARNING( "CMonsterWrapper::TransState, pTempMap == NULL\n" );
		return 0;
	}
	pMonster->SetState( newState );

	return 1;
	TRY_END;
	return -1;
}

//怪物对其他怪物释放技能
int CMonsterWrapper::UseSkill( const AIMapInfo& mapInfo, unsigned long monsterID, unsigned long tgtMonsterID, int skillID )
{
	TRY_BEGIN;

	PlayerID targetID;
	targetID.dwPID = tgtMonsterID;
	targetID.wGID = MONSTER_GID;
	return CMonsterWrapper::UseSkill( mapInfo, monsterID, targetID, skillID );

	TRY_END;
	return -1;
}

int CMonsterWrapper::UseSkill( const AIMapInfo& mapInfo, unsigned long monsterID, const PlayerID& tgtPlayerID, int skillID )
{	
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster)
	{
		D_WARNING( "CMonsterWrapper::UseSkill, pMonster == NULL\n" );
		return 0; 
	}

	CMuxMap* pTempMap = pMonster->GetMap();
	if( NULL == pTempMap )
	{
		D_WARNING( "CMonsterWrapper::UseSkill, pTempMap == NULL\n" );
		return 0; 
	}

	return pTempMap->OnMonsterUseSkill( pMonster, skillID, tgtPlayerID );

	
	TRY_END;
	return -1;
}


int CMonsterWrapper::GetMonsterSkillProp( int skillID, CMuxSkillProp*& pSkillProp ) 
{
	TRY_BEGIN;

	pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( skillID );
	if( NULL == pSkillProp )
	{	
		D_WARNING( "CMonsterWrapper::GetMonsterSkillProp, can not find skill:%d\n", skillID );
		return 0;
	}

	return 1;
	TRY_END;
	return -1;
}


int CMonsterWrapper::GetMonsterHp( const AIMapInfo& mapInfo, unsigned long monsterID )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{	
		D_WARNING( "CMonsterWrapper::GetMonsterHp, pMonster == NULL\n" );
		return 0; 
	}

	return pMonster->GetHp();
	TRY_END;
	return -1;
}

int CMonsterWrapper::GetState( const AIMapInfo& mapInfo, unsigned long monsterID, MONSTER_STAT& state )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{	
		D_WARNING( "CMonsterWrapper::GetState, pMonster == NULL\n" );
		return 0; 
	}
	state = pMonster->GetState();

	return 1;
	TRY_END;
	return -1;
}

int CMonsterWrapper::GetAIEventInfo( AIEventInfo*& pEventInfo )
{
	TRY_BEGIN;

	pEventInfo = CMonster::GetAIEventInfo();

	return 1;
	TRY_END;
	return -1;
}

int CMonsterWrapper::IsInAttackRange( const AIMapInfo& mapInfo, unsigned long monsterID, const PlayerID& playerID, int dist, bool& res )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster ) 
	{	
		D_WARNING( "CMonsterWrapper::IsInAttackRange, pMonster == NULL\n" );
		return 0; 
	}

	TYPE_POS monsterPosX =pMonster->GetPosX(), monsterPosY = pMonster->GetPosY();
	TYPE_POS targetPosX = 0, targetPosY = 0;

	if( playerID.wGID != MONSTER_GID )
	{
		CPlayer* pPlayer = GetPlayerPtrByID( playerID );
		if ( NULL == pPlayer )
		{
			D_WARNING( "CMonsterWrapper::IsInAttackRange, 没有指定玩家:%d:%d\n", playerID.wGID, playerID.dwPID );
			return 0;
		}

		pPlayer->GetPos( targetPosX, targetPosY );
	}else
	{
		CMonster* pMonster = GetMonsterPtrByID( mapInfo, playerID.dwPID );
		if( NULL == pMonster )
		{
			D_WARNING( "CMonsterWrapper::IsInAttackRange, 没有指定怪物:%d:%d\n", playerID.wGID, playerID.dwPID );
			return 0;
		}
		pMonster->GetPos( targetPosX, targetPosY );
	}


	res = IsInRange( monsterPosX, monsterPosY, targetPosX, targetPosY, dist );

	return 1;
	TRY_END;
	return -1;
}

//
//int CMonsterWrapper::GetPlayerID( CPlayer* pPlayer, unsigned long& lPlayerID )
//{
//	TRY_BEGIN;
//
//	if( NULL == pPlayer) 
//	{ 
//		D_WARNING( "CMonsterWrapper::GetPlayerID, pPlayer == NULL\n" );
//		return 0;
//	}
//	lPlayerID = pPlayer->GetPID();
//
//	return 1;
//	TRY_END;
//	return -1;
//}


int CMonsterWrapper::GetMonsterPosition( const AIMapInfo& mapInfo, unsigned long monsterID, TYPE_POS& x, TYPE_POS& y )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster) 
	{ 
		D_WARNING( "CMonsterWrapper::GetMonsterPosition, pMonster == NULL\n" );
		return 0;
	}
	pMonster->GetPos( x, y);

	return 1;
	TRY_END;
	return -1;
}


int CMonsterWrapper::GetPlayerPosition( const PlayerID& playerID, TYPE_POS& x, TYPE_POS& y )
{
	TRY_BEGIN;

	CPlayer* pPlayer = GetPlayerPtrByID( playerID );
	if ( NULL == pPlayer )
	{
		D_WARNING( "CMonsterWrapper::GetPlayerPosition, 找不到玩家：%d:%d\n", playerID.wGID, playerID.dwPID );
		return 0;
	}

	pPlayer->GetPos( x, y);

	return 1;
	TRY_END;
	return -1;
}

//int CMonsterWrapper::MonsterMove( unsigned long mapID, unsigned long monsterID, TYPE_POS x, TYPE_POS y, TYPE_POS nTargetX, TYPE_POS nTargetY)
//{
//	D_ERROR( "怪物AI调用已废弃移动接口，改用start,beyond, stop\n" );
//	return 0;
//	//if( NULL == pMonster)
//	//{
//	//	D_WARNING( "CMonsterWrapper::MonsterMove, pMonster == NULL\n" );
//	//	return 0; 
//	//}
//
//	////D_WARNING( "CMonsterWrapper::MonsterMove, called by AI, monster:%x\n", pMonster->GetMonsterID() );
//
//	//CNewMap* pMap = pMonster->GetMap();
//	//if( NULL == pMap ) 
//	//{ 
//	//	D_WARNING( "CMonsterWrapper::MonsterMove, pMap == NULL\n" );
//	//	return 0;
//	//}
//	//if( !pMap->OnMonsterMove( pMonster, x, y, nTargetX, nTargetY) )
//	//{
//	//	return 0;
//	//}
//	//return 1;
//}

//怪物开始移动
int CMonsterWrapper::MonsterStartMove( const AIMapInfo& mapInfo, unsigned long monsterID, TYPE_POS gridCurX, TYPE_POS gridCurY, TYPE_POS gridTargetX, TYPE_POS gridTargetY, float fSpeed  ) 
{ 
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{
		D_WARNING( "CMonsterWrapper::MonsterStartMove, pMonster == NULL\n" );
		return 0; 
	}

	CNewMap* pMap = pMonster->GetMap();
	if( NULL == pMap ) 
	{ 
		D_WARNING( "CMonsterWrapper::MonsterStartMove, pMap == NULL\n" );
		return 0;
	}
	if( !pMap->OnMonsterStartMove( pMonster, gridCurX, gridCurY, gridTargetX, gridTargetY, fSpeed) )
	{
		return 0;
	}

	return 1;
	TRY_END;
	return -1;
};

//怪物移动中跨格(以MonsterStartMove为前提)
int CMonsterWrapper::MonsterBeyondGrid( const AIMapInfo& mapInfo, unsigned long monsterID, TYPE_POS gridOrgX, TYPE_POS gridOrgY, TYPE_POS gridNewX, TYPE_POS gridNewY ) 
{ 
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{
		D_WARNING( "CMonsterWrapper::MonsterBeyondGrid, pMonster == NULL\n" );
		return 0; 
	}

	CNewMap* pMap = pMonster->GetMap();
	if( NULL == pMap ) 
	{ 
		D_WARNING( "CMonsterWrapper::MonsterBeyondGrid, pMap == NULL\n" );
		return 0;
	}

	//D_DEBUG("CMonsterWrapper::MonsterBeyondGrid 传入%d %d %d %d\n",gridOrgX, gridOrgY, gridNewX, gridNewY);
	/*if( gridNewX == 0 )
	{
		D_WARNING( "CMonsterWrapper::MonsterBeyondGrid, pMap == NULL\n" );
	}*/
	if( !pMap->OnMonsterBeyondGrid( pMonster, gridOrgX, gridOrgY, gridNewX, gridNewY, false/*正常移动，需要通知新视野块*/) )
	{
		return 0;
	}

	return 1;
	TRY_END;
	return -1;
};

//怪物停止移动(以MonsterStartMove为前提)
int CMonsterWrapper::MonsterStopMove( const AIMapInfo& mapInfo, unsigned long monsterID ) 
{ 
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{
		D_WARNING( "CMonsterWrapper::MonsterStopMove, pMonster == NULL\n" );
		return 0; 
	}

	CNewMap* pMap = pMonster->GetMap();
	if( NULL == pMap ) 
	{ 
		D_WARNING( "CMonsterWrapper::MonsterStopMove, pMap == NULL\n" );
		return 0;
	}
	if( !pMap->OnMonsterStopMove( pMonster ) )
	{
		return 0;
	}

	return 1;
	TRY_END;
	return -1;
};

int CMonsterWrapper::IsBlockedPoint( const AIMapInfo& mapInfo, unsigned long monsterID, TYPE_POS x, TYPE_POS y, bool& isBlocked)
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster) 
	{ 
		D_WARNING( "CMonsterWrapper::IsBlockedPoint, pMonster == NULL\n" );
		return 0;
	}
	CNewMap* pMap = pMonster->GetMap();
	if( NULL == pMap ) 
	{ 
		D_WARNING( "CMonsterWrapper::IsBlockedPoint, pMap == NULL\n" );
		return 0;
	}
	isBlocked = pMap->IsGridCanPass( x, y );

	return 1;
	TRY_END;
	return -1;
}


int CMonsterWrapper::GetSpawnedPoint( const AIMapInfo& mapInfo, unsigned long monsterID, TYPE_POS& x, TYPE_POS& y)
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster) 
	{ 
		D_WARNING( "CMonsterWrapper::GetSpawnedPoint, pMonster == NULL\n" );
		return 0;
	}
	x = pMonster->GetBornPtX();
	y = pMonster->GetBornPtY();

	return 1;
	TRY_END;
	return -1;
}


int CMonsterWrapper::GetMonsterSpeed( const AIMapInfo& mapInfo, unsigned long monsterID, float& Speed)
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster) 
	{ 
		D_WARNING( "CMonsterWrapper::GetMonsterSpeed, pMonster == NULL\n" );
		return 0;
	}

	Speed = pMonster->GetSpeed();

	return 1;
	TRY_END;
	return -1;
}

int CMonsterWrapper::AddMonsterHp( const AIMapInfo& mapInfo, unsigned long monsterID, int nHpAdded)
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster) 
	{ 
		D_WARNING( "CMonsterWrapper::AddMonsterHp, pMonster == NULL\n" );
		return 0;
	}
	
	pMonster->AddHp( nHpAdded);
	D_DEBUG("调用CMonsterWrapper::AddMonsterHp \n");

	return 1;
	TRY_END;
	return -1;
}

int CMonsterWrapper::GetMonsterInitHp( const AIMapInfo& mapInfo, unsigned long monsterID, unsigned int& nInitHp )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster) 
	{ 
		D_WARNING( "CMonsterWrapper::GetMonsterInitHp, pMonster == NULL\n" );
		return 0;
	}
	nInitHp = pMonster->GetInitHp();

	return 1;
	TRY_END;
	return -1;
}

//int CMonsterWrapper::IsMonsterInMap( unsigned long mapID, unsigned long monsterID, bool& bRet)
//{
//	TRY_BEGIN;
//
//	CMonster* pMonster = GetMonsterPtrByID( mapID, monsterID );
//
//	if( NULL == pMonster)
//	{
//		D_WARNING("IsMonsterInMap: pMonster == NULL\n");
//		return 0;
//	}
//	CNewMap* pMap = pMonster->GetMap();
//	if( NULL == pMap )
//	{
//		bRet = false;
//		return 1;
//	}
//	if( !pMap->IsMonsterInMap( pMonster ) )
//	{
//		bRet = false;
//		return 1;
//	}
//	bRet = true;
//
//	return 1;
//	TRY_END;
//	return -1;
//}

int CMonsterWrapper::ReturnStarted( const AIMapInfo& mapInfo, unsigned long monsterID )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{
		D_WARNING( "CMonsterWrapper::ReturnStarted pMonster==NULL\n" );
		return 0;
	}
	pMonster->ReturnStarted();

	return 1;
	TRY_END;
	return -1;
}


int CMonsterWrapper::ReturnEnded( const AIMapInfo& mapInfo, unsigned long monsterID )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster)
	{
		D_WARNING( "CMonsterWrapper::ReturnEnded pMonster==NULL\n" );
		return 0;
	}
	pMonster->ReturnEnded();
	
	return 1;
	TRY_END;
	return -1;
}

////怪物通知客户端停止
//int CMonsterWrapper::MonsterStop( CMonster* pMonster)
//{
//	TRY_BEGIN;
//
//	if( NULL == pMonster)
//	{
//		D_WARNING( "CMonsterWrapper::MonsterStop pMonster==NULL\n" );
//		return 0;
//	}
//	pMonster->Stop();
//
//	return 1;
//	TRY_END;
//	return -1;
//}

//int CMonsterWrapper::GetMapMonsterIn( unsigned long mapID, unsigned long monsterID, unsigned long& mapID)
//{
//	TRY_BEGIN;
//
//	CMonster* pMonster = GetMonsterPtrByID( mapID, monsterID );
//
//	if( NULL == pMonster)
//	{
//		D_WARNING( "CMonsterWrapper::GetMapMonsterIn pMonster==NULL\n" );
//		return 0;
//	}
//	CNewMap* pMap = pMonster->GetMap();
//	if( NULL == pMap)
//	{
//		D_WARNING( "CMonsterWrapper::GetMapMonsterIn pMap==NULL\n" );
//		return 0;
//	}
//	mapID = pMap->GetMapNo();
//
//	return 1;
//	TRY_END;
//	return -1;
//}

int CMonsterWrapper::GetMapExtent( const AIMapInfo& mapInfo, TYPE_POS& maxX, TYPE_POS& maxY )
{
	TRY_BEGIN;

	CMuxMapProperty* pMapProperty = NULL;

	if ( mapInfo.isNormalOrCopy )
	{
		//普通地图，直接根据地图号找地图属性；
		pMapProperty = CManMapProperty::FindMapProperty( mapInfo.mapID );	
	} else {
		//副本地图，根据副本号找到地图，再找地图属性;
		CMuxMap* pMap = NULL;
		if ( !CManMuxCopy::FindCopyByCopyID( mapInfo.mapID, pMap ) )
		{
			D_ERROR( "CMonsterWrapper::GetMapExtent，欲查找的副本%d已不存在\n", mapInfo.mapID );
			return -1;
		}
		if ( NULL != pMap )
		{
			pMapProperty = CManMapProperty::FindMapProperty( pMap->GetMapNo() );
		}
	}

	if( NULL == pMapProperty)
	{
		D_WARNING( "CMonsterWrapper::GetMapExtent pMapProperty==NULL\n" );
		return 0;
	}
	maxX = pMapProperty->GetGridWidth();
	maxY = pMapProperty->GetGridHeight();

	return 1;
	TRY_END;
	return -1;
}

int CMonsterWrapper::SendThreatList( const AIMapInfo& mapInfo, unsigned long monsterID, ThreatList& list)
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster )
	{
		D_WARNING( "CMonsterWrapper::SendThreatList pMonster==NULL\n" );
		return 0;
	}
	
	D_DEBUG("发送威胁列表 怪物ID:%d\n",pMonster->GetMonsterID() );
	MGThreatList msg;
	msg.threatList = list;
	TYPE_POS posX = 0, posY =0;
	pMonster->GetPos( posX, posY );

	CMuxMap* pMap = pMonster->GetMap();
	if( NULL != pMap )
	{
		pMap->NotifySurrounding<MGThreatList>( &msg, posX, posY ); 
	} else {
		return 0;
	}

	return 1;
	TRY_END;
	return -1;
}

///是否为跟随玩家的NPC；
int CMonsterWrapper::IsEscortedNPC( const AIMapInfo& mapInfo, unsigned long monsterID, bool& bRet)
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		D_WARNING( "CMonsterWrapper::IsEscortedNPC pMonster==NULL\n" );
		return 0;
	}

	bRet = pMonster->IsFollowing();

	return 1;
	TRY_END;
	return -1;
}

///得到NPC跟随的玩家，如果为非跟随NPC，返回-1；
int CMonsterWrapper::GetEscortHost( const AIMapInfo& mapInfo, unsigned long monsterID, PlayerID& playerID )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		D_WARNING( "CMonsterWrapper::IsEscortedNPC pMonster==NULL\n" );
		playerID.wGID = 0;
		playerID.dwPID = 0;
		return -1;
	}

	CPlayer* pPlayer = NULL;

	if ( pMonster->IsFollowing() )
	{
		pPlayer = pMonster->GetFollowingPlayer();
	}

	if ( NULL != pPlayer )
	{
		playerID = pPlayer->GetFullID();
		return 0;
	} else {
		playerID.wGID = 0;
		playerID.dwPID = 0;
		return -1;
	}

	return 1;
	TRY_END;
	return -1;
}

///判断NPC是否不调用AI, isDisableAI=true时不调用AI，isDisableAI=false时调用AI；
int CMonsterWrapper::IsMonsterDisableAI( const AIMapInfo& mapInfo, unsigned long monsterID, bool& isDisableAI )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	isDisableAI = true; //默认不调用AI；
	if ( NULL == pMonster )
	{
		D_WARNING( "CMonsterWrapper::IsMonsterDisableAI pMonster==NULL\n" );
		return 0;
	}

	isDisableAI = pMonster->IsDisableAI();

	return 1;
	TRY_END;
	return -1;
}

///查询跟随NPC所跟随玩家的移动信息
int CMonsterWrapper::GetFollowPlayerMoveInfo( const AIMapInfo& mapInfo, unsigned long monsterID, bool& isFollowing/*是否为跟随NPC，false的话，后续返回值皆无效*/
								   , bool& isPlayerMoving, TYPE_POS& targetX, TYPE_POS& targetY, float& moveSpeed )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		D_WARNING( "CMonsterWrapper::GetFollowPlayerMoveInfo pMonster==NULL\n" );
		return 0;
	}

	isFollowing = pMonster->IsFollowing();
	if ( !isFollowing )
	{
		//非跟随NPC，没有可供查询的信息；
		return 1;
	}

	CPlayer* pPlayer = pMonster->GetFollowingPlayer();
	if ( NULL == pPlayer )
	{
		D_WARNING( "CMonsterWrapper::GetFollowPlayerMoveInfo 取不到跟随NPC所跟随的玩家\n" );
		isFollowing = false;//取不到跟随玩家的指针，防止AI错误使用无效的移动信息；
		return 0;
	}

	isPlayerMoving = pPlayer->IsMoving();
	targetX = pPlayer->GetGridTargetX();
	targetY = pPlayer->GetGridTargetY();
	moveSpeed = pPlayer->GetSpeed();

	return 1;
	TRY_END;
	return -1;
}


int CMonsterWrapper::IsMonsterMoving( const AIMapInfo& mapInfo, unsigned long monsterID, bool& isMoving )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if( NULL == pMonster )
	{
		return -1;
	}

    isMoving = pMonster->IsMonsterMoving();
	
	return 1;
	TRY_END;
	return -1;
}


int CMonsterWrapper::GetMonsterSize( const AIMapInfo& mapInfo, unsigned long monsterID, int& monsterSize )
{
	TRY_BEGIN;

    CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster )
	{
		return -1;
	}

	monsterSize = pMonster->GetMonsterSize();

	return 1;
	TRY_END;
	return -1;
}

//取怪物类型；
int CMonsterWrapper::GetMonsterType( const AIMapInfo& mapInfo, unsigned long monsterID, unsigned long& monsterTypeID )
{
	TRY_BEGIN;

    CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster )
	{
		return -1;
	}

	monsterTypeID = pMonster->GetClsID();

	return 1;
	TRY_END;
	return -1;
}


const char* CMonsterWrapper::GetMonsterName( const AIMapInfo& mapInfo, TYPE_ID monsterID )
{
	TRY_BEGIN;

    CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster )
	{
		return NULL;
	}

	return pMonster->GetNpcName();

	//CNPCProp* pProp = pMonster->GetNpcProp();
	//if( !pProp )
	//	return NULL;

	//return pProp->m_npcName;

	TRY_END;
	return NULL;
}


//获取玩家姓名和HP百分比
int CMonsterWrapper::GetPlayerInfo( const PlayerID& reqID, const char*& playerName, float& hpPercent,char& sex, int& level )
{
	TRY_BEGIN;

	CGateSrv* pGateSrv = CManG_MProc::FindProc( reqID.wGID );
	if( NULL == pGateSrv )
	{
		return -1;
	}
	CPlayer* pPlayer = pGateSrv->FindPlayer( reqID.dwPID );
	if( NULL == pPlayer )
	{
		return -1;
	}

	playerName = pPlayer->GetNickName();
	hpPercent = pPlayer->GetCurrentHP() / (float)pPlayer->GetMaxHP();
	level = pPlayer->GetLevel();
	sex = pPlayer->GetSex();

	return 1;
	TRY_END;
	return -1;
}


int CMonsterWrapper::SetPlayerHpPercent( const PlayerID& reqID, float hpPercent )
{
	TRY_BEGIN;

	D_WARNING( "CMonsterWrapper::SetPlayerHpPercent，使用已废弃接口\n" );

	//CGateSrv* pGateSrv = CManG_MProc::FindProc( reqID.wGID );
	//if( NULL == pGateSrv )
	//{
	//	return -1;
	//}
	//CPlayer* pPlayer = pGateSrv->FindPlayer( reqID.dwPID );
	//if( NULL == pPlayer )
	//{
	//	return -1;
	//}

	//CMuxMap* pMap = pPlayer->GetCurMap();
	//if( !pMap )
	//	return -1;

	//MGRoleattUpdate updatemsg;
	//updatemsg.nAddType = MGRoleattUpdate::TYPE_SET;
	//updatemsg.changeTime = 0;
	//updatemsg.bFloat = false;
	//updatemsg.lChangedPlayerID = pPlayer->GetFullID();
	//updatemsg.nOldValue = pPlayer->GetCurrentHP();
	//updatemsg.nNewValue = (int)(pPlayer->GetMaxHP() * hpPercent);
	//updatemsg.nType = RT_HP;
	//pMap->NotifySurrounding<MGRoleattUpdate>( &updatemsg, pPlayer->GetPosX(), pPlayer->GetPosY() );

	//pPlayer->GetDBProp().uiHP = updatemsg.nNewValue;

	return 1;
	TRY_END;
	return -1;
}

int CMonsterWrapper::SetMonsterHpPercent( const AIMapInfo& mapInfo,  TYPE_ID monsterID, float hpPercent )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster )
	{
		return -1;
	}

	CMuxMap* pMap = pMonster->GetMap();
	if( !pMap )
		return -1;
	
	unsigned long newHp = (unsigned long) (pMonster->GetMaxHp() * hpPercent);

	pMonster->SetHp( newHp );
	pMap->NotifySurrounding<MGMonsterInfo>( pMonster->GetMonsterInfo() ,  pMonster->GetPosX(), pMonster->GetPosY()  );

	return 1;
	TRY_END;
	return -1;
}

int CMonsterWrapper::ClearMonsterBuff( const AIMapInfo& mapInfo,  TYPE_ID monsterID, bool bBuff )	//7 清楚所有怪物的BUFF或者DEBUFF效果
{
	TRY_BEGIN;

   	return 1;
	TRY_END;
	return -1;
}

///请求在指定地图指定点召唤一个指定类型怪物，
///    为了防止不正确的调用时机导致迭代器错误，本函数调用过程中不会立刻生成此怪物，而只是保存该请求，并在本函数返回后的某安全时刻刷此怪。
int CMonsterWrapper::CallMonsterReq( unsigned long callerID, unsigned long npcType, const AIMapInfo& mapInfo, TYPE_POS posX, TYPE_POS posY, unsigned int flag )
{
	TRY_BEGIN;

	CNewMap* pMap = FindMapByAiMapInfo( mapInfo );	
	if( NULL == pMap)
	{
		D_WARNING( "CMonsterWrapper::CallMonsterReq pMap==NULL\n" );
		return 0;
	}

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, callerID );
	if( NULL == pMonster )
	{
		D_WARNING( "CMonsterWrapper::CallMonsterReq callerID invalid\n" );
		return 0;
	}

	PlayerID rewardID = pMonster->GetRewardInfo().rewardPlayerID;

	pMap->AddOneNoCreatorNpc( callerID, npcType, posX, posY, rewardID, flag );

	return 1;
	TRY_END;
	return -1;
}

//int CMonsterWrapper::GetScopeMonsters( const AIMapInfo& mapInfo,  TYPE_ID monsterID, ScopeMonsterInfo*& scopeMonstrArr, int& arrSize )
//{
//	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
//	if( NULL == pMonster )
//	{
//		return -1;
//	}
//
//	CMuxMap* pMap = pMonster->GetMap();
//	if( NULL == pMap )
//		return -1;
//
//	int posX = pMonster->GetPosX();
//	int posY = pMonster->GetPosY();
//
//	pMap->GetScopeMonsters( posX, posY, m_scopeMonsters, ARRAY_SIZE(m_scopeMonsters), arrSize );
//
//	//TYPE_POS blockX,blockY;
//	//MuxMapSpace::GridToBlockPos( posX, posY, blockX, blockY );
//	//TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
//	//if ( !pMap->GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	//{
//	//	//输入点为无效点；
//	//	return -1;
//	//}
//	//MuxMapBlock* pBlock = NULL;
//	//int tempArrSize = 0;
//	////int nRange = pSkill->GetRange();
//	//for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	//{
//	//	for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//	//	{
//	//		pBlock = pMap->GetMapBlockByBlock( x,y );
//	//		if ( NULL != pBlock )
//	//		{
//	//			if( !pBlock->GetBlockMonsterID( m_scopeMonsters, MAX_SCOPE_MONSTER, tempArrSize ) )
//	//				break;
//	//		}
//	//	}
//	//}
//
//	scopeMonstrArr = &m_scopeMonsters[0];
//	//arrSize = tempArrSize;
//	return 1;
//}


//int CMonsterWrapper::GetScopePlayers( const AIMapInfo& mapInfo, TYPE_ID monsterID, ScopePlayerInfo*& scopePlayerArr, int& arrSize )
//{
//	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
//	if( NULL == pMonster )
//	{
//		return -1;
//	}
//
//	CMuxMap* pMap = pMonster->GetMap();
//	if( NULL == pMap )
//		return -1;
//
//	int posX = pMonster->GetPosX();
//	int posY = pMonster->GetPosY();
//
//	pMap->GetScopePlayers( posX, posY, m_scopePlayers, ARRAY_SIZE(m_scopePlayers), arrSize );
//	//TYPE_POS blockX,blockY;
//	//MuxMapSpace::GridToBlockPos( posX, posY, blockX, blockY );
//	//TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
//	//if ( !pMap->GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	//{
//	//	//输入点为无效点；
//	//	return -1;
//	//}
//	//MuxMapBlock* pBlock = NULL;
//	//int tempArrSize = 0;
//	////int nRange = pSkill->GetRange();
//	//for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	//{
//	//	for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//	//	{
//	//		pBlock = pMap->GetMapBlockByBlock( x,y );
//	//		if ( NULL != pBlock )
//	//		{
//	//			if( !pBlock->GetBlockPlayerInfo( m_scopePlayers, MAX_SCOPE_MONSTER, tempArrSize ) )
//	//				break;
//	//		}
//	//	}
//	//}
//
//	scopePlayerArr = &m_scopePlayers[0];
//	//arrSize = tempArrSize;
//	return 1;
//}

int CMonsterWrapper::IsInNeighborBlock( const AIMapInfo& mapInfo,  TYPE_ID monsterID, const PlayerID& playerID, bool& bNeighbor )
{
	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster )
	{
		return -1;
	}

	CMuxMap* pMap = pMonster->GetMap();
	if( NULL == pMap )
		return -1;

	CGateSrv* pGateSrv = CManG_MProc::FindProc( playerID.wGID );
	if( NULL == pGateSrv )
	{
		return -1;
	}
	CPlayer* pPlayer = pGateSrv->FindPlayer( playerID.dwPID );
	if( NULL == pPlayer )
	{
		return -1;
	}

	int posX = pMonster->GetPosX();
	int posY = pMonster->GetPosY();
	TYPE_POS blockX1,blockY1,blockX2,blockY2;
	MuxMapSpace::GridToBlockPos( posX, posY, blockX1, blockY1 );
	posX = pPlayer->GetPosX();
	posY = pPlayer->GetPosY();
	MuxMapSpace::GridToBlockPos( posX, posY, blockX2, blockY2 );
	bNeighbor = ( (abs(blockX1-blockX2) <= 1) && (abs(blockY1-blockY2) <= 1) );
	return 1;
}


int CMonsterWrapper::IsInNeighborBlock( const AIMapInfo& mapInfo,  TYPE_ID monsterID1, TYPE_ID monsterID2, bool& bNeighbor )
{
	CMonster* pMonster1 = GetMonsterPtrByID( mapInfo, monsterID1 );
	if( NULL == pMonster1 )
	{
		return -1;
	}

	CMonster* pMonster2 = GetMonsterPtrByID( mapInfo, monsterID2 );
	if( NULL == pMonster2 )
	{
		return -1;
	}

	CMuxMap* pMap = pMonster1->GetMap();
	if( NULL == pMap )
		return -1;

	int posX = pMonster1->GetPosX();
	int posY = pMonster1->GetPosY();
	TYPE_POS blockX1,blockY1,blockX2,blockY2;
	MuxMapSpace::GridToBlockPos( posX, posY, blockX1, blockY1 );
	posX = pMonster2->GetPosX();
	posY = pMonster2->GetPosY();
	MuxMapSpace::GridToBlockPos( posX, posY, blockX2, blockY2 );
	bNeighbor = ( (abs(blockX1-blockX2) <= 1) && (abs(blockY1-blockY2) <= 1) );
	return 1;
}


int CMonsterWrapper::SetMonsterMesh( const AIMapInfo& mapInfo,  TYPE_ID monsterID, TYPE_ID meshID )	//
{
	ACE_UNUSED_ARG( mapInfo ); ACE_UNUSED_ARG( monsterID ); ACE_UNUSED_ARG( meshID );
	return 1;
}


int CMonsterWrapper::SetMonsterSize( const AIMapInfo& mapInfo,  TYPE_ID monsterID, float newSize )
{
	ACE_UNUSED_ARG( mapInfo ); ACE_UNUSED_ARG( monsterID ); ACE_UNUSED_ARG( newSize );
	return 1;
}



int CMonsterWrapper::GetMonsterAttackInterval( const AIMapInfo& mapInfo,  TYPE_ID monsterID, int& attackInterval ) //攻击间隔
{
	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster )
	{
		return -1;
	}

	attackInterval = pMonster->GetAttackInterval();

	//CNPCProp* pNpcProp = pMonster->GetNpcProp();
	//if( pNpcProp )
	//	attackInterval = pNpcProp->m_nAttackInterval;

	return 1;
}


int CMonsterWrapper::MonsterPlaySound( const AIMapInfo& mapInfo,  TYPE_ID monsterID, const char* path, CHAT_TYPE chatType )
{
	ACE_UNUSED_ARG( path ); ACE_UNUSED_ARG( chatType );
	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster )
	{
		return -1;
	}

	return 1;
}


int CMonsterWrapper::GetMonsterLevel( const AIMapInfo& mapInfo,  TYPE_ID monsterID, unsigned long& level )
{
	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster )
	{
		return -1;
	}

	CMuxMap* pMap = pMonster->GetMap();
	if( NULL == pMap )
		return -1;

	level = pMonster->GetLevel();
	return 1;
}

///////////////////////////以下护送任务添加////////////////////////////////////////////////////////

///设置玩家任务完成，0：设置成功，-1：设置失败，可能玩家身上无任务；
int CMonsterWrapper::SetTaskFinish( const PlayerID& playerID, unsigned int taskID, bool isSubmit/*是否同时提交任务*/  )
{
	TRY_BEGIN;

	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	bool isSetOK = false;
	if ( isSubmit )
	{
		isSetOK = pPlayer->SetTaskStat( taskID, NTS_FINISH );//完成且提交
	} else {
		isSetOK = pPlayer->SetTaskStat( taskID, NTS_TGT_ACHIEVE );//完成，但未提交
	}

	if ( isSetOK )
	{
		return 0;
	} else {
		return -1;
	}
	TRY_END;
	return -1;
}

///设置玩家任务失败，0：设置成功，-1：设置失败，可能玩家身上无任务；
int CMonsterWrapper::SetTaskFail( const PlayerID& playerID, unsigned int taskID )
{
	TRY_BEGIN;

	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	if ( pPlayer->SetTaskStat( taskID, NTS_FAIL ) )
	{
		return 0;
	} else {
		return -1;
	}
	TRY_END;
	return -1;
}

///设置玩家任务失败计时开始，0：设置成功，-1：设置失败，可能玩家身上无任务；
int CMonsterWrapper::SetTaskFailTimeSt( const PlayerID& playerID, unsigned int taskID, unsigned int failTime )
{
	TRY_BEGIN;

	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	if ( pPlayer->NotifyTaskFailTime( taskID, failTime ) )
	{
		return 0;
	} else {
		return -1;
	}

	return -1;
	TRY_END;
	return -1;
}

///设置玩家任务失败计时中止，0：设置成功，-1：设置失败，可能玩家身上无任务；
int CMonsterWrapper::SetTaskFailTimeCancel( const PlayerID& playerID, unsigned int taskID )
{
	TRY_BEGIN;

	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	if ( pPlayer->NotifyTaskFailTime( taskID, 0 ) )
	{
		return 0;
	} else {
		return -1;
	}

	return -1;
	TRY_END;
	return -1;
}

///设置怪物死亡，dieOrVanish(客户端表现)：true--死亡，vanish--消失，0：设置成功，-1：设置失败，可能此怪物不存在(warning)；
int CMonsterWrapper::SetMonsterDie( const AIMapInfo& mapInfo, TYPE_ID monsterID, bool dieOrVanish )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		return -1;
	}

	GCMonsterDisappear::E_DIS_TYPE disType = GCMonsterDisappear::M_DIE;
	if ( !dieOrVanish )
	{
		disType = GCMonsterDisappear::M_VANISH;		
	}
	pMonster->OnMonsterDie( NULL/*无人杀它*/, disType );
	
	return 0;
	TRY_END;
	return -1;
}

///设置怪物无敌，0：设置成功，-1：设置失败，可能此怪物不存在(warning)；
int CMonsterWrapper::SetMonsterUndefeat( const AIMapInfo& mapInfo, TYPE_ID monsterID, bool isCanBeAttack )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		return -1;
	}

	//怪物设为无敌；
	pMonster->SetAttackFlag( isCanBeAttack );
	
	return 0;
	TRY_END;
	return -1;
}


int CMonsterWrapper::IsPlayerEvil( const PlayerID& playerID, bool& bEvil )
{
	TRY_BEGIN;
	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	bEvil = pPlayer->IsEvil();
	
	return 0;
	TRY_END;
	return -1;
}

int CMonsterWrapper::QueryPlayerTaskState(const PlayerID& playerID, unsigned int taskID )
{
	TRY_BEGIN;
	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	int taskstate = (int)pPlayer->GetTaskStatus( taskID );
	return taskstate;
	TRY_END;
	return -1;
}


int CMonsterWrapper::IsPlayerRogue( const PlayerID& playerID, bool& bRogue )
{
	TRY_BEGIN;
	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	bRogue = pPlayer->IsHaveRogueState();
	
	return 0;
	TRY_END;
	return -1;
}


int CMonsterWrapper::GetMonsterRaceAttribute( const AIMapInfo& mapInfo,  TYPE_ID monsterID, UINT& raceAttribute )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		return -1;
	}

	//获取怪物种族属性
	raceAttribute = pMonster->GetRaceAttribute();
	
	return 0;
	TRY_END;
	return -1;
}


int CMonsterWrapper::ChangeMonsterName( const AIMapInfo& mapInfo, const PlayerID& playerID, TYPE_ID monsterID, const char* name )
{
	TRY_BEGIN;

	//CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	//if ( NULL == pMonster )
	//{
	//	return -1;
	//}

	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	//CMuxMap* pMap = pMonster->GetMap();
	//if( !pMap )
	//{
	//	return -1;
	//}

	MGChangeTargetName srvmsg;
	SafeStrCpy( srvmsg.changeMsg.changeName, name );
	srvmsg.changeMsg.nameLen = (UINT)strlen( srvmsg.changeMsg.changeName ); 
	//srvmsg.changeMsg.targetID = pMonster->GetCreatureID();
	srvmsg.changeMsg.targetID.wGID = MONSTER_GID;
	srvmsg.changeMsg.targetID.dwPID = monsterID;

	//pMap->NotifySurrounding<MGChangeTargetName>( &srvmsg, pMonster->GetPosX(), pMonster->GetPosY() );
	pPlayer->SendPkg<MGChangeTargetName>( &srvmsg );

	return 0;
	TRY_END;
	return -1;
}


int CMonsterWrapper::ChangeMonsterIsDrop( const AIMapInfo& mapInfo,  TYPE_ID monsterID, bool isDropItem/*是否掉落包裹*/ )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		return -1;
	}

	//设置是否掉落物品
	pMonster->SetDropItemFlag( isDropItem );

	return 0;
	TRY_END;
	return -1;
}


//获得玩家种族
int CMonsterWrapper::GetPlayerRace( const PlayerID& playerID, unsigned& usRace )
{
	TRY_BEGIN;
	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	usRace = pPlayer->GetRace();
	return 0;
	TRY_END;
	return -1;
}


//获得是否能被攻击
int CMonsterWrapper::GetIsCanBeAttack( const AIMapInfo& mapInfo,  TYPE_ID monsterID, bool& isCanBeAttack )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		return -1;
	}


	isCanBeAttack = pMonster->IsCanBeAttacked();

	return 0;
	TRY_END;
	return -1;
}

//获得是否能被玩家攻击
int CMonsterWrapper::GetIsCanBeAttackPvc( const AIMapInfo& mapInfo,  TYPE_ID monsterID, bool& isCanBeAttackPvc )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		return -1;
	}


	isCanBeAttackPvc = pMonster->IsCanBeAttackedPvc();

	return 0;
	TRY_END;
	return -1;
}


//获得怪物的inhirentstand
int CMonsterWrapper::GetInherentStand( const AIMapInfo& mapInfo,  TYPE_ID monsterID, unsigned int& inherentStand )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		return -1;
	}

	inherentStand = pMonster->GetInherentStand();

	return 0;
	TRY_END;
	return -1;
}


int CMonsterWrapper::SendSpecialString( const AIMapInfo& mapInfo, const PlayerID& playerID, TYPE_ID monsterID, unsigned int stringType, const char* stringContent )
{
	TRY_BEGIN;

	//CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	//if ( NULL == pMonster )
	//{
	//	return -1;
	//}

	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		D_WARNING( "CMonsterWrapper::SendSpecialString找不到指定的player:%d:%d\n", playerID.wGID, playerID.dwPID );
		return -1;
	}

	//CMuxMap* pMap = pMonster->GetMap();
	//if( NULL == pMap )
	//{
	//	return -1;
	//}
	
	MGSendSpecialString gateMsg;
	StructMemSet( gateMsg, 0, sizeof(gateMsg) );
	gateMsg.stringMsg.characterID.dwPID = monsterID;
	gateMsg.stringMsg.characterID.wGID = MONSTER_GID;
	gateMsg.stringMsg.stringType = stringType;
	SafeStrCpy( gateMsg.stringMsg.stringContent, stringContent );
	gateMsg.stringMsg.stringLen = (unsigned int)strlen( gateMsg.stringMsg.stringContent );
	pPlayer->SendPkg<MGSendSpecialString>( &gateMsg );
//	pMap->NotifySurrounding<MGSendSpecialString>( &gateMsg, pMonster->GetPosX(), pMonster->GetPosY() );

	return 0;
	TRY_END;
	return -1;
}

int CMonsterWrapper::SetItemNpcSeq( const AIMapInfo& mapInfo, unsigned  int monsterID, unsigned int itemSeq, unsigned int status )
{
	TRY_BEGIN;

	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		return -1;
	}

	CMuxMap* pMap = pMonster->GetMap();
	if( NULL == pMap )
	{
		return -1;
	}
	
	if( !pMap->SetMapItemNpcState( itemSeq, status ) )
	{
		return -1;
	}
		
	return 0;
	TRY_END;
	return -1;
}


int CMonsterWrapper::IsCanPass( const AIMapInfo& mapInfo, float orgPosX, float orgPosY, float tgtPosX, float tgtPosY,  bool& isCanPass )
{
	TRY_BEGIN;
	CMuxMap* pMap = FindMapByAiMapInfo( mapInfo );
	if( NULL == pMap )
	{
		return -1;
	}

	//CMuxMapProperty* pProperty = pMap->GetMapProperty();
	//if( NULL == pProperty )
	//{
	//	return -1;
	//}

	//isCanPass = true;//by dzj, 10.04.12，因为原pProperty的IsPassOK都是返回真，且怪物本来就不限制行走；
	isCanPass = pMap->IsPassOK( orgPosX, orgPosY, tgtPosX, tgtPosY );
	return 0;
	TRY_END;
	return -1;
}


//获取玩家的目标
int CMonsterWrapper::GetPlayerTargetPos( const PlayerID& playerID, float& targetPosX, float& targetPosY )
{
	TRY_BEGIN;
	CPlayer* pPlayer = GetPlayerPtrByID( playerID );

	if ( NULL == pPlayer )
	{
		return -1;
	}

	targetPosX = pPlayer->GetFloatTargetX();
	targetPosY = pPlayer->GetFloatTargetY();
	return 0;
	TRY_END;
	return -1;
}


int CMonsterWrapper::GetIsAggressive( const AIMapInfo& mapInfo, unsigned int monsterID, unsigned int& isAggressive )
{
	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );

	if ( NULL == pMonster )
	{
		return -1;
	}

	isAggressive = pMonster->GetIsAggressive();
	return 0;
}


//TD 事件到达
int CMonsterWrapper::NotifyLevelMapInfo(const AIMapInfo& mapInfo, unsigned count)
{
	CMuxMap* pMap = FindMapByAiMapInfo( mapInfo );
	if( NULL == pMap )
	{
		return -1;
	}

	pMap->OnHandleScriptNotifyLevelMapInfo( count );
	return 0;
}


int CMonsterWrapper::GetMonsterKen( const AIMapInfo& mapInfo, unsigned int monsterID, int& ken )
{
	CMonster* pMonster = GetMonsterPtrByID( mapInfo, monsterID );
	if( NULL == pMonster )
	{
		return -1;
	}
	
	ken = pMonster->GetSight();
	return 0;
}

#endif //AI_SUP_CODE
