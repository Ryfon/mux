﻿/**
* @file manmonstercreator.cpp
* @brief 管理所有的怪物生成器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: manmonstercreator.cpp
* 摘    要: 管理所有的怪物生成器
* 作    者: dzj
* 完成日期: 2007.12.24
*
*/

#ifdef ACE_WIN32
#pragma once
#endif //ACE_WIN32

#include "Utility.h"

#include "manmonstercreator.h"
#include "monster.h"
#include "../MuxMap/muxmapbase.h"
#include "../MuxMap/manmapproperty.h"
#include <algorithm>

///本管理器管理的所有生成器属性；
map< unsigned long, map<unsigned long, CMutiMonsterCreatorProp*> > CManMonsterCreatorProp::m_mapMonsterCreators;///本管理器管理的所有生成器属性；

#ifdef NEW_NPC_CRE

///将指定地图的生成器填入输入向量；
void CManMonsterCreatorProp::FillMapCreators( CMuxMap* pInMap )
{
	TRY_BEGIN;

	if ( NULL == pInMap )
	{
		return;
	}

	map< unsigned long, map<unsigned long, CMutiMonsterCreatorProp*> >::iterator fliter = m_mapMonsterCreators.find( pInMap->GetMapNo() );
	if ( fliter == m_mapMonsterCreators.end() )
	{
		//没有该地图的生成器；
		return;
	}

	CCreatorIns* pCreatorIns = NULL;
	map<unsigned long, CMutiMonsterCreatorProp*>* pMapCreators = &(fliter->second);
	for ( map<unsigned long, CMutiMonsterCreatorProp*>::iterator tmpiter = pMapCreators->begin();
		tmpiter != pMapCreators->end(); ++ tmpiter )
	{
		if ( NULL != tmpiter->second )
		{
			pCreatorIns = NEW CCreatorIns( pInMap, tmpiter->second );
			if ( ! (pInMap->PushMapCreator( pCreatorIns ) ) )
			{
				delete pCreatorIns; pCreatorIns = NULL;
			}
		}
	}

	return;
	TRY_END;
	return;
}

///清指定地图的生成器；
void CManMonsterCreatorProp::ClearMapCreators( CMuxMap* pInMap )
{
	TRY_BEGIN;

	if ( NULL == pInMap )
	{
		return;
	}

	CCreatorIns* pCreatorIns = pInMap->PopMapCreator();
	while ( NULL != pCreatorIns )
	{
		delete pCreatorIns; pCreatorIns = NULL;
		pCreatorIns = pInMap->PopMapCreator();
	}

	return;
	TRY_END;
	return;
}

///从怪物生成器配置文件中读配置；
bool CManMonsterCreatorProp::LoadFromXML( const char* creatorPropFile )
{
	TRY_BEGIN;

	if ( NULL == creatorPropFile )
	{
		return false;
	}
	CXmlManager xmlLoader;
	if(!xmlLoader.Load(creatorPropFile))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;
	CElement* tmpCreatorEle = NULL;
	CMutiMonsterCreatorProp tmpProp;
	unsigned long mapID = 0;

	unsigned int rootLevel = pRoot->Level();
	int sortNum = 0;
	SingleMonsterCreatorProp npcInfo;
	npcInfo.NpcInfoClear();//准备读下一个npcInfo;
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		sortNum = 0;
		tmpEle = *tmpiter;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			const char* pTmpRead1 = (tmpEle)->GetAttr("MapNo");
			if ( NULL == pTmpRead1 )
			{
				D_ERROR("%s中格式错，读第%d条记录的MapNo错", creatorPropFile, tmpiter - elementSet.begin() );
				return false;
			}
			mapID = atoi( pTmpRead1 );//地图号;
			continue;
		} else if ( tmpEle->Level() == rootLevel+2 ) {
			tmpCreatorEle = tmpEle;//NPCCreator层节点;
			tmpProp.CMutiMonsterCreatorPropInit();//清本成员准备读下一个creator属性；
			if ( FillPropByElement( mapID, tmpCreatorEle, tmpProp, (int)(tmpiter-elementSet.begin()), sortNum ) )
			{
				for ( int i=0; i<sortNum; ++i )
				{
					///继续读NPC INFO节点；
					++tmpiter;
					if ( tmpiter == elementSet.end() )
					{
						D_ERROR( "CManMonsterCreatorProp::LoadFromXML中读NPC INFO中NPCNo时出错" );
						return false;
					}
					tmpEle = *tmpiter;
					/*<NPC NPCNo="33554435" Num="1" Direction="3" />*/
					if ( !IsXMLValValid( tmpEle, "NPCNo", i ) )
					{
						return false;
					}		
					npcInfo.npcClsID = atoi( tmpEle->GetAttr( "NPCNo" ) );

					tmpEle = *tmpiter;
					if ( !IsXMLValValid( tmpEle, "Num", i ) )
					{
						return false;
					}		
					npcInfo.npcNum = atoi( tmpEle->GetAttr( "Num" ) );

					tmpEle = *tmpiter;
					if ( !IsXMLValValid( tmpEle, "Direction", i ) )
					{
						return false;
					}		
					npcInfo.npcDir = atoi( tmpEle->GetAttr( "Direction" ) );

					tmpProp.m_vecNPCNo.push_back( npcInfo );//NpcInfo入creator中的数组；
					npcInfo.NpcInfoClear();//准备读下一个npcInfo;
				}
				tmpProp.CalAllNpcNum();//计算该生成器要刷的怪物总数；
				AddMonsterCreatorProp( mapID, tmpProp );
			} else {
				D_WARNING( "读怪物生成器属性错，属性:%s,IN:%s\n", (*tmpiter)->Name(), creatorPropFile );
				return false;
			}
		} else {
			D_WARNING( "怪物生成器XML配置错,IN:%s\n", creatorPropFile );
			return false;
		}
	}

	return true;
	TRY_END;
	return false;
}

///填充生成器属性；
bool CManMonsterCreatorProp::FillPropByElement( unsigned long mapID, CElement* pCreatorEle, CMutiMonsterCreatorProp& tmpProp, int nRecord, int& sortNum )
{
	TRY_BEGIN;

	if ( NULL == pCreatorEle )
	{
		return false;
	}
	tmpProp.m_dwMapNo = mapID;

	if ( !IsXMLValValid( pCreatorEle, "NPCCreatorNO", nRecord ) )
	{
		return false;
	}		
	tmpProp.m_dwClsID = atoi( pCreatorEle->GetAttr( "NPCCreatorNO" ) );//怪物生成器类型号；NPCCreatorNo="33554433"

	if ( !IsXMLValValid( pCreatorEle, "NPCCreatorName", nRecord ) )
	{
		return false;
	}		
	SafeStrCpy(tmpProp.m_strCreatorName, pCreatorEle->GetAttr( "NPCCreatorName" ) );//char m_strCreatorName[32];//NPCCreatorName="狼人"

	if ( !IsXMLValValid( pCreatorEle, "AreaMethod", nRecord ) )
	{
		return false;
	}		
	tmpProp.m_wAreaMethod = atoi( pCreatorEle->GetAttr( "AreaMethod" ) );

	if ( !IsXMLValValid( pCreatorEle, "TimeMethod", nRecord ) )
	{
		return false;
	}		
	tmpProp.m_wTimeMethod = atoi( pCreatorEle->GetAttr( "TimeMethod" ) );

	if ( !IsXMLValValid( pCreatorEle, "SupplyMethod", nRecord ) )
	{
		return false;
	}		
	tmpProp.m_wSupplyMethod = atoi( pCreatorEle->GetAttr( "SupplyMethod" ) );

	if ( ( 2 == tmpProp.m_wSupplyMethod )
		|| ( 4 == tmpProp.m_wSupplyMethod )
		|| ( 3 == tmpProp.m_wSupplyMethod )
		)
	{
		//固定间隔刷新；
		if ( !IsXMLValValid( pCreatorEle, "RefreshTime", nRecord ) )
		{
			return false;
		}		
		tmpProp.m_refreshInterval = atoi( pCreatorEle->GetAttr( "RefreshTime" ) );		
		if ( ( tmpProp.m_refreshInterval <= 0 ) && ( 4 == tmpProp.m_wSupplyMethod ) )
		{
			///定时刷新的间隔不能小于0，但整批补充暂时允许
			D_ERROR( "怪物生成器文件，第%d个生成器，定时刷新或整批刷怪物的刷新间隔小于等于0\n", nRecord );
			return false;
		}
	}

	if ( !IsXMLValValid( pCreatorEle, "Left", nRecord ) )
	{
		return false;
	}		
	tmpProp.m_wLeft = atoi( pCreatorEle->GetAttr( "Left" ) );

	if ( !IsXMLValValid( pCreatorEle, "Top", nRecord ) )
	{
		return false;
	}		
	tmpProp.m_wTop = atoi( pCreatorEle->GetAttr( "Top" ) );

	if ( !IsXMLValValid( pCreatorEle, "Right", nRecord ) )
	{
		return false;
	}		
	tmpProp.m_wRight = atoi( pCreatorEle->GetAttr( "Right" ) );

	if ( !IsXMLValValid( pCreatorEle, "Bottom", nRecord ) )
	{
		return false;
	}		
	tmpProp.m_wBottom = atoi( pCreatorEle->GetAttr( "Bottom" ) );

	if ( !IsXMLValValid( pCreatorEle, "TimeFormat", nRecord ) )
	{
		return false;
	}		
	tmpProp.m_wTimeFormat = atoi( pCreatorEle->GetAttr( "TimeFormat" ) );

	if ( !IsXMLValValid( pCreatorEle, "StartTime", nRecord ) )
	{
		return false;
	}
	unsigned long lsttime = atoi( pCreatorEle->GetAttr( "StartTime" ) );
	tmpProp.m_StartTime.timeMin = (unsigned short) (lsttime % 100);
	lsttime = lsttime / 100;
	tmpProp.m_StartTime.timeHour = (unsigned short) (lsttime % 100);
	lsttime = lsttime / 100;
	tmpProp.m_StartTime.timeDay = (unsigned short) (lsttime % 100);
	lsttime = lsttime / 100;
	tmpProp.m_StartTime.timeMonth = (unsigned short) (lsttime % 100);
	lsttime = lsttime / 100;
	tmpProp.m_StartTime.timeYear = (unsigned short) (lsttime % 100);
	lsttime = lsttime / 100;

	if ( !IsXMLValValid( pCreatorEle, "EndTime", nRecord ) )
	{
		return false;
	}		
	unsigned long lendtime = atoi( pCreatorEle->GetAttr( "EndTime" ) );
	tmpProp.m_EndTime.timeMin = (unsigned short) (lendtime % 100);
	lendtime = lendtime / 100;
	tmpProp.m_EndTime.timeHour = (unsigned short) (lendtime % 100);
	lendtime = lendtime / 100;
	tmpProp.m_EndTime.timeDay = (unsigned short) (lendtime % 100);
	lendtime = lendtime / 100;
	tmpProp.m_EndTime.timeMonth = (unsigned short) (lendtime % 100);
	lendtime = lendtime / 100;
	tmpProp.m_EndTime.timeYear = (unsigned short) (lendtime % 100);
	lendtime = lendtime / 100;

	if ( !IsXMLValValid( pCreatorEle, "SortNum", nRecord ) )
	{
		return false;
	}
	sortNum = atoi( pCreatorEle->GetAttr( "SortNum" ) );//本生成器使用时刷的NPC类型数；
	if ( sortNum <= 0 )
	{
		D_WARNING("SortNum = 0 ID:%d\n", tmpProp.m_dwClsID );
		return false;
	}
	D_INFO( "生成器%d混生怪物种类数:%d\n", tmpProp.GetClsID(), sortNum );

	if ( !IsXMLValValid( pCreatorEle, "Active", nRecord ) )
	{
		return false;
	}
	int isActive = atoi( pCreatorEle->GetAttr( "Active" ) );//本生成器使用时刷的NPC类型数；
	if ( 1==isActive )
	{
		//生成器默认开启；
		tmpProp.m_bIsInitOpen = true;
	} else {
		//生成器默认关闭；
		tmpProp.m_bIsInitOpen = false;
	}

#ifdef ITEM_NPC
	if ( !IsXMLValValid( pCreatorEle, "Itemnpcseq", nRecord ) )
	{
		return false;
	}
	tmpProp.m_itemNpcSeq = (int)atoi( pCreatorEle->GetAttr( "Itemnpcseq" ) );
#endif //ITEM_NPC

	return true;
	TRY_END;
	return false;
}

///添加一个怪物生成器属性到管理器；
bool CManMonsterCreatorProp::AddMonsterCreatorProp( unsigned long mapID/*地图ID号*/, const CMutiMonsterCreatorProp& creatorProp )
{
	TRY_BEGIN;

	map< unsigned long, map<unsigned long, CMutiMonsterCreatorProp*> >::iterator fliter = m_mapMonsterCreators.find( mapID );
	if ( fliter == m_mapMonsterCreators.end() )
	{
		//没有该地图的生成器，在map中对应该地图的选项中增加一项；
		map<unsigned long, CMutiMonsterCreatorProp*> tmpMap;
		m_mapMonsterCreators.insert( pair< unsigned long, map<unsigned long, CMutiMonsterCreatorProp*> >( mapID, tmpMap ) );
	}
	fliter = m_mapMonsterCreators.find( mapID );
	if ( fliter == m_mapMonsterCreators.end() )
	{
		//仍然找不到地图对应项，失败；
		return false;
	}
	map<unsigned long, CMutiMonsterCreatorProp*>* pMapItem = &(fliter->second);
	unsigned long inCreatorID = creatorProp.GetClsID();
	map<unsigned long, CMutiMonsterCreatorProp*>::iterator tmpiter = pMapItem->find( inCreatorID );
	if ( tmpiter != pMapItem->end() )
	{
		D_WARNING( "地图号%d,怪物生成器%d重复\n", mapID, inCreatorID );
		return false;
	}

	CMutiMonsterCreatorProp* pMonsterCreator = NEW CMutiMonsterCreatorProp;//保存怪物生成器属性；
	*pMonsterCreator = creatorProp;
	pMapItem->insert( pair<unsigned long, CMutiMonsterCreatorProp*>( inCreatorID, pMonsterCreator ) );
	return true;

	TRY_END;
	return false;
}

bool CCreatorIns::InitCreatorIns( CNewMap* pOwnerMap, CMutiMonsterCreatorProp* pCreatorProp )
{
	m_isInSupply = false;//不在补充队列中；
	m_pOwnerMap = NULL;
	m_pCreatorProp = NULL;

	if ( ( NULL == pOwnerMap )
		|| ( NULL == pCreatorProp )
		)
	{
		return false;
	}

	if ( pCreatorProp->GetResMapNo() != pOwnerMap->GetMapNo() )
	{
		D_ERROR( "CCreatorIns::InitCreatorIns，属性中指出的地图号%d,不同于传入地图号%d\n", pCreatorProp->GetResMapNo(), pOwnerMap->GetMapNo() );
		return false;//生成器属性中指明的所属地图号与传入地图的地图号不匹配；
	}

	m_pOwnerMap = pOwnerMap;
	m_pCreatorProp = pCreatorProp;

	if ( m_pCreatorProp->m_bIsInitOpen )
	{
		ResumeCreator();
	} else {
		PauseCreator();
	}

	m_bIsMonsterNeedSupply = false;
	m_wCurNum = 0;
	m_lastRefreshTime = ACE_OS::gettimeofday();

	m_vecSupplyNum.clear();//初始化成与属性中的m_vecNPCNo的大小一致；
	int npcclsnum = (int)(pCreatorProp->m_vecNPCNo.size());
	if ( npcclsnum <= 0 )
	{
		//生成器中的怪物类型数小于等于0，不可能错误；
		m_pOwnerMap = NULL;
		m_pCreatorProp = NULL;
		return false;
	}
	for ( int i=0; i<npcclsnum; ++i )
	{
		m_vecSupplyNum.push_back( 0 );//初始无待补充怪物；
	}

	return true;
}

///尝试补充怪物；
bool CCreatorIns::TrySupplyMonster( bool& isNeedMoreSupply/*下次是否仍需补充*/ )
{
	isNeedMoreSupply = false;//默认补充一次后就无需再补充;

	if ( ( NULL == m_pOwnerMap )
		|| ( NULL == m_pCreatorProp )
		)
	{
		return false;//所属地图空或生成器属性空；
	}

	if ( !IsActive() )
	{
		//生成器暂停；
		return true;
	}

	if ( ( 2==m_pCreatorProp->m_wSupplyMethod ) 
		|| ( 4 == m_pCreatorProp->m_wSupplyMethod ) 
		|| ( 3 == m_pCreatorProp->m_wSupplyMethod ) )
	{
		if ( m_pCreatorProp->m_refreshInterval > 0 )
		{
			//是否要检测补充间隔；
			if ( ( ACE_OS::gettimeofday() - m_lastRefreshTime ).sec() > m_pCreatorProp->m_refreshInterval )
			{
				//已到补充时间；
				m_lastRefreshTime = ACE_OS::gettimeofday();
			} else {
				//还未到补充时间
				isNeedMoreSupply = true;/*下次仍需补充*/ 
				return true;
			}
		} else {
			//小于等于0就不需要检测了，直接补充;
		}
	} 

	if ( !IsNeedSupply() )
	{
		return true;
	}
	SupplyMonster( isNeedMoreSupply );		
	return true;
}

///尝试使用本生成器在指定的地图上刷怪物；
///目前没有考虑生成器中指定的时间条件，以后时间条件加入后修改, by dzj, 08.04.08；
bool CCreatorIns::TryRefreshMonster()
{
	TRY_BEGIN;

	/**
	1、找到地图上的可用点;
	2、在这些可用点中随机挑选部分点；
	3、在挑选出的各点上执行单点刷怪；
	*/

	if ( ( NULL == m_pOwnerMap )
		|| ( NULL == m_pCreatorProp )
		)
	{
		return false;//所属地图空或生成器属性空；
	}

	if ( !IsActive() )
	{
		//生成器暂停；
		return true;
	}

    ClearCurNum();
	if ( m_pCreatorProp->m_vecCondiPt.empty() )
	{
		CMuxMapProperty* pMapProperty = m_pOwnerMap->GetMapProperty();
		if ( NULL == pMapProperty )
		{
			return false;
		}
		pMapProperty->GetMultiRefreshMonsterPtRect( m_pCreatorProp->m_wLeft, m_pCreatorProp->m_wTop
			, m_pCreatorProp->m_wRight, m_pCreatorProp->m_wBottom, m_pCreatorProp->m_vecCondiPt );

		if ( !(m_pCreatorProp->m_vecCondiPt.empty()) )
		{
			//随机取固定数量点；
			random_shuffle( m_pCreatorProp->m_vecCondiPt.begin(), m_pCreatorProp->m_vecCondiPt.end() );
		}
	}

	if ( m_pCreatorProp->m_vecCondiPt.empty() )
	{
		D_WARNING( "CCreatorIns::TryRefreshMonster无可用刷怪点，生成器号:%d，在地图:%d,区域左上角(%d,%d)，区域右下角(%d,%d)\n"
			, m_pCreatorProp->GetClsID(), m_pOwnerMap->GetMapNo(), m_pCreatorProp->m_wLeft, m_pCreatorProp->m_wTop, m_pCreatorProp->m_wRight, m_pCreatorProp->m_wBottom );
		return false;
	}

	int vecCondiPtNum = (int)(m_pCreatorProp->m_vecCondiPt.size());

#ifdef ITEM_NPC
	if( m_pCreatorProp->IsCreateItemNpc() )
	{
		if( m_pCreatorProp->m_vecNPCNo.size() != 1 )
		{
			D_ERROR("CCreatorIns::TryRefreshMonster() 创建NPC的生成器生成的SingleMonsterCreatorProp只能是唯一\n");
			return false;
		}

		if( m_pCreatorProp->m_vecNPCNo[0].npcNum != 1 )
		{
			D_ERROR("CCreatorIns::TryRefreshMonster() 刷物件NPC的SingleMonsterCreatorProp里的npcNum只能为1\n");
			return false;
		}
	}
#endif  //ITEM_NPC

	//刷不同类怪物；
	for ( vector<SingleMonsterCreatorProp>::iterator iter=m_pCreatorProp->m_vecNPCNo.begin(); iter!=m_pCreatorProp->m_vecNPCNo.end(); ++iter )
	{
		//刷同类怪物中的多个；
		for ( int rfnum=0; rfnum<iter->npcNum; ++rfnum )
		{
			if ( GetCurNum() >= vecCondiPtNum )
			{
				return true; //可用刷怪点不够；
			}
			SinglePtRefreshWithCreator( iter->npcClsID
				, m_pCreatorProp->m_vecCondiPt[GetCurNum()].nPosX, m_pCreatorProp->m_vecCondiPt[GetCurNum()].nPosY, iter->npcDir );
			IncCurNum();
		}
	}

	return true;
	TRY_END;
	return false;
}

///添加一个待补充的怪物；
void CCreatorIns::AddSupplyMonsterInfo( unsigned long tmpClsID )
{
	TRY_BEGIN;

	if ( ( NULL == m_pOwnerMap )
		|| ( NULL == m_pCreatorProp )
		)
	{
		return;//所属地图空或生成器属性空；
	}

	switch ( m_pCreatorProp->m_wSupplyMethod )
	{
	case 1:
		//永不补充；
		break;
	case 2:
		{
			//逐个补充，在原出生点重新生成；
			vector<unsigned short>::iterator iterSupply = m_vecSupplyNum.begin();
			for ( vector<SingleMonsterCreatorProp>::iterator iter=m_pCreatorProp->m_vecNPCNo.begin(); iter!=m_pCreatorProp->m_vecNPCNo.end(); ++iter, ++iterSupply )
			{
				if ( iterSupply == m_vecSupplyNum.end() )
				{
					//超过了待补数组界；
					break;
				}
				if ( iter->npcClsID == tmpClsID )
				{
					++ (*iterSupply);//补充数量加1;
					break;
				}
			}
		}
		SetNeedSupply();
		break;
	case 3:
		//整批补充，如果已经没怪了，则一次性补充
		if ( GetCurNum() <= 0 )
		{
			//只要设一下标记即可，不必记录具体需要补充的怪物类别；
			SetNeedSupply();
		}
		break;
	case 4:
		//固定刷新间隔刷新；
		{
			//同逐个补充，在原出生点重新生成；
			vector<unsigned short>::iterator iterSupply = m_vecSupplyNum.begin();
			for ( vector<SingleMonsterCreatorProp>::iterator iter=m_pCreatorProp->m_vecNPCNo.begin(); iter!=m_pCreatorProp->m_vecNPCNo.end(); ++iter, ++iterSupply )
			{
				if ( iterSupply == m_vecSupplyNum.end() )
				{
					break;
				}
				if ( iter->npcClsID == tmpClsID )
				{
					++ (*iterSupply);//补充数量加1;
					break;
				}
			}
		}
		SetNeedSupply();
		break;
	default:
		{
			D_ERROR( "CCreatorIns::AddSupplyMonsterInfo不可识别的m_wSupplyMethod\n" );
		};
	}

	return;
	TRY_END;
	return;
}

///设补充怪物标记
void CCreatorIns::SetNeedSupply() 
{ 
	if ( !m_bIsMonsterNeedSupply )
	{
		//当前无需补充，是第一个死的怪物，重置上次刷新时刻为当前时刻，以便从死亡时刻开始起计算重生时间；
		m_lastRefreshTime = ACE_OS::gettimeofday();
	}
	m_bIsMonsterNeedSupply = true; 
	if ( !IsInSupply() )
	{
		//当前还不在补充队列中，加入补充队列；
		if ( NULL != m_pOwnerMap )
		{
			m_pOwnerMap->AddToSupplyCreator( this );//将自身加到补充列表中去；
		}
	}
	return;
}

///补充怪物；
void CCreatorIns::SupplyMonster( bool& isMoreSupply )
{
	isMoreSupply = false;//初始假设此次补充后无需再补充

	TRY_BEGIN;

	if ( ( NULL == m_pOwnerMap )
		|| ( NULL == m_pCreatorProp )
		)
	{
		return;//所属地图空或生成器属性空；
	}

	switch ( m_pCreatorProp->m_wSupplyMethod )
	{
	case 1:
		//永不补充，从管理器中删去；
		ClearNeedSupply();
		break;
	case 2:
		{
			//逐个补充，在原出生点重新生成；
			int vecCondiPtNum = (int)(m_pCreatorProp->m_vecCondiPt.size());
			if ( vecCondiPtNum <= 0 )
			{
				D_ERROR( "CCreatorIns::SupplyMonster，补充怪物时发现可用刷怪点数为0\n" );
				return;
			}
			int randPos = RAND % vecCondiPtNum;
			bool isNeedNextSupply = false;//下一次是否还需要补充；

			vector<unsigned short>::iterator iterSupply = m_vecSupplyNum.begin();
			for ( vector<SingleMonsterCreatorProp>::iterator iter=m_pCreatorProp->m_vecNPCNo.begin(); iter!=m_pCreatorProp->m_vecNPCNo.end(); ++iter, ++iterSupply )
			{
				if ( iterSupply == m_vecSupplyNum.end() )
				{
					//超过了待补数组界；
					break;
				}
				//刷补充个数个该类怪物；
				//for ( int rfnum=0; rfnum<iter->supplyInfo.supplyNum; ++rfnum )
				if ( *iterSupply > 0 )
				{
					if ( GetCurNum() >= vecCondiPtNum )
					{
						break; //可用刷怪点不够；
					}
					randPos = RAND % vecCondiPtNum;
					SinglePtRefreshWithCreator( iter->npcClsID, m_pCreatorProp->m_vecCondiPt[randPos].nPosX, m_pCreatorProp->m_vecCondiPt[randPos].nPosY, iter->npcDir );
					IncCurNum();
					--(*iterSupply);
					if ( (*iterSupply) > 0 )
					{
						isNeedNextSupply = true;
					}
				}
			}
			if ( !isNeedNextSupply )
			{
				ClearNeedSupply();
			} else {
				isMoreSupply = true;//下次仍需补充;
			}
		}
		break;
	case 3:
		{
			//整批补充，如果计数器到0，则重刷；
			if ( GetCurNum() <= 0 )
			{
				TryRefreshMonster();//重新刷所有怪物;
			} else {
				D_ERROR( "CCreatorIns::SupplyMonster，整批补充怪物时，发现当前该生成器还有怪物存活!\n" );
			}
		}
		ClearNeedSupply();
		break;
	case 4:
		{
			//同逐个补充，在原出生点重新生成；
			int vecCondiPtNum = (int)(m_pCreatorProp->m_vecCondiPt.size());
			if ( vecCondiPtNum <= 0 )
			{
				D_ERROR( "CCreatorIns::SupplyMonster，补充怪物时发现可用刷怪点数为0\n" );
				return;
			}
			int randPos = RAND % vecCondiPtNum;

			vector<unsigned short>::iterator iterSupply = m_vecSupplyNum.begin();
			for ( vector<SingleMonsterCreatorProp>::iterator iter=m_pCreatorProp->m_vecNPCNo.begin(); iter!=m_pCreatorProp->m_vecNPCNo.end(); ++iter, ++iterSupply )
			{
				if ( iterSupply == m_vecSupplyNum.end() )
				{
					//超过了待补数组界；
					break;
				}
				//刷补充个数个该类怪物；
				for ( int rfnum=0; rfnum<(*iterSupply); ++rfnum )
				{
					if ( GetCurNum() >= vecCondiPtNum )
					{
						break; //可用刷怪点不够；
					}
					randPos = RAND % vecCondiPtNum;
					SinglePtRefreshWithCreator( iter->npcClsID, m_pCreatorProp->m_vecCondiPt[randPos].nPosX, m_pCreatorProp->m_vecCondiPt[randPos].nPosY, iter->npcDir );
					IncCurNum();
				}
				(*iterSupply) = 0;//清补充数量信息；
			}
		}
		ClearNeedSupply();
		break;
	default:
		{
			D_ERROR( "CCreatorIns::SupplyMonster不可识别的m_wSupplyMethod\n" );
			ClearNeedSupply();
		};
	}

	return;
    TRY_END;
	return;
}

///每次本生成器生成的怪物应该删除时，调用本函数以检测是否补充并执行补充操作；
void CCreatorIns::OnMonsterDelSupply( CMonster* pMonster )
{
	TRY_BEGIN;

	if ( ( NULL == m_pOwnerMap )
		|| ( NULL == m_pCreatorProp )
		)
	{
		return;//所属地图空或生成器属性空；
	}

	if ( NULL == pMonster )
	{
		return;
	}

	if ( pMonster->GetOwnCreator() != this )
	{
		D_ERROR( "CCreatorIns::OnMonsterDelSupply，怪物的生成器不是this\n" );
		return;
	}
	
	DecCurNum();

	//以下检测是否需要补充怪物；
	/*
		1.	永不补充
		生成器只在第一次初始化的时候（对公共地图来说是启动服务器的时候；对副本来说是每一次创建副本）生成NPC，NPC消失后不补充；
		2.	逐个不断补充
		只要生成器处于开启状态，则服务器会不断补充。一旦玩家杀死生成器中的NPC（或者说是该NPC的实体消失），则服务器会马上补充上该个NPC；
		3.	整批补充
		当生成器处于开启状态时，服务器会为每个生成器维护一个计数器。刚开始生成时，计数器为生成器区域中的完整数字；当生成器中的NPC实体每消失一个，则计数器减1，直到计数器为0时，才重新再刷新一批。
		换句话说，也就是当生成器中所有NPC都清空的时候，生成器才开始一次性的整批补充。
	*/

	AddSupplyMonsterInfo( pMonster->GetClsID() );//添加补充怪物信息;

	return;
	TRY_END;
	return;
}

///无生成器单点刷怪；
bool CCreatorIns::SinglePtRefreshNoCreator( unsigned long dwNPCNo, CNewMap* pMap, TYPE_POS dwX, TYPE_POS dwY, unsigned short wDir, CMonster*& pMonsterCreated, CPlayer* pFollowPlayer )
{
	if ( !SinglePtRefreshMonster(dwNPCNo, pMap, dwX, dwY, wDir, NULL, pMonsterCreated, pFollowPlayer) )
	{
		return false;
	}

	return true;
}

///在地图上的指定点刷怪物；
bool CCreatorIns::SinglePtRefreshWithCreator( unsigned long dwNPCNo, TYPE_POS dwX, TYPE_POS dwY, unsigned short wDir )
{	
	if ( ( NULL == m_pOwnerMap )
		|| ( NULL == m_pCreatorProp )
		)
	{
		return false;//所属地图空或生成器属性空；
	}

	CMonster* pNewMonster = NULL;//有生成器者不需要输出新生成怪物的指针，只有非生成器生成者才有可能需要；
	if ( !SinglePtRefreshMonster(dwNPCNo, m_pOwnerMap, dwX, dwY, wDir, this, pNewMonster) )
	{
		return false;
	}

#ifdef ITEM_NPC
	if( m_pCreatorProp->IsCreateItemNpc() )
	{
		unsigned int itemSeq = (unsigned int)m_pCreatorProp->GetItemNpcSeq();
		pNewMonster->SetItemNpcFlag( true );
		pNewMonster->SetItemNpcSeq( itemSeq );
		
		if( m_pOwnerMap->IsHaveItemNpc( itemSeq ) )
		{
			bool bRet = false;
			bRet = m_pOwnerMap->SetMapItemNpcMonsterId( itemSeq, pNewMonster->GetMonsterID() );
			bRet = m_pOwnerMap->SetMapItemNpcState( itemSeq, 0 );			//初始状态都为0
			if( !bRet )
			{
				D_ERROR("CCreatorIns::SinglePtRefreshWithCreator 设置ITEMNPC 出错 ItemSeq:%d\n", itemSeq );
			}
		}else
		{
			//初次生成
			MapItemNpc* pItemNpc = NEW MapItemNpc;
			pItemNpc->itemNpcSeq = itemSeq;
			pItemNpc->resMonsterId = pNewMonster->GetMonsterID();
			pItemNpc->initStatus = 0;
			pItemNpc->curStatus = 0;
			if( !m_pOwnerMap->SetItemNpc( itemSeq, pItemNpc ) )
			{
				D_ERROR("地图 设置ITEMNPC错误 itemSeq:%d\n", itemSeq );
				delete pItemNpc;
			}
		}
	}
#endif  //ITEM_NPC

	return true;
}

//获取该生成器的ID
unsigned int CCreatorIns::GetCreatorID()
{
	if ( NULL == m_pCreatorProp )
	{
		D_ERROR( "CCreatorIns::GetCreatorID(), NULL == m_pCreatorProp\n" );
		return 0;
	}

	return m_pCreatorProp->GetClsID();
}//unsigned int CCreatorIns::GetCreatorID()


bool CCreatorIns::SinglePtRefreshMonster( unsigned long dwNPCNo, CNewMap* pMap, TYPE_POS dwX, TYPE_POS dwY
											 , unsigned short wDir, CCreatorIns* pCreator, CMonster*& pMonsterCreated, CPlayer* pFollowPlayer )
{
	TRY_BEGIN;

	pMonsterCreated = NULL;

	if ( NULL == pMap )
	{
		D_ERROR( "SinglePtRefreshMonster， NULL==pMap\n" );
		return false;
	}

	CNPCProp* pNpcProp = CManNPCProp::FindObj( dwNPCNo );
	if( NULL == pNpcProp )
	{
		D_ERROR( "SinglePtRefreshMonster， NPC属性空，NPCNo%d\n", dwNPCNo );
		return false;
	}

	//21是物件NPC不判断阻挡点
	if( pNpcProp->GetNpcType() != 21 )
	{
		if ( !pMap->IsEnableAddMonster( dwX, dwY ) )
		{
			//地图上该点是否允许添加怪物;
			D_WARNING( "SinglePtRefreshMonster,生成器%d,添加怪物类型%d，点%d,%d不允许添加怪物\n"
				, (NULL!=pCreator)?pCreator->GetCreatorID():0, dwNPCNo, dwX, dwY );
			return false;
		}
	}

//cb2解禁
#if 0
	if ( pNpcProp->GetNpcType() == 23 )
	{
		if ( !pMap->IsEnabelAddTower(dwX,dwY) )
		{
			//地图上该点是否允许添加怪物;
			D_WARNING( "SinglePtRefreshMonster,添加怪物错误2，点%d,%d不允许添加塔\n", dwX, dwY );
			return false;
		}
	}
#endif

	CMonster* pNewMonster = NULL;
	bool isUseManMonster = !(pMap->IsInsCopyMap());//是否使用CManMonster,普通地图使用，副本不使用;
	if ( isUseManMonster )
	{
		CManMonster::AddNewMonster( dwNPCNo, pNewMonster );//在全局列表中新增一个怪物；
	} else {
		CMuxCopyInfo::CreateNewMonster( dwNPCNo, pNewMonster );
	}
	
	if ( NULL != pNewMonster )
	{
		pNewMonster->SetOwnCreator( pMap, pCreator );//设怪物的所属生成器属性；
		pNewMonster->SetDir( wDir );//设置怪物面对的方向；

		if ( NULL != pFollowPlayer )//有跟随玩家;
		{
			pNewMonster->SetFollowing( pFollowPlayer );//设置怪物的跟随状态；
			pNewMonster->SetSpeed( pFollowPlayer->GetSpeed() );
			pFollowPlayer->SetFollowingMonster( pNewMonster );//添加跟随NPC；
		}

		if ( !( pMap->AddMonsterToMap( pNewMonster, dwX, dwY ) ) )//将新NPC加入到地图中去；
		{
			//经过了前面的IsAddMonsterEnable()判断，不应该再到此处；
			D_ERROR( "SinglePtRefreshMonster，添加怪物错误1，无法在地图点%d,%d添加怪物，不应该到此处\n", dwX, dwY );
			if ( isUseManMonster )
			{
				CManMonster::ReleaseManedMonster( pNewMonster );
			} else {
				CMuxCopyInfo::ReleaseOldMonster( pNewMonster );
			}
			return false;
		} else {
			//添加成功；
			pMonsterCreated = pNewMonster;
		}
	} else {
		if ( ( NULL != pCreator ) )
		{
			D_WARNING( "生成器%d刷怪，无法创建怪物:%d\n", pCreator->GetCreatorID(), dwNPCNo );
		} else {
			D_WARNING( "无生成器刷怪，无法创建怪物:%d", dwNPCNo );
		}
		return false;
	}

	return true;
	TRY_END;
	return false;
}
#endif //NEW_NPC_CRE
