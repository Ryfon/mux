﻿/**
* @file manmonstercreator.h
* @brief 管理所有的怪物生成器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: manmonstercreator.h
* 摘    要: 管理所有的怪物生成器
* 作    者: dzj
* 完成日期: 2007.12.24
*
*/

#pragma once

#include "../Player/Player.h"

#include "../MuxMap/MapCommonHead.h"

#include <vector>
using namespace std;

#define NEW_NPC_CRE //新NPC生成器相关代码

#ifdef NEW_NPC_CRE

struct MonsterCreatorTime
{
	unsigned short timeYear;//年
	unsigned short timeMonth;//月
	unsigned short timeDay;//日
	unsigned short timeHour;//时
	unsigned short timeMin;//分
};

///生成器信息中的某种怪物生成信息；
struct SingleMonsterCreatorProp
{
	///清成员；
	void NpcInfoClear()
	{
		npcClsID = 0ul;
		npcNum = 0u;
		npcDir = 0u;
	}
	/*
     NPC NPCNo="33554435" Num="1" Direction="3"
	*/
	unsigned long  npcClsID;//NPCNo="33554435",NPC类别号；
	unsigned short npcNum;//Num="1",每次刷新时的刷新数目；
	unsigned short npcDir;//Direction="3",每次刷出来的NPC朝向；
};

///包含多个SingleMonsterCreatorProp的生成器属性信息；
class CMutiMonsterCreatorProp
{
public:
	CMutiMonsterCreatorProp() 
	{
		CMutiMonsterCreatorPropInit();
	};
	~CMutiMonsterCreatorProp() {};

	friend class CManMonsterCreatorProp;
	friend class CCreatorIns;

	CMutiMonsterCreatorProp& operator = ( const CMutiMonsterCreatorProp& inProp )
	{
		if ( this == &inProp )
		{
			return *this;
		}

		m_bIsInitOpen = inProp.m_bIsInitOpen;
		m_dwMapNo = inProp.m_dwMapNo;
		m_dwClsID = inProp.m_dwClsID;
		SafeStrCpy( m_strCreatorName, inProp.m_strCreatorName );
		m_wAreaMethod = inProp.m_wAreaMethod;
		m_wTimeMethod = inProp.m_wTimeMethod;
		m_wSupplyMethod = inProp.m_wSupplyMethod;
		m_wLeft = inProp.m_wLeft;
		m_wTop = inProp.m_wTop;
		m_wRight = inProp.m_wRight;
		m_wBottom = inProp.m_wBottom;

		m_wTimeFormat = inProp.m_wTimeFormat;

		m_StartTime = inProp.m_StartTime;//StartTime="1700",有效时间开始；
		m_EndTime = inProp.m_EndTime;//EndTime="2300",有效时间结束；
		m_wSortNum = inProp.m_wSortNum;//SortNum="2",本生成器刷的怪物种类；

		m_vecNPCNo.assign( inProp.m_vecNPCNo.begin(), inProp.m_vecNPCNo.end() );

		m_wAllNpcNum = inProp.m_wAllNpcNum;//一次全新刷新需要刷的各类怪物总数；

		m_refreshInterval = inProp.m_refreshInterval;//固定刷新间隔方式下的刷新间隔(单位:秒)；

		m_vecCondiPt.assign( inProp.m_vecCondiPt.begin(), inProp.m_vecCondiPt.end() );
#ifdef ITEM_NPC
		m_itemNpcSeq = inProp.m_itemNpcSeq;
#endif  //ITEM_NPC
		return *this;
	}

public:
	///取本生成器的类ID号；
	unsigned long GetClsID() const { return m_dwClsID; }

	///取本生成器对应的地图号；
	unsigned long GetResMapNo() const { return m_dwMapNo; }

private:
	void CMutiMonsterCreatorPropInit()
	{
		m_bIsInitOpen = false;
		m_refreshInterval = 99999;
		m_vecCondiPt.clear();//清可用生成点，每次生成怪物时随机从中取一个；
		m_dwMapNo = 0;//MapNo="1" ，所属地图；
		/*
		<NPCCreator NPCCreatorNo="33554434" NPCCreatorName="护法军团" AreaMethod="2" TimeMethod="1" SupplyMethod="2" 
		Left="78" Top="55" Right="126" Bottom="97" TimeFormat="3" StartTime="1700" EndTime="2300" SortNum="2">
		<NPC NPCNo="33554433" Num="9" Direction="3" /> 
		<NPC NPCNo="33554434" Num="5" Direction="3" /> 
		</NPCCreator>
		*/
		m_dwClsID = 0; //NPCCreatorNO="33554433" 
		StructMemSet( m_strCreatorName, 0, sizeof(m_strCreatorName) );//NPCCreatorName="护法军团"
		m_wAreaMethod = 0;//AreaMethod="2"，刷新区域类型；
		m_wTimeMethod = 0;//TimeMethod="1"，刷新时间类型；
		m_wSupplyMethod = 0;//SupplyMethod="2"，补充方式类型；
		m_wLeft = 0;//Left="78",刷新区域左界；
		m_wTop = 0;//Top="55",刷新区域上界；
		m_wRight = 0;//Right="126",刷新区域右界；
		m_wBottom = 0;//Bottom="97",刷新区域下界；

		m_wTimeFormat = 0;//TimeFormat="3",刷新时间格式；
		StructMemSet( m_StartTime, 0, sizeof(m_StartTime) );//StartTime="1700",有效时间开始；
		StructMemSet( m_EndTime, 0, sizeof(m_EndTime) );//EndTime="2300",有效时间结束；
		m_wSortNum = 0;//SortNum="2",本生成器刷的怪物种类；

		m_vecNPCNo.clear();//NPCNo="33554433"

		m_wAllNpcNum = 0;//一次全新刷新需要刷的各类怪物总数；
#ifdef ITEM_NPC
		m_itemNpcSeq = -1;
#endif //ITEM_NPC
	}

	///计算一次完全刷新需要刷的各类怪物总数；
	void CalAllNpcNum()
	{
		m_wAllNpcNum = 0;
		for ( vector<SingleMonsterCreatorProp>::iterator iter=m_vecNPCNo.begin(); iter!=m_vecNPCNo.end(); ++iter )
		{
			m_wAllNpcNum += iter->npcNum;
		}
		return;
	}

#ifdef ITEM_NPC
	//是否生成ITEM NPC
	bool IsCreateItemNpc()
	{
		return (m_itemNpcSeq >= 0);
	}

	int GetItemNpcSeq()
	{
		return m_itemNpcSeq;
	}
#endif //ITEM_NPC

private:
	unsigned long m_dwMapNo;//MapNo="1" ，所属地图；
	/*
       <NPCCreator NPCCreatorNo="33554434" NPCCreatorName="护法军团" AreaMethod="2" TimeMethod="1" SupplyMethod="2" 
	       Left="78" Top="55" Right="126" Bottom="97" TimeFormat="3" StartTime="1700" EndTime="2300" SortNum="2">
            <NPC NPCNo="33554433" Num="9" Direction="3" /> 
            <NPC NPCNo="33554434" Num="5" Direction="3" /> 
       </NPCCreator>
	*/
    unsigned long m_dwClsID; //NPCCreatorNO="33554433" 
	char m_strCreatorName[32];//NPCCreatorName="护法军团"

	bool m_bIsInitOpen;//生成器初始是否开启；

	unsigned short m_wAreaMethod;//AreaMethod="2"，刷新区域类型；
	unsigned short m_wTimeMethod;//TimeMethod="1"，刷新时间类型；
	unsigned short m_wSupplyMethod;//SupplyMethod="2"，补充方式类型；
	unsigned short m_wLeft;//Left="78",刷新区域左界；
	unsigned short m_wTop;//Top="55",刷新区域上界；
	unsigned short m_wRight;//Right="126",刷新区域右界；
	unsigned short m_wBottom;//Bottom="97",刷新区域下界；

	unsigned short m_wTimeFormat;//TimeFormat="3",刷新时间格式；
	MonsterCreatorTime  m_StartTime;//StartTime="1700",有效时间开始；
	MonsterCreatorTime  m_EndTime;//EndTime="2300",有效时间结束；
	unsigned short m_wSortNum;//SortNum="2",本生成器刷的怪物种类；

	vector<SingleMonsterCreatorProp> m_vecNPCNo;//NPCNo="33554433"

	unsigned short m_wAllNpcNum;//一次全新刷新需要刷的各类怪物总数；

	int  m_refreshInterval;//固定刷新间隔方式下的刷新间隔(单位:秒)；

	vector<MuxPoint> m_vecCondiPt;//可用生成点，每次生成怪物时随机从中取一个；
#ifdef ITEM_NPC
	int m_itemNpcSeq;			  //物件NPC的序列,大于0说明是生成物件NPC
#endif //ITEM_NPC
};

///各地图实例实际使用的生成器实例；
class CCreatorIns
{
private:
	CCreatorIns();

public:
	CCreatorIns( CNewMap* pOwnerMap, CMutiMonsterCreatorProp* pCreatorProp )
	{
		InitCreatorIns( pOwnerMap, pCreatorProp );
	}
	~CCreatorIns(){}

public:
	bool InitCreatorIns( CNewMap* pOwnerMap, CMutiMonsterCreatorProp* pCreatorProp );
	CNewMap* GetOwnerMap() { return m_pOwnerMap; };

public:
	///尝试使用本生成器在指定的地图上刷怪物；
	///目前没有考虑生成器中指定的时间条件，以后时间条件加入后修改, by dzj, 08.04.08；
	bool TryRefreshMonster();

	unsigned short GetCurNum() { return m_wCurNum; }
	void ClearCurNum() { m_wCurNum = 0; }
	void IncCurNum() { ++m_wCurNum; }
	void DecCurNum() 
	{ 
		if ( m_wCurNum > 0 )
		{
			--m_wCurNum; 
		}
	}

	///每次本生成器生成的怪物应该删除时，调用本函数以检测是否补充并执行补充操作；
	void OnMonsterDelSupply( CMonster* pMonster );

public:
	void SetInSupply() { m_isInSupply = true; }
	void ClearInSupply() { m_isInSupply = false; }
	bool IsInSupply() { return m_isInSupply; }

private:
	bool m_isInSupply;//是否在补充队列中；

public:
	///添加一个待补充的怪物；
	void AddSupplyMonsterInfo( unsigned long tmpClsID );
	///尝试补充怪物；
	bool TrySupplyMonster( bool& isNeedMoreSupply/*下次是否仍需补充*/ );

	///无生成器单点刷怪；
	static bool SinglePtRefreshNoCreator( unsigned long dwNPCNo, CNewMap* pMap, TYPE_POS dwX, TYPE_POS dwY, unsigned short wDir, CMonster*& pMonsterCreated, CPlayer* pFollowPlayer  );

	///单点刷怪；
	static bool SinglePtRefreshMonster( unsigned long dwNPCNo, CNewMap* pMap, TYPE_POS dwX, TYPE_POS dwY, unsigned short wDir, CCreatorIns* pCreator, CMonster*& pMonsterCreated, CPlayer* pFollowPlayer = NULL );	

public:
	///暂停生成器；
	inline void PauseCreator() { m_bIsCurOpen = false; }
	///恢复生成器；
	inline void ResumeCreator() { m_bIsCurOpen = true; }
	///本生成器是否开启；
	inline bool IsActive() { return m_bIsCurOpen; }
	inline void SetActiveFlag( bool isActive ) { m_bIsCurOpen = isActive; }
	//获取该生成器的ID
	unsigned int GetCreatorID();

private:
	///是否需要补充怪物；
	bool IsNeedSupply()	{ return m_bIsMonsterNeedSupply; }
	///清补充怪物标记，每次补充怪物完毕后设置；
	void ClearNeedSupply() { m_bIsMonsterNeedSupply = false; }
	///设补充怪物标记
	void SetNeedSupply();
	///补充怪物；
	void SupplyMonster( bool& isMoreSupply );

	///在地图上的指定点刷怪物；
    bool SinglePtRefreshWithCreator( unsigned long dwNPCNo, TYPE_POS dwX, TYPE_POS dwY, unsigned short wDir );	

private:
	CNewMap* m_pOwnerMap;
	CMutiMonsterCreatorProp* m_pCreatorProp;

private:
	bool           m_bIsCurOpen;//该NPC生成器当前是否开启
	bool           m_bIsMonsterNeedSupply;//本生成器是否有需要补充的怪物；
	unsigned short m_wCurNum;//当前本生成器已生成的总数；

	ACE_Time_Value m_lastRefreshTime;//固定刷新间隔方式时的上次刷新时间；
	vector<unsigned short> m_vecSupplyNum;//补充数目,vector大小与属性中的m_vecNPCNo的大小一致；
};

#include "Utility.h"

#include "XmlManager.h"

#include <map>
#include <set>
using namespace std;

/**
* 怪物生成器管理器
* 内部维护一个地图ID――生成器ID――怪物生成器的映射;
* 负责向外提供怪物生成器查询，其向外提供的生成器不可被修改；
*/
class CManMonsterCreatorProp
{
private:
	CManMonsterCreatorProp( const CManMonsterCreatorProp& );  //屏蔽这两个操作；
	CManMonsterCreatorProp& operator = ( const CManMonsterCreatorProp& );//屏蔽这两个操作；

private:
	CManMonsterCreatorProp(){}
	~CManMonsterCreatorProp(){}

public:
	///初始化，从配置文件读入所有的怪物生成器属性;
	static bool InitAllMonsterProp()
	{ 
		m_mapMonsterCreators.clear();
		return LoadFromXML( "config/npc/npc_createinfo.xml" );
	};
	///释放本管理器管理的所有怪物生成器属性；
	static bool ReleaseAllMonsterProp()
	{
		TRY_BEGIN;

		map<unsigned long, CMutiMonsterCreatorProp*>* tmpmap = NULL;
		for ( map< unsigned long, map<unsigned long, CMutiMonsterCreatorProp*> >::iterator fliter = m_mapMonsterCreators.begin();
			fliter != m_mapMonsterCreators.end(); ++fliter )
		{
			tmpmap = &(fliter->second);
			for ( map<unsigned long, CMutiMonsterCreatorProp*>::iterator tmpiter = tmpmap->begin();
				tmpiter != tmpmap->end(); ++tmpiter )
			{
				if ( NULL != tmpiter->second )
				{
					delete tmpiter->second; tmpiter->second = NULL;
				}
			}
			tmpmap->clear();
		}

		m_mapMonsterCreators.clear();
		return true;

		TRY_END;
		return false;
	};

public:
	///将指定地图的生成器填入输入向量；
	static void FillMapCreators( CMuxMap* pInMap );

	///清指定地图的生成器；
	static void ClearMapCreators( CMuxMap* pInMap );

private:
	///从怪物生成器配置文件中读配置；
	static bool LoadFromXML( const char* creatorPropFile );

	///填充生成器属性；
	static bool FillPropByElement( unsigned long mapID, CElement* pCreatorEle, CMutiMonsterCreatorProp& tmpProp, int nRecord, int& sortNum );

	///将一个怪物生成器属性添加到管理器；
	static bool AddMonsterCreatorProp( unsigned long mapID/*地图ID号*/, const CMutiMonsterCreatorProp& creatorProp );

private:
	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
	{
		if ( NULL == pEle->GetAttr( attName ) )
		{
			D_ERROR( "读怪物生成器XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
			return false;
		}
		return true;
	}

private:
	/*///本管理器管理的所有怪物生成器属性<地图ID号，<生成器号，生成器> >*/
	static map< unsigned long, map<unsigned long, CMutiMonsterCreatorProp*> > m_mapMonsterCreators;
};

#endif //NEW_NPC_CRE


