﻿/**
* @file monsterwrapper.h
* @brief 定义怪物AI用扩展接口
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: monsterwrapper.h
* 摘    要: 定义怪物AI用扩展接口
* 作    者: dzj
* 完成日期: 2007.04.01
*
*/

#pragma once

#include "../Fsm/IFsm.h"
#include "Utility.h"
#include <vector>
#include "../monster/monster.h"

#ifdef AI_SUP_CODE

class CMonster;
class CPlayer;
class ACE_Time_Value;
class CNpcSkill;

#define MAX_SCOPE_MONSTER 30

class CMonsterWrapper
{
private:
	CMonsterWrapper();
	~CMonsterWrapper();

private:
	///根据AIMapInfo寻找相应地图对象指针；
	static CMuxMap* FindMapByAiMapInfo( const AIMapInfo& mapInfo );
	///根据ID号找怪物指针，找不到返回NULL；
	static CMonster* GetMonsterPtrByID( const AIMapInfo& mapInfo, unsigned long monsterID );
	///根据ID号找玩家指针，找不到返回NULL；	
	static CPlayer* GetPlayerPtrByID( const PlayerID& playerID );

private:
	static ScopeMonsterInfo m_scopeMonsters[MAX_SCOPE_MONSTER];	//用于返回范围BLOCK中的怪物
	static ScopePlayerInfo m_scopePlayers[MAX_SCOPE_MONSTER];	//用于返回范围BLOCK中的玩家

public:
	//获取事件信息结构体
	static int GetAIEventInfo( AIEventInfo*& pEventInfo);

public:
	///设置怪物的AI标记；
	static int SetMonsterAIFlag( const AIMapInfo& mapInfo, unsigned long monsterID, void* aiFlag );
	/////注册怪物接收玩家事件；
	//static int RegisterMonster( CMonster* pMonster, CPlayer* pPlayer );
	/////反注册怪物接收玩家事件；
	//static int UnRegisterMonster( CMonster* pMonster, CPlayer* pPlayer );
	///取怪物AIType;
	static int GetMonsterAIType( const AIMapInfo& mapInfo, unsigned long monsterID );
	///取怪物AIID；
	static unsigned long GetMonsterAIID( const AIMapInfo& mapInfo, unsigned long monsterID );
	///怪物向周围人说话；
	static int MonsterSpeak( const AIMapInfo& mapInfo, unsigned long monsterID, const char* pMsg, CHAT_TYPE chatType );  //3  只能用CT_SCOPE  CT_WORLD  by hyn 08.12.22
	//更改怪物的状态
	static int TransState( const AIMapInfo& mapInfo, unsigned long monsterID, MONSTER_STAT newState);

	//怪物用技能攻击玩家
	static int UseSkill( const AIMapInfo& mapInfo, unsigned long monsterID, const PlayerID& tgtPlayerID, int skillID);
	//怪物对其他怪物释放技能
	static int UseSkill( const AIMapInfo& mapInfo, unsigned long monsterID, unsigned long tgtMonsterID, int skillID );
	//返回怪物技能的指针
	static int GetMonsterSkillProp( int skillID, CMuxSkillProp*& pNpcSkill ); 
	//返回怪物的HP
	static int GetMonsterHp( const AIMapInfo& mapInfo, unsigned long monsterID );
	//获取怪物的状态
	static int GetState( const AIMapInfo& mapInfo, unsigned long monsterID, MONSTER_STAT& state);
	////获取玩家ID
	//static int GetPlayerID( CPlayer* pPlayer, unsigned long& lPlayerID );
	//判断是否在攻击范围之内
	static int IsInAttackRange( const AIMapInfo& mapInfo, unsigned long monsterID, const PlayerID& playerID, int dist, bool& res);
	//获取怪物的位置
	static int GetMonsterPosition( const AIMapInfo& mapInfo, unsigned long monsterID, TYPE_POS& x, TYPE_POS& y);
	//获取玩家的位置
	static int GetPlayerPosition( const PlayerID& playerID, TYPE_POS& x, TYPE_POS& y );
	////怪物移动
	//static int MonsterMove( unsigned long mapID, unsigned long monsterID, TYPE_POS x, TYPE_POS y, TYPE_POS nTargetx, TYPE_POS nTargetY);
	//怪物开始移动
	static int MonsterStartMove( const AIMapInfo& mapInfo, unsigned long monsterID, TYPE_POS gridCurX, TYPE_POS gridCurY, TYPE_POS gridTargetX, TYPE_POS gridTargetY, float fSpeed );
	//怪物移动中跨格(以MonsterStartMove为前提)
	static int MonsterBeyondGrid( const AIMapInfo& mapInfo, unsigned long monsterID, TYPE_POS gridOrgX, TYPE_POS gridOrgY, TYPE_POS gridNewX, TYPE_POS gridNewY );
	//怪物停止移动(以MonsterStartMove为前提)
	static int MonsterStopMove( const AIMapInfo& mapInfo, unsigned long monsterID );
	//是否障碍点
	static int IsBlockedPoint( const AIMapInfo& mapInfo, unsigned long monsterIDm, TYPE_POS x, TYPE_POS y, bool& IsBlocked);
	//获取怪物的出生点位置：
	static int GetSpawnedPoint( const AIMapInfo& mapInfo, unsigned long monsterID, TYPE_POS& x, TYPE_POS& y);
	//获取怪物的移动速度
	static int GetMonsterSpeed( const AIMapInfo& mapInfo, unsigned long monsterID, float& Speed);
	//增加怪物的HP
	static int AddMonsterHp( const AIMapInfo& mapInfo, unsigned long monsterID, int nHpAdded); 
	//获取怪物初始HP
	static int GetMonsterInitHp( const AIMapInfo& mapInfo, unsigned long monsterID, unsigned int& nInitHp);
	//怪物回归
	static int ReturnStarted( const AIMapInfo& mapInfo, unsigned long monsterID );
	//怪物结束
	static int ReturnEnded( const AIMapInfo& mapInfo, unsigned long monsterID );
	////怪物当前停止,让客户端也停止
	//static int MonsterStop( CMonster* pMonster);
	////查询怪物所在地图的ID
	//static int GetMapMonsterIn( unsigned long mapID, unsigned long monsterID, unsigned long& mapID);
	//这个地图的最大X和Y
	static int GetMapExtent( const AIMapInfo& mapInfo, TYPE_POS& maxX, TYPE_POS& maxY );
	//发送怪物威胁列表
	static int SendThreatList( const AIMapInfo& mapInfo, unsigned long monsterID, ThreatList& list);

	///是否为跟随玩家的NPC；
	static int IsEscortedNPC( const AIMapInfo& mapInfo, unsigned long monsterID, bool& bRet);

	///得到NPC跟随的玩家，如果为非跟随NPC，返回-1；
	static int GetEscortHost( const AIMapInfo& mapInfo, unsigned long monsterID, PlayerID& playerID );

	///判断NPC是否不调用AI, isDisableAI=true时不调用AI，isDisableAI=false时调用AI；
	static int IsMonsterDisableAI( const AIMapInfo& mapInfo, unsigned long monsterID, bool& isDisableAI );

	///查询跟随NPC所跟随玩家的移动信息
	static int GetFollowPlayerMoveInfo( const AIMapInfo& mapInfo, unsigned long monsterID, bool& isFollowing/*是否为跟随NPC，false的话，后续返回值皆无效*/
		, bool& isPlayerMoving, TYPE_POS& targetX, TYPE_POS& targetY, float& moveSpeed );

	///查询该怪物是否在移动
	static int IsMonsterMoving( const AIMapInfo& mapInfoD, unsigned long monsterID, bool& isMoving );

	///取怪物的size 距离判定用
	static int GetMonsterSize( const AIMapInfo& mapInfo, unsigned long monsterID, int& monsterSize );

	///取怪物类型；
	static int GetMonsterType( const AIMapInfo& mapInfo, unsigned long monsterID, unsigned long& monsterTypeID );

	///1 获取怪物的名字
	static const char* GetMonsterName( const AIMapInfo& mapInfo, TYPE_ID monsterID );

	///2 获取玩家姓名和HP百分比
	static int GetPlayerInfo( const PlayerID& reqID, const char*& playerName, float& hpPercent, char& sex, int& level );

	///8 设置玩家hp百分比
	static int SetPlayerHpPercent( const PlayerID& reqID, float hpPercent );	

	//设置怪物hp百分比
	static int SetMonsterHpPercent(  const AIMapInfo& mapInfo,  TYPE_ID monsterID, float hpPercent );	

	/////6 指定做的事actionType可以由葛清龙具体指定,到时候一一实现
	//static int GetScopeMonsters( const AIMapInfo& mapInfo,  TYPE_ID monsterID, ScopeMonsterInfo*& scopeMonstrArr , int& arrSize );	

	//static int GetScopePlayers( const AIMapInfo& mapInfo, TYPE_ID monsterID, ScopePlayerInfo*& scopePlayerArr, int& arrSize );

	///7 清楚所有怪物的BUFF或者DEBUFF效果
	static int ClearMonsterBuff( const AIMapInfo& mapInfo,  TYPE_ID monsterID, bool bBuff );	

	///请求在指定地图指定点召唤一个指定类型怪物，
	///    为了防止不正确的调用时机导致迭代器错误，本函数调用过程中不会立刻生成此怪物，而只是保存该请求，并在本函数返回后的某安全时刻刷此怪。
	static int CallMonsterReq( unsigned long callerID, unsigned long npcType, const AIMapInfo& mapInfo, TYPE_POS posX, TYPE_POS posY, unsigned int aiFlag = 0 );

	static int IsInNeighborBlock( const AIMapInfo& mapInfo,  TYPE_ID monsterID, const PlayerID& playerID, bool& bNeighbor );

	static int IsInNeighborBlock( const AIMapInfo& mapInfo,  TYPE_ID monsterID1, TYPE_ID monsterID2, bool& bNeighbor );

	static int SetMonsterMesh(  const AIMapInfo& mapInfo,  TYPE_ID monsterID, TYPE_ID meshID );	//

	static int SetMonsterSize(  const AIMapInfo& mapInfo,  TYPE_ID monsterID,float newSize );		//为标准大小

	static int GetMonsterAttackInterval(  const AIMapInfo& mapInfo,  TYPE_ID monsterID, int& attackInterval ); //攻击间隔

	static int MonsterPlaySound( const AIMapInfo& mapInfo,  TYPE_ID monsterID, const char* path, CHAT_TYPE chatType );

	static int GetMonsterLevel( const AIMapInfo& mapInfo,  TYPE_ID monsterID, unsigned long& level );

	///////////////////////////以下护送任务添加////////////////////////////////////////////////////////

	///设置玩家任务完成，0：设置成功，-1：设置失败，可能玩家身上无此任务；
	static int SetTaskFinish( const PlayerID& playerID, unsigned int taskID, bool isSubmit/*是否同时提交任务*/ );

	///设置玩家任务失败，0：设置成功，-1：设置失败，可能玩家身上无此任务；
	static int SetTaskFail( const PlayerID& playerID, unsigned int taskID );

	///设置玩家任务失败计时开始，0：设置成功，-1：设置失败，可能玩家身上无此任务；
	static int SetTaskFailTimeSt( const PlayerID& playerID, unsigned int taskID, unsigned int failTime );

	///设置玩家任务失败计时中止，0：设置成功，-1：设置失败，可能玩家身上无此任务；
	static int SetTaskFailTimeCancel( const PlayerID& playerID, unsigned int taskID );

	///设置怪物死亡，dieOrVanish(客户端表现)：true--死亡，vanish--消失，0：设置成功，-1：设置失败，可能此怪物不存在(warning)；
	static int SetMonsterDie( const AIMapInfo& mapInfo, TYPE_ID monsterID, bool dieOrVanish );

	///设置怪物无敌，0：设置成功，-1：设置失败，可能此怪物不存在(warning)；
	static int SetMonsterUndefeat( const AIMapInfo& mapInfo, TYPE_ID monsterID, bool isCanBeAttack/*是否可被攻击*/  );

	//查询玩家是否为魔头
	static int IsPlayerEvil( const PlayerID& playerID, bool& bEvil );

	//查询玩家身上的任务状态
	static int QueryPlayerTaskState( const PlayerID& playerID, unsigned int taskID );

	//玩家是否为流氓状态
	static int IsPlayerRogue( const PlayerID& playerID, bool& bRogue ); 

	//获取怪物的种族属性
	static int GetMonsterRaceAttribute( const AIMapInfo& mapInfo,  TYPE_ID monsterID, UINT& raceAttribute );

	//更改怪物的名字
	static int ChangeMonsterName( const AIMapInfo& mapInfo, const PlayerID& playerID, TYPE_ID monsterID, const char* name );

	//更改怪物的掉落集属性]
	static int ChangeMonsterIsDrop( const AIMapInfo& mapInfo,  TYPE_ID monsterID, bool isDropItem/*是否掉落包裹*/ );

	//获得玩家种族
	static int GetPlayerRace( const PlayerID& playerID, unsigned& usRace );

	//获得是否能被攻击
	static int GetIsCanBeAttack( const AIMapInfo& mapInfo,  TYPE_ID monsterID, bool& isCanBeAttack );

	//获得是否能被玩家攻击
	static int GetIsCanBeAttackPvc( const AIMapInfo& mapInfo, TYPE_ID monsterID, bool& isCanBeAttackPvc );
	
	//获得怪物的inhirentstand
	static int GetInherentStand( const AIMapInfo& mapInfo,  TYPE_ID monsterID, unsigned int& inherentStand );

	//NPC同步表现
	static int SendSpecialString( const AIMapInfo& mapInfo, const PlayerID& playerID, TYPE_ID monsterID, unsigned int stringType, const char* stringContent );
	//物件NPC机制
	static int SetItemNpcSeq( const AIMapInfo& mapInfo, unsigned int monsterID, unsigned int itemSeq, unsigned int status );
	//是否能通过 A点到B点
	static int IsCanPass( const AIMapInfo& mapInfo, float orgPosX, float orgPosY, float tgtPosX, float tgtPosY,  bool& isCanPass );
	//获取玩家的目标
	static int GetPlayerTargetPos( const PlayerID& playerID, float& targetPosX, float& targetPosY );
	//是否IsAggressive
	static int GetIsAggressive( const AIMapInfo& mapInfo, unsigned int monsterID, unsigned int& isAggressive );
	//TD 事件到达
	static int NotifyLevelMapInfo(const AIMapInfo& mapInfo, unsigned count);
	//获取monster的ken
	static int GetMonsterKen( const AIMapInfo& mapInfo, unsigned int monsterID, int& ken ); 
};

#endif //AI_SUP_CODE
