﻿#include "IBuff.h"
#include "G_MProc.h"
#include "monster/monster.h"

unsigned long CBuffManager::m_lBuffID;		//全局的BUFFID,只会递增
vector<IBuff*> CBuffManager::m_vecBuffs;  //BUFF


void StrCreateInfo::ResetCreateInfo()
{
	createType = CREATE_NULL;
	createID.dwPID = 0;
	createID.wGID = 0;
	pCreatePlayer = NULL;
	pCreateMonster = NULL;
}


void StrCreateInfo::SetCreateInfo( ECreateType type, const PlayerID& ID )
{
	createType = type;
	createID = ID;
}


ECreateType StrCreateInfo::GetCreateType()
{
	return createType;
}


void StrCreateInfo::UpdateCreateInfo()
{
	if( createType == CREATE_PLAYER )
	{
		
		CGateSrv* pGateSrv = CManG_MProc::FindProc( createID.wGID );
		if( NULL == pGateSrv )
		{
			ResetCreateInfo();
			return;
		}
		pCreatePlayer = pGateSrv->FindPlayer( createID.dwPID );
		if( NULL == pCreatePlayer )
		{
			ResetCreateInfo();
		}
	}else if( createType == CREATE_MONSTER ){
		pCreateMonster = CManMonster::FindMonster( createID.dwPID );
		if( NULL == pCreateMonster )
		{
			ResetCreateInfo();
		}
	}else{
		ResetCreateInfo();
	}
}


IBuff::IBuff()
{
	m_pBuffProp = NULL;  //该BUFF影响的属性
	m_bDelFlag = false;	//当此标志被至TRUE，下次轮询会回收
	m_nextDotTime = ACE_Time_Value::zero;
	m_lastUpdateTime = ACE_Time_Value::zero;
}


CPlayer* StrCreateInfo::GetCreatePlayer()
{
	return pCreatePlayer;
}

const PlayerID& StrCreateInfo::GetCreateID()
{
	return createID;
}


void IBuff::SetDelFlag()
{
	m_bDelFlag = true;
}


//获取删除标志量
bool IBuff::GetDelFlag()
{
	return m_bDelFlag;
}


void IBuff::SetUpdateTime( const ACE_Time_Value& curTime)
{
	m_lastUpdateTime = curTime;
}


const ACE_Time_Value& IBuff::GetUpdateTime()
{
	return m_lastUpdateTime;
}


void IBuff::SetBuffProp( CBuffProp* pBuffProp )
{
	m_pBuffProp = pBuffProp;
}

void IBuff::SetCreateInfo( const PlayerID& createID, ECreateType type )
{
	m_createInfo.SetCreateInfo( type, createID );
}


const ACE_Time_Value& IBuff::GetNextDotTime()
{
	return m_nextDotTime;
}


bool IBuff::IsDotType()
{
	if( m_pBuffProp )
		return m_pBuffProp->IsDotType();
	D_ERROR("IBuff::IsDotType() m_pBuffProp = NULL ");
	return false;
}



void IBuff::SetNextDotTime( const ACE_Time_Value& time )
{
	m_nextDotTime = time;
}



bool IBuff::IsBuffEnd( const ACE_Time_Value& currentTime )
{
	return (currentTime >= m_lastUpdateTime	+ m_leftTime);
}


void IBuff::SetLeftTime( const ACE_Time_Value& leftTime )
{
	m_leftTime = leftTime;
}


const ACE_Time_Value& IBuff::GetLeftTime()
{
	return m_leftTime;
}


int IBuff::GetLeftSecond()	//返回BUFF剩余的秒数
{
	return (int)( m_leftTime.sec() );
}


TYPE_ID IBuff::GetTypeID()
{
	if( m_pBuffProp )
		return m_pBuffProp->GetBuffID();
	D_ERROR("IBuff::GetTypeID() m_pBuffProp = NULL ");
	return 0;
}


TYPE_ID IBuff::GetMapSrvCreateID()
{
	return m_buffID.uiBuffID;
}



ECreateType IBuff::GetCreateType()
{
	return m_createInfo.GetCreateType();
}


BUFF_TYPE IBuff::GetBuffType()
{
	if( m_pBuffProp )
	{
		return m_pBuffProp->GetBuffPropType();
	}
	D_ERROR("IBuff::GetBuffType() m_pBuffProp = NULL ");
	return (BUFF_TYPE)0;
}


CBuffProp* IBuff::GetBuffProp()
{
	return m_pBuffProp;
}


const BuffID& IBuff::GetBuffID()
{
	return m_buffID;
}


void IBuff::SetBuffID( const BuffID& buffID )
{
	m_buffID = buffID;
}


int IBuff::GetBuffVal()
{
	if( m_pBuffProp )
	{
		return m_pBuffProp->GetBuffPropVal();
	} else {
		D_ERROR("IBuff::GetBuffVal() m_pBuffProp = NULL ");
	}
	return 0;
}


void IBuff::UpdateCreateInfo()
{
	m_createInfo.UpdateCreateInfo();
}


CPlayer* IBuff::GetCreatePlayer()
{
	return m_createInfo.GetCreatePlayer();
}


const PlayerID& IBuff::GetCreateID()
{
	return m_createInfo.GetCreateID();
}



void CBuffManager::TimeProc()
{
	TRY_BEGIN;
	if( m_vecBuffs.size() == 0)
	{
		return;
	}

	IBuff* pTmpBuff = NULL;
	for( vector<IBuff*>::iterator iter = m_vecBuffs.begin(); iter != m_vecBuffs.end(); )
	{
		pTmpBuff = *iter;
		if( pTmpBuff == NULL)
		{
			iter = m_vecBuffs.erase( iter );  //如果为NULL就直接删除
			continue;
		}
		//如果已经设置回收，则回收
		if( pTmpBuff->GetDelFlag() )
		{
			D_DEBUG("buff被回收\n");
			iter = m_vecBuffs.erase( iter );  
			pTmpBuff->Release();
		}else{
			pTmpBuff->Update();
			iter++;
		}
	}
	TRY_END;
}


void CBuffManager::InsertBuff(IBuff* pBuff)
{
	if ( NULL != pBuff )
	{
		m_vecBuffs.push_back( pBuff );
	}
}


unsigned long CBuffManager::GenerateBuffID()
{
	return m_lBuffID++;
}


IBuff* CBuffManager::FindBuff( unsigned long lBuffID)
{
	IBuff* pTempBuff = NULL;
	for(vector<IBuff*>::iterator iter = m_vecBuffs.begin(); iter != m_vecBuffs.end(); ++iter)
	{
		pTempBuff = *iter;
		if( NULL == pTempBuff)
		{
			continue;
		}
		if( lBuffID == pTempBuff->GetMapSrvCreateID() )
		{
			return pTempBuff;
		}
	}
	return NULL;
}
