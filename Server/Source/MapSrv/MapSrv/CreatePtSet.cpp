﻿#include "CreatePtSet.h"

map<unsigned int, CCreatePtSet*> CCreatePtSetManager::m_mapCreateSet;

CCreatePtSet::CCreatePtSet( unsigned int mapID, unsigned int setID ):m_mapID( mapID ), m_createSetID( setID )
{

}

CCreatePtSet::~CCreatePtSet(void)
{
}


void CCreatePtSet::PushCreatePt( const MuxPoint& pt )
{
	m_vecCreatePt.push_back( pt );
}


unsigned int CCreatePtSet::GetMapID()
{
	return m_mapID;
}


unsigned int CCreatePtSet::GetSetID()
{
	return m_createSetID;
}


unsigned int CCreatePtSet::GetVecSize()
{
	return (unsigned int)m_vecCreatePt.size();
}


bool CCreatePtSet::GetRandomPt( MuxPoint& randPt)
{
	if( m_vecCreatePt.empty() )
		return false;

	unsigned int ran = (unsigned int)RandNumber( 0, int(m_vecCreatePt.size() - 1) );
	randPt = m_vecCreatePt[ran];
	return true;
}


bool CCreatePtSetManager::IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
{
	if ( NULL == pEle || NULL == attName )
	{
		return false;
	}
	if ( NULL == pEle->GetAttr( attName ) )
	{
		D_ERROR( "CreateSetXML配置文件第%d条记录属性%s失败\n", nRecord, attName );
		return false;
	}
	return true;
}


bool CCreatePtSetManager::LoadFromXML( const char* szFileName )
{
	TRY_BEGIN;
	if ( NULL == szFileName )
	{
		D_ERROR( "CCreatePtSetManager::LoadFromXML，input file name NULL\n" );
		return false;
	}

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(szFileName))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}


	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;
	
	unsigned int rootLevel =  pRoot->Level();
	unsigned int mapID = 0;
	unsigned int setID = 0;
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( NULL == tmpEle )
			continue;

		if( tmpEle->Level() == rootLevel + 1 )
		{
			mapID = (unsigned int)atoi( tmpEle->GetAttr("MapID") );
			setID = (unsigned int)atoi( tmpEle->GetAttr("SetID") );
			CCreatePtSet* pSet = NEW CCreatePtSet( mapID, setID );
			if( FillSingleCreateSet( pSet, tmpEle, &xmlLoader ) )
			{
				m_mapCreateSet.insert( make_pair( setID, pSet ) );
			}else
			{
				delete pSet; pSet = NULL;
			}
		}
	}
	return true;
	TRY_END;
	return false;
}


bool CCreatePtSetManager::FillSingleCreateSet( CCreatePtSet* pSet, CElement* pEle, CXmlManager* pXmlLoader )
{
	if( NULL == pSet )
		return false;

	if( NULL == pEle )
		return false;

	if( NULL == pXmlLoader )
		return false;

	
	vector<CElement*> elementSet;
	pXmlLoader->FindChildElements( pEle, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}

	CElement* tmpEle = NULL;
	unsigned int posX = 0, posY = 0;
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( NULL == tmpEle )
			continue;

		posX = (unsigned int)atoi( tmpEle->GetAttr("x") );
		posY = (unsigned int)atoi( tmpEle->GetAttr("y") );
		pSet->PushCreatePt( MuxPoint(posX, posY) );
	}
	return true;
}


bool CCreatePtSetManager::Init()
{
	return LoadFromXML("config/createset.xml" );
}


void CCreatePtSetManager::Release()
{
	CCreatePtSet* pSet = NULL;
	for ( map<unsigned int, CCreatePtSet*>::iterator tmpiter = m_mapCreateSet.begin();
		tmpiter != m_mapCreateSet.end(); ++tmpiter )
	{
		pSet = tmpiter->second;
		if( NULL != pSet )
		{
			delete pSet; pSet = NULL;
		}
	}
	m_mapCreateSet.clear();
}


CCreatePtSet* CCreatePtSetManager::FindCreateSet( unsigned int createSetId )
{
	map<unsigned int, CCreatePtSet*>::iterator iter = m_mapCreateSet.find( createSetId );
	if( iter != m_mapCreateSet.end() )
	{
		return iter->second;
	}
	return NULL;
}