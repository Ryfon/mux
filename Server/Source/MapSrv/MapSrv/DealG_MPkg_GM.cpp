﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: hyn
* 完成日期: 2007.12.17
*
*/

#include "DealG_MPkg.h"

#include "MuxMap/mannormalmap.h"
#include "GMCmdManager.h"

bool CDealG_MPkg::OnGmCmdRequest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	TRY_BEGIN;

	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	PKG_CONVERT( GMGmCmdRequest );

	std::string strCmd;
	vector<string> vecCmdArgs;
	if( !CGMCmdManager::IsGmCmd( pMsg->strChat, strCmd, vecCmdArgs ) )
	{
		D_ERROR("CDealG_MPkg::OnGmCmdRequest 发来GM命令请求却不是GM命令\n");
		MGGmCmdResult errMsg;
		errMsg.requestPlayerID = pMsg->requestPlayerID;
		errMsg.resultCode = 0;
		MsgToPut* pNewMsg = CreateSrvPkg( MGGmCmdResult, pOwner, errMsg );
		pOwner->SendPkgToGateServer( pNewMsg );
		return false;
	}

	CGMCmdManager::DealReqGmCmd( strCmd.c_str(), vecCmdArgs, pMsg->requestPlayerID );

	return true;
	TRY_END;
	return false;
}