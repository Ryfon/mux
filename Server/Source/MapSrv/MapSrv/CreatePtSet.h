﻿
#ifndef CREATEPTSET_H
#define CREATEPTSET_H

#include <vector>
#include <map>
#include "Utility.h"
#include "MuxMap/MapCommonHead.h"
#include "../../Base/XmlManager.h"
using namespace std;

class CCreatePtSet
{
public:
	CCreatePtSet( unsigned int mapID, unsigned int setID );
	~CCreatePtSet(void);

public:
	void PushCreatePt( const MuxPoint& pt );

	unsigned int GetMapID();
	unsigned int GetSetID();
	unsigned int GetVecSize();
	bool GetRandomPt( MuxPoint& pt );

private:
	unsigned int m_mapID;				//所属的mapID 
	unsigned int m_createSetID;			//生成怪的集合
	vector<MuxPoint> m_vecCreatePt;		//随即怪的集合
};



class CCreatePtSetManager
{
private:
	CCreatePtSetManager();
	~CCreatePtSetManager();

public:
	static bool Init();
	static void Release();

	static CCreatePtSet* FindCreateSet( unsigned int createSetId );

private:
	static bool LoadFromXML( const char* szFileName );

	static bool FillSingleCreateSet( CCreatePtSet* pSet, CElement* pEle, CXmlManager* xmlLoader );

	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord );


private:
	static map<unsigned int, CCreatePtSet*> m_mapCreateSet;
};




#endif //CREATEPTSET_H
