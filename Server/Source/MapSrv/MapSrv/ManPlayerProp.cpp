﻿#include "ManPlayerProp.h"
#include "XmlEventHandler.h"
#include "XmlManager.h"



CClassLevelProp::CClassLevelProp(void)
{
}

CClassLevelProp::~CClassLevelProp(void)
{
	ReleaseManedObj();
}




//读取某个职业的属性配置表
bool CClassLevelProp::LoadFromXML(const char* szFileName)
{
	TRY_BEGIN;

	if ( NULL == szFileName )
	{
		return false;
	}
	CXmlManager xmlLoader;
	if(!xmlLoader.Load(szFileName))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;
	//CElement* tmpPropEle = NULL;
	CLevelProp tmpLevelProp;
	
	int nCurPos = 0;
	unsigned int rootLevel =  pRoot->Level();
	unsigned long lLastLevelExp = 0ul;
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			nCurPos++;
			if( ! IsXMLValValid(tmpEle, "Class", nCurPos) )
			{
				return false;
			}
			m_lClassID = atoi( tmpEle->GetAttr("Class") );
			if( ! IsXMLValValid(tmpEle, "Level", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.m_lLevel = atoi( tmpEle->GetAttr("Level"));
			if( ! IsXMLValValid(tmpEle, "HpMax", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.lMaxHP = atoi( tmpEle->GetAttr("HpMax"));
			if( ! IsXMLValValid(tmpEle, "MpMax", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.lMaxMP = atoi( tmpEle->GetAttr("MpMax"));
			if( ! IsXMLValValid(tmpEle, "Str", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.nStrength = atoi( tmpEle->GetAttr("Str"));
			if( ! IsXMLValValid(tmpEle, "Agi", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.nAgility = atoi( tmpEle->GetAttr("Agi"));
			if( ! IsXMLValValid(tmpEle, "Int", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.nIntelligence = atoi( tmpEle->GetAttr("Int"));
			if( ! IsXMLValValid(tmpEle, "PhysicsAttack", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.m_nPhyAttack = atoi( tmpEle->GetAttr("PhysicsAttack"));
			if( ! IsXMLValValid(tmpEle, "MagicAttack", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.m_nMagAttack = atoi( tmpEle->GetAttr("MagicAttack"));
			if( ! IsXMLValValid(tmpEle, "PhysicsDefend", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.m_nPhyDefend = atoi( tmpEle->GetAttr("PhysicsDefend"));
			if( ! IsXMLValValid(tmpEle, "MagicDefend", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.m_nMagDefend = atoi( tmpEle->GetAttr("MagicDefend"));
			if( ! IsXMLValValid(tmpEle, "PhysicsHit", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.m_nPhyHit = atoi( tmpEle->GetAttr("PhysicsHit"));
			if( ! IsXMLValValid(tmpEle, "MagicHit", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.m_nMagHit = atoi( tmpEle->GetAttr("MagicHit"));
			if( ! IsXMLValValid(tmpEle, "Exp", nCurPos) )
			{
				return false;
			}
			tmpLevelProp.lNextLevelExp = atoi( tmpEle->GetAttr("Exp"));
	   	 	tmpLevelProp.lLastLevelExp = lLastLevelExp;
			if( tmpLevelProp.lNextLevelExp == 0)
			{
				m_usMaxLevel = (unsigned short)tmpLevelProp.m_lLevel;
			}
		} 
		AddObject(tmpLevelProp); //加入配置类
		//为下一级作准备
		lLastLevelExp = tmpLevelProp.lNextLevelExp;
	}

	return true;
	TRY_END;
	return false;
}


//所有职业的属性类的初始配置
bool CManPlayerProp::Init()
{
	CClassLevelProp* tmpLevelConfig = NEW CClassLevelProp;
	if( !tmpLevelConfig->LoadFromXML("config/character/assassin/assassin_prop.xml") )
	{
		D_ERROR("读取刺客配置文件出错或者找不到配置文件\n");
		return false;
	}
	AddObject( tmpLevelConfig );
	D_DEBUG("读取刺客职业属性成功\n");
	
	tmpLevelConfig = NEW CClassLevelProp;	
	if( !tmpLevelConfig->LoadFromXML("config/character/warrior/warrior_prop.xml") )
	{
		D_ERROR("读取剑士配置文件出错或者找不到配置文件\n");
		return false;
	}
	AddObject( tmpLevelConfig );
	D_DEBUG("读取剑士职业属性成功\n");
		
	tmpLevelConfig = NEW CClassLevelProp;	
	if( !tmpLevelConfig->LoadFromXML("config/character/mage/mage_prop.xml"))
	{
		D_ERROR("读取魔法师配置文件出错或者找不到配置文件\n");
		return false;
	}
	AddObject( tmpLevelConfig );
	D_DEBUG("读取魔法师属性职业属性成功\n");
	
	tmpLevelConfig = NEW CClassLevelProp;	
	if( !tmpLevelConfig->LoadFromXML("config/character/paladin/paladin_prop.xml") )
	{
		D_ERROR("读取圣骑士配置文件出错或者找不到配置文件\n");
		return false;
	}
	AddObject( tmpLevelConfig );
	D_DEBUG("读取圣骑士属性职业属性成功\n");

	tmpLevelConfig = NEW CClassLevelProp;	
	if( !tmpLevelConfig->LoadFromXML("config/character/blader/blader_prop.xml") )
	{
		D_ERROR("读取魔剑士配置文件出错或者找不到配置文件\n");
		return false;
	}
	AddObject( tmpLevelConfig );
	D_DEBUG("读取魔剑士属性职业属性成功\n");

	tmpLevelConfig = NEW CClassLevelProp;	
	if( !tmpLevelConfig->LoadFromXML("config/character/hunter/hunter_prop.xml") )
	{
		D_ERROR("读取猎人配置文件出错或者找不到配置文件\n");
		return false;
	}
	AddObject( tmpLevelConfig );
	D_DEBUG("读取猎人属性职业属性成功\n");

	return true;
}

const std::map<unsigned long, CClassLevelProp*>& CManPlayerProp::GetAllClassLevelProp()
{
	return GetMapObject();
}
