﻿/********************************************************************
	created:	2008/07/22
	created:	22:7:2008   11:02
	file:		GroupTeamManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef GROUPTEAMMANAGER_H
#define GROUPTEAMMANAGER_H


#include <map>

#include "../../Base/aceall.h"
#include "GroupTeam.h"

class GroupTeamManager
{
	friend class ACE_Singleton<GroupTeamManager, ACE_Null_Mutex>;

private:
	GroupTeamManager( const GroupTeamManager& );  //屏蔽这两个操作；
	GroupTeamManager& operator = ( const GroupTeamManager& );//屏蔽这两个操作；

public:
	GroupTeamManager(){}

	virtual ~GroupTeamManager();

public:
	CGroupTeam* CreateGroupTeam( unsigned long teamID, ITEM_SHAREMODE itemShareMode,EXPERIENCE_SHAREMODE expreShareMode );

	CGroupTeam* GetGroupTeamByID( unsigned long teamID );

	void RecoverGroupTeam( CGroupTeam* pGroupTeam );

	//当接到组队的详细信息时
	void OnRecvTeamDetialInfo( unsigned teamID,const PlayerID * playerArr, 
		const int* memberSeqArr, 
		int count, 
		ITEM_SHAREMODE itemShareMode,
		EXPERIENCE_SHAREMODE expreShareMode,
		ROLLITEM_MODE itemRollLevel,
		int leaderIndex
		);
	////当接到玩家进入组队时
	//void OnRecvPlayerEnterGroupTeam( unsigned teamID, const PlayerID& playerID ,int memberSeq );
	////当接到玩家退出组队时
	//void OnRecvPlayerLeaveGroupTeam( unsigned teamID, const PlayerID& playerID, int memberSeq );
	//当接到玩家同意Roll点的回应时
	void OnRecvPlayerIsAgreeRollItem( unsigned rollItemID ,const PlayerID& playerID, bool isAgree );
	//当改变组队的队长时
	void OnRecvChangeTeamLeader( unsigned teamID, const PlayerID& playerID );
	//当改变组队的道具分配模式时
	void OnRecvChangeTeamItemShareMode( unsigned teamID,ITEM_SHAREMODE shareMode );
	//当改变组队的经验分配模式时
	void OnRecvChangeTeamExpreShareMode( unsigned teamID, EXPERIENCE_SHAREMODE expreShareMode );
	//改变组队的Roll物品等级
	void OnRecvChangeTeamRollItemLevel( unsigned teamID, unsigned int rollItemLevel );

	void OnRecvKickPlayerOnTeam( unsigned teamID, const PlayerID& playerID );

	void OnRecvDisBandTeam( unsigned teamID );

	void OnRecvTeamPlayerGetNewItem( unsigned int teamID, unsigned int itemTypeID, const PlayerID& pickPlayerID );

public:
	//当玩家组队状态切换地图时
	void OnGroupPlayerSwichMap( CPlayer* pMember );
	
	//当玩家组队状态离开地图时
	void OnGroupPlayerExitMap( CPlayer* pMember );

private:
	std::map<unsigned long,CGroupTeam *> mGroupTeamMan;
};


typedef ACE_Singleton<GroupTeamManager, ACE_Null_Mutex> GroupTeamManagerSingle;

#endif

