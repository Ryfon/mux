﻿#pragma once
#include "IBuff.h"
#include "tcache/tcache.h"

class CPlayer;

class CScampBuff :
	public IBuff
{
public:
	CScampBuff(void);
	~CScampBuff(void);

	
	PoolFlagDefine()
	{
		m_pOwner = NULL;
		PoolInit();
	};

public:
	virtual void Update();
	
	virtual void BuffStart();

	virtual void BuffEnd();

	virtual void DotHotProcess(){};

	virtual void Release();

public:
	void Initialize( CPlayer* pOwner, int seconds );

private:
	CPlayer* m_pOwner; 
};
