﻿#include "ProfileAnalyzeDeclare.h"

PROFILE_INIT;

//申明标记为A的性能分析逻辑
DECLARE_PROFILE_MARK( A );
DECLARE_PROFILE_MARK_STEP( A,ExecuteUpdate );
DECLARE_PROFILE_MARK_STEP( A,ExecuteItemLevelUp );
DECLARE_PROFILE_MARK_STEP( A,ReadLevelCri );

