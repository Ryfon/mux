﻿/********************************************************************
	created:	2008/07/21
	created:	21:7:2008   15:35
	file:		GroupTeam.h
	author:		管传淇
	
	purpose:	
*********************************************************************/


#ifndef GROUPITEM_H_
#define GROUPITEM_H_

#include <vector>
#include <assert.h>

#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/itemgloal.h"
using namespace ITEM_GLOBAL;
class CPlayer;

class TeamMemberControl
{
private:
	TeamMemberControl& operator=( const TeamMemberControl& );
	TeamMemberControl( const TeamMemberControl& );

public:

	TeamMemberControl(){}

	~TeamMemberControl(){}

	void Init( unsigned int mapid )
	{
		mMapID = mapid;
		mPickItemIndex = 0;
		mTurnPickVec.clear();
	}

	void PushTurnPlayer( CPlayer* pPlayer );

	//轮流拾取本次轮到的玩家
	CPlayer* NowTurnPlayer();

	//移除玩家 
	bool RemoveTurnPlayer( const PlayerID&  playerID );

	unsigned int GetOwnerMap() { return mMapID; }

	void ClearTurnPickPlayer() {  mTurnPickVec.clear(); /*mPickItemIndex = 0;*/ } 

	void ResetPickItemIndex() { mPickItemIndex = 0; }

	const std::vector<CPlayer *>& GetTurnPickerVec() { return mTurnPickVec; }

	void TeamDisBandNotify();

	//释放该队伍中所有人的组队状态
	void ReleaseAllPlayerGroupState();

	//释放组队中玩家的组队状态
	void ReleasePlayerGroupState( CPlayer* pPlayer );

	//是否还有玩家存在
	bool HaveExistPlayer() { return !mTurnPickVec.empty(); }

	//该玩家是否在这个控制器中
	bool IsPlayerExist( CPlayer* pPlayer );

	bool CheckHaveTask( unsigned int taskID );

	void TeamMemberGetNewItem( const PlayerID& pickPlayerID, unsigned int itemTypeID );

private:
	unsigned int mMapID;
	unsigned int mPickItemIndex;
	std::vector<CPlayer *> mTurnPickVec;
};


class CGroupTeam
{
public:
	typedef struct StrTeamMember
	{
		StrTeamMember() 
		{
			playerID.wGID = 0;
			playerID.dwPID = 0;
		}

		PlayerID playerID;

		StrTeamMember& operator=( const StrTeamMember& tm )
		{
			this->playerID = tm.playerID;
			return *this;
		};
	}TeamPlayer;

public:
	CGroupTeam( unsigned long teamID, ITEM_SHAREMODE itemShareMode, EXPERIENCE_SHAREMODE expreShareMode ) :
	  mGroupTeamID(teamID),mLeaderIndex(0),mItemShareMode(itemShareMode),mExpreShareMode(expreShareMode),mRollItemLevel(E_ORANGE)
	  {
		  mGroupMemberVec.clear();//成员
		  mGroupTeamMemberControlVec.clear();//
	  };

	~CGroupTeam()
	{
		//如果有玩家管理器，则删除对应的内存
		if ( !mGroupTeamMemberControlVec.empty() )
		{
			for (std::vector<TeamMemberControl *>::iterator lter = mGroupTeamMemberControlVec.begin();
				lter != mGroupTeamMemberControlVec.end(); 
				++lter )
			{
				if ( NULL != (*lter) )
				{
					delete *lter;
				}
			}
			mGroupTeamMemberControlVec.clear();
		}

	};

	//发送组员消息
	void SendTeamMemberDetailInfo( MGTeamMemberDetailInfoUpdate* pDetialMsg, CPlayer* exceptPlayer  ); 
	//获取组队ID号
	unsigned long GetGroupTeamID() { return  mGroupTeamID; }
	//获取组队队长在队伍中的编号
	unsigned char GetGroupTeamLeaderIndex() { return mLeaderIndex; }
	//玩家切换地图
	bool OnSwitchMapExitGroupTeam( CPlayer* pPlayer, unsigned int mapID );
	//玩家退出组队
	void ExitGroupTeam( const PlayerID& playerID );
	//设置组队队长中玩家队长的索引
	void SetGroupTeamLeaderIndex( const PlayerID& playerID );
	//组队队长是否在
	bool GetGroupTeamLeaderID( PlayerID& playerID );
	//改变组队玩家在客户端的显示
	void NotifyChangeTeamState( const PlayerID& changePlayerID, char state );

	//接受到组队成员的信息
	void RecvGroupMemberInfo(
		const PlayerID * playerArr, 
		const int* memberSeqArr,
		int count,
		int leadIndex
		);

	//更新组队信息
	void UpdateGroupTeamInfo( 
		ITEM_SHAREMODE itemShareMode, //物品分配方式
		EXPERIENCE_SHAREMODE expreShareMode,//经验分配方式
		ROLLITEM_MODE rollItemLevel //RollItem的等级
		);
	//这个组队对象所管理的地图上的组队成员是否为空
	bool IsMemberEmpty();
	//解散组队队伍
	void OnDisBandGroupTeam();
	//判断玩家是否已经存在了组队
	bool IsPlayerExists( const PlayerID& playerID );
	//获取组队物品分享规则
	ITEM_SHAREMODE GetGroupItemShareMode() { return mItemShareMode; }
	//获取组队的经验分享模式
	EXPERIENCE_SHAREMODE GetGroupTeamExpreShareMode() { return mExpreShareMode; }
	//组队经验分享计算
	void ShareExpreOnGroupTeam( CPlayer* expreMainPlayer, long expre,int monsterLevel, bool isExpCritical, bool isBossMonster  );
	//踢出队伍
	void KickPlayerOnTeam( const PlayerID& playerID );
	//获取Roll道具的等级
	ITEM_MASSLEVEL GetRollItemMassLevel() { return mRollItemLevel; }
	//设置Roll道具的等级
	void SetRollItemMassLevel( ITEM_MASSLEVEL massLevel ) { mRollItemLevel = massLevel; }
	//获得了道具，通知其他玩家
	void GetNewItemNoticeOtherMembers( CPlayer* pMember,const ItemInfo_i& itemEle );
	//获取组队成员的列表
	std::vector<TeamPlayer>& GetTeamMemberVec() { return mGroupMemberVec; }
	//获取在统一地图的玩家
	unsigned int GetTeamMemberInUnSafeArea( unsigned int mapID, std::vector<CPlayer *>& playerVec );

	unsigned int GetTeamMemberOnTheMap( unsigned int mapID, std::vector<CPlayer *>& playerVec );

	TeamMemberControl* GetTeamMemberControl( unsigned int mapID ); 

	TeamMemberControl* NewTeamMemberControl( unsigned int mapID );

	void OnChangePickItemMode();

	void ClearTeamMemberControl();

	//检查组队成员是否拥有编号为taskID的任务
	bool CheckTeamMemberHaveTask( unsigned int taskID,unsigned int mapID );
	//当组队玩家的杀怪计数器改变时
	void TeamPlayerKillNPCEvent(  const PlayerID& killNPCPlayer, unsigned int mapID,unsigned int reasonObjID/*事件相关物体类型*/, unsigned int reasonObjNum/*事件相关物体数量*/  );

	void NoticeTeamMemberGetNewItem( const PlayerID& pickPlayerID, unsigned int itemTypeID );

private:
	std::vector<TeamPlayer> mGroupMemberVec;//成员
	std::vector<TeamMemberControl *> mGroupTeamMemberControlVec;//
	unsigned long mGroupTeamID;//组队ID号
	unsigned char mLeaderIndex;//组队队长在队伍中的索引
	ITEM_SHAREMODE mItemShareMode;//道具分配方式
	EXPERIENCE_SHAREMODE mExpreShareMode;//经验分享模式
	ITEM_MASSLEVEL  mRollItemLevel;// RollItem的等级
	PlayerID mLeaderPlayerID;//队长的PlayerID号


public:
	//更新队伍的道具共享方式
	void UpdateGroupTeamItemShareMode( ITEM_SHAREMODE itemShareMode );

	//更新在轮流模式下能拾取道具的人,返回CPlayer的指针
	CPlayer* IncPickItemIndexOnTurnMode( unsigned mapID );
	//更新队伍的经验分享方式
	void UpdateGroupTeamExpreShareMode( EXPERIENCE_SHAREMODE expreShareMode )
	{
		mExpreShareMode = expreShareMode;
	}

	//更新队伍的RollITEM的物品等级
	void UpdateGroupTeamRollItemLevel( ITEM_MASSLEVEL massLevel )
	{
		if( massLevel > E_WHITE )
			mRollItemLevel = massLevel;
	}

	//更新队长的索引
	void UpdateGroupTeamLeaderIndex( unsigned char leaderIndex )
	{
		mLeaderIndex = leaderIndex;
	}
};

#endif
