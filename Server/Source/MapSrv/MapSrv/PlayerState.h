﻿/** @file PlayerState.h
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-6-24
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef PLAYERSTATE_H
#define PLAYERSTATE_H

//#include "../../Base/PkgProc/SrvProtocol.h"
//using namespace MUX_PROTO;
#include "GroupTeam.h"
#include "../../Base/Utility.h"

class CPlayerStateManager;
class RideTeam;
class CGroupTeam;

class IPlayerState
{
public:
	IPlayerState():mpStateMan(NULL){}

	virtual ~IPlayerState(){};

public:
	//进入状态开始时
	virtual void OnBegin() = 0 ;

	//状态将要结束时
	virtual void OnEnd() = 0;

	//状态是否被激活
	virtual bool IsActive() = 0;

	//附加状态所跟踪的人
	void SetAttachPlayer( CPlayerStateManager* lStateMan ){ mpStateMan = lStateMan ; }

private:
	IPlayerState& operator=( const IPlayerState& );
	IPlayerState( const IPlayerState& );

protected:
	CPlayerStateManager* mpStateMan;
};

class CNormalState:public IPlayerState
{
public:
	CNormalState():m_bActive(false){}
	virtual ~CNormalState(){}
	virtual void OnBegin(){ m_bActive = true; }

	virtual void OnEnd(){ m_bActive = false; };

	virtual bool IsActive() { return m_bActive; }
private:
	bool m_bActive;
};

struct InviteItem
{
	unsigned int playerUID;
	InviteItem():playerUID(0){}
};

struct BeInviteItem
{
	PlayerID playerID;
	unsigned int playerUID;

	BeInviteItem()
	{
		playerID.dwPID = playerID.wGID = 0;
		playerUID = 0;
	}
};

//骑乘状态
class CRideState:public IPlayerState
{
public:
	enum RIDETYPE
	{
		E_LEADER = 0x0,//队长
		E_ASSIST,//队员
		E_RIDETYPE_MAX,
	};

	enum INVITETYPE
	{
		E_INVITE = 0x0,//邀请
		E_BEINVITE,//被邀请
		E_NULL_INVITE,//没有处于邀请状态
		E_MAXINVITE,
	};

public:
	virtual void OnBegin();

	virtual void OnEnd();

	virtual bool IsActive() { return m_bActive; }

public:

	CRideState():m_bActive(false),
		m_rideItemUID(0),
		m_rideType(E_RIDETYPE_MAX),
		m_rideItemState(E_RIDESTATE_MAX),
		m_pTeam(NULL),
		m_rideItemType(0)
	{
	}

	~ CRideState()
	{
	}

	//重置
	void ReSet();

	//获取骑乘队伍的编号
	long GetRideTeamID();

	//设置骑乘队伍所使用道具在服务器的唯一编号
	void SetRideItemID( long itemUID ){ m_rideItemUID = itemUID ;}

	//获取该骑乘道具在服务器的唯一编号
	long GetRideItemID() { return m_rideItemUID; }

	//设置玩家骑乘队员的类型
	void SetPlayerRideType( RIDETYPE rideType ); 

	//获取玩家队员的类型
	RIDETYPE GetPlayerRideType() {  return m_rideType; }
	//获取玩家骑乘的类型
	ERIDESTATE GetPlayerRideState() { return m_rideItemState; }
	//设置骑乘队伍
	void SetRideTeam( RideTeam* pTeam );
	//获取骑乘队伍指针
	RideTeam* GetRideTeam() { return m_pTeam; }
	//设置玩家骑乘的状态 是否为装备,道具等
	void SetRideItemState( ERIDESTATE rideState ) { m_rideItemState = rideState;  }
	//设置骑乘状态是否激活
	void SetActive( bool isActive ) { m_bActive = isActive ;}
	//设置消费道具在包裹中的编号
	void SetRideItemType( long rideType )
	{ 
		if ( E_RIDE_CONSUME != m_rideItemState )
		{
			D_ERROR( "SetRideItemType, E_RIDE_CONSUME != m_rideItemState\n" );
		}
		m_rideItemType = rideType; 
	} 
	//是否处于骑乘邀请状态
	bool IsInviteState() { return !mInviteTargetPlayerVec.empty() || !mBeInvitelaunchPlayerVec.empty(); }

	//清空属于该玩家的邀请和被邀请骑乘列表
	void ClearInvitePlayerList();

	//将邀请玩家加入自己的邀请列表
	void PushInviteTargetPlayerList( unsigned int playerUID );

	//被邀请玩家将邀请自己的玩家加入自己的被邀请列表
	void PushInviteLaunchPlayerList( const PlayerID& launchPlayerID, unsigned int playerUID );

	//玩家是否邀请了playerUID所指的玩家
	bool IsInvitingTargetPlayer( unsigned int playerUID );

	//从邀请列表中将playerUID删除
	void RemoveInvitePlayer( unsigned int playerUID );

	//将被邀请列表中将playerUID所指的玩家删除
	void RemoveInviteLaunchPlayer( unsigned int playerUID );

	//获取所有队员的ID号
	void GetTeamAllMemberID( PlayerID& leadid, PlayerID& memberid );

	//骑乘队伍是否满了
	bool IsRideTeamFull();

	//获取骑乘道具的类型
	unsigned long GetRideItemType() { return m_rideItemType; }

	//重新设置骑乘邀请的状态
	void ResetRideInviteState();

private:
	CRideState& operator=( const CRideState& );
	CRideState( const CRideState& );

private:
	bool m_bActive;//是否被激活
	long m_rideItemUID;//所骑乘的道具ID号
	RIDETYPE  m_rideType;//玩家的在骑乘队伍中的身份
	ERIDESTATE m_rideItemState;//玩家骑乘道具的类型
	RideTeam* m_pTeam;//骑乘队伍指针
	unsigned long   m_rideItemType;

private:
	std::vector<InviteItem> mInviteTargetPlayerVec;
	std::vector<BeInviteItem>   mBeInvitelaunchPlayerVec;
};


//组队状态
class CGroupState:public IPlayerState
{
public:
	CGroupState():m_bActive(false),m_pGroupTeam(NULL){}

	~CGroupState(){};

	void ReSet()
	{
		m_bActive = false;
		m_pGroupTeam = NULL;
	}

public:
	virtual void OnBegin();

	virtual void OnEnd();

	virtual bool IsActive() { return m_bActive; }

public:
	//获取组队的ID号
	unsigned long GetGroupTeamID();
	//获取组队指针
	CGroupTeam* GetGroupTeam();
	//设置所属的组队指针
	void SetOwnerGroupTeam( CGroupTeam* pGroupTeam );

private:
	CGroupState( const CGroupState& );
	CGroupState& operator=( const CGroupState& );


private:
	bool m_bActive;//是否被激活
	CGroupTeam* m_pGroupTeam;
};


#endif


