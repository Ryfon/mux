﻿#pragma once

#include "MuxBuffEle.h"

class CMonster;

class CMonsterBuffs
{
#define MAX_BUFF_SIZE 32 //最大BUFF数量

public:
	CMonsterBuffs(void);
	~CMonsterBuffs(void);

	void AttachOwner( CMonster* pMonster )	//设置宿主
	{
		m_pOwner = pMonster;
	}
	
	//设置减速标志
	void SetSpeedDown( bool bFlag );
	bool IsSpeedDown();

	//设置加速标志
	void SetSpeedUp( bool bFlag );
	bool IsSpeedUp();

	//增加SP速度
	bool IsSpPowerSpeedUp();
	void SetSpPowerSpeedUp( bool bFlag );

	//移动免疫
	bool IsMoveImmunity();
	void SetMoveImmunity( bool bFlag );

	void SetAbsorbDamage( unsigned int damage );	//设置吸收伤害量

	bool AbsorbCheck( unsigned int& damage );
	
	bool IsHaveBuff()
	{
		return (m_buffNum != 0);
	}

	inline void AddBuffNum()
	{
		m_buffNum++;
	}

	inline void SubBuffNum()
	{
		m_buffNum--;
	}

	//死亡时的BUFF处理
	void OnBuffDie();

	bool CreateMuxBuff( MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID );

	void BuffProcess();

	bool IsPosValid( unsigned int index )
	{
		if( index >= ARRAY_SIZE( m_pMonsterBuffs ) )
		{
			D_ERROR("IsBuffValid index超过m_pPlayerBuffs size");
			return false;
		}

		return (m_buffPosFlag>>index)&0x01;
	}

	void SetPosValid( unsigned int index )
	{
		if( index >= ARRAY_SIZE( m_pMonsterBuffs ) )
		{
			D_ERROR("SetPosValid index超过m_pPlayerBuffs size");
			return;
		}

		m_buffPosFlag |= 0x01<<index;
	}
	
	void SetPosInValid( unsigned int index )
	{
		if( index >= ARRAY_SIZE( m_pMonsterBuffs ) )
		{
			D_ERROR("SetPosInValid index超过m_pPlayerBuffs size");
			return;
		}

		m_buffPosFlag &= ~(0x01<<index);
	}

	bool FindInvalidIndex( unsigned int& index)
	{
		for( unsigned int i=0; i < ARRAY_SIZE(m_pMonsterBuffs); ++i )
		{
			if( !IsPosValid(i) )
			{
				index = i;
				return true;
			}
		}
		return false;
	}



	bool MonsterBuffStartWrapper( CMuxBuffEle* pBuffEle, unsigned int buffIndex, MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID );

	bool MonsterBuffEndWrapper( CMuxBuffEle* pBuffEle, unsigned int buffIndex );

	//解除性BUFF
	bool RelieveBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve/*是否只是探测可能性*/ );


	const PlayerInfo_5_Buffers& GetBuffData()
	{
		return m_buffData;
	}
	
	//更新怪物buff信息
	void UpdateMonsterBuffData();
	
private:
	CMonster* m_pOwner;
	CMuxBuffEle m_pMonsterBuffs[MAX_BUFF_SIZE];
	unsigned int m_buffPosFlag;
	unsigned int m_buffNum;
	PlayerInfo_5_Buffers m_buffData;		//怪物的buff信息
};
