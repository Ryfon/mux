﻿
#ifndef MUXBUFF_ELE_H
#define MUXBUFF_ELE_H

#include "MuxSkillProp.h"
#include "Utility.h"

class CPlayer;
class CMonster;

struct HpMpUpdateInfo
{
	bool isHpMpBuff;				//是否是HpMp更新的buff
	int  hpUpdateVal;				//绝对值
	int  mpUpdateVal;				//绝对值
	float perHp;					//百分比
	float perMp;					//百分比
	unsigned int updateInterval;	//更新的间隔

	HpMpUpdateInfo()
	{
		Reset();
	}

	void Reset()
	{
		isHpMpBuff = false;
		hpUpdateVal = 0;				//绝对值
		mpUpdateVal = 0;				//绝对值
		perHp = 0.;					//百分比
		perMp = 0.f;					//百分比
		updateInterval = 0;			//更新的间隔
	}
};

class CMuxBuffEle
{
public:
	CMuxBuffEle();
	~CMuxBuffEle();

private:
	//Buff的初始化
	bool BuffInit( MuxBuffProp* pMuxBuffProp, unsigned int buffTime, const PlayerID& createID );
	//是否是更新hpmp的buff
	bool IsHpMpBuff();
	
	//dot的间隔
	unsigned int GetHpMpUpdateInterval();
	//初始化m_hpmpUpdateInfo
	void InitHpMpUpdateInfo( MuxBuffProp* pBuffProp, const ACE_Time_Value& curTime );

public:
	void OnPlayerHpMpUpdate( CPlayer* pTargetPlayer, unsigned int buffIndex );
	void OnMonsterHpMpUpdate( CMonster* pTargetMonster, unsigned int buffIndex );

	//该ELE是否生效
	bool IsValid()
	{
		return m_bValid;
	}

	//返回true说明buff已结束
	bool OnPlayerBuffUpdate( CPlayer* pPlayer, unsigned int buffIndex );
	bool OnPlayerBuffStart( CPlayer* pPlayer, unsigned int buffIndex,  MuxBuffProp* pMuxBuffProp, unsigned int buffTime, const PlayerID& createID, bool isNotiPlayer = true );
	void OnPlayerBuffEnd( CPlayer* pPlayer, unsigned int buffIndex, bool isNotiPlayer = true );
	void OnPlayerBuffChangeStatus( CPlayer* pPlayer, unsigned int buffIndex, MuxBuffProp* pBuffProp,  unsigned int buffLevel, bool isBuffUpgrade, const PlayerID& creareID );	//改变buff的时间 等级会调用

	bool OnMonsterBuffUpdate( CMonster* pMonster, unsigned int buffIndex );
	bool OnMonsterBuffStart( CMonster* pMonster, unsigned int buffIndex,  MuxBuffProp* pMuxBuffProp, unsigned int buffTime, const PlayerID& createID, bool isNotiPlayer = true );
	void OnMonsterBuffEnd( CMonster* pMonster, unsigned int buffIndex, bool isNotiPlayer = true );
	void OnMonsterBuffChangeStatus( CMonster* pMonster, unsigned int buffIndex, MuxBuffProp* pBuffProp, unsigned int buffLevel, bool isBuffUpgrade, const PlayerID& createID );

	unsigned int GetPushIndex()
	{
		if( m_pBuffProp )
		{
			return m_pBuffProp->PushIndex;
		}
		D_ERROR("GetPushIndex m_pBuffProp = NULL\n");
		return 0;
	}

	const PlayerID& GetCreateID()
	{
		return m_createID;
	}

	unsigned int GetBuffID()
	{
		if(!m_pBuffProp )
			return 0;

		return m_pBuffProp->mID;
	}

	unsigned int GetBuffType()
	{
		if( NULL == m_pBuffProp )
			return 0;
		return m_pBuffProp->AssistType;
	}

	unsigned int GetBuffTime()
	{
		return (unsigned int)m_leftTime.sec();
	}

	unsigned int GetBuffLevel()
	{
		return m_buffLevel;
	}

	unsigned int GetBuffValue()
	{
		if( NULL == m_pBuffProp )
			return 0;

		return m_pBuffProp->Value;
	}

	bool IsSaveDb()
	{
		if( NULL == m_pBuffProp )
			return false;
		return m_pBuffProp->isOffLineSave;
	}

	bool IsDeathClear()
	{
		if( NULL == m_pBuffProp )
			return false;
		return m_pBuffProp->isDeathClear;
	}

	bool IsRightClickClear()
	{
		if( NULL == m_pBuffProp )
			return false;
		return m_pBuffProp->isRightClickClear;
	}

	bool IsGoodBuff()
	{
		if( NULL == m_pBuffProp )
			return false;

		return (m_pBuffProp->bgType != 1);
	}

	//是否是休息技能的buff
	bool IsRestBuff()
	{
		if( NULL == m_pBuffProp )
			return false;

		return (m_pBuffProp->bgType == 2);
	}


	unsigned int GetRelieveLevel()
	{
		if( NULL == m_pBuffProp )
			return 0;

		return m_pBuffProp->BuffLevel;
	}

	unsigned int GetRelieveType()
	{
		if( NULL == m_pBuffProp )
			return 0;

		return m_pBuffProp->SubType;
	}

private: 
	void SingleHpMpBuffCheck( MuxBuffProp* pBuffProp );

private:
	bool m_bValid;						//该BUFF是否生效
	unsigned int m_buffLevel;				//关于buff的等级
	MuxBuffProp* m_pBuffProp;
	PlayerID m_createID;				//生成者的ID 
	ACE_Time_Value m_leftTime;			//BUFF剩余的时间
	ACE_Time_Value m_lastUpdateTime;	//上次更新的时间
	ACE_Time_Value m_hpmpUpdateTimer;	//用于dot，hot的timer
	HpMpUpdateInfo m_hpmpUpdateInfo;	//hpmp的更新
	bool m_bEternal;					//是否是永久buff 永久buff剩余时间无效
};


#endif //MUXBUFF_ELE_H
