﻿#include "RacePrestigeManager.h"
#include "Player/Player.h"
#include "monster/monster.h"
#include "NpcStandRule.h"
#include "Rank/rankeventmanager.h"

CRacePrestigeManager::CRacePrestigeManager(void)
{
	m_pOwner = NULL;
}

CRacePrestigeManager::~CRacePrestigeManager(void)
{
}


void CRacePrestigeManager::AttachPlayer( CPlayer* pPlayer )		//获得寄主
{
	m_pOwner = pPlayer;
}


void CRacePrestigeManager::AddRacePrestige( unsigned int addValue )	 
{
	if ( !m_pOwner )
		return;

#ifdef ANTI_ADDICTION
	m_pOwner->TriedStateProfitCheck( addValue,	TYPE_RACE_PRESTIGE );
#endif /*ANTI_ADDICTION*/

	FullPlayerInfo * playerInfo = m_pOwner->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CRacePrestigeManager::AddRacePrestige, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_1_Base& playerBase = playerInfo->baseInfo;
    playerBase.racePrestige += addValue;
	if( playerBase.racePrestige > MAX_RACE_PRESTIGE )
		playerBase.racePrestige = MAX_RACE_PRESTIGE;

	//RankEventManagerSingle::instance()->MonitorPlayerPrestige( m_pOwner );
	RankEventManager::MonitorPlayerPrestige( m_pOwner );

	m_pOwner->OnNotifyRacePrestige( addValue );
}

bool CRacePrestigeManager::SubRacePrestige( unsigned int subValue )
{
	if ( !m_pOwner )
		return false;

	FullPlayerInfo * playerInfo = m_pOwner->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CRacePrestigeManager::SubRacePrestige, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_1_Base& playerBase = playerInfo->baseInfo;
	if( subValue > playerBase.racePrestige )
		return false;

	playerBase.racePrestige -= subValue;

	//RankEventManagerSingle::instance()->MonitorPlayerPrestige( m_pOwner );
	RankEventManager::MonitorPlayerPrestige( m_pOwner );

	m_pOwner->OnNotifyRacePrestige( (int)(subValue) * -1 );
	return true;
}


void CRacePrestigeManager::OnKillOther( CPlayer* pKilledPlayer )
{
	if( NULL == m_pOwner || NULL == pKilledPlayer )
		return;

	//同种族不影响种族声望
	if( m_pOwner->GetRace() == pKilledPlayer->GetRace() )
		return;

	//每次击杀加1,当然要符合条件
	if( m_pOwner->CanGetKillProfit( pKilledPlayer->GetLevel() ) )
	{
		AddRacePrestige( 1 );
		pKilledPlayer->SubRacePrestige( 1 );
	}
}


void CRacePrestigeManager::OnKillNpc( CMonster* pDeadNpc )				//当杀死一个NPC时
{
	if( NULL == m_pOwner || NULL == pDeadNpc )
		return;

	unsigned short npcRace = pDeadNpc->GetRaceAttribute();
	if( npcRace == NPC_RACE_MONSTER || npcRace == NPC_RACE_NEUTRAL || npcRace == m_pOwner->GetRace() )
		return;

	AddRacePrestige( 1 );
}

