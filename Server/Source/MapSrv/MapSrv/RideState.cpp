﻿#include "RideState.h"
#include "Item/ItemManager.h"
#include <assert.h>
#include <algorithm>
#include <functional>

#include "MuxMap/muxmapbase.h"
#include "Item/CItemPropManager.h"


class FindRideTeamFunc:public std::binary_function<RideTeam*,unsigned long,bool>
{
public:
	bool operator()( RideTeam * pRideTeam1, unsigned int teamID ) const 
	{
		if ( !pRideTeam1 )
			return false;

		if ( pRideTeam1->GetRideTeamID() == teamID )
			return true;

		return false;
	}
};

///after RideTeam created
bool RideTeamManager::AfterRideTeamCreated( RideTeam* pTeam, CPlayer* pLeader, float fSpeed )
{
	if ( ( NULL == pTeam ) || ( NULL == pLeader ) )
	{
		return false;
	}

	CMuxMap* pMap = pLeader->GetCurMap();
	if ( NULL == pMap )
	{
		D_ERROR( "RideTeamManager::AfterRideTeamCreated, NULL current map\n" );
		return false;
	}
	if ( !(pMap->AddRideTeamWithLeader( pTeam, pLeader )) )
	{
		return false;
	}

	//队伍的加速度
	pTeam->OnAddPlayerSpeed( fSpeed );

	m_RideTeamVec.push_back( pTeam );
	pTeam->NoticeRideTeamAppear();

	return true;
}


RideTeam* RideTeamManager::NewRideTeam(CPlayer* pLeader, CBaseItem* pRideItem, float speed )
{
	if ( NULL == pLeader )
	{
		return NULL;
	}

	//如果创建队伍成功
	RideTeam* pTeam = NEW RideTeam( pLeader, pRideItem, speed );
	if ( NULL == pTeam )
	{
		return NULL;	
	}

	bool isok = AfterRideTeamCreated( pTeam, pLeader, speed );
	if ( !isok )
	{
		delete pTeam; pTeam = NULL;
	}
	return pTeam;
}

RideTeam* RideTeamManager::NewRideTeam(CPlayer* pLeader, unsigned long itemTypeID, float speed )
{
	if ( NULL == pLeader )
	{
		return NULL;
	}

	//如果创建队伍成功
	RideTeam* pTeam = NEW RideTeam( pLeader, itemTypeID, speed );
	if ( NULL == pTeam )
	{
		return NULL;
	}

	bool isok = AfterRideTeamCreated( pTeam, pLeader, speed );
	if ( !isok )
	{
		delete pTeam; pTeam = NULL;
	}
	return pTeam;

	//
	//if ( pTeam )
	//{
	//	////队伍ID号
	//	//long teamID = pLeader->GetDBID();
	//	//pTeam->SetRideTeamID( teamID );

	//	//队伍的加速度
	//	pTeam->OnAddPlayerSpeed( speed );

	//	//加入玩家
	//	MuxMapBlock* pNode = pTeam->GetPlayerOwnerMapNode(pLeader);
	//	if( pNode )
	//		pNode->AddRideTeam( pTeam );
	//	else
	//	{
	//		delete pTeam;
	//		return NULL;
	//	}
	//	m_RideTeamVec.push_back( pTeam );
	//	pTeam->NoticeRideTeamAppear();
	//}
	//return pTeam;
}


RideTeam* RideTeamManager::GetRideTeamByID( unsigned long teamID )
{
	if ( m_RideTeamVec.empty() )
		return NULL; 

	std::vector<RideTeam *>::iterator lter = std::find_if( m_RideTeamVec.begin(),m_RideTeamVec.end(), bind2nd( FindRideTeamFunc(),teamID ) );
	if ( lter != m_RideTeamVec.end() )
		return *lter;

	return NULL;
}

//以后做成内存池
void RideTeamManager::RecoverRideTeam( RideTeam* pRideTeam )
{
	if ( !pRideTeam )
		return;

	std::vector<RideTeam *>::iterator lter = std::find_if( m_RideTeamVec.begin(),m_RideTeamVec.end(), bind2nd( FindRideTeamFunc(),pRideTeam->GetRideTeamID() ) );
	if ( lter != m_RideTeamVec.end() )
	{
		m_RideTeamVec.erase( lter );
	}

	delete pRideTeam;
	pRideTeam = NULL;
}

void RideTeamManager::OnPlayerLeaveSrv(CPlayer *pPlayer)
{
	if (!pPlayer)
		return;

	CRideState& rideState = pPlayer->GetSelfStateManager().GetRideState();

	//如果处于骑乘邀请状态,离开服务器，通知另一玩家结束骑乘邀请
	if ( rideState.IsInviteState() )
		rideState.ResetRideInviteState();

	if ( rideState.IsActive() )
	{
		RideTeam* pTeam = rideState.GetRideTeam();
		//assert( pTeam != NULL );
		if ( NULL == pTeam )
		{
			D_ERROR( "RideTeamManager::OnPlayerLeaveSrv, NULL == pTeam\n" );
			return;
		}
		
		if ( pTeam != NULL )
		{
			//新的需求,任意玩家离开队伍,解散骑乘状态
			pTeam->DissolveRideTeam();
			RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam );
		}
		else
		{
			D_ERROR("玩家处于骑乘状态，但是获取不到骑乘队伍的指针\n");
		}
	}
}

void RideTeamManager::OnPlayerSwitchMap(CPlayer* pPlayer )
{
	if ( !pPlayer )
		return;

	CRideState& rideState = pPlayer->GetSelfStateManager().GetRideState();

	//如果处于骑乘邀请状态,切换服务器，通知另一玩家结束骑乘邀请
	if ( rideState.IsInviteState() )
		rideState.ResetRideInviteState();

	if ( rideState.IsActive() )//骑乘状态被激活
	{
		RideTeam* pTeam = rideState.GetRideTeam();
		if ( NULL == pTeam )
		{
			D_ERROR( "RideTeamManager::OnPlayerSwitchMap 玩家处于骑乘状态，但是获取不到骑乘队伍的指针\n" );
			return;
		}

		//如果队伍满的,解除骑乘队员的骑乘状态
		if ( pTeam->IsTeamFull() )
		{
			CPlayer* pMember = pTeam->GetRideMember();
			if ( pMember )
			{
				CRideState& rideState = pMember->GetSelfStateManager().GetRideState();
				rideState.ReSet();
				//修改BUG:跳转后，本身的状态标记并没有清除，现在清除
				pMember->CancelRideState();
			}
#ifdef _DEBUG
			D_DEBUG("跳转地图,解散骑乘队员%s的骑乘状态\n", pMember->GetNickName() );
#endif
		}

		//通知周围的人，队伍解散掉
		MGMountTeamDisBand disbandsrv;
		disbandsrv.mountTeamID = pPlayer->GetFullID();
		CMuxMap* pPlayerMap = pPlayer->GetCurMap();
		if ( NULL != pPlayerMap )
		{
			pPlayerMap->NotifySurrounding<MGMountTeamDisBand>( &disbandsrv, pPlayer->GetPosX(), pPlayer->GetPosY() );
		}

		CMuxMap* pMap = pPlayer->GetCurMap();
		if ( NULL == pMap )
		{
			D_ERROR( "RideTeamManager::OnPlayerSwitchMap, NULL current map\n" );
			return;
		}
		if ( !(pMap->DelRideTeamWithLeader( pTeam, pPlayer )) )
		{
			return;
		}

		CRideState& rideState = pPlayer->GetSelfStateManager().GetRideState();
		rideState.ReSet();
		//修改BUG:跳转后，本身的状态标记并没有清除，现在清除
		pPlayer->CancelRideState();

		//删除该快内存
		RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam );
	}

	return;
}

void RideTeamManager::OnPlayerSwitchMap(CPlayer* pPlayer ,unsigned int newMapID, unsigned int targetPosX, unsigned int targetPosY )
{
	if ( !pPlayer )
		return;

	CRideState& rideState = pPlayer->GetSelfStateManager().GetRideState();

	//如果处于骑乘邀请状态,切换服务器，通知另一玩家结束骑乘邀请
	if ( rideState.IsInviteState() )
		rideState.ResetRideInviteState();

	if ( rideState.IsActive() )//骑乘状态被激活
	{
		RideTeam* pTeam = rideState.GetRideTeam();
		if ( pTeam )
		{
			//如果队伍满的,如果是双人骑乘状态
			if ( pTeam->IsTeamFull() )
			{
				//通知骑乘队员跳地图
				CPlayer* pTeamMember = pTeam->GetRideMember();

				//解散骑乘队伍
				pTeam->DissolveRideTeam();
				RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam );

				if ( pTeamMember && pTeamMember->GetDBUID() != pPlayer->GetDBUID() )
				{
					pTeamMember->IssueSwitchMap( newMapID, targetPosX , targetPosY );
#ifdef _DEBUG
					D_DEBUG("跳转地图,解散骑GMPlayerLeave乘队员%s的骑乘状态\n", pTeamMember->GetNickName() );
#endif
				}
				return;
			}
			else//如果是一个人的队伍
			{
				//通知周围的人，队伍解散掉
				MGMountTeamDisBand disbandsrv;
				disbandsrv.mountTeamID = pPlayer->GetFullID();
				CMuxMap* pPlayerMap    = pPlayer->GetCurMap();
				if ( NULL != pPlayerMap )
				{
					pPlayerMap->NotifySurrounding<MGMountTeamDisBand>( &disbandsrv, pPlayer->GetPosX(), pPlayer->GetPosY() );
				}

				CMuxMap* pMap = pPlayer->GetCurMap();
				if ( NULL == pMap )
				{
					D_ERROR( "RideTeamManager::OnPlayerSwitchMap, NULL current map\n" );
					return;
				}
				if ( !(pMap->DelRideTeamWithLeader( pTeam, pPlayer )) )
				{
					return;
				}

				//删除该快内存
				RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam );

				//骑乘玩家的状态重置
				rideState.ReSet();
				pPlayer->CancelRideState();
				return;
			}
		}
		else
		{
			D_ERROR("RideTeamManager::OnPlayerSwichMap, NULL = pTeam\n");
		}
	}
}




//void RideTeam::OnRideTeamMoveToNewMapNode( MuxMapBlock* pOldNode )
//{
//	assert( m_pLeader != NULL );
//
//	MuxMapBlock* pNewNode = GetPlayerOwnerMapNode( m_pLeader );
//	if ( pNewNode != pOldNode )
//	{
//		pOldNode->DeleteRideTeam(this);
//		pNewNode->AddRideTeam(this);
//	}
//
//	//同时修改队伍其他队伍的节点
//	ChangeTeamMemberNode();
//}

//进入骑乘队伍
void RideTeam::EnterRideTeam( CPlayer* pPlayer )
{
	if ( !pPlayer )
		return;

	//如果有值，证明加入RideTeam错误
	if ( m_pMember != NULL )
	{
		D_ERROR("进入骑乘队伍时，该队伍已经满了\n");
		return;
	}

	if ( m_pLeader == NULL )
	{
		D_ERROR("进入骑乘队伍。队长指针为NULL\n");
		return;
	}

	m_pMember = pPlayer;
	m_pMember->ForceSetFloatPos( false/*非移动状态下重设位置*/, m_pLeader->GetFloatPosX(), m_pLeader->GetFloatPosY() );//骑乘乘员拉至与骑乘队长同一位置;

	NoticeRideTeamAppear();

	MGMountStateChange leaderState;
	leaderState.rideState  = E_RIDE_INFO_UPDATE;
	leaderState.attachInfo1 = m_pMember->GetPID();
	leaderState.attachInfo2 = m_pMember->GetGID();
	m_pLeader->SendPkg<MGMountStateChange>(&leaderState);

	return;
}

//增加队伍的速度
void RideTeam::OnAddPlayerSpeed(float fspeed )
{
	if( NULL != m_pLeader )
		m_pLeader->SetRideMaxSpeed( (int)fspeed );

	OnChangePlayerSpeed();
}

//减少队伍的速度
void RideTeam::OnSubPlayerSpeed(float fspeed )
{
	ACE_UNUSED_ARG( fspeed );

	if ( NULL != m_pLeader )
		m_pLeader->SetRideMaxSpeed( 0 );

	OnChangePlayerSpeed();
}

void RideTeam::OnChangePlayerSpeed( )
{
	if ( NULL == m_pLeader )
	{
		D_ERROR( "RideTeam::OnChangePlayerSpeed, NULL == m_pLeader\n" );
		return;
	}

	if ( m_pLeader )
	{
		////添加在服务器的速度
		///m_pLeader->GetItemAddtionProp().AddSpeed += speed;

		////通知客户端速度修改了
		//MGRoleattUpdate updateAttr;
		//updateAttr.nOldValue = 0;
		//updateAttr.nNewValue = (int)( m_pLeader->GetSpeed() * 100);
		//updateAttr.lChangedPlayerID = m_pLeader->GetFullID();
		//updateAttr.nType = RT_SPEED;	//修改速度
		//updateAttr.changeTime = 0;		//启程为永久 7.9 hyn
		//updateAttr.bFloat = true;
		//m_pLeader->SendPkg<MGRoleattUpdate>( &updateAttr );

		m_pLeader->NoticeSpeedChange();

		//D_DEBUG("%d 队伍速度变为:%d 变化幅度:%f\n",GetRideTeamID(),updateAttr.nNewValue,speed );
	}
}

//获取抗击晕等级
int RideTeam::GetStunRank()
{
	int stun = -1;

	//只有是骑乘装备的时候才
	if ( m_pRideItem )
	{
		EquipProperty* pEquipProp = CEquipPropertyManagerSingle::instance()->GetSpecialPropertyT( m_pRideItem->GetItemID() );
		if ( pEquipProp )
			stun = pEquipProp->Stun;
	}

	return stun;
}

//void RideTeam::ChangeTeamMemberPos()
//{
//	//如果不在一个节点，那么从原block节点删除，加入新的block节点
//	if( !m_pMember )
//		return;
//
//	MuxMapBlock* pNewNode = GetPlayerOwnerMapNode( m_pLeader );
//	MuxMapBlock* pOldNode = GetPlayerOwnerMapNode( m_pMember );
//	if (  pNewNode && pOldNode && pNewNode != pOldNode )
//	{
//		//当删除队员成功后，才考虑将该玩家加入新的block
//		if ( pOldNode->DeletePlayerFromBlock(m_pMember) == true )
//		{
//			pNewNode->AddPlayerToBlock( m_pMember );
//			unsigned short nX = 0,nY = nX;
//			MuxMapSpace::FloatToGridPos( m_pLeader->GetFloatPosX(), m_pLeader->GetFloatPosY(), nX, nY );
//			m_pMember->SetPlayerPos( nX, nY, true );
//
//			//通知玩家位置变化；
//			MGSetPlayerPos returnMsg;
//			returnMsg.lNotiPlayerID = m_pMember->GetFullID();
//			returnMsg.setpos.posX = m_pMember->GetPosX();
//			returnMsg.setpos.posY = m_pMember->GetPosY();
//			returnMsg.setpos.reason = POS_INNER_JUMP;
//			returnMsg.setpos.playerID = m_pMember->GetFullID();
//			m_pMember->SendPkg<MGSetPlayerPos>( &returnMsg );
//		}
//		else
//		{
//			D_ERROR("RideTeam::ChangeTeamMemberPos() 改变骑乘队员的坐标失败！\n");
//		}
//	}
//}

//退出骑乘队伍
void RideTeam::ExitRideTeam( CPlayer* pPlayer )
{
	if ( !m_pMember || !pPlayer )
		return;

	if ( m_pMember->GetFullID() != pPlayer->GetFullID())
		return;

	
	D_DEBUG("%s 骑乘队员离开骑乘队伍\n",pPlayer->GetNickName() );

	m_pMember = NULL;
	NoticeRideTeamAppear();

	pPlayer->GetSelfStateManager().GetRideState().OnEnd();
}

//解散队伍
void RideTeam::DissolveRideTeam()
{
	if ( !m_pLeader )
	{
		D_ERROR("解散队伍出错,队长指针为NULL\n");
		return;
	}

	D_DEBUG("%s 骑乘队长解散了骑乘队伍%d\n",m_pLeader->GetNickName(),GetRideTeamID() );

	MGMountTeamDisBand disbandsrv;
	disbandsrv.mountTeamID = m_pLeader->GetFullID();
	NoticeMsgToAll<MGMountTeamDisBand>( &disbandsrv, true );

	//恢复玩家使用骑乘前的速度
	OnSubPlayerSpeed( m_speed );

	CMuxMap* pMap = m_pLeader->GetCurMap();
	if ( NULL == pMap )
	{
		D_ERROR( "RideTeam::DissolveRideTeam, NULL current map\n" );
		return;
	}
	if ( !(pMap->DelRideTeamWithLeader( this, m_pLeader )) )
	{
		return;
	}

	m_pLeader->GetSelfStateManager().GetRideState().OnEnd();
	m_pLeader->ResetMoveStat();//10.03.09，移动bug查找修改
	m_pLeader = NULL;

	if ( NULL != m_pMember )
	{
		m_pMember->GetSelfStateManager().GetRideState().OnEnd();
		m_pMember->ResetMoveStat();//10.03.09，移动bug查找修改
		m_pMember = NULL;
	}

	return;
}

void RideTeam::NoticeRideTeamAppear()
{
	MGMountTeamInfo appearMsg;
	GetRideTeamMember(appearMsg.MountTeam[0],appearMsg.MountTeam[1]);
	GetRideTeamPos( appearMsg.x, appearMsg.y );
	GetRideItemInfo( appearMsg.rideItem );
	GetMountItemType( appearMsg.mountType );
	NoticeMsgToAll( &appearMsg,true );

	D_DEBUG("NoticeRideTeamAppear---MGMountTeamInfo\n");
}

void RideTeam::OnRideTeamMoveToNewBlock()
{
	if ( !m_pLeader )
		return;

	CMuxMap* pPlayerMap = m_pLeader->GetCurMap();
	if ( !pPlayerMap )
		return;

	MGMountTeamInfo appearMsg;
	GetRideTeamMember(appearMsg.MountTeam[0],appearMsg.MountTeam[1]);
	GetRideTeamPos(  appearMsg.x, appearMsg.y );
	GetRideItemInfo( appearMsg.rideItem );
	GetMountItemType( appearMsg.mountType );

	if ( NULL == pPlayerMap )
	{
		return;
	}
	pPlayerMap->NotifySurrounding<MGMountTeamInfo>( &appearMsg, m_pLeader->GetPosX(), m_pLeader->GetPosY() );

	D_DEBUG("OnRideTeamMoveToNewBlock--MGMountTeamInfo\n");

	return;
}

void RideTeam::NoticeRideTeamInfoToOtherPlayer( CPlayer* pPlayer )
{
	if ( !pPlayer )
		return;

	MGMountTeamInfo appearMsg;
	GetRideTeamMember(appearMsg.MountTeam[0],appearMsg.MountTeam[1]);
	GetRideTeamPos( appearMsg.x, appearMsg.y );
	GetRideItemInfo( appearMsg.rideItem );
	GetMountItemType( appearMsg.mountType );

	pPlayer->SendPkg<MGMountTeamInfo>(&appearMsg);
}

bool RideTeam::IsTeamLeader(CPlayer* pPlayer )
{
	if ( !pPlayer )
		return false;

	return pPlayer->GetFullID().wGID == m_pLeader->GetFullID().wGID
		&& pPlayer->GetFullID().dwPID == m_pLeader->GetFullID().dwPID;
}


void RideTeam::GetRideTeamPos( int& x, int& y)
{
	if ( m_pLeader )
	{
		x = m_pLeader->GetPosX();
		y = m_pLeader->GetPosY();
	}
	else
	{
		x = y = 0;
	}
}

void RideTeam::GetRideItemInfo( ItemInfo_i& rideItemInfo )
{
	//rideItemInfo.ucCount = rideItemInfo.ucLevelUp = rideItemInfo.uiID = rideItemInfo.randSet/*usAddId*/ = rideItemInfo.usItemTypeID = rideItemInfo.usWearPoint = 0;
	StructMemSet(rideItemInfo,0,sizeof(rideItemInfo));

	if ( m_pRideItem )
	{
		if ( m_pLeader )
		{
			ItemInfo_i * itemInfo = m_pLeader->GetValidDriveEquip();
			if ( NULL != itemInfo)
			{
				rideItemInfo = *itemInfo;
			}
		}
	}
	else
	{
		rideItemInfo.uiID = 0;
		rideItemInfo.usItemTypeID =  mItemTypeID;
	}
}

void RideTeam::GetMountItemType( char& mountItemType )
{
	if ( m_pLeader)
	{
		mountItemType =  (char)m_pLeader->GetSelfStateManager().GetRideState().GetPlayerRideState();
	}
	else
		mountItemType = 0;
}

void RideTeam::GetRideTeamMember( PlayerID& leaderID, PlayerID& memberID )
{
	if ( m_pLeader )
		leaderID = m_pLeader->GetFullID();
	else
	{
		leaderID.dwPID = 0;
		leaderID.wGID  = 0;
	}
	
	if ( m_pMember )
		memberID = m_pMember->GetFullID();
	else
	{
		memberID.dwPID = 0;
		memberID.wGID = 0;
	}
}

//MuxMapBlock* RideTeam::GetPlayerOwnerMapNode(CPlayer* pPlayer )
//{
//	if ( !pPlayer )
//		return NULL;
//
//	CNewMap* pMap = pPlayer->GetCurMap();
//	if ( pMap )
//	{
//		unsigned short blockX=0, blockY=0;
//		MuxMapSpace::GridToBlockPos( pPlayer->GetPosX(), pPlayer->GetPosY(), blockX, blockY );
//		return pMap->GetMapBlockByBlock( blockX, blockY );
//
//	}
//	
//	return NULL;
//	 
//}

