﻿/********************************************************************
	created:	2009/10/27
	created:	27:10:2009   14:58
	file:		OLTestQuestionlibrary.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef OLTEST_QUESTION_LIBRARY_H
#define OLTEST_QUESTION_LIBRARY_H

#include <map>

struct Question
{
	unsigned int questionID;
	unsigned int answerID;
	unsigned int limittime;
};



class OLTestQuestionLibrary
{
public:
	static void ReadQuestionLib();

	static Question* GetQuestionInfo( unsigned int questionID )
	{
		std::map<unsigned int, Question >::iterator lter = mQuestionsLibrary.find( questionID );
		if ( lter != mQuestionsLibrary.end() )
		{
			return &lter->second;
		}
		return NULL;
	}

private:
	static std::map<unsigned int, Question > mQuestionsLibrary;
};



#endif