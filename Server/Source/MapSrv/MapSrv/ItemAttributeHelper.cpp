﻿#include "ItemAttributeHelper.h"
#include "Player/Player.h"
#include "Item/CSpecialItemProp.h"
#include "Item/ItemDelegate.h"
//#include "ItemPropRandom.h"
#include "EmbedDiamond.h"

#include "Item/ManItemRandomProps.h"

void ItemAttributeHelper::OnEquipItemAffectPlayer(CPlayer* pPlayer, const ItemInfo_i& equipItem )
{
	if ( !pPlayer )
		return;

	if ( equipItem.usWearPoint >0 )//装备的耐久度要大于0，才能将属性加成在装备上面
	{
		CBaseItem* pItem = pPlayer->GetItemDelegate().GetItemByItemUID( equipItem.uiID );
		if ( !pItem )
			return;

		if (!pItem->IsCanUseByProperty(pPlayer))//玩家的属性点满足要求装备要求，才能将属性加成在装备上面
		{
			pPlayer->GetItemDelegate().SetEquipPosUnValid(pItem->GetGearArmType());
			return;
		}

		pPlayer->GetItemDelegate().SetEquipPosValid(pItem->GetGearArmType());//设置装备位有效

		//将道具的基础属性+到玩家的道具总属性上
		pItem->AddItemPropToPlayer( pPlayer, equipItem );

		//算属性的时候，将宝石的属性也算上
		pPlayer->GetItemEmbedDiamondManager().OnPlayerEquipItem( equipItem.uiID );

		//如果是武器或者是装备,看是否有幸运属性或卓越属性
		if( pItem->GetItemTypeID() == E_TYPE_WEAPON  || pItem->GetItemTypeID() == E_TYPE_EQUIP )
		{
			//直接通过luckyID与excellentID来决定是否卓越/幸运装备 int  itemLuckyType = pItem->GetItemLuckyType();
			//if ( itemLuckyType == 0 )
			//	return;

			if ( equipItem.luckyid > 0 )
			{
				_item_lucky_excellent_affect_player( pPlayer, pItem, equipItem, 1/*幸运*/, true );
			}

			if ( equipItem.excellentid > 0 )
			{
				_item_lucky_excellent_affect_player( pPlayer, pItem, equipItem, 2/*卓越*/, true );
			}

			//if( itemLuckyType >  0 )//如果是幸运以上道具( type==1 幸运, type==2 卓越)
			//{
			//	
			//	//_item_luckyattribute_affect_player(  pPlayer, pItem, equipItem.luckyid , true );

			//	if ( itemLuckyType == 2  )//如果是卓越装备,则需要随机卓越属性
			//	{
			//		
			//		//_item_excellentattribute_affect_player(  pPlayer , pItem, equipItem.excellentid, true  );
			//	}
			//}
		}
	}
}

void ItemAttributeHelper::OnUnEquipItemAffectPlayer( CPlayer* pPlayer, const ItemInfo_i& unequipItem )
{
	if (  !pPlayer)
		return;

	if ( unequipItem.usWearPoint > 0 )
	{
		OnWearPointEmptyAffectPlayer( pPlayer, unequipItem );
	}
	if( pPlayer->GetCurrentHP() > pPlayer->GetMaxHP() )
	{
		pPlayer->SetPlayerHp( pPlayer->GetMaxHP() );
		pPlayer->NotifyHpChange();
	}

	if ( pPlayer->GetCurrentMP() > pPlayer->GetMaxMP() )
	{
		pPlayer->SetPlayerMp( pPlayer->GetMaxMP() );
		pPlayer->NotifyMpChange();
	}
	
}

void ItemAttributeHelper::OnWearPointEmptyAffectPlayer(CPlayer* pPlayer, const ItemInfo_i& wearEmptyitem )
{
	if ( !pPlayer )
		return;

	CBaseItem* pItem = pPlayer->GetItemDelegate().GetItemByItemUID( wearEmptyitem.uiID );
	if ( !pItem )
		return;

	pPlayer->GetItemDelegate().SetEquipPosUnValid(pItem->GetGearArmType());//设置装备位无效

	//将道具的基础属性从玩家的属性上移除 
	pItem->SubItemPropToPlayer( pPlayer, wearEmptyitem );

	//算属性的时候，将宝石的属性也减去
	pPlayer->GetItemEmbedDiamondManager().OnPlayerUnEquipItem( wearEmptyitem.uiID );

	//如果是武器或者是装备,看是否有幸运属性或卓越属性
	if( pItem->GetItemTypeID() == E_TYPE_WEAPON  || pItem->GetItemTypeID() == E_TYPE_EQUIP )
	{
		//直接通过luckyID与excellentID来决定是否卓越/幸运装备 int  itemLuckyType = pItem->GetItemLuckyType();
		//if ( itemLuckyType == 0 )
		//	return;

		if ( pItem->GetItemLuckyID() > 0 )
		{
			_item_lucky_excellent_affect_player( pPlayer, pItem, wearEmptyitem, 1/*幸运*/, false );
		}

		if ( pItem->GetItemExcellentID() > 0 )
		{
			_item_lucky_excellent_affect_player( pPlayer, pItem, wearEmptyitem, 2/*卓越*/, false );
		}

		//if( itemLuckyType >  0 )//如果是幸运以上道具( type==1 幸运, type==2 卓越)
		//{
		//	//_item_luckyattribute_affect_player(  pPlayer, pItem, wearEmptyitem.luckyid , false );
		//	if ( itemLuckyType == 2  )//如果是卓越装备,则需要随机卓越属性
		//	{
		//		//_item_excellentattribute_affect_player(  pPlayer , pItem, wearEmptyitem.excellentid, false  );
		//	}
		//}
	}
}

void ItemAttributeHelper::OnRepairWearPointEmtpyItem(CPlayer* pPlayer, const ItemInfo_i& repaireItem )
{
	OnEquipItemAffectPlayer( pPlayer, repaireItem );
}

//装备卓越或幸运属性影响
bool ItemAttributeHelper::_item_lucky_excellent_affect_player( CPlayer* pPlayer, CBaseItem* pItem, const ItemInfo_i& equipItem, unsigned int luckyorexcel/*lucky1或excel2*/, bool isequipitem )
{
	if ( ( NULL == pPlayer || NULL == pItem  ) )
	{
		D_ERROR( "_item_lucky_excellent_affect_player，输入参数空\n" );
		return false;
	}
	unsigned int orgSeed = 0;
	if ( 1 == luckyorexcel )
	{
		orgSeed = equipItem.luckyid;
	} else if ( 2 == luckyorexcel ) {
		orgSeed = equipItem.excellentid;
	} else {
		D_ERROR( "_item_lucky_excellent_affect_player, 错误的luckorexcel:%d\n", luckyorexcel );
		return false;
	}

	GenedProps genProps;//该道具的相关加成；
	if ( !CManItemRandSets::GetRandProps( (1==luckyorexcel)/*幸运或卓越*/, orgSeed, equipItem.randSet, genProps ) )
	{
		D_ERROR( "_item_lucky_excellent_affect_player，GetRandProps失败\n" );
		return false;
	}

	unsigned int genPropNum = genProps.GetPropNum();
	RPropID propID;
	short   propVal = 0;
	for ( unsigned int i=0; i<genPropNum; ++i )
	{
		if ( !genProps.GetIndexProp( i, propID, propVal ) )
		{
			D_ERROR( "_item_lucky_excellent_affect_player，GetIndexProp失败，索引值%d\n", i );
			continue;
		}
		int& playerAttriIDref = pPlayer->GetNewItemAddRandAtt( (ItemRandAddAtt)propID );
		//if ( NULL == playerAttriIDref )
		//{
		//	D_ERROR( "_item_lucky_excellent_affect_player，GetItemAttributeByAttriID失败，属性ID：%d\n", propID );
		//	continue;
		//}
		if ( isequipitem )
		{
			playerAttriIDref += propVal;
		} else {
			playerAttriIDref -= propVal;
		}
	}

	return true;
}


//void ItemAttributeHelper::_item_luckyattribute_affect_player( CPlayer* pPlayer, CBaseItem* pItem, unsigned int luckseed , bool isequipitem )
//{
//	if ( !pPlayer || !pItem  )
//		return ;
//
//	RandItemAttributeSet* pLuckyAttributeSet = ItemRandAttributeSetManagerSingle::instance()->GetItemPropRandSet( pItem->GetItemLuckyID() );
//	if ( pLuckyAttributeSet )
//	{
//		CRandGen::SetSeed( luckseed );
//		//seedMT( luckseed );//幸运的随机种子
//
//		std::vector<int> itemRandPropVec;//随机出幸运属性个数和属性编号
//		pLuckyAttributeSet->randomProp( itemRandPropVec );//, tmpseed );// , &randomMT );
//
//		//依次遍历每个随机出来的属性，随机出每个属性的具体值
//		for( std::vector<int>::iterator iter = itemRandPropVec.begin(); iter != itemRandPropVec.end(); ++iter )
//		{
//			int       attributeID   = *iter;
//			int* playerAttriIDref   = pPlayer->GetItemAttributeByAttriID( attributeID );
//			if ( !playerAttriIDref )
//				continue;
//
//			/*根据道具的等级,装备位置,属性编号,获取对应的随机范围对象*/
//			ItemRandAttribute randAttribute;
//			if( LuckyRandAttributeManagerSingle::instance()->GetExcellentAttribute( GetItemLevelRank(pItem->GetItemLevel()), (int)pItem->GetGearArmType(), attributeID,  randAttribute ) )
//			{
//				//在该范围内获取道具的幸运随机属性
//				int  randAttriValue = ExecRandItemAttribute( randAttribute );//, tmpseed );// , &randomMT );
//
//				if ( isequipitem )
//					*playerAttriIDref  += randAttriValue;
//				else
//					*playerAttriIDref  -= randAttriValue;
//			}
//		}
//	}
//}

//void ItemAttributeHelper::_item_excellentattribute_affect_player(CPlayer* pPlayer,CBaseItem* pItem , unsigned int excellentseed , bool isequipitem )
//{
//	if ( !pPlayer || !pItem  )
//		return ;
//
//	RandItemAttributeSet* pExcellentAttributeSet = ItemRandAttributeSetManagerSingle::instance()->GetItemPropRandSet( pItem->GetItemExcellentID() );
//	if ( pExcellentAttributeSet )
//	{
//		CRandGen::SetSeed( excellentseed );
//		//seedMT( excellentseed );//卓越的随机种子
//
//		std::vector<int> itemRandPropVec;//随机出幸运属性个数和属性编号
//		pExcellentAttributeSet->randomProp( itemRandPropVec );//, tmpseed );// , &randomMT );
//
//		//依次遍历每个随机出来的属性，随机出每个属性的具体值
//		for( std::vector<int>::iterator iter = itemRandPropVec.begin(); iter != itemRandPropVec.end(); ++iter )
//		{
//			int       attributeID   = *iter;
//			int* playerAttriIDref   = pPlayer->GetItemAttributeByAttriID( attributeID );
//			if ( !playerAttriIDref )
//				continue;
//
//			/*根据道具的等级,装备位置,属性编号,获取对应的卓越随机范围对象*/
//			ItemRandAttribute randAttribute;
//			if( ExcellentRandAttributeManagerSingle::instance()->GetExcellentAttribute( GetItemLevelRank(pItem->GetItemLevel()), (int)pItem->GetGearArmType(), attributeID,  randAttribute ) )
//			{
//				//在该范围内获取道具的随机属性
//				int  randAttriValue = ExecRandItemAttribute( randAttribute ); //, tmpseed );// , &randomMT );
//
//				if ( isequipitem )
//					*playerAttriIDref  += randAttriValue;
//				else
//					*playerAttriIDref  -= randAttriValue;
//			}
//		}
//	}
//}

int ItemAttributeHelper::ExecRandItemAttribute(ItemRandAttribute& randAttribute) //, unsigned int& seed)// unsigned int (*pRandFunc)(void)  )
{
	int randfvalue = randAttribute.Min + CRandGen::GetRand()/*pRandFunc()*/%( randAttribute.Max - randAttribute.Min + 1 );
	return randfvalue;
}