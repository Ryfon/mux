﻿#include "UnionManager.h"
#include "../../../Base/Utility.h"
#include "../BuffProp.h"
#include "../Player/Player.h"
#include "../MuxMap/muxmapbase.h"

CUnion::CUnion(unsigned int unionID)
	:m_id(unionID)
{
	StructMemSet(m_name, 0x00, sizeof(m_name));
	m_captionID = 0;
	m_active = 0;
	m_prestige = 0;
	m_level = 0;
	StructMemSet(m_badge, 0x00, sizeof(m_badge));
	m_haveIssuedTask = false;
	m_issuedTasksEndTime = 0;
	//StructMemSet( m_unionAttributeEffect, 0, sizeof( m_unionAttributeEffect ) );
	m_unionAttributeEffect.Reset();
	StructMemSet(activeTimesArg, 0x00, sizeof(activeTimesArg));
	StructMemSet(prestigeTimesArg, 0x00, sizeof(prestigeTimesArg));
	StructMemSet(m_showInfo, 0x00, sizeof(m_showInfo));
}

CUnion::~CUnion(void)
{

}

void CUnion::SetName(const char *pName)
{
	if ( NULL == pName )
	{
		return;
	}
	SafeStrCpy(m_name, pName);
	
	SafeStrCpy(m_showInfo.showInfos.showInfo.name, pName);
	m_showInfo.showInfos.showInfo.nameLen = (USHORT)strlen(m_showInfo.showInfos.showInfo.name);
	return;
}

void CUnion::SetActive(unsigned int val)
{
	m_active = val;
	return;
}

void CUnion::SetCaption(unsigned int roleId)
{
	m_captionID = roleId;
	return;
}

void CUnion::SetPrestige(unsigned int val)
{
	m_prestige = val;
	return;
}

void CUnion::SetBadge(const char* newBadge)
{
	if ( NULL == newBadge )
	{
		return;
	}
	if(strncmp(m_badge, newBadge, (USHORT)strlen(newBadge)) != 0)
	{
		OnBadgeChanage(newBadge);
	}

	return;
}

void CUnion::SetLevel(unsigned short level)
{
	if(level != m_level)
	{
		OnLevelChanage(level);//等级变化处理
	}
	
	return;
}

void CUnion::SetNTimesActive(BYTE times, unsigned int expireTime)
{
	activeTimesArg.times = times;
	activeTimesArg.expireTime = expireTime;

	return;
}

void CUnion::SetNTimesPrestige(BYTE times, unsigned int expireTime)
{
	prestigeTimesArg.times = times;
	prestigeTimesArg.expireTime = expireTime;

	return;
}

// 设置工会发布任务
void CUnion::SetIssuedTasks(time_t taskEndTime)
{
	//判断参数有效性([当前时间 >= 终止时间]无效)
	time_t now = ACE_OS::time();
	if(now >= taskEndTime)
	{
		D_ERROR("工会ID=%d发布任务时,任务终止时间%d小于系统当前时间%d\n", m_id, taskEndTime, now);
		return;
	}

	//判断是否已发布
	if(CheckTaskHaveIssue())
	{
		D_ERROR("工会ID=%d此次发布任务时,该公会已处于已发布状态\n", m_id);
		return;
	}

	////清空
	//m_issuedTasks.clear();
	//m_haveIssuedTask = false;
	//m_issuedTasksEndTime = 0;

	//std::list<UnionIssueTaskArg *> issuetask;
	//size_t size = CUnionConfigSingle::instance()->GetIssueTaskArg(m_level, issuetask);
	//if(size == 0)
	//{
	//	D_ERROR("发布工会任务时没有找到任务列表，工会信息:id=%d, level=%d\n", m_id, m_level);
	//	return;
	//}

	//for(std::list<UnionIssueTaskArg *>::iterator i = issuetask.begin(); i != issuetask.end(); i++)
	//{
	//	m_issuedTasks.insert((*i)->taskid);
	//}

	m_haveIssuedTask = true;
	m_issuedTasksEndTime = taskEndTime;

	//09.04.28,取消工会任务新实现,CreateUnionTaskPkg();

	return;
}

//09.04.28,取消工会任务新实现,///创建工会任务信息包；
//bool CUnion::CreateUnionTaskPkg()
//{
//	memset(&m_unionTasksPkg, 0x00, sizeof(m_unionTasksPkg));
//	m_unionTasksPkg.taskEndTime = (unsigned int)(GetTaskEndTime());
//	int pkgCapacity = sizeof( m_unionTasksPkg.unionTask ) / sizeof( m_unionTasksPkg.unionTask[0] );
//	if ( pkgCapacity > m_issuedTasks.size() )
//	{
//		D_ERROR( "工会%d,等级%d，工会任务数量超限！\n", m_id, m_level );
//		return false;
//	}
//
//	int count = 0;
//	for ( std::set<unsigned int>::iterator iter=m_issuedTasks.begin(); iter!=m_issuedTasks.end(); ++iter )
//	{
//		m_unionTasksPkg.unionTask[count] = *iter;
//		count++;
//
//		if(count == pkgCapacity)
//		{
//			break;
//		}
//	}
//	return true;
//}
//
/////发送工公任务给玩家；
//bool CUnion::SendUnionTaskToPlayer( CPlayer* pPlayer )
//{
//	if ( NULL == pPlayer )
//	{
//		return false;
//	}
//	
//	//设置玩家可以使用工会任务对话标记，确保玩家通过合法的路径到达工会任务选择，防止玩家绕过NPC对话，直接使用工会任务选择
//	//  当然，玩家可以在某一时刻到达工会任务选择，然后不选工会任务，直至稍后其觉得合适时再选择工作任务，若需要防止此种情况，则需另做判断；
//	pPlayer->SetCanSelectRandomTask();	
//	pPlayer->SendPkg< MGDisplayUnionIssuedTasks >( &m_unionTasksPkg );
//	return true;
//}
//
/////玩家选择工会任务；
//bool CUnion::OnPlayerSelectUnionTask( CPlayer* pPlayer, unsigned int taskPos/*所接任务序号，用于校验*/, unsigned int taskID/*所接任务ID号*/ )
//{
//	if ( NULL == pPlayer )
//	{
//		return false;
//	}
//
//	//设置玩家可以使用工会任务对话标记，确保玩家通过合法的路径到达工会任务选择，防止玩家绕过NPC对话，直接使用工会任务选择
//	//  当然，玩家可以在某一时刻到达工会任务选择，然后不选工会任务，直至稍后其觉得合适时再选择工作任务，若需要防止此种情况，则需另做判断；
//	if ( !pPlayer->CanSelectRandomTask() )
//	{
//		D_WARNING( "玩家%s未经过NPC对话就直接选择工会任务!!\n", pPlayer->GetAccount() );
//		return false;
//	}
//	pPlayer->ClearCanSelectRandomTask();//清玩家可以使用工会对话标记；
//
//	if ( taskPos >= m_issuedTasks.size() )
//	{
//		D_WARNING( "玩家%s发来欲接工会任务序号%d非法!\n", pPlayer->GetNickName(), taskPos );
//		return false;
//	}
//	std::set<unsigned int>::iterator iter = m_issuedTasks.begin();
//	advance( iter, taskPos );
//	if ( *iter != taskID )
//	{
//		D_WARNING( "玩家%s欲接工会任务，其当前可接工会任务中第%d个不是%d!\n", pPlayer->GetNickName(), taskPos, taskID );
//		return false;
//	}
//
//	///以下给玩家绑定任务taskID对应任务脚本并开始执行;
//	pPlayer->OnChatToTask( taskID, 0, true );
//
//	return true;
//}

//设置工会技能
size_t CUnion::SetSkills(const unsigned int *skills)
{
	if(NULL == skills)
		return 0;

	//清空
	ReleaseSkills();

	for(int i = 0; i < MAX_UNION_SKILL_NUM; i++)
	{
		if(skills[i] == 0)
			break;

		CMuxSkillProp *pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( skills[i] );
		if(NULL != pSkillProp )
		{
			CMuxPlayerSkill* pSkill = g_poolManager->RetriveSkill();
			if( NULL == pSkill )
			{
				continue;
			}
			pSkill->InitGuildSkill( pSkillProp, this );
			m_skills.insert(make_pair(skills[i], pSkill));
		}
	}

	return m_skills.size();
}


void CUnion::ReleaseSkills()
{
	CMuxPlayerSkill* pSkill = NULL;
	for( std::map<unsigned int, CMuxPlayerSkill* >::iterator iter = m_skills.begin(); iter != m_skills.end(); ++iter )
	{
		pSkill = iter->second;
		if( NULL == pSkill )
			continue;

		g_poolManager->ReleaseSkill( pSkill );
	}
	m_skills.clear();
}


// 获取已发布人物列表	
std::set<unsigned int>& CUnion::GetIssuedTasks(void)
{
	return m_issuedTasks;
}

bool CUnion::CheckTaskHaveIssue(void)
{
	if(!m_haveIssuedTask)
		return false;

	time_t now = ACE_OS::time();
	if(now >= m_issuedTasksEndTime)
	{
		m_haveIssuedTask = false;
		m_issuedTasksEndTime = 0;
		m_issuedTasks.clear();

		return false;
	}

	return true;
}

time_t CUnion::GetTaskEndTime(void)
{
	return m_issuedTasksEndTime;
}

// 获取工会技能列表
std::map<unsigned int, CMuxPlayerSkill*> & CUnion::GetSkills(void)
{
	return m_skills;
}

CMuxPlayerSkill * CUnion::GetSkillArgsByID(unsigned int skillID)
{
	std::map<unsigned int, CMuxPlayerSkill*>::iterator i = m_skills.find(skillID);
	if(i != m_skills.end())
		return i->second;

	return NULL;
}

unsigned int  CUnion::GetActive(void)
{
	return m_active;
}

unsigned int CUnion::GetPrestige(void)
{
	return m_prestige;
}

const char* CUnion::GetName(void)
{
	return m_name;
}

void CUnion::InitUnionAttributeEffect()	//初试化被动技能影响属性列表
{
	CMuxPlayerSkill* pSkill = NULL;
	ClearUnionEffect();
	for( std::map<unsigned int, CMuxPlayerSkill*>::iterator iter = m_skills.begin(); iter != m_skills.end(); ++iter )
	{
		pSkill = iter->second;
		if( NULL == pSkill )
			continue;

		//永久属性技能
		if( pSkill->GetSkillType() != 3 )
			continue;

		MuxBuffProp* pBuffProp = pSkill->GetBuffProp();
		if( NULL == pBuffProp )
			return;
		
		AddUnionEffect( pBuffProp );
	}
	NotifyUnionSkillChange();
}


void CUnion::NotifyUnionSkillChange()		//技能变化 影响显示部分的通知
{
	//如果加血的话得通知周围玩家HP变更 太脑残了
	CPlayer* pTmpPlayer = NULL;
	for( size_t i = 0; i < unionMembers.size(); ++i )
	{
		CGateSrv* pGateSrv = CManG_MProc::FindProc( unionMembers[i].wGID );
		if( NULL == pGateSrv )
		{
			return;
		}
		pTmpPlayer = pGateSrv->FindPlayer( unionMembers[i].dwPID );
		if( NULL == pTmpPlayer )
		{
			return;
		}
		
		//通知MAX_HP变化
		MGRoleattUpdate max;
		max.bFloat = false;
		max.changeTime = 0;
		max.nAddType = MGRoleattUpdate::TYPE_SET;
		max.lChangedPlayerID = pTmpPlayer->GetFullID();
		max.nNewValue = pTmpPlayer->GetMaxHP();
		max.nType = RT_HPMAX;
		CMuxMap* pMap = pTmpPlayer->GetCurMap();
		if( pMap )
		{
			pMap->NotifySurrounding<MGRoleattUpdate>(&max, pTmpPlayer->GetPosX(), pTmpPlayer->GetPosY() );
		}

		max.nNewValue = pTmpPlayer->GetBaseMaxHp();
		pTmpPlayer->SendPkg<MGRoleattUpdate>( &max );

		//通知HP变化
		if( pTmpPlayer->GetCurrentHP() > pTmpPlayer->GetMaxHP() )
		{
			pTmpPlayer->SetPlayerHp( pTmpPlayer->GetMaxHP() );

			MGRoleattUpdate currHp;
			currHp.bFloat = false;
			currHp.changeTime = 0;
			currHp.nAddType = MGRoleattUpdate::TYPE_SET;
			currHp.nNewValue = pTmpPlayer->GetCurrentHP();
			currHp.lChangedPlayerID = pTmpPlayer->GetFullID();
			currHp.nType = RT_HP;
			if(NULL != pMap)
				pMap->NotifySurrounding<MGRoleattUpdate>( &currHp, pTmpPlayer->GetPosX(), pTmpPlayer->GetPosY() );
		}
	}
}


void CUnion::AddUnionEffect( MuxBuffProp* pBuffProp )		//根据每个被动技能增加相应的属性
{
	if( NULL == pBuffProp )
		return;
	
	if( pBuffProp->AssistType == BUFF_SP_COMPLEX )
	{
		for( size_t i =0; i< pBuffProp->vecBuffProp.size(); ++i )
		{
			AddSingleBuffProp( pBuffProp->vecBuffProp[i] );
		}
	}else
	{
		AddSingleBuffProp( pBuffProp );
	}
}


void CUnion::AddSingleBuffProp( MuxBuffProp* pBuffProp )
{
	if( NULL == pBuffProp) 
		return;
	
	int nBuffVal = pBuffProp->Value;
	switch( pBuffProp->AssistType )
	{
		case BUFF_PHY_ATTACK_UP:           	//物理攻击的提升
		m_unionAttributeEffect.nPhyAttack += nBuffVal;
		break;
	
	case BUFF_MAG_ATTACK_UP:            	//魔法攻击的提升
		m_unionAttributeEffect.nMagAttack += nBuffVal;
		break;
	
	case BUFF_PHY_DEFEND_UP:          	//物理防御提升
		m_unionAttributeEffect.nPhyDefend += nBuffVal;
		break;
	
	case BUFF_MAG_DEFNED_UP:        	//魔法防御提升
		m_unionAttributeEffect.nMagDefend += nBuffVal;
		break;
		
	case BUFF_PHY_HIT_UP:          	//物理命中提升
		m_unionAttributeEffect.nPhyHitLevel += nBuffVal;
		break;

	case BUFF_MAG_HIT_UP:           	//魔法命中提升
		m_unionAttributeEffect.nMagHitLevel += nBuffVal;
		break;

	case BUFF_PHY_CRI_UP:            	//物理暴击
		m_unionAttributeEffect.nPhyCriLevel += nBuffVal;
		break;

	case BUFF_MAG_CRI_UP:            	//魔法暴击等级上升
		m_unionAttributeEffect.nMagCriLevel += nBuffVal;
		break;

	//case BUFF_STR:            	//增加STR
	//	m_unionAttributeEffect.nStr += nBuffVal;
	//	break;

	//case BUFF_AGI:           	//增加AGI
	//	m_unionAttributeEffect.nAgi += nBuffVal;
	//	break;

	//case BUFF_INT:           	//增加INT
	//	m_unionAttributeEffect.nInt += nBuffVal;
	//	break;
	
	case BUFF_ADD_PHYCRI_DAMAGE:         	//提高物理暴击追加伤害
		m_unionAttributeEffect.nPhyCriDamAddition += nBuffVal;
		break;

	case BUFF_ADD_MAGCRI_DAMAGE:         	//提高魔法暴击追加伤害
		m_unionAttributeEffect.nMagCriDmgAddition += nBuffVal;
		break;

	case BUFF_RATE_PHYDAMAGE_UP:        	//提高物理伤害的百分比,float型
		m_unionAttributeEffect.nAddPhyDamageRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGDAMAGE_UP:      	//提高魔法伤害的百分比
		m_unionAttributeEffect.nAddMagDamageRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_RESIST_FAINT:          	//提高抗击晕的百分比
		m_unionAttributeEffect.fResistFaintRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_RESIST_BONDAGE:         	//提高抗束缚的百分比
		m_unionAttributeEffect.fResistBondageRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_ADD_FAINT:      	//提高击晕的百分比
		m_unionAttributeEffect.fAddFaintRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_ADD_BONDAGE:         	//提高击晕的百分比
		m_unionAttributeEffect.fAddBondageRate += nBuffVal / (float)100;
		break;

	case BUFF_ADD_AOE_RANGE:        	//提高范围攻击的作用范围
		m_unionAttributeEffect.nAddAoeRange += nBuffVal;
		break;

	case BUFF_ADD_ATK_DISTANCE:         	//提高攻击技能的施放距离
		m_unionAttributeEffect.nAddAtkDistance += nBuffVal;
		break;

	case BUFF_RATE_PHYCRI_HIT_UP:          	//提高物理暴击百分比
		m_unionAttributeEffect.fAddPhyCriRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGCRI_HIT_UP:        	//提高魔法暴击百分比
		m_unionAttributeEffect.fAddMagCriRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_PHYHIT_UP:          	//提高物理攻击时的命中百分比
		m_unionAttributeEffect.fAddPhyHitRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGHIT_UP:			//40提高魔法攻击时的命中百分比
		m_unionAttributeEffect.fAddMagHitRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_PHYCRI_DAMAGE_UP:         	//提高物理暴击攻击的伤害比率
		m_unionAttributeEffect.fAddPhyCriDmgRate += nBuffVal / (float)100;
		break;

	case BUFF_RATE_MAGCRI_DAMAGE_UP:          	//提高魔法暴击攻击的伤害比率
		m_unionAttributeEffect.fAddMagCriDmgRate += nBuffVal / (float)100;
		break;

	case BUFF_PHYJOUK_LEVEL:        	//提高物理攻击闪避等级
		m_unionAttributeEffect.fPhyJoukRate += nBuffVal / (float)100;
		break;

	case BUFF_MAGJOUK_LEVEL:          	//提高魔法攻击闪避等级
		m_unionAttributeEffect.fMagJoukRate += nBuffVal / (float)100;
		break;

	case BUFF_MAXHP_UP:
		m_unionAttributeEffect.nMaxHpUp += nBuffVal;
		break;

	default:
		D_ERROR("无效战盟BUFF类型\n");
		break;
	}
}


AttributeEffect* CUnion::GetUnionEffect()
{
	return &m_unionAttributeEffect;
}

void CUnion::ClearUnionEffect()
{
	m_unionAttributeEffect.Reset();
}



bool CUnion::InLowActiveState(void)
{
	//低活跃度状态: 等级为1,且当前活跃度低于该等级需要的活跃度
	if(m_level == 1)
	{
		UnionBaseArg *pBaseArg = CUnionConfigSingle::instance()->GetBaseArg(m_level);
		if((NULL != pBaseArg) && (m_active < pBaseArg->dayexpendActive))
			return true;
	}
		
	return false;
}

void CUnion::RegisterPlayer(CPlayer *pPlayer, const char *unionName, unsigned int ownerUiid, unsigned int unionActive, unsigned short unionLevel)
{
	if(NULL != pPlayer)
	{
		pPlayer->Union(this);
		unionMembers.push_back(pPlayer->GetFullID());
		
		SetName(unionName);
		SetCaption(ownerUiid);
		SetActive(unionActive);
		SetLevel(unionLevel);

		if(unionLevel <= CUnionConfigSingle::instance()->GetClothesLevel())//收回工会时装(工会此前可能降级)
		{
			pPlayer->WithdrawalUnionFashion();
		}
	}

	return;
}

void CUnion::UnregisterPlayer(CPlayer *pPlayer, bool quitUnion)
{
	if(NULL != pPlayer)
	{
		if(quitUnion)//退出战盟
		{
			//未交付的工会任务失败
			pPlayer->MakeUnfinishUnionTaskFail();

			//收回战盟服饰
			pPlayer->WithdrawalUnionFashion();

			//通知周围玩家
			CMuxMap* pMap = pPlayer->GetCurMap();
			if(NULL != pMap)
			{
				MGUnionMemberShowInfo showInfo;
				StructMemSet(showInfo, 0x00, sizeof(showInfo));
				showInfo.showInfos.showInfo.otherPlayerID = pPlayer->GetFullID();
				pMap->NotifySurrounding<MGUnionMemberShowInfo>( &showInfo, pPlayer->GetPosX(), pPlayer->GetPosY() );
			}
			
		}

		PlayerID playerid = pPlayer->GetFullID();
		for(std::vector<PlayerID>::iterator iter = unionMembers.begin(); iter != unionMembers.end(); ++iter)
		{
			if(playerid == *iter)
			{
				unionMembers.erase(iter);
				break;
			}
		}

		pPlayer->Union(NULL);
	}

	return;
}

void CUnion::OnLevelChanage(unsigned short level)
{
	if(level < m_level)//降级处理
	{
		if(level <= CUnionConfigSingle::instance()->GetBadgeLevel())//收回徽章
		{
			StructMemSet(m_badge, 0x00, sizeof(m_badge));
		}

		if(level <= CUnionConfigSingle::instance()->GetClothesLevel())//收回工会时装
		{
			if(unionMembers.size() > 0)
			{
				PlayerID playerid;
				CGateSrv* pGateSrv = NULL;
				CPlayer *pPlayer = NULL;
				
				for(size_t i = 0; i < unionMembers.size(); i++)
				{
					playerid = unionMembers[i];
					CGateSrv* pGateSrv = CManG_MProc::FindProc( playerid.wGID );	
					if(NULL != pGateSrv)
					{
						CPlayer* pPlayer = pGateSrv->FindPlayer( playerid.dwPID );
						if(NULL != pPlayer)
						{
							pPlayer->WithdrawalUnionFashion();
						}	
					}
				}
			}
		}
	}

	//更新
	m_level = level;
	m_showInfo.showInfos.showInfo.level = level;
	
	//通知所有工会成员的周围的玩家
	NotifyUnionShowInfo();
	
	return;
}

void CUnion::OnBadgeChanage(const char *newBadge)
{
	if ( NULL == newBadge )
	{
		return;
	}
	//更新徽章
	SafeStrCpy(m_badge, newBadge);
	SafeStrCpy(m_showInfo.showInfos.showInfo.badge, newBadge);
	m_showInfo.showInfos.showInfo.badgeLen = (USHORT)strlen(m_showInfo.showInfos.showInfo.badge);

	//通知所有工会成员的周围的玩家
	NotifyUnionShowInfo();
}


void CUnion::NotifyUnionShowInfo(void)
{
	if(unionMembers.size() == 0)
		return;

	PlayerID playerid;
	CGateSrv *pGateSrv = NULL;
	CPlayer *pPlayer = NULL;
	for(size_t i = 0; i < unionMembers.size(); i++)
	{
		playerid = unionMembers[i];
		pGateSrv = CManG_MProc::FindProc(playerid.wGID);
		if(NULL != pGateSrv)
		{
			pPlayer = pGateSrv->FindPlayer(playerid.dwPID);
			if(NULL != pPlayer)
			{
				CMuxMap* pMap = pPlayer->GetCurMap();
				if(NULL != pMap)
				{
					MGUnionMemberShowInfo *pShowInfo = GetUnionMemberShowInfo(pPlayer);
					pMap->NotifySurrounding<MGUnionMemberShowInfo>( pShowInfo, pPlayer->GetPosX(), pPlayer->GetPosY() );
				}
			}
		}
	}

	return;
}

int CUnion::ComputeActive(unsigned short mode, int modeArg)
{
	int active = CUnionConfigSingle::instance()->GetActiveGainMode(mode, modeArg);
	if(active > 0)
	{
		if(0 != activeTimesArg.times)//n倍活跃度
		{
			time_t now = ACE_OS::time(NULL);
			if(now < activeTimesArg.expireTime)//有效时间内
			{
				active = active * activeTimesArg.times;
			}
			else
			{
				activeTimesArg.times = 0;
				activeTimesArg.expireTime = 0;
			}
		}	
	}

	return active;
}

int CUnion::ComputePrestige(unsigned short mode, int modeArg)
{
	int prestige = CUnionConfigSingle::instance()->GetPrestigeGainMode(mode, modeArg);
	if(prestige > 0)
	{
		if(0 != prestigeTimesArg.times)//n倍威望
		{
			time_t now = ACE_OS::time(NULL);
			if(now < prestigeTimesArg.expireTime)
			{
				prestige = prestige * prestigeTimesArg.times;
			}
			else
			{
				prestigeTimesArg.times = 0;
				prestigeTimesArg.expireTime = 0;
			}
		}
	}

	return prestige;
}

MGUnionMemberShowInfo* CUnion::GetUnionMemberShowInfo(CPlayer *pPlayer)
{
	if ( NULL == pPlayer )
	{
		return NULL;
	}
	m_showInfo.showInfos.showInfo.otherPlayerID = pPlayer->GetFullID();
	m_showInfo.showInfos.showInfo.isCaption = (pPlayer->GetPlayerUID() == m_captionID) ? true : false;

	return &m_showInfo;
}

CUnionManager::CUnionManager(void)
{

}

CUnionManager::~CUnionManager(void)
{
	for(std::map<unsigned int, CUnion*>::iterator i = m_unionList.begin(); m_unionList.end() != i; i++)
	{
		delete i->second;
		i->second = NULL;
	}

	m_unionList.clear();
}

CUnion * CUnionManager::RetrieveUnion(unsigned int unionID)
{
	CUnion *pUnion = FindUnion(unionID);
	if(NULL != pUnion)
		return pUnion;

	pUnion = NEW CUnion(unionID);
	if(NULL == pUnion)
		return NULL;

	AddUnion(unionID, pUnion);
	return pUnion;
}

//删除工会
void CUnionManager::DestroyUnion(unsigned int unionID)
{
	CUnion *pUnion = FindUnion(unionID);
	if(NULL == pUnion)
		return;

	DeleteUnion(unionID);
	
	delete pUnion;
	pUnion = NULL;
	return;
}


//增加工会
bool CUnionManager::AddUnion(unsigned int unionID, CUnion *pUnion)
{
	if ( (0 == unionID) || (NULL == pUnion) )
	{
		D_ERROR( "CUnionManager::AddUnion, (0 == unionID) || (NULL == pUnion)\n" );
		return false;
	}

	std::map<unsigned int, CUnion*>::iterator iter = m_unionList.find(unionID);
	if ( m_unionList.end() != iter )
	{
		D_ERROR( "错误：AddUnion时，已存在此union:%d\n", unionID );
		return false;
	}

	m_unionList.insert(make_pair(unionID, pUnion));
	return true;
}

//查找工会
CUnion * CUnionManager::FindUnion(unsigned int unionID)
{
	std::map<unsigned int, CUnion*>::iterator i = m_unionList.find(unionID);
	if ( m_unionList.end() == i )
	{
		return NULL;
	}

	return i->second;
}

//删除工会
void CUnionManager::DeleteUnion(unsigned int unionID)
{
	m_unionList.erase(unionID);
	return;
}

//工会数目
size_t CUnionManager::UnionNum(void)
{
	return m_unionList.size();
}
