﻿/** @file UnionConfig.h 
@brief 
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2009-4-15
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef UNION_CONFIG_H
#define UNION_CONFIG_H

#include "../../../Base/PkgProc/CliProtocol.h"
#include "../../../Base/PkgProc/SrvProtocol.h"
#include "../../../Base/aceall.h"
#include <list>
#include <map>


using namespace MUX_PROTO;

#pragma pack(push,1)

//工会基本信息参数（和等级有关）
typedef struct strUnionBaseArg
{
	unsigned short level;//等级
	unsigned short maxPopulation;//人口上限
	unsigned int dayexpendActive;//日消耗活跃度点数
	unsigned int advanceExpendActive;//升级消耗活跃度点数
	unsigned int advanceGainActive;//升级返还的活跃度	
}UnionBaseArg;

//工会可发布任务参数（和等级有关，同一等级可以多个发布）
typedef struct strUnionIssueTaskArg
{
	unsigned short level;//等级
	unsigned int  taskid;//任务id
}UnionIssueTaskArg;

//工会技能
typedef struct strUnionSkillArg
{
	unsigned int id;//技能编号
	unsigned short upgradeLevel;//学习技能所需工会等级
	char name[MAX_NAME_SIZE];//机能名称
	unsigned short type;//技能类型(1：永久2：辅助）
	unsigned short target;// 技能目标
	char skillNote[MAX_NAME_SIZE];//技能注释
	unsigned int linkID;//附加技能编号
	unsigned int periodOfTime;//附加技能持续时间
	unsigned short linkRate; //附加仅能出现几率
	unsigned short publicCDType;//机能所属公共集
	unsigned short level;//技能等级
	unsigned short attDistMin;//最小释放距离
	unsigned short attDistMax;//最大释放距离
	unsigned int ullageMp;//消耗魔法值				
	unsigned int curseTime;//技能施展时间
	unsigned int CoolDown;//机能冷却时间
	unsigned int icon;//技能图标
	unsigned int nextSkillID;//下一技能编号
}UnionSkillArg;

//工会活跃度或者威望兑换参数
typedef struct strUnionExchangeArg
{
	int unit;
	int rate;
}UnionExchangeArg;

#pragma pack(pop)

class CUnionConfig
{
	friend class ACE_Singleton<CUnionConfig, ACE_Null_Mutex>;

public:
	enum ActiveGainMode//获取活跃度的方式
	{
		GM_OR_ITEM = 0, //gm修改或者功能性道具修改
		ON_LINE_TIME = 1,//在线时长
		PLAYER_LEVEL_UP = 2,//玩家升级
		DAILY_TASK_FININSH = 3,//完成日常任务
		CUNTRY_TASK_FINISH = 4,//完成国家任务
		REPEAT_TASK_FINISH = 5,//完成重复任务
		MEMBER_LEAVE = 6 //工会成员离开
	};

	enum PrestigeGainMode//获取威望方式
	{
		GM_OR_ITEM_2 = 0,//gm修改或者功能性道具修改
		COST_STAR = 1,//消耗星钻
		DAILY_TASK_FINISH_PRES = 2,//完成日常任务
		CYCLE_TASK_FINISH = 3,//完成循环任务
		MILITARY_SUCCESSES = 4,//战绩
		UNION_CONTRIBUTE = 5,//捐献
		CALL_TOGETHER = 6 //召集令
	};

private:
	CUnionConfig(void);

	~CUnionConfig(void);

public:
	int Init(void);

	void Release(void);

public:
	//获取基本参数
	UnionBaseArg * GetBaseArg(unsigned short level);

	//获取任务发布参数
	size_t GetIssueTaskArg(unsigned short level, std::list<UnionIssueTaskArg *> &taskList);

	//获取任务有效期参数
	unsigned int GetTaskValidTimeArg(unsigned short interval);

	//获取活跃度参数
	int GetActiveGainMode(unsigned short mode, int modeArg = 0);

	//获取威望参数
	int GetPrestigeGainMode(unsigned short mode, int modeArg = 0);

	// 收回徽章的等级
	unsigned short GetBadgeLevel(void);

	// 收回时装的等级
	unsigned short GetClothesLevel(void);

	//获取创建战盟的花费参数
	UnionCostArg& GetCreateUnionCost(void){return m_unionCostArg;}

private:
	std::map<unsigned short, UnionBaseArg *> m_baseArg; //基本参数

	std::list<UnionIssueTaskArg *> m_issueTaskArg;//发布任务参数

	std::map<unsigned short, unsigned int> m_taskValidTimeArg;//任务有效期参数

	std::map<unsigned short, UnionExchangeArg> m_gainActiveArg;//活跃度参数

	std::map<unsigned short, UnionExchangeArg> m_gainPrestigeArg;//威望参数

	unsigned short m_badgeLevel;// 收回徽章的等级

	unsigned short m_clothesLevel;// 收回时装的等级

	UnionCostArg m_unionCostArg;//创建战盟的消耗参数
};

typedef ACE_Singleton<CUnionConfig, ACE_Null_Mutex> CUnionConfigSingle;


class CUnionSkillManager
{
	friend class ACE_Singleton<CUnionSkillManager, ACE_Null_Mutex>;

private:
	CUnionSkillManager(void);

	~CUnionSkillManager(void);

public:
	int Init(void);

	void Release(void);

public:
	//获取技能信息
	UnionSkillArg * FindUnionSkill(unsigned int skillID);

private:
	std::map<unsigned int, UnionSkillArg *> m_skillList; //工会技能列表
};

typedef ACE_Singleton<CUnionSkillManager, ACE_Null_Mutex> CUnionSkillManagerSingle;

#endif/*UNION_CONFIG_H*/
