﻿#include "UnionConfig.h"
#include "../../../Base/Utility.h"
#include "../../../Base/XmlManager.h"
#include <vector>

CUnionConfig::CUnionConfig(void)
{
	m_badgeLevel = 0;
	m_clothesLevel = 0;
	StructMemSet( m_unionCostArg, 0x00, sizeof(m_unionCostArg));
}

CUnionConfig::~CUnionConfig(void)
{
	Release();
}

int CUnionConfig::Init(void)
{
	TRY_BEGIN;

	const char *pszFileName = "config/guild/guildconfig.xml";

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR("加载文件%s出错\n", pszFileName);
		return -1;
	}

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("配置文件%s可能为空\n", pszFileName);
		return -1;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);

	//编译警告去除，by dzj, 09.07.08, const char *pTemp = NULL;
	CElement *pElement = NULL;
	for(std::vector<CElement *>::iterator i = childElements.begin(); i != childElements.end(); i++)
	{	
		pElement = *i;
		if( strcmp(pElement->Name(), "GuildBasic" ) == 0 )
		{
			UnionBaseArg *pArg = NEW UnionBaseArg;
			if(NULL == pArg)
				return -1;

			pArg->level = STRING_TO_NUM(pElement->GetAttr("level"));
			pArg->maxPopulation = STRING_TO_NUM(pElement->GetAttr("MaxPopulation"));
			pArg->dayexpendActive = STRING_TO_NUM(pElement->GetAttr("DayExpendActive"));
			pArg->advanceExpendActive = STRING_TO_NUM(pElement->GetAttr("AdvanceExpendActive"));
			pArg->advanceGainActive = STRING_TO_NUM(pElement->GetAttr("AdvanceGainActive"));

			m_baseArg.insert(make_pair(pArg->level, pArg));
			continue;
		}
		
		if( strcmp(pElement->Name(), "PostTask" ) == 0)
		{	
			UnionIssueTaskArg *pArg = NEW UnionIssueTaskArg;
			if(NULL == pArg)
				return -1;

			pArg->level = STRING_TO_NUM(pElement->GetAttr("GuildLevel"));
			pArg->taskid = STRING_TO_NUM(pElement->GetAttr("TaskId"));

			m_issueTaskArg.push_back(pArg);
			continue;
		}

		if( strcmp(pElement->Name(), "PostTaskExpend" ) == 0 )
		{
			unsigned short duration = STRING_TO_NUM(pElement->GetAttr("Duration"));
			unsigned int expendActive = STRING_TO_NUM(pElement->GetAttr("ExpendActive"));

			m_taskValidTimeArg.insert(make_pair(duration, expendActive));
			continue;
		}

		if( strcmp(pElement->Name(), "GainActiveWay" ) == 0)
		{
			unsigned short mode = STRING_TO_NUM(pElement->GetAttr("way"));			
			const char *pGainMode = pElement->GetAttr("GainActive");
			if(NULL != pGainMode)
			{
				char temp[50];
				SafeStrCpy(temp, pGainMode);

				UnionExchangeArg arg;
				StructMemSet( arg, 0x00, sizeof(arg));
				char *pFind = strtok(temp,":");
				if(NULL != pFind)
				{
					arg.unit = atoi(pFind);
					pFind = strtok(NULL, ":");
					if(NULL != pFind)
						arg.rate = atoi(pFind);
				}

				m_gainActiveArg.insert(make_pair(mode, arg));
			}

			continue;
		}

		if( strcmp(pElement->Name(), "GainPrestigeWay" ) == 0)
		{
			unsigned short mode = STRING_TO_NUM(pElement->GetAttr("way"));
			const char *pGainMode = pElement->GetAttr("GainPrestige");
			if(NULL != pGainMode)
			{
				char temp[50];
				SafeStrCpy(temp, pGainMode);

				UnionExchangeArg arg;
				StructMemSet( arg, 0x00, sizeof(arg));
				char *pFind = strtok(temp,":");
				if(NULL != pFind)
				{
					arg.unit = atoi(pFind);
					pFind = strtok(NULL, ":");
					if(NULL != pFind)
						arg.rate = atoi(pFind);
				}

				m_gainPrestigeArg.insert(make_pair(mode, arg));
			}

			continue;
		}

		if( strcmp(pElement->Name(), "LevelDown" ) == 0 )
		{
			m_badgeLevel = STRING_TO_NUM(pElement->GetAttr("BadgeLevel"));
			m_clothesLevel = STRING_TO_NUM(pElement->GetAttr("ClothesLevel"));

			continue;
		}

		if( strcmp(pElement->Name(), "CreateCost") == 0)
		{
			m_unionCostArg.money = STRING_TO_NUM(pElement->GetAttr("Money"));
			m_unionCostArg.itemID = STRING_TO_NUM(pElement->GetAttr("Item"));
			m_unionCostArg.itemCount = 1;

			continue;
		}
	}

	return 0;
	TRY_END;
	return -1;
}

void CUnionConfig::Release(void)
{
	for(std::map<unsigned short, UnionBaseArg *>::iterator i = m_baseArg.begin(); m_baseArg.end() != i; i++)
	{
		delete i->second;
		i->second = NULL;
	}
	m_baseArg.clear();

	for(std::list<UnionIssueTaskArg *>::iterator i = m_issueTaskArg.begin(); m_issueTaskArg.end() !=i; i++)
	{
		delete *i;
		*i = NULL;
	}
	m_issueTaskArg.clear();
}

UnionBaseArg * CUnionConfig::GetBaseArg(unsigned short level)
{
	std::map<unsigned short, UnionBaseArg *>::iterator i = m_baseArg.find(level);
	if(m_baseArg.end() != i)
		return i->second;

	return NULL;
}

size_t CUnionConfig::GetIssueTaskArg(unsigned short level, std::list<UnionIssueTaskArg *> &taskList)
{
	for(std::list<UnionIssueTaskArg *>::iterator i = m_issueTaskArg.begin(); m_issueTaskArg.end() !=i; i++)
	{
		if ( NULL == (*i) )
		{
			continue;
		}
		if(level == (*i)->level)
		{
			taskList.push_back(*i);
		}
	}

	return taskList.size();
}

unsigned int CUnionConfig::GetTaskValidTimeArg(unsigned short interval)
{
	std::map<unsigned short, unsigned int >::iterator i = m_taskValidTimeArg.find(interval);
	if(i != m_taskValidTimeArg.end())
		return i->second;

	return 0;
}

int CUnionConfig::GetActiveGainMode(unsigned short mode, int modeArg)
{
	std::map<unsigned short, UnionExchangeArg >::iterator i = m_gainActiveArg.find(mode);
	if(i != m_gainActiveArg.end())
	{
		if(0 == i->second.rate)
			return i->second.unit;
		else
			return modeArg * (i->second.unit)/(i->second.rate);
	}

	return 0;
}

int CUnionConfig::GetPrestigeGainMode(unsigned short mode, int modeArg)
{
	std::map<unsigned short, UnionExchangeArg >::iterator i = m_gainPrestigeArg.find(mode);
	if(i != m_gainPrestigeArg.end())
	{
		if(0 == i->second.rate)
			return i->second.unit;
		else
			return modeArg * (i->second.unit)/(i->second.rate);
	}

	return 0;
}

unsigned short CUnionConfig::GetBadgeLevel(void)
{
	return m_badgeLevel;
}

unsigned short CUnionConfig::GetClothesLevel(void)
{
	return m_clothesLevel;
}

CUnionSkillManager::CUnionSkillManager(void)
{

}

CUnionSkillManager::~CUnionSkillManager(void)
{
	Release();
}

int CUnionSkillManager::Init(void)
{
	TRY_BEGIN;

	const char *pszFileName = "config/common/guildskill.xml";

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR("加载文件%s出错\n", pszFileName);
		return -1;
	}

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("配置文件%s可能为空\n", pszFileName);
		return -1;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);

	//编译警告去除，by dzj, 09.07.08, const char *pTemp = NULL;
	CElement *pElement = NULL;
	for(std::vector<CElement *>::iterator i = childElements.begin(); i != childElements.end(); i++)
	{	
		pElement = *i;
		if( strcmp(pElement->Name(), "skill" ) == 0 )
		{
			UnionSkillArg *pSkill = NEW UnionSkillArg;
			if(NULL == pSkill)
				return -1;

			pSkill->id = STRING_TO_NUM(pElement->GetAttr("nID"));
			pSkill->upgradeLevel =STRING_TO_NUM(pElement->GetAttr("nUpgradeLevel"));
			SafeStrCpy(pSkill->name, pElement->GetAttr("nName"));
			pSkill->type = STRING_TO_NUM(pElement->GetAttr("nType"));
			pSkill->target = STRING_TO_NUM(pElement->GetAttr("nTarget"));
			SafeStrCpy(pSkill->skillNote, pElement->GetAttr("nSkillNote"));
			pSkill->linkID =  STRING_TO_NUM(pElement->GetAttr("nLinkID"));
			pSkill->periodOfTime = STRING_TO_NUM(pElement->GetAttr("nPeriodOfTime"));
			pSkill->linkRate =STRING_TO_NUM(pElement->GetAttr("nLinkRate"));
			pSkill->publicCDType = STRING_TO_NUM(pElement->GetAttr("PublicCDType"));
			pSkill->level = STRING_TO_NUM(pElement->GetAttr("nLevel"));
			pSkill->attDistMin = STRING_TO_NUM(pElement->GetAttr("nAttDistMin"));
			pSkill->attDistMax =STRING_TO_NUM(pElement->GetAttr("nAttDistMax"));
			pSkill->ullageMp = STRING_TO_NUM(pElement->GetAttr("nUllageMp"));
			pSkill->curseTime = STRING_TO_NUM(pElement->GetAttr("nCurseTime"));
			pSkill->CoolDown = STRING_TO_NUM(pElement->GetAttr("nCoolDown"));
			pSkill->icon = STRING_TO_NUM(pElement->GetAttr("nIcon"));
			pSkill->nextSkillID = STRING_TO_NUM(pElement->GetAttr("nNextSkillID"));

			m_skillList.insert(make_pair(pSkill->id, pSkill));
		}
	}

	return 0;
	TRY_END;
	return -1;
}

void CUnionSkillManager::Release(void)
{
	for(std::map<unsigned int, UnionSkillArg *>::iterator i = m_skillList.begin(); i != m_skillList.end(); i++)
	{
		delete i->second;
		i->second = NULL;
	}
	m_skillList.clear();

	return;
}

UnionSkillArg * CUnionSkillManager::FindUnionSkill(unsigned int skillID)
{
	std::map<unsigned int, UnionSkillArg *>::iterator i = m_skillList.find(skillID);
	if(m_skillList.end() != i)
		return i->second;

	return NULL;
}
