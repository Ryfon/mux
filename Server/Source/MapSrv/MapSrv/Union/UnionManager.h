﻿/** @file UnionManager.h 
@brief 
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2009-4-7
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef UNION_MANAGER_H
#define UNION_MANAGER_H


#include "../../../Base/PkgProc/CliProtocol.h"
#include "UnionConfig.h"
#include <map>
#include <set>
#include <vector>
#include "../BattleUtility.h"
#include "../MuxSkillConfig.h"
#include "../MuxPlayerSkill.h"



using namespace MUX_PROTO;


//前置声明
class	CUnionManager;
class	CMuxPlayerSkill;

class CUnion
{
	friend class CUnionManager;

	typedef struct strActiveAndPrestiveNTimesArg
	{
		BYTE times;//倍数
		unsigned int expireTime;//终止时间
	}ActiveAndPrestiveNTimesArg;

private:
	CUnion(unsigned int unionID);

	~CUnion(void);

public:
	//设置名字
	void SetName(const char *pName);

	//设置工会会长
	void SetCaption(unsigned int roleId);

	// 设置工会活跃度
	void SetActive(unsigned int val);

	// 设置威望
	void SetPrestige(unsigned int val);

	// 设置工会等级
	void SetLevel(unsigned short level);

	// 设置工会发布任务
	void SetIssuedTasks(time_t taskEndTime);

	// 设置工会技能
	size_t SetSkills(const unsigned int *skills);
	void ReleaseSkills();

	//设置徽章
	void SetBadge(const char* newBadge);

	//设置活跃度n倍
	void SetNTimesActive(BYTE times, unsigned int expireTime);

	//设置威望n倍
	void SetNTimesPrestige(BYTE times, unsigned int expireTime);

	// 是否是公会会长
	bool IsUnionLeader(unsigned int roleID){return roleID == m_captionID;}

public:
	// 获取已发布任务列表	
	std::set<unsigned int>& GetIssuedTasks(void);

	// 获取工会技能列表
	std::map<unsigned int, CMuxPlayerSkill*> & GetSkills(void);

	// 获取指定技能的参数信息
	CMuxPlayerSkill* GetSkillArgsByID(unsigned int skillID);

	//获取当前活跃度
	unsigned int GetActive(void);

	//获取当前等级
	unsigned short GetLevel(void){ return m_level;}

	//获取当前威望
	unsigned int GetPrestige(void);

	//获取工会名字
	const char* GetName(void);

	// 判读工会任务是否发布
	bool CheckTaskHaveIssue(void);

	// 获取工会任务截止时间
	time_t GetTaskEndTime(void);

	void InitUnionAttributeEffect();	//初试化被动技能影响属性列表
	void NotifyUnionSkillChange();		//技能变化 影响显示部分的通知

	void AddUnionEffect( MuxBuffProp* pArg );		//根据每个被动技能增加相应的属性
	void AddSingleBuffProp( MuxBuffProp* pBuffProp );

	void ClearUnionEffect();		//清除工会技能的影响
	AttributeEffect* GetUnionEffect();		//获得工会技能对人物的影响

	//是否处于低活跃度（不能使用工会技能，工会仓库）
	bool InLowActiveState(void);

	//注册玩家
	void RegisterPlayer(CPlayer *pPlayer, const char *unionName, unsigned int ownerUiid, unsigned int unionActive, unsigned short unionLevel);

	//取消玩家注册
	void UnregisterPlayer(CPlayer *pPlayer, bool quitUnion = false);

	//计算活跃度
	int ComputeActive(unsigned short mode, int modeArg = 0);

	//计算威望
	int ComputePrestige(unsigned short mode, int modeArg = 0);

	MGUnionMemberShowInfo *GetUnionMemberShowInfo(CPlayer *pPlayer);

private:
	//等级变化处理
	void OnLevelChanage(unsigned short level);

	//徽章变化处理
	void OnBadgeChanage(const char *pBadge);

	//通知显示信息变化
	void NotifyUnionShowInfo(void);

private:
	unsigned int m_id;//工会ID
	char m_name[MAX_UNIONNAME_LEN];//工会名字
	unsigned int m_captionID;//工会队长(对应角色ID)
	unsigned int m_active;//工会活跃度
	unsigned int m_prestige;//工会威望
	unsigned short m_level;//工会等级
	char m_badge[200];//工会徽章

	MGUnionMemberShowInfo m_showInfo;

	std::set<unsigned int> m_issuedTasks;//工会已发布任务列表
	std::map<unsigned int, CMuxPlayerSkill* > m_skills;//工会技能列表

	bool m_haveIssuedTask;//是否已发布
	time_t m_issuedTasksEndTime;//已发布任务的结束时间

	AttributeEffect m_unionAttributeEffect;

	std::vector<PlayerID> unionMembers;//工会成员列表

	ActiveAndPrestiveNTimesArg activeTimesArg;//工会活跃度n倍参数
	ActiveAndPrestiveNTimesArg prestigeTimesArg;//工会威望n被参数
};


class CUnionManager
{
	friend class ACE_Singleton<CUnionManager, ACE_Null_Mutex>;

private:
	CUnionManager(void);

	~CUnionManager(void);

public:
	//查找工会,没有则创建
	CUnion * RetrieveUnion(unsigned int unionID);

	//删除工会
	void DestroyUnion(unsigned int unionID);

	//工会数目
	size_t UnionNum(void);

private:
	//增加工会
	bool AddUnion(unsigned int unionID, CUnion *pUnion);

	//查找工会
	CUnion * FindUnion(unsigned int unionID);

	//删除工会
	void DeleteUnion(unsigned int unionID);

private:
	std::map<unsigned int, CUnion*> m_unionList;
};

typedef ACE_Singleton<CUnionManager, ACE_Null_Mutex>  CUnionManagerSingle;

#endif/*UNION_MANAGER_H*/
