﻿#include "GMCmdManager.h"
#include "Player/Player.h"
#include "Player/MuxPet.h"
#include "monster/monster.h"
#include "string.h"
#include "stdio.h"
#include "PlayerManager.h"
#include "Item/ItemBuilder.h"
#include "Lua/FightCalScript.h"
#include "MuxMap/muxmapbase.h"
#include "ProfileAnalyzeDeclare.h"
#include "Rank/rankeventmanager.h"
#include "Lua/BattleScript.h"
#include "RaceManager.h"

#include "monster/manmonstercreator.h"
#ifdef OPEN_PUNISHMENT
	#include "Activity/PunishmentManager.h"
#endif //OPEN_PUNISHMENT

#include "MuxMap/manmuxcopy.h"
#include "MuxMap/mannormalmap.h"

using namespace MUX_PROTO;

extern bool g_bGmShield;

#ifdef ACE_WIN32
	#pragma warning(disable:4996) //屏蔽当前GMCmdManager.cpp的4996警告
#else	//ACE_WIN32
	
#endif  //ACE_WIN32

std::map<std::string, CGMCmdManager::PFN_Fun> CGMCmdManager::m_GMCmdList;
bool CGMCmdManager::m_bGmShield;
unsigned int CGMCmdManager::m_rsInterval;
std::map<std::string, CGMCmdManager::PREQ_Fun> CGMCmdManager::m_mapReqGmCmd;

int CGMCmdManager::GMCmdInit(void)
{
	RegisterGMCmd("set", SetAttribute);
	RegisterGMCmd("add", AddAttribute);
	RegisterGMCmd("addskill", AddSkill);
	RegisterGMCmd("king", King);
	RegisterGMCmd("unking",Unking);
	RegisterGMCmd("createnpc", CreateNPC);//参数形式：NPC类型ID，NPC位置X，NPC位置Y
	RegisterGMCmd("delnpc", DestoryNPC);
	RegisterGMCmd("moveto", MoveTo);//同地图内跳转；
	RegisterGMCmd("goto", Go);
	RegisterGMCmd("drag", Drag);
	RegisterGMCmd("hide", Hide);
	RegisterGMCmd("unhide", Unhide);
	RegisterGMCmd("getattr", GetAttribute);
	RegisterGMCmd("make",CreateItem);//创建道具的GM命令
	RegisterGMCmd("srvversion", GetVersion);
	RegisterGMCmd("monsterstate", GetMonsterState);
	RegisterGMCmd("playerbuff", AddPlayerBuff);
	RegisterGMCmd("monsterbuff", AddMonsterBuff);
	RegisterGMCmd("LevelUpItem", LevelUpItem );
	RegisterGMCmd("ChangeItem",ChangeItem );
	RegisterGMCmd("threatliststart",ThreatListStart );
	RegisterGMCmd("threatlistend",ThreatListEnd );
	RegisterGMCmd("iteminfo", GetItemInfo);
	RegisterGMCmd( "changeitemaddid", ChangeItemAddID );
	RegisterGMCmd("updatespt", UpdateScript);
	RegisterGMCmd("changeitemlevel",ChangeItemLevel );
	RegisterGMCmd("selfpos", GetSelfPos ); //查询玩家自身位置；
	RegisterGMCmd("movingplayer", GetMapMovingPlayer );//查询地图上的移动玩家信息；
	RegisterGMCmd("viewdebug", ViewMapDebugInfo ); //查看地图相关调试信息；
	RegisterGMCmd("cleartask", ClearTaskHistory );//清除任务记录
	RegisterGMCmd("goodevilpoint",&QueryGoodEvilPoint );//查询善恶值
	RegisterGMCmd("setgoodevilpoint",&SetGoodEvilPoint );//设置善恶值
	RegisterGMCmd("goodeviltime",&ShowGoodEvilPointTime );//显示善恶时间
	RegisterGMCmd("reloadaispt", &ReloadAIScript );//重载AI脚本；
	//RegisterGMCmd("getplayersur", &GetPlayerSur );//查找指定帐号玩家位置；
	RegisterGMCmd("report", &ShowProfileAnalyze );//打印函数分析的报表
	RegisterGMCmd("openanalyze",&OpenProfileSwitch );//打开性能分析开关
	RegisterGMCmd("closeanalyze",&TurnOffProfileSwitch );//关闭性能分析开关
	RegisterGMCmd("swichmap", &SwichMapReq );
#ifdef OPEN_PUNISHMENT
	RegisterGMCmd("startpunishment", &StartPunishment );	//开始天谴
	RegisterGMCmd("endpunishment", &EndPunishment );		//关闭天谴
	RegisterGMCmd("punishplayer", &PunishPlayer );		//天谴玩家
#endif
	RegisterGMCmd("finishtask", &GMFinishTask );//设置玩家已完成任务但尚未完成提交；
	RegisterGMCmd("addactive", &AddUnionActive );//修改玩家所属工会的活跃度
	RegisterGMCmd("addprestige", &AddUnionPrestige );//修改工会威望
	RegisterGMCmd("addpetexp", &AddPetExp );//增加宠物经验
	RegisterGMCmd("setpethunger", &SetPetHunger );//设置宠物饥饿度

	RegisterGMCmd("showplate", &ShowProtectPlate );
	RegisterGMCmd("addcityexp", &AddCityExp );
	RegisterGMCmd("getcityinfo", &GetCityInfo);
	RegisterGMCmd("givetask", &GiveTask);		//给予玩家任务
	RegisterGMCmd("submittask", &SubmitTask);		//把任务设置成完成

	RegisterGMCmd("givepunishtask", &GivePunishTask);		//给予天谴任务
	RegisterGMCmd("finishpunishtask", &FinishPunishTask);		//把天谴任务设置成完成
	RegisterGMCmd("giveuppunishtask", &GiveUpPunishTask);		//放弃天谴任务
	RegisterGMCmd("showpethunger", &ShowPetHunger);		//放弃天谴任务
	RegisterGMCmd("kickallplayer", &KickAllPlayer );	//踢除所有玩家
	RegisterGMCmd("setrace", SetPlayerRace );	//设置玩家种族(临时)
	RegisterGMCmd("setluckysystem", SetLuckySystem );	//设置玩家种族(临时)
	RegisterGMCmd("sendstr", SendSpecialString );	//设置玩家种族(临时)
#ifdef ITEM_NPC
	RegisterGMCmd("ipstatus", ChangeItemSeqStatus );	//设置玩家种族(临时)
#endif //ITEM_NPC
	RegisterGMCmd("showstorage", OnShowStoragePlate );//弹出玩家的托盘

	RegisterGMCmd("trycopy", &OnTryCopy );//创建一个指定副本，并将该玩家加入此副本;
	RegisterGMCmd("trypcopy", &OnTryPCopy );//进入指定的伪副本;
	RegisterGMCmd("leavecopy", &OnLeaveCopy );//玩家离开当前副本;

	RegisterGMCmd("getpetskill", &OnGetAllPetSkill );//获得所有宠物技能
	RegisterGMCmd("rbs", &ReloadBattleScript );//重载战斗脚本
	RegisterGMCmd("raceking" ,&BeRaceMaster );
	RegisterGMCmd("raceplate", &ShowRaceMasterPlate );
	RegisterGMCmd("racemsg", &RaceMasterNewPublicMsg );
	RegisterGMCmd("racewar", &RaceMasterDeclareWar );
	RegisterGMCmd("ttwarrebirth", &TestWarRebirthPt);
	RegisterGMCmd("staw", &StartAttackWar);		//开启攻城战
	//RegisterGMCmd("actwflag", &ActivateCityWar);		//激活攻城战
	RegisterGMCmd("csn", &ChangeSelfName );		//更改自己的姓名
	RegisterGMCmd("setcr", &SetCreator );		//设置生成器
	RegisterGMCmd("addwt", &AddWarTimer );		//设置生成器
	RegisterGMCmd("ttblockquad", &TestBlockQuad);		//　　　　　　　　
	RegisterGMCmd("setrsinterval", &SetRsInterval );		//　
	RegisterGMCmd("selectking", &SelectRaceMaster );//选择族长的命令
	RegisterGMCmd("createrb", &CreateRookieBreak );	//新手闪烁
	RegisterGMCmd("mapbuff", &CreateMapBuff );	//地图buff
	RegisterGMCmd("selrole", &ReturnSelRole ); //返回角色选择

	RegisterGMCmd("modpwstate", &ModifyPWState);//修改伪线状态
	RegisterGMCmd("modpwmapstate", &ModifyPWMapState);//修改伪线地图状态
	RegisterGMCmd("shiftpw", &ShiftPW);//切线
	RegisterGMCmd("kickoffpw", &KickOffPW);//踢线
	
	m_bGmShield = g_bGmShield;	//是否屏蔽GM命令 
	m_rsInterval = 3;	//默认3秒
	InitReqGmCmd();		//初始化请求系的GM命令
	return 0;	
}

int CGMCmdManager::InitReqGmCmd()		//对转发来的GM命初始化
{
	RegisterReqGmCmd("kickplayer", &OnKickPlayerRequest );
	return 0;
}

bool CGMCmdManager::IsGmCmd(const char* szString, std::string &strCmd, std::vector<std::string> &cmdArgs)
{
	//是否包含GM指令关键字.
	///////////////////////////////////////////////
	//by dzj, 08.07.16
	//char *pAt = strchr(szCmd, '@');
	//if((NULL == pAt) || ( pAt != szCmd )) 
	//	return false;
	if ( *szString != '@' )
	{
		//首字符不是'@'
		return false;
	}
	//by dzj, 08.07.16
	///////////////////////////////////////////////

	static char szCmd[256];
	SafeStrCpy(szCmd, szString);

	// 是否有GM指令
	char *pCmd = szCmd + 1;
	pCmd = strtok(pCmd, " ");
	if( NULL==pCmd ) 
	{
		return false;
	}	
	strCmd = pCmd;

    //获取GM指令的参数
	char *pCmdArg = strtok( NULL, " " );
	while(pCmdArg != NULL)
	{
		cmdArgs.push_back( pCmdArg );
		pCmdArg = strtok( NULL, " " );
	}

	return true;
}

int CGMCmdManager::DealGMCmd(const char *pszCmd, std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}

	//先判断是否是需要转发的GM命令，是的话直接返回
	//if( m_bGmShield && ( 0 != strcmp( pGameManager->GetAccount(), "dengzijian") ) )
	if( m_bGmShield && !pGameManager->IsGmAccount() )
	{
		return GM_CMD_ERROR;
	}

	PREQ_Fun pReqFun = FindReqGmCmd( pszCmd );
	if( NULL != pReqFun )
	{
		return GM_TRANSFER;
	}


	PFN_Fun pFun = FindGMCmd( pszCmd ); 
	if ( NULL == pFun )
	{
		return GM_CMD_ERROR;
	}

	return pFun( cmdArgs, pGameManager );
}


int CGMCmdManager::DealReqGmCmd(const char *pszCmd, std::vector<std::string> &cmdArgs, const PlayerID& requestPlayerID )
{
	PREQ_Fun pFun = FindReqGmCmd( pszCmd );
	if( NULL == pFun )
	{
		return GM_CMD_ERROR;
	}

	return pFun( cmdArgs, requestPlayerID );
}


CGMCmdManager::PFN_Fun CGMCmdManager::FindGMCmd( const char *pszCmd )
{
	std::map<std::string, PFN_Fun>::iterator i = m_GMCmdList.find( pszCmd );
	if ( i != m_GMCmdList.end() )
	{
		return i->second;
	} else {
		return NULL;
	}
}


CGMCmdManager::PREQ_Fun CGMCmdManager::FindReqGmCmd(const char *pszCmd)
{
	std::map<std::string, PREQ_Fun>::iterator i = m_mapReqGmCmd.find( pszCmd );
	if ( i != m_mapReqGmCmd.end() )
	{
		return i->second;
	} else {
		return NULL;
	}
}

void CGMCmdManager::RegisterGMCmd(const char* pszCmd, PFN_Fun CmdFun)
{
	std::map<std::string, PFN_Fun>::iterator i = m_GMCmdList.find(pszCmd);
	if ( i != m_GMCmdList.end() )
	{
		D_WARNING( "重复注册的GM命令:%s\n", pszCmd );
	} else {
		m_GMCmdList[pszCmd] = CmdFun;
	}

	return;
}

//注册请求的GM命令
void CGMCmdManager::RegisterReqGmCmd(const char* pszCmd, PREQ_Fun CmdFun)
{
	std::map<std::string, PREQ_Fun>::iterator i = m_mapReqGmCmd.find(pszCmd);
	if ( i != m_mapReqGmCmd.end() )
	{
		D_WARNING( "重复注册的GM命令:%s\n", pszCmd );
	} else {
		m_mapReqGmCmd[pszCmd] = CmdFun;
	}

	return;	
}


int CGMCmdManager::SetAttribute( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	////////GM参数有效性验证

	//必须是2个参数或者3个参数
	if ( !(cmdArgs.size() == 3 || cmdArgs.size() == 2) )
	{
		pGameManager->SendSystemChat( "GM命令：参数数目错误" );
		return GM_CMD_ERROR;
	}

	//修改的属性是否是在范围之内
	EAttribute usAttributeNo = (EAttribute)atoi(cmdArgs[0].c_str());
	if(usAttributeNo > ATTR_MAX || usAttributeNo == 0)
	{
		pGameManager->SendSystemChat( "GM命令：属性范围错误" );
		return GM_CMD_ERROR;
	}

	unsigned int uiData = (unsigned int) atoi(cmdArgs[1].c_str());
	//atoi失败的话会返回0,此时应返回参数错误
	if( uiData == 0 )
	{
		pGameManager->SendSystemChat( "GM命令：参数数字错误" );
		return GM_CMD_ERROR;
	}

	if ( (ATTR_HP == usAttributeNo) && ( uiData == 987654321 ) )
	{
		pGameManager->SendSystemChat( "特殊HP设置，将玩家置死!" );		
		uiData = 0;
	}	
	
	//3参通过名字来查找
	CPlayer* pTargetPlayer = NULL;
	if( cmdArgs.size() == 3 )
	{
		pTargetPlayer = CPlayerManager::FindPlayerByAccount( cmdArgs[2].c_str() );
		if(NULL == pTargetPlayer)
		{
			pGameManager->SendSystemChat( "GM命令:找不到指定玩家" );
			return GM_CMD_ERROR;
		}
	} else {
		pTargetPlayer = pGameManager;
	}
	
	_ChangeAttribute(pTargetPlayer, TYPE_SET, usAttributeNo, uiData);
		
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::AddAttribute( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是2个参数或者3个参数
	if ( !(cmdArgs.size() == 3 || cmdArgs.size() == 2) )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错" );
		return GM_CMD_ERROR;
	}
	
	//0或大于总数的参数都不接受
	EAttribute usAttributeNo = (EAttribute)atoi(cmdArgs[0].c_str());
	if(usAttributeNo > ATTR_MAX || usAttributeNo == 0)
	{
		pGameManager->SendSystemChat( "GM指令指定属性类型错" );
		return GM_CMD_ERROR;
	}
	
	unsigned int uiData = (unsigned int) atoi(cmdArgs[1].c_str()); 
	//atoi失败的话会返回0,此时应返回参数错误
	if( uiData == 0 )
	{
		pGameManager->SendSystemChat( "GM指令指定数字错误" );
		return GM_CMD_ERROR;
	}
		
	CPlayer* pTargetPlayer = NULL;
	//3参通过名字来查找
	if( cmdArgs.size() == 3 )
	{
		pTargetPlayer = CPlayerManager::FindPlayerByAccount( cmdArgs[2].c_str() );
		if(NULL == pTargetPlayer)
		{
			pGameManager->SendSystemChat( "GM指令找不到指定玩家" );
			return GM_CMD_ERROR;
		}
	} else {
		//2参则就是自己
		pTargetPlayer = pGameManager;
	}
		
	_ChangeAttribute(pTargetPlayer, TYPE_ADD, usAttributeNo, uiData);
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::AddSkill( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddSkill，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}
	
	//必须是1个参数或者2个参数
	if ( !(cmdArgs.size() == 1 || cmdArgs.size() == 2) )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}
	
	unsigned int uiSkillID = (unsigned int) atoi(cmdArgs[0].c_str());

	CPlayer* pTargetPlayer = NULL;	//
	if( cmdArgs.size() == 2 )
	{
		pTargetPlayer =  CPlayerManager::FindPlayerByAccount( cmdArgs[1].c_str() );  //若没有2参直接用会导致崩溃。。。
		if(NULL == pTargetPlayer)
		{
			pGameManager->SendSystemChat( "GM指令找不到指定的玩家" );
			return GM_CMD_ERROR;
		}
	} else {
		//1参则就是自己
		pTargetPlayer = pGameManager;
	}

	//如果玩家已经拥有该技能则不能添加
	if( !pTargetPlayer->AddSkillByID( uiSkillID ) )
	{
		pGameManager->SendSystemChat( "该技能该玩家已拥有或非该玩家职业的技能" );
		return GM_CMD_ERROR;
	}
	
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::King( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令King，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	pGameManager->SetAttackFlag( false );
	pGameManager->SendSystemChat( "GM指令:角色无敌成功\n" );
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::Unking( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令Unking，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	pGameManager->SetAttackFlag( true );
	pGameManager->SendSystemChat( "GM指令:角色解除无敌成功\n" );
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::CreateNPC( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令CreateNPC，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.size() != 3 ) //参数形式：NPC类型ID，NPC位置X，NPC位置Y
	{
		pGameManager->SendSystemChat( "GM命令CreateNPC参数数目出错\n" );
		return GM_CMD_ERROR;
	}

	CNewMap* pMap = pGameManager->GetCurMap();
	if ( NULL == pMap )
	{
		pGameManager->SendSystemChat( "GM所在地图空，无法创建NPC\n" );
	}

	unsigned long dwNPCNo = atoi(cmdArgs[0].c_str());
	TYPE_POS dwX = atoi(cmdArgs[1].c_str());
	TYPE_POS dwY = atoi(cmdArgs[2].c_str());

	CMonster* pMonsterCreated = NULL;
	if ( ! CCreatorIns::SinglePtRefreshNoCreator( dwNPCNo, pMap, dwX, dwY, 0, pMonsterCreated, pGameManager ) )
	{
		pGameManager->SendSystemChat( "无法在指定点创建指定NPC\n" );
	} else {
		pGameManager->SendSystemChat( "在指定点创建NPC成功\n" );
	}

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::DestoryNPC( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令DestoryNPC，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	return GM_CMD_SUCCESS;
}

///GM命令，同地图内跳转；
int CGMCmdManager::MoveTo( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令MoveTo，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( !(cmdArgs.size() == 2 || cmdArgs.size() == 3) )  //3参为地图号 POSX POSY
	{
		pGameManager->SendSystemChat( "GM命令参数数目出错\n" );
		return GM_CMD_ERROR;
	}

	if( cmdArgs.size() == 2 )
	{
		unsigned short targetX = atoi( cmdArgs[0].c_str() );//目标点格子X坐标；
		unsigned short targetY = atoi( cmdArgs[1].c_str() );//目标点格子Y坐标；

		CMuxMap* pMap = pGameManager->GetCurMap();
		if ( NULL == pMap )
		{
			pGameManager->SendSystemChat( "GM命令执行错：当前不在地图内\n" );
			return GM_CMD_ERROR;
		}

		bool isMoveOk = pMap->OnPlayerInnerJump( pGameManager, targetX, targetY );
		if ( !isMoveOk )
		{
			pGameManager->SendSystemChat( "GM命令执行错：同地图跳转失败\n" );
			return GM_CMD_ERROR;
		} else {
			pGameManager->SendSystemChat( "GM命令执行成功：同地图跳转\n" );
		}

		return GM_CMD_SUCCESS;
	}

	if( cmdArgs.size() == 3 )
	{
		unsigned short mapID = atoi( cmdArgs[0].c_str() );		//地图ID号
		unsigned short targetX = atoi( cmdArgs[1].c_str() );	//目标点格子X坐标；
		unsigned short targetY = atoi( cmdArgs[2].c_str() );	//目标点格子Y坐标；
		pGameManager->IssueSwitchMap( mapID, targetX, targetY );
		D_DEBUG( "玩家%s跳地图，目标：%d(%d,%d)\n", pGameManager->GetAccount(), mapID, targetX, targetY );
		return GM_CMD_SUCCESS;
	}
	return GM_CMD_ERROR;
}

int CGMCmdManager::Go( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令Go，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::Drag( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令Drag，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::Hide( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令Hide，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::Unhide( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令Unhide，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::GetMonsterState( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetMonsterState，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是1,那么出错
	if( cmdArgs.size() != 1)
	{
		pGameManager->SendSystemChat("GM命令参数数目出错\n");
		return GM_CMD_ERROR;
	}
	
	unsigned long lMonsterID = atoi( cmdArgs[0].c_str() );
	if( lMonsterID == 0)
	{
		D_ERROR("输入的参数无效\n");
		return GM_CMD_ERROR;
	}

	CNewMap* pCurMap = pGameManager->GetCurMap();
	if ( NULL == pCurMap )
	{
		D_ERROR( "CGMCmdManager::GetMonsterState，GM当前所在地图空\n" );
		return GM_CMD_ERROR;
	}

	stringstream sysBuf;
	CMonster* pMonster = pCurMap->GetMonsterPtrInMap( lMonsterID );
	if( NULL == pMonster )
	{
		pGameManager->SendSystemChat("找不到该ID的怪物\n");
	} else {
		TYPE_POS nPosX = 0, nPosY =0;
		pMonster->GetPos( nPosX, nPosY );
		sysBuf <<"怪物的状态:"  <<pMonster->GetState()  <<"  怪物的位置 X:"  <<nPosX  <<" Y:"  <<nPosY;
		pGameManager->SendSystemChat( sysBuf.str().c_str() );
	}
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::AddPlayerBuff( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddPlayerBuff，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是2,那么出错
	if( cmdArgs.size() != 2)
	{
		pGameManager->SendSystemChat("GM命令参数数目出错\n");
		return GM_CMD_ERROR;
	}

	unsigned long uiBuffID = (unsigned long) atoi( cmdArgs[0].c_str() );
	if( uiBuffID == 0)
	{
		D_ERROR("输入的参数BUFF ID无效\n");
		return GM_CMD_ERROR;
	}

	TYPE_TIME uiTime = (unsigned int) atoi( cmdArgs[1].c_str() );
	if( uiTime == 0)
	{
		D_ERROR("输入的时间参数无效\n");
		return GM_CMD_ERROR;
	}
	
	CBuffProp* pBuffProp = CManBuffProp::FindObj(uiBuffID);
	//if( pBuffProp )
		//pGameManager->CreateBuff( pBuffProp, uiTime, pGameManager->GetFullID(), CREATE_PLAYER);

	return GM_CMD_SUCCESS;
}


int CGMCmdManager::AddMonsterBuff( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddMonsterBuff，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是3,那么出错
	if( cmdArgs.size() != 3)
	{
		pGameManager->SendSystemChat("GM命令参数数目出错\n");
		return GM_CMD_ERROR;
	}

	TYPE_ID monsterID;
	monsterID = atoi( cmdArgs[0].c_str() );
	if( monsterID == 0)
	{
		D_WARNING("输入的参数怪物ID无效\n");
		return GM_CMD_ERROR;
	}

	CNewMap* pCurMap = pGameManager->GetCurMap();
	if ( NULL == pCurMap )
	{
		D_ERROR( "CGMCmdManager::GetMonsterState，GM当前所在地图空\n" );
		return GM_CMD_ERROR;
	}
	CMonster* pMonster = pCurMap->GetMonsterPtrInMap( monsterID );
	if( NULL == pMonster )
	{
		D_WARNING("找不到该ID的怪物\n");
		return GM_CMD_ERROR;
	}


	TYPE_ID uiBuffID = (unsigned int) atoi( cmdArgs[1].c_str() );
	if( uiBuffID == 0)
	{
		D_ERROR("输入的参数BUFF ID无效\n");
		return GM_CMD_ERROR;
	}


	TYPE_TIME uiTime = (unsigned int) atoi( cmdArgs[2].c_str() );
	if( uiTime == 0)
	{
		D_ERROR("输入的时间参数无效\n");
		return GM_CMD_ERROR;
	}

	PlayerID createID;
	createID.dwPID = pMonster->GetMonsterID();
	createID.wGID = MONSTER_GID;
	//pMonster->CreateBuff( uiBuffID, uiTime, createID, CREATE_NULL);
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::LevelUpItem( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( pGameManager == NULL )
	{
		D_WARNING( "GM命令LevelUpItem，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	pGameManager->ShowPlateCmd( PLATE_UPGRADE );
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::ChangeItem( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( pGameManager == NULL )
	{
		D_WARNING( "GM命令ChangeItem，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	pGameManager->ShowPlateCmd( PLATE_MODIFY );
	return 0;
}

//设置或取消查看地图调试信息开关；
int CGMCmdManager::ViewMapDebugInfo( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	TRY_BEGIN;

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ViewMapDebugInfo，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是1,那么出错
	if( cmdArgs.size() != 0 )
	{
		pGameManager->SendSystemChat("GM命令ViewMapDebugInfo参数数目出错\n");
		return GM_CMD_ERROR;
	}

	CMuxMap* pCurMap = pGameManager->GetCurMap();
	if ( NULL == pCurMap )
	{
		D_WARNING( "ViewMapDebugInfo当前所在地图空\n" );
		return GM_CMD_ERROR;
	}

	MuxMapSpace::InverseViewDebug();

	stringstream syschat;
	syschat << "反转view mapdebuginfo标志成功\n";

	pGameManager->SendSystemChat( syschat.str().c_str() );

	syschat.str("");

	return GM_CMD_SUCCESS;
	TRY_END;
	return GM_CMD_ERROR;
}

//设置玩家已完成任务但尚未完成提交；
int CGMCmdManager::GMFinishTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	TRY_BEGIN;

	if ( NULL == pGameManager )
	{
		D_WARNING( "GMFinishTask，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是1,那么出错
	if( cmdArgs.size() != 1 )
	{
		pGameManager->SendSystemChat("GM命令GMFinishTask参数数目出错\n");
		return GM_CMD_ERROR;
	}

	unsigned int taskid  = (unsigned int) atoi( cmdArgs[0].c_str() );
	if ( pGameManager->SetTaskStat( taskid, NTS_TGT_ACHIEVE ) )
	{
		pGameManager->SendSystemChat("已设置任务状态为目标达成\n");
	} else {
		pGameManager->SendSystemChat("尚未接取此任务，GM指令执行失败\n");
	}	

	return GM_CMD_SUCCESS;
	TRY_END;
	return GM_CMD_ERROR;	
}

//查询服务器端的移动玩家信息, for debug, by dzj, 08.10.22；
int CGMCmdManager::GetMapMovingPlayer( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	TRY_BEGIN;

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetMapMovingPlayer，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是1,那么出错
	if( cmdArgs.size() != 0 )
	{
		pGameManager->SendSystemChat("GM命令GetMapMovingPlayer参数数目出错\n");
		return GM_CMD_ERROR;
	}

	CMuxMap* pCurMap = pGameManager->GetCurMap();
	if ( NULL == pCurMap )
	{
		pGameManager->SendSystemChat("GM命令GetMapMovingPlayer所在地图空\n");
		return GM_CMD_ERROR;
	}

	pCurMap->NotifyGMMovingPlayerInfo( pGameManager );

	return GM_CMD_SUCCESS;
	TRY_END;
	return GM_CMD_ERROR;	
}

//查询服务器端玩家自身位置, for debug, by dzj, 08.10.22；
int CGMCmdManager::GetSelfPos( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	TRY_BEGIN;

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetSelfPos，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是1,那么出错
	if( cmdArgs.size() != 0 )
	{
		pGameManager->SendSystemChat("GM命令GetSelfPos参数数目出错\n");
		return GM_CMD_ERROR;
	}

	CMuxMap* pCurMap = pGameManager->GetCurMap();
	if ( NULL == pCurMap )
	{
		pGameManager->SendSystemChat("GM命令GetSelfPos所在地图空\n");
		return GM_CMD_ERROR;
	}

	stringstream syschat;
	syschat << "自身服务器端当前位置(" << pGameManager->GetMapID() << ":" << pGameManager->GetPosX() << "," << pGameManager->GetPosY()
		<< "),速度属性值" << pGameManager->GetSpeed() << ",是否移动" << pGameManager->IsMoving() << ",是否在移动列表" << pGameManager->IsInMovingList() << "\n" ;

	pGameManager->SendSystemChat( syschat.str().c_str() );

	syschat.str("");

	return GM_CMD_SUCCESS;
	TRY_END;
	return GM_CMD_ERROR;
}

int CGMCmdManager::ChangeItemLevel(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ChangeItemLevel，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是1,那么出错
	if( cmdArgs.size() != 3 )
	{
		pGameManager->SendSystemChat("GM命令参数数目出错\n");
		return GM_CMD_ERROR;
	}

	unsigned int page  = (unsigned int) atoi( cmdArgs[0].c_str() );
	unsigned int index = (unsigned int) atoi( cmdArgs[1].c_str() );
	unsigned int level = (unsigned int) atoi( cmdArgs[2].c_str() );

	FullPlayerInfo* pFullPlayerInfo = pGameManager->GetFullPlayerInfo();
	if ( NULL == pFullPlayerInfo )
	{
		D_ERROR( "CGMCmdManager::ChangeItemLevel, NULL == pFullPlayerInfo\n" );
		return GM_CMD_ERROR;
	}

	PlayerInfo_3_Pkgs& fullPlayerInfo = pFullPlayerInfo->pkgInfo;

	unsigned int itemIndex = ( page - 1) * 36 + (index - 1);
	if ( itemIndex >= ARRAY_SIZE(fullPlayerInfo.pkgs) )
	{
		pGameManager->SendSystemChat("GM命令参数错\n");
		D_ERROR( "CGMCmdManager::ChangeItemLevel, 玩家%s发来GM指令， itemIndex(%d) >= ARRAY_SIZE(fullPlayerInfo.pkgs)", pGameManager->GetAccount(), itemIndex );
		return GM_CMD_ERROR;
	}

	ItemInfo_i& itemInfo = fullPlayerInfo.pkgs[itemIndex];

	SET_ITEM_LEVEL( itemInfo.ucLevelUp, level );

	pGameManager->NoticePkgItemChange( itemIndex );
	pGameManager->SendSystemChat("修改道具等级成功\n" );
	return GM_CMD_SUCCESS;
} //int CGMCmdManager::ChangeItemLevel(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )

int CGMCmdManager::ChangeItemAddID(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ChangeItemAddID，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是1,那么出错
	if( cmdArgs.size() != 3 )
	{
		pGameManager->SendSystemChat("GM命令参数数目出错\n");
		return GM_CMD_ERROR;
	}

	unsigned int page  = (unsigned int) atoi( cmdArgs[0].c_str() );
	unsigned int index = (unsigned int) atoi( cmdArgs[1].c_str() );
	unsigned int addID = (unsigned int) atoi( cmdArgs[2].c_str() );

	FullPlayerInfo* pFullPlayerInfo = pGameManager->GetFullPlayerInfo();
	if ( NULL == pFullPlayerInfo )
	{
		D_ERROR( "CGMCmdManager::ChangeItemAddID, NULL == pFullPlayerInfo\n" );
		return GM_CMD_ERROR;
	}

	PlayerInfo_3_Pkgs& fullPlayerInfo = pFullPlayerInfo->pkgInfo;

	int itemIndex =  ( page - 1) * 36 + (index - 1);
	if ( itemIndex >= ARRAY_SIZE(fullPlayerInfo.pkgs) )
	{
		pGameManager->SendSystemChat("GM命令参数错\n");
		D_ERROR( "CGMCmdManager::ChangeItemAddID, 玩家%s发来GM指令， itemIndex(%d) >= ARRAY_SIZE(fullPlayerInfo.pkgs)", pGameManager->GetAccount(), itemIndex );
		return GM_CMD_ERROR;
	}//if ( itemIndex >= ARRAY_SIZE(fullPlayerInfo.pkgs) )

	ItemInfo_i& itemInfo = fullPlayerInfo.pkgs[itemIndex];
	//去除追加，原位置用于随机集 itemInfo.randSet/*usAddId*/ = addID;
	itemInfo.randSet/*usAddId*/ = 0;
	pGameManager->NoticePkgItemChange( itemIndex );
	pGameManager->SendSystemChat("修改道具追加编号成功\n" );
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ThreatListStart( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetMonsterState，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是1,那么出错
	if( cmdArgs.size() != 1)
	{
		pGameManager->SendSystemChat("GM命令参数数目出错\n");
		return GM_CMD_ERROR;
	}
	
	unsigned long lMonsterID = atoi( cmdArgs[0].c_str() );
	if( lMonsterID == 0)
	{
		D_ERROR("输入的参数无效\n");
		return GM_CMD_ERROR;
	}

	//stringstream sysBuf;
	CNewMap* pCurMap = pGameManager->GetCurMap();
	if ( NULL == pCurMap )
	{
		D_ERROR( "CGMCmdManager::GetMonsterState，GM当前所在地图空\n" );
		return GM_CMD_ERROR;
	}
	CMonster* pMonster = pCurMap->GetMonsterPtrInMap( lMonsterID );
	if( NULL == pMonster )
	{
		pGameManager->SendSystemChat("找不到该ID的怪物\n");
	} else {
		pMonster->OnDebugThreatListStart();
		pGameManager->SetDebugMonsterID( lMonsterID );
		D_DEBUG("怪物威胁列表调试开始 怪物ID:%d\n",lMonsterID );
		pGameManager->SendSystemChat("怪物威胁列表调试开始\n" );
	}
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::ThreatListEnd( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetMonsterState，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是1,那么出错
	if( cmdArgs.size() != 0)
	{
		pGameManager->SendSystemChat("GM命令参数数目出错\n");
		return GM_CMD_ERROR;
	}
	
	unsigned long lMonsterID = pGameManager->GetDebugMonsterID();
	if( lMonsterID == 0)
	{
		D_ERROR("输入的参数无效\n");
		return GM_CMD_ERROR;
	}

	//stringstream sysBuf;
	CNewMap* pCurMap = pGameManager->GetCurMap();
	if ( NULL == pCurMap )
	{
		D_ERROR( "CGMCmdManager::GetMonsterState，GM当前所在地图空\n" );
		return GM_CMD_ERROR;
	}
	CMonster* pMonster = pCurMap->GetMonsterPtrInMap( lMonsterID );
	if( NULL == pMonster )
	{
		pGameManager->SendSystemChat("找不到该ID的怪物\n");
	} else {
		pMonster->OnDebugThreatListEnd();
		pGameManager->SendSystemChat("怪物威胁列表调试结束\n");
	}
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::GetAttribute( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数或者2个参数
	if ( !(cmdArgs.size() == 1 || cmdArgs.size() == 2) )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	EAttribute attr = (EAttribute)atoi(cmdArgs[0].c_str());
	
	
	//2参的话通过名字来查找
	CPlayer* pTargetPlayer = NULL;
	if( cmdArgs.size() == 2 )
	{
		pTargetPlayer = CPlayerManager::FindPlayerByAccount( cmdArgs[1].c_str() );
		if(NULL == pTargetPlayer)
		{
			pGameManager->SendSystemChat( "GM指令找不到指定玩家" );
			return GM_CMD_ERROR;
		}
	} else {
		pTargetPlayer = pGameManager;
	}
	

	stringstream syschat;

	switch(attr)
	{
	case ATTR_LEVEL:		//等级		
		syschat << "GM查询:该玩家的等级为" << pTargetPlayer->GetLevel() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;
	
	case ATTR_TITLE:	//称号
	    break;

	case ATTR_EXP: //经验
		syschat << "GM查询:该玩家的经验值为" << pTargetPlayer->GetExp() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;
	
	case ATTR_SPEED:		//速度
		syschat <<"GM查询：玩家速度为"  <<pTargetPlayer->GetSpeed()  <<endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_SPMAX:		//最大SP数
		break;
	
	case ATTR_SPNUMMAX:	//最大SP槽数
		break;

	case ATTR_HP:	//当前HP
		syschat << "GM查询:该玩家的HP为" << pTargetPlayer->GetCurrentHP() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;
	
	case ATTR_MP:	//当前MP
		syschat << "GM查询:该玩家的MP为" << pTargetPlayer->GetCurrentMP() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;
	
	case ATTR_SP:	//当前SP
		break;
	
	case ATTR_SPNUM:	//当前SP槽数
		break;
	
	case ATTR_PHYLEVEL:	//物理等级
		syschat << "GM查询:该玩家的物理技能等级为" << pTargetPlayer->GetPhyLevel() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_MAGLEVEL:	//魔法等级
		syschat << "GM查询:该玩家的魔法技能等级为" << pTargetPlayer->GetMagLevel() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_PHYLEVEL_MAX:	//最大物理等级
	   break;

	case ATTR_MAGLEVEL_MAX:	//最大魔法等级
		break;

	case ATTR_PHYEXP: 
		syschat << "GM查询:该玩家的物理技能熟练度为" << pTargetPlayer->GetPhyExp() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_MAGEXP:
		syschat << "GM查询:该玩家的魔法技能熟练度为" << pTargetPlayer->GetMagExp() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_PHY_ALLOPT:  //物理可分配点数
		syschat << "GM查询:该玩家的物理可分配点数为" << pTargetPlayer->GetPhyAllPoint() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_MAG_ALLOPT:  //魔法可分配点数
		syschat << "GM查询:该玩家的魔法可分配点数为" << pTargetPlayer->GetMagAllPoint() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_PHY_TOTALPT: //物理总获得点数
		syschat << "GM查询:该玩家的物理总点数为" << pTargetPlayer->GetPhyTotalPoint() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_MAG_TOTALPT: //魔法总获得点数
		syschat << "GM查询:该玩家的魔法总点数为" << pTargetPlayer->GetMagTotalPoint() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_MONEY: //银币
		syschat << "GM查询:该玩家的银币为" << pTargetPlayer->GetSilverMoney() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;
	case ATTR_GOLD_MONEY://金币
		syschat << "GM查询:该玩家的金币为" << pTargetPlayer->GetGoldMoney() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_BUFF:  //BUFF效果
		break;

	case ATTR_PHYATTACK:  //物理攻击
		break;

	case ATTR_STR:	//力量
		break;

	case ATTR_INT:  //智力
		
		break;

	case ATTR_AGI:  //敏捷
		break;

	case ATTR_HPMAX:	//最大HP
		syschat << "GM查询:该玩家的最大HP" << pTargetPlayer->GetMaxHP() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_MPMAX:	//最大MP
		syschat << "GM查询:该玩家的最大MP为" << pTargetPlayer->GetMaxMP() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	case ATTR_MAGATTACK:	//魔法攻击力
		break;

	case ATTR_PHYDEFEND:	//物理防御力
		break;

	case ATTR_MAGDEFEND: 	//魔法防御力
		
		break;

	case ATTR_PHYHIT:	//物理命中
		
		break;

	case ATTR_MAGHIT:	//魔法命中
		break;

	case ATTR_RACE_PRESTIGE:
		syschat << "GM查询:该玩家的种族声望" << pTargetPlayer->GetRacePrestige() << endl;
		pTargetPlayer->SendSystemChat( syschat.str().c_str() );
		break;

	default:
		pGameManager->SendSystemChat( "GM指令指定的查询类型错" );
		return GM_CMD_ERROR;
		break;
	}
	
	return GM_CMD_SUCCESS;
}

//创建道具
int CGMCmdManager::CreateItem( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令CreateItem，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//如果GM的参数不是2,那么出错
	if( cmdArgs.size() != 2)
	{
		pGameManager->SendSystemChat("GM命令参数数目出错");
		return GM_CMD_ERROR;
	}

	unsigned long uiItemID = (unsigned long)atof( cmdArgs[0].c_str() );
	if(uiItemID == 0)
	{
		pGameManager->SendSystemChat("GM命令参数Item ID出错");
		return GM_CMD_ERROR;
	}
	unsigned int uiNum = (unsigned int) atoi( cmdArgs[1].c_str() );
	if( uiNum == 0 )
	{
		pGameManager->SendSystemChat("GM命令参数Item数目出错");
		return GM_CMD_ERROR;
	}
	
	if( !pGameManager->GetNormalItem( uiItemID, uiNum ) )
	{
		pGameManager->SendSystemChat("GM命令:找不到确切的物品ID");
		return GM_CMD_ERROR;
	}

	pGameManager->SendSystemChat("GM命令:添加道具成功");

	return GM_CMD_SUCCESS;
}


int CGMCmdManager::GetVersion( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetVersion，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if( !(cmdArgs.empty()) )
	{
		pGameManager->SendSystemChat("GM命令:参数数目出错");
		return GM_CMD_ERROR;
	}

	pGameManager->SendSystemChat( g_szVersion );

	return GM_CMD_SUCCESS;
}

//type 0代表set 1代表add
int CGMCmdManager::_ChangeAttribute( CPlayer* pPlayer, ChangeType nType, EAttribute attr, unsigned int nUpdateVal )
{
	switch(attr)
	{
	case ATTR_LEVEL:		//等级
		//_ChangeLevel(pPlayer, nType, nUpdateVal);
		break;
	
	case ATTR_TITLE:	//称号
	    break;

	case ATTR_EXP: //经验
		_ChangeExp( pPlayer, nType, nUpdateVal );
		break;
	
	case ATTR_SPEED:
		_ChangeSpeed( pPlayer, nType, nUpdateVal);
		break;

	case ATTR_SPMAX:	//最大SP数
		break;
	
	case ATTR_SPNUMMAX:	//最大SP槽数
		break;

	case ATTR_HP:	//当前HP
		_ChangeHp(  pPlayer, nType, nUpdateVal);
		break;
	
	case ATTR_MP:	//当前MP
		_ChangeMp(  pPlayer, nType, nUpdateVal);
		break;
	
	case ATTR_SP:	//当前SP
		_ChangeSpPower( pPlayer, nType, nUpdateVal );
		break;
	
	case ATTR_SPNUM:	//当前SP槽数
		break;
	
	case ATTR_PHYLEVEL:	//物理等级
		//_ChangePhyLevel( pPlayer, nType, nUpdateVal );		
		break;

	case ATTR_MAGLEVEL:	//魔法等级
		//_ChangeMagLevel( pPlayer, nType, nUpdateVal );		
		break;

	case ATTR_PHYLEVEL_MAX:	//最大物理等级
	   _ChangeMaxPhyLevel( pPlayer, nType, nUpdateVal );		
	   break;

	case ATTR_MAGLEVEL_MAX:	//最大魔法等级
		 _ChangeMaxMagLevel( pPlayer, nType, nUpdateVal );		
		break;

	case ATTR_PHYEXP:
		_ChangePhyExp( pPlayer, nType, nUpdateVal);
		break;

	case ATTR_MAGEXP:
		_ChangeMagExp( pPlayer, nType, nUpdateVal);
		break;

	case ATTR_PHY_ALLOPT:  //物理可分配点数
		break;

	case ATTR_MAG_ALLOPT:  //魔法可分配点数
		break;

	case ATTR_PHY_TOTALPT: //物理总获得点数
		break;

	case ATTR_MAG_TOTALPT: //魔法总获得点数
		break;

	case ATTR_MONEY: //银币
		_ChangeMoney( pPlayer, nType, nUpdateVal );
		break;
	case ATTR_GOLD_MONEY: //金币
		_ChangeGoldMoney( pPlayer, nType, nUpdateVal );
		break;

	case ATTR_BUFF:  //BUFF效果
		break;

	case ATTR_PHYATTACK:  //物理攻击
		_ChangePhyAttack( pPlayer, nType, nUpdateVal);
		break;

	case ATTR_STR:	//力量
		_ChangeStr( pPlayer, nType, nUpdateVal);
		break;

	case ATTR_INT:  //智力
		_ChangeInt( pPlayer, nType, nUpdateVal);
		break;

	case ATTR_AGI:  //敏捷
		break;

	case ATTR_HPMAX:	//最大HP
		break;

	case ATTR_MPMAX:	//最大MP
		break;

	case ATTR_MAGATTACK:		//魔法攻击力
		_ChangeMagAttack( pPlayer, nType, nUpdateVal);
		break;

	case ATTR_PHYDEFEND:		//物理防御力
		_ChangePhyDefend( pPlayer, nType, nUpdateVal);
		break;

	case ATTR_MAGDEFEND: 	//魔法防御力
		_ChangeMagDefend( pPlayer, nType, nUpdateVal);
		break;

	case ATTR_PHYHIT:	//物理命中
		_ChangePhyHit( pPlayer, nType, nUpdateVal );
		break;

	case ATTR_MAGHIT:	//魔法命中
		_ChangeMagHit( pPlayer, nType, nUpdateVal );
		break;
	
	case ATTR_RACE_PRESTIGE:	//种族声望
		_ChangeRacePrestige( pPlayer, nType, nUpdateVal );
		break;

	default:
		pPlayer->SendSystemChat( "GM指令参数指定的类型错误" );
		return GM_CMD_ERROR;
		break;
	}
	
	return 0;
}


bool CGMCmdManager::NpcQueryPropertyEqual (CPlayer* pPlayer, EAttribute attr, int nUpdateVal)
{
	if( !pPlayer )
		return false;

	bool bRet = false;
	switch(attr)
	{
	case ATTR_LEVEL:		//等级
		bRet = (pPlayer->GetLevel() == nUpdateVal);
		break;
	
	case ATTR_TITLE:	//称号
	    break;

	case ATTR_EXP: //经验
		bRet = ( (int)pPlayer->GetExp() == nUpdateVal);
		break;
	
	case ATTR_SPEED:
		break;

	case ATTR_SPMAX:	//最大SP数
		break;
	
	case ATTR_SPNUMMAX:	//最大SP槽数
		break;

	case ATTR_HP:	//当前HP
		break;
	
	case ATTR_MP:	//当前MP
		break;
	
	case ATTR_SP:	//当前SP
		break;
	
	case ATTR_SPNUM:	//当前SP槽数
		break;
	
	case ATTR_PHYLEVEL:	//物理等级
		bRet = ( (int)(pPlayer->GetPhyLevel()) ==  nUpdateVal);
		break;

	case ATTR_MAGLEVEL:	//魔法等级
		bRet = ( (int)(pPlayer->GetMagLevel()) ==  nUpdateVal);
		break;

	case ATTR_PHYLEVEL_MAX:	//最大物理等级
		bRet = ( (int)(pPlayer->GetMaxPhyLevelLimit()) ==  nUpdateVal);
	    break;

	case ATTR_MAGLEVEL_MAX:	//最大魔法等级
		bRet = ( (int)(pPlayer->GetMaxMagLevelLimit()) ==  nUpdateVal);
		break;

	case ATTR_PHYEXP:
		bRet = ( (int)(pPlayer->GetPhyExp()) ==  nUpdateVal);
		break;

	case ATTR_MAGEXP:
		bRet = ( (int)(pPlayer->GetMagExp()) ==  nUpdateVal);
		break;

	case ATTR_PHY_ALLOPT:  //物理可分配点数
		bRet = ( pPlayer->GetPhyAllPoint() ==  nUpdateVal);
		break;

	case ATTR_MAG_ALLOPT:  //魔法可分配点数
		bRet = ( pPlayer->GetMagAllPoint() ==  nUpdateVal);
		break;

	case ATTR_PHY_TOTALPT: //物理总获得点数
		bRet = ( pPlayer->GetPhyTotalPoint() ==  nUpdateVal);
		break;

	case ATTR_MAG_TOTALPT: //魔法总获得点数
		bRet = ( pPlayer->GetMagTotalPoint() ==  nUpdateVal);
		break;

	case ATTR_MONEY: //银币
		bRet = ( (int)(pPlayer->GetSilverMoney()) ==  nUpdateVal);
		break;
	case ATTR_GOLD_MONEY:  //金币
		bRet = ( (int)(pPlayer->GetGoldMoney()) ==  nUpdateVal);
		break;

	case ATTR_BUFF:  //BUFF效果
		break;

	case ATTR_PHYATTACK:  //物理攻击
		break;

	case ATTR_STR:	//力量
		break;

	case ATTR_INT:  //智力
		break;

	case ATTR_AGI:  //敏捷
		break;

	case ATTR_HPMAX:	//最大HP
		break;

	case ATTR_MPMAX:	//最大MP
		break;

	case ATTR_MAGATTACK:		//魔法攻击力
		break;

	case ATTR_PHYDEFEND:		//物理防御力
		break;

	case ATTR_MAGDEFEND: 	//魔法防御力
		break;

	case ATTR_PHYHIT:	//物理命中
		break;

	case ATTR_MAGHIT:	//魔法命中
		break;

	case ATTR_GOODEVILPOINT:
		bRet = ( pPlayer->GetGoodEvilPoint() ==  nUpdateVal);
		break;

	case 99:
		bRet = ( pPlayer->GetRace() ==  nUpdateVal);
		break;

	case 98:
		bRet = ( (unsigned char)pPlayer->GetSex() ==  nUpdateVal);
		break;

	case 97:
		bRet = ( pPlayer->GetClass() ==  nUpdateVal);
		break;

	default:
		bRet = false;
		break;
	}
	
	return bRet;
}


bool CGMCmdManager::NpcQueryPropertyLowerThan(CPlayer* pPlayer, EAttribute attr,  int nUpdateVal)
{
	if( !pPlayer )
		return false;

	bool bRet = false;
	switch(attr)
	{
	case ATTR_LEVEL:		//等级
		bRet = (pPlayer->GetLevel() < (unsigned short)nUpdateVal);
		break;
	
	case ATTR_TITLE:	//称号
	    break;

	case ATTR_EXP: //经验
		bRet = ( pPlayer->GetExp() <  (unsigned int)nUpdateVal);
		break;
	
	case ATTR_SPEED:
		break;

	case ATTR_SPMAX:	//最大SP数
		break;
	
	case ATTR_SPNUMMAX:	//最大SP槽数
		break;

	case ATTR_HP:	//当前HP
		break;
	
	case ATTR_MP:	//当前MP
		break;
	
	case ATTR_SP:	//当前SP
		break;
	
	case ATTR_SPNUM:	//当前SP槽数
		break;
	
	case ATTR_PHYLEVEL:	//物理等级
		bRet = ( pPlayer->GetPhyLevel() <  (unsigned int)nUpdateVal);
		break;

	case ATTR_MAGLEVEL:	//魔法等级
		bRet = ( pPlayer->GetMagLevel() < (unsigned int)nUpdateVal);
		break;

	case ATTR_PHYLEVEL_MAX:	//最大物理等级
		bRet = ( pPlayer->GetMaxPhyLevelLimit() < (unsigned char)nUpdateVal);
	    break;

	case ATTR_MAGLEVEL_MAX:	//最大魔法等级
		bRet = ( pPlayer->GetMaxMagLevelLimit() < (unsigned char)nUpdateVal);
		break;

	case ATTR_PHYEXP:
		bRet = ( pPlayer->GetPhyExp() < (unsigned int)nUpdateVal);
		break;

	case ATTR_MAGEXP:
		bRet = ( pPlayer->GetMagExp() < (unsigned int)nUpdateVal);
		break;

	case ATTR_PHY_ALLOPT:  //物理可分配点数
		bRet = ( pPlayer->GetPhyAllPoint() < (unsigned short)nUpdateVal);
		break;

	case ATTR_MAG_ALLOPT:  //魔法可分配点数
		bRet = ( pPlayer->GetMagAllPoint() < (unsigned short)nUpdateVal);
		break;

	case ATTR_PHY_TOTALPT: //物理总获得点数
		bRet = ( pPlayer->GetPhyTotalPoint() < (unsigned short)nUpdateVal);
		break;

	case ATTR_MAG_TOTALPT: //魔法总获得点数
		bRet = ( pPlayer->GetMagTotalPoint() < (unsigned short)nUpdateVal);
		break;

	case ATTR_MONEY: //银币
		bRet = ( pPlayer->GetSilverMoney() < (unsigned int)nUpdateVal);
		break;
	case ATTR_GOLD_MONEY: //金币
		bRet = ( pPlayer->GetGoldMoney() < (unsigned int)nUpdateVal);
		break;

	case ATTR_BUFF:  //BUFF效果
		break;

	case ATTR_PHYATTACK:  //物理攻击
		break;

	case ATTR_STR:	//力量
		break;

	case ATTR_INT:  //智力
		break;

	case ATTR_AGI:  //敏捷
		break;

	case ATTR_HPMAX:	//最大HP
		break;

	case ATTR_MPMAX:	//最大MP
		break;

	case ATTR_MAGATTACK:		//魔法攻击力
		break;

	case ATTR_PHYDEFEND:		//物理防御力
		break;

	case ATTR_MAGDEFEND: 	//魔法防御力
		break;

	case ATTR_PHYHIT:	//物理命中
		break;

	case ATTR_MAGHIT:	//魔法命中
		break;

	case ATTR_GOODEVILPOINT:
		bRet = ( pPlayer->GetGoodEvilPoint() < nUpdateVal);
		break;

	default:
		bRet = false;
		break;
	}
	
	return bRet;
}


bool CGMCmdManager::NpcQueryPropertyMoreThan(CPlayer* pPlayer, EAttribute attr,int nUpdateVal)
{
	if( !pPlayer )
		return false;

	bool bRet = false;
	switch(attr)
	{
	case ATTR_LEVEL:		//等级
		bRet = (pPlayer->GetLevel() >= (unsigned short)nUpdateVal);
		break;
	
	case ATTR_TITLE:	//称号
	    break;

	case ATTR_EXP: //经验
		bRet = ( pPlayer->GetExp() >=  (unsigned int)nUpdateVal);
		break;
	
	case ATTR_SPEED:
		break;

	case ATTR_SPMAX:	//最大SP数
		break;
	
	case ATTR_SPNUMMAX:	//最大SP槽数
		break;

	case ATTR_HP:	//当前HP
		break;
	
	case ATTR_MP:	//当前MP
		break;
	
	case ATTR_SP:	//当前SP
		break;
	
	case ATTR_SPNUM:	//当前SP槽数
		break;
	
	case ATTR_PHYLEVEL:	//物理等级
		bRet = ( pPlayer->GetPhyLevel() >=  (unsigned int)nUpdateVal);
		break;

	case ATTR_MAGLEVEL:	//魔法等级
		bRet = ( pPlayer->GetMagLevel() >=  (unsigned int)nUpdateVal);
		break;

	case ATTR_PHYLEVEL_MAX:	//最大物理等级
		bRet = ( pPlayer->GetMaxPhyLevelLimit() >=  (unsigned char)nUpdateVal);
	    break;

	case ATTR_MAGLEVEL_MAX:	//最大魔法等级
		bRet = ( pPlayer->GetMaxMagLevelLimit() >= (unsigned char)nUpdateVal);
		break;

	case ATTR_PHYEXP:
		bRet = ( pPlayer->GetPhyExp() >=  (unsigned int)nUpdateVal);
		break;

	case ATTR_MAGEXP:
		bRet = ( pPlayer->GetMagExp() >=  (unsigned int)nUpdateVal);
		break;

	case ATTR_PHY_ALLOPT:  //物理可分配点数
		bRet = ( pPlayer->GetPhyAllPoint() >=  (unsigned int)nUpdateVal);
		break;

	case ATTR_MAG_ALLOPT:  //魔法可分配点数
		bRet = ( pPlayer->GetMagAllPoint() >=  (unsigned int)nUpdateVal);
		break;

	case ATTR_PHY_TOTALPT: //物理总获得点数
		bRet = ( pPlayer->GetPhyTotalPoint() >=  (unsigned short)nUpdateVal);
		break;

	case ATTR_MAG_TOTALPT: //魔法总获得点数
		bRet = ( pPlayer->GetMagTotalPoint() >=  (unsigned short)nUpdateVal);
		break;

	case ATTR_MONEY: //银币
		bRet = ( pPlayer->GetSilverMoney() >=  (unsigned int)nUpdateVal);
		break;
	case ATTR_GOLD_MONEY: //金币
		bRet = ( pPlayer->GetGoldMoney() >=  (unsigned int)nUpdateVal);
		break;

	case ATTR_BUFF:  //BUFF效果
		break;

	case ATTR_PHYATTACK:  //物理攻击
		break;

	case ATTR_STR:	//力量
		break;

	case ATTR_INT:  //智力
		break;

	case ATTR_AGI:  //敏捷
		break;

	case ATTR_HPMAX:	//最大HP
		break;

	case ATTR_MPMAX:	//最大MP
		break;

	case ATTR_MAGATTACK:		//魔法攻击力
		break;

	case ATTR_PHYDEFEND:		//物理防御力
		break;

	case ATTR_MAGDEFEND: 	//魔法防御力
		break;

	case ATTR_PHYHIT:	//物理命中
		break;

	case ATTR_MAGHIT:	//魔法命中
		break;

	case ATTR_GOODEVILPOINT:
		bRet = ( pPlayer->GetGoodEvilPoint() >= nUpdateVal);
		break;

	default:
		bRet = false;
		break;
	}
	
	return bRet;
}


bool CGMCmdManager::NpcAddSkill( TYPE_ID skillID, CPlayer* pPlayer)
{
	if(!pPlayer)
		return false;


	//如果玩家已经拥有该技能则不能添加
	if( !pPlayer->AddSkillByID( skillID ) )
	{
		pPlayer->SendSystemChat( "该技能该玩家已拥有或非该玩家职业的技能" );
		return false;
	}
	
	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_SKILL;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = skillID;
	updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	return true;

}


int CGMCmdManager::_ChangeLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeLevel, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	//PlayerInfo_1_Base& dbInfo = pPlayer->GetDBProp();
	//step1 根据属性判断每一个范围的可修改的范围
	if( nType == TYPE_SET)
	{
		if( nUpdateVal > 90 )
		{
			pPlayer->SendSystemChat("GM命令:参数修改值不正确" );
			return GM_CMD_ERROR;
		}
	}else{
		if( nUpdateVal + pPlayer->GetLevel() > 90)
		{
			pPlayer->SendSystemChat("GM命令:参数修改后值不正确" );
			return GM_CMD_ERROR;
		}
	} 

	pPlayer->SendSystemChat("GM命令：修改等级成功" );

	MGPlayerLevelUp levelupMsg;
	levelupMsg.nNewLevel = pPlayer->GetLevel();
	levelupMsg.playerID = pPlayer->GetFullID();

	//pPlayer->NotifyPlayerNearby( &levelupMsg );
	CMuxMap* pPlayerMap = pPlayer->GetCurMap();
	if ( NULL != pPlayerMap )
	{
		pPlayerMap->NotifySurrounding<MGPlayerLevelUp>( &levelupMsg, pPlayer->GetPosX(), pPlayer->GetPosY() );
	} 

	//by dzj, 07.23，重复发bug; pPlayer->SendPkg< MGPlayerLevelUp >( &levelupMsg );
	return 1;
}


int CGMCmdManager::_ChangeExp(CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeExp, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	ACE_UNUSED_ARG( nType );
	//未使用，编译警告去除，by dzj, 09.07.07,PlayerInfo_1_Base& dbInfo = pPlayer->GetDBProp();	
	pPlayer->AddExp( nUpdateVal, GCPlayerLevelUp::IN_TASK, false );	//增加经验
	pPlayer->SendSystemChat( "GM命令:修改经验成功" );

	return 1;
}


int CGMCmdManager::_ChangeSpeed( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeSpeed, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	//速度
	if( nType == TYPE_ADD)
	{	
		pPlayer->SendSystemChat("GM命令：速度不允许ADD，放弃执行\n");
		return GM_CMD_ERROR;
	}

	if( nUpdateVal > 6)
	{
		pPlayer->SendSystemChat("GM命令：参数不正确，放弃速度设置\n");
		return GM_CMD_ERROR;
	}
	
	if( pPlayer->SetSpeed( (float)nUpdateVal ) )
	{
		pPlayer->SendSystemChat( "GM命令操作修改速度成功" );
	}
	return 1;
}


int CGMCmdManager::_ChangePhyAttack( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangePhyAttack, NULL == pPlayer\n" );
		return 1;
	}
	PlayerSecondProp& tempInfo = pPlayer->GetTmpProp();
	D_ERROR( "现玩家物攻由基本属性计算+装备加成而来，因此直接修改无效\n" );
	pPlayer->SendSystemChat( "现玩家物攻由基本属性计算+装备加成而来，因此直接修改无效");
	return 1;
	tempInfo.nPhysicsAttack = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + tempInfo.nPhysicsAttack;
	pPlayer->SendSystemChat( "GM命令: 修改物理攻击成功");

	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_PHYATTACK;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = tempInfo.nPhysicsAttack;
	updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	return 1;
}


int CGMCmdManager::_ChangeMagAttack( CPlayer* pPlayer, ChangeType nType,unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeMagAttack, NULL == pPlayer\n" );
		return 1;
	}
	PlayerSecondProp& tempInfo = pPlayer->GetTmpProp();
	D_ERROR( "现玩家魔攻由基本属性计算+装备加成而来，因此直接修改无效\n" );
	pPlayer->SendSystemChat( "现玩家魔攻由基本属性计算+装备加成而来，因此直接修改无效");
	return 1;

	tempInfo.nMagicAttack = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + tempInfo.nMagicAttack;
	pPlayer->SendSystemChat( "GM命令: 修改魔法攻击成功");

	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_MAGATTACK;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = tempInfo.nMagicAttack;
	updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	return 1;
}


int CGMCmdManager::_ChangeHp( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeHp, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	PlayerInfo_1_Base& dbInfo = pPlayer->GetDBProp();
	dbInfo.uiHP = (nType == TYPE_SET) ? nUpdateVal : nUpdateVal + dbInfo.uiHP;
	pPlayer->SendSystemChat( "GM命令操作修改HP成功" );
	
	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_HP;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = dbInfo.uiHP;
	updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	updateAttr.changeTime = 0;//hyn检查; 检查完毕
	updateAttr.nAddType = MGRoleattUpdate::TYPE_SET;//hyn检查;检查完毕
	updateAttr.bFloat = false;//hyn检查; 检查完毕
	
	//pPlayer->NotifyPlayerNearby( &updateAttr );
	CMuxMap* pPlayerMap = pPlayer->GetCurMap();
	if ( NULL != pPlayerMap )
	{
		pPlayerMap->NotifySurrounding<MGRoleattUpdate>( &updateAttr, pPlayer->GetPosX(), pPlayer->GetPosY() );
	} 

	if ( pPlayer->GetCurrentHP() <= 0 )
	{
		pPlayer->SubPlayerHp( 0, NULL, pPlayer->GetFullID() );//如果血量到0，则应调用死亡过程；
	}

	return 1;
}

int CGMCmdManager::_ChangeMp( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeMp, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	PlayerInfo_1_Base& dbInfo = pPlayer->GetDBProp();
	dbInfo.uiMP = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + dbInfo.uiMP;
	pPlayer->SendSystemChat( "GM命令操作修改MP成功" );

	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_MP;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = dbInfo.uiMP;
	updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );

	//pPlayer->NotifyPlayerNearby( &updateAttr );
	CMuxMap* pPlayerMap = pPlayer->GetCurMap();
	if ( NULL != pPlayerMap )
	{
		pPlayerMap->NotifySurrounding<MGRoleattUpdate>( &updateAttr, pPlayer->GetPosX(), pPlayer->GetPosY() );
	}

	return 1;
}


int CGMCmdManager::_ChangePhyDefend( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangePhyDefend, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	PlayerSecondProp& tempInfo = pPlayer->GetTmpProp();
	D_ERROR( "现玩家物防由基本属性计算+装备加成而来，因此直接修改无效\n" );
	pPlayer->SendSystemChat( "现玩家物防由基本属性计算+装备加成而来，因此直接修改无效");
	return 1;

	tempInfo.nPhysicsDefend = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + tempInfo.nPhysicsDefend;
	pPlayer->SendSystemChat( "GM命令操作修改物理防御成功" );

	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_PHYDEFEND;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = tempInfo.nPhysicsDefend;
	updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	return 1;
}


int CGMCmdManager::_ChangeMagDefend( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeMagDefend, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	PlayerSecondProp& tempInfo = pPlayer->GetTmpProp();
	D_ERROR( "现玩家魔防由基本属性计算+装备加成而来，因此直接修改无效\n" );
	pPlayer->SendSystemChat( "现玩家魔防由基本属性计算+装备加成而来，因此直接修改无效");
	return 1;

	tempInfo.nMagicDefend = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + tempInfo.nMagicDefend;
	pPlayer->SendSystemChat( "GM命令操作修改魔法防御成功" );

	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_MAGDEFEND;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = tempInfo.nMagicDefend;
	updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	return 1;
}


int CGMCmdManager::_ChangePhyHit( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangePhyHit, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	PlayerSecondProp& tempInfo = pPlayer->GetTmpProp();
	D_ERROR( "现玩家物理命中由基本属性计算+装备加成而来，因此直接修改无效\n" );
	pPlayer->SendSystemChat( "现玩家物理命中由基本属性计算+装备加成而来，因此直接修改无效");
	return 1;

	tempInfo.nPhysicsHit = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + tempInfo.nPhysicsHit;
	pPlayer->SendSystemChat( "GM命令操作修改物理命中成功" );

	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_PHYHIT;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = tempInfo.nPhysicsHit;
	updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	return 1;
}

int CGMCmdManager::_ChangeMagHit( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeMagHit, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	PlayerSecondProp& tempInfo = pPlayer->GetTmpProp();
	D_ERROR( "现玩家魔法命中由基本属性计算+装备加成而来，因此直接修改无效\n" );
	pPlayer->SendSystemChat( "现玩家魔法命中由基本属性计算+装备加成而来，因此直接修改无效");
	return 1;

	tempInfo.nMagicHit = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + tempInfo.nMagicHit;
	pPlayer->SendSystemChat( "GM命令操作修改魔法命中成功" );

	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_MAGHIT;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = tempInfo.nMagicHit;
	updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	return 1;
}

int CGMCmdManager::_ChangePhyExp( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	return 1;
	//10.09.20已废弃//if ( NULL == pPlayer )
	//{
	//	D_ERROR( "CGMCmdManager::_ChangePhyExp, NULL == pPlayer\n" );
	//	return GM_CMD_ERROR;
	//}

	//if (nType == TYPE_SET)
	//{
	//	D_WARNING("_ChangePhyExp 熟练度不允许设置\n");
	//	return GM_CMD_ERROR;	
	//}

	//pPlayer->AddPhyExp( nUpdateVal );
	//pPlayer->SendSystemChat( "GM命令操作修改物理技能熟练度成功" );

	//MGRoleattUpdate updateAttr;
	//updateAttr.nType = RT_PHYEXP;
	//updateAttr.nOldValue = 0;
	//updateAttr.nNewValue = pPlayer->GetPhyExp();
	//updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	//updateAttr.changeTime = 0;
	//updateAttr.bFloat = false;
	//pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	//return 1;
}

int CGMCmdManager::_ChangeMagExp( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	return 1;
	//10.09.20已废弃//if ( NULL == pPlayer )
	//{
	//	D_ERROR( "CGMCmdManager::_ChangeMagExp, NULL == pPlayer\n" );
	//	return GM_CMD_ERROR;
	//}

	//if (nType == TYPE_SET)
	//{
	//	D_WARNING("_ChangeMagExp 熟练度不允许设置\n");
	//	return GM_CMD_ERROR;	
	//}

	//pPlayer->AddMagExp( nUpdateVal );
	//pPlayer->SendSystemChat( "GM命令操作修改魔法技能熟练度成功" );

	//MGRoleattUpdate updateAttr;
	//updateAttr.nType = RT_MAGEXP;
	//updateAttr.nOldValue = 0;
	//updateAttr.nNewValue = pPlayer->GetMagExp();
	//updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	//updateAttr.changeTime = 0;
	//updateAttr.bFloat = false;
	//pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	//return 1;
}


int CGMCmdManager::_ChangeInt( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeInt, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	PlayerSecondProp& tempInfo = pPlayer->GetTmpProp();
	D_ERROR( "现玩家智力由基本属性+装备加成而来，不可直接修改\n" );
	pPlayer->SendSystemChat( "现玩家智力由基本属性+装备加成而来，不可直接修改\n" );
	return 1;

	//tempInfo.nIntelligence = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + tempInfo.nIntelligence;
	//pPlayer->SendSystemChat( "GM命令操作修改魔法智力成功" );

	//MGRoleattUpdate updateAttr;
	//updateAttr.nType = RT_INT;
	//updateAttr.nOldValue = 0;
	//updateAttr.nNewValue = tempInfo.nIntelligence;
	//updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	//updateAttr.changeTime = 0;
	//updateAttr.bFloat = false;
	//pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	return 1;
}


int CGMCmdManager::_ChangeStr( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CGMCmdManager::_ChangeStr, NULL == pPlayer\n" );
		return GM_CMD_ERROR;
	}

	PlayerSecondProp& tempInfo = pPlayer->GetTmpProp();
	D_ERROR( "现玩家力量由基本属性+装备加成而来，不可直接修改\n" );
	pPlayer->SendSystemChat( "现玩家力量由基本属性+装备加成而来，不可直接修改\n" );
	return 1;

	//tempInfo.nStrength = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + tempInfo.nStrength;
	//pPlayer->SendSystemChat( "GM命令操作修改魔法力量成功" );

	//MGRoleattUpdate updateAttr;
	//updateAttr.nType = RT_STR;
	//updateAttr.nOldValue = 0;
	//updateAttr.nNewValue = tempInfo.nStrength;
	//updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	//updateAttr.changeTime = 0;
	//updateAttr.bFloat = false;
	//pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	//return 1;
}

int CGMCmdManager::_ChangeGoldMoney( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if( !pPlayer )
	{
		return GM_CMD_ERROR;
	}

	if( pPlayer->GetGoldMoney() > MAX_MONEY )
	{
		pPlayer->SendSystemChat("玩家本身金币超过限制,改为最大金钱限制");
		pPlayer->SetGoldMoney( MAX_MONEY );
		return GM_CMD_ERROR;
	}

	if( nUpdateVal > MAX_MONEY && nType == TYPE_SET )
	{
		pPlayer->SendSystemChat("修改的金币数超过限制1");
		return GM_CMD_ERROR;
	}

	if( nType == TYPE_ADD )
	{
		if( nUpdateVal + pPlayer->GetGoldMoney() > MAX_MONEY )
		{
			pPlayer->SendSystemChat("修改的金币数超过限制2");
			return GM_CMD_ERROR;
		}
	}

	pPlayer->SetGoldMoney( (nType == TYPE_SET)? nUpdateVal : nUpdateVal + pPlayer->GetGoldMoney() );
	pPlayer->SendSystemChat( "GM命令操作修改金币成功" );

	pPlayer->NotifyGoldMoney();

	//排行榜暂时不理会金币，by dzj, 10.07.15, RankEventManagerSingle::instance()->MonitorPlayerMoneyChange( pPlayer, pPlayer->GetSilverMoney() );

	return 1;

}

int CGMCmdManager::_ChangeMoney( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	if( !pPlayer )
	{
		return false;
	}

	//先对本身的金钱和要修改的金钱作下查看
	PlayerInfo_1_Base& dbInfo = pPlayer->GetDBProp();

	if( pPlayer->GetSilverMoney() > MAX_MONEY )
	{
		pPlayer->SendSystemChat("玩家本身银币超过限制,改为最大金钱限制");
		pPlayer->SetSilverMoney( MAX_MONEY );
		return GM_CMD_ERROR;
	}

	if( nUpdateVal > MAX_MONEY && nType == TYPE_SET )
	{
		pPlayer->SendSystemChat("修改的银币数超过限制1");
		return GM_CMD_ERROR;
	}

	if( nType == TYPE_ADD )
	{
		if( nUpdateVal + pPlayer->GetSilverMoney() > MAX_MONEY )
		{
			pPlayer->SendSystemChat("修改的银币数超过限制2");
			return GM_CMD_ERROR;
		}
	}

	pPlayer->SetSilverMoney( (nType == TYPE_SET)? nUpdateVal : nUpdateVal + dbInfo.dwSilverMoney );
	pPlayer->SendSystemChat( "GM命令操作修改银币成功" );

	return 1;
}

int CGMCmdManager::GetItemInfo( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "CGMCmdManager::GetItemInfo，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是3个参数
	if ( cmdArgs.size() != 3)
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	//获取参数（0:背包索引，1：在背包中的索引，2：属性索引）
	unsigned short usPkgIndex = STRING_TO_NUM(cmdArgs[0].c_str()) - 1;
	unsigned short usItemInPkgIndex = STRING_TO_NUM(cmdArgs[1].c_str()) - 1;
	unsigned short usItemInfoIndex = STRING_TO_NUM(cmdArgs[2].c_str());
	
	FullPlayerInfo* pFullPlayerInfo = pGameManager->GetFullPlayerInfo();
	if (NULL == pFullPlayerInfo)
	{
		return GM_CMD_ERROR;
	}

	unsigned short usItemInternalIndex = usPkgIndex * PKG_GROUP_NUM + usItemInPkgIndex;
	if( (usPkgIndex >= PACKAGE_SIZE/PKG_GROUP_NUM) || (usItemInPkgIndex >= PKG_GROUP_NUM) || (usItemInternalIndex >= PACKAGE_SIZE) )
	{
		pGameManager->SendSystemChat("GM指令中的物品索引无效一");
		return GM_CMD_ERROR;
	}

	if ( usItemInternalIndex >= ARRAY_SIZE(pFullPlayerInfo->pkgInfo.pkgs) )
	{
		D_ERROR( "CGMCmdManager::GetItemInfo, usItemInternalIndex(%d) >= ARRAY_SIZE(pFullPlayerInfo->pkgInfo.pkgs)\n", usItemInternalIndex );
		pGameManager->SendSystemChat("GM指令中的物品索引无效二");
		return GM_CMD_ERROR;
	}

	if(pFullPlayerInfo->pkgInfo.pkgs[usItemInternalIndex].uiID == 0)
	{
		pGameManager->SendSystemChat("GM指令物品不存在");
		return GM_CMD_ERROR;
	}

	stringstream syschat;
	switch(usItemInfoIndex)
	{
	case 0:		//耐久度		
		syschat << "GM查询:玩家物品的耐久度为" << pFullPlayerInfo->pkgInfo.pkgs[usItemInternalIndex].usWearPoint << endl;
		pGameManager->SendSystemChat( syschat.str().c_str() );
		break;

	default:
		pGameManager->SendSystemChat( "GM指令指定的查询类型错" );
		return GM_CMD_ERROR;
		break;
	}


	return GM_CMD_SUCCESS;
}


int CGMCmdManager::UpdateScript( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	CFightCalScriptManager::Release();	//先清除原先的脚本
	
	if ( !CFightCalScriptManager::Init() )//战斗计算脚本管理器；
	{
		pGameManager->SendSystemChat("读取战斗脚本失败,请修改后重新读取\n");
		return GM_CMD_ERROR;
	}else{
		pGameManager->SendSystemChat("战斗计算脚本读取成功\n");		
	}
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ClearTaskHistory(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	FullPlayerInfo * playerInfo = pGameManager->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_WARNING( "GM命令GetAttribute，NULL == playerInfo\n" );
		return GM_CMD_ERROR;
	}

	StructMemSet( (playerInfo->taskInfo), 0, sizeof(playerInfo->taskInfo) );

	//TaskRecordInfo* taskRdArr = pGameManager->GetFullPlayerInfo()->taskInfo;
	//if ( !taskRdArr )
	//	return GM_CMD_ERROR;

	//memset( taskRdArr, 0x0, sizeof(TaskRecordInfo)*MAX_TASKRD_SIZE);
	pGameManager->SendSystemChat("清除任务记录成功\n");		
	return GM_CMD_SUCCESS;
}

////查找指定帐号玩家位置；
//int CGMCmdManager::GetPlayerSur( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
//{
//	//判断玩家是否为空
//	if ( NULL == pGameManager )
//	{
//		D_WARNING( "GM命令GetPlayerPos，输入GM玩家的指针空\n" );
//		return GM_CMD_ERROR;
//	}
//
//	//必须是3个参数
//	if ( 1 != cmdArgs.size() )
//	{
//		pGameManager->SendSystemChat( "GM指令参数数目错误" );
//		return GM_CMD_ERROR;
//	}
//
//	CPlayer* pFindPlayer = CPlayerManager::FindPlayerByAccount( cmdArgs[0] );
//	if ( NULL == pFindPlayer )
//	{
//		pGameManager->SendSystemChat( "找不到指定帐号玩家" );
//		return GM_CMD_ERROR;
//	}
//
//	unsigned int mapPlayerNum=0u, surPlayerNum=0u, surMonsterNum=0u;
//	pFindPlayer->GetSurPlayerMonsterNum( mapPlayerNum, surPlayerNum, surMonsterNum );
//
//	stringstream syschat;
//	syschat<<"玩家"<<pFindPlayer->GetNickName()<<"地图:"<<pFindPlayer->GetMapID()<<",总人数:"<<mapPlayerNum
//		<<",位置("<< pFindPlayer->GetFloatPosX()<<","<<pFindPlayer->GetFloatPosY()<<")，周围(玩家数:怪物数)=("
//		<<surPlayerNum<<":"<<surMonsterNum<<")"<<endl;
//
//	pGameManager->SendSystemChat( syschat.str().c_str() );
//
//	return GM_CMD_SUCCESS;
//}

#include "LogicAI/LogicAIEngine.h"
//重载AI脚本；
int CGMCmdManager::ReloadAIScript( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	TRY_BEGIN;

	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ReloadAIScript，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	logicai::LogicAIEngine::Refresh();

	pGameManager->SendSystemChat("重新加载AI脚本完成!\n");		

	return GM_CMD_SUCCESS;
	TRY_END;
	return GM_CMD_ERROR;
}

int CGMCmdManager::QueryGoodEvilPoint(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	stringstream syschat;
	syschat<<"玩家的善恶值为"<<  pGameManager->GetGoodEvilPoint() <<endl;
	pGameManager->SendSystemChat( syschat.str().c_str() );
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ShowGoodEvilPointTime(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	FullPlayerInfo * playerInfo = pGameManager->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CGMCmdManager::ShowGoodEvilPointTime, NULL == GetFullPlayerInfo()\n");
		return 0;
	}

	stringstream syschat;
	syschat<<"玩家处于恶魔时间为"<<playerInfo->baseInfo.uiEvilTime/EVIL_UPDATE_TIME<<endl;
	pGameManager->SendSystemChat( syschat.str().c_str() );
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ShowProfileAnalyze(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.empty() )
		return GM_CMD_ERROR;

	//所要打印的标记
	GET_ANALYZE_REPORT( cmdArgs[0] );

	stringstream syschat;
	syschat<<"分析"<<cmdArgs[0]<<"性能成功"<<endl;
	pGameManager->SendSystemChat( syschat.str().c_str() );

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::OpenProfileSwitch(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}
	OPEN_PROFILE_SWICH;

	stringstream syschat;
	syschat<<"打开了性能分析开关"<<endl;
	pGameManager->SendSystemChat( syschat.str().c_str() );

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::TurnOffProfileSwitch(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}
	CLOSE_PROFILE_SWICH;


	stringstream syschat;
	syschat<<"关闭了性能分析开关"<<endl;
	pGameManager->SendSystemChat( syschat.str().c_str() );

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::SetGoodEvilPoint(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GetAttribute，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	FullPlayerInfo * playerInfo = pGameManager->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CGMCmdManager::SetGoodEvilPoint, NULL == GetFullPlayerInfo()\n");
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.size() > 0 )
	{
		//int goodevilpoint = pGameManager->GetFullPlayerInfo()->baseInfo.sGoodEvilPoint;

		int point = STRING_TO_NUM(cmdArgs[0].c_str());
		pGameManager->OnGMUpdateGoodEvilPoint( point );

		stringstream syschat;
		syschat << "玩家" << pGameManager->GetNickName() << "的善恶值变为" << playerInfo->baseInfo.sGoodEvilPoint << endl;
		pGameManager->SendSystemChat( syschat.str().c_str() );
		return GM_CMD_SUCCESS;
	}

	return GM_CMD_ERROR;
}

int CGMCmdManager::SwichMapReq( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SwichMapReq，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.size() < 3 )
		return GM_CMD_ERROR;

	int mapid = STRING_TO_NUM( cmdArgs[0].c_str() );
	int x     = STRING_TO_NUM( cmdArgs[1].c_str() );
	int y     = STRING_TO_NUM( cmdArgs[2].c_str() );
	
	pGameManager->IssueSwitchMap( mapid, x, y );
	
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::SetLuckySystem( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SwichMapReq，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.size() != 2 )
		return GM_CMD_ERROR;

	unsigned int proLuck = (unsigned int)STRING_TO_NUM( cmdArgs[0].c_str() );
	unsigned int coeLuck = (unsigned int)STRING_TO_NUM( cmdArgs[1].c_str() );
	CSpSkillManager::SetProLuck( proLuck );
	CSpSkillManager::SetCoeLuck( coeLuck );
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::SendSpecialString( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SendSpecialString，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.size() != 2 )
	{
		return GM_CMD_ERROR;
	}

	//调试用GM命令，已不需要，去除，CMuxMap* pMap = pGameManager->GetCurMap();
	//if ( NULL == pMap )
	//{
	//	return GM_CMD_ERROR;
	//}

	//MGSendSpecialString gateMsg;
	//memset( &gateMsg, 0, sizeof(gateMsg) );
	//gateMsg.stringMsg.characterID.dwPID = 0;//无怪物；
	//gateMsg.stringMsg.characterID.wGID = MONSTER_GID;
	//gateMsg.stringMsg.stringType = stringType;
	//SafeStrCpy( gateMsg.stringMsg.stringContent, cmdArgs[1].c_str() );
	//gateMsg.stringMsg.stringLen = (unsigned int)strlen( gateMsg.stringMsg.stringContent );
	//pMap->NotifySurrounding<MGSendSpecialString>( &gateMsg, pGameManager->GetPosX(), pGameManager->GetPosY() );

	return GM_CMD_SUCCESS;
}

#ifdef ITEM_NPC 
int CGMCmdManager::ChangeItemSeqStatus( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SwichMapReq，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	CMuxMap* pMap = pGameManager->GetCurMap();
	if( !pMap )
	{
		D_WARNING( "GM命令ChangeItemSeqStatus，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.size() != 2 )
		return GM_CMD_ERROR;
	
	unsigned int itemSeq = (unsigned int)STRING_TO_NUM( cmdArgs[0].c_str() );
	unsigned int itemNpcStatus = (unsigned int)STRING_TO_NUM( cmdArgs[1].c_str() );
	if( !pMap->SetMapItemNpcState( itemSeq, itemNpcStatus ) )
	{
		return GM_CMD_ERROR;
	}
	return GM_CMD_SUCCESS;
}
#endif //ITEM_NPC

#ifdef OPEN_PUNISHMENT
int CGMCmdManager::StartPunishment( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  )
{
	if ( NULL == pGameManager )
	{
		return 0;
	}
	if ( !cmdArgs.empty() )
	{
		return 0;
	}
	CPunishmentManager::NotifyPunishStart();
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::EndPunishment( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  )
{
	if ( NULL == pGameManager )
	{
		return 0;
	}
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	CPunishmentManager::NotifyPunishEnd();
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::PunishPlayer( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  )
{
	if ( NULL == pGameManager )
	{
		return 0;
	}
	if ( !cmdArgs.empty() )
	{
		return 0;
	}
	CPunishmentManager::PunishingPlayer( pGameManager );
	return GM_CMD_SUCCESS;
}
#endif //OPEN_PUNISHMENT

//玩家离开当前副本;
int CGMCmdManager::OnLeaveCopy( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}

	if ( 0 != cmdArgs.size() )
	{
		//唯一参数:副本地图ID号；
		return GM_CMD_ERROR;
	}

	if ( !(pGameManager->IsPlayerCurInCopy()) )
	{
		D_WARNING( "GM指令，离开副本尝试，玩家%s当前不在副本中，忽略此命令\n", pGameManager->GetAccount() );
		return GM_CMD_ERROR;
	}

	D_DEBUG( "GM指令，离开副本尝试，玩家%s\n", pGameManager->GetAccount() );

	pGameManager->IssuePlayerLeaveCopyHF();

	return GM_CMD_SUCCESS;
}

//创建一个指定副本，并将该玩家加入此副本;
int CGMCmdManager::OnTryCopy( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}
	if ( 1 != cmdArgs.size() )
	{
		//唯一参数:副本地图ID号；
		return GM_CMD_ERROR;
	}

	unsigned int copymapid = atoi( cmdArgs[0].c_str() );

	pGameManager->TransPlayerToCopyQuest( copymapid, 1 );

	D_DEBUG( "GM指令，真副本尝试，玩家%s，副本类型%d\n", pGameManager->GetAccount(), copymapid );

	return GM_CMD_SUCCESS;
}

//创建一个指定副本，并将该玩家加入此副本;
int CGMCmdManager::OnTryPCopy( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}
	if ( 1 != cmdArgs.size() )
	{
		//唯一参数:副本地图ID号；
		return GM_CMD_ERROR;
	}

	unsigned int copymapid = atoi( cmdArgs[0].c_str() );

	pGameManager->TransPlayerToPCopyQuest( copymapid, 1 );

	D_DEBUG( "GM指令，伪副本尝试，玩家%s，副本类型%d\n", pGameManager->GetAccount(), copymapid );

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ReloadBattleScript( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if( NULL == pGameManager )
		return GM_CMD_ERROR;

	if( !CBattleScriptManager::Reload() )
	{
		pGameManager->SendSystemChat("重置脚本失败");
		return GM_CMD_ERROR;
	}

	return GM_CMD_SUCCESS;
}


int CGMCmdManager::OnGetAllPetSkill( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}

	pGameManager->GmGetPetAllPetSkill();
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::BeRaceMaster(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}

	AllRaceMasterManagerSingle::instance()->OnRecvNewRaceMasterBorn( pGameManager->GetRace(), pGameManager->GetDBUID(), pGameManager->GetNickName(), "GMTest" , true );
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ShowRaceMasterPlate(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}

	AllRaceMasterManagerSingle::instance()->ShowRaceMasterPlate( pGameManager->GetRace() );
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::RaceMasterNewPublicMsg(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}

	AllRaceMasterManagerSingle::instance()->OnPlayerSetNewPublicMsg( pGameManager, pGameManager->GetRace(), "GM测试");
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::RaceMasterDeclareWar( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}

	int otherrace = RandNumber( 1, 3 );
	AllRaceMasterManagerSingle::instance()->OnPlayerDeclareWarToOtherRace( pGameManager, pGameManager->GetRace(),otherrace );
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::TestWarRebirthPt( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令TestWarRebirth，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( (2 != cmdArgs.size()) && (3 != cmdArgs.size()) )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}
	
	//玩家所处的地图
	CMuxMap *pMap = pGameManager->GetCurMap();
	if(NULL == pMap)
	{
		pGameManager->SendSystemChat( "当前地图信息为空" );
		return GM_CMD_ERROR;
	}
	
	//是否设置内外城关系
	MapWarAdditionalInfo *pAddtionalInfo = pMap->GetWarAdditionalInfo();
	if(NULL == pAddtionalInfo)
	{
		pGameManager->SendSystemChat( "当前地图没有附加信息" );
		return GM_CMD_ERROR;
	}

	//是否有关联地图信息
	CMuxMap *pAssociatedMap = CManNormalMap::FindMap(pAddtionalInfo->associatedMapId);
	if(NULL == pAssociatedMap)
	{
		pGameManager->SendSystemChat( "当前地图没有关联地图信息\n" );
		return GM_CMD_ERROR;
	}

	//是否有关联的附加信息
	MapWarAdditionalInfo *pAssociatedAddtionalInfo = pAssociatedMap->GetWarAdditionalInfo();
	if(NULL == pAssociatedAddtionalInfo)
	{
		pGameManager->SendSystemChat( "关联地图没有附加信息\n" );
		return GM_CMD_ERROR;
	}

	stringstream syschat;
	int subcmd = ACE_OS::atoi(cmdArgs[0].c_str());//子命令[0:攻城战开关 1:复活点]
	int arg1 = ACE_OS::atoi(cmdArgs[1].c_str());//子命令的参数
	if((cmdArgs.size() == 2) && (subcmd == 0))//攻城战开关
	{
		bool onFlag = (arg1 == 0 ? true : false);//开关
		bool currWarState  = pAddtionalInfo->warStartFlag;//当前是否在攻城战

		if(onFlag && !currWarState)//开启攻城战
		{
			pAddtionalInfo->SetWarStart();
			pAssociatedAddtionalInfo->SetWarStart();
			syschat<<"玩家"<< pGameManager->GetNickName() << "开启攻城战" << endl;
		}
		else if(!onFlag && currWarState)//关闭攻城战
		{
			pAddtionalInfo->SetWarEnd();
			pAssociatedAddtionalInfo->SetWarEnd();
			syschat<<"玩家"<< pGameManager->GetNickName() << "关闭攻城战" << endl;
		}
		else
		{
			syschat<<"玩家"<< pGameManager->GetNickName() << "设置未起作用" << endl;
		}

		pGameManager->SendSystemChat( syschat.str().c_str() );
		return GM_CMD_SUCCESS;
	}
	
	if((cmdArgs.size() == 3) && (subcmd == 1))//复活点设置
	{
		unsigned short arg2 = ACE_OS::atoi(cmdArgs[2].c_str());//种族
		bool currWarState  = pAddtionalInfo->warStartFlag;//当前是否在攻城战
		if(!currWarState)
		{
			syschat<<"玩家"<< pGameManager->GetNickName() << "所处地图攻城战未开启" << endl;
			return GM_CMD_ERROR;
		}

		if(arg1 == 0)//设置当前地图的复活点归属
		{
			pAddtionalInfo->SetWarRebirthRace(arg2);
			pAddtionalInfo->SetWarSwitchRace(arg2);
			syschat<<"玩家:"<< pGameManager->GetNickName() << "设置当前地图：" << pMap->GetMapNo() << "复活点种族归属:" << arg2 << endl;
		}
		else//设置关联地图的复活点归属
		{
			pAssociatedAddtionalInfo->SetWarRebirthRace(arg2);
			pAssociatedAddtionalInfo->SetWarSwitchRace(arg2);
			syschat<<"玩家:"<< pGameManager->GetNickName() << "设置关联地图：" << pAssociatedMap->GetMapNo() << "复活点种族归属:" << arg2 << endl;
		}

		pGameManager->SendSystemChat( syschat.str().c_str() );
		return GM_CMD_SUCCESS;	
	}

	return GM_CMD_SUCCESS;
}


//开启攻城战
int CGMCmdManager::StartAttackWar( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	CWarTaskManager::NotifyAttackCityBegin();
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::ActivateCityWar( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ActivateCityWar，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.size() != 1 )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	bool bActivate = ( (atoi)(cmdArgs[0].c_str()) != 0 );
	CWarTaskManager::SetActivateFlag( bActivate );
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::ChangeSelfName( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ChangeSelfName，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.size() != 1 )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	MGChangeTargetName ctn;
	SafeStrCpy( ctn.changeMsg.changeName, cmdArgs[0].c_str() );
	ctn.changeMsg.nameLen = (unsigned int)strlen( ctn.changeMsg.changeName );
	ctn.changeMsg.targetID = pGameManager->GetFullID();
	CMuxMap* pMap = pGameManager->GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGChangeTargetName>( &ctn, pGameManager->GetPosX(), pGameManager->GetPosY() );
	}
	return GM_CMD_SUCCESS;
}


//设置当前地图的生成器
int CGMCmdManager::SetCreator( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ChangeSelfName，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( cmdArgs.size() != 2 )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	unsigned int creatorID = (unsigned int)atoi( cmdArgs[0].c_str() );
	bool isActive = ( atoi( cmdArgs[1].c_str() ) != 0 );
	
	CMuxMap* pMap = pGameManager->GetCurMap();
	if( pMap )
	{
		pMap->SetNpcCreatorActiveFlag( creatorID, isActive );
	}
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::AddWarTimer( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ChangeSelfName，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if( cmdArgs.size() != 6 )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	unsigned int timerID = (unsigned int)atoi( cmdArgs[0].c_str() );
	unsigned int leftSecond = (unsigned int)atoi( cmdArgs[1].c_str() );
	unsigned int stInfoTime = (unsigned int)atoi( cmdArgs[2].c_str() );
	unsigned int isClockWise = (unsigned int)atoi( cmdArgs[3].c_str() );
	bool isInformClient = ( (unsigned int)atoi( cmdArgs[4].c_str() ) != 0 );
	
	CMuxMap* pMap = pGameManager->GetCurMap();
	if( pMap )
	{
		CWarTimerManager& warTimerMan = pMap->GetWarTimerManager();
		warTimerMan.AddWarTimer( timerID, leftSecond, stInfoTime, isClockWise, isInformClient, cmdArgs[5].c_str() );
	}
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::TestBlockQuad( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令TestBlockQuad，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if( cmdArgs.size() != 2 )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}
	
	int regionID = (unsigned int)atoi( cmdArgs[0].c_str() );
	int val = (unsigned int)atoi( cmdArgs[1].c_str() );


	CMuxMap *pMap = pGameManager->GetCurMap();
	if(NULL == pMap)
	{
		pGameManager->SendSystemChat( "当前地图信息为空" );
		return GM_CMD_ERROR;
	}

	if(pMap->GetMapNo() != regionID/100)
	{
		pGameManager->SendSystemChat( "阻挡区编号和玩家所在地图编号不匹配" );
		return GM_CMD_ERROR;
	}

#ifdef HAS_PATH_VERIFIER
	pMap->SetBlockQuard(regionID, (0 != val) ? true : false);
	stringstream syschat;
	syschat << "修改碰撞信息,地图编号:" << pMap->GetMapNo() << "区域：" << regionID << "值:" << val; 
	pGameManager->SendSystemChat(syschat.str().c_str());
#endif/*HAS_PATH_VERIFIER*/

	return GM_CMD_SUCCESS;
}


//设置被动休息技能的回血间隔
int CGMCmdManager::SetRsInterval( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SetRsInterval，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if( cmdArgs.size() != 1 )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	unsigned int rsInterval = (unsigned int)atoi( cmdArgs[0].c_str() );
	m_rsInterval = rsInterval;
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::CreateRookieBreak( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SetRsInterval，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	pGameManager->CreateRookieBreak();
	return GM_CMD_SUCCESS;
}


//创建地图buff
int CGMCmdManager::CreateMapBuff( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager  )
{
	if ( NULL == pGameManager )
	{
		return GM_CMD_ERROR;
	}

	if( cmdArgs.empty() )
	{
		return GM_CMD_ERROR;
	}

	if( cmdArgs.size() != 3 )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	unsigned int race = (unsigned int)(atoi)(cmdArgs[0].c_str());
	unsigned int buffID = (unsigned int)(atoi)(cmdArgs[1].c_str());
	unsigned int buffTime = (unsigned int)(atoi)(cmdArgs[2].c_str());

	CMuxMap* pMap = pGameManager->GetCurMap();
	if( pMap )
	{
		pMap->CreateMapBuff( race, buffID, buffTime );
	}
	return GM_CMD_SUCCESS;
}

//返回角色选择
int CGMCmdManager::ReturnSelRole(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ReturnSelRole，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}
	
	MGReturnSelRole gateMsg;
	gateMsg.lNotiPlayerID = pGameManager->GetFullID();
	CGateSrv *pGateSrv = CManG_MProc::FindProc( pGameManager->GetGID() );
	if( NULL == pGateSrv )
	{
		D_ERROR("CGMCmdManager::ReturnSelRole 找不到ID为%d的GateSrv\n", pGameManager->GetGID() );
		return GM_CMD_ERROR;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( MGReturnSelRole, pGateSrv, gateMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );
	return GM_CMD_SUCCESS;
	
}


int CGMCmdManager::SelectRaceMaster(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SelectRaceMaster，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	MGStartSelectRaceMaster srvmsg;
	pGameManager->SendPkg<MGStartSelectRaceMaster>( &srvmsg );
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::OnKickPlayerRequest( const std::vector<std::string> &cmdArgs, const PlayerID& requestPlayerID )
{
	if( cmdArgs.empty() )
	{
		return GM_CMD_ERROR;
	}

	//这个命令可以直接通过名字来寻找
	CPlayer* pTargetPlayer = CPlayerManager::FindPlayerByRoleName( cmdArgs[0] );
	if( NULL == pTargetPlayer )
	{
		D_ERROR("CGMCmdManager::OnKickPlayerRequest 找不到目标玩家%s \n", cmdArgs[0].c_str() );
		MGGmCmdResult gateMsg;
		gateMsg.requestPlayerID = requestPlayerID;
		gateMsg.resultCode = 0;
		CGateSrv *pGateSrv = CManG_MProc::FindProc( requestPlayerID.wGID );
		if( NULL == pGateSrv )
		{
			D_ERROR("CGMCmdManager::OnKickPlayerRequest 找不到ID为%d的GateSrv\n", requestPlayerID.wGID );
			return GM_CMD_ERROR;
		}
		MsgToPut* pNewMsg = CreateSrvPkg( MGGmCmdResult, pGateSrv, gateMsg );
		pGateSrv->SendPkgToGateServer( pNewMsg );
		return GM_CMD_ERROR;
	}

	//有的话踢这个玩家下线
	CGateSrv* pGateSrv = CManG_MProc::FindProc( pTargetPlayer->GetGID() );
	if ( NULL == pGateSrv )
	{
		D_ERROR( "CGMCmdManager::OnKickPlayerRequest，找不到目标玩家(%d:%d)对应的gatesrv实例\n", pTargetPlayer->GetGID(), pTargetPlayer->GetPID() );
		return GM_CMD_ERROR;
	}
	pGateSrv->NotifyGateSrvKickPlayer( pTargetPlayer->GetFullID() );

	//还要通知用GM指令的玩家使用成功
	MGGmCmdResult gateMsg;
	gateMsg.requestPlayerID = requestPlayerID;
	gateMsg.resultCode = 1;		//表示成功
	pGateSrv = CManG_MProc::FindProc( requestPlayerID.wGID );
	if( NULL == pGateSrv )
	{
		D_ERROR("CGMCmdManager::OnKickPlayerRequest 找不到requestPlayer的ID为%d的GateSrv\n", requestPlayerID.wGID );
		return GM_CMD_ERROR;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( MGGmCmdResult, pGateSrv, gateMsg );
	pGateSrv->SendPkgToGateServer( pNewMsg );

	return GM_CMD_SUCCESS;
}





int CGMCmdManager::SetPlayerRace(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SetPlayerRace，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数
	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	unsigned int race = (unsigned int)(atoi)(cmdArgs[0].c_str());
	if( race > (unsigned int)(RACE_ELF) || race == 0 )
	{
		pGameManager->SendSystemChat( "GM指令参数错误" );
		return GM_CMD_ERROR;
	}

	PlayerInfo_1_Base& dbInfo = pGameManager->GetDBProp();
	dbInfo.ucRace = race;
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::OnShowStoragePlate(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令OnShowStoragePlate，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	pGameManager->GetPlayerStorage().ShowStoragePlate();
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::AddUnionActive(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddUnionActive，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数
	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	CUnion *pUnion = pGameManager->Union();
	if(NULL == pUnion)
	{
		pGameManager->SendSystemChat( "玩家没有所属工会" );
		return GM_CMD_ERROR;
	}

	unsigned int currentActive = pUnion->GetActive();
	int changeValue = atoi(cmdArgs[0].c_str());
	unsigned int newActive = currentActive + changeValue;

	if((changeValue > 0) && (newActive < currentActive))
	{	
		pGameManager->SendSystemChat( "GM指令操作后越界" );
		return GM_CMD_ERROR;
	}

	if((changeValue < 0) && (newActive > currentActive))
	{
		pGameManager->SendSystemChat( "GM指令操作后越界" );
		return GM_CMD_ERROR;
	}

	pGameManager->ChanageUnionActive(0, changeValue);

	//通知活跃度
	stringstream syschat;
	syschat<<"玩家"<<pGameManager->GetNickName()<<"所属工会最新活跃度为:"<< newActive << endl;

	pGameManager->SendSystemChat( syschat.str().c_str() );

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::AddUnionPrestige(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddUnionPrestige，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数
	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	CUnion *pUnion = pGameManager->Union();
	if(NULL == pUnion)
	{
		pGameManager->SendSystemChat( "玩家没有所属工会" );
		return GM_CMD_ERROR;
	}

	unsigned int currentPrestige = pUnion->GetPrestige();
	int changeValue = atoi(cmdArgs[0].c_str());
	unsigned int newPrestige = currentPrestige + changeValue;

	if((changeValue > 0) && (newPrestige < currentPrestige))
	{	
		pGameManager->SendSystemChat( "GM指令操作后越界" );
		return GM_CMD_ERROR;
	}

	if((changeValue < 0) && (newPrestige > currentPrestige))
	{
		pGameManager->SendSystemChat( "GM指令操作后越界" );
		return GM_CMD_ERROR;
	}

	pGameManager->ChanageUnionPrestige(0, changeValue);

	//通知威望
	stringstream syschat;
	syschat<<"玩家"<<pGameManager->GetNickName()<<"所属工会最新威望为:"<< newPrestige << endl;

	pGameManager->SendSystemChat( syschat.str().c_str() );

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::AddPetExp(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddUnionActive，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数
	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	unsigned long addExp = atoi( cmdArgs[0].c_str() );
	pGameManager->AddPetExp( addExp );
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::SetPetHunger(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddUnionActive，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数
	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	unsigned long setHunger = atoi( cmdArgs[0].c_str() );
	if( !pGameManager->SetPetHunger( setHunger ) )
	{
		pGameManager->SendSystemChat( "GM指令参数范围错误" );
		return GM_CMD_ERROR;
	}
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::ShowPetHunger(  const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	ACE_UNUSED_ARG( cmdArgs );

	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ShowPetHunger，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	CMuxPet* pPet = pGameManager->GetPet();
	if( pPet)
	{
		unsigned int petHunger = pPet->GetHunger();
		stringstream ss;
		ss <<"当前宠物的饥饿度为"  <<petHunger; 
		pGameManager->SendSystemChat( ss.str().c_str() );
	}
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ShowProtectPlate(const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddUnionActive，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数
	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}
	
	unsigned int type = atoi( cmdArgs[0].c_str() );
	pGameManager->GetProtectItemPlate().ShowProtectItemPlate( (PROTECTITEM_PLATE_TYPE)type );
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::AddCityExp( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )	//增加关卡城市的经验
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddUnionActive，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数
	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}
	
	unsigned int addExp = atoi( cmdArgs[0].c_str() );
	CMuxMap* pMap = pGameManager->GetCurMap();
	if( NULL != pMap )
	{
		pMap->AddCityExp( addExp );
	}

	return GM_CMD_SUCCESS;
}


int CGMCmdManager::GetCityInfo( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )	//获取关卡地图的信息
{
	if ( !cmdArgs.empty() )
	{
		return 0;
	}

	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令AddUnionActive，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	CMuxMap* pMap = pGameManager->GetCurMap();
	if( NULL == pMap )
		return GM_CMD_ERROR;

	CityInfo* pCityInfo = pMap->GetCityInfo();
	if( NULL == pCityInfo )
	{
		pGameManager->SendSystemChat("玩家所在的地图没有关卡地图信息");
		return GM_CMD_ERROR;
	}

	std::stringstream ss;
	ss <<"城市等级为"  << pCityInfo->cityLevel  <<"     城市经验值为"   << pCityInfo->cityExp  <<"      城市归属为" 
		<< pCityInfo->ucRace;

	pGameManager->SendSystemChat( ss.str().c_str() );
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::SubmitTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )		//提交任务
{
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令SubmitTask，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数
	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	unsigned int taskID = (unsigned int)atoi( cmdArgs[0].c_str() );
	pGameManager->SetTaskStat( taskID, NTS_FINISH );
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::GivePunishTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )	//添加天谴任务
{
	if ( !cmdArgs.empty() )
	{
		return GM_CMD_ERROR;
	}

	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GivePunishTask，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if( !pGameManager->GivePunishTask() )
	{
		pGameManager->SendSystemChat("添加天谴任务失败,不在天谴时间内");
		return GM_CMD_ERROR;
	}

	return GM_CMD_SUCCESS;
}


int CGMCmdManager::FinishPunishTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )	//完成天谴任务
{
	if ( !cmdArgs.empty() )
	{
		return GM_CMD_ERROR;
	}

	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令FinishPunishTask，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if( !pGameManager->FinishPunishTask() )
	{
		pGameManager->SendSystemChat("结束天谴任务失败,您还尚接受天谴任务");
		return GM_CMD_ERROR;
	}

	return GM_CMD_SUCCESS;
}


int CGMCmdManager::GiveUpPunishTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )	//放弃天谴任务
{
	if ( !cmdArgs.empty() )
	{
		return GM_CMD_ERROR;
	}

	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令FinishPunishTask，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if( !pGameManager->GiveUpPunishTask() )
	{
		pGameManager->SendSystemChat("放弃天谴任务失败,您还尚接受天谴任务");
		return GM_CMD_ERROR;
	}

	return GM_CMD_SUCCESS;
}


int CGMCmdManager::KickAllPlayer( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )  //踢去所有玩家
{
	ACE_UNUSED_ARG( cmdArgs );
	//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令KickAllPlayer，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	MGKickAllPlayer kickMsg;
	CManG_MProc::SendMsgToAllGate<MGKickAllPlayer>( &kickMsg );
	return GM_CMD_SUCCESS;
}


int CGMCmdManager::GiveTask( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )		//添加任务
{
		//判断玩家是否为空
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令GiveTask，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	//必须是1个参数
	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	unsigned int taskID = (unsigned int)atoi( cmdArgs[0].c_str() );

	pGameManager->AddTask( taskID );
	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ModifyPWState( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ModifyPWState，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( 2 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	MGModifyPWStateForward forward;
	forward.pwId = (unsigned short)STRING_TO_NUM(cmdArgs[0].c_str());
	forward.newState = (unsigned char)STRING_TO_NUM(cmdArgs[1].c_str());
	pGameManager->SendPkg<MGModifyPWStateForward>(&forward);

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ModifyPWMapState( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ModifyPWMapState，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( 3 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	MGModifyPWMapStateForward forward;
	forward.pwId = (unsigned short)STRING_TO_NUM(cmdArgs[0].c_str());
	forward.pwMapId = (unsigned short)STRING_TO_NUM(cmdArgs[1].c_str());
	forward.newState = (unsigned char)STRING_TO_NUM(cmdArgs[2].c_str());
	pGameManager->SendPkg<MGModifyPWMapStateForward>(&forward);

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::ShiftPW( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令ShiftPW，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	MGShiftPWReqForward forward;
	forward.targetPWId = (unsigned short)STRING_TO_NUM(cmdArgs[0].c_str());
	pGameManager->SendPkg<MGShiftPWReqForward>(&forward);

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::KickOffPW( const std::vector<std::string> &cmdArgs, CPlayer* pGameManager )
{
	if ( NULL == pGameManager )
	{
		D_WARNING( "GM命令KickOffPW，输入GM玩家的指针空\n" );
		return GM_CMD_ERROR;
	}

	if ( 1 != cmdArgs.size() )
	{
		pGameManager->SendSystemChat( "GM指令参数数目错误" );
		return GM_CMD_ERROR;
	}

	MGKickOffPWForward forward;
	forward.targetPWId = (unsigned short)STRING_TO_NUM(cmdArgs[0].c_str());
	pGameManager->SendPkg<MGKickOffPWForward>(&forward);

	return GM_CMD_SUCCESS;
}

int CGMCmdManager::_ChangePhyLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)	//变更物理技能等级
{
	return 1;//
	//10.09.20已废弃//PlayerInfo_1_Base& dbInfo = pPlayer->GetDBProp();
	//dbInfo.usPhysicsLevel = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + dbInfo.usPhysicsLevel;

	//pPlayer->ReloadPhyProp();

	//MGRoleattUpdate updateAttr;
	//updateAttr.nType = RT_PHYLEVEL;
	//updateAttr.nOldValue = 0;
	//updateAttr.nNewValue = dbInfo.usPhysicsLevel;
	//updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	//updateAttr.changeTime = 0;
	//updateAttr.bFloat = false;
	//pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	//return 1;
}


int CGMCmdManager::_ChangeMagLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)	//变更魔法技能等级
{
	return 1;
	//10.09.20已废弃//PlayerInfo_1_Base& dbInfo = pPlayer->GetDBProp();
	//dbInfo.usMagicLevel = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + dbInfo.usMagicLevel;

	//pPlayer->ReloadMagProp();

	//MGRoleattUpdate updateAttr;
	//updateAttr.nType = RT_MAGLEVEL;
	//updateAttr.nOldValue = 0;
	//updateAttr.nNewValue = dbInfo.usMagicLevel;
	//updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	//updateAttr.changeTime = 0;
	//updateAttr.bFloat = false;
	//pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );
	//return 1;
}


int CGMCmdManager::_ChangeMaxPhyLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	return 1;
	//10.09.20已废弃//PlayerInfo_1_Base& dbInfo = pPlayer->GetDBProp();
	//dbInfo.ucMaxPhyLevelLimit = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + dbInfo.ucMaxPhyLevelLimit;

	////还要更改最大熟练度限制
	//pPlayer->ReloadPhyProp();

	//MGRoleattUpdate updateAttr;
	//updateAttr.nType = RT_PHYLEVEL_MAX;
	//updateAttr.nOldValue = 0;
	//updateAttr.nNewValue = dbInfo.ucMaxPhyLevelLimit;
	//updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	//updateAttr.changeTime = 0;
	//updateAttr.bFloat = false;
	//pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );

	//////该消息已废除，因为相关属性已另行计算或通知, by dzj, 10.05.26, MGTmpInfoUpdate tmpInfo;
	////tmpInfo.lNotiPlayerID = pPlayer->GetFullID();
	////tmpInfo.playerID = pPlayer->GetFullID();
	////tmpInfo.tmpInfo = pPlayer->GetTmpProp();
	////pPlayer->SendPkg<MGTmpInfoUpdate>( &tmpInfo );

	//pPlayer->AddPhyExp( 0 );
	//return 1;
}


int CGMCmdManager::_ChangeMaxMagLevel( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal)
{
	return 1;
	//10.09.20已废弃//PlayerInfo_1_Base& dbInfo = pPlayer->GetDBProp();
	//dbInfo.ucMaxMagLevelLimit = (nType == TYPE_SET)? nUpdateVal : nUpdateVal + dbInfo.ucMaxMagLevelLimit;

	////还要更改最大熟练度限制
	//pPlayer->ReloadMagProp();

	//MGRoleattUpdate updateAttr;
	//updateAttr.nType = RT_MAGLEVEL_MAX;
	//updateAttr.nOldValue = 0;
	//updateAttr.nNewValue = dbInfo.ucMaxMagLevelLimit;
	//updateAttr.lChangedPlayerID = pPlayer->GetFullID();
	//updateAttr.changeTime = 0;
	//updateAttr.bFloat = false;
	//pPlayer->SendPkg<MGRoleattUpdate>( &updateAttr );

	//////该消息已废除，因为相关属性已另行计算或通知, by dzj, 10.05.26, MGTmpInfoUpdate tmpInfo;
	////tmpInfo.lNotiPlayerID = pPlayer->GetFullID();
	////tmpInfo.playerID = pPlayer->GetFullID();
	////tmpInfo.tmpInfo = pPlayer->GetTmpProp();
	////pPlayer->SendPkg<MGTmpInfoUpdate>( &tmpInfo );

	//pPlayer->AddMagExp( 0 );
	//return 1;
}


int CGMCmdManager::_ChangeSpPower( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal )
{
	if (nType == TYPE_SET)
	{
		D_WARNING("_ChangeSpPower 熟练度不允许设置\n");
		return GM_CMD_ERROR;	
	}

	pPlayer->AddSpPower( nUpdateVal );
	pPlayer->SendSystemChat( "GM命令操作修改SP能量成功" );
	return 1;
}


int CGMCmdManager::_ChangeRacePrestige( CPlayer* pPlayer, ChangeType nType, unsigned int nUpdateVal )
{
	if (nType == TYPE_SET)
	{
		D_WARNING("_ChangeSpPower 种族声望不允许设置\n");
		return GM_CMD_ERROR;	
	}

	pPlayer->AddRacePrestige( nUpdateVal );
	pPlayer->SendSystemChat( "GM命令操作修改种族声望成功" );
	return 1;
}

