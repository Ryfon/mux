﻿#include "PlayerStateManager.h"
#include "RideState.h"
#include "Item/ItemManager.h"
#include "Item/CItemPropManager.h"
#include <assert.h>

//进入骑乘状态
void CRideState::OnBegin()
{
	m_bActive = false;
	
	CPlayer* pPlayer = mpStateMan->GetAttachPlayer();
	if( NULL == pPlayer )
	{
		D_ERROR( "CRideState::OnBegin, NULL == pPlayer\n" );
		return;
	}

	ResetRideInviteState();

	RideTeam* pTeam = NULL;
	//如果是队长
	if ( m_rideType == E_LEADER )
	{
		CBaseItem* pItem = NULL;
		ItemInfo_i* pItemInfo = NULL;
		float speed = 0.0f;
		bool isSuccess= false;

		//骑乘装备
		if ( m_rideItemState == E_RIDE_EQUIP )
		{
			pItemInfo = pPlayer->GetValidDriveEquip();
			if ( NULL == pItemInfo )
			{
				D_ERROR( "CRideState::OnBegin, 玩家%s, E_RIDE_EQUIP, NULL == pItemInfo\n", pPlayer->GetAccount() );
				return;
			}
			//const PlayerInfo_2_Equips& equipInfo = pPlayer->GetFullPlayerInfo()->equipInfo;
			//unsigned int driveIndex = EQUIPTYPE_POS[E_DRIVE];
			//if ( driveIndex >= ARRAY_SIZE(equipInfo.equips) )
			//{
			//	D_ERROR( "CRideState::OnBegin, driveIndex(%d) >= ARRAY_SIZE(equipInfo.equips)\n", driveIndex );
			//	return;
			//}
			////获取骑乘位置上的道具
			//itemInfo = equipInfo.equips[ driveIndex ];
			//if ( itemInfo.uiID == PlayerInfo_2_Equips::INVALIDATE )
			//{
			//	D_DEBUG("骑乘位置上根本没有道具\n");
			//	return;
			//}

			if ( pItemInfo->usWearPoint == 0 )
			{
				pPlayer->SendSystemChat( "CRideState::OnBegin, 骑乘装备耐久度为0，不能上马" );
				return;
			}

			pItem = pPlayer->GetItemDelegate().GetEquipItem( pItemInfo->uiID );
			if ( NULL == pItem )
			{
				D_ERROR("玩家%s激活骑乘道具，但对应装备的骑乘道具在玩家装备管理器中找不到:%d\n", pPlayer->GetNickName(), pItemInfo->uiID );
				return;
			}

			CMountItem* pMountItem = dynamic_cast<CMountItem *>( pItem );
			if ( NULL == pMountItem )
			{
				D_ERROR( "CRideState::OnBegin, 玩家%s, NULL == pMountItem\n", pPlayer->GetAccount() );
				return;
			}

			//转变成骑乘装备
			if ( pMountItem->GetItemSpeed() > 0)
			{
				speed = (float) pMountItem->GetItemSpeed();
			}
		
			isSuccess = true;
		} else if ( m_rideItemState == E_RIDE_CONSUME )	{ //如果是消耗品
			unsigned itemType = GetRideItemType();
			FunctionProperty* functionProp = CFunctionPropertyManagerSingle::instance()->GetSpecialPropertyT( itemType );
			if ( NULL == functionProp )
			{
				D_ERROR( "CRideState::OnBegin, NULL == functionProp, 玩家%s, itemType%d", pPlayer->GetAccount(), itemType );
				return;
			}
			////增加速度
			speed  = functionProp->AddSpeed;
			isSuccess = true;
		} else {
			D_ERROR( "CRideState::OnBegin, m_rideItemState 未知类型%d，玩家%s骑乘失败\n", m_rideItemState, pPlayer->GetAccount() );
			return;
		}

		//如果成功使用骑乘
		if ( isSuccess )
		{
			m_bActive = true;

			if( pPlayer->IsSpeedUp() )
			{
				if( !pPlayer->EndSpeedUpBuff() )
				{
					D_ERROR( "CRideState::OnBegin,玩家%s有加速BUFF却结束加速BUFF失败\n", pPlayer->GetAccount() );
				}
			}

			//激活骑乘状态
			pPlayer->ActiveRideState();

			if ( NULL != pItemInfo )
			{
				SetRideItemID( pItemInfo->uiID );
			} else {
				//记录该小队使用的道具
				D_WARNING( "CRideState::OnBegin,玩家%s使用了E_RIDE_CONSUME类型骑乘道具，SetRideItemID为0\n", pPlayer->GetAccount() );
				SetRideItemID( 0 );
			}

			//创建骑乘队伍
			if ( m_rideItemState == E_RIDE_EQUIP )
			{
				 pTeam = RideTeamManagerSingle::instance()->NewRideTeam( pPlayer, pItem, speed);
			} else {
				 pTeam = RideTeamManagerSingle::instance()->NewRideTeam( pPlayer, GetRideItemType(),speed );
			}

			if ( NULL == pTeam )
			{
				D_ERROR("CRideState::OnBegin, 玩家%s, 骑乘队伍创建失败!\n", pPlayer->GetNickName() );
				return;
			}

			//设置所属的队伍
			SetRideTeam( pTeam );

			MGMountStateChange mountState;
			mountState.rideState  = m_rideItemState;

			//如果是消耗品,则需要记录本次使用的道具类型
			if( m_rideItemState == E_RIDE_CONSUME )
			{
				mountState.attachInfo1 = GetRideItemType();
			} else {
				mountState.attachInfo1 = 0;
			}

			mountState.attachInfo2 = 0;
			pPlayer->SendPkg<MGMountStateChange>( &mountState );
		}		
	} else if( m_rideType == E_ASSIST )	{ //如果是队员
		//assert( m_pTeam!= NULL );
		if ( NULL == m_pTeam )
		{
			D_ERROR( "CRideState::OnBegin, m_rideType(E_ASSIST), 玩家%s, NULL==m_pTeam\n", pPlayer->GetAccount() );
			return;
		}
		m_bActive = true;
		//加入队伍
		m_pTeam->EnterRideTeam( pPlayer );
		//激活骑乘状态
		pPlayer->ActiveRideState();
	} else {
		D_ERROR( "CRideState::OnBegin, 玩家%s, m_rideType%d，即不是队长也不是队员\n", pPlayer->GetAccount(), m_rideType );
		return;
	}

	D_ERROR( "CRideState::OnBegin, 玩家%s, 不可能到此处\n", pPlayer->GetAccount() );
	return;
}

//退出骑乘状态
void CRideState::OnEnd()
{
	CPlayer* pPlayer = mpStateMan->GetAttachPlayer();
	if ( NULL == pPlayer )
	{
		D_ERROR( "CRideState::OnEnd, NULL == pPlayer\n" );
		return;
	}

	//如果是队长,通知GateSrv状态改变
	if ( m_rideType == E_LEADER )
	{
		ItemInfo_i* pRideItem = pPlayer->GetValidDriveEquip();
		if ( NULL == pRideItem )
		{
			D_ERROR( "CRideState::OnEnd, NULL == pRideItem, 玩家:%s\n", pPlayer->GetAccount() );
			return;
		}
		pPlayer->UseItemAffectWear( pRideItem );
		//PlayerInfo_2_Equips& equipInfo = pPlayer->GetFullPlayerInfo()->equipInfo;
		//unsigned int tmpindex = EQUIPTYPE_POS[E_DRIVE];
		//if ( tmpindex >= ARRAY_SIZE(equipInfo.equips) )
		//{
		//	D_ERROR( "CRideState::OnEnd, tmpindex(%d) >= ARRAY_SIZE(equipInfo.equips)\n", tmpindex );
		//	return;
		//}
		//ItemInfo_i& rideItem = equipInfo.equips[ EQUIPTYPE_POS[E_DRIVE] ];
		//pPlayer->UseItemAffectWear( const_cast<ItemInfo_i *>( &rideItem ) );
	} else if ( m_rideType == E_ASSIST ) {//如果时骑乘队员,那么离队后通知GateSrv端骑乘队长保存的的骑乘信息
		if ( NULL == m_pTeam )
		{
			D_ERROR( "CRideState::OnEnd, NULL == m_pTeam, 玩家:%s\n", pPlayer->GetAccount() );
			return;
		}
		//更新GateSrv中保存的骑乘队伍的队员信息
		CPlayer* pTmpLeader = m_pTeam->GetRideLeader();
		if ( NULL == pTmpLeader )
		{
			D_ERROR( "CRideState::OnEnd, NULL == m_pTmpLeader, 玩家:%s\n", pPlayer->GetAccount() );
			return;
		}
		MGMountStateChange mountState;
		mountState.rideState  = E_RIDE_INFO_UPDATE;
		mountState.attachInfo1 = 0;
		mountState.attachInfo2 = 0;
		pTmpLeader->SendPkg<MGMountStateChange>( &mountState );
	} else {
		D_ERROR( "CRideState::OnEnd, 玩家%s, 既不是队长也不是队员\n", pPlayer->GetAccount() );
		return;
	}

	ReSet();
	//取消骑乘状态
	pPlayer->CancelRideState();

	MGMountStateChange mountState;
	mountState.rideState  = E_UNRIDE;
	mountState.attachInfo1 = 0;
	mountState.attachInfo2 = 0;
	pPlayer->SendPkg<MGMountStateChange>( &mountState );

	return;
}

void CRideState::SetRideTeam(RideTeam *pTeam)
{
	m_pTeam = pTeam;
}

long CRideState::GetRideTeamID()
{
	if( m_pTeam )
		return m_pTeam->GetRideTeamID();

	return 0;
}

void CRideState::SetPlayerRideType( RIDETYPE rideType )
{
	m_rideType = rideType; 
	if ( NULL == mpStateMan )
	{
		D_ERROR( "SetPlayerRideType, NULL == mpStateMan\n" );
		return;
	}
	CPlayer * pPlayer = mpStateMan->GetAttachPlayer();
	if ( NULL == pPlayer )
	{
		D_ERROR( "SetPlayerRideType, NULL == pPlayer\n" );
		return;
	}
	if ( rideType == E_LEADER )
	{
		D_DEBUG("%s成为骑乘队长\n",pPlayer->GetNickName() );
	} else {
		D_DEBUG("%s成为骑乘队员\n",pPlayer->GetNickName() );
	}
	return;
}

void CRideState::ReSet()
{
	m_bActive = false;
	m_rideItemUID = 0;
	m_rideType = E_RIDETYPE_MAX;
	m_rideItemState = E_UNRIDE ;
	m_pTeam = NULL;
	m_rideItemType = 0;
	ResetRideInviteState();
}

void CRideState::ResetRideInviteState()
{
	ClearInvitePlayerList();
}

bool CRideState::IsRideTeamFull()
{
	if(m_pTeam)
	{ 
		return m_pTeam->IsTeamFull();
	} 
	
	return false;
}

void CRideState::ClearInvitePlayerList()
{
	if ( NULL == mpStateMan )
	{
		D_ERROR( "ClearInvitePlayerList, NULL == mpStateMan\n" );
		return;
	}
	CPlayer* pPlayer = mpStateMan->GetAttachPlayer();
	if ( !pPlayer )
		return;

	//1.先将自己邀请玩家骑乘列表删除
	mInviteTargetPlayerVec.clear();

	//2.遍历所有邀请过该玩家的玩家列表
	if ( !mBeInvitelaunchPlayerVec.empty() )
	{
		unsigned int playerUID = pPlayer->GetDBUID();	
		//依次找到邀请过该玩家骑乘的玩家,将该玩家从他们的邀请列表中删除
		for ( std::vector<BeInviteItem>::iterator lter = mBeInvitelaunchPlayerVec.begin(); lter != mBeInvitelaunchPlayerVec.end();  ++lter )
		{
			const BeInviteItem& beinviteItem = *lter;
			CGateSrv* pGateSrv = CManG_MProc::FindProc( beinviteItem.playerID.wGID );
			if ( pGateSrv )
			{
				CPlayer* pRidelaunchPlayer = pGateSrv->FindPlayer( beinviteItem.playerID.dwPID );//如果还能找到邀请玩家，
				if ( pRidelaunchPlayer && pRidelaunchPlayer->GetDBUID() == beinviteItem.playerUID  )
				{
					pRidelaunchPlayer->GetSelfStateManager().GetRideState().RemoveInvitePlayer( playerUID );//把自己从每个邀请该玩家上马的玩家的邀请名单中删除
				}
			}
		}

		//3.清空所有邀请过自己骑乘的玩家列表
		mBeInvitelaunchPlayerVec.clear();
	}
}

void CRideState::PushInviteTargetPlayerList(unsigned int playerUID )
{
	InviteItem inviteItem;
	inviteItem.playerUID  = playerUID;
	mInviteTargetPlayerVec.push_back( inviteItem );
}

void CRideState::PushInviteLaunchPlayerList(const PlayerID& launchPlayerID, unsigned int playerUID )
{
	BeInviteItem beinviteItem;
	beinviteItem.playerID  = launchPlayerID;
	beinviteItem.playerUID = playerUID;
	mBeInvitelaunchPlayerVec.push_back( beinviteItem );
}

bool CRideState::IsInvitingTargetPlayer(unsigned int playerUID )
{
	if ( mInviteTargetPlayerVec.empty() )
		return false;

	for ( std::vector<InviteItem>::iterator lter = mInviteTargetPlayerVec.begin();
		lter != mInviteTargetPlayerVec.end();
		++lter )
	{
		const InviteItem& inviteItem = *lter;
		if ( inviteItem.playerUID == playerUID )
			return true;
	}
	return false;
}

void CRideState::RemoveInvitePlayer(unsigned int playerUID )
{
	if ( mInviteTargetPlayerVec.empty() )
		return;

	for ( std::vector<InviteItem>::iterator lter = mInviteTargetPlayerVec.begin();
		  lter != mInviteTargetPlayerVec.end(); 
		  ++lter )
	{
		const InviteItem& inviteItem = *lter;
		if ( inviteItem.playerUID == playerUID )
		{
			mInviteTargetPlayerVec.erase( lter );
			return;
		}
	}
}

void CRideState::RemoveInviteLaunchPlayer( unsigned int playerUID )
{
	if ( mBeInvitelaunchPlayerVec.empty() )
		return;

	for ( std::vector<BeInviteItem>::iterator lter = mBeInvitelaunchPlayerVec.begin(); 
		  lter != mBeInvitelaunchPlayerVec.end(); 
		  ++lter )
	{
		const BeInviteItem& beinviteItem = *lter;
		if ( beinviteItem.playerUID == playerUID )
		{
			mBeInvitelaunchPlayerVec.erase( lter );
			return;
		}
	}
}

void CRideState::GetTeamAllMemberID(PlayerID& leadid, PlayerID& memberid )
{
	leadid.dwPID = leadid.wGID = memberid.dwPID = memberid.wGID = 0;
	if ( m_pTeam )
	{
		m_pTeam->GetRideTeamMember( leadid, memberid );
	}
}

/////////////////////////  组队信息 ///////////////////////////////////////////
void CGroupState::SetOwnerGroupTeam(CGroupTeam* pGroupTeam )
{
	if ( NULL == pGroupTeam )
	{
		D_ERROR( "CGroupState::SetOwnerGroupTeam，NULL==pGroupteam\n" );
		return;
	}

	if ( pGroupTeam )
		m_pGroupTeam = pGroupTeam;
}

void CGroupState::OnBegin()
{
	if ( NULL == mpStateMan )
	{
		D_ERROR( "CGroupState::OnBegin, NULL == mpStateMan\n" );
		return;
	}
	CPlayer* pOwner = mpStateMan->GetAttachPlayer();
	if ( pOwner ){
		//[玩家的组队状态发生改变时 simpleinfochange]
		pOwner->OnSimpleInfoChange();
	}
	m_bActive = true;
}

void CGroupState::OnEnd()
{
	if ( NULL == mpStateMan)
	{
		D_ERROR( "CGroupState::OnEnd, NULL == mpStateMan\n" );
		return;
	}
	//assert( m_bActive == true );
	if ( !m_bActive )
	{
		D_ERROR( "CGroupState::OnEnd, m_bActive\n" );
	}

	CPlayer* pOwner = mpStateMan->GetAttachPlayer();
	if ( pOwner ){
		//[玩家的组队状态发生改变时 simpleinfochange]
		pOwner->OnSimpleInfoChange();
	}

	m_bActive = false;
	m_pGroupTeam = NULL;
}

CGroupTeam* CGroupState::GetGroupTeam()
{
	return m_pGroupTeam;
}

unsigned long CGroupState::GetGroupTeamID()
{
	if ( NULL == m_pGroupTeam )
	{
		D_ERROR( "CGroupState::GetGroupTeamID, NULL == m_pGroupTeam\n" );
		return -1;
	}

	return m_pGroupTeam->GetGroupTeamID();
}

