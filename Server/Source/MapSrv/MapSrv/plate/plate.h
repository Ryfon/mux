﻿/**
* @file plate.h
* @brief 升级、追加、改造用托盘定义
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: plate.h
* 摘    要: 升级、追加、改造用托盘定义
* 作    者: dzj
* 完成日期: 2008.07.30
*
* 修改管传淇
*
*/

#pragma once

#include "../Item/ItemManager.h"
#include "../Item/ItemChangeCri.h"

class CPlayer;

#define PLATE_ASSTPOS_NUM 26 //托盘辅料位置个数；
#define CON_INDEX 1 //条件的索引
#define CRI_INDEX 3 //增加概率的索引
#define GOOD_INDEX 2 //保宝石的索引

///描述托盘条件；
struct PlateCondition
{
	TYPE_ID       conType;//需要的类型；
	unsigned long conNum;//需要的该类型数量；

	PlateCondition():conType(0),conNum(0){}
};


struct ChangeCriRd
{
	unsigned int odd;
	unsigned int sumodd;
	unsigned int level;
	unsigned int masslevel;

	void Reset()
	{
		odd = sumodd = level = masslevel = 0;
	}
};

struct LevelUpCriRd
{
	unsigned int levelUpOdd;
	unsigned int levelUpSumOdd;
	float        levelUpCri;
	int          updateFailstart;
	int          updateFailEnd;

	void Reset()
	{
		levelUpOdd = levelUpSumOdd  = 0;
		levelUpCri = 0.0f;
		updateFailstart = updateFailEnd = 0;
	}

};

enum ADD_ASSIST_ERRNO
{
	E_ITEM_INVALID  = 0x1,
	E_NOT_CHANGE_ITEM,//不符合改造规则的物品
	E_NOT_UPDATE_ITEM,//不符合升级规则的物品
	E_NOT_ADD_ITEM,//不符合配方要求
	E_NOT_CHANGE_CONDITION,//不符合改造条件
	E_NOT_UPDATE_CONDITION,//不符合升级条件
	E_NOT_ADD_CONDITION,//不符合追加条件
	E_TAKE_ITEM_BEFORE_ADD,//放入物品前，请取出物品
	E_UPDATE_ITEM_NOT_REPLACE,//升级的物品不能被替换
	E_UPDATE_AXIOLOGY_NOT_REPLACE,//升级的宝石不能被替换
};

///升级、追加、改造用托盘
///本类只保存物品的ID号，真正物品从玩家包裹中取走等TryExec时才执行；
class CCommonPlate
{
public:
	CCommonPlate():m_playerOwner(NULL) {};
	~CCommonPlate() {};

public:	
	///初始化托盘;
	void Init( PlayerID ownerID, EPlateType inPlateType )
	{
		ACE_UNUSED_ARG( ownerID );
		ResetPlate();
		m_plateType = inPlateType;
	};

	void AttachPlayer( CPlayer* pPlayer ) 
	{
		ResetPlate();
		m_playerOwner = pPlayer;
	}

	///取当前托盘类型；
	EPlateType GetPlateType()
	{
		return m_plateType;
	}

    ///重置拖盘，将主料与辅料都放回玩家包裹，由于真正物品从玩家包裹中取走等TryExec时才执行，因此实际上只需清空即可；
	void ResetPlate()
	{
		m_plateType = INVALID_PLATE;
		m_isConFulfil = false;
		m_MainItem.uiID = m_MainItem.usItemTypeID = 0;
		m_updateID =0;
		m_changeCriRd.Reset();
		m_levelUpCriRd.Reset();
		m_AssistItemCount = 0;
		m_isConRead = false;
		m_consumeMoney = 0;
		for ( int i=0; i<PLATE_ASSTPOS_NUM; ++i )
		{
			if ( ( i >= ARRAY_SIZE(m_AsstItem) )
				|| ( i >= ARRAY_SIZE(m_plateCon) )
				)
			{
				D_ERROR( "ResetPlate, i(%d)越界, m_AsstItem或m_plateCon\n", i );
				return;
			}
			m_AsstItem[i].uiID = m_AsstItem[i].usItemTypeID = 0;
			m_plateCon[i].conType = m_plateCon[i].conNum = 0;//清空条件；
		}
	};

	bool IsConFulfil()
	{
		return m_isConFulfil;
	}

	///增加主位置物品，如果新放入物品不可加工，或者新放入物品原来就在主料位置，则返回false; 
	bool AddMainItem( CPlayer* pPlayer, ItemInfo_i inItemInfo , int& errNo );

	///增加辅位置物品，返回false说明位置不够;
	bool AddAsstItem( CPlayer* pPlayer, ItemInfo_i inItemInfo,unsigned int index, int& errNo );

	///取走主位置物品;
	void TakeMainItem( CPlayer* pPlayer );

	///取走辅位置物品，如果辅位置找不到指定的物品，则返回false;
	bool TakeAsstItem( CPlayer* pPlayer, ItemInfo_i inItemInfo,unsigned int Pos );

    ///尝试执行相应操作，根据托盘类型，分别可能为升级、追加或改造
	///执行过程中会调用相应接口收去玩家的旧物品，给玩家新物品
	///由于真正物品从玩家包裹中取走等TryExec时才执行，因此也不需要显式地将没有用掉的辅料之类还给玩家
	///但是需要在本函数中检测各位置物品是否的确在玩家包裹中
	///如果执行条件不满足，则返回失败；
	bool TryPlateExec();

	//设置plate的条件
	void SetConditionID( unsigned int conditionID ) { m_updateID = conditionID; }  

	//升级，追加，改造成功后，取走辅助位置的物品
	void ConsumeAssistItem( CPlayer* pPlayer );

	bool FindAssistItem( unsigned int itemUID );

	unsigned int GetConsumeMoney() { return m_consumeMoney; }//获取消耗的金钱

	void SetConsumeMoney( unsigned int money ) { m_consumeMoney = money; }

	bool OnCalculateLevelUpItemCri( CPlayer* pPlayer ,unsigned int & levelodd, unsigned int & levelsum );
private:

	///清plate条件，拖盘初始化，主料被取走等情况下调用
	void ResetCon()
	{
		m_isConRead = false;
		m_changeCriRd.Reset();
		m_levelUpCriRd.Reset();
		m_consumeMoney = 0;
		for ( int i=0; i<PLATE_ASSTPOS_NUM; ++i )
		{
			if ( i >= ARRAY_SIZE(m_plateCon) )
			{
				D_ERROR( "CCommonPlate::ResetCon, i(%d)越界, m_plateCon\n", i );
				return;
			}
			m_plateCon[i].conType = m_plateCon[i].conNum = 0;//清空条件；
		}
	}

	///读拖盘条件，主料位置新放置物品时调用，没有主料，或主料不可加工时返回false；
	bool ReadCondition();

	///检查托盘条件是否符合；
	bool CheckPlateCondition();

	bool CheckAssitItemIsReadyAdded( const ItemInfo_i& inItemInfo ,unsigned index );

	bool CheckItemValid( ItemInfo_i& itemInfo );

	bool CheckItemBeforeExec( ItemInfo_i& itemInfo );
	
private:
	//当加入附属物品时
	bool OnAddAssistItem( CPlayer* pPlayer, const ItemInfo_i& assitItemInfo, unsigned int index ,int& errNo );

	//当改造物品时加入附属物品
	bool OnAddAssistItemWhenChangeItem(  CPlayer* pPlayer, const ItemInfo_i& assitItemInfo );

	//当改造物品时取走物品
	bool OnTakeAssistItemWhenChangeItem( CPlayer* pPlayer, const ItemInfo_i& assitItemInfo );

	bool ReplaceAssistItemWhenChangeItem( CPlayer* pPlayer,ItemInfo_i& oldassitItemInfo, const ItemInfo_i& newassisItemInfo );

	//当加入主物品时
	bool OnAddMainItem( CPlayer* pPlayer, const ItemInfo_i& mainItemInfo ,int& errNo );

	//当取走副物品时
	bool OnTakeAssisItem( CPlayer* pPlayer, ItemInfo_i& assistItem );

	//通知玩家改造物品的概率
	void NoticePlayerChangeItemCri( CPlayer* pPlayer );

	//通知玩家升级物品的概率改变
	void NoticePlayerLevelUpItemCri( CPlayer* pPlayer, unsigned int levelupodd, unsigned int levelupsum );

	//当加入主要物品后
	void AfterAddMainItem( CPlayer* pPlayer );

	//检查所放的物品是否在材料中
	bool AddItemInPlateCondition( const ItemInfo_i& itemInfo );


private:
	CPlayer*       m_playerOwner;
	EPlateType     m_plateType;//托盘类型；
	ItemInfo_i     m_MainItem;//主位置物品；
	PlateCondition m_plateCon[PLATE_ASSTPOS_NUM];//托盘条件；
	bool           m_isConFulfil;//plate条件是否满足；
	bool           m_isConRead;//是否已读取主料加工条件；
	unsigned int   m_AssistItemCount;//放入辅料的个数
	ItemInfo_i     m_AsstItem[PLATE_ASSTPOS_NUM];//辅料位置物品；

//一些升级，追加，改造的条件
	unsigned int   m_updateID;//升级，改造，追加的编号
	ChangeCriRd    m_changeCriRd;//改造的概率
	LevelUpCriRd   m_levelUpCriRd;//升级的概率

//本次消耗的物品
	unsigned int   m_consumeMoney;

};
