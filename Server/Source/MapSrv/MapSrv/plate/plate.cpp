﻿/**
* @file plate.h
* @brief 升级、追加、改造用托盘定义
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: plate.h
* 摘    要: 升级、追加、改造用托盘定义
* 作    者: dzj
* 完成日期: 2008.07.30
*
*/

#include "plate.h"
#include "../Item/ItemUpdateConfig.h"
#include "../Item/ItemUpdateManager.h"
#include "../Player/Player.h"
#include "../Item/CItemPropManager.h"
#include "../Item/ItemLeavelUpCri.h"


///增加主位置物品，如果新放入物品不可加工，则返回false; 
bool CCommonPlate::AddMainItem(CPlayer* pPlayer, ItemInfo_i inItemInfo , int& errNo )
{
	if ( !pPlayer )
		return false;

	//即使原来的主位置有物品，也没影响，因为此刻还没有真正从玩家包裹中取物品；
	if ( inItemInfo.uiID == m_MainItem.uiID )
	{
		//新放入的物品没有变化；
		return false;
	}
	else
	{
		if ( !CheckItemValid(inItemInfo) )
		{
			errNo = (int)E_ITEM_INVALID;
			return false;
		}

		//检查加入的主物品是否合法
		if ( !OnAddMainItem( pPlayer, inItemInfo, errNo ) )
		{
			/*if ( m_plateType == PLATE_UPGRADE )
				errNo = (int)E_NOT_UPDATE_ITEM;
			else if( m_plateType == PLATE_ADD )
				errNo = (int)E_NOT_ADD_ITEM;
			else if( m_plateType == PLATE_MODIFY )
				errNo = (int)E_NOT_CHANGE_ITEM;
			else
				errNo = (int)E_ITEM_INVALID;*/
			return false;
		}

		//清空前面改造道具所留下来的条件
		ResetCon();

		ItemInfo_i oldMainItem = m_MainItem;//暂存旧主料；
		m_MainItem = inItemInfo;
 
		if ( !ReadCondition() ) //每次放新主料时都读一次条件
		{
			m_MainItem = oldMainItem;//重置为旧的主料，因为新主料不可被加工
			errNo = (int)E_NOT_UPDATE_ITEM;
			return false;
		}
	}
	
	//加入主物品后
	AfterAddMainItem( pPlayer );

	return true;
}

///增加辅位置物品，返回false说明位置不够;
bool CCommonPlate::AddAsstItem( CPlayer* pPlayer, ItemInfo_i inItemInfo,unsigned int index , int& errNo )
{
	if ( !pPlayer )
		return false;

	//道具不合法
	if ( !CheckItemValid(inItemInfo) )
	{
		errNo = (int)E_ITEM_INVALID;
		return false;
	}

	//检测添加的物品是否能够放入托盘
	if ( !OnAddAssistItem( pPlayer, inItemInfo ,index, errNo ) )
		return false;

	return true;
}

///取走主位置物品;
void CCommonPlate::TakeMainItem( CPlayer* pPlayer )
{
	ACE_UNUSED_ARG( pPlayer );
	//重置各方面的条件，仅仅保留，现在托盘的类型
	m_MainItem.uiID = m_MainItem.usItemTypeID = 0;
	m_isConFulfil = m_isConRead = false;
	m_AssistItemCount = 0;
	m_consumeMoney = 0;
	m_changeCriRd.Reset();
	m_levelUpCriRd.Reset();
	StructMemSet(m_AsstItem,0x0,sizeof(ItemInfo_i)*PLATE_ASSTPOS_NUM );

	//因为如果是追加，将主物品拿走后，还是要保留追加配方的编号
	if ( m_plateType != PLATE_ADD )
		m_updateID = 0;
}

///取走辅位置物品，如果辅位置找不到指定的物品，则返回false;
bool CCommonPlate::TakeAsstItem( CPlayer* pPlayer, ItemInfo_i inItemInfo,unsigned int Pos )
{
	ACE_UNUSED_ARG( Pos );

	if ( !pPlayer )
		return false;

	bool isFound = false;
	for ( int i=0; i<PLATE_ASSTPOS_NUM; ++i )
	{
		if ( m_AsstItem[i].uiID == inItemInfo.uiID )
		{
			//从包裹中取出物品的回调
			OnTakeAssisItem( pPlayer,m_AsstItem[i] );
			isFound = true;
			break;
		}
	}

	return isFound;
}

///尝试执行相应操作，根据托盘类型，分别可能为升级、追加或改造
///执行过程中会调用相应接口收去玩家的旧物品，给玩家新物品
///由于真正物品从玩家包裹中取走等TryExec时才执行，因此也不需要显式地将没有用掉的辅料之类还给玩家
///但是需要在本函数中检测各位置物品是否的确在玩家包裹中
///如果执行条件不满足，则返回失败；
bool CCommonPlate::TryPlateExec() 
{ 
	//检查托盘条件；
	if ( !m_playerOwner )
		return false;

	if ( ! CheckPlateCondition() )
	{
		//plate条件不满足；
		return false;
	}

	if ( m_playerOwner->IsUseGoldMoney() )
	{
		//使用金币;
		if ( m_playerOwner->GetTryExecPlateMoney() > m_playerOwner->GetGoldMoney() )
		{
			m_playerOwner->SendSystemChat("金币不足，不能升级道具");
			return false;
		} else {
			//扣除金币
			m_playerOwner->SubGoldMoney( m_playerOwner->GetTryExecPlateMoney() );
		}
	} else {
		//使用银币;
		if ( m_playerOwner->GetTryExecPlateMoney() > m_playerOwner->GetSilverMoney() )
		{
			m_playerOwner->SendSystemChat("银币不足，不能升级道具");
			return false;
		} else {
			//扣除银币
			m_playerOwner->SubSilverMoney( m_playerOwner->GetTryExecPlateMoney() );
		}
	}
	
	//2.因为之前条件已满足，因此如果主料与辅料都没问题，则可以直接找到主料对应的新物品，将新物品加给玩家，同时将主料与辅料收去，???,邓子建检查；
	bool bExecute = false;
	switch( m_plateType )
	{
	case PLATE_UPGRADE:
		{
			bExecute = ItemUpdateManager::ExecuteUpdate( m_playerOwner, &m_MainItem, m_AsstItem, E_LEVELUP ,m_updateID ,NULL); 
		}
		break;
	case PLATE_ADD:
		{
			bExecute = ItemUpdateManager::ExecuteUpdate( m_playerOwner, &m_MainItem, m_AsstItem,E_ADDID, m_updateID ,NULL );
		}
		break;
	case PLATE_MODIFY:
		{
			bExecute = ItemUpdateManager::ExecuteUpdate( m_playerOwner, &m_MainItem, m_AsstItem, E_CHANGE,0 , &m_changeCriRd );
		}
		break;
	default:
		break;
	}

	ConsumeAssistItem( m_playerOwner );//如果执行成功，消耗辅助的道具

	TakeMainItem( m_playerOwner );//执行完成后，相当于取走了物品

	return bExecute; 
};



///读托盘条件，主料位置新放置物品时调用，没有主料，或主料不可加工时返回false
bool CCommonPlate::ReadCondition()
{
	if ( 0 == m_MainItem.uiID )
	{
		//没有放主料;
		m_isConRead = false;
		return false;
	}

	if ( !m_playerOwner )
		return false;

	//升级测试
	switch( m_plateType )
	{
	//升级
	case PLATE_UPGRADE:
		{
			//读取升级所需的材料
			ItemUpdateManager::ReadLevelUpMaterial( m_playerOwner, m_MainItem,m_plateCon, m_updateID );
		}
		break;
	//追加
	case PLATE_ADD:
		{
			ItemUpdateManager::ReadAddIDMaterial( m_playerOwner, m_plateCon ,m_updateID );
		}
		break;
	//改造
	case PLATE_MODIFY:
		{
			ItemUpdateManager::ReadChangeMaterial( m_playerOwner, m_MainItem,m_plateCon );
		}
		break;
	default:
		break;
	}

	m_isConRead = true;
	return m_isConRead;
}

///检查托盘条件是否符合，假定所有的主料以及辅料都没有问题（有这些item，这些item属于该玩家，这些item的类型没有问题）；
bool CCommonPlate::CheckPlateCondition() 
{ 
	if ( 0 == m_MainItem.uiID )
	{
		//没有放主料;
		return false;
	}

	if ( !m_isConRead )
	{
		//不可能错误，只要有合法主料，就应该有对应条件；
		return false;
	}

	//3.并使用辅料检测该条件，一一比对；
	PlateCondition tempCon[PLATE_ASSTPOS_NUM];//临时托盘条件；
	StructMemCpy( tempCon, m_plateCon, sizeof(PlateCondition)*PLATE_ASSTPOS_NUM );

	ItemInfo_i     m_consumeItem[PLATE_ASSTPOS_NUM];
	unsigned       m_consumeIndex;
	m_consumeIndex = 0;
	for ( int asstpos = 0 ; asstpos<PLATE_ASSTPOS_NUM; ++asstpos )
	{
		if ( 0 != m_AsstItem[asstpos].uiID ) 
		{
			//检测刚才放入的道具是否已经变化,如被扔掉等等情况
			if ( !CheckItemBeforeExec( m_AsstItem[asstpos] ) )
				continue;

			//该位置确实有辅料，检查该辅料是否可以为条件作贡献；
			for ( int conpos=0; conpos<PLATE_ASSTPOS_NUM; ++conpos )
			{
				if ( ( tempCon[conpos].conNum > 0 ) 
					&& ( ( m_AsstItem[asstpos].usItemTypeID / 10 )== tempCon[conpos].conType )
					)
				{
					ItemInfo_i& cosumeItem = m_consumeItem[m_consumeIndex];
					if ( m_AsstItem[asstpos].ucCount > (char)tempCon[conpos].conNum )
					{
						cosumeItem = m_AsstItem[asstpos];
						cosumeItem.ucCount = (char)tempCon[conpos].conNum;
						tempCon[conpos].conNum = 0;
					}
					else
					{
						cosumeItem = m_AsstItem[asstpos];
						cosumeItem.ucCount = 0;
						tempCon[conpos].conNum -= m_AsstItem[asstpos].ucCount;
					}
					m_consumeIndex++;
					break;//继续检查下一个辅料位置；
				}
			}
		}
	}

	m_isConFulfil = false;//先假设条件符合；
	if ( m_consumeIndex != 0 )
	{
		m_isConFulfil = true;//先假设条件符合；
		for ( int conpos=0; conpos<PLATE_ASSTPOS_NUM; ++conpos )
		{
			if ( tempCon[conpos].conNum > 0 ) 
			{
				m_isConFulfil = false; //只要临时条件中有一个还没有满足，则整个条件不满足；
				break;
			}
		}
	}
	return m_isConFulfil;
}

bool CCommonPlate::FindAssistItem(unsigned int itemUID )
{
	//检测该物品是否已经被放入过，是否合法 
	for ( unsigned i = 0 ; i < PLATE_ASSTPOS_NUM ; i++ )
	{
		const ItemInfo_i& tmpInfo =  m_AsstItem[i];
		if ( tmpInfo.uiID == itemUID )
			return true;
	}
	return false;
}

void CCommonPlate::ConsumeAssistItem( CPlayer* pPlayer )
{
	if ( !pPlayer )
		return;

	//1 处理此次的改造，升级，追加，改造的必须的道具条件
	for( unsigned int i = 0; i< PLATE_ASSTPOS_NUM; i++ )
	{
		PlateCondition& condition = m_plateCon[i];
		if ( condition.conType != 0  && condition.conNum != 0 )
		{
			pPlayer->OnDropItemByConditionID( condition.conType , condition.conNum );
		}
	}
	
	//2 特殊处理情况，如升级的提高概率的宝石，保不变化的宝石， 改造的提高概率的
	switch( m_plateType )
	{
	//改造道具，处理提高改造道具的装备
	case PLATE_MODIFY:
		{
			//如果放入的不光是改造的必需条件,还有一些增加改造几率的附属装备的话,则需要将这些装备扔掉
			for ( unsigned int i = 0  ; i< PLATE_ASSTPOS_NUM ; i++ )
			{
				ItemInfo_i& tmpItem = m_AsstItem[i];
				if ( tmpItem.uiID == 0 )
					continue;

				//如果不是必须条件的道具，则是增加装备的道具,需要将它们扔掉
				if( AddItemInPlateCondition( tmpItem ) == false )
				{
					pPlayer->OnDropItem( tmpItem.uiID );
				}
			}
			
		}
		break;
	case PLATE_UPGRADE:
		{
			//如果不光要处理升级条件,还要处理辅助升级的条件
			for ( unsigned int i = 0 ; i< PLATE_ASSTPOS_NUM ; i++ )
			{
				ItemInfo_i& tmpItem = m_AsstItem[i];
				if ( tmpItem.uiID == 0 )
					continue;

				//如果不是必须条件的道具，则是增加概率的的道具,需要将它们扔掉
				if( AddItemInPlateCondition( tmpItem ) == false )
				{
					int  itemIndex = -1;
					bool isEquip   = false;
					ItemInfo_i* pItemInfo = pPlayer->GetItemInfoByItemUID( tmpItem.uiID , itemIndex, isEquip );
					if ( pItemInfo )
					{
						if ( pItemInfo->ucCount == 1 )
						{
							pPlayer->OnDropItem( tmpItem.uiID );
						}
						else
						{
							pItemInfo->ucCount--;
							if ( !isEquip )
								pPlayer->NoticePkgItemChange( itemIndex );
							else
								pPlayer->NoticeEquipItemChange( itemIndex );
						}
					}
				}
			}
		}
		break;
	default:
		break;
	}
}

void  CCommonPlate::AfterAddMainItem( CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		return;
	}
	//如果是改造
	if ( m_plateType == PLATE_MODIFY  )
	{
		CBaseItem* pMainItem = pPlayer->GetItemDelegate().GetItemByItemUID( m_MainItem.uiID );
		if ( pMainItem )
		{
			unsigned int massLevel = (unsigned int)pMainItem->GetItemMassLevel();
			int k1 = massLevel;

			unsigned int itemLevel = ( unsigned int )pMainItem->GetItemPlayerLevel();
			int k2 = (itemLevel - 1)/30 +1;

			unsigned changeCirID = k1 *10 + k2;

			ChangeCri* pCri = ItemChangeCirManager::GetChangeCri( changeCirID );
			if ( pCri )
			{
				m_changeCriRd.level = itemLevel;
				m_changeCriRd.masslevel = massLevel;
				m_changeCriRd.odd = pCri->odd;
				m_changeCriRd.sumodd = pCri->Sumodd;
			}
		}

		//是否已经放入了辅助道具
		if ( m_AssistItemCount > 0 )
		{
			for ( unsigned i = 0, j = 0 ; i < PLATE_ASSTPOS_NUM && j < m_AssistItemCount; i++ ,j++)
			{
				ItemInfo_i& assitItem = m_AsstItem[i];
				if ( !AddItemInPlateCondition(assitItem) )//材料不增加概率，只有道具才增加改造概率
					OnAddAssistItemWhenChangeItem( pPlayer, assitItem );
			}		
		}
		NoticePlayerChangeItemCri( pPlayer );
	}
	else if( m_plateType == PLATE_UPGRADE )//道具升级的概率
	{
		LevelUpCri* pCri = CItemLeavelUpCriManager::GetLevelUpCri( m_updateID );
		if ( pCri )
		{
			m_levelUpCriRd.levelUpOdd = pCri->odd;
			m_levelUpCriRd.levelUpSumOdd = pCri->Sumodd;
			m_levelUpCriRd.levelUpCri = pCri->cri;
			m_levelUpCriRd.updateFailstart = pCri->updateFailstart;
			m_levelUpCriRd.updateFailEnd = pCri->updateFailEnd;
		}

		//放入主物品后,计算升级的概率
		unsigned int levelUpOdd = 0 , levelUpSum = 0;
		OnCalculateLevelUpItemCri( pPlayer, levelUpOdd, levelUpSum );
	}
}


bool CCommonPlate::OnAddMainItem( CPlayer* pPlayer, const ItemInfo_i& mainItemInfo ,int& errNo )
{
	if ( NULL == pPlayer )
	{
		return false;
	}
	//只要放入了材料，就不能被替换了
	if ( m_MainItem.uiID != mainItemInfo.uiID && m_MainItem.uiID != 0 )
	{
		if ( m_AssistItemCount > 0 )
		{
			errNo = (int)E_TAKE_ITEM_BEFORE_ADD;
			return false;
		}
	}

	//是不能升级，追加，改造的物品
	if ( !pPlayer->IsCanUpdateItem( mainItemInfo.uiID ) )
	{
		errNo = (int)E_ITEM_INVALID;
		return false;
	}

	switch( m_plateType )
	{
	//用来比较放入的物品是否是图纸所要求的道具
	case PLATE_ADD:
		{
			CBaseItem* pBuleItem = pPlayer->GetItemDelegate().GetItemByItemUID( m_updateID );
			if ( !pBuleItem )
			{
				errNo = (int)E_ITEM_INVALID;
				return false;
			}

			CBaseItem*  pMainItem = pPlayer->GetItemDelegate().GetItemByItemUID( mainItemInfo.uiID );
			if ( !pMainItem )
			{
				errNo = (int)E_ITEM_INVALID;
				return false;
			}

			if ( pMainItem->GetItemTypeID() == E_TYPE_FASHION )
			{
				errNo = (int)E_NOT_ADD_ITEM;
				return false;
			}

			//检测放入物品的等级是否大于图纸的等级
			if ( pMainItem->GetItemPlayerLevel() >= pBuleItem->GetItemPlayerLevel() )
			{
				//检测放入的装备的种族是否符合图纸的要求
				if ( ( pBuleItem->GetItemPublicProp()->mCanUseRace & pMainItem->GetItemPublicProp()->mCanUseRace ) != 0 )
				{
					//检测放入装备的职业是否符合图纸的要求
					if ( ( pBuleItem->GetItemPublicProp()->mCanUseClass & pMainItem->GetItemPublicProp()->mCanUseClass ) != 0 )
					{
						CBulePrintItem* pBulePrintItem = dynamic_cast<CBulePrintItem *>( pBuleItem );
						if ( !pBulePrintItem )
							return false;

						CBulePrint* bulePrintProp = pBulePrintItem->GetBulePrintProperty();
						if( !bulePrintProp )
							return false;

						int gearArmIndex = ( 1 << (int)pMainItem->GetGearArmType() );
						int validGearArm = bulePrintProp->GetGearArmType();
						if ( ( validGearArm & gearArmIndex )   == 0  )//追加的位置不符合图纸的要求
						{
							errNo = (int)GCAddSthToPlateRst::E_NOT_ADD_ITEM_EQUIPINDEX_LOST;
							return false;
						}

						return true;
					}
					else
					{
						errNo = (int)GCAddSthToPlateRst::E_NOT_ADD_ITEM_CLASS_LOST;
						return false;
					}
				}
				else
				{
					errNo = ( int )GCAddSthToPlateRst::E_NOT_ADD_ITEM_RACE_LOST;
					return false;
				}

			}
			else
			{
				errNo = (int)GCAddSthToPlateRst::E_NOT_ADD_ITEM_LEVEL_LOST;
				return false;
			}
		}
		break;
	case PLATE_UPGRADE:
		{
			CBaseItem* pMainItem = pPlayer->GetItemDelegate().GetItemByItemUID( mainItemInfo.uiID );
			if ( !pMainItem )
			{
				errNo = (int)E_ITEM_INVALID;
				return false;
			}

			if ( pMainItem->GetItemTypeID() == E_TYPE_FASHION )
			{
				errNo = (int)E_NOT_UPDATE_ITEM;
				return false;
			}

			//如果在上马状态
			if ( pPlayer->GetSelfStateManager().IsOnRideState() )
			{
				//升级的物品不能是正在坐骑的物品
				if( pPlayer->GetSelfStateManager().GetRideState().GetRideItemID() == pMainItem->GetItemUID() )
				{
					errNo = (int)GCAddSthToPlateRst::E_NOT_UPDATE_ITEM_USEING_RIDEITEM;
					return false;
				}
			} 

			if ( pMainItem->GetGearArmType() == E_DRIVE )
			{
				if ( pMainItem->GetItemID()/100000 % 10  == 1 )//如果双人骑乘道具，不能放入升级托盘
				{
					errNo = (int)GCAddSthToPlateRst::E_NOT_UPDATE_ITEM_DOUBLE_RIDEITEM;
					return false;
				}
			} 
 
			if ( GET_ITEM_LEVELUP( mainItemInfo.ucLevelUp ) >= pMainItem->GetLevelUpLimit() )
			{
				errNo = (int)GCAddSthToPlateRst::E_NOT_UPDATE_ITEM_REACH_MAXLEVEL;
				return false;
			}

			if ( m_MainItem.uiID != mainItemInfo.uiID && m_MainItem.uiID != 0 )
			{
				if ( !pMainItem )
				{
					errNo = (int)E_ITEM_INVALID;
					return false;
				}

				if ( pMainItem->GetItemUseType() != E_EQUIP)
				{
					errNo = (int)GCAddSthToPlateRst::E_NOT_UPDATE_ITEM_NOT_EQUIPITEM;
					return false;
				}

				//if ( pMainItem->GetItemMassLevel() < E_ORANGE )
				//{
				//	errNo = (int)GCAddSthToPlateRst::E_NOT_UPDATE_ITEM_MASS_LOST;
				//	return false;
				//}

				if ( m_AssistItemCount  > 0 )
				{
					//升级替换，只能替换原来等级和材质和升级等级一致的
					if ( pMainItem->GetItemPlayerLevel() == pMainItem->GetItemPlayerLevel()
						&& pMainItem->GetItemMassLevel() == pMainItem->GetItemMassLevel()
						&& GET_ITEM_LEVELUP( mainItemInfo.ucLevelUp ) == GET_ITEM_LEVELUP( m_MainItem.ucLevelUp )  )
					{
						return true;
					}
					else
					{
						errNo = E_UPDATE_ITEM_NOT_REPLACE;
						return false;
					}
				}
			}
			else
			{
				CBaseItem* pItem = pPlayer->GetItemDelegate().GetItemByItemUID( mainItemInfo.uiID );
				if ( !pItem )
					return false;

				if ( pItem->GetItemUseType() !=  E_EQUIP )
				{
					errNo = (int)GCAddSthToPlateRst::E_NOT_UPDATE_ITEM_NOT_EQUIPITEM;
					return false;
				}

				//if ( pItem->GetItemMassLevel() < E_ORANGE )
				//{
				//	errNo = (int)GCAddSthToPlateRst::E_NOT_UPDATE_ITEM_MASS_LOST;
				//	return false;
				//}
			}
		}
		break;
	case PLATE_MODIFY:
		{
			CBaseItem* pItem = pPlayer->GetItemDelegate().GetItemByItemUID( mainItemInfo.uiID );
			if ( !pItem )
			{
				errNo = (int)E_ITEM_INVALID;
				return false;
			}

			if ( pItem->GetItemTypeID() == E_TYPE_FASHION )
			{
				errNo = (int)E_NOT_CHANGE_ITEM;
				return false;
			}

			/*去掉物品品质紫色相关代码(zsw/2010.8.13)
			if ( pItem->GetItemMassLevel() >= E_PURPLE )
			{
				errNo = (int)E_NOT_CHANGE_ITEM;
				return false;
			}*/

			if ( pItem->GetItemUseType() !=  E_EQUIP )
			{
				errNo = (int)GCAddSthToPlateRst::E_NOT_CHANGE_ITEM_NOT_EQUIPITEM ;
				return false;
			}

			//获取新道具的共有属性,如果找不到，则证明该道具不能被改造了
			unsigned int newItemTypeID = mainItemInfo.usItemTypeID + 10;
			CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( newItemTypeID );
			if ( !pPublicProp )
			{
				errNo = (int)E_NOT_CHANGE_ITEM;
				return false;
			}
		}
		break;
	default:
		break;
	}
	return true;
}

bool CCommonPlate::OnAddAssistItem(CPlayer* pPlayer, const ItemInfo_i& assitItemInfo, unsigned int index ,int& errNo )
{
	if ( !pPlayer )
		return false;

	if ( m_MainItem.uiID == 0 )
	{
		errNo = (int)E_ITEM_INVALID;  
		return false;
	}

	if ( index >= PLATE_ASSTPOS_NUM )
	{
		errNo = (int)E_ITEM_INVALID;  
		return false;
	}

	//如果道具是保护状态,不允许放入托盘中
	if ( pPlayer->GetProtectItemManager().FindItemIsProtected( assitItemInfo.uiID ) != NULL )
	{
		if ( m_plateType == PLATE_MODIFY )
			errNo = (int)E_NOT_CHANGE_CONDITION;
		else if( m_plateType == PLATE_UPGRADE )
			errNo = (int)E_NOT_UPDATE_CONDITION;
		else if( m_plateType == PLATE_ADD )
			errNo = (int)E_NOT_ADD_CONDITION;
		else
			errNo = E_ITEM_INVALID;

		return false;
	}

	switch( m_plateType )
	{
	case PLATE_MODIFY://如果是改造
		{
			//如果不属于添加的条件
			bool   isPlateCondition =  AddItemInPlateCondition( assitItemInfo );
			if ( ! isPlateCondition )//如果不属于托盘条件
			{
				//如果把装备放入改造界面
				CBaseItem* pAssistItem = pPlayer->GetItemDelegate().GetItemByItemUID( assitItemInfo.uiID );
				if ( !pAssistItem )
				{
					errNo = (int)E_ITEM_INVALID;  
					return false;
				}

				if ( pAssistItem->GetItemUseType() == E_AXIOLOGY  )
				{
					errNo = (int)E_NOT_CHANGE_CONDITION;
					return false;
				}
			}

			//如果原来这个位置已经放了道具
			if ( m_AsstItem[index].uiID != 0 )
			{
				//如果不属于托盘的条件，则是增加概率的装备.如果满足条件，则是改造所需要的宝石
				if ( !isPlateCondition )
				{
					//如果使用新的道具，替换原来的物品，并相应的改变道具
					if( !ReplaceAssistItemWhenChangeItem( pPlayer, m_AsstItem[index], assitItemInfo ) )
					{
						errNo = (int)E_NOT_CHANGE_CONDITION;
						return false;
					}

					//通知概率的改变
					NoticePlayerChangeItemCri( pPlayer );
				}
				else
					CheckAssitItemIsReadyAdded( assitItemInfo, index  );
			}
			else
			{
				//检查是否为能放入的物品,如果不能，则判断是否能提高改造概率的东西
				if( !isPlateCondition )
				{
					//玩家增加了改造的概率,如果不增加，则证明道具不符合要求
					if( OnAddAssistItemWhenChangeItem( pPlayer, assitItemInfo ) )
					{
						//通知客户端概率改变
						NoticePlayerChangeItemCri( pPlayer );
					}
					else
					{
						errNo = (int)E_NOT_CHANGE_CONDITION;
						return false;
					}
				}
				CheckAssitItemIsReadyAdded( assitItemInfo, index );
			}
		}
		break;
	case PLATE_UPGRADE://如果是升级,放入的必须是价值物,并且只有放了主物品才能放其他材料
		{
			CBaseItem* pItem =  pPlayer->GetItemDelegate().GetItemByItemUID( assitItemInfo.uiID );
			if ( !pItem )
				return false;

			//如果不属于升级材料,那么验证是否为追加概率的物品或宝石
			if( !AddItemInPlateCondition(assitItemInfo ) )
			{
				if ( index == 2 )//追加概率的宝石只能放入第二位
				{
					ITEM_TYPE itemType = pItem->GetItemUseType();
					if ( itemType == E_AXIOLOGY )//必须是增加概率的宝石
					{
						CAxiologyItem* pAxiologyItem =  dynamic_cast<CAxiologyItem *>( pItem );
						if( pAxiologyItem && pAxiologyItem->GetAxiologyItemProperty()->type != AxiologyProperty::E_ADDLEVELCRI )
						{
							errNo = (int)E_NOT_UPDATE_CONDITION;
							return false;
						}
					}
					else if( itemType == E_EQUIP )//必须是可装备的物品,且等级必须小于10级
					{
						if( GET_ITEM_LEVELUP( assitItemInfo.ucLevelUp ) < 10 || GET_ITEM_LEVELUP( m_MainItem.ucLevelUp )< 10 )
						{
							errNo = (int)E_NOT_UPDATE_CONDITION;
							return false;
						}
					}
				}
				else
				{
					errNo = (int)E_NOT_UPDATE_CONDITION;
					return false;
				}

				if ( m_AssistItemCount > 0 )
				{
					//检验追加概率的物品的数量是否超过限制
					unsigned int addcriitemnum = 0;
					for ( unsigned int i = 0 ; i< m_AssistItemCount && i < PLATE_ASSTPOS_NUM; i++ )
					{
						ItemInfo_i& itemInfo = m_AsstItem[i];
						if ( !AddItemInPlateCondition( itemInfo) )//检测是否是所需材料,不是则是追加概率的物品
							addcriitemnum++;
					}

					if ( m_AsstItem[index].uiID == 0 )//属于新添加的物品,判断所放的追加物品的个数是否合法
					{
						if ( GET_ITEM_LEVELUP( m_MainItem.ucLevelUp ) > 15 && addcriitemnum >= 2 )
						{
							errNo = (int)E_NOT_UPDATE_CONDITION;
							return false;
						}
						else if( GET_ITEM_LEVELUP( m_MainItem.ucLevelUp >= 10 ) && addcriitemnum >= 1 )
						{
							errNo = (int)E_NOT_UPDATE_CONDITION; 
							return false;
						}
					}
				}
				
				if( CheckAssitItemIsReadyAdded( assitItemInfo, index ) )
				{
					unsigned int levelupodd  =  0, levelupsum = 0;
					OnCalculateLevelUpItemCri( pPlayer, levelupodd, levelupsum );
					return true;
				}
				return false;
			}
			else
			{
				//检验材料是否重复放入过
				if( FindAssistItem(  assitItemInfo.uiID ) )
					return false;

				return CheckAssitItemIsReadyAdded( assitItemInfo , index );
			}
		}
		break;
	case PLATE_ADD://如果是追加，那么放入的物品也必须是价值物，同时必需是满足追加条件的道具
		{	
			//检测是否在追加的条件中
			if ( !AddItemInPlateCondition( assitItemInfo ) )
			{
				errNo = (int)E_NOT_ADD_CONDITION;
				return false;
			}

			return CheckAssitItemIsReadyAdded( assitItemInfo, index );
		}
		break;
	default:
		break;
	}

	return true;
}

bool CCommonPlate::CheckItemValid(ItemInfo_i& inItemInfo )
{
	if ( !m_playerOwner )
		return false;

	int itemIndex = -1;
	bool isEquip  = false;
	//该道具是否存在
	ItemInfo_i* findItem = m_playerOwner->GetItemInfoByItemUID( inItemInfo.uiID, itemIndex, isEquip );
	if ( !findItem )
		return false;

	inItemInfo = *findItem;

	return true;
}

bool CCommonPlate::CheckItemBeforeExec( ItemInfo_i& itemInfo )
{
	if ( !m_playerOwner )
		return false;

	int itemIndex = -1;
	bool isEquip  = false;
	//该道具是否存在
	ItemInfo_i* findItem = m_playerOwner->GetItemInfoByItemUID( itemInfo.uiID, itemIndex, isEquip );
	if ( !findItem )
		return false;

	CBaseItem* pItem = m_playerOwner->GetItemDelegate().GetItemByItemUID( itemInfo.uiID );
	if ( !pItem )
		return false;

	itemInfo = *findItem;

	//如果是宝石
	if ( pItem->GetItemTypeID() == E_TYPE_AXIOLOGY )
	{
		//新的材料机制
		int itemCount  = m_playerOwner->OnGetItemCountByConditionID( itemInfo.usItemTypeID/10 );
		
		//unsigned char 最大个数为127
		if ( itemCount >= 128 )
			itemCount = 127;

		itemInfo.ucCount = itemCount;
	}

	return true;
}

bool CCommonPlate::CheckAssitItemIsReadyAdded( const ItemInfo_i& assitItemInfo ,unsigned index )
{
	if ( index >= PLATE_ASSTPOS_NUM )
		return false;

	switch( m_plateType )
	{
	//升级
	case PLATE_UPGRADE:
	case PLATE_MODIFY:
	case PLATE_ADD:
		{
			if ( m_AsstItem[index].uiID == 0 )
			{
				m_AssistItemCount++;
			}

			m_AsstItem[index] = assitItemInfo;
		}
		break;
	default:
		break;
	};
	return true;
}

bool CCommonPlate::OnTakeAssisItem(CPlayer *pPlayer, MUX_PROTO::ItemInfo_i &assistItem)
{
	if ( !pPlayer)
		return false;

	//如果是改造,则可能改变改造的概率
	if( m_plateType == PLATE_MODIFY )
	{
		//检测这个道具是否在改造的条件中,如果是条件，则不需要通知客户端概率改变
		if ( !AddItemInPlateCondition(assistItem) )
		{
			OnTakeAssistItemWhenChangeItem( pPlayer, assistItem );

			NoticePlayerChangeItemCri( pPlayer );
		}
		assistItem.uiID = assistItem.usItemTypeID = 0;
		m_AssistItemCount--;
	}
	else if( m_plateType == PLATE_UPGRADE )
	{
		bool isCondition = AddItemInPlateCondition( assistItem );//检测是否为升级材料
		//将原来的这个道具位变空
		assistItem.uiID = assistItem.usItemTypeID = 0;
		m_AssistItemCount--;

		if ( !isCondition )//如果不是必须材料,则是增加升级概率的物品
		{
			//重新计算升级概率
			unsigned int levelUpOdd = 0 , levelUpSum = 0;
			OnCalculateLevelUpItemCri( pPlayer,levelUpOdd,levelUpSum );
		}
	}
	else if( m_plateType == PLATE_ADD )
	{
		assistItem.uiID = assistItem.usItemTypeID = 0;
		m_AssistItemCount -- ;
	}

	return true;
}


bool CCommonPlate::AddItemInPlateCondition(const ItemInfo_i& itemInfo )
{
	unsigned int i = 0;
	for ( i = 0 ; i< PLATE_ASSTPOS_NUM; i++ )
	{
		PlateCondition& pCondition = m_plateCon[i]; 
		//新的识别机制
		if ( pCondition.conType == ( itemInfo.usItemTypeID / 10 ) )
			break;
	}

	return i != PLATE_ASSTPOS_NUM;
}

void CCommonPlate::NoticePlayerChangeItemCri(CPlayer* pPlayer )
{
	if ( pPlayer )
	{
		MGChangeItemCri changeCriMsg;

		if ( m_changeCriRd.sumodd > 0)
		{
			changeCriMsg.changeCri = (unsigned)( (float)m_changeCriRd.odd /m_changeCriRd.sumodd  * 100 );
			if ( changeCriMsg.changeCri > 100 )
				changeCriMsg.changeCri =  100;
		}
		else
			changeCriMsg.changeCri = 0;

		pPlayer->SendPkg<MGChangeItemCri>(&changeCriMsg);
	}
}

void CCommonPlate::NoticePlayerLevelUpItemCri(CPlayer* pPlayer, unsigned int levelupodd, unsigned int levelupsum )
{
	if ( pPlayer )
	{
		MGChangeItemCri levelupMsg;

		if ( levelupsum > 0 )
		{
			levelupMsg.changeCri = (unsigned)( (float)levelupodd / levelupsum *100 );
			if ( levelupMsg.changeCri > 100 )
				levelupMsg.changeCri = 100;
		}
		else
			levelupMsg.changeCri = 0;

		pPlayer->SendPkg<MGChangeItemCri>(&levelupMsg);

		D_DEBUG("%s 升级物品的概率为 %d \n", pPlayer->GetNickName(), levelupMsg.changeCri );
	}
}


bool CCommonPlate::OnAddAssistItemWhenChangeItem( CPlayer* pPlayer, const ItemInfo_i& assitItemInfo  )
{
	if ( !pPlayer )
		return false;

	CBaseItem* pItemAssist = pPlayer->GetItemDelegate().GetItemByItemUID( assitItemInfo.uiID );
	if ( pItemAssist )
	{
		//获取放入托盘的物品的品质
		unsigned masslevel = (unsigned int)pItemAssist->GetItemMassLevel();

		//只有当品质和等级都大时才进行加概率,依据李磊要求修改放入的等级
		if ( pItemAssist->GetItemPlayerLevel() >= m_changeCriRd.level &&
			 masslevel >= m_changeCriRd.masslevel )
		{
			ItemCri* pItemCri= ItemCriManager::GetItemCri( masslevel );
			if ( pItemCri )
			{
				m_changeCriRd.odd += pItemCri->odd;
				return true;
			}
		}
	}
	return false;
}

bool CCommonPlate::OnCalculateLevelUpItemCri(CPlayer* pPlayer ,unsigned int & levelodd, unsigned int & levelsum )
{
	if ( !pPlayer )
		return false;
 
	levelodd    = m_levelUpCriRd.levelUpOdd;
	levelsum    = m_levelUpCriRd.levelUpSumOdd;

	if ( m_plateType == PLATE_UPGRADE )
	{
		//如果已经放了辅助道具
		if ( m_AssistItemCount > 0 )
		{
			unsigned int useItemCount = 0 ;
			for ( unsigned i = 0 ; i < PLATE_ASSTPOS_NUM && useItemCount < m_AssistItemCount ; i++ )
			{
				const ItemInfo_i& assitItem = m_AsstItem[i];
				if ( assitItem.usItemTypeID != 0 )
				{
					if ( !AddItemInPlateCondition(assitItem) )//材料不增加概率，只有道具才增加改造概率
					{
						CBaseItem* pItemAssist = pPlayer->GetItemDelegate().GetItemByItemUID( assitItem.uiID );
						if ( pItemAssist )
						{
							if ( pItemAssist->GetItemUseType() == E_EQUIP )//如果是装备
							{
								unsigned int addbasecri     =  3 + ( GET_ITEM_LEVELUP( assitItem.ucLevelUp ) - 10 ) * 3 ;
								levelodd  +=  m_levelUpCriRd.levelUpSumOdd * addbasecri/100;
							}
							else if( pItemAssist->GetItemUseType() == E_AXIOLOGY  )//如果是宝石
							{
								CAxiologyItem* pAxiologyItem = ( CAxiologyItem * )pItemAssist;
								//如果是增加成功几率的宝石
								if ( pAxiologyItem )
								{
									const AxiologyProperty* pProp = pAxiologyItem->GetAxiologyItemProperty();
									if ( pProp && pProp->type == AxiologyProperty::E_ADDLEVELCRI )
										levelodd = (unsigned int)( m_levelUpCriRd.levelUpOdd  * ( 100 + pProp->value  * m_levelUpCriRd.levelUpCri )/100 );
								}
							}
						}
					}

					useItemCount ++ ;
				}
			}		
		}
		//通知改变客户端的升级概率
		NoticePlayerLevelUpItemCri( pPlayer, levelodd, levelsum  );
	}

	return true;
}


bool CCommonPlate::OnTakeAssistItemWhenChangeItem(CPlayer* pPlayer, const ItemInfo_i& assitItemInfo )
{
	if ( !pPlayer )
		return false;

	CBaseItem* pItemAssist = pPlayer->GetItemDelegate().GetItemByItemUID( assitItemInfo.uiID );
	if ( pItemAssist )
	{
		//获取放入托盘的物品的品质
		unsigned masslevel = (unsigned int)pItemAssist->GetItemMassLevel();
		ItemCri* pItemCri= ItemCriManager::GetItemCri( masslevel );
		if ( pItemCri )
		{
			m_changeCriRd.odd -= pItemCri->odd;
			return true;
		}
	}
	return false;
}

bool CCommonPlate::ReplaceAssistItemWhenChangeItem(CPlayer *pPlayer, MUX_PROTO::ItemInfo_i &oldassitItemInfo, const MUX_PROTO::ItemInfo_i &newassisItemInfo )
{
	if ( !pPlayer )
		return false;

	//增加现在的物品
	if(  OnAddAssistItemWhenChangeItem( pPlayer, newassisItemInfo ) )
	{
		//取走原来的物品
		OnTakeAssistItemWhenChangeItem( pPlayer, oldassitItemInfo );

		//替换原来位置的索引
		oldassitItemInfo = newassisItemInfo;

		return true;
	}

	return false;
}

