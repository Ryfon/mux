﻿#ifndef CAST_MANAGER_H_
#define CAST_MANAGER_H_

#include "Utility.h"

class CPlayer;
class CastManager
{
public:
	CastManager():m_skillID(0),m_time(0),m_pPlayer(NULL),m_startTime(0){}
	~CastManager(){}
	void OnStart( int skillID, int time );
	void OnStop();
	void AttachPlayer(CPlayer * pPlayer){ m_pPlayer = pPlayer; }
	bool IsInCast(){ return m_skillID != 0; }
	int  GetCastSkillID(){ return m_skillID; }
	int  GetCastTime(){ return m_time; }
	unsigned int GetCastStartTime(){ return m_startTime; }
private:
	int		m_skillID;	//读条涉及的技能id
	int		m_time;		//读条所需时间（单位：秒）
	CPlayer*m_pPlayer;	
	unsigned int m_startTime;	//开始读条的时间
};

#endif
