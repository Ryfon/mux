﻿#include "RollItem.h"
#include "RollItemManager.h"
#include "CSpecialItemProp.h"
#include "../Player/Player.h"

void RollItem::TriggerRoll( const std::vector<CPlayer *>& playerVec )
{
	TRY_BEGIN;

	
	if ( !mpItemPkg )
		return;

	mpItemPkg->IncRollCount();

	mpItemPkg->LockItem( mpItemEle.GetItemTypeID() );

	rollPeopleCount = 0;
	//通知每个人,Roll点开始
	for ( std::vector<CPlayer *>::const_iterator lter = playerVec.begin();
		lter!= playerVec.end();
		lter++
		)
	{
		CPlayer* player = *lter;
		if ( player  
			 && player->GetSelfStateManager().IsGroupState()//是否在组队状态
			 && (unsigned int)(player->GetSelfStateManager().GetGroupState().GetGroupTeamID())  == mOwnerGroupID //组队ID相等
			 && !player->GetSelfStateManager().IsInSafeArea() )//并且在非安全区
		{
			//询问玩家是否Roll点
			MGReqAgreeRollItem srvmsg;
			srvmsg.rollID = mrollID;
			srvmsg.rollItemID = mpItemEle.GetItemTypeID();
			player->SendPkg<MGReqAgreeRollItem>(&srvmsg);

			mRollPlayerIDVec.push_back( player->GetFullID() );
			rollPeopleCount++;
		}
	}
	mBeginRollTime =  ACE_OS::gettimeofday();

	TRY_END;
}

void RollItem::RandRollItem()
{
	TRY_BEGIN;


	if ( !mpItemPkg )
		return;

	//如果该道具没有参与Roll点
	mpItemPkg->DecRollCount();

	//锁定包裹中的物品
	mpItemPkg->UnLockItem( mpItemEle.GetItemTypeID() );

	//Roll点状态结束
	mrollOver = true;

	if ( !mIsHaveWinPlayer )
	{
		D_DEBUG("玩家都没有参与本次Roll点,不进行本次Roll点的分配操作\n");
		CItemPkgEle *pPkgEle = mpItemPkg->GetEleByItemTypeId(mpItemEle.GetItemTypeID());
		if(NULL != pPkgEle)
			pPkgEle->UpdateRollState(CItemPkgEle::ROLL_NO_RESULT);

		return;
	}

	//roll有结果
	CItemPkgEle *pPkgEle = mpItemPkg->GetEleByItemTypeId(mpItemEle.GetItemTypeID());
	if(NULL != pPkgEle)
		pPkgEle->UpdateRollState(CItemPkgEle::ROLL_HAVE_RESULT);

	CPlayer* pWinPlayer = NULL;
	CGateSrv*pSrv = CManG_MProc::FindProc( mWinPlayerID.wGID );
	if ( pSrv )
		pWinPlayer = pSrv->FindPlayer( mWinPlayerID.dwPID );

	if ( pWinPlayer )//如果这个Roll点的人还在服务器,找到这个玩家,然后把道具给玩家 
	{
		if ( !mpItemPkg )
		{
			D_ERROR("Roll点物品时，Roll物品所属的道具包消失\n");
			return;
		}

		//构造本次roll点的结果 
		MGRollItemResult rollResultMsg;
		rollResultMsg.rollID = mrollID;
		rollResultMsg.rollItemID = mpItemEle.GetItemTypeID();
		ACE_OS::strncpy( rollResultMsg.rollPlayerName ,mWinPlayerName ,MAX_NAME_SIZE );
		
		//通知其他所有玩家谁roll到了物品
		for ( std::vector<PlayerID>::iterator lter = mRollPlayerIDVec.begin(); 
			lter != mRollPlayerIDVec.end();
			++lter )
		{
			const PlayerID& tmpID = *lter;
			CGateSrv*pSrv = CManG_MProc::FindProc( tmpID.wGID );
			if ( pSrv )
			{
				CPlayer* pPlayer = pSrv->FindPlayer( tmpID.dwPID );
				if ( pPlayer )
				{
					pPlayer->SendPkg<MGRollItemResult>( &rollResultMsg );
				}
			}
		}

		//将道具放入玩家背包
		mpItemPkg->OnRollItemToPlayer( pWinPlayer,mpItemEle, mrollID );

#ifdef _DEBUG
		D_DEBUG("%s玩家随到了物品\n",pWinPlayer->GetNickName() );
#endif
	}
	else
	{
		D_DEBUG("Roll点最大的玩家%s已经不在服务器了\n",mWinPlayerName );

		MGRollItemResult rollResultMsg;
		rollResultMsg.rollID = mrollID;
		rollResultMsg.rollItemID = mpItemEle.GetItemTypeID();
		ACE_OS::strncpy( rollResultMsg.rollPlayerName,mWinPlayerName ,MAX_NAME_SIZE );

		//通知其他所有玩家谁roll到了物品
		for ( std::vector<PlayerID>::iterator lter = mRollPlayerIDVec.begin(); 
			lter != mRollPlayerIDVec.end();
			++lter )
		{
			const PlayerID& tmpID = *lter;
			CGateSrv*pSrv = CManG_MProc::FindProc( tmpID.wGID );
			if ( pSrv )
			{
				CPlayer* pPlayer = pSrv->FindPlayer( tmpID.dwPID );
				if ( pPlayer )
				{
					pPlayer->SendPkg<MGRollItemResult>( &rollResultMsg );
				}
			}
		}
	}
	
	TRY_END;
}

//当接到玩家反馈的Roll物品请求时
bool RollItem::RecvRollReq( CPlayer* player, bool isAgree )
{
	if ( !player )
		return false;

	TRY_BEGIN;


	if( isAgree ) //如果同意RolL点
	{
		//如果接到了结果
		recvRollAgreeCount++;

		//随机
		int rn = RandNumber( 1, 100 );
#ifdef _DEBUG
		D_DEBUG("Roll点:%sRoll到了%d点\n",player->GetNickName(), rn );
#endif
		
		if ( rn > maxRollNum )//除非Roll到的点数比原来大，否则还是按先Roll的玩家获得物品
		{
			maxRollNum   = rn;
			mWinPlayerID = player->GetFullID();//获得该道具的玩家
			ACE_OS::strncpy( mWinPlayerName, player->GetNickName(), MAX_NAME_SIZE );//记录Roll到的玩家姓名
			mIsHaveWinPlayer = true;

#ifdef _DEBUG
			D_DEBUG("玩家%s现成了Roll点中的最大值玩家\n", mWinPlayerName );
#endif	
		}

		//通知Roll到的点数给其他的玩家
		MGRollItemNum rollNum;
		rollNum.num = rn;
		rollNum.rollID = mpItemEle.GetItemTypeID();
		ACE_OS::strncpy( rollNum.rollPlayerName, player->GetNickName(), MAX_NAME_SIZE );

		for ( std::vector<PlayerID>::iterator lter = mRollPlayerIDVec.begin(); 
			lter != mRollPlayerIDVec.end();
			++lter )
		{
			const PlayerID& tmpID = *lter;
			CGateSrv*pSrv = CManG_MProc::FindProc( tmpID.wGID );
			if ( pSrv )
			{
				CPlayer* pTmpPlayer = pSrv->FindPlayer( tmpID.dwPID );
				if ( pTmpPlayer )
				{
					pTmpPlayer->SendPkg<MGRollItemNum>( &rollNum );
				}
			}
		}
	}
	else
	{
		recvRollDisAgrCount++;

		//通知其他玩家，玩家放弃了Roll点
		MGRollItemNum rollNum;
		rollNum.num = 0;
		rollNum.rollID = mpItemEle.GetItemTypeID();
		ACE_OS::strncpy( rollNum.rollPlayerName, player->GetNickName(), MAX_NAME_SIZE );

		for ( std::vector<PlayerID>::iterator lter = mRollPlayerIDVec.begin(); 
			lter != mRollPlayerIDVec.end();
			++lter )
		{
			const PlayerID& tmpID = *lter;
			CGateSrv*pSrv = CManG_MProc::FindProc( tmpID.wGID );
			if ( pSrv )
			{
				CPlayer* pTmpPlayer = pSrv->FindPlayer( tmpID.dwPID );
				if ( pTmpPlayer )
				{
					pTmpPlayer->SendPkg<MGRollItemNum>( &rollNum );
				}
			}
		}
	}

	//如果人数到达了预期的人数，开始Roll物品
	if ( ( recvRollDisAgrCount + recvRollAgreeCount ) == rollPeopleCount )
	{
		RandRollItem();
		return true;
	}

	TRY_END;

	return false;
}

bool RollItem::IsPass30s()
{
	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	ACE_Time_Value diffTime = currentTime - mBeginRollTime;
	return ( diffTime.sec() >= 35 && (mrollOver == false ));
}

void RollItem::OnPlayerLeaveSrv( CPlayer* pLeavePlayer )
{
	if ( !pLeavePlayer )
		return;

	bool isRollPlayer = false;
	for ( std::vector<PlayerID>::iterator lter = mRollPlayerIDVec.begin(); 
		lter != mRollPlayerIDVec.end();
		++lter )
	{
		const PlayerID& tmpID = *lter;
		if ( pLeavePlayer->GetFullID() == tmpID )
		{
			isRollPlayer = true;
			break;
		}
	}

	//离开服务器都是同意roll点
	if ( isRollPlayer )
	{
		RecvRollReq( pLeavePlayer, true );
	}
}

void RollItem::CheckTimeout(bool &needDel)
{
	needDel = false;

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	ACE_Time_Value diffTime = currentTime - mBeginRollTime;
	if((diffTime.sec() >= 30))//30秒roll是否超时
	{	
		if(!IsRollOver())
		{	
			RandRollItem();
			return;
		}
	}

	if(diffTime.sec() >= 60)//60秒是否拾取超时
	{
		CItemPkgEle *pPkgEle = mpItemPkg->GetEleByItemTypeId(mpItemEle.GetItemTypeID());
		if(NULL != pPkgEle)
			pPkgEle->UpdateRollState(CItemPkgEle::ROLL_PICK_TIMEOUT);
			
		needDel = true;
	}

	return;
}
