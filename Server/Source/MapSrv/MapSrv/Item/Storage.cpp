﻿
#include "Storage.h"
#include "../Player/Player.h"
#include "ExpandStorageInfo.h"
#include "../LogManager.h"

#define	 STORAGE_NUM_PERPAGE STORAGE_COLUMN*8


void CPlayerStorage::AttachPlayer( CPlayer* pAttachPlayer )
{
	if ( pAttachPlayer )
	{
		m_pAttachPlayer = pAttachPlayer;

		Reset();

		m_storageUID = m_pAttachPlayer->GetDBUID();
	}
}

void CPlayerStorage::RecvStorageInfo(const ItemInfo_i *storageInfoArr, unsigned int page)
{
	if ( !m_pAttachPlayer )
		return;

	if ( page >= STORAGE_MAX_PAGE )
	{
		D_ERROR("玩家%s接受到的仓库信息页数过大%d\n", m_pAttachPlayer->GetNickName(), page );
		return;
	}

	if ( !storageInfoArr )
	{
		D_ERROR("玩家%s接到的仓库内容为空,第%d页信息\n", m_pAttachPlayer->GetNickName(), page );
		return;
	}

	ItemInfo_i* pUpdatePos = ( mStorageItemArr + page * STORAGE_PAGE_SIZE ); 
	if ( pUpdatePos )//更新对应的页数数据
	{
		ACE_OS::memcpy( pUpdatePos,storageInfoArr ,sizeof(ItemInfo_i) * STORAGE_PAGE_SIZE  );
	}

	//将对应页的信息通知client
	NoticeStroageItemInfoToPlayer( page );
}


void CPlayerStorage::RecvStorageSecurityInfo(unsigned int validRow , STORAGE_SECURITY_STRATEGY SecurityMode ,const char* psd )
{
	if ( !m_pAttachPlayer )
		return;

	if ( !psd )
		return;

	if ( validRow >= MAX_STORAGE_ROWCOUNT )
	{
		D_ERROR("玩家%s的可用的行数%d出错,超过最大行数27\n",m_pAttachPlayer->GetNickName(), validRow );
		return;
	}

	if ( SecurityMode >= E_STARATEGY_MAX )
	{
		D_ERROR("玩家%s的仓库安全模式错误\n",m_pAttachPlayer->GetNickName() );
		return;
	}

	m_ValidRowCount = validRow;  
	m_SecurityMode  = SecurityMode;
	SafeStrCpy( m_storagePassWord, psd );
	//ACE_OS::strncpy( m_storagePassWord , psd , sizeof(m_storagePassWord) );

	if ( m_bStorageLock )//默认为锁定状态.如果没有安全模式，则得由锁定状态变为非锁定状态.如果为解锁状态，则应该是跳地图前的修改
	{
		if ( ( m_SecurityMode == E_UNSECURITY_MODE ) ||
			(  m_SecurityMode == E_STARATEGY_MAX   ) ||
			   CheckPassWordEmpty() )//如果仓库有安全策略,则需要服务器在第一次使用前锁定仓库
		{
			UnLockStoragePlate();
		}
		else
		{
			LockStoragePlate();
		}
	}

	//将安全信息通知client
	NoticeStroageSafeInfoToPlayer();
}

//通知仓库的锁定状态
void CPlayerStorage::NoticeSotoragePlateLockState()
{
	if ( !m_pAttachPlayer )
		return;

	MGChangeStorageLockState lockState;
	lockState.changeStorageState.isLock = m_bStorageLock;
	m_pAttachPlayer->SendPkg<MGChangeStorageLockState>( &lockState );
}

//安全信息通知
void CPlayerStorage::NoticeStroageSafeInfoToPlayer()
{
	if ( !m_pAttachPlayer )
		return;

	MGStorageSafeInfo safeInfo;
	safeInfo.safeInfo.validRow     = m_ValidRowCount;
	safeInfo.safeInfo.safeStrategy = m_SecurityMode;
	m_pAttachPlayer->SendPkg<MGStorageSafeInfo>( &safeInfo );
}

//道具信息通知
void CPlayerStorage::NoticeStroageItemInfoToPlayer( unsigned storagePageIndex )
{
	if ( !m_pAttachPlayer )
		return;

	if ( storagePageIndex >= STORAGE_MAX_PAGE )
	{
		D_ERROR("%s 通知仓库的页数非法 %d \n",m_pAttachPlayer->GetNickName(), storagePageIndex );
		return;
	}

	unsigned int _index    = storagePageIndex * STORAGE_PAGE_SIZE;
	if ( _index > ( MAX_STORAGE_ITEMCOUNT - STORAGE_PAGE_SIZE ) )
	{
		D_ERROR("%s 仓库索引非法 %d \n",m_pAttachPlayer->GetNickName(), _index );
		return;
	}

	ItemInfo_i* pItemIndex =  mStorageItemArr + _index;

	MGStorageItemInfo storagePageItemInfo;
	StructMemSet( storagePageItemInfo, 0x0, sizeof(storagePageItemInfo) );
	//StructMemCpy( storagePageItemInfo.storageItemInfo.itemInfoArr, pItemIndex , sizeof(storagePageItemInfo.storageItemInfo.itemInfoArr)  );
	//todo
	for ( int i = 0; i<STORAGE_PAGE_SIZE; ++i)
	{
		storagePageItemInfo.storageItemInfo.itemInfoArr[i].itemIndex = -1;
		if (!IsLeftUpStorageIndex(i+_index))
		{
			continue;
		}
		storagePageItemInfo.storageItemInfo.itemInfoArr[i].itemIndex = i+_index;
		storagePageItemInfo.storageItemInfo.itemInfoArr[i].itemInfo = *(pItemIndex+i);
	}
	storagePageItemInfo.storageItemInfo.itemLen   = STORAGE_PAGE_SIZE;
	//storagePageItemInfo.storageItemInfo.pageIndex = storagePageIndex;

	m_pAttachPlayer->SendPkg<MGStorageItemInfo>( &storagePageItemInfo );

}

void CPlayerStorage::NoticeStorageInfoChange(unsigned int storageIndex, bool isNoticeClient )
{
	if ( !m_pAttachPlayer )
		return;


	if ( storageIndex >= MAX_STORAGE_ITEMCOUNT )
	{
		D_ERROR("%s 更新仓库对应位的索引出错 %d\n", m_pAttachPlayer->GetNickName(), storageIndex );
		return;
	}

	const ItemInfo_i& storageItem = mStorageItemArr[storageIndex];

	if (isNoticeClient)
	{
		if (!IsLeftUpStorageIndex(storageIndex))
		{
			//非左上角的位置在玩家看来是空的
			MGUpdateStorageItemInfoByIndex updateStorageInfo;
			StructMemSet( updateStorageInfo, 0x0 ,sizeof(updateStorageInfo) );
			updateStorageInfo.updateStorageItem.storageIndex = storageIndex;
			m_pAttachPlayer->SendPkg<MGUpdateStorageItemInfoByIndex>( &updateStorageInfo );

			unsigned int index = storageIndex;
			ItemInfo_i item;
			GetItemInfoByStorageIndex(index,item,storageIndex);
		}
		MGUpdateStorageItemInfoByIndex updateStorageInfo;
		StructMemSet( updateStorageInfo, 0x0 ,sizeof(updateStorageInfo) );
		updateStorageInfo.updateStorageItem.storageIndex = storageIndex;
		updateStorageInfo.updateStorageItem.storageItem  = storageItem;
		m_pAttachPlayer->SendPkg<MGUpdateStorageItemInfoByIndex>( &updateStorageInfo );
	}

	MGUpdateStorageItemInfo updateStorageItem;
	updateStorageItem.updateStorageItem.storageIndex = storageIndex;
	updateStorageItem.updateStorageItem.storageItem  = storageItem;
	updateStorageItem.updateStorageItem.storageUID   = GetStorageUID();
	m_pAttachPlayer->SendPkg<MGUpdateStorageItemInfo>( &updateStorageItem );

}

void CPlayerStorage::NoticeUpdateDBStorageSafeInfo()
{
	if ( !m_pAttachPlayer )
		return;

	MGUpdateStorageSafeInfo safeInfo;
	StructMemSet( safeInfo, 0x0 ,sizeof(safeInfo) );
	safeInfo.updateSafeInfo.storageUID = GetStorageUID();
	safeInfo.updateSafeInfo.validRow   = GetStorageValidRow();
	safeInfo.updateSafeInfo.strategyMode = GetStorageSafeStrategy();
	SafeStrCpy(  safeInfo.updateSafeInfo.newPsd, m_storagePassWord  );
	//ACE_OS::strncpy( safeInfo.updateSafeInfo.newPsd, m_storagePassWord , sizeof(safeInfo.updateSafeInfo.newPsd) );
	m_pAttachPlayer->SendPkg<MGUpdateStorageSafeInfo>( &safeInfo );
}

void CPlayerStorage::NoticeUpdateDBForbiddenInfo()
{
	if ( !m_pAttachPlayer  )
		return;

	MGUpdateStorageForbiddenInfo forbiddenInfo;
	forbiddenInfo.updateForbiddenInfo.forbiddenTime = GetStorageUnForbiddenTime();
	forbiddenInfo.updateForbiddenInfo.inputErrCount = GetStorageInputPsdErrCount();
	forbiddenInfo.updateForbiddenInfo.storageUID    = GetStorageUID();
	m_pAttachPlayer->SendPkg<MGUpdateStorageForbiddenInfo>( &forbiddenInfo );
}

void CPlayerStorage::NoticeInputStoragePsdErr()
{
	if ( !m_pAttachPlayer )
		return;

	if ( m_passErrCount < 5 )//输入错误次数小于5次
	{
		MGTakeItemCheckPsdError checkPsdError;
		checkPsdError.checkPsdError.errNo = 5 - m_passErrCount;
		m_pAttachPlayer->SendPkg<MGTakeItemCheckPsdError>( &checkPsdError );
	}
	else if( m_passErrCount == 5 )//通知client 仓库已被禁止解锁
	{
		MGTakeItemCheckPsdError checkPsdError;
		checkPsdError.checkPsdError.errNo = 5;
		m_pAttachPlayer->SendPkg<MGTakeItemCheckPsdError>( &checkPsdError );
	}
}

void CPlayerStorage::NoticeStorageSecurityStrategy()
{
	if ( !m_pAttachPlayer )
		return;

	MGUpdateStorageSecurityStrategy securityMode;
	securityMode.securityMode.newStrategy = m_SecurityMode;
	m_pAttachPlayer->SendPkg<MGUpdateStorageSecurityStrategy>( &securityMode );

	NoticeUpdateDBStorageSafeInfo();
}

void CPlayerStorage::NoticeStorageValidRowUpdate()
{
	if ( !m_pAttachPlayer )
		return;

	MGUpdateStorageValidRow updateValidRow;
	updateValidRow.updateValidRow.validRow = m_ValidRowCount;
	m_pAttachPlayer->SendPkg<MGUpdateStorageValidRow>( &updateValidRow );

	NoticeUpdateDBStorageSafeInfo();
}

void CPlayerStorage::NoticeTakeItemCheckPsdRst( int reasonNo )
{
	if ( !m_pAttachPlayer )
		return;

	MGTakeItemCheckPsdError checkPsdError;
	checkPsdError.checkPsdError.errNo = reasonNo;
	m_pAttachPlayer->SendPkg<MGTakeItemCheckPsdError>( &checkPsdError );
}

void CPlayerStorage::NoticeExpendStorageRst( int reasonNo  )
{
	if ( !m_pAttachPlayer )
		return;

	MGExtendStorageError extendError;
	extendError.extendError.errNo = reasonNo;
	m_pAttachPlayer->SendPkg<MGExtendStorageError>( &extendError );
}

//通知玩家打开仓库界面
bool CPlayerStorage::ShowStoragePlate()
{
	if ( !m_pAttachPlayer )
		return false;

	MGShowStoragePlate showplate;
	m_pAttachPlayer->SendPkg<MGShowStoragePlate>( &showplate );

	if ( !m_bqueryStorageInfo )
	{
		MGQueryStorageInfo queryMsg;
		queryMsg.queryStorageInfo.queryPlayerID = m_pAttachPlayer->GetFullID();
		queryMsg.queryStorageInfo.storageUID    = m_pAttachPlayer->GetDBUID();
		m_pAttachPlayer->SendPkg< MGQueryStorageInfo >( &queryMsg );

		m_bqueryStorageInfo = true;
	}
	else
	{
		//通知安全信息
		NoticeStroageSafeInfoToPlayer();

		//通知锁定信息
		NoticeSotoragePlateLockState();
	}

	CLogManager::DoWarehouseOpen(m_pAttachPlayer, m_storageUID, !CheckPassWordEmpty());//记日至
	return true;
}

//关闭仓库界面
void CPlayerStorage::CloseStoragePlate()
{

	if( GetStorageSafeStrategy() == E_EVERYSESSION_CHECK )//如果采取的安全策略是每次对话的第一次输入密码,则关闭界面后,等于结束了本次对话,重新锁定仓库
		LockStoragePlate();

	m_takeItemWating.Reset();
}

//检查密码是否为空
bool CPlayerStorage::CheckPassWordEmpty()
{
	return m_storagePassWord[0] == '\0';
}

bool CPlayerStorage::PutItemToStorageFromPkg( unsigned int pkgIndex )
{
	int errNo = 1;

	do 
	{
		if ( !m_pAttachPlayer )
			break;

		if ( pkgIndex >= NORMAL_PKG_SIZE )
		{
			D_ERROR("将物品放入仓库时,玩家 %s 从包裹错误的索引位 %d 取出\n", m_pAttachPlayer->GetNickName(), pkgIndex );
			break;
		}

		FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CPlayerStorage::PutItemToStorageFromPkg, NULL == m_pAttachPlayer->GetFullPlayerInfo()\n");
			break;
		}
		ItemInfo_i& pkgItem  = playerInfo->pkgInfo.pkgs[pkgIndex];
		if ( pkgItem.uiID == 0 || pkgItem.usItemTypeID == 0 )
		{
			D_ERROR("%s 取出的包裹位 %d 上的道具为空\n", m_pAttachPlayer->GetNickName(), pkgIndex );
			break;
		}

		CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( pkgItem.usItemTypeID );
		if ( !pPublicProp )
		{
			D_ERROR("%s 从包裹取出的物品 %d 在道具属性表中找不到\n", m_pAttachPlayer->GetNickName(), pkgItem.usItemTypeID  );
			break;
		}

		if ( pPublicProp->m_cannotDepot == 1 )//不可存仓库
		{
			break;
		}

		if ( pPublicProp->m_itemMaxAmount == 1 )//如果为不可堆叠的物品
		{
			//遍历整个仓库格子
			int i = GetFreeStorageIndex(CBaseItem::GetWidth(pkgItem.usItemTypeID),CBaseItem::GetHeight(pkgItem.usItemTypeID));
			if ( i != -1 )
			{
				m_pAttachPlayer->OnPutItemToStorage( pkgItem.uiID );//已经全部交换进入玩家仓库,故将数据从玩家包裹中删除,并重置
				CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DEPOSIT, pkgItem.uiID, pkgItem.usItemTypeID, pkgItem.ucCount);//记日至
				SetStorageItem(pkgItem,i);
				m_pAttachPlayer->ClearPkgItem(pkgItem.uiID);
				//pkgItem.ucCount = pkgItem.ucLevelUp = pkgItem.uiID = pkgItem.randSet/*usAddId*/ = pkgItem.usItemTypeID = pkgItem.usWearPoint = 0;//将对应的包裹位置空

				NoticeStorageInfoChange( i );//通知仓库对应位更新
				m_pAttachPlayer->NoticePkgItemChange( pkgIndex );//通知client更新包裹界面
				errNo = 0;
			}
			else //仓库已满
			{
				errNo = 3;
			}
		}
		else//如果为可堆叠物品
		{
			unsigned int itemMaxCount   = pPublicProp->m_itemMaxAmount;//该类道具的最大堆叠数
			unsigned int needPutCount   = pkgItem.ucCount;//需要放入仓库的道具数量

			ItemInfo_i   tmpItemArr[MAX_STORAGE_ITEMCOUNT];//临时的存储更新后仓库格子变化的结果
			unsigned int tmpItemIndexArr[MAX_STORAGE_ITEMCOUNT] = { 0 };//临时的存储需要更新仓库格子的索引
			unsigned int iTmpVaildIndex = 0;//需要更新的仓库格子的数目
			int iFirstEmptyIndex = -1;//第一个找到的空位

			unsigned int validItemCount = GetValidStorageItemCount();
			//遍历整个仓库格子
			for ( unsigned int i = 0 ; i< validItemCount  &&  needPutCount > 0 && i< ARRAY_SIZE(mStorageItemArr) ; i++ )
			{
				const ItemInfo_i& storageItem = mStorageItemArr[i];
				if ( storageItem.uiID == 0  && iFirstEmptyIndex == -1 )
					iFirstEmptyIndex = i;//记录仓库中第一个空的格子

				//找到同类型的道具,堆叠物品
				if ( storageItem.usItemTypeID == pkgItem.usItemTypeID )
				{
					int  subCount = ( itemMaxCount - storageItem.ucCount );
					if ( subCount > 0 )
					{
						if (iTmpVaildIndex >= ARRAY_SIZE(tmpItemArr))
						{
							D_ERROR( "CPlayerStorage::PutItemToStorageFromPkg, iTmpValidIndex(%d) >= ARRAY_SIZE(tmpItemArr)\n", iTmpVaildIndex );
							break;
						}
						if (iTmpVaildIndex >= ARRAY_SIZE(tmpItemIndexArr))
						{
							D_ERROR( "CPlayerStorage::PutItemToStorageFromPkg, iTmpValidIndex(%d) >= ARRAY_SIZE(tmpItemIndexArr)\n", iTmpVaildIndex );
							break;
						}
						if ( needPutCount < (unsigned int)subCount )//能一次堆叠完
						{
							tmpItemArr[iTmpVaildIndex]      = storageItem;
							tmpItemArr[iTmpVaildIndex].ucCount     += needPutCount;
							tmpItemIndexArr[iTmpVaildIndex] = i;

							needPutCount = 0;
						}
						else//只能堆叠部分物品,还有剩余部分需要继续堆叠
						{
							tmpItemArr[iTmpVaildIndex]  = storageItem;
							tmpItemArr[iTmpVaildIndex].ucCount  = itemMaxCount;
							tmpItemIndexArr[ iTmpVaildIndex ] = i;

							needPutCount -= subCount;
						}

						iTmpVaildIndex++;
					}
				}
			}

			if ( needPutCount > 0 )//没有堆叠完,还有剩余的物品
			{
				if ( iFirstEmptyIndex ==  -1 )//如果没有可堆叠的位置
				{
					errNo = 3;
					break;
				}

				if (iTmpVaildIndex >= ARRAY_SIZE(tmpItemArr))
				{
					D_ERROR( "CPlayerStorage::PutItemToStorageFromPkg, iTmpValidIndex(%d) >= ARRAY_SIZE(tmpItemArr)\n", iTmpVaildIndex );
					break;
				}
				if (iTmpVaildIndex >= ARRAY_SIZE(tmpItemIndexArr))
				{
					D_ERROR( "CPlayerStorage::PutItemToStorageFromPkg, iTmpValidIndex(%d) >= ARRAY_SIZE(tmpItemIndexArr)\n", iTmpVaildIndex );
					break;
				}
				tmpItemArr[iTmpVaildIndex] = pkgItem;
				tmpItemArr[iTmpVaildIndex].ucCount = needPutCount;
				tmpItemIndexArr[ iTmpVaildIndex ]  = iFirstEmptyIndex;
				iTmpVaildIndex++;
			}

			if ( iTmpVaildIndex  > 0 )
			{
				for( unsigned int i = 0; i< iTmpVaildIndex && i< ARRAY_SIZE(tmpItemArr) && i< ARRAY_SIZE(tmpItemIndexArr); i++ )
				{
					const ItemInfo_i&  tmpUpdateItem = tmpItemArr[i];
					unsigned int       updateIndex   = tmpItemIndexArr[i];

					if (updateIndex >= ARRAY_SIZE(mStorageItemArr))
					{
						D_ERROR( "CPlayerStorage::PutItemToStorageFromPkg, updateIndex(%d) >= ARRAY_SIZE(mStorageItemArr)\n", updateIndex );
						continue;
					}
					mStorageItemArr[ updateIndex ] = tmpUpdateItem;

					//将改变的每一位仓库位通知更新
					NoticeStorageInfoChange( updateIndex );
				}
			}

			CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DEPOSIT, pkgItem.uiID, pkgItem.usItemTypeID, pkgItem.ucCount);//记日至
			m_pAttachPlayer->OnPutItemToStorage( pkgItem.uiID );//已经全部交换进入玩家仓库,故将数据从玩家包裹中删除,并重置
			pkgItem.ucCount = pkgItem.ucLevelUp = pkgItem.uiID = pkgItem.randSet/*usAddId*/ = pkgItem.usItemTypeID = pkgItem.usWearPoint = 0;//将对应的包裹位置空
			m_pAttachPlayer->NoticePkgItemChange( pkgIndex );//通知client更新包裹界面
			errNo = 0;
			break;
		}

	} while( false );

	MGPutItemToStorageError putStorageErr;
	putStorageErr.putItemError.errNo    = errNo;//如果是0,则代表放入仓库正确
	putStorageErr.putItemError.pkgIndex = pkgIndex;
	m_pAttachPlayer->SendPkg<MGPutItemToStorageError>( &putStorageErr );

	return false;
}

bool CPlayerStorage::PutItemToStorageFromPkg( unsigned int pkgIndex, unsigned int storageIndex )
{
	int errNo = 1;

	do 
	{
		if ( !m_pAttachPlayer )
			break;

		if ( pkgIndex >= NORMAL_PKG_SIZE )
		{
			D_ERROR("将物品放入仓库时,玩家 %s 从包裹错误的索引位 %d 取出\n", m_pAttachPlayer->GetNickName(), pkgIndex );
			break;
		}

		if ( storageIndex >= MAX_STORAGE_ITEMCOUNT )
		{
			D_ERROR("将物品放入仓库时,玩家 %s 放入错误的仓库索引位%d\n",m_pAttachPlayer->GetNickName(),storageIndex );
			break;
		}

		if( !CheckPlayerStorageIndexIsValid( storageIndex ) )
		{
			D_ERROR("玩家 %s 交换的仓库道具的索引不合法 %d 当前的可用行数为 %d\n", m_pAttachPlayer->GetNickName() , storageIndex, m_ValidRowCount );
			break;
		}

		FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CPlayerStorage::PutItemToStorageFromPkg, NULL == m_pAttachPlayer->GetFullPlayerInfo()\n");
			break;
		}
		ItemInfo_i& pkgItem  = playerInfo->pkgInfo.pkgs[pkgIndex];
		if ( pkgItem.uiID == 0 || pkgItem.usItemTypeID == 0 )
		{
			D_ERROR("%s 取出的包裹位 %d 上的道具为空\n", m_pAttachPlayer->GetNickName(), pkgIndex );
			break;
		}

		CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( pkgItem.usItemTypeID );
		if ( !pPublicProp )
		{
			D_ERROR("%s 从包裹取出的物品 %d 在道具属性表中找不到\n", m_pAttachPlayer->GetNickName(), pkgItem.usItemTypeID  );
			break;
		}

		if ( pPublicProp->m_cannotDepot == 1 )//不可存仓库
		{
			break;
		}

		//ItemInfo_i& storageItem = mStorageItemArr[ storageIndex ];
		//if ( ( storageItem.uiID == 0 ) || ( storageItem.usItemTypeID != pkgItem.usItemTypeID ))//如果这一位为空 或不同道具的交换
		if ( IsFreeStorageIndex( storageIndex,CBaseItem::GetWidth(pkgItem.usItemTypeID),CBaseItem::GetHeight(pkgItem.usItemTypeID) ) )
		{


				m_pAttachPlayer->OnPutItemToStorage( pkgItem.uiID );//已经全部交换进入玩家仓库,故将数据从玩家包裹中删除,并重置
				CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DEPOSIT, pkgItem.uiID, pkgItem.usItemTypeID, pkgItem.ucCount);//记日至
				SetStorageItem(pkgItem,storageIndex);
				m_pAttachPlayer->ClearPkgItem(pkgItem.uiID);
				//pkgItem.ucCount = pkgItem.ucLevelUp = pkgItem.uiID = pkgItem.randSet/*usAddId*/ = pkgItem.usItemTypeID = pkgItem.usWearPoint = 0;//将对应的包裹位置空

				NoticeStorageInfoChange( storageIndex );//通知仓库对应位更新
				m_pAttachPlayer->NoticePkgItemChange( pkgIndex );//通知client更新包裹界面
				errNo = 0;


			//if ( storageItem.uiID != 0 )//如果是不同道具间的交换
			//{
			//	if(  !m_pAttachPlayer->OnGetItemFromStorage( storageItem ) )
			//	{
			//		errNo = 2;
			//		break;
			//	}
			//} 

			//if (  !m_pAttachPlayer->OnPutItemToStorage( pkgItem.uiID ) )
			//{
			//	errNo = 2;
			//	break;
			//}

			//ItemInfo_i tmpItem = storageItem;
			//storageItem =  pkgItem;
			//pkgItem = tmpItem;

			////通知client的仓库和背包界面更新
			//m_pAttachPlayer->NoticePkgItemChange( pkgIndex );
			//NoticeStorageInfoChange( storageIndex );
			//CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DEPOSIT, storageItem.uiID, storageItem.usItemTypeID, storageItem.ucCount);//记日至
			//if(0 != pkgItem.uiID)
			//{	
			//	CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DRAW, pkgItem.uiID, pkgItem.usItemTypeID, pkgItem.ucCount);//记日至
			//}
			//errNo = 0;
			
			break;
		}
		else//如果这一位不为空,则证明有道具
		{
			errNo = 3;
			//if ( storageItem.usItemTypeID == pkgItem.usItemTypeID )//如果装备类型是一致的
			//{
			//	if( pPublicProp->m_itemMaxAmount == 1 )//如果是不可堆叠的物品,则直接改变内存中的道具
			//	{
			//		CBaseItem* pItem = m_pAttachPlayer->GetItemDelegate().GetPkgItem( pkgItem.uiID );
			//		if ( pItem )
			//		{
			//		    pItem->SetItemUID( storageItem.uiID );
			//			ItemInfo_i tmpItem = storageItem;
			//			storageItem =  pkgItem;
			//			pkgItem = tmpItem;

			//			//通知client的仓库和背包界面更新
			//			m_pAttachPlayer->NoticePkgItemChange( pkgIndex );
			//			NoticeStorageInfoChange( storageIndex );
			//			CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DRAW, pkgItem.uiID, pkgItem.usItemTypeID, pkgItem.ucCount);//记日至
			//			CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DEPOSIT, storageItem.uiID, storageItem.usItemTypeID, storageItem.ucCount);//记日至
			//			errNo = 0;
			//		}
			//	}
			//	else //如果是可堆叠的物品
			//	{
			//		if ( storageItem.ucCount < pPublicProp->m_itemMaxAmount )
			//		{
			//			int  subCount = pPublicProp->m_itemMaxAmount - storageItem.ucCount;
			//			int changeCount = 0;//用于记日至
			//			if ( subCount < pkgItem.ucCount )//将多余的物品还是留在背包中
			//			{
			//				changeCount = subCount;

			//				storageItem.ucCount = pPublicProp->m_itemMaxAmount;
			//				pkgItem.ucCount    -= subCount;
			//			}
			//			else
			//			{
			//				changeCount = pkgItem.ucCount;

			//				storageItem.ucCount += pkgItem.ucCount;
			//				m_pAttachPlayer->OnPutItemToStorage( pkgItem.uiID );//已经全部交换进入玩家仓库,故将数据从玩家包裹中删除,并重置
			//				pkgItem.ucCount = pkgItem.ucLevelUp = pkgItem.uiID = pkgItem.randSet/*usAddId*/ = pkgItem.usItemTypeID = pkgItem.usWearPoint = 0;
			//			}

			//			//通知client的仓库和背包界面更新
			//			m_pAttachPlayer->NoticePkgItemChange( pkgIndex );
			//			NoticeStorageInfoChange( storageIndex );
			//			CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DEPOSIT, storageItem.uiID, storageItem.usItemTypeID, changeCount);//记日至
			//			errNo = 0;
			//			break;
			//		}
			//	}//pPublicProp->m_itemMaxAmount == 1
			//}//storageItem.usItemTypeID == pkgItem.usItemTypeID
		}

	} while( false );

	MGPutItemToStorageError putStorageErr;
	putStorageErr.putItemError.errNo    = errNo;
	putStorageErr.putItemError.pkgIndex = pkgIndex;
	m_pAttachPlayer->SendPkg<MGPutItemToStorageError>( &putStorageErr );

	return false;
}

bool CPlayerStorage::TakeItemFromStorageToPkg(unsigned int storageIndex, unsigned int pkgindex )
{
	int errNo = 1;

	do 
	{
		if ( !m_pAttachPlayer )
			break;

		if ( pkgindex >= NORMAL_PKG_SIZE )
		{
			D_ERROR("将物品从仓库中取出时,玩家 %s 放入包裹错误的索引位 %d \n", m_pAttachPlayer->GetNickName(), pkgindex );
			break;
		}

		if ( storageIndex >= MAX_STORAGE_ITEMCOUNT )
		{
			D_ERROR("将物品从仓库取出时,玩家 %s 从错误的仓库索引位 %d 放入\n",m_pAttachPlayer->GetNickName(),storageIndex );
			break;
		}

		if( !CheckPlayerStorageIndexIsValid( storageIndex ) )
		{
			D_ERROR("TakeItemFromStorageToPkg 玩家 %s 取出的仓库道具的索引不合法 %d 当前的可用行数为 %d\n", m_pAttachPlayer->GetNickName() , storageIndex, m_ValidRowCount );
			break;
		}

		ItemInfo_i& storageItem = mStorageItemArr[storageIndex];
		if ( storageItem.uiID == 0 || storageItem.usItemTypeID == 0 )
		{
			D_ERROR("%s 取出的仓库位 %d 上的道具为空\n", m_pAttachPlayer->GetNickName(), storageIndex );
			break;
		}

		FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CPlayerStorage::TakeItemFromStorageToPkg, NULL == m_pAttachPlayer->GetFullPlayerInfo()\n");
			break;
		}
		ItemInfo_i& pkgItem  = playerInfo->pkgInfo.pkgs[pkgindex];
		//if ( ( pkgItem.uiID == 0 ) || ( pkgItem.usItemTypeID != storageItem.usItemTypeID ) )//如果要交换的位置道具为空,或是不同的道具间的交换
		if (m_pAttachPlayer->IsFreePkgIndex(pkgindex,CBaseItem::GetWidth(storageItem.usItemTypeID),CBaseItem::GetHeight(storageItem.usItemTypeID) ) )
		{
			////如果交换物品不为空
			//if ( pkgItem.usItemTypeID != 0  )
			//{
			//	CItemPublicProp* pPkgPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( pkgItem.usItemTypeID );
			//	if ( !pPkgPublicProp )
			//	{
			//		D_ERROR("%s 将要和仓库物品交换的物品 %d 在道具属性表中找不到\n", m_pAttachPlayer->GetNickName(), pkgItem.usItemTypeID  );
			//		break;
			//	}

			//	if( pPkgPublicProp->m_cannotDepot == 1 )
			//	{
			//		errNo = 4;//被交换的物品不能放入仓库
			//		break;
			//	}
			//}

			//创建个新的道具
			if( ItemManagerSingle::instance()->RecordItemDetialInfo( m_pAttachPlayer, storageItem, pkgindex, false ) )
			{
				//if ( pkgItem.uiID != 0  )//如果和非同类物品交换,回收原来放于此的道具
				//	m_pAttachPlayer->OnPutItemToStorage( pkgItem.uiID );

				//ItemInfo_i tmpItem = pkgItem;
				//pkgItem     = storageItem;
				//storageItem = tmpItem;

				m_pAttachPlayer->SetPkgItem(storageItem,pkgindex);
				ClearStorageItem(storageItem.uiID);

				//通知client的仓库和背包界面更新
				m_pAttachPlayer->NoticePkgItemChange( pkgindex );
				NoticeStorageInfoChange( storageIndex );
				errNo = 0;
				CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DRAW, pkgItem.uiID, pkgItem.usItemTypeID, pkgItem.ucCount);//记日至
				if(0 != storageItem.uiID)
				{
					CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DEPOSIT, storageItem.uiID, storageItem.usItemTypeID, storageItem.ucCount);//记日至
				}
				break;
			}

			errNo = 5;//仓库中的物品无法放进背包
			break;
		}
		else
		{
			errNo = 5;
		}
		//else if( pkgItem.usItemTypeID == storageItem.usItemTypeID  )//如果是同类型的道具
		//{
		//	CItemPublicProp* pStroagePublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( storageItem.usItemTypeID );
		//	if ( !pStroagePublicProp )
		//	{
		//		D_ERROR("%s 从仓库中取出的物品 %d 在道具属性表中找不到\n", m_pAttachPlayer->GetNickName(), storageItem.usItemTypeID  );
		//		break;
		//	}

		//	//获取该类道具的最大堆叠数目
		//	unsigned int maxAccount = pStroagePublicProp->m_itemMaxAmount;
		//	if ( maxAccount == 1 )//如果是不可堆叠的物品
		//	{
		//		CBaseItem* pItem = m_pAttachPlayer->GetItemDelegate().GetPkgItem( pkgItem.uiID );
		//		if ( pItem )
		//		{
		//			pItem->SetItemUID( storageItem.uiID );

		//			ItemInfo_i tmpItem = storageItem;
		//			storageItem =  pkgItem;
		//			pkgItem = tmpItem;

		//			//通知client的仓库和背包界面更新
		//			m_pAttachPlayer->NoticePkgItemChange( pkgindex );
		//			NoticeStorageInfoChange( storageIndex );
		//			errNo = 0;
		//			CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DRAW, pkgItem.uiID, pkgItem.usItemTypeID, pkgItem.ucCount);//记日至
		//			CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DEPOSIT, storageItem.uiID, storageItem.usItemTypeID, storageItem.ucCount);//记日至
		//			break;
		//		}
		//	}
		//	else//如果是可堆叠
		//	{
		//		if( (unsigned int)pkgItem.ucCount <  maxAccount )//如果还没有堆叠满,合并两个道具
		//		{
		//			int subItem = ( maxAccount - pkgItem.ucCount );
		//			unsigned changeCount = 0;//用于记日至，统计变化的个数
		//			if ( storageItem.ucCount > subItem )
		//			{
		//				changeCount = subItem;

		//				pkgItem.ucCount = maxAccount;
		//				storageItem.ucCount -= subItem;
		//				}
		//			else
		//			{
		//				changeCount = storageItem.ucCount ;

		//				pkgItem.ucCount += storageItem.ucCount ; 
		//				storageItem.ucCount = storageItem.ucLevelUp = storageItem.uiID = storageItem.randSet/*usAddId*/ = storageItem.usItemTypeID = storageItem.usWearPoint = 0;		
		//			}

		//			//通知client的仓库和背包界面更新
		//			m_pAttachPlayer->NoticePkgItemChange( pkgindex );
		//			NoticeStorageInfoChange( storageIndex );
		//			errNo = 0;
		//			CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DRAW, pkgItem.uiID, pkgItem.usItemTypeID, changeCount);//记日至
		//			break;
		//		}
		//	}
		//}

	} while( false );

	MGTakeItemFromStorageError takeItemError;
	takeItemError.takeItemError.storageIndex = storageIndex;
	takeItemError.takeItemError.errNo = errNo;
	m_pAttachPlayer->SendPkg<MGTakeItemFromStorageError>( &takeItemError );
	
	return false;
}


bool CPlayerStorage::TakeItemFromStorageToPkg( unsigned int storageIndex )
{
	int errNo = 1;

	do 
	{
		if ( !m_pAttachPlayer )
			break;

		if ( storageIndex >= MAX_STORAGE_ITEMCOUNT )
		{
			D_ERROR("将物品从仓库取出时,玩家 %s 从错误的仓库索引位 %d 放入\n",m_pAttachPlayer->GetNickName(),storageIndex );
			break;
		}

		if( !CheckPlayerStorageIndexIsValid( storageIndex ) )
		{
			D_ERROR("TakeItemFromStorageToPkg 玩家 %s 取出的仓库道具的索引不合法 %d 当前的可用行数为 %d\n", m_pAttachPlayer->GetNickName() , storageIndex, m_ValidRowCount );
			break;
		}

		ItemInfo_i& storageItem = mStorageItemArr[storageIndex];
		if ( storageItem.uiID == 0 || storageItem.usItemTypeID == 0 )
		{
			D_ERROR("%s 取出的仓库位 %d 上的道具为空\n", m_pAttachPlayer->GetNickName(), storageIndex );
			break;
		}

		CItemPublicProp* pStroagePublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( storageItem.usItemTypeID );
		if ( !pStroagePublicProp )
		{
			D_ERROR("%s 从仓库中取出的物品 %d 在道具属性表中找不到\n", m_pAttachPlayer->GetNickName(), storageItem.usItemTypeID  );
			break;
		}

		unsigned int maxAccount =  pStroagePublicProp->m_itemMaxAmount;
		if ( maxAccount > 1 )//如果是可堆叠的物品
		{
			if( m_pAttachPlayer->CanGetItem( storageItem.usItemTypeID, storageItem.ucCount ) )//看是否有足够的容量放入物品
			{
				m_pAttachPlayer->OnNpcGiveItem( storageItem.usItemTypeID, storageItem.ucCount );//如果可行,放入物品
			}
			else//提示背包满
			{
				errNo = 5;
				break;
			}
		}
		else if( maxAccount == 1 )//如果是不可堆叠的物品,找背包空位,没有空位,失败
		{
			unsigned int taskID = 0;
			//是否为任务物品
			TaskProperty* pTaskProp = CTaskPropertyManagerSingle::instance()->GetSpecialPropertyT( storageItem.usItemTypeID );
			if ( pTaskProp )
				taskID = pTaskProp->TaskID;//获取任务编号

			int tmpErrno = 0;
			if( m_pAttachPlayer->GiveNewItemToPlayer( storageItem, taskID, tmpErrno ) == false )//给玩家出错
			{
				errNo = 5;
				break;
			}
		}

		CLogManager::DoWarehouseOperateItem(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WTT_DRAW, storageItem.uiID, storageItem.usItemTypeID, storageItem.ucCount);//记日至
		ClearStorageItem(storageItem.uiID);
		//storageItem.ucCount = storageItem.ucLevelUp = storageItem.uiID = storageItem.randSet/*usAddId*/ = storageItem.usItemTypeID = storageItem.usWearPoint = 0;
		NoticeStorageInfoChange( storageIndex );
		errNo = 0;
		break;

	} while( false );

	MGTakeItemFromStorageError takeItemError;
	takeItemError.takeItemError.storageIndex = storageIndex;
	takeItemError.takeItemError.errNo = errNo;
	m_pAttachPlayer->SendPkg<MGTakeItemFromStorageError>( &takeItemError );
	
	return false;
}


//将物品从仓库中取出到包裹中的指定位置
void CPlayerStorage::HandleTakeItemFromStorageIndexToPkgIndex(unsigned int storageIndex ,unsigned int pkgIndex )
{
	if ( !m_pAttachPlayer )
		return;

	//如果现在是锁定仓库的
	if( IsLockStorage() )
	{
		if( !CheckStoragePassForbiddenTime() )//是否过了解禁时间
		{
			MGTakeItemFromStorageError takeItemError;
			takeItemError.takeItemError.storageIndex = storageIndex;
			takeItemError.takeItemError.errNo = 6;//输入错误密码次数过多,仓库被锁定
			m_pAttachPlayer->SendPkg<MGTakeItemFromStorageError>( &takeItemError );
			return;
		}

		//弹出输入密码的框
		ShowTakeItemPassWord();

		m_takeItemWating.StartWaiting( storageIndex, pkgIndex, E_TAKEITEM_FROM_STORAGE_TO_PKGINDEX );

	}
	else//如果是非锁定的
	{
		TakeItemFromStorageToPkg(  storageIndex, pkgIndex );
	}
}

void CPlayerStorage::HandleTakeItemFromStorageToPkg(unsigned int storageIndex )
{
	if ( !m_pAttachPlayer )
		return;

	//如果现在是锁定仓库
	if ( IsLockStorage() )
	{
		if( !CheckStoragePassForbiddenTime() )//是否过了解禁时间
		{
			MGTakeItemFromStorageError takeItemError;
			takeItemError.takeItemError.storageIndex = storageIndex;
			takeItemError.takeItemError.errNo = 6;//输入错误密码次数过多,仓库被锁定
			m_pAttachPlayer->SendPkg<MGTakeItemFromStorageError>( &takeItemError );
			return;
		}

		ShowTakeItemPassWord();

		m_takeItemWating.StartWaiting( storageIndex, 0, E_TAKEITEM_FROM_STORAGE_TO_PKG_EMPTYINDEX );
	}
	else
	{
		TakeItemFromStorageToPkg( storageIndex );
	}
}

void CPlayerStorage::HandlePutItemToStorageFromPkg(unsigned int pkgIndex)
{
	if ( !m_pAttachPlayer )
		return;

	PutItemToStorageFromPkg(  pkgIndex );
}

void CPlayerStorage::HandlePutItemToStorageIndexFromPkgIndex(unsigned int storageIndex, unsigned int pkgIndex )
{
	if ( !m_pAttachPlayer )
		return;

	if( !CheckPlayerStorageIndexIsValid( storageIndex ) )//检验是否在合法的仓库索引范围
	{
		D_ERROR("玩家 %s 放入仓库的索引 %d 非法,玩家可用的行数为:%d \n", m_pAttachPlayer->GetNickName(), storageIndex , m_ValidRowCount  );
		return;
	}

	if ( IsLockStorage() )//如果锁定的仓库界面
	{
		if ( CheckPutItemNeedInputPsd(storageIndex) )//检测放入仓库的物品,是否和一个不位空的道具交换,如果是,则需要输入密码
		{
			//是否被解锁禁用
			if( !CheckStoragePassForbiddenTime() )//是否过了解禁时间
			{
				MGTakeItemFromStorageError takeItemError;
				takeItemError.takeItemError.storageIndex = storageIndex;
				takeItemError.takeItemError.errNo = 6;//输入错误密码次数过多,仓库被锁定
				m_pAttachPlayer->SendPkg<MGTakeItemFromStorageError>( &takeItemError );
				return;
			}	
			ShowTakeItemPassWord();

			m_takeItemWating.StartWaiting(  storageIndex, pkgIndex ,E_TAKEITEM_FROM_PKG_TO_STORAGEINDEX );
		}
		else//如果不需要输入密码
		{
			PutItemToStorageFromPkg( pkgIndex, storageIndex );
		}
	}
	else
	{
		PutItemToStorageFromPkg( pkgIndex , storageIndex );
	}
}


void CPlayerStorage::HandlePlayerChangeStorageLockState()
{
	if ( !m_pAttachPlayer )
		return;

	if ( !IsLockStorage() && CheckPassWordEmpty() )//如果仓库现在处于非锁定状态,则当要转换为锁定状态时,如果密码为空,要提示玩家设置密码
	{
		ShowFirstSetPassWordDialog();
		return;
	}
	
	if ( IsLockStorage() )//仓库为琐定的,则现在是unLock操作,则要有检查密码操作
	{
		if( !CheckStoragePassForbiddenTime() )//是否过了解禁时间
		{
			MGTakeItemFromStorageError takeItemError;
			takeItemError.takeItemError.storageIndex = 0;
			takeItemError.takeItemError.errNo = 6;//输入错误密码次数过多,仓库被锁定
			m_pAttachPlayer->SendPkg<MGTakeItemFromStorageError>( &takeItemError );

			CLogManager::DoWareHousePrivate(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WPT_UNLOCK, 0);//记日至
			return;
		}

		ShowChangeLockStoragePassWrod();
	}
	else
	{
		LockStoragePlate();//如果为非锁定,则变仓库界面为锁定
		CLogManager::DoWareHousePrivate(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WPT_LOCK, 1);//记日至
	}
}


void CPlayerStorage::HandleInputPsdToUnLockStorage( const char* psd )
{
	if ( !m_pAttachPlayer )
		return;

	if ( !psd )
		return;

	bool okFlag = false;
	if( CheckPassWordIsCorrect( psd ) )//如果输入的密码正确
	{
		if ( IsLockStorage() )//之前状态只能处于锁定状态,
		{
			UnLockStoragePlate();
			okFlag = true;
		}
		else
			D_ERROR("%s 的仓库处于非锁定状态,但接到了玩家解除锁定的密码消息\n",m_pAttachPlayer->GetNickName() );

		NoticeInputStoragePsdRst( 0 );
	}
	else
	{
		D_ERROR("%s 改变仓库密码验证失败 输入密码:%s 仓库实际密码:%s \n", m_pAttachPlayer->GetNickName() ,  psd , m_storagePassWord );

	}

	CLogManager::DoWareHousePrivate(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WPT_UNLOCK, okFlag ? 1 : 0);//记日至
}

void CPlayerStorage::NoticeInputStoragePsdRst( int reasonNo )
{
	if ( !m_pAttachPlayer )
		return;

	MGTakeItemCheckPsdError checkPsdError;
	checkPsdError.checkPsdError.errNo = reasonNo;
	m_pAttachPlayer->SendPkg<MGTakeItemCheckPsdError>( &checkPsdError );
}


void CPlayerStorage::HandlePlayerTakeItemPsdCheck( const char* psd )
{
	if ( !m_pAttachPlayer )
		return;

	if ( !IsLockStorage() )
	{
		D_ERROR("%s 之前不是处于锁定仓库状态,但接到了玩家取出物品,输入密码的消息\n", m_pAttachPlayer->GetNickName() );
		return;
	}

	if ( !m_takeItemWating.IsChecking() )
	{
		D_ERROR("%s 不在等待输入密码的过程中,却接到了输入密码取出物品消息\n", m_pAttachPlayer->GetNickName() );
		return;
	}

	if ( CheckPassWordIsCorrect(psd) )//检验密码是否正确,如果正确
	{
		unsigned int storageIndex = 0;
		unsigned int pkgIndex = 0;
		TAKEITEM_OPER_TYPE takeOp = E_TAKEITEM_OPER_MAX;
		m_takeItemWating.GetWaitingInfo( storageIndex, pkgIndex, takeOp );

		switch( takeOp )
		{
		case E_TAKEITEM_FROM_PKG_TO_STORAGEINDEX:
			{
				PutItemToStorageFromPkg( pkgIndex, storageIndex );
			}
			break;
		case E_TAKEITEM_FROM_STORAGE_TO_PKGINDEX:
			{
				TakeItemFromStorageToPkg( storageIndex, pkgIndex );
			}
			break;
		case E_TAKEITEM_FROM_STORAGE_TO_PKG_EMPTYINDEX:
			{
				TakeItemFromStorageToPkg( storageIndex );
			}
			break;
		default:
			break;
		}

		//解除锁定
		UnLockStoragePlate();

		//通知取出物品成功
		NoticeTakeItemCheckPsdRst( 0 );
	}
	else
	{
		D_ERROR("%s 取出物品密码验证失败 输入密码:%s 仓库实际密码:%s \n", m_pAttachPlayer->GetNickName() ,  psd , m_storagePassWord );
	}

	//m_takeItemWating.Reset();
}

void CPlayerStorage::HandlePlayerSetNewPassWord( const char* psd , STORAGE_SECURITY_STRATEGY securityMode )
{
	int errNo = 1;

	if ( !m_pAttachPlayer )
		return;

	do 
	{
		if ( !CheckStoragePassForbiddenTime() )
		{
			D_ERROR("玩家 %s 仓库被解锁锁定,但接到了设置密码的消息\n", m_pAttachPlayer->GetNickName() );
			break;
		}

		if( m_takeItemWating.IsChecking() )
		{
			D_ERROR("玩家 %s 还处于等待密码输入状态,接到了设置新密码的消息\n", m_pAttachPlayer->GetNickName()  );
			break;
		}

		if ( !psd )
		{
			D_ERROR("玩家 %s 设置密码,发来的密码为空\n", m_pAttachPlayer->GetNickName() );
			break;
		}

		if ( ( securityMode == E_UNSECURITY_MODE ) ||  ( securityMode == E_STARATEGY_MAX) )
		{
			D_ERROR("玩家 %s 设置密码,发来的安全模式 %d 为错误的\n", m_pAttachPlayer->GetNickName() , (unsigned int)securityMode );
			break;
		}

		if ( !CheckPassWordEmpty() )
		{
			D_ERROR("玩家%s的仓库密码不为空,但接到玩家设置密码的消息\n",m_pAttachPlayer->GetNickName() );
			break;
		}

		size_t newPsdSize = ACE_OS::strnlen( psd,MAX_STORAGE_PASSWORD_SIZE );
		if ( newPsdSize < 6 || newPsdSize > 16 )
		{
			errNo = 2;//新密码不符合规范
			break;
		}
		for ( size_t i = 0; i<newPsdSize; ++i)
		{
			if ( isdigit(psd[i]) == 0 )
			{
				errNo = 2;//新密码不符合规范
				break;
			}
		}
		if (errNo == 2)
		{
			break;
		}

		SafeStrCpy( m_storagePassWord , psd );
		//ACE_OS::strncpy( m_storagePassWord , psd , sizeof(m_storagePassWord) );
		D_DEBUG("玩家 %s 设置仓库密码成功 PSD:%s\n", m_pAttachPlayer->GetNickName() , m_storagePassWord );

		m_SecurityMode = securityMode;

		//设置密码后锁定托盘
		LockStoragePlate();

		//通知玩家的安全策略
		NoticeStorageSecurityStrategy();

		errNo = 0;

	} while( false );

	MGSetNewPassWordError psdError;
	psdError.setNewPsdError.errNo = errNo;
	m_pAttachPlayer->SendPkg<MGSetNewPassWordError>( &psdError );

	CLogManager::DoWareHousePrivate(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WPT_CHANGE_PASSWORD, (errNo == 0) ? true : false);//记日至
}


void CPlayerStorage::HandlePlayerResetPassWord(const char* pOldPsd, const char* pNewPsd , STORAGE_SECURITY_STRATEGY securityMode )
{
	int errNo = 0;
	if ( !m_pAttachPlayer )
		return;
	do 
	{
		if ( !CheckStoragePassForbiddenTime() )
		{
			D_ERROR("玩家 %s 仓库被解锁锁定,但接到了重置密码的消息\n", m_pAttachPlayer->GetNickName() );
			break;
		}

		if ( CheckPassWordEmpty() )
		{
			D_ERROR("玩家 %s 没有设置过密码,但接到了重设置密码消息\n", m_pAttachPlayer->GetNickName() );
			break;
		}

		if ( !pOldPsd )
		{
			D_ERROR("玩家 %s 重设置密码.旧的密码为空\n",m_pAttachPlayer->GetNickName() );
			break;
		}

		if ( !pNewPsd )
		{
			D_ERROR("玩家 %s 重设置密码.新的密码为空\n",m_pAttachPlayer->GetNickName() );
			break;
		}

		if ( ( securityMode == E_UNSECURITY_MODE ) ||  ( securityMode == E_STARATEGY_MAX) )
		{
			D_ERROR("玩家 %s 重设置密码,发来的安全模式 %d 为错误的\n", m_pAttachPlayer->GetNickName() , (unsigned int)securityMode );
			break;
		}

		//如果输入的旧的密码错误
		if ( ACE_OS::strncmp( m_storagePassWord,pOldPsd , sizeof(m_storagePassWord) ) != 0 )
		{
			D_ERROR("玩家 %s 重置密码失败,旧密码:%s 玩家输入的密码:%s \n",  m_pAttachPlayer->GetNickName() , m_storagePassWord ,pOldPsd  );
			errNo = 1;//原来的密码错误
			break;
		}

		size_t newPsdSize = ACE_OS::strnlen( pNewPsd,MAX_STORAGE_PASSWORD_SIZE );
		if ( newPsdSize < 6 || newPsdSize > 16 )
		{
			errNo = 2;//新密码不符合规范
			break;
		}
		for ( size_t i = 0; i<newPsdSize; ++i)
		{
			if ( isdigit(pNewPsd[i]) == 0 )
			{
				errNo = 2;//新密码不符合规范
				break;
			}
		}
		if (errNo!= 0)
		{
			break;
		}

		SafeStrCpy( m_storagePassWord , pNewPsd );
		//ACE_OS::strncpy( m_storagePassWord , pNewPsd , sizeof(m_storagePassWord) );
		D_DEBUG("玩家%s 重置密码成功 密码从　%s  变化为　%s \n",  m_pAttachPlayer->GetNickName() , pOldPsd , m_storagePassWord );
		m_SecurityMode = securityMode;

		//重新设置密码后,之前的密码取出物品重置
		m_takeItemWating.Reset();

		//重设置密码和安全模式后锁定托盘
		LockStoragePlate();

		//通知玩家的安全策略
		NoticeStorageSecurityStrategy();

		//如果有记录之前连续输入密码错误的次数
		if ( IsHaveForbiddenInfo() )
		{
			//重置仓库禁用时间
			ResetStorageForbiddenInfo();
			
			//更新DB保存的仓库禁用信息
			NoticeUpdateDBForbiddenInfo();
		}

	} while( false );

	MGResetPassWordError resetError;
	resetError.resetPsdError.errNo = errNo;
	m_pAttachPlayer->SendPkg<MGResetPassWordError>( &resetError );

	CLogManager::DoWareHousePrivate(m_pAttachPlayer, m_storageUID, LOG_SVC_NS::WPT_CHANGE_PASSWORD, (errNo == 0) ? true : false);//记日至
}

void CPlayerStorage::HandlePlayerExtendStorage()
{
	if ( !m_pAttachPlayer )
		return;

	int errNo = 0;

	do 
	{
		//如果还没有扩充完毕
		if( m_ValidRowCount < MAX_STORAGE_ROWCOUNT )
		{
			ExpandStorageInfo * pExpandStorageInfo = ExpandStorageInfoManagerSingle::instance()->GetExpandStorageInfoByCurRow( m_ValidRowCount );
			if ( pExpandStorageInfo )
			{
				if (   pExpandStorageInfo->curRow  == m_ValidRowCount
					&& pExpandStorageInfo->nextRow <= MAX_STORAGE_ROWCOUNT )
				{

					//1.验证金钱是否满足条件
					if ( m_pAttachPlayer->IsUseGoldMoney() )
					{
						//使用金币
						if( m_pAttachPlayer->GetGoldMoney() < pExpandStorageInfo->money )
						{
							errNo = 1;
							break;
						}
					} else {
						//使用银币
						if( m_pAttachPlayer->GetSilverMoney() < pExpandStorageInfo->money )
						{
							errNo = 1;
							break;
						}
					}

					//2.验证道具是否满足条件
					bool isCheckOK = true;
					const std::vector<ExpandCosumeItem>& consumeItemVec = pExpandStorageInfo->consumeItemVec;
					for ( unsigned int i = 0 ; i< consumeItemVec.size() && isCheckOK ; i++ )
					{
						const ExpandCosumeItem& consumeItem = consumeItemVec[i];
						if( !m_pAttachPlayer->ItemNumReachTargetNum( consumeItem.itemTypeID/10 , consumeItem.itemCount ) )
						{
							errNo = 1;
							isCheckOK = false;
						}
					}

					if (  !isCheckOK )
						break;

					//3.扣除玩家的金钱和道具
					m_pAttachPlayer->SubSilverMoney( pExpandStorageInfo->money );//双币种判断
					for ( unsigned int i = 0 ; i< consumeItemVec.size(); i++ )
					{
						const ExpandCosumeItem& consumeItem = consumeItemVec[i];
						m_pAttachPlayer->OnDropItemByConditionID( consumeItem.itemTypeID/10, consumeItem.itemCount );
					}
					
					//4.通知玩家可用的仓库行数更新
					m_ValidRowCount = pExpandStorageInfo->nextRow;
					NoticeStorageValidRowUpdate();
					CLogManager::DoWarehouseExtend(m_pAttachPlayer, m_storageUID, m_ValidRowCount);//记日至
				}
				else
				{
					errNo = 1;
				}
			}
			else
			{
				errNo = 2;
			}
		}

	} while( false );


	NoticeExpendStorageRst( errNo );
}


void CPlayerStorage::HandlePlayerSwapStorageItem(unsigned int fromStorageIndex, unsigned int toStorageIndex )
{
	if ( !m_pAttachPlayer )
		return;
	
	if ( !CheckPlayerStorageIndexIsValid( fromStorageIndex) )
	{
		D_ERROR("%s 交换仓库中的道具，取出位置不合法 %d \n" , m_pAttachPlayer->GetNickName(), fromStorageIndex );
		return;
	}

	if ( !CheckPlayerStorageIndexIsValid( toStorageIndex) )
	{
		D_ERROR("%s 交换仓库中的道具，放入位置不合法 %d \n", m_pAttachPlayer->GetNickName(), toStorageIndex );
		return;
	}

	if (fromStorageIndex >= ARRAY_SIZE(mStorageItemArr))
	{
		D_ERROR("CPlayerStorage::HandlePlayerSwapStorageItem, fromStorageIndex(%d) >= ARRAY_SIZE(mStorageItemArr)\n", fromStorageIndex );
		return;
	}
	if (toStorageIndex >= ARRAY_SIZE(mStorageItemArr))
	{
		D_ERROR("CPlayerStorage::HandlePlayerSwapStorageItem, toStorageIndex(%d) >= ARRAY_SIZE(mStorageItemArr)\n", toStorageIndex );
		return;
	}
	ItemInfo_i fromItem = mStorageItemArr[ fromStorageIndex ];
	ItemInfo_i toItem   = mStorageItemArr[ toStorageIndex   ];

	if ( fromItem.uiID == 0 && toItem.uiID == 0 )
	{
		D_ERROR("%s 交换仓库道具，被交换的位置都为空\n", m_pAttachPlayer->GetNickName() );
		return;
	}

	if ( fromItem.uiID != toItem.uiID )
	{
		unsigned int leftUpItemTo = 0, leftUpItemFrom = 0;
		GetItemInfoByStorageIndex(toStorageIndex,toItem,leftUpItemTo);
		GetItemInfoByStorageIndex(fromStorageIndex,fromItem,leftUpItemFrom);
		ClearStorageItem(toItem.uiID);
		ClearStorageItem(fromItem.uiID);
		if (IsFreeStorageIndex(toStorageIndex,CBaseItem::GetWidth(fromItem.usItemTypeID),CBaseItem::GetHeight(fromItem.usItemTypeID)))
		{
			SetStorageItem(fromItem,toStorageIndex);
			if ( PlayerInfo_3_Pkgs::INVALIDATE != toItem.uiID )
			{
				if (IsFreeStorageIndex(fromStorageIndex,CBaseItem::GetWidth(toItem.usItemTypeID),CBaseItem::GetHeight(toItem.usItemTypeID)))
				{
					SetStorageItem(toItem,fromStorageIndex);
				}
				else
				{
					ClearStorageItem(fromItem.uiID);
					SetStorageItem(fromItem,leftUpItemFrom);
					SetStorageItem(toItem,leftUpItemTo);
					return;
				}
			}
		}
		else
		{
			SetStorageItem(fromItem,leftUpItemFrom);
			SetStorageItem(toItem,leftUpItemTo);
			return;
		}
	}
	else
	{
		unsigned int leftUpItemFrom = 0;
		GetItemInfoByStorageIndex(fromStorageIndex,fromItem,leftUpItemFrom);
		ClearStorageItem(fromItem.uiID);
		if (IsFreeStorageIndex(toStorageIndex,CBaseItem::GetWidth(fromItem.usItemTypeID),CBaseItem::GetHeight(fromItem.usItemTypeID)))
		{
			SetStorageItem(fromItem,toStorageIndex);
		}
		else
		{
			SetStorageItem(fromItem,leftUpItemFrom);
			return;
		}
	}


	NoticeStorageInfoChange( fromStorageIndex );
	NoticeStorageInfoChange( toStorageIndex );
}

#define  STORAGE_LABLE_ITEM_COUNT (MAX_STORAGE_ITEMCOUNT /3)

void CPlayerStorage::HandleSortStorageLableItem( unsigned int lableIndex )
{
	return;
	if ( !m_pAttachPlayer )
		return;

	if( !CheckStorageLableIndexIsValid( lableIndex ) )
	{
		D_ERROR("%s 排序的仓库Label不合法 %d ,玩家可用行数为:%d \n",m_pAttachPlayer->GetNickName(), lableIndex ,GetStorageValidRow() );
		return;
	}

	ItemInfo_i*  pSortItemBase   = GetStorageItemArr();//获取仓库的道具指针
	unsigned int validItemCount  = GetValidStorageItemCount();//可用的道具格个数

	unsigned int sortStartPos = lableIndex   * STORAGE_LABLE_ITEM_COUNT;
	unsigned int sortEndPos   = sortStartPos + STORAGE_LABLE_ITEM_COUNT;
	if ( sortEndPos > validItemCount )//获取本次排序的范围
		sortEndPos = validItemCount;

	if ( sortEndPos <= sortStartPos )
	{
		D_ERROR("%s 排序仓库的范围不对,范围为 %d to %d \n",m_pAttachPlayer->GetNickName(),sortStartPos,  sortEndPos );
		return;
	}

	unsigned int sortCount = sortEndPos - sortStartPos;
	if ( sortCount > STORAGE_LABLE_ITEM_COUNT )
	{
		D_ERROR("%s 排序仓库的范围过大 %d\n", m_pAttachPlayer->GetNickName() , sortCount );
		return;
	}

	pSortItemBase += sortStartPos;
	ItemInfo_i  tmpPageItem[STORAGE_LABLE_ITEM_COUNT];
	StructMemCpy( tmpPageItem , pSortItemBase ,sizeof(ItemInfo_i) * sortCount  );

	bool isSwap = false;
	for ( int i = sortCount -1 ; i >= 1  ; i-- )
	{
		for (  int j = 0 ; j< i;  j++ )
		{
			ItemInfo_i& itemEle =   pSortItemBase[j];
			ItemInfo_i& itemEle2  = pSortItemBase[j+1];

			if( m_pAttachPlayer->CompareSortItem( itemEle , itemEle2 ) )
			{
				ItemInfo_i tmpItem = itemEle;
				itemEle  = itemEle2;
				itemEle2 = tmpItem;

				if( !isSwap )  
					isSwap = true;
			}
		}
	}

	if ( isSwap )
	{
		for ( unsigned int i = sortStartPos, j = 0 ; i < sortEndPos && j < sortCount && j < ARRAY_SIZE(tmpPageItem) ; i++ , j++ )
		{
			const ItemInfo_i& oldItem = tmpPageItem[j];
			const ItemInfo_i& newItem = pSortItemBase[j];
			if ( oldItem.uiID != newItem.uiID )
			{
				NoticeStorageInfoChange( i );
			}
		}
	}
}

bool CPlayerStorage::CheckPutItemNeedInputPsd( unsigned int storageIndex )
{
	if ( !m_pAttachPlayer )
		return false;

	if ( storageIndex >= MAX_STORAGE_ITEMCOUNT )
		return false;

	const ItemInfo_i& storageItem = 	mStorageItemArr[storageIndex];
	if ( storageItem.uiID == 0 || storageItem.usItemTypeID == 0 )
		return false;

	return true;
}

//如果检查密码错误
bool CPlayerStorage::CheckPassWordIsCorrect(const char* psd )
{
	if ( !m_pAttachPlayer )
		return false;

	if ( !psd )
		return false;

	if ( ACE_OS::strncmp( m_storagePassWord,psd, sizeof(m_storagePassWord)) == 0 )
	{
		//之前有输入密码错误的次数，证明之前连续输入密码出错过
		if ( IsHaveForbiddenInfo() )
		{
			//本次输入密码正确，重置仓库禁用信息
			ResetStorageForbiddenInfo();

			//通知DB更新
			NoticeUpdateDBForbiddenInfo();
		}

		return true;
	}
	else
	{
		if ( m_passErrCount < 5 )//如果输入密码错误的次数小于5次，则每次连续输入，错误次数+1
		{
			m_passErrCount++;
			if ( m_passErrCount ==  5 )//如果到了连续输入出错的最大次数，仓库解禁，设置仓库解禁时间
			{
				m_storageForbiddenTime = (unsigned int)time(NULL) + 3600;//这么多秒后解除锁定
			}

			//通知DB更新
			NoticeUpdateDBForbiddenInfo();

			//通知玩家输入密码错误的消息
			NoticeInputStoragePsdErr();
		}
	}

	return false;
}


bool CPlayerStorage::CheckStoragePassForbiddenTime()
{
	if ( GetStorageUnForbiddenTime() > 0 )
	{
		unsigned int curtime = (unsigned int)time(NULL);

		if(  curtime > m_storageForbiddenTime )//是否过了解禁时间
		{
			ResetStorageForbiddenInfo();

			NoticeUpdateDBForbiddenInfo();

			return true;
		}
		else
		{
			return false;
		}
	}

	return true;
}

void CPlayerStorage::ShowPassWordDialog( SHOW_STORAGE_DIALOG_TYPE showType)
{
	if ( !m_pAttachPlayer )
		return;

	if ( showType >= E_SHOW_TYPE_MAX )
		return;

	MGShowStoragePasswordDlg showStorageDlg;
	showStorageDlg.showPsdDlg.dlgType = showType;
	m_pAttachPlayer->SendPkg<MGShowStoragePasswordDlg>( &showStorageDlg );
}

//获得一个空的仓库位置
int CPlayerStorage::GetFreeStorageIndex(unsigned int width /* = 1 */, unsigned int height /* = 1 */)
{
	int freeIndex = -1;
	for (unsigned i = 0; i<GetValidStorageItemCount(); ++i)
	{
		if (PlayerInfo_2_Equips::INVALIDATE == mStorageItemArr[i].uiID)
		{
			if (i/STORAGE_COLUMN != (i+width-1)/STORAGE_COLUMN)
			{//道具宽度超出改点至背包最右边的宽度
				continue;
			}
			bool isFree = true;
			for (unsigned int w = 0; w<width; ++w)
			{
				for (unsigned int h = 0; h<height; ++h)
				{
					unsigned index = i+w+h*STORAGE_COLUMN;
					if ( 
						(i < STORAGE_NUM_PERPAGE && index >= STORAGE_NUM_PERPAGE)
						|| (i < STORAGE_NUM_PERPAGE*2 && index >= STORAGE_NUM_PERPAGE*2)  
						)
					{
						isFree = false;
						break;
					}
					if (index < GetValidStorageItemCount() && index < ARRAY_SIZE(mStorageItemArr) )
					{
						if (PlayerInfo_2_Equips::INVALIDATE != mStorageItemArr[index].uiID)
						{
							isFree = false;
							break;
						}
					}
					else
					{
						isFree = false;
						break;
					}
				}
				if (!isFree)
				{
					break;
				}
			}
			if (isFree)
			{
				freeIndex = i;
				break;
			}
		}
	}
	return freeIndex;
}

//清除仓库中该uid的道具
void CPlayerStorage::ClearStorageItem( unsigned int itemUID )
{
	if (PlayerInfo_3_Pkgs::INVALIDATE == itemUID)
	{
		return;
	}
	for ( int i = 0; i<ARRAY_SIZE(mStorageItemArr); ++i)
	{
		if (mStorageItemArr[i].uiID == itemUID)
		{
			ItemInfo_i& itemInfo = mStorageItemArr[i];

			itemInfo.uiID = PlayerInfo_3_Pkgs::INVALIDATE;
			itemInfo.usItemTypeID = 0;
			itemInfo.usWearPoint = 0;
			itemInfo.randSet/*usAddId*/ = 0;
			itemInfo.ucLevelUp = 0;
			itemInfo.ucCount = 0;
			NoticeStorageInfoChange(i,false);
		}
	}
}

//填充仓库中的道具
bool CPlayerStorage::SetStorageItem( const ItemInfo_i& itemInfo, unsigned int storageIndex )
{
	if (storageIndex >= GetValidStorageItemCount())
	{
		return false;
	}
	if ( PlayerInfo_3_Pkgs::INVALIDATE == itemInfo.uiID )
	{
		mStorageItemArr[storageIndex] = itemInfo;
		NoticeStorageInfoChange(storageIndex,false);
		return true;
	}
	unsigned int width = CBaseItem::GetWidth(itemInfo.usItemTypeID);
	unsigned int height = CBaseItem::GetHeight(itemInfo.usItemTypeID);
	for ( unsigned int w = 0; w < width; ++w )
	{
		for ( unsigned int h = 0; h < height; ++h )
		{
			unsigned int index = storageIndex + w + h*STORAGE_COLUMN;
			if (index < GetValidStorageItemCount() && index < ARRAY_SIZE(mStorageItemArr) )
			{
				mStorageItemArr[index] = itemInfo;
				NoticeStorageInfoChange(index,false);
			}
		}
	}
	return true;
}

//仓库位置是否空
bool CPlayerStorage::IsFreeStorageIndex(unsigned int storageIndex, unsigned int width /* = 1 */, unsigned int height /* = 1 */)
{
	if (storageIndex/STORAGE_COLUMN != (storageIndex+width-1)/STORAGE_COLUMN)
	{//道具宽度超出改点至仓库最右边的宽度
		return false;
	}
	for (unsigned int w = 0; w<width; ++w)
	{
		for (unsigned int h = 0; h<height; ++h)
		{
			unsigned index = storageIndex+w+h*STORAGE_COLUMN;
			if ( 
				(storageIndex < STORAGE_NUM_PERPAGE && index >= STORAGE_NUM_PERPAGE)
			 || (storageIndex < STORAGE_NUM_PERPAGE*2 && index >= STORAGE_NUM_PERPAGE*2)  
			   )
			{
				return false;
			}
			if (index < GetValidStorageItemCount() && index < ARRAY_SIZE(mStorageItemArr))
			{
				if (PlayerInfo_2_Equips::INVALIDATE != mStorageItemArr[index].uiID)
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}

//根据仓库index获得道具的索引
bool CPlayerStorage::GetItemInfoByStorageIndex( unsigned int storageIndex, ItemInfo_i& itemInfo_out, unsigned int& leftUpIndex_out )
{
	if (storageIndex >= GetValidStorageItemCount() || storageIndex >= ARRAY_SIZE(mStorageItemArr))
	{
		return false;
	}
	ItemInfo_i & itemInfo = mStorageItemArr[storageIndex];
	if (PlayerInfo_3_Pkgs::INVALIDATE == itemInfo.uiID)
	{
		itemInfo_out = itemInfo;
		leftUpIndex_out = storageIndex;
		return true;
	}
	else
	{//找实体模型的左上角
		int index = storageIndex;
		while (index - 1 >= 0)
		{
			index -= 1;
			if (mStorageItemArr[index].uiID != itemInfo.uiID)
			{
				index += 1;
				break;
			}
		}
		while (index - STORAGE_COLUMN >= 0)
		{
			index -= STORAGE_COLUMN;
			if (mStorageItemArr[index].uiID != itemInfo.uiID)
			{
				index += STORAGE_COLUMN;
				break;
			}
		}
		itemInfo_out = mStorageItemArr[index];
		leftUpIndex_out = index;
		return true;
	}
	return false;
}

//该道具位置是否位于左上角
bool CPlayerStorage::IsLeftUpStorageIndex(unsigned int storageIndex)
{
	if (storageIndex >= ARRAY_SIZE(mStorageItemArr))
	{
		D_ERROR("CPlayerStorage::IsLeftUpStorageIndex storageIndex(%d) >= ARRAY_SIZE(mStorageItemArr)\n",storageIndex);
		return false;
	}

	if ( PlayerInfo_3_Pkgs::INVALIDATE == mStorageItemArr[storageIndex].uiID )
	{
		return true;
	}

	if (storageIndex>=1)
	{
		if (mStorageItemArr[storageIndex-1].uiID == mStorageItemArr[storageIndex].uiID)
		{
			return false;
		}
	}
	if (storageIndex>=STORAGE_COLUMN)
	{
		if (mStorageItemArr[storageIndex-STORAGE_COLUMN].uiID == mStorageItemArr[storageIndex].uiID)
		{
			return false;
		}
	}
	return true;
}

bool CPlayerStorage::CheckPlayerStorageIndexIsValid(unsigned int storageIndex )
{
	if ( !m_pAttachPlayer )
		return false;

	if ( m_ValidRowCount >= MAX_STORAGE_ROWCOUNT || m_ValidRowCount < 2 )
		return false;
	
	return ( storageIndex < ( m_ValidRowCount * STORAGE_COLUMN ) );
}

#define LABLE_ROW_COUNT (MAX_STORAGE_ROWCOUNT / 3)


bool CPlayerStorage::CheckStorageLableIndexIsValid(unsigned int labelIndex )
{
	if ( !m_pAttachPlayer )
		return false;

	if (  labelIndex >= 3 )
		return false;

	unsigned int validRow = GetStorageValidRow();

	unsigned int validLable = 0;
	if ( validRow  < LABLE_ROW_COUNT * 1 )
	{
		validLable = 1;
	}
	else if ( validRow < LABLE_ROW_COUNT * 2 )
	{
		validLable = 2;
	}
	else if( validRow < LABLE_ROW_COUNT * 3 )
	{
		validLable = 3;
	}

	if ( labelIndex < validLable )
		return true;
	
	return false;
}
