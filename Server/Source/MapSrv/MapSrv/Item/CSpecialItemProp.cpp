﻿#include "CSpecialItemProp.h"
#include "CItemPropManager.h"
#include "CItemPropGenerator.h"
#include "../Player/Player.h"


#include "ItemLevelUpAttribute.h"
#include "ItemLeavelUpCri.h"

CBaseItem::~CBaseItem(){};

//获取套装ID号
int CBaseItem::GetSuitID(){ return -1; }

///玩家与Item对话，nOption:玩家所选择的选项，==0表示初始选择NPC；
bool CBaseItem::OnChatOption( CPlayer* pOwner/*道具使用者*/, int nOption )
{
	if ( NULL == m_ItemPublicProp )
	{
		//属性指针空；
		return false;
	}

	if ( NULL == m_ItemPublicProp->m_pItemScript )
	{
		//属性所挂脚本空；
		return false;
	}

	return m_ItemPublicProp->m_pItemScript->OnItemChat( pOwner, nOption );
}

bool CBaseItem::IsCanUse( CPlayer* pUsePlayer, int playerRace, int playerClass, int playerLevel )
{
	if ( NULL == pUsePlayer )
	{
		D_ERROR( "CBaseItem::IsCanUse, NULL == pUsePlayer\n" );
		return false;
	}

	//判断现在的种族，看是否能使用
	bool tmpbool = false;
	unsigned int race  = 1u << playerRace;
	//bool tmpbool = ACE_BIT_ENABLED( m_ItemPublicProp->mCanUseRace, race );//2010。5。13 ,以后没有种族的概念，不需要判断了
	//if( tmpbool )
	{
		//如果种族能使用，那么判断职业是否能使用
		unsigned int canClass = 1u << playerClass;
		tmpbool = ACE_BIT_ENABLED( m_ItemPublicProp->mCanUseClass, canClass );
		if ( !tmpbool )
		{
			pUsePlayer->SendSystemChat( "职业不符合要求，不能使用物品" );
			D_DEBUG( "%s职业不符合要求，不能使用物品%d(UID:%d)\n", pUsePlayer->GetAccount(), GetItemID(), GetItemUID() );
			return false;
		}
		//如果都可用,判断是否达到了使用的等级
		if ( (unsigned int)playerLevel < GetItemPlayerLevel() )
		{
			pUsePlayer->SendSystemChat( "等级不符合要求,不能使用物品" );
			D_DEBUG( "%s等级不符合要求,不能使用物品%d(UID:%d)\n", pUsePlayer->GetAccount(), GetItemID(), GetItemUID() );
			return false;
		}
		return IsCanUseByProperty(pUsePlayer);
	}
	return false;
}

bool CBaseItem::IsCanUseByProperty( CPlayer* pUsePlayer )
{
	if ( NULL == pUsePlayer )
	{
		D_ERROR( "CBaseItem::IsCanUseByProperty, NULL == pUsePlayer\n" );
		return false;
	}
	if ( pUsePlayer->GetStrength() < GetEquipNeedStr() )
	{
		pUsePlayer->SendSystemChat( "力量不符合要求,不能使用物品" );
		D_DEBUG( "%s力量%d不符合要求%d,不能使用物品%d(UID:%d)\n"
			, pUsePlayer->GetAccount(), pUsePlayer->GetStrength(), GetEquipNeedStr(), GetItemID(), GetItemUID() );
		return false;
	}
	if ( pUsePlayer->GetAgility() < GetEquipNeedAgi() )
	{
		pUsePlayer->SendSystemChat( "敏捷不符合要求,不能使用物品" );
		D_DEBUG( "%s敏捷%d不符合要求%d,不能使用物品%d(UID:%d)\n"
			, pUsePlayer->GetAccount(), pUsePlayer->GetAgility(), GetEquipNeedAgi(), GetItemID(), GetItemUID() );
		return false;
	}
	if ( pUsePlayer->GetIntelligence() < GetEquipNeedInt() )
	{
		pUsePlayer->SendSystemChat( "智力不符合要求,不能使用物品" );
		D_DEBUG( "%s智力%d不符合要求%d,不能使用物品%d(UID:%d)\n"
			, pUsePlayer->GetAccount(), pUsePlayer->GetIntelligence(), GetEquipNeedInt(), GetItemID(), GetItemUID() );
		return false;
	}
	if (  pUsePlayer->GetSpirit() < GetEquipNeedSpi() )
	{
		pUsePlayer->SendSystemChat( "精神不符合要求,不能使用物品" );
		D_DEBUG( "%s精神%d不符合要求%d,不能使用物品%d(UID:%d)\n"
			, pUsePlayer->GetAccount(), pUsePlayer->GetSpirit(), GetEquipNeedSpi(), GetItemID(), GetItemUID() );
		return false;
	}
	if ( pUsePlayer->GetVitality() < GetEquipNeedVit() )
	{
		pUsePlayer->SendSystemChat( "体质不符合要求,不能使用物品" );
		D_DEBUG( "%s体质%d不符合要求%d,不能使用物品%d(UID:%d)\n"
			, pUsePlayer->GetAccount(), pUsePlayer->GetVitality(), GetEquipNeedVit(), GetItemID(), GetItemUID() );
		return false;
	}
	return true;
}

char CBaseItem::GetItemAuctionType()
{
	ITEM_TYPEID itemTypeID = GetItemTypeID();
	if ( itemTypeID == E_TYPE_WEAPON )//如果是武器
		return 1;

	if ( itemTypeID == E_TYPE_EQUIP )//如果是装备
	{
		CEquipItemEx* pEquipItem = (CEquipItemEx *)this;
		ITEM_GEARARM_TYPE gearArmType = pEquipItem->GetGearArmType();
		if ( gearArmType == E_HEAD )
		{
			return 2;
		}

		if ( gearArmType == E_SHOULDER )
		{
			return 3;
		}

		if ( gearArmType == E_BODY )
		{
			return 4;
		}

		if ( gearArmType == E_HAND )
		{
			return 5;
		}

		if ( gearArmType == E_FOOT )
		{
			return 6;
		}

		if ( //gearArmType  == E_EAR
			gearArmType == E_NECKLACE
			//||gearArmType == E_BRACELET
			||gearArmType == E_FINGER)
		{
			return 7;
		}

		if ( gearArmType == E_BACK )
		{
			return 9;
		}
	}

	if ( itemTypeID == E_TYPE_MOUNT )
	{
		return 8;
	}

	if ( itemTypeID == E_TYPE_SKILL 
		|| itemTypeID == E_BULEPRINT 
		|| itemTypeID == E_FUNCTION )
	{
		if ( itemTypeID == E_FUNCTION )
		{
			CFunctionItem* pFuncItem = (CFunctionItem *)this;
			const FunctionProperty* pFunItemProp = pFuncItem->GetFunctionProperty();
			if ( pFunItemProp )
			{
				if( pFunItemProp->Type ==  FunctionProperty::E_FUNC_SKILLBOOK
					|| pFunItemProp->Type == FunctionProperty::E_FUNC_EXPIRE_BOOK )//如果是经验书或者是技能书
					return 10;
			}	
		}
		else
		{
			return 10;
		}
	}

	if ( itemTypeID == E_TYPE_AXIOLOGY )//如果是价值物
	{
		CAxiologyItem* pAxiologyItem =  (CAxiologyItem *)this;
		const AxiologyProperty* pAxiologyProp = pAxiologyItem->GetAxiologyItemProperty();
		if ( pAxiologyProp )
		{
			if ( (int)pAxiologyProp->type != -1
				 && (int)pAxiologyProp->type != 0  )
			{
				return 11;
			}
		}
	}

	return 12;
}

#define REAL_MODEL

//获得道具实体模型的宽度
int CBaseItem::GetWidth()
{
#ifdef REAL_MODEL
	if (m_ItemPublicProp)
	{
		return m_ItemPublicProp->m_width;
	}
#endif
	return 1;
}

//获得道具实体模型的高度
int CBaseItem::GetHeight()
{
#ifdef REAL_MODEL
	if (m_ItemPublicProp)
	{
		return m_ItemPublicProp->m_height;
	}
#endif
	return 1;
}

int CBaseItem::GetWidth(int m_itemTypeID)
{
#ifdef REAL_MODEL
	CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( m_itemTypeID );
	if (pPublicProp)
	{
		return pPublicProp->m_width;
	}
#endif
	if (PlayerInfo_3_Pkgs::INVALIDATE == m_itemTypeID )
	{
		return 0;
	}
	return 1;
}

int CBaseItem::GetHeight(int m_itemTypeID)
{
#ifdef REAL_MODEL
	CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( m_itemTypeID );
	if (pPublicProp)
	{
		return pPublicProp->m_height;
	}
#endif
	if (PlayerInfo_3_Pkgs::INVALIDATE == m_itemTypeID )
	{
		return 0;
	}
	return 1;
}

//置武器升级属性;
bool CWeaponItemEx::SetLevelUpProp()
{
	if ( NULL == mpWeaponImpl )
	{
		return false;
	}
	unsigned int levelUpID  = mpWeaponImpl->brand * 100u + GetItemLevel();//GET_ITEM_LEVELUP( itemInfo.ucLevelUp );
	m_pLevelProp = ItemLevelUpAttributeManager::GetLevelUpAttribute(  levelUpID );
	return true;
}

unsigned short CWeaponItemEx::LevelUpEquipNeedStr() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipStrAdd:0 ; }//升级后增加的装配对基本属性点要求
unsigned short CWeaponItemEx::LevelUpEquipNeedAgi() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipAgiAdd:0 ; }//升级后增加的装配对基本属性点要求
unsigned short CWeaponItemEx::LevelUpEquipNeedInt() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipIntAdd:0 ; }//升级后增加的装配对基本属性点要求
unsigned short CWeaponItemEx::LevelUpEquipNeedVit() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipVitAdd:0 ; }//升级后增加的装配对基本属性点要求
unsigned short CWeaponItemEx::LevelUpEquipNeedSpi() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipSpiAdd:0 ; }//升级后增加的装配对基本属性点要求

//置武器升级属性;
bool CEquipItemEx::SetLevelUpProp()
{
	if ( NULL == mpEquipImpl )
	{
		return false;
	}
	unsigned int levelUpID  = mpEquipImpl->brand * 100u + GetItemLevel();//GET_ITEM_LEVELUP( itemInfo.ucLevelUp );
	m_pLevelProp = ItemLevelUpAttributeManager::GetLevelUpAttribute( levelUpID );
	return true;
}

unsigned short CEquipItemEx::LevelUpEquipNeedStr() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipStrAdd:0 ; }//升级后增加的装配对基本属性点要求
unsigned short CEquipItemEx::LevelUpEquipNeedAgi() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipAgiAdd:0 ; }//升级后增加的装配对基本属性点要求
unsigned short CEquipItemEx::LevelUpEquipNeedInt() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipIntAdd:0 ; }//升级后增加的装配对基本属性点要求
unsigned short CEquipItemEx::LevelUpEquipNeedVit() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipVitAdd:0 ; }//升级后增加的装配对基本属性点要求
unsigned short CEquipItemEx::LevelUpEquipNeedSpi() { return (NULL != m_pLevelProp) ? m_pLevelProp->equipSpiAdd:0 ; }//升级后增加的装配对基本属性点要求


void CWeaponItemEx::AddItemPropToPlayer(CPlayer* pPlayer,const ItemInfo_i& itemInfo )
{
	if ( !mpWeaponImpl )
		return;

	if ( !pPlayer )
		return;


	ItemAddtionProp& m_PlayerItemProp = pPlayer->GetItemAddtionProp();
	//记录装备的速度属性
	unsigned int itemAddSpeed = (unsigned int)mpWeaponImpl->AddSpeed;

	m_PlayerItemProp.Antistun += mpWeaponImpl->Antistun;
	m_PlayerItemProp.Antitie  += mpWeaponImpl->Antitie;
	m_PlayerItemProp.AttackMagMax += mpWeaponImpl->AttackMagMax;
	m_PlayerItemProp.AttackMagMin += mpWeaponImpl->AttackMagMin;
	m_PlayerItemProp.AttackPhyMax += mpWeaponImpl->AttackPhyMax;
	m_PlayerItemProp.AttackPhyMin += mpWeaponImpl->AttackPhyMin;
	m_PlayerItemProp.Hp += mpWeaponImpl->Hp;
	m_PlayerItemProp.Mp += mpWeaponImpl->Mp;
	m_PlayerItemProp.MagicAttack += mpWeaponImpl->MagicAttack;
	m_PlayerItemProp.MagicAttackAdd += mpWeaponImpl->MagicAttackAdd;
	m_PlayerItemProp.MagicAttackPercent += mpWeaponImpl->MagicAttackPercent;
	m_PlayerItemProp.MagicCritical += mpWeaponImpl->MagicCritical;
	m_PlayerItemProp.MagicCriticalDamage += mpWeaponImpl->MagicCriticalDamage;
	m_PlayerItemProp.MagicCriticalDamageDerate += mpWeaponImpl->MagicCriticalDamageDerate;
	m_PlayerItemProp.MagicDefend += mpWeaponImpl->MagicDefend;
	m_PlayerItemProp.MagicDefendPercent +=mpWeaponImpl->MagicDefendPercent;
	m_PlayerItemProp.MagicDeratePercent +=mpWeaponImpl->MagicDeratePercent;
	m_PlayerItemProp.MagicHit += mpWeaponImpl->MagicHit;
	m_PlayerItemProp.MagicJouk += mpWeaponImpl->MagicJouk;
	m_PlayerItemProp.MagicRift += mpWeaponImpl->MagicRift;
	m_PlayerItemProp.PhysicsAttack += mpWeaponImpl->PhysicsAttack;
	m_PlayerItemProp.PhysicsAttackAdd += mpWeaponImpl->PhysicsAttackAdd;
	m_PlayerItemProp.PhysicsAttackPercent += mpWeaponImpl->PhysicsAttackPercent;
	m_PlayerItemProp.PhysicsCritical += mpWeaponImpl->PhysicsCritical;
	m_PlayerItemProp.PhysicsCriticalDamage += mpWeaponImpl->PhysicsCriticalDamage;
	m_PlayerItemProp.PhysicsCriticalDamageDerate += mpWeaponImpl->PhysicsCriticalDamageDerate;
	m_PlayerItemProp.PhysicsDefend += mpWeaponImpl->PhysicsDefend;
	m_PlayerItemProp.PhysicsDefendPercent += mpWeaponImpl->PhysicsDefendPercent;
	m_PlayerItemProp.PhysicsDeratePercent += mpWeaponImpl->PhysicsDeratePercent;
	m_PlayerItemProp.PhysicsHit += mpWeaponImpl->PhysicsHit;
	m_PlayerItemProp.PhysicsJouk += mpWeaponImpl->PhysicsJouk;
	m_PlayerItemProp.PhysicsRift += mpWeaponImpl->PhysicsRift;
	m_PlayerItemProp.Stun += mpWeaponImpl->Stun;
	m_PlayerItemProp.Tie += mpWeaponImpl->Tie;
	m_PlayerItemProp.excellentbiff += mpWeaponImpl->ExcellentBiff;
	m_PlayerItemProp.luckybiff += mpWeaponImpl->LuckyBiff;
	////道具对5个人物基本属性的添加体现在幸运卓越属性中，而幸运卓越属性由随机种子算得，by dzj, 10.05.25, m_PlayerItemProp.agility += mpWeaponImpl->agility;
	//m_PlayerItemProp.strength += mpWeaponImpl->strength;
	//m_PlayerItemProp.spirit += mpWeaponImpl->spirit;
	//m_PlayerItemProp.intelligence += mpWeaponImpl->intelligence;
	//m_PlayerItemProp.physique += mpWeaponImpl->physique;

	//ChangeProp* pChangeProp = ItemChangeAttributeManager::GetChangeItemAttribute( itemInfo.usAddId );
	//if ( pChangeProp != NULL )
	//{
	//	//追加增加道具的速度属性
	//	itemAddSpeed += (unsigned int)pChangeProp->AddSpeed;

	//	m_PlayerItemProp.Antistun += pChangeProp->Antistun;
	//	m_PlayerItemProp.Antitie  += pChangeProp->Antitie;
	//	m_PlayerItemProp.AttackMagMax += pChangeProp->AttackMagMax;
	//	m_PlayerItemProp.AttackMagMin += pChangeProp->AttackMagMin;
	//	m_PlayerItemProp.AttackPhyMax += pChangeProp->AttackPhyMax;
	//	m_PlayerItemProp.AttackPhyMin += pChangeProp->AttackPhyMin;
	//	m_PlayerItemProp.Hp += pChangeProp->Hp;
	//	m_PlayerItemProp.MagicAttack += pChangeProp->MagicAttack;
	//	m_PlayerItemProp.MagicAttackAdd += pChangeProp->MagicAttackAdd;
	//	m_PlayerItemProp.MagicAttackPercent += pChangeProp->MagicAttackPercent;
	//	m_PlayerItemProp.MagicCritical += pChangeProp->MagicCritical;
	//	m_PlayerItemProp.MagicCriticalDamage  += pChangeProp->MagicCriticalDamage;
	//	m_PlayerItemProp.MagicCriticalDamageDerate += pChangeProp->MagicCriticalDamageDerate;
	//	m_PlayerItemProp.MagicDefend += pChangeProp->MagicDefend;
	//	m_PlayerItemProp.MagicDefendPercent +=pChangeProp->MagicDefendPercent;
	//	m_PlayerItemProp.MagicDeratePercent +=pChangeProp->MagicDeratePercent;
	//	m_PlayerItemProp.MagicHit += pChangeProp->MagicHit;
	//	m_PlayerItemProp.MagicJouk += pChangeProp->MagicJouk;
	//	m_PlayerItemProp.MagicRift += pChangeProp->MagicRift;
	//	m_PlayerItemProp.PhysicsAttack += pChangeProp->PhysicsAttack;
	//	m_PlayerItemProp.PhysicsAttackAdd += pChangeProp->PhysicsAttackAdd;
	//	m_PlayerItemProp.PhysicsAttackPercent += pChangeProp->PhysicsAttackPercent;
	//	m_PlayerItemProp.PhysicsCritical += pChangeProp->PhysicsCritical;
	//	m_PlayerItemProp.PhysicsCriticalDamage += pChangeProp->PhysicsCriticalDamage;
	//	m_PlayerItemProp.PhysicsCriticalDamageDerate += pChangeProp->PhysicsCriticalDamageDerate;
	//	m_PlayerItemProp.PhysicsDefend += pChangeProp->PhysicsDefend;
	//	m_PlayerItemProp.PhysicsDefendPercent += pChangeProp->PhysicsDefendPercent;
	//	m_PlayerItemProp.PhysicsDeratePercent += pChangeProp->PhysicsDeratePercent;
	//	m_PlayerItemProp.PhysicsHit += pChangeProp->PhysicsHit;
	//	m_PlayerItemProp.PhysicsJouk += pChangeProp->PhysicsJouk;
	//	m_PlayerItemProp.PhysicsRift += pChangeProp->PhysicsRift;
	//	m_PlayerItemProp.Stun += pChangeProp->Stun;
	//	m_PlayerItemProp.Tie += pChangeProp->Tie;
	//}

	if ( NULL == m_pLevelProp )
	{
		D_ERROR( "CWeaponItemEx::AddItemPropToPlayer, NULL == m_pLevelProp, itemxmlID:%d, brand:%d, level:%d\n"
			, GetItemID(), mpWeaponImpl->brand, GetItemLevel() );
		return;
	}

	//unsigned int levelUpID  = mpWeaponImpl->brand * 100u + GET_ITEM_LEVELUP( itemInfo.ucLevelUp );
	//LevelUpProp* pLevelProp = ItemLevelUpAttributeManager::GetLevelUpAttribute(  levelUpID );
	//if ( pLevelProp )
	//{
		unsigned int Modulus = CItemLevelUpModulus::GetLevelUpModulus( GetItemPlayerLevel() );

		m_PlayerItemProp.AttackMagMax  += ( m_pLevelProp->AttackMagMax * Modulus );
		m_PlayerItemProp.AttackMagMin  += ( m_pLevelProp->AttackMagMin * Modulus );
		m_PlayerItemProp.AttackPhyMax  += ( m_pLevelProp->AttackPhyMax * Modulus );
		m_PlayerItemProp.AttackPhyMin  += ( m_pLevelProp->AttackPhyMin * Modulus );
		m_PlayerItemProp.PhysicsAttack += ( m_pLevelProp->PhysicsAttack * Modulus );
		m_PlayerItemProp.MagicAttack   += ( m_pLevelProp->MagicAttack * Modulus );
		m_PlayerItemProp.PhysicsDefend += ( m_pLevelProp->PhysicsDefend * Modulus );
		m_PlayerItemProp.MagicDefend   += ( m_pLevelProp->MagicDefend * Modulus );

		//升级影响玩家的速度属性
		itemAddSpeed += m_pLevelProp->speed * Modulus;
	//}

	pPlayer->EquipItemChangeSpeed( itemAddSpeed );
}

void CWeaponItemEx::SubItemPropToPlayer(CPlayer* pPlayer,const ItemInfo_i& itemInfo )
{
	if ( !mpWeaponImpl )
		return;

	if ( !pPlayer )
		return;


	ItemAddtionProp& m_PlayerItemProp = pPlayer->GetItemAddtionProp();
	//记录装备的初始速度
	unsigned int subItemSpeed = mpWeaponImpl->AddSpeed;
	m_PlayerItemProp.Antistun -= mpWeaponImpl->Antistun;
	m_PlayerItemProp.Antitie  -= mpWeaponImpl->Antitie;
	m_PlayerItemProp.AttackMagMax -= mpWeaponImpl->AttackMagMax;
	m_PlayerItemProp.AttackMagMin -= mpWeaponImpl->AttackMagMin;
	m_PlayerItemProp.AttackPhyMax -= mpWeaponImpl->AttackPhyMax;
	m_PlayerItemProp.AttackPhyMin -= mpWeaponImpl->AttackPhyMin;
	m_PlayerItemProp.Hp -= mpWeaponImpl->Hp;
	m_PlayerItemProp.Mp -= mpWeaponImpl->Mp;
	m_PlayerItemProp.MagicAttack -= mpWeaponImpl->MagicAttack;
	m_PlayerItemProp.MagicAttackAdd -= mpWeaponImpl->MagicAttackAdd;
	m_PlayerItemProp.MagicAttackPercent -= mpWeaponImpl->MagicAttackPercent;
	m_PlayerItemProp.MagicCritical -= mpWeaponImpl->MagicCritical;
	m_PlayerItemProp.MagicCriticalDamage -= mpWeaponImpl->MagicCriticalDamage;
	m_PlayerItemProp.MagicCriticalDamageDerate -= mpWeaponImpl->MagicCriticalDamageDerate;
	m_PlayerItemProp.MagicDefend -= mpWeaponImpl->MagicDefend;
	m_PlayerItemProp.MagicDefendPercent -=mpWeaponImpl->MagicDefendPercent;
	m_PlayerItemProp.MagicDeratePercent -=mpWeaponImpl->MagicDeratePercent;
	m_PlayerItemProp.MagicHit -= mpWeaponImpl->MagicHit;
	m_PlayerItemProp.MagicJouk -= mpWeaponImpl->MagicJouk;
	m_PlayerItemProp.MagicRift -= mpWeaponImpl->MagicRift;
	m_PlayerItemProp.PhysicsAttack -= mpWeaponImpl->PhysicsAttack;
	m_PlayerItemProp.PhysicsAttackAdd -= mpWeaponImpl->PhysicsAttackAdd;
	m_PlayerItemProp.PhysicsAttackPercent -= mpWeaponImpl->PhysicsAttackPercent;
	m_PlayerItemProp.PhysicsCritical -= mpWeaponImpl->PhysicsCritical;
	m_PlayerItemProp.PhysicsCriticalDamage -= mpWeaponImpl->PhysicsCriticalDamage;
	m_PlayerItemProp.PhysicsCriticalDamageDerate -= mpWeaponImpl->PhysicsCriticalDamageDerate;
	m_PlayerItemProp.PhysicsDefend -= mpWeaponImpl->PhysicsDefend;
	m_PlayerItemProp.PhysicsDefendPercent -= mpWeaponImpl->PhysicsDefendPercent;
	m_PlayerItemProp.PhysicsDeratePercent -= mpWeaponImpl->PhysicsDeratePercent;
	m_PlayerItemProp.PhysicsHit -= mpWeaponImpl->PhysicsHit;
	m_PlayerItemProp.PhysicsJouk -= mpWeaponImpl->PhysicsJouk;
	m_PlayerItemProp.PhysicsRift -= mpWeaponImpl->PhysicsRift;
	m_PlayerItemProp.Stun -= mpWeaponImpl->Stun;
	m_PlayerItemProp.Tie -= mpWeaponImpl->Tie;
	m_PlayerItemProp.excellentbiff -= mpWeaponImpl->ExcellentBiff;
	m_PlayerItemProp.luckybiff -= mpWeaponImpl->LuckyBiff;
	////道具对5个人物基本属性的添加体现在幸运卓越属性中，而幸运卓越属性由随机种子算得，by dzj, 10.05.25, m_PlayerItemProp.agility -= mpWeaponImpl->agility;
	//m_PlayerItemProp.strength -= mpWeaponImpl->strength;
	//m_PlayerItemProp.spirit -= mpWeaponImpl->spirit;
	//m_PlayerItemProp.intelligence -= mpWeaponImpl->intelligence;
	//m_PlayerItemProp.physique -= mpWeaponImpl->physique;

	//ChangeProp* pChangeProp = ItemChangeAttributeManager::GetChangeItemAttribute( itemInfo.usAddId );
	//if ( pChangeProp != NULL )
	//{
	//	//累计玩家追加的速度属性
	//	subItemSpeed += (unsigned int)pChangeProp->AddSpeed;

	//	m_PlayerItemProp.Antistun -= pChangeProp->Antistun;
	//	m_PlayerItemProp.Antitie  -= pChangeProp->Antitie;
	//	m_PlayerItemProp.AttackMagMax -= pChangeProp->AttackMagMax;
	//	m_PlayerItemProp.AttackMagMin -= pChangeProp->AttackMagMin;
	//	m_PlayerItemProp.AttackPhyMax -= pChangeProp->AttackPhyMax;
	//	m_PlayerItemProp.AttackPhyMin -= pChangeProp->AttackPhyMin;
	//	m_PlayerItemProp.Hp -= pChangeProp->Hp;
	//	m_PlayerItemProp.MagicAttack -= pChangeProp->MagicAttack;
	//	m_PlayerItemProp.MagicAttackAdd -= pChangeProp->MagicAttackAdd;
	//	m_PlayerItemProp.MagicAttackPercent -= pChangeProp->MagicAttackPercent;
	//	m_PlayerItemProp.MagicCritical -= pChangeProp->MagicCritical;
	//	m_PlayerItemProp.MagicCriticalDamage  -= pChangeProp->MagicCriticalDamage;
	//	m_PlayerItemProp.MagicCriticalDamageDerate -= pChangeProp->MagicCriticalDamageDerate;
	//	m_PlayerItemProp.MagicDefend -= pChangeProp->MagicDefend;
	//	m_PlayerItemProp.MagicDefendPercent -=pChangeProp->MagicDefendPercent;
	//	m_PlayerItemProp.MagicDeratePercent -=pChangeProp->MagicDeratePercent;
	//	m_PlayerItemProp.MagicHit -= pChangeProp->MagicHit;
	//	m_PlayerItemProp.MagicJouk -= pChangeProp->MagicJouk;
	//	m_PlayerItemProp.MagicRift -= pChangeProp->MagicRift;
	//	m_PlayerItemProp.PhysicsAttack -= pChangeProp->PhysicsAttack;
	//	m_PlayerItemProp.PhysicsAttackAdd -= pChangeProp->PhysicsAttackAdd;
	//	m_PlayerItemProp.PhysicsAttackPercent -= pChangeProp->PhysicsAttackPercent;
	//	m_PlayerItemProp.PhysicsCritical -= pChangeProp->PhysicsCritical;
	//	m_PlayerItemProp.PhysicsCriticalDamage -= pChangeProp->PhysicsCriticalDamage;
	//	m_PlayerItemProp.PhysicsCriticalDamageDerate -= pChangeProp->PhysicsCriticalDamageDerate;
	//	m_PlayerItemProp.PhysicsDefend -= pChangeProp->PhysicsDefend;
	//	m_PlayerItemProp.PhysicsDefendPercent -= pChangeProp->PhysicsDefendPercent;
	//	m_PlayerItemProp.PhysicsDeratePercent -= pChangeProp->PhysicsDeratePercent;
	//	m_PlayerItemProp.PhysicsHit -= pChangeProp->PhysicsHit;
	//	m_PlayerItemProp.PhysicsJouk -= pChangeProp->PhysicsJouk;
	//	m_PlayerItemProp.PhysicsRift -= pChangeProp->PhysicsRift;
	//	m_PlayerItemProp.Stun -= pChangeProp->Stun;
	//	m_PlayerItemProp.Tie -= pChangeProp->Tie;
	//}

	if ( NULL == m_pLevelProp )
	{
		D_ERROR( "CWeaponItemEx::SubItemPropToPlayer, NULL == m_pLevelProp, itemxmlID:%d, brand:%d, level:%d\n"
			, GetItemID(), mpWeaponImpl->brand, GetItemLevel() );
		return;
	}

	//unsigned int levelUpID = mpWeaponImpl->brand * 100u + GET_ITEM_LEVELUP( itemInfo.ucLevelUp );
	//LevelUpProp* pLevelProp = ItemLevelUpAttributeManager::GetLevelUpAttribute(  levelUpID  );
	//if ( pLevelProp )
	//{
		unsigned int Modulus = CItemLevelUpModulus::GetLevelUpModulus( GetItemPlayerLevel() );

		m_PlayerItemProp.AttackMagMax  -= ( m_pLevelProp->AttackMagMax * Modulus );
		m_PlayerItemProp.AttackMagMin  -= ( m_pLevelProp->AttackMagMin * Modulus );
		m_PlayerItemProp.AttackPhyMax  -= ( m_pLevelProp->AttackPhyMax * Modulus );
		m_PlayerItemProp.AttackPhyMin  -= ( m_pLevelProp->AttackPhyMin * Modulus );
		m_PlayerItemProp.PhysicsAttack -= ( m_pLevelProp->PhysicsAttack * Modulus );
		m_PlayerItemProp.MagicAttack   -= ( m_pLevelProp->MagicAttack * Modulus );
		m_PlayerItemProp.PhysicsDefend -= ( m_pLevelProp->PhysicsDefend * Modulus );
		m_PlayerItemProp.MagicDefend   -= ( m_pLevelProp->MagicDefend * Modulus );

		//累计玩家升级后的速度属性
		subItemSpeed += ( m_pLevelProp->speed * Modulus );
	//}

	//如果移除的道具的速度大于或等于玩家的最大装备速度,则需要重新选出装备的最大速度
	if ( subItemSpeed >= (unsigned int)pPlayer->GetEquipMaxSpeed() )
	{
		int maxspeed = pPlayer->GetEquipedItemMaxSpeed( itemInfo.uiID );
		pPlayer->SetEquipMaxSpeed( maxspeed );
	}
}

void CEquipItemEx::AddItemPropToPlayer( CPlayer* pPlayer,const ItemInfo_i& itemInfo  )
{
	if ( !mpEquipImpl )
		return; 

	if ( !pPlayer )
		return;

	ItemAddtionProp& m_PlayerItemProp = pPlayer->GetItemAddtionProp();
	unsigned int addItemSpeed = 0;
	addItemSpeed = mpEquipImpl->AddSpeed;
	m_PlayerItemProp.Antistun += mpEquipImpl->Antistun;
	m_PlayerItemProp.Antitie  += mpEquipImpl->Antitie;
	m_PlayerItemProp.Hp += mpEquipImpl->Hp;
	m_PlayerItemProp.Mp += mpEquipImpl->Mp;
	m_PlayerItemProp.MagicAttack += mpEquipImpl->MagicAttack;
	m_PlayerItemProp.MagicAttackAdd += mpEquipImpl->MagicAttackAdd;
	m_PlayerItemProp.MagicAttackPercent += mpEquipImpl->MagicAttackPercent;
	m_PlayerItemProp.MagicCritical += mpEquipImpl->MagicCritical;
	m_PlayerItemProp.MagicCriticalDamage += mpEquipImpl->MagicCriticalDamage;
	m_PlayerItemProp.MagicCriticalDamageDerate += mpEquipImpl->MagicCriticalDamageDerate;
	m_PlayerItemProp.MagicDefend += mpEquipImpl->MagicDefend;
	m_PlayerItemProp.MagicDefendPercent +=mpEquipImpl->MagicDefendPercent;
	m_PlayerItemProp.MagicDeratePercent +=mpEquipImpl->MagicDeratePercent;
	m_PlayerItemProp.MagicHit += mpEquipImpl->MagicHit;
	m_PlayerItemProp.MagicJouk += mpEquipImpl->MagicJouk;
	m_PlayerItemProp.MagicRift += mpEquipImpl->MagicRift;
	m_PlayerItemProp.PhysicsAttack += mpEquipImpl->PhysicsAttack;
	m_PlayerItemProp.PhysicsAttackAdd += mpEquipImpl->PhysicsAttackAdd;
	m_PlayerItemProp.PhysicsAttackPercent += mpEquipImpl->PhysicsAttackPercent;
	m_PlayerItemProp.PhysicsCritical += mpEquipImpl->PhysicsCritical;
	m_PlayerItemProp.PhysicsCriticalDamage += mpEquipImpl->PhysicsCriticalDamage;
	m_PlayerItemProp.PhysicsCriticalDamageDerate += mpEquipImpl->PhysicsCriticalDamageDerate;
	m_PlayerItemProp.PhysicsDefend += mpEquipImpl->PhysicsDefend;
	m_PlayerItemProp.PhysicsDefendPercent += mpEquipImpl->PhysicsDefendPercent;
	m_PlayerItemProp.PhysicsDeratePercent += mpEquipImpl->PhysicsDeratePercent;
	m_PlayerItemProp.PhysicsHit += mpEquipImpl->PhysicsHit;
	m_PlayerItemProp.PhysicsJouk += mpEquipImpl->PhysicsJouk;
	m_PlayerItemProp.PhysicsRift += mpEquipImpl->PhysicsRift;
	m_PlayerItemProp.Stun += mpEquipImpl->Stun;
	m_PlayerItemProp.Tie += mpEquipImpl->Tie;
	m_PlayerItemProp.MagicCriticalDerate += mpEquipImpl->MagicCriticalDerate;
	m_PlayerItemProp.PhysicsCriticalDerate += mpEquipImpl->PhysicsCriticalDerate;
	m_PlayerItemProp.excellentbiff += mpEquipImpl->ExcellentBiff;
	m_PlayerItemProp.luckybiff += mpEquipImpl->LuckyBiff;
	////道具对5个人物基本属性的添加体现在幸运卓越属性中，而幸运卓越属性由随机种子算得，by dzj, 10.05.25, m_PlayerItemProp.agility += mpEquipImpl->agility;
	//m_PlayerItemProp.strength += mpEquipImpl->strength;
	//m_PlayerItemProp.spirit += mpEquipImpl->spirit;
	//m_PlayerItemProp.intelligence += mpEquipImpl->intelligence;
	//m_PlayerItemProp.physique += mpEquipImpl->physique;

	/*ChangeProp* pChangeProp = ItemChangeAttributeManager::GetChangeItemAttribute( itemInfo.usAddId );
	if ( pChangeProp != NULL )
	{
		addItemSpeed += pChangeProp->AddSpeed;

		m_PlayerItemProp.Antistun += pChangeProp->Antistun;
		m_PlayerItemProp.Antitie  += pChangeProp->Antitie;
		m_PlayerItemProp.AttackMagMax += pChangeProp->AttackMagMax;
		m_PlayerItemProp.AttackMagMin += pChangeProp->AttackMagMin;
		m_PlayerItemProp.AttackPhyMax += pChangeProp->AttackPhyMax;
		m_PlayerItemProp.AttackPhyMin += pChangeProp->AttackPhyMin;
		m_PlayerItemProp.Hp += pChangeProp->Hp;
		m_PlayerItemProp.MagicAttack += pChangeProp->MagicAttack;
		m_PlayerItemProp.MagicAttackAdd += pChangeProp->MagicAttackAdd;
		m_PlayerItemProp.MagicAttackPercent += pChangeProp->MagicAttackPercent;
		m_PlayerItemProp.MagicCritical += pChangeProp->MagicCritical;
		m_PlayerItemProp.MagicCriticalDamage  += pChangeProp->MagicCriticalDamage;
		m_PlayerItemProp.MagicCriticalDamageDerate += pChangeProp->MagicCriticalDamageDerate;
		m_PlayerItemProp.MagicDefend += pChangeProp->MagicDefend;
		m_PlayerItemProp.MagicDefendPercent +=pChangeProp->MagicDefendPercent;
		m_PlayerItemProp.MagicDeratePercent +=pChangeProp->MagicDeratePercent;
		m_PlayerItemProp.MagicHit += pChangeProp->MagicHit;
		m_PlayerItemProp.MagicJouk += pChangeProp->MagicJouk;
		m_PlayerItemProp.MagicRift += pChangeProp->MagicRift;
		m_PlayerItemProp.PhysicsAttack += pChangeProp->PhysicsAttack;
		m_PlayerItemProp.PhysicsAttackAdd += pChangeProp->PhysicsAttackAdd;
		m_PlayerItemProp.PhysicsAttackPercent += pChangeProp->PhysicsAttackPercent;
		m_PlayerItemProp.PhysicsCritical += pChangeProp->PhysicsCritical;
		m_PlayerItemProp.PhysicsCriticalDamage += pChangeProp->PhysicsCriticalDamage;
		m_PlayerItemProp.PhysicsCriticalDamageDerate += pChangeProp->PhysicsCriticalDamageDerate;
		m_PlayerItemProp.PhysicsDefend += pChangeProp->PhysicsDefend;
		m_PlayerItemProp.PhysicsDefendPercent += pChangeProp->PhysicsDefendPercent;
		m_PlayerItemProp.PhysicsDeratePercent += pChangeProp->PhysicsDeratePercent;
		m_PlayerItemProp.PhysicsHit += pChangeProp->PhysicsHit;
		m_PlayerItemProp.PhysicsJouk += pChangeProp->PhysicsJouk;
		m_PlayerItemProp.PhysicsRift += pChangeProp->PhysicsRift;
		m_PlayerItemProp.Stun += pChangeProp->Stun;
		m_PlayerItemProp.Tie += pChangeProp->Tie;
	}*/

	if ( NULL == m_pLevelProp )
	{
		D_ERROR( "CEquipItemEx::AddItemPropToPlayer, NULL == m_pLevelProp, itemxmlID:%d, brand:%d, level:%d\n"
			, GetItemID(), mpEquipImpl->brand, GetItemLevel() );
		return;
	}

	//unsigned int levelUpID = mpEquipImpl->brand*100u + GET_ITEM_LEVELUP( itemInfo.ucLevelUp );
	//LevelUpProp* pLevelProp = ItemLevelUpAttributeManager::GetLevelUpAttribute(  levelUpID );
	//if ( pLevelProp )
	//{
		unsigned int Modulus = CItemLevelUpModulus::GetLevelUpModulus( GetItemPlayerLevel() );

		m_PlayerItemProp.AttackMagMax  += ( m_pLevelProp->AttackMagMax * Modulus );
		m_PlayerItemProp.AttackMagMin  += ( m_pLevelProp->AttackMagMin * Modulus );
		m_PlayerItemProp.AttackPhyMax  += ( m_pLevelProp->AttackPhyMax * Modulus );
		m_PlayerItemProp.AttackPhyMin  += ( m_pLevelProp->AttackPhyMin * Modulus );
		m_PlayerItemProp.PhysicsAttack += ( m_pLevelProp->PhysicsAttack * Modulus );
		m_PlayerItemProp.MagicAttack   += ( m_pLevelProp->MagicAttack * Modulus );
		m_PlayerItemProp.PhysicsDefend += ( m_pLevelProp->PhysicsDefend * Modulus );
		m_PlayerItemProp.MagicDefend   += ( m_pLevelProp->MagicDefend * Modulus );
	
		addItemSpeed += ( m_pLevelProp->speed * Modulus );
	//}

	pPlayer->EquipItemChangeSpeed( addItemSpeed );
}

void CEquipItemEx::SubItemPropToPlayer(CPlayer* pPlayer,const ItemInfo_i& itemInfo )
{
	if ( !mpEquipImpl )
		return;

	if ( !pPlayer)
		return;

	ItemAddtionProp& m_PlayerItemProp = pPlayer->GetItemAddtionProp();
	unsigned int subItemSpeed = 0;
	subItemSpeed = (unsigned int)mpEquipImpl->AddSpeed;
	m_PlayerItemProp.Antistun -= mpEquipImpl->Antistun;
	m_PlayerItemProp.Antitie  -= mpEquipImpl->Antitie;
	m_PlayerItemProp.Hp -= mpEquipImpl->Hp;
	m_PlayerItemProp.Mp -= mpEquipImpl->Mp;
	m_PlayerItemProp.MagicAttack -= mpEquipImpl->MagicAttack;
	m_PlayerItemProp.MagicAttackAdd -= mpEquipImpl->MagicAttackAdd;
	m_PlayerItemProp.MagicAttackPercent -= mpEquipImpl->MagicAttackPercent;
	m_PlayerItemProp.MagicCritical -= mpEquipImpl->MagicCritical;
	m_PlayerItemProp.MagicCriticalDamage -= mpEquipImpl->MagicCriticalDamage;
	m_PlayerItemProp.MagicCriticalDamageDerate -= mpEquipImpl->MagicCriticalDamageDerate;
	m_PlayerItemProp.MagicDefend -= mpEquipImpl->MagicDefend;
	m_PlayerItemProp.MagicDefendPercent -=mpEquipImpl->MagicDefendPercent;
	m_PlayerItemProp.MagicDeratePercent -=mpEquipImpl->MagicDeratePercent;
	m_PlayerItemProp.MagicHit -= mpEquipImpl->MagicHit;
	m_PlayerItemProp.MagicJouk -= mpEquipImpl->MagicJouk;
	m_PlayerItemProp.MagicRift -= mpEquipImpl->MagicRift;
	m_PlayerItemProp.PhysicsAttack -= mpEquipImpl->PhysicsAttack;
	m_PlayerItemProp.PhysicsAttackAdd -= mpEquipImpl->PhysicsAttackAdd;
	m_PlayerItemProp.PhysicsAttackPercent -= mpEquipImpl->PhysicsAttackPercent;
	m_PlayerItemProp.PhysicsCritical -= mpEquipImpl->PhysicsCritical;
	m_PlayerItemProp.PhysicsCriticalDamage -= mpEquipImpl->PhysicsCriticalDamage;
	m_PlayerItemProp.PhysicsCriticalDamageDerate -= mpEquipImpl->PhysicsCriticalDamageDerate;
	m_PlayerItemProp.PhysicsDefend -= mpEquipImpl->PhysicsDefend;
	m_PlayerItemProp.PhysicsDefendPercent -= mpEquipImpl->PhysicsDefendPercent;
	m_PlayerItemProp.PhysicsDeratePercent -= mpEquipImpl->PhysicsDeratePercent;
	m_PlayerItemProp.PhysicsHit -= mpEquipImpl->PhysicsHit;
	m_PlayerItemProp.PhysicsJouk -= mpEquipImpl->PhysicsJouk;
	m_PlayerItemProp.PhysicsRift -= mpEquipImpl->PhysicsRift;
	m_PlayerItemProp.Stun -= mpEquipImpl->Stun;
	m_PlayerItemProp.Tie -= mpEquipImpl->Tie;
	m_PlayerItemProp.MagicCriticalDerate -= mpEquipImpl->MagicCriticalDerate;
	m_PlayerItemProp.PhysicsCriticalDerate -= mpEquipImpl->PhysicsCriticalDerate;
	m_PlayerItemProp.excellentbiff -= mpEquipImpl->ExcellentBiff;
	m_PlayerItemProp.luckybiff -= mpEquipImpl->LuckyBiff;
	////道具对5个人物基本属性的添加体现在幸运卓越属性中，而幸运卓越属性由随机种子算得，by dzj, 10.05.25, m_PlayerItemProp.agility -= mpEquipImpl->agility;
	//m_PlayerItemProp.strength -= mpEquipImpl->strength;
	//m_PlayerItemProp.spirit -= mpEquipImpl->spirit;
	//m_PlayerItemProp.intelligence -= mpEquipImpl->intelligence;
	//m_PlayerItemProp.physique -= mpEquipImpl->physique;

	//ChangeProp* pChangeProp = ItemChangeAttributeManager::GetChangeItemAttribute( itemInfo.usAddId );
	//if ( pChangeProp != NULL )
	//{
	//	subItemSpeed += (unsigned int)pChangeProp->AddSpeed;

	//	m_PlayerItemProp.Antistun -= pChangeProp->Antistun;
	//	m_PlayerItemProp.Antitie  -= pChangeProp->Antitie;
	//	m_PlayerItemProp.AttackMagMax -= pChangeProp->AttackMagMax;
	//	m_PlayerItemProp.AttackMagMin -= pChangeProp->AttackMagMin;
	//	m_PlayerItemProp.AttackPhyMax -= pChangeProp->AttackPhyMax;
	//	m_PlayerItemProp.AttackPhyMin -= pChangeProp->AttackPhyMin;
	//	m_PlayerItemProp.Hp -= pChangeProp->Hp;
	//	m_PlayerItemProp.MagicAttack -= pChangeProp->MagicAttack;
	//	m_PlayerItemProp.MagicAttackAdd -= pChangeProp->MagicAttackAdd;
	//	m_PlayerItemProp.MagicAttackPercent -= pChangeProp->MagicAttackPercent;
	//	m_PlayerItemProp.MagicCritical -= pChangeProp->MagicCritical;
	//	m_PlayerItemProp.MagicCriticalDamage  -= pChangeProp->MagicCriticalDamage;
	//	m_PlayerItemProp.MagicCriticalDamageDerate -= pChangeProp->MagicCriticalDamageDerate;
	//	m_PlayerItemProp.MagicDefend -= pChangeProp->MagicDefend;
	//	m_PlayerItemProp.MagicDefendPercent -=pChangeProp->MagicDefendPercent;
	//	m_PlayerItemProp.MagicDeratePercent -=pChangeProp->MagicDeratePercent;
	//	m_PlayerItemProp.MagicHit -= pChangeProp->MagicHit;
	//	m_PlayerItemProp.MagicJouk -= pChangeProp->MagicJouk;
	//	m_PlayerItemProp.MagicRift -= pChangeProp->MagicRift;
	//	m_PlayerItemProp.PhysicsAttack -= pChangeProp->PhysicsAttack;
	//	m_PlayerItemProp.PhysicsAttackAdd -= pChangeProp->PhysicsAttackAdd;
	//	m_PlayerItemProp.PhysicsAttackPercent -= pChangeProp->PhysicsAttackPercent;
	//	m_PlayerItemProp.PhysicsCritical -= pChangeProp->PhysicsCritical;
	//	m_PlayerItemProp.PhysicsCriticalDamage -= pChangeProp->PhysicsCriticalDamage;
	//	m_PlayerItemProp.PhysicsCriticalDamageDerate -= pChangeProp->PhysicsCriticalDamageDerate;
	//	m_PlayerItemProp.PhysicsDefend -= pChangeProp->PhysicsDefend;
	//	m_PlayerItemProp.PhysicsDefendPercent -= pChangeProp->PhysicsDefendPercent;
	//	m_PlayerItemProp.PhysicsDeratePercent -= pChangeProp->PhysicsDeratePercent;
	//	m_PlayerItemProp.PhysicsHit -= pChangeProp->PhysicsHit;
	//	m_PlayerItemProp.PhysicsJouk -= pChangeProp->PhysicsJouk;
	//	m_PlayerItemProp.PhysicsRift -= pChangeProp->PhysicsRift;
	//	m_PlayerItemProp.Stun -= pChangeProp->Stun;
	//	m_PlayerItemProp.Tie -= pChangeProp->Tie;
	//}

	if ( NULL == m_pLevelProp )
	{
		D_ERROR( "CEquipItemEx::SubItemPropToPlayer, NULL == m_pLevelProp, itemxmlID:%d, brand:%d, level:%d\n"
			, GetItemID(), mpEquipImpl->brand, GetItemLevel() );
		return;
	}

	//unsigned int levelUpID =  mpEquipImpl->brand*100u + GET_ITEM_LEVELUP( itemInfo.ucLevelUp );
	//LevelUpProp* pLevelProp = ItemLevelUpAttributeManager::GetLevelUpAttribute(  levelUpID );
	//if ( pLevelProp )
	//{
		unsigned int Modulus = CItemLevelUpModulus::GetLevelUpModulus( GetItemPlayerLevel() );

		m_PlayerItemProp.AttackMagMax  -= ( m_pLevelProp->AttackMagMax * Modulus );
		m_PlayerItemProp.AttackMagMin  -= ( m_pLevelProp->AttackMagMin * Modulus );
		m_PlayerItemProp.AttackPhyMax  -= ( m_pLevelProp->AttackPhyMax * Modulus );
		m_PlayerItemProp.AttackPhyMin  -= ( m_pLevelProp->AttackPhyMin * Modulus );
		m_PlayerItemProp.PhysicsAttack -= ( m_pLevelProp->PhysicsAttack * Modulus );
		m_PlayerItemProp.MagicAttack   -= ( m_pLevelProp->MagicAttack * Modulus );
		m_PlayerItemProp.PhysicsDefend -= ( m_pLevelProp->PhysicsDefend * Modulus );
		m_PlayerItemProp.MagicDefend   -= ( m_pLevelProp->MagicDefend * Modulus );

		subItemSpeed += (unsigned int)( m_pLevelProp->speed * Modulus );
	//}

	//如果移除的道具的速度大于或等于玩家的最大装备速度,则需要重新选出装备的最大速度
	if (  subItemSpeed >= (unsigned int)pPlayer->GetEquipMaxSpeed() )
	{
		int maxspeed = pPlayer->GetEquipedItemMaxSpeed( itemInfo.uiID );
		pPlayer->SetEquipMaxSpeed( maxspeed );
	}
}

void CMountItem::AddItemPropToPlayer(CPlayer *pPlayer, const MUX_PROTO::ItemInfo_i &itemInfo)
{
	if ( !mMountPropImpl )
		return; 

	if ( !pPlayer )
		return;

	ItemAddtionProp& m_PlayerItemProp = pPlayer->GetItemAddtionProp();
	m_PlayerItemProp.Antistun += mMountPropImpl->Antistun;
	m_PlayerItemProp.Antitie  += mMountPropImpl->Antitie;
	m_PlayerItemProp.Hp += mMountPropImpl->Hp;
	m_PlayerItemProp.Mp += mMountPropImpl->Mp;
	m_PlayerItemProp.MagicAttack += mMountPropImpl->MagicAttack;
	m_PlayerItemProp.MagicAttackAdd += mMountPropImpl->MagicAttackAdd;
	m_PlayerItemProp.MagicAttackPercent += mMountPropImpl->MagicAttackPercent;
	m_PlayerItemProp.MagicCritical += mMountPropImpl->MagicCritical;
	m_PlayerItemProp.MagicCriticalDamage += mMountPropImpl->MagicCriticalDamage;
	m_PlayerItemProp.MagicCriticalDamageDerate += mMountPropImpl->MagicCriticalDamageDerate;
	m_PlayerItemProp.MagicDefend += mMountPropImpl->MagicDefend;
	m_PlayerItemProp.MagicDefendPercent +=mMountPropImpl->MagicDefendPercent;
	m_PlayerItemProp.MagicDeratePercent +=mMountPropImpl->MagicDeratePercent;
	m_PlayerItemProp.MagicHit += mMountPropImpl->MagicHit;
	m_PlayerItemProp.MagicJouk += mMountPropImpl->MagicJouk;
	m_PlayerItemProp.MagicRift += mMountPropImpl->MagicRift;
	m_PlayerItemProp.PhysicsAttack += mMountPropImpl->PhysicsAttack;
	m_PlayerItemProp.PhysicsAttackAdd += mMountPropImpl->PhysicsAttackAdd;
	m_PlayerItemProp.PhysicsAttackPercent += mMountPropImpl->PhysicsAttackPercent;
	m_PlayerItemProp.PhysicsCritical += mMountPropImpl->PhysicsCritical;
	m_PlayerItemProp.PhysicsCriticalDamage += mMountPropImpl->PhysicsCriticalDamage;
	m_PlayerItemProp.PhysicsCriticalDamageDerate += mMountPropImpl->PhysicsCriticalDamageDerate;
	m_PlayerItemProp.PhysicsDefend += mMountPropImpl->PhysicsDefend;
	m_PlayerItemProp.PhysicsDefendPercent += mMountPropImpl->PhysicsDefendPercent;
	m_PlayerItemProp.PhysicsDeratePercent += mMountPropImpl->PhysicsDeratePercent;
	m_PlayerItemProp.PhysicsHit += mMountPropImpl->PhysicsHit;
	m_PlayerItemProp.PhysicsJouk += mMountPropImpl->PhysicsJouk;
	m_PlayerItemProp.PhysicsRift += mMountPropImpl->PhysicsRift;
	m_PlayerItemProp.Stun += mMountPropImpl->Stun;
	m_PlayerItemProp.Tie += mMountPropImpl->Tie;
	m_PlayerItemProp.MagicCriticalDerate += mMountPropImpl->MagicCriticalDerate;
	m_PlayerItemProp.PhysicsCriticalDerate += mMountPropImpl->PhysicsCriticalDerate;

	/*ChangeProp* pChangeProp = ItemChangeAttributeManager::GetChangeItemAttribute( itemInfo.usAddId );
	if ( pChangeProp != NULL )
	{
		m_PlayerItemProp.Antistun += pChangeProp->Antistun;
		m_PlayerItemProp.Antitie  += pChangeProp->Antitie;
		m_PlayerItemProp.AttackMagMax += pChangeProp->AttackMagMax;
		m_PlayerItemProp.AttackMagMin += pChangeProp->AttackMagMin;
		m_PlayerItemProp.AttackPhyMax += pChangeProp->AttackPhyMax;
		m_PlayerItemProp.AttackPhyMin += pChangeProp->AttackPhyMin;
		m_PlayerItemProp.Hp += pChangeProp->Hp;
		m_PlayerItemProp.MagicAttack += pChangeProp->MagicAttack;
		m_PlayerItemProp.MagicAttackAdd += pChangeProp->MagicAttackAdd;
		m_PlayerItemProp.MagicAttackPercent += pChangeProp->MagicAttackPercent;
		m_PlayerItemProp.MagicCritical += pChangeProp->MagicCritical;
		m_PlayerItemProp.MagicCriticalDamage  += pChangeProp->MagicCriticalDamage;
		m_PlayerItemProp.MagicCriticalDamageDerate += pChangeProp->MagicCriticalDamageDerate;
		m_PlayerItemProp.MagicDefend += pChangeProp->MagicDefend;
		m_PlayerItemProp.MagicDefendPercent +=pChangeProp->MagicDefendPercent;
		m_PlayerItemProp.MagicDeratePercent +=pChangeProp->MagicDeratePercent;
		m_PlayerItemProp.MagicHit += pChangeProp->MagicHit;
		m_PlayerItemProp.MagicJouk += pChangeProp->MagicJouk;
		m_PlayerItemProp.MagicRift += pChangeProp->MagicRift;
		m_PlayerItemProp.PhysicsAttack += pChangeProp->PhysicsAttack;
		m_PlayerItemProp.PhysicsAttackAdd += pChangeProp->PhysicsAttackAdd;
		m_PlayerItemProp.PhysicsAttackPercent += pChangeProp->PhysicsAttackPercent;
		m_PlayerItemProp.PhysicsCritical += pChangeProp->PhysicsCritical;
		m_PlayerItemProp.PhysicsCriticalDamage += pChangeProp->PhysicsCriticalDamage;
		m_PlayerItemProp.PhysicsCriticalDamageDerate += pChangeProp->PhysicsCriticalDamageDerate;
		m_PlayerItemProp.PhysicsDefend += pChangeProp->PhysicsDefend;
		m_PlayerItemProp.PhysicsDefendPercent += pChangeProp->PhysicsDefendPercent;
		m_PlayerItemProp.PhysicsDeratePercent += pChangeProp->PhysicsDeratePercent;
		m_PlayerItemProp.PhysicsHit += pChangeProp->PhysicsHit;
		m_PlayerItemProp.PhysicsJouk += pChangeProp->PhysicsJouk;
		m_PlayerItemProp.PhysicsRift += pChangeProp->PhysicsRift;
		m_PlayerItemProp.Stun += pChangeProp->Stun;
		m_PlayerItemProp.Tie += pChangeProp->Tie;
	}*/
}

void CMountItem::SubItemPropToPlayer(CPlayer* pPlayer,const ItemInfo_i& itemInfo )
{
	if ( !mMountPropImpl )
		return; 

	if ( !pPlayer )
		return;

	ItemAddtionProp& m_PlayerItemProp = pPlayer->GetItemAddtionProp();
	m_PlayerItemProp.Antistun -= mMountPropImpl->Antistun;
	m_PlayerItemProp.Antitie  -= mMountPropImpl->Antitie;
	m_PlayerItemProp.Hp -= mMountPropImpl->Hp;
	m_PlayerItemProp.Mp -= mMountPropImpl->Mp;
	m_PlayerItemProp.MagicAttack -= mMountPropImpl->MagicAttack;
	m_PlayerItemProp.MagicAttackAdd -= mMountPropImpl->MagicAttackAdd;
	m_PlayerItemProp.MagicAttackPercent -= mMountPropImpl->MagicAttackPercent;
	m_PlayerItemProp.MagicCritical -= mMountPropImpl->MagicCritical;
	m_PlayerItemProp.MagicCriticalDamage -= mMountPropImpl->MagicCriticalDamage;
	m_PlayerItemProp.MagicCriticalDamageDerate -= mMountPropImpl->MagicCriticalDamageDerate;
	m_PlayerItemProp.MagicDefend -= mMountPropImpl->MagicDefend;
	m_PlayerItemProp.MagicDefendPercent -=mMountPropImpl->MagicDefendPercent;
	m_PlayerItemProp.MagicDeratePercent -=mMountPropImpl->MagicDeratePercent;
	m_PlayerItemProp.MagicHit -= mMountPropImpl->MagicHit;
	m_PlayerItemProp.MagicJouk -= mMountPropImpl->MagicJouk;
	m_PlayerItemProp.MagicRift -= mMountPropImpl->MagicRift;
	m_PlayerItemProp.PhysicsAttack -= mMountPropImpl->PhysicsAttack;
	m_PlayerItemProp.PhysicsAttackAdd -= mMountPropImpl->PhysicsAttackAdd;
	m_PlayerItemProp.PhysicsAttackPercent -= mMountPropImpl->PhysicsAttackPercent;
	m_PlayerItemProp.PhysicsCritical -= mMountPropImpl->PhysicsCritical;
	m_PlayerItemProp.PhysicsCriticalDamage -= mMountPropImpl->PhysicsCriticalDamage;
	m_PlayerItemProp.PhysicsCriticalDamageDerate -= mMountPropImpl->PhysicsCriticalDamageDerate;
	m_PlayerItemProp.PhysicsDefend -= mMountPropImpl->PhysicsDefend;
	m_PlayerItemProp.PhysicsDefendPercent -= mMountPropImpl->PhysicsDefendPercent;
	m_PlayerItemProp.PhysicsDeratePercent -= mMountPropImpl->PhysicsDeratePercent;
	m_PlayerItemProp.PhysicsHit -= mMountPropImpl->PhysicsHit;
	m_PlayerItemProp.PhysicsJouk -= mMountPropImpl->PhysicsJouk;
	m_PlayerItemProp.PhysicsRift -= mMountPropImpl->PhysicsRift;
	m_PlayerItemProp.Stun -= mMountPropImpl->Stun;
	m_PlayerItemProp.Tie -= mMountPropImpl->Tie;
	m_PlayerItemProp.MagicCriticalDerate -= mMountPropImpl->MagicCriticalDerate;
	m_PlayerItemProp.PhysicsCriticalDerate -= mMountPropImpl->PhysicsCriticalDerate;

	/*ChangeProp* pChangeProp = ItemChangeAttributeManager::GetChangeItemAttribute( itemInfo.usAddId );
	if ( pChangeProp != NULL )
	{
		m_PlayerItemProp.Antistun -= pChangeProp->Antistun;
		m_PlayerItemProp.Antitie  -= pChangeProp->Antitie;
		m_PlayerItemProp.AttackMagMax -= pChangeProp->AttackMagMax;
		m_PlayerItemProp.AttackMagMin -= pChangeProp->AttackMagMin;
		m_PlayerItemProp.AttackPhyMax -= pChangeProp->AttackPhyMax;
		m_PlayerItemProp.AttackPhyMin -= pChangeProp->AttackPhyMin;
		m_PlayerItemProp.Hp -= pChangeProp->Hp;
		m_PlayerItemProp.MagicAttack -= pChangeProp->MagicAttack;
		m_PlayerItemProp.MagicAttackAdd -= pChangeProp->MagicAttackAdd;
		m_PlayerItemProp.MagicAttackPercent -= pChangeProp->MagicAttackPercent;
		m_PlayerItemProp.MagicCritical -= pChangeProp->MagicCritical;
		m_PlayerItemProp.MagicCriticalDamage  -= pChangeProp->MagicCriticalDamage;
		m_PlayerItemProp.MagicCriticalDamageDerate -= pChangeProp->MagicCriticalDamageDerate;
		m_PlayerItemProp.MagicDefend -= pChangeProp->MagicDefend;
		m_PlayerItemProp.MagicDefendPercent -=pChangeProp->MagicDefendPercent;
		m_PlayerItemProp.MagicDeratePercent -=pChangeProp->MagicDeratePercent;
		m_PlayerItemProp.MagicHit -= pChangeProp->MagicHit;
		m_PlayerItemProp.MagicJouk -= pChangeProp->MagicJouk;
		m_PlayerItemProp.MagicRift -= pChangeProp->MagicRift;
		m_PlayerItemProp.PhysicsAttack -= pChangeProp->PhysicsAttack;
		m_PlayerItemProp.PhysicsAttackAdd -= pChangeProp->PhysicsAttackAdd;
		m_PlayerItemProp.PhysicsAttackPercent -= pChangeProp->PhysicsAttackPercent;
		m_PlayerItemProp.PhysicsCritical -= pChangeProp->PhysicsCritical;
		m_PlayerItemProp.PhysicsCriticalDamage -= pChangeProp->PhysicsCriticalDamage;
		m_PlayerItemProp.PhysicsCriticalDamageDerate -= pChangeProp->PhysicsCriticalDamageDerate;
		m_PlayerItemProp.PhysicsDefend -= pChangeProp->PhysicsDefend;
		m_PlayerItemProp.PhysicsDefendPercent -= pChangeProp->PhysicsDefendPercent;
		m_PlayerItemProp.PhysicsDeratePercent -= pChangeProp->PhysicsDeratePercent;
		m_PlayerItemProp.PhysicsHit -= pChangeProp->PhysicsHit;
		m_PlayerItemProp.PhysicsJouk -= pChangeProp->PhysicsJouk;
		m_PlayerItemProp.PhysicsRift -= pChangeProp->PhysicsRift;
		m_PlayerItemProp.Stun -= pChangeProp->Stun;
		m_PlayerItemProp.Tie -= pChangeProp->Tie;
	}*/
}