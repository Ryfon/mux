﻿#include "ItemManager.h"
#include "ItemBuilder.h"
#include "CItemPackage.h"
#include "../Player/Player.h"
#include "../ItemAttributeHelper.h"

void ItemManagerEx::RecordPlayerItemDetialInfo(CPlayer *pPlayer)
{
	TRY_BEGIN;

	if ( !pPlayer )
		return;

#define  ITEMPKG_MAXPAGE 8
	//登记玩家所拥有的所装备物品的信息
	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("ItemManagerEx::RecordPlayerItemDetialInfo, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_2_Equips& equipInfoArr = playerInfo->equipInfo;
	for( int i = 0 ; i< EQUIP_SIZE ;i++ )
	{
		ItemInfo_i& equipInfo = equipInfoArr.equips[i];
		if ( equipInfo.usItemTypeID != PlayerInfo_2_Equips::INVALIDATE )//如果栏位有道具，则生成道具详细信息
		{
			RecordItemDetialInfo( pPlayer, equipInfo ,i, true );
		}
	}

	//记录时装物品
	RecordFashionItemDetailInfo(pPlayer);

	//记录每个包裹下分页是否有道具的标志
	bool pageEmptyArr[ITEMPKG_MAXPAGE] = { false };
	unsigned int pageCount = 0;//记录有多少个小的道具分页
	//获取包裹数组
	PlayerInfo_3_Pkgs& pkgInfoArr = playerInfo->pkgInfo;
	//读取包裹包中的道具简明信息
	ItemPosition tempItemPosition[PACKAGE_SIZE];
	StructMemSet(tempItemPosition,0,sizeof(tempItemPosition));
	for (int i = 0; i<ARRAY_SIZE(tempItemPosition); ++i)
	{
		tempItemPosition[i].itemIndex = -1;
	}
	unsigned int t = 0;
	for( int j = 0 ; j< PACKAGE_SIZE; j++ )
	{
		ItemInfo_i& pkgInfo =  pkgInfoArr.pkgs[j];
		if( pkgInfo.usItemTypeID !=  PlayerInfo_3_Pkgs::INVALIDATE )//如果该栏位有道具，则生成道具详细信息
		{
			//只有处在道具实体模型左上角的ItemInfo_i信息才有效
			//如果不是左上角，则道具的上方和左边必有同uid信息
			if (!pPlayer->IsLeftUpPkgIndex(j,pkgInfoArr.pkgs[j].uiID))
			{
				continue;
			}
			//if (j-1>=0)
			//{
			//	if (pkgInfoArr.pkgs[j-1].uiID == pkgInfoArr.pkgs[j].uiID)
			//	{
			//		continue;
			//	}
			//}
			//if (j-PKG_COLUMN>=0)
			//{
			//	if (pkgInfoArr.pkgs[j-PKG_COLUMN].uiID == pkgInfoArr.pkgs[j].uiID)
			//	{
			//		continue;
			//	}
			//}
			tempItemPosition[t].itemIndex = j;
			tempItemPosition[t].itemInfo = pkgInfoArr.pkgs[j];
			t++;
			RecordItemDetialInfo( pPlayer, pkgInfo, j, false );
			//设置该道具分页是有道具的标记
			unsigned pageIndex = j / PKG_PAGE_SIZE;
			if ( pageIndex >= ARRAY_SIZE(pageEmptyArr) )
			{
				D_ERROR( "ItemManagerEx::RecordPlayerItemDetialInfo, pageIndex(%d) >= ARRAY_SIZE(pageEmptyArr)\n", pageIndex );
				continue;
			}
			if ( !pageEmptyArr[pageIndex] )
			{
				pageEmptyArr[pageIndex] = true;
				++pageCount;//道具分页个数+1
			}
		}
	}
	//将包裹中的道具发送给客户端
	for(unsigned int pageIndex = 0; pageIndex <= t/PKG_PAGE_SIZE; pageIndex++)
	{
		MGItemPkgConciseInfo srvpkgmsg;
		StructMemCpy( srvpkgmsg.pkgInfo, tempItemPosition + pageIndex*PKG_PAGE_SIZE, sizeof(ItemPosition)*PKG_PAGE_SIZE );
		if (srvpkgmsg.pkgInfo[0].itemIndex != -1)
		{
			pPlayer->SendPkg<MGItemPkgConciseInfo>( &srvpkgmsg );
		}
	}
	//unsigned int pageTmpCount = 0;
	//for ( unsigned int pageIndex = 0 ; pageIndex< ITEMPKG_MAXPAGE && pageTmpCount < pageCount ; pageIndex++ )
	//{
	//	//如果该页有道具
	//	if ( pageEmptyArr[pageIndex] )
	//	{
	//		//todo sck
	//		MGItemPkgConciseInfo srvpkgmsg;
	//		//srvpkgmsg.pageIndex = pageIndex;
	//		for (int i = 0; i<PKG_PAGE_SIZE; ++i)
	//		{
	//			srvpkgmsg.pkgInfo[i].itemIndex = pageIndex*PKG_PAGE_SIZE+i;
	//			srvpkgmsg.pkgInfo[i].itemInfo = *(pkgInfoArr.pkgs + pageIndex*PKG_PAGE_SIZE+i);
	//		}
	//		//StructMemCpy( srvpkgmsg.pkgInfo, pkgInfoArr.pkgs + pageIndex*PKG_PAGE_SIZE, sizeof(ItemInfo_i)*PKG_PAGE_SIZE );
	//		pPlayer->SendPkg<MGItemPkgConciseInfo>( &srvpkgmsg );

	//		++pageTmpCount;
	//	}
	//}

	//通知玩家保护道具
	pPlayer->GetProtectItemManager().NoticeProtectItemDetialInfoToPlayer();

	TRY_END;
}

CBaseItem* ItemManagerEx::RecordItemDetialInfo(CPlayer* pPlayer, ItemInfo_i& itemInfo ,unsigned int index, bool isEquip /* = false  */)
{
	TRY_BEGIN;

	if ( !pPlayer )
		return NULL;

	FullPlayerInfo* pFullPlayerInfo = pPlayer->GetFullPlayerInfo();
	if(NULL == pFullPlayerInfo)
		return NULL;

	const ACE_Time_Value lastOffLineTV = ACE_Time_Value(pFullPlayerInfo->stateInfo.GetOffLineTick());//上次离线时间
	const ACE_Time_Value onLineTV = ACE_Time_Value(pFullPlayerInfo->stateInfo.GetOnlineTick());//本次上线时间

	CBaseItem* pItem = CItemBuilderSingle::instance()->CreateItem( itemInfo.usItemTypeID );
	if ( pItem )
	{
		TaskProperty* pTaskProp = CTaskPropertyManagerSingle::instance()->GetSpecialPropertyT( itemInfo.usItemTypeID );
		if ( pTaskProp )
			pItem->SetTaskID( pTaskProp->TaskID );

		//走时间流逝的耐久度的重新计算(2010.6.28 zsw)
		if( ACE_BIT_ENABLED( pItem->WearConsumeMode(),CItemPublicProp::LAST_DAY ) )
		{
			unsigned int expireTime = lastOffLineTV.sec() + itemInfo.usWearPoint;
			if(expireTime <= onLineTV.sec())
				itemInfo.usWearPoint = 0;
			else
				itemInfo.usWearPoint = expireTime - onLineTV.sec();
		}

		//插入全局道具管理
		pItem->SetItemUID( itemInfo.uiID );
		pItem->SetItemOwner( pPlayer->GetFullID() );
		pItem->SetItemLevel(GET_ITEM_LEVELUP(itemInfo.ucLevelUp));
		if ( isEquip )
		{
			//如果该道具是装备，那么加入玩家的装备管理器
			pPlayer->GetItemDelegate().PushEquipItem( pItem );
			////耐久度>0的话，才把装备属性加上
			//if ( itemInfo.usWearPoint > 0 )
			//	pItem->AddItemPropToPlayer( pPlayer, itemInfo );
			ItemAttributeHelper::OnEquipItemAffectPlayer( pPlayer, itemInfo );
			

			//玩家装备属于套装,则判断是否将套装的属性加给玩家
			if ( pItem->GetSuitID() > 0)
				pPlayer->GetSuitDelegate().EquipSuitPart( pItem->GetSuitID(), index, itemInfo.usWearPoint>0 );
		}
		else
		{
			//如果是放在包裹中的道具，那么加入玩家的包裹管理器
			pPlayer->GetItemDelegate().PushPkgItem( pItem );
		}
	}
	return pItem;

	TRY_END;

	return NULL;
}

//玩家退出时，移除玩家的道具在服务器的内容
void ItemManagerEx::RemovePlayerItem(CPlayer *pPlayer)
{
	if ( !pPlayer )
		return ;

	TRY_BEGIN;

	pPlayer->GetItemDelegate().OnPlayerExit();

	TRY_END;
}

void ItemManagerEx::RecordFashionItemDetailInfo(  CPlayer* pPlayer )
{
	TRY_BEGIN;

	if ( NULL == pPlayer )
		return;

	FullPlayerInfo* pFullPlayerInfo = pPlayer->GetFullPlayerInfo();
	if(NULL == pFullPlayerInfo)
		return;

	const ACE_Time_Value lastOffLineTV = ACE_Time_Value(pFullPlayerInfo->stateInfo.GetOffLineTick());//上次离线时间
	const ACE_Time_Value onLineTV = ACE_Time_Value(pFullPlayerInfo->stateInfo.GetOnlineTick());//本次上线时间

	PlayerInfo_Fashions fashion = pPlayer->Fashions();
	for(int i = 0; i < MAX_FASHION_COUNT; i++)
	{
		ItemInfo_i& itemInfo = fashion.fashionData[i];
		if(0 != itemInfo.uiID)
		{
			CBaseItem* pItem = CItemBuilderSingle::instance()->CreateItem( itemInfo.usItemTypeID );
			if ( pItem )
			{
				//走时间流逝的耐久度的重新计算(2010.6.28 zsw)
				if( ACE_BIT_ENABLED( pItem->WearConsumeMode(),CItemPublicProp::LAST_DAY ) )
				{
					unsigned int expireTime = lastOffLineTV.sec() + itemInfo.usWearPoint;
					if(expireTime <= onLineTV.sec())
						itemInfo.usWearPoint = 0;
					else
						itemInfo.usWearPoint = expireTime - onLineTV.sec();
				}

				//插入全局道具管理
				pItem->SetItemUID( itemInfo.uiID );
				pItem->SetItemOwner( pPlayer->GetFullID() );
				pItem->SetItemLevel(GET_ITEM_LEVELUP(itemInfo.ucLevelUp));
				pPlayer->GetItemDelegate().PushFashionItem( pItem );
			}
		}
	}

	TRY_END;
	return;
}

void ItemManagerEx::CheckPlayerEquipItem( CPlayer* pPlayer )
{
	TRY_BEGIN;
	if ( !pPlayer )
		return;

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("ItemManagerEx::CheckPlayerEquipItem, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_2_Equips& equipInfoArr = playerInfo->equipInfo;
	for( int i = 0, j = 0 ; i< EQUIP_SIZE && j< 1000 ;i++,j++ )//参数j为防止陷入死循环，做遍历上限次数保护
	{
		ItemInfo_i& equipInfo = equipInfoArr.equips[i];
		if ( equipInfo.usItemTypeID != PlayerInfo_2_Equips::INVALIDATE )//如果栏位有道具，则生成道具详细信息
		{
			CBaseItem* pItem = pPlayer->GetItemDelegate().GetItemByItemUID( equipInfo.uiID );
			if ( NULL == pItem )
			{
				continue;
			}
			if (pItem->IsCanUseByProperty(pPlayer))//如果玩家属性点满足
			{
				if (!pPlayer->GetItemDelegate().IsEquipPosValid(pItem->GetGearArmType()))//之前玩家属性点不满足
				{
					ItemAttributeHelper::OnEquipItemAffectPlayer( pPlayer, equipInfo );
					pPlayer->GetItemDelegate().SetEquipPosValid(pItem->GetGearArmType());
					//pPlayer->ReloadSecondProp();
					i = -1;//当玩家属性点发生改变时，重新遍历一下装备列表
				}
			}
			else
			{
				if (pPlayer->GetItemDelegate().IsEquipPosValid(pItem->GetGearArmType()))
				{
					ItemAttributeHelper::OnUnEquipItemAffectPlayer( pPlayer, equipInfo );
					pPlayer->GetItemDelegate().SetEquipPosUnValid(pItem->GetGearArmType());
					//pPlayer->ReloadSecondProp();
					i = -1;//当玩家属性点发生改变时，重新遍历一下装备列表
				}
			}
		}
	}
	pPlayer->ReloadSecondProp();
	TRY_END;
	return;
}

CBaseItem* CItemBuilderEx::CreateItem( unsigned long itemID )
{
	CBaseItem* pItem = CreateRawItem( itemID );
	return pItem;
}

CBaseItem* CItemBuilderEx::CreateRawItem( unsigned long  itemID )
{
	CBaseItem* pItem =  NULL;

	TRY_BEGIN;

	CItemPublicProp* pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemID );
	if ( pItemPublicProp )//获取该道具的共有属性属性
	{
		ITEM_TYPEID typeID =  pItemPublicProp->GetItemTypeID();
		switch( typeID )
		{
			//@brief:创建武器道具
		case E_TYPE_WEAPON:
			{
				WeaponProperty* pWeaponProp = CWeaponPropertyManagerSingle::instance()->GetSpecialPropertyT( itemID );
				if ( pWeaponProp )
				{
					pItem = NEW CWeaponItemEx( pItemPublicProp, pWeaponProp );
				}
				else
				{
					D_ERROR("创建武器失败:%u\n", itemID );
				}
			}
			break;
			//@brief:创建除了武器以外的其他道具
		case E_TYPE_EQUIP:
			{
				EquipProperty* pEquipProp = CEquipPropertyManagerSingle::instance()->GetSpecialPropertyT( itemID );
				if ( pEquipProp )
				{
					pItem = NEW CEquipItemEx( pItemPublicProp, pEquipProp );
				}
				else
				{
					D_ERROR("创建道具失败:%u\n", itemID );
				}
			}
			break;
		case E_TYPE_CONSUME:
			{
				ConsumeProperty* pCosumeProp = CConsumePropertyManagerSingle::instance()->GetSpecialPropertyT( itemID );
				if ( pCosumeProp )
				{
					pItem = NEW CConsumeItem( pItemPublicProp, pCosumeProp );
				}
				else
				{
					D_ERROR("消费性道具创建失败:%u\n",itemID );
				}
			}
			break;
		case E_TYPE_TASK:
			{
				TaskProperty* pTaskProp = CTaskPropertyManagerSingle::instance()->GetSpecialPropertyT( itemID );
				if ( pTaskProp )
				{
					pItem = NEW CTaskItem( pItemPublicProp, pTaskProp );
				}
				else
				{
					D_ERROR("创建任务道具失败:%u\n", itemID );
				}
			}
			break;
		case E_TYPE_FUNCTION:
			{
				FunctionProperty* pFunctionProp = CFunctionPropertyManagerSingle::instance()->GetSpecialPropertyT( itemID );
				if ( pFunctionProp )
				{
					pItem = NEW CFunctionItem( pItemPublicProp, pFunctionProp );
				}
				else
				{
					D_ERROR("创建功能性道具失败:%u\n",itemID);
				}
			}
			break;
		case E_TYPE_AXIOLOGY:
			{
				AxiologyProperty* pAxiologyProp = CAxiologyPropertyManagerSingle::instance()->GetSpecialPropertyT( itemID );
				if ( pAxiologyProp )
				{
					pItem = NEW CAxiologyItem( pItemPublicProp, pAxiologyProp );
				}
				else
				{
					D_ERROR("创建价值物品失败:%u\n", itemID );
				}
			}
			break;
		case E_TYPE_BULEPRINT:
			{
				CBulePrint* pBulePrintProp = BulePrintManagerSingle::instance()->GetSpecialPropertyT( itemID );
				pItem = NEW CBulePrintItem( pItemPublicProp ,  pBulePrintProp );
				if ( !pItem)
				{
					D_ERROR("创建图纸物品失败:%u\n", itemID );
				}
			
			}
			break;
		case E_TYPE_SKILL:
			{
				SkillBookProperty* pSkillBookProp =  CSkillBookPropertyManagerSingle::instance()->GetSpecialPropertyT( itemID );
				pItem = NEW CSkillBookItem( pItemPublicProp ,pSkillBookProp );
				if ( !pItem )
				{
					D_ERROR("创建技能书失败:%u\n", itemID );
				}
			}
			break;
		case E_TYPE_FASHION:
			{
				FashionProperty* pFashionProp =  CFashionPropertyManagerSingle::instance()->GetSpecialPropertyT( itemID );
				pItem = NEW CFashionItem( pItemPublicProp ,pFashionProp );
				if ( !pItem )
				{
					D_ERROR("创建时装失败:%u\n", itemID );
				}
			}
			break;
		case E_TYPE_MOUNT:
			{
				MountProperty* pMountProp = CMountPropertyManagerSingle::instance()->GetSpecialPropertyT( itemID );
				pItem = NEW CMountItem( pItemPublicProp, pMountProp );
				if ( !pItem )
				{
					D_ERROR("创建骑乘失败:%u\n", itemID );
				}
			}
			break;
		default:
			break;
		}
	}
	else
	{
		D_ERROR("创建的道具在一级属性表中没有找到:%u\n", itemID );
	}

	TRY_END;

	return pItem;
}

bool CItemBuilderEx::GMCreateItem( CPlayer* pPlayer, unsigned long itemTypeID, int count )
{
	if ( count < 0 )
	{
		D_ERROR( "CItemBuilderEx::GMCreateItem, count < 0\n" );
		return false;
	}

	if ( NULL == pPlayer )
	{
		D_ERROR( "CItemBuilderEx::GMCreateItem, NULL == pPlayer\n" );
		return false;
	}

	TRY_BEGIN;

	CItemPublicProp* pProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
	if ( NULL == pProp )
	{
		D_ERROR( "CItemBuilderEx::GMCreateItem, NULL == pProp\n" );
		return false;
	}

	return pPlayer->GetNormalItem( itemTypeID, count, 0 );

	//int taskID = 0;
	//TaskProperty* pTaskProp = CTaskPropertyManagerSingle::instance()->GetSpecialPropertyT( itemTypeID );
	//if ( pTaskProp )
	//{
	//	taskID = pTaskProp->TaskID;
	//}

	//int errNo = 0;
	////如果是不可堆叠的物品
	//if ( pProp->m_itemMaxAmount == 1 )
	//{
	//	for ( int i = 0 ; i< count ; i++ )
	//	{
	//		if ( !(pPlayer->GetNormalItem( itemTypeID, 1, taskID )) )
	//		{
	//			break;
	//		}
	//		//与GetNormalItem功能重复，改用GetNormalItem，CItemPkgEle tmpItemEle;
	//		//tmpItemEle.SetItemEleProp( itemTypeID, 1,taskID   );
	//		//if ( !( pPlayer->GiveNewItemToPlayer( tmpItemEle.GetItemEleInfo() ,tmpItemEle.GetTaskID(),  errNo ) )
	//		//{
	//		//	break;
	//		//}
	//	}
	//} else { //如果是可堆叠的物品
	//	//与GetNormalItem功能重复，改用GetNormalItem，CItemPkgEle tmpItemEle;
	//	//tmpItemEle.SetItemEleProp( itemTypeID, count, taskID );
	//	//return pPlayer->GiveNewItemToPlayer( tmpItemEle.GetItemEleInfo() , tmpItemEle.GetTaskID(), errNo );
	//	return pPlayer->GetNormalItem( itemTypeID, count, taskID );
	//}

	//return true;

	TRY_END;

	return false;
}

