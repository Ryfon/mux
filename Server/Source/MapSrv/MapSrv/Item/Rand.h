﻿/// \file
/// \brief \b [Internal] Random number generator
///
/// This file is part of RakNet Copyright 2003 Kevin Jenkins.
///
/// Usage of RakNet is subject to the appropriate license agreement.
/// Creative Commons Licensees are subject to the
/// license found at
/// http://creativecommons.org/licenses/by-nc/2.5/
/// Single application licensees are subject to the license found at
/// http://www.jenkinssoftware.com/SingleApplicationLicense.html
/// Custom license users are subject to the terms therein.
/// GPL license users are subject to the GNU General Public
/// License as published by the Free
/// Software Foundation; either version 2 of the License, or (at your
/// option) any later version.


#ifndef __RAND_H
#define __RAND_H 

 /// Initialise seed for Random Generator
 /// \param[in] seed The seed value for the random number generator.
extern void seedMT( unsigned int seed );

/// \internal
extern unsigned int reloadMT( void );

/// Gets a random unsigned int
/// \return an integer random value.
extern unsigned int randomMT( void );

/// Gets a random float
/// \return 0 to 1.0f, inclusive
extern float frandomMT( void );

/// Randomizes a buffer
extern void  fillBufferMT( void *buffer, unsigned int bytes );

class CRandGen
{
public:
	static bool SetSeed( unsigned int newSeed ) { m_curSeed = newSeed; return true; }

public:
	static unsigned int GetRand() {
		//Robert Jenkins's 32 bit integer hash function, from: http://www.concentric.net/~Ttwang/tech/inthash.htm;
		unsigned int hashValue = (m_curSeed+0x7ed55d16) + (m_curSeed<<12);
		hashValue = (hashValue^0xc761c23c) ^ (hashValue>>19);
		hashValue = (hashValue+0x165667b1) + (hashValue<<5);
		hashValue = (hashValue+0xd3a2646c) ^ (hashValue<<9);
		hashValue = (hashValue+0xfd7046c5) + (hashValue<<3);
		hashValue = (hashValue^0xb55a4f09) ^ (hashValue>>16);

		m_curSeed = hashValue+1;
		return hashValue;
	}

private:
	static unsigned int m_curSeed;
};

#endif
