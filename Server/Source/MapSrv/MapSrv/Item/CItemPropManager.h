﻿/** @file CItemPropManager.h
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-4-29
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef ITEM_PROPMANAGER_H
#define ITEM_PROPMANAGER_H

#include <map>
#include <vector>

#include "CSpecialItemProp.h"
#include "aceall.h"
#include "Utility.h"
using namespace MUX_PROTO;


class  CElement;
//class  CItemPublicProp;
//struct WeaponProperty;
//struct EquipProperty;
//struct ConsumeProperty;
//struct TaskProperty;
//struct FunctionProperty;
//struct AxiologyProperty;
//struct PlateCondition;

//New_ItemSystem
class IItemPropertyLoadTemplate
{
public:
	IItemPropertyLoadTemplate(){}

	virtual ~IItemPropertyLoadTemplate();

public:
	bool InsertItemPublicProperty( CElement* pxmlElement );

	virtual bool InsertItemSpecialProperty( CElement* pxmlElement );

	virtual bool LoadXML( const char* szFilePath );

	void GetItemCanUseRace( CElement* pxmlElement, unsigned int& canUseRace );

	void GetItemCanUseClass( CElement *pxmlElement, CItemPublicProp* pItemPublicProp );

	void GetItemWearConsume( CElement* pxmlElement, unsigned int& comsumeWear );

	void GetItemWearZero( CElement* pxmlElement, unsigned int& WearZero );
};

class CItemPublicPropManagerEx
{
	friend class ACE_Singleton<CItemPublicPropManagerEx, ACE_Null_Mutex>;

private:
	CItemPublicPropManagerEx( const CItemPublicPropManagerEx& );  //屏蔽这两个操作；
	CItemPublicPropManagerEx& operator = ( const CItemPublicPropManagerEx& );//屏蔽这两个操作；

public:
	CItemPublicPropManagerEx(){}

	~CItemPublicPropManagerEx();

public:
	void InsertItemPublicProp( CItemPublicProp* itemPublicProp );

	CItemPublicProp* GetItemPublicProp( long itemID );

	bool SetItemTypeID( long itemID, ITEM_TYPEID typeID );

private:
	std::map<long,CItemPublicProp*> mItemPublicPropMan;
};
typedef ACE_Singleton<CItemPublicPropManagerEx, ACE_Null_Mutex> CItemPublicPropManagerExSingle;

//加载属性的模板
template<class T>
class CPropertyManagerT : public IItemPropertyLoadTemplate
{
public:
	CPropertyManagerT(){}

	virtual ~CPropertyManagerT()
	{
		for ( typename std::map<unsigned int ,T*>::iterator lter = mPropertyMapT.begin(); lter!= mPropertyMapT.end(); ++lter )
		{
			if ( NULL != lter->second )
			{
				delete lter->second;
				lter->second = NULL;
			}
		}
	}

private:
	CPropertyManagerT( const CPropertyManagerT<T>& );
	CPropertyManagerT<T>& operator= ( const CPropertyManagerT<T>& );

public:
	virtual bool InsertItemSpecialProperty( CElement * pxmlElement );

	T* GetSpecialPropertyT( unsigned int itemTypeID )
	{
		typename std::map<unsigned int ,T*>::iterator lter = mPropertyMapT.find( itemTypeID ) ;
		if ( lter != mPropertyMapT.end() )
			return lter->second;

		return NULL;
	}

	CItemPublicProp* GetItemPublicProp( unsigned int itemTypeID )
	{
		return CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
	}

private:
	std::map<unsigned int, T*> mPropertyMapT;

};

//装备物品读取
typedef ACE_Singleton<CPropertyManagerT<WeaponProperty>,ACE_Null_Mutex> CWeaponPropertyManagerSingle;

//装备物品读取
typedef ACE_Singleton<CPropertyManagerT<EquipProperty>,ACE_Null_Mutex> CEquipPropertyManagerSingle;

//任务物品的读取
typedef ACE_Singleton<CPropertyManagerT<ConsumeProperty>,ACE_Null_Mutex> CConsumePropertyManagerSingle;

//任务物品的读取
typedef ACE_Singleton<CPropertyManagerT<TaskProperty>,ACE_Null_Mutex> CTaskPropertyManagerSingle;

//功能性物品的读取
typedef ACE_Singleton<CPropertyManagerT<FunctionProperty>,ACE_Null_Mutex> CFunctionPropertyManagerSingle;

//价值物道具的读取
typedef ACE_Singleton<CPropertyManagerT<AxiologyProperty>,ACE_Null_Mutex> CAxiologyPropertyManagerSingle;

//技能书
typedef ACE_Singleton<CPropertyManagerT<SkillBookProperty>,ACE_Null_Mutex> CSkillBookPropertyManagerSingle;

//时装
typedef ACE_Singleton<CPropertyManagerT<FashionProperty>, ACE_Null_Mutex> CFashionPropertyManagerSingle;


typedef ACE_Singleton<CPropertyManagerT<MountProperty>, ACE_Null_Mutex> CMountPropertyManagerSingle;

#define  BulePrintFactorMAX 10
#define  BulePrintCriMax 8

struct PlateCondition;

//图纸类
class CBulePrint
{
public:
	//图纸的合成因素
	struct  BulePrintFactor
	{
		unsigned int itemTypeID;
		unsigned int count;
		BulePrintFactor():itemTypeID(0),count(0){}
	};

	//图纸的合成概率
	struct BulePrintCri
	{
		unsigned int Addid;
		unsigned int Odd;
		BulePrintCri():Addid(0),Odd(0){}
	};

public:
	explicit CBulePrint( CElement* pxmlElement );

	~CBulePrint(){}

public:
	//是否到达了图纸的条件
	void GetBulePrintCondition( PlateCondition* conditionArr );
	//图纸的随机方案的结果
	unsigned int BulePrintRandonResult();
	//获取图纸的ID号
	unsigned int GetBulePrintID() { return bulePrintID; }

	int GetGearArmType() { return GearArmType ;}

private:
	CBulePrint::BulePrintFactor fatcorArr[BulePrintFactorMAX];
	CBulePrint::BulePrintCri criArr[BulePrintCriMax];
	unsigned int bulePrintID;
	unsigned int OddTotal;
	unsigned int factorIndex;
	unsigned int criIndex;
	int   GearArmType;
};

////图纸道具属性的读取
typedef ACE_Singleton<CPropertyManagerT<CBulePrint>,ACE_Null_Mutex> BulePrintManagerSingle;



struct MountLevelUpProp
{
	int ActiveTime;
	int BeKickCri;
	MountLevelUpProp():ActiveTime(0),BeKickCri(0){}
};


class MountLevelUpPropManager
{
public:

	void LoadXML( const char* szFilePath );

	MountLevelUpProp* GetMountLevelUpProp( unsigned int levelUpID );

private:

	std::map<unsigned int, MountLevelUpProp> mMountLevelUpPropManager;

};

typedef ACE_Singleton<MountLevelUpPropManager,ACE_Null_Mutex> MountLevelUpPropManagerSingle;


///幸运星系统
struct LuckStarCondtion
{
	unsigned int conditionID;
	unsigned int propID;
	unsigned int compareCondition;
	unsigned int compareValue;	
	LuckStarCondtion():propID(0),compareCondition(0),compareValue(0){}
};

class CPlayer;
struct LuckStar
{
	unsigned int luckItemID;
	std::vector<LuckStarCondtion> luckStartConditonVec;

	LuckStar():luckItemID(0){}

	void Reset()
	{
		luckItemID = 0 ;
		luckStartConditonVec.clear();
	}

	//获取幸运星的条件
	LuckStarCondtion* GetConditionByID( unsigned int conditionID );

	//凋落道具后，随机生成一个条件
	int RandomCondition();

	//检查玩家的属性是否满足幸运星的条件
	bool CheckPlayerPropertyFillCondition( unsigned int conditionID,CPlayer* pPlayer );
};


class LuckStarManager
{
public:

	void LoadXML( const char* pxmlFile );

	LuckStar* GetLuckStar( unsigned int itemTypeID );

private:
	std::map< unsigned int, LuckStar> mLuckStarMap;
};
typedef ACE_Singleton<LuckStarManager,ACE_Null_Mutex> LuckStarManagerSingle;


class DiePunishInfo
{
	friend class ACE_Singleton<DiePunishInfo, ACE_Null_Mutex>;

public:
	DiePunishInfo():moneyprecent(10),moneymin(1),moneymax(1000000),evilprecent(3),expmin(1),expmax(1000000)
	{
	}

public:
	void LoadXML();

	void GetSubMoneyInfo( int& precent, int& min, int& max )
	{
		precent = moneyprecent;
		min = moneymin;
		max = moneymax;
	}

	void GetEvilExpInfo( int&precent, int&min, int&max )
	{
		precent = evilprecent;
		min = expmin;
		max = expmax;
	}

private:
	int moneyprecent;
	int moneymin;
	int moneymax;
	int evilprecent;
	int expmin;
	int expmax;
};

typedef ACE_Singleton<DiePunishInfo, ACE_Null_Mutex> DiePunishInfoSingle;


#endif
