﻿#include "ItemLeavelUpCri.h"
#include "XmlManager.h"
#include "../../../Base/Utility.h"


std::vector<LevelUpCri> CItemLeavelUpCriManager::mLevelUpCriVec;
std::vector<LevelUpModulus> CItemLevelUpModulus::mLevelUpModulus;

void CItemLeavelUpCriManager::LoadXML(const char* pszXMLFile )
{
	if ( !pszXMLFile )
		return;

	D_DEBUG("LoadXML:%s\n", pszXMLFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszXMLFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszXMLFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszXMLFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	for ( std::vector<CElement *>::iterator lter = childElements.begin();
		lter!= childElements.end();
		++lter )
	{
		PushLevelUpCri( *lter );
	}
	TRY_END;
}

void CItemLeavelUpCriManager::PushLevelUpCri( CElement* pxmlElement )
{
	if ( !pxmlElement )
		return;

	LevelUpCri levelCri;
	
	const char* p = pxmlElement->GetAttr("Levelup");
	if ( p )
	{
		levelCri.level = ACE_OS::atoi( p );
	}
	else
	{
		D_ERROR("ItemLeavelUpCri 找不到 Levelup\n");
	}

	p = pxmlElement->GetAttr("odd");
	if ( p )
	{
		levelCri.odd = ACE_OS::atoi( p );
	}
	else
	{
		D_ERROR("ItemLeavelUpCri 找不到odd\n");
	}

	p = pxmlElement->GetAttr("Sumodd");
	if ( p )
	{
		levelCri.Sumodd = ACE_OS::atoi( p );
	}
	else
	{
		D_ERROR("ItemLeavelUpCri 找不到Sumodd\n");
	}

	p = pxmlElement->GetAttr("Cri");
	if ( p )
	{
		levelCri.cri = (float)atof(p);
	}
	else
	{
		D_ERROR("ItemLeavelUpCri 找不到Cri\n");
	}

	p = pxmlElement->GetAttr("UpgradeFail");
	if ( p )
	{
		int failstart = ACE_OS::atoi( p );
		if ( failstart !=  0 )
		{
			if ( failstart == -1 )
				levelCri.updateFailstart =  levelCri.updateFailEnd = -1;
			else
			{
				char* pFailRangeStart = ACE_OS::strtok( const_cast<char *>(p) , ",");
				if ( pFailRangeStart )
				{
					levelCri.updateFailstart = ACE_OS::atoi( pFailRangeStart );
					char* pFailRangeEnd = ACE_OS::strtok(NULL, ",");
					if ( pFailRangeEnd )
						levelCri.updateFailEnd = ACE_OS::atoi( pFailRangeEnd );
				}
			}
		}
	}

	mLevelUpCriVec.push_back( levelCri );
}

bool CItemLeavelUpCriManager::IsLevelUp(unsigned int level )
{
	unsigned int tmpLevel = level - 1;
	if ( tmpLevel >= ARRAY_SIZE(mLevelUpCriVec) )
	{
		D_ERROR( "CItemLeavelUpCriManager::IsLevelUp，错误的输入level%d\n", level );
		return false;
	}
	LevelUpCri& tmpLevelUpCri  = mLevelUpCriVec[tmpLevel];
	unsigned int randResult = RandNumber( 1,tmpLevelUpCri.Sumodd );
	if ( randResult <= tmpLevelUpCri.odd )
	{
		return true;
	}

	return false;
}

LevelUpCri* CItemLeavelUpCriManager::GetLevelUpCri(unsigned int level)
{
	int count = (int)mLevelUpCriVec.size();
	for ( int i = 0 ; i < count; i++ )
	{
		LevelUpCri& levelCir = mLevelUpCriVec[i];
		if ( levelCir.level == level )
			return &(mLevelUpCriVec[i]);
	}
	return	NULL;
}


void CItemLevelUpModulus::LoadXML(const char* pszXMLFile )
{
	if ( !pszXMLFile )
		return;

	D_DEBUG("LoadXML:%s\n", pszXMLFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszXMLFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszXMLFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszXMLFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	for ( std::vector<CElement *>::iterator lter = childElements.begin();
		lter!= childElements.end();
		++lter )
	{
		PushLevelUpModulus( *lter );
	}
	TRY_END;
}

void CItemLevelUpModulus::PushLevelUpModulus(CElement* pxmlElement )
{
	if ( !pxmlElement )
		return;

	LevelUpModulus levelUpModulus;
	
	const char* p = pxmlElement->GetAttr("Level");
	if ( p )
	{
		levelUpModulus.brand = ACE_OS::atoi( p );
	}
	else
	{
		D_ERROR("CItemLevelUpModulus 找不到Level属性\n");
	}

	p = pxmlElement->GetAttr("Modulus");
	if ( p )
	{
		levelUpModulus.Modulus = ACE_OS::atoi( p );
	}
	else
	{
		D_ERROR("CItemLevelUpModulus 找不到Modulus属性\n");
	}

	mLevelUpModulus.push_back( levelUpModulus );
}

unsigned int CItemLevelUpModulus::GetLevelUpModulus( unsigned int level )
{
	unsigned int brand = (unsigned)(level-1)/10+1;

	for ( std::vector<LevelUpModulus>::iterator lter = mLevelUpModulus.begin();
		lter!= mLevelUpModulus.end();
		++lter)
	{
		LevelUpModulus& levelUp = *lter;
		if ( levelUp.brand == brand )
			return levelUp.Modulus;
	}
	return 1;
}
