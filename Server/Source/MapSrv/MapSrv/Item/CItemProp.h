﻿/** @file CItemProp.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-4-29
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/


#ifndef ITEM_PROP_H
#define ITEM_PROP_H


#include "../../../Base/itemgloal.h"
#include <string>
using namespace ITEM_GLOBAL;


class CNormalScript;

class CItemPublicProp
{
public:
	enum eWearConsumeMode
	{
		DIE = (1<<0),				//死亡
		USE_TIME = (1<<1),			//使用次数
		INJURED_TIME = (1<<2),		//受伤害次数
		USE_SKILL_TIME = (1<<3),	//使用技能次数
		LAST_DAY = (1<<4)			//最后期限
	};

	enum eWearZeroMode
	{
		WARE_NO_AFFECT = (1<<0),	//可以穿上，但是穿上无属性
		CAN_NOT_USE = (1<<1),		//不能使用
		DESTROY = (1<<2)			//销毁
	};

public:
	CItemPublicProp():m_itemID(0),m_itemPrice(0),m_itemLevel(0),m_itemMaxAmount(0),m_itemType(E_EQUIP),
		m_itemMetal(E_METAL_BIG),m_itemTypeID(E_TYPE_WEAPON),m_itemMassLevel(E_GRAY),m_cannotChuck(0),
		m_cannotTrade(0),m_cannotLost(0),m_cannotDepot(0),mCanUseRace(0),mCanUseClass(0),mBindEquip(E_UN_BIND),m_pItemScript(NULL)
		,rq_strength(0),rq_agility(0),rq_intelligence(0),rq_spirit(0),rq_physique(0),m_width(1),m_height(1)
	{
	};
	~CItemPublicProp(){};

public:
	std::string m_itemName;//道具名称
	std::string m_itemClassStr;
	u32 m_itemID;//道具类型ID
	s32 m_itemPrice;//道具价钱
	s32 m_itemLevel;//装备道具等级
	s32 m_itemMaxAmount;//道具的最大个数
	ITEM_TYPE  m_itemType;//道具类型
	ITEM_METAL m_itemMetal;//道具的材质
	ITEM_TYPEID m_itemTypeID; //每个物品具体的类型
	//ITEM_NATION m_itemRace;//所属种族
	//ITEM_CLASS  m_itemClass;//所属职业
	ITEM_MASSLEVEL m_itemMassLevel;//质量等级
	int m_cannotChuck;//是否可删除
	int m_cannotTrade;//是否可交易
	int m_cannotLost;//是否死忙掉落
	int m_cannotDepot;//是否可存仓库
	int m_maxWear;//最大的耐久度
	int m_repairMode;//修复模式
	unsigned int m_wearOMode;//耐久度为零时的处理模式
	unsigned int m_wearConsumeMode;//耐久度的消耗方式
	unsigned int mCanUseRace;//所能装备的种族
	unsigned int mCanUseClass;//所能装备的职业
	unsigned int m_width;//道具所占格子的宽度
	unsigned int m_height;//道具所占格子的高度
	ITEM_BIND_TYPE mBindEquip;//所绑定的类型
	CNormalScript* m_pItemScript;//item所挂脚本；

public:
	//取属性中的随机集号(内部高16bit为卓越，低16bit为幸运)
	//    装备掉落时用到，有可能掉落装备是否卓越/幸运会在运营过程中改变，因此这一随机集号属性只在掉落一刻发生作用，并相应地记至具体item实例上；
	unsigned int GetPropRandSet( bool& isLucky, bool& isExcellent );

private:
	unsigned int m_randSet;//对应的随机集号，item对应的随机集号可能发生变化，因此在掉落时即时记在item的身上

public:
	unsigned short rq_strength; //使用所需要的力量
	unsigned short rq_agility;  //使用所需要的敏捷
	unsigned short rq_intelligence;//使用所需要的智力
	unsigned short rq_spirit;//使用所需要的精神
	unsigned short rq_physique;//使用所需要的体质

public:
	u32 GetItemID(){ return m_itemID ; }
	ITEM_TYPEID GetItemTypeID() { return m_itemTypeID; } 
};

#endif 
