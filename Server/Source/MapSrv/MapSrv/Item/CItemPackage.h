﻿/** @file CItemPackage.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-4-29
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

/************************************************************************/
/* todo wcj 2011.01.07
道具包元素类	：	道具包内的一个道具元素信息
道具包类		：	包含多个道具包元素对象
道具包管理器类	：	提供以下接口
					道具包的产生(杀死怪物后根据怪物的配置输就生成，只有这一种方式生成道具)
					销毁/释放
					监视已经生成的道具包的信息（如过期等）
					获取生成的道具的信息
*/
/************************************************************************/
#pragma once

#include <map>
#include <vector>
#include <time.h>

class CGroupTeam;
class CBaseItem;
class CItemPackageManager;
class CPlayer;

#include "../MuxMap/MapCommonHead.h"
#include "CItemPropManager.h"

using namespace MUX_PROTO;

#define  PICK_PKG_RANGE 10//拾取道具包的范围

class CItemPkgEle
{
public:
	typedef enum
	{
		INIT,//初始状态

		TRIGGER_TIMEOUT,//未触发（60秒超时）
		TRIGGERED,//已触发（60秒内事件触发）
		
		ROLL_NO_RESULT,//已触发但没有结果(队伍中任何人可以拾取)
		ROLL_HAVE_RESULT,//已触发有结果(队伍中roll点最大者可以拾取)

		ROLL_PICK_TIMEOUT//在触发后的60秒内没有捡起(触发后60秒超时，任何人可以捡起)
	}RollItemState;


public:
	CItemPkgEle()
	{
		mEleItemInfo.ucCount = mEleItemInfo.ucLevelUp = mEleItemInfo.uiID = mEleItemInfo.randSet/*usAddId*/ = mEleItemInfo.usItemTypeID = mEleItemInfo.usWearPoint = 0;
		mEleItemInfo.randSet/*usAddId*/ = 0;
		mEleItemInfo.excellentid = mEleItemInfo.luckyid = 0;
		mEleTaskID = mElePrice = 0;
		mEleMassLevel = E_GRAY;
		mEleLock = false;
		mOwnerID.dwPID = 0;
		mOwnerID.wGID  = 0;
		rollState = INIT;
		roleId = 0;
	}

	//是否被锁了
	bool    IsLock() { return mEleLock;}

	//锁住该物品
	void    Lock() { mEleLock =true; }
	
	//卸载物品
	void    UnLock() { mEleLock = false; }

	////根据已有的道具信息,设置该道具信息的属性
	//void SetItemEleProp( const ItemInfo_i& itemInfo, unsigned int price, ITEM_MASSLEVEL massLevel, unsigned int taskID );

	//使用道具信息设新道具属性，用于玩家掉落
	void SetItemWithItemInfo_i( const ItemInfo_i& itemInfo, unsigned int price, ITEM_MASSLEVEL massLevel, unsigned int taskID );

	//设置道具，随机属性新生成
	void SetItemPropNewRand( unsigned int itemTypeID, unsigned char itemCount, unsigned int taskID, unsigned int itemLevel );

	//设置道具属性，随机属性手动赋入
	void SetItemPropExistRand( unsigned int itemTypeID, unsigned char itemCount ,unsigned int taskID ,unsigned int itemLevel
		, unsigned int randSet, unsigned int luckyid, unsigned int excelid );

	//获取任务ID
	unsigned int GetTaskID() const { return mEleTaskID; }

	//道具的类型在XML中的编号
	unsigned int GetItemTypeID() const { return mEleItemInfo.usItemTypeID; }

	//获取装备道具价格
	unsigned int GetPrice()  const { return mElePrice; }

	//获取装备材质等级
	ITEM_MASSLEVEL GetMassLevel() const { return mEleMassLevel; }

	//获取装备的ItemInfo信息
	const ItemInfo_i& GetItemEleInfo() const { return mEleItemInfo; }

	bool operator==( const CItemPkgEle& itemEle )
	{
		if ( mEleItemInfo.ucCount == itemEle.mEleItemInfo.ucCount
			&& mEleItemInfo.usItemTypeID == itemEle.mEleItemInfo.usItemTypeID 
			&& mEleItemInfo.ucLevelUp == itemEle.mEleItemInfo.ucLevelUp 
			&& mEleItemInfo.luckyid  ==  itemEle.mEleItemInfo.luckyid
			&& mEleItemInfo.excellentid == itemEle.mEleItemInfo.excellentid )
			return true;

		return false;
	}

	void IncItemCount() { mEleItemInfo.ucCount ++ ; }

	const PlayerID& GetOwnerID(){ return mOwnerID; }

	void  SetOwnerID( const PlayerID& ownerID ) { mOwnerID = ownerID; }

	bool  IsHaveOwner() { return mOwnerID.dwPID !=0 ; }

	void Clear() 
	{ 
		mEleItemInfo.usItemTypeID = mEleItemInfo.usWearPoint = mEleItemInfo.uiID =  mEleItemInfo.luckyid = mEleItemInfo.excellentid = mEleItemInfo.ucCount = 0; 
		mEleItemInfo.randSet/*usAddId*/ = 0;
	}

public:
	//roll点物品trigger超时
	void CheckTriggerTimeout(ITEM_MASSLEVEL rollSet);
	//是否roll点物品
	bool IsIncludedRollSet(ITEM_MASSLEVEL rollSet);
	//检查roll点物品状态,并判断是否可以捡起
	bool CheckRollItemState(PlayerID playerid, bool isTeamMember, CItemPkgEle::RollItemState &currentState);
	//更新Roll状态
	void UpdateRollState(RollItemState newState);
	//设置rollid
	void SetRollID(unsigned int id) {roleId = id;}
	//获取rollid
	unsigned int GetRollID(void){ return roleId;}

private:
	ItemInfo_i mEleItemInfo;//道具包中元素的基本信息
	unsigned int mEleTaskID;//任务物品的ID号,非任务物品为0
	unsigned int mElePrice;//物品的价钱
	ITEM_MASSLEVEL mEleMassLevel;//物品的材质等级
	bool mEleLock;
	PlayerID mOwnerID;

private:
	RollItemState rollState;
	unsigned int roleId;
};

//比较函数，用于Sort排序
class ItemEleCompareFunc:public std::binary_function<CItemPkgEle,CItemPkgEle,bool>
{
public:
	bool operator()( const CItemPkgEle& pItem1,const CItemPkgEle& pItem2 ) const
	{

		unsigned int taskID1 = pItem1.GetTaskID();
		unsigned int taskID2 = pItem2.GetTaskID();

		//如果是任务道具
		if ( taskID1 > taskID2 )
		{
			return true;
		}
		if ( pItem1.GetMassLevel() > pItem2.GetMassLevel() )
		{
			return true;
		}
		else
		{
			if ( pItem1.GetMassLevel() == pItem2.GetMassLevel() )
			{
				if ( pItem1.GetPrice() > pItem2.GetPrice() )
				{
					return true;
				}
			}
		}
		return false;
	}
};


//@brief:物件包,包含有每次生成的道具
class CItemsPackage
{
	friend class CItemPackageManager;

public:
	explicit CItemsPackage( PlayerID Owner, CNewMap* _map ):m_pkgIndex(0),x(0),y(0),m_monsterID(0),m_bPass60s(false),m_map(_map),m_Owner(Owner),mIsOpen(false),mPkgMoney(0),mMoney(0),
		mPkgMaxMassLevel(E_GRAY),mItemShareMode(E_TEAM_MODE/*E_TURN_MODE,缺省值变为队伍拾取(zsw/2010.8.13)*/),mItemMassLevel(E_ORANGE),bIsRoll(false),bGroupPick(false),rollCount(0),
		groupTeamID(0),mItemPkgType(0),mItemLiveTime(120){}

	explicit CItemsPackage( CNewMap* _map ):m_pkgIndex(0),x(0),y(0),m_monsterID(0),m_bPass60s(false),m_map(_map),mIsOpen(false),mPkgMoney(0),mMoney(0),
		mPkgMaxMassLevel(E_GRAY),mItemShareMode(E_TEAM_MODE/*E_TURN_MODE,缺省值变为队伍拾取(zsw/2010.8.13)*/),mItemMassLevel(E_ORANGE),bIsRoll(false),bGroupPick(false),rollCount(0),
		groupTeamID(0),mItemPkgType(0),mItemLiveTime(120)
	{
		m_Owner.dwPID = m_Owner.wGID  = 0;
	}

	~CItemsPackage()
	{
	}

private:
	CItemsPackage& operator=( const CItemsPackage& itemPkg );
	CItemsPackage( const CItemsPackage& itemPkg );

public:
	u32  GetItemsPackageIndex() { return m_pkgIndex; }
	bool RemoveItemEle( CItemPkgEle& itemEle );
	PlayerID&  GetOwner(){ return m_Owner; }
	CNewMap* GetOwnerMap() { return m_map; }
	void  SortItemEle();
	int  FindItemIndex( CItemPkgEle& itemEle );

public:
	//@brief:当玩家提取一个道具时
	bool OnPickOneItem( CPlayer* pPlayer ,unsigned long itemIndex,int pkgIndex = -1 );

	//@brief:当玩家提取所有道具时
	bool OnPickAllItems( CPlayer* pPlayer );

	//@brief:
	bool CheckItemPackEleEmpty();

	//@brief: 拾取道具失败
	void PickItemFailed( CPlayer* pPlayer, unsigned itemID ,int reason );

	//@brief:当道具包过期时
	void OnLiveEnd();

	//@brief:获取包创建的时间
	ACE_Time_Value GetItemPkgCreateTime() { return m_CreateTime ; }

	//@brief:设置包创建的时间
	void SetItemCreateTime( ACE_Time_Value& time ) { m_CreateTime = time ; }

	//@brief:设置一个道具包的编号
	void SetItemPkgIndex( u32 index ) { m_pkgIndex = index ; }

	//@brief:设置包的生成位置
	void SetItemPkgPos( s32 _x, s32 _y ) {  x= _x; y = _y ;};

	//@brief:包裹打开过了60秒钟
	void PassSixtySeconds(){ m_bPass60s = true;  DisableRollPrivilege();}

	//包裹是否已经过了60秒
	bool IsPassSixtySeconds() { return m_bPass60s; }

	//包裹的X坐标
	s32  GetItemPosX() { return x; }

	//包裹的Y坐标
	s32  GetItemPosY() { return y; }

	//包裹所属的地图
	void SetItemPkgOwnerMap( CNewMap* pMap ) { m_map = pMap ; }


	void OnQueryItemPkgEleConciseInfo(  CPlayer* pPlayer, MGItemPackInfo* itempkgInfo  );

	void SetMonsterID( long monsterID ) { PlayerID tmpID; tmpID.wGID = 1000; tmpID.dwPID = monsterID; SetDropOwner(tmpID); }

	void SetPlayerID(  PlayerID playerID ) { SetDropOwner(playerID); }

	void SetDropOwner( PlayerID dropOwnerID ) { m_dropOwnerID = dropOwnerID; } 

	PlayerID GetDropOwnerID() { return m_dropOwnerID; }

	//@当玩家查询该包信息时
	void OnQueryItemPkgDetailInfo( CPlayer* player, unsigned int queryType );

	//@通知其他人，一个玩家提取道具包中的一个道具,通知其他玩家打开该包裹的玩家减少该道具
	void NoticeOnePlayerPickItem( unsigned long itemID,bool isMoney );

	//@brief 通知周围的玩家
	template<class T>
	void OnNoticSurroundPlayer( T& _msg );

	void NoticePlayerItemPkgAppear( CPlayer* pPlayer );

	void OnRollItemToPlayer( CPlayer* pPlayer,CItemPkgEle& pItem, unsigned int rollId );

	void MoneyAutoInPackage();//钱自动进包

//////////////////////////////////////////////////////////////////////////
//M2新的需求

	//道具包是否被人打开过 新需求
	bool IsOpenItemPkg() { return mIsOpen; }

	//获取道具包被打开的时间
	const ACE_Time_Value& GetItemPkgOpenedTime() { return m_openTime; } 

	//获取包裹的道具的最大材质等级
	unsigned int GetItemPkgType() { return mItemPkgType; }

	//获取道具包中的道具列表
	std::vector<CItemPkgEle>& GetItemPkgItemEleVec() { return mItemEleVec; }

	//设置道具包所拥有的金钱数目
	void SetItemPkgOwnerMoney( long money ) { mPkgMoney =  mMoney = money; } 

	//道具包中是或否有金钱
	bool ItemPkgHaveMoney() { return mMoney!=0 ; }

	//是否曾经有金钱
	bool IsMoneyPkg() { return mPkgMoney!=0; }

//////////////////////////////////////////////////////////////////////////
	//设置道具的所属玩家
	void SetOwner( CPlayer* pPlayer );
	//设置道具的拾取模式为组队拾取模式
	void SetGroupPickMode( CGroupTeam* pGroupTeam );
	//是否处于组队模式拾取
	bool IsGroupPickMode() { return  bGroupPick; }
	//玩家是否能拾取该道具包
	bool IsPlayerCanPickItemPkg( CPlayer* pPlayer );
	//是否应该Roll点
	bool IsShouldRollItem() { return bIsRoll && bGroupPick; }
	//Roll点道具以后
	void  AfterBeginRollItem() { bIsRoll = false; }
	//获取道具包的拾取规则
	ITEM_SHAREMODE GetItemPkgPickMode() { return mItemShareMode; }
	//现在包裹中最大的道具等级
	ITEM_MASSLEVEL GetItemPkgMaxMassLevel();
	//设置包裹的生存期
	void SetItemLiveItem( unsigned int liveTime ) { mItemLiveTime = liveTime; } 
	//获取包裹的生存期
	unsigned int GetLiveTime() { return 60 * 3/*改为6分钟mItemLiveTime*/; }

///Roll物品
	//开始Roll点道具
	void BeginRollItem();
	//增加道具包中Roll点的个数
	void IncRollCount() { rollCount++; }
	//减少道具包中的Roll点的道具个数
	void DecRollCount() { rollCount--; }
	//该道具包是否在Roll点
	bool IsRolling() { return rollCount != 0; }
	//锁定包裹中的道具
	void LockItem( unsigned int itemTypeID );
	//解除锁定包裹中的道具
	void UnLockItem( unsigned int itemTypeID );
	//设置Roll点后的物品的所有权
	void SetRollItemOwner( unsigned int itemTypeID, const PlayerID& playerID );

	//是否可以立即拾取某物品
	bool CheckPickPrivilege( CPlayer* pPlayer ,unsigned long itemIndex);

	//从包裹中拾取一个物品
	bool OnNewPickOneItem( CPlayer* pPlayer ,unsigned long itemIndex,int pkgIndex = -1 );

	//队伍拾取Roll点失效
	void DisableRollPrivilege(void);

	//获取包裹中的物品根据物品类型编号
	CItemPkgEle* GetEleByItemTypeId(unsigned int itemTypeId);

private:
	std::vector<CItemPkgEle> mItemEleVec;//用vector记录每个包裹里的道具
	u32  m_pkgIndex;
	s32  x;//道具包的具体方位 X
	s32  y;//道具包的具体方位 Y
	long m_monsterID;//创建包裹时,包裹所属的怪物ID 
	PlayerID  m_dropOwnerID;//掉落所有者的ID
	bool m_bPass60s;//是否道具是否过了60秒钟 
	ACE_Time_Value m_CreateTime;//道具包的创建时间
	CNewMap* m_map;

private:
	PlayerID  m_Owner;//包裹的所有权
	bool mIsOpen;//道具包是否打开过
	ACE_Time_Value m_openTime;//道具包打开的时间
	long    mMoney;//道具包所含有的金钱数
	long    mPkgMoney;
	ITEM_MASSLEVEL mPkgMaxMassLevel;//道具包的最大等级

private:
	ITEM_SHAREMODE  mItemShareMode;//道具的分享方式
	ITEM_MASSLEVEL  mItemMassLevel;//道具的Roll等级
	bool bIsRoll;//对于高级物品，Roll方式拾取
	bool bGroupPick;//是否时组队拾取
	unsigned int rollCount;//正在Roll的物品数
	unsigned int groupTeamID;//组队队伍的ID号
	std::vector<CPlayer *> mCanPickPlayer;//本次能被拾取的人

private:
	int mItemPkgType;//包裹的掉落类型
	unsigned int mItemLiveTime;//包裹的生存期
};


class CMonster;

//@brief:道具包管理
class CItemPackageManager
{
	friend class ACE_Singleton<CItemPackageManager, ACE_Null_Mutex>;

private:
	CItemPackageManager()
	{
		m_currentTime = time(NULL);
		m_lastUpdateTime = m_currentTime;
		m_PackageIndex = 1;
	};

public:
	~CItemPackageManager()
	{
		Release();
	}


public:
	//@死亡一个怪物,创建一个道具包
	CItemsPackage* CreateItemPackageEx( CMonster* pDeadMonster );

	//@死亡玩家掉道具包的处理
	CItemsPackage* DiedPlayerDropItems( CPlayer* pDeadPlayer );

	//@回收一个道具包(以后打算做成内存池)
	bool           RecoverItemPackage( CItemsPackage*  pTmpPackage );

	//@从管理队列中删除道具包
	bool           RemoveItemPackage( CItemsPackage* pTmpPackage );

	//@监视道具包是否过期
    void           MonitorItemPkgLive();

	//@释放所有的道具包
	void           Release();

	//@获取道具包信息
	CItemsPackage* GetItemPackage( s32 pkgIndex );

	//获取包裹的生存时间
	unsigned int   GetPkgItemLiveTime( unsigned int type ); 

	//获取死亡玩家包裹的生存周期,测试用列说是 3mins
	unsigned int   GetDeadPlayerItemPkgLiveTime() { return 180; }

public:
	//读取包裹生存时间配置表
	void           ReadItemPkgLiveTime( const char* pkgConfigXML );

private:
	void IncPackageIndex() { m_PackageIndex++; }
	u32  GetPackageIndex() { return m_PackageIndex; } 

private:
    u32 m_PackageIndex;//道具包的编号
	std::map<s32,CItemsPackage *> m_PackageMan;
	std::map<s32,unsigned int>    m_PackageTypeLiveTime;

	time_t m_currentTime;
	time_t m_lastUpdateTime;
};

typedef ACE_Singleton<CItemPackageManager, ACE_Null_Mutex> CItemPackageManagerSingle;
