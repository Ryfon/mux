﻿/********************************************************************
	created:	2008/08/13
	created:	13:8:2008   16:29
	file:		ItemLeavelUpCri.h
	author:		管传淇
	
	purpose:	
*********************************************************************/


#ifndef ITEMLEAVELUPCRI_H_
#define ITEMLEAVELUPCRI_H_

#include <vector>

struct  LevelUpCri
{
	unsigned int level;
	unsigned int  odd;
	unsigned int  Sumodd;
	float  cri;
	int    updateFailstart;
	int    updateFailEnd;

	LevelUpCri():level(0),odd(0),Sumodd(0),cri(0.0f),updateFailstart(0),updateFailEnd(0) {}

	void Reset()
	{
		level = odd = Sumodd = 0;
		cri = 0.0f;
		updateFailstart = updateFailEnd = 0;
	}
};

class CElement;

class CItemLeavelUpCriManager
{
public:
	//从XML文件中读取文件
	static void LoadXML( const char* pszXMLFile );
	//添加升级的系数
	static void PushLevelUpCri(  CElement* pxmlElement );
	//传入等级，根据具体等级，判断是否应该升级
	static bool IsLevelUp( unsigned int level );

	static LevelUpCri* GetLevelUpCri( unsigned int level );

private:
	static std::vector<LevelUpCri> mLevelUpCriVec;
};

struct LevelUpModulus
{
	unsigned int brand;
	unsigned int Modulus;
};

class CItemLevelUpModulus
{
public:
	//从XML文件中读取文件
	static void LoadXML( const char* pszXMLFile );
	//添加升级系数
	static void PushLevelUpModulus( CElement* pxmlElement );
	//获取道具的升级系数
	static unsigned int GetLevelUpModulus( unsigned int brand ); 

private:
	static std::vector<LevelUpModulus> mLevelUpModulus;

};


#endif
