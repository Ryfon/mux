﻿/********************************************************************
	created:	2008/08/15
	created:	15:8:2008   10:13
	file:		ItemUpdateConfig.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef ITEMUPDATECONFIG_H
#define ITEMUPDATECONFIG_H

#include "../../../Base/PkgProc/CliProtocol_T.h"
using namespace MUX_PROTO;

struct PlateCondition;
struct LevelUpCri;
class CPlayer;

class ItemUpdateConfig
{
public:
	//加载升级的各项属性，升级包括升级，追加，改造
	static bool LoadItemUpdateConfig();

	//读取升级的各项属性
	static bool ReadLevelUpMaterial( CPlayer* pPlayer, MUX_PROTO::ItemInfo_i &mainItem, PlateCondition *levelUpCondition, unsigned int& levelUpID  );
	//读取升级的概率
	static bool ReadLevelCri( CPlayer* pPlayer, unsigned int levelUp , LevelUpCri& levelUpCri );
	//读取改造道具的概率
	static bool ReadChangeMaterial( CPlayer* pPlayer, MUX_PROTO::ItemInfo_i &mainItem, PlateCondition *changeCondition );
};


#endif
