﻿/** @file CEquipItemManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-5-4
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/


#pragma once 

class CBaseItem;
class CPlayer;

#include "aceall.h"
#include "../../../Base/PkgProc/CliProtocol_T.h"
using namespace MUX_PROTO;


//装备规则,以后可能将对应的各项装备规则做成相应的类来完成
//现在尽用纯粹的逻辑判断 

class CEquipItemManagerEx
{
	friend class ACE_Singleton<CEquipItemManagerEx, ACE_Null_Mutex>;

public:
	CEquipItemManagerEx(){}

	~CEquipItemManagerEx(){}

public:
	//@brief:装备物品,道具的唯一编号
	void UseItem( CPlayer* pPlayer, unsigned long itemUID );

	//@整理背包
	void OnSortPkgItem(  CPlayer* pPlayer , unsigned int labelIndex );

	//@brief:返回装备的结果
	void OnUseItemResult( CPlayer* pPlayer, const ItemInfo_i& itemInfo );

	//@brief:使用道具失败
	void OnUseItemFailed( CPlayer* pPlayer, unsigned long itemUID );

	//@brief:不使用道具
	void UnUseItem( CPlayer* pPlayer, unsigned long itemUID, unsigned int fromEquipPos, unsigned int toPkgPos );

	//@brief:使用道具的结果
	void OnUnUseItemResult( CPlayer* pPlayer, unsigned itemTypeID, unsigned itemUID );

	//装备道具
	void EquipItem( CPlayer* pPlayer, unsigned long itemUID, unsigned int equipIndex, unsigned int equipReturnIndex=0 );

	//交换包裹中的物品
	void SwapItemOnItemPkg( CPlayer* pPlayer, unsigned itemFrom, unsigned itemTo );

	//合并物品
	bool CombindItem( CPlayer* pPlayer, ItemInfo_i& itemFrom, unsigned int from, ItemInfo_i& itemTo, unsigned int To  );

	//使用攻击性消耗道具时
	void UseAttackConsumeItem( CPlayer* pPlayer, unsigned int itemUID, unsigned int x,unsigned int y, PlayerID& targetID );

	//一键换装
	int ChanageSuit( CPlayer *pPlayer, BYTE suitIndex, bool forceRepair = false);
};

typedef ACE_Singleton<CEquipItemManagerEx, ACE_Null_Mutex> EquipItemManagerSingle;
