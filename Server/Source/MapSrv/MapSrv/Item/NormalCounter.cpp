﻿/**
* @file NormalCounter.cpp
* @brief 一般计数器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: NormalCounter.cpp
* 摘    要: 计数器定义
* 作    者: dzj
* 完成日期: 20008.06.24
*
*/

#include "NormalCounter.h"

#include "../Player/Player.h"

//map< unsigned int, NormalCounterProp* > CNormalCounterPropManager::m_mapCounterProp;//各种类型的计数器属性；

/////计数器到达时调用脚本;
//bool NormalCounterProp::CounterMeetCallScript( CPlayer* pOwner /*计数器拥有者*/)
//{
//	if ( NULL != m_pCounterScript )
//	{
//		return m_pCounterScript->OnMeetCounter( pOwner );
//	}
//
//	return true;
//}

/////初始化计数器管理器，读入各计数器属性；
//bool CNormalCounterPropManager::Init() 
//{ 
//  TRY_BEGIN;
//	// 
//	Release();
//
//	// 装载
//	CXmlManager xmlManager;
//	if( !xmlManager.Load("config/item/counter.xml") )
//	{
//		D_ERROR("计数器配置信息加载出错\n");
//		return false;
//	}
//
//	// 根元素
//	CElement *pRoot = xmlManager.Root();
//	if(NULL == pRoot)
//	{
//		D_ERROR("计数器配置信息可能为空\n");
//		return false;
//	}
//
//	// 根元素的所有子元素
//	std::vector<CElement *> childElements;
//	xmlManager.FindChildElements(pRoot, childElements);
//
//	//<counter CounterID="10001" CounterType="1" TargetObjID="10001"/>
//	unsigned int counterID = 0;
//	COUNTER_TARGET_TYPE counterType = CT_NPC_KILL;
//	unsigned int targetObjID = 0;
//
//	CElement* pElement = NULL;
//	int curpos = 0;
//	for ( vector<CElement *>::iterator iter=childElements.begin(); childElements.end()!=iter; ++iter )
//	{
//		curpos = (int) ( iter -childElements.begin() );
//		pElement = *iter;
//		if(pElement->Level() == 1)
//		{
//			if ( !IsXMLValValid( pElement, "CounterID", curpos ) )
//			{
//				return false;
//			}	
//			counterID = atoi( pElement->GetAttr( "CounterID" ) );
//			if ( !IsXMLValValid( pElement, "CounterType", curpos ) )
//			{
//				return false;
//			}	
//			counterType = (COUNTER_TARGET_TYPE)( atoi( pElement->GetAttr("CounterType") ) );
//			if ( !IsXMLValValid( pElement, "TargetObjID", curpos ) )
//			{
//				return false;
//			}	
//			targetObjID = atoi( pElement->GetAttr( "TargetObjID" ) );
//
//			CNormalScript* pCounterScript = CNormalScriptManager::FindScripts( counterID );
//			if ( NULL == pCounterScript )
//			{
//				D_ERROR( "找不到计数器%d对应的完成脚本", counterID );
//				return false;
//			}
//
//			//读到了一个完整的计数器属性；
//			NormalCounterProp* newCounterProp = NEW NormalCounterProp;
//			newCounterProp->Init( counterID, counterType, targetObjID, pCounterScript );
//
//			m_mapCounterProp.insert( pair<unsigned int, NormalCounterProp*>( counterID, newCounterProp ) );
//		}//if(pElement->Level() == 1)
//	}//for ( vector<CElement *>::iterator iter=childElements.begin()...
//
//	return true; 
//	TRY_END;
//  return false;
//};

///事件发生时try inc counter，影响因素，事件类型，事件相关参数(例如杀怪类型，数量，收集物类型，收集物数量);
///    如果本计数器对输入事件发生响应(输入事件与本计数器类型一致)且响应后计数条件已满足，函数返回true，否则返回false;
bool CNormalCounter::OnCounterEvent( CPlayer* pOwner, COUNTER_TARGET_TYPE ctType/*发生的事件类型*/, unsigned int reasonObjID/*事件相关物体类型*/, unsigned int reasonObjNum/*事件相关物体数量*/ )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( !( m_CounterProp.IsAccord(ctType, reasonObjID) ) )
	{
		//发生的事件与本计数器无关
		return false;
	}

	//发生的事件为本计数器关心事件；
	m_curCount += reasonObjNum;

	unsigned taskID = m_CounterProp.GetCoresTaskID();
	//如果任务已完成，则不通知客户端改变
	if ( pOwner->GetTaskStatus( taskID ) == NTS_TGT_ACHIEVE )
		return true;
	else
	{
		//检查任务是否达到了目标
		if ( !CheckCounterIsFinish(pOwner) )
		{
			//如果没有达到目标，则更新现在的计数器
			pOwner->GetTaskRecordManager().OnTaskCounterNumChange( m_CounterProp.GetCoresTaskID(), m_curCount );
			return false;
		}

	}
	return true;
}

bool CNormalCounter::OnDropItemCounterEvent(CPlayer* pOwner, COUNTER_TARGET_TYPE ctType, unsigned int reasonObjID, unsigned int reasonObjNum )
{
	if ( !pOwner )
		return false;

	if ( !( m_CounterProp.IsAccord(ctType, reasonObjID) ) )
	{
		//发生的事件与本计数器无关
		return false;
	}

	bool isOver1 = false; 
	bool isOver2 = false;
	//丢弃之前是否已经完成了
	if( m_curCount >= m_CounterProp.m_targetCount )
		isOver1 = true;

	if ( reasonObjNum >= m_curCount )
		m_curCount = 0;
	else
		m_curCount -=reasonObjNum;

	//丢弃之后是否还是完成状态
	if( m_curCount >= m_CounterProp.m_targetCount )
		isOver2 = true;

	//任务ID号
	unsigned int taskID    = m_CounterProp.GetCoresTaskID();
	//如果前后两个状态改变了
	if ( isOver1 != isOver2 )
	{
		//如果原来是完成的，现在丢弃了道具，则处于不完成状态
		if ( pOwner->GetTaskStatus( taskID ) == NTS_TGT_ACHIEVE )
		{
			pOwner->SetTaskStat( taskID, NTS_TGT_NOT_ACHIEVE );//将玩家对应任务设为目标未达成；
		}
		//更新最新的任务表示
		pOwner->GetTaskRecordManager().OnTaskCounterNumChange( taskID , m_curCount );
	}
	return true;
}

bool CNormalCounter::CheckCounterIsFinish( CPlayer* pOwner )
{
	if ( !pOwner )
		return false;

	unsigned taskID = m_CounterProp.GetCoresTaskID();

	//如果之前未完成，而现在完成了，通知客户端更新状态
	if ( pOwner->GetTaskStatus( taskID ) == NTS_TGT_NOT_ACHIEVE )
	{
		unsigned targetCount = m_CounterProp.GetTargetCount();
		if ( m_curCount >= targetCount )
		{
			pOwner->GetTaskRecordManager().OnTaskCounterNumChange( taskID, targetCount );
			pOwner->SetTaskStat( taskID, NTS_TGT_ACHIEVE );//将玩家对应任务设为目标达成；
			return true;
		}
	}

	return false;
}

bool CNormalCounter::CheckItemPkgCounterNum(CPlayer *pOwner)
{
	if ( !pOwner )
		return false;

	//通知客户端重新计数
	m_curCount = 0 ;

	FullPlayerInfo * playerInfo = pOwner->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CNormalCounter::CheckItemPkgCounterNum, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& pkg = playerInfo->pkgInfo;
	for ( unsigned int i = 0 ; i< PACKAGE_SIZE; i++ )
	{
		ItemInfo_i& itemInfo = pkg.pkgs[i];
		if (!pOwner->IsLeftUpPkgIndex(i,itemInfo.uiID))
		{
			continue;
		}
		if( m_CounterProp.IsAccord( CT_COLLECT,itemInfo.usItemTypeID ) )
		{
			if(  OnCounterEvent( pOwner, CT_COLLECT, itemInfo.usItemTypeID, itemInfo.ucCount ) )
				break;
		}
	}
	return true;
}

void CNormalCounter::RefreshCounterNum( CPlayer* pOwner , unsigned int taskID )
{
	if ( !pOwner )
		return;

	m_curCount = 0;
	pOwner->GetTaskRecordManager().OnTaskCounterNumChange( taskID, m_curCount );
}




