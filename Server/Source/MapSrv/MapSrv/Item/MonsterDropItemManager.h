﻿/** @file MonsterDropItemManager.h
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-6-16
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/


#ifndef MONSTER_DROPITEM_MANAGER_H
#define MONSTER_DROPITEM_MANAGER_H

#include "../../../Base/Utility.h"
#include <vector>
#include <map>
#include <algorithm>
#include <functional>
#include <time.h>


class CBaseItem;

enum RULETYPE
{
	E_DAY = 0x0,
	E_DARK,
	E_RULETYPEMAX,
};

//掉落规则
struct DropRule
{
public:
	long itemID;//道具编号
	long startIndex;//起始编号
	long endIndex;//结束编号
public:
	DropRule():itemID(0),startIndex(0),endIndex(0){}
};

//函数对象，处理和规则的比较
class FindDropRuleFunc:public std::binary_function<DropRule,long,bool>
{
public:
	bool operator()( const DropRule& dropRule ,long index ) const
	{
		if ( (dropRule.startIndex-1) < index &&  index < (dropRule.endIndex+1) )
			return true;

		return false;
	}
};

class CItemPkgEle;

//掉落规则记录集
class DropItemRuleSet
{
public:
	explicit DropItemRuleSet( long setID/*记录集编号*/ ):mSetID(setID),mTotalRange(0),mCurrentIndex(1){}

	explicit DropItemRuleSet():mSetID(0),mTotalRange(0),mCurrentIndex(1){}

	~DropItemRuleSet()
	{
	}

public:
	void SetDropRuleInfo( long setID ) { mSetID = setID; mTotalRange = 0 ; mCurrentIndex =1 ;}

	long GetStartRangeIndex() { return mCurrentIndex; }

	long GetEndRangeIndex( long range ) 
	{ 
		mCurrentIndex += range;
		return mCurrentIndex-1;
	}

	int RandonDropItem( std::vector<CItemPkgEle>& itemEleVec, int randCount );

public:
	void PushDropConfig( long itemID, long range );

	long GetRuleSetID() { return mSetID; } 

	//增加非随机的范围,从而确立该记录集的随机总范围
	void AddUnRandRange( long unRandRange ) { mTotalRange+= unRandRange; } 


private:
	std::vector<DropRule> mDropConfigVec;//道具生成规则
	long mSetID;//道具集的编号
	long mTotalRange;//随机数的范围
	long mCurrentIndex;
};

class CDropItemRuleSetManager
{
public:
	CDropItemRuleSetManager():dropSetIndex(0){}

	virtual ~CDropItemRuleSetManager();

private:
	CDropItemRuleSetManager (const CDropItemRuleSetManager& );
	CDropItemRuleSetManager& operator = (const CDropItemRuleSetManager& );

public:
	DropItemRuleSet* NewDropItemRule( long setID );

	DropItemRuleSet* GetDropItemRule( long setID );

	virtual RULETYPE GetRuleType() { return E_RULETYPEMAX ; }

private:
	DropItemRuleSet dropSetArr[40000];
	unsigned int dropSetIndex;
};

//白天的掉落规则
class CDayItemDropRuleManager:public CDropItemRuleSetManager
{
public:
	friend class ACE_Singleton<CDayItemDropRuleManager, ACE_Null_Mutex>;	
};
typedef ACE_Singleton<CDayItemDropRuleManager, ACE_Null_Mutex> DayDropRuleManagerSingle;

//晚上的掉落规则
class CDarkItemDropRuleManager:public CDropItemRuleSetManager
{
public:
	friend class ACE_Singleton<CDarkItemDropRuleManager, ACE_Null_Mutex>;
};
typedef ACE_Singleton<CDarkItemDropRuleManager,ACE_Null_Mutex> DarkDropRuleManagerSingle;


//
class CDropRuleStateManager
{
	friend class ACE_Singleton<CDropRuleStateManager, ACE_Null_Mutex>;	

public:
	CDropRuleStateManager():mpDropItemRule(NULL),mCurrentState(E_RULETYPEMAX)
	{
		lastUpdateTime = (unsigned long)time(NULL);
	}

	~CDropRuleStateManager()
	{

	}

private:
	//切换状态
	void ChangeState( RULETYPE ruleType );

	int GetCurrentHour();

public:
	//检查掉落状态
	void CheckRuleState();

	//获取现在的掉落规则
	CDropItemRuleSetManager* CurrentDropRuleState() { return mpDropItemRule; }

	//监视是否需要切换状态
	void MonitorCheckTime();

	//返回当前的状态
	RULETYPE GetCurrentRuleType() { return mCurrentState; }

private:
	CDropRuleStateManager( CDropRuleStateManager& );
	CDropRuleStateManager& operator=( const CDropRuleStateManager& );

private:
	CDropItemRuleSetManager* mpDropItemRule;
	RULETYPE         mCurrentState;//当前的状态
	unsigned long    lastUpdateTime;
};
typedef ACE_Singleton<CDropRuleStateManager,ACE_Null_Mutex> DropRuleStateSingle;


#endif
