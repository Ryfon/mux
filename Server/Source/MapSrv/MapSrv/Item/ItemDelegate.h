﻿/********************************************************************
	created:	2008/09/04
	created:	4:9:2008   9:56
	file:		ItemDelegate.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef ITEMDELEGATE_H
#define ITEMDELEGATE_H


#include <vector>

class  CPlayer;

//#include "../../../Base/PkgProc/CliProtocol_T.h"
#include "CItemPackage.h"
//using namespace MUX_PROTO;

class ItemDelegate
{
public:
	ItemDelegate():mpAttachPlayer(NULL),mEquipValidPos(0){}

	void PushEquipItem( CBaseItem* pItem );

	void PushPkgItem( CBaseItem* pItem );

	void PushFashionItem( CBaseItem *pItem);

	void UnEquipItem( CBaseItem* pItem );

	void EquipItem( CBaseItem* pItem );

	void SetEquipPosValid(unsigned int pos)
	{
		if (pos > EQUIP_SIZE)
		{
			return;
		}
		mEquipValidPos |= (1<<pos);
	}

	void SetEquipPosUnValid(unsigned int pos)
	{
		if (pos > EQUIP_SIZE)
		{
			return;
		}
		mEquipValidPos &= (~(1<<pos));
	}

	bool IsEquipPosValid( unsigned int pos )
	{
		if (pos > EQUIP_SIZE)
		{
			return false;
		}
		return (mEquipValidPos & (1<<pos)) != 0;
	}

	bool DropPkgItem( unsigned int itemUID );

	bool DropEquipItem( unsigned int itemUID );

	void OnFashionItem( CBaseItem* pItem );

	void OffFashionItem(  CBaseItem* pItem );

	bool OnPlayerExit();

	void RemovePkgItem( unsigned int itemUID );

	//获取包裹中的道具数目 
	unsigned int GetItemCountOnPkgs() { return (unsigned int)mPkgItemMap.size(); }

	//获取装备中的道具数目
	unsigned int GetItemCountOnEquip() { return (unsigned int)mEquipItemMap.size(); }

	////依附与一个玩家,并记录下该玩家的所有道具信息
	void AttachPlayer( CPlayer* pAttachPlayer );

	//死亡掉落包裹的逻辑
	void OnPlayerDieDropItem( std::vector<CItemPkgEle>& dropItemEle,unsigned int& money );

	//依据包裹中的编号，获取道具
	CBaseItem* GetEquipItemByIndex( unsigned int index ); 

	CBaseItem* GetPkgItem( unsigned itemUID );

	CBaseItem* GetEquipItem( unsigned itemUID );

	CBaseItem* GetFashionItem( unsigned itemUID);

	//依据道具ID号查找道具
	CBaseItem* GetItemByItemUID( unsigned int itemUID );

	void GetCanDropPkgItem( std::vector<CBaseItem *>& unProtectPkgItemVec );

	void GetCanDropEquipItem( std::vector<CBaseItem *>& unProtectEquipItemVec );

private:
	CPlayer* mpAttachPlayer;
	std::map<unsigned int,CBaseItem *> mEquipItemMap;
	std::map<unsigned int,CBaseItem *> mPkgItemMap;
	std::map<unsigned int,CBaseItem *> mFashionItemMap;
	unsigned int mEquipValidPos;
};

#endif
