﻿#include "ItemLevelUpAttribute.h"
#include "XmlManager.h"
#include "../../../Base/Utility.h"

std::map< unsigned int, LevelUpProp> ItemLevelUpAttributeManager::mItemLevelUpPropManager;

void ItemLevelUpAttributeManager::LoadXML(const char* pszFileName )
{
	if ( !pszFileName )
		return;

	if ( !pszFileName )
		return;

	D_DEBUG("LoadXML:%s\n", pszFileName);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszFileName );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszFileName);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	for ( std::vector<CElement *>::iterator lter = childElements.begin();
		lter!= childElements.end();
		++lter )
	{
		PushLevelUpAttribute( *lter );
	}

	TRY_END;
}

void ItemLevelUpAttributeManager::PushLevelUpAttribute(CElement* pxmlElement )
{
	if ( !pxmlElement )
		return;

	unsigned int LevelUpId  = 0;

	const char* p  = pxmlElement->GetAttr("LevelUpId");
	if ( p )
		LevelUpId = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性LevelUpId出错\n");
		return;
	}

	LevelUpProp levelUpprop;

	//AttackPhyMin
	p = pxmlElement->GetAttr("AttackPhyMin");
	if ( p )
		levelUpprop.AttackPhyMin = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性AttackPhyMin出错\n");
	}

	//AttackPhyMax
	p = pxmlElement->GetAttr("AttackPhyMax");
	if ( p )
		levelUpprop.AttackPhyMax = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性AttackPhyMax出错\n");
	}

	//AttackMagMin
	p = pxmlElement->GetAttr("AttackMagMin");
	if ( p )
		levelUpprop.AttackMagMin = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性AttackMagMin出错\n");
	}

	//AttackMagMax
	p = pxmlElement->GetAttr("AttackMagMax");
	if ( p )
		levelUpprop.AttackMagMax = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性AttackMagMax出错\n");
	}

	//PhysicsAttack
	p = pxmlElement->GetAttr("PhysicsAttack");
	if ( p )
		levelUpprop.PhysicsAttack = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性PhysicsAttack出错\n");
	}

	//MagicAttack
	p = pxmlElement->GetAttr("MagicAttack");
	if ( p )
		levelUpprop.MagicAttack = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性MagicAttack出错\n");
	}

	//PhysicsDefend
	p = pxmlElement->GetAttr("PhysicsDefend");
	if ( p )
		levelUpprop.PhysicsDefend = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性PhysicsDefend出错\n");
	}

	//MagicDefend
	p = pxmlElement->GetAttr("MagicDefend");
	if ( p )
		levelUpprop.MagicDefend = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性MagicDefend出错\n");
	}

	p = pxmlElement->GetAttr("Speed");
	if ( p )
		levelUpprop.speed = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemLevelUpAttributeManager 加载属性Speed出错\n");
	}

	p = pxmlElement->GetAttr("nEquipStrAdd");
	if ( p )
	{
		levelUpprop.equipStrAdd = ACE_OS::atoi( p );
	} else {
		D_ERROR("ItemLevelUpAttributeManager 加载属性nEquipStrAdd出错\n");
	}

	p = pxmlElement->GetAttr("nEquipIntAdd");
	if ( p )
	{
		levelUpprop.equipIntAdd = ACE_OS::atoi( p );
	} else {
		D_ERROR("ItemLevelUpAttributeManager 加载属性nEquipIntAdd出错\n");
	}

	p = pxmlElement->GetAttr("nEquipAgiAdd");
	if ( p )
	{
		levelUpprop.equipAgiAdd = ACE_OS::atoi( p );
	} else {
		D_ERROR("ItemLevelUpAttributeManager 加载属性nEquipAgiAdd出错\n");
	}

	p = pxmlElement->GetAttr("nEquipVitAdd");
	if ( p )
	{
		levelUpprop.equipVitAdd = ACE_OS::atoi( p );
	} else {
		D_ERROR("ItemLevelUpAttributeManager 加载属性nEquipVitAdd出错\n");
	}

	p = pxmlElement->GetAttr("nEquipSpiAdd");
	if ( p )
	{
		levelUpprop.equipSpiAdd = ACE_OS::atoi( p );
	} else {
		D_ERROR("ItemLevelUpAttributeManager 加载属性nEquipSpiAdd出错\n");
	}

	mItemLevelUpPropManager.insert( std::pair<unsigned int, LevelUpProp>(LevelUpId,levelUpprop) );

	return;
}

LevelUpProp* ItemLevelUpAttributeManager::GetLevelUpAttribute(unsigned int levelUpID )
{
	std::map< unsigned int, LevelUpProp>::iterator lter = mItemLevelUpPropManager.find( levelUpID );
	if ( lter != mItemLevelUpPropManager.end() )
	{
		return &( lter->second );
	}
	else
		return  NULL;

}

//////////////////////////////////////////////////////////////////////////
std::map< unsigned int ,ChangeProp > ItemChangeAttributeManager::mItemChangePropManager;


void ItemChangeAttributeManager::LoadXML(const char *pszFileName)
{

	if ( !pszFileName )
		return;

	if ( !pszFileName )
		return;

	D_DEBUG("LoadXML:%s\n", pszFileName);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszFileName );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszFileName);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	for ( std::vector<CElement *>::iterator lter = childElements.begin();
		lter!= childElements.end();
		++lter )
	{
		PushChangeAttribute( *lter );
	}
	TRY_END;
}

ChangeProp* ItemChangeAttributeManager::GetChangeItemAttribute(unsigned int changeID )
{
	std::map< unsigned int ,ChangeProp >::iterator lter = mItemChangePropManager.find( changeID );
	if ( lter != mItemChangePropManager.end() )
	{
		return &(lter->second);
	}

	return NULL;
}

void ItemChangeAttributeManager::PushChangeAttribute(CElement *pxmlElement)
{
	if ( !pxmlElement )
		return;

	unsigned int changeID  = 0;

	const char* p  = pxmlElement->GetAttr("ID");
	if ( p )
		changeID = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性ID出错\n");
		return;
	}

	ChangeProp changeProp;
	StructMemSet( changeProp,0x0,sizeof(ChangeProp) );

	//AttackPhyMin
	p = pxmlElement->GetAttr("AttackPhyMin");
	if ( p )
		changeProp.AttackPhyMin = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性changeProp出错\n");
	}

	//AttackPhyMax
	p = pxmlElement->GetAttr("AttackPhyMax");
	if ( p )
		changeProp.AttackPhyMax = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性AttackPhyMax出错\n");
	}

	//AttackMagMin
	p = pxmlElement->GetAttr("AttackMagMin");
	if ( p )
		changeProp.AttackMagMin = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性AttackMagMin出错\n");
	}

	//AttackMagMax
	p = pxmlElement->GetAttr("AttackMagMax");
	if ( p )
		changeProp.AttackMagMax = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性AttackMagMax出错\n");
	}

	//HP
	p = pxmlElement->GetAttr("Hp");
	if ( p )
		changeProp.Hp = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性Hp出错\n");
	}

	//MP
	p = pxmlElement->GetAttr("Mp");
	if ( p )
		changeProp.Mp = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性Mp出错\n");
	}


	//PhysicsAttack
	p = pxmlElement->GetAttr("PhysicsAttack");
	if ( p )
		changeProp.PhysicsAttack = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsAttack出错\n");
	}

	//MagicAttack
	p = pxmlElement->GetAttr("MagicAttack");
	if ( p )
		changeProp.MagicAttack = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicAttack出错\n");
	}

	//PhysicsDefend
	p = pxmlElement->GetAttr("PhysicsDefend");
	if ( p )
		changeProp.PhysicsDefend = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsDefend出错\n");
	}

	//MagicDefend
	p = pxmlElement->GetAttr("MagicDefend");
	if ( p )
		changeProp.MagicDefend = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicDefend出错\n");
	}

	//PhysicsHit
	p = pxmlElement->GetAttr("PhysicsHit");
	if ( p )
		changeProp.PhysicsHit = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsHit出错\n");
	}

	//MagicHit
	p = pxmlElement->GetAttr("MagicHit");
	if ( p )
		changeProp.MagicHit = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicHit出错\n");
	}
	
	//PhysicsCritical
	p = pxmlElement->GetAttr("PhysicsCritical");
	if ( p )
		changeProp.PhysicsCritical = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsCritical出错\n");
	}

	//MagicCritical
	p = pxmlElement->GetAttr("MagicCritical");
	if ( p )
		changeProp.MagicCritical = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicCritical出错\n");
	}

	//PhysicsAttackPercent
	p = pxmlElement->GetAttr("PhysicsAttackPercent");
	if ( p )
		changeProp.PhysicsAttackPercent = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsAttackPercent出错\n");
	}

	//MagicAttackPercent
	p = pxmlElement->GetAttr("MagicAttackPercent");
	if ( p )
		changeProp.MagicAttackPercent = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicAttackPercent出错\n");
	}

	//PhysicsCriticalDamage
	p = pxmlElement->GetAttr("PhysicsCriticalDamage");
	if ( p )
		changeProp.PhysicsCriticalDamage = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsCriticalDamage出错\n");
	}

	//MagicCriticalDamage
	p = pxmlElement->GetAttr("MagicCriticalDamage");
	if ( p )
		changeProp.MagicCriticalDamage = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicCriticalDamage出错\n");
	}

	//PhysicsDefendPercent
	p = pxmlElement->GetAttr("PhysicsDefendPercent");
	if ( p )
		changeProp.PhysicsDefendPercent = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsDefendPercent出错\n");
	}

	//MagicDefendPercent
	p = pxmlElement->GetAttr("MagicDefendPercent");
	if ( p )
		changeProp.MagicDefendPercent = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicDefendPercent出错\n");
	}

	//PhysicsCriticalDerate
	p = pxmlElement->GetAttr("PhysicsCriticalDerate");
	if ( p )
		changeProp.PhysicsCriticalDerate = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsCriticalDerate出错\n");
	}

	//MagicCriticalDerate
	p = pxmlElement->GetAttr("MagicCriticalDerate");
	if ( p )
		changeProp.MagicCriticalDerate = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicCriticalDerate出错\n");
	}

	//PhysicsCriticalDamageDerate
	p = pxmlElement->GetAttr("PhysicsCriticalDamageDerate");
	if ( p )
		changeProp.PhysicsCriticalDamageDerate = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsCriticalDamageDerate出错\n");
	}

	//MagicCriticalDamageDerate
	p = pxmlElement->GetAttr("MagicCriticalDamageDerate");
	if ( p )
		changeProp.MagicCriticalDamageDerate = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicCriticalDamageDerate出错\n");
	}

	//PhysicsJouk
	p = pxmlElement->GetAttr("PhysicsJouk");
	if ( p )
		changeProp.PhysicsJouk = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsJouk出错\n");
	}

	//MagicJouk
	p = pxmlElement->GetAttr("MagicJouk");
	if ( p )
		changeProp.MagicJouk = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicJouk出错\n");
	}

	//PhysicsDeratePercent
	p = pxmlElement->GetAttr("PhysicsDeratePercent");
	if ( p )
		changeProp.PhysicsDeratePercent = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsDeratePercent出错\n");
	}

	//MagicDeratePercent
	p = pxmlElement->GetAttr("MagicDeratePercent");
	if ( p )
		changeProp.MagicDeratePercent = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicDeratePercent出错\n");
	}

	//Stun
	p = pxmlElement->GetAttr("Stun");
	if ( p )
		changeProp.Stun = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性Stun出错\n");
	}

	//Tie
	p = pxmlElement->GetAttr("Tie");
	if ( p )
		changeProp.Tie = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性Tie出错\n");
	}

	//Antistun
	p = pxmlElement->GetAttr("Antistun");
	if ( p )
		changeProp.Antistun = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性Antistun出错\n");
	}

	//Antitie
	p = pxmlElement->GetAttr("Antitie");
	if ( p )
		changeProp.Antitie = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性Antitie出错\n");
	}

	//PhysicsAttackAdd
	p = pxmlElement->GetAttr("PhysicsAttackAdd");
	if ( p )
		changeProp.PhysicsAttackAdd = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性StunPhysicsAttackAdd\n");
	}

	//MagicAttackAdd
	p = pxmlElement->GetAttr("MagicAttackAdd");
	if ( p )
		changeProp.MagicAttackAdd = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicAttackAdd出错\n");
	}

	//PhysicsRift
	p = pxmlElement->GetAttr("PhysicsRift");
	if ( p )
		changeProp.PhysicsRift = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性PhysicsRift出错\n");
	}

	//MagicRift
	p = pxmlElement->GetAttr("MagicRift");
	if ( p )
		changeProp.MagicRift = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性MagicRift出错\n");
	}

	//AddSpeed
	p = pxmlElement->GetAttr("AddSpeed");
	if ( p )
		changeProp.AddSpeed = ACE_OS::atoi( p );
	else
	{
		D_ERROR("ItemChangeAttributeManager 加载属性AddSpeed出错\n");
	}

	mItemChangePropManager.insert( std::pair< unsigned int ,ChangeProp >( changeID,changeProp ) );
}
