﻿#include "AuctionManager.h"
#include "../Player/Player.h"
#include "CSpecialItemProp.h"


void AuctionManager::OnStartNewAuction(int pkgIndex, int origiprice, int fixprice, char expiretype )
{
	if ( !m_pAttachPlayer )
		return;

	int errNo = 0;
	do 
	{
		FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("AuctionManager::OnStartNewAuction, NULL == m_pAttachPlayer->GetFullPlayerInfo()\n");
			return;
		}
		PlayerInfo_3_Pkgs& pkgInfo = playerInfo->pkgInfo;
		if ( pkgIndex >= NORMAL_PKG_SIZE || pkgIndex < 0)
		{
			D_ERROR("%s 拍卖的物品索引号出错 %d", m_pAttachPlayer->GetNickName(),pkgIndex );
			errNo = 6;
			break;
		}

		if ( origiprice > fixprice )
		{
			D_ERROR("%s 拍卖的物品的初始价格 %d 高于一口价价格 %d\n", m_pAttachPlayer->GetNickName(),origiprice,  fixprice );
			errNo = 4;
			break;
		}


		ItemInfo_i auctionItem = pkgInfo.pkgs[pkgIndex];
		int costmoney = 0;

		CBaseItem* pAuctionItem = m_pAttachPlayer->GetItemDelegate().GetItemByItemUID( auctionItem.uiID );
		if ( !pAuctionItem )
		{
			D_ERROR("%s 拍卖的物品在玩家物品管理器中找不到\n", m_pAttachPlayer->GetNickName() );
			errNo = 6;
			break;
		}


		int expiretime = 0;
		if ( expiretype == 0 ){
			expiretime = 28800;
			costmoney  = (int)(pAuctionItem->GetItemPrice()*0.3);
		}
		else if( expiretype == 1 ){
			expiretime = 43200;
			costmoney  = (int)(pAuctionItem->GetItemPrice()*0.45);
		}
		else if( expiretype == 2){
			expiretime = 86400;
			costmoney  = (int)(pAuctionItem->GetItemPrice()*0.9);
		}

		if ( (unsigned int)costmoney > m_pAttachPlayer->GetGoldMoney() )
		{
			D_ERROR("%s 拍卖物品托管所需的费用 %d 大于玩家包裹里的金币数 %d \n", m_pAttachPlayer->GetNickName(),costmoney,m_pAttachPlayer->GetGoldMoney() );
			errNo = 1;
			break;
		}
		
		ITEM_BIND_TYPE  bindType = pAuctionItem->GetItemBindType();
		if ( bindType == E_EQUIP_BIND && ITEM_IS_EQUIPED( auctionItem.ucLevelUp ) )//装备绑定的判断
		{
			errNo =  5;
			D_DEBUG("%s 拍卖物品 %d 是装备绑定道具 ,已经被装备过了,不能被拍卖\n", m_pAttachPlayer->GetNickName(),auctionItem.usItemTypeID );
			break;
		}
		else if( bindType == E_PICK_BIND )
		{
			errNo = 5;
			D_DEBUG("%s 拍卖物品 %d 是拾取绑定道具 ,不能被拍卖\n", m_pAttachPlayer->GetNickName(),auctionItem.usItemTypeID );
			break;
		}


		//如果是走时间流逝的道具，物品不能被拍卖
		if ( ACE_BIT_ENABLED( pAuctionItem->WearConsumeMode() ,CItemPublicProp::LAST_DAY ) )
		{
			D_DEBUG("%s 走时间流逝的道具 %d 不能被拍卖\n",m_pAttachPlayer->GetNickName(), auctionItem.usItemTypeID );
			errNo = 3;
			break;
		}
		else//否则检测物品耐久度是否有损耗
		{
			if ( pAuctionItem->MaxWear() != auctionItem.usWearPoint  )
			{
				D_DEBUG("%s 拍卖的物品 %d 耐久度有损失(Max:%d Now:%d)，不能被拍卖\n", m_pAttachPlayer->GetNickName(), auctionItem.usItemTypeID ,
					pAuctionItem->MaxWear(), auctionItem.usWearPoint );
				errNo = 3;
				break;
			}
		}

		if( m_pAttachPlayer->GetProtectItemManager().FindItemIsProtected( auctionItem.uiID ) !=NULL )
		{
			D_DEBUG("%s 拍卖的物品是保护道具\n", m_pAttachPlayer->GetNickName() );
			errNo = 2;
			break;
		}

		//通知FunctionSrv开始拍卖这个物品
		MGPlayerStartNewAuctionNext nextNewAuction;
		StructMemSet( nextNewAuction, 0x0, sizeof(MGPlayerStartNewAuctionNext) );
		NewAuctionItem& newAuctionItem = nextNewAuction.startNewAuctionItem.newAuctionItem;
		nextNewAuction.startNewAuctionItem.playerID = m_pAttachPlayer->GetFullID();
		SafeStrCpy( newAuctionItem.itemName, pAuctionItem->GetItemName() );//获取道具姓名
		SafeStrCpy( newAuctionItem.ownerName, m_pAttachPlayer->GetNickName() );//拍卖玩家的姓名
		SafeStrCpy( newAuctionItem.itemClass, pAuctionItem->GetItemClass() );//道具所能装备的职业
		newAuctionItem.masslevel = (char)pAuctionItem->GetItemMassLevel();//道具的材质等级
		newAuctionItem.level     = pAuctionItem->GetItemPlayerLevel();//获取道具的等级
		newAuctionItem.equipPos  = pAuctionItem->GetItemAuctionType();//获取拍卖的类型
		newAuctionItem.expiretime = (unsigned int)time(NULL) + expiretime;//获取道具的过期时间
		newAuctionItem.fixprice   = fixprice;//道具的一口价格
		newAuctionItem.origiAuctionprice = origiprice;//道具的初始拍卖价格
		newAuctionItem.iteminfo   = auctionItem;//道具的ItemInfo信息
		newAuctionItem.playerUID  = m_pAttachPlayer->GetDBUID();//拍卖玩家的唯一编号
		m_pAttachPlayer->SendPkg<MGPlayerStartNewAuctionNext>( &nextNewAuction );

		//扣除管理费用
		m_pAttachPlayer->SubGoldMoney( costmoney );//双币种判断，拍卖固定扣金币？

		//丢弃这个道具
		m_pAttachPlayer->OnDropItem( auctionItem.uiID );

	} while( false );

	MGPlayerStartNewAuctionRst newAuctionRst;
	newAuctionRst.newAuctionRst.rstno = errNo;
	m_pAttachPlayer->SendPkg<MGPlayerStartNewAuctionRst>(&newAuctionRst);
}

void AuctionManager::OnPlayerBidItemByNewPrice(int auctionUID, int oldprice, int newprice )
{
	if ( !m_pAttachPlayer )
		return;

	int errNo = 0;
	do 
	{
		if ( newprice <= oldprice )
		{
			D_ERROR("%s 参与竞拍%d,但竞拍价格出错 oldprice:%d >= newprice:%d \n",m_pAttachPlayer->GetNickName(), auctionUID, oldprice, newprice );
			errNo = 2;
			break;
		}

		unsigned int  subprice =  newprice - oldprice;
		if ( subprice >  m_pAttachPlayer->GetGoldMoney() )
		{
			D_ERROR("%s 玩家包裹的金钱数小于竞拍价格增幅", m_pAttachPlayer->GetNickName() );
			errNo = 1;
			break;
		}

		//先扣钱，如果失败了，那么在通过消息归还给玩家
		m_pAttachPlayer->SubGoldMoney( subprice );//双币种判断，拍卖固定使用金币？

		//开始进行竞拍的下一步动作
		MGPlayerBidItemNext bidItemNext;
		StructMemSet( bidItemNext, 0x0, sizeof(bidItemNext) );
		SafeStrCpy( bidItemNext.bidItemSection2.bidPlayerName, m_pAttachPlayer->GetNickName() );
		bidItemNext.bidItemSection2.auctionUID = auctionUID;
		bidItemNext.bidItemSection2.newprice   = newprice;
		bidItemNext.bidItemSection2.lastprice  = oldprice;
		bidItemNext.bidItemSection2.playerUID  = m_pAttachPlayer->GetDBUID();
		bidItemNext.bidItemSection2.playerID   = m_pAttachPlayer->GetFullID();
		m_pAttachPlayer->SendPkg<MGPlayerBidItemNext>( &bidItemNext );

	} while( false );

	if ( errNo != 0 )
	{
		MGPlayerBidItemErr bidrst;
		bidrst.auctionUID = auctionUID;
		bidrst.rstNo      = errNo;
		m_pAttachPlayer->SendPkg<MGPlayerBidItemErr>( &bidrst );
	}
}