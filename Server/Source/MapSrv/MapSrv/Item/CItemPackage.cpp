﻿#include "CItemPackage.h"
#include "ItemManager.h"
#include "LoadDropItemXML.h"
#include "RollItemManager.h"
#include "../monster/monster.h"
#include "../MuxMap/muxmapbase.h"
#include "../GroupTeam.h"


inline void CItemPkgEle::CheckTriggerTimeout(ITEM_MASSLEVEL rollSet)
{
	if(IsIncludedRollSet(rollSet))
		UpdateRollState(TRIGGER_TIMEOUT);

	return;
}

inline bool CItemPkgEle::IsIncludedRollSet(ITEM_MASSLEVEL rollSet)
{
	bool flag = false;
	//return true;//调试

	switch(rollSet)
	{
	case E_ORANGE://橙色集(仅包含橙色品质)
		{
			if(mEleMassLevel == E_ORANGE)
				flag = true;
		}	
		break;

	case E_BLUE://蓝色集(包含橙色、蓝色、绿色品质)
		{
			if((mEleMassLevel == E_ORANGE) || (mEleMassLevel == E_BLUE) || (mEleMassLevel == E_GREEN))
				flag = true;
		}
		break;

	case E_GREEN://绿色集(包含橙色、绿色品质)
		{
			if((mEleMassLevel == E_ORANGE) || (mEleMassLevel == E_GREEN))
				flag = true;
		}
		break;

	default:
		break;
	}

	return flag;
}

inline bool CItemPkgEle::CheckRollItemState(PlayerID playerid, bool isTeamMember, CItemPkgEle::RollItemState &currentState)
{
	bool canPick = false;//是否可以捡起？
	
	switch(rollState)
	{
	case INIT://初始状态（物品创建）
		{		
		}
		break;

	case TRIGGER_TIMEOUT://未触发
		{
			canPick = true;
		}
		break;

	case TRIGGERED://已触发
		{
		}
		break;

	case ROLL_NO_RESULT://已触发但没有结果
		{
			if(isTeamMember)
				canPick = true;
		}
		break;
	case ROLL_HAVE_RESULT://已触发有结果
		{
			if(isTeamMember)
			{
				if(this->mOwnerID == playerid)
					canPick = true;
			}
		}
		break;

	case ROLL_PICK_TIMEOUT://在触发后的60秒内没有捡起(超时，任何人可以捡起)
		{
			canPick = true;
		}
		break;

	default:
		break;
	}

	currentState = rollState;
	return canPick;
}

inline void CItemPkgEle::UpdateRollState(CItemPkgEle::RollItemState newState)
{
	switch(newState)
	{
	case INIT://初始状态（物品创建）
		{			
		}
		break;

	case TRIGGER_TIMEOUT://未触发（60秒超时）
		{
			if(INIT == rollState)
				rollState = newState;
		}
		break;

	case TRIGGERED://已触发（60秒内事件触发）
		{
			if(INIT == rollState)
				rollState = newState;
		}
		break;

	case ROLL_NO_RESULT://已触发但没有结果
		{
			if(CItemPkgEle::TRIGGERED == rollState)
				rollState = newState;
		}
		break;
	case ROLL_HAVE_RESULT://已触发有结果
		{
			if(CItemPkgEle::TRIGGERED == rollState)
				rollState = newState;
		}
		break;

	case ROLL_PICK_TIMEOUT://在触发后的60秒内没有捡起(超时，任何人可以捡起)
		{
			if((ROLL_NO_RESULT == rollState) || (ROLL_HAVE_RESULT == rollState))
				rollState = newState;
		}
		break;

	default:
		break;
	}

	return;
}

void CItemsPackage::SortItemEle()
{
	if ( mItemEleVec.empty() )
		return;
	
	unsigned itemCount = (unsigned)mItemEleVec.size();
	int VecCount =  (int)(  itemCount - 1 );
	for ( int i = VecCount ; i >= 1  ; i-- )
	{
		for (  int j = 0 ; j< i;  j++ )
		{
			CItemPkgEle& itemEle = mItemEleVec[j];
			CItemPkgEle& itemEle2  = mItemEleVec[j+1];
		
			bool bswap = false;
			if ( itemEle2.GetTaskID() > itemEle.GetTaskID() )
			{
				bswap = true;
			}
			else if( itemEle2.GetTaskID() == itemEle.GetTaskID() )
			{
				if ( itemEle2.GetMassLevel() > itemEle.GetMassLevel() )
				{
					bswap = true;
				}
				else if( itemEle2.GetMassLevel() == itemEle.GetMassLevel() )
				{
					if ( itemEle2.GetPrice() > itemEle.GetPrice() )
					{
						bswap = true;
					}
				}
			}
			
			if ( bswap )
			{
				CItemPkgEle tmpItem = itemEle;
				itemEle  = itemEle2;
				itemEle2 = tmpItem;
			}
		}
	}

	//获取包裹的最大材质等级
	mPkgMaxMassLevel = GetItemPkgMaxMassLevel();
	if ( mPkgMaxMassLevel < E_GREEN )//如果小于紫色，则查看是否有宝石
	{
		//监测是否有宝石
		for ( unsigned int i = 0 ; i< itemCount; i++ )
		{
			CItemPkgEle& itemEle = mItemEleVec[i];
			if ( itemEle.GetItemTypeID() / 1000000 == 220 )//宝石的特有标示,编号前3位为220
			{
				mItemPkgType = 1;
				break;
			}
		}
	}
	else
	{
		mItemPkgType = 2;
	}
}


bool CItemsPackage::RemoveItemEle( CItemPkgEle &itemEle )
{
	if( !m_map )
		return false;

	bool isSuccess = false;

	TRY_BEGIN;

	std::vector<CItemPkgEle>::iterator lter = mItemEleVec.begin();
	for ( ;lter!= mItemEleVec.end() && !isSuccess ; )
	{
		if ( *lter ==  itemEle )
		{
			(*lter).Clear();
			isSuccess = true;
		}
		else
			++lter;
	}
	TRY_END;

	return isSuccess;
}

int CItemsPackage::FindItemIndex(CItemPkgEle& itemEle )
{
	unsigned size = (unsigned)mItemEleVec.size();
	for ( unsigned int i = 0 ; i< size; i++ )
	{
		CItemPkgEle& tmpEle =  mItemEleVec[i];
		if ( tmpEle == itemEle )
		{
			if( IsMoneyPkg() )
				return i+1;
			else
				return i;
		}
	}
	return -1;
}

//@brief:当道具包的生命期结束时
void CItemsPackage::OnLiveEnd()
{
	if ( !m_map )
		return;

	m_map->HandleItemsPkgDisAppear( this );
}


void CItemsPackage::PickItemFailed(CPlayer* pPlayer, unsigned itemID ,int reason )
{
	if (!pPlayer )
		return;

	if ( reason < 0 )
		return;

	MGPickItemFaild srvmsg;
	srvmsg.itemID = itemID;
	srvmsg.result = (EPickItemResult)reason;

	pPlayer->SendPkg<MGPickItemFaild>( &srvmsg );
}


//@brief:当玩家选取了道具包中的所有道具时
bool CItemsPackage::OnPickAllItems(CPlayer* pPlayer )
{
	if ( !pPlayer )
		return false;

	if ( pPlayer->IsBondage() )
	{
		D_DEBUG("玩家%s束缚，不能拾取道具\n",pPlayer->GetNickName() );
		return false;
	}

	do 
	{
		//判断是不是需要Roll点，如果需要Roll点，那么弹出Roll点框
		if (  IsShouldRollItem() )
			BeginRollItem();


		if ( ItemPkgHaveMoney() )
			OnPickOneItem( pPlayer,0 );

		if( mItemEleVec.empty() )
			break;

		unsigned int itemIndexBase = 0;
		if ( IsMoneyPkg() )
			itemIndexBase = 1;


		//拾取所有道具
		unsigned int itemCount = (unsigned int)mItemEleVec.size();
		for ( unsigned int index = 0 ; index< itemCount; index++ )
		{
			CItemPkgEle& itemEle = mItemEleVec[index];
			if( itemEle.GetItemTypeID() > 0 )
			{
				OnPickOneItem( pPlayer, index + itemIndexBase );
			}
		}

	} while( false );
	//拾取金钱
	
	//@brief:如果道具包为空掉了
	if ( CheckItemPackEleEmpty() && !ItemPkgHaveMoney() )
	{
		//@把道具从管理列表中删除
		if( CItemPackageManagerSingle::instance()->RemoveItemPackage( this ) )
		{
			//@回收此块内存
			CItemPackageManagerSingle::instance()->RecoverItemPackage( this );
		}
	} 

	return true;
}

bool CItemsPackage::CheckItemPackEleEmpty()
{
	if ( mItemEleVec.empty() )
		return true;
	 

	unsigned int size = (unsigned)mItemEleVec.size();
	for ( unsigned int i = 0 ; i< size; i++ )
	{
		CItemPkgEle& itemEle = mItemEleVec[i];
		if ( itemEle.GetItemTypeID() != 0 )
			return false;
	}

	return true;
}

//@brief:当玩家选取了道具包中的一个道具时,ItemIndex是在道具包中的CItemPkgEle的mEleID
bool CItemsPackage::OnPickOneItem( CPlayer* pPlayer ,unsigned long itemIndex,int pkgIndex )
{
	return OnNewPickOneItem(pPlayer, itemIndex, pkgIndex);//新的拾取功能

	if ( !pPlayer )
		return false;

	if ( pPlayer->IsBondage() )
	{
		D_DEBUG("玩家%s束缚，不能拾取道具\n",pPlayer->GetNickName() );
		return false;
	}

	//骑乘态对玩家的拾取包裹的消息不处理 
	if ( pPlayer->IsRideState() )
		return false;

	if ( pPlayer->TradeInfo() != NULL )//玩家处于交易状态,不能进行拾取
		return false;

	unsigned int petdistance = PICK_PKG_RANGE;
	if ( pPlayer->IsActivePet() )
		petdistance = 40;

	////稍微放宽点距离
	if( !IsInAccurateDistance( pPlayer->GetPosX(), pPlayer->GetPosY(), GetItemPosX(),GetItemPosY(), petdistance+2/*客户端显示位置容许误差2*/ ) )
		return false;

	int  errNo = 0;
	bool bPickSuccess = false;//拾取成功标记
	int  pickIndex = itemIndex;

	do 
	{
		//如果没有过一分钟,则玩家所有权错误 m_Owner记录了该包裹所属的玩家
		if (  !IsPassSixtySeconds() )
		{
			//如果没过1分钟，那么检测该包裹是否能被该玩家拾取
			if( !IsPlayerCanPickItemPkg( pPlayer ) )
			{
				errNo = E_PKG_OWNER;	
				break;
			}
		}

		//如果道具包空了，且道具还包含金钱
		if ( CheckItemPackEleEmpty() && !ItemPkgHaveMoney() )
		{
			errNo = E_PKG_EMPTY;//道具包空 
			break;
		}

		if ( IsMoneyPkg() )
		{
			//如果选择的是第一个道具
			if ( pickIndex == 0 )
			{
				if ( mMoney > 0)
				{
#ifdef ANTI_ADDICTION
					if( pPlayer->IsTriedOrAddictionState() )//如果属于防沉迷和不健康状态
					{
						pPlayer->SendSystemChat("由于您处于防沉迷状态，您的物品被大陆的力量吞噬了");
					}
					//commentted by sck 2010/02/21
					//else
					//{
						pPlayer->AddSilverMoney( mMoney );//双币种判断
					//}
#else //ANTI_ADDICTION
					pPlayer->AddSilverMoney( mMoney );//双币种判断
#endif

					mMoney = 0;//金钱拾取完毕后，记为0
					bPickSuccess = true;
					NoticeOnePlayerPickItem( 0, true );
				}
				break;
			}
			else if( pickIndex > 0 )
				pickIndex--;
		}	

		if ( pickIndex < 0 || pickIndex >= (int)mItemEleVec.size() )
			break;

		CItemPkgEle& itemEle =  mItemEleVec[pickIndex];
		
		if ( itemEle.GetItemTypeID()  == 0 )
		{
			errNo =  E_PKG_ERROR;
			break;
		}

		//如果道具处于锁定状态时不能被拾取的
		if ( itemEle.IsLock() )
		{
			errNo = E_PKG_OWNER;	
			break;
		}

		//如果该物品已经有了所有者，比较所有者的ID是否正确
		if ( itemEle.IsHaveOwner() && itemEle.GetOwnerID() != pPlayer->GetFullID() )
		{
			errNo = E_PKG_OWNER;
			break;
		}

		//如果是任务物品，必须身上有这个任务才能拾取道具
		unsigned taskID = itemEle.GetTaskID();
		if ( taskID != 0  )
		{
			bool isCanPickTaskItem = true;
			
			//根据任务获取对应的计数器
			CNormalCounter* pCounter = pPlayer->GetTaskCounter( itemEle.GetTaskID() ,CT_COLLECT );
			if ( pCounter == NULL )
			{
				errNo = E_PKG_UNUSEITEM;
				isCanPickTaskItem = false;
			}

			//如果计数器满了，也无法拾取道具
			if ( pCounter && pCounter->IsReachMaxCounter() )
			{
				errNo = E_PKG_UNUSEITEM;
				isCanPickTaskItem = false;
			}

			//如果拾取任务物品失败
			if ( !isCanPickTaskItem )
			{
				//通知拾取失败
				PickItemFailed( pPlayer, itemIndex ,errNo );

				//如果不能拾取该物品的人拾取了，则该任务道具要消失掉
				NoticeOnePlayerPickItem( itemIndex , false );

				//删除道具
				RemoveItemEle( itemEle );

				return false;
			}
		}
		
#ifdef ANTI_ADDICTION
		if( pPlayer->IsTriedOrAddictionState() )//如果属于防沉迷和不健康状态
		{
			pPlayer->SendSystemChat("由于您处于防沉迷状态，您的物品被大陆的力量吞噬了");
			NoticeOnePlayerPickItem( itemIndex, false );
			RemoveItemEle( itemEle );//删除道具
			return true;
		}
		else
		{
			bPickSuccess =  pPlayer->GiveNewItemToPlayer( itemEle.GetItemEleInfo(), itemEle.GetTaskID(), errNo, pkgIndex );
			if ( bPickSuccess )
			{
				NoticeOnePlayerPickItem( itemIndex, false );

				RemoveItemEle( itemEle );//删除道具
			}
		}
#else//ANTI_ADDICTION
		bPickSuccess =  pPlayer->GiveNewItemToPlayer( itemEle.GetItemEleInfo(), itemEle.GetTaskID(), errNo );
		if ( bPickSuccess )
		{
			NoticeOnePlayerPickItem( itemIndex, false );

			RemoveItemEle( itemEle );//删除道具
		}
#endif
	} while( false );

	if(  !bPickSuccess )//如果拾取失败
	{
		PickItemFailed( pPlayer, itemIndex ,errNo );
		return false;
	}
	return true;
}

//@brief:通知周围的人，一个玩家拾取了周围的一个道具
void CItemsPackage::NoticeOnePlayerPickItem( unsigned long itemID, bool isMoney )
{
	if ( !m_map )
		return;

	MGOnePlayerPickItem srvmsg;
	srvmsg.itemID = itemID;
	srvmsg.pkgIndex = GetItemsPackageIndex();
	srvmsg.bIsPickMoney = isMoney;
	m_map->NotifySurrounding<MGOnePlayerPickItem>( &srvmsg, x, y );
}

void CItemsPackage::NoticePlayerItemPkgAppear( CPlayer* pPlayer )
{
	if( !pPlayer )
		return;

	MGItemPackAppear pkgAppearMsg;
	pkgAppearMsg.px =  GetItemPosX();
	pkgAppearMsg.py =  GetItemPosY();
	pkgAppearMsg.pkgID = GetItemsPackageIndex();
	pkgAppearMsg.OwnerID = GetOwner();
	pkgAppearMsg.dropOwnerID = GetDropOwnerID();
	if ( IsPlayerCanPickItemPkg(pPlayer) )
	{
		pkgAppearMsg.type = ( ( 1<<16 ) | GetItemPkgType() );
	}
	else
	{
		pkgAppearMsg.type = GetItemPkgType();
	}
	for (size_t i = 0; i < mItemEleVec.size(); ++i)
	{
		pkgAppearMsg.itemInfo = mItemEleVec[i].GetItemEleInfo();
		pkgAppearMsg.itemIndex = FindItemIndex(mItemEleVec[i]);
		pPlayer->SendPkg<MGItemPackAppear>( &pkgAppearMsg );
	}
}


void CItemsPackage::SetRollItemOwner(unsigned int itemTypeID, const PlayerID& playerID )
{
	std::vector<CItemPkgEle>::iterator lter = mItemEleVec.begin();
	for ( ;lter!= mItemEleVec.end(); ++lter )
	{
		CItemPkgEle& itemEle = *lter;
		//进来之前，道具已经被接触锁定了
		if ( itemEle.GetItemTypeID() == itemTypeID && !itemEle.IsLock() )
		{
			itemEle.SetOwnerID( playerID );
		}
	}
}

bool CItemsPackage::CheckPickPrivilege(CPlayer* pPlayer ,unsigned long itemIndex)
{
	if ( !pPlayer )
		return false;
	
	if(!IsGroupPickMode())//非组队模式
	{
		if(IsPassSixtySeconds())//任何人都可以拾取包裹（当然包括包裹中的任何物品）
			return true;
		else
		{
			if(pPlayer->GetFullID() == m_Owner)
				return true;
			else 
				return false;
		}
	}

	//以下处理组队模式下的判断(自由拾取或者队伍拾取)
	if(itemIndex >= mItemEleVec.size())
	{
		D_ERROR("CItemsPackage::CheckPickPrivilege itemIndex >= mItemEleVec.size()\n");
		return false;
	}

	CItemPkgEle &pkgEle = mItemEleVec[itemIndex];
	CPlayerStateManager&  stateMan = pPlayer->GetSelfStateManager();
	bool isTeamMember = false;
	if ( stateMan.IsGroupState() && stateMan.GetGroupState().GetGroupTeamID() == groupTeamID )
	{
		for (std::vector<CPlayer *>::iterator lter = mCanPickPlayer.begin(); lter != mCanPickPlayer.end(); ++lter )
		{
			CPlayer* tmpPlayer= *lter;
			if ( tmpPlayer && tmpPlayer->GetFullID() == pPlayer->GetFullID() )
			{
				isTeamMember = true;
				break;
			}
		}
	}

	if((mItemShareMode != E_TEAM_MODE)/*自由拾取*/ || 
	  ((mItemShareMode == E_TEAM_MODE) && !pkgEle.IsIncludedRollSet(mItemMassLevel))/*队伍拾取，但不需要roll点*/)
	{
		if(isTeamMember)
			return true;
		else
		{
			if(IsPassSixtySeconds())
				return true;
			else
				return false;
		}
	}

	//队伍拾取，且需要roll点
	CItemPkgEle::RollItemState currItemState;
	if(!pkgEle.CheckRollItemState(pPlayer->GetFullID(), isTeamMember, currItemState))
	{
		if((isTeamMember) && (CItemPkgEle::INIT == currItemState))
		{
			RollItem* pRollItem = RollItemManagerSingle::instance()->CreateRollItem( this, pkgEle, groupTeamID  );
			if(NULL == pRollItem)
				D_ERROR("CItemsPackage::CheckPickPrivilege  createrollItem出错\n");
			else
			{	
				pRollItem->TriggerRoll( mCanPickPlayer );
				pkgEle.UpdateRollState(CItemPkgEle::TRIGGERED);
				pkgEle.SetRollID(pRollItem->GetRollItemID());
			}
		}

		return false;
	}

	return true;
}

bool CItemsPackage::OnNewPickOneItem(  CPlayer* pPlayer ,unsigned long itemIndex, int pkgIndex )
{
	if(NULL == pPlayer)
		return false;

	bool bPickSuccess = false;//缺省不成功
	int errNo = -1;
	unsigned long pickIndex = itemIndex;

	do 
	{
		//如果道具包空了，且道具还包含金钱
 		if ( CheckItemPackEleEmpty() && !ItemPkgHaveMoney() )
		{
			errNo = E_PKG_EMPTY;//道具包空 
			break;
		}

		if ( IsMoneyPkg() )
		{
			//如果选择的是第一个道具
			if ( pickIndex == 0 )
			{
				if ( mMoney > 0)
				{
#ifdef ANTI_ADDICTION
					if( pPlayer->IsTriedOrAddictionState() )//如果属于防沉迷和不健康状态
					{
						pPlayer->SendSystemChat("由于您处于防沉迷状态，您的物品被大陆的力量吞噬了");
					}
					//commentted by sck 2010/02/21
					//else
					//{
					pPlayer->AddSilverMoney( mMoney );//双币种判断
					//}
#else //ANTI_ADDICTION
					pPlayer->AddSilverMoney( mMoney );//双币种判断
#endif

					mMoney = 0;//金钱拾取完毕后，记为0
					bPickSuccess = true;
					NoticeOnePlayerPickItem( 0, true );
				}

				break;
			}
			else if( pickIndex > 0 )
				pickIndex--;
		}	

		if ( pickIndex < 0 || pickIndex >= (int)mItemEleVec.size() )
			break;

		CItemPkgEle& pkgEle =  mItemEleVec[pickIndex];
		if(!CheckPickPrivilege(pPlayer, pickIndex))
		{
			D_DEBUG("玩家%s没有权限拾取\n",pPlayer->GetNickName() );
			break;
		}
			
		if ( pPlayer->IsBondage() )
		{
			D_DEBUG("玩家%s束缚，不能拾取道具\n",pPlayer->GetNickName() );
			break;
		}

		//骑乘态对玩家的拾取包裹的消息不处理 
		if ( pPlayer->IsRideState() )
		{	
			D_DEBUG("玩家%s骑乘状态，不能拾取道具\n",pPlayer->GetNickName() );
			break;
		}

		if ( pPlayer->TradeInfo() != NULL )//玩家处于交易状态,不能进行拾取
		{	
			D_DEBUG("玩家%s交易状态，不能拾取道具\n",pPlayer->GetNickName() );
			break;
		}

		unsigned int petdistance = PICK_PKG_RANGE;
		if ( pPlayer->IsActivePet() )
			petdistance = 40;

		////稍微放宽点距离
		if( !IsInAccurateDistance( pPlayer->GetPosX(), pPlayer->GetPosY(), GetItemPosX(),GetItemPosY(), petdistance+2/*客户端显示位置容许误差2*/ ) )
		{	
			D_DEBUG("玩家%s距离拾取范围太大\n",pPlayer->GetNickName() );
			break;
		}
	
#ifdef ANTI_ADDICTION
		if( pPlayer->IsTriedOrAddictionState() )//如果属于防沉迷和不健康状态
		{
			pPlayer->SendSystemChat("由于您处于防沉迷状态，您的物品被大陆的力量吞噬了");
			NoticeOnePlayerPickItem( itemIndex, false );
			RemoveItemEle( pkgEle );//删除道具
			if(pkgEle.GetRollID() != 0)
				RollItemManagerSingle::instance()->RemoveRollItem(pkgEle.GetRollID());//删除rollItem
			
			bPickSuccess = true;
		}
		else
		{
			bPickSuccess =  pPlayer->GiveNewItemToPlayer( pkgEle.GetItemEleInfo(), pkgEle.GetTaskID(), errNo, pkgIndex );
			if ( bPickSuccess )
			{
				NoticeOnePlayerPickItem( itemIndex, false );
				RemoveItemEle( pkgEle );//删除道具
				if(pkgEle.GetRollID() != 0)
					RollItemManagerSingle::instance()->RemoveRollItem(pkgEle.GetRollID());//删除rollItem
			}
		}
#else//ANTI_ADDICTION
		bPickSuccess =  pPlayer->GiveNewItemToPlayer( pkgEle.GetItemEleInfo(), pkgEle.GetTaskID(), errNo );
		if ( bPickSuccess )
		{
			NoticeOnePlayerPickItem( itemIndex, false );
			RemoveItemEle( pkgEle );//删除道具
			if(pkgEle.GetRollID() != 0)
				RollItemManagerSingle::instance()->RemoveRollItem(pkgEle.GetRollID());//删除rollItem
		}
#endif	

	}while(0);

	if( !bPickSuccess )//如果拾取失败
	{
		PickItemFailed( pPlayer, itemIndex ,errNo );
		return false;
	}

	return true;
}

void CItemsPackage::DisableRollPrivilege(void)
{
	if(mItemEleVec.size() > 0)
	{
		ACE_Time_Value passTime = ACE_OS::gettimeofday() - m_CreateTime;
		if(passTime.sec() > 60)
		{
			if(IsGroupPickMode() && (E_TEAM_MODE == mItemShareMode))
			{
				for(std::vector<CItemPkgEle>::iterator iter = mItemEleVec.begin(); mItemEleVec.end() != iter; ++iter)
				{
					(*iter).CheckTriggerTimeout(mItemMassLevel);
				}
			}
		}
	}
}

CItemPkgEle* CItemsPackage::GetEleByItemTypeId(unsigned int itemTypeId)
{
	for(std::vector<CItemPkgEle>::iterator iter = mItemEleVec.begin(); mItemEleVec.end() != iter; ++iter)
	{
		if((*iter).GetItemTypeID() == itemTypeId)
		{
			return &(*iter);
		}
	}

	return NULL;
}

//当玩家Roll到包裹中的物品时
void CItemsPackage::OnRollItemToPlayer(CPlayer* pPlayer,CItemPkgEle& pItemEle, unsigned int rollId )
{
	if ( !pPlayer )
		return;

	//Roll点后，设置玩家的道具所有权
	SetRollItemOwner( pItemEle.GetItemTypeID(), pPlayer->GetFullID() );

	//Roll到了以后，道具包就归属该玩家了
	int errNo = 0;

#ifdef ANTI_ADDICTION
	bool bTired = pPlayer->IsTriedOrAddictionState();
	if( !bTired )//如果没有处于沉迷状态
	{
		bool bPickSuccess = pPlayer->GiveNewItemToPlayer( pItemEle.GetItemEleInfo(), pItemEle.GetTaskID(), errNo );
		if( bPickSuccess )
		{
			int itemIndex = FindItemIndex( pItemEle );
			if ( itemIndex != -1 )
			{
				//通知周围的玩家，该包裹中的哪个道具被拾取
				NoticeOnePlayerPickItem( itemIndex , false );

				//移除该道具
				RemoveItemEle( pItemEle );

				RollItemManagerSingle::instance()->RemoveRollItem(rollId);//删除rollItem
			}

		}
		else
		{
			PickItemFailed( pPlayer, 0 ,errNo );
			return;
		}
	}
	else//如果处于沉迷状态
	{
		int itemIndex = FindItemIndex( pItemEle );
		if ( itemIndex != -1 )
		{
			//通知周围的玩家，该包裹中的哪个道具被拾取
			NoticeOnePlayerPickItem( itemIndex , false );

			//移除该道具
			RemoveItemEle( pItemEle );

			RollItemManagerSingle::instance()->RemoveRollItem(rollId);//删除rollItem

			//通知玩家原因
			pPlayer->SendSystemChat("由于您处于防沉迷状态，您的物品被大陆的力量吞噬了");

		}
	}
	
	//检测包裹是否已经为空了
	if ( CheckItemPackEleEmpty() && !ItemPkgHaveMoney() )
	{
		//@把道具从管理列表中删除
		if( CItemPackageManagerSingle::instance()->RemoveItemPackage( this ) )
		{
			//@回收此块内存
			CItemPackageManagerSingle::instance()->RecoverItemPackage( this );
		}
	} 
	return;

#else //ANTI_ADDICTION

	//如果拾取道具成功
	if( pPlayer->GiveNewItemToPlayer( pItemEle.GetItemEleInfo(), pItemEle.GetTaskID(), errNo ) == true )
	{
		int itemIndex = FindItemIndex( pItemEle );
		if ( itemIndex != -1 )
		{
			//通知周围的玩家，该包裹中的哪个道具被拾取
			NoticeOnePlayerPickItem( itemIndex , false );

			//移除该道具
			RemoveItemEle( pItemEle );

			RollItemManagerSingle::instance()->RemoveRollItem(rollId);//删除rollItem
		}


		//检测包裹是否已经为空了
		if ( CheckItemPackEleEmpty() && !ItemPkgHaveMoney() )
		{
			//@把道具从管理列表中删除
			if( CItemPackageManagerSingle::instance()->RemoveItemPackage( this ) )
			{
				//@回收此块内存
				CItemPackageManagerSingle::instance()->RecoverItemPackage( this );
			}
		} 
	}
	else//如果失败了，通知该玩家
	{
		PickItemFailed( pPlayer, 0 ,errNo );
	}
#endif

}

//钱自动进包
void CItemsPackage::MoneyAutoInPackage()
{
	if (!IsMoneyPkg())
	{
		return;
	}
	if (bGroupPick)
	{//组队情况
		CPlayer * pPlayer = NULL;
		CGateSrv*pSrv = CManG_MProc::FindProc( m_Owner.wGID );
		if ( pSrv )
		{
			pPlayer = pSrv->FindPlayer( m_Owner.dwPID );
		}
		if ( NULL == pPlayer)
		{
			return;
		}
		CGroupTeam* pTeam = pPlayer->GetSelfStateManager().GetGroupState().GetGroupTeam();
		if ( NULL == pTeam )
		{
			D_ERROR( "CItemsPackage::MoneyAutoInPackage, NULL==pTeam\n" );
			return;
		}
		size_t num = mCanPickPlayer.size();
		if ( 0 == num )
		{
			return;
		}
		int averageMoney = (int)(mPkgMoney/num);
		for (size_t i = 0; i<num; ++i)
		{
			CPlayer * pickPlayer = mCanPickPlayer[i];
			if (NULL == pickPlayer)
			{
				continue;
			}

			pickPlayer->AddSilverMoney(averageMoney);
			D_DEBUG("玩家%s组队模式银币%d自动进包\n",pPlayer->GetNickName(),averageMoney);
			//发送银币掉落消息
			MGItemPackAppear pkgAppearMsg;
			pkgAppearMsg.px =  GetItemPosX();
			pkgAppearMsg.py =  GetItemPosY();
			pkgAppearMsg.pkgID = GetItemsPackageIndex();
			pkgAppearMsg.OwnerID = GetOwner();
			pkgAppearMsg.dropOwnerID = GetDropOwnerID();
			pkgAppearMsg.type = GetItemPkgType();
			StructMemSet(pkgAppearMsg.itemInfo,0,sizeof(pkgAppearMsg.itemInfo));
			pkgAppearMsg.itemInfo.uiID = SILVER_MONEY_ITEMID;
			//为什么用usItemTypeID而不是ucCount来代表钱数，因为ucCount的类型是CHAR，范围太小，容易溢出
			pkgAppearMsg.itemInfo.usItemTypeID = averageMoney; 
			pkgAppearMsg.itemIndex = -1;
			pickPlayer->SendPkg<MGItemPackAppear>( &pkgAppearMsg );
		}

	}
	else
	{
		CPlayer * pPlayer = NULL;
		CGateSrv*pSrv = CManG_MProc::FindProc( m_Owner.wGID );
		if ( pSrv )
		{
			pPlayer = pSrv->FindPlayer( m_Owner.dwPID );
		}
		if ( NULL == pPlayer)
		{
			return;
		}
		pPlayer->AddSilverMoney(mPkgMoney);
		D_DEBUG("玩家%s非组队模式银币%d自动进包\n",pPlayer->GetNickName(),mPkgMoney);
		//发送银币掉落消息
		MGItemPackAppear pkgAppearMsg;
		pkgAppearMsg.px =  GetItemPosX();
		pkgAppearMsg.py =  GetItemPosY();
		pkgAppearMsg.pkgID = GetItemsPackageIndex();
		pkgAppearMsg.OwnerID = GetOwner();
		pkgAppearMsg.dropOwnerID = GetDropOwnerID();
		pkgAppearMsg.type = GetItemPkgType();
		StructMemSet(pkgAppearMsg.itemInfo,0,sizeof(pkgAppearMsg.itemInfo));
		pkgAppearMsg.itemInfo.uiID = SILVER_MONEY_ITEMID;
		pkgAppearMsg.itemInfo.usItemTypeID = mPkgMoney; 
		pkgAppearMsg.itemIndex = -1;
		pPlayer->SendPkg<MGItemPackAppear>( &pkgAppearMsg );
	}
}

//@brief:通知道具包周围的玩家
template<class T>
void CItemsPackage::OnNoticSurroundPlayer(T& _msg )
{
	TRY_BEGIN;

	if ( !m_map )
	{
		D_ERROR("道具包准备通知周围的玩家，但是道具包所属的地图指针为NULL\n" );
		return;
	}

	m_map->NotifySurrounding<T>( &_msg, x,y );

	TRY_END;
}


void CItemsPackage::OnQueryItemPkgEleConciseInfo(CPlayer *pPlayer, MUX_PROTO::MGItemPackInfo *itempkgInfo)
{
	if ( NULL == pPlayer || NULL == itempkgInfo )
	{
		D_ERROR( "CItemsPackage::OnQueryItemPkgEleConciseInfo, NULL == pPlayer || NULL == itempkgInfo\n" );
		return;
	}

	TRY_BEGIN;

	int validItemCount = 0;
	if ( !mItemEleVec.empty() )
	{
		unsigned int itemIndex = IsMoneyPkg()?1:0;//如果有金钱则从1开始,无金钱从0开始

		std::vector<CItemPkgEle>::const_iterator lter = mItemEleVec.begin(),
			lterEnd = mItemEleVec.end();
		for ( ;lter != lterEnd; ++lter, ++validItemCount, ++itemIndex )
		{
			if ( validItemCount >= ARRAY_SIZE(itempkgInfo->itemInfoArr) )
			{
				D_ERROR( "CItemsPackage::OnQueryItemPkgEleConciseInfo, 包裹索引%d，道具数量%d越界!!!\n", GetItemsPackageIndex(), mItemEleVec.size() );
				break;
			}
			itempkgInfo->itemInfoArr[validItemCount] = (*lter).GetItemEleInfo();
			itempkgInfo->itemInfoArr[validItemCount].uiID = itemIndex; 
		}
	}

	itempkgInfo->itemCount = validItemCount;	
	itempkgInfo->silverMoney     = mMoney;//双币种判断；
	itempkgInfo->goldMoney     = 0;//双币种判断；

	TRY_END;
}

//获取包裹中最大的MassLevel信息
class ItemMassLevelFunc:public std::binary_function<CItemPkgEle,CItemPkgEle, bool>
{
public:
	bool operator()( const CItemPkgEle& pItem1, const CItemPkgEle& pItem2 ) const
	{
		if (  pItem1.GetMassLevel() < pItem2.GetMassLevel() )
			return true;

		return false;
	}
};

////@brief:获取道具包中最大的道具包属性
//unsigned int CItemsPackage::GetItemPkgType()
//{
//	//if ( mPkgMaxMassLevel >=  E_PURPLE )
//	//	return 2;
//	//else if( isHaveGem )//如果有宝石
//	//	return 1;
//	//else
//	//	return 0;
//	return mItemPkgType;
//}

void CItemsPackage::OnQueryItemPkgDetailInfo(CPlayer* player, unsigned int queryType )
{
	TRY_BEGIN;

	if ( !player )
		return;

	if ( player->IsBondage() )
	{
		D_DEBUG("玩家%s束缚，不能拾取道具\n",player->GetNickName() );
		return;
	}
/*
#ifdef ANTI_ADDICTION
//防沉迷不给查包
	if( player->IsTriedOrAddictionState() )
	{
		D_WARNING("%s 处于沉迷状态忽略查包消息\n", player->GetNickName() );
		return;
	}
#endif //ANTI_ADDICTION
*/
	//D_DEBUG("玩家查询第%d个道具包的信息\n", GetItemsPackageIndex() );

	unsigned int petdistance = PICK_PKG_RANGE;
	if ( player->IsActivePet() )
		petdistance = 40;

	if( !IsInAccurateDistance( player->GetPosX(), player->GetPosY(), GetItemPosX(),GetItemPosY(), petdistance+2/*客户端显示位置容许误差2*/ ) )
	{
		D_DEBUG("玩家拾取包裹%d 失败 玩家坐标( %d, %d ) 包裹坐标( %d, %d ), 失去允许的合法距离%d\n", GetItemsPackageIndex(),player->GetPosX(), player->GetPosY(), GetItemPosX(), GetItemPosY(),petdistance );
		return;
	}

	//骑乘态对玩家的拾取包裹的消息不处理 
	if ( player->IsRideState() )
	{
		D_DEBUG("玩家处于骑马状态，不能失去包裹\n");
		return;
	}

	if ( !IsPlayerCanPickItemPkg(player) )
	{
		D_DEBUG("该包裹  %d 玩家不能拾取\n",GetItemsPackageIndex() );
		return;
	}

	//如果之前没有打开过道具,则记录下该道具打开的时间
	if ( !IsOpenItemPkg() )
	{
		mIsOpen = true;
		m_openTime = ACE_OS::gettimeofday();
	}

	unsigned long pkgIndex = GetItemsPackageIndex();

	//////////////////////////////////////////////////////////////////////////
	MGItemPackInfo itempkgInfo;
	StructMemSet( itempkgInfo, 0x0,sizeof(MGItemPackInfo) );
	itempkgInfo.pkgID = pkgIndex;
	OnQueryItemPkgEleConciseInfo( player, &itempkgInfo );
	itempkgInfo.queryType = queryType;
	player->SendPkg<MGItemPackInfo>( &itempkgInfo );

	//D_DEBUG("发送包裹 %d 信息给玩家,查询包裹信息成功\n", GetItemsPackageIndex() );

	//判断是不是需要Roll点
	if (  IsShouldRollItem() )
		BeginRollItem();

	TRY_END;
}

//设置道具包的归属
void CItemsPackage::SetOwner(CPlayer *pPlayer)
{
	if ( !pPlayer )//如果玩家不在服务器了
	{
		PassSixtySeconds();//则掉落的包裹大家都能拾取
		return;
	}

	//玩家是否处于组队状态
	if ( pPlayer->GetSelfStateManager().IsGroupState() )
	{ 
		bGroupPick = true;
		//获取玩家的组队信息
		 CGroupTeam* pTeam = pPlayer->GetSelfStateManager().GetGroupState().GetGroupTeam();
		 //assert( pTeam != NULL );
		 if ( NULL == pTeam )
		 {
			 D_ERROR( "CItemsPackage::SetOwner, NULL==pTeam\n" );
			 return;
		 }
		 if ( pTeam )//设置组队模式
			 SetGroupPickMode(pTeam);//设置处于组队模式
	}
	else
	{
		m_Owner = pPlayer->GetFullID();
		bGroupPick = false;
	}
}

//当道具包属于队伍拾取模式时
void CItemsPackage::SetGroupPickMode(CGroupTeam *pGroupTeam)
{
	if ( ( NULL == pGroupTeam )
		|| ( NULL == m_map )
		)
	{
		D_ERROR( "CItemsPackage::SetGroupPickMode，空指针\n" );
		return;
	}

	if ( !m_map )
		return;

	if ( pGroupTeam )
	{
		mCanPickPlayer.clear();

		groupTeamID    = pGroupTeam->GetGroupTeamID();//队伍ID
		mItemShareMode = pGroupTeam->GetGroupItemShareMode();//队伍分配模式
		unsigned int mapID = GetOwnerMap()->GetMapNo();//获取道具包所在的地图编号，通过编号获取该地图上的玩家
		if ( mItemShareMode == E_FREE_MODE )//自由模式,获取掉落时刻能够拾取的玩家名单
		{
			pGroupTeam->GetTeamMemberInUnSafeArea( mapID, mCanPickPlayer );
		}
		else if( mItemShareMode == E_TEAM_MODE  )//如果是队伍模式
		{
			bIsRoll = false;

			//则判断是否需要Roll点
			mItemMassLevel = pGroupTeam->GetRollItemMassLevel();
			//检测是否含有需要Roll点的物品
			std::vector<CItemPkgEle>::const_iterator lter = mItemEleVec.begin();
			for ( ;lter != mItemEleVec.end(); ++lter )
			{
				if ( (*lter).GetMassLevel() >= mItemMassLevel )
				{
					bIsRoll = true;
					break;
				}
			}
			
			if ( true/*bIsRoll,组队模式中轮流拾取取消(zsw/2010.8.13)*/ )
			{
				//如果需要Roll点，那么同样也需获取该地图上的玩家
				pGroupTeam->GetTeamMemberInUnSafeArea( mapID, mCanPickPlayer );
			}
			else
			{
				/*组队模式中轮流拾取取消(zsw/2010.8.13)
				CPlayer* pPickPlayer  = pGroupTeam->IncPickItemIndexOnTurnMode( m_map->GetMapNo() );
				if ( pPickPlayer !=NULL)
					mCanPickPlayer.push_back(pPickPlayer);

#ifdef _DEBUG
				D_DEBUG("队伍拾取模式 拾取玩家%s\n", pPickPlayer->GetNickName() );
#endif
				*/
			}
		}
		/*组队模式中轮流拾取取消(zsw/2010.8.13)
		else if ( mItemShareMode == E_TURN_MODE )//轮流模式||队伍模式 获取本次轮流到的能拾取的玩家名单
		{
			CPlayer*  pPickPlayer = pGroupTeam->IncPickItemIndexOnTurnMode( m_map->GetMapNo() );
			if ( pPickPlayer )
				mCanPickPlayer.push_back(pPickPlayer);

#ifdef _DEBUG
			D_DEBUG("轮流模式 拾取玩家%s\n", pPickPlayer->GetNickName() );
#endif
		}
		*/
	}
}

bool CItemsPackage::IsPlayerCanPickItemPkg(CPlayer *pPlayer)
{
	if ( !pPlayer )
		return false;

	//60秒后谁都能拾取
	if ( IsPassSixtySeconds() )
		return true;
	else
	{
		//如果组队模式,判断是否能够拾取
		if ( IsGroupPickMode() )
		{
			CPlayerStateManager&  stateMan = pPlayer->GetSelfStateManager();
			if ( stateMan.IsGroupState() && stateMan.GetGroupState().GetGroupTeamID() == groupTeamID )
			{
				std::vector<CPlayer *>::iterator lter = mCanPickPlayer.begin();
				for ( ;lter != mCanPickPlayer.end(); ++lter )
				{
					CPlayer* tmpPlayer= *lter;
					if ( tmpPlayer && tmpPlayer->GetFullID() == pPlayer->GetFullID() )
						break;
				}
				return lter != mCanPickPlayer.end();
			}
			else
				return false;
		}
		else//如果非组队模式,则是该道具的归属玩家
		{
			return pPlayer->GetFullID() == m_Owner;
		}
	}
}

ITEM_MASSLEVEL CItemsPackage::GetItemPkgMaxMassLevel()
{
	std::vector<CItemPkgEle>::const_iterator lter = std::max_element( mItemEleVec.begin(), mItemEleVec.end(), ItemMassLevelFunc() );
	if ( lter != mItemEleVec.end()  )
	{
		return (*lter).GetMassLevel();
	}
	return E_GRAY;
}

void CItemsPackage::LockItem(unsigned int itemTypeID )
{
	std::vector<CItemPkgEle>::iterator lter = mItemEleVec.begin();
	for ( ;lter!= mItemEleVec.end(); ++lter )
	{
		CItemPkgEle& itemEle = *lter;
		if ( itemEle.GetItemTypeID() == itemTypeID && !itemEle.IsLock() )//如果该包裹中的这个道具没有被锁定
		{
			itemEle.Lock();
			break;
		}
	}
}

void CItemsPackage::UnLockItem(unsigned int itemTypeID )
{
	std::vector<CItemPkgEle>::iterator lter = mItemEleVec.begin();
	for ( ;lter!= mItemEleVec.end(); ++lter )
	{
		CItemPkgEle& itemEle = *lter;
		if ( itemEle.IsLock() && itemEle.GetItemTypeID() == itemTypeID )//如果该包裹中的这个道具没有被锁定
		{
			itemEle.UnLock();
			break;
		}
	}
}

void CItemsPackage::BeginRollItem()
{
	if ( mItemEleVec.empty() )
		return;

	if ( mCanPickPlayer.empty() )
		return;

	//对包裹里面需要的Roll点物品,Roll点
	std::vector<CItemPkgEle>::iterator lter = mItemEleVec.begin();
	for ( ;lter!= mItemEleVec.end(); ++lter )
	{
		const CItemPkgEle& itemEle = *lter;
		if ( itemEle.GetMassLevel() >= mItemMassLevel )
		{
			RollItem* pRollItem = RollItemManagerSingle::instance()->CreateRollItem( this, itemEle, groupTeamID  );
			if ( !pRollItem )
				continue;

			pRollItem->TriggerRoll( mCanPickPlayer );
		}
	}

	AfterBeginRollItem();
}

void CItemPackageManager::Release()
{
	TRY_BEGIN;

	std::map<s32,CItemsPackage *>::iterator lter = m_PackageMan.begin();
	for (; lter!= m_PackageMan.end(); lter++)
	{
		CItemsPackage* pPackage = lter->second;
		if ( pPackage )
			delete pPackage;
	}

	m_PackageMan.clear();

	TRY_END;
}


//@brief:监视每个道具包的生命期
void CItemPackageManager::MonitorItemPkgLive()
{
#define ITEMPKGOPENLIVETIME 60
#define ITEMPKGPROTECTTIME 30
#define ITEMPKGUPDATETIME 15

	TRY_BEGIN;

	m_currentTime = time(NULL);
	time_t tmpPassTime =  m_currentTime - m_lastUpdateTime;
	if ( tmpPassTime >= ITEMPKGUPDATETIME )//每15秒更新一次
	{
		if ( m_PackageMan.empty() )
			return;

		std::map<s32,CItemsPackage *>::iterator lter = m_PackageMan.begin();
	
		for ( ;lter != m_PackageMan.end(); )
		{
			CItemsPackage* pPackage = static_cast<CItemsPackage *>( lter->second );
			if ( pPackage && pPackage->GetOwnerMap() )
			{
				time_t diffTime = m_currentTime - pPackage->GetItemPkgCreateTime().sec();

				if ( !pPackage->IsPassSixtySeconds() )
				{
					//是否过了60秒,包裹创建一分钟后包裹权限属于所有人
					if ( diffTime >=  60/*ITEMPKGPROTECTTIME*/ )//如果道具过了30秒,则拾取权限变更
					{
						pPackage->PassSixtySeconds();
						pPackage->GetOwnerMap()->NotifyItemPkgsAppearEx( pPackage );
					}
				}
			
				bool IsNeedDel = false;
				do 
				{
					//@是否曾经打开过包裹,打开包裹后，道具包在一分钟内消失
					/*if ( pPackage->IsOpenItemPkg() )
					{
						time_t passTime = m_currentTime - pPackage->GetItemPkgOpenedTime().sec();
						if ( passTime >= ITEMPKGOPENLIVETIME )
						{
							D_DEBUG("包裹打开超过了1分钟,包裹消失:%d\n",pPackage->GetItemsPackageIndex() );
							IsNeedDel = true;
							break;
						}
					}道具实体模型忽略打开*/

					//是否过了道具包的生命期
					if ( diffTime >= pPackage->GetLiveTime() )//如果包裹过了包裹的生存周期
					{
						IsNeedDel = true;
						break;
					}
				} while(false);

				if ( IsNeedDel /*&& !pPackage->IsRolling()在包裹的生命期内roll属性最大持续2分钟*/ )//如果检测到到需要删除此包裹,并且此包没有Roll物品的话
				{
					CItemPackageManagerSingle::instance()->RecoverItemPackage( pPackage );
					m_PackageMan.erase( lter++ );	
					continue;
				}
			}
			else
			{
				m_PackageMan.erase(lter++);
				//D_DEBUG("每3分钟更新一次道具包是否存在，发现道具包的指针为NULL\n");
				continue;
			}
			lter++;
		}
		m_lastUpdateTime = m_currentTime;
	}
	TRY_END;
}


//@brief:以后将要修改
bool CItemPackageManager::RecoverItemPackage(CItemsPackage* pTmpPackage )
{
	if ( !pTmpPackage )
		return false;

	
	pTmpPackage->OnLiveEnd();

	//以后道具包将做成一个内存池，现在暂时如此
	if ( pTmpPackage)
		delete pTmpPackage;

	pTmpPackage = NULL;

	return true;

}

bool CItemPackageManager::RemoveItemPackage(CItemsPackage *pTmpPackage)
{
	if ( !pTmpPackage )
		return false;

	return m_PackageMan.erase( pTmpPackage->GetItemsPackageIndex() )!=0 ;
}

CItemsPackage* CItemPackageManager::CreateItemPackageEx(CMonster* pDeadMonster )
{
	TRY_BEGIN;

	if ( NULL == pDeadMonster )
	{
		D_ERROR( "创建道具包，道具包所依赖的怪物，不存在\n" );
		return NULL;
	}

	CNewMap* pMap = pDeadMonster->GetMap();
	if ( NULL == pMap )
	{
		D_ERROR( "CItemPackageManager::CreateItemPackageEx, 怪物掉包，NULL == pMap" );
		return NULL;
	}
	
	//获得奖励的玩家信息
	const RewardInfo& rewardInfo = pDeadMonster->GetRewardInfo();

	CPlayer* pOwnerPlayer =  NULL;

	if ( rewardInfo.rewardPlayerID.wGID != MONSTER_GID && rewardInfo.rewardPlayerID.dwPID != 0 )//如果CMonster纪录了该拾取的玩家
	{
		CGateSrv* pGateSrv = CManG_MProc::FindProc( rewardInfo.rewardPlayerID.wGID );
		if ( NULL == pGateSrv )
		{
			D_ERROR( "CItemPackageManager::CreateItemPackageEx, NULL == pGateSrv\n" );
			return NULL;
		}

		pOwnerPlayer = pGateSrv->FindPlayer( rewardInfo.rewardPlayerID.dwPID );
	}
	
	//创建道具包
	CItemsPackage* newItemPkg = NEW CItemsPackage( rewardInfo.rewardPlayerID, pDeadMonster->GetMap() );
	if ( NULL == newItemPkg )
	{
		D_ERROR( "CItemPackageManager::CreateItemPackageEx, NULL == newItemPkg\n" );
		return NULL;
	}

	std::vector<CItemPkgEle>& itemEleVec = newItemPkg->GetItemPkgItemEleVec();
	long money = 0;

	////设置道具包的包裹所包含的道具和金钱
	MonsterDropItemSetSingle::instance()->RunMonsterDropItemSet( pOwnerPlayer, pDeadMonster->GetMonsterDropSetID(), itemEleVec, money );
	newItemPkg->SetItemPkgOwnerMoney( money );
	//如果道具包为空，并且金钱数为0,即为空包，则不掉包裹
	if ( itemEleVec.empty() && money == 0 )
	{
		delete newItemPkg; newItemPkg = NULL;
		return NULL;
	}

	ACE_Time_Value timeValue = ACE_OS::gettimeofday();
	//道具包的创建时间
	newItemPkg->SetItemCreateTime( timeValue );

	//设置道具包的坐标位置
	TYPE_POS xpos ,ypos;
	pDeadMonster->GetPos( xpos, ypos );
	newItemPkg->SetItemPkgPos( xpos, ypos );

	//获取最新的道具包的编号
	s32 pkgIndex = GetPackageIndex();
	newItemPkg->SetItemPkgIndex( pkgIndex );

	//根据怪物创建对应的道具和金钱
	//这个排序效率较低，应换较快算法，且感觉不排序也没什么影响，因此暂时去除，待策划提出时重加，by dzj, 10.06.03, newItemPkg->SortItemEle();//排序包裹中的道具
	newItemPkg->SetOwner(pOwnerPlayer);//设置该道具包所属的玩家,或者组队
	newItemPkg->SetItemLiveItem( GetPkgItemLiveTime( newItemPkg->GetItemPkgType() ) );

	//包裹在地图上出现；
	newItemPkg->SetItemPkgOwnerMap( pMap );
	newItemPkg->SetMonsterID( pDeadMonster->GetMonsterID() );
	pMap->HandleItemPkgFirstAppear( newItemPkg );
	newItemPkg->MoneyAutoInPackage();

	m_PackageMan.insert( std::pair<s32,CItemsPackage*>( pkgIndex ,newItemPkg ) );

	IncPackageIndex();

	return newItemPkg;
	TRY_END;

	return NULL;
}

CItemsPackage* CItemPackageManager::DiedPlayerDropItems( CPlayer* pDeadPlayer )
{
	if ( NULL == pDeadPlayer )
	{
		D_ERROR("玩家死亡，掉落包裹失败，玩家指针为NULL\n");
		return NULL;
	}

	if ( !pDeadPlayer->IsDying() )
	{
		D_ERROR( "玩家%s死亡掉包，实际玩家当前未死亡\n", pDeadPlayer->GetAccount() );
		return NULL;
	}

	//@brief:获取地图信息
	CNewMap* pMap = pDeadPlayer->GetCurMap();
	if ( NULL == pMap )
	{
		D_ERROR( "CItemPackageManager::DiedPlayerDropItems, NULL == pMap, 不掉包\n" );
		return NULL;
	}

	std::vector<CItemPkgEle> dropItemVec;
	unsigned int dropmoney = 0;

	//如果是普通玩家,非红名玩家,死亡后只掉金钱
	if ( pDeadPlayer->GetGoodEvilPoint() >= 0 )
	{
		int precent=0, minmoney=0, maxmoney=0;
		DiePunishInfoSingle::instance()->GetSubMoneyInfo( precent, minmoney, maxmoney );

		unsigned int allmoney = pDeadPlayer->GetSilverMoney();//双币种判断,按姚斌：玩家掉落为银币
		if ( allmoney > 0 )
		{
			unsigned int submoney = allmoney * precent/100;
			if ( submoney < (unsigned int)minmoney )
				submoney = minmoney;
			else if( submoney > (unsigned int)maxmoney )
				submoney = maxmoney;

			if ( submoney > allmoney )
				submoney = allmoney;

			pDeadPlayer->SubSilverMoney( submoney );//双币种判断

			dropmoney = submoney;
		}
	} else if(  pDeadPlayer->GetGoodEvilPoint() < 0 ) {//如果是红名玩家，掉落身上的金钱和装备
		pDeadPlayer->GetItemDelegate().OnPlayerDieDropItem( dropItemVec, dropmoney );
	}

	if ( dropItemVec.empty() && dropmoney <= 0 )
	{
		return NULL;
	}

	CItemsPackage* newItemPkg = NEW CItemsPackage( pDeadPlayer->GetCurMap() );
	if ( NULL == newItemPkg )
	{
		D_ERROR( "CItemPackageManager::DiedPlayerDropItems，玩家死亡掉包，NULL == newItemPkg\n" );
		return NULL;
	}

	//获取最新的道具包的编号
	s32 pkgIndex = GetPackageIndex();
	newItemPkg->SetItemPkgIndex( pkgIndex );
	m_PackageMan.insert( std::pair<s32,CItemsPackage*>( pkgIndex ,newItemPkg ) );
	IncPackageIndex();

	//设为过了60秒，60秒后谁都能拾取包裹,实现了死亡掉落的包裹谁都能拾取的功能
	newItemPkg->PassSixtySeconds();

	//获取属于该包裹的道具列表
	std::vector<CItemPkgEle>& itemEleVec = newItemPkg->GetItemPkgItemEleVec();
	for ( std::vector<CItemPkgEle>::iterator lter = dropItemVec.begin();
		lter != dropItemVec.end(); 
		++lter )
	{
		CItemPkgEle& tmpInfo = *lter;
		itemEleVec.push_back( tmpInfo );
	}

	newItemPkg->SetItemPkgOwnerMoney( dropmoney );

	ACE_Time_Value timeValue =  ACE_OS::gettimeofday();
	//道具包的创建时间
	newItemPkg->SetItemCreateTime( timeValue );

	//设置道具包的坐标位置
	TYPE_POS xpos ,ypos;
	pDeadPlayer->GetPos( xpos, ypos );
	newItemPkg->SetItemPkgPos( xpos, ypos );

	//根据怪物创建对应的道具和金钱
	//这个排序效率较低，应换较快算法，且感觉不排序也没什么影响，因此暂时去除，待策划提出时重加，by dzj, 10.06.03, newItemPkg->SortItemEle();//排序包裹中的道具
	//修改BUG:197534 包裹不满3分钟就消失
	//newItemPkg->SetItemLiveItem( GetPkgItemLiveTime( newItemPkg->GetItemPkgType() ) );//设置包裹的生存周期
	newItemPkg->SetItemLiveItem( GetDeadPlayerItemPkgLiveTime() );//设置死亡玩家包裹的生存周期

	//包裹在地图上出现;
	newItemPkg->SetItemPkgOwnerMap( pMap );
	newItemPkg->SetPlayerID(  pDeadPlayer->GetFullID() );
	pMap->HandleItemPkgFirstAppear( newItemPkg );
	newItemPkg->MoneyAutoInPackage();

	return newItemPkg;
}


CItemsPackage* CItemPackageManager::GetItemPackage(s32 pkgIndex )
{
	CItemsPackage* itemPkg = NULL;

	TRY_BEGIN;

	std::map<s32,CItemsPackage *>::iterator lter = m_PackageMan.find(pkgIndex);
	if ( lter!= m_PackageMan.end() )
	{
		itemPkg = lter->second;
	}

	TRY_END;

	return itemPkg;
}

unsigned int CItemPackageManager::GetPkgItemLiveTime( unsigned int type )
{
	std::map<s32,unsigned int>::iterator lter = m_PackageTypeLiveTime.find( type );
	if ( lter != m_PackageTypeLiveTime.end() )
	{
		return lter->second;
	}
	return 0;
}

void CItemPackageManager::ReadItemPkgLiveTime(const char* pkgConfigXML )
{
	if ( !pkgConfigXML )
		return;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pkgConfigXML))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pkgConfigXML );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pkgConfigXML);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	for ( std::vector<CElement *>::iterator lter = childElements.begin(); 
		lter != childElements.end();
		++lter )
	{
		CElement* pElement = *lter;
		if ( pElement )
		{
			unsigned int itemTypeID = 0;
			if ( pElement->GetAttr("Type") )
				itemTypeID = ACE_OS::atoi( pElement->GetAttr("Type") );

			unsigned int VanishTime = 0;
			if ( pElement->GetAttr("VanishTime") )
				VanishTime = ACE_OS::atoi( pElement->GetAttr("VanishTime") );

			m_PackageTypeLiveTime[itemTypeID] = VanishTime;
		}
	}
	return;
}//void CItemPackageManager::ReadItemPkgLiveTime(const char* pkgConfigXML )


//设置道具，随机属性新生成
void CItemPkgEle::SetItemPropNewRand( unsigned int itemTypeID, unsigned char itemCount, unsigned int taskID, unsigned int itemLevel )
{
	mEleItemInfo.usItemTypeID = itemTypeID;
	mEleItemInfo.ucCount   = (char)itemCount;
	mEleItemInfo.ucLevelUp = (char)itemLevel;

	CItemPublicProp* pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
	if ( NULL == pItemPublicProp )
	{
		D_ERROR( "CItemPkgEle::SetItemPropExistRand, NULL == pItemPublicProp\n" );
		return;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//随机集相关属性填充...
	mEleItemInfo.randSet = 0;//初始化随机集相关属性
	mEleItemInfo.luckyid = 0;
	mEleItemInfo.excellentid = 0;

	//这是一个新装备，若当前属性有随机集，则给其赋上随机集/幸运随机种子/卓越随机种子
	bool isLucky = false, isExcellent = false;
	unsigned int propRandSet = pItemPublicProp->GetPropRandSet(isLucky, isExcellent);
	if (  propRandSet > 0 )
	{
		mEleItemInfo.randSet = propRandSet;
	}
	if ( isLucky )
	{
		mEleItemInfo.luckyid = RandNumber( 1, 65535 );
	}
	if ( isExcellent )
	{
		mEleItemInfo.excellentid = RandNumber( 1,65535 );
	}
	//...随机集相关属性填充
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	if(!ACE_BIT_ENABLED(pItemPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY))
	{
		mEleItemInfo.usWearPoint = pItemPublicProp->m_maxWear;
	} else {
		time_t now;
		ACE_OS::time(&now);
		mEleItemInfo.usWearPoint = (TYPE_ID)(now + pItemPublicProp->m_maxWear * 60);//上限以分钟为单位
	}

	mElePrice = pItemPublicProp->m_itemPrice;
	mEleMassLevel = (ITEM_MASSLEVEL)pItemPublicProp->m_itemMassLevel;
	mEleTaskID = taskID;

	return;
	return;
}// void CItemPkgEle::SetItemPropNewRand( unsigned int itemTypeID, unsigned char itemCount, unsigned int taskID, unsigned int itemLevel )


//设置道具信息的属性，随机属性手动赋入
void CItemPkgEle::SetItemPropExistRand( unsigned int itemTypeID, unsigned char itemCount ,unsigned int taskID ,unsigned int itemLevel, unsigned int randSet, unsigned int luckyID, unsigned int excelID )
{
	mEleItemInfo.usItemTypeID = itemTypeID;
	mEleItemInfo.ucCount   = (char)itemCount;
	mEleItemInfo.ucLevelUp = (char)itemLevel;

	CItemPublicProp* pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
	if ( NULL == pItemPublicProp )
	{
		D_ERROR( "CItemPkgEle::SetItemPropExistRand, NULL == pItemPublicProp\n" );
		return;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//随机集相关属性填充...
	mEleItemInfo.randSet = randSet;//设置随机集相关属性
	mEleItemInfo.luckyid = luckyID;
	mEleItemInfo.excellentid = excelID;
	//...随机集相关属性填充
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	if(!ACE_BIT_ENABLED(pItemPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY))
	{
		mEleItemInfo.usWearPoint = pItemPublicProp->m_maxWear;
	} else {
		time_t now;
		ACE_OS::time(&now);
		mEleItemInfo.usWearPoint = (TYPE_ID)(now + pItemPublicProp->m_maxWear * 60);//上限以分钟为单位
	}

	mElePrice = pItemPublicProp->m_itemPrice;
	mEleMassLevel = (ITEM_MASSLEVEL)pItemPublicProp->m_itemMassLevel;
	mEleTaskID = taskID;

	return;
} //void CItemPkgEle::SetItemPropExistRand( unsigned int itemTypeID, unsigned char itemCount ,unsigned int taskID ,unsigned int itemLevel, unsigned int randSet, unsigned int luckyID, unsigned int excelID )

//使用道具信息设新道具属性，用于玩家掉落
void CItemPkgEle::SetItemWithItemInfo_i( const ItemInfo_i& itemInfo, unsigned int price, ITEM_MASSLEVEL massLevel, unsigned int taskID )
{
	mEleItemInfo = itemInfo;
	mElePrice = price;
	mEleMassLevel = massLevel;
	mEleTaskID = taskID;
	return;
}


//void CItemPkgEle::SetNewItemEleProp(unsigned int itemTypeID, char itemCount ,unsigned int taskID ,unsigned int itemLevel /* = 0 */, unsigned int itemaddid /* = 0 */, unsigned int randSet )
//{
//	mEleItemInfo.usItemTypeID = itemTypeID;
//	mEleItemInfo.ucCount   = (char)itemCount;
//	mEleItemInfo.ucLevelUp = (char)itemLevel;
//
//	mEleItemInfo.randSet = randSet;//初始化随机集相关属性
//	mEleItemInfo.luckyid = luckyID;
//	mEleItemInfo.excellentid = excelID;
//
//	CItemPublicProp* pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
//	if ( NULL == pItemPublicProp )
//	{
//		D_ERROR( "CItemPkgEle::SetItemPropExistRand, NULL == pItemPublicProp\n" );
//		return;
//	}
//
//
//	/////////////////////////////////////////////////////////////////////////////////////////////////////
//	//随机集相关属性填充...
//	
//	//直接使用属性的幸运与卓越随机集号判定，随机集号可能会变化，另外，之前的判定根据幸运卓越类型来决定是否要随机属性，这样对于旧装备，逻辑判断会错
//	//************************************************************************/
//	///* 如果是可装备道具或者是武器,道具有随机幸运属性和随机卓越属性*/
//	///************************************************************************/
//	//bool isLuckyItem     =  false;
//	//bool isExcellentItem =  false;
//	//if ( pItemPublicProp->m_itemTypeID == E_TYPE_WEAPON )
//	//{
//	//	WeaponProperty* pWeaponProp = CWeaponPropertyManagerSingle::instance()->GetSpecialPropertyT( itemTypeID );
//	//	if ( pWeaponProp && pWeaponProp->luckytype > 0 )
//	//	{
//	//		isLuckyItem = true;
//	//		if ( pWeaponProp->luckytype == 2 )
//	//			isExcellentItem = true;
//	//	}
//	//}
//	//else if( pItemPublicProp->m_itemTypeID == E_TYPE_EQUIP )
//	//{
//	//	EquipProperty* pEquipProp =  CEquipPropertyManagerSingle::instance()->GetSpecialPropertyT( itemTypeID );
//	//	if ( pEquipProp && pEquipProp->luckytype > 0 )
//	//	{
//	//		isLuckyItem = true;
//	//		if ( pEquipProp->luckytype == 2 )
//	//			isExcellentItem = true;
//	//	}
//	//}
//
//	//if ( isLuckyItem )
//	//{
//	//	if ( 0 != randSet )
//	//	{
//	//		mEleItemInfo.randSet/*usAddId*/ = randSet;
//	//	} else {
//	//		mEleItemInfo.randSet/*usAddId*/ = pItemPublicProp->m_randSet;
//	//	}		
//	//	mEleItemInfo.luckyid = RandNumber( 1, 65535 );
//	//}
//
//	//if ( isExcellentItem )
//	//{
//	//	if ( 0 != randSet )
//	//	{
//	//		mEleItemInfo.randSet/*usAddId*/ = randSet;
//	//	} else {
//	//		mEleItemInfo.randSet/*usAddId*/ = pItemPublicProp->m_randSet;
//	//	}		
//	//	mEleItemInfo.excellentid = RandNumber( 1,65535 );
//	//}
//
//	mEleItemInfo.randSet = 0;//初始化随机集相关属性
//	mEleItemInfo.luckyid = 0;
//	mEleItemInfo.excellentid = 0;
//
//	if ( 0 != randSet )
//	{
//		//只要有显式指定随机集，则表明这是一个旧装备，必须按之前保存的随机集号初始化装备;
//		mEleItemInfo.randSet = randSet;
//	} else {
//		//这是一个新装备，若当前属性有随机集，则给其赋上随机集/幸运随机种子/卓越随机种子
//		bool isLucky = false, isExcellent = false;
//		unsigned int propRandSet = pItemPublicProp->GetPropRandSet(isLucky, isExcellent);
//		if (  propRandSet > 0 )
//		{
//			mEleItemInfo.randSet = propRandSet;
//		}
//		if ( isLucky )
//		{
//			mEleItemInfo.luckyid = RandNumber( 1, 65535 );
//		}
//		if ( isExcellent )
//		{
//			mEleItemInfo.excellentid = RandNumber( 1,65535 );
//		}
//	}
//
//	//...随机集相关属性填充
//	/////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	if(!ACE_BIT_ENABLED(pItemPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY))
//	{
//		mEleItemInfo.usWearPoint = pItemPublicProp->m_maxWear;
//	} else {
//		time_t now;
//		ACE_OS::time(&now);
//		mEleItemInfo.usWearPoint = (TYPE_ID)(now + pItemPublicProp->m_maxWear * 60);//上限以分钟为单位
//	}
//
//	mElePrice = pItemPublicProp->m_itemPrice;
//	mEleMassLevel = (ITEM_MASSLEVEL)pItemPublicProp->m_itemMassLevel;
//	mEleTaskID = taskID;
//}

//void CItemPkgEle::SetItemEleProp(const ItemInfo_i& itemInfo, unsigned int price , ITEM_MASSLEVEL massLevel,unsigned int taskID  )
//{
//	mEleItemInfo = itemInfo;
//	mElePrice = price;
//	mEleMassLevel = massLevel;
//	mEleTaskID = taskID;
//}

