﻿#include "ItemUpdateConfig.h"

#include "ItemLeavelUpCri.h"
#include "ItemLeavelUpMaterial.h"
#include "ItemLevelUpAttribute.h"
#include "SuitAttributeManager.h"

#include "../plate/plate.h"
#include "../Player/Player.h"
#include "../TaskIndexManager.h"
#include "ExpandStorageInfo.h"
#include "../OLTestQuestionlibrary.h"
#include "../MapPublicNoticeManager.h"


bool ItemUpdateConfig::LoadItemUpdateConfig()
{
	ExpandStorageInfoManagerSingle::instance()->LoadXML("config/storage/storageextend.xml");
	CItemLevelUpMaterial::LoadXML("config/item/itemlevelupmaterial.xml");
	CItemLeavelUpCriManager::LoadXML("config/item/itemlevelupcri.xml");
	ItemLevelUpAttributeManager::LoadXML("config/item/itemlevelupattribute.xml");
	CItemLevelUpModulus::LoadXML("config/item/itemlevelupmodulus.xml");
	CSuitManagerSingle::instance()->LoadXML("config/item/suitattribute.xml");
	CItemChangeMaterial::LoadXML("config/item/itemchangematerial.xml");
	ItemChangeCirManager::LoadXML("config/item/itemchangecri.xml");
	ItemCriManager::LoadXML("config/item/itemcri.xml");
	ItemChangeAttributeManager::LoadXML("config/item/addattribute.xml");
	TaskIndexInfoManagerSingle::instance()->LoadAllTaskXML();
	CItemPackageManagerSingle::instance()->ReadItemPkgLiveTime("config/item/itempkgconfig.xml");
	OLTestQuestionLibrary::ReadQuestionLib();
	MapPublicNoticeManager::LoadNoticeConfig();
	DiePunishInfoSingle::instance()->LoadXML();

	return true;
}


bool  ItemUpdateConfig::ReadLevelUpMaterial(CPlayer* pPlayer, MUX_PROTO::ItemInfo_i &mainItem, PlateCondition *levelUpCondition, unsigned int& levelUpID )
{
	if( !pPlayer )
		return false;

	if ( !levelUpCondition )
		return false;

	//获取该道具现在的各项信息
	CBaseItem* pItem = pPlayer->GetItemDelegate().GetItemByItemUID( mainItem.uiID );
	if ( !pItem )
		return false;

	//材料每5档变一级
	unsigned int criBaseID = GET_ITEM_LEVELUP( mainItem.ucLevelUp ) / 5 + 1;
	unsigned int levelUpCriID = pItem->GetUpdateItemMaterialID() * 100 + criBaseID;

	//读取升级物品的条件
	MaterialSet* pMaterialSet = CItemLevelUpMaterial::GetMaterialSet( levelUpCriID );
	if ( pMaterialSet )
	{
		pPlayer->SetTryExecPlateMoney( pMaterialSet->updateConsumeMoney );

		unsigned int conditionIndex = 0;
		std::vector<Material>::const_iterator lter = pMaterialSet->materialVec.begin();
		for ( ; lter != pMaterialSet->materialVec.end(); ++lter )
		{
			if ( !( conditionIndex < PLATE_ASSTPOS_NUM ) )
			{
				D_ERROR( "ItemUpdateConfig::ReadLevelUpMaterial, assert( conditionIndex < PLATE_ASSTPOS_NUM )\n" );
				return false;
			}

			if ( conditionIndex < PLATE_ASSTPOS_NUM )
			{
				const Material& material = *lter;
				PlateCondition& plateCondition = *(levelUpCondition+conditionIndex);
				plateCondition.conNum = material.count;
				plateCondition.conType = material.itemTypeID;
				conditionIndex++;
			}
			else
				break;
		}
	}

	//概率跟接下来的升级等级有关
	unsigned int nextLevel = GET_ITEM_LEVELUP( mainItem.ucLevelUp ) + 1;
	levelUpID = pItem->GetUpdateItemCriID() * 100 + nextLevel;

	return true;
}

bool ItemUpdateConfig::ReadLevelCri(CPlayer* pPlayer, unsigned int levelUp , LevelUpCri& levelUpCri )
{
	ACE_UNUSED_ARG( pPlayer );

	//读取升级到的概率
	LevelUpCri* pLevelCri = CItemLeavelUpCriManager::GetLevelUpCri( levelUp );
	if ( pLevelCri )
	{
		levelUpCri.level = pLevelCri->level;
		levelUpCri.odd   = pLevelCri->odd;
		levelUpCri.Sumodd = pLevelCri->Sumodd;
		levelUpCri.cri = pLevelCri->cri;
		levelUpCri.updateFailstart = pLevelCri->updateFailstart;
		levelUpCri.updateFailEnd  = pLevelCri->updateFailEnd;
		return true;
	}
	else
	{
		levelUpCri.level = levelUpCri.odd = levelUpCri.Sumodd = 0;
		return false;
	}
}


bool ItemUpdateConfig::ReadChangeMaterial(CPlayer* pPlayer, MUX_PROTO::ItemInfo_i &mainItem, PlateCondition *changeCondition )
{
	if ( !changeCondition )
		return false;

	if ( pPlayer == NULL )
		return false;

	//获取该道具现在的各项信息
	CBaseItem* pItem = pPlayer->GetItemDelegate().GetItemByItemUID( mainItem.uiID );
	if ( !pItem )
		return false;

	unsigned int itemLevel = pItem->GetItemPlayerLevel();
	unsigned int t1 = (itemLevel - 1)/30 +1;

	unsigned int itemMassLevel = pItem->GetItemMassLevel();
	unsigned int t2 = itemMassLevel;

	unsigned int itemGearType = (unsigned int)pItem->GetGearArmType();
	unsigned int t3 = 1;
	if( itemGearType >5 )
	{
		if ( itemGearType < 10  )
			t3 = 2;
		else
			t3 = 3;
	}

	unsigned int changeID = t1*100 + t2*10 + t3;

	MaterialSet* pMaterialSet = CItemChangeMaterial::GetMaterialSet( changeID );
	if ( pMaterialSet )
	{
		unsigned int conditionIndex = 0;

		//设置升级所需的金钱
		pPlayer->SetTryExecPlateMoney( pMaterialSet->updateConsumeMoney );

		std::vector<Material>::const_iterator lter = pMaterialSet->materialVec.begin();
		for ( ; lter != pMaterialSet->materialVec.end(); ++lter )
		{
			if ( !( conditionIndex < PLATE_ASSTPOS_NUM ) )
			{
				D_ERROR( "ItemUpdateConfig::ReadChangeMaterial, assert( conditionIndex < PLATE_ASSTPOS_NUM )\n" );
				return false;
			}

			if ( conditionIndex < PLATE_ASSTPOS_NUM )
			{
				const Material& material = *lter;
				PlateCondition& plateCondition = *(changeCondition+conditionIndex);
				plateCondition.conNum = material.count;
				plateCondition.conType = material.itemTypeID;
				conditionIndex++;
			}
			else
				break;
		}
	}

	return true;
}
