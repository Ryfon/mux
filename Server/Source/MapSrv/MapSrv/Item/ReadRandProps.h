﻿
/*
  道具随机属性读取，因为客户端可能使用不同的读取方法，故读取从ManItemRandomProps中单列出来
  by dzj, 05.18
*/

#ifndef READ_RAND_PROPS
#define READ_RAND_PROPS

#include "ManItemRandomProps.h"

//读取随机集并加入管理器
bool ReadItemRandomProps();

#endif //READ_RAND_PROPS


