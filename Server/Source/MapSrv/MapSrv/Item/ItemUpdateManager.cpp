﻿#include "ItemUpdateManager.h"
#include "ItemLeavelUpCri.h"
#include "ItemChangeCri.h"
#include "ItemUpdateConfig.h"
#include "../MuxMap/muxmapbase.h"
#include "../Player/Player.h"
#include "CItemIDGenerator.h"
#include "../LogManager.h"

bool ItemUpdateManager::ExecuteUpdate(CPlayer* pPlayer, ItemInfo_i* mainItem, ItemInfo_i* assistItem, EnumItemUpdate eUpdateType,unsigned int UpdateID, void* attachInfo )
{
	if ( !pPlayer || !mainItem || !assistItem  )
	{
		D_ERROR( "ItemUpdateManager::ExecuteUpdate, 输入参数空\n" );	
		return false;
	}

	bool isSuccess = false ;
	switch( eUpdateType )
	{
	//升级
	case E_LEVELUP:
		{
			isSuccess = ItemUpdateManager::ExecuteItemLevelUp( pPlayer, mainItem, assistItem, UpdateID );
		}
		break;
	//追加
	case E_ADDID:
		{
			isSuccess = ItemUpdateManager::ExecuteItemAddID( pPlayer, mainItem, assistItem, UpdateID );
		}
		break;

	//改造
	case E_CHANGE:
		{
			isSuccess = ItemUpdateManager::ExecuteItemChange( pPlayer, mainItem, assistItem, attachInfo );
		}
		break;
	default:
		break;
	}
	return isSuccess;
}


//读取升级道具所需要的资源
bool ItemUpdateManager::ReadLevelUpMaterial( CPlayer* pPlayer, MUX_PROTO::ItemInfo_i &mainItem, PlateCondition *levelUpCondition, unsigned int& levelUpID )
{
	return ItemUpdateConfig::ReadLevelUpMaterial( pPlayer,mainItem,levelUpCondition,levelUpID );
}

//读取追加道具所需要的资源
bool ItemUpdateManager::ReadAddIDMaterial( CPlayer* pPlayer, PlateCondition *levelUpCondition, unsigned int bulePrintID )
{
	if ( pPlayer == NULL )
		return false;

	CBaseItem* pBulePrint = pPlayer->GetItemDelegate().GetItemByItemUID( bulePrintID );
	if ( pBulePrint )
	{
		CBulePrintItem* pBuleItem = dynamic_cast<CBulePrintItem *>( pBulePrint );
		if ( pBuleItem )
		{
			CBulePrint* pBuleProp = pBuleItem->GetBulePrintProperty();
			if ( pBuleProp )
				pBuleProp->GetBulePrintCondition( levelUpCondition );
		}

		return true;
	}
	return false;
}

//读取改造道具所需要的资源
bool ItemUpdateManager::ReadChangeMaterial( CPlayer* pPlayer,MUX_PROTO::ItemInfo_i &mainItem, PlateCondition *changeCondition )
{
	return ItemUpdateConfig::ReadChangeMaterial(pPlayer, mainItem, changeCondition );
}

//执行道具升级
bool ItemUpdateManager::ExecuteItemLevelUp(CPlayer* pPlayer, ItemInfo_i* mainItem, ItemInfo_i* assistItem ,unsigned int updateID )
{
	if ( !pPlayer || !mainItem || !assistItem  )
	{
		D_ERROR( "ItemUpdateManager::ExecuteItemLevelUp, 输入参数空\n" );
		return false;
	}

	//读取升级的几率
	LevelUpCri tmpLevelUpCri;
	ItemUpdateConfig::ReadLevelCri( pPlayer, updateID, tmpLevelUpCri );

	int  mainItemIndex = -1;
	bool isEquip       = false;
	ItemInfo_i* mainItemInfo = pPlayer->GetItemInfoByItemUID( mainItem->uiID, mainItemIndex, isEquip );
	if ( !mainItemInfo )
	{
		D_ERROR( "ItemUpdateManager::ExecuteItemLevelUp, NULL == mainItemInfo, 玩家:%s, itemuiID:%d, updateID:%d\n"
			, pPlayer->GetAccount(), mainItem->uiID, updateID );
		return false;
	}

	if ( mainItemIndex == -1 )
	{
		D_ERROR( "ItemUpdateManager::ExecuteItemLevelUp, mainItemIndex == -1, 玩家:%s, itemuiID:%d, itemType:%d, updateID:%d\n"
			, pPlayer->GetAccount(), mainItem->uiID, mainItemInfo->usItemTypeID, updateID );
		return false;
	}

	//获取随机的范围
	unsigned int randomstart = 0 , randomend = 0;
	pPlayer->GetCommonPlate().OnCalculateLevelUpItemCri( pPlayer, randomstart, randomend );

	ItemInfo_i oldUpdateItemInfo = *mainItemInfo;//记录变化前的数值
	bool isMainItemChange = false;
	//开始随机
	unsigned int randResult = RAND%( randomend + 1);
	bool isUpdateSuccess = false;

	//如果道具升级成功
	if ( randResult <= randomstart )
	{
		mainItemInfo->ucLevelUp++;
		isMainItemChange =true;
		isUpdateSuccess  =true;
		CBaseItem* pPlayerItem = pPlayer->GetItemDelegate().GetEquipItem( mainItemInfo->uiID );
		if ( NULL == pPlayerItem )
		{
			pPlayerItem = pPlayer->GetItemDelegate().GetPkgItem( mainItemInfo->uiID );			
		}
		if ( NULL != pPlayerItem )
		{
			pPlayerItem->SetItemLevel(GET_ITEM_LEVELUP(mainItemInfo->ucLevelUp));//升级属性改变；
			mainItemInfo->usWearPoint = pPlayerItem->MaxWear();//重置耐久；		
		} else {
			D_ERROR( "ItemUpdateManager::ExecuteItemLevelUp, NULL == pPlayerItem, item uiID:%d, itemTypeID:%d, index:%d\n"
				, mainItemInfo->uiID, mainItemInfo->usItemTypeID, mainItemIndex );
			return false;
		}
	} else {
		//不降级,不丢弃
		if ( tmpLevelUpCri.updateFailstart == 0 )
		{
			D_DEBUG("玩家%s升级装备失败，但配置文件显示不丢弃和不降低道具等级\n", pPlayer->GetNickName() );
		} else if( tmpLevelUpCri.updateFailstart == -1 ) {//丢弃
			pPlayer->OnDropItem( mainItemInfo->uiID );
		} else if( tmpLevelUpCri.updateFailstart > 0 && tmpLevelUpCri.updateFailEnd >= tmpLevelUpCri.updateFailstart ) {//根据配置降低道具的
			unsigned int decLevel = RandNumber( tmpLevelUpCri.updateFailstart , tmpLevelUpCri.updateFailEnd );
			if ( decLevel > (unsigned int)GET_ITEM_LEVELUP( mainItemInfo->ucLevelUp ) )
			{
				RESET_ITEM_LEVEL( mainItemInfo->ucLevelUp );
			} else {
				mainItemInfo->ucLevelUp -= decLevel;
			}
			isMainItemChange =true;
		}
	}

	//通知客户端改变了道具
	if( isMainItemChange )
	{
		D_DEBUG("玩家道具升级，%d道具等级变为%d级\n", mainItemInfo->usItemTypeID, GET_ITEM_LEVELUP(mainItemInfo->ucLevelUp) );
		//如果是使用包裹中的道具
		if ( !isEquip )
		{
			pPlayer->NoticePkgItemChange( mainItemIndex );
		} else {//如果是使用装备栏中的物品升级
			//通知client装备的道具属性改变了
			pPlayer->NoticeEquipItemChange( mainItemIndex );
			//升级后,道具改变了玩家的属性
			pPlayer->OnUpdateEquipItemInfo( oldUpdateItemInfo, *mainItemInfo );
		}
	}

	//if(isUpdateSuccess)
	CLogManager::DoItemPropertyChange(pPlayer, LOG_SVC_NS::IPT_LEVEL, mainItemInfo->uiID, mainItemInfo->usItemTypeID, (unsigned int)GET_ITEM_LEVELUP(oldUpdateItemInfo.ucLevelUp), (unsigned int)GET_ITEM_LEVELUP(mainItemInfo->ucLevelUp) );//记日至

	return isUpdateSuccess;
}


//追加道具的ID号
bool ItemUpdateManager::ExecuteItemAddID(CPlayer* pPlayer, ItemInfo_i* mainItem, ItemInfo_i* assistItem ,unsigned int bulePrintUID )
{
	if (  !pPlayer || !mainItem || !assistItem )
		return false;

	CBaseItem* pBulePrint =  pPlayer->GetItemDelegate().GetItemByItemUID( bulePrintUID );
	if ( pBulePrint )
	{
		 //获取图纸对象，将是否追加成功由图纸负责
		CBulePrintItem* pBuleItem = dynamic_cast<CBulePrintItem *>( pBulePrint );
		if ( !pBuleItem )
			return false;

		CBulePrint* pBuleProp = pBuleItem->GetBulePrintProperty();
		if ( pBuleProp )
		{
			//随机的追加值
			unsigned addid = pBuleProp->BulePrintRandonResult();

			int  mainItemIndex = -1;
			bool isEquip = false;//是否是装备
			ItemInfo_i* itemInfo = pPlayer->GetItemInfoByItemUID( mainItem->uiID, mainItemIndex, isEquip );
			if ( !itemInfo )
				return false;

			if ( mainItemIndex == -1 )
				return false;

			ItemInfo_i oldAddItemInfo = *itemInfo;
			//去除追加，原位置用于随机集 itemInfo->randSet/*usAddId*/ = addid;

			if ( isEquip == false )//如果是包裹中的道具
				pPlayer->NoticePkgItemChange( mainItemIndex );
			else
			{
				//通知对应栏位的数据改变了
				pPlayer->NoticeEquipItemChange( mainItemIndex );

				//玩家的属性改变
				pPlayer->OnUpdateEquipItemInfo( oldAddItemInfo, *itemInfo );
			}

			//完了后，丢弃图纸
			pPlayer->OnDropItem( pBulePrint->GetItemUID() );

			//去除追加，原位置用于随机集 CLogManager::DoItemPropertyChange(pPlayer, LOG_SVC_NS::IPT_ADD, mainItem->uiID, mainItem->usItemTypeID, oldAddItemInfo.randSet/*usAddId*/, itemInfo->randSet/*usAddId*/);//记日至
			return true;
		}
	}

	return false;
}

//
bool ItemUpdateManager::ExecuteItemChange( CPlayer* pPlayer, ItemInfo_i* mainItem, ItemInfo_i* assistItem, void* attachInfo )
{
	if ( !pPlayer || !mainItem || !assistItem || !attachInfo  )
		return false;

	ChangeCriRd* pCri = ( ChangeCriRd *)attachInfo;

	bool isSuccess = false;
	//获取改造的编号
	if ( pCri )
	{
		int  mainItemIndex = -1;
		bool isEquip = false;
		//找寻在背包和装备栏中的位置
		ItemInfo_i* mainItemInfo = pPlayer->GetItemInfoByItemUID( mainItem->uiID ,mainItemIndex, isEquip );
		if ( !mainItemInfo || mainItemIndex == -1  )
			return false;

		ItemInfo_i olditeminfo =  *mainItemInfo;

		//来判断本次是不是改造成功
		unsigned int randIndex = RAND%( pCri->sumodd +1);
		if( randIndex <= pCri->odd )
		{
			//获取新的道具编号
			CBaseItem* pChangeMainItem = pPlayer->GetItemDelegate().GetItemByItemUID( mainItem->uiID );
			if ( !pChangeMainItem )
				return false;

			unsigned int newItemType = olditeminfo.usItemTypeID + 10;//新的道具ID
			//获取新道具的共有属性
			ITEM_TYPEID itemTypeID =pChangeMainItem->GetItemTypeID();
			CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( newItemType );
			if ( !pPublicProp )
			{
				D_ERROR("改造道具，新的道具属性不对 %d 道具表中没有此编号\n",newItemType );
#ifdef _DEBUG
				pPlayer->SendSystemChat("不能改造道具，因为您改造的道具已经是最终道具了");
#endif	
				return false;
			}

			//获取新道具的特有属性
			void* pSpecialProp = NULL;
			switch( itemTypeID )
			{
			case E_TYPE_WEAPON:
				{
					WeaponProperty* pWeaponProp = CWeaponPropertyManagerSingle::instance()->GetSpecialPropertyT(newItemType);
					pSpecialProp = (void *)pWeaponProp;
				}
				break;
			case E_TYPE_EQUIP:
				{
					EquipProperty* pEquipProp   = CEquipPropertyManagerSingle::instance()->GetSpecialPropertyT( newItemType );
					pSpecialProp = (void *)pEquipProp;
				}
				break;
			default:
				break;
			}

			//组合成一个新道具
			if ( pSpecialProp && pPublicProp )
			{
				//告诉client丢弃道具
				MGDropItemResult dropolditem;
				dropolditem.result = true;
				dropolditem.uiItemID = mainItemInfo->uiID;
				pPlayer->SendPkg<MGDropItemResult>( &dropolditem );

				pChangeMainItem->SetItemSpecialProp( pSpecialProp );
				pChangeMainItem->SetItemPublicProp( pPublicProp );

				mainItemInfo->usItemTypeID = newItemType;

				//改造后变成新的道具，获取新的道具编号
				unsigned int newItemUID = ItemIDGeneratorSingle::instance()->NewItemID();
				if ( isEquip == false )
				{
					mainItemInfo->uiID = newItemUID;
					pChangeMainItem->SetItemUID( mainItemInfo->uiID );
					pPlayer->NoticePkgItemChange( mainItemIndex );
				}
				else
				{
					//属性改变,算上新的属性,通知client
					pPlayer->OnUpdateEquipItemInfo( olditeminfo,*mainItemInfo );

					mainItemInfo->uiID = newItemUID;
					pChangeMainItem->SetItemUID( mainItemInfo->uiID );

					//通知对应栏位的数据改变了
					pPlayer->NoticeEquipItemChange( mainItemIndex );

				}

				isSuccess = true;
			}
		}
		else//如果失败了，则消失物品
		{
			pPlayer->OnDropItem( mainItem->uiID );
			/*if ( isEquip )
				pPlayer->NoticeEquipItemChange( mainItemIndex );*/
		}

		CLogManager::DoItemPropertyChange(pPlayer, LOG_SVC_NS::IPT_MASS, olditeminfo.uiID, olditeminfo.usItemTypeID, olditeminfo.usItemTypeID, mainItemInfo->usItemTypeID);
	}

	return isSuccess;
}
