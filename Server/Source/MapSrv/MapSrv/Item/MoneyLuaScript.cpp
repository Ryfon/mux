﻿#include "MoneyLuaScript.h"


lua_State* CMoneyScriptExtent::L;


long CMoneyScriptExtent::CalMoneyFunc( long money, float randmin, float randmax )
{
	ACE_UNUSED_ARG( money );
	ACE_UNUSED_ARG( randmin );
	ACE_UNUSED_ARG( randmax );

	//CLuaRestoreStack rs( L );//准备恢复堆栈

	//lua_pushnumber(L, money);
	//lua_pushnumber(L, randmin);
	//lua_pushnumber(L, randmax);

	//if ( lua_pcall(L,3,1,0) != 0 )
	//	return -1;

	//if ( !lua_isnumber(L,-1) )
	//	return -1;

	//long result = (long)lua_tonumber(L,-1);
	//return result;
	return 0;
}

bool CMoneyLuaScript::Init(const char *luaScriptFile)
{
	if( LoadFrom<CMoneyScriptExtent>(luaScriptFile) )
	{
		CMoneyScriptExtent::GetCallFuncLuaState("CalMoneyFuc");
		return true;
	}
	return false;
}

long CMoneyLuaScript::RunScript(long money, float randmin, float randmax)
{
	return CMoneyScriptExtent::CalMoneyFunc(money,randmin,randmax);
}

