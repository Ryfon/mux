﻿#include "ItemLeavelUpMaterial.h"
#include "XmlManager.h"
#include <assert.h>
#include "../../../Base/Utility.h"


std::map< unsigned, MaterialSet> CItemLevelUpMaterial::mMaterialSetMap;

void CItemLevelUpMaterial::LoadXML(const char* pxmlFile )
{
	if ( !pxmlFile )
		return;

	D_DEBUG("LoadXML:%s\n", pxmlFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pxmlFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pxmlFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pxmlFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	for ( std::vector<CElement *>::iterator lter = childElements.begin();
		lter!= childElements.end();
		++lter )
	{
		PushMaterialSet( *lter );
	}

	TRY_END;
}

void CItemLevelUpMaterial::PushMaterialSet( CElement* pxmlElement )
{
	if ( !pxmlElement )
		return;

	unsigned int materialID = 0 ;

	MaterialSet materialset;
	const char* p = pxmlElement->GetAttr("ID");
	if ( p )
	{
		materialID = ACE_OS::atoi( p );
	}
	else
	{
		D_ERROR(" ItemLevelUpMaterial 读取XMLID时出错\n");
		return;
	}

	const char* pszUpdateMoney = pxmlElement->GetAttr("UpgradeMoney");
	if ( pszUpdateMoney )
		materialset.updateConsumeMoney =  ACE_OS::atoi( pszUpdateMoney );

	static char signFormat[] = {"material%d"};
	for ( int i = 1 ; i<= 10; i++ )
	{
		char szField[56]={0};
		ACE_OS::snprintf(
			szField,
			sizeof(szField),
			signFormat,
			i
			);

		const char* pMaterial = pxmlElement->GetAttr(szField);
		if ( pMaterial )
		{
			Material material;
			p = ACE_OS::strtok( const_cast<char *>( pMaterial ), ",");
			if ( p )
			{
				material.itemTypeID = ACE_OS::atoi( p );
				if ( p )
				{
					p = ACE_OS::strtok( NULL, ",");
					if ( p )
					{
						material.count = ACE_OS::atoi( p );
						materialset.materialVec.push_back( material );
					}
				}
			}		
		}
		else
		{
			return;
		}
	}

	if ( materialID )
		mMaterialSetMap.insert( std::pair<unsigned, MaterialSet>( materialID, materialset ) );
}

MaterialSet* CItemLevelUpMaterial::GetMaterialSet(unsigned long itemID )
{
	std::map< unsigned, MaterialSet>::iterator lter = mMaterialSetMap.find(itemID);
	if ( lter != mMaterialSetMap.end() )
	{
		return &( lter->second );
	}
	else
		return NULL;
}

/************************************************************************/
/*   改造                                                               */
/************************************************************************/
std::map<unsigned ,MaterialSet > CItemChangeMaterial::mChangeMaterialSetMap;

void CItemChangeMaterial::LoadXML(const char *pxmlFile)
{
	if ( !pxmlFile )
		return;

	D_DEBUG("LoadXML:%s\n", pxmlFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pxmlFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pxmlFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pxmlFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	for ( std::vector<CElement *>::iterator lter = childElements.begin();
		lter!= childElements.end();
		++lter )
	{
		PushChangeMaterialSet( *lter );
	}

	TRY_END;
}

void CItemChangeMaterial::PushChangeMaterialSet(CElement *pxmlElement)
{
	if( !pxmlElement )
		return;

	if ( !pxmlElement )
		return;

	unsigned int materialID = 0 ;

	MaterialSet materialset;
	const char* p = pxmlElement->GetAttr("ID");
	if ( p )
	{
		materialID = ACE_OS::atoi( p );
	}
	else
	{
		D_ERROR(" CItemChangeMaterial 读取XMLID时出错\n");
		return;
	}

	const char* q = pxmlElement->GetAttr("ChangeMoney");
	if ( q )
	{
		materialset.updateConsumeMoney =  ACE_OS::atoi( q );
	}
	else
	{
		D_ERROR(" CItemChangeMaterial 读取 updateConsumeMoney 错误\n");
		return;
	}

	static char signFormat[] = {"material%d"};
	for ( int i = 1 ; i<= 10; i++ )
	{
		char szField[56]={0};
		ACE_OS::snprintf(
			szField,
			sizeof(szField),
			signFormat,
			i
			);

		const char* pMaterial = pxmlElement->GetAttr(szField);
		if ( pMaterial )
		{
			Material material;
			p = ACE_OS::strtok( const_cast<char *>( pMaterial ), ",");
			if ( p )
			{
				material.itemTypeID = ACE_OS::atoi( p );
				if ( p )
				{
					p = ACE_OS::strtok( NULL, ",");
					if ( p )
					{
						material.count = ACE_OS::atoi( p );
						materialset.materialVec.push_back( material );
					}
				}
			}		
		}
		else
		{
			return;
		}
	}

	if ( materialID )
		mChangeMaterialSetMap.insert( std::pair<unsigned, MaterialSet>( materialID, materialset ) );
}

MaterialSet* CItemChangeMaterial::GetMaterialSet(unsigned long itemID )
{
	std::map< unsigned, MaterialSet>::iterator lter = mChangeMaterialSetMap.find(itemID);
	if ( lter != mChangeMaterialSetMap.end() )
	{
		return &( lter->second );
	}
	else
		return NULL;
}
