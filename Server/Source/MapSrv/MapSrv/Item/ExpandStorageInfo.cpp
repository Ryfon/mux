﻿#include "ExpandStorageInfo.h"
#include "XmlManager.h"
#include "../../../Base/Utility.h"

void ExpandStorageInfoManager::LoadXML(const char* pXMLFileName )
{
	if ( !pXMLFileName )
		return;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pXMLFileName))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pXMLFileName );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pXMLFileName);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	//遍历所有的XML中的节点
	for ( std::vector<CElement*>::iterator lter = childElements.begin(); lter != childElements.end(); ++ lter )
	{
		CElement* pElement = *lter;
		if ( !pElement )
			continue;

		unsigned int currow  = ACE_OS::atoi( pElement->GetAttr("Num1") );
		unsigned int nextrow = ACE_OS::atoi( pElement->GetAttr("Num2") );
		char*  pmoney  = const_cast<char *>( pElement->GetAttr("money") );
		if ( !pmoney )
			continue;

		char*  pitem   = const_cast<char *>( pElement->GetAttr("item") );
		if ( !pitem )
			continue;

		//记录仓库扩充的信息
		ExpandStorageInfo* pNewStorageInfo = NEW ExpandStorageInfo;
		if ( !pNewStorageInfo )
			return;

		pNewStorageInfo->curRow  = currow;//当前行数
		pNewStorageInfo->nextRow = nextrow;//扩充后的下一个行数
		pNewStorageInfo->money   = ACE_OS::atoi( pmoney );//设置金钱

		unsigned int index = 0;
		std::vector<char *> expandMaterialVec;
		char  *q  = ACE_OS::strtok( pitem , ";");//依据分拆字符串
		while( q )
		{
			expandMaterialVec.push_back( q );
			q = ACE_OS::strtok( NULL, ";");
		}

		unsigned int materialLen  = (unsigned int)expandMaterialVec.size();
		for ( unsigned int i = 0 ; i < materialLen; i+= 2  )
		{
			char* _item  = expandMaterialVec[i];
			char* _count = expandMaterialVec[i+1];
			if ( !_item || !_count )
				continue;

			ExpandCosumeItem consumeItem; 
			consumeItem.itemTypeID = ACE_OS::atoi( _item  );
			consumeItem.itemCount  = ACE_OS::atoi( _count );
			pNewStorageInfo->consumeItemVec.push_back( consumeItem );
		}

		mExpandStorageInfoMap.insert( std::pair<unsigned int, ExpandStorageInfo *>(currow, pNewStorageInfo ) );
	}
}
