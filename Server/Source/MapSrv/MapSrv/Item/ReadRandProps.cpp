﻿
#include "ReadRandProps.h"

#include "XmlManager.h"

#include <vector>

using namespace std;

bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
{
	if ( NULL == pEle || NULL == attName )
	{
		return false;
	}
	if ( NULL == pEle->GetAttr( attName ) )
	{
		D_ERROR( "装备随机属性XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
		return false;
	}
	return true;
}

//读取随机集并加入管理器
bool ReadItemRandomProps()
{
	const char* pfilepath = "config/item/luckyexcel.xml";
	D_DEBUG( "开始读取%s\n", pfilepath );

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(pfilepath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pfilepath );
		return false;
	} 

	CElement *pRoot = xmlLoader.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pfilepath);
		return false;
	}

	//randProp成员
	RPropID        attrID;//Player::GetItemAttributeByAttriID(unsigned int attributeID )
	unsigned int   weight;//在集中所占权重
	unsigned short minVal;//随出来的最小值
	unsigned short maxVal;//随出来的最大值
	CRandProp      randProp;//当前读取的随机属性
	CRandSet       randSet;//当前读取的随机集

	//randSet成员
	unsigned int   randSetID;//随机集号；
	unsigned int   minPropNum;//随机属性个数最小值
	unsigned int   maxPropNum;//随机属性个数最大值

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		D_ERROR("%s解析失败\n",pfilepath);
		return false;
	}
	CElement* tmpEle = NULL;

	unsigned int readnum = 0;
	unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if ( tmpEle->Level() == rootLevel+1 )
		{
			if ( readnum > 0 )
			{
				CManItemRandSets::AddOneRandSets( randSet );//添加一个随机集;
			}
			++readnum;
			randProp.ResetRandProp();
			randSet.ResetRandSet();
			//新shop层;
			if ( !IsXMLValValid( tmpEle, "ID", readnum ) )
			{
				return false;
			}
			randSetID = atoi( tmpEle->GetAttr( "ID" ) );//随机集ID号
			if ( !IsXMLValValid( tmpEle, "MinPropNum", readnum ) )
			{
				return false;
			}
			minPropNum = atoi( tmpEle->GetAttr( "MinPropNum" ) );//最少随机属性数量
			if ( !IsXMLValValid( tmpEle, "MaxPropNum", readnum ) )
			{
				return false;
			}
			maxPropNum = atoi( tmpEle->GetAttr( "MaxPropNum" ) );//最大随机属性数量

			randSet.SetRandSetInfo( randSetID, minPropNum, maxPropNum );
			//以下继续读入该商店的各个标签；
			continue;
		}
		
		randProp.ResetRandProp();
		if ( tmpEle->Level() == rootLevel+2 )
		{
			if ( !IsXMLValValid( tmpEle, "Attribute", readnum ) )
			{
				return false;
			}
			int tmpID = atoi( tmpEle->GetAttr( "Attribute" ) );//属性标识号（物攻/魔攻等等）
			if ( tmpID>=(int)IRA_max )
			{
				D_ERROR( "%s中错误的属性标识号%d\n", pfilepath, tmpID );
				return false;
			}
			attrID = (RPropID)tmpID;
			if ( !IsXMLValValid( tmpEle, "Weight", readnum ) )
			{
				return false;
			}
			weight = atoi( tmpEle->GetAttr( "Weight" ) );//在集中的权重
			if ( !IsXMLValValid( tmpEle, "MinVal", readnum ) )
			{
				return false;
			}
			minVal = (unsigned short)atoi( tmpEle->GetAttr( "MinVal" ) );//随出最小值
			if ( !IsXMLValValid( tmpEle, "MaxVal", readnum ) )
			{
				return false;
			}
			maxVal = (unsigned short)atoi( tmpEle->GetAttr( "MaxVal" ) );//随出最大值
			if ( minVal > maxVal )
			{
				D_ERROR( "%s中属性%d的最小值%d大于最大值%d\n", pfilepath, attrID, minVal, maxVal );
				return false;
			}
			randProp.SetRandPropVal( attrID, weight, minVal, maxVal );
			randSet.AddOneRandProp( randProp );
		}
	}

	if ( readnum > 0 )
	{
		CManItemRandSets::AddOneRandSets( randSet );//添加最后一个随机集;
	}

	D_DEBUG( "读取%s完毕\n", pfilepath );

	return true;
}
