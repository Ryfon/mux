﻿#include "RepairWearRule.h"
#include "../../../Base/XmlManager.h"
#include "../../../Base/Utility.h"
#include "../Player/Player.h"


CRepairWearRuleManager::CRepairWearRuleManager(void)
{
	INVALD_VALUE = 0xff;
	for( unsigned int i=0; i < sizeof(m_wear)/sizeof(m_wear[0]); ++i )
	{	
		for( unsigned int j=0; j < sizeof(m_wear[0])/sizeof(m_wear[0][0]); ++j )
		{
			m_wear[i][j] = INVALD_VALUE;
		}
	}
}

CRepairWearRuleManager::~CRepairWearRuleManager(void)
{

}

int CRepairWearRuleManager::LoadXML(const char* pszFileName)
{
	if(pszFileName == NULL)
		return -1;

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR("耐久度配置文件加载出错\n");
		return -1;
	}

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("耐久度配置文件可能为空\n");
		return -1;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);

	int levelCount, massLevelCount, levelUpCount;
	levelCount = massLevelCount = levelUpCount = 0;
	int maxCount = sizeof(m_wear[0])/sizeof(m_wear[0][0]);
	const char *pTemp = NULL;
	CElement *pElement = NULL;

	//策划提供的配置文件比较怪异，参考wear.xml
	for(std::vector<CElement *>::iterator i = childElements.begin(); i != childElements.end(); i++)
	{	
		pElement = *i;
		pTemp = pElement->GetAttr("Level");
		if( pTemp != NULL )
		{
			if(levelCount < maxCount)
			{
				m_wear[LEVEL_INDEX][levelCount] = ACE_OS::atoi(pElement->GetAttr("LevelCri"));
				levelCount++;
			}
		}
		else
		{
			pTemp = pElement->GetAttr("MassLevel");
			if( pTemp != NULL)
			{	
				if(massLevelCount < maxCount)
				{
					m_wear[MASS_LEVEL_INDEX][massLevelCount] = ACE_OS::atoi(pElement->GetAttr("MassLevelCri"));
					massLevelCount++;
				}
			}
			else
			{
				pTemp = pElement->GetAttr("LevelUp");
				if( pTemp != NULL )
				{
					if(levelUpCount < maxCount)
					{
						m_wear[LEVEL_UP_INDEX][levelUpCount] = ACE_OS::atoi(pElement->GetAttr("LevelUpCri"));
						levelUpCount++;
					}
				}
			}
		}
	}

	return 0;
	TRY_END;
	return -1;
}

unsigned int CRepairWearRuleManager::CalcRepairWearMoney(unsigned int uiWear, unsigned short usLevel, unsigned short usMassLevel, unsigned short usLevelUp)
{
	unsigned short usLeastWear = 100;
	if(uiWear < usLeastWear)//(内部耐久度100对应外部1点，如果内部耐久度消耗小于100则返回)
		return 0;

	unsigned short usLevelCri, usMassLevelCri, usLevelUpCri;

	usLevelCri = GetValue(LEVEL_INDEX, (usLevel - 1) / 10 + 1); 
	usMassLevelCri  = GetValue(MASS_LEVEL_INDEX, usMassLevel);
	usLevelUpCri = GetValue(LEVEL_UP_INDEX, GET_ITEM_LEVELUP((char)usLevelUp) );

	if((INVALD_VALUE == usLevelCri) || (INVALD_VALUE == usMassLevelCri) || (INVALD_VALUE == usLevelUpCri))
		return 0;
	else
		return uiWear * (usLevelCri + usMassLevelCri + usLevelUpCri) / usLeastWear;
}

int CRepairWearRuleManager::GetValue(eWearIndex tIndex1, int iIndex2)
{
	if(tIndex1 > LEVEL_UP_INDEX)
		return INVALD_VALUE;

	if( iIndex2 > 
		(int)(sizeof(m_wear[0])/sizeof(m_wear[0][0])) 
		)
	{
		return INVALD_VALUE;
	}

	return m_wear[tIndex1][iIndex2];
}
