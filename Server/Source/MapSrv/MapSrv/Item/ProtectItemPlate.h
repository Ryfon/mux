﻿/********************************************************************
	created:	2009/06/03
	created:	3:6:2009   14:04
	file:		ProtectItemPlate.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef PROTECTITEM_PLATE_H
#define PROTECTITEM_PLATE_H

#include "ProtectItemManager.h"


class CProtectItemPlate
{
public:
	CProtectItemPlate():m_pAttachPlayer(NULL),m_plateType(E_PROTECT_PLATE_INVALID),m_isPutItem(false),m_cosumeItemUID(0)
	{
		StructMemSet( m_protectMainItem , 0x0, sizeof(ItemInfo_i) );
	}

	~CProtectItemPlate(){}

public:
	void AttachPlayer( CPlayer* pPlayer ) 
	{  
		m_pAttachPlayer = pPlayer; 
	}

	//显示保护道具托盘
	void ShowProtectItemPlate( PROTECTITEM_PLATE_TYPE plateType );

	//取消保护道具托盘状态
	void CancelPlate() { ResetPlate(); }

	//把道具放到保护托盘中
	void PutItemToProtectPlate( unsigned int itemUID );

	//把道具从保护托盘中取走
	void TakeItemFromProtectPlate( unsigned int itemUID );

	//开始执行托盘程序
	void TryExecPlate();

	//设置显示托盘所消耗的道具
	void SetConsumeItem( unsigned int itemUID ) { m_cosumeItemUID = itemUID; }

	void CossumeItem();

private:
	void ResetPlate()
	{ 
		m_cosumeItemUID = 0;
		m_plateType = E_PROTECT_PLATE_INVALID;
		m_isPutItem = false;
		m_protectMainItem.ucCount = m_protectMainItem.ucLevelUp = m_protectMainItem.uiID = m_protectMainItem.randSet/*usAddId*/ = m_protectMainItem.usItemTypeID = m_protectMainItem.usWearPoint =0; 
	}

	void OnTakeProtectItem()
	{
		m_isPutItem = false;
		m_protectMainItem.ucCount = m_protectMainItem.ucLevelUp = m_protectMainItem.uiID = m_protectMainItem.randSet/*usAddId*/ = m_protectMainItem.usItemTypeID = m_protectMainItem.usWearPoint =0; 
	}


	PROTECTITEM_PLATE_TYPE GetProtectPlateType() { return m_plateType ; }

	void SetProtectPlateType( PROTECTITEM_PLATE_TYPE inPlateType ) {  m_plateType = inPlateType ;}

private:
	CPlayer* m_pAttachPlayer;
	PROTECTITEM_PLATE_TYPE m_plateType;
	ItemInfo_i m_protectMainItem;
	bool       m_isPutItem;
	unsigned int m_cosumeItemUID;
};

#endif
