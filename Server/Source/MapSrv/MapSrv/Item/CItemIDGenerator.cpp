﻿
#include "CItemIDGenerator.h"
#include "../G_MProc.h"
using namespace MUX_PROTO;


//获取一个新的ItemID编号
unsigned long CItemIDGenerator::NewItemID()
{
	unsigned long  itemID = _now;
	IncID();

	if ( CheckNeedRequestNewID() )
	{
		RequestNewIDRange();
	}

	return itemID;
}

//@brief:当接到具体的ID范围时
void CItemIDGenerator::OnGetNewIDRange( unsigned long newbegin, unsigned long newend )
{

	if ( newbegin > newend )
	{
		D_ERROR("接受的ID号错误,开始小于结束 begin:%d  < end:%d\n",newbegin,newend );
		return;
	}

	if ( newbegin < _now )
	{
		D_ERROR("新获取的ID号比本地保存的ID号码还小 本地:%d  接收的:%d\n",_now,newbegin );
		return;
	}

	_now = _begin = newbegin;
	_end = newend;
	isRequest = true;
}

//@brief:判断是否需要发生ID请求
bool CItemIDGenerator::CheckNeedRequestNewID()
{
	return ( _end - _now ) <= IDRANGE_GUARD ;
}

//@brief:请求新的ID范围
void CItemIDGenerator::RequestNewIDRange()
{
	CGateSrv* pProc = CManG_MProc::FindRandGateSrv();
	if ( pProc )
	{
		MGBatchItemSequence srvmsg;
#ifdef GATE_SENDV
		bool isNewPkg = false;
		MsgToPut* pNewMsg = CreateGateSrvPkg( MGBatchItemSequence, pProc, srvmsg, isNewPkg );
		if ( isNewPkg )
		{
			pProc->SendOldRsvNew( pNewMsg );
		}
#else  //GATE_SENDV
		MsgToPut* pMsg = CreateSrvPkg( MGBatchItemSequence, pProc, srvmsg);
		pProc->SendPkgToGateServer(pMsg);
#endif //GATE_SENDV
	}
}


//@brief:当服务器刚起来时，向DB服务器请求被赋予的ItemID的范围
void CItemIDGenerator::Init()
{
	RequestNewIDRange();
}

