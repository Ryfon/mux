﻿/** @file MoneyLuaScript.h
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-6-17
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#include "../Lua/LuaScript.h"

class CMoneyScriptExtent
{
public:
	static void Init( lua_State* pLuaState )
	{
		L = pLuaState;
	}

	static void GetCallFuncLuaState( const char* pFuncName )
	{
		lua_getglobal(L,pFuncName);
	}

	static long CalMoneyFunc( long money, float randmin, float randmax );

private:
	static lua_State* L;
};


class CMoneyLuaScript:public CLuaScriptBase
{
public:
	CMoneyLuaScript(){}
	virtual ~CMoneyLuaScript(){}

private:
	CMoneyLuaScript( const CMoneyLuaScript&);
	CMoneyLuaScript& operator=( const CMoneyLuaScript& );

public:
	///根据脚本初始化；
	bool Init( const char* luaScriptFile );

	long RunScript( long money, float randmin, float randmax );

};
