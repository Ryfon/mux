﻿/** @file LoadDropItemXML.h
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-6-16
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef LOAD_DROPITEMXML_H
#define LOAD_DROPITEMXML_H

#include "MonsterDropItemManager.h"
#include "MoneyGenerator.h"
#include "CItemPackage.h"
#include <vector>

class CElement;
class CBaseItem;
class CPlayer;

class FindSameItemEle:public std::binary_function<CItemPkgEle, unsigned int,bool >
{
public:
	bool operator()( CItemPkgEle& pItemEle, unsigned int itemTypeID ) const 
	{
		if( pItemEle.GetItemEleInfo().usItemTypeID == itemTypeID )
			return true;

		return false;
	}
};


/************************************************************************/
/*         普通XML格式的掉落模板                                        */
/************************************************************************/

class ILoadDropCommonRuleXML
{
public:
	ILoadDropCommonRuleXML(){}

	//加载XML文件
	virtual void LoadXML( const char* pszXMLPath );

	//插入XML的详细信息
	virtual void InsertXMLDetailInfo( std::vector<CElement*>& pxmlElementVec );

	virtual ~ILoadDropCommonRuleXML(){};
};


//掉落普通道具
class LoadDropCommonItemRule:public ILoadDropCommonRuleXML
{
	friend class ACE_Singleton<LoadDropCommonItemRule, ACE_Null_Mutex>;	
	public:
		//constructor
		LoadDropCommonItemRule(){};

		//destructor 
		~LoadDropCommonItemRule(){};
};
typedef ACE_Singleton<LoadDropCommonItemRule, ACE_Null_Mutex> DropCommonItemRuleSingle;

//掉落标准道具
class LoadDropStandByItemRule:public ILoadDropCommonRuleXML
{
	friend class ACE_Singleton<LoadDropStandByItemRule, ACE_Null_Mutex>;
};
typedef ACE_Singleton<LoadDropStandByItemRule, ACE_Null_Mutex> DropStandByItemRuleSingle;

//掉落特殊道具
class LoadDropSpecialItemRule:public ILoadDropCommonRuleXML
{
	friend class ACE_Singleton<LoadDropSpecialItemRule, ACE_Null_Mutex>;
};
typedef ACE_Singleton<LoadDropSpecialItemRule, ACE_Null_Mutex> DropSpecialItemRuleSingle;



/************************************************************************/
/*                   怪物所掉的宝物的XML配置                            */
/************************************************************************/
struct SetID2Count
{
	long setID;//记录集的ID号
	int  randCount;//随机的个数
};

class CMonsterDropItemSet
{
	friend class ACE_Singleton<CMonsterDropItemSet, ACE_Null_Mutex>;	

public:
	void LoadXML( const char* pszXMLFile );

	void InsertXMLDetialInfo( std::vector<CElement* >& xmlElementVec );

public:
	void  RunMonsterDropItemSet( CPlayer* pPlayer ,long setID, std::vector<CItemPkgEle>& itemEleVec, long& money );
private:
	std::multimap<long,SetID2Count> mMonsterDropItemMap;
};
typedef ACE_Singleton<CMonsterDropItemSet, ACE_Null_Mutex> MonsterDropItemSetSingle;



/************************************************************************/
/*           掉落任务物品的规则                                         */
/************************************************************************/

struct DropTaskItemRule
{
public:
	DropTaskItemRule( long itemID, long value,long taskID ):mItemID(itemID),mValue(value),mTaskID(taskID){};

	~DropTaskItemRule(){}

public:
	long mItemID;
	long mValue;
	long mTaskID;
};



class CTaskDropItemSet
{
	friend class ACE_Singleton<CTaskDropItemSet, ACE_Null_Mutex>;

public:
	CTaskDropItemSet(){}

	~CTaskDropItemSet()
	{
		std::map<long, DropTaskItemRule *>::iterator lter = mDropTaskItemMap.begin();
		for( ; lter != mDropTaskItemMap.end(); ++lter )
		{
			if ( NULL != lter->second )
			{
				delete lter->second;
			}
		}
		mDropTaskItemMap.clear();
	}

public:
	void LoadXML( const char * pszXMLFile );

	void InsertXMLDetialInfo( std::vector<CElement*>& pxmlElementVec );

	bool IsTaskItemDropSet( unsigned long setID );

	/*CBaseItem* RunDropTaskItem( long setID, CPlayer* pPlayer, std::vector<CBaseItem *>& itemVec );*/

	bool RunDropTaskItem( long setID, CPlayer* pPlayer, std::vector<CItemPkgEle>& itemEleVec  );

private:
	std::map<long, DropTaskItemRule *> mDropTaskItemMap;
};

typedef ACE_Singleton<CTaskDropItemSet, ACE_Null_Mutex> CDropTaskItemSetSingle;


#endif
