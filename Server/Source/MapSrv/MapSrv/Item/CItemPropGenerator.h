﻿
#ifndef ITEMPROP_GENERATOR_H
#define ITEMPROP_GENERATOR_H

//#include "../../../Base/itemgloal.h"
#include "../../../Base/Utility.h"
#include "CSpecialItemProp.h"
#include <vector>
#include <map>
using namespace ITEM_GLOBAL;


#define  ADDITIONPROP_MAX 19

class PropertyFunctor
{
public:
	PropertyFunctor(s32 offset, s32 ratio, s32 _mi, s32 _mx ):_PropOffset(offset),_ratio(ratio),_max(_mx),_min(_mi){}
	
	s32 GetFunctorID() { return _PropOffset; }
	
	s32 Generator( CBaseItem* pItem )
	{
		ACE_UNUSED_ARG(pItem);
		//if ( !pItem )
		//	return 0;

		//if ( _PropOffset < 0 || _PropOffset>= ADDITIONPROP_MAX  )
		//	return 0;

		//int* pAdditionInfo = (int *)pItem->GetEquipAdditionInfo();

		//int rt = ACE_OS::rand()% 100;
		//if ( rt < _ratio )
		//{
		//	int * a = pAdditionInfo + _PropOffset;
		//	*a =  RandNumber( _min,_max );

		//	//D_DEBUG("生成随机属性:ItemID:%d Value:%d  Offset:%d \n", pItem->GetItemID(), *a ,_PropOffset  );

		//	return *a;
		//}
		///*else
		//{
		//	D_DEBUG("分配道具属性时，没有随机到该属性:%d \n", _PropOffset );
		//}*/
		return 0;
	}
	

protected:
	s32 _PropOffset;//所针对的字段
	s32 _ratio;//随机比率
	s32 _max;//最大值
	s32 _min;//最小值
};


class PropertyGenerator
{
public:
	typedef std::vector<PropertyFunctor *> PropertyFuncVec;
	typedef PropertyFuncVec::iterator PropertyFuncVecIter;

public:
	~PropertyGenerator() { Release(); } 

public:
	void Release()
	{
		PropertyFuncVecIter lter = mFunctorVec.begin();
		for ( ; mFunctorVec.end()!= lter; )
		{
			if ( NULL != (*lter) )
			{
				delete *lter;
			}
			lter = mFunctorVec.erase(lter);
		}
	}

	PropertyFunctor* GetPropFunctor( s32 id )
	{
		PropertyFunctor* pFuctor = NULL;

		PropertyFuncVecIter lter = mFunctorVec.begin();
		for ( ;lter!= mFunctorVec.end(); ++lter )
		{
			if (  (*lter)->GetFunctorID() == id )
			{
				pFuctor = (*lter);
				break;
			}
		}
		return pFuctor;
	}

	void PushPropertyFunctor( PropertyFunctor* _functor )
	{
		mFunctorVec.push_back( _functor );
	}

	void Generator( CBaseItem *val )
	{
		PropertyFuncVecIter lter = mFunctorVec.begin();
		for ( ;lter!= mFunctorVec.end(); ++lter )
		{
			if ( NULL != (*lter) )
			{
				(*lter)->Generator( val );
			}
		}
	}
private:
	PropertyFuncVec mFunctorVec;
};


class PropertyGeneratorManager
{
	friend class ACE_Singleton<PropertyGeneratorManager, ACE_Null_Mutex>;

public:
	typedef std::map<s32,PropertyGenerator*> PropGeneratorMap;
	typedef PropGeneratorMap::iterator PropGeneratorMapIter;
	
public:
	~PropertyGeneratorManager()
	{
		PropGeneratorMapIter lter = mGeneratorMap.begin();

		for ( ;lter != mGeneratorMap.end() ;lter++ )
		{
			if ( NULL != lter->second )
			{
				delete lter->second;
			}
		}

		mGeneratorMap.clear();
	}

	PropertyGenerator* GetPropertyGenerator( s32 itemID )
	{
		PropertyGenerator* p = NULL;

		PropGeneratorMapIter lter = mGeneratorMap.find( itemID );
		if ( lter!= mGeneratorMap.end() )
		{
			p = lter->second;
		}
		return p;
	}

	void PushPropertyGenerator( s32 itemID, PropertyGenerator* propGener )
	{
		if ( !propGener )
			return;

		if ( GetPropertyGenerator(itemID) != NULL  )
			return;

		mGeneratorMap.insert( std::pair<s32,PropertyGenerator*>(itemID, propGener) );
	}

	bool Generator( CBaseItem* _item )
	{
		bool isSuccess = false;

		s32 itemTypeID = _item->GetItemID();

		PropertyGenerator*  pGenerator = GetPropertyGenerator(itemTypeID);
		if ( pGenerator!=NULL )
		{
			pGenerator->Generator( _item );
			isSuccess = true;
		}

		return isSuccess;
	}

	//@创建装备和道具的随机属性发生器
	bool CreateItemPropGenerator( s32 itemID, s32 offset, const char* pTokenStr )
	{
		bool isSuccess = false;
		PropertyGenerator* pGenerator = GetPropertyGenerator( itemID );

		if (  !pGenerator )
		{
			pGenerator = NEW PropertyGenerator;
			if (!pGenerator )
				return false;

			PushPropertyGenerator(itemID, pGenerator);
		}

		if ( pGenerator->GetPropFunctor(offset)!= NULL )//重复属性
		{
			D_DEBUG("插入重复属性！ ItemID:%d  Index:%d  Value:%s\n",itemID,offset,pTokenStr );
		}
		else
		{
			do 
			{
				int   valueArr[3] = {0,0,0};
				int   index = 0;

				char* p = ACE_OS::strtok( const_cast<char*>(pTokenStr),"," );
				while ( p )
				{
					if (index >= ARRAY_SIZE(valueArr))
					{
						D_ERROR( "PropertyGeneratorManager::CreateItemPropGenerator,index(%d) >= ARRAY_SIZE(valueArr)\n",index);
						break;
					}
					valueArr[index++] = ACE_OS::atoi( p );
					p = ACE_OS::strtok(NULL, ",");
				}

				if ( index!= 3 )
				{
					D_DEBUG("解析错误! ItemID:%d  Index:%d  Value:%s\n",itemID,offset,pTokenStr  );
					break;
				}

				if ( valueArr[0] == 0 )//如果概率为0
					break;

				if ( valueArr[1] > valueArr[2] )// 如果所规定的概率范围错误
				{
					D_DEBUG("概率范围错误! ItemID:%d  Index:%d  Value:%s\n",itemID,offset,pTokenStr  );
					break;
				}

				PropertyFunctor* pNewFunctor = NEW PropertyFunctor( offset ,valueArr[0], valueArr[1],valueArr[2] );
				if ( pNewFunctor )
				{
					pGenerator->PushPropertyFunctor( pNewFunctor );

					//D_DEBUG(" Item %d  Index:%d  Value:%s \n",itemID, offset ,pTokenStr );

					isSuccess = true;
				}

			} while( !isSuccess );
		}
		return isSuccess;
	}
private:
	PropGeneratorMap mGeneratorMap;
};

typedef ACE_Singleton<PropertyGeneratorManager, ACE_Null_Mutex> PropertyGeneratorManagerSingle;

#endif
