﻿#include "ProtectItemPlate.h"
#include "../../../Base/PkgProc/SrvProtocol.h"
#include "../Player/Player.h"

void CProtectItemPlate::ShowProtectItemPlate( PROTECTITEM_PLATE_TYPE plateType  )
{
	if ( !m_pAttachPlayer )
		return;

	if ( plateType > E_PROTECT_PLATE_INVALID )
	{
		D_ERROR("%s 显示保护道具托盘错误 托盘类型%d \n", m_pAttachPlayer->GetNickName(),plateType );
		return;
	}
	else
	{
		ResetPlate();

		SetProtectPlateType( plateType );

		MGShowProtectPlate showPlate;
		showPlate.plateType = GetProtectPlateType();
		m_pAttachPlayer->SendPkg<MGShowProtectPlate>( &showPlate );
	}
}

void CProtectItemPlate::PutItemToProtectPlate( unsigned int itemUID  )
{
	if ( !m_pAttachPlayer )
		return;

	ADD_PROTECTITEM_RST addItemRst = E_CAN_NOT_PROECTED;//初始为不可保护状态

	do 
	{
		//获取当前的托盘类型
		PROTECTITEM_PLATE_TYPE protectPlateType =  GetProtectPlateType();

		//保护托盘处于不可用状态
		if ( protectPlateType == E_PROTECT_PLATE_INVALID )
		{
			D_ERROR("%s 托盘处于非可用状态,但确放入物品\n",m_pAttachPlayer->GetNickName() );
			break;
		}

		//检查道具是否为玩家包裹中的物品
		CBaseItem* pItem = m_pAttachPlayer->GetItemDelegate().GetPkgItem( itemUID );
		if ( !pItem )//只对可装备物品进行保护
		{
			D_ERROR("放入保护托盘的道具在玩家%s包裹中找不到\n", m_pAttachPlayer->GetNickName() );
			addItemRst = E_CAN_NOT_PROECTED;
			break;
		}

		if ( ( protectPlateType == E_PROTECT_NORMAL ) || ( protectPlateType == E_PROTECT_SPECIAL ) )//如果是保护道具托盘(普通/特殊)
		{
			if ( pItem->GetItemUseType() != E_EQUIP )//非装备不能被保护
			{
				addItemRst = E_CAN_NOT_PROECTED;
				break;
			}
		}
		else //如果需要解除保护(普通/特殊)
		{
			//检查是否为保护道具
			ProtectItem* pProtectItem = m_pAttachPlayer->GetProtectItemManager().FindItemIsProtected( itemUID );
			bool isCheckSuc = true;
			if ( !pProtectItem )//如果不是给出对应的提示
			{
				isCheckSuc = false;
			}
			else if( pProtectItem->protectTime > 0 )//还在解除保护时间内
			{
				isCheckSuc = false;
			}

			if ( !isCheckSuc )
			{
				if ( protectPlateType == E_UNPROTECT_SPECIAL )
					addItemRst = E_UNPROTECT_NOT_SPECIALPROTECT_ITEM;
				else
					addItemRst = E_UNPROTECT_NOT_NORMALPROTECT_ITEM;

				break;
			}
		}

	
		//获取该道具此时的保护类型
		PROTECTED_ITEM_TYPE protectType = E_NO_PROTECT;//如果找不到该道具,则默认为非保护的
		ProtectItem* pProtectItem = m_pAttachPlayer->GetProtectItemManager().FindItemIsProtected( itemUID );
		if ( pProtectItem )
			protectType = pProtectItem->protectType;

		if ( protectType== E_NO_PROTECT )//如果是没有保护过的道具,则都可以顺利加上保护
		{
			if( protectPlateType == E_PROTECT_NORMAL || protectPlateType == E_PROTECT_SPECIAL  )
				addItemRst = E_ADD_ITEM_SUCCESS;
			else
			{
				if( protectPlateType == E_UNPROTECT_SPECIAL  )
					addItemRst = E_UNPROTECT_NOT_SPECIALPROTECT_ITEM;
				else if ( protectPlateType == E_UNPROTECT_NORMAL )
					addItemRst = E_UNPROTECT_NOT_NORMALPROTECT_ITEM;
			}
		}
		else if( protectType == E_NORMAL_PROTECT )//如果已经是普通保护的道具
		{
			if ( protectPlateType == E_UNPROTECT_NORMAL )//则只能放入解除普通保护的托盘
			{
				addItemRst = E_ADD_ITEM_SUCCESS;
			}
			//否则返回错误
			else if( protectPlateType == E_PROTECT_SPECIAL )//如果是特殊保护托盘,普通保护道具能放入特殊保护托盘
			{
				addItemRst = E_ADD_ITEM_SUCCESS;
			}
			else if( protectPlateType == E_PROTECT_NORMAL  )//如果是普通保护的托盘
			{
				addItemRst = E_CAN_NOT_PROECTED;
			}
			else if( protectPlateType == E_UNPROTECT_SPECIAL )//如果放入的是特殊保护的托盘
			{
				addItemRst = E_UNPROTECT_NOT_SPECIALPROTECT_ITEM;
			}
		}
		else if(  protectType == E_SPECIAL_PROTECT )//如果已经是特殊保护的道具
		{
			if ( protectPlateType == E_UNPROTECT_SPECIAL )//只能放入解除特殊保护的托盘
			{
				//如果超过了解除特殊保护道具的等待个数
				if ( m_pAttachPlayer->GetProtectItemManager().IsReachMaxUnSpecialItemWaitCount() )
				{
					addItemRst = E_REACH_MAX_PROTECT_SPECIALITEM_COUNT;//数量达到限制
				}
				else
				{
					addItemRst = E_ADD_ITEM_SUCCESS;
				}

			}
			//否则返回错误
			else if ( protectPlateType == E_PROTECT_NORMAL )//如果是普通保护的托盘
			{
				addItemRst = E_ADD_SPECIALITEM_TO_NORMALPLATE;
			}
			else if( protectPlateType == E_UNPROTECT_NORMAL )//如果是解除普通保护的托盘
			{
				addItemRst = E_UNPROTECT_NOT_NORMALPROTECT_ITEM;
			}
			else if( protectPlateType == E_PROTECT_SPECIAL )//如果是特殊保护的托盘
			{
				addItemRst = E_CAN_NOT_PROECTED;
			}
		}
	} while( false );
	
	//如果放入成功
	if ( addItemRst == E_ADD_ITEM_SUCCESS )
	{
		FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CProtectItemPlate::PutItemToProtectPlate, NULL == GetFullPlayerInfo()\n");
			return;
		}
		PlayerInfo_3_Pkgs& pkgInfo = playerInfo->pkgInfo;
		for ( unsigned int i = 0 ; i< PACKAGE_SIZE; i++ )
		{
			ItemInfo_i& itemInfo = pkgInfo.pkgs[i];
			if ( itemInfo.uiID == itemUID )
			{
				m_protectMainItem = itemInfo;
				break;
			}
		}
		m_isPutItem = true;
	}


	MGPutItemToProtectPlateRst putItemRst;
	putItemRst.addItemRst = addItemRst;
	m_pAttachPlayer->SendPkg<MGPutItemToProtectPlateRst>( &putItemRst );
}


void CProtectItemPlate::TakeItemFromProtectPlate(unsigned int itemUID )
{
	if ( !m_pAttachPlayer )
		return;

	//如果放入过物品,则取走物品
	if ( m_isPutItem )
	{
		OnTakeProtectItem();

		MGTakeItemFromProtectPlateRst takeRst;
		takeRst.itemUID = itemUID;
		takeRst.takeRst = 0;
		m_pAttachPlayer->SendPkg<MGTakeItemFromProtectPlateRst>( &takeRst );
	}
}

void CProtectItemPlate::TryExecPlate()
{
	if ( !m_pAttachPlayer )
		return;

	if ( !m_isPutItem )
	{
		D_ERROR("%s 还未放入保护物品,无法保护道具\n", m_pAttachPlayer->GetNickName() );
		return;
	}

	//检验装备是否合法
	CBaseItem* pItem = m_pAttachPlayer->GetItemDelegate().GetPkgItem( m_protectMainItem.uiID );
	if ( !pItem )
	{
		D_ERROR("%s 保护操作时,玩家的道具不在玩家的包裹内\n",m_pAttachPlayer->GetNickName() );
		return;
	}

	PROTECTITEM_PLATE_TYPE protectPlateType =  GetProtectPlateType();

	switch( protectPlateType )
	{
	case E_PROTECT_SPECIAL:
		{
			ProtectItem* pProtectItem = m_pAttachPlayer->GetProtectItemManager().FindItemIsProtected( m_protectMainItem.uiID );
			if ( pProtectItem && pProtectItem->protectType == E_NORMAL_PROTECT  )
			{
				pProtectItem->protectType = E_SPECIAL_PROTECT;
				m_pAttachPlayer->GetProtectItemManager().NoticeItemProtectPropUpdate( *pProtectItem, E_UPDATE_PROTECT_ITEM );
			}
			else
			{
				ProtectItem newSpecialProtect;
				newSpecialProtect.protectItemUID = m_protectMainItem.uiID;
				newSpecialProtect.protectType    = E_SPECIAL_PROTECT;
				newSpecialProtect.protectTime    = 0;
				m_pAttachPlayer->GetProtectItemManager().PushNewProtectItem( newSpecialProtect );
			}
		}
		break;
	case E_PROTECT_NORMAL:
		{
			ProtectItem newNormalProtect;
			newNormalProtect.protectItemUID = m_protectMainItem.uiID;
			newNormalProtect.protectType    = E_NORMAL_PROTECT;
			newNormalProtect.protectTime    = 0;
			m_pAttachPlayer->GetProtectItemManager().PushNewProtectItem( newNormalProtect );
		}
		break;
	case E_UNPROTECT_SPECIAL:
		{
			m_pAttachPlayer->GetProtectItemManager().CancelItemSpecialProtect( m_protectMainItem.uiID  );
		}
		break;
	case E_UNPROTECT_NORMAL:
		{
			m_pAttachPlayer->GetProtectItemManager().RemoveProtectItem( m_protectMainItem.uiID );
		}
		break;
	default:
		break;
	}

	CossumeItem();
}

void CProtectItemPlate::CossumeItem()
{
	if ( !m_pAttachPlayer )
		return;

	if ( m_cosumeItemUID != 0 )
	{
		//PROTECTITEM_PLATE_TYPE protectPlateType =  GetProtectPlateType();
		//if ( protectPlateType == E_PROTECT_NORMAL || protectPlateType == E_UNPROTECT_NORMAL )
		{
			FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
			if ( NULL == playerInfo )
			{
				D_ERROR("CProtectItemPlate::CossumeItem, NULL == GetFullPlayerInfo()\n");
				return;
			}
			PlayerInfo_3_Pkgs& pkgInfo = playerInfo->pkgInfo;
			for ( unsigned int i = 0 ; i< PACKAGE_SIZE; i++ )
			{
				ItemInfo_i& itemInfo = pkgInfo.pkgs[i];
				if ( itemInfo.uiID == m_cosumeItemUID )
				{
					if (!m_pAttachPlayer->IsLeftUpPkgIndex(i,itemInfo.uiID))
					{
						continue;
					}
					itemInfo.ucCount--;
					if ( itemInfo.ucCount == 0 )
						m_pAttachPlayer->OnDropItem( itemInfo.uiID );
					else
						m_pAttachPlayer->NoticePkgItemChange( i );

					return;
				}
			}
		}
	}
}

