﻿#include "UseItemRule.h"
#include "CEquipItemManager.h"
#include "ItemManager.h"

#include "../Player/Player.h"
#include "LoadDropItemXML.h"
#include "../LogManager.h"

void UseItemRuleExecute::UseItem( CPlayer* pPlayer, unsigned long itemUID )
{
	if ( !pPlayer)
		return;

	//玩家死亡,不能使用道具
	if ( pPlayer->IsDying() )
		return;

	TRY_BEGIN;

	//@brief:找出该道具在玩家内存中的编号,检查道具是否合法
	int  index    = -1;
	bool isEquip  = false;
	ItemInfo_i* pItemInfo = pPlayer->GetItemInfoByItemUID( itemUID,index,isEquip );
	if ( pItemInfo == NULL || index == -1 )
		return;

	ItemInfo_i sItemInfo = *pItemInfo;

	//使用的道具在不在玩家身上
	if ( index == PACKAGE_SIZE )
	{
		D_ERROR("玩家%s作弊，使用了不属于该玩家的道具 %d\n", pPlayer->GetNickName(),itemUID  );
		return;
	}

	CBaseItem* pItem = pPlayer->GetItemDelegate().GetPkgItem( sItemInfo.uiID );
	if ( !pItem )
	{
		D_ERROR("玩家%s使用道具%d，但该道具在玩家的包裹管理器中找不到\n", pPlayer->GetNickName(), sItemInfo.uiID );
		return;
	}
	
	CItemPublicProp* prop = pItem->GetItemPublicProp();
	if ( !prop )
		return;

	//如果是时间道具，检查是否过期
	if ( ACE_BIT_ENABLED( prop->m_wearConsumeMode,CItemPublicProp::LAST_DAY ) )
	{
		time_t now = time(NULL);
		if( (unsigned int)now > sItemInfo.usWearPoint )
		{
			pPlayer->SendSystemChat("使用的道具已经过期，不能使用");
			return;
		}
	}

	bool isUseSuccess = false;
	ITEM_TYPE itemUseType = pItem->GetItemUseType();
	if ( itemUseType == E_BULEPRINT )//如果是图纸道具
	{
		isUseSuccess= UseBulePrintRule::Execute( pPlayer, pItem, index );
		//if ( isUseSuccess )
		//{
		//	//EquipItemManagerSingle::instance()->OnUseItemResult( pPlayer,sItemInfo );
		//	//使用物品计数器,使用成功,使用物品计数器+1
		//	pPlayer->TryCounterEvent( CT_USEITEM, sItemInfo.usItemTypeID , 1 );
		//}
	}
	else if( itemUseType == E_SKILL )//如果是技能书
	{
		isUseSuccess = UseSkillBookRule::Execute( pPlayer, pItem, index );
		//if ( isUseSuccess )
		//{
		//	//使用物品计数器,使用成功,使用物品计数器+1
		//	pPlayer->TryCounterEvent( CT_USEITEM, sItemInfo.usItemTypeID , 1 );
		//}			
	}
	else//如果是其他类型的道具
	{
		//看这个道具是不是玩家能使用的,如果能使用，则进行下面的操作,否则告诉客户端使用失败
		if ( pItem->IsCanUse( pPlayer, pPlayer->GetRace(), pPlayer->GetClass() ,pPlayer->GetLevel() ) )
		{
			//if ( itemUseType != E_CONSUME )
			{
				//@brief:根据不同的类型，使用不同的使用道具规则
				switch( itemUseType )
				{
				case E_FUNCTION://功能性物品
					{
						isUseSuccess = UseFunctionRule::Execute( pPlayer, pItem, index );
					}
					break;
				case E_AXIOLOGY://价值物
					{
						isUseSuccess = UseAxiologyRule::Execute( pPlayer, pItem, index );
					}
					break;
				case E_TASK://任务道具
					{
						isUseSuccess =  pPlayer->OnChatToItem( pItem, 0 );//第一次使用，阶段0；
					}
					break;
				case E_FASHION://时装
					{
						isUseSuccess = UseFashionRule::Execute( pPlayer, itemUID );
						//if(isUseSuccess) 
						//{
						//	//使用物品计数器,使用成功,使用物品计数器+1
						//	pPlayer->TryCounterEvent( CT_USEITEM, sItemInfo.usItemTypeID , 1 );
						//	return;
						//}
					}
					break;
				case E_CONSUME://可消耗道具
					{
						//可消耗道具
						if ( pPlayer->GetCastManager().IsInCast() )
						{
							pPlayer->GetCastManager().OnStop();
						}
						isUseSuccess = UseConsumeRule::Execute( pPlayer, pItem, index );
					}
					break;
				default:
					{
						D_ERROR("%s 使用了错误的类型道具 类型编号 %d \n",pPlayer->GetNickName(), (unsigned int)itemUseType );
						break;
					}
				}

			
			}
			//else
			//{		
			//	//可消耗道具
			//	isUseSuccess = UseConsumeRule::Execute( pPlayer, pItem, index );
			//	if( isUseSuccess )
			//	{
			//		//使用物品计数器,使用成功,使用物品计数器+1
			//		pPlayer->TryCounterEvent( CT_USEITEM, sItemInfo.usItemTypeID , 1 );
			//	}
			//}
		}
		else
		{
			pPlayer->SendSystemChat("使用的道具不符合玩家的职业，种族和等级");
		}
	}

	if(isUseSuccess)//如果使用道具成功
	{
		//使用物品计数器,使用成功,使用物品计数器+1
		pPlayer->TryCounterEvent( CT_USEITEM, sItemInfo.usItemTypeID , 1 );


		CLogManager::DoUseItem(pPlayer, sItemInfo);//记日至
	}

	TRY_END;
}

//使用装备或者武器
bool UseEquipRule::Execute( CPlayer* pPlayer ,CBaseItem* pItem, unsigned int pkgIndex,unsigned int equipIndex,unsigned int equipReturnIndex )
{
	if ( !pPlayer ||  !pItem )
	{
		D_ERROR( "UseEquipRule::Execute，输入参数空\n" );
		return false;
	}

	if( pPlayer->IsFaint() )
	{
		D_ERROR( "UseEquipRule::Execute，玩家%s处于眩晕态，不可装备\n", pPlayer->GetAccount() );
		return false;
	}

	if (  equipIndex >= EQUIP_SIZE )
	{
		D_ERROR("玩家%s 装备道具位置错误 位置:%d \n", pPlayer->GetNickName(),equipIndex  );
		return false;
	}

	if ( pkgIndex >= PACKAGE_SIZE )
	{
		D_ERROR("玩家%s 装备包裹中的物品位置错误 位置:%d \n", pPlayer->GetNickName(), pkgIndex );
		return false;
	}

	TRY_BEGIN;

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("UseEquipRule::Execute, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& itemPkgs  = playerInfo->pkgInfo;//包裹信息
	if ( pkgIndex >= ARRAY_SIZE(itemPkgs.pkgs) )
	{
		D_ERROR("UseEquipRule::Execute, pkgIndex >= ARRAY_SIZE(itemPkgs.pkgs)\n");
		return false;
	}
	ItemInfo_i pkgItemInfo = itemPkgs.pkgs[pkgIndex];//获取包裹中的这个道具
	unsigned int leftUpPkgIndex = 0;
	pPlayer->GetItemInfoByPkgIndex(pkgIndex,pkgItemInfo,leftUpPkgIndex);

	CItemPublicProp* prop = pItem->GetItemPublicProp();
	if ( NULL == prop )
	{
		D_ERROR( "UseEquipRule::Execute，玩家%s装备包裹%d物品%d的属性找不到，装备失败\n", pPlayer->GetAccount(), pkgIndex, pkgItemInfo.usItemTypeID );
		return false;
	}

	//如果是时间道具,检查是否过期
	if ( ACE_BIT_ENABLED( prop->m_wearConsumeMode,CItemPublicProp::LAST_DAY ) )
	{
		time_t now = time(NULL);
		if( (unsigned int)now > pkgItemInfo.usWearPoint )
		{
			pPlayer->SendSystemChat("使用的道具已经过期，不能使用");
			return false;
		}
	}

	ITEM_GEARARM_TYPE gearArmType = pItem->GetGearArmType();//获取要装备上去的道具的位置
	if ( gearArmType == E_DRIVE /*&& pPlayer->IsRideState()*/ )
	{
		CRideState& ridestate = pPlayer->GetSelfStateManager().GetRideState();	////队长如果在装备模式下,不能换,队员可以换装备
		if ( ridestate.IsActive() )
		{
			if (   ridestate.GetPlayerRideState() == E_RIDE_EQUIP 
				&& ridestate.GetPlayerRideType()  == CRideState::E_LEADER )
			{
				D_ERROR( "UseEquipRule::Execute，玩家%s装备包裹%d物品%d，骑乘队不可换此装备\n", pPlayer->GetAccount(), pkgIndex, pkgItemInfo.usItemTypeID );
				return false;
			}
		}

		////队长不能换,队员可以换装备
		//if( pPlayer->GetSelfStateManager().GetRideState().GetPlayerRideType() ==  CRideState::E_LEADER )
		//{
		//	return false;
		//}
	}

	if( gearArmType != E_POSITION_MAX )
	{
		bool isCanEquip = false;
		//检测现在的装备位置上是不是有装备了
		//比较该装备是否能够装备在该位置上
		if ( IsEquipPosAccord( gearArmType, equipIndex ) )
		{
			isCanEquip = true;
		}
 
		//如果位置错误，则不能装备该物品
		if ( !isCanEquip ) 
		{
			D_ERROR( "UseEquipRule::Execute，玩家%s装备包裹%d物品%d，装备位置校验错误(%d)\n"
				, pPlayer->GetAccount(), pkgIndex, pkgItemInfo.usItemTypeID, equipIndex );
			return false;
		}

		ItemInfo_i* doubleHandEquip = NULL;			//装备副手装备时，是否已经装备双手装备
		unsigned int doubleHandEquipIndex = 0;
		ItemInfo_i* secondHandEquip = NULL;			//装备双手装备时，是否已经装备副手装备
		unsigned int secondHandEquipIndex = 0;
		//如果是双手装备，则同时判断副手是否空，若非空，则仍然装备失败(最终装配位置为主手，其它地方会做替换判断，因此这里不需检查主手是否空)
		if ( E_DOUBLEHAND == gearArmType )
		{
			//检查副手;
			bool fooBool = false;
			unsigned int equipPos = 0, fooPos2 = 0;
			bool isEquipPosOK = GetDefaultEquipPos( E_SECONDHAND, equipPos, fooBool, fooPos2 );
			if ( !isEquipPosOK )
			{
				D_ERROR("UseEquipRule::Execute, 取副手装配位失败\n");
				return false;
			}
			ItemInfo_i* pTmpItem = pPlayer->GetEquipByPos( equipPos );
			if ( NULL == pTmpItem )
			{
				D_ERROR("UseEquipRule::Execute, equipPos(%d)，装备双手武器检查副手，GetEquipByPos失败\n", equipPos);
				return false;
			}
			if ( PlayerInfo_2_Equips::INVALIDATE != pTmpItem->uiID )
			{
				//D_ERROR( "玩家%s装备包裹位置%d中物品，该物品为双手装备，而玩家副手非空，装备失败\n", pPlayer->GetAccount(), equipPos );
				//return false;
				if (pPlayer->IsItemPkgFull())
				{
					D_DEBUG("玩家%s包裹已满，无法卸下副手装备\n",pPlayer->GetAccount());
					return false;
				}
				secondHandEquip = pTmpItem;
				secondHandEquipIndex = equipPos;
			}
		} else if (( E_SECONDHAND == gearArmType ) || ( E_SINGLEHAND == gearArmType )) { //如果是副手或单手装备，要判断不能装有双手装备
			bool fooBool = false;
			unsigned int equipPos = 0, fooPos2 = 0;
			bool isEquipPosOK = GetDefaultEquipPos( E_DOUBLEHAND, equipPos, fooBool, fooPos2 );
			if ( !isEquipPosOK )
			{
				D_ERROR("UseEquipRule::Execute, 取双手装配位失败\n");
				return false;
			}
			ItemInfo_i* pTmpItem = pPlayer->GetEquipByPos( equipPos );
			if ( NULL == pTmpItem )
			{
				D_ERROR("UseEquipRule::Execute, equipPos(%d)，装备副手武器检查双手，GetEquipByPos失败\n", equipPos);
				return false;
			}
			if ( PlayerInfo_2_Equips::INVALIDATE != pTmpItem->uiID )
			{
				CBaseItem * pEquipItem = pPlayer->GetItemDelegate().GetItemByItemUID( pTmpItem->uiID );
				if ( NULL != pEquipItem )
				{
					if ( E_DOUBLEHAND == pEquipItem->GetGearArmType() )
					{
						//D_DEBUG("玩家%s已装备双手装备，试图装副手装备，装备失败\n", pPlayer->GetAccount() );
						//return false;
						doubleHandEquip = pTmpItem;
						doubleHandEquipIndex = equipPos;
					}
				}
			}
		}

		//判断将要装备的物品的位置是否已经有了装备
		ItemInfo_i* pTgtPosEquip = NULL;
		if ( NULL != doubleHandEquip )
		{
			pTgtPosEquip = doubleHandEquip;
		}
		else
		{
			pTgtPosEquip = pPlayer->GetEquipByPos( equipIndex );
			if ( NULL == pTgtPosEquip )
			{
				D_DEBUG("玩家%s装备item，取目标位置(%d)item信息失败\n", pPlayer->GetAccount(), equipIndex );
				return false;
			}
		}

		//检验是否有空间放入卸下装备
		int secondPkgIndex = -1;
		if (NULL != pTgtPosEquip)
		{
			CBaseItem * pTgtItem = pPlayer->GetItemDelegate().GetItemByItemUID( pTgtPosEquip->uiID );
			if ( NULL != pTgtItem )
			{
				pPlayer->ClearPkgItem(pkgItemInfo.uiID);
				if (!pPlayer->IsFreePkgIndex(equipReturnIndex,pTgtItem->GetWidth(),pTgtItem->GetHeight()))
				{
					pPlayer->SetPkgItem(pkgItemInfo,leftUpPkgIndex);
					return false;
				}
				if ( NULL != secondHandEquip)
				{
					CBaseItem * pSecondHandEquip = pPlayer->GetItemDelegate().GetItemByItemUID( secondHandEquip->uiID );
					if ( NULL != pSecondHandEquip )
					{
						pPlayer->SetPkgItem(pkgItemInfo,equipReturnIndex);
						secondPkgIndex = pPlayer->GetFreePkgIndex(0,pSecondHandEquip->GetWidth(),pSecondHandEquip->GetHeight());
						if ( -1 == secondPkgIndex )
						{
							pPlayer->ClearPkgItem(pkgItemInfo.uiID);
							pPlayer->SetPkgItem(pkgItemInfo,leftUpPkgIndex);
							return false;
						}
					}
				}
			}
		}
		else if ( NULL != secondHandEquip)
		{
			CBaseItem * pSecondHandEquip = pPlayer->GetItemDelegate().GetItemByItemUID( secondHandEquip->uiID );
			if ( NULL != pSecondHandEquip )
			{
				secondPkgIndex = pPlayer->GetFreePkgIndex(0,pSecondHandEquip->GetWidth(),pSecondHandEquip->GetHeight());
				if ( -1 == secondPkgIndex )
				{
					return false;
				}
			}
		}

		ItemInfo_i  tmpInfo = {0,0,0,0,0,0};

		//卸载原装备（效果去除）
		if ( pTgtPosEquip->uiID != PlayerInfo_2_Equips::INVALIDATE )
		{
			tmpInfo = *pTgtPosEquip;
			unsigned int tmpIndex = doubleHandEquip == NULL ? equipIndex : doubleHandEquipIndex;
			pPlayer->OnUnEquipItem( *pTgtPosEquip, tmpIndex );
		}

		if (NULL != secondHandEquip)
		{
			pPlayer->OnUnEquipItem(*secondHandEquip,secondHandEquipIndex);
		}

		pPlayer->OnEquipItem( pkgItemInfo, equipIndex );

		pPlayer->ClearPkgItem(pkgItemInfo.uiID);
		ItemInfo_i nullInfo;
		StructMemSet(nullInfo,0,sizeof(nullInfo));
		if ( NULL != doubleHandEquip )
		{
			*pTgtPosEquip = nullInfo;
			bool fooBool = false;
			unsigned int equipPos = 0, fooPos2 = 0;
			bool isEquipPosOK = GetDefaultEquipPos( E_SECONDHAND, equipPos, fooBool, fooPos2 );
			if ( isEquipPosOK )
			{
				ItemInfo_i *secondHand = pPlayer->GetEquipByPos(equipPos);
				if ( NULL != secondHand )
				{
					*secondHand = pkgItemInfo;
				}
			}
			pkgItemInfo = tmpInfo;
			pPlayer->SetPkgItem(tmpInfo,equipReturnIndex);
			pPlayer->NoticePkgItemChange( equipReturnIndex );
		}
		else if ( NULL != secondHandEquip )
		{
			*pTgtPosEquip   = pkgItemInfo;
			//pkgItemInfo     = tmpInfo;
			pPlayer->SetPkgItem(tmpInfo,equipReturnIndex);
			pPlayer->NoticePkgItemChange( equipReturnIndex );
			if (-1 != secondPkgIndex)
			{
				pPlayer->SetPkgItem(*secondHandEquip,secondPkgIndex);
				*secondHandEquip = nullInfo;
				pPlayer->NoticePkgItemChange( secondPkgIndex );
			}
			else
			{
				D_ERROR("玩家%d卸下的副手装备%d无法放入包裹\n",pPlayer->GetAccount(),doubleHandEquip->uiID);
			}
		}
		else
		{
			*pTgtPosEquip   = pkgItemInfo;
			//pkgItemInfo     = tmpInfo;
			pPlayer->SetPkgItem(tmpInfo,equipReturnIndex);
			pPlayer->NoticePkgItemChange( equipReturnIndex );
		}

		if(!ITEM_IS_EQUIPED(pTgtPosEquip->ucLevelUp))
		{
			CLogManager::DoItemPropertyChange(pPlayer, LOG_SVC_NS::IPT_BIND, pTgtPosEquip->uiID, pTgtPosEquip->usItemTypeID, 0, 1);//记日至
		}

		EQUIP_ITEM_MASK( pTgtPosEquip->ucLevelUp );//设置这个装备被使用过

		pPlayer->NoticeEquipItemChange( equipIndex );
		if ( NULL != doubleHandEquip )
		{
			pPlayer->NoticeEquipItemChange(doubleHandEquipIndex);
		}
		if ( NULL != secondHandEquip )
		{
			pPlayer->NoticeEquipItemChange(secondHandEquipIndex);
		}
		pPlayer->NoticePkgItemChange( pkgIndex );

		if ( gearArmType != E_DRIVE )
		{
			///使用道具是否影响耐久度？
			pPlayer->UseItemAffectWear(pTgtPosEquip);
		}

		if ( pItem )//使用物品计数器,使用成功,使用物品计数器+1
		{
			pPlayer->TryCounterEvent( CT_USEITEM, pItem->GetItemID() , 1 );
		}

		return true;
	}

	return false;

	TRY_END;
	return false;
}

/************************************************************************/
/* CPlayer 使用的人
   CBaseItem 使用的道具
   long 使用的道具在道具包的编号*/
/************************************************************************/

bool UseConsumeRule::Execute( CPlayer* pPlayer ,CBaseItem* pItem, unsigned int pkgIndex )
{
	if ( !pPlayer )
		return false;

	if ( !pItem)
		return false;

	if ( pkgIndex >= PACKAGE_SIZE )
		return false;

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("UseConsumeRule::Execute, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& itemPkgs  = playerInfo->pkgInfo;//包裹信息
	ItemInfo_i& l_itemInfo = itemPkgs.pkgs[pkgIndex];//获取包裹中的这个道具

	if ( pItem->GetItemUseType() != E_CONSUME )
		return false;
	
	CConsumeItem* pConsumeItem = dynamic_cast< CConsumeItem *>( pItem );
	if ( !pConsumeItem )
		return false;

	const ConsumeProperty* pConsumeProp = pConsumeItem->GetConsumeProperty();
	if ( !pConsumeProp )
		return false;

	bool bCanUse = true;


	if ( pPlayer->IsRideState() )
	{
		if( pConsumeProp->comsumeType == ConsumeProperty::E_RESUME_IMMUNITY 
			/*|| pConsumeProp->comsumeType == ConsumeProperty::E_RESUME_ABSORB 
			||  pConsumeProp->comsumeType == ConsumeProperty::E_RESUME_SINGLE 
			||  pConsumeProp->comsumeType == ConsumeProperty::E_RESUME_MULTI*/ )
		{
			bCanUse = false;
		}
	}


	if ( bCanUse )
	{
		//如果玩家在束缚
		if ( pPlayer->IsFaint() || pPlayer->IsSheep() )
		{
			//只能使用解除和免疫的道具
			if( pConsumeProp->comsumeType != ConsumeProperty::E_RESUME_UNCHAIN  
				&& pConsumeProp->comsumeType != ConsumeProperty::E_RESUME_IMMUNITY )
			{
				bCanUse = false;
			}
		}

		if( pPlayer->IsInSleep() )
		{
			bCanUse = false;
		}
	}
	
	//同时个数也要>0
	if( bCanUse && l_itemInfo.ucCount > 0 )
	{
		if( pPlayer->UseItemSkill(pConsumeProp->comsumeType,pConsumeProp->skillID ) )
		{
			//使用道具成功　
			EquipItemManagerSingle::instance()->OnUseItemResult( pPlayer,l_itemInfo );

			//使用后个数-1
			l_itemInfo.ucCount--;

			if ( l_itemInfo.ucCount == 0 )
			{
				pPlayer->OnDropItem( l_itemInfo.uiID );
			}
			else
			{
				pPlayer->NoticePkgItemChange( pkgIndex );
			}
		}
		else
		{
			EquipItemManagerSingle::instance()->OnUseItemFailed( pPlayer,l_itemInfo.uiID );
			return false;
		}
	}
	else
	{
		EquipItemManagerSingle::instance()->OnUseItemFailed( pPlayer,l_itemInfo.uiID );
		return false;
	}
	return true;
}

/************************************************************************/
/* 使用功能性道具                                                       */
/************************************************************************/

bool UseFunctionRule::Execute(CPlayer* pPlayer ,CBaseItem* pItem, unsigned int pkgIndex )
{
	if ( !pPlayer )
		return false;

	if( pPlayer->IsFaint() )
		return false;

	if ( !pItem )
		return false;

	TRY_BEGIN;

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("UseFunctionRule::Execute, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& itemPkgs  = playerInfo->pkgInfo;//包裹信息
	if (pkgIndex >= ARRAY_SIZE(itemPkgs.pkgs))
	{
		D_ERROR("UseFunctionRule::Execute, pkgIndex(%d) >= ARRAY_SIZE(itemPkgs.pkgs)\n", pkgIndex);
		return false;
	}
	ItemInfo_i& l_itemInfo = itemPkgs.pkgs[pkgIndex];//获取包裹中的这个包裹
	bool isUsed    = false;//是否使用成功 
	bool isNeedDel = false;//是否需要删除

	if ( l_itemInfo.ucCount <= 0  )
	{
		D_ERROR("%s使用功能性道具错误，道具个数为0，仍然被使用。道具编号%d 道具位索引%d\n", pPlayer->GetNickName(), l_itemInfo.usItemTypeID, pkgIndex );
		return false;
	}

	CFunctionItem* pFunctionItem = dynamic_cast<CFunctionItem *>( pItem );
	if ( !pFunctionItem )
		return false;

	const FunctionProperty* pFunctionProp = pFunctionItem->GetFunctionProperty();
	if ( !pFunctionProp )
	{
		D_ERROR("使用道具时，找不到功能道具的特有属性:%d\n", pItem->GetItemID() );
		return false;
	}

	if ( pFunctionProp->sex != 0 )
	{
		if( pFunctionProp->sex != pPlayer->GetSex() )
		{
			pPlayer->SendSystemChat("与使用道具所需性别不一致,不能使用道具");
			return false;
		}
	}

	//if ( pFunctionProp->Strength > pPlayer->GetStrength() 
	//	|| pFunctionProp->Vitality > pPlayer->GetVitality()
	//	|| pFunctionProp->Agility > pPlayer->GetAgility()
	//	|| pFunctionProp->Spirit > pPlayer->GetSpirit()
	//	|| pFunctionProp->Intelligence > pPlayer->GetIntelligence())
	//{
	//	pPlayer->SendSystemChat("未达到使用技能所需属性点，不能使用道具");
	//	return false;
	//}
	//
	//if ( pFunctionProp->PreSkillID != 0 )
	//{
	//	if ( pPlayer->FindMuxSkill( pFunctionProp->PreSkillID ) == NULL )
	//	{
	//		pPlayer->SendSystemChat("未学会道具所需前置技能，不能使用道具");
	//		return false;
	//	}
	//}

	if ( pFunctionProp->TaskScript != 0 )//属于脚本位有数据，则要么为幸运星，要么为脚本
	{
		if ( pFunctionProp->Type == FunctionProperty::E_LUCK_STAR )//如果是幸运星
		{
			//幸运星的道具
			std::vector<CItemPkgEle> itemEleVec;
			long money = 0;
			//依据掉落集生成道具
			MonsterDropItemSetSingle::instance()->RunMonsterDropItemSet( pPlayer,pFunctionProp->TaskScript, itemEleVec, money );
			

			//检测道具是否能放入背包，如果能，就放入，否则提示失败
			unsigned int itemArr[20] = { 0 };
			unsigned int itemIndex = 0;
			std::vector<CItemPkgEle>::iterator lter = itemEleVec.begin();
			for( ; lter != itemEleVec.end() && itemIndex < 20 ; ++lter )
			{
				if ( itemIndex+1 >= ARRAY_SIZE(itemArr) )
				{
					D_ERROR( "UseFunctionRule::Execute, 越界, itemArr size %d, 索引位置%d\n", ARRAY_SIZE(itemArr), itemIndex+1 );
					return false;
				}
				CItemPkgEle& itemEle = *lter;
				itemArr[itemIndex]   = itemEle.GetItemTypeID();
				itemArr[itemIndex+1] = 1;
				itemIndex += 2;
			}

			//检测玩家是否能有背包空间接受这些道具
			if( pPlayer->NpcCanGiveItem( itemArr, itemIndex ) )
			{
				//依次给道具
				for ( unsigned int i = 0; i< itemIndex; i+=2 )
				{
					pPlayer->OnNpcGiveItem( itemArr[i], itemArr[i+1] );
				}
				//给完后将主物品丢掉
				pPlayer->OnDropItem( pItem->GetItemUID() );
				return true;
			}
			else
			{
				//通知client背包已满
				MGChat chat;
				StructMemSet( chat, 0x00, sizeof(chat));
				chat.chatType = CT_SCRIPT_MSG;
				chat.extInfo = 0;
				SafeStrCpy(chat.strChat, "背包空位不够,请清理背包");
				pPlayer->SendPkg<MGChat>(&chat);
				return false;
			}
		}
		else//如果是绑定脚本的道具
		{
			isUsed    = pPlayer->OnChatToItem( pItem, 0 );
			isNeedDel = false;
		}
	}
	else
	{
		FunctionProperty::FunctionType type = pFunctionProp->Type;
		switch( type )
		{
			//回城卷
		case FunctionProperty::E_BACKHOME:
			{

			}
			break;
			//骑乘道具
		case FunctionProperty::E_RIDE:
			{
				if ( pPlayer->GetSelfStateManager().IsInDunGeonPreArea() )
				{
					D_DEBUG("玩家%s 处于非骑乘区域,不能使用骑乘道具\n", pPlayer->GetNickName() );
					return false;
				}

				//获取骑乘状态引用
				CRideState& ridestate = pPlayer->GetSelfStateManager().GetRideState();
				if ( ridestate.IsActive() )//检验是否处于骑乘状态
				{
					D_ERROR("玩家处于骑乘状态,骑乘状态无法使用骑乘道具!!\n");
					return false;
				}
				else
				{
					//设置骑乘类型为装备
					ridestate.SetRideItemState( E_RIDE_CONSUME );
					ridestate.SetRideItemType(  l_itemInfo.usItemTypeID );
					//设置玩家处于骑乘状态
					pPlayer->GetSelfStateManager().SetPlayerRideLeaderState();
					isUsed    = true;
					isNeedDel = true;
				}
			}
			break;
			//宠物蛋
		case FunctionProperty::E_PETEGG:
			isNeedDel = isUsed = pPlayer->PlayerGainPet();
			break;
		case FunctionProperty::E_PETFOOD:
			isNeedDel = isUsed = pPlayer->PlayerFeedPet( (unsigned)pFunctionProp->AddSpeed );
			break;
		case FunctionProperty::E_SPECIAL_PROTECT:
			{
				pPlayer->GetProtectItemPlate().ShowProtectItemPlate( E_PROTECT_SPECIAL );
				pPlayer->GetProtectItemPlate().SetConsumeItem( l_itemInfo.uiID );
				isUsed    = true;
				isNeedDel = false;
			}
			break;
		case FunctionProperty::E_UNSPECIAL_PROTECT:
			{
				pPlayer->GetProtectItemPlate().ShowProtectItemPlate( E_UNPROTECT_SPECIAL );
				pPlayer->GetProtectItemPlate().SetConsumeItem( l_itemInfo.uiID );
				isUsed    = true;
				isNeedDel = false;
			}
			break;
		case FunctionProperty::E_NORMAL_PROTECT:
			{
				pPlayer->GetProtectItemPlate().ShowProtectItemPlate( E_PROTECT_NORMAL );
				pPlayer->GetProtectItemPlate().SetConsumeItem( l_itemInfo.uiID );
				isUsed    = true;
				isNeedDel = false;
			}
			break;
		case FunctionProperty::E_UNNORMAL_PROTECT:
			{
				pPlayer->GetProtectItemPlate().ShowProtectItemPlate( E_UNPROTECT_NORMAL );
				pPlayer->GetProtectItemPlate().SetConsumeItem( l_itemInfo.uiID );
				isUsed    = true;
				isNeedDel = false;
			}
			break;
		case FunctionProperty::E_ADD_UNION_ACTIVE_ONCE://一次性增加工会活跃度
			{
				CUnion *pUnion = pPlayer->Union();
				if(NULL == pUnion)
				{
					D_DEBUG("玩家%s 没有加入工会,不能使用该物品\n", pPlayer->GetNickName() );
					return false;
				}

				if(pUnion->GetActive() >= 4294967295)
				{
					D_DEBUG("玩家%s所属工会当前活跃度达到上限,不能使用该物品\n", pPlayer->GetNickName() );
					return false;
				}

				pPlayer->ChanageUnionActive(0, (int)pFunctionProp->maxWear);
				isNeedDel = isUsed = true;
			}
			break;
		case FunctionProperty::E_SET_UNION_ACTIVE_TIMES://设置工会活跃度倍率
			{
				CUnion *pUnion = pPlayer->Union();
				if(NULL == pUnion)
				{
					D_DEBUG("玩家%s 没有加入工会,不能使用该物品\n", pPlayer->GetNickName() );
					return false;
				}

				pPlayer->SetUnionActOrPresTimes(true, pFunctionProp->repairMode, (unsigned int)(pFunctionProp->maxWear*60 + ACE_OS::time(NULL)));
				isNeedDel = isUsed = true;
			}
			break;
		case FunctionProperty::E_ADD_UNION_PRESTIGE_ONCE://一次性增加工会威望
			{
				CUnion *pUnion = pPlayer->Union();
				if(NULL == pUnion)
				{
					D_DEBUG("玩家%s 没有加入工会,不能使用该物品\n", pPlayer->GetNickName() );
					return false;
				}

				if(pUnion->GetPrestige() >= 4294967295)
				{
					D_DEBUG("玩家%s所属工会当前威望达到上限,不能使用该物品\n", pPlayer->GetNickName() );
					return false;
				}

				pPlayer->ChanageUnionPrestige(0, (int)pFunctionProp->maxWear);
				isNeedDel = isUsed = true;
			}
			break;
		case FunctionProperty::E_SET_UNION_PRESTIGE_TIMES://设置工会威望倍率
			{
				CUnion *pUnion = pPlayer->Union();
				if(NULL == pUnion)
				{
					D_DEBUG("玩家%s 没有加入工会,不能使用该物品\n", pPlayer->GetNickName() );
					return false;
				}

				pPlayer->SetUnionActOrPresTimes(false, pFunctionProp->repairMode, (unsigned int)(pFunctionProp->maxWear*60 + ACE_OS::time(NULL)));
				isNeedDel = isUsed = true;
			}
			break;

		default:
			break;
		}
	}

	if( isUsed )
	{
		//先通知使用情况
		EquipItemManagerSingle::instance()->OnUseItemResult( pPlayer,l_itemInfo );
	}
	else
	{
		EquipItemManagerSingle::instance()->OnUseItemFailed( pPlayer,l_itemInfo.uiID );
	}


	if ( isNeedDel )
	{
		//每次使用，剩余个数减1
		l_itemInfo.ucCount--;
		//使用完了,个数为0,则扔掉道具
		if ( l_itemInfo.ucCount == 0 )
		{
			pPlayer->OnDropItem( l_itemInfo.uiID );
		} else {//否则通知客户端，包裹内的物品发生变化
			pPlayer->NoticePkgItemChange( pkgIndex );
		}
	}
	
	return isUsed;
	TRY_END;
	return false;
}

bool UseBulePrintRule::Execute(CPlayer *pPlayer, CBaseItem *pItem, unsigned int pkgIndex)
{
	if ( !pPlayer )
		return false;

	if ( !pItem )
		return false;

	if ( pkgIndex >= PACKAGE_SIZE )
		return false;

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("UseBulePrintRule::Execute, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& itemPkgs  = playerInfo->pkgInfo;//包裹信息
	const ItemInfo_i& l_itemInfo = itemPkgs.pkgs[pkgIndex];//获取包裹中的这个包裹
	if ( l_itemInfo.uiID !=  pItem->GetItemUID() )
	{
		D_ERROR("使用的道具与client传来的道具不相符合\n");
		return false;
	}

	if ( pPlayer->IsPlayerForgingItem() )
		pPlayer->ClosePlateCmd();

#ifdef _DEBUG
	D_DEBUG("%s玩家使用图纸%d\n", pPlayer->GetNickName(), pItem->GetItemID() );
#endif

	if( pPlayer->ShowPlateCmd( PLATE_ADD ) )
		pPlayer->SetPlateCondition( pItem->GetItemUID() );

	EquipItemManagerSingle::instance()->OnUseItemResult( pPlayer,l_itemInfo );

	return true;
}


bool UseSkillBookRule::Execute(CPlayer* pPlayer, CBaseItem* pItem, unsigned int pkgIndex )
{
	if ( !pPlayer )
		return false;

	if( pPlayer->IsFaint() )
		return false;

	if ( !pItem )
		return false;

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("UseSkillBookRule::Execute, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& itemPkgs  = playerInfo->pkgInfo;//包裹信息
	if (pkgIndex >= ARRAY_SIZE(itemPkgs.pkgs))
	{
		D_ERROR("UseSkillBookRule::Execute, pkgIndex(%d) >= ARRAY_SIZE(itemPkgs.pkgs)\n",pkgIndex );
		return false;
	}
	ItemInfo_i& l_itemInfo = itemPkgs.pkgs[pkgIndex];//获取包裹中的这个包裹
	if ( l_itemInfo.uiID !=  pItem->GetItemUID() )
	{
		D_ERROR("使用的道具与client传来的道具不相符合\n");
		return false;
	}

	CSkillBookItem* pSkillBook = dynamic_cast<CSkillBookItem *>( pItem ) ;
	if ( !pSkillBook )
		return false;

	const SkillBookProperty* pSkillProp = pSkillBook->GetSkillBookProperty();
	if ( pSkillProp )
	{
#ifdef _DEBUG
		D_DEBUG("玩家%s使用技能书，编号:%d\n", pPlayer->GetNickName(), pItem->GetItemTypeID() );
#endif
		bool isSuccess = false;//本次使用道具的结果
		bool isNormalWashSkill = true;//是否普通洗点技能，用于宠物变身技能(非洗点技能)提示信息；

		if( pSkillProp->skillType == SkillBookProperty::E_PET_TRANS_BOOK )//如果宠物等级
		{
			if ( pPlayer->GetPetInfoInstable()->petInfoInstable.petLevel >= pItem->GetItemPlayerLevel() )//与规定的宠物等级相符合
			{
				isSuccess = pPlayer->StudyBodytran( pSkillProp->skillID );
				isNormalWashSkill = false;//是否普通洗点技能，用于宠物变身技能(非洗点技能)提示信息；
			}
			else
			{
				pPlayer->SendSystemChat("宠物等级与使用宠物技能书等级不符合，不能使用该道具");
				return false;
			}
		}
		else//如果不是宠物技能书，走正常的使用流程
		{
			if ( pItem->IsCanUse( pPlayer, pPlayer->GetRace(), pPlayer->GetClass() ,pPlayer->GetLevel() ) )//玩家是否能够使用这个升级道具
			{
				switch( pSkillProp->skillType )
				{
				case SkillBookProperty::E_RESIGN_PHY_SKILL:
					{
						D_ERROR( "调用已废弃功能-清物理技能点，技能书编号:%d\n", pItem->GetItemTypeID() );
						//10.09.20已废弃//isSuccess = pPlayer->CleanPhySkillPoint();
						//if ( isSuccess )
						//	pPlayer->UpdateSkillInfoToDB();
					}
					break;
				case SkillBookProperty::E_RESIGN_MIG_SKILL:
					{
						D_ERROR( "调用已废弃功能-清魔法技能点，技能书编号:%d\n", pItem->GetItemTypeID() );
						//10.09.20已废弃//isSuccess = pPlayer->CleanMagSkillPoint();
						//if ( isSuccess )
						//	pPlayer->UpdateSkillInfoToDB();
					}
					break;
				case SkillBookProperty::E_RESIGN_ALL_SKILL:
					{
						D_ERROR( "调用已废弃功能，清物理魔法技能点, 技能书编号:%d\n", pItem->GetItemTypeID() );
						//bool bRet1 =  pPlayer->CleanPhySkillPoint();
						//bool bRet2 =  pPlayer->CleanMagSkillPoint();
						//isSuccess = bRet1 || bRet2;
						//if ( isSuccess )
						//	pPlayer->UpdateSkillInfoToDB();
					}
					break;
				case SkillBookProperty::E_UPDATE_SKILL:
					{
						if( !pPlayer->UpgradeAssistSkill( pSkillProp->skillID ) )
						{
							pPlayer->SendSystemChat("升级辅助技能失败");
							return false;
						}
						else
						{
							pPlayer->UpdateSkillInfoToDB();
						}
						pPlayer->OnDropItem( pItem->GetItemUID() );
						return true;
					}
					break;
				case SkillBookProperty::E_LEARN_SKILL:
					{
						if ( pSkillProp->Strength > pPlayer->GetStrength() 
							|| pSkillProp->Vitality > pPlayer->GetVitality()
							|| pSkillProp->Agility > pPlayer->GetAgility()
							|| pSkillProp->Spirit > pPlayer->GetSpirit()
							|| pSkillProp->Intelligence > pPlayer->GetIntelligence())
						{
							pPlayer->SendSystemChat("未达到使用技能所需属性点，不能使用道具");
							return false;
						}
						
						if ( pSkillProp->PreSkillID != 0 )
						{
							//判断是否学过前置技能，有前置技能的升级技能也算学过前置技能
							bool isLearnPreSkill = false;
							CMuxSkillProp* pProp = MuxSkillPropSingleton::instance()->FindTSkillProp(pSkillProp->PreSkillID);
							if ( NULL == pProp )
							{
								return false;
							}
							unsigned int skillID = pSkillProp->PreSkillID;
							const int maxLoop = 100;
							int count = 0;
							while (skillID != 0 && count < maxLoop)
							{
								++count;
								if ( pPlayer->FindMuxSkill( skillID ) )
								{
									isLearnPreSkill = true;
									break;
								}
								skillID = pProp->GetNextSkillID();
								pProp = MuxSkillPropSingleton::instance()->FindTSkillProp(skillID);
								if ( NULL == pProp )
								{
									break;
								}
							}
							if ( !isLearnPreSkill )
							{
								pPlayer->SendSystemChat("未学会道具所需前置技能，不能使用道具");
								return false;
							}
						}
						
						//不能重复学习技能以及低等级的技能
						CMuxSkillProp* pProp = MuxSkillPropSingleton::instance()->FindTSkillProp(pSkillProp->skillID);
						if ( NULL == pProp )
						{
							return false;
						}
						unsigned int skillID = pSkillProp->skillID;
						const int maxLoop = 100;
						int count = 0;
						while (skillID != 0 && count < maxLoop)
						{
							++count;
							if ( pPlayer->FindMuxSkill( skillID ) )
							{
								pPlayer->SendSystemChat("已经学会该技能，不需要再学习");
								return false;
							}
							skillID = pProp->GetNextSkillID();
							pProp = MuxSkillPropSingleton::instance()->FindTSkillProp(skillID);
							if ( NULL == pProp )
							{
								break;
							}
						}
						
						if ( !pPlayer->AddSkillByID( pSkillProp->skillID ) )
						{
							pPlayer->SendSystemChat("使用技能书失败");
							return false;
						}
						else
						{
							pPlayer->UpdateSkillInfoToDB();
						}
						pPlayer->OnDropItem( pItem->GetItemUID() );
						return true;
					}
					break;
				default:
					break;
				}
			}
			else
			{
				pPlayer->SendSystemChat("使用的道具不符合玩家的职业，种族和等级");
				return false;
			}
		}
	
		if ( isSuccess )
		{	
			//先通知使用情况
			EquipItemManagerSingle::instance()->OnUseItemResult( pPlayer,l_itemInfo );

			l_itemInfo.ucCount--;
			if(  l_itemInfo.ucCount == 0 )
			{
				pPlayer->OnDropItem( pItem->GetItemUID() );
			}
			else
			{
				pPlayer->NoticePkgItemChange( pkgIndex );
			}

#ifdef _DEBUG
			D_DEBUG("%s使用技能书成功,丢弃技能书\n",pPlayer->GetNickName() );
#endif
		}else{

			EquipItemManagerSingle::instance()->OnUseItemFailed( pPlayer,l_itemInfo.uiID );
			if ( isNormalWashSkill )//如果使用失败，并且不属于宠物技能书，则提示client错误信息
			{
				pPlayer->SendSystemChat("您没有需要洗点的技能");
			}
		}
	}
	return true;
}

bool UseAxiologyRule::Execute(CPlayer *pPlayer, CBaseItem *pItem, unsigned int pkgIndex)
{
	if ( !pPlayer )
		return false;

	if ( !pItem )
		return false;

	if( pPlayer->IsFaint() )
		return false;

	if ( pkgIndex >= PACKAGE_SIZE )
		return false;

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("UseAxiologyRule::Execute, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& itemPkgs  = playerInfo->pkgInfo;//包裹信息
	ItemInfo_i l_itemInfo = itemPkgs.pkgs[pkgIndex];//获取包裹中的这个包裹
	if ( l_itemInfo.uiID !=  pItem->GetItemUID() )
	{
		D_ERROR("使用的道具与client传来的道具不相符合\n");
		return false;
	}

	TRY_BEGIN;

	CombindAxiolog combindAxiologArr[PACKAGE_SIZE];
	unsigned int combindAxiologIndex = 0;

	CAxiologyItem* pAxiologyItem = dynamic_cast<CAxiologyItem *>( pItem );
	if ( !pAxiologyItem )
		return false;

	bool isUsed = false;

	//获取价值物的属性
	const AxiologyProperty* pAxiologProp = pAxiologyItem->GetAxiologyItemProperty();
	if ( pAxiologProp )
	{
		//价值物类型
		unsigned int type = pAxiologProp->type;
		//
		float value = pAxiologProp->value;
		//道具在XML表中的编号
		unsigned int itemTypeID = pItem->GetItemID();

		bool bCombind = false;

		//如果是可合并的物品
		if ( type == AxiologyProperty::E_CONBIND )
		{	
			FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
			if ( NULL == playerInfo )
			{
				D_ERROR("UseAxiologyRule::Execute, NULL == GetFullPlayerInfo()\n");
				return false;
			}
			PlayerInfo_3_Pkgs&   itemPkgs  = playerInfo->pkgInfo;//玩家的包裹信息

			//合成所需要的宝石的个数
			unsigned int needcount = (unsigned int)( value );
			if ( needcount > 0 )
			{
				ItemInfo_i& selfItem = itemPkgs.pkgs[pkgIndex];
				//如果自己的个数已经>所需要的个数
				unsigned int itemCount = (unsigned int)selfItem.ucCount;
				if ( itemCount > needcount )//如果自身就能完成合成宝石的工作
				{
					if ( combindAxiologIndex >= ARRAY_SIZE(combindAxiologArr) )
					{
						D_ERROR("UseAxiologyRule::Execute, combindAxiologIndex(%d) >= ARRAY_SIZE(combindAxiologArr)\n", combindAxiologIndex);
						return false;
					}
					UseAxiologyRule::CombindAxiolog& cAxiolog = combindAxiologArr[combindAxiologIndex];
					combindAxiologIndex++;
					cAxiolog.pkgIndex = pkgIndex;
					cAxiolog.count = ( selfItem.ucCount - needcount );

					needcount = 0;
					bCombind = true;
				}
				else
				{
					if ( combindAxiologIndex >= ARRAY_SIZE(combindAxiologArr) )
					{
						D_ERROR("UseAxiologyRule::Execute, combindAxiologIndex(%d) >= ARRAY_SIZE(combindAxiologArr)\n", combindAxiologIndex);
						return false;
					}
					UseAxiologyRule::CombindAxiolog& cAxiolog = combindAxiologArr[combindAxiologIndex];
					combindAxiologIndex++;
					cAxiolog.pkgIndex = pkgIndex;
					cAxiolog.count    = 0;//设置为0，表示该道具等下要被扔掉
				
					needcount -= itemCount;
					//遍历包裹，找出其他能够参与合成的宝石
					for ( unsigned int index = 0 ; index < PACKAGE_SIZE && needcount > 0 ; ++index )
					{
						//如果是自身，则跳过
						if ( index == pkgIndex )
							continue;

						const ItemInfo_i& item = itemPkgs.pkgs[index];
						if ( item.usItemTypeID == itemTypeID )
						{
							itemCount = (unsigned int)item.ucCount;

							if ( combindAxiologIndex >= ARRAY_SIZE(combindAxiologArr) )
							{
								D_ERROR("UseAxiologyRule::Execute, combindAxiologIndex(%d) >= ARRAY_SIZE(combindAxiologArr)\n", combindAxiologIndex);
								return false;
							}
							UseAxiologyRule::CombindAxiolog& axiolog = combindAxiologArr[combindAxiologIndex];
							combindAxiologIndex++;
							axiolog.pkgIndex = index;
							//根据个数，判断对该宝石的处理
							if ( itemCount > needcount )
							{
								axiolog.count = ( itemCount - needcount );
								needcount = 0;
							}
							else
							{
								needcount -= itemCount;
								axiolog.count = 0;
							}

							if ( combindAxiologIndex ==PACKAGE_SIZE )
								break;
						}
					}

					//合并宝石成功
					if ( needcount == 0 )
						bCombind = true;
				}
			}

			bool isused = false;
			if ( bCombind )
			{
				if (  needcount == 0 && combindAxiologIndex > 0)
				{
					unsigned int newItemID = itemTypeID + 10;					
					//CItemPkgEle tmpItemEle;
					//int errNo = 0;
					//tmpItemEle.SetItemEleProp( newItemID, 1, 0 );
					//if( pPlayer->GiveNewItemToPlayer( tmpItemEle.GetItemEleInfo(), tmpItemEle.GetTaskID(), errNo ) == true )
					if ( pPlayer->GetNormalItem( newItemID, 1, 0 ) ) //GiveNewItemToPlayer与GetNormalItem功能上似乎重复，因此这样改，by dzj, 10.05.20;
					{
						for ( unsigned j = 0; j< combindAxiologIndex && j< ARRAY_SIZE(combindAxiologArr) ; ++j )
						{
							UseAxiologyRule::CombindAxiolog& cAxiolog = combindAxiologArr[j];
							unsigned int pkgIndex = cAxiolog.pkgIndex;
							if ( pkgIndex >= ARRAY_SIZE(itemPkgs.pkgs) )
							{
								D_ERROR("UseAxiologyRule::Execute, pkgIndex(%d) >= ARRAY_SIZE(itemPkgs.pkgs)\n", pkgIndex);
								return false;
							}
							ItemInfo_i&  pkgItem = itemPkgs.pkgs[ pkgIndex ];

							//通知客户端该位置的道具个数改变了
							if ( cAxiolog.count > 0 )
							{
								pkgItem.ucCount = cAxiolog.count;
								pPlayer->NoticePkgItemChange(  pkgIndex );
							}
							else
								pPlayer->OnDropItem(  pkgItem.uiID );
						}
						isused = true;
					} else {
						//见上注释，此时没有errNo，if ( errNo == E_PKG_FULL )
						//{
							pPlayer->SendSystemChat("背包没有空格,无法合成宝石");
							isused = true;
						//}
					}
				}
			}//_if_bcombind
			else
			{
				pPlayer->SendSystemChat("宝石数量不足,无法合成宝石");
				isused = true;
			}

			if ( isused )
				EquipItemManagerSingle::instance()->OnUseItemResult( pPlayer,l_itemInfo );			
			else
				EquipItemManagerSingle::instance()->OnUseItemFailed( pPlayer,l_itemInfo.uiID );

			return isused;

		}//_if_type=COMBIND
	}
	TRY_END;

	return false;
}

bool UseDamageConsumeRule::Execute(CPlayer* pPlayer, void* useConsumeItem )
{
	if ( !pPlayer )
		return false;

	if ( !useConsumeItem )
		return false;

	//主动休息状态不能使用伤害道具
	if ( pPlayer->IsFaint() || pPlayer->IsInRest() )
		return false;

	if ( pPlayer->GetSelfStateManager().IsOnRideState()  )
		return false;

	if( pPlayer->IsAnamorphicState())
		return false;

	GMUseDamageItem* pDamageItem = (GMUseDamageItem *)useConsumeItem;
	//道具的UID号
	unsigned int itemUID = pDamageItem->itemUID;

	//@brief:找出该道具是否在玩家的包裹中,检查道具是否合法
	int index = -1;
	bool isEquip = false;
	ItemInfo_i* sItemInfo = pPlayer->GetItemInfoByItemUID( itemUID, index ,isEquip );
	if ( sItemInfo == NULL || index == -1 )
		return false;
	
	CBaseItem* pItem = pPlayer->GetItemDelegate().GetPkgItem( itemUID );
	if ( !pItem )
	{
		D_ERROR("%s 使用的消耗性攻击道具不合法，服务器找不到该道具 %d\n", pPlayer->GetNickName(), pDamageItem->itemUID );
		return false;
	}

	//该道具必须是消耗品
	if ( pItem->GetItemUseType() != E_CONSUME )
	{
		D_ERROR("%s 使用的道具不属于消耗品 %d\n", pPlayer->GetNickName(),pItem->GetItemUseType() );
		return false;
	}

	bool isUseSuccess = false;
	//判断种族，等级，职业是否能够使用
	if( pItem->IsCanUse( pPlayer,pPlayer->GetRace(), pPlayer->GetClass(), pPlayer->GetLevel()  ) )
	{
		CConsumeItem* pConsumeItem = dynamic_cast<CConsumeItem *>( pItem );

		//获取消耗品属性
		const ConsumeProperty* pProp = pConsumeItem->GetConsumeProperty();
		if ( !pProp )
			return false;

		if ( pPlayer->UseDamageItem( pItem->GetItemID(),pProp->skillID,pDamageItem->attackPosX, pDamageItem->attackPosY, pDamageItem->targetID ) )
		{
			EquipItemManagerSingle::instance()->OnUseItemResult( pPlayer,*sItemInfo );

			sItemInfo->ucCount--;
			if ( sItemInfo->ucCount == 0 )
				pPlayer->OnDropItem( itemUID );
			else
				pPlayer->NoticePkgItemChange( index );

			isUseSuccess = true;
		}
	}

	if ( !isUseSuccess )
		EquipItemManagerSingle::instance()->OnUseItemFailed( pPlayer, itemUID );

	return true;
}

bool UseFashionRule::Execute(  CPlayer* pPlayer, unsigned int itemUID, unsigned int equipIndex )
{
	if( !ExecuteFashion(pPlayer, itemUID, equipIndex))
		EquipItemManagerSingle::instance()->OnUseItemFailed( pPlayer,itemUID );

	return true;
}

bool UseFashionRule::Execute(  CPlayer* pPlayer, unsigned int itemUID )
{
	return ExecuteNonFashion(pPlayer, itemUID);
}

bool UseFashionRule::ExecuteFashion(  CPlayer* pPlayer, unsigned int itemUID, unsigned int equipIndex )
{
	//参数校验
	if((NULL == pPlayer) || (0 == itemUID) || (equipIndex < EQUIP_SIZE ) || (equipIndex >= EQUIP_SIZE + MAX_FASHION_COUNT))
		return false;

	//道具是否存在
	int pkgIndex = -1;
	bool isEquipItem = false;
	ItemInfo_i *pItem = pPlayer->GetItemInfoByItemUID( itemUID,pkgIndex, isEquipItem );
	
	if(-1 == pkgIndex || pItem == NULL )//道具不存在
		return false;

	ItemInfo_i itemInfo = *pItem;
	CBaseItem* pItemBase = pPlayer->GetItemDelegate().GetPkgItem( itemUID );
	if( NULL == pItemBase )
	{
		D_ERROR("玩家%s使用道具%d，但该道具在玩家的包裹管理器中找不到\n", pPlayer->GetNickName(), itemUID );
		return false;
	}

	CItemPublicProp* pPublicProp = pItemBase->GetItemPublicProp();
	if ( NULL == pPublicProp )
		return false;

	//当前状态能否进行装备(死亡，击晕，使用技能时不能)
	if(pPlayer->IsDying() || pPlayer->IsFaint() || pPlayer->IsInActionTime())
		return false;

	//道具是否失效
	if ( ACE_BIT_ENABLED( pPublicProp->m_wearConsumeMode,CItemPublicProp::LAST_DAY ) )
	{
		time_t now = time(NULL);
		if( (unsigned int)now > pItem->usWearPoint )
		{
			pPlayer->SendSystemChat("使用的道具已经过期，不能使用\n");
			return false;
		}
	}

	//玩家是否可以使用
	if( !pItemBase->IsCanUse( pPlayer, pPlayer->GetRace(), pPlayer->GetClass() ,pPlayer->GetLevel() ) )
	{
		return false;
	}

	unsigned int realEquipIndex = equipIndex - EQUIP_SIZE;//统一编号0-15装备，16-18时装
	ITEM_GEARARM_TYPE gearArmType = pItemBase->GetGearArmType();//物品装备部位
	unsigned int itemSelfEquiIndex = 0;
	if(gearArmType  == 1)
	{
		itemSelfEquiIndex = 0;
	}
	else if(gearArmType == 2)
	{
		itemSelfEquiIndex = 1;
	}
	else if(gearArmType == 3)
	{
		itemSelfEquiIndex = 2;
	}

	if(itemSelfEquiIndex != realEquipIndex)
		return false;

	FashionProperty* pFashionProp = CFashionPropertyManagerSingle::instance()->GetSpecialPropertyT( pItem->usItemTypeID );
	if(NULL ==  pFashionProp)
		return false;

	//如果是工会时装，判断是否有工会
	if((pFashionProp->unionFlag) && (pPlayer->Union() == NULL))
	{
		pPlayer->SendSystemChat("玩家没有工会，不能使用\n");
		return false;
	}

	if((0 != pFashionProp->sex) && (pPlayer->GetSex() != pFashionProp->sex))
	{	
		pPlayer->SendSystemChat("性别不符合要求，无法使用物品\n");
		return false;
	}

	PlayerInfo_Fashions& fashions = pPlayer->Fashions();//时装服饰
	if (realEquipIndex >= ARRAY_SIZE(fashions.fashionData))
	{
		D_ERROR("UseFashionRule::ExecuteFashion, realEquipIndex(%d) >= ARRAY_SIZE(fashions.fashionData)\n", realEquipIndex );
		return false;
	}
	ItemInfo_i& equipItemInfo = fashions.fashionData[realEquipIndex];
	ItemInfo_i  tmpInfo = {0,0,0,0,0,0};
	if(0 != fashions.fashionData[realEquipIndex].uiID)
	{
		//已装备则拿下旧的
		CBaseItem* pUnEquipItemBase = pPlayer->GetItemDelegate().GetFashionItem( fashions.fashionData[realEquipIndex].uiID );
		if ( NULL == pUnEquipItemBase )
		{
			D_ERROR("玩家%s卸载的装备%d 在装备管理器中找不到\n", pPlayer->GetNickName(),fashions.fashionData[realEquipIndex].usItemTypeID );
			return false;
		}
		pPlayer->ClearPkgItem(itemInfo.uiID);
		if (!pPlayer->IsFreePkgIndex(pkgIndex,pUnEquipItemBase->GetWidth(),pUnEquipItemBase->GetHeight()))
		{
			pPlayer->SetPkgItem(itemInfo,pkgIndex);
			return false;
		}
		
		tmpInfo = equipItemInfo;
		pPlayer->OffFashionItem(fashions.fashionData[realEquipIndex], pUnEquipItemBase, equipIndex);
	}

	pPlayer->OnFashionItem(*pItem, pItemBase, equipIndex);
	pPlayer->ClearPkgItem(itemUID);

	equipItemInfo = itemInfo;
	pPlayer->SetPkgItem(tmpInfo,pkgIndex);

	pPlayer->NoticeEquipItemChange( equipIndex );
	pPlayer->NoticePkgItemChange( pkgIndex );
	
	return true;
}

bool UseFashionRule::ExecuteNonFashion( CPlayer* pPlayer, unsigned int itemUID )
{
	//参数校验
	if((NULL == pPlayer) || (0 == itemUID))
		return false;

	//道具是否存在
	int  pkgIndex = -1;
	bool isEquip  = false;
	ItemInfo_i *pItem = pPlayer->GetItemInfoByItemUID( itemUID, pkgIndex,isEquip );
	
	if( (-1 == pkgIndex) || (pItem == NULL) )//道具不存在
		return false;

	if(0 == pItem->ucCount)
		return false;

	CBaseItem* pItemBase = pPlayer->GetItemDelegate().GetPkgItem( itemUID );
	if( NULL == pItemBase )
	{
		D_ERROR("玩家%s使用道具%d，但该道具在玩家的包裹管理器中找不到\n", pPlayer->GetNickName(), itemUID );
		return false;
	}

	CItemPublicProp* pPublicProp = pItemBase->GetItemPublicProp();
	if ( NULL == pPublicProp )
		return false;

	//道具是否失效
	if ( ACE_BIT_ENABLED( pPublicProp->m_wearConsumeMode,CItemPublicProp::LAST_DAY ) )
	{
		time_t now = time(NULL);
		if( (unsigned int)now > pItem->usWearPoint )
		{
			pPlayer->SendSystemChat("使用的道具已经过期，不能使用\n");
			return false;
		}
	}

	//玩家是否可以使用
	if( !pItemBase->IsCanUse( pPlayer, pPlayer->GetRace(), pPlayer->GetClass() ,pPlayer->GetLevel() ) )
	{
		return false;
	}

	ITEM_GEARARM_TYPE  gearArmType = pItemBase->GetGearArmType();//物品装备部位
	if((gearArmType < 4) || (gearArmType >8))
	{
		return false;
	}

	FashionProperty* pFashionProp = CFashionPropertyManagerSingle::instance()->GetSpecialPropertyT( pItem->usItemTypeID );
	if(NULL ==  pFashionProp)
		return false;

	if((0 != pFashionProp->sex) && (pPlayer->GetSex() != pFashionProp->sex))
	{	
		pPlayer->SendSystemChat("性别不符合要求，无法使用物品");
		return false;
	}

	if((gearArmType == 4) || (gearArmType == 5 ))//化妆类
	{
		//变形/死亡状态不可使用
		if(pPlayer->IsAnamorphicState() || pPlayer->IsDying() )
		{
			pPlayer->SendSystemChat("使用失败：处于变形或死亡状态");
			return false;
		}

		BYTE classIndex = pPlayer->GetClass() - 1; 
		if(classIndex >= (sizeof(pFashionProp->maleMode) / sizeof(pFashionProp->maleMode[0])))
			classIndex = 0;

		unsigned int val = (pPlayer->GetSex() == SEX_MALE) ? pFashionProp->maleMode[classIndex] : pFashionProp->femaleMode[classIndex];
		FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("UseFashionRule::ExecuteNonFashion, NULL == GetFullPlayerInfo()\n");
			return false;
		}
		PlayerInfo_1_Base &baseInfo = playerInfo->baseInfo;
		if(gearArmType == 4)
		{
			baseInfo.usHair = val;
		}
		else
		{
			baseInfo.usFace = val;
		}

	}
	else//变形类
	{
		//变形/使用技能/死亡/骑乘状态不可用
		if(pPlayer->IsAnamorphicState() || pPlayer->IsInActionTime() || pPlayer->IsDying() || pPlayer->GetSelfStateManager().IsOnRideState())
		{
			pPlayer->SendSystemChat("使用失败：处于变形或死亡或晕眩或骑乘状态\n");
			return false;
		}

		pPlayer->SetAnamorphic(pItem->usItemTypeID);
	}

	EquipItemManagerSingle::instance()->OnUseItemResult( pPlayer, *pItem );

	pItem->ucCount--;
	if(1 == pFashionProp->expend)
	{
		if ( pItem->ucCount == 0 )
		{
			pPlayer->OnDropItem( pItem->uiID );
		}
		else
		{
			pPlayer->NoticePkgItemChange( pkgIndex );
		}
	}

	return true;
}

void UseFashionRule::UnuseItem( CPlayer* pPlayer, unsigned long itemUID, unsigned int From, unsigned int To )
{
	ACE_UNUSED_ARG( itemUID );

	//参数校验
	if ( (NULL == pPlayer) || (From >= MAX_FASHION_COUNT) || (To >= PACKAGE_SIZE) )
		return;

	//判断玩家所处状态
	if ( pPlayer->IsDying() || pPlayer->IsFaint())
	{
		pPlayer->SendSystemChat("使用失败：处于晕眩或死亡状态");
		return;
	}

	//要拿下的物品的信息
	PlayerInfo_Fashions& fashions = pPlayer->Fashions();
	ItemInfo_i& fashionItem = fashions.fashionData[From];
	CBaseItem* pFashionBase = pPlayer->GetItemDelegate().GetFashionItem( fashionItem.uiID );
	if ( NULL == pFashionBase )
	{
		D_ERROR("UseFashionRule::UnUseItem 卸载道具，装备栏为空,%s找不到对应的道具%d\n", pPlayer->GetNickName(), fashionItem.uiID );
		return;
	}

	ITEM_GEARARM_TYPE equipGear = pFashionBase->GetGearArmType();
	if ( equipGear == E_POSITION_MAX )
	{
		D_ERROR("CEquipItemManagerEx::UnUseItem 卸载道具,装备的位置为空\n");
		return;
	}

	//目标位置的物品信息
	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("UseFashionRule::UnuseItem, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_3_Pkgs& Pkgs  = playerInfo->pkgInfo;
	ItemInfo_i pkgItem= Pkgs.pkgs[To];
	unsigned int leftUpTo = 0;
	pPlayer->GetItemInfoByPkgIndex(To,pkgItem,leftUpTo);
	CBaseItem* pPkgBase  = NULL;
	if ( pkgItem.uiID != PlayerInfo_3_Pkgs::INVALIDATE )//目标位置有物品
	{
		pPkgBase = pPlayer->GetItemDelegate().GetPkgItem( pkgItem.uiID );
		if ( NULL == pPkgBase)
		{
			D_ERROR("卸载道具，%s的包裹管理器找不到对应的道具%d!\n",pPlayer->GetNickName(), pkgItem.uiID );
			return;
		}

		ITEM_GEARARM_TYPE pkgGear = pPkgBase->GetGearArmType();
		if ( pkgGear == E_POSITION_MAX )
		{
			D_ERROR("卸载道具,装备的位置为空\n");
			return;
		}

		//@比较装备位置是否相等
		if (  pkgGear ==  equipGear )//目标位置物品可以装备到相同部位
		{
			if( !pPkgBase->IsCanUse( pPlayer,pPlayer->GetRace(), pPlayer->GetClass(), pPlayer->GetLevel() ) )//然后判断该道具能否被使用
				return;
		}
		else
		{
			pPlayer->SendSystemChat("包裹中的道具不能装备到该位置");
			return;
		}
	}

	//验证是否有足够空间交换
	if ( NULL != pPkgBase )
	{
		pPlayer->ClearPkgItem(pkgItem.uiID);
		if (!pPlayer->IsFreePkgIndex(To,pFashionBase->GetWidth(),pFashionBase->GetHeight()))
		{
			pPlayer->SetPkgItem(pkgItem,leftUpTo);
			pPlayer->SendSystemChat("包裹没有足够空间放入道具");
			return;
		}
	}
	else
	{
		if (!pPlayer->IsFreePkgIndex(To,pFashionBase->GetWidth(),pFashionBase->GetHeight()))
		{
			pPlayer->SendSystemChat("包裹没有足够空间放入道具");
			return;
		}
	}

	//卸载装备在身上的道具属性
	pPlayer->OffFashionItem( fashionItem, pFashionBase, From + EQUIP_SIZE );

	//如果交换的道具存在，则加上交换上去的道具属性
	if( NULL != pPkgBase )
	{
		pPlayer->OnFashionItem( pkgItem, pPkgBase, From + EQUIP_SIZE);
	}

	//交换道具
	ItemInfo_i tmpInfo = fashionItem;
	fashionItem = pkgItem;
	//pkgItem = tmpInfo;
	pPlayer->SetPkgItem(tmpInfo,To);

	pPlayer->NoticeEquipItemChange( From + EQUIP_SIZE );
	pPlayer->NoticePkgItemChange( To );
}
