﻿/** @file CItemIDGenerator.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-5-4
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

//#ifndef ITEMID_GENERATOR_H
//#define ITEMID_GENERATOR_H


#pragma  once


#define  IDRANGE_GUARD 500

#include "../../../Base/aceall.h"


class CItemIDGenerator
{
	friend class ACE_Singleton<CItemIDGenerator, ACE_Null_Mutex>;

public:
	CItemIDGenerator(){ _now = 0ul ; _end = 0ul; _begin = 0ul; isRequest = false; }

public:

	//@brief:返回ItemID的编号
	unsigned long NewItemID();

	//@brief:向DB服务器获取服务器最新的itemID编号范围
	void RequestNewIDRange();

	//@brief:当接受来最新的ID范围时
	void OnGetNewIDRange( unsigned long newbegin, unsigned long newend  );

	//@brief:检测是否现在需要发送ID请求信息
	bool CheckNeedRequestNewID();

	bool IsRequest() {  return isRequest; }

	//@brief:初始化时
	void Init();

private:
	void IncID() { _now++ ; }

private:
	unsigned long _end;
	unsigned long _begin;
	unsigned long _now;

	bool isRequest;
};

typedef ACE_Singleton<CItemIDGenerator, ACE_Null_Mutex> ItemIDGeneratorSingle;
