﻿#include "SuitDelegate.h"
#include "../Player/Player.h"
#include <assert.h>
#include "SuitAttributeManager.h"
#include "../MuxMap/muxmapbase.h"

//使用装备
void SuitDelegate::EquipSuitPart( unsigned int suitID, unsigned int suitPart,bool wearPointIsNotEmpty )
{
	int partIndex = IsSuitExist( suitID );
	//如果之前套装已经存在
	if ( partIndex >= 0 )
	{
		SuitInfo& sInfo = mSuitInfoVec[partIndex];
		sInfo.suitEquipID |= 1<< suitPart;

		if ( wearPointIsNotEmpty )
			sInfo.existSuitEquipID |= 1<< suitPart;
		

		//套装装备完全装备,并且耐久度不为0
		if ( sInfo.suitEquipID == sInfo.pSuit->GetSuitEquipID()  && sInfo.existSuitEquipID == sInfo.suitEquipID )
		{
			//将该套装的属性增加给玩家
			sInfo.pSuit->AddSuitAttributeToPlayer( pAttachPlayer );
		}
	}
	else//如果之前套装不存在，则插入新的套装
	{
		CSuit* pSuit = CSuitManagerSingle::instance()->GetSuit( suitID );
		if ( pSuit )
		{
			//将该套装编号插入到套装代理中
			SuitInfo tmpSuitInfo;
			tmpSuitInfo.pSuit = pSuit;
			tmpSuitInfo.suitEquipID |= 1<<suitPart;
	
			//如果耐久度不为0
			if ( wearPointIsNotEmpty )
				tmpSuitInfo.existSuitEquipID |= 1<<suitPart;

			mSuitInfoVec.push_back( tmpSuitInfo );
		}
	}
}

//卸载装备
void SuitDelegate::UnEquipSuitPart( unsigned int suitID, unsigned int suitPart )
{
	int partIndex = IsSuitExist( suitID );
	if ( partIndex >= 0 )
	{
		SuitInfo& sInfo = mSuitInfoVec[partIndex];

		bool isEquipTotal = ( sInfo.suitEquipID == sInfo.pSuit->GetSuitEquipID() );//在卸载之前，判断是否装备了整个套装
	
		if ( isEquipTotal && sInfo.existSuitEquipID == sInfo.suitEquipID )
		{
			if ( sInfo.pSuit )//因为套装不完整，所有将该装备的属性从玩家身上减去
				sInfo.pSuit->SubSuitAttributeToPlayer( pAttachPlayer );
		}

		sInfo.suitEquipID &= ~( 1<<suitPart );//将该位取反

		//如果该位起作用了， 则将该位取反
		if ( sInfo.existSuitEquipID & (1<<suitPart) )
			sInfo.existSuitEquipID &= ~(1<<suitPart);

		//如果装备了整个套装
	
	} else {
		//assert( false );
		D_ERROR( "SuitDelegate::UnEquipSuitPart, assert( false )\n" );
	}
}

//该套装是否存在
int SuitDelegate::IsSuitExist(unsigned int suitID )
{
	std::vector<SuitInfo>::const_iterator lter = mSuitInfoVec.begin();
	unsigned int index = 0;
	for ( ;lter != mSuitInfoVec.end(); ++lter, ++index )
	{
		const SuitInfo& sInfo = *lter;
		if( sInfo.pSuit && ( sInfo.pSuit->GetSuitID() == suitID ) )
			return index;
	}
	return -1;
}

//套装是否装备完全
bool SuitDelegate::IsSuitEquipComplate(unsigned int suitID)
{
	int partIndex = IsSuitExist( suitID );
	if ( partIndex >=0 )
	{
		SuitInfo& sInfo = mSuitInfoVec[partIndex];
		return ( sInfo.suitEquipID == sInfo.pSuit->GetSuitEquipID() );
	}
	return false;
}

//当耐久度降为0是，减去玩家的套装属性
void SuitDelegate::OnWearPointEmptySubSuitProp( unsigned int suitID,unsigned int equipPos )
{
	if ( !pAttachPlayer )
		return;

	if ( equipPos > EQUIP_SIZE )
		return;

	int partIndex = IsSuitExist( suitID );
	if ( partIndex >=0 )
	{
		SuitInfo& sInfo = mSuitInfoVec[partIndex];
		if ( sInfo.suitEquipID == sInfo.pSuit->GetSuitEquipID() && sInfo.existSuitEquipID == sInfo.suitEquipID )
		{
			sInfo.pSuit->SubSuitAttributeToPlayer( pAttachPlayer );
		}

		//如果该位起作用了， 则将该位取反
		if ( sInfo.existSuitEquipID & (1<<equipPos) )
			sInfo.existSuitEquipID &= ~(1<<equipPos );
	}
}

void SuitDelegate::AfterRepairWearEmptySuitItem( unsigned int suitID, unsigned int equipPos )
{
	if ( !pAttachPlayer )
		return;

	if ( equipPos > EQUIP_SIZE )
		return;

	int partIndex = IsSuitExist( suitID );
	if ( partIndex >=0 )
	{
		SuitInfo& sInfo = mSuitInfoVec[partIndex];
		sInfo.existSuitEquipID |= 1<< equipPos;

		//套装装备完全装备
		if ( sInfo.suitEquipID == sInfo.pSuit->GetSuitEquipID()  && sInfo.existSuitEquipID == sInfo.suitEquipID )
		{
			//将该套装的属性增加给玩家
			sInfo.pSuit->AddSuitAttributeToPlayer( pAttachPlayer );
		}
	}
}
