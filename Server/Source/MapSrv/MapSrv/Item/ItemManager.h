﻿/** @file ItemManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-5-4
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/


#pragma once

#include <map>
#include "../../../Base/aceall.h"
#include "CSpecialItemProp.h"

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class ItemManagerEx
{
public:
	friend class ACE_Singleton<ItemManagerEx, ACE_Null_Mutex>;

public:
	//登陆时，记录每个玩家的详细道具信息
	void RecordPlayerItemDetialInfo( CPlayer* pPlayer );

	//@记录每个道具的详细信息
	CBaseItem* RecordItemDetialInfo( CPlayer* pPlayer, ItemInfo_i& itemInfo ,unsigned int index, bool isEquip = false );

	//玩家推出时，移除玩家在服务器的信息
	void RemovePlayerItem( CPlayer* pPlayer );

	//记录时装信息
	void RecordFashionItemDetailInfo(  CPlayer* pPlayer );

	//基本点改变时，重新检查身上装备
	void CheckPlayerEquipItem( CPlayer* pPlayer );

};

typedef ACE_Singleton<ItemManagerEx, ACE_Null_Mutex> ItemManagerSingle;
