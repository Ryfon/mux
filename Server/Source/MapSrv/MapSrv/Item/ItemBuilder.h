﻿/** @file CItemBuilder.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-5-2
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

//#ifndef ITEM_PACKAGE_H
//#define ITEM_PACKAGE_H

#pragma once

//#include "CSpecialItemProp.h"
#include "CItemPropManager.h"
//#include "ItemManager.h"
#include "CItemIDGenerator.h"

#include "ace/Singleton.h"
#include "ace/Null_Mutex.h"

class CBaseItem;
class CPlayer;

class CItemBuilderEx
{
	friend class ACE_Singleton<CItemBuilderEx, ACE_Null_Mutex>;
public:

	CBaseItem* CreateItem( unsigned long itemID );

	CBaseItem* CreateRawItem( unsigned long  itemID );

	bool GMCreateItem( CPlayer* pPlayer, unsigned long itemTypeID, int count );

};


typedef  ACE_Singleton<CItemBuilderEx, ACE_Null_Mutex> CItemBuilderSingle;

//#endif
