﻿#include "ItemChangeCri.h"
#include "XmlManager.h"
#include "../../../Base/Utility.h"

std::vector<ChangeCri> ItemChangeCirManager::ChangeCriVec;

void ItemChangeCirManager::LoadXML(const char *pszXMLFile)
{
	if ( !pszXMLFile )
		return;

	D_DEBUG("LoadXML:%s\n", pszXMLFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszXMLFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszXMLFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszXMLFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	for ( std::vector<CElement *>::iterator lter = childElements.begin();
		lter!= childElements.end();
		++lter )
	{
		PushChangeCri( *lter );
	}
	TRY_END;
}

void ItemChangeCirManager::PushChangeCri(CElement *pxmlElement)
{
	if ( !pxmlElement )
		return;


	ChangeCri changeCri;
	changeCri.level = ACE_OS::atoi( pxmlElement->GetAttr("level") );
	changeCri.odd   = ACE_OS::atoi( pxmlElement->GetAttr("odd") );
	changeCri.Sumodd = ACE_OS::atoi( pxmlElement->GetAttr("Sumodd") );

	ChangeCriVec.push_back( changeCri );
}

ChangeCri* ItemChangeCirManager::GetChangeCri(unsigned int level)
{
	std::vector<ChangeCri>::iterator lter = ChangeCriVec.begin();
	for ( ; lter!= ChangeCriVec.end(); ++lter )
	{
		ChangeCri& changeCri = *lter;
		if ( changeCri.level == level )
		{
			return (ChangeCri *)&(*lter);
		}
	}

	return NULL;
}


std::vector<ItemCri> ItemCriManager::itemCriVec;

void ItemCriManager::LoadXML(const char* pszXMLFile )
{
	if ( !pszXMLFile )
		return;

	D_DEBUG("LoadXML:%s\n", pszXMLFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszXMLFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszXMLFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszXMLFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	for ( std::vector<CElement *>::iterator lter = childElements.begin();
		lter!= childElements.end();
		++lter )
	{
		PushItemCri( *lter );
	}

	TRY_END;
}

void ItemCriManager::PushItemCri(CElement *pxmlElement)
{
	if( !pxmlElement )
		return;

	ItemCri itemCri;
	itemCri.level = ACE_OS::atoi( pxmlElement->GetAttr("level") );
	itemCri.odd   = ACE_OS::atoi( pxmlElement->GetAttr("odd") );

	itemCriVec.push_back( itemCri );
}

ItemCri* ItemCriManager::GetItemCri(unsigned int itemLevel)
{
	 for ( std::vector<ItemCri>::iterator lter = itemCriVec.begin();
		 lter!= itemCriVec.end();
		 ++lter)
	 {
		 ItemCri& itemCri = *lter;
		 if ( itemLevel == itemCri.level )
		 {
			 return &(*lter);
		 }
	 }

	 return NULL;
}
