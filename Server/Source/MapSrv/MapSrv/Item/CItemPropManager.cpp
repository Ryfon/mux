﻿#include "CItemPropManager.h"
#include "../Lua/NpcChatScript.h"
#include "../../../Base/XmlManager.h"
#include "../plate/plate.h"
#include "../Player/Player.h"

IItemPropertyLoadTemplate::~IItemPropertyLoadTemplate()
{
}

bool IItemPropertyLoadTemplate::InsertItemSpecialProperty( CElement* pxmlElement )
{ 
	ACE_UNUSED_ARG( pxmlElement ); 
	return false; 
}

bool IItemPropertyLoadTemplate::LoadXML(const char* szFilePath )
{
	if ( !szFilePath )
		return false;

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(szFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szFilePath );
		return false;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szFilePath);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	for ( std::vector<CElement*>::iterator lter = childElements.begin(); lter != childElements.end(); ++ lter )
	{
		//@如果插入公有属性成功
		if( InsertItemPublicProperty( *lter ) )
		{
			InsertItemSpecialProperty( *lter );
		}
	}

	return true;
	TRY_END;
	return false;
}

void IItemPropertyLoadTemplate::GetItemCanUseClass(CElement *pxmlElement, CItemPublicProp* pItemPublicProp )
{
	if ( !pxmlElement )
		return;

	if ( !pItemPublicProp )
		return;


	const char * pclassStr = pxmlElement->GetAttr("Class");
	if ( !pclassStr )
		return;

	pItemPublicProp->m_itemClassStr = pclassStr;
	const char* p = ACE_OS::strtok( const_cast<char *>( pclassStr ), ",");
	while ( p )
	{
		ACE_SET_BITS( pItemPublicProp->mCanUseClass, 1<<ACE_OS::atoi(p) );
		p = ACE_OS::strtok( NULL, ",");
	}
}

void IItemPropertyLoadTemplate::GetItemCanUseRace(CElement* pxmlElement, unsigned int& canUseRace )
{
	if ( !pxmlElement )
		return;

	canUseRace = ~0;

	//const char* pRaceStr = pxmlElement->GetAttr("Race");
	//if ( !pRaceStr )
	//	return;

	//const char* p = ACE_OS::strtok( const_cast<char *>( pRaceStr ), ",");
	//while ( p )
	//{
	//	ACE_SET_BITS( canUseRace, 1<<ACE_OS::atoi(p) );
	//	p = ACE_OS::strtok(NULL, ",");
	//}
}

void IItemPropertyLoadTemplate::GetItemWearConsume(CElement* pxmlElement, unsigned int& comsumeWear )
{
	if ( !pxmlElement )
		return;

	comsumeWear = 0;

	const char* pWearConsume = pxmlElement->GetAttr("ConsumeWear");
	if ( !pWearConsume )
		return;

	const char* p = ACE_OS::strtok( const_cast<char *>( pWearConsume ) , ",");
	while( p )
	{
		ACE_SET_BITS(comsumeWear, 1<<ACE_OS::atoi(p));
		p = ACE_OS::strtok(NULL,",");
	}
}

void IItemPropertyLoadTemplate::GetItemWearZero( CElement* pxmlElement, unsigned int& WearZero )
{
	if ( !pxmlElement )
		return;

	WearZero = 0;

	const char* pWearZero = pxmlElement->GetAttr("WearOMode");
	if ( !pWearZero )
		return;

	const char* p = ACE_OS::strtok( const_cast<char *>( pWearZero ) , ",");
	while( p )
	{
		ACE_SET_BITS(WearZero, 1<<ACE_OS::atoi(p));
		p = ACE_OS::strtok(NULL,",");
	}
}

bool IItemPropertyLoadTemplate::InsertItemPublicProperty(CElement* pxmlElement )
{
	if ( !pxmlElement )
		return false;

	CItemPublicProp* pPublicPorp = NEW CItemPublicProp;
	if ( !pPublicPorp )
		return false;

	//读取Item名称
	const char* pTmpName = pxmlElement->GetAttr("Name");
	if ( pTmpName ){
		pPublicPorp->m_itemName = pTmpName;
	}	
	//ItemID
	pPublicPorp->m_itemID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );
	//Price
	pPublicPorp->m_itemPrice = ACE_OS::atoi( pxmlElement->GetAttr("Price") );
	//MetalType
	pPublicPorp->m_itemMetal = (ITEM_METAL)ACE_OS::atoi( pxmlElement->GetAttr("MetalType") );
	//ItemType
	pPublicPorp->m_itemType = (ITEM_TYPE)ACE_OS::atoi( pxmlElement->GetAttr("ItemType") );
	//Level
	pPublicPorp->m_itemLevel = ACE_OS::atoi( pxmlElement->GetAttr("Level") );
	//MaxAmount
	pPublicPorp->m_itemMaxAmount = ACE_OS::atoi( pxmlElement->GetAttr("MaxAmount") );
	//MassLevel
	pPublicPorp->m_itemMassLevel = /*(ITEM_MASSLEVEL)ACE_OS::atoi( pxmlElement->GetAttr("MassLevel") )*/E_GRAY;
	//CannotDepot
	pPublicPorp->m_cannotDepot = ACE_OS::atoi( pxmlElement->GetAttr("CannotDepot") );

	//randSet取消，直接使用luckID以及excellentID指明对应随机集，if ( NULL == pxmlElement->GetAttr("RandSet") )
	//{
	//	pPublicPorp->m_randSet = 0;
	//	D_ERROR( "item:%d没有配置随机集号\n", pPublicPorp->m_itemID );
	//} else {
	//	pPublicPorp->m_randSet = atoi( pxmlElement->GetAttr("RandSet") );
	//}

	//maxWearPoint
	const char* pWear = pxmlElement->GetAttr("MaxWear");
	if ( pWear )
		pPublicPorp->m_maxWear = ACE_OS::atoi( pWear );
	else
		pPublicPorp->m_maxWear = 0;
	//RepairMode
	const char* pRepair = pxmlElement->GetAttr("RepairMode");
	if ( pRepair )
		pPublicPorp->m_repairMode = ACE_OS::atoi( pRepair );
	else
		pPublicPorp->m_repairMode = 0;

	const char* pBindEquip = pxmlElement->GetAttr("BindEquip");
	if ( pBindEquip )
		pPublicPorp->mBindEquip = (ITEM_BIND_TYPE)ACE_OS::atoi( pBindEquip );

	//Class
	GetItemCanUseClass( pxmlElement, pPublicPorp );
	//Race
	GetItemCanUseRace( pxmlElement, pPublicPorp->mCanUseRace );
	//wearCosumeType
	GetItemWearConsume( pxmlElement,pPublicPorp->m_wearConsumeMode );
	//WearOMode
	GetItemWearZero( pxmlElement, pPublicPorp->m_wearOMode);

	if ( NULL != pxmlElement->GetAttr( "TaskScript" ) )
	{
		unsigned long scriptID = ACE_OS::atoi( pxmlElement->GetAttr( "TaskScript" ) );
		if ( scriptID > 0 )
		{
			pPublicPorp->m_pItemScript = CNormalScriptManager::FindScripts( scriptID );//赋此Item对应的脚本；
		} else {
			pPublicPorp->m_pItemScript = NULL;
		}
	} else {
		//D_WARNING( "物体%s属性中没有指定脚本项属性\n", pPublicPorp->m_ItemName );
	}

	/*使用物品所需要的条件*/
	if ( NULL != pxmlElement->GetAttr("nEquipStr"))
	{
		pPublicPorp->rq_strength = (unsigned short)ACE_OS::atoi( pxmlElement->GetAttr("nEquipStr") );
	}

	if ( NULL != pxmlElement->GetAttr("nEquipVit") )
	{
		pPublicPorp->rq_physique = (unsigned short)ACE_OS::atoi( pxmlElement->GetAttr("nEquipVit") );
	}

	if ( NULL != pxmlElement->GetAttr("nEquipAgi") )
	{
		pPublicPorp->rq_agility = (unsigned short)ACE_OS::atoi( pxmlElement->GetAttr("nEquipAgi") );
	}

	if ( NULL != pxmlElement->GetAttr("nEquipInt") )
	{
		pPublicPorp->rq_intelligence = (unsigned short)ACE_OS::atoi( pxmlElement->GetAttr("nEquipInt") );
	}

	if ( NULL != pxmlElement->GetAttr("nEquipSpi") )
	{
		pPublicPorp->rq_spirit = (unsigned short)ACE_OS::atoi( pxmlElement->GetAttr("nEquipSpi") );
	}

	if ( NULL != pxmlElement->GetAttr("ItemWidth") )
	{
		pPublicPorp->m_width = ACE_OS::atoi( pxmlElement->GetAttr("ItemWidth") );
	}
	else
	{
		pPublicPorp->m_width = 1;
	}

	if ( NULL != pxmlElement->GetAttr("ItemHeight") )
	{
		pPublicPorp->m_height = ACE_OS::atoi( pxmlElement->GetAttr("ItemHeight") );
	}
	else
	{
		pPublicPorp->m_height = 1;
	}

	if ( CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(pPublicPorp->m_itemID ) !=NULL )
	{
		delete pPublicPorp;
	}
	else
		CItemPublicPropManagerExSingle::instance()->InsertItemPublicProp( pPublicPorp );
	
	return true;
}

CItemPublicProp* CItemPublicPropManagerEx::GetItemPublicProp(long itemID )
{
	std::map<long,CItemPublicProp*>::iterator lter = mItemPublicPropMan.find( itemID );
	if ( lter != mItemPublicPropMan.end() )
	{
		return lter->second;
	}
	return NULL;
}

CItemPublicPropManagerEx::~CItemPublicPropManagerEx()
{
	std::map<long,CItemPublicProp*>::iterator lter = mItemPublicPropMan.begin();
	for ( ; lter!=mItemPublicPropMan.end(); lter++ )
	{
		if ( NULL != lter->second )
		{
			delete lter->second;
		}
	}
	mItemPublicPropMan.clear();
}

void CItemPublicPropManagerEx::InsertItemPublicProp(CItemPublicProp* itemPublicProp )
{
	if ( !itemPublicProp )
		return;

	mItemPublicPropMan.insert( std::pair<long,CItemPublicProp*>(itemPublicProp->GetItemID(), itemPublicProp) ); 
}

bool CItemPublicPropManagerEx::SetItemTypeID(long itemID, ITEM_TYPEID typeID )
{
	CItemPublicProp* pProp = GetItemPublicProp(itemID) ; 
	if ( pProp )
	{
		pProp->m_itemTypeID = typeID;
		return true;
	}
	return false;
}

template<>
bool CPropertyManagerT<WeaponProperty>::InsertItemSpecialProperty(CElement *pxmlElement)
{
	if ( !pxmlElement )
		return false;


	WeaponProperty* weaponProp = NEW WeaponProperty;
	if ( !weaponProp )
		return false;

	StructMemSet( *weaponProp, 0x0,sizeof(WeaponProperty) );

	long itemID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );
	weaponProp->mGearArmType = (ITEM_GEARARM_TYPE)ACE_OS::atoi( pxmlElement->GetAttr("GearArmType") );
	weaponProp->brand = ACE_OS::atoi( pxmlElement->GetAttr("Brand"));
	weaponProp->SkillRatio = ACE_OS::atoi( pxmlElement->GetAttr("SkillRatio") );
	weaponProp->SkillID = ACE_OS::atoi( pxmlElement->GetAttr("SkillID") );
	weaponProp->AttackPhyMax = ACE_OS::atoi( pxmlElement->GetAttr("AttackPhyMax") );
	weaponProp->AttackPhyMin = ACE_OS::atoi( pxmlElement->GetAttr("AttackPhyMin") );
	weaponProp->AttackMagMax = ACE_OS::atoi( pxmlElement->GetAttr("AttackMagMax") );
	weaponProp->AttackMagMin = ACE_OS::atoi( pxmlElement->GetAttr("AttackMagMin") );
	weaponProp->Hp = /*ACE_OS::atoi( pxmlElement->GetAttr("Hp") )*/0;
	weaponProp->Mp = /*ACE_OS::atoi( pxmlElement->GetAttr("Mp") )*/0;
	weaponProp->PhysicsAttack = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttack") )*/0;
	weaponProp->MagicAttack = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicAttack") )*/0;
	if (pxmlElement->GetAttr("PhysicsDefend"))
	{
		weaponProp->PhysicsDefend = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDefend") );
	}
	if (pxmlElement->GetAttr("MagicDefend"))
	{
		weaponProp->MagicDefend = ACE_OS::atoi( pxmlElement->GetAttr("MagicDefend") );
	}
	weaponProp->PhysicsHit = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsHit") );
	weaponProp->MagicHit = ACE_OS::atoi( pxmlElement->GetAttr("MagicHit") );
	weaponProp->PhysicsCritical = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCritical") )*/0;
	weaponProp->MagicCritical = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicCritical") )*/0;
	weaponProp->PhysicsAttackPercent = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttackPercent") );
	weaponProp->MagicAttackPercent = ACE_OS::atoi( pxmlElement->GetAttr("MagicAttackPercent") );
	weaponProp->PhysicsCriticalDamage = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDamage") )*/0;
	weaponProp->MagicCriticalDamage = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDamage") )*/0;
	weaponProp->PhysicsDefendPercent = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDefendPercent") )*/0;
	weaponProp->MagicDefendPercent = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicDefendPercent") )*/0;
	weaponProp->PhysicsCriticalDamageDerate = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDamageDerate") )*/0;
	weaponProp->MagicCriticalDamageDerate = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDamageDerate") )*/0;
	weaponProp->PhysicsJouk = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsJouk") )*/0;
	weaponProp->MagicJouk  = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicJouk") )*/0;
	weaponProp->PhysicsDeratePercent = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDeratePercent") )*/0;
	weaponProp->MagicDeratePercent = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicDeratePercent") )*/0;
	weaponProp->Stun = ACE_OS::atoi( pxmlElement->GetAttr("Stun") );
	weaponProp->Tie = ACE_OS::atoi( pxmlElement->GetAttr("Tie") );
	weaponProp->Antistun = ACE_OS::atoi( pxmlElement->GetAttr("Antistun") );
	weaponProp->Antitie = ACE_OS::atoi( pxmlElement->GetAttr("Antitie") );
	weaponProp->AddSpeed = ACE_OS::atoi( pxmlElement->GetAttr("AddSpeed") );
	weaponProp->PhysicsAttackAdd = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttackAdd") )*/0;
	weaponProp->MagicAttackAdd = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicAttackAdd") )*/0;
	weaponProp->PhysicsRift = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsRift") )*/0;
	weaponProp->MagicRift = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicRift") )*/0;
	if (pxmlElement->GetAttr("ExcellentBiffLevel"))
		weaponProp->ExcellentBiff = ACE_OS::atoi( pxmlElement->GetAttr("ExcellentBiffLevel") );
	if (pxmlElement->GetAttr("LuckyBiffLevel"))
		weaponProp->LuckyBiff = ACE_OS::atoi( pxmlElement->GetAttr("LuckyBiffLevel") );
	weaponProp->SuitId = ACE_OS::atoi( pxmlElement->GetAttr("SuitId") );
	weaponProp->itemLevelUpLimit = ACE_OS::atoi( pxmlElement->GetAttr("UpgradeLimit") );
	weaponProp->itemCriID = ACE_OS::atoi( pxmlElement->GetAttr("UpgradeNum1") );
	weaponProp->itemMaterialID = ACE_OS::atoi( pxmlElement->GetAttr("UpgradeNum2") );
	//直接通过luckyID与excellentID来决定是否卓越/幸运装备if ( NULL != pxmlElement->GetAttr("Masslevel2") )
	//{
	//	weaponProp->luckytype = ACE_OS::atoi( pxmlElement->GetAttr("Masslevel2") );
	//}
	if(  NULL != pxmlElement->GetAttr("LuckyID") )
	{
		weaponProp->luckid = ACE_OS::atoi( pxmlElement->GetAttr("LuckyID") );
	}
	if ( NULL != pxmlElement->GetAttr("ExcellentID") )
	{
		weaponProp->excellentid = ACE_OS::atoi( pxmlElement->GetAttr("ExcellentID") );
	}

	////道具对5个人物基本属性的添加体现在幸运卓越属性中，而幸运卓越属性由随机种子算得，by dzj, 10.05.25, if ( NULL != pxmlElement->GetAttr("Str") ){
	//	weaponProp->strength = ACE_OS::atoi( pxmlElement->GetAttr("Str") );
	//}

	//if ( NULL != pxmlElement->GetAttr("Agi") ){
	//	weaponProp->agility  = ACE_OS::atoi( pxmlElement->GetAttr("Agi") );
	//}

	//if ( NULL != pxmlElement->GetAttr("Int" ) ){
	//	weaponProp->intelligence = ACE_OS::atoi( pxmlElement->GetAttr("Int") );
	//}

	//if ( NULL != pxmlElement->GetAttr("Spi") ){
	//	weaponProp->spirit = ACE_OS::atoi( pxmlElement->GetAttr("Spi") );
	//}

	//if ( NULL != pxmlElement->GetAttr("Vit") ){
	//	weaponProp->physique = ACE_OS::atoi( pxmlElement->GetAttr("Vit") );
	//}

	if ( pxmlElement->GetAttr("SkillRatio") )
		weaponProp->SkillRatio = ACE_OS::atoi( pxmlElement->GetAttr("SkillRatio") );

	if ( pxmlElement->GetAttr("SkillID") )
		weaponProp->SkillID = ACE_OS::atoi( pxmlElement->GetAttr("SkillID") );

	if ( mPropertyMapT.find(itemID) != mPropertyMapT.end() )
	{
		D_DEBUG("WeaponProperty 重复的道具ID号:%d\n",itemID );
		delete weaponProp;
	}
	else
	{
		mPropertyMapT.insert( std::pair<long,WeaponProperty*>(itemID,weaponProp) );
		CItemPublicPropManagerExSingle::instance()->SetItemTypeID( itemID,E_TYPE_WEAPON );
	}

	return true;
}

template<>
bool CPropertyManagerT<EquipProperty>::InsertItemSpecialProperty(CElement *pxmlElement)
{
	if ( !pxmlElement )
		return false;

	EquipProperty* equipProp = NEW EquipProperty;
	if ( !equipProp )
		return false;

	unsigned int itemID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );
	equipProp->mGearArmType = (ITEM_GEARARM_TYPE)ACE_OS::atoi( pxmlElement->GetAttr("GearArmType") );
	equipProp->brand = ACE_OS::atoi( pxmlElement->GetAttr("Brand") );
	equipProp->Hp = /*ACE_OS::atoi( pxmlElement->GetAttr("Hp") )*/0;
	equipProp->Mp = /*ACE_OS::atoi( pxmlElement->GetAttr("Mp") )*/0;
	equipProp->PhysicsAttack = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttack") );
	equipProp->MagicAttack = ACE_OS::atoi( pxmlElement->GetAttr("MagicAttack") );
	equipProp->PhysicsDefend = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDefend") );
	equipProp->MagicDefend = ACE_OS::atoi( pxmlElement->GetAttr("MagicDefend") );
	equipProp->PhysicsHit = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsHit") )*/0;
	equipProp->MagicHit = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicHit") )*/0;
	equipProp->PhysicsCritical = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCritical") )*/0;
	equipProp->MagicCritical = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicCritical") )*/0;
	equipProp->PhysicsAttackPercent = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttackPercent") )*/0;
	equipProp->MagicAttackPercent = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicAttackPercent") )*/0;
	equipProp->PhysicsCriticalDamage = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDamage") )*/0;
	equipProp->MagicCriticalDamage = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDamage") )*/0;
	equipProp->PhysicsDefendPercent = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDefendPercent") );
	equipProp->MagicDefendPercent = ACE_OS::atoi( pxmlElement->GetAttr("MagicDefendPercent") );
	equipProp->PhysicsCriticalDerate = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDerate") )*/0;
	equipProp->MagicCriticalDerate = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDerate") )*/0;
	equipProp->PhysicsCriticalDamageDerate = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDamageDerate") )*/0;
	equipProp->MagicCriticalDamageDerate = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDamageDerate") )*/0;
	equipProp->PhysicsJouk = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsJouk") )*/0;
	equipProp->MagicJouk = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicJouk") )*/0;
	equipProp->PhysicsDeratePercent = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDeratePercent"));
	equipProp->MagicDeratePercent = ACE_OS::atoi( pxmlElement->GetAttr("MagicDeratePercent") );
	equipProp->Stun = ACE_OS::atoi( pxmlElement->GetAttr("Stun") );
	equipProp->Tie = ACE_OS::atoi( pxmlElement->GetAttr("Tie") );
	equipProp->Antistun = ACE_OS::atoi( pxmlElement->GetAttr("Antistun") );
	equipProp->Antitie = ACE_OS::atoi( pxmlElement->GetAttr("Antitie") );
	equipProp->AddSpeed = ACE_OS::atoi( pxmlElement->GetAttr("AddSpeed") );
	equipProp->PhysicsAttackAdd = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttackAdd") )*/0;
	equipProp->MagicAttackAdd = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicAttackAdd") )*/0;
	equipProp->PhysicsRift = /*ACE_OS::atoi( pxmlElement->GetAttr("PhysicsRift") )*/0;
	equipProp->MagicRift = /*ACE_OS::atoi( pxmlElement->GetAttr("MagicRift") )*/0;
	if (pxmlElement->GetAttr("ExcellentBiffLevel"))
		equipProp->ExcellentBiff = ACE_OS::atoi( pxmlElement->GetAttr("ExcellentBiffLevel") );
	if (pxmlElement->GetAttr("LuckyBiffLevel"))
		equipProp->LuckyBiff = ACE_OS::atoi( pxmlElement->GetAttr("LuckyBiffLevel") );
	equipProp->SuitId = ACE_OS::atoi( pxmlElement->GetAttr("SuitId") );
	equipProp->itemLevelUpLimit = ACE_OS::atoi( pxmlElement->GetAttr("UpgradeLimit") );
	equipProp->itemCriID = ACE_OS::atoi( pxmlElement->GetAttr("UpgradeNum1") );
	equipProp->itemMaterialID = ACE_OS::atoi( pxmlElement->GetAttr("UpgradeNum2") );

	////道具对5个人物基本属性的添加体现在幸运卓越属性中，而幸运卓越属性由随机种子算得，by dzj, 10.05.25, if ( NULL != pxmlElement->GetAttr("Str") ){
	//	equipProp->strength = ACE_OS::atoi( pxmlElement->GetAttr("Str") );
	//}

	//if ( NULL != pxmlElement->GetAttr("Agi") ){
	//	equipProp->agility  = ACE_OS::atoi( pxmlElement->GetAttr("Agi") );
	//}

	//if ( NULL != pxmlElement->GetAttr("Int" ) ){
	//	equipProp->intelligence = ACE_OS::atoi( pxmlElement->GetAttr("Int") );
	//}

	//if ( NULL != pxmlElement->GetAttr("Spi") ){
	//	equipProp->spirit = ACE_OS::atoi( pxmlElement->GetAttr("Spi") );
	//}

	//if ( NULL != pxmlElement->GetAttr("Vit") ){
	//	equipProp->physique = ACE_OS::atoi( pxmlElement->GetAttr("Vit") );
	//}

	//直接通过luckyID与excellentID来决定是否卓越/幸运装备if ( NULL != pxmlElement->GetAttr("Masslevel2") )
	//{
	//	equipProp->luckytype = ACE_OS::atoi( pxmlElement->GetAttr("Masslevel2") );
	//}
	if(  NULL != pxmlElement->GetAttr("LuckyID") )
	{
		equipProp->luckid = ACE_OS::atoi( pxmlElement->GetAttr("LuckyID") );
	}
	if ( NULL != pxmlElement->GetAttr("ExcellentID") )
	{
		equipProp->excellentid = ACE_OS::atoi( pxmlElement->GetAttr("ExcellentID") );
	}

	if ( mPropertyMapT.find(itemID) !=mPropertyMapT.end() )
	{
		D_DEBUG("EquipProperty重复的装备编号:%d\n",itemID );
		delete equipProp;
	}
	else
		mPropertyMapT.insert( std::pair<unsigned int,EquipProperty*>(itemID,equipProp) );

	CItemPublicPropManagerExSingle::instance()->SetItemTypeID( itemID,E_TYPE_EQUIP );

	return true;
}

template<>
bool CPropertyManagerT<MountProperty>::InsertItemSpecialProperty(CElement * pxmlElement )
{
	if ( !pxmlElement )
		return false;

	MountProperty* pProperty = NEW MountProperty;
	if ( !pProperty )
		return false;

	unsigned int itemID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );

	const char* p = pxmlElement->GetAttr("GearArmType");
	if ( p )
		pProperty->mGearArmType = (ITEM_GEARARM_TYPE)ACE_OS::atoi( pxmlElement->GetAttr("GearArmType") );
	
	p = pxmlElement->GetAttr("Brand");
	if ( p )
		pProperty->brand = ACE_OS::atoi( pxmlElement->GetAttr("Brand") );

	p = pxmlElement->GetAttr("Hp");
	if ( p )
		pProperty->Hp = ACE_OS::atoi( pxmlElement->GetAttr("Hp") );
	
	p = pxmlElement->GetAttr("Mp");
	if ( p )
		pProperty->Mp = ACE_OS::atoi( pxmlElement->GetAttr("Mp") );
	
	p = pxmlElement->GetAttr("PhysicsAttack");
	if ( p )
		pProperty->PhysicsAttack = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttack") );
	
	p = pxmlElement->GetAttr("MagicAttack");
	if ( p )
		pProperty->MagicAttack = ACE_OS::atoi( pxmlElement->GetAttr("MagicAttack") );
	
	p = pxmlElement->GetAttr("PhysicsDefend");
	if ( p )
		pProperty->PhysicsDefend = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDefend") );
	
	if( pxmlElement->GetAttr("MagicDefend") )
		pProperty->MagicDefend = ACE_OS::atoi( pxmlElement->GetAttr("MagicDefend") );
	
	if(  pxmlElement->GetAttr("PhysicsHit") )
		pProperty->PhysicsHit = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsHit") );
	
	if(  pxmlElement->GetAttr("PhysicsHit") )
		pProperty->MagicHit = ACE_OS::atoi( pxmlElement->GetAttr("MagicHit") );
	
	if(  pxmlElement->GetAttr("PhysicsCritical") )
		pProperty->PhysicsCritical = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCritical") );
	
	if( pxmlElement->GetAttr("MagicCritical") )
		pProperty->MagicCritical = ACE_OS::atoi( pxmlElement->GetAttr("MagicCritical") );
	
	if( pxmlElement->GetAttr("PhysicsAttackPercent") )
		pProperty->PhysicsAttackPercent = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttackPercent") );
	
	if( pxmlElement->GetAttr("MagicAttackPercent") ) 
		pProperty->MagicAttackPercent = ACE_OS::atoi( pxmlElement->GetAttr("MagicAttackPercent") );

	if( pxmlElement->GetAttr("PhysicsCriticalDamage") )
		pProperty->PhysicsCriticalDamage = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDamage") );
	
	if( pxmlElement->GetAttr("MagicCriticalDamage") )
		pProperty->MagicCriticalDamage = ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDamage") );
	
	if( pxmlElement->GetAttr("PhysicsDefendPercent") )
		pProperty->PhysicsDefendPercent = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDefendPercent") );

	if( pxmlElement->GetAttr("MagicDefendPercent") )
		pProperty->MagicDefendPercent = ACE_OS::atoi( pxmlElement->GetAttr("MagicDefendPercent") );
	
	if( pxmlElement->GetAttr("PhysicsCriticalDerate") )
		pProperty->PhysicsCriticalDerate = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDerate") );
	
	if( pxmlElement->GetAttr("MagicCriticalDerate") )
		pProperty->MagicCriticalDerate = ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDerate") );
	
	if( pxmlElement->GetAttr("PhysicsCriticalDamageDerate") ) 
		pProperty->PhysicsCriticalDamageDerate = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDamageDerate") );
	
	if( pxmlElement->GetAttr("MagicCriticalDamageDerate") ) 
		pProperty->MagicCriticalDamageDerate = ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDamageDerate") );

	if( pxmlElement->GetAttr("PhysicsJouk") )
		pProperty->PhysicsJouk = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsJouk") );
	
	if( pxmlElement->GetAttr("MagicJouk") )
		pProperty->MagicJouk = ACE_OS::atoi( pxmlElement->GetAttr("MagicJouk") );
	
	if( pxmlElement->GetAttr("PhysicsDeratePercent") )
		pProperty->PhysicsDeratePercent = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDeratePercent"));

	if( pxmlElement->GetAttr("MagicDeratePercent") )
		pProperty->MagicDeratePercent = ACE_OS::atoi( pxmlElement->GetAttr("MagicDeratePercent") );
	
	if( pxmlElement->GetAttr("Stun") )
		pProperty->Stun = ACE_OS::atoi( pxmlElement->GetAttr("Stun") );
	
	if( pxmlElement->GetAttr("Tie") ) 
		pProperty->Tie = ACE_OS::atoi( pxmlElement->GetAttr("Tie") );
	
	if( pxmlElement->GetAttr("Antistun") )
		pProperty->Antistun = ACE_OS::atoi( pxmlElement->GetAttr("Antistun") );
	
	if( pxmlElement->GetAttr("Antitie") )
		pProperty->Antitie = ACE_OS::atoi( pxmlElement->GetAttr("Antitie") );
	
	if( pxmlElement->GetAttr("AddSpeed") )
		pProperty->AddSpeed = (int)atof( pxmlElement->GetAttr("AddSpeed") );
	
	if( pxmlElement->GetAttr("PhysicsAttackAdd") )
		pProperty->PhysicsAttackAdd = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttackAdd") );
	
	if( pxmlElement->GetAttr("MagicAttackAdd") )
		pProperty->MagicAttackAdd = ACE_OS::atoi( pxmlElement->GetAttr("MagicAttackAdd") );
	
	if( pxmlElement->GetAttr("PhysicsRift") )
		pProperty->PhysicsRift = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsRift") );
	
	if( pxmlElement->GetAttr("MagicRift") )
		pProperty->MagicRift = ACE_OS::atoi( pxmlElement->GetAttr("MagicRift") );
	
	if( pxmlElement->GetAttr("SuitId") )
		pProperty->SuitId = ACE_OS::atoi( pxmlElement->GetAttr("SuitId") );
	
	if( pxmlElement->GetAttr("UpgradeLimit") )
		pProperty->itemLevelUpLimit = ACE_OS::atoi( pxmlElement->GetAttr("UpgradeLimit") );
	
	if( pxmlElement->GetAttr("UpgradeNum1") )
		pProperty->itemCriID = ACE_OS::atoi( pxmlElement->GetAttr("UpgradeNum1") );
	
	if( pxmlElement->GetAttr("UpgradeNum2") )
		pProperty->itemMaterialID = ACE_OS::atoi( pxmlElement->GetAttr("UpgradeNum2") );

    p = pxmlElement->GetAttr("time");
	if ( p )
		pProperty->ActiveTime = ACE_OS::atoi( p );

	p = pxmlElement->GetAttr("cri");
	if ( p )
		pProperty->BeKickCri = ACE_OS::atoi( p );

	if ( mPropertyMapT.find(itemID) !=mPropertyMapT.end() )
	{
		D_DEBUG("MountProperty重复的装备编号:%d\n",itemID );
		delete pProperty;
	}
	else
		mPropertyMapT.insert( std::pair<unsigned int,MountProperty*>( itemID,pProperty ) );

	CItemPublicPropManagerExSingle::instance()->SetItemTypeID( itemID,E_TYPE_MOUNT  );
	return true;
}

//管理消耗品
template<>
bool CPropertyManagerT<ConsumeProperty>::InsertItemSpecialProperty(CElement *pxmlElement)
{
	if ( !pxmlElement )
		return false;

	ConsumeProperty* consumeProp = NEW ConsumeProperty;
	if ( !consumeProp )
		return false;

	long itemID = ACE_OS::atoi(  pxmlElement->GetAttr("ItemID")  );

	consumeProp->Uniquely =  ACE_OS::atoi( pxmlElement->GetAttr("Uniquely") );
	consumeProp->skillID  =  ACE_OS::atoi( pxmlElement->GetAttr("ItemSkillID") );

	if ( pxmlElement->GetAttr("Type") )
		consumeProp->comsumeType = (ConsumeProperty::ConsumeType)ACE_OS::atoi( pxmlElement->GetAttr("Type") );
	

	if ( mPropertyMapT.find(itemID) != mPropertyMapT.end() )
	{
		D_DEBUG("ConsumeProperty 重复的道具ID号:%d\n", itemID );
		delete consumeProp;
	}
	else
	{
		mPropertyMapT.insert( std::pair<long,ConsumeProperty *>( itemID, consumeProp ) );
		CItemPublicPropManagerExSingle::instance()->SetItemTypeID( itemID, E_TYPE_CONSUME );
	}
	
	return true;
}


//管理任务物品
template<>
bool CPropertyManagerT<TaskProperty>::InsertItemSpecialProperty(CElement *pxmlElement)
{
	if ( !pxmlElement )
		return true;

	TaskProperty* tProp = NEW  TaskProperty;
	if ( !tProp )
		return false;

	long itemID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );

	const char* p = pxmlElement->GetAttr("TaskScript");
	if ( p )
		tProp->TaskScript = ACE_OS::atoi( p );
	else
	{
		D_ERROR("任务道具的TaskScript属性不存在\n");
	}

	p = pxmlElement->GetAttr("Uniquely");
	if ( p )
		tProp->Uniquely = ACE_OS::atoi( p );
	else
	{
		D_ERROR("任务道具的Uniquely属性不存在\n");
	}

	p = pxmlElement->GetAttr("Type");
	if ( p )
		tProp->type = ACE_OS::atoi( p );
	else
	{
		D_ERROR("任务道具的Type属性不存在\n");
	}

	p = pxmlElement->GetAttr("TaskID");
	if ( p )
	{
		tProp->TaskID = ACE_OS::atoi( p );
		if ( tProp->TaskID == 0 )
			tProp->TaskID = 1;
	}
	else
	{
		D_ERROR("任务道具的TaskID属性不存在\n");
	}

	if ( mPropertyMapT.find(itemID) != mPropertyMapT.end() )
	{
		D_ERROR("taskitem.xml里面任务道具编号重复:%d\n",itemID );
		delete tProp;
	}
	else
	{
		mPropertyMapT.insert( std::pair<unsigned int,TaskProperty *>( itemID,tProp ) );
		CItemPublicPropManagerExSingle::instance()->SetItemTypeID( itemID,E_TYPE_TASK );
	}
	return true;
}


//管理功能性道具
template<>
bool CPropertyManagerT<FunctionProperty>::InsertItemSpecialProperty(CElement *pxmlElement)
{
	if ( !pxmlElement )
		return false;

	FunctionProperty* pFunctionProp = NEW FunctionProperty;
	if ( !pFunctionProp )
		return false;

	long itemID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );

	pFunctionProp->Expend = ACE_OS::atoi( pxmlElement->GetAttr("Expend"));
	pFunctionProp->TaskScript = ACE_OS::atoi( pxmlElement->GetAttr("TaskScript") );
	pFunctionProp->Type = (FunctionProperty::FunctionType)ACE_OS::atoi( pxmlElement->GetAttr("Type") );
	pFunctionProp->AddSpeed = (float)atof( pxmlElement->GetAttr("AddSpeed") );
	pFunctionProp->maxWear = ACE_OS::atoi( pxmlElement->GetAttr("MaxWear"));
	pFunctionProp->repairMode = ACE_OS::atoi(pxmlElement->GetAttr("RepairMode"));
	if ( pxmlElement->GetAttr("sex") )
		pFunctionProp->sex  = ACE_OS::atoi( pxmlElement->GetAttr("sex") );
	else
		pFunctionProp->sex  = 0;
	//if ( pxmlElement->GetAttr("nSTR") )
	//	pFunctionProp->Strength  = ACE_OS::atoi( pxmlElement->GetAttr("nSTR") );
	//else
	//	pFunctionProp->Strength  = 0;
	//if ( pxmlElement->GetAttr("nVIT") )
	//	pFunctionProp->Vitality  = ACE_OS::atoi( pxmlElement->GetAttr("nVIT") );
	//else
	//	pFunctionProp->Vitality  = 0;
	//if ( pxmlElement->GetAttr("nAGI") )
	//	pFunctionProp->Agility  = ACE_OS::atoi( pxmlElement->GetAttr("nAGI") );
	//else
	//	pFunctionProp->Agility  = 0;
	//if ( pxmlElement->GetAttr("nSPI") )
	//	pFunctionProp->Spirit  = ACE_OS::atoi( pxmlElement->GetAttr("nSPI") );
	//else
	//	pFunctionProp->Spirit  = 0;
	//if ( pxmlElement->GetAttr("nINT") )
	//	pFunctionProp->Intelligence  = ACE_OS::atoi( pxmlElement->GetAttr("nINT") );
	//else
	//	pFunctionProp->Intelligence  = 0;
	//if ( pxmlElement->GetAttr("PreSkill") )
	//	pFunctionProp->PreSkillID  = ACE_OS::atoi( pxmlElement->GetAttr("PreSkill") );
	//else
	//	pFunctionProp->PreSkillID  = 0;
	
	if ( mPropertyMapT.find( itemID) != mPropertyMapT.end() )
	{
		D_ERROR("functionitem.xml里面function道具编号重复:%d\n",itemID );
		delete pFunctionProp;
	}
	else
	{
		mPropertyMapT.insert( std::pair<unsigned int,FunctionProperty *>(itemID,pFunctionProp) );
		CItemPublicPropManagerExSingle::instance()->SetItemTypeID( itemID,E_TYPE_FUNCTION );
	}
	return true;
}

template<>
bool CPropertyManagerT<SkillBookProperty>::InsertItemSpecialProperty(CElement *pxmlElement)
{
	if ( !pxmlElement )
		return false;

	SkillBookProperty* pSkillBook = NEW SkillBookProperty;
	if ( !pSkillBook )
		return false;

	long itemID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );
	pSkillBook->skillID = ACE_OS::atoi( pxmlElement->GetAttr("SkillID") );
	pSkillBook->skillType = (SkillBookProperty::SkillBookType)ACE_OS::atoi( pxmlElement->GetAttr("Type") );
	if ( pxmlElement->GetAttr("nSTR") )
		pSkillBook->Strength  = ACE_OS::atoi( pxmlElement->GetAttr("nSTR") );
	else
		pSkillBook->Strength  = 0;
	if ( pxmlElement->GetAttr("nVIT") )
		pSkillBook->Vitality  = ACE_OS::atoi( pxmlElement->GetAttr("nVIT") );
	else
		pSkillBook->Vitality  = 0;
	if ( pxmlElement->GetAttr("nAGI") )
		pSkillBook->Agility  = ACE_OS::atoi( pxmlElement->GetAttr("nAGI") );
	else
		pSkillBook->Agility  = 0;
	if ( pxmlElement->GetAttr("nSPI") )
		pSkillBook->Spirit  = ACE_OS::atoi( pxmlElement->GetAttr("nSPI") );
	else
		pSkillBook->Spirit  = 0;
	if ( pxmlElement->GetAttr("nINT") )
		pSkillBook->Intelligence  = ACE_OS::atoi( pxmlElement->GetAttr("nINT") );
	else
		pSkillBook->Intelligence  = 0;
	if ( pxmlElement->GetAttr("PreSkill") )
		pSkillBook->PreSkillID  = ACE_OS::atoi( pxmlElement->GetAttr("PreSkill") );
	else
		pSkillBook->PreSkillID  = 0;

	mPropertyMapT.insert( std::pair<unsigned int,SkillBookProperty *>(itemID, pSkillBook ) );

	CItemPublicPropManagerExSingle::instance()->SetItemTypeID( itemID, E_TYPE_SKILL );
	return true;
}


//管理图纸的属性
template<>
bool CPropertyManagerT<AxiologyProperty>::InsertItemSpecialProperty( CElement* pxmlElement )
{
	if ( !pxmlElement )
		return false;

	AxiologyProperty* pAxiologyProp = NEW AxiologyProperty;
	if ( !pAxiologyProp )
		return false;

	long itemID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );

	pAxiologyProp->value  = (float)atof( pxmlElement->GetAttr("value") );
	pAxiologyProp->type   = ( AxiologyProperty::AxiologyType )ACE_OS::atoi( pxmlElement->GetAttr("Type") );

	mPropertyMapT.insert( std::pair<unsigned int,AxiologyProperty *>(itemID,pAxiologyProp) );
	CItemPublicPropManagerExSingle::instance()->SetItemTypeID( itemID,E_TYPE_AXIOLOGY );
	
	return true;
}




//管理图纸的属性
template<>
bool CPropertyManagerT<CBulePrint>::InsertItemSpecialProperty( CElement* pxmlElement )
{
	if ( !pxmlElement )
		return false;

	CBulePrint* pBulePrint = NEW CBulePrint( pxmlElement );
	if( !pBulePrint )
		return false;

	unsigned int bulePrintID = pBulePrint->GetBulePrintID();
	if ( mPropertyMapT.find(bulePrintID) != mPropertyMapT.end() )
	{
		D_ERROR("%d 图纸编号重复\n",bulePrintID );
		delete pBulePrint;
	}
	else
	{
		mPropertyMapT.insert( std::pair<unsigned int, CBulePrint *>( bulePrintID , pBulePrint ) );
	}

	CItemPublicPropManagerExSingle::instance()->SetItemTypeID( bulePrintID,E_TYPE_BULEPRINT );

	return true;
}


CBulePrint::CBulePrint(CElement *pxmlElement)
{
	if ( !pxmlElement )
		return;

	factorIndex = 0;

	bulePrintID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );
	GearArmType = 0;
	const char * pGearArmTypeStr = pxmlElement->GetAttr("GearArmType");
	if ( pGearArmTypeStr )
	{
		char* p = ACE_OS::strtok( const_cast<char *>(pGearArmTypeStr ), "," );
		while( p )
		{
			GearArmType |= 1<< ACE_OS::atoi( p );
			p = ACE_OS::strtok( NULL, "," );
		}
	}


	static char materialFormat[] = {"material%d"};
	for ( int i = 1 ; i<= 10; i++ )
	{
		static char szField[56]={0};
		ACE_OS::snprintf(szField,sizeof(szField),materialFormat,i);
		//获取材料编号
		const char* pMaterial = pxmlElement->GetAttr(szField);
		if ( pMaterial )
		{
			if (factorIndex >= ARRAY_SIZE(fatcorArr))
			{
				D_ERROR( "CBulePrint::CBulePrint, factorIndex(%d) >= ARRAY_SIZE(fatcorArr)\n", factorIndex );
				break;
			}
			BulePrintFactor &factor = fatcorArr[factorIndex];
			char *p = ACE_OS::strtok( const_cast<char *>( pMaterial ),",");
			if ( p )
			{
				factor.itemTypeID = ACE_OS::atoi( p );
				p = ACE_OS::strtok( NULL,",");
				if ( p )
				{
					factor.count = ACE_OS::atoi( p );
					factorIndex++;
				}
			}
		}
	}


	//随机的总概率
	OddTotal = 0 ;

	criIndex = 0;
	static char AddidFormat[] = {"Addid%d"};
	static char OddFormat[] = {"Odd%d"};
	for ( int i = 1 ; i<=8; i++ )
	{
		static char szField1[56] = {0};
		ACE_OS::snprintf(szField1,sizeof(szField1),AddidFormat,i);
		
		const char * pAddid = pxmlElement->GetAttr(szField1);
		if ( pAddid )
		{
			int  addid = ACE_OS::atoi( pAddid );
			if ( addid )
			{
				static char szField2[56] = {0};
				ACE_OS::snprintf(szField2,sizeof(szField2),OddFormat,i);
				const char* pOdd = pxmlElement->GetAttr(szField2);
				if ( pOdd )
				{
					int odd = ACE_OS::atoi( pOdd );

					OddTotal+= odd;

					if (criIndex >= ARRAY_SIZE(criArr))
					{
						D_ERROR( "CBulePrint::CBulePrint, criIndex(%d) >= ARRAY_SIZE(criArr)\n", criIndex );
						break;
					}
					BulePrintCri &cri = criArr[criIndex];
					cri.Addid = addid;
					cri.Odd = OddTotal;

					criIndex++;
				}
			}
		}
	}
}



void  CBulePrint::GetBulePrintCondition( PlateCondition* conditionArr )
{
	if ( !conditionArr )
		return;

	for ( unsigned int i = 0u ; i< factorIndex && i<ARRAY_SIZE(fatcorArr); i++ )
	{
		PlateCondition& condition = conditionArr[i];
		condition.conNum = fatcorArr[i].count;
		condition.conType = fatcorArr[i].itemTypeID;
	}
}

//随机追加的结果
unsigned int CBulePrint::BulePrintRandonResult()
{
	unsigned int randNum = (unsigned int) RandNumber( 1, OddTotal );

	bool bFind = false;
	unsigned int  i = 0u;
	for ( ; i< criIndex && i<ARRAY_SIZE(criArr); i++ )
	{
		BulePrintCri& cri = criArr[i];
		if ( randNum <= cri.Odd )
		{
			bFind = true;
			break;
		}
	}

	if ( bFind )
	{
		return criArr[i].Addid;
	}

	return 1;
}

//加载幸运星的XML文件
void LuckStarManager::LoadXML( const char* pxmlFile )
{
	if ( !pxmlFile )
		return;


	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pxmlFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pxmlFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pxmlFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	unsigned int preItemID = 0;
	LuckStar tmpLuckStar;
	for ( std::vector<CElement*>::iterator lter = childElements.begin(); lter != childElements.end(); ++ lter )
	{
		CElement* pxmlElement = *lter;
		if ( pxmlElement )
		{
			unsigned int startItemID = ACE_OS::atoi( pxmlElement->GetAttr("ItemID") );
			if ( startItemID != preItemID && preItemID != 0  )//如果和之前的幸运星编号不一样了
			{
				mLuckStarMap.insert( std::pair<unsigned int,LuckStar>( preItemID, tmpLuckStar ) );
				tmpLuckStar.Reset();
			}

			tmpLuckStar.luckItemID = startItemID;
			
			LuckStarCondtion tmpCondition;
			tmpCondition.conditionID = ACE_OS::atoi( pxmlElement->GetAttr("NumId") );
			tmpCondition.propID      = ACE_OS::atoi( pxmlElement->GetAttr("change") );
			tmpCondition.compareCondition = ACE_OS::atoi( pxmlElement->GetAttr("id") );
			tmpCondition.compareValue     = ACE_OS::atoi( pxmlElement->GetAttr("value") );
			tmpLuckStar.luckStartConditonVec.push_back( tmpCondition );
		}
	}

	if ( tmpLuckStar.luckStartConditonVec.size() > 0 )
	{
		mLuckStarMap.insert( std::pair<unsigned int,LuckStar>( tmpLuckStar.luckItemID, tmpLuckStar ) );
	}

	TRY_END;
}


int LuckStar::RandomCondition()
{
	unsigned int randomcount = (unsigned int)luckStartConditonVec.size();

	if ( randomcount > 0 )
	{
		unsigned int randomIndex = RandNumber( 0, randomcount - 1 );

		if ( randomIndex < randomcount )
		{
			LuckStarCondtion& startCondition = luckStartConditonVec[randomIndex];
			return startCondition.conditionID;
		}
	}
	return -1;
}

LuckStarCondtion* LuckStar::GetConditionByID(unsigned int conditionID )
{
	unsigned int count = (unsigned int)luckStartConditonVec.size();
	for ( unsigned int i = 0; i<count; i++ )
	{
		LuckStarCondtion& condition = luckStartConditonVec[i];
		if ( condition.conditionID == conditionID )
		{
			return &condition;
		}
	}
	return NULL;
}

LuckStar* LuckStarManager::GetLuckStar(unsigned int itemTypeID )
{
	std::map< unsigned int, LuckStar>::iterator lter = mLuckStarMap.find( itemTypeID );
	if ( lter != mLuckStarMap.end() )
	{
		return &lter->second;
	}
	return NULL;
}

/*
compareCondition
0 < 
1 <=
2 = 
3 >=
4 >
*/

/*
propID
1 等级 level
2 职业 class
3 性别 sex
*/

bool LuckStar::CheckPlayerPropertyFillCondition(unsigned int conditionID,CPlayer* pPlayer )
{
	if ( !pPlayer )
		return false;

	LuckStarCondtion* pCondition =  GetConditionByID( conditionID );
	if ( pCondition )
	{
		unsigned int propID =  pCondition->propID;
		unsigned int compareTarget     = pCondition->compareValue;
		unsigned int compareCondition  = pCondition->compareCondition;
		unsigned int compareValue = 0;
		switch( propID )
		{
		case 1:
			compareValue = pPlayer->GetLevel();
			break;
		case 2:
			compareValue = pPlayer->GetClass();
			break;
		case 3:
			compareValue = pPlayer->GetSex();
			break;
		default:
			break;
		}

		bool isFillCondition = false;
		switch( compareCondition )
		{
		case 0:
			isFillCondition = compareValue < compareTarget;
			break;
		case 1:
			isFillCondition = compareValue <= compareTarget;
			break;
		case 2:
			isFillCondition = ( compareValue == compareTarget );
			break;
		case 3:
			isFillCondition = ( compareValue >= compareTarget );
			break;
		case 4:
			isFillCondition = ( compareValue > compareTarget );
			break;
		}

		return isFillCondition;
	}
	return false;
}


template<>
bool CPropertyManagerT<FashionProperty>::InsertItemSpecialProperty(CElement *pxmlElement)
{
	if ( NULL == pxmlElement )
		return false;

	FashionProperty* fashionProp = NEW FashionProperty;
	if ( NULL == fashionProp )
		return false;

	unsigned int itemID = ACE_OS::atoi(pxmlElement->GetAttr("ItemID"));
	fashionProp->gearArmType = (ITEM_GEARARM_TYPE)(ACE_OS::atoi( pxmlElement->GetAttr("GearArmType") ));
	fashionProp->expend = ACE_OS::atoi(pxmlElement->GetAttr("Expend"));
	fashionProp->sex = ACE_OS::atoi( pxmlElement->GetAttr("Sex") );
	
	int i = 0;
	const char *pModel = pxmlElement->GetAttr("ModelM");
	if(NULL != pModel)
	{
		char *pTemp = ACE_OS::strtok( const_cast<char *>(pModel) , ",");
		while((NULL != pTemp) && (i < sizeof(fashionProp->maleMode)/sizeof(fashionProp->maleMode[0])))
		{
			fashionProp->maleMode[i] = ACE_OS::atoi(pTemp);
			pTemp = ACE_OS::strtok( NULL , ",");

			i++;
		}
	}
	else
	{
		while(i < sizeof(fashionProp->maleMode)/sizeof(fashionProp->maleMode[0]))
		{	
			fashionProp->maleMode[i] = 0;
			i++;
		}
	}

	pModel = pxmlElement->GetAttr("ModelF");
	i = 0;
	if(NULL != pModel)
	{
		char *pTemp = ACE_OS::strtok( const_cast<char *>(pModel) , ",");
		while((NULL != pTemp) && (i < sizeof(fashionProp->femaleMode)/sizeof(fashionProp->femaleMode[0])))
		{
			fashionProp->femaleMode[i] = ACE_OS::atoi(pTemp);
			pTemp = ACE_OS::strtok( NULL , ",");

			i++;
		}
	}
	else
	{
		while(i < sizeof(fashionProp->femaleMode)/sizeof(fashionProp->femaleMode[0]))
		{	
			fashionProp->femaleMode[i] = 0;
			i++;
		}
	}
	
	fashionProp->unionFlag = (0 != ACE_OS::atoi( pxmlElement->GetAttr("GuildType") )) ? true :false;

	if ( mPropertyMapT.find(itemID) !=mPropertyMapT.end() )
	{
		D_DEBUG("FashionProperty重复的装备编号:%d\n",itemID );
		delete fashionProp;
	}
	else
		mPropertyMapT.insert( std::pair<unsigned int,FashionProperty*>(itemID, fashionProp) );

	CItemPublicPropManagerExSingle::instance()->SetItemTypeID( itemID,E_TYPE_FASHION );

	return true;
}


void MountLevelUpPropManager::LoadXML( const char* szFilePath )
{
	if ( !szFilePath )
		return;

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(szFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szFilePath );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szFilePath);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	for ( std::vector<CElement*>::iterator lter = childElements.begin(); lter != childElements.end(); ++ lter )
	{
		CElement* pxmlElement = *lter;
		if ( pxmlElement )
		{
			const char* p = pxmlElement->GetAttr("LevelUpId");
			if ( p == NULL )
				continue;

			unsigned int levelUpId = ACE_OS::atoi( p );

			MountLevelUpProp prop;

			p = pxmlElement->GetAttr("time");
			if ( p!= NULL )
				prop.ActiveTime = ACE_OS::atoi( p );

			p = pxmlElement->GetAttr("cri");
			if ( p!= NULL )
				prop.BeKickCri  = ACE_OS::atoi( p );

			mMountLevelUpPropManager[ levelUpId ] = prop;
		}
	}

	TRY_END;
}

MountLevelUpProp* MountLevelUpPropManager::GetMountLevelUpProp( unsigned int levelUpID )
{
	std::map<unsigned int, MountLevelUpProp>::iterator lter = mMountLevelUpPropManager.find( levelUpID );
	if ( lter != mMountLevelUpPropManager.end() )
	{
		return &lter->second;
	}

	return NULL;
}


void DiePunishInfo::LoadXML()
{

	const char szFilePath[] = {"config/dieprocess.xml"};

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(szFilePath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", szFilePath );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",szFilePath);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	for ( std::vector<CElement *>::iterator lter = childElements.begin(); lter!= childElements.end(); ++lter )
	{
		CElement* pElement = *lter;
		if ( pElement )
		{
			const char* pname = pElement->Name();
			if ( ACE_OS::strcmp( pname, "submoney") == 0 )
			{
				moneyprecent = ACE_OS::atoi( pElement->GetAttr("precent") );
				moneymin     = ACE_OS::atoi( pElement->GetAttr("min") );
				moneymax     = ACE_OS::atoi( pElement->GetAttr("max") );
			}
			else if( ACE_OS::strcmp( pname, "evilpunish" ) == 0 )
			{
				evilprecent = ACE_OS::atoi( pElement->GetAttr("precent") );
				expmin     = ACE_OS::atoi( pElement->GetAttr("min") );
				expmax     = ACE_OS::atoi( pElement->GetAttr("max") );
			}
		}
	}

	TRY_END;
}