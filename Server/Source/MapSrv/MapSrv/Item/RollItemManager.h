﻿/********************************************************************
created:	2008/07/23
created:	23:7:2008   11:44
file:		RollItemManager.cpp
author:		管传淇

purpose:	
*********************************************************************/

#ifndef ROLLITEMMANAGER_H
#define ROLLITEMMANAGER_H

#include "RollItem.h"
#include <map>

class CItemsPackage;
class CBaseItem;

class CItemPkgEle;

class RollItemManager
{
	friend class ACE_Singleton<RollItemManager, ACE_Null_Mutex>;

public:
	RollItemManager():rollItemIndex(1)
	{
		updateTime = ACE_OS::gettimeofday();
	}

	~RollItemManager()
	{
		for ( std::map<int, RollItem*>::iterator lter = mRollItemMap.begin();
			lter!= mRollItemMap.end();
			++lter )
		{
			if ( NULL != lter->second )
			{
				delete lter->second;
			}
		}
		mRollItemMap.clear();
	}

public:
	//创建RollItem点
	RollItem* CreateRollItem( CItemsPackage* pItemPkg, const CItemPkgEle& pItemEle, unsigned int groupID );

	void PushRollItem( unsigned long rollItemID, RollItem* pItem );

	RollItem* GetRollItem( unsigned long rollItemID );

	void RemoveRollItem( unsigned long rollItemID );

	void RemoveRollItem( RollItem* pItem );

	void MonitorRollItemStatus();

	void OnPlayerLeaveSrv( CPlayer* pLeavaPlayer );

private:
	void IncRollItemIndex() { ++rollItemIndex; } 

	int  GetRollItemIndex() { return rollItemIndex; }

private:
	int  rollItemIndex;
	std::map<int, RollItem*> mRollItemMap;
	ACE_Time_Value updateTime;

};

typedef ACE_Singleton<RollItemManager, ACE_Null_Mutex> RollItemManagerSingle;

#endif
