﻿#include "CEquipItemManager.h"
#include "ItemManager.h"
#include "UseItemRule.h"
#include "../MuxMap/muxmapbase.h"
#include "../RideState.h"

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void CEquipItemManagerEx::UseItem(CPlayer *pPlayer, unsigned long itemUID)
{
	if ( !pPlayer)
		return;

	pPlayer->CheckInvincible();

	UseItemRuleExecute::UseItem( pPlayer, itemUID );
}

void CEquipItemManagerEx::EquipItem(CPlayer* pPlayer, unsigned long itemUID,unsigned int equipIndex,unsigned int equipReturnIndex )
{

	if ( !pPlayer )
		return;

	//////////////////////////////////////////
	//copy...
	////如果骑乘状态,是不允许装备道具的
	//if ( pPlayer->GetSelfStateManager().IsOnRideState() )
	//	return;
	//..copy
	///////////////////////////////////////////////////

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CEquipItemManagerEx::EquipItem, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_3_Pkgs& itemPkgs  = playerInfo->pkgInfo;//包裹信息

	ItemInfo_i sItemInfo;
	//查验道具是否合法
	int pkgindex = 0;
	for ( ; pkgindex< PACKAGE_SIZE ;pkgindex++ )
	{
		sItemInfo = itemPkgs.pkgs[pkgindex];
		if ( sItemInfo.uiID == itemUID )//根据唯一的ID号，找到道具包中的道具
			break;
	}

	if ( pkgindex != PACKAGE_SIZE )
	{
		CBaseItem* pItem = pPlayer->GetItemDelegate().GetPkgItem( itemUID );
		if ( pItem )
		{
			//看这个道具是不是玩家能使用的,如果能使用，则进行下面的操作,否则告诉客户端使用失败
			if ( pItem->IsCanUse( pPlayer,pPlayer->GetRace(), pPlayer->GetClass() ,pPlayer->GetLevel() ) )
			{
				//如果装备道具失败
				if( !UseEquipRule::Execute( pPlayer, pItem, pkgindex, equipIndex, equipReturnIndex )  )
				{
					D_ERROR( "EquipItem，玩家%s将装备%d(类型%d)从包裹%d装备至位置%d失败\n", pPlayer->GetAccount(), sItemInfo.uiID, sItemInfo.usItemTypeID, pkgindex, equipIndex );
					EquipItemManagerSingle::instance()->OnUseItemFailed( pPlayer,itemUID );
				}
			}
		}
		else
		{
			D_ERROR("%s从包裹装备的道具在包裹管理器中找不到 %d\n",pPlayer->GetNickName(),itemUID );
		}
	}
}

void CEquipItemManagerEx::OnSortPkgItem(CPlayer* pPlayer , unsigned int labelIndex )
{
	if ( NULL == pPlayer )
	{
		return;
	}
	if ( labelIndex > 3 )
	{
		D_ERROR("玩家 %s 传来的背包排序页数不对 %d >=3 \n", pPlayer->GetNickName(),labelIndex );
		return;
	}

	unsigned int pageSize  = PKG_PAGE_SIZE * 2;
	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CEquipItemManagerEx::OnSortPkgItem, NULL == GetFullPlayerInfo()\n");
		return;
	}
	ItemInfo_i* pSortItemBegin  = playerInfo->pkgInfo.pkgs + 	labelIndex *  pageSize;
	ItemInfo_i* pSortItemEnd    = pSortItemBegin + pageSize;

	bool isSwap = false;
	for ( int i = pageSize - 1 ; i >= 1  ; i-- )
	{
		for (  int j = 0 ; j< i;  j++ )
		{
			ItemInfo_i& itemEle =   pSortItemBegin[j];
			ItemInfo_i& itemEle2  = pSortItemBegin[j+1];

			if( pPlayer->CompareSortItem( itemEle , itemEle2 ) )
			{
				ItemInfo_i tmpItem = itemEle;
				itemEle  = itemEle2;
				itemEle2 = tmpItem;

				if( !isSwap )  
					isSwap = true;
			}
		}
	}

	if ( isSwap )
	{
		//todo sck
		MGItemPkgConciseInfo  srvpkgmsg;
		//srvpkgmsg.pageIndex = labelIndex * 2;
		for (int i = 0; i<ARRAY_SIZE(srvpkgmsg.pkgInfo); ++i)
		{
			srvpkgmsg.pkgInfo[i].itemIndex = labelIndex *  pageSize +i;
			srvpkgmsg.pkgInfo[i].itemInfo = *(pSortItemBegin+i);
		}
		//StructMemCpy( srvpkgmsg.pkgInfo, pSortItemBegin, sizeof(ItemInfo_i)*PKG_PAGE_SIZE );
		pPlayer->SendPkg<MGItemPkgConciseInfo>( &srvpkgmsg );


		MGItemPkgConciseInfo srvpkgmsg2;
		//srvpkgmsg2.pageIndex = labelIndex * 2 + 1;
		for (int i = 0; i<ARRAY_SIZE(srvpkgmsg.pkgInfo); ++i)
		{
			srvpkgmsg2.pkgInfo[i].itemIndex = labelIndex *  pageSize+PKG_PAGE_SIZE +i;
			srvpkgmsg2.pkgInfo[i].itemInfo = *(pSortItemBegin+PKG_PAGE_SIZE+i);
		}
		//StructMemCpy( srvpkgmsg2.pkgInfo, pSortItemBegin + PKG_PAGE_SIZE , sizeof(ItemInfo_i)*PKG_PAGE_SIZE );
		pPlayer->SendPkg<MGItemPkgConciseInfo>( &srvpkgmsg2 );
	}
}

void  CEquipItemManagerEx::OnUseItemResult(CPlayer* pPlayer, const ItemInfo_i&  itemInfo)
{
	if ( !pPlayer )
		return;

	MGPlayerUseItem  srvmsg;
	srvmsg.litemInfo = itemInfo;
	srvmsg.lUsePlayerID = pPlayer->GetFullID();
	CMuxMap* pPlayerMap = pPlayer->GetCurMap();
	if ( NULL != pPlayerMap )
	{
		pPlayerMap->NotifySurrounding<MGPlayerUseItem>( &srvmsg, pPlayer->GetPosX(), pPlayer->GetPosY() );
	} 
}

void CEquipItemManagerEx::OnUseItemFailed(CPlayer *pPlayer, unsigned long itemUID )
{
	ACE_UNUSED_ARG( itemUID );
	if ( !pPlayer )
		return;

	MGUseItemResult srvmsg;
	srvmsg.result = false;
	pPlayer->SendPkg<MGUseItemResult>( &srvmsg );
}

void CEquipItemManagerEx::UnUseItem(CPlayer* pPlayer, unsigned long itemUID, unsigned int fromEquipPos, unsigned int toPkgPos )
{
	ACE_UNUSED_ARG( itemUID );
	if ( !pPlayer )
		return;

	if ( pPlayer->IsDying() )
		return;

	if ( pPlayer->IsFaint() )
	{
		D_DEBUG("玩家眩晕，无法卸载装备\n");
		return;
	}

	if ( fromEquipPos >= EQUIP_SIZE )
	{
		D_ERROR("卸载位置错误!! From Equip Pos :%d\n",fromEquipPos );
		return;
	}

	if ( toPkgPos >= NORMAL_PKG_SIZE )//不能放入人物背包
	{
		D_ERROR("装载位置错误!! To Pkg Pos: %d\n", toPkgPos );
		return;
	}

	long unUseItemType = -1 ;
	long unUseItemUID  = -1 ;

	if ( IsEquipPosAccord(E_DRIVE, fromEquipPos) )
	{
		CRideState& ridestate = pPlayer->GetSelfStateManager().GetRideState();
		if ( ridestate.IsActive() 
			&& ( ridestate.GetPlayerRideState() == E_RIDE_EQUIP ) 
			&& ( ridestate.GetPlayerRideType()  == CRideState::E_LEADER ) )//如果是队长
		{

			pPlayer->SendSystemChat("处于骑乘状态，不能直接卸载骑乘装备,必须先下马");
			return ;
		}
	}

	ItemInfo_i* pEquipInfo = pPlayer->GetEquipByPos( fromEquipPos );
	if ( NULL == pEquipInfo )
	{
		D_ERROR("CEquipItemManagerEx::UnUseItem, NULL == pEquipInfo\n");
		return;
	}

	CBaseItem* pEquipItem = pPlayer->GetItemDelegate().GetEquipItem( pEquipInfo->uiID );
	if ( !pEquipItem )
	{
		D_ERROR("CEquipItemManagerEx::UnUseItem 卸载道具，装备栏为空,%s找不到对应的道具\n", pPlayer->GetNickName() );
		return;
	}

	CBaseItem* pPkgItem  = NULL;

	ITEM_GEARARM_TYPE equipGear = pEquipItem->GetGearArmType();
	if ( equipGear == E_POSITION_MAX )
	{
		D_ERROR("CEquipItemManagerEx::UnUseItem 卸载道具,装备的位置为空\n");
		return;
	}

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CEquipItemManagerEx::UnUseItem, NULL == GetFullPlayerInfo()\n");
		return;
	}

	PlayerInfo_3_Pkgs& itemPkgs  = playerInfo->pkgInfo;//包裹信息
	if ( toPkgPos >= ARRAY_SIZE(itemPkgs.pkgs) )
	{
		D_ERROR("CEquipItemManagerEx::UnUseItem, toPkgPos%d越界\n", toPkgPos);
		return;
	}
	ItemInfo_i pkgItemInfo = itemPkgs.pkgs[toPkgPos];
	if ( pkgItemInfo.uiID != PlayerInfo_3_Pkgs::INVALIDATE )
	{
		pPkgItem = pPlayer->GetItemDelegate().GetPkgItem( pkgItemInfo.uiID );
		if ( !pPkgItem)
		{
			D_ERROR("卸载道具，%s的包裹管理器找不到对应的道具%d!\n",pPlayer->GetNickName(), pkgItemInfo.uiID );
			return;
		}

		ITEM_GEARARM_TYPE pkgGear = pPkgItem->GetGearArmType();
		if ( IsGearTypeSameEquipPos( pkgGear, equipGear ) )
		{
			//然后判断该道具能否被使用
			if( !pPkgItem->IsCanUse( pPlayer,pPlayer->GetRace(), pPlayer->GetClass(), pPlayer->GetLevel() ) )
			{
				return;
			}
		} else {
			pPlayer->SendSystemChat("包裹中的道具不能装备到该位置");
			return;
		}
	}
	//验证卸下的道具是否能放入包裹
	if (NULL != pPkgItem)
	{
		pPlayer->ClearPkgItem(pPkgItem->GetItemUID());
	}
	if (!pPlayer->IsFreePkgIndex(toPkgPos,pEquipItem->GetWidth(),pEquipItem->GetHeight()))
	{
		if (NULL != pPkgItem)
		{
			pPlayer->SetPkgItem(pkgItemInfo,toPkgPos);
		}
		pPlayer->SendSystemChat("包裹没有足够空间放入卸下的道具");
		return;
	}
	//记录卸载道具的ID号
	unUseItemType = pEquipInfo->usItemTypeID;
	unUseItemUID  = pEquipInfo->uiID;

	//卸载装备在身上的道具属性
	pPlayer->OnUnEquipItem( *pEquipInfo, fromEquipPos );
	//如果交换的道具存在，则加上交换上去的道具属性
	if ( pPkgItem )
	{
		pPlayer->OnEquipItem( pkgItemInfo, fromEquipPos );
	}


	//交换道具
	ItemInfo_i tmpInfo = *pEquipInfo;
	*pEquipInfo = pkgItemInfo;
	//pkgItemInfo = tmpInfo;
	pPlayer->SetPkgItem(tmpInfo,toPkgPos);

	pPlayer->NoticeEquipItemChange( fromEquipPos );
	pPlayer->NoticePkgItemChange( toPkgPos );

	return;
}

void CEquipItemManagerEx::OnUnUseItemResult(CPlayer *pPlayer, unsigned int itemTypeID, unsigned int itemUID)
{
	if ( !pPlayer )
		return;

	//@brief:通知周围的玩家,不使用道具
	pPlayer->OnUnUseItemNotifyNearByPlayer( itemTypeID );

	//@brief:通知自己,不使用道具
	MGUnUseItem srvmsg;
	srvmsg.playerID = pPlayer->GetFullID();
	srvmsg.equipIndex = itemUID;
	pPlayer->SendPkg<MGUnUseItem>( &srvmsg );
}

void CEquipItemManagerEx::SwapItemOnItemPkg(CPlayer *pPlayer, unsigned int itemFrom, unsigned int itemTo)
{
	if ( !pPlayer)
		return;

	//如果位置一样，则不是交换物品
	if ( itemFrom == itemTo )
		return;

	if ( pPlayer->IsDying() )
		return;

	if ( pPlayer->IsFaint() )
	{
		D_DEBUG("玩家眩晕，无法交换包裹里的装备\n");
		return;
	}

	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CEquipItemManagerEx::SwapItemOnItemPkg, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_3_Pkgs& itemPkgs  = playerInfo->pkgInfo;//包裹信息
	if ( itemTo >= ARRAY_SIZE(itemPkgs.pkgs) )
	{
		D_ERROR( "CEquipItemManagerEx::SwapItemOnItemPkg, itemTo(%d) >= ARRAY_SIZE(itemPkgs.pkgs)\n", itemTo);
		return;
	}
	if ( itemFrom >= ARRAY_SIZE(itemPkgs.pkgs) )
	{
		D_ERROR( "CEquipItemManagerEx::SwapItemOnItemPkg, itemFrom(%d) >= ARRAY_SIZE(itemPkgs.pkgs)\n", itemFrom);
		return;
	}
	ItemInfo_i pkgItemTo   = itemPkgs.pkgs[itemTo];
	ItemInfo_i pkgItemFrom = itemPkgs.pkgs[itemFrom];


	bool isCombind = false;

	if ( pkgItemTo.uiID != pkgItemFrom.uiID )
	{
		if ( pkgItemTo.usItemTypeID == pkgItemFrom.usItemTypeID //类型一致
			&&
			GET_ITEM_LEVELUP( pkgItemTo.ucLevelUp ) == GET_ITEM_LEVELUP( pkgItemFrom.ucLevelUp )//等级一致
			&&
			//去除追加，原位置用于随机集 pkgItemTo.randSet/*usAddId*/ == pkgItemFrom.randSet/*usAddId*///追加一致
			pkgItemTo.randSet/*usAddId*/ == pkgItemFrom.randSet/*usAddId*///随机集一致
			&&
			pkgItemTo.usWearPoint == pkgItemFrom.usWearPoint//耐久度一致
			&&
			pkgItemTo.excellentid == pkgItemFrom.excellentid //卓越属性一致
			&& 
			pkgItemTo.luckyid  == pkgItemFrom.luckyid //幸运属性一致
			)
		{
			isCombind = CombindItem( pPlayer, pkgItemFrom, itemFrom, pkgItemTo, itemTo );
		}

		if( !isCombind ) 
		{
			unsigned int leftUpItemTo = 0, leftUpItemFrom = 0;
			pPlayer->GetItemInfoByPkgIndex(itemTo,pkgItemTo,leftUpItemTo);
			pPlayer->GetItemInfoByPkgIndex(itemFrom,pkgItemFrom,leftUpItemFrom);
			CBaseItem* pFromItem = pPlayer->GetItemDelegate().GetPkgItem( pkgItemFrom.uiID );
			if ( !pFromItem )
			{
				D_ERROR("玩家%s交换道具，但交换的物品在包裹管理器中找不到%d\n",pPlayer->GetNickName(),pkgItemFrom.uiID );
				return;
			}

			CBaseItem* pToItem = pPlayer->GetItemDelegate().GetPkgItem( pkgItemTo.uiID );

			//ItemInfo_i tmpItem = pkgItemTo;
			//pkgItemTo = pkgItemFrom;
			//pkgItemFrom = tmpItem;
			
			//验证包裹是否有空间交换两道具并交换
			if ( NULL != pToItem )
			{//交换两道具
				pPlayer->ClearPkgItem(pkgItemTo.uiID);
				pPlayer->ClearPkgItem(pkgItemFrom.uiID);
				if (pPlayer->IsFreePkgIndex(itemTo,pFromItem->GetWidth(),pFromItem->GetHeight()))
				{
					pPlayer->SetPkgItem(pkgItemFrom,itemTo);
					if (pPlayer->IsFreePkgIndex(itemFrom,pToItem->GetWidth(),pToItem->GetHeight()))
					{
						pPlayer->SetPkgItem(pkgItemTo,itemFrom);
					}
					else
					{
						pPlayer->ClearPkgItem(pkgItemFrom.uiID);
						pPlayer->SetPkgItem(pkgItemFrom,leftUpItemFrom);
						pPlayer->SetPkgItem(pkgItemTo,leftUpItemTo);
						pPlayer->SendSystemChat("没有足够空间交换道具");
						return;
					}
				}
				else
				{
					pPlayer->SetPkgItem(pkgItemFrom,leftUpItemFrom);
					pPlayer->SetPkgItem(pkgItemTo,leftUpItemTo);
					pPlayer->SendSystemChat("没有足够空间交换道具");
					return;
				}
			}
			else
			{//移到空位
				pPlayer->ClearPkgItem(pkgItemFrom.uiID);
				if (pPlayer->IsFreePkgIndex(itemTo,pFromItem->GetWidth(),pFromItem->GetHeight()))
				{
					pPlayer->SetPkgItem(pkgItemFrom,itemTo);
				}
				else
				{
					pPlayer->SetPkgItem(pkgItemFrom,leftUpItemFrom);
					pPlayer->SendSystemChat("没有足够空间移动道具");
					return;
				}
			}

			pPlayer->NoticePkgItemChange( itemFrom );
			pPlayer->NoticePkgItemChange( itemTo );
		}
	}
	else
	{
		CBaseItem* pFromItem = pPlayer->GetItemDelegate().GetPkgItem( pkgItemFrom.uiID );
		if ( !pFromItem )
		{
			D_ERROR("玩家%s交换道具，但交换的物品在包裹管理器中找不到%d\n",pPlayer->GetNickName(),pkgItemFrom.uiID );
			return;
		}
		unsigned int leftUpItemFrom = 0;
		pPlayer->GetItemInfoByPkgIndex(itemFrom,pkgItemFrom,leftUpItemFrom);
		pPlayer->ClearPkgItem(pkgItemFrom.uiID);
		if (pPlayer->IsFreePkgIndex(itemTo,pFromItem->GetWidth(),pFromItem->GetHeight()))
		{
			pPlayer->SetPkgItem(pkgItemFrom,itemTo);
		}
		else
		{
			pPlayer->SetPkgItem(pkgItemFrom,leftUpItemFrom);
			pPlayer->SendSystemChat("没有足够空间移动道具");
			return;
		}
		pPlayer->NoticePkgItemChange( itemFrom );
		pPlayer->NoticePkgItemChange( itemTo );
	}
}

bool CEquipItemManagerEx::CombindItem( CPlayer* pPlayer, ItemInfo_i& itemFrom, unsigned int from, ItemInfo_i& itemTo, unsigned int To )
{
	if ( !pPlayer )
		return false;

	if ( pPlayer->IsDying() )
		return false;

	if ( pPlayer->IsFaint() )
	{
		D_DEBUG("玩家眩晕，无法合并物品\n");
		return false;
	}

	bool isCanCombind = false;
	if ( from>= NORMAL_PKG_SIZE && To >= NORMAL_PKG_SIZE )
	{
		isCanCombind = true;
	}

	if ( from < NORMAL_PKG_SIZE && To < NORMAL_PKG_SIZE )
	{
		isCanCombind = true;
	}

	if ( !isCanCombind )
	{
		D_ERROR("玩家%s :不同属性背包中的物品不能交换%d To %d\n",pPlayer->GetNickName(), from,To );
		return false;
	}

	CBaseItem* pToItem     = pPlayer->GetItemDelegate().GetPkgItem( itemTo.uiID );
	if ( !pToItem )
	{
		D_ERROR("玩家%s交换道具，但交换的物品在包裹管理器中找不到%d\n",pPlayer->GetNickName(),itemTo.uiID );
		return false;
	}

	CBaseItem* pFromItem   = pPlayer->GetItemDelegate().GetPkgItem( itemFrom.uiID );
	if ( !pFromItem )
	{
		D_ERROR("玩家%s交换道具，但交换的物品在包裹管理器中找不到%d\n",pPlayer->GetNickName(),itemTo.uiID );
		return false;
	}

	//如果达到了最大值
	unsigned maxCount = pToItem->GetMaxItemCount();
	if ( (unsigned int)itemTo.ucCount == maxCount  )
		return false;

	unsigned int lostCount = maxCount - itemTo.ucCount;
	if ( lostCount > 0 )
	{
		if ( lostCount >= (unsigned int)itemFrom.ucCount )
		{
			itemTo.ucCount += itemFrom.ucCount;
			//丢弃原来的道具
			pPlayer->OnDropItem( itemFrom.uiID );

			//更新新的道具个数
			pPlayer->NoticePkgItemChange( To );
		
		}
		else
		{
			//更新原位置的道具信息
			itemFrom.ucCount -= lostCount;
			pPlayer->NoticePkgItemChange( from );
			
			itemTo.ucCount  += lostCount;
			pPlayer->NoticePkgItemChange( To );
		}
		return true;
	}

	return false;
}

int CEquipItemManagerEx::ChanageSuit( CPlayer *pPlayer, BYTE suitIndex, bool forceRepair)
{
	//暂时先不处理一键换装
	return -1;
	//1.参数有效性
	//2.当前状态是否可以换装
	//3.确定套装包含的道具集及有效性(判断是否有足够空间用来交换，交换的装备是否有效)
	//4.执行装备或拿下
	
	
	//1.参数有效性
	if((NULL == pPlayer) || (suitIndex >= MAX_SUIT_COUNT))
		return -1;

	//2.当前状态是否可以换装(死亡/晕眩/骑乘)
	if ( pPlayer->IsDying() )
	{
		pPlayer->SendSystemChat("玩家处于死亡状态");
		return -1;
	}

	if( pPlayer->IsFaint() )
	{	
		pPlayer->SendSystemChat("玩家处于晕眩状态");
		return -1;
	}

	if( NULL != pPlayer->TradeInfo())
	{
		pPlayer->SendSystemChat("玩家处于交易状态");
		return -1;
	}

	//3.确定套装包含的道具集及有效性
	SuitInfo_i *pSuit = pPlayer->Suit(suitIndex);
	if(NULL == pSuit)
	{
		D_ERROR( "ChanageSuit, NULL == pSuit\n" );
		return -1;
	}

	//3.1确定要装备的道具的数目,判断每个道具的有效性
	bool isFull = true;
	SuitInfo_i tempSuit = *pSuit;//备份
	CBaseItem *pBases[EQUIP_SIZE] = {0};
	int newEquipCount = pPlayer->IsValidSuit(&tempSuit, &pBases[0], isFull);
	if(newEquipCount == -1)
	{
		pPlayer->SendSystemChat("一键换装时要换上的部分装备无效\n");
		return -1;
	}

	if(!isFull)
	{
		if(!forceRepair)
			return 1;
		else
		{
			*pSuit = tempSuit;

			MGQuerySuitInfoRst rst;
			rst.index = suitIndex;
			rst.ret = pPlayer->Suit(suitIndex, rst.suit) ? 0 : -1;

			pPlayer->SendPkg<MGQuerySuitInfoRst>( &rst );
		}
	}

	//3.2确定要拿下的的道具的数目,判断每个道具的有效性
	int oldEquipedCount = 0;
	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CEquipItemManagerEx::ChangeSuit, NULL == GetFullPlayerInfo()\n");
		return -1;
	}
	PlayerInfo_3_Pkgs& packages  = playerInfo->pkgInfo;//包裹信息
	PlayerInfo_2_Equips& equips  = playerInfo->equipInfo;//装备信息
	for(int i = 0; i < ARRAY_SIZE(equips.equips); i++)
	{
		if(0 != equips.equips[i].uiID)
		{
			CBaseItem *tempBase = pPlayer->GetItemDelegate().GetEquipItem( equips.equips[i].uiID);
			if(NULL == tempBase)
			{
				pPlayer->SendSystemChat("一键换装时要卸下的部分装备没有详细信息\n");
				return -1;
			}

			oldEquipedCount++;
		}
	}

	//3.3判断是否有足够的空间放置拿下的装备
	if(newEquipCount < oldEquipedCount)
	{
		int freeSpace = 0;
		for(int i = 0; i < NORMAL_PKG_SIZE; i++)
		{
			if ( i >= ARRAY_SIZE(packages.pkgs) )
			{
				D_ERROR( "ChanageSuit, i >= ARRAY_SIZE(packages.pkgs)\n" );
				break;
			}
			if(0 == packages.pkgs[i].uiID)
			{
				freeSpace++;
			}
		}

		if(freeSpace + newEquipCount < oldEquipedCount)
		{
			pPlayer->SendSystemChat("一键换装时包裹中空间不足\n");
			return -1;
		}
	}

	//4.执行装备或拿下(先执行换上留出空间，稍后卸下的物品)
	for(int i = 0; i < ARRAY_SIZE(equips.equips); i++)
	{
		if ( i >= ARRAY_SIZE(pSuit->equips) )
		{
			D_ERROR( "一键换装, i >= ARRAY_SIZE(pSuit->equips)\n" );
			break;
		}
		if((0 != pSuit->equips[i]) && (pSuit->equips[i] != equips.equips[i].uiID))
		{
			for(int j = 0; j < NORMAL_PKG_SIZE; j++)
			{
				if ( j >= ARRAY_SIZE(packages.pkgs) )
				{
					D_ERROR( "一键换装, j >= ARRAY_SIZE(packages.pkgs)\n" );
					break;
				}
				if(pSuit->equips[i] == packages.pkgs[j].uiID)
				{
					ItemInfo_i temp = packages.pkgs[j];
					if ( !UseEquipRule::Execute( pPlayer, pBases[i], j, i ) )
					{
						D_ERROR( "ChanageSuit, 玩家%s将装备%d(类型%d)从包裹%d装备至位置%d失败\n", pPlayer->GetAccount(), temp.uiID, temp.usItemTypeID, j, i );
						break;
					} else {
						OnUseItemResult( pPlayer, temp );					
						break;
					}
				}
			}
		}
	}

	for(int i = 0; i < ARRAY_SIZE(equips.equips); i++)
	{
		if ( i >= ARRAY_SIZE(pSuit->equips) )
		{
			D_ERROR( "一键换装2, i >= ARRAY_SIZE(pSuit->equips)\n" );
			break;
		}
		if((0 == pSuit->equips[i]) && (0 != equips.equips[i].uiID))
		{
			for(int j = 0; j <NORMAL_PKG_SIZE; j++)
			{
				if ( j >= ARRAY_SIZE(packages.pkgs) )
				{
					D_ERROR( "一键换装2, j >= ARRAY_SIZE(packages.pkgs)\n" );
					break;
				}
				if(0 == packages.pkgs[j].uiID)
				{
					EquipItemManagerSingle::instance()->UnUseItem( pPlayer ,0, i, j );
					break;
				}
			}
		}
	}

	return 0;
}
