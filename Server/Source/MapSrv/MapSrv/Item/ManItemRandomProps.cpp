﻿
#include "ManItemRandomProps.h"

HASH_MAP<unsigned int, CRandSet> CManItemRandSets::m_randSets;//所有的道具随机属性集；

bool CRandSet::RandSetGetProps( GenedProps& outProps )
{
	unsigned int propNum = m_minPropNum + CRandGen::GetRand()%(m_maxPropNum-m_minPropNum+1);//应随出的属性数量
	if ( propNum >= m_arrPropNum ) //全部属性都要
	{		
		propNum = m_arrPropNum;//最多不能超过本集的有效属性数；
	}

	unsigned int randval = 0;
	unsigned int tmpWeight = GetWeightSum();//初始为总权值
	unsigned int curSum = 0;
	bool validcondi[RANDPROP_MAXNUM];//可供选取/已选取的属性；
	StructMemSet( validcondi, false, sizeof(validcondi) );
	for ( unsigned int i=0; i<propNum; ++i )
	{
		randval = CRandGen::GetRand() % tmpWeight;
		curSum = 0;
		for ( unsigned int j=0; j<m_arrPropNum; ++j )
		{
			if ( ( j >= ARRAY_SIZE(m_arrProps) )
				|| ( j >= ARRAY_SIZE(validcondi) )
				)
			{
				D_ERROR( "CRandSet::RandSetGetProps, j(%d) >= ARRAY_SIZE(m_arrProps) || ARRAY_SIZE(validcondi)\n", j );
				break;
			}

			if ( !validcondi[j] )
			{
				curSum += m_arrProps[j].GetWeight();
				if ( randval < curSum )
				{
					//选中此属性
					outProps.AddProp( m_arrProps[j].GetRPropID(), m_arrProps[j].GetRandVal() );
					validcondi[j] = true;
					tmpWeight -= m_arrProps[j].GetWeight();
					break;
				}//if ( randval < curSum )
			}//if ( !validcondi[j] )，排除已选取属性;
		}//for ( int j=0; j<m_arrPropNum; ++j )，找到对应randval的随机属性
		//随下一属性；
	}

	return true;
}// bool CRandSet::RandSetGetProps( unsigned int outPropNum, GenedProps& outProps )

//输入生成信息，得到对应的随机属性以及随机值
bool CManItemRandSets::GetRandProps( bool luckyOrExcel/*true:幸运,false:卓越*/, unsigned int orgInfo, unsigned int randSetID, GenedProps& outProps )
{
	unsigned int setID  = luckyOrExcel? (randSetID&0xffff) : (randSetID&(0xffff<<16))>>16;//取真正的幸运或卓越随机集号；
	if ( m_randSets.end() == m_randSets.find(setID) )
	{
		D_ERROR( "CManItemRandSets::GetRandProps，无效的随机集号%d\n", setID );
		return false;
	}

	CRandGen::SetSeed( orgInfo );
	return m_randSets[setID].RandSetGetProps( outProps );
}
