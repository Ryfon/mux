﻿/********************************************************************
	created:	2009/06/03
	created:	3:6:2009   9:30
	file:		ProtectItemManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/


#ifndef PROTECT_ITEM_MANAGER_H
#define PROTECT_ITEM_MANAGER_H

#include "CSpecialItemProp.h"
#include <vector>

#define  MAX_SPECIALPROTECT_ITEM_COUNT 6

class CProtectItemManager
{
public:

	CProtectItemManager():m_pAttachPlayer(NULL),specialProtectWaitItemCount(0){}


	~CProtectItemManager(){}

public:

	void AttachPlayer( CPlayer* pPlayer );

	//接受玩家的保护道具的详细信息
	void RecvPlayerProtectItem( const ProtectItem* protectItemArr , unsigned int pageIndex, bool isEnd );

	void UpdatePlayerProtectItemToDB();

	//将玩家的保护道具信息发送给客户端
	void NoticeProtectItemDetialInfoToPlayer();

	//查找道具是否为保护道具
	ProtectItem* FindItemIsProtected( unsigned int itemUID );

	//查找道具的保护种类
	PROTECTED_ITEM_TYPE GetItemProtectedType( unsigned int itemUID );

	//释放道具的特殊保护功能
	void ReleaseSpecialItemProtect( unsigned int itemUID );

	//取消进入特殊保护的等待时间
	void CancelItemUnSpecialProtectWaitTime( unsigned int itemUID );

	//取消道具的特殊保护,进入特殊保护时间
	void CancelItemSpecialProtect( unsigned int itemUID );

	//通知保护道具的属性改变了
	void NoticeItemProtectPropUpdate( const ProtectItem&  protectItem , UPDATE_PROTECTITEM_PROP opType  );

	//放入新的保护道具
	int  PushNewProtectItem( const ProtectItem& newProtectItem );
 
	bool IsReachMaxUnSpecialItemWaitCount() { return specialProtectWaitItemCount >= MAX_SPECIALPROTECT_ITEM_COUNT ; }

	//从保护道具中删除
	void RemoveProtectItem( unsigned int itemUID );

	int  GetSpecialProtectItemCount() { return specialProtectWaitItemCount ; }

	//是否有保护道具
	bool IsHaveProtectItem() { return !mProtectItemVec.empty(); }

	//获取保护道具的列表
	const std::vector<ProtectItem>& GetProtectItemVec() { return mProtectItemVec ;}

private:
	CPlayer* m_pAttachPlayer;
	std::vector<ProtectItem> mProtectItemVec;
	unsigned int specialProtectWaitItemCount;
};

#endif
