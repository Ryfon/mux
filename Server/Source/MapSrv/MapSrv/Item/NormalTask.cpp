﻿/**
* @file NormalTask.cpp
* @brief 一般任务
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: NormalTask.cpp
* 摘    要: 任务定义
* 作    者: dzj
* 完成日期: 20008.06.24
*
*/

#include "NormalTask.h"
#include "../Player/Player.h"
#include "../Union/UnionManager.h"

map< unsigned int, NormalTaskProp* > CNormalTaskPropManager::m_mapTaskProp;//各种类型的任务属性；

///初始化计数器管理器，读入各计数器属性；
bool CNormalTaskPropManager::Init() 
{ 
	Release();

	TRY_BEGIN;

	// 装载
	CXmlManager xmlManager;
	if( !xmlManager.Load("config/task/task.xml") )
	{
		D_ERROR("任务配置信息加载出错\n");
		return false;
	}

	// 根元素
	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("任务配置信息可能为空\n");
		return false;
	}

	// 根元素的所有子元素
	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);

	//<task TaskID="10001"/>
	int taskID = 0;

	CElement* pElement = NULL;
	int curpos = 0;
	for ( vector<CElement *>::iterator iter=childElements.begin(); childElements.end()!=iter; ++iter )
	{
		curpos = (int) ( iter -childElements.begin() );
		pElement = *iter;
		if(pElement->Level() == 1)
		{
			if ( !IsXMLValValid( pElement, "ID", curpos ) )
			{
				return false;
			}	
			taskID = atoi( pElement->GetAttr( "ID" ) );
			if ( taskID < 0 )
			{
				D_ERROR("任务的编号为负 %d\n",taskID );
				return false;
			}

			bool bcanDrop  = ( strcmp( pElement->GetAttr("GiveUp"),"False" )!=0 );
			bool bcanShare = ( strcmp( pElement->GetAttr("Share"),"False" )!=0 );
			unsigned int eTaskType = (unsigned int)atoi( pElement->GetAttr("Class") );
			unsigned int reserver =  (unsigned int)atoi( pElement->GetAttr("RecordReserve") );
			unsigned int level = (unsigned int)atoi( pElement->GetAttr("Level"));

			unsigned int TimeLimit = 0;
			if ( pElement->GetAttr("TimeLimit") )
			{
				 TimeLimit = ( unsigned int)atoi( pElement->GetAttr("TimeLimit") );//完成任务的时间限制
			}
		
			int targetBegin = -1;
			int targetEnd = -1;
			int targetCount = 0;
			COUNTER_TARGET_TYPE bCounterType  = CT_INVALID;

			int subType = atoi( pElement->GetAttr("IntSubTaskType") );
			//if ( subType > 0 &&  subType < 6 )
			//{
			//	bCounterType = ( COUNTER_TARGET_TYPE )subType;
			//	//if ( bCounterType == CT_PK )//暂时先这么做,8.5号以后在修改
			//	//	bCounterType = CT_COLLECT;
			//}

			bCounterType = ( COUNTER_TARGET_TYPE )subType;

			//任务能否被重复
			bool isRepeat = ( strcmp( pElement->GetAttr("Repeat"),"False" )!=0 );

			//如果是计数器类的任务
			//if ( bCounterType == CT_NPC_KILL )
			{
				targetBegin = atoi( pElement->GetAttr("TargetBegin") );
				targetEnd   = atoi( pElement->GetAttr("TargetEnd") );
				if ( targetBegin > -1 && targetEnd > -1 )
				{
					targetCount =  atoi( pElement->GetAttr("TargetValue") );
				}
			}

			if ( NULL == FindTaskProp( taskID ) ) //之前没有该任务属性;
			{
				//读到了一个完整的任务属性；
				NormalTaskProp* newTaskProp = NEW NormalTaskProp;
				newTaskProp->Init( taskID ,eTaskType, bcanDrop, bcanShare, reserver, level , bCounterType ,targetEnd, targetBegin , targetCount, TimeLimit ,isRepeat );
				m_mapTaskProp.insert( pair<unsigned int, NormalTaskProp*>( taskID, newTaskProp ) );
			}

		}//if(pElement->Level() == 1)
	}//for ( vector<CElement *>::iterator iter=childElements.begin()...

	return true; 
	TRY_END;
	return false;
};

void LimitTimeTaskManager::MonitorLimitTimeTask()
{
	TRY_BEGIN;

	if ( m_LimitTimeTaskVec.empty() )
		return;

	time_t m_currentTime = time(NULL);
	//如果大于3秒
	if ( m_currentTime - m_TaskCheckPoint > 3 )
	{
		std::vector<CNormalTask *>::iterator lter = m_LimitTimeTaskVec.begin();
		while ( lter!= m_LimitTimeTaskVec.end() )
		{
			CNormalTask * pLimitTimeTask = *lter;
			if ( pLimitTimeTask == NULL )
			{
				++lter;
				continue;
			}

			CPlayer* pAttachPlayer = pLimitTimeTask->GetTaskAttachPlayer();
			if ( pAttachPlayer == NULL )
			{
				++lter;
				D_ERROR("任务对象所依附的玩家指针为NULL\n");
				continue;
			}

			D_DEBUG("开始遍历玩家%s的限制时间的任务\n",pAttachPlayer->GetNickName() );

			NormalTaskProp* pProp = pLimitTimeTask->GetTaskProp();
			if ( !pProp )
			{
				++lter;
				D_ERROR("获取不了任务的属性\n");
				continue;
			}

			unsigned int taskLimitTime = (unsigned int)pLimitTimeTask->GetTaskStartTime();
			if ( (unsigned int)m_currentTime > taskLimitTime )
			{
				pAttachPlayer->SetTaskStat( pLimitTimeTask->GetTaskID(),NTS_FAIL );
				lter = m_LimitTimeTaskVec.erase( lter );

				D_DEBUG("玩家%s限时任务%d过期\n", pAttachPlayer->GetNickName(), pLimitTimeTask->GetTaskID() );
			}
			else
			{
				D_DEBUG("玩家%s 的记时任务%d 将要在%d 秒后结束\n",pAttachPlayer->GetNickName(),
					pLimitTimeTask->GetTaskID(),
					(taskLimitTime - m_currentTime)
					);

				++lter;
			}
		}

		m_TaskCheckPoint = m_currentTime;
	}

	TRY_END;
}


void LimitTimeTaskManager::OnPlayerLeaveSrv(CPlayer* pLeavePlayer )
{
	TRY_BEGIN;

	if ( !pLeavePlayer )
		return;

	if ( m_LimitTimeTaskVec.empty() )
		return;

	std::vector<CNormalTask *>::iterator lter = m_LimitTimeTaskVec.begin();
	while( lter!= m_LimitTimeTaskVec.end() )
	{
		CNormalTask* pNormal = *lter;
		if ( pNormal )
		{
			CPlayer* pPlayer = pNormal->GetTaskAttachPlayer();
			//if ( pPlayer  && pPlayer->GetFullID() == pLeavePlayer->GetFullID() )
			if ( pPlayer && pPlayer->GetDBUID() == pLeavePlayer->GetDBUID() )
			{
				D_DEBUG("删除玩家%s 的计时任务 %d\n", pPlayer->GetNickName(),pNormal->GetTaskID()  );
				lter = m_LimitTimeTaskVec.erase( lter );
				continue;
			}
		}

		++lter;
	}

	TRY_END;
}

void LimitTimeTaskManager::RemoveLimitTimeTask(CPlayer *pFinishPlayer, unsigned int taskID)
{
	TRY_BEGIN;

	if ( !pFinishPlayer )
		return;

	if ( m_LimitTimeTaskVec.empty() )
	{
		//D_ERROR("玩家%s有计时任务，但在计时任务管理器中的容器为空\n", pFinishPlayer->GetNickName() );
		return;
	}

	std::vector<CNormalTask *>::iterator lter = m_LimitTimeTaskVec.begin();
	while( lter!= m_LimitTimeTaskVec.end() )
	{
		CNormalTask* pNormal = *lter;
		if ( pNormal && ( pNormal->GetTaskID() == taskID ) )
		{
			CPlayer* pPlayer = pNormal->GetTaskAttachPlayer();
			//删除记时任务
			//if ( pPlayer  && pPlayer->GetFullID() == pFinishPlayer->GetFullID() )
			if ( pPlayer && pPlayer->GetDBUID() == pFinishPlayer->GetDBUID() )
			{
				D_DEBUG("删除玩家%s 的计时任务 %d\n", pPlayer->GetNickName(),taskID  );
				lter = m_LimitTimeTaskVec.erase( lter );
				break;
			}
		}

		++lter;
	}


	TRY_END;
}

void CNormalTask::CheckIsLimitTimeTask()
{
	if ( !m_ptaskAttachPlayer )
		return;

	//如果是限时任务
	if ( IsLimitTimeTask() )
	{
		if( m_pTaskProp )
		{
			if(!m_pTaskProp->IsUnionTask())
			{
				//获取规定完成任务的时间
				m_taskStartTime = time(NULL) + m_pTaskProp->GetTaskLimitTime();
			}
			else
			{
				//工会任务的时间和任务发布时间有关
				CUnion *pUnion = m_ptaskAttachPlayer->Union();
				if(NULL != pUnion)
				{
					m_taskStartTime = pUnion->GetTaskEndTime();
				}
			}

			//插入监视队列
			CLimitTimeTaskManagerSingle::instance()->PushLimitTimeTask( this );
		}
	}
}

