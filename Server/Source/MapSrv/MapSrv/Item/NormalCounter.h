﻿/**
* @file NormalCounter.h
* @brief 一般计数器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: NormalCounter.h
* 摘    要: 计数器声明
* 作    者: dzj
* 完成日期: 20008.06.24
*
*/

#pragma once

#include "../Lua/NpcChatScript.h"
#include <map>
#include <vector>

using namespace std;

///计数器类型;
enum COUNTER_TARGET_TYPE
{
	CT_NPC_KILL = 0x1, /*目标：杀怪计数*/
	CT_COLLECT,      /*目标：收集计数*/
	CT_PK     ,      /*目标：杀人计数*/
	CT_FRIEND ,      /*目标：交友计数*/
	CT_TEAM_SHARE,	 /*组队任务共享型计数 此计数跟随CGroupTeam*/
	CT_ANSWER ,      /*目标:答题计数*/
	CT_USEITEM,      /*目标:使用物品计数*/
	CT_INVALID       /*无效计数器*/
};

///计数器属性;
struct NormalCounterProp
{
	friend class CNormalCounter;
	friend class MapCounterInfo;

public:
	NormalCounterProp() : m_pCounterScript(NULL) {};
	~NormalCounterProp() {};

	void Init( unsigned int counterIdType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount, unsigned int taskID )
	{
		m_counterType  = (COUNTER_TARGET_TYPE) counterIdType;
		m_counterIdMin = counterIdMin;
		m_counterIdMax = counterIdMax;
		m_targetCount  = targetCount;
		m_taskID       = taskID;
	}

	unsigned int GetTargetCount() 
	{
		return m_targetCount;
	}

	unsigned int GetCoresTaskID()
	{
		return m_taskID;
	}

	unsigned int GetCounterIDMin() { return m_counterIdMin; }

	unsigned int GetCounterIDMax() { return m_counterIdMax; }

private:
	inline bool IsAccord( COUNTER_TARGET_TYPE inType, unsigned int inObjID )
	{
		if ( inType == CT_PK )//如果杀人计数
		{
			if ( m_counterIdMax == 0 )
				return true;
			else
				return inObjID == m_counterIdMax;
		}
		else if( inType == CT_FRIEND )//如果交友计数
		{
			if ( m_counterIdMax == 0 )//如果所有性别的
			{
				return true;
			}
			else if( m_counterIdMax == 3 )//如果是只能算异性
			{
				unsigned int sexinfo = inObjID & 0xFF;
				if ( sexinfo == 1)
					return true;
				else 
					return false;
			}
			else
			{
				unsigned int sexinfo = inObjID>>16;
				return sexinfo == m_counterIdMax;
			}
		}

		return ( ( inType == m_counterType ) && ( inObjID <= m_counterIdMax ) && ( inObjID >= m_counterIdMin ) );
	}

	NormalCounterProp& operator=( const NormalCounterProp& );
	NormalCounterProp( const NormalCounterProp& );
	/////计数器到达时调用脚本;
	//bool CounterMeetCallScript( CPlayer* pOwner /*计数器拥有者*/);

private:
	unsigned int          m_counterIdMin;//计数器最小标识号；
	unsigned int          m_counterIdMax;//计数器最大标识号；
	COUNTER_TARGET_TYPE   m_counterType;//计数器目标事件类型；
	unsigned int          m_targetCount;//目标计数值；
	CNormalScript*        m_pCounterScript;//计数器所挂脚本；
	unsigned int          m_taskID;//计数器对应的任务号；
};

/////计数器属性管理器；
//class CNormalCounterPropManager
//{
//private:
//	CNormalCounterPropManager(){};
//	~CNormalCounterPropManager(){};
//
//private:
//	///释放内存，清空map;
//	static void Clear()
//	{
//		for ( map< unsigned int, NormalCounterProp* >::iterator iter=m_mapCounterProp.begin(); iter!=m_mapCounterProp.end(); ++iter )
//		{
//			if ( NULL != iter->second )
//			{
//				delete iter->second; iter->second = NULL;
//			}
//		}
//		m_mapCounterProp.clear();
//	}
//
//public:
//	///管理器释放；
//	static bool Release()
//	{
//		Clear();
//		return true;
//	}
//
//	///初始化计数器属性管理器，读入各计数器属性；
//	static bool Init();
//
//	///寻找指定ID号的计数器属性；
//	static NormalCounterProp* FindCounterProp( unsigned int counterID )
//	{
//		map< unsigned int, NormalCounterProp* >::iterator iter = m_mapCounterProp.find( counterID );
//		if ( iter != m_mapCounterProp.end() )
//		{
//			return iter->second;
//		}
//
//		return NULL;
//	};
//
//private:
//	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
//	{
//		if ( NULL == pEle->GetAttr( attName ) )
//		{
//			D_ERROR( "读计数器XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
//			return false;
//		}
//		return true;
//	}
//
//private:
//	static map< unsigned int, NormalCounterProp* > m_mapCounterProp;//各种类型的计数器属性；
//};

///计数器;
class CNormalCounter
{
public:
	CNormalCounter() {};
	~CNormalCounter() {};

private:
	CNormalCounter( const CNormalCounter& );
	CNormalCounter& operator=( const CNormalCounter & );

public:
	///构造指定类型与目标计数的计数器；
	bool Init( unsigned int counterIdType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount, unsigned int taskID )
	{
		m_curCount     = 0;
		if ( counterIdType > CT_INVALID )
			return false;

		m_CounterProp.Init( counterIdType, counterIdMin, counterIdMax, targetCount, taskID );// = CNormalCounterPropManager::FindCounterProp( counterID );
		return true;
	}

	COUNTER_TARGET_TYPE GetCounterType()
	{
		return m_CounterProp.m_counterType;
	}

	void SetCurCount( unsigned int count )
	{
		if ( count <= m_CounterProp.GetTargetCount() )
			m_curCount = count;
	}

	unsigned int GetOwnerTaskID()
	{
		return m_CounterProp.GetCoresTaskID();
	}

	//记录计数器是否达到了计数器的目标
	bool CheckCounterIsFinish( CPlayer* pOwner );

	//接任务时检查道具包中是否已经有这样的道具了
	bool CheckItemPkgCounterNum(  CPlayer* pOwner );

	//是否达到了最大计数
	bool IsReachMaxCounter() {  return m_curCount >= m_CounterProp.GetTargetCount(); } 

	//刷新计数器的当前数目
	void RefreshCounterNum( CPlayer* pOwner , unsigned int taskID );

	//获取当前计数器数目
	unsigned int GetCurCounterCount() { return m_curCount; }

	//获取计数器的目标数目
	unsigned int GetCounterTargetCount() { return m_CounterProp.GetTargetCount(); }

	//获取计数器的范围
	void GetCounterRange( unsigned int& rangestart, unsigned int& rangeend ){  rangestart = m_CounterProp.GetCounterIDMin(); rangeend = m_CounterProp.GetCounterIDMax(); }

public:
	///事件发生时try inc counter，影响因素:事件类型，事件相关参数(例如杀怪类型，数量，收集物类型，收集物数量);
	///    如果本计数器对输入事件发生响应(输入事件与本计数器类型一致)，函数返回true，否则返回false;
	bool OnCounterEvent( CPlayer* pOwner, COUNTER_TARGET_TYPE ctType/*发生的事件类型*/, unsigned int reasonObjID/*事件相关物体类型*/, unsigned int reasonObjNum/*事件相关物体数量*/ );

	//当丢弃道具时触发收集器变化
	bool OnDropItemCounterEvent( CPlayer* pOwner, COUNTER_TARGET_TYPE ctType/*发生的事件类型*/, unsigned int reasonObjID/*事件相关物体类型*/, unsigned int reasonObjNum/*事件相关物体数量*/ );
private:
	unsigned int        m_curCount;//当前计数;
	NormalCounterProp   m_CounterProp;//计数器属性;
};
