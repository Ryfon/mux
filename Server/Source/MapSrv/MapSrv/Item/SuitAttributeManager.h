﻿/********************************************************************
	created:	2008/08/14
	created:	14:8:2008   11:05
	file:		SuitAttributeManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef SUITATTRIBUTEMANAGER_H
#define SUITATTRIBUTEMANAGER_H


#include <map>
#include "../../../Base/PkgProc/CliProtocol_T.h"
#include "../../../Base/PkgProc/CliProtocol.h"

#include "../../../Base/aceall.h"

class CElement;
class CPlayer;

//套装
class CSuit
{
public:
	explicit CSuit( CElement* pxmlElement );

	~CSuit(){};

public:
	//是否达到了套装的要求
	//bool IsReachSuitCondition( CPlayer * pPlayer );

	//如果达到要求，把套装所增加的属性给玩家加上
	void AddSuitAttributeToPlayer( CPlayer * pPlayer );

	//将该套装的所有属性减掉，因为玩家不是一个完整的套装了
	void SubSuitAttributeToPlayer( CPlayer* pPlayer );

	unsigned int GetSuitID() { return suitID; }

	int GetSuitEquipID() { return suitEquipID; }

private:
	//std::vector<unsigned int> suitEquipIndexVec;//满足套装装备条件的数组
	int suitEquipID;
	ItemAddtionProp mSuitAddtionProp;
	unsigned int suitID;
};

class CSuitManager
{
	friend class ACE_Singleton<CSuitManager, ACE_Null_Mutex>;	

private:
	CSuitManager( const CSuitManager& );  //屏蔽这两个操作；
	CSuitManager& operator = ( const CSuitManager& );//屏蔽这两个操作；

public:
	CSuitManager(){}

	~CSuitManager();

public:
	//加载XML文件
	void LoadXML( const char* pxmlFile );
	//将套装加入管理器
	void PuchSuitToManager( CElement* pElement );
	//检查玩家的套装的状态
	//void CheckPlayerSuitState( CPlayer* pPlayer );
	//取得套装
	CSuit* GetSuit( unsigned int suitID );
private:
	std::map<unsigned int, CSuit *> mSuitManager;

};

typedef ACE_Singleton<CSuitManager, ACE_Null_Mutex> CSuitManagerSingle;

#endif
