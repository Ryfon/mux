﻿/**
* @file NormalTask.h
* @brief 一般任务
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: NormalTask.h
* 摘    要: 一般任务声明
* 作    者: dzj
* 完成日期: 20008.06.25
*
*/

#pragma once

#include "XmlManager.h"
#include "Utility.h"
#include "NormalCounter.h"
#include "../../../Base/PkgProc/CliProtocol_T.h"

#include <time.h>
#include <vector>

using namespace std;
using namespace MUX_PROTO;

//enum TaskType
//{
//	E_MASTERSTROKE = 0x0,//主线任务
//	E_LATERAL,//支线任务
//	E_EVERYDAY,//每日任务
//	E_DUNGEON,//副本任务
//	E_ACTIVE,//活动任务
//	E_SYSTEM,//系统任务
//	E_FEAST,//节日任务
//	E_WAR,	//攻城任务
//};

///任务属性;
struct NormalTaskProp
{
	friend class CNormalTask;

public:
	NormalTaskProp() {};
	~NormalTaskProp() {};

	void Init( unsigned int taskID ,unsigned int taskType, bool isCanDrop, bool isCanShare, 
		unsigned int RecordReserve, unsigned int level , COUNTER_TARGET_TYPE countType ,
		int targetMaxID,int targetMinID, unsigned int targetCount ,unsigned int LimitTime , bool isRepeat )
	{
		m_taskID = taskID;

		//gcq
		m_eTaskType = taskType;
		m_canDrop = isCanDrop; 
		m_canShare = isCanShare;
		m_RecordReserve = RecordReserve;
		m_needLevel = level;
		m_eCounterType = countType;

		m_targetMaxID = targetMaxID;
		m_targetMinID = targetMinID;
		m_targetCount = targetCount;
		m_LimitTime   = LimitTime;
		m_isCanRepeat = isRepeat;
	}

	//任务的计数器类型
	COUNTER_TARGET_TYPE GetCountType() { return m_eCounterType ;}

	//任务的类型
	unsigned int   GetTaskType() { return m_eTaskType; }

	//如果不等于0时,该任务为限时任务
	bool IsLimitTimeTask() { return m_LimitTime!=0 ; }

	bool IsTaskCanRepeat() { return m_isCanRepeat; }

	//获取任务完成时间
	unsigned int GetTaskLimitTime() { return m_LimitTime; }

	//是否工会任务
	bool IsUnionTask(void){ return m_taskID/1300000 == 1 ? true : false; }
private:
	unsigned int          m_taskID;//任务标识号；

//gcq 
	unsigned int          m_eTaskType;//任务类型
	COUNTER_TARGET_TYPE   m_eCounterType;//计数器类型
	bool                  m_canDrop;//任务是否可被丢弃
	bool                  m_canShare;//任务是否能被分享
	unsigned int          m_RecordReserve;//是否保留记录
	unsigned int          m_needLevel;//所需玩家等级


public:
	int                   m_targetMaxID;
	int                   m_targetMinID;
	unsigned int          m_targetCount;
	unsigned int          m_LimitTime;//完成任务限制的时间
	bool                  m_isCanRepeat;

};

///任务属性管理器；
class CNormalTaskPropManager
{
private:
	CNormalTaskPropManager(){};
	~CNormalTaskPropManager(){};

private:
	///释放内存，清空map;
	static void Clear()
	{
		for ( map< unsigned int, NormalTaskProp* >::iterator iter=m_mapTaskProp.begin(); iter!=m_mapTaskProp.end(); ++iter )
		{
			if ( NULL != iter->second )
			{
				delete iter->second; iter->second = NULL;
			}
		}
		m_mapTaskProp.clear();
	}

public:
    ///管理器释放；
	static bool Release()
	{
		Clear();
		return true;
	}

	///初始化计数器属性管理器，读入各计数器属性；
	static bool Init();

	///寻找指定ID号的计数器属性；
	static NormalTaskProp* FindTaskProp( unsigned int taskID )
	{
		map< unsigned int, NormalTaskProp* >::iterator iter = m_mapTaskProp.find( taskID );
		if ( iter != m_mapTaskProp.end() )
		{
			return iter->second;
		}

		return NULL;
	};

private:
	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
	{
		if ( NULL == pEle || NULL == attName )
		{
			return false;
		}
		if ( NULL == pEle->GetAttr( attName ) )
		{
			D_ERROR( "读任务XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
			return false;
		}
		return true;
	}

private:
	static map< unsigned int, NormalTaskProp* > m_mapTaskProp;//各种类型的任务属性；
};


class CNormalTask;

//////////////////////////////////////////////////////////////////////////
class LimitTimeTaskManager
{
	friend class ACE_Singleton<LimitTimeTaskManager, ACE_Null_Mutex>;
public:
	LimitTimeTaskManager():m_TaskCheckPoint(0){}

	void PushLimitTimeTask( CNormalTask* newLimitTimeTask )
	{
		m_LimitTimeTaskVec.push_back( newLimitTimeTask );
	}

	//监视所有的限制时间任务的进行
	void MonitorLimitTimeTask();

	//当玩家离开服务器时
	void OnPlayerLeaveSrv( CPlayer* pLeavePlayer );

	//当玩家完成任务时，删除对应的计时任务
	void RemoveLimitTimeTask( CPlayer* pFinishPlayer, unsigned int taskID );

private:
	time_t m_TaskCheckPoint;
	std::vector<CNormalTask *> m_LimitTimeTaskVec;
};

typedef ACE_Singleton<LimitTimeTaskManager, ACE_Null_Mutex> CLimitTimeTaskManagerSingle;


//////////////////////////////////////////////////////////////////////////
///任务;
class CNormalTask
{
public:
	CNormalTask() {};
	~CNormalTask() {};

public:
	///构造指定类型任务；
	bool InitNormalTask( CPlayer* pTaskPlayer, unsigned int taskID )
	{
		m_ptaskAttachPlayer = pTaskPlayer;
		m_taskID            = taskID;
		m_taskStartTime     = 0;
		m_pTaskProp = CNormalTaskPropManager::FindTaskProp( taskID );
		if ( NULL == m_pTaskProp )
		{
			D_WARNING( "找不到任务%d的属性\n", taskID );
			return false;
		}
		SetTaskStat( NTS_TGT_NOT_ACHIEVE );//初始状态为已接受，但未达成任务目标；
		return true;
	}

	inline void SetTaskStat( ETaskStatType inStat )
	{
		m_taskStat = inStat;
	}

	inline ETaskStatType GetTaskStat() 
	{
		return m_taskStat;
	}

	inline unsigned int GetTaskID ()
	{
		return m_taskID;
	}

	inline unsigned int GetTaskReserverID() 
	{
		if ( m_pTaskProp )
			return m_pTaskProp->m_RecordReserve;

		return 0;
	}

	inline bool GetTaskCanShare()
	{
		if ( m_pTaskProp )
			return m_pTaskProp->m_canShare;

		return 0;
	}

	inline bool CanTaskCanDrop()
	{
		if ( m_pTaskProp )
			return m_pTaskProp->m_canDrop;

		return 0;
	}

	inline bool IsLimitTimeTask()
	{
		if ( m_pTaskProp )
			return m_pTaskProp->IsLimitTimeTask();

		return false;
	}

	//检查该任务是否是限时任务
	void CheckIsLimitTimeTask();

	//获取任务的开始时间
	inline time_t GetTaskStartTime()
	{
		return m_taskStartTime;
	}

	inline void  SetTaskStartTime( unsigned int taskStartTime )
	{
		m_taskStartTime = taskStartTime;
	}

	inline COUNTER_TARGET_TYPE  GetTaskCountType()
	{
		if( m_pTaskProp )
			return m_pTaskProp->m_eCounterType;

		return CT_INVALID;
	}

	inline NormalTaskProp* GetTaskProp() { return m_pTaskProp; }

	inline CPlayer* GetTaskAttachPlayer(){ return m_ptaskAttachPlayer; }

	bool IsUnionTask(void)
	{
		if((NULL != m_pTaskProp) && m_pTaskProp->IsUnionTask())
			return true;

		return false;
	}

	//是不是每日任务
	bool IsEveryDayTask() 
	{ 
		if ( m_pTaskProp )
			return ( m_pTaskProp->GetTaskType() == 12 );

		return  false;
	}

private:
	unsigned int        m_taskID;//任务ID号；
	ETaskStatType       m_taskStat;//任务实例当前状态；
	time_t              m_taskStartTime;//该任务开始时间
	CPlayer*            m_ptaskAttachPlayer;//任务所归属的玩家
	NormalTaskProp*     m_pTaskProp;//任务属性；
};





