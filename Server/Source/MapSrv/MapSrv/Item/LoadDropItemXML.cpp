﻿#include "LoadDropItemXML.h"
#include "MonsterDropItemManager.h"
#include "MoneyGenerator.h"
#include "XmlManager.h"
#include "ItemBuilder.h"
#include "../Player/Player.h"


void ILoadDropCommonRuleXML::LoadXML(const char* pszXMLPath )
{
	if ( !pszXMLPath )
		return;

	D_DEBUG("LoadXML:%s\n", pszXMLPath);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszXMLPath))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszXMLPath );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszXMLPath);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	InsertXMLDetailInfo(childElements);

	TRY_END;
}


void ILoadDropCommonRuleXML::InsertXMLDetailInfo( std::vector<CElement*>& childElements )
{
	long setID = 0;
	//遍历所有的XML中的节点
	for ( std::vector<CElement*>::iterator lter = childElements.begin(); lter != childElements.end(); ++ lter )
	{
		CElement* pElement = *lter;
		if ( pElement )
		{
			//记录集的标号
			int tmpSetID = ACE_OS::atoi( pElement->GetAttr("LootName") );
 			if ( tmpSetID != setID )
			{
				////插入白天规则
				DropItemRuleSet* pDayRuleSet  = DayDropRuleManagerSingle::instance()->NewDropItemRule(tmpSetID);
				if ( !pDayRuleSet )
					return;

				//插入夜晚的掉落规则
				DropItemRuleSet* pDarkRuleSet = DarkDropRuleManagerSingle::instance()->NewDropItemRule(tmpSetID);
				if ( !pDarkRuleSet )
					return;

				setID = tmpSetID;
			}

			int itemID   = ACE_OS::atoi( pElement->GetAttr("ItemID"));
			int LootProA = ACE_OS::atoi( pElement->GetAttr("LootProA") );
			int LootProB = ACE_OS::atoi( pElement->GetAttr("LootProB") );

			DropItemRuleSet* pdayRuleSet = DayDropRuleManagerSingle::instance()->GetDropItemRule( setID );
			if ( pdayRuleSet && LootProA )
			{
				pdayRuleSet->PushDropConfig( itemID,LootProA );
			}

			DropItemRuleSet* pdarkRuleSet = DarkDropRuleManagerSingle::instance()->GetDropItemRule( setID );
			if ( pdarkRuleSet && LootProB )
			{
				pdarkRuleSet->PushDropConfig( itemID, LootProB );
			}
		}
	}
}


/************************************************************************/
/*                                                                      */
/************************************************************************/

void CMonsterDropItemSet::LoadXML(const char *pszXMLFile)
{
	if ( !pszXMLFile )
		return;

	D_DEBUG("LoadXML:%s\n", pszXMLFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszXMLFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszXMLFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszXMLFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	InsertXMLDetialInfo(childElements);

	TRY_END;
}

void CMonsterDropItemSet::InsertXMLDetialInfo(std::vector<CElement*> &xmlElementVec)
{
	static char DropFormat[] = {"Drop%d"};
	static char RandNumFormat[] = {"RandNum%d"};

	for ( std::vector<CElement*>::iterator lter = xmlElementVec.begin(); lter != xmlElementVec.end(); ++ lter )
	{
		CElement* pElement = *lter;
		if ( pElement )
		{
			long setID = ACE_OS::atoi( pElement->GetAttr("SetName") );

			for ( int i = 1 ; i<= 15 ;i++)
			{
				char szDrop[16] = {0};
				char szRandNum[16] = {0};
				ACE_OS::snprintf(szDrop, 16,DropFormat,i);
				ACE_OS::snprintf(szRandNum,16,RandNumFormat,i);

				const char* pDrop =  pElement->GetAttr(szDrop);
				if( !pDrop )
					continue;

				int dropID = ACE_OS::atoi( pDrop );


				if ( dropID > 0 )
				{

					const char* pRand = pElement->GetAttr(szRandNum);
					if ( !pRand )
						continue;

					int randNum = ACE_OS::atoi( pRand );
					SetID2Count setC = {dropID,randNum};
			
					mMonsterDropItemMap.insert( std::pair<long,SetID2Count>(setID,setC) );
				}
			}
		}
	}
}

void CMonsterDropItemSet::RunMonsterDropItemSet(CPlayer* pPlayer ,long setID, std::vector<CItemPkgEle>& itemEleVec, long& money )
{
	TRY_BEGIN;

	itemEleVec.clear();

	typedef std::multimap<long,SetID2Count>::const_iterator CIT;
	typedef std::pair<CIT,CIT> Range;

	Range range =  mMonsterDropItemMap.equal_range(setID);
	//随机掉落集  遍历每个掉落集，生成掉落道具
	for ( CIT i = range.first; i!=range.second; ++i )
	{
		const SetID2Count& setc = i->second;
		DropItemRuleSet* pRuleSet = DropRuleStateSingle::instance()->CurrentDropRuleState()->GetDropItemRule(setc.setID );
		if ( pRuleSet )
		{
			//本次随机的道具编号放在itemIDVec中
			pRuleSet->RandonDropItem( itemEleVec, setc.randCount );
		}
		else
		{
			//@如果是金钱
			if( MoneyGeneratorSingle::instance()->IsMoneySetID( setc.setID ) )
			{
				//根据规则，随机产生金钱
				money = MoneyGeneratorSingle::instance()->Generator( setc.setID );
			}
			//如果是任务道具的掉落集
			else if ( CDropTaskItemSetSingle::instance()->IsTaskItemDropSet( setc.setID ) )
			{
				////如果玩家没接到过任何任务，则跳过生成任务道具步骤
				CDropTaskItemSetSingle::instance()->RunDropTaskItem( setc.setID, pPlayer, itemEleVec );
			}
		}
		if ( itemEleVec.size() > 16 )
		{
			D_ERROR( "CMonsterDropItemSet::RunMonsterDropItemSet，单个掉落包裹中道具种类数%d过多\n", itemEleVec.size() );
			break;
		}
	}

	TRY_END;
}



/************************************************************************/
/*                               关于任务物品的掉落                     */
/************************************************************************/

void CTaskDropItemSet::LoadXML(const char * pszXMLFile )
{
	if ( !pszXMLFile )
		return;

	D_DEBUG("LoadXML:%s\n", pszXMLFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszXMLFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszXMLFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszXMLFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);
	InsertXMLDetialInfo(childElements);

	TRY_END;
}

void CTaskDropItemSet::InsertXMLDetialInfo( std::vector<CElement*>& pxmlElementVec )
{
	if ( pxmlElementVec.empty()  )
		return;

	for ( std::vector<CElement*>::iterator lter = pxmlElementVec.begin(); lter != pxmlElementVec.end(); ++ lter )
	{
		CElement* pElement = *lter;
		if ( pElement )
		{
			unsigned int  SetID =  atoi( pElement->GetAttr("LootName") );
			unsigned long itemID = atoi( pElement->GetAttr("ItemID") );
			unsigned long DropPro= atoi( pElement->GetAttr("DropPro") );
			unsigned long taskID = atoi( pElement->GetAttr("TaskID") );
			if ( SetID != 0 )
			{
				if( mDropTaskItemMap.find( SetID ) !=  mDropTaskItemMap.end() )
				{
					D_ERROR("DropTask掉落集的标号 %d 重复了,配置出错\n", SetID );
					continue;
				}

				DropTaskItemRule *dropTaslRule = NEW DropTaskItemRule(itemID,DropPro,taskID );
				if ( dropTaslRule )
				{
					mDropTaskItemMap.insert( std::pair<long, DropTaskItemRule *>(SetID,dropTaslRule ) );
				}
			}
		}
	}
}

bool CTaskDropItemSet::IsTaskItemDropSet(unsigned long setID )
{
	std::map<long, DropTaskItemRule *>::iterator lter = mDropTaskItemMap.find(setID);
	return  lter != mDropTaskItemMap.end();
}

bool CTaskDropItemSet::RunDropTaskItem(long setID, CPlayer* pPlayer, std::vector<CItemPkgEle> &itemEleVec)
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CTaskDropItemSet::RunDropTaskItem, NULL == pPlayer\n" );
		return false;
	}

	TRY_BEGIN;

	std::map<long, DropTaskItemRule*>::iterator lter = mDropTaskItemMap.find(setID);
	if ( lter != mDropTaskItemMap.end() )
	{
		DropTaskItemRule *pDropTaskRule = lter->second;
		if ( !pDropTaskRule )
			return false;

		int value  = pDropTaskRule->mValue;
		int taskID = pDropTaskRule->mTaskID;
		int itemTypeID = pDropTaskRule->mItemID;

		
		std::vector<CItemPkgEle>::iterator lter = std::find_if( itemEleVec.begin(), itemEleVec.end(),bind2nd( FindSameItemEle(),itemTypeID ) );
		if ( lter != itemEleVec.end() )
			return false;

		bool isHaveTask =  false;
		//如果该玩家没有该任务，查找该组队的其他玩家 
		if ( !pPlayer->FindPlayerTask( taskID ) )
		{
			//如果玩家组队了
			CGroupTeam* pTeam = pPlayer->GetSelfStateManager().GetGroupTeam();
			if ( pTeam )
				isHaveTask = pTeam->CheckTeamMemberHaveTask( taskID, pPlayer->GetMapID() );
		}
		else
			isHaveTask = true;


		//如果玩家或组队拥有任务编号
		if ( isHaveTask )
		{
			int i = RAND%100;
			//如果随机到了该物品
			if ( i <= value )
			{
				CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
				if ( pPublicProp )
				{
					CItemPkgEle itemEle;
					//by dzj, 10.05.20, itemEle.SetItemEleProp( itemTypeID, 1, taskID );
					itemEle.SetItemPropNewRand( itemTypeID, 1, taskID, 0 );
					itemEleVec.push_back( itemEle );
				}
			}
		}
	}

	TRY_END;

	return true;
}

