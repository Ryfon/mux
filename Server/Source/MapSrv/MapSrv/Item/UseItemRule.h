﻿/** @file UseItemRule_h
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-6-19
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/


#ifndef USEITEM_RULE_H
#define USEITEM_RULE_H

class CBaseItem;
class CPlayer;


//使用道具规则的执行者
class UseItemRuleExecute
{
public:
	UseItemRuleExecute(){}
	~UseItemRuleExecute(){}

public:
	static void UseItem( CPlayer* pPlayer, unsigned long itemUID );
};


//使用可装备道具
class UseEquipRule
{
public:
	UseEquipRule(){}
	~UseEquipRule(){}

public:
	//@param:CPlayer 使用了道具
	//@param:CBaseItem 使用了什么道具
	//@param:index  使用的道具在道具包中的编号
	static bool Execute(  CPlayer* pPlayer ,CBaseItem* pItem, unsigned int pkgIndex,unsigned int equipIndex,unsigned int equipReturnIndex=0 );
};

//使用可消耗物品
class UseConsumeRule
{
public:
	UseConsumeRule(){}

	~UseConsumeRule(){}
public:
	//@param:CPlayer 使用了道具
	//@param:CBaseItem 使用了什么道具
	//@param:index  使用的道具在道具包中的编号
	static bool Execute( CPlayer* pPlayer ,CBaseItem* pItem, unsigned int pkgIndex );
};

//使用功能性物品
class UseFunctionRule
{
public:
	UseFunctionRule(){}

	~UseFunctionRule(){}

public:

	static bool Execute( CPlayer* pPlayer ,CBaseItem* pItem, unsigned int pkgIndex );
};

//使用图纸道具
class UseBulePrintRule
{
public:
	UseBulePrintRule(){}

	~UseBulePrintRule(){}

public:
	static bool Execute( CPlayer* pPlayer ,CBaseItem* pItem, unsigned int pkgIndex );
};


//使用价值物
class UseAxiologyRule
{
public:
	UseAxiologyRule(){}

	~UseAxiologyRule(){}

public:
	typedef struct combindAxiolog
	{
		unsigned int pkgIndex;
		unsigned int count;
		combindAxiolog():pkgIndex(0),count(0){}
	}CombindAxiolog;

public:
	static bool Execute( CPlayer* pPlayer ,CBaseItem* pItem, unsigned int pkgIndex );
};

//使用技能书
class UseSkillBookRule
{

public:
	UseSkillBookRule(){}

	~UseSkillBookRule(){}

public:
	static bool Execute( CPlayer* pPlayer, CBaseItem* pItem, unsigned int pkgIndex );
};

//使用攻击性消耗道具
class UseDamageConsumeRule
{
public:
	UseDamageConsumeRule(){}

	~UseDamageConsumeRule(){};

public:
	static bool Execute( CPlayer* pPlayer, void* useConsumeItem );
};

//使用时装道具
class UseFashionRule
{
public:
	UseFashionRule(){}
	
	~UseFashionRule(){}
public:
	//@param:CPlayer 使用了道具
	//@param:CBaseItem 使用了什么道具
	//@param:index  使用的道具在道具包中的编号
	//服饰类(装备)
	static bool Execute(  CPlayer* pPlayer, unsigned int itemUID, unsigned int equipIndex );

	//化妆类/变形类(使用)
	static bool Execute( CPlayer* pPlayer, unsigned int itemUID );

	static void UnuseItem( CPlayer* pPlayer, unsigned long itemUID, unsigned int From, unsigned int To );

private:
	static bool ExecuteFashion(  CPlayer* pPlayer, unsigned int itemUID, unsigned int equipIndex );

	static bool ExecuteNonFashion( CPlayer* pPlayer, unsigned int itemUID );
};

#endif

