﻿/********************************************************************
	created:	2009/09/24
	created:	24:9:2009   10:41
	file:		ExpandStorageInfo.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef  EXPAND_STORAGE_INFO_H
#define  EXPAND_STORAGE_INFO_H

#include <vector>
#include <map>
#include "aceall.h"


struct ExpandCosumeItem
{
	unsigned int itemTypeID;
	unsigned int itemCount;

	ExpandCosumeItem():itemTypeID(0),itemCount(0){}
};


struct ExpandStorageInfo
{
	unsigned int curRow;
	unsigned int nextRow;
	unsigned int money;
	std::vector<ExpandCosumeItem> consumeItemVec;

	ExpandStorageInfo():curRow(0),nextRow(0),money(0){}
};


class ExpandStorageInfoManager
{
	friend class ACE_Singleton<ExpandStorageInfoManager, ACE_Null_Mutex>;

public:
	ExpandStorageInfoManager()
	{
	}

	~ExpandStorageInfoManager()
	{
		std::map< unsigned int, ExpandStorageInfo *>::iterator lter = mExpandStorageInfoMap.begin();
		for( ;lter != mExpandStorageInfoMap.end(); ++lter )
		{
			if ( NULL != lter->second )
			{
				delete lter->second;
			}
		}
		mExpandStorageInfoMap.clear();
	}

public:
	void LoadXML( const char* pXMLFileName );

public:
	
	ExpandStorageInfo * GetExpandStorageInfoByCurRow( unsigned int curRow )
	{
		if ( mExpandStorageInfoMap.empty() )
			return NULL;

		std::map< unsigned int, ExpandStorageInfo *>::iterator lter = mExpandStorageInfoMap.find( curRow );
		if ( lter != mExpandStorageInfoMap.end() )
		{
			return lter->second;
		}

		return NULL;
	}


	void PushExpandStorageInfo( unsigned int row , ExpandStorageInfo* expandStorage )
	{
		if ( NULL == expandStorage )
		{
			return;
		}
		if ( !GetExpandStorageInfoByCurRow(row) )
		{
			mExpandStorageInfoMap.insert( std::pair<unsigned int, ExpandStorageInfo *>( row,expandStorage ) );
		}	
	}



private:
	std::map< unsigned int, ExpandStorageInfo *> mExpandStorageInfoMap;
};


typedef ACE_Singleton<ExpandStorageInfoManager, ACE_Null_Mutex> ExpandStorageInfoManagerSingle;

#endif