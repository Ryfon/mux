﻿/********************************************************************
	created:	2008/08/18
	created:	18:8:2008   14:34
	file:		ItemChangeCri.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef ITEMCHANGECRI_H
#define ITEMCHANGECRI_H

#include <vector>
class CElement;


struct ChangeCri
{
	unsigned int level ;
	unsigned int odd;
	unsigned int Sumodd;
	
	ChangeCri():level(0),odd(0),Sumodd(0){}

	void Reset()
	{
		level = odd =Sumodd = 0;
	}
};

//itemchangecri
class ItemChangeCirManager
{
public:
	static void LoadXML( const char* pszXMLFile );

	static void PushChangeCri( CElement* pxmlElement );

	static ChangeCri* GetChangeCri( unsigned int level );

private:
	static std::vector<ChangeCri> ChangeCriVec;
};

struct ItemCri
{
	unsigned int level;
	unsigned int odd;

	ItemCri():level(0),odd(0){}
};


//itemCri
class ItemCriManager
{
public:
	static void LoadXML( const char* pszXMLFile );

	static void PushItemCri( CElement* pxmlElement );

	static ItemCri* GetItemCri( unsigned int itemLevel );

private:
	static std::vector<ItemCri> itemCriVec;
};

#endif
