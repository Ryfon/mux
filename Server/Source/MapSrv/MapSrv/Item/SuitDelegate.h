﻿/********************************************************************
	created:	2008/08/25
	created:	25:8:2008   13:06
	file:		SuitDelegate.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef SUITDELEGATE_H
#define SUITDELEGATE_H


#include <vector>

class CSuit;

//套装的信息
struct SuitInfo
{
	CSuit* pSuit;//套装编号
	int  suitEquipID;//玩家装备了那些位置
	int  existSuitEquipID;//套装起作用的位置
	SuitInfo():pSuit(NULL),suitEquipID(0),existSuitEquipID(0){}

};


class CPlayer;

//套装代理
class SuitDelegate
{
public:
	SuitDelegate():pAttachPlayer(NULL){}

	~SuitDelegate(){}

public:
	//依附所属的玩家
	void AttachPlayer( CPlayer* pPlayer )
	{ 
		mSuitInfoVec.clear();
		pAttachPlayer = pPlayer; 
	}

	//当装备套装的其中一部分时
	void EquipSuitPart( unsigned int suitID, unsigned int suitPart,bool wearPointIsNotEmpty );

	//当不装备套装的其中一部分时
	void UnEquipSuitPart( unsigned int suitID, unsigned int suitPart );

	//该套装是否已经存在了
	int IsSuitExist( unsigned int suitID );

	//套装是否装备完整
	bool IsSuitEquipComplate( unsigned int suitID );

	void OnWearPointEmptySubSuitProp( unsigned int suitID , unsigned int equipPos );

	void AfterRepairWearEmptySuitItem( unsigned int suitID, unsigned int equipPos );

private:
	std::vector<SuitInfo> mSuitInfoVec;
	CPlayer* pAttachPlayer;
};

#endif
