﻿/********************************************************************
	created:	2009/09/10
	created:	10:9:2009   10:49
	file:		Storage.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef STORAGE_H_
#define STORAGE_H_

#pragma once


#include "../../../Base/PkgProc/SrvProtocol.h"
using namespace MUX_PROTO;

class CPlayer;

enum  TAKEITEM_OPER_TYPE
{
	E_TAKEITEM_FROM_PKG_TO_STORAGEINDEX = 0x0,//从包裹放入仓库指定位置
	E_TAKEITEM_FROM_STORAGE_TO_PKGINDEX,//从仓库放入包裹指定位置
	E_TAKEITEM_FROM_PKG_TO_STORAGE_EMPTYINDEX,//从包裹到仓库的第一个空的位置
	E_TAKEITEM_FROM_STORAGE_TO_PKG_EMPTYINDEX,//从仓库到背包的第一个空的位置
	E_TAKEITEM_OPER_MAX,
};

#define STORAGE_COLUMN 8

struct TakeItemCheckWaiting
{
//功能函数
	TakeItemCheckWaiting():itoPkgIndex(0),ifromStorageIndex(0),bIsChecking(false),eTakeItemOper(E_TAKEITEM_OPER_MAX)
	{}

	void Reset() {  SetTakeItemInfo( E_TAKEITEM_OPER_MAX , 0, 0 ); bIsChecking = false;  eTakeItemOper = E_TAKEITEM_OPER_MAX ; }

	void SetTakeItemInfo( TAKEITEM_OPER_TYPE opType, unsigned int storageIndex, unsigned int pkgIndex )
	{
		itoPkgIndex  = pkgIndex;
		ifromStorageIndex = storageIndex;
		eTakeItemOper = opType;
	}

	void StartWaiting( unsigned int storageIndex ,unsigned int pkgIndex , TAKEITEM_OPER_TYPE opType  ) 
	{
		SetTakeItemInfo( opType, storageIndex , pkgIndex );
		bIsChecking =  true; 
	}

	void GetWaitingInfo( unsigned int& storageIndex, unsigned int& pkgIndex ,TAKEITEM_OPER_TYPE& opType)
	{
		storageIndex = ifromStorageIndex;
		pkgIndex     = itoPkgIndex;
		opType = eTakeItemOper;
	}

	void EndWaiting() 
	{
		Reset();
	}

	//是否正在等待client输入密码
	bool IsChecking() {  return bIsChecking; }

// 数据
	unsigned int itoPkgIndex;//放入背包的位置
	unsigned int ifromStorageIndex;//从仓库取出的位置
	bool         bIsChecking;//是否正在等待client的回应
	TAKEITEM_OPER_TYPE eTakeItemOper;//等待验证的操作类型
};



class CPlayerStorage
{
public:
	CPlayerStorage():m_ValidRowCount(8),m_pAttachPlayer(NULL),m_SecurityMode( E_UNSECURITY_MODE ),m_bStorageLock(true),
		m_passErrCount(0), m_storageForbiddenTime(0),m_bqueryStorageInfo(false)
	{
		StructMemSet( mStorageItemArr,     0x0,sizeof(mStorageItemArr) );
		StructMemSet( m_storagePassWord , '\0',sizeof(m_storagePassWord) );
	}

	void AttachPlayer( CPlayer* pAttachPlayer );

	//接收仓库道具信息
	void RecvStorageInfo( const ItemInfo_i* storageInfoArr, unsigned int page );

	//接收仓库的安全信息
	void RecvStorageSecurityInfo( unsigned int validRow , STORAGE_SECURITY_STRATEGY SecurityMode ,const char* psd );

	//接受仓库的禁用信息
	void RecvForbiddenInfo( unsigned int forbiddenTime, unsigned int inputErrCount ) { m_storageForbiddenTime = forbiddenTime; m_passErrCount = inputErrCount; }

	//锁定玩家的托盘
	void LockStoragePlate()  { m_bStorageLock = true;  NoticeSotoragePlateLockState(); }

	//解除锁定玩家的托盘
	void UnLockStoragePlate(){ m_bStorageLock = false; NoticeSotoragePlateLockState(); }
	
	//现在是否锁定了仓库
	bool IsLockStorage() { return m_bStorageLock; }

	//设置仓库的锁定状态
	void SetStorageLockState( bool isLock ) { m_bStorageLock = isLock ; NoticeSotoragePlateLockState(); }

	//显示仓库托盘
	bool ShowStoragePlate();

	//关闭仓库托盘
	void CloseStoragePlate();

	//通知玩家的托盘锁定的状态
	void NoticeSotoragePlateLockState();

	//将仓库的安全信息通知玩家
	void NoticeStroageSafeInfoToPlayer();

	//仓库的对应页的道具信息
	void NoticeStroageItemInfoToPlayer( unsigned storagePageIndex );

	//通知仓库的合法的行数更新
	void NoticeStorageValidRowUpdate();

	//通知仓库的安全策略更新
	void NoticeStorageSecurityStrategy();

	//弹出第一次设置密码框的界面
	void ShowFirstSetPassWordDialog(){ ShowPassWordDialog(E_SET_PASSWORD); } 

	//弹出输入密码界面
	void ShowTakeItemPassWord(){ ShowPassWordDialog(E_INPUT_PASSWORD); }

	//改变仓库琐定状态时提示密码框
	void ShowChangeLockStoragePassWrod() { ShowPassWordDialog(E_CHANGESTATE_PASSWORD); }

	//通知client对应的仓库位改变了
	void NoticeStorageInfoChange( unsigned int storageIndex, bool isNoticeClient = true );

	//通知client输入密码错误
	void NoticeInputStoragePsdRst( int reasonNo );
	
	//取出物品时的密码验证错误
	void NoticeTakeItemCheckPsdRst( int reasonNo );

	//扩展仓库错误
	void NoticeExpendStorageRst( int reasonNo );

	//获取仓库的安全策略
	STORAGE_SECURITY_STRATEGY GetStorageSafeStrategy() { return m_SecurityMode; }

	void NoticeUpdateDBStorageSafeInfo();

	void NoticeUpdateDBForbiddenInfo();

	void NoticeInputStoragePsdErr();

public:
	//从仓库取出来,放入包裹指定位置
	void HandleTakeItemFromStorageIndexToPkgIndex( unsigned int storageIndex ,unsigned int pkgIndex );

	//从仓库取出来,放入包裹
	void HandleTakeItemFromStorageToPkg( unsigned int storageIndex );

	//将包裹的物品取出来放入仓库指定位置
	void HandlePutItemToStorageIndexFromPkgIndex( unsigned int storageIndex, unsigned int pkgIndex );

	//将包裹的物品放入仓库
	void HandlePutItemToStorageFromPkg( unsigned int pkgIndex );

	//排序仓库的对应lable上的道具
	void HandleSortStorageLableItem( unsigned int lableIndex );

	//检查玩家输入的仓库密码是否
	void HandlePlayerTakeItemPsdCheck( const char* psd );

	//玩家改变仓库的锁定状态
	void HandlePlayerChangeStorageLockState();

	//玩家输入密码改变仓库的锁定状态
	void HandleInputPsdToUnLockStorage( const char* psd );

	//玩家扩展仓库时
	void HandlePlayerExtendStorage();

	//玩家设置密码 
	void HandlePlayerSetNewPassWord( const char* psd , STORAGE_SECURITY_STRATEGY securityMode );

	//玩家重新设置密码
	void HandlePlayerResetPassWord( const char* pOldPsd, const char* pNewPsd , STORAGE_SECURITY_STRATEGY securityMode );

	//交换道具
	void HandlePlayerSwapStorageItem( unsigned int fromStorageIndex, unsigned int toStorageIndex );

private:
	//将道具从仓库放入包裹,相对于点左键放入
	bool PutItemToStorageFromPkg( unsigned int pkgIndex, unsigned int storageIndex );

	//将道具放入仓库,相对于点右键放入
	bool PutItemToStorageFromPkg( unsigned int pkgIndex );

	//将仓库物品放入背包对应的位置
	bool TakeItemFromStorageToPkg( unsigned int storageIndex, unsigned int pkgindex );

	//从仓库中点右键放入背包
	bool TakeItemFromStorageToPkg( unsigned int storageIndex );

	//通知client显示对应类型的密码框
	void ShowPassWordDialog( SHOW_STORAGE_DIALOG_TYPE showType );

	//交换仓库的锁定状态
	void SwapStorageLockState() { m_bStorageLock = m_bStorageLock ? false:true; }

private:
	//获得一个空的仓库位置
	int GetFreeStorageIndex(unsigned int width = 1, unsigned int height = 1);
	//清除仓库中该uid的道具
	void ClearStorageItem( unsigned int itemUID );
	//填充仓库中的道具
	bool SetStorageItem(const ItemInfo_i& itemInfo, unsigned int storageIndex);
	//仓库位置是否空
	bool IsFreeStorageIndex(unsigned int storageIndex, unsigned int width = 1, unsigned int height = 1);
	//根据仓库index获得道具的索引
	bool GetItemInfoByStorageIndex( unsigned int storageIndex, ItemInfo_i& itemInfo, unsigned int& leftUpIndex );
	//该道具位置是否位于左上角
	bool IsLeftUpStorageIndex(unsigned int storageIndex);

private:
	void Reset()
	{ 
		StructMemSet( mStorageItemArr,    0x0, sizeof(mStorageItemArr) ); 
		StructMemSet( m_storagePassWord , '\0',sizeof(m_storagePassWord) );
		m_ValidRowCount = 8;  
		m_storageUID = 0;  
		m_SecurityMode = E_UNSECURITY_MODE; 
		m_bStorageLock = true; 
		m_bqueryStorageInfo = false;
		m_passErrCount = 0;
		m_storageForbiddenTime = 0;
		m_takeItemWating.Reset();
	}

	//密码是否为空
	bool CheckPassWordEmpty();

	//将物品放入仓库是否需要输入密码
	bool CheckPutItemNeedInputPsd( unsigned int storageIndex );

	//检查玩家输入的仓库密码是否为正确的
	bool CheckPassWordIsCorrect( const char* psd );

	//检查玩家的仓库索引是合法的
	bool CheckPlayerStorageIndexIsValid( unsigned int storageIndex );

	bool CheckStorageLableIndexIsValid( unsigned int labelIndex );

	unsigned GetValidStorageItemCount() { return m_ValidRowCount * STORAGE_COLUMN; }

	unsigned GetStorageUID() { return m_storageUID ;}

	unsigned GetStorageValidRow() { return m_ValidRowCount ; }

	ItemInfo_i* GetStorageItemArr() { return mStorageItemArr ; }

	//重置仓库的禁用时间
	void ResetStorageForbiddenInfo() { m_passErrCount = m_storageForbiddenTime = 0 ; }

	//是否有输入密码错误的次数
	bool IsHaveForbiddenInfo() { return m_passErrCount > 0 || m_storageForbiddenTime > 0; }

	//检查仓库是不是被禁止使用的时间
	bool CheckStoragePassForbiddenTime();

	//获得仓库的禁用时间
	unsigned int GetStorageUnForbiddenTime() { return m_storageForbiddenTime; }

	//获得仓库的输入密码错误的次数
	unsigned int GetStorageInputPsdErrCount() { return m_passErrCount; }
private:
	unsigned int m_storageUID;//仓库的编号,和玩家角色对应
	unsigned int m_ValidRowCount;//可用的行数
	ItemInfo_i   mStorageItemArr[MAX_STORAGE_ITEMCOUNT];//仓库在MapSrv端的副本
	STORAGE_SECURITY_STRATEGY m_SecurityMode;//仓库的安全类型
	char         m_storagePassWord[MAX_STORAGE_PASSWORD_SIZE];//仓库密码
	bool         m_bStorageLock;//仓库是否被锁定

	unsigned int m_passErrCount;//玩家输入错误的次数
	unsigned int m_storageForbiddenTime;//玩家被禁止使用仓库的时间

	bool         m_bqueryStorageInfo;

	CPlayer*     m_pAttachPlayer;

private:
	TakeItemCheckWaiting m_takeItemWating;
};

#endif
