﻿#include "RollItemManager.h"
#include <assert.h>
#include "../../../Base/Utility.h"

#define  ROLLITEMLIMITTIME 5

void RollItemManager::PushRollItem(unsigned long rollItemID, RollItem* pItem )
{
	//assert( GetRollItem(rollItemID) ==NULL );
	//assert( pItem != NULL );
	if ( ( NULL == pItem )
		|| ( NULL != GetRollItem(rollItemID) )
		)
	{
		D_ERROR( "RollItemManager::PushRollItem，空指针\n" );
		return;
	}

	mRollItemMap.insert( std::pair<int,RollItem *>(rollItemID,pItem) );
}

RollItem* RollItemManager::GetRollItem(unsigned long rollItemID)
{
	std::map<int,RollItem *>::iterator lter = mRollItemMap.find(rollItemID);
	if ( lter != mRollItemMap.end() )
		return lter->second;
	return NULL;
}

RollItem* RollItemManager::CreateRollItem(CItemsPackage* pItemPkg, const CItemPkgEle& pItemEle, unsigned int groupID )
{
	unsigned long rollID = GetRollItemIndex();

	if ( NULL == pItemPkg )
	{
		D_ERROR("RollItemManager::CreateRollItem, NULL == pItemPkg\n");
		return NULL;
	}

	RollItem* pRollItem = NEW RollItem( pItemPkg, pItemEle, rollID, groupID );
	if ( pRollItem )
		PushRollItem( rollID,pRollItem );

	IncRollItemIndex();
	return pRollItem;
}

void RollItemManager::RemoveRollItem(unsigned long rollItemID)
{
	ACE_UNUSED_ARG( rollItemID );

	std::map<int,RollItem *>::iterator lter = mRollItemMap.find(rollItemID);
	if ( lter != mRollItemMap.end() )
	{
		D_DEBUG("RollItemManager::RemoveRollItem 移除%d\n",rollItemID );

		RollItem* pRollItem = lter->second;
		if ( pRollItem  )
			delete pRollItem;

		mRollItemMap.erase(lter);
	}
}

void RollItemManager::RemoveRollItem(RollItem* pRollItem )
{
	TRY_BEGIN;

	//assert(  pRollItem != NULL );
	if ( NULL == pRollItem )
	{
		D_ERROR( "RollItemManager::RemoveRollItem，NULL == pRollItem\n" );
		return;
	}

	RemoveRollItem( pRollItem->GetRollItemID() );

	TRY_END;
}

void RollItemManager::MonitorRollItemStatus()
{
	TRY_BEGIN;

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();

	ACE_Time_Value diffTime = currentTime - updateTime;
	//每5秒更新检测一次
	if ( diffTime.sec() >= ROLLITEMLIMITTIME )
	{
		std::map<int,RollItem *>::iterator lter = mRollItemMap.begin();
		for ( ;lter!= mRollItemMap.end(); )
		{
			RollItem* pRollItem = (RollItem *)lter->second;
			if ( !pRollItem )
				continue;

			/*去掉原来的判断
			//如果已经过了30秒了
			if( pRollItem->IsPass30s() )
			{
				pRollItem->RandRollItem();
#ifdef _DEBUG
				D_DEBUG("RollItemManager::MonitorRollItemStatus 包裹%d过了Roll30秒\n",pRollItem->GetRollItemID() );
#endif
			}

			//如果已经Roll点完毕
			if ( pRollItem->IsRollOver() )
			{
				delete pRollItem;
				mRollItemMap.erase( lter++ );
				continue;
			}
			*/

			bool needDel = false;
			pRollItem->CheckTimeout(needDel);
			if(needDel)
			{
				delete pRollItem;
				mRollItemMap.erase( lter++ );
				continue;
			}

			lter++;
		}
	}

	TRY_END;
}

void RollItemManager::OnPlayerLeaveSrv(CPlayer* pLeavaPlayer )
{
	TRY_BEGIN;

	if ( pLeavaPlayer )
	{
		std::map<int,RollItem *>::iterator lter = mRollItemMap.begin();
		for ( ;lter!= mRollItemMap.end(); ++lter )
		{
			RollItem* pRollItem = lter->second;
			if ( pRollItem )
			{
				pRollItem->OnPlayerLeaveSrv( pLeavaPlayer );
			}
		}
	}

	TRY_END;
}
