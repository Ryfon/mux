﻿/********************************************************************
	created:	2010/04/01
	created:	1:4:2010   14:09
	file:		AuctionManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef AUCTION_MANAGER_
#define AUCTION_MANAGER_

class CPlayer;

class AuctionManager
{
public:
	AuctionManager():m_pAttachPlayer(0){}

	void AttachPlayer( CPlayer* pAttachPlayer )
	{
		m_pAttachPlayer = pAttachPlayer;
	}

	//玩家开始拍卖一个新的物品时
	void OnStartNewAuction( int pkgIndex, int origiprice, int fixprice, char expiretype );

	//当玩家竞拍物品出一个最新的价钱时，验证出的价格是否合法
	void OnPlayerBidItemByNewPrice( int auctionUID, int oldprice, int newprice );

private:
	CPlayer* m_pAttachPlayer;
};

#endif