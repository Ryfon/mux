﻿/********************************************************************
	created:	2008/08/18
	created:	18:8:2008   9:18
	file:		ItemUpdateManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef ITEMUPDATEMANAGER_H
#define ITEMUPDATEMANAGER_H


#include "../../../Base/PkgProc/CliProtocol_T.h"
using namespace MUX_PROTO;


class  CPlayer;
struct  PlateCondition;

enum EnumItemUpdate
{
	E_LEVELUP =0x0,//升级
	E_ADDID,//追加
	E_CHANGE,//改造
};

class ItemUpdateManager
{
public:
	//pPlayer执行操作的人 mainItem 本次升级的主料  assistItem 本次升级的辅料   EnumItemUpdate 本次升级的类型
	static bool ExecuteUpdate( CPlayer* pPlayer, ItemInfo_i* mainItem, ItemInfo_i* assistItem, EnumItemUpdate eUpdateType,unsigned int UpdateID, void* attachInfo );

	//读取升级所需的材料
	static bool ReadLevelUpMaterial( CPlayer* pPlayer,MUX_PROTO::ItemInfo_i &mainItem, PlateCondition *levelUpCondition, unsigned int& levelUpID );

	//读取追加所需要的材料
	static bool ReadAddIDMaterial( CPlayer* pPlayer, PlateCondition *levelUpCondition, unsigned int bulePrintID );

	//读取改造所需要的材料
	static bool ReadChangeMaterial( CPlayer* pPlayer, MUX_PROTO::ItemInfo_i &mainItem, PlateCondition *changeCondition );
private:
	//执行道具升级
	static bool ExecuteItemLevelUp( CPlayer* pPlayer, ItemInfo_i* mainItem, ItemInfo_i* assistItem ,unsigned int updateID );

	//执行道具追加
	static bool ExecuteItemAddID( CPlayer* pPlayer, ItemInfo_i* mainItem, ItemInfo_i* assistItem ,unsigned int bulePrintUID );
	
	//执行道具的改造
	static bool ExecuteItemChange( CPlayer* pPlayer, ItemInfo_i* mainItem, ItemInfo_i* assistItem, void* attachInfo );
};


#endif
