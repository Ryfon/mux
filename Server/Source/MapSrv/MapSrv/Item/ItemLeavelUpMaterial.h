﻿/********************************************************************
	created:	2008/08/13
	created:	13:8:2008   17:13
	file:		ItemLeavelUpMaterial.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef ITEMLEVELUPMATERIAL_H
#define ITEMLEVELUPMATERIAL_H


#include <vector>
#include <map>

class CElement;

struct Material
{
	unsigned int itemTypeID;
	unsigned int count;
	Material():itemTypeID(0),count(0){}
};

struct MaterialSet
{
	std::vector<Material> materialVec;
	int  updateConsumeMoney;

	MaterialSet():updateConsumeMoney(0){}
};

class CItemLevelUpMaterial
{
public:
	static void LoadXML( const char* pxmlFile );

	static void PushMaterialSet( CElement* pxmlElement );

	static MaterialSet* GetMaterialSet( unsigned long itemID );

private:
	static std::map< unsigned, MaterialSet> mMaterialSetMap;
	
};

class CItemChangeMaterial
{
public:
	static void LoadXML( const char* pxmlFile );

	static void PushChangeMaterialSet( CElement* pxmlElement );

	static MaterialSet* GetMaterialSet( unsigned long itemChangeID );

private:
	static std::map<unsigned ,MaterialSet > mChangeMaterialSetMap;

};


#endif
