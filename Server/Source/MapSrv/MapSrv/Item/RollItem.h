﻿/********************************************************************
	created:	2008/07/23
	created:	23:7:2008   9:07
	file:		RollItem.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef _ROLLITEM_H
#define _ROLLITEM_H


#include <vector>

#include "CItemPackage.h"

class CBaseItem;
class CPlayer;
class CItemPkgEle;

class RollItem
{
public:
	RollItem( CItemsPackage* pPkg, const CItemPkgEle& pItemEle,unsigned rollID, unsigned int groupID ):mpItemPkg(pPkg),mpItemEle(pItemEle),
		mrollID(rollID),
		mBeginRollTime(0),
		maxRollNum(0),mOwnerGroupID(groupID),
		mrollOver(false),
		recvRollAgreeCount(0),
		recvRollDisAgrCount(0),
		rollPeopleCount(0),
		mIsHaveWinPlayer(false)
	{
		mWinPlayerID.dwPID = mWinPlayerID.wGID = 0;
		mWinPlayerName[0] = '\0';
	}

	~RollItem(){}

private:
	RollItem( const RollItem& );  //屏蔽这两个操作；
	RollItem& operator = ( const RollItem& );//屏蔽这两个操作；

public:
	//触发Roll点
	void TriggerRoll( const std::vector<CPlayer *>& playerVec );
	//随机Roll点
	void RandRollItem();
	//接到传来的是否同意Roll点的结果
	bool RecvRollReq( CPlayer* player, bool isAgree );
	//判断是否过了30秒应该Roll点了
	bool IsPass30s();
	//获取Roll物品的ID号
	int  GetRollItemID() { return mrollID; }
	//是否Roll点完成
	bool IsRollOver(){ return mrollOver; }
	//玩家离开服务器
	void OnPlayerLeaveSrv( CPlayer* pLeavePlayer );

public:
	void CheckTimeout(bool &needDel);

private:
	CItemsPackage * mpItemPkg;//发送Roll的道具包裹
	CItemPkgEle mpItemEle;
	int  mrollID;
	ACE_Time_Value mBeginRollTime;
	int  maxRollNum;
	unsigned int mOwnerGroupID;//Roll所属的组队编号
	PlayerID mWinPlayerID;
	char  mWinPlayerName[MAX_NAME_SIZE];
	bool  mIsHaveWinPlayer;
	bool  mrollOver;//有没有执行RandRollIte m

	std::vector<PlayerID> mRollPlayerIDVec;

	int  recvRollAgreeCount;//接到Roll点回执的结果
	int  recvRollDisAgrCount;//接到不同意Roll点的回执
	int  rollPeopleCount;//参加Roll点的人数
};


#endif
