﻿#include "ItemDelegate.h"
#include "../Player/Player.h"
#include "../LogManager.h"

//class FindItemFunc:public std::binary_function<CBaseItem *,unsigned int ,bool>
//{
//public:
//	bool operator()( CBaseItem* pItem1,unsigned int itemUID ) const
//	{
//		if ( !pItem1  )
//			return false;
//
//		if ( pItem1->GetItemUID() == itemUID )
//			return true;
//
//		return false;
//	}
//};

class DeleteItemPointFunc:public std::unary_function<pair<unsigned int,CBaseItem *>,bool>
{
public:
	bool operator()( pair<unsigned int,CBaseItem *> pItem ) const
	{
		if ( !pItem.second )
			return false;

		delete pItem.second;pItem.second = NULL;
		return true;
	}
};


void ItemDelegate::AttachPlayer(CPlayer *pAttachPlayer)
{
	mpAttachPlayer = pAttachPlayer;
	mEquipItemMap.clear();
	mPkgItemMap.clear();
	mFashionItemMap.clear();
}

void ItemDelegate::PushEquipItem(CBaseItem* pItem )
{
	if ( !pItem )
		return;

	TRY_BEGIN;

	mEquipItemMap.insert( std::make_pair(pItem->GetItemUID(), pItem) );

	TRY_END;
}

void ItemDelegate::PushPkgItem(CBaseItem* pItem )
{
	if ( !pItem )
		return;

	TRY_BEGIN;

	mPkgItemMap.insert( std::make_pair(pItem->GetItemUID(), pItem) );


	TRY_END;
}

void ItemDelegate::PushFashionItem( CBaseItem *pItem)
{
	if(!pItem)
		return;
	
	TRY_BEGIN
	
	mFashionItemMap.insert( std::make_pair(pItem->GetItemUID(), pItem) );

	TRY_END;

	return;
}

CBaseItem* ItemDelegate::GetEquipItem(unsigned itemUID )
{
	TRY_BEGIN;

	if ( mEquipItemMap.empty() )
		return NULL;

	std::map<unsigned int,CBaseItem *>::iterator equipIter = mEquipItemMap.find(itemUID);
	if ( equipIter != mEquipItemMap.end()  )
	{
		return (equipIter->second);
	}

	TRY_END;

	return NULL;

}

CBaseItem* ItemDelegate::GetFashionItem( unsigned itemUID)
{
	TRY_BEGIN;

	if ( mFashionItemMap.empty() )
		return NULL;

	std::map<unsigned int,CBaseItem *>::iterator equipIter = mFashionItemMap.find(itemUID);
	if ( equipIter != mFashionItemMap.end()  )
	{
		return (equipIter->second);
	}

	TRY_END;

	return NULL;
}

CBaseItem* ItemDelegate::GetPkgItem(unsigned int itemUID)
{
	TRY_BEGIN;

	if ( mPkgItemMap.empty() )
		return NULL;

	std::map<unsigned int, CBaseItem *>::iterator pkgIter = mPkgItemMap.find(itemUID);
	if ( pkgIter != mPkgItemMap.end()  )
	{
		return (pkgIter->second);
	}

	TRY_END;

	return NULL;
}

CBaseItem* ItemDelegate::GetItemByItemUID(unsigned int itemUID )
{
	TRY_BEGIN;

	CBaseItem* pItem = GetPkgItem( itemUID );
	if ( pItem == NULL )
	{
		pItem = GetEquipItem( itemUID );
	}

	return pItem;

	TRY_END;
	return NULL;
}


void ItemDelegate::GetCanDropEquipItem(std::vector<CBaseItem *>& unProtectEquipItemVec )
{
	if ( !mpAttachPlayer )
		return;

	unProtectEquipItemVec.clear();

	std::map<unsigned int,CBaseItem *>::iterator lter = mEquipItemMap.begin();
	for ( ;lter != mEquipItemMap.end(); ++lter )
	{
		CBaseItem* pItem = (lter->second);
		if ( pItem && !pItem->IsTaskItem() )//任务道具不能掉落
		{
			if( mpAttachPlayer->IsCanDeadDrop( pItem->GetItemUID() ) )
				unProtectEquipItemVec.push_back( pItem );
		}
	}
}

void ItemDelegate::GetCanDropPkgItem(std::vector<CBaseItem *>& unProtectPkgItemVec )
{
	if ( !mpAttachPlayer )
		return;

	unProtectPkgItemVec.clear();

	std::map<unsigned int,CBaseItem *>::iterator lter = mPkgItemMap.begin();
	for ( ;lter != mPkgItemMap.end(); ++lter )
	{
		CBaseItem* pItem = (lter->second);
		if ( pItem && !pItem->IsTaskItem() )//任务物品不能掉落
		{
			if ( mpAttachPlayer->IsCanDeadDrop( pItem->GetItemUID() ) )
				unProtectPkgItemVec.push_back( pItem );
		}
	}
}


void ItemDelegate::RemovePkgItem(unsigned int itemUID )
{
	if ( mPkgItemMap.empty() )
		return;

	TRY_BEGIN;

	mPkgItemMap.erase(itemUID);

	TRY_END;
}

void ItemDelegate::EquipItem(CBaseItem* pItem )
{
	if ( !pItem )
		return;

	TRY_BEGIN;

	std::map<unsigned int,CBaseItem *>::iterator pkgIter = mPkgItemMap.find(pItem->GetItemUID());
	if ( pkgIter != mPkgItemMap.end() )
	{
		PushEquipItem( pItem );
		mPkgItemMap.erase( pkgIter );
	}


	TRY_END;
}

void ItemDelegate::UnEquipItem(CBaseItem* pItem )
{
	if ( !pItem )
		return;

	TRY_BEGIN;

	std::map<unsigned int,CBaseItem *>::iterator equipIter = mEquipItemMap.find(pItem->GetItemUID());

	if ( equipIter != mEquipItemMap.end() )
	{
		PushPkgItem( pItem );
		mEquipItemMap.erase( equipIter );
	}

	TRY_END;
}

bool ItemDelegate::DropEquipItem(unsigned int itemUID )
{

	TRY_BEGIN;

	std::map<unsigned int,CBaseItem *>::iterator equipIter = mEquipItemMap.find(itemUID);

	if ( equipIter!=  mEquipItemMap.end() )
	{
		if ( NULL != (equipIter->second) )
		{
			delete (equipIter->second);
		}
		mEquipItemMap.erase( equipIter );
		return true;
	}

	TRY_END;

	return false;
}

void ItemDelegate::OnFashionItem( CBaseItem* pItem )
{
	if ( !pItem )
		return;

	TRY_BEGIN;

	std::map<unsigned int,CBaseItem *>::iterator pkgIter = mPkgItemMap.find(pItem->GetItemUID());
	if ( pkgIter != mPkgItemMap.end() )
	{
		PushFashionItem( pItem );
		mPkgItemMap.erase( pkgIter );
	}


	TRY_END;
}

void ItemDelegate::OffFashionItem( CBaseItem* pItem)
{
	if ( !pItem )
		return;

	TRY_BEGIN;

	std::map<unsigned int,CBaseItem *>::iterator equipIter = mFashionItemMap.find(pItem->GetItemUID());

	if ( equipIter != mFashionItemMap.end() )
	{
		PushPkgItem( pItem );
		mFashionItemMap.erase( equipIter );
	}

	TRY_END;
}

bool ItemDelegate::DropPkgItem(unsigned int itemUID)
{
	TRY_BEGIN;

	std::map<unsigned int,CBaseItem *>::iterator pkgIter = mPkgItemMap.find(itemUID);

	if ( pkgIter != mPkgItemMap.end() )
	{
		if ( NULL != (pkgIter->second) )
		{
			delete (pkgIter->second);
		}
		mPkgItemMap.erase( pkgIter );
		return true;
	}

	TRY_END;

	return false;
}

bool ItemDelegate::OnPlayerExit()
{

	TRY_BEGIN;

	if ( !mpAttachPlayer )
		return false;

	if ( mPkgItemMap.size() )
		std::for_each(mPkgItemMap.begin(),mPkgItemMap.end(),DeleteItemPointFunc() );

	mPkgItemMap.clear();

	if ( mEquipItemMap.size() )
		std::for_each(mEquipItemMap.begin(), mEquipItemMap.end(), DeleteItemPointFunc() );

	mEquipItemMap.clear();

	if ( mFashionItemMap.size() )
		std::for_each(mFashionItemMap.begin(), mFashionItemMap.end(), DeleteItemPointFunc() );

	mFashionItemMap.clear();

	TRY_END;

	return true;
}


CBaseItem*	ItemDelegate::GetEquipItemByIndex(unsigned int index )
{
	TRY_BEGIN;

	if ( !mpAttachPlayer )
		return NULL;

	if ( index >= mEquipItemMap.size() )
		return NULL;

	unsigned int tmpindex = 0;
	std::map<unsigned int,CBaseItem*>::iterator iter = mEquipItemMap.begin();
	for ( ; iter!=mEquipItemMap.end(); ++iter )
	{
		++tmpindex;
		if ( tmpindex > index )
		{
			return iter->second;
		}
	}

	return NULL;

	TRY_END;

	return NULL;
}


void ItemDelegate::OnPlayerDieDropItem( std::vector<CItemPkgEle>& dropItemEle,unsigned int& money )
{
	if ( !mpAttachPlayer )
		return;

	TRY_BEGIN;

	dropItemEle.clear();
	money = 0;

	short goodevilpoint = mpAttachPlayer->GetGoodEvilPoint();
	if ( goodevilpoint >= 0 )
	{
		D_DEBUG("%s为非红名玩家死亡不需要掉落装备\n", mpAttachPlayer->GetNickName() );
		return;
	}


	//如果善恶值小于0
	if( goodevilpoint < 0 )
	{
		//对恶魔的惩罚,需要掉落一定的金钱 
		unsigned int tmpmoney = mpAttachPlayer->GetGoldMoney();//双币种判断，按姚斌：玩家掉落为金钱
		money = tmpmoney/10;//总数的10%
		if(0 == money)
		{
			if(tmpmoney > 0)//最小值
				money = 1;
		}
		else if(money > 1000000)//最大值
		{
			money = 1000000;
		}

		mpAttachPlayer->SubGoldMoney( money );//双币种判断
			
		//获取未保护的身上装备
		std::vector<CBaseItem *> unProtectEquipItemVec;
		GetCanDropEquipItem( unProtectEquipItemVec );

		//恶魔玩家需要掉落装备在身上的物品1-2件
		unsigned int equipdropNum = RandNumber(1,2);
		if ( !unProtectEquipItemVec.empty() )
		{
			if ( unProtectEquipItemVec.size() < equipdropNum )
				equipdropNum = (unsigned int)unProtectEquipItemVec.size();

			//随机打乱保护装备的列表
			std::random_shuffle( unProtectEquipItemVec.begin(), unProtectEquipItemVec.end() );

			FullPlayerInfo * playerInfo = mpAttachPlayer->GetFullPlayerInfo();
			if ( NULL == playerInfo )
			{
				D_ERROR("ItemDelegate::OnPlayerDieDropItem, NULL == GetFullPlayerInfo()\n");
				return;
			}
			PlayerInfo_2_Equips& equipPkg = playerInfo->equipInfo;
			for ( unsigned int i = 0 ; i < equipdropNum ; i++ )
			{
				CBaseItem* pItem  = unProtectEquipItemVec[i];
				if ( !pItem )		
					continue;

				for ( unsigned int i = 0 ; i< EQUIP_SIZE; i++ )
				{
					const ItemInfo_i& tmpInfo = equipPkg.equips[i];
					if ( tmpInfo.uiID == pItem->GetItemUID() )
					{
						ItemInfo_i tmpdrop = tmpInfo;
						tmpdrop.uiID = 0;

						CItemPkgEle itemEle;
						//10.05.20, itemEle.SetItemEleProp( tmpdrop, pItem->GetItemPrice(), pItem->GetItemMassLevel() ,0 );
						itemEle.SetItemWithItemInfo_i( tmpdrop, pItem->GetItemPrice(), pItem->GetItemMassLevel(), 0 );
						dropItemEle.push_back( itemEle );

						D_DEBUG("%s 玩家死亡丢弃装备道具%d\n", mpAttachPlayer->GetNickName(), tmpInfo.uiID );
						mpAttachPlayer->OnDropItem( tmpInfo.uiID );
						break;
					}
				}					
			}
		}
	}

	//恶魔玩家每次死亡掉落最大5件物品
	unsigned int maxDropNum = 5;
	unsigned int randStart  = 3;
	////如果是正常或英雄玩家,有60%都不掉道具
	//if ( goodevilpoint >= 0 )
	//{
	//	//40%的道具掉落概率,如果>40则返回
	//	if( RandNumber(1,100) > 40 )
	//		return;
	//	//英雄和普通玩家每次最大掉落3件物品
	//	maxDropNum = 3;
	//	randStart =  1;
	//}


	//正常人和英雄玩家每次从背包掉落最大3个道具,魔头至少3个,最大5个
	unsigned int pkgdropNum = RandNumber( randStart,maxDropNum);

	//从包裹中选出未保护的包裹物品
	std::vector<CBaseItem *>  unProtectPkgItemVec;
	GetCanDropPkgItem( unProtectPkgItemVec );

	if ( !unProtectPkgItemVec.empty() )
	{
		if ( unProtectPkgItemVec.size() < pkgdropNum )
			pkgdropNum = (unsigned int) (unProtectPkgItemVec.size());

		//打乱可以丢弃的包裹道具
		std::random_shuffle( unProtectPkgItemVec.begin(), unProtectPkgItemVec.end() );
		
		FullPlayerInfo * playerInfo = mpAttachPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CEquipItemManagerEx::SwapItemOnItemPkg, NULL == GetFullPlayerInfo()\n");
			return;
		}
		PlayerInfo_3_Pkgs& pkg = playerInfo->pkgInfo;
		for ( unsigned int i = 0 ; i< pkgdropNum ; i++ )
		{
			CBaseItem* pItem  = unProtectPkgItemVec[i];
			if ( !pItem )		
				continue;

			for( unsigned int  i = 0 ; i< PACKAGE_SIZE; i++ )
			{
				const ItemInfo_i& tmpInfo = pkg.pkgs[i];
				if ( tmpInfo.uiID == pItem->GetItemUID() )
				{
					ItemInfo_i tmpdrop = tmpInfo;
					tmpdrop.uiID = 0;

					CItemPkgEle itemEle;
					//10.05.20, itemEle.SetItemEleProp( tmpdrop, pItem->GetItemPrice(), pItem->GetItemMassLevel() ,0 );
					itemEle.SetItemWithItemInfo_i( tmpdrop, pItem->GetItemPrice(), pItem->GetItemMassLevel(), 0 );
					dropItemEle.push_back( itemEle );

					D_DEBUG("%s 玩家死亡丢弃包裹道具%d\n", mpAttachPlayer->GetNickName(), tmpInfo.uiID );
					mpAttachPlayer->OnDropItem( tmpInfo.uiID ); 
					break;
				}
			}
		}
	}

	ItemInfo_i drops[5/*MAX_DIE_DROP_COUNT*/];
	unsigned short maxDropCount = dropItemEle.size() > 5 ? 5 : (unsigned short)dropItemEle.size();
	if(maxDropCount > 0)
	{
		for(int i = 0; i < maxDropCount; i++)
		{	
			drops[i] = dropItemEle[i].GetItemEleInfo();
		}
		CLogManager::DoPlayerDead(mpAttachPlayer, 2, 0, 0, maxDropCount, drops);//记日至
	}

	TRY_END;
}
