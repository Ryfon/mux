﻿/** @file CSpecialItemProp.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-4-29
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef SPECIAL_ITEM_PROP_H
#define SPECIAL_ITEM_PROP_H

#include <aceall.h>
#include "CItemProp.h"
#include "../../../Base/PkgProc/CliProtocol_T.h"
using namespace MUX_PROTO;

#include "Utility.h"

//using namespace ITEM_GLOBAL;


class CPlayer;


struct WeaponProperty
{
	WeaponProperty()
	{
		StructMemSet( *this, 0, sizeof(*this) );//自身初始化值
	}
	ITEM_GEARARM_TYPE  mGearArmType;
	int  brand;
	int  SkillRatio;
	int  SkillID;
	int  AttackPhyMax;
	int  AttackPhyMin;
	int  AttackMagMax;
	int  AttackMagMin;
	int  Speed;
	int  Hp;
	int  Mp;
	int  PhysicsAttack;
	int  MagicAttack;
	int  PhysicsDefend;
	int  MagicDefend;
	int  PhysicsHit;
	int  MagicHit;
	int  PhysicsCritical;
	int  MagicCritical;
	int  PhysicsAttackPercent;
	int  MagicAttackPercent;
	int  PhysicsCriticalDamage;
	int  MagicCriticalDamage;
	int  PhysicsDefendPercent;
	int  MagicDefendPercent;
	int  PhysicsCriticalDamageDerate;
	int  MagicCriticalDamageDerate;
	int  PhysicsJouk;
	int  MagicJouk;
	int  PhysicsDeratePercent;
	int  MagicDeratePercent;
	int  Stun;
	int  Tie;
	int  Antistun;
	int  Antitie;
	int  AddSpeed;
	int  PhysicsAttackAdd;
	int  MagicAttackAdd;
	int  PhysicsRift;
	int  MagicRift;
	int  ExcellentBiff;
	int  LuckyBiff;
	int  SuitId;
	int  itemCriID;
	int  itemMaterialID;
	int  itemLevelUpLimit;
	int  luckid;//幸运属性ID
	int  excellentid;//卓越属性ID
	////直接通过luckyID与excellentID来决定是否卓越/幸运装备int  luckytype;//幸运类型
	//unsigned short  strength;
	//unsigned short  agility;
	//unsigned short  intelligence;
	//unsigned short  spirit;
	//unsigned short  physique;
};


struct EquipProperty
{
	EquipProperty()
	{
		StructMemSet( *this, 0, sizeof(*this) );//自身初始化值
	}
	ITEM_GEARARM_TYPE mGearArmType;
	int brand;
	int Hp;
	int Mp;
	int PhysicsAttack;
	int MagicAttack;
	int PhysicsDefend;
	int MagicDefend;
	int PhysicsHit;
	int MagicHit;
	int PhysicsCritical;
	int MagicCritical;
	int PhysicsAttackPercent;
	int MagicAttackPercent;
	int PhysicsCriticalDamage;
	int MagicCriticalDamage;
	int PhysicsDefendPercent;
	int MagicDefendPercent;
	int PhysicsCriticalDerate;
	int MagicCriticalDerate;
	int PhysicsCriticalDamageDerate;
	int MagicCriticalDamageDerate;
	int PhysicsJouk;
	int MagicJouk;
	int PhysicsDeratePercent;
	int MagicDeratePercent;
	int Stun;
	int Tie;
	int Antistun;
	int Antitie;
	int  AddSpeed;
	int  PhysicsAttackAdd;
	int  MagicAttackAdd;
	int  PhysicsRift;
	int  MagicRift;
	int  ExcellentBiff;
	int  LuckyBiff;
	int  SuitId;
	int  itemCriID;
	int  itemMaterialID;
	int  itemLevelUpLimit;
	int  luckid;//幸运属性ID
	int  excellentid;//卓越属性ID
	////直接通过luckyID与excellentID来决定是否卓越/幸运装备int  luckytype;//幸运类型
	//unsigned short  strength;
	//unsigned short  agility;
	//unsigned short  intelligence;
	//unsigned short  spirit;
	//unsigned short  physique;
};

struct ConsumeProperty
{
	enum ConsumeType
	{
		E_RESUME_HP = 0x1,//恢复HP
		E_RESUME_MP,//恢复MP
		E_RESUME_SP,//恢复SP
		E_RESUME_ABSORB,//吸收
		E_RESUME_IMMUNITY,//免疫
		E_RESUME_UNCHAIN,//解除
		E_RESUME_SINGLE,//单体
		E_RESUME_MULTI,//范围
	};

	int Uniquely;
	int skillID;
	ConsumeType comsumeType;
};

struct  TaskProperty
{
	int Uniquely;
	int TaskScript;
	int TaskID;
	int type;//任务道具的类型
};

struct FunctionProperty
{
	enum FunctionType
	{
		E_BACKHOME = 0,//回城
		E_RIDE,//骑乘
		E_PETEGG, //宠物蛋
		E_PETFOOD, //宠物食物
		E_FUNC_SKILLBOOK,//技能书
		E_SPECIAL_PROTECT = 5,//特殊保护道具
		E_UNSPECIAL_PROTECT = 6,//解除特殊保护道具
		E_NORMAL_PROTECT = 7,//普通保护道具
		E_UNNORMAL_PROTECT = 8,//解除普通保护道具
		E_FUNC_EXPIRE_BOOK = 10,//经验书
		E_ADD_UNION_ACTIVE_ONCE = 11,//一次性增加活跃度
		E_SET_UNION_ACTIVE_TIMES = 12,//设置工会活跃度倍率
		E_ADD_UNION_PRESTIGE_ONCE = 13,//一次性增加威望
		E_SET_UNION_PRESTIGE_TIMES = 14,//设置工会活跃度倍率
		E_LUCK_STAR = 99,//幸运星
		E_FUCNTIONTYPE_MAX,
	};

	int TaskScript;
	FunctionType Type;//功能性物品的类型
	int Expend;
	float AddSpeed;
	int sex;//可以使用的性别
	unsigned short maxWear;//一次性增加(策划指定，用于计算活跃度和威望）
	unsigned short repairMode;//倍数，此时maxWear为时间（策划指定，用于计算活跃度或威望）
	//unsigned int Strength;//需要的力量
	//unsigned int Vitality;//需要的体质
	//unsigned int Agility;//需要的敏捷
	//unsigned int Spirit;//需要的精神
	//unsigned int Intelligence;//需要的智力
	//unsigned int PreSkillID;//前置技能，使用该道具需要学会的技能
};

struct AxiologyProperty
{
	enum AxiologyType
	{
		E_NORMAL = 0,//普通的
		E_ADDLEVELCRI = 1, // 增加升级概率
		E_CONBIND = 7,//可合并的价值物
		E_GOOD = 8,//无痕的
		E_PERFECT =9,//完美无暇的
	};

	float value;
	AxiologyType type;//价值品的类型
};

struct SkillBookProperty
{
	enum SkillBookType
	{
		E_RESIGN_PHY_SKILL =0x0 ,//洗点物理技能
		E_RESIGN_MIG_SKILL ,//洗点魔法技能
		E_RESIGN_ALL_SKILL ,//全洗物理和魔法技能
		E_PET_TRANS_BOOK,//宠物变身技能书
		E_LEARN_SKILL,//学习技能
		E_UPDATE_SKILL = 0x9,//升级技能　
	};
	unsigned int skillID;
	SkillBookType skillType;//技能类型
	unsigned int Strength;//需要的力量
	unsigned int Vitality;//需要的体质
	unsigned int Agility;//需要的敏捷
	unsigned int Spirit;//需要的精神
	unsigned int Intelligence;//需要的智力
	unsigned int PreSkillID;//前置技能，使用该道具需要学会的技能
};

struct FashionProperty
{
	ITEM_GEARARM_TYPE gearArmType;
	BYTE expend;
	BYTE sex;//0:通用,1:男,2女
	unsigned int maleMode[6];
	unsigned int femaleMode[6];
	bool unionFlag;
};


struct MountProperty
{
	ITEM_GEARARM_TYPE mGearArmType;
	int brand;
	int Hp;
	int Mp;
	int PhysicsAttack;
	int MagicAttack;
	int PhysicsDefend;
	int MagicDefend;
	int PhysicsHit;
	int MagicHit;
	int PhysicsCritical;
	int MagicCritical;
	int PhysicsAttackPercent;
	int MagicAttackPercent;
	int PhysicsCriticalDamage;
	int MagicCriticalDamage;
	int PhysicsDefendPercent;
	int MagicDefendPercent;
	int PhysicsCriticalDerate;
	int MagicCriticalDerate;
	int PhysicsCriticalDamageDerate;
	int MagicCriticalDamageDerate;
	int PhysicsJouk;
	int MagicJouk;
	int PhysicsDeratePercent;
	int MagicDeratePercent;
	int Stun;
	int Tie;
	int Antistun;
	int Antitie;
	int  AddSpeed;
	int  PhysicsAttackAdd;
	int  MagicAttackAdd;
	int  PhysicsRift;
	int  MagicRift;
	int  SuitId;
	int  itemCriID;
	int  itemMaterialID;
	int  itemLevelUpLimit;
	int  ActiveTime;//上马的时间
	int  BeKickCri;//被打下来的概率
	MountProperty():ActiveTime(0),BeKickCri(0),itemCriID(0),itemMaterialID(0),itemLevelUpLimit(0){}
};

struct LevelUpProp;

class CBaseItem
{
public:
	explicit CBaseItem(CItemPublicProp* pItemProp):m_ItemPublicProp(pItemProp),m_ItemUID(-1),m_IsLock(false),m_TaskID(0),m_itemLevel(0)
	{
		m_PlayID.dwPID = m_PlayID.wGID = 0;
	}
	virtual ~CBaseItem();

private:
	CBaseItem& operator=( const CBaseItem& pItem );
	CBaseItem( const CBaseItem& pItem );

public:
	void    SetItemUID( unsigned int itemnewUID ) { m_ItemUID = itemnewUID; }
	u32     GetItemUID() { return m_ItemUID ; }//唯一性ID
	u32     GetItemID() { return m_ItemPublicProp->GetItemID(); }//道具在XML表中的编号
	ITEM_TYPE       GetItemUseType() { return m_ItemPublicProp->m_itemType; }//获取道具的装备类型
	ITEM_TYPEID     GetItemTypeID() { return m_ItemPublicProp->GetItemTypeID(); }//道具的具体类型
	void    SetItemUID( s32 itemUID ) { m_ItemUID = itemUID; }
	void    SetItemOwner( PlayerID playerID ) { m_PlayID = playerID; }//设置道具的所有者
	PlayerID&   GetItemOwner() { return m_PlayID;}//取得道具的所有者
	s32     GetItemPrice() { return m_ItemPublicProp->m_itemPrice; }//获取装备的价钱
	bool    IsLock() { return m_IsLock;}
	void    Lock() { m_IsLock =true; }
	void    UnLock() { m_IsLock = false; }
	bool    IsCanUse( CPlayer* pUsePlayer, int playerRace, int playerClass, int playerLevel );
	bool	IsCanUseByProperty( CPlayer* pUsePlayer );//玩家属性点是否满足使用条件
	bool    IsReachMaxItemCount( int count ) { return count >= m_ItemPublicProp->m_itemMaxAmount; }
	unsigned int GetMaxItemCount() { return m_ItemPublicProp->m_itemMaxAmount; }
	int		MaxWear(void){return m_ItemPublicProp->m_maxWear;}
	int		RepairMode(void){return m_ItemPublicProp->m_repairMode;}
	unsigned int WearZeroMode(void) {return m_ItemPublicProp->m_wearOMode;}
	unsigned int WearConsumeMode(void) {return m_ItemPublicProp->m_wearConsumeMode;}
	unsigned int GetItemPlayerLevel(void) { return m_ItemPublicProp->m_itemLevel; }
	CItemPublicProp* GetItemPublicProp() { return m_ItemPublicProp; }//获取共有属性
	bool IsTaskItem() { return m_TaskID != 0 ; }//判断该道具是否是任务物品
	unsigned int GetTaskID() { return m_TaskID; }//获取该道具的任务ID
	void SetTaskID( unsigned int taskID ) { m_TaskID = taskID; }//设置任务物品ID
	void ClearTaskID() { m_TaskID = 0; }//清除绑定在道具上的ID号
	const char* GetItemName() { return m_ItemPublicProp->m_itemName.c_str(); }
	const char* GetItemClass() { return m_ItemPublicProp->m_itemClassStr.c_str();}
	char  GetItemAuctionType();//获得道具的拍卖类型
	int	  GetWidth();//获得道具实体模型的宽度
	int	  GetHeight();//获得道具实体模型的高度
	static int GetWidth(int m_itemTypeID);
	static int GetHeight(int m_itemTypeID);
	char  GetItemLevel() {return m_itemLevel;}

	virtual void  SetItemLevel(char itemLevel) {m_itemLevel = itemLevel;}//置升级属性

	//返回道具绑定类型
	ITEM_BIND_TYPE GetItemBindType() { return m_ItemPublicProp->mBindEquip; }

//M2新需求 获取道具的物品等级
	ITEM_MASSLEVEL GetItemMassLevel() { return m_ItemPublicProp->m_itemMassLevel; }

	///玩家与Item对话，nOption:玩家所选择的选项，==0表示初始选择NPC，任何新道具的使用都会重置旧道具阶段；
	bool OnChatOption( CPlayer* pOwner/*道具使用者*/, int nOption );

public:
	//@brief:初始化Item操作
	virtual bool Init() = 0;    

	//获取套装ID号
	virtual int GetSuitID();
	
	//将道具的加的属性给玩家
	virtual void AddItemPropToPlayer( CPlayer* pPlayer,const ItemInfo_i& itemInfo ) { ACE_UNUSED_ARG(pPlayer); ACE_UNUSED_ARG(itemInfo); };

	//将道具加的属性从玩家身上扣除
	virtual void SubItemPropToPlayer( CPlayer* pPlayer,const ItemInfo_i& itemInfo) { ACE_UNUSED_ARG(pPlayer); ACE_UNUSED_ARG(itemInfo); };

	//获取道具的装备位置
	virtual ITEM_GEARARM_TYPE GetGearArmType() { return E_POSITION_MAX; }

	//设置道具的共有属性
	void SetItemPublicProp( CItemPublicProp* pPublicProp )
	{
		if( NULL == pPublicProp ) 
		{
			D_ERROR( "SetItemPublicProp, NULL == pPublicProp\n" );
			return;
		}
		m_ItemPublicProp = pPublicProp;
		return;
	}
	//设置道具的特有属性
	virtual void SetItemSpecialProp( void* pItemProp ) {ACE_UNUSED_ARG(pItemProp);};

	//获取升级道具的概率ID号
	virtual unsigned int GetUpdateItemCriID() { return 0; }

	//获取升级道具的材料编号
	virtual unsigned int GetUpdateItemMaterialID() { return 0; }

	virtual int GetLevelUpLimit() { return -1; }

	virtual int GetItemSpeed() { return 0; }

	//直接通过luckyID与excellentID来决定是否卓越/幸运装备 int GetItemLuckyType() { return 0; } 

	virtual int GetItemLuckyID() { return 0; } 

	virtual int GetItemExcellentID(){ return 0; }

public:
	//物品装配要求属性点
	unsigned short GetEquipNeedStr() { return ((NULL != m_ItemPublicProp) ? m_ItemPublicProp->rq_strength:0) + LevelUpEquipNeedStr(); };
	unsigned short GetEquipNeedAgi() { return ((NULL != m_ItemPublicProp) ? m_ItemPublicProp->rq_agility:0) + LevelUpEquipNeedAgi(); };
	unsigned short GetEquipNeedInt() { return ((NULL != m_ItemPublicProp) ? m_ItemPublicProp->rq_intelligence:0) + LevelUpEquipNeedInt(); };
	unsigned short GetEquipNeedVit() { return ((NULL != m_ItemPublicProp) ? m_ItemPublicProp->rq_physique:0) + LevelUpEquipNeedVit(); };
	unsigned short GetEquipNeedSpi() { return ((NULL != m_ItemPublicProp) ? m_ItemPublicProp->rq_spirit:0) + LevelUpEquipNeedSpi(); };

	virtual unsigned short LevelUpEquipNeedStr() { return 0; }//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedAgi() { return 0; }//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedInt() { return 0; }//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedVit() { return 0; }//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedSpi() { return 0; }//升级后增加的装配对基本属性点要求

protected:
	CItemPublicProp* m_ItemPublicProp;//道具的共有属性
	s32              m_ItemUID;//道具的唯一性ID
	PlayerID         m_PlayID;//该道具所属的角色
	bool             m_IsLock;//道具是否被锁定
	unsigned  int    m_TaskID;//任务物品的ID号
	char			 m_itemLevel;//道具的道具等级
};

//@brief:武器道具 New_ItemSystem
class CWeaponItemEx : public CBaseItem
{
public:
	CWeaponItemEx( CItemPublicProp* pPublicItemProp,WeaponProperty* pWeaponImpl ):CBaseItem(pPublicItemProp),mpWeaponImpl(pWeaponImpl)
	{
		m_pLevelProp = NULL;
		//SetLevelUpProp();
	}
	virtual  ~CWeaponItemEx(){};

	//置武器升级属性;
	bool SetLevelUpProp();
	virtual void SetItemLevel(char itemLevel) 
	{
		CBaseItem::SetItemLevel(itemLevel);
		SetLevelUpProp();
	}

public:

	bool Init() { return true; }

	const WeaponProperty* GetWeaponProperty() { return mpWeaponImpl; }

	//获取套装的装备位置
	virtual ITEM_GEARARM_TYPE GetGearArmType() { return mpWeaponImpl->mGearArmType; }

	//套装ID号
	virtual int GetSuitID() { return mpWeaponImpl->SuitId; }

	//将道具的加的属性给玩家
	virtual void AddItemPropToPlayer( CPlayer* pPlayer,const ItemInfo_i& itemInfo  );

	//将道具的追加属性从玩家身上扣除
	virtual void SubItemPropToPlayer( CPlayer* pPlayer,const ItemInfo_i& itemInfo );

	virtual void SetItemSpecialProp( void* pItemProp )
	{ 
		if( NULL == pItemProp )
		{
			D_ERROR( "CWeaponItemEx::SetItemSpecialProp, NULL == pItemProp\n" );
			return; 
		}
		mpWeaponImpl = (WeaponProperty*)pItemProp;
		SetLevelUpProp();
	}

	virtual unsigned short LevelUpEquipNeedStr();//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedAgi();//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedInt();//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedVit();//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedSpi();//升级后增加的装配对基本属性点要求

public:
	virtual unsigned int GetUpdateItemCriID() { return mpWeaponImpl->itemCriID; }

	virtual unsigned int GetUpdateItemMaterialID() { return mpWeaponImpl->itemMaterialID; }

	virtual int GetLevelUpLimit() { return mpWeaponImpl->itemLevelUpLimit; }

	virtual int GetItemSpeed() { return mpWeaponImpl->AddSpeed; }

	//直接通过luckyID与excellentID来决定是否卓越/幸运装备 virtual int GetItemLuckyType(){ return mpWeaponImpl->luckytype; }

	virtual int GetItemLuckyID() { return mpWeaponImpl->luckid; }

	virtual int GetItemExcellentID() { return mpWeaponImpl->excellentid; }

private:
	CWeaponItemEx( CWeaponItemEx& weapon );
	CWeaponItemEx& operator=( const CWeaponItemEx& weapon );

private:
	WeaponProperty* mpWeaponImpl;
	LevelUpProp*    m_pLevelProp;//升级属性
};


//@brief:装备道具 New_ItemSystem
class CEquipItemEx : public CBaseItem
{
public:
	CEquipItemEx( CItemPublicProp* pPublicItemProp,EquipProperty* pEquipImpl ):CBaseItem(pPublicItemProp),mpEquipImpl(pEquipImpl)
	{
		m_pLevelProp = NULL;
		//SetLevelUpProp();
	}
	virtual ~CEquipItemEx(){};

	//置武器升级属性;
	bool SetLevelUpProp();
	virtual void SetItemLevel(char itemLevel) 
	{
		CBaseItem::SetItemLevel(itemLevel);
		SetLevelUpProp();
	}

public:

	bool Init() { return true; }

	const EquipProperty* GetEquipProperty() { return mpEquipImpl; }

	//获取道具的装备位置
	virtual ITEM_GEARARM_TYPE GetGearArmType() { return mpEquipImpl->mGearArmType; }

	//将道具的加的属性给玩家
	virtual void AddItemPropToPlayer(  CPlayer* pPlayer,const ItemInfo_i& itemInfo  );

	//卸载装备时，将该装备加给玩家的属性扣除
	virtual void SubItemPropToPlayer(  CPlayer* pPlayer,const ItemInfo_i& itemInfo );

	//套装ID号
	virtual int GetSuitID() { return mpEquipImpl->SuitId; }

	//设置装备的特有属性
	virtual void SetItemSpecialProp(void* pItemProp )
	{
		if( NULL == pItemProp )
		{
			D_ERROR( "CEquipItemEx::SetItemSpecialProp, NULL == pItemProp\n" );
			return; 
		}
		//原来一直都错了吗？by dzj, 10.05.24, if ( !mpEquipImpl )
		//	return;
		mpEquipImpl = (EquipProperty* )pItemProp;
		SetLevelUpProp();
	}

	virtual unsigned short LevelUpEquipNeedStr();//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedAgi();//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedInt();//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedVit();//升级后增加的装配对基本属性点要求
	virtual unsigned short LevelUpEquipNeedSpi();//升级后增加的装配对基本属性点要求

	virtual unsigned int GetUpdateItemCriID() { return mpEquipImpl->itemCriID; }

	virtual unsigned int GetUpdateItemMaterialID() { return mpEquipImpl->itemMaterialID; }

	virtual int GetLevelUpLimit() { return mpEquipImpl->itemLevelUpLimit; }

	virtual int GetItemSpeed() { return mpEquipImpl->AddSpeed; }

	//直接通过luckyID与excellentID来决定是否卓越/幸运装备 virtual int GetItemLuckyType(){ return mpEquipImpl->luckytype; }

	virtual int GetItemLuckyID() { return mpEquipImpl->luckid; }

	virtual int GetItemExcellentID() { return mpEquipImpl->excellentid; }

private:
	CEquipItemEx( const CEquipItemEx& equip );
	CEquipItemEx& operator=( const CEquipItemEx& equip );

private:
	EquipProperty*  mpEquipImpl;
	LevelUpProp*    m_pLevelProp;//升级属性
};

//消耗品
class CConsumeItem : public CBaseItem
{
public:
	CConsumeItem( CItemPublicProp* pPublicItemProp, ConsumeProperty* pConsumeImpl ):CBaseItem(pPublicItemProp),mpConsumeImpl(pConsumeImpl){}
	virtual ~CConsumeItem(){};

public:
	bool Init() { return true; }

	const ConsumeProperty* GetConsumeProperty() { return mpConsumeImpl; }

private:
	CConsumeItem( const CConsumeItem& consume );
	CConsumeItem& operator=( const CConsumeItem& consume );

private:
	ConsumeProperty* mpConsumeImpl;
};

//功能性物品
class CFunctionItem:public CBaseItem
{
public:
	CFunctionItem( CItemPublicProp* pPublicItemProp, FunctionProperty* pFunctionProp ):CBaseItem(  pPublicItemProp ),mFunctionImpl(pFunctionProp){}
public:
	bool Init() { return true; }

	const FunctionProperty* GetFunctionProperty() { return mFunctionImpl; }

private:
	CFunctionItem( const CFunctionItem& funcItem );
	CFunctionItem& operator=( const CFunctionItem& funcItem );

private:
	FunctionProperty* mFunctionImpl;
};

//任务物品
class CTaskItem:public CBaseItem
{
public:
	CTaskItem( CItemPublicProp* pPublicItemProp , TaskProperty* taskProp ):CBaseItem( pPublicItemProp), mTaskProp(taskProp){}

public:
	 bool Init() { return true; }

private:
	CTaskItem( CTaskItem& task );
	CTaskItem& operator=( CTaskItem& task );

private:
	TaskProperty* mTaskProp;
};

//价值品
class CAxiologyItem:public CBaseItem
{
public:
	CAxiologyItem( CItemPublicProp* pPublicItemProp , AxiologyProperty* AxiologyProp ):CBaseItem( pPublicItemProp ),mAxiologyProp(AxiologyProp){}

public:
	virtual bool Init() { return true; }

	const AxiologyProperty* GetAxiologyItemProperty() { return mAxiologyProp ; }
private:
	CAxiologyItem& operator=( const CAxiologyItem& axiology );
	CAxiologyItem( CAxiologyItem& axiology );

private:
	AxiologyProperty* mAxiologyProp;
};

class CBulePrint;
//图纸道具
class CBulePrintItem:public CBaseItem
{
public:
	explicit CBulePrintItem( CItemPublicProp* pPublicItemProp, CBulePrint* pBuleProp ):CBaseItem( pPublicItemProp ),mBulePrint(pBuleProp) {}

public:
	virtual bool Init() { return true; }

	CBulePrint* GetBulePrintProperty() { return mBulePrint; }

private:
	CBulePrintItem& operator =( const CBulePrintItem& );
	CBulePrintItem( CBulePrintItem& );
private:
	CBulePrint* mBulePrint; 
};


class CSkillBookItem:public CBaseItem
{
public:
	CSkillBookItem( CItemPublicProp* pPublicItemProp,SkillBookProperty* skillProp  ):CBaseItem( pPublicItemProp ),mSkillBookProp(skillProp){}

public:
	virtual bool Init() { return true; }

	const SkillBookProperty* GetSkillBookProperty() { return mSkillBookProp; }

private:
	CSkillBookItem( const CSkillBookItem& skillbook );
	CSkillBookItem& operator= ( const CSkillBookItem& skillbook );
private:
	SkillBookProperty* mSkillBookProp;
};

//时装
class CFashionItem : public CBaseItem
{
public:
	CFashionItem( CItemPublicProp* pPublicItemProp, FashionProperty* pFashionImpl ):CBaseItem(pPublicItemProp),mpFashionImpl(pFashionImpl){}
	virtual ~CFashionItem(){};

public:
	bool Init() { return true; }

	virtual ITEM_GEARARM_TYPE GetGearArmType() { return mpFashionImpl->gearArmType; }

	const FashionProperty* GetFashionProperty() { return mpFashionImpl; }

private:
	CFashionItem( const CFashionItem& fashion );
	CFashionItem& operator=( const CFashionItem& fashion );

private:
	FashionProperty* mpFashionImpl;
};

//骑乘装备
class CMountItem:public CBaseItem
{
public:
	CMountItem( CItemPublicProp* pPublicItemProp, MountProperty* pMountImpl ):CBaseItem( pPublicItemProp ),mMountPropImpl(pMountImpl){}
	virtual ~CMountItem(){};

public:
	bool Init() { return  true; }

	virtual ITEM_GEARARM_TYPE GetGearArmType() { return E_DRIVE; }

	const MountProperty* GetMountProperty() { return mMountPropImpl; }

	virtual unsigned int GetUpdateItemCriID() { return mMountPropImpl->itemCriID; }

	virtual unsigned int GetUpdateItemMaterialID() { return mMountPropImpl->itemMaterialID; }

	virtual int GetLevelUpLimit() { return mMountPropImpl->itemLevelUpLimit; }

	virtual int GetItemSpeed() { return mMountPropImpl->AddSpeed; }

public:
	//将道具的加的属性给玩家
	virtual void AddItemPropToPlayer(  CPlayer* pPlayer,const ItemInfo_i& itemInfo  );

	//卸载装备时，将该装备加给玩家的属性扣除
	virtual void SubItemPropToPlayer(  CPlayer* pPlayer,const ItemInfo_i& itemInfo );

private:
	CMountItem( const CMountItem& mountItem);
	CMountItem& operator=( const CMountItem& mountItem );

private:
	MountProperty* mMountPropImpl;
};

//class CMoneyItem:public CBaseItem
//{
//public:
//
//	bool Init() { return true; }
//
//	CMoneyItem(long money):CBaseItem(NULL),mMoney(money){}
//
//	//取得金钱数目
//	long GetMoney() { return mMoney; }
//
//	//返回金钱类型
//	virtual ITEM_TYPEID GetItemTypeID() { return E_TYPE_MONEY; }
//
//private:
//	long mMoney;
//};

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
////功能物品
//class CFunctionItem:public CBaseItem
//{
//public:
//	virtual bool Init( s32 itemID );
//
//public:
//	CFunctionItem( CItemPublicProp* pPublicItemProp,FunctionItemProp* pItemProp ):CBaseItem(pPublicItemProp),mItemProp(pItemProp){}
//	virtual ~CFunctionItem(){};
//private:
//	FunctionItemProp* mItemProp;
//};
//
//
////技能物品
//class CSkillItem:public CBaseItem
//{
//public:
//	virtual bool Init();
//
//public:
//	CSkillItem( CItemPublicProp* pPublicItemProp, SkillItemProp* pItemProp ):CBaseItem(pPublicItemProp),mSkillProp(pItemProp){}
//	virtual  ~CSkillItem(){};
//private:
//	SkillItemProp* mSkillProp;
//};
//
//
////任务物品
//class CTaskItem:public CBaseItem
//{
//public:
//	virtual bool Init();
//
//public:
//	CTaskItem( CItemPublicProp* pPublicItemProp, TaskItemProp* pItemProp):CBaseItem(pPublicItemProp),mTaskProp(pItemProp){}
//	virtual ~CTaskItem(){};
//private:
//	TaskItemProp* mTaskProp;
//};
//
////药品
//class CDrugItem:public CBaseItem
//{
//public:
//	virtual  bool Init();
//
//public:
//	CDrugItem( CItemPublicProp* pPublicItemProp,DrugItemProp* pItemProp):CBaseItem(pPublicItemProp),mDrugProp(pItemProp){}
//	virtual ~CDrugItem(){};
//
//private:
//	DrugItemProp* mDrugProp;
//};

//武器物品
//class CWeaponItem:public CBaseItem
//{
//public:
//	virtual bool Init();
//
//	virtual bool InitialItemInfo(const EquipItemInfo& itemInfo );
//
//public:
//	CWeaponItem( CItemPublicProp* pPublicItemProp,WeaponSecondProp* pItemProp):CBaseItem(pPublicItemProp),mSecondProp(pItemProp){}
//	virtual ~CWeaponItem(){};
//
//public:
//	virtual   EquipAdditonInfo* GetEquipAdditionInfo() { return &mAdditionProp.addInfo; }//获取附加属性
//	
//	WeaponPeculiarProp* GetWeaponPeculiarProperty() { return &mAdditionProp.peculiarProp; }//获取武器的特有属性
//	
//	virtual void GetItemInfo( EquipItemInfo& itemInfo );
//	
//	virtual ITEM_GEARARM_TYPE GetGearArmType() { return mSecondProp->mGearArmType; }
//	
//	//当玩家查询道具包中的武器信息时
//	virtual void OnPlayerQueryItemPkgInfo( CPlayer* pPlayer ,unsigned pkgIndex );
//	
//
//private:
//	WeaponSecondProp* mSecondProp;
//	WeaponAdditionProp mAdditionProp;
//};
//
//
////其他物品
//class COtherItem:public CBaseItem
//{
//public:
//	virtual bool Init();
//
//	virtual bool InitialItemInfo(const EquipItemInfo& itemInfo );
//
//public:
//	COtherItem( CItemPublicProp* pPublicItemProp, EquipSecondProp* pItemProp):CBaseItem(pPublicItemProp),mSecondProp(pItemProp){}
//	virtual ~COtherItem(){};
//
//public:
//
//	EquipPeculiarProp* GetEquipPeculiarProperty() { return &mAddititonProp.peculiarProp; }//获取装备的特有属性
//
//	virtual   EquipAdditonInfo* GetEquipAdditionInfo() { return &mAddititonProp.addInfo; }//获取装备的附加属性
//	
//	virtual void GetItemInfo( EquipItemInfo& itemInfo );
//
//	virtual ITEM_GEARARM_TYPE GetGearArmType() { return mSecondProp->mGearArmType; }
//
//	//当武器查询包裹中的道具信息时
//	virtual void OnPlayerQueryItemPkgInfo(  CPlayer* pPlayer ,unsigned pkgIndex );
//	
//private:
//	EquipSecondProp* mSecondProp;
//	EquipAdditionProp mAddititonProp;
//	
//};

#endif

