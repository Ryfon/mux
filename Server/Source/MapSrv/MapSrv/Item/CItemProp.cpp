﻿
#include "CItemProp.h"
#include "CItemPropManager.h"

//取属性中的随机集号(内部高16bit为卓越，低16bit为幸运)
//    装备掉落时用到，有可能掉落装备是否卓越/幸运会在运营过程中改变，因此这一随机集号属性只在掉落一刻发生作用，并相应地记至具体item实例上；
unsigned int CItemPublicProp::GetPropRandSet( bool& isLucky, bool& isExcellent )
{
	unsigned int randSet = 0;
	isLucky     =  false;
	isExcellent =  false;
	if ( m_itemTypeID == E_TYPE_WEAPON )
	{
		WeaponProperty* pWeaponProp = CWeaponPropertyManagerSingle::instance()->GetSpecialPropertyT( m_itemID );
		if ( NULL != pWeaponProp )
		{
			isLucky = (pWeaponProp->luckid > 0);
			isExcellent = (pWeaponProp->excellentid > 0);
			randSet |= pWeaponProp->luckid;
			randSet |= pWeaponProp->excellentid << 16;			
		}
	} else if( m_itemTypeID == E_TYPE_EQUIP ) {
		EquipProperty* pEquipProp =  CEquipPropertyManagerSingle::instance()->GetSpecialPropertyT( m_itemID );
		if ( NULL != pEquipProp )
		{
			isLucky = (pEquipProp->luckid > 0);
			isExcellent = (pEquipProp->excellentid > 0);
			randSet |= pEquipProp->luckid;
			randSet |= pEquipProp->excellentid << 16;			
		}
	}

	return randSet;
}