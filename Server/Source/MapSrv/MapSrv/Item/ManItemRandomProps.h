﻿
/*
  道具随机属性管理
  by dzj, 05.18
*/

#ifndef MAN_ITEM_RANDOM_PROPS
#define MAN_ITEM_RANDOM_PROPS

#include "Rand.h"

#ifdef FOR_CLI //用于客户端
#define D_ERROR(...) 
#define ARRAY_SIZE(x) ( sizeof(x)/sizeof(x[0]) )
#else //FOR_CLI
#include "Utility.h"
#endif //FOR_CLI

#ifdef WIN32
#include <hash_map>
#define HASH_MAP stdext::hash_map
#else //WIN32
#include <ext/hash_map>
#define HASH_MAP __gnu_cxx::hash_map
#endif //WIN32

typedef ItemRandAddAtt RPropID;

struct GenedProps;

//随机属性生成描述；
class CRandProp
{
public:
	CRandProp()
	{
		ResetRandProp();
	}

	bool ResetRandProp()
	{
		m_attrID = m_attrID;
		m_Weight = 0;
		m_minVal = 0;
		m_maxVal = 0;
		return true;
	}

public:
	inline RPropID GetRPropID() const { return m_attrID; }
	inline unsigned int GetWeight() const { return m_Weight; }
	inline unsigned short GetRandVal() const
	{ 
		return m_minVal + CRandGen::GetRand()%(m_maxVal-m_minVal+1); 
	}

	//置值;
	inline bool SetRandPropVal( RPropID attrID, unsigned int weight, unsigned short minVal, unsigned short maxVal ) 
	{
		m_attrID = attrID;//Player::GetItemAttributeByAttriID(unsigned int attributeID )
		m_Weight = weight;//在集中所占权重
		m_minVal = minVal;//随出来的最小值
		m_maxVal = maxVal;//随出来的最大值
		return true;
	}

private:
	/*<RandomProp  Attribute="1" Weight="20" MinVal="10" MaxVal="30" />*/
	RPropID        m_attrID;//Player::GetItemAttributeByAttriID(unsigned int attributeID )
	unsigned int   m_Weight;//在集中所占权重
	unsigned short m_minVal;//随出来的最小值
	unsigned short m_maxVal;//随出来的最大值
};//class CRandProp

//随机属性集
class CRandSet
{
public:
	static const unsigned int RANDPROP_MAXNUM = 6;//最多随机属性数量

public:
	CRandSet()
	{
		ResetRandSet();
	}

	bool ResetRandSet()
	{
		m_randSetID  = 0;
		m_minPropNum = 0;
		m_maxPropNum = 0;
		m_arrPropNum = 0;
		m_weightSum = 0;
		return true;
	}

public:
	bool RandSetGetProps( GenedProps& outProps );

public:
	inline unsigned int GetSetID() const { return m_randSetID; }

	inline bool SetRandSetInfo( unsigned int randSetID, unsigned int minPropNum, unsigned int maxPropNum )
	{
		m_randSetID = randSetID, m_minPropNum = minPropNum, m_maxPropNum = maxPropNum;
		return true;
	}

	bool AddOneRandProp( const CRandProp& randProp )
	{
		if ( m_arrPropNum >= ARRAY_SIZE(m_arrProps) )
		{
			D_ERROR("CRandSet:;AddOneRandProp，超出数组范围\n");
			return false;
		}
		m_arrProps[m_arrPropNum++] = randProp;
		m_weightSum += randProp.GetWeight();//总权值，用于之后计算
		return true;
	}

	inline unsigned int GetWeightSum() { return m_weightSum; };

private:
	unsigned int m_randSetID;//随机集号；

private:
	unsigned int m_weightSum;//总权值；

private:
	unsigned int m_minPropNum;//随机属性个数最小值
	unsigned int m_maxPropNum;//随机属性个数最大值

	unsigned int m_arrPropNum;//随机属性数组大小；
	CRandProp  m_arrProps[RANDPROP_MAXNUM];
};//class CRandSet

//最终得到的某个Item的幸运/卓越属性
struct GenedProps
{
public:
	GenedProps()
	{
		propNum = 0;
	}

public:

	//添加一个随得的属性；
	bool AddProp( RPropID propID, short propVal )
	{
		if ( propNum >= ARRAY_SIZE(arrPropID) )
		{
			D_ERROR("GenedProps::AddProp，数量越界\n");
			return false;
		}
		arrPropID[propNum] = propID;
		arrPropVal[propNum] = propVal;
		++propNum;
		return true;
	}

	//取内部属性数目
	inline unsigned int GetPropNum() { return propNum; }
	//取第index个属性的类型与值
	inline bool GetIndexProp( unsigned int index, RPropID& propID, short& propVal )
	{
		if ( ( index >= propNum ) || ( index >= ARRAY_SIZE(arrPropID) ) )
		{
			D_ERROR( "GetIndexProp，输入索引%d错\n", index );
			return false;
		}
		propID = arrPropID[index];
		propVal = arrPropVal[index];
		return true;
	}

private:
	unsigned int propNum;//属性个数
	RPropID      arrPropID[CRandSet::RANDPROP_MAXNUM];//随到的各随机属性ID；
	short        arrPropVal[CRandSet::RANDPROP_MAXNUM];//随到的各随机属性值；	
}; //struct GenedProps

//随机属性管理
class CManItemRandSets
{
public:
	static bool AddOneRandSets( const CRandSet& randSet )
	{
		if ( m_randSets.end() != m_randSets.find(randSet.GetSetID()) )
		{
			D_ERROR( "重复的随机集%d", randSet.GetSetID() );
			return false;
		}
		m_randSets.insert( std::pair<unsigned int, CRandSet>(randSet.GetSetID(), randSet) );
		return true;
	}

	//输入生成信息，得到对应的随机属性以及随机值, 返回值：false 无对应属性
	static bool GetRandProps( bool luckyOrExcel/*true:幸运,false:卓越*/, unsigned int orgInfo, unsigned int randSetID, GenedProps& outProps );

private:
	static HASH_MAP<unsigned int, CRandSet> m_randSets;//所有的道具随机属性集；
}; //class CManItemRandSets

#endif //MAN_ITEM_RANDOM_PROPS


