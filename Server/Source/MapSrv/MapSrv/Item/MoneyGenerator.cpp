﻿#include "MoneyGenerator.h"
#include "XmlManager.h"
#include <vector>
#include "../../../Base/Utility.h"





void CSetMoneyMap::PushRule(CElement* pXMLElement )
{
	if ( !pXMLElement )
		return;

	long setID = ACE_OS::atoi( pXMLElement->GetAttr("LootName") );

	MoneyRule* moneyRule = NEW MoneyRule;
	if ( moneyRule )
	{
		moneyRule->money   = atoi( pXMLElement->GetAttr("Money") );
		moneyRule->randmin = (float)atof( pXMLElement->GetAttr("RandomMin") );
		moneyRule->randmax = (float)atof( pXMLElement->GetAttr("RandomMax") );

		mMoneyRule.insert( std::pair<long,MoneyRule*>(setID,moneyRule) );
	}
}


void MoneyGenerator::LoadXML(const char* pxmlFilePath/*,const char* luaScriptPath */)
{
	if ( !pxmlFilePath )
		return;

	

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pxmlFilePath))
		return;

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
		return;

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);


	std::vector<CElement *>::iterator lter = childElements.begin();
	for ( ; lter != childElements.end(); ++lter )
	{
		CElement* pxmlElement = *lter;
		if ( !pxmlElement )
			return;

		mSetMoneyMap.PushRule( pxmlElement );
	}

	TRY_END;
}

long MoneyGenerator::Generator(long setID)
{
	long money = -1;
	MoneyRule* pMoneyRule = mSetMoneyMap.GetRuleBySetID( setID );
	if ( pMoneyRule )
	{
		float randmax = pMoneyRule->randmax;
		float randmin = pMoneyRule->randmin;
		int rank    = pMoneyRule->money;
		if( RandNumber(1,100) <= rank )
		{
			money = RandNumber( (int)randmin,(int)randmax );
		}
		else
			money = 0;
	}
	return money;
}
