﻿#ifndef REPAIR_WEAR_RULE_H
#define REPAIR_WEAR_RULE_H


#include "../../../Base/aceall.h"


class CRepairWearRuleManager
{
	friend class ACE_Singleton<CRepairWearRuleManager, ACE_Null_Mutex>;
public:
	enum eWearIndex
	{
		LEVEL_INDEX = 0,
		MASS_LEVEL_INDEX = 1,
		LEVEL_UP_INDEX = 2
	};
	
private:
	/// 构造
	CRepairWearRuleManager(void);

	/// 析构
	~CRepairWearRuleManager(void);

public:
	int LoadXML(const char* pszFileName);

	unsigned int CalcRepairWearMoney(unsigned int uiWear, unsigned short usLevel, unsigned short usMassLevel, unsigned short usLevelUp);

private:
	int GetValue(eWearIndex tIndex1, int iIndex2);

private:
	int m_wear[3][50];
	unsigned short INVALD_VALUE;

};

typedef ACE_Singleton<CRepairWearRuleManager, ACE_Null_Mutex> CRepairWairRuleManagerSingle;

#endif/*REPAIR_WEAR_RULE_H*/
