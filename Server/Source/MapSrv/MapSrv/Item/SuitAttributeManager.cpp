﻿#include "SuitAttributeManager.h"
#include "XmlManager.h"
#include "../MuxMap/muxmapbase.h"


CSuit::CSuit(CElement* pxmlElement )
{
	suitEquipID = 0;

	if ( !pxmlElement )
		return;


	if ( pxmlElement->GetAttr("ID") )
		suitID = ACE_OS::atoi( pxmlElement->GetAttr("ID") );
	else
		suitID = 0;

	//该套装的部件要求
	//head
	if ( pxmlElement->GetAttr("Head") )
	{
		int head = ACE_OS::atoi( pxmlElement->GetAttr("Head") );
		if ( head )
		{
			suitEquipID |= 1 << EQUIPTYPE_POS[E_HEAD];
		}
	}

	//body
	if ( pxmlElement->GetAttr("Body") )
	{
		int Body = ACE_OS::atoi( pxmlElement->GetAttr("Body") );
		if ( Body )
		{
			suitEquipID |= 1<< EQUIPTYPE_POS[E_BODY];
		}
	}
	
	//hand
	if ( pxmlElement->GetAttr("Hand") )
	{
		int Hand = ACE_OS::atoi( pxmlElement->GetAttr("Hand") );
		if ( Hand )
		{
			suitEquipID |= 1<< EQUIPTYPE_POS[E_HAND];
		}
	}

	if ( pxmlElement->GetAttr("Foot") )
	{
		//foot
		int Foot = ACE_OS::atoi( pxmlElement->GetAttr("Foot") );
		if ( Foot )
		{
			suitEquipID |= 1<< EQUIPTYPE_POS[E_FOOT];
		}
	}

	if ( pxmlElement->GetAttr("Shoulder") )
	{
		//shoulder
		int Shoulder = ACE_OS::atoi( pxmlElement->GetAttr("Shoulder") );
		if ( Shoulder )
		{
			suitEquipID |= 1<< EQUIPTYPE_POS[E_SHOULDER];
		}
	}

	if ( pxmlElement->GetAttr("Back") )
	{
		//Back
		int Back = ACE_OS::atoi( pxmlElement->GetAttr("Back") );
		if ( Back )
		{
			suitEquipID |= 1<< EQUIPTYPE_POS[E_BACK];
		}
	}

	if ( pxmlElement->GetAttr("Torque") )
	{
		//Torque
		int Torque = ACE_OS::atoi( pxmlElement->GetAttr("Torque") );
		if ( Torque )
		{
			suitEquipID |= 1<< EQUIPTYPE_POS[E_NECKLACE];
		}
	}
	
	if ( pxmlElement->GetAttr("Ring1" ) )
	{
		//ring1
		int Ring1 = ACE_OS::atoi( pxmlElement->GetAttr("Ring1") );
		if ( Ring1 )
		{
			D_ERROR( "指环已废弃，套装Ring1属性配置,suitID%d", suitID );
			//suitEquipID |= 1<< EQUIPTYPE_POS[E_RING];
		}
	}

	if ( pxmlElement->GetAttr("Ring2") )
	{
		//ring2
		int Ring2 = ACE_OS::atoi( pxmlElement->GetAttr("Ring2") );
		if ( Ring2 )
		{
			D_ERROR( "指环已废弃，套装Ring2属性配置,suitID%d", suitID );
		}
	}
	
	if ( pxmlElement->GetAttr("Weapon1") )
	{
		int Weapon1 = ACE_OS::atoi( pxmlElement->GetAttr("Weapon1") );
		if ( Weapon1 )
		{
			suitEquipID |= 1<<EQUIPTYPE_POS[E_FIRSTHAND] ;
		}
	}

	if ( pxmlElement->GetAttr("Weapon2") )
	{
		int Weapon2 = ACE_OS::atoi( pxmlElement->GetAttr("Weapon2") );
		if ( Weapon2 )
		{
			suitEquipID |= 1<< EQUIPTYPE_POS[E_SECONDHAND];
		}
	}

	if ( pxmlElement->GetAttr("Weapon3") )
	{
		int Weapon3 = ACE_OS::atoi( pxmlElement->GetAttr("Weapon3") );
		if ( Weapon3 )
		{
			suitEquipID |= 1<< EQUIPTYPE_POS[E_DOUBLEHAND]/*EQUIPTYPE_POS[PlayerInfo_2_Equips::INDEX_TWO_HAND]*/;
		}
	}
	
	if ( pxmlElement->GetAttr("Mounts") )
	{
		int Mounts = ACE_OS::atoi( pxmlElement->GetAttr("Mounts") );
		if ( Mounts )
		{
			suitEquipID |= 1<<  EQUIPTYPE_POS[E_DRIVE];
		}
	}

	//该套装的属性
	mSuitAddtionProp.AttackPhyMin = ACE_OS::atoi( pxmlElement->GetAttr("AttackPhyMin") );
	mSuitAddtionProp.AttackPhyMax = ACE_OS::atoi( pxmlElement->GetAttr("AttackPhyMax") );
	mSuitAddtionProp.AttackMagMin = ACE_OS::atoi( pxmlElement->GetAttr("AttackMagMin") );
	mSuitAddtionProp.AttackMagMax = ACE_OS::atoi( pxmlElement->GetAttr("AttackMagMax") );
	mSuitAddtionProp.Hp   = ACE_OS::atoi( pxmlElement->GetAttr("Hp") );
	mSuitAddtionProp.Mp   = ACE_OS::atoi( pxmlElement->GetAttr("Mp") );
	mSuitAddtionProp.PhysicsAttack   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttack") );
	mSuitAddtionProp.MagicAttack   = ACE_OS::atoi( pxmlElement->GetAttr("MagicAttack") );
	mSuitAddtionProp.PhysicsDefend   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDefend") );
	mSuitAddtionProp.MagicDefend   = ACE_OS::atoi( pxmlElement->GetAttr("MagicDefend") );
	mSuitAddtionProp.PhysicsHit   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsHit") );
	mSuitAddtionProp.MagicHit   = ACE_OS::atoi( pxmlElement->GetAttr("MagicHit") );
	mSuitAddtionProp.PhysicsCritical   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCritical") );
	mSuitAddtionProp.MagicCritical   = ACE_OS::atoi( pxmlElement->GetAttr("MagicCritical") );
	mSuitAddtionProp.PhysicsAttackPercent   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttackPercent") );
	mSuitAddtionProp.MagicAttackPercent   = ACE_OS::atoi( pxmlElement->GetAttr("MagicAttackPercent") );
	mSuitAddtionProp.PhysicsCriticalDamage   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDamage") );
	mSuitAddtionProp.MagicCriticalDamage   = ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDamage") );
	mSuitAddtionProp.PhysicsDefendPercent   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDefendPercent") );
	mSuitAddtionProp.MagicDefendPercent   = ACE_OS::atoi( pxmlElement->GetAttr("MagicDefendPercent") );
	mSuitAddtionProp.PhysicsCriticalDerate   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDerate") );
	mSuitAddtionProp.MagicCriticalDerate   = ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDerate") );
	mSuitAddtionProp.PhysicsCriticalDamageDerate   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsCriticalDamageDerate") );
	mSuitAddtionProp.MagicCriticalDamageDerate   = ACE_OS::atoi( pxmlElement->GetAttr("MagicCriticalDamageDerate") );
	mSuitAddtionProp.PhysicsJouk   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsJouk") );
	mSuitAddtionProp.MagicJouk   = ACE_OS::atoi( pxmlElement->GetAttr("MagicJouk") );
	mSuitAddtionProp.PhysicsDeratePercent   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsDeratePercent") );
	mSuitAddtionProp.MagicDeratePercent   = ACE_OS::atoi( pxmlElement->GetAttr("MagicDeratePercent") );
	mSuitAddtionProp.Stun   = ACE_OS::atoi( pxmlElement->GetAttr("Stun") );
	mSuitAddtionProp.Tie   = ACE_OS::atoi( pxmlElement->GetAttr("Tie") );
	mSuitAddtionProp.Antistun   = ACE_OS::atoi( pxmlElement->GetAttr("Antistun") );
	mSuitAddtionProp.Antitie   = ACE_OS::atoi( pxmlElement->GetAttr("Antitie") );
	mSuitAddtionProp.PhysicsAttackAdd   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsAttackAdd") );
	mSuitAddtionProp.MagicAttackAdd   = ACE_OS::atoi( pxmlElement->GetAttr("MagicAttackAdd") );
	mSuitAddtionProp.PhysicsRift   = ACE_OS::atoi( pxmlElement->GetAttr("PhysicsRift") );
	mSuitAddtionProp.MagicRift   = ACE_OS::atoi( pxmlElement->GetAttr("MagicRift") );
	mSuitAddtionProp.AddSpeed   = ACE_OS::atoi( pxmlElement->GetAttr("AddSpeed") );
}
//
//bool CSuit::IsReachSuitCondition( CPlayer * pPlayer )
//{
//	if ( !pPlayer )
//		return false;
//
//	//一次检查套装的装备位置
//	PlayerInfo_2_Equips& equipInfo = pPlayer->GetFullPlayerInfo()->equipInfo;
//	int count =  (int)suitEquipIndexVec.size();
//
//	for ( int i = 0; i< count; i++ )
//	{
//		int equipIndex = suitEquipIndexVec[i];
//		ItemInfo_i& itemInfo = equipInfo.equips[equipIndex];
//		if ( itemInfo.uiID != PlayerInfo_2_Equips::INVALIDATE  )
//		{
//			CBaseItem* pItem = ItemManagerSingle::instance()->GetItemByItemUID( itemInfo.uiID );
//			if ( !pItem )
//				return false;
//
//			if( pItem->GetSuitID() != suitID )
//				return false;
//		}
//		else
//			return false;
//	}
//
//	return true;
//}

void CSuit::AddSuitAttributeToPlayer(CPlayer * pPlayer )
{
	if ( !pPlayer )
		return;

	ItemAddtionProp& itemAddtionProp = pPlayer->GetItemAddtionProp();

	itemAddtionProp.AddSpeed += mSuitAddtionProp.AddSpeed;
	itemAddtionProp.Antistun += mSuitAddtionProp.Antistun;
	itemAddtionProp.Antitie  += mSuitAddtionProp.Antitie;
	itemAddtionProp.AttackMagMax += mSuitAddtionProp.AttackMagMax;
	itemAddtionProp.AttackMagMin += mSuitAddtionProp.AttackMagMin;
	itemAddtionProp.AttackPhyMax += mSuitAddtionProp.AttackPhyMax;
	itemAddtionProp.AttackPhyMin += mSuitAddtionProp.AttackPhyMin;
	itemAddtionProp.MagicAttack  += mSuitAddtionProp.MagicAttack;
	itemAddtionProp.MagicAttackAdd += mSuitAddtionProp.MagicAttackAdd;
	itemAddtionProp.MagicAttackPercent += mSuitAddtionProp.MagicAttackPercent;
	itemAddtionProp.MagicCritical += mSuitAddtionProp.MagicCritical;
	itemAddtionProp.MagicCriticalDamage += mSuitAddtionProp.MagicCriticalDamage;
	itemAddtionProp.MagicCriticalDamageDerate += mSuitAddtionProp.MagicCriticalDamageDerate;
	itemAddtionProp.MagicCriticalDerate += mSuitAddtionProp.MagicCriticalDerate;
	itemAddtionProp.MagicDefend += mSuitAddtionProp.MagicDefend;
	itemAddtionProp.MagicDefendPercent += mSuitAddtionProp.MagicDefendPercent;
	itemAddtionProp.MagicDeratePercent += mSuitAddtionProp.MagicDeratePercent;
	itemAddtionProp.MagicHit += mSuitAddtionProp.MagicHit;
	itemAddtionProp.MagicJouk += mSuitAddtionProp.MagicJouk;
	itemAddtionProp.MagicRift += mSuitAddtionProp.MagicRift;
	itemAddtionProp.PhysicsCriticalDamage += mSuitAddtionProp.PhysicsCriticalDamage;
	itemAddtionProp.PhysicsCritical += mSuitAddtionProp.PhysicsCritical;
	itemAddtionProp.PhysicsCriticalDamageDerate += mSuitAddtionProp.PhysicsCriticalDamageDerate;
	itemAddtionProp.PhysicsCriticalDerate += mSuitAddtionProp.PhysicsCriticalDerate;
	itemAddtionProp.PhysicsDefend += mSuitAddtionProp.PhysicsDefend;
	itemAddtionProp.PhysicsDefendPercent += mSuitAddtionProp.PhysicsDefendPercent;
	itemAddtionProp.PhysicsDeratePercent += mSuitAddtionProp.PhysicsDeratePercent;
	itemAddtionProp.PhysicsHit += mSuitAddtionProp.PhysicsHit;
	itemAddtionProp.PhysicsJouk += mSuitAddtionProp.PhysicsJouk;
	itemAddtionProp.PhysicsRift += mSuitAddtionProp.PhysicsRift;
	itemAddtionProp.PhysicsAttack += mSuitAddtionProp.PhysicsAttack;
	itemAddtionProp.PhysicsAttackAdd += mSuitAddtionProp.PhysicsAttackAdd;
	itemAddtionProp.PhysicsAttackPercent += mSuitAddtionProp.PhysicsAttackPercent;
	itemAddtionProp.Mp += mSuitAddtionProp.Mp;
	itemAddtionProp.Hp += mSuitAddtionProp.Hp;

	//int oldmp = itemAddtionProp.Mp;
	//int oldhp = itemAddtionProp.Hp;
	//CNewMap* pMap= pPlayer->GetCurMap();
	//if ( !pMap )
	//	return;

	//MuxMapBlock* pBlock = pMap->GetMapBlockByGrid( pPlayer->GetPosX(), pPlayer->GetPosY() );
	//if ( !pBlock )
	//	return;

	//if ( oldmp != itemAddtionProp.Mp )
	//{
	//	MGRoleattUpdate attrUpdate;
	//	attrUpdate.lChangedPlayerID = pPlayer->GetFullID();
	//	attrUpdate.nOldValue = 0;
	//	attrUpdate.nNewValue = pPlayer->GetMaxMP();
	//	attrUpdate.nType = RT_MPMAX;
	//	attrUpdate.changeTime = 0;
	//	attrUpdate.nAddType = MGRoleattUpdate::TYPE_SET;	//直接设置
	//	attrUpdate.bFloat = false;
	//	pBlock->NotifyBlockPlayerExceptThisPlayer<MGRoleattUpdate>(&attrUpdate,pPlayer);

	//}

	//if ( oldhp != itemAddtionProp.Hp )
	//{
	//	MGRoleattUpdate attrUpdate;
	//	attrUpdate.lChangedPlayerID = pPlayer->GetFullID();
	//	attrUpdate.nOldValue = 0;
	//	attrUpdate.nNewValue = pPlayer->GetMaxHP();
	//	attrUpdate.nType = RT_HPMAX;
	//	attrUpdate.changeTime = 0;
	//	attrUpdate.nAddType = MGRoleattUpdate::TYPE_SET;	//直接设置
	//	attrUpdate.bFloat = false;
	//	pBlock->NotifyBlockPlayerExceptThisPlayer<MGRoleattUpdate>(&attrUpdate,pPlayer);
	//}
}


void CSuit::SubSuitAttributeToPlayer(CPlayer* pPlayer )
{
	if ( !pPlayer )
		return;

	ItemAddtionProp& itemAddtionProp = pPlayer->GetItemAddtionProp();

	itemAddtionProp.AddSpeed -= mSuitAddtionProp.AddSpeed;
	itemAddtionProp.Antistun -= mSuitAddtionProp.Antistun;
	itemAddtionProp.Antitie  -= mSuitAddtionProp.Antitie;
	itemAddtionProp.AttackMagMax -= mSuitAddtionProp.AttackMagMax;
	itemAddtionProp.AttackMagMin -= mSuitAddtionProp.AttackMagMin;
	itemAddtionProp.AttackPhyMax -= mSuitAddtionProp.AttackPhyMax;
	itemAddtionProp.AttackPhyMin -= mSuitAddtionProp.AttackPhyMin;
	itemAddtionProp.MagicAttack  -= mSuitAddtionProp.MagicAttack;
	itemAddtionProp.MagicAttackAdd -= mSuitAddtionProp.MagicAttackAdd;
	itemAddtionProp.MagicAttackPercent -= mSuitAddtionProp.MagicAttackPercent;
	itemAddtionProp.MagicCritical -= mSuitAddtionProp.MagicCritical;
	itemAddtionProp.MagicCriticalDamage -= mSuitAddtionProp.MagicCriticalDamage;
	itemAddtionProp.MagicCriticalDamageDerate -= mSuitAddtionProp.MagicCriticalDamageDerate;
	itemAddtionProp.MagicCriticalDerate -= mSuitAddtionProp.MagicCriticalDerate;
	itemAddtionProp.MagicDefend -= mSuitAddtionProp.MagicDefend;
	itemAddtionProp.MagicDefendPercent -= mSuitAddtionProp.MagicDefendPercent;
	itemAddtionProp.MagicDeratePercent -= mSuitAddtionProp.MagicDeratePercent;
	itemAddtionProp.MagicHit -= mSuitAddtionProp.MagicHit;
	itemAddtionProp.MagicJouk -= mSuitAddtionProp.MagicJouk;
	itemAddtionProp.MagicRift -= mSuitAddtionProp.MagicRift;
	itemAddtionProp.PhysicsCriticalDamage -= mSuitAddtionProp.PhysicsCriticalDamage;
	itemAddtionProp.PhysicsCritical -= mSuitAddtionProp.PhysicsCritical;
	itemAddtionProp.PhysicsCriticalDamageDerate -= mSuitAddtionProp.PhysicsCriticalDamageDerate;
	itemAddtionProp.PhysicsCriticalDerate -= mSuitAddtionProp.PhysicsCriticalDerate;
	itemAddtionProp.PhysicsDefend -= mSuitAddtionProp.PhysicsDefend;
	itemAddtionProp.PhysicsDefendPercent -= mSuitAddtionProp.PhysicsDefendPercent;
	itemAddtionProp.PhysicsDeratePercent -= mSuitAddtionProp.PhysicsDeratePercent;
	itemAddtionProp.PhysicsHit -= mSuitAddtionProp.PhysicsHit;
	itemAddtionProp.PhysicsJouk -= mSuitAddtionProp.PhysicsJouk;
	itemAddtionProp.PhysicsRift -= mSuitAddtionProp.PhysicsRift;
	itemAddtionProp.PhysicsAttack -= mSuitAddtionProp.PhysicsAttack;
	itemAddtionProp.PhysicsAttackAdd -= mSuitAddtionProp.PhysicsAttackAdd;
	itemAddtionProp.PhysicsAttackPercent -= mSuitAddtionProp.PhysicsAttackPercent;	
	itemAddtionProp.Mp -= mSuitAddtionProp.Mp;
	itemAddtionProp.Hp -= mSuitAddtionProp.Hp;

	//int oldmp = itemAddtionProp.Mp;
	//int oldhp = itemAddtionProp.Hp;
	////通知周围的玩家角色信息改变
	//pPlayer->NotifyOtherPlayerInfo();

	//CNewMap* pMap= pPlayer->GetCurMap();
	//if ( !pMap )
	//	return;

	//MuxMapBlock* pBlock = pMap->GetMapBlockByGrid( pPlayer->GetPosX(), pPlayer->GetPosY() );
	//if ( !pBlock )
	//	return;

	//if ( oldmp != itemAddtionProp.Mp )
	//{
	//	MGRoleattUpdate attrUpdate;
	//	attrUpdate.lChangedPlayerID = pPlayer->GetFullID();
	//	attrUpdate.nOldValue = 0;
	//	attrUpdate.nNewValue = pPlayer->GetMaxMP();
	//	attrUpdate.nType = RT_MPMAX;
	//	attrUpdate.changeTime = 0;
	//	attrUpdate.nAddType = MGRoleattUpdate::TYPE_SET;	//直接设置
	//	attrUpdate.bFloat = false;
	//	pBlock->NotifyBlockPlayerExceptThisPlayer<MGRoleattUpdate>(&attrUpdate,pPlayer );
	//}

	//if ( oldhp != itemAddtionProp.Hp )
	//{
	//	MGRoleattUpdate attrUpdate;
	//	attrUpdate.lChangedPlayerID = pPlayer->GetFullID();
	//	attrUpdate.nOldValue = 0;
	//	attrUpdate.nNewValue = pPlayer->GetMaxHP();
	//	attrUpdate.nType = RT_HPMAX;
	//	attrUpdate.changeTime = 0;
	//	attrUpdate.nAddType = MGRoleattUpdate::TYPE_SET;	//直接设置
	//	attrUpdate.bFloat = false;
	//	pBlock->NotifyBlockPlayerExceptThisPlayer<MGRoleattUpdate>(&attrUpdate,pPlayer );
	//}
}

void CSuitManager::LoadXML(const char *pxmlFile)
{
	if ( !pxmlFile )
		return;

	if ( !pxmlFile )
		return;

	D_DEBUG("LoadXML:%s\n", pxmlFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pxmlFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pxmlFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pxmlFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	for ( std::vector<CElement *>::iterator lter = childElements.begin();
		lter!= childElements.end();
		++lter )
	{
		PuchSuitToManager( *lter );
	}
	TRY_END;
}

void CSuitManager::PuchSuitToManager(CElement* pElement )
{
	if ( !pElement )
		return ;

	CSuit* pSuit = NEW CSuit( pElement );
	if ( pSuit )
		mSuitManager.insert( std::pair<unsigned int,CSuit *>( pSuit->GetSuitID(), pSuit ) );
}

//void CSuitManager::CheckPlayerSuitState( CPlayer* pPlayer )
//{
//	if ( !pPlayer )
//		return;
//
//	//以后优化对象，现在先这么写吧 ,以后打算在CPlayer类身上保存套装索引，这样就不用每次都遍历了 Notice！！！
//	PlayerInfo_2_Equips& equipInfo = pPlayer->GetFullPlayerInfo()->equipInfo;
//	std::vector<unsigned int> suitVec;
//	for ( int  i = 0 ; i< EQUIP_SIZE; i++ )
//	{
//		ItemInfo_i& itemInfo = equipInfo.equips[i];
//		if ( itemInfo.uiID != PlayerInfo_2_Equips::INVALIDATE  )
//		{
//			CBaseItem* pItem = ItemManagerSingle::instance()->GetItemByItemUID( itemInfo.uiID );
//			if ( pItem )
//			{
//				int suit = pItem->GetSuitID();
//				if ( suit > 0 )
//					suitVec.push_back( suit );
//			}
//		}
//	}
//
//	//根据玩家身上的套装编号给玩家增加属性
//	if ( !suitVec.empty() )
//	{
//		std::unique(suitVec.begin(), suitVec.end());
//		std::vector<unsigned int>::iterator lter = suitVec.begin();
//		for ( ; lter != suitVec.end(); ++lter )
//		{
//			unsigned int suitID = *lter;
//			std::map<unsigned int, CSuit *>::iterator lter = mSuitManager.find( suitID );
//			if ( lter != mSuitManager.end() )
//			{
//				CSuit *pSuit = lter->second;
//				//如果到达了套装的装备要求
//				if ( pSuit && pSuit->IsReachSuitCondition(pPlayer) )
//					pSuit->AddSuitAttributeToPlayer( pPlayer );
//			}
//		}
//	}
//}

CSuitManager::~CSuitManager()
{
	std::map<unsigned int, CSuit *>::iterator lter = mSuitManager.begin();
	for ( ; lter!= mSuitManager.end(); ++lter )
	{
		if ( NULL != lter->second )
		{
			delete lter->second;
		}
	}
	mSuitManager.clear();
}

CSuit* CSuitManager::GetSuit(unsigned int suitID )
{
	std::map<unsigned int, CSuit *>::iterator lter = mSuitManager.find( suitID );
	if ( lter != mSuitManager.end() )
	{
		return lter->second;
	}
	return NULL;
}
