﻿#include "ProtectItemManager.h"
#include "../../../Base/PkgProc/SrvProtocol.h"
#include "../Player/Player.h"
#include <ctime>

void CProtectItemManager::AttachPlayer(CPlayer* pPlayer )
{
	m_pAttachPlayer = pPlayer;
	mProtectItemVec.clear();
}

void CProtectItemManager::RecvPlayerProtectItem( const ProtectItem* protectItemArr , unsigned int pageIndex, bool isEnd )
{
	if ( !m_pAttachPlayer )
		return;

	if ( !protectItemArr )
		return;

	if ( pageIndex == 0)
		mProtectItemVec.clear();

	for ( unsigned int i = 0 ; i< PKG_PAGE_SIZE; i++ )
	{
		const ProtectItem& protectItem = protectItemArr[i];

		CBaseItem* pItem = m_pAttachPlayer->GetItemDelegate().GetItemByItemUID( protectItem.protectItemUID );
		if ( pItem )
		{
			if ( protectItem.protectItemUID != 0 && protectItem.protectType != E_NO_PROTECT )
			{
				mProtectItemVec.push_back( protectItem );

				if ( protectItem.protectType == E_SPECIAL_PROTECT && protectItem.protectTime != 0 )
				{
					specialProtectWaitItemCount++;
				}
			}
		}
	}

	if ( isEnd )
		NoticeProtectItemDetialInfoToPlayer();
}

void CProtectItemManager::UpdatePlayerProtectItemToDB()
{
	if( !m_pAttachPlayer )
		return;

	if ( mProtectItemVec.empty() )
	{
		MGDbSaveInfo saveinfo;
		saveinfo.infoType = DI_PROTECTITEM;
		saveinfo.playerID = m_pAttachPlayer->GetFullID();
		StructMemSet( saveinfo.protectitemInfo, 0x0 ,sizeof(saveinfo.protectitemInfo) );//清空原数据
		saveinfo.protectitemInfo.mCount = 0;
		saveinfo.protectitemInfo.isEnd  = true;
		m_pAttachPlayer->ForceGateSend<MGDbSaveInfo>( &saveinfo );
	}
	else
	{
		MGDbSaveInfo saveinfo;
		saveinfo.infoType = DI_PROTECTITEM;
		saveinfo.playerID = m_pAttachPlayer->GetFullID();

		unsigned int protectItemCount = (unsigned) mProtectItemVec.size();

		//分页发送
		unsigned int splitPage  = 0;
		if ( protectItemCount % PKG_PAGE_SIZE == 0 )
		{
			splitPage = protectItemCount / PKG_PAGE_SIZE;
		}
		else
		{
			splitPage = protectItemCount / PKG_PAGE_SIZE + 1;
		}

		for( unsigned int pageIndex = 0; pageIndex< splitPage ; pageIndex++ )//从第一页开始发送
		{
			unsigned int itemStart = pageIndex * PKG_PAGE_SIZE, itemIndex = itemStart;
			unsigned int itemEnd   = itemStart + PKG_PAGE_SIZE;
			itemEnd =  itemEnd > protectItemCount?protectItemCount:itemEnd;

			if ( itemEnd <= itemStart )
				return;

			//清空数据
			StructMemSet( saveinfo.protectitemInfo, 0x0 ,sizeof(saveinfo.protectitemInfo) );//清空原数据

			for ( ; itemIndex < itemEnd ; ++itemIndex )
			{
				unsigned int tmpIndex = itemIndex -  itemStart;
				if ( tmpIndex >= ARRAY_SIZE(saveinfo.protectitemInfo.mProtectItemArr) )
				{
					D_ERROR( "CProtectItemManager::UpdatePlayerProtectItemToDB, tmpIndex(%d) >= ARRAY_SIZE(saveinfo.protectitemInfo.mProtectItemArr)\n", tmpIndex );
					break;
				}
				if ( itemIndex >= mProtectItemVec.size() )
				{
					D_ERROR( "CProtectItemManager::UpdatePlayerProtectItemToDB, itemIndex(%d) >= mProtectItemVec.size()\n", itemIndex );
					break;
				}
				saveinfo.protectitemInfo.mProtectItemArr[tmpIndex]  = mProtectItemVec[itemIndex];
			}
			saveinfo.protectitemInfo.mCount = pageIndex;
			saveinfo.protectitemInfo.isEnd  = ( pageIndex == splitPage - 1 )?true:false;

			m_pAttachPlayer->ForceGateSend<MGDbSaveInfo>( &saveinfo );
		}	
	}
}

void CProtectItemManager::NoticeProtectItemDetialInfoToPlayer()
{
	if ( !m_pAttachPlayer )
		return;

	//如果是空的,则不需要发送消息
	if ( mProtectItemVec.empty() )
		return;

	unsigned int protectItemCount = (unsigned) mProtectItemVec.size();

	//分页发送
	unsigned int splitPage  = 0;
	if ( protectItemCount % PKG_PAGE_SIZE == 0 )
	{
		splitPage = protectItemCount / PKG_PAGE_SIZE;
	}
	else
	{
		splitPage = protectItemCount / PKG_PAGE_SIZE + 1;
	}

	MGSendProtectItemWithProp protectItemInfo;
	for( unsigned int pageIndex = 0; pageIndex< splitPage ; pageIndex++ )
	{
		unsigned int itemStart = pageIndex * PKG_PAGE_SIZE, itemIndex = itemStart;
		unsigned int itemEnd   = itemStart + PKG_PAGE_SIZE;
		itemEnd =  itemEnd > protectItemCount?protectItemCount:itemEnd;

		if ( itemEnd <= itemStart )
			return;

		//清空数据
		StructMemSet( protectItemInfo, 0x0 ,sizeof(protectItemInfo) );

		for ( ; itemIndex < itemEnd ; ++itemIndex )
		{
			unsigned int tmpIndex = itemIndex -  itemStart;
			if ( tmpIndex >= ARRAY_SIZE(protectItemInfo.protectItemArr) )
			{
				D_ERROR( "CProtectItemManager::NoticeProtectItemDetialInfoToPlayer, tmpIndex(%d) >= ARRAY_SIZE(protectItemInfo.protectItemArr)\n", tmpIndex );
				break;
			}
			if ( itemIndex >= mProtectItemVec.size() )
			{
				D_ERROR( "CProtectItemManager::NoticeProtectItemDetialInfoToPlayer, itemIndex(%d) >= mProtectItemVec.size()\n", itemIndex );
				break;
			}
			protectItemInfo.protectItemArr[tmpIndex]  = mProtectItemVec[itemIndex];
		}
		protectItemInfo.itemCount = itemIndex - itemStart;

		if ( protectItemInfo.itemCount > 0 )
			m_pAttachPlayer->SendPkg<MGSendProtectItemWithProp>( &protectItemInfo );
	}	
}

ProtectItem* CProtectItemManager::FindItemIsProtected(unsigned int itemUID )
{
	if ( !m_pAttachPlayer)
		return NULL;

	if ( !mProtectItemVec.empty() )
	{
		unsigned int protectCount = (unsigned)mProtectItemVec.size();
		for ( unsigned int i = 0 ; i< protectCount; i++ )
		{
			const ProtectItem& proItem = mProtectItemVec[i];
			if ( proItem.protectItemUID == itemUID )
			{
				return &mProtectItemVec[i];
			}
		}
	}

	return NULL;
}


PROTECTED_ITEM_TYPE CProtectItemManager::GetItemProtectedType(unsigned int itemUID )
{
	if ( !m_pAttachPlayer)
		return E_NO_PROTECT;

	ProtectItem* pProtectItem = FindItemIsProtected( itemUID );
	if ( pProtectItem  )
		return pProtectItem->protectType;

	return E_NO_PROTECT;
}


void CProtectItemManager::RemoveProtectItem(unsigned int itemUID )
{
	if( !m_pAttachPlayer )
		return;

	if ( !mProtectItemVec.empty() )
	{
		std::vector<ProtectItem>::iterator lter = mProtectItemVec.begin();
		for ( ;lter != mProtectItemVec.end();  )
		{
			ProtectItem& protectItem = *lter;
			if ( protectItem.protectItemUID == itemUID )
			{
				//通知客户端删除
				protectItem.protectTime = 0;
				protectItem.protectType = E_NO_PROTECT;
				NoticeItemProtectPropUpdate( protectItem,  E_DEL_PROTECT_ITEM );

				lter = mProtectItemVec.erase( lter );
			}
			else
				++lter;
		}
	}
}

//取消特殊保护等待时间
void CProtectItemManager::CancelItemUnSpecialProtectWaitTime(unsigned int itemUID )
{
	if ( !m_pAttachPlayer )
		return;

	//0x0 代表成功
	//0x1 代表失败
	unsigned cancelItemRst = 0x0;
	ProtectItem* pProtectItem = FindItemIsProtected( itemUID );
	if ( pProtectItem && pProtectItem->protectType == E_SPECIAL_PROTECT )
	{
		pProtectItem->protectTime = 0 ;

		specialProtectWaitItemCount--;

		NoticeItemProtectPropUpdate( *pProtectItem , E_UPDATE_PROTECT_ITEM );
	}
	else
	{
		cancelItemRst = 0x1;
	}

	MGCancelSpecialPropRst cancelRst;
	cancelRst.itemUID = itemUID;
	cancelRst.opRst   = cancelItemRst;

	m_pAttachPlayer->SendPkg<MGCancelSpecialPropRst>( &cancelRst );
}

//取消道具的特殊保护
void CProtectItemManager::CancelItemSpecialProtect(unsigned int itemUID )
{
	if ( !m_pAttachPlayer )
		return;

	ProtectItem* pProtectItem = FindItemIsProtected( itemUID );
	if ( pProtectItem && pProtectItem->protectType == E_SPECIAL_PROTECT )
	{
		pProtectItem->protectTime = (unsigned int)time(NULL) + 259200;//解除保护

		//通知client更新
		NoticeItemProtectPropUpdate( *pProtectItem,  E_UPDATE_PROTECT_ITEM );

		//进入取消特殊保护等待时间的道具个数+1
		specialProtectWaitItemCount++;
	}
}

void CProtectItemManager::ReleaseSpecialItemProtect(unsigned int itemUID)
{
	if ( !m_pAttachPlayer )
		return;

	//0x0 代表成功
	//0x1 未过期
	//0x2 非特殊保护道具
	unsigned releaseItemRst = 0x0;

	ProtectItem* pProtectItem = FindItemIsProtected( itemUID );
	if ( pProtectItem && pProtectItem->protectType == E_SPECIAL_PROTECT )
	{
		time_t curtime = time(NULL);
		if (  (unsigned int)curtime >= pProtectItem->protectTime )
		{
			//等待解除保护的个数-1
			specialProtectWaitItemCount--;

			//在服务器端删除保护道具
			RemoveProtectItem( itemUID );
		}
		else
		{
			releaseItemRst = 0x1;//未过期
		}
	}
	else
	{
		releaseItemRst = 0x2;//非特殊保护道具
	}

	MGReleaseSpecialPropRst releaseRst;
	releaseRst.itemUID = itemUID;
	releaseRst.opRst = releaseItemRst;
	m_pAttachPlayer->SendPkg<MGReleaseSpecialPropRst>( &releaseRst );
}

void CProtectItemManager::NoticeItemProtectPropUpdate(const MUX_PROTO::ProtectItem &protectItem, MUX_PROTO::UPDATE_PROTECTITEM_PROP opType)
{
	if ( !m_pAttachPlayer )
		return;

	//通知client更新对应的保护道具
	MGSetItemProtectProp updateProp;
	updateProp.protectItem = protectItem;
	updateProp.opType = opType;
	m_pAttachPlayer->SendPkg<MGSetItemProtectProp>( &updateProp );

	//因为保护道具,比较重要,立即存盘
	UpdatePlayerProtectItemToDB();
}


//返回非0,都为失败
//0 成功
//-1 失败
//-2 特殊保护道具数量已满 
int CProtectItemManager::PushNewProtectItem(const ProtectItem& newProtectItem )
{
	if ( !m_pAttachPlayer )
		return 0;

	mProtectItemVec.push_back( newProtectItem );

	NoticeItemProtectPropUpdate( newProtectItem,E_ADD_NEW_PROTECT_ITEM );

	return 0;
}



