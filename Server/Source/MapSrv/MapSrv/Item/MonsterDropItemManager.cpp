﻿#include "MonsterDropItemManager.h"
#include <time.h>
#include "LoadDropItemXML.h"
#include "ItemBuilder.h"
#include "CItemPropManager.h"

class FindDropItem:public std::binary_function<CItemPkgEle, unsigned int,bool >
{
public:
	bool operator()( const CItemPkgEle& itemPkgEle, unsigned int itemTypeID ) const
	{
		return itemPkgEle.GetItemTypeID() == itemTypeID;
	}
};


int DropItemRuleSet::RandonDropItem( std::vector<CItemPkgEle>& itemEleVec, int randCount )
{
	TRY_BEGIN;

	if ( randCount <= 0 )
		return 0;

	if ( mTotalRange <= 0)
		return 0;

	//随机mRandCount次
	size_t nSize = itemEleVec.size();
	
	for ( int  i = 0 ; i < randCount; i++ )
	{
		//@brief:随机一个数
		long tmpIndex  = RAND%(mTotalRange) + 1;
		std::vector<DropRule>::iterator lter = std::find_if(
			mDropConfigVec.begin(),
			mDropConfigVec.end(),
			bind2nd(FindDropRuleFunc(),tmpIndex)
			);

		if ( lter != mDropConfigVec.end() )
		{
			unsigned int itemTypeID = lter->itemID;
			//放弃对已经放入包裹的判断，不检测是否该道具存在，就不掉该道具了
			CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
			if ( pPublicProp )
			{
				////如果是功能性物品
				//if ( pPublicProp->GetItemTypeID() ==  E_TYPE_FUNCTION )
				//{
				//	FunctionProperty* pFunctionProp =   CFunctionPropertyManagerSingle::instance()->GetSpecialPropertyT( itemTypeID );
				//	//如果是幸运星
				//	if ( pFunctionProp && ( pFunctionProp->Type == FunctionProperty::E_LUCK_STAR ) )
				//	{
				//		//获取幸运星
				//		LuckStar* pLuckStar = LuckStarManagerSingle::instance()->GetLuckStar( pPublicProp->GetItemTypeID() );
				//		if ( pLuckStar )
				//		{
				//			//获取本次随机的条件编号
				//			int randCondition =  pLuckStar->RandomCondition();
				//			if ( randCondition > 0 )
				//			{
				//				CItemPkgEle itemEle;
				//				itemEle.SetItemEleProp( itemTypeID, randCondition , 0 );
				//				itemEleVec.push_back( itemEle );
				//			}
				//		}
				//		continue;
				//	}
				//}

				//如果是不可堆叠物品
				if ( pPublicProp->m_itemMaxAmount == 1 )
				{
					CItemPkgEle itemEle;
					//by dzj, 10.05.20, itemEle.SetItemEleProp( itemTypeID, 1, 0 );
					itemEle.SetItemPropNewRand( itemTypeID, 1, 0/*如果是任务道具，会从任务道具集中掉落，不会到此*/, 0 );					
					itemEleVec.push_back( itemEle );
				}
				else
				{
					std::vector<CItemPkgEle>::iterator lter =std::find_if(
						itemEleVec.begin(),
						itemEleVec.end(),
						bind2nd( FindDropItem(), itemTypeID )
						);

					if ( lter != itemEleVec.end() )
						(*lter).IncItemCount();
					else
					{
						CItemPkgEle itemEle;
						//by dzj, 10.05.20, itemEle.SetItemEleProp( itemTypeID, 1, 0 );
						itemEle.SetItemPropNewRand( itemTypeID, 1, 0/*如果是任务道具，会从任务道具集中掉落，不会到此*/, 0 );
						itemEleVec.push_back( itemEle );
					}
				}
			}

		}
	}
		
	return (int)(itemEleVec.size() - nSize);
	TRY_END;

	return 0;
}

void DropItemRuleSet::PushDropConfig(long itemID, long range)
{
	if ( itemID != 0   )
	{
		DropRule dropConfig;
		dropConfig.itemID = itemID;
		dropConfig.startIndex = GetStartRangeIndex();
		dropConfig.endIndex   = GetEndRangeIndex(range);
		mDropConfigVec.push_back(dropConfig);
		mTotalRange += range;

		//assert( dropConfig.startIndex <= dropConfig.endIndex );
		if (!( dropConfig.startIndex <= dropConfig.endIndex ))
		{
			D_ERROR( "DropItemRuleSet::PushDropConfig, assert( dropConfig.startIndex <= dropConfig.endIndex )\n" );
			return;
		}
	}
	else//如果ItemID==0，则代表非随机的范围
	{
		AddUnRandRange( range );
	}
}

DropItemRuleSet* CDropItemRuleSetManager::NewDropItemRule( long setID )
{
	if (dropSetIndex >= ARRAY_SIZE(dropSetArr))
	{
		D_ERROR( "CDropItemRuleSetManager::NewDropItemRule, dropSetIndex(%d) >= ARRAY_SIZE(dropSetArr)\n", dropSetIndex );
		return NULL;
	}
	DropItemRuleSet* pItemRuleSet = &dropSetArr[dropSetIndex++];
	if ( NULL != pItemRuleSet )
	{
		pItemRuleSet->SetDropRuleInfo( setID );
	}

	return pItemRuleSet;
}

DropItemRuleSet* CDropItemRuleSetManager::GetDropItemRule(long setID )
{
	for ( unsigned int i = 0 ; i< dropSetIndex && i< ARRAY_SIZE(dropSetArr) ;i++)
	{
		DropItemRuleSet* pDropSet = &(dropSetArr[i]);
		if ( NULL == pDropSet )
		{
			D_ERROR( "GetDropItemRule，不可能取址错误\n" );
			continue;
		}
		if( pDropSet->GetRuleSetID() == setID )
		{
			return pDropSet;
		}
	}
	//std::map<long, DropItemRuleSet*>::iterator lter = mDropRultSetMan.find(setID);
	//if ( lter != mDropRultSetMan.end() )
	//{
	//	return lter->second;
	//}
	return NULL;
}

CDropItemRuleSetManager::~CDropItemRuleSetManager()
{
	/*for ( std::map<long, DropItemRuleSet*>::iterator lter = mDropRultSetMan.begin();lter!= mDropRultSetMan.end(); )
	{
		DropItemRuleSet* pSet = lter->second;

		delete pSet;
		pSet = NULL;
		lter = mDropRultSetMan.erase(lter);
	}*/
}

void CDropRuleStateManager::ChangeState(RULETYPE ruleType)
{
	switch( ruleType )
	{
	case E_DAY:
		{
			mpDropItemRule = DayDropRuleManagerSingle::instance();
			mCurrentState  = E_DAY;
		}
		break;
	case E_DARK:
		{
			mpDropItemRule = DarkDropRuleManagerSingle::instance();
			mCurrentState  = E_DARK;
		}
		break;
	default:
		break;
	}
}

int CDropRuleStateManager::GetCurrentHour()
{
	//获取当前时间
	time_t now;
	time(&now);
	struct tm *tmp = ACE_OS::localtime(&now);
	if ( NULL == tmp )
	{
		return 0;
	}
	return tmp->tm_hour;
}

//根据当前时间来改变
void CDropRuleStateManager::CheckRuleState()
{
	int hour = GetCurrentHour();
	RULETYPE type = E_RULETYPEMAX;
	if ( hour >= 18 && hour < 22 )//晚上6:00-10点掉落集合B,应lilei修改
	{
		type = E_DARK;
	}
	else
	{
		type = E_DAY;
	}
	ChangeState( type );
}

//@brief:每半个小时检查当前的掉落规则是否满足时间需求
void CDropRuleStateManager::MonitorCheckTime()
{
	//获取当前时间
	unsigned long current = (unsigned long)time(NULL);

	unsigned long difft = current - lastUpdateTime;
	if (  difft <  60 )//每半个小时,检测一次是否切换状态,如果没到检测时间，则直接返回
		return;
	
	lastUpdateTime = current;
	CheckRuleState();
}
