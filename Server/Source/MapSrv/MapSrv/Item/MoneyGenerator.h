﻿/** @file MoneyGenerator.h
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：guanchuanqi
*	完成日期：2008-6-17
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/


#ifndef MONEYGENERATOR_H
#define MONEYGENERATOR_H

#include <map>
#include "../../../Base/aceall.h"
//#include "MoneyLuaScript.h"

class CElement;

struct MoneyRule
{
	int money;
	float randmin;
	float randmax;
	MoneyRule():money(0),randmin(0.0f),randmax(0.0f){}
};

class CSetMoneyMap
{
public:
	CSetMoneyMap(){};

	~CSetMoneyMap()
	{
		for (std::map<long,MoneyRule*>::iterator lter =mMoneyRule.begin(); lter!=mMoneyRule.end();++lter  )
		{
			if ( NULL != lter->second )
			{
				delete lter->second;
			}
		}
		mMoneyRule.clear();
	}

public:
	MoneyRule* GetRuleBySetID( long setID )
	{
		std::map<long,MoneyRule*>::iterator lter = mMoneyRule.find(setID);
		if ( lter != mMoneyRule.end() )
		{
			return lter->second;
		}
		return NULL;
	}

	void PushRule( CElement* pXMLElement );

private:
	CSetMoneyMap& operator=( CSetMoneyMap& );
	CSetMoneyMap( CSetMoneyMap& );

private:
	std::map<long,MoneyRule*> mMoneyRule;
};


class MoneyGenerator
{
	friend class ACE_Singleton<MoneyGenerator, ACE_Null_Mutex>;
public:
	void LoadXML( const char* pxmlFilePath/*,const char* luaScriptPath*/ );

	long Generator( long setID );

public:
	bool IsMoneySetID( long setID )
	{
		bool isFind  = ( mSetMoneyMap.GetRuleBySetID( setID ) != NULL );
		return isFind;
	}

private:
	CSetMoneyMap mSetMoneyMap;
	//CMoneyLuaScript mMoneyScript;
};
typedef ACE_Singleton<MoneyGenerator, ACE_Null_Mutex> MoneyGeneratorSingle;


#endif
