﻿/********************************************************************
	created:	2008/08/13
	created:	13:8:2008   19:11
	file:		ItemLevelUpAttribute.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef ITEMLEVELUPATTRIBUTE_H
#define ITEMLEVELUPATTRIBUTE_H

#include <map>

class CElement;

struct LevelUpProp
{
	unsigned int AttackPhyMin;
	unsigned int AttackPhyMax;
	unsigned int AttackMagMin;
	unsigned int AttackMagMax;
	unsigned int PhysicsAttack;
	unsigned int MagicAttack;
	unsigned int PhysicsDefend;
	unsigned int MagicDefend;
	unsigned int speed;
	unsigned int equipStrAdd;//装备所需力量点增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
	unsigned int equipIntAdd;//装备所需智力增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
	unsigned int equipAgiAdd;//装备所需敏捷增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
	unsigned int equipVitAdd;//装备所需体质增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
	unsigned int equipSpiAdd;//装备所需精神增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
	LevelUpProp():AttackPhyMin(0),AttackPhyMax(0),AttackMagMin(0),AttackMagMax(0),PhysicsAttack(0),MagicAttack(0),PhysicsDefend(0),MagicDefend(0),speed(0)
	{
		equipStrAdd = 0;//装备所需力量点增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
		equipIntAdd = 0;//装备所需智力增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
		equipAgiAdd = 0;//装备所需敏捷增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
		equipVitAdd = 0;//装备所需体质增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
		equipSpiAdd = 0;//装备所需精神增加，真正需要点=装备基本需求点+升级等级需要的额外增加点;
	}
};

struct ChangeProp
{
	unsigned int AttackPhyMin;
	unsigned int AttackPhyMax;
	unsigned int AttackMagMin;
	unsigned int AttackMagMax;
	unsigned int Hp;
	unsigned int Mp;
	unsigned int PhysicsAttack;
	unsigned int MagicAttack;
	unsigned int PhysicsDefend;
	unsigned int MagicDefend;
	unsigned int PhysicsHit;
	unsigned int MagicHit;
	unsigned int PhysicsCritical;
	unsigned int MagicCritical;
	unsigned int PhysicsAttackPercent;
	unsigned int MagicAttackPercent;
	unsigned int PhysicsCriticalDamage;
	unsigned int MagicCriticalDamage;
	unsigned int PhysicsDefendPercent;
	unsigned int MagicDefendPercent;
	unsigned int PhysicsCriticalDerate;
	unsigned int MagicCriticalDerate;
	unsigned int PhysicsCriticalDamageDerate;
	unsigned int MagicCriticalDamageDerate;
	unsigned int PhysicsJouk;
	unsigned int MagicJouk;
	unsigned int PhysicsDeratePercent;
	unsigned int MagicDeratePercent;
	unsigned int Stun;
	unsigned int Tie;
	unsigned int Antistun;
	unsigned int Antitie;
	unsigned int PhysicsAttackAdd;
	unsigned int MagicAttackAdd;
	unsigned int PhysicsRift;
	unsigned int MagicRift;
	unsigned int AddSpeed;

};


class  ItemLevelUpAttributeManager
{
public:
	static void LoadXML( const char* pszFileName );

	static void PushLevelUpAttribute( CElement* pxmlElement );

	static LevelUpProp* GetLevelUpAttribute( unsigned int levelUpID );

private:
	static std::map< unsigned int, LevelUpProp> mItemLevelUpPropManager;
};



class ItemChangeAttributeManager
{
public:
	static void LoadXML( const char* pszFileName );

	static void PushChangeAttribute( CElement* pxmlElement );

	static ChangeProp* GetChangeItemAttribute( unsigned int changeID );

private:
	static std::map< unsigned int ,ChangeProp > mItemChangePropManager;
};


#endif
