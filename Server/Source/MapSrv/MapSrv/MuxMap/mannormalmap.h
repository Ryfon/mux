﻿/*
* Copyright(c) 2007 上海第9城市研发部
* All rights reserved
*
* 文件名称:mannormalmap.h
* 摘要:地图信息管理类,包含了地图读取方法
* 作者:hyn
* 完成日期:
*
* 修改：针对副本的调整修改，by dzj, 09.10.15;
*
*/
#ifndef MAPMANAGER_H
#define MAPMANAGER_H

#include "LoadConfig/LoadSrvConfig.h"

#include "muxmapbase.h"

#include <string>
#include <map>
using namespace std;

///管理map的mapmanager
class CManNormalMap
{
private:
	CManNormalMap();
	~CManNormalMap();

public:

	///根据配置初始化所有普通地图, by dzj,07.12.28；
	static bool InitAllNormalMap();

	///释放管理的所有普通地图；
	static void ReleaseAllNormalMap(void);

	///第一次刷所有地图的所有初始怪物, by dzj, 07.12.28;
	static void FirstRefreshMonster();

	///地图管理器慢时钟;
	static void MapManagerSlowTimer( const ACE_Time_Value& curTime );

	static void OnCityAttackStart( unsigned int actID );

	///获取该地图ID在地图管理器里的指针，如果无，返回NULL
	static CNewMap* FindMap(long lMapID);

	static int GetMapNum()
	{
		return (int)m_mapList.size();
	}

	static void GetMannedMap( vector<long>& vecMannedMap)
	{
		vecMannedMap.clear();
		for(map<long,CNewMap*>::iterator iter = m_mapList.begin(); iter != m_mapList.end(); ++iter)
		{
			vecMannedMap.push_back(iter->first);
		}
	}

	static void OnActivityStart( unsigned int actID )
	{
		for( map<long,CNewMap*>::iterator iter = m_mapList.begin(); iter != m_mapList.end(); iter++)
		{
			if( NULL == iter->second )
				continue;

			iter->second->OnActivityStart( actID );
		}
	}


	static void OnActivityEnd( unsigned int actID )
	{
		for( map<long,CNewMap*>::iterator iter = m_mapList.begin(); iter != m_mapList.end(); iter++)
		{
			if( NULL == iter->second )
				continue;

			iter->second->OnActivityEnd( actID );
		}
	}

	//template<class T>
	//static void BroadCastToAllPlayers( T* T_msg )
	//{
	//	if ( !T_msg )
	//		return;

	//	if ( m_mapList.empty() )
	//		return;

	//	for( map<long,CNewMap*>::iterator iter = m_mapList.begin(); iter != m_mapList.end(); iter++)
	//	{
	//		if( NULL == iter->second )
	//			continue;

	//		iter->second->NotifyAllUseBrocast<T>( T_msg );
	//	}
	//}

	static bool OnDelWarTimer( unsigned int timerID )
	{
		for( map<long,CNewMap*>::iterator iter = m_mapList.begin(); iter != m_mapList.end(); ++iter)
		{
			if( NULL == iter->second )
				continue;
			CWarTimerManager& timerManager = iter->second->GetWarTimerManager();
			timerManager.DeleteWarTimer( timerID );
		}
		return true;
	}

private:
	static vector<string> m_veCNewMapNames;//本SRV管理的所有map的文件名;
	static map<long, CNewMap*> m_mapList; //管理map的map列表

};

#endif /*MAPMANAGER_H*/
