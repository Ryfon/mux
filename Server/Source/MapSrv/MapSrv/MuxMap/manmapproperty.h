﻿/**
* @file manmapproperty.h
* @brief 定义地图属性管理器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: manmapproperty.h
* 摘    要: 管理地图属性，将地图静态属性从地图动态信息中分离出来，for 副本;
* 作    者: dzj
* 开始日期: 2009.09.10
* 
*/

#pragma once

#ifndef MAN_MAP_PROPERTY_H
#define MAN_MAP_PROPERTY_H

#include "mapproperty.h"

#include "LoadConfig/LoadSrvConfig.h"

#include <string>
#include <map>
#include <vector>

using namespace std;


class CManMapProperty
{
private:
	CManMapProperty();
	~CManMapProperty();

public:
	static CMuxMapProperty* FindMapProperty( unsigned int mapid )
	{
		map< unsigned int, CMuxMapProperty* >::iterator iter = m_mapMapPropertys.find( mapid );
		if ( m_mapMapPropertys.end() == iter )
		{
			return NULL;
		}
		return iter->second;
	}

public:
	///初始化所有地图的地图属性；
	static bool InitAllMapProperty();

	///释放所有地图的地图属性；
	static bool ReleaseAllMapProperty();

	///得到所有的普通地图地图号
	static bool GetAllNormalMapID( vector<unsigned short>& outNormalMapIDs );

	///读入各地图复活点，并将这些复活点添加到相应地图中去；
	static bool ReadRebirthConfig();

private:
	///读入所有可能的地图名字，准备载入其属性；
    static bool ReadAvailibleMapName( vector<ToReadMapInfo>& outIDMapNames );


public:
	static map< unsigned int, CMuxMapProperty* > m_mapMapPropertys;	//地图属性容器；
};

#endif //MAN_MAP_PROPERTY_H

