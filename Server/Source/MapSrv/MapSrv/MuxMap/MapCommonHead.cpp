﻿/**
* @file MapCommonHead.cpp
* @brief 定义NPC
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MapCommonHead.cpp
* 摘    要: 定义部分地图公用函数
* 作    者: dzj
* 开始日期: 2008.07.02
* 
*/

#include "MapCommonHead.h"

namespace MuxMapSpace
{
	bool GetViewDebug() 
	{
		return g_bIsViewDebug;
	}

	void InverseViewDebug()
	{
		g_bIsViewDebug = !g_bIsViewDebug;
	}
	
	///浮点坐标转成格子坐标；
	void FloatToGridPos( float fPosX, float fPosY, unsigned short& gridX, unsigned short& gridY )
	{
		gridX = (unsigned short) (fPosX / METER_PERGRID);
		gridY = (unsigned short) (fPosY / METER_PERGRID);
	}

    ///格子坐标转成小块坐标；
	void GridToBlockPos( unsigned short gridX, unsigned short gridY, unsigned short& blockX, unsigned short& blockY )
	{
		blockX = gridX / MAPBLOCK_SIZE;
		blockY = gridY / MAPBLOCK_SIZE;
	}

	///浮点坐标转成小块坐标；
	void FloatToBlockPos( float fPosX, float fPosY, unsigned short& blockX, unsigned short& blockY )
	{
		unsigned short gridPosX = 0, gridPosY = 0;
		FloatToGridPos( fPosX, fPosY, gridPosX, gridPosY );
		GridToBlockPos( gridPosX, gridPosY, blockX, blockY );
	}

	///计算距离平方；
	unsigned int GetSqrDistance( unsigned short gridX1, unsigned short gridY1, unsigned short gridX2, unsigned short gridY2 )
	{
		int dx1 = gridX1 - gridX2;
		int dx2 = gridY1 - gridY2;
		return dx1*dx1 + dx2*dx2;
	}
};



