﻿/**
* @file mapcomponent.h
* @brief 地图用组件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: mapcomponent.h
* 摘    要: 地图用组件(例如：地图计数器，地图计时器)
* 作    者: dzj
* 完成日期: 2008.03.25
*
*/

#ifndef MAP_COMPONENT_H
#define MAP_COMPONENT_H

#include "../Item/NormalCounter.h"
#include <vector>
#include <list>
#include "PkgProc/SrvProtocol.h"
using namespace std;

class CMuxMap;

///地图计数器信息；
class MapCounterInfo
{
private:
	MapCounterInfo();
public:
	/*Init( unsigned int counterIdType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount, unsigned int taskID )*/
	explicit MapCounterInfo( unsigned counterClsID, unsigned int counterType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount )
		: m_CounterClsID(counterClsID), m_curCount(0)
	{
		m_CounterProp.Init( counterType, counterIdMin, counterIdMax, targetCount, 0/*无任务绑定*/ );
	};

	~MapCounterInfo(){};

public:
	inline unsigned int GetCounterClsID() { return m_CounterClsID; };

	///检测计数，如果相符加计数值，如果计数满，返回真，不相符或计数未满返回假
	bool IsCountFinish( COUNTER_TARGET_TYPE ctType, unsigned int reasonObjID, unsigned int reasonObjNum )
	{
		if ( m_CounterProp.IsAccord( ctType, reasonObjID ) )
		{
			m_curCount += reasonObjNum;
			if ( m_curCount >= m_CounterProp.GetTargetCount() )
			{
				return true;
			}
		}
		return false;
	}

private:
	unsigned int        m_CounterClsID;//计数器类型号，用于脚本标识；
	unsigned int        m_curCount;//当前计数;
	NormalCounterProp   m_CounterProp;//计数器属性;
};

///地图计数器；
class CCounterOfMap
{
public:
	CCounterOfMap() 
	{ 
		m_pOwnerMap = NULL; 
		for ( int i=0; i<ARRAY_SIZE(m_arrMapCounters); ++i )
		{
			m_arrMapCounters[i].clear();
		}
		InitCounterOfMap( NULL );
	};
	~CCounterOfMap();

public:
	void InitCounterOfMap(CMuxMap* pOwnerMap)
	{
		m_pOwnerMap = pOwnerMap;
		MapCounterInfo* pCounterInfo = NULL;
		for ( int i=0; i<ARRAY_SIZE(m_arrMapCounters); ++i )
		{
			if ( m_arrMapCounters[i].empty() )
			{
				continue;
			}

			for ( vector<MapCounterInfo*>::iterator iter=m_arrMapCounters[i].begin(); iter!=m_arrMapCounters[i].end(); ++iter )
			{
				pCounterInfo = *iter;
				if ( NULL != pCounterInfo )
				{
					delete pCounterInfo; pCounterInfo = NULL;
				}
			}
			m_arrMapCounters[i].clear();
		}
	}

public:
	///计数器添加
	bool AddCounterOfMap( unsigned int counterClsId/*编号，用于脚本标识*/, unsigned int counterType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount );

	///计数事件检测（计数事件到达时回调关卡地图相应接口）
	bool TryMapCounterEvent( COUNTER_TARGET_TYPE ctType, unsigned int reasonObjID, unsigned int reasonObjNum );


private:
	CMuxMap* m_pOwnerMap;//所属地图；
	vector<MapCounterInfo*> m_arrMapCounters[CT_INVALID+1];//所有属于当前地图的计数器（不包含玩家身上的计数器）;
};


///计时器信息；
class MapTimerInfo
{
private:
	MapTimerInfo();
public:
	explicit MapTimerInfo( unsigned int timerClsID/*类型号，用于脚本标识*/, const ACE_Time_Value& expireTime/*到期时刻*/ ) : m_TimerClsID(timerClsID), m_ExpireTime(expireTime)
	{};

	~MapTimerInfo(){};

public:

	inline unsigned int GetTimerClsID() { return m_TimerClsID; };

	inline bool IsTimeExpire( const ACE_Time_Value& checkTime )
	{
		return (checkTime >= m_ExpireTime);
	};

	///输入时钟是否应在自身位置之前；
	inline bool DeterTimeOrder( const ACE_Time_Value& checkTime )
	{
		return (checkTime < m_ExpireTime);
	}

	inline ACE_Time_Value GetExpireTime()
	{
		return m_ExpireTime;
	}

private:
	unsigned int m_TimerClsID;//时钟类型ID；
	ACE_Time_Value m_ExpireTime;//到期时间；
};


class MapDetailTimerInfo
{
private:
	MapDetailTimerInfo();
public:
	explicit MapDetailTimerInfo( unsigned int timerClsID/*类型号，用于脚本标识*/, const unsigned int hour, const unsigned int minute ) : m_detailTimerClsID(timerClsID), m_hour(hour),m_minute(minute)
	{};

	~MapDetailTimerInfo(){};

	inline unsigned int GetHour()  {  return m_hour; };
	inline unsigned int GetMinute()  {  return m_minute; };
	inline unsigned int GetTimerId() { return m_detailTimerClsID; };

private:
	unsigned int m_detailTimerClsID;
	unsigned int m_hour;		//小时
	unsigned int m_minute;		//分钟

};


///地图计时器；
class CTimerOfMap
{
public:
	CTimerOfMap() : m_pOwnerMap(NULL) 
	{
		m_listTimer.clear();
	};

	~CTimerOfMap();

public:
	void InitTimerOfMap(CMuxMap* pOwnerMap)
	{
		m_pOwnerMap = pOwnerMap;

		MapTimerInfo* pTimerInfo = NULL;
		if (!( m_listTimer.empty() ))
		{
			for ( list<MapTimerInfo*>::iterator iter=m_listTimer.begin(); iter!=m_listTimer.end(); ++iter )
			{
				pTimerInfo = *iter;
				if ( NULL != pTimerInfo )
				{				
					delete pTimerInfo; pTimerInfo = NULL;			
				}				
			}
		}

		m_listTimer.clear();
	}

	///地图时钟循环检测；
	bool MapTimerCheck( const ACE_Time_Value& curTime );

public:
	///地图计时器添加
	bool InsertMapTimer( unsigned int timerClsID/*类型号，用于脚本标识*/, const ACE_Time_Value& expireTime/*到期时刻*/ );
	bool DeleteMapTimer( unsigned int timerClsID );		//删除指定计时器
	bool IsHaveMapTimer( unsigned int timerClsID );

private:
	CMuxMap* m_pOwnerMap;//所属地图；
	list<MapTimerInfo*> m_listTimer;//按时间顺序排列的计时信息；
};



///地图计时器；
class CDetailTimerOfMap
{
public:
	CDetailTimerOfMap() : m_pOwnerMap(NULL) 
	{
		m_listDetailTimer.clear();
		InitTimerOfMap( NULL );
	};

	~CDetailTimerOfMap()
	{
		InitTimerOfMap( NULL );
	};

public:
	void InitTimerOfMap(CMuxMap* pOwnerMap)
	{
		m_pOwnerMap = pOwnerMap;
		m_listDetailTimer.clear();
	}

	///地图时钟循环检测；
	bool MapDetailTimerCheck();

public:
	///地图计时器添加
	bool InsertDetailMapTimer( unsigned int timerClsID/*类型号，用于脚本标识*/, const unsigned int hour, const unsigned int minute );	  //到期小时，到期分钟
	bool DeleteDetailMapTimer( unsigned int timerClsID );

private:
	CMuxMap* m_pOwnerMap;//所属地图；
	list<MapDetailTimerInfo> m_listDetailTimer;//按时间顺序排列的计时信息；
};

struct WarTimerInfo
{
	unsigned int timerID;				//计时器的ID
	unsigned int leftMiliSecond;			//剩余时间(msec)
	unsigned int isClockWise;			//给客户端用
	unsigned int startInformTime;		//到点开始通知的时间(msec)
	bool     isInformClient;			//是否通知客户端
	bool     isStartInform;				//是否到点
	char     timerInfo[64];				//计时器的时间信息
	ACE_Time_Value lastUpdateTime;		//上次更新的时间

	WarTimerInfo():timerID(0),leftMiliSecond(0),isClockWise(0),startInformTime(0),isInformClient(false),isStartInform(false)
	{
		StructMemSet(timerInfo,0,sizeof(timerInfo));
		lastUpdateTime = ACE_Time_Value::zero;
	}

	void InitWarTimer( unsigned int inTimerID, unsigned int time, unsigned int stInfoTime, unsigned int inIsClockWise, bool inIsInformClient, const char* pTimerInfo )
	{
		timerID = inTimerID;
		leftMiliSecond = time * 1000;
		startInformTime = stInfoTime * 1000;
		SafeStrCpy( timerInfo, pTimerInfo );
		isInformClient = inIsInformClient;
		isClockWise = inIsClockWise;
		lastUpdateTime = ACE_OS::gettimeofday();
	}

	//0 删除  1继续更新 2继续更新外加全地图通知
	unsigned int WarTimerProcess()
	{
		ACE_Time_Value currentTime = ACE_OS::gettimeofday();
		unsigned int pastMiliSecond = (unsigned int)(currentTime - lastUpdateTime).msec();
		if( pastMiliSecond >= leftMiliSecond )
		{
			//返回false表示到时了 可以删除了
			return 0;
		}

		leftMiliSecond -= pastMiliSecond;

		//如果需要通知客户端
		if( isInformClient )
		{
			//但是还没有启动通知，则查询是否要启动通知
			if( !isStartInform )
			{
				if( leftMiliSecond <= startInformTime )
				{
					isStartInform = true;
					lastUpdateTime = currentTime;
					return 2;
				}
			}
		}
		//表示还要继续更新
		lastUpdateTime = currentTime;
		return 1;
	}

	//显示 timer
	void OnDisplayWarTimer( CPlayer* pNotifyPlayer );
};

struct WarCounterInfo
{
	unsigned int counterID;
	unsigned int counterNum;
	char szCounterInfo[64];

	WarCounterInfo():counterID(0),counterNum(0)
	{
		StructMemSet(szCounterInfo,0,sizeof(szCounterInfo));
	}

	void InitWarCounter( unsigned int inCounterID, unsigned int inCounterNum, const char* wordInfo )
	{
		counterID = inCounterID;
		counterNum = inCounterNum;
		SafeStrCpy( szCounterInfo, wordInfo );
	}

	unsigned int GetCounterNum()
	{
		return counterNum;
	}

	void OnDisplayCounterInfo( CPlayer* pPlayer );
};


class CWarTimerManager
{
public:
	CWarTimerManager():m_pOwnerMap(NULL)
	{
	
	}

	~CWarTimerManager()
	{
		for( size_t i = 0; i< m_vecWarTimer.size(); ++i )
		{
			if( NULL != m_vecWarTimer[i] )
			{
				delete m_vecWarTimer[i]; m_vecWarTimer[i] = NULL;
			}
		}
		m_vecWarTimer.clear();
	}


public:
	void WarTimerProcess();
	

	void AddWarTimer( unsigned int inTimerID, unsigned int time, unsigned int stInfoTime, unsigned int isClockWise, bool isInformClient, const char* pTimerInfo );
	bool DeleteWarTimer( unsigned int timerID );
	void OnPlayerEnterMap( CPlayer* pPlayer );
	//全地图通知该warTimer的信息
	void MapDisplayWarTimer( WarTimerInfo* pWarTimer );

	void AttachOwnerMap( CMuxMap* pMap )
	{
		m_pOwnerMap = pMap;
	}

	void InitWarTimer( CMuxMap* pMap )
	{
		m_pOwnerMap = pMap;
		for( size_t i = 0; i< m_vecWarTimer.size(); ++i )
		{
			if( NULL != m_vecWarTimer[i] )
			{
				delete m_vecWarTimer[i]; m_vecWarTimer[i] = NULL;
			}
		}
		m_vecWarTimer.clear();
	}

private:
	vector<WarTimerInfo*> m_vecWarTimer;
	CMuxMap* m_pOwnerMap;				
};

class CWarCounterManager
{
public:
	CWarCounterManager()
		:m_pOwnerMap(NULL)
	{

	}
	void AddWarCounter( unsigned int counterID, unsigned int counterNum, const char* szCounterInfo );
	bool DelWarCounter( unsigned int counterID );

	unsigned int GetCounterNum( unsigned int counterID );
	void SetCounterNum( unsigned int counterID, unsigned int counterNum );

	//向地图上所有人显示计数器信息
	void MapDisplayCounterInfo( unsigned int counterID );
	//向单人发送计数信息
	void OnPlayerDisplayCounterInfo( unsigned int counterID, CPlayer* pPlayer );

	void AttachOwnerMap( CMuxMap* pMap )
	{
		m_pOwnerMap = pMap;
	}

private:
	vector<WarCounterInfo*> m_vecWarCounter;
	CMuxMap* m_pOwnerMap;		//所属地图
};

#endif //MAP_COMPONENT_H

