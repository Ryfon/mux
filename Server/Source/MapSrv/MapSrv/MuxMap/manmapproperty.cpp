﻿/**
* @file manmapproperty.cpp
* @brief 定义地图属性管理器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: manmapproperty.cpp
* 摘    要: 管理地图属性，将地图静态属性从地图动态信息中分离出来，for 副本;
* 作    者: dzj
* 开始日期: 2009.09.10
* 
*/

#include "manmapproperty.h"

map< unsigned int, CMuxMapProperty* > CManMapProperty::m_mapMapPropertys;


void SplitString(const char* szString, vector<string>& vString, char* splitChar )
{ 
	if ( szString )
	{
		char* p = strtok( (char*)szString,splitChar);
		while( p != NULL  )
		{
			vString.push_back(p);
			p = strtok(NULL,splitChar);
		}
	}
}

///从mapmanconfig.xml中读入所有可能的地图名字
bool CManMapProperty::ReadAvailibleMapName( vector<ToReadMapInfo>& outMapNames )
{
	TRY_BEGIN;

	CXmlManager xmlLoader;
	if( !xmlLoader.Load("config/mapconfig/mapmanconfig.xml") )
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}

	int tmpsrvid = 0;
	int selfsrvid = CLoadSrvConfig::GetSelfID();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin(); tmpiter != elementSet.end(); ++tmpiter )
	{
		bool isCopy = false;
		const char* isCopyStr = (*tmpiter)->GetAttr("IsCopy");
		if ( NULL != isCopyStr )
		{
			isCopy = (atoi(isCopyStr)>0) ? true:false;		
		}

		if ( !isCopy )
		{
			if ( CLoadSrvConfig::IsCopyMapSrv() )
			{
				// non copy map, copy mapsrv need not read these map property;
				continue;
			}
			//if not copy, check if need be maned by self;
			const char* mansrvidstr = (*tmpiter)->GetAttr("MapSrvID");
			if ( NULL == mansrvidstr )
			{
				D_ERROR( "ReadAvailibleMapName, record%d, error MapSrvID\n", tmpiter-elementSet.begin() );
				continue;
			}
			unsigned short mansrvid = (unsigned short)atoi(mansrvidstr);
			if ( mansrvid != CLoadSrvConfig::GetSelfID() )
			{
				continue; //should not be maned by self;
			}
		} 
		else 
		{
			if ( !CLoadSrvConfig::IsCopyMapSrv() )
			{
				//copy map, non copy mapsrv need not read these map property;
				continue;
			}
			//and all mapsrv read all copy propertys;
		}

		ToReadMapInfo pTmpRead;
		const char* srvpathfile = (*tmpiter)->GetAttr("srvpathfile");
		if ( NULL == srvpathfile )
		{
			D_ERROR("mapmanconfig.xml中格式错，读第%d条记录的MapFileName错", tmpiter - elementSet.begin() );
			return false;
		}
		char* szFileName = "config/mapconfig/mapmanconfig.xml";
		//sceneId 	场景的ID
		const char * szSceneId = (*tmpiter)->GetAttr( "sceneid" );
		if ( !szSceneId )
		{
			D_ERROR( "attribute  sceneId invalid, load xml file failed.%s\n", szFileName );
			return false;
		}
		//scenetype	场景类型 主城(1),野外(2),副本(3)
		const char * szSceneType = (*tmpiter)->GetAttr( "scenetype" );
		if (!szSceneType)
		{
			D_ERROR( "attribute  scenetype invalid, load xml file failed.%s\n", szFileName );
			return false;
		}
		//scenename	场景名称
		const char * szSceneName = (*tmpiter)->GetAttr( "scenename" );
		if (!szSceneName)
		{
			D_ERROR( "attribute  scenename invalid, load xml file failed.%s\n", szFileName );
			return false;
		}
		//StartTime 	场景开启时间
		const char * szStartTime = (*tmpiter)->GetAttr( "starttime" );
		if ( !szStartTime )
		{
			D_ERROR( "starttime invalid, load xml file failed.%s\n", szFileName );
			return false;
		}
		//RunTime		场景开启持续时间
		const char * szRunTime = (*tmpiter)->GetAttr( "runtime" );
		if ( !szRunTime )
		{
			D_ERROR( "runtime invalid, load xml file failed.%s\n", szFileName );
			return false;
		}
		//entercondition 进入条件(属性)  类型,数值;类型,数值
		const char * szEnterCondition = (*tmpiter)->GetAttr( "entercondition" );
		if ( !szEnterCondition )
		{
			D_ERROR( "szEnterCondition invalid, load xml file failed.%s\n", szFileName );
			return false;
		}
		//leavecondition 离开条件(属性)  类型,数值;类型,数值
		const char * szLeaveCondition = (*tmpiter)->GetAttr( "leavecondition" );
		if ( !szLeaveCondition )
		{
			D_ERROR( "szLeaveCondition invalid, load xml file failed.%s\n", szFileName );
			return false;
		}

		//SceneBuff	进入时肯定要附加的BUFF，同时离开时也是必须要删除的BUFF
		const char * szSceneBuff = (*tmpiter)->GetAttr( "scenebuff" );
		if ( !szSceneBuff )
		{
			D_ERROR( "scenebuff invalid, load xml file failed.%s\n", szFileName );
			return false;
		}
		//SceneFlag	场景标记(限制)-禁止战斗，禁止PK，禁止摆摊，禁止使用传送道具（传出），不可骑乘，不可寻路
		const char * szSceneFlag = (*tmpiter)->GetAttr( "sceneflag" );
		if ( !szSceneFlag )
		{
			D_ERROR( "sceneflag invalid, load xml file failed.%s\n", szFileName );
			return false;
		}

		//MaxUser 最大玩家数量，超过此数量无法进入(在传送的某个地图前得判断该场景人数)
		const char * szMaxUser = (*tmpiter)->GetAttr( "maxuser" );
		if ( !szMaxUser )
		{
			D_ERROR( "maxuser invalid, load xml file failed.%s\n", szFileName );
			return false;
		}
		//MapSrvId 离开时有这个BUFF才可以离开(同时删除)
		const char * szMapSrvId = (*tmpiter)->GetAttr( "mapsrvid" );
		if ( !szMapSrvId )
		{
			D_ERROR( "mapsrvid invalid, load xml file failed.%s\n", szFileName );
			return false;
		}

 
		unsigned int i;

		pTmpRead.nSceneIndex = atoi( szSceneId );
		pTmpRead.nSceneType = atoi( szSceneType );
		sprintf_s(pTmpRead.szSceneName,sizeof(pTmpRead.szSceneName),"%s",szSceneName);
		pTmpRead.mapFileName = srvpathfile;
		pTmpRead.dwStartTime	= atoi( szStartTime );
		pTmpRead.dwRunTime		= atoi( szRunTime );


		std::vector<string> vString1;
		std::vector<string> vString2;
		vString1.clear();
		vString2.clear();
		SplitString( szEnterCondition, vString1, ";" );
		for ( i=0; i<vString1.size(); i++ )
		{
			vString2.clear();
			SplitString(vString1[i].c_str(),vString2, ",");			
			if ( vString2.size() == 2 )
			{
				ToReadMapInfo::SCondition		sInfo;
				sInfo.nType		= atoi(vString2[0].c_str());
				sInfo.nValue	= atoi(vString2[1].c_str());
				pTmpRead.vEnterConditions.push_back( sInfo );
			}			
		}

		vString1.clear();
		vString2.clear();
		SplitString( szLeaveCondition, vString1, ";" );
		for ( i=0; i<vString1.size(); i++ )
		{
			vString2.clear();
			SplitString(vString1[i].c_str(),vString2, ",");			
			if ( vString2.size() == 2 )
			{
				ToReadMapInfo::SCondition		sInfo;
				sInfo.nType		= atoi(vString2[0].c_str());
				sInfo.nValue	= atoi(vString2[1].c_str());
				pTmpRead.vLeaveConditions.push_back( sInfo );
			}			
		}
 
		vString1.clear();
		SplitString( szSceneBuff, vString1, "," );
		for ( i=0; i<vString1.size(); i++ )
		{
			int nBuffIDX = atoi(vString1[i].c_str());
			pTmpRead.vSceneBuff.push_back( nBuffIDX );
		}

		vString1.clear();
		SplitString( szSceneFlag, vString1, "," );
		for ( i=0; i<vString1.size(); i++ )
		{
			int nBuffIDX = atoi(vString1[i].c_str());
			pTmpRead.vSceneFlag.push_back( nBuffIDX );
		}

		outMapNames.push_back( pTmpRead );
	}

	return true;
	TRY_END;
	return false;
}

//初始化所有地图的地图属性；
bool CManMapProperty::InitAllMapProperty()
{
	TRY_BEGIN;

	vector<ToReadMapInfo> tmptoread;
	ReadAvailibleMapName( tmptoread );

	CMuxMapProperty* pMapProperty = NULL;
	//读入各地图；
	for ( vector<ToReadMapInfo>::iterator iter = tmptoread.begin(); iter != tmptoread.end(); ++iter )
	{
		pMapProperty = NEW CMuxMapProperty;
		pMapProperty->SetToReadMapInfo(*iter);
		if ( iter->isCopy )
		{
			pMapProperty->SetIsCopyMap();
		}
		string strPath = "config/mapconfig/map/";
		strPath = strPath + iter->mapFileName;
		bool bRet = pMapProperty->ReadMapInfo( strPath.c_str() );
		if( !bRet )
		{
			delete pMapProperty; 
			pMapProperty = NULL;
			return false;
		}

		unsigned int curmapid = pMapProperty->GetPropertyMapNo();
		map< unsigned int, CMuxMapProperty* >::iterator mapiter = m_mapMapPropertys.find( curmapid );
		if ( m_mapMapPropertys.end() != mapiter )
		{
			if ( NULL != mapiter->second )
			{
				D_ERROR( "InitAllMapProperty,地图(%s:%d)，同ID地图(%s)之前已读入\n", iter->mapFileName.c_str(), curmapid, mapiter->second->GetMapName() );
			} 
			else 
			{
				D_ERROR( "InitAllMapProperty,地图(%s:%d)，之前同ID地图空\n", iter->mapFileName.c_str(), curmapid );
			}
			delete pMapProperty; 
			pMapProperty = NULL;
			return false;
		}

		//pNewMap->FillMapMonsterCreator();//读属于本地图的怪物生成器, by dzj, 07.12.27；
		m_mapMapPropertys.insert( pair<unsigned int, CMuxMapProperty*>( curmapid, pMapProperty ) );
		D_DEBUG( "InitAllMapProperty read map %d's property\n", curmapid );
	}

	//读入各地图复活点，并将这些复活点添加到相应地图中去；
	if ( !ReadRebirthConfig() )
	{
		D_ERROR( "读地图复活点配置失败\n" );
		return false;
	}

	CMuxMapProperty* pToProcProperty = NULL;
	for ( map<unsigned int, CMuxMapProperty*>::iterator iter = m_mapMapPropertys.begin(); 
		iter != m_mapMapPropertys.end(); ++iter )
	{
		pToProcProperty = iter->second;
		if ( NULL != pToProcProperty )
		{
			pToProcProperty->FillRebirthPt();
		}		
	}

	return true;
	TRY_END;
	return false;
}

///得到所有的普通地图地图号
bool CManMapProperty::GetAllNormalMapID( vector<unsigned short>& outNormalMapIDs )
{
	CMuxMapProperty* mapProp = NULL;
	for ( map< unsigned int, CMuxMapProperty* >::iterator iter=m_mapMapPropertys.begin(); iter!=m_mapMapPropertys.end(); ++iter )
	{
		mapProp = iter->second;
		if ( NULL != mapProp )
		{
			if ( !(mapProp->IsCopyMap()) )
			{
				//all normal map maned by self;
				outNormalMapIDs.push_back( mapProp->GetPropertyMapNo() );
			}
		}
	}

	return true;
}

//释放所有地图的地图属性；
bool CManMapProperty::ReleaseAllMapProperty()
{
	CMuxMapProperty* mapProp = NULL;
	for ( map< unsigned int, CMuxMapProperty* >::iterator iter=m_mapMapPropertys.begin(); iter!=m_mapMapPropertys.end(); ++iter )
	{
		mapProp = iter->second;
		if ( NULL != mapProp )
		{
			delete mapProp; mapProp = NULL;
		}
	}

	m_mapMapPropertys.clear();

	return false;
}

///读入各地图复活点，并将这些复活点添加到相应地图中去；
bool CManMapProperty::ReadRebirthConfig()
{
	TRY_BEGIN;

	CXmlManager xmlLoader;
	if(!xmlLoader.Load("config/mapconfig/rebirthpoint.xml"))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}

	//<RebirthPoint PointID="1101201" PointType="city" LocusMap="1101" LeftTopXPos="473" LeftTopYPos="56" RightBottomXPos="477" RightBottomYPos="88" /> 
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin(); tmpiter != elementSet.end(); ++tmpiter )
	{
		const char* pTmpLocusMap = (*tmpiter)->GetAttr("LocusMap");//先根据地图编号找到对应的地图，如果本mapsrv不管理该地图，则不再继续读出生点信息；
		if ( NULL == pTmpLocusMap )
		{
			D_ERROR("rebirthpoint.xml中格式错，读第%d条记录的LocusMap错\n", tmpiter - elementSet.begin() );
			return false;
		}
		unsigned long tmpMapID = atoi( pTmpLocusMap );//读入重生点所在的地图号；

		CMuxMapProperty* pMapProperty = FindMapProperty( tmpMapID );//检查相应地图是否被本mapsrv管理；
		if ( NULL == pMapProperty )
		{
			//没有管理该重生点对应的地图属性；
			D_DEBUG( "ReadRebirthConfig,重生点对应地图%d不由本mapsrv管理\n", tmpMapID );
			continue;
		}

		MapRebirthPt* pRebirthPt = NEW MapRebirthPt;//new 一个新的重生点，并开始为该重生点赋值；

		pRebirthPt->rptMapID = tmpMapID;

		const char* pTmpPointID = (*tmpiter)->GetAttr("PointID");
		if ( NULL == pTmpPointID )
		{
			D_ERROR("rebirthpoint.xml中格式错，读第%d条记录的PointID错\n", tmpiter - elementSet.begin() );
			delete pRebirthPt; pRebirthPt = NULL;
			return false;
		}
		pRebirthPt->rptID = atoi( pTmpPointID );

		const char* pTmpPointType = (*tmpiter)->GetAttr("PointType");
		if ( NULL == pTmpPointType )
		{
			D_ERROR("rebirthpoint.xml中格式错，读第%d条记录的PointType错\n", tmpiter - elementSet.begin() );
			delete pRebirthPt; pRebirthPt = NULL;
			return false;
		}
		pRebirthPt->rptType = (MRPT_TYPE)atoi( pTmpPointType );	  //增加新的复活点信息	

		const char* pTmpLeftTopXPos = (*tmpiter)->GetAttr("LeftTopXPos");
		if ( NULL == pTmpLeftTopXPos )
		{
			D_ERROR("rebirthpoint.xml中格式错，读第%d条记录的LeftTopXPos错\n", tmpiter - elementSet.begin() );
			delete pRebirthPt; pRebirthPt = NULL;
			return false;
		}
		pRebirthPt->rptLeftTop.nPosX = atoi( pTmpLeftTopXPos );

		const char* pTmpLeftTopYPos = (*tmpiter)->GetAttr("LeftTopYPos");
		if ( NULL == pTmpLeftTopYPos )
		{
			D_ERROR("rebirthpoint.xml中格式错，读第%d条记录的LeftTopYPos错\n", tmpiter - elementSet.begin() );
			delete pRebirthPt; pRebirthPt = NULL;
			return false;
		}
		pRebirthPt->rptLeftTop.nPosY = atoi( pTmpLeftTopYPos );

		const char* pTmpRightBottomXPos = (*tmpiter)->GetAttr("RightBottomXPos");
		if ( NULL == pTmpRightBottomXPos )
		{
			D_ERROR("rebirthpoint.xml中格式错，读第%d条记录的RightBottomXPos错\n", tmpiter - elementSet.begin() );
			delete pRebirthPt; pRebirthPt = NULL;
			return false;
		}
		pRebirthPt->rptRightBottom.nPosX = atoi( pTmpRightBottomXPos );
		
		const char* pTmpRightBottomYPos = (*tmpiter)->GetAttr("RightBottomYPos");
		if ( NULL == pTmpRightBottomYPos )
		{
			D_ERROR("rebirthpoint.xml中格式错，读第%d条记录的RightBottomYPos错\n", tmpiter - elementSet.begin() );
			delete pRebirthPt; pRebirthPt = NULL;
			return false;
		}
		pRebirthPt->rptRightBottom.nPosY = atoi( pTmpRightBottomYPos );

		//最后判断 左上右下是否符合规则
		if( pRebirthPt->rptLeftTop.nPosX > pRebirthPt->rptRightBottom.nPosX || pRebirthPt->rptLeftTop.nPosY > pRebirthPt->rptRightBottom.nPosY )
		{
			D_ERROR("rebirthpoint.xml中复活点左上比右下点大，读第%d条记录的RightBottomYPos错\n", tmpiter - elementSet.begin() );
			delete pRebirthPt; pRebirthPt = NULL;
			return false;
		}

		if ( !pMapProperty->AddRebirthRgn( pRebirthPt ) )//将重生点加到相应地图中去；
		{
			D_ERROR("ReadRebirthConfig，rebirthpoint.xml第%d条记录的重生点加入对应地图时出错\n", tmpiter - elementSet.begin() );
			delete pRebirthPt; pRebirthPt = NULL;
			return false;
		}
	}

	return true;
	TRY_END;
	return false;
}





