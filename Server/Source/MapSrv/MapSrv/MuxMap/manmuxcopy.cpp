﻿/**
* @file manmuxcopy.cpp
* @brief 定义副本管理器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: manmuxcopy.cpp
* 摘    要: 管理活动副本;
* 作    者: dzj
* 开始日期: 2009.09.14
* 
*/

#include "manmuxcopy.h"

#include "manmapproperty.h"
#include "../LogManager.h"

map<unsigned int, CNewMap*> CManMuxCopy::m_allCopys;//本mapsrv管理的所有副本(副本ID号<-->副本指针)；
//unsigned int CManMuxCopy::m_copyidGener;//副本号生成依据号，每生成一个新的副本+1;
MGGlobeCopyInfos  CManMuxCopy::m_globePCopyInfos;//全局伪副本信息；

unsigned int CManMuxCopy::m_copyMapPoolCurPos=0;//当前的分配位置，每次都从末尾分配并释放回末尾
CNewMap**     CManMuxCopy::m_arrCopyMapPool=NULL;//副本地图池

unsigned int  CManMuxCopy::m_copyMapPoolSize = 0;//副本地图池大小

int           CManMuxCopy::m_LastEPos = 0;//用于遍历副本房间；


//从池中分配副本地图；
CNewMap* CManMuxCopy::GetNewCopyMapFromPool()
{
	if ( m_copyMapPoolCurPos <= 0 )
	{
		D_ERROR( "CManMuxCopy::CreateNewCopy，副本地图池已用完，不能创建新副本\n" );
		return NULL;
	}

	//副本池中还有可用副本；
	--m_copyMapPoolCurPos;

	if ( m_copyMapPoolCurPos>=m_copyMapPoolSize )
	{
		D_ERROR( "不可能错误，当前可分配位置%d错\n", m_copyMapPoolCurPos );
		return NULL;
	}

	CNewMap* pNewCopyMap = m_arrCopyMapPool[m_copyMapPoolCurPos] ;
	if ( NULL == pNewCopyMap )
	{
		D_ERROR( "不可能错误，欲分配副本位置%d副本指针为空\n", m_copyMapPoolCurPos );
		return NULL;
	}

	m_arrCopyMapPool[m_copyMapPoolCurPos] = NULL;

	return pNewCopyMap;
}

//释放副本地图回池中；
void CManMuxCopy::ReleaseCopyMapToPool( CNewMap* pToReleaseMap )
{
	if ( NULL == pToReleaseMap )
	{
		D_ERROR( "ReleaseCopyMapToPool, 不可能错误，输入的欲释放地图空\n" );
		return;
	}

	if ( m_copyMapPoolCurPos>=m_copyMapPoolSize )
	{
		D_ERROR( "ReleaseCopyMapToPool, 不可能错误，欲释放至位置%d错\n", m_copyMapPoolCurPos );
		return;
	}

	if ( NULL != m_arrCopyMapPool[m_copyMapPoolCurPos] )
	{
		D_ERROR( "ReleaseCopyMapToPool, 不可能错误，欲释放位置%d副本指针非空\n", m_copyMapPoolCurPos );
		return;
	}

	pToReleaseMap->ReleaseMuxMap();

	m_arrCopyMapPool[m_copyMapPoolCurPos] = pToReleaseMap;
	++m_copyMapPoolCurPos;

	return;
}

///创建一个新副本；
bool CManMuxCopy::CreateNewCopy( unsigned int mapid/*副本对应地图*/, unsigned int copyid/*副本ID号*/ )
{
	CMuxMapProperty* pMapProperty = CManMapProperty::FindMapProperty( mapid );
	if ( NULL == pMapProperty )
	{
		D_ERROR( "CManMuxCopy::CreateNewCopy, can not find map property %d\n", mapid );
		return false;
	}
	if ( !(pMapProperty->IsCopyMap()) )
	{
		D_ERROR( "CManMuxCopy::CreateNewCopy, map property %d is not copy map\n", mapid );
		return false;
	}

	//检查原来是否有相同的副本实例号副本，防止原副本丢失以及管理混乱；
	CNewMap* pNewCopyMap = NULL;
	bool isOrgExist = FindCopyByCopyID( copyid/*副本ID号*/, pNewCopyMap/*所找到的副本*/ );
	if ( isOrgExist )
	{
		D_ERROR( "不可能错误，CManMuxCopy::CreateNewCopy，重复的副本号:%d\n", copyid );
		return false;
	}

	//副本池中分配副本；
	pNewCopyMap = GetNewCopyMapFromPool();
	if ( NULL == pNewCopyMap )
	{
		D_ERROR( "CManMuxCopy::CreateNewCopy，副本地图池分配副本失败，自身是否副本服务器%d\n", CLoadSrvConfig::IsCopyMapSrv()?1:0 );
		return false;
	}

	if ( !(pNewCopyMap->InitMapProperty( pMapProperty )) )
	{
		D_ERROR( "CManMuxCopy::CreateNewCopy，副本地图%dInitMapProperty失败\n", mapid );
		ReleaseCopyMapToPool( pNewCopyMap );
		return false;
	}

	//copyid = GetOneNewCopyID();
	if ( (!pNewCopyMap->InitMapCopyInfo( copyid )) )
	{
		D_ERROR( "CManMuxCopy::CreateNewCopy, InitMapCopyInfo失败,mapid%d,copyid%d\n", mapid, copyid );
		ReleaseCopyMapToPool( pNewCopyMap );
		return false;
	}
	m_allCopys.insert( pair<unsigned int, CNewMap*>( copyid, pNewCopyMap ) );//稍后刷怪时要查找此地图，因此先加入管理器；

	pNewCopyMap->FillMapMonsterCreator();//读属于本地图的怪物生成器, by dzj, 07.12.27；
	pNewCopyMap->FirstRefreshMonster();//生成该副本怪物；

	D_DEBUG( "创建副本(uid:%d)，地图号%d\n", copyid, mapid );

	//调试进出副本，pNewCopyMap->IssueDelCopyQuest();//马上发起删除，调试玩家进入边缘条件；
	//pNewCopyMap->IssueDelCopyQuest();//马上发起删除，调试玩家进入边缘条件；

	return true;
}

///通过副本ID找副本
bool CManMuxCopy::FindCopyByCopyID( unsigned int copyid/*副本ID号*/, CNewMap*& pFoundCopy/*所找到的副本*/ )
{
	pFoundCopy = NULL;

	map<unsigned int, CNewMap*>::iterator iter = m_allCopys.find( copyid );
	if ( m_allCopys.end() == iter )
	{
		return false;//指定的副本不存在；
	}

	pFoundCopy = iter->second;
	
	return true;
}

///查询全局副本信息(本mapsrv管理的伪副本)
bool CManMuxCopy::OnQueryGlobePCopyInfo( const PlayerID& playerID ) 
{
	if ( m_globePCopyInfos.globeCopyInfos.infoNum <= 0 )
	{
		//本mapsrv没有管理任何伪副本；
		return true;
	}

	m_globePCopyInfos.notiPlayerID = playerID;

	CGateSrv* pGateSrv = CManG_MProc::FindProc( playerID.wGID );
	if( NULL == pGateSrv )
	{
		D_ERROR( "OnQueryGlobePCopyInfo 找不到GateSrv%d 不能发送消息\n", playerID.wGID );
		return false;
	}
	MsgToPut* pNewMsg = CreateSrvPkg( MGGlobeCopyInfos, pGateSrv, m_globePCopyInfos );
	pGateSrv->SendPkgToGateServer( pNewMsg );//通知伪副本查询结果；

	return true; 
};

///查询某伪副本是否可进
bool CManMuxCopy::IsPCopyOpen( unsigned short pcopyMapID )
{
	for ( unsigned int i=0; i<m_globePCopyInfos.globeCopyInfos.infoNum; ++i )
	{
		if ( i >= ARRAY_SIZE(m_globePCopyInfos.globeCopyInfos.infoArr) )
		{
			D_ERROR( "CManMuxCopy::IsPCopyOpen, i(%d) >= ARRAY_SIZE(m_globePCopyInfos.globeCopyInfos.infoArr)\n", i );
			return false;
		}
		if ( m_globePCopyInfos.globeCopyInfos.infoArr[i].copyMapID == pcopyMapID )
		{
			return true;//此伪副本在开启列表中；
		}
	}

	return false;
}

///开启伪副本;
bool CManMuxCopy::OnPCopyOpen( unsigned int pcopyMapID/*欲开启伪副本的mapid*/ )
{
	if ( m_globePCopyInfos.globeCopyInfos.infoNum >= ARRAY_SIZE(m_globePCopyInfos.globeCopyInfos.infoArr) )
	{
		D_WARNING( "欲开启伪副本%d时，发现本mapsrv管理的伪副本数量%d已超限\n", pcopyMapID, m_globePCopyInfos.globeCopyInfos.infoNum );
		return false;
	}

	for ( unsigned int i=0; i<m_globePCopyInfos.globeCopyInfos.infoNum; ++i )
	{
		if ( m_globePCopyInfos.globeCopyInfos.infoArr[i].copyMapID == pcopyMapID )
		{
			D_WARNING( "欲开启伪副本%d时，发现此伪副本之前已开启\n", pcopyMapID );
			return false;
		}
	}

	D_DEBUG( "开启伪副本%d\n", pcopyMapID );
	CLogManager::DoInstanceChange(pcopyMapID, 0, LOG_SVC_NS::CPT_CREATE);//记日至

	m_globePCopyInfos.globeCopyInfos.infoArr[m_globePCopyInfos.globeCopyInfos.infoNum].copyMapID = pcopyMapID;
	m_globePCopyInfos.globeCopyInfos.infoArr[m_globePCopyInfos.globeCopyInfos.infoNum].curCopyNum = 1;
	m_globePCopyInfos.globeCopyInfos.infoArr[m_globePCopyInfos.globeCopyInfos.infoNum].isCopyOpen = true;
	m_globePCopyInfos.globeCopyInfos.infoArr[m_globePCopyInfos.globeCopyInfos.infoNum].isMaxCopyNum = false;//由于是伪副本，因此没有此限制；

	++(m_globePCopyInfos.globeCopyInfos.infoNum);

	return true;
}

///关闭伪副本;
bool CManMuxCopy::OnPCopyClose( unsigned int pcopyMapID/*欲开启伪副本的mapid*/ )
{
	if ( m_globePCopyInfos.globeCopyInfos.infoNum <= 0 )
	{
		D_WARNING( "欲关闭伪副本%d时，发现本mapsrv管理的伪副本<0为%d\n", pcopyMapID, m_globePCopyInfos.globeCopyInfos.infoNum );
		return false;
	}

	for ( unsigned int i=0; i<m_globePCopyInfos.globeCopyInfos.infoNum; ++i )
	{
		if ( i >= ARRAY_SIZE(m_globePCopyInfos.globeCopyInfos.infoArr) )
		{
			D_ERROR( "CManMuxCopy::IsPCopyOpen, i(%d) >= ARRAY_SIZE(m_globePCopyInfos.globeCopyInfos.infoArr)\n", i );
			return false;
		}
		if ( m_globePCopyInfos.globeCopyInfos.infoArr[i].copyMapID == pcopyMapID )
		{
			if ( (m_globePCopyInfos.globeCopyInfos.infoNum-1) >= ARRAY_SIZE(m_globePCopyInfos.globeCopyInfos.infoArr) )
			{
				D_ERROR( "CManMuxCopy::IsPCopyOpen, (m_globePCopyInfos.globeCopyInfos.infoNum-1)(%d) >= ARRAY_SIZE(m_globePCopyInfos.globeCopyInfos.infoArr)\n"
					, (m_globePCopyInfos.globeCopyInfos.infoNum-1) );
				return false;
			}
			//删除本元素；
			m_globePCopyInfos.globeCopyInfos.infoArr[i].copyMapID = m_globePCopyInfos.globeCopyInfos.infoArr[m_globePCopyInfos.globeCopyInfos.infoNum-1].copyMapID;
			m_globePCopyInfos.globeCopyInfos.infoArr[i].curCopyNum = m_globePCopyInfos.globeCopyInfos.infoArr[m_globePCopyInfos.globeCopyInfos.infoNum-1].curCopyNum;
			m_globePCopyInfos.globeCopyInfos.infoArr[i].isCopyOpen = m_globePCopyInfos.globeCopyInfos.infoArr[m_globePCopyInfos.globeCopyInfos.infoNum-1].isCopyOpen;
			m_globePCopyInfos.globeCopyInfos.infoArr[i].isMaxCopyNum = m_globePCopyInfos.globeCopyInfos.infoArr[m_globePCopyInfos.globeCopyInfos.infoNum-1].isMaxCopyNum;
			--(m_globePCopyInfos.globeCopyInfos.infoNum);
			D_DEBUG( "关闭伪副本%d\n", pcopyMapID );
			CLogManager::DoInstanceChange(pcopyMapID, 0, LOG_SVC_NS::CPT_DESTROY);//记日至
			return true;
		}
	}

	D_WARNING( "欲关闭伪副本%d时，发现之前并未开启此副本\n", pcopyMapID );
	return false;
}

///centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
bool CManMuxCopy::OnCenterSrvDelCopyQuest( const CopyManagerEle& copyInfo )
{
	map<unsigned int, CNewMap*>::iterator iter = m_allCopys.find( copyInfo.copyID );
	if ( m_allCopys.end() == iter )
	{
		D_ERROR( "CManMuxCopy::OnCenterSrvDelCopyQuest，欲删的副本%d不存在\n", copyInfo.copyID );
		return false;//欲删的副本不存在；
	}

	CNewMap* pFoundCopy = iter->second;
	if ( NULL == pFoundCopy )
	{
		D_ERROR( "CManMuxCopy::OnCenterSrvDelCopyQuest，找到的副本%d指针空\n", copyInfo.copyID );
		return false;
	}

	unsigned int foundcopyid = 0;
	if ( !(pFoundCopy->GetMapCopyID( foundcopyid )) )
	{
		D_ERROR( "CManMuxCopy::OnCenterSrvDelCopyQuest，取不到所找到的副本%d的副本号\n", copyInfo.copyID );
		return false;
	}

	if ( ( copyInfo.copyID != foundcopyid /*副本ID号*/ )
		|| ( copyInfo.enterReq.copyMapID != pFoundCopy->GetMapNo() /*副本地图号*/ )
		)
	{
		D_ERROR( "CManMuxCopy::OnCenterSrvDelCopyQuest，欲删的副本(mapid:%d,uid:%d)与找到的副本(mapid:%d,uid:%d)信息不匹配\n"
			, copyInfo.enterReq.copyMapID, copyInfo.copyID, pFoundCopy->GetMapNo(), foundcopyid );
		return false;//欲删的副本不存在；
	}

	//校验通过，开始删副本:1.issue no_more_player;2.for each player exec del;3.set self to_del flag;4.check until to_delflag && curplayernum <=0;
	pFoundCopy->IssueDelCopyQuest();
	//pFoundCopy->BaseMapIssueNomorePlayerQuest();
	//pFoundCopy->BaseMapKickAllCopyPlayers();
	//pFoundCopy->BaseMapDetectDelCond();

	return true;
}

///收到centersrv命令强制删某副本；
bool CManMuxCopy::ForceDelCopy( const CopyManagerEle& copyInfo )
{
	map<unsigned int, CNewMap*>::iterator iter = m_allCopys.find( copyInfo.copyID );
	if ( m_allCopys.end() == iter )
	{
		D_ERROR( "CManMuxCopy::ForceDelCopy，欲删的副本%d不存在\n", copyInfo.copyID );
		return false;//欲删的副本不存在；
	}

	CNewMap* pFoundCopy = iter->second;
	if ( NULL == pFoundCopy )
	{
		D_ERROR( "CManMuxCopy::ForceDelCopy，找到的副本%d指针空\n", copyInfo.copyID );
		return false;
	}

	unsigned int foundcopyid = 0;
	if ( !(pFoundCopy->GetMapCopyID( foundcopyid )) )
	{
		D_ERROR( "CManMuxCopy::ForceDelCopy，取不到所找到的副本%d的副本号\n", copyInfo.copyID );
		return false;
	}

	if ( ( copyInfo.copyID != foundcopyid /*副本ID号*/ )
		|| ( copyInfo.enterReq.copyMapID != pFoundCopy->GetMapNo() /*副本地图号*/ )
		)
	{
		D_ERROR( "CManMuxCopy::ForceDelCopy，欲删的副本(mapid:%d,uid:%d)与找到的副本(mapid:%d,uid:%d)信息不匹配\n"
			, copyInfo.enterReq.copyMapID, copyInfo.copyID, pFoundCopy->GetMapNo(), foundcopyid );
		return false;//欲删的副本不存在；
	}

	if ( pFoundCopy->GetMapPlayerNum() > 0 )
	{
		D_ERROR( "CManMuxCopy::ForceDelCopy，欲删的副本(mapid:%d,uid:%d)中仍有玩家\n"
			, copyInfo.enterReq.copyMapID, copyInfo.copyID );
		return false;
	}

	ReleaseCopyMapToPool( pFoundCopy );

	m_allCopys.erase( iter );

	return true;
}

///执行所管理副本的时钟循环；
bool CManMuxCopy::ManCopyTimerProc( const ACE_Time_Value& curTime )
{
	if ( m_LastEPos >= (int)(m_allCopys.size()) )
	{
		m_LastEPos = 0;
	}

	static ACE_Time_Value lastBigLoopTime = ACE_OS::gettimeofday();
	static int            smallLoopNum = 1;//各小循环的每次遍历数，自适应调整，以小值开始启动；
	static int            bigLoopExped = 0;//大循环中已逝去小循环的已遍历数；
	int passedmsec = (ACE_OS::gettimeofday() - lastBigLoopTime).msec();//大循环已逝去毫秒数；
	int numpere = smallLoopNum;//本次遍历数，将在AutoExpNumCal中被修改；
	bool isUpdateLastBigLoopTime = false;//是否更新大循环起始时刻；
	AutoExpNumCal( passedmsec, (const int)m_allCopys.size(), smallLoopNum, bigLoopExped, numpere, isUpdateLastBigLoopTime );		
	if ( isUpdateLastBigLoopTime )
	{
		lastBigLoopTime = ACE_OS::gettimeofday();
	}
	int numcure = 0;//当前遍历的副本地图数；

	CNewMap* pCopyMap = NULL;

	map<unsigned int, CNewMap*>::iterator iter = m_allCopys.begin();
	advance( iter, m_LastEPos );	
	if ( iter == m_allCopys.end() )
	{
		iter = m_allCopys.begin();
		m_LastEPos = 0;
	}

	for ( ; iter!=m_allCopys.end(); )
	{
		if ( numcure >= numpere )
		{
			break;//已遍历超过此次欲遍历数；
		}

		++numcure;//本次已遍历的副本数；
		++m_LastEPos;//最后遍历的元素位置；

		pCopyMap = iter->second;
		if ( NULL == pCopyMap )
		{
			D_ERROR( "副本地图遍历，NULL == pCopyMap\n" );
			m_allCopys.erase( iter++ );
			continue;
		}

		pCopyMap->MapCopyTimerProc( curTime );
		++iter;//刷新下一遍历副本位置；
	}

	//CNewMap* pNewMap = NULL;
	//for ( map<unsigned int, CNewMap*>::iterator iter=m_allCopys.begin(); iter!=m_allCopys.end(); )
	//{
	//	pNewMap = iter->second;
	//	if ( NULL != pNewMap )
	//	{
	//		pNewMap->MapCopyTimerProc( curTime );//统一由centersrv发起副本删除；
	//	}
	//	++iter;		
	//}

	return true;
}


