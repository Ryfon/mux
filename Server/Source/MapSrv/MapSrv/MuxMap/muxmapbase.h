﻿/**
* @file muxmapbase.h
* @brief 
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: muxmapbase.h
* 摘    要: 
* 作    者: dzj
* 开始日期: 2009.10.07
* 
*/

#ifndef MUX_MAPBASE_H
#define MUX_MAPBASE_H

#include "manmapproperty.h"
#include "muxnormalmap.h"
#include "muxcopy.h"
#include "maprelation.h"

class CCounterOfMap;
class CTimerOfMap;
class CDetailTimerOfMap;

class CMuxMap
{
	//感兴趣区域，用于攻击消息范围广播；
	class AreaOfInterest
	{
	private:
		static const unsigned int posMap[9];//三横三竖静态对应位置
		static const int extRevMap[16][2];//ext位置对应的块偏移值
		static const int extExtRevMap[24][2];//extext位置对应的块偏移值

	private:
		static inline bool GetLogValAsm( unsigned int inVal, unsigned int& outVal )
		{
			if ( ( 0 == inVal ) || ( inVal > 0xffff ) )
			{
				return false;
			}
			outVal = 0;
#ifdef WIN32
			__asm {
				bsf eax, inVal
					mov outVal, eax
			}
#else //WIN32
			__asm__( "BSF %%eax, %0" : "=r"(outVal) : "a"(inVal));
#endif //WIN32
			return true;
		}

	public:
		AreaOfInterest()
		{
			m_isInited = false;
			InitAreaOfInterest( 0, 0 );
		}

	private:
		//使用中心块坐标初始化区域
		bool InitAreaOfInterest( TYPE_POS cenX, TYPE_POS cenY )
		{		
			extmask = 0;
			extextmask = 0;
			m_vecExtArea.clear();//扩展区域包含块；
			m_cenX = (int)cenX;
			m_cenY = (int)cenY;
			return true;
		}

	public:
		//取核心区对应扩展区域
		vector<MuxMapBlock*>* GetExtArea( CMuxMap* pResMap );

		//重置区域，但保持中心点
		bool ResetRegion()
		{
			extmask = 0;
			extextmask = 0;
			m_vecExtArea.clear();
			return true;
		}

		//添加一块核心区域
		bool AddOneKernelBlock( TYPE_POS blockX, TYPE_POS blockY, TYPE_POS newBlockX=0, TYPE_POS newBlockY=0 )
		{
			if ( !m_isInited )
			{
				InitAreaOfInterest( blockX, blockY );
				m_isInited = true;
			}
			TYPE_POS maskX=0, maskY=0;
			if ( blockX > m_cenX )
			{
				if ( blockX != (m_cenX+1) )
				{
					D_ERROR( "AddOneKernelBlock, blockX%d != (m_cenX%d+1)\n", blockX, m_cenX );
					return false;
				}
				maskX = 2;
			} else {
				if ( blockX == m_cenX )
				{
					maskX = 1;
				} else if ( blockX+1 == m_cenX ) {
					maskX = 0;
				} else {
					D_ERROR( "AddOneKernelBlock, blockX%d+1 != m_cenX%d\n", blockX, m_cenX );
					return false;
				}
			}

			if ( blockY > m_cenY )
			{
				if ( blockY != (m_cenY+1) )
				{
					D_ERROR( "AddOneKernelBlock, blockY%d != (m_cenY%d+1)\n", blockY, m_cenY );
					return false;
				}
				maskY = 2;
			} else {
				if ( blockY == m_cenY )
				{
					maskY = 1;
				} else if ( blockY+1 == m_cenY ) {
					maskY = 0;
				} else {
					D_ERROR( "AddOneKernelBlock, blockY%d+1 != m_cenY%d\n", blockY, m_cenY );
					return false;
				}
			}

			int mappos = maskY*3 + maskX;
			if ( mappos >= ARRAY_SIZE(posMap) )
			{
				D_ERROR( "AddOneKernelBlock, maskY%d，maskX%d\n", maskY, maskX );	  
				return false;
			}			
			switch( posMap[mappos] )
			{
			case 0:
				{
					extmask |= (0x7<<0 | 0x3<<14);		
				}
				break;
			case 1:
				{
					extmask |= (0x7<<1);		
				}
				break;
			case 2:
				{
					extmask |= (0x1f<<2);		
				}
				break;
			case 3:
				{
					extmask |= (0x7<<5);		
				}
				break;
			case 4:
				{
					extmask |= (0x1f<<6);		
				}
				break;
			case 5:
				{
					extmask |= (0x7<<9);		
				}
				break;
			case 6:
				{
					extmask |= (0x1f<<10);		
				}
				break;
			case 7:
				{
					extmask |= (0x7<<13);		
				}
				break;
			case 8:
				{
					extmask |= 0;//中心块		
				}
				break;
			default:
				{
					D_ERROR( "AddOneKernelBlock, maskY%d, maskX%d\n", maskY, maskX );
					return false;		
				}
			}

			if ( blockX == newBlockX && blockY == newBlockY )
			{
				return true;
			}
			else
			{
				int diffX = (int)newBlockX - (int)m_cenX;
				int diffY = (int)newBlockY - (int)m_cenY;
				switch(diffX)
				{
				case -2:
					{
						switch(diffY)
						{
						case 2:
							//extext 16 17 18 19 20
							extextmask |= (0x1f << 16);
							break;
						case 1:
							//extext 18 19 20 
							extextmask |= (0x7 << 18);
							break;
						case 0:
							//extext 20 21 22
							extextmask |= (0x7 << 20);
							break;
						case -1:
							//extext 21 22 23
							extextmask |= (0x7 << 21);
							break;
						case -2:
							//extext 0 1 2 22 23
							extextmask |= ( 0x7 | (0x3 << 22) );
							break;
						}
					}
					break;
				case -1:
					{
						switch(diffY)
						{
						case 2:
							//extext 15 16 17
							extextmask |= (0x7 << 15);
							break;
						case -2:
							//extext 1 2 3
							extextmask |= (0x7 << 1);
							break;
						}
					}
					break;
				case 0:
					{
						switch(diffY)
						{
						case 2:
							//extext 14 15 16
							extextmask |= (0x7 << 14);
							break;
						case -2:
							//extext 2 3 4
							extextmask |= (0x7 << 2);
							break;
						}
					}
					break;
				case 1:
					{
						switch(diffY)
						{
						case 2:
							//extext 13 14 15
							extextmask |= (0x7 << 13);
							break;
						case -2:
							//extext 3 4 5
							extextmask |= (0x7 << 3);
							break;
						}
					}
					break;
				case 2:
					{
						switch(diffY)
						{
						case 2:
							//extext 10 11 12 13 14
							extextmask |= (0x1f << 10);
							break;
						case 1:
							//extext 9 10 11
							extextmask |= (0x7 << 9);
							break;
						case 0:
							//extext 8 9 10
							extextmask |= (0x7 << 8);
							break;
						case -1:
							//extext 7 8 9
							extextmask |= (0x7 << 7);
							break;
						case -2:
							//extext 4 5 6 7 8
							extextmask |= (0x1f << 4);
							break;
						}
					}
					break;
				}
			}

			return true;
		}

	private:
		bool m_isInited;
		vector<MuxMapBlock*> m_vecExtArea;//扩展区域包含块；
		int m_cenX;
		int m_cenY;

		/*
		ext~0   ext~1  ext~2 ext~3 ext~4    ext~5  ext~6
		ext~23  ext0   ext1  ext2  ext3     ext4   ext~7
		ext~22	ext15   0     1     2       ext5   ext~8
		ext~21	ext14   7     8     3       ext6   ext~9
		ext~20	ext13   6     5     4       ext7   ext~10
		ext~19	ext12  ext11  ext10  ext9   ext8   ext~11
		ext~18  ext~17 ext~16 ext~15 ext~14 ext~13 ext~12

		111 = 0x7; 11111 = 1+2+4+8+16 = 0x1f
		0: (111<<0 | 11<<14), 1: 111<<1, 2: 11111<<2, 3: 111<<5, 4: 11111<<6
		5: 111<<9, 6: 11111<<10, 7: 111<<13
		*/
		unsigned short extmask;//内部各bit标明上述ext位置是否影响
		unsigned int extextmask;//内部bit标明上述extext位置是否影响

	};//class AreaOfInterest

	//攻击中的减血信息，用于在攻击完成后统一减血，保证消息顺序
	struct SubHpInfo
	{
	public:
		SubHpInfo()
		{
			isCheckWeaponSkill = false;
			pTgtMonster = NULL;
			pTgtPlayer = NULL;
			battleFlag = MISS;
			hpSubed = 0;
			hpLeft = 0;
			reactTime = 0;
			isBuffSelf = false;
			buffID = 0;
			buffTime = 0;
		}

	public:
		static bool WeaponSkillPvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster );
        static bool WeaponSkillPvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer );

	public:
		//攻击目标数组中的一个目标减血；
		bool SubOneTgtHp( CPlayer* pAttacker, CMuxPlayerSkill* pSkill, CBattleScript* pBattleScript );

	public:
		bool      isCheckWeaponSkill;//是否原始目标
		CMonster* pTgtMonster;
		CPlayer* pTgtPlayer;
		EBattleFlag battleFlag;
		unsigned int hpSubed;
		unsigned int hpLeft;
		unsigned int reactTime;
		bool         isBuffSelf;//是否对自身施放buff
		unsigned int buffID;//可能的附带buffID;
		unsigned int buffTime;
	};//struct SubHpInfo

	struct SubHpInfoArr{ //单次攻击的总减血信息
	public:
		SubHpInfoArr()
		{
			ResetSubHpInfoArr();
		}

	public:
		void ResetSubHpInfoArr()
		{
			StructMemSet( m_SubHpArr, 0, sizeof(m_SubHpArr) );
			curTgtNum = 0;
		}

		bool AddOneSubInfo( bool isCheckWeaponSkill, CPlayer* pPlayer, CMonster* pMonster, int subHp, unsigned int reactTime
			, EBattleFlag battleFlag, bool isBuffSelf/*攻击者自身或攻击目标*/, unsigned int buffID, unsigned int buffTime )
		{
			if ( curTgtNum >= ARRAY_SIZE(m_SubHpArr) )
			{
				D_ERROR( "SubHpInfoArr::AddOneSubInfo，伤血目标数太多\n" );
				return false;
			}

			if ( (subHp <= 0) && (battleFlag != MISS) )
			{
				D_WARNING( "AddOneSubInfo，伤血为0但返回为非MISS\n" );
			}

			m_SubHpArr[curTgtNum].isCheckWeaponSkill = isCheckWeaponSkill;
			m_SubHpArr[curTgtNum].pTgtPlayer = pPlayer;
			m_SubHpArr[curTgtNum].pTgtMonster = pMonster;
			m_SubHpArr[curTgtNum].hpSubed = subHp;
			m_SubHpArr[curTgtNum].reactTime = reactTime;
			m_SubHpArr[curTgtNum].battleFlag = battleFlag;
			m_SubHpArr[curTgtNum].isBuffSelf = isBuffSelf;
			m_SubHpArr[curTgtNum].buffID = buffID;
			m_SubHpArr[curTgtNum].buffTime = buffTime;

			++curTgtNum;

			return true;
		}

	public:
		//对数组中各目标进行减血；
		bool SubArrTgtsHp( CPlayer* pAttacker, CMuxPlayerSkill* pSkill );

	private:
		static CGUseSkill2 dumpUseSkill;
		SubHpInfo m_SubHpArr[ARRAY_SIZE(dumpUseSkill.arrTarget)];//单个攻击消息可能目标数最大为CGUseSkill中相应数组的元素个数
		unsigned int curTgtNum;
	};//struct SubHpInfoArr{ //单次攻击的总减血信息

	enum MAPINS_TYPE
	{
		MUX_NORMAL_MAP = 0,
		MUX_COPY_MAP = 1
	};

private:
	CMuxMap( const CMuxMap& map);
	CMuxMap& operator = ( const CMuxMap& map ); //屏蔽这两个操作；

public:
	CMuxMap( bool isNormalOrCopy, unsigned short width, unsigned short height );

	~CMuxMap();

	///使用地图属性初始化地图;
	bool InitMapProperty( CMuxMapProperty* pMapProperty );

	///初始化副本地图的副本相关信息；
	bool InitMapCopyInfo( unsigned int copyid );

	///释放地图内存,???,邓子建检查；
	void ReleaseMuxMap();

public:
	inline CMuxMapProperty* GetMapProperty() { return m_pMapProperty; }

    //获取攻城战附加信息
	MapWarAdditionalInfo* GetWarAdditionalInfo(){ return m_pWarAdditionInfo; }
	
	//设置攻城战地图关联信息
	void InitWarMapRelation(void);

	//攻城战开始
	bool SetWarStart(void);
	
	//攻城战结束
	bool SetWarEnd();

	//是否在攻城战中
	bool InWarState(void);

	//获取攻城战开启时间
	unsigned int GetWarStartTime(void);
	
	//获取攻城战内城复活点
	bool GetWarInnerRebirthPt(unsigned short race, unsigned short &mapId, unsigned int &posX, unsigned int &posY, unsigned char &err);
	
	//获取攻城战外城复活点
	bool GetWarOuterRebirthPt(unsigned short race, unsigned short &mapId, unsigned int &posX, unsigned int &posY, unsigned char &err);
	
	//获取攻城战外城缺省复活点
	bool GetWarDefaultRebirthPt(unsigned short race, unsigned short &mapId, unsigned int &posX, unsigned int &posY, unsigned char &err);

	//修改攻城战复活点种族归属
	bool ModityWarRebirthRace(unsigned short race);

	//修改攻城战跳转点种族归属
	bool ModityWarSwitchRace(unsigned short race);
	//杀死所有地图上的玩家
	bool KillAllPlayerByRace( unsigned int race );
	//传送玩家
	bool SwitchMapPlayer( unsigned int race, unsigned int mapID, unsigned int posX1, unsigned int posY1, unsigned int posX2, unsigned int posY2 );
	//  [1/5/2010 shenchenkai]
	//检测是否有异族进入地图并通知
	//去除种族后废弃，by dzj, 10.07.30;//void OnCheckPlayerEnterMapNotify(CPlayer * pPlayer);
	//检测是否有异族攻击NPC并通知
	//种族废弃后去除，by dzj, 10.07.30//void OnCheckPlayerAttackNpcNotify(CPlayer * pPlayer);
	//  [1/5/2010 shenchenkai]


private:
	CMuxMapProperty* m_pMapProperty;//对应地图属性；
	
	MapWarAdditionalInfo *m_pWarAdditionInfo;//攻城战附加信息（内外城关系，复活点归属，跳转点归属）

	//  [1/5/2010 shenchenkai]
	ACE_Time_Value			m_tInvade;//外族入侵时间
	ACE_Time_Value			m_tAttackNpc;//外族攻击npc时间
	//  [1/5/2010 shenchenkai]

public:
	///发起删除副本；
	void IssueDelCopyQuest()
	{
		if ( !IsInsCopyMap() )
		{
			D_WARNING( "普通地图试图执行BaseMapIssueNomorePlayerQuest\n" );
			return;
		}
		if ( NULL == m_pCopyMapInfo )
		{
			D_WARNING( "BaseMapIssueNomorePlayerQuest, NULL == m_pCopyMapInfo\n" );
		}

		m_pCopyMapInfo->OnIssueDelCopyQuest();

		return;
	}

	/////发出nomoreplayer请求给center;
	//void BaseMapIssueNomorePlayerQuest()
	//{
	//	if ( !IsInsCopyMap() )
	//	{
	//		D_WARNING( "普通地图试图执行BaseMapIssueNomorePlayerQuest\n" );
	//		return;
	//	}
	//	if ( NULL == m_pCopyMapInfo )
	//	{
	//		D_WARNING( "BaseMapIssueNomorePlayerQuest, NULL == m_pCopyMapInfo\n" );
	//	}
	//	m_pCopyMapInfo->IssueNomorePlayerQuest();
	//	return;
	//}

	////只会由copy自身检测到并发出///发出delselfcopy请求给center;
	////void BaseMapIssueDelSelfCopyQuest()
	////{
	////	if ( !IsInsCopyMap() )
	////	{
	////		D_WARNING( "普通地图试图执行BaseMapIssueNomorePlayerQuest\n" );
	////		return;
	////	}
	////	if ( NULL == m_pCopyMapInfo )
	////	{
	////		D_WARNING( "BaseMapIssueNomorePlayerQuest, NULL == m_pCopyMapInfo\n" );
	////	}
	////	m_pCopyMapInfo->IssueDelSelfCopyQuest();
	////	return;
	////}

	/////踢本副本所有玩家（准备删副本自身）
	//void BaseMapKickAllCopyPlayers()
	//{
	//	if ( !IsInsCopyMap() )
	//	{
	//		D_WARNING( "普通地图试图执行BaseMapKickAllCopyPlayers\n" );
	//		return;
	//	}
	//	if ( NULL == m_pCopyMapInfo )
	//	{
	//		D_WARNING( "BaseMapKickAllCopyPlayers, NULL == m_pCopyMapInfo\n" );
	//	}
	//	m_pCopyMapInfo->IssueKickAllCopyPlayers();
	//	return;
	//}

	/////设置副本自身检测待删条件（待副本中玩家数降至0后即可执行发起删除自身操作）
	//void BaseMapDetectDelCond()
	//{
	//	if ( !IsInsCopyMap() )
	//	{
	//		D_WARNING( "普通地图试图执行BaseMapDetectDelCond\n" );
	//		return;
	//	}
	//	if ( NULL == m_pCopyMapInfo )
	//	{
	//		D_WARNING( "BaseMapDetectDelCond, NULL == m_pCopyMapInfo\n" );
	//	}
	//	m_pCopyMapInfo->IssueDetectDelCond();
	//	return;
	//}

	inline bool IsInsCopyMap()
	{
		return MUX_COPY_MAP == m_NormalOrCopy;
	}

public:
	///取玩家的MapInfoID;
	const AIMapInfo& GetMapInfoID()
	{
		return m_AIMapInfo;
	}

	inline bool GetMapCopyID( unsigned int& copyid ) 
	{
		copyid = 0;
		if ( NULL == m_pCopyMapInfo )
		{			
			return false;
		}
		copyid = m_pCopyMapInfo->GetCopyID();
		return true;
	}

public:
	///副本地图设置所有玩家的outmapinfo;
	bool SetAllPlayerOutPos( unsigned short mapid, unsigned short posX, unsigned short posY, unsigned short extent )
	{
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "SetAllPlayerOutPos, NULL == m_pCopyMapInfo\n" );
			return false;
		}

		return m_pCopyMapInfo->SetAllPlayerOutPos( mapid, posX, posY, extent );
	}

	////玩家进入副本时的检测(之前是否进过此副本，如果进过，是用何种卷轴进入的等等) 
	//inline bool PlayerEnterCheck( const PlayerID& playerID/*, const char* roleName*/, unsigned int& scrollType/*in_out scrollType*/, bool& isOrgEnterThis/*之前是否进过此副本*/ )
	//{
	//	if ( NULL == m_pCopyMapInfo )
	//	{
	//		D_ERROR( "CMuxMap::PlayerEnterCheck, NULL == m_pCopyMapInfo\n" );
	//		return false;
	//	}

	//	return m_pCopyMapInfo->PlayerEnterCheck( playerID/*, roleName*/, scrollType, isOrgEnterThis );
	//}

	////此工作已改至center,///副本内的玩家离开组队时的处理（应发起将其从副本中踢出去）;
	//bool OnPlayerLeaveSTeamProc( const PlayerID& playerID )
	//{
	//	if ( NULL == m_pCopyMapInfo )
	//	{
	//		D_ERROR( "CMuxMap::OnPlayerLeaveSTeamProc, NULL == m_pCopyMapInfo\n" );
	//		return false;
	//	}

	//	return m_pCopyMapInfo->OnPlayerLeaveSTeamProc( playerID );
	//}

private:
	AIMapInfo          m_AIMapInfo;
	MAPINS_TYPE        m_NormalOrCopy;//normal map or copy map;
	CMuxNormalMapInfo* m_pNormalMapInfo;//property for normal map, NULL if copy map;
	CMuxCopyInfo*      m_pCopyMapInfo;//property for copy map, NULL if normal map;

private:
	/// exec when map start, etc. when copy created
	bool OnMapStart();

private:
	inline CMapScript* GetMapScript()
	{
		if ( NULL == m_pMapProperty )
		{
			return NULL;
		}

		return m_pMapProperty->GetMapScript();
	}

public:
	/////向pConcentBlock块周围的块通知该块上的玩家信息;
	//void BlockConcentPlayerNoti( MuxMapBlock* pConcentBlock, unsigned int& allNotiNum/*向多少人通知了多少个人的信息(发消息的数量)*/ );

	/////使用组播向周边块上的玩家广播；
	//template< typename T_Msg >
	//void GroupSurNotify( T_Msg* pMsg, unsigned short blockX, unsigned short blockY ) 
	//{
	//	if ( NULL == m_pNormalMapInfo )
	//	{
	//		return;
	//	}

	//	m_pNormalMapInfo->GroupSurNotify<T_Msg>( pMsg, blockX, blockY );
	//};

//private:
//	CGroupNotiInfo m_GroupNotiInfo;//组通知玩家信息；

public:
	/// add rideteam to map;
	bool AddRideTeamWithLeader( RideTeam* pTeam, CPlayer* pPlayer );

	///delete rideteam from map;
	bool DelRideTeamWithLeader( RideTeam* pTeam, CPlayer* pPlayer );

	///notify hp add event;
	void NotifySurMonsterPlayerHpAdd( PlayerID addStarter, PlayerID addTarget, int hpAdd, unsigned short centerX, unsigned short centerY );

	/////temp for monster wrapper;
	//bool GetScopeMonsters( unsigned short centerX, unsigned short centerY, ScopeMonsterInfo* scopeMonstrArr, int inArrSize, int& arrSize );

	/////temp for monster wrapper;
	//bool GetScopePlayers( unsigned short centerX, unsigned short centerY, ScopePlayerInfo* scopePlayerArr, int inArrSize, int& arrSize );

#ifdef ITEM_NPC
#define MAX_MAP_ITEM_NPC 32

private:
	MapItemNpc* m_pArrMapItemNpc[MAX_MAP_ITEM_NPC];			        //所有的NPC
	MGItemNpcStatusInfo m_itemNpcChangedInfo;			//变化的物件NPC的信息

public:
	bool SetMapItemNpcMonsterId( unsigned int itemNpcSeq, unsigned int monsterId );
	
	bool SetMapItemNpcState( unsigned int itemNpcSeq, unsigned int curStatus );
	
	bool SetItemNpc( unsigned int itemSeq, MapItemNpc* pMapItemNpc );

	void ItemNpcSort();
	
	void FillChangedItemNpcInfo();

	bool IsNeedNotifyItemNpc();

	void NotifyAllChangedItemNpcStatus( CPlayer* pPlayer );

	bool GetCharaterIDByItemSeq( unsigned int itemSeq, PlayerID& characterID );

	bool IsItemNpcChange( unsigned int itemSeq, unsigned int& changedStatus );

	bool IsHaveItemNpc( unsigned int itemSeq );
#endif //ITEM_NPC

public:
	/////从.map文件中读地图基本信息；
	//bool ReadMapBaseInfo( const char* mapfileName );
    ///取地图ID号；
	inline unsigned long GetMapNo() 
	{ 
		if ( NULL == m_pMapProperty ) 
		{
			return 0;
		}

		return m_pMapProperty->GetPropertyMapNo();
	} //by dzj, 07.12.27;

	////获得此块区域的信息
	//int GetMapAreaInfo( int areaInfo );

	////加载任务区域脚本信息
	//bool LoadAreaTaskScript( unsigned int areaID );

	//bool ReloadMapScript();

//#ifdef STAGE_MAP_SCRIPT
//
//private:
//	///装入关卡地图；
//	void LoadMapScript();
//
////private:
////	CMapScript* m_pMapScript;//地图脚本；
//
//#endif //STAGE_MAP_SCRIPT

public:
	///副本地图时钟循环；
	bool MapCopyTimerProc( const ACE_Time_Value& curTime );
	///普通地图时钟循环；
	bool MapNormalTimerProc( const ACE_Time_Value& curTime );

private:
	///普通地图与副本地图共用时钟
	void MapCommonTimerProc( const ACE_Time_Value& curTime );

private:
	///将某类怪物添加到删除列表；
	void AddToDelMonsterType( unsigned int monsterClsID );
	///定时检查是否有某类怪物需要被删除；
	bool TimerCheckToDelTypeMonster();
public:
	bool DelTypeMonster( unsigned int npcTypeId );

private:
	vector<unsigned int> m_vecToDelMonsterType;//待删的某类怪物类型号;

private:
	ACE_Time_Value m_lastSupplyTime;//最后一次补充怪物的时间；

public:
	///将玩家从移动玩家列表中删去；
	bool DelPlayerFromMovingList( CPlayer* pPlayer );

private:
	///将玩家加至移动玩家列表；
	bool AddPlayerToMovingList( CPlayer* pPlayer );
	///定时更新移动玩家的位置；
	void MovingPlayerTimer();

private:
	int m_LastEPos;//上次遍历的结束位置，每次遍历当前地图玩家总数的1/4;

public:
	///玩家移动一次，返回值：玩家是否到达目标点(或是否要从移动列表中删去)；
	void PlayerMoveOnce( CPlayer* pPlayer, bool isForceCal/*是否强制计算，而不管是否跨格*/, bool& isAchieve, bool& isNeedNoti );

public:
	//调整玩家所在块
	void AdjustPlayerBlock( bool isManualSet/*是否手动调，手动调时必须周围全通知到而不是只通知新视野*/, CPlayer* pPlayer, unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY );

	///玩家试图移动;
	bool PlayerTryNewMove( CPlayer* pMovePlayer, float fCurX, float fCurY, float fTargetX, float fTargetY );

private:
	///向周围范围广播玩家的新移动消息；
	void PlayerNewMovePlayerBrocast( CPlayer* pMovePlayer, bool isPureNotify/*纯粹通知，不作任何标志位的修改*/  );
	///向周围怪物广播玩家的新移动消息；
	void PlayerNewMoveMonsterBrocast( CPlayer* pMovePlayer );

//private:
//	///累计块内玩家怪物数量；
//	void CalBlockPlayerMonsterNum( unsigned short blockX, unsigned blockY, unsigned int& toAddPlayerNum, unsigned int& toAddMonsterNum );

public:
	////取指定点视野内玩家与怪物数量；
	//bool GetPosSurPlayerMonsterNum( unsigned short posX, unsigned short posY, unsigned int& playerNum, unsigned int& monsterNum );

	///取当前地图的移动列表大小；
	int GetMovingPlayerNum();//取当前地图的移动列表大小；

    ///通知GM当前地图中的当前移动玩家；
    void NotifyGMMovingPlayerInfo( CPlayer* pGMPlayer );

	//该位置对应的属性
	bool GetPosProperty( unsigned short x, unsigned short y, unsigned int& areaID, bool& bPK, bool& bFight, bool& bPreArea );

	//void GetPosProperty( unsigned short x, unsigned short y, bool& bPK, bool& bFight, bool& bPreArea );

	//玩家移动到一个新的位置时
	void PlayerMoveToNewPos( CPlayer* pMovePlayer, unsigned int orgX, unsigned int orgY, unsigned int newX, unsigned int newY );

private:
	list<CPlayer*>   m_MovingPlayers;//移动玩家列表；

private:
	///怪物加入细节地图(normalmap/copymap)
	bool AddMonsterToExtMap( CMonster* pMonster, unsigned short gridX, unsigned short gridY );
	///怪物从细节地图(normalmap/copymap)删除;
	bool DeletePlayerFromExtMap( CMonster* pMonster );

public:
	///取地图内玩家总数；
	unsigned int GetMapPlayerNum();
	///添加一个玩家到地图上，同时加至块上
	bool AddPlayerToMap( CPlayer* pPlayer, unsigned short gridX, unsigned short gridY );
	///添加一个怪物到地图上，同时加至块上
	bool AddMonsterToMap( CMonster* pMonster, unsigned short gridX, unsigned short gridY );
	///从地图上删去玩家，同时从块中删去
	bool DeletePlayerFromMap( CPlayer* pPlayer, bool isSwitchMap );
	///从地图上删去怪物，同时从块中删去，之前的OnDisappear已调用，此处只删怪物；
	bool DeleteMonsterFromMap( CMonster* pMonster );
	///检查玩家是否在地图内(玩家指针不知是否仍有效)；
	bool IsDoubtPlayerPtrInMap( CPlayer* pPlayer );
	///检查玩家是否在地图内(调用前已确知该玩家指针有效)；
	inline bool IsValidPlayerPtrInMap( CPlayer* pPlayer )
	{
		if ( NULL == pPlayer )
		{
			return false;
		}
		return ( pPlayer->GetMapID() == GetMapNo() );
	}
	///检查怪物是否在地图内(调用前不知道该怪物指针是否仍然有效)；
	bool IsDoubtMonsterPtrInMap( CMonster* pMonster );
	///检查怪物是否在地图内(调用前已确知该怪物指针仍然有效)；
	inline bool IsValidMonsterPtrInMap( CMonster* pMonster )
	{
		if ( NULL == pMonster )
		{
			return false;
		}
		return ( pMonster->GetMapIDOfMonster() == GetMapNo() );
	}
#ifdef USE_SELF_HASH
	///取地图内的怪物指针；
    CMonster* GetMonsterPtrInMap( unsigned long monsterID );
#endif //USE_SELF_HASH

public:
	///检测是否为伪副本且已关闭，若已关闭，应发起踢此刚进入者；
	bool CheckIfPCopyClose( CPlayer* pPlayer );
	///脚本开启伪副本；
	bool OnPCopyOpen();
	///脚本关闭伪副本，默认先传地图内所有玩家出副本；
	bool OnPCopyClose();

	///设置本地图为TD地图;
	inline bool SetTDFlag()
	{
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::SetTDFlag, NULL == m_pCopyMapInfo, map:%d!", GetMapNo() );
			return false;
		}

		return m_pCopyMapInfo->SetTDFlag();		
	}

private:
	bool m_isPCopyAndClose;//是否为伪副本&&已关闭，两项为真时才为真；

public:
	//unsigned short GetGridHeight() 
	//{ 
	//	return m_gridHeight; 
	//}
	//unsigned short GetGridWidth() 
	//{ 
	//	return m_gridWidth; 
	//}
	///块坐标是否有效；
	bool IsBlockValid( unsigned short blockX, unsigned short blockY ) 
	{
		if ( NULL == m_pMapProperty )
		{
			return false;
		}

		return (blockX<m_pMapProperty->GetBlockWidth()) && (blockY<m_pMapProperty->GetBlockHeight());
	}
	///格坐标是否有效；
	bool IsGridValid( unsigned short gridX, unsigned short gridY ) 
	{
		if ( NULL == m_pMapProperty )
		{
			return false;
		}

		return (gridX<m_pMapProperty->GetGridWidth()) && (gridY<m_pMapProperty->GetGridHeight());
	}

	/////取指定块周边块的坐标范围；
	//bool GetBlockNearbyScope( unsigned short blockX, unsigned short blockY, unsigned short& minBlockX, unsigned short& minBlockY, unsigned short& maxBlockX, unsigned short& maxBlockY );

	/////增加对应八方向的新块计算函数，输入：旧块X，旧块Y，新块X， 新块Y，输出：新视野块X，新视野块Y（最多5组，不允许跳块，即，输入必须为邻块）,???,贺轶男检查;
	//bool GetNewViewportBlocks( unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY
	//	, bool& isJump/*是否跳块，输入非相邻块*/, unsigned short& viewportBlockNum/*新视野块数目*/, unsigned short outBlockX[5], unsigned short outBlockY[5] );

	inline bool IsGridCanPass( unsigned short gridX, unsigned short gridY )
	{
		if ( NULL == m_pMapProperty )
		{
			return false;
		}

		return m_pMapProperty->IsGridCanPass( gridX, gridY );
	}

	inline MuxMapBlock* GetMapBlockByBlock( unsigned short blockX, unsigned short blockY )
	{
		if ( IsInsCopyMap() )
		{
			//副本；
			if ( NULL == m_pCopyMapInfo )
			{
				D_ERROR( "CMuxMap::GetMapBlockByBlock, NULL == m_pCopyMapInfo!" );
				return NULL;
			}
			return (m_pCopyMapInfo->GetMapBlockByBlock(blockX, blockY));
		} else {
			//普通地图
			if ( NULL == m_pNormalMapInfo )
			{
				D_ERROR( "CMuxMap::GetMapBlockByBlock, NULL == m_pNormalMapInfo!" );
				return NULL;
			}
			return (m_pNormalMapInfo->GetMapBlockByBlock(blockX, blockY));
		}

		return NULL;
	}

public:
	///向周边块上的玩家广播；
	template< typename T_Msg >
	void NotifySurroundingBlock( T_Msg* pMsg, unsigned short blockX, unsigned short blockY ) 
	{
		D_ERROR( "call disable interface : NotifySurroundingBlock\n" );
		//TRY_BEGIN;

		//unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

		//if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
		//{
		//	//输入点为无效点；
		//	return;
		//}

		//MuxMapBlock* pBlock = NULL;
		//for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
		//{
		//	for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		//	{
		//		pBlock = GetMapBlockByBlock( x,y );
		//		if ( NULL != pBlock )
		//		{
		//			pBlock->NotifyBlockPlayers<T_Msg>( pMsg );//组播放至NotifyBlockPlayers内部;
		//		}
		//	}
		//}

		//return;
		//TRY_END;
		//return;
	};

	//template< typename T_Msg >
	//void NotifySurroundingPlayersInScope( T_Msg* pMsg, unsigned short gridX, unsigned short gridY, unsigned int specDistance ) //通知距指定点距离小于定值的玩家；
	//{
	//	TRY_BEGIN;

	//	unsigned short blockX=0, blockY=0;
	//	MuxMapSpace::GridToBlockPos( gridX, gridY, blockX, blockY );

	//	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	//	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	//	{
	//		//输入点为无效点；
	//		return;
	//	}

	//	MuxMapBlock* pBlock = NULL;
	//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	//	{
	//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
	//		{
	//			pBlock = GetMapBlockByBlock( x,y );
	//			if ( NULL != pBlock )
	//			{
	//				pBlock->NotifyBlockPlayersInScope<T_Msg>( pMsg, gridX, gridY, specDistance );
	//			}
	//		}
	//	}

	//	return;
	//	TRY_END;
	//	return;
	//}

	//template<typename T_Msg>
	//void NotifySurroundingPlayersExceptThisPlayer( T_Msg* pMsg, unsigned short gridX, unsigned short gridY, CPlayer* exceptPlayer  )
	//{
	//	TRY_BEGIN;

	//	if ( !exceptPlayer )
	//		return;

	//	unsigned short blockX=0, blockY=0;
	//	MuxMapSpace::GridToBlockPos( gridX, gridY, blockX, blockY );

	//	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	//	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	//	{
	//		//输入点为无效点；
	//		return;
	//	}

	//	MuxMapBlock* pBlock = NULL;
	//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	//	{
	//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
	//		{
	//			pBlock = GetMapBlockByBlock( x,y );
	//			if ( NULL != pBlock )
	//			{
	//				pBlock->NotifyBlockPlayerExceptThisPlayer<T_Msg>( pMsg, exceptPlayer );
	//			}
	//		}
	//	}
	//	return;

	//	TRY_END;
	//	return;
	//}

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//以下为原CMuxMap中的成员属性，基本照搬过来..
public:

    ///取一个可用点，用于机器人随机出生与复活；
	bool GetOneAvailPt( MuxPoint& availPt );

	///添加一个本地图的复活点;
	bool AddRebirthPt( MapRebirthPt* pRebirthPt );
	//初始化地图的复活点，目前只处理第一个复活区域；
	bool InitRebirthPt();
	///取一个可用复活点;
	bool GetOneRebirthPt( unsigned long& mapID, MuxPoint& rebirthPt, unsigned char rebirthRace );

	bool GetDefaultRebirthPt(unsigned long& mapID, MuxPoint& rebirthPt);

private:
	///玩家出现相关通知；
	void PlayerAppearNotify( CPlayer* pAppearPlayer, bool isNotiSelf/*是否通知出现者本人*/ );
	///玩家离开当前位置相关通知；
	void PlayerLeaveNotify( CPlayer* pLeavePlayer );

	///玩家跨块消息发来时的相关通知；
	void PlayerBeyondBlockNotify( CPlayer* pBeyondPlayer );

	//通知周围格特殊怪物 离开或者进入视野
	void MonsterLeaveViewNotify( CMonster* pMonster );

	/////玩家跨块相关通知；
	//void PlayerBeyondBlockNotifyByBlockPos( CPlayer* pBeyondPlayer, unsigned short blockX, unsigned short blockY );

	/////玩家出现块相关通知；
	//void PlayerAppearNotifyByBlockPos( CPlayer* pAppearPlayer, bool isNotiSelf/*是否通知出现者本人*/, unsigned short blockX, unsigned short blockY );
	/////玩家离开块相关通知；
	//void PlayerLeaveNotifyByBlockPos( CPlayer* pLeavePlayer, unsigned short blockX, unsigned short blockY );
	
//地图的逻辑事件处理
public:
	/////玩家移动状态改变通知周边怪物(新称动，打人，打怪)，应AI要求添加；
	//void PlayerMovChgMonsterNotify( CPlayer* pMovChgPlayer );
	///玩家死亡通知；
	void PlayerDieNotify( CPlayer* pDiePlayer );

	///玩家同地图内跳转；
	bool OnPlayerInnerJump( CPlayer* pMovPlayer, TYPE_POS targetGridX, TYPE_POS targetGridY );

	///玩家出现在地图上;
	bool OnPlayerAppear( CPlayer* pAppearPlayer );

	///玩家离开地图
	bool OnPlayerLeave( CPlayer* pLeavePlayer, bool isSwitchMapLeave = false );

	//玩家移动处理;
	bool OnPlayerNewMove(CPlayer* pPlayer,const GMRun* pRunBuf );

	//玩家发来跨块消息
	bool OnPlayerBeyondBlock( CPlayer* pPlayer, const GMPlayerBeyondBlock* beyondMsg );

	/////怪物移动处理，由AI调用；
	//bool OnMonsterMove(CMonster* pMoveMonster,int newGridX, int newGridY, int tgtGridX, int tgtGridY );

	//怪物开始移动
	bool OnMonsterStartMove( CMonster* pMoveMonster, TYPE_POS gridCurX, TYPE_POS gridCurY, TYPE_POS gridTargetX, TYPE_POS gridTargetY, float fSpeed );

	//怪物移动中跨格(以MonsterStartMove为前提)
	bool OnMonsterBeyondGrid( CMonster* pMoveMonster, TYPE_POS gridOrgX, TYPE_POS gridOrgY, TYPE_POS gridNewX, TYPE_POS gridNewY
		, bool isForceAndNotNotiMove/*是否强制设位置且不通知周围此移动，怪物移动状态下改变位置为false，非移动状态下改变位置为true(目前只有被击退一种情形)*/ );

	//怪物停止移动(以MonsterStartMove为前提)
	bool OnMonsterStopMove( CMonster* pStopMonster );

	///真正从地图上，地图块内删去怪物，由地图时钟调用，之前怪物的OnDisappear已经调用通知周围该怪物已删除；
	bool FinalDelMonster( CMonster* pMonsterToDel );

	///怪物在地图上消失
	bool OnMonsterDisappear( CMonster* pDisappearMonster, GCMonsterDisappear::E_DIS_TYPE disType );

	/////怪物出生,调用时怪物已加入地图；
	//bool MonsterBornNotify( CMonster* pBornMonster, unsigned short bornGridX, unsigned short bornGridY );
	
	//对玩家使用辅助技能
	bool OnPlayerUseAssistSkillToPlayer(CPlayer* pUseSkillPlayer, const PlayerID& targetID, TYPE_ID skillID, bool isUnionSkill );
	//对怪物使用辅助技能
	bool OnPlayerUseAssistSkillToMonster(CPlayer* pUseSkillPlayer, TYPE_ID monsterID, TYPE_ID skillID, bool isUnionSkill );
	//获取范围内的玩家和怪物
	bool GetScopeCreature( TYPE_POS posX, TYPE_POS posY, int range, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster );
	
	///处理需要玩家信息的请求
	bool OnReqRoleInfo(CPlayer* pPlayer, CPlayer* pNoticePlayer);
	///处理需要怪物信息的请求
	bool OnReqMonsterInfo(CPlayer* pNoticePlayer, const GMReqMonsterInfo* pReqMonsterInfo);
	///处理玩家聊天
	bool HandlePlayerChat( CPlayer* pPlayer, const GMChat* pChat);
	//玩家升级协议
	bool HandlePlayerLevelUp(CPlayer* pPlayer, int levelUpType);
	//判断玩家是否在怪物的视野范围内
	bool IsPlayerInMonsterSight(CMonster* pMonser, CPlayer* pPlayer, int nDist);
	/////处理怪物停止移动
	//bool HandleMonsterStop( CMonster* pMonster);

	////@brief:处理地图上道具包的出现
	//bool HandleItemsPkgAppear( CItemsPackage* pitemPkg );
	bool HandleItemPkgFirstAppear( CItemsPackage* pitemPkg );

	//@brief:处理地图上道具包的消失
	bool HandleItemsPkgDisAppear( CItemsPackage* pItemPkg );
	//@brief:处理玩家查询道具包信息
	bool HandleQueryItemsPkg( CPlayer* pPlayer, unsigned long pkgIndex, unsigned int queryType );
	//@brief:将该道具包的信息,发送给周围的玩家
	void NotifyItemPkgsAppearEx( CItemsPackage* pItemPkg );

	void NotifyItemPkgFirstAppearEx( CItemsPackage* pItemPkg );

//有效性检测
public:
	/////取有效范围1;
	//void GetValidScopeRectGrid( TYPE_POS gridLeft, TYPE_POS gridTop, TYPE_POS gridRight, TYPE_POS gridBottom, TYPE_POS& gridMinX
	//	, TYPE_POS& gridMaxX, TYPE_POS& gridMinY, TYPE_POS& gridMaxY);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	////获取指定格子周围范围的格子
	//void GetValidBlock( int nX, int nY, int nScope, int& nMinX, int& nMaxX, int& nMinY, int& nMaxY );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	template<typename T_Msg>
	void NotifySurroundingExceptPlayer( T_Msg* pMsg, int nPosX, int nPosY,const PlayerID& exceptPlayerID  )
	{
		if ( NULL == pMsg )
		{
			D_ERROR( "CMuxMap::NotifySurroundingExceptPlayer，输入参数空\n" );
			return;
		}

		if ( IsInsCopyMap() )
		{
			if ( NULL == m_pCopyMapInfo )
			{
				D_ERROR( "CMuxMap::NotifySurroundingExceptPlayer, NULL == m_pCopyMapInfo\n" );
				return;
			}
			m_pCopyMapInfo->NotifySurroundingExceptPlayer( pMsg, nPosX, nPosY , exceptPlayerID );
		} else {
			//普通地图；
			if ( NULL == m_pNormalMapInfo )
			{
				D_ERROR( "CMuxMap::NotifySurroundingExceptPlayer, NULL == m_pNormalMapInfo\n" );
				return;
			}

			m_pNormalMapInfo->NotifySurroundingExceptPlayer( pMsg, nPosX, nPosY , exceptPlayerID );
		}
		return;
	}

	//向指定块上玩家广播
    template< typename T_Msg >
	void NotifyBlocks( T_Msg* pMsg, vector<MuxMapBlock*>* vecBlocks )
	{
		if ( NULL == pMsg )
		{
			D_ERROR( "CMuxMap::NotifyBlocks，输入参数空\n" );
			return;
		}

		if ( IsInsCopyMap() )
		{
			if ( NULL == m_pCopyMapInfo )
			{
				D_ERROR( "CMuxMap::NotifyBlocks, NULL == m_pCopyMapInfo\n" );
				return;
			}
			m_pCopyMapInfo->NotifyBlocks( pMsg );
		} else {
			//普通地图；
			if ( NULL == m_pNormalMapInfo )
			{
				D_ERROR( "CMuxMap::NotifyBlocks, NULL == m_pNormalMapInfo\n" );
				return;
			}
			if (NULL == vecBlocks)
			{
				D_ERROR( "CMuxMap::NotifyBlocks, NULL == vecBlocks\n" );
				return;
			}

			m_pNormalMapInfo->NotifyBlocks( pMsg, *vecBlocks );
		}
		return;
	}//void NotifyBlocks( T_Msg* pMsg, vector<MuxMapBlock*>* vecBlocks )

	///向周边玩家广播；
	template< typename T_Msg >
	void NotifySurrounding( T_Msg* pMsg, int nPosX, int nPosY ) 
	{
		if ( NULL == pMsg )
		{
			D_ERROR( "CMuxMap::NotifySurrounding，输入参数空\n" );
			return;
		}

		if ( IsInsCopyMap() )
		{
			if ( NULL == m_pCopyMapInfo )
			{
				D_ERROR( "CMuxMap::NotifySurrounding, NULL == m_pCopyMapInfo\n" );
				return;
			}
			m_pCopyMapInfo->NotifySurrounding( pMsg, nPosX, nPosY );
		} else {
			//普通地图；
			if ( NULL == m_pNormalMapInfo )
			{
				D_ERROR( "CMuxMap::NotifySurrounding, NULL == m_pNormalMapInfo\n" );
				return;
			}

			m_pNormalMapInfo->NotifySurrounding( pMsg, nPosX, nPosY );
		}
		return;
	};

	template< typename T_Msg >
	void NotifyAllUseBrocast( T_Msg* pMsg )
	{
		if ( NULL == pMsg )
		{
			D_ERROR( "CMuxMap::NotifyAllUseBrocast，输入参数空\n" );
			return;
		}

		if ( IsInsCopyMap() )
		{
			//副本；
			if ( NULL == m_pCopyMapInfo )
			{
				D_ERROR( "CMuxMap::NotifyAllUseBrocast, NULL == m_pCopyMapInfo\n" );
				return;
			}
			return m_pCopyMapInfo->NotifyAllUseBrocast<T_Msg>( pMsg );
		} else {
			//普通地图；
			if ( NULL == m_pNormalMapInfo )
			{
				D_ERROR( "CMuxMap::NotifyAllUseBrocast, NULL == m_pNormalMapInfo\n" );
				return;
			}
			return m_pNormalMapInfo->NotifyAllUseBrocast<T_Msg>( pMsg );
		}
	}

	///读入所有属于本地图的怪物生成器;
	void FillMapMonsterCreator();
	///添加一个本map的怪物生成器；
	bool PushMapCreator( CCreatorIns* pCreatorIns );
	///弹出一个本map的怪物生成器，如果本地图已无生成器，则返回NULL；
	CCreatorIns* PopMapCreator();
	bool SetNpcCreatorActiveFlag( unsigned int creatorID, bool isActive );

	///第一次刷怪
	void FirstRefreshMonster();
	///尝试补充怪
	void MapTrySupplyMonster();
	/////获取地图上指定范围内的可用刷怪点(矩形)
	//bool GetMultiRefreshMonsterPtRect( TYPE_POS gridLeft, TYPE_POS gridTop, TYPE_POS gridRight, TYPE_POS gridBottom, vector<MuxPoint>& vecOutPts );
	///检查地图上某点是否可以添加新怪物，本函数要与AddMonsterToMap统一；
	bool IsEnableAddMonster( unsigned short gridX, unsigned short gridY ) { return IsGridCanPass( gridX, gridY ); };
	//检查地图上某点是否可以添加塔
	bool IsEnabelAddTower(TYPE_POS gridX, TYPE_POS gridY);
	//随机获取指定范围内的一个可用点
	bool GetRandPointByRanage( TYPE_POS gridLeft, TYPE_POS gridTop, TYPE_POS gridRight, TYPE_POS gridBottom, TYPE_POS &randPosX, TYPE_POS &randPosY);	

	bool ControlCheckCvp( TYPE_ID buffID, float linkRate, CMonster* pAtkMonster, CPlayer* pTargetPlayer, float& realRate);

	
	////获取新型ＭＡＰ里攻击范围内的怪物
	//void _GetAttackRangeMonsters( TYPE_POS attackPosX, TYPE_POS attackPosY, int attackRange, vector<CMonster*>& vecMonsters);

	//检测单次玩家攻击怪物是否成功
	EBattleResult CheckAoe( CPlayer* pAttackPlayer, TYPE_ID skillID, CPlayerSkill*& pSkill, TYPE_POS x2, TYPE_POS y2 );
	
public:
	///添加一个无生成器NPC生成请求至队列；
	bool AddOneNoCreatorNpc( unsigned long callerID, unsigned long npcType, TYPE_POS posX, TYPE_POS posY, const PlayerID& rewardInfo, unsigned int aiFlag );

	///添加一个待补充的怪物；
	bool AddToSupplyCreator( CCreatorIns* pCreatorIns );

private:
	/////本地图的可用点；
	//不再使用此成员，机器人出生复活点随便选，因为他们的移动本来也不考虑障碍，by dzj, 09.10.12,vector<MuxPoint*> m_vecAvailPt;

	///////本地图的复活点信息；
	//vector< MapRebirthPt* >    m_vecNewMapRebirthPts;//本地图的所有复活点信息；

	/////本地图的可用复活点；
	//vector<MuxPoint*> m_vecAvailRebirthPt;					//普通复活点
	//vector<MuxPoint*> m_vecAvailCityAttackRebirthPt;		//攻城战进攻者所用复活点
	//vector<MuxPoint*> m_vecAvailCityDefendRebirthPt;		//攻城战防守者所用复活点

	///本地图的怪物生成器；
	vector< CCreatorIns* > m_vecMonsterCreators;
	vector< NoCreatorNpcSupplyInfo > m_vecNoCreatorNpcSupplyQueue;//无生成器的NPC补充队列，之所以使用一个队列而不是即时加入地图，主要是为了防止在遍历各怪物相关迭代器的过程中补充怪物，那样做可能导致迭代器出错。
	vector< CCreatorIns* > m_vecCreatorNpcSuppplyQueue;//有生成器的NPC补充队列;

	//...以上为原CMuxMap中的成员属性，基本照搬过来
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef STAGE_MAP_SCRIPT
public:
	//取地图实例保存的脚本用数据
    inline bool MapGetMapScriptTarget( int& getVal )
	{
		getVal = m_MapScriptTarget;
		return true;
	}
	//保存地图实例脚本用数据；
    inline bool MapSetMapScriptTarget( int tosetVal )
	{
		m_MapScriptTarget = tosetVal;
		return true;
	}

private:
	int m_MapScriptTarget;//脚本用数据，用于在脚本各次调用之间保存脚本需要的数据，希望这一个就够了，若以后策划要增加，则改为使用另外的结构，以map<string,int>来保存；

public:
	///计数器添加
	bool MapAddCounterOfMap( unsigned int counterClsId/*编号，用于脚本标识*/, unsigned int counterType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount );

	///地图计时器添加
	bool MapInsertMapTimer( unsigned int timerClsID/*类型号，用于脚本标识*/, unsigned int resTime/*到期时刻，单位:分钟*/ );
	///地图计时器删除 
	bool MapDeleteMapTimer( unsigned int timerClsID );
	///地图计时器查询
	bool MapIsHaveMapTimer( unsigned int timerClsID );

	bool MapInsertDetailTimer( unsigned int timerClsId, unsigned int hour, unsigned int minute );		//增加固定时间的定时器
	bool MapDeleteDetailTimer( unsigned int timerClsId );

	CWarTimerManager& GetWarTimerManager()
	{
		return m_warTimerManager;
	}

	CWarCounterManager& GetWarCounterManager()
	{
		return m_warCounterManager;
	}

	///向地图内的所有玩家广播chat msg;
	bool MapBrocastChatMsg( const char* notimsg );
	
	//设置关卡地图信息指针
	inline bool SetCityInfo( CityInfo* pCityInfo )
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::SetCityInfo，NULL == m_pNormalMapInfo\n" );
			return false;
		}

		if(NULL ==  pCityInfo)
		{
			D_ERROR( "CMuxMap::SetCityInfo，NULL == pCityInfo\n" );
			return false;
		}

		m_pNormalMapInfo->SetCityInfo( pCityInfo );
		return true;
	}

	//增加关卡地图经验值
	void AddCityExp( unsigned int addExp )
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::AddCityExp，NULL == m_pNormalMapInfo\n" );
			return;
		}

		m_pNormalMapInfo->AddCityExp( addExp );

		UpdateCityInfo();

		return;

	}

	//更新关卡地图信息
	bool UpdateCityInfo()
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::UpdateCityInfo，NULL == m_pNormalMapInfo\n" );
			return false;
		}

		CityInfo* pCityInfo = m_pNormalMapInfo->GetCityInfo();
		if ( NULL == pCityInfo )
		{
			return false;
		}

		MGUpdateCityInfo updateMsg;
		updateMsg.updateCityInfo = *pCityInfo;

		CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
		if( NULL == pGateSrv )
		{
			return false;
		}
		MsgToPut* pNewMsg = CreateSrvPkg( MGUpdateCityInfo, pGateSrv, updateMsg );

		pGateSrv->SendPkgToGateServer( pNewMsg );

		return true;
	}

	//获取种族信息
	unsigned char GetCityRace()
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::GetCityRace，NULL == m_pNormalMapInfo\n" );
			return 0;
		}

		return m_pNormalMapInfo->GetCityRace();
	}

	//是否拥有关卡地图信息
	bool IsHaveCityInfo()
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::IsHaveCityInfo，NULL == m_pNormalMapInfo\n" );
			return false;
		}

		return m_pNormalMapInfo->IsHaveCityInfo();
	}

	//获取关卡地图信息
	CityInfo* GetCityInfo()
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::GetCityInfo，NULL == m_pNormalMapInfo\n" );
			return NULL;
		}

		return m_pNormalMapInfo->GetCityInfo();	    
	}

	//清除地图关卡信息
	void ClearCityInfo()
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::ClearCityInfo，NULL == m_pNormalMapInfo\n" );
			return;
		}

		return m_pNormalMapInfo->ClearCityInfo();	    
	};

#ifdef HAS_PATH_VERIFIER
	//设置移动校验器
	void SetPathVerifier(void);
	//初始化阻挡碰撞信息
	void SetBlockQuardInfo(void);
	//获取已激活的碰撞信息
	void GetEnableBlockQuads(std::vector<int> &blockQuads);
	//设置单个阻挡碰撞
	void SetBlockQuard(int regionId, bool val);
	//直线通行验证？
	bool IsPassOK( float sPosX, float sPosY, float tPosX, float tPosY );
	//阻挡碰撞验证？
	bool BlockTestOK( float sPosX, float sPosY, float tPosX, float tPosY );
	//碰撞区设置有效
	void OnBlockQuadEnable(int regionId);
#endif

private:
	CTimerOfMap*      m_pTimerOfMap;//地图计时器；
	CCounterOfMap*    m_pCounterOfMap;//地图计数器；
	CDetailTimerOfMap* m_pDetailTimerOfMap;	//固定时间的定时器
	CWarTimerManager m_warTimerManager;		//warTimer
	CWarCounterManager m_warCounterManager;	//warCounter

	//CityInfo*         m_pCityInfo;			//关卡地图信息

#ifdef HAS_PATH_VERIFIER
	CPathVerifier::CBlockQuad *m_pBlockQuards;//地图碰撞信息
	int				m_blockQuadNum;//碰撞区域个数（最大15个）
	CAccessRegionPathVerifier *m_pPathVerifier;//通行验证
#endif

public://事件脚本处理；

	///玩家进入地图，回调脚本事件处理函数；
	bool OnMapPlayerEnterScriptHandle( CPlayer* pEnterPlayer );

	///玩家离开地图，回调脚本事件处理函数；
	bool OnMapPlayerLeaveScriptHandle( CPlayer* pLeavePlayer );

	///地图计时器到，回调脚本事件处理函数；
	bool OnMapTimerScriptHandle( unsigned int mapTimerClsID /*到期的时钟号，传递给脚本以便作相应处理*/ );

	///地图计数器到，回调脚本事件处理函数；
	bool OnMapCounterScriptHandle( unsigned int mapCounterClsID /*计数满的计数器号，传递给脚本以便作相应处理*/ );

	bool OnDetailTimerScriptHandle( unsigned int timerId );	 /*到期的定时器号*/
	bool OnNpcDeadScriptHandle( CPlayer* pKiller, unsigned int npcTypeId );	/*npc死亡*/
	bool OnPlayerDieScriptHandle( CPlayer* pDiePlayer );
	bool OnHandleScriptLeaveArea( unsigned int areaID, CPlayer* pMovePlayer );
	bool OnHandleScriptNotifyLevelMapInfo( unsigned int count );

	bool OnHandleScriptEnterArea( unsigned int areaID, CPlayer* pMovePlayer );
	bool OnHandleScriptNpcFirstBeAttacked( unsigned int monsterID, CPlayer* pAttacker );
	//给地图上所有玩家上buff
	bool CreateMapBuff( unsigned int race, unsigned int buffID, unsigned int buffTime );


#endif //STAGE_MAP_SCRIPT

public:
	bool BattleModeCheckPvc( CPlayer* pAttackPlayer, CMonster* pMonster, bool bGoodSkill );
	bool BattleModeCheckPvp( CPlayer* pAttackPlayer, CPlayer* pTargetPlayer, bool bGoodSkill );
	bool RookieProtect( CPlayer* pAttackPlayer,  CPlayer* pTargetPlayer, bool bGoodSkill, bool bInScopeAttack = false);

	//通过技能属性获取AOE技能的对象
	bool OnNpcAttackNpc( CMonster* pAtkMonster, CMonster* pTargetMonster, CNpcSkill* pNpcSkill, EScopeFlag scopeFlag, const PlayerID& orgTargetID );
	bool OnNpcAttackPlayer( CMonster* pAtkMonster, CPlayer* pTargetPlayer, CNpcSkill* pNpcSkill, EScopeFlag scopeFlag, const PlayerID& orgTargetID );

	//玩家使用技能
	bool OnPlayerUseSkill( CPlayer* pUseSkillPlayer, const GMUseSkill* pUseSkillInfo);
	//玩家使用技能，6.30版本矩形攻击及相应范围攻击修改
	bool OnPlayerUseSkill2( CPlayer* pUseSkillPlayer, const GMUseSkill2* pUseSkillInfo);
	///技能攻击目标检测;
	bool SkillAttackTargetCheck( CPlayer* pAttackPlayer, const PlayerID& targetID
		, unsigned int& checkRet, CPlayer*& pTgtPlayer, CMonster*& pTgtMonster, TYPE_POS& targetPosX, TYPE_POS& targetPosY, bool& isPvc );
	//战斗判断，消耗脚本以及分割技能攻击者位置重设
	bool PlayerUseSkillCommonCheck( CPlayer* pUseSkillPlayer, const GMUseSkill2* pUseSkillInfo
		, EBattleResult& ret/*技能使用结果*/, unsigned int& logSkillID/*用于记日志（在使用技能成功时才记日志，但是在判断的过程中技能编号有可能会变成新技能编号，因此保存旧的技能编号）*/
		, bool& isSplitSkill/*是否分割技能*/, CMuxPlayerSkill* pSkill, bool& isAoe/*是否aoe*/, bool& isTeamAoe/*是否组队aoe*/, bool& isNormalAoe/*是否普通aoe*/);
	//空放技能成功处理
	bool SucEmptyUseProc( CPlayer* pUseSkillPlayer, const GMUseSkill2* pUseSkillInfo, CMuxPlayerSkill* pSkill, bool isSplitSkill, EBattleResult ret );

	//怪物使用技能
	bool OnMonsterUseSkill( CMonster* pUseSkillMonster, unsigned int skillID, const PlayerID& targetID );

	EBattleResult CheckNewBattlePvc( CPlayer* pUseSkillPlayer, const PlayerID& targetID, TYPE_ID skillID, CMuxPlayerSkill*& pSkill,CMonster*& pTargetMonster );
	EBattleResult CheckNewBattlePvp( CPlayer* pUseSkillPlayer, const PlayerID& targetID, TYPE_ID skillID, CMuxPlayerSkill*& pSkill,CPlayer*& pTargetPlayer );
	//改用脚本后已废弃? void AfterCheckProcess( CPlayer* pUseSkill, CMuxPlayerSkill* pSkill, bool isPvc, unsigned int targetLevel, GMUseSkill* pUseSkillInfo );
    bool SinglePvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer, CMuxPlayerSkill* pSkill, bool isOrgTgt, AttackInfoPerTgt& outAtkInfoPerTgt
		, AreaOfInterest& attackAoe, SubHpInfoArr& subHpInfoArr );
	bool SinglePvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster, CMuxPlayerSkill* pSkill, bool isOrgTgt, AttackInfoPerTgt& outAtkInfoPerTgt
		, bool isKickBack, TYPE_POS kickPosX, TYPE_POS kickPosY
		, AreaOfInterest& attackAoe, SubHpInfoArr& subHpInfoArr );
	//bool SinglePvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer, CMuxPlayerSkill* pSkill, const PlayerID& orgTargetID,  EScopeFlag scopeFlag, const GMUseSkill* pUseSkill );
	//bool SinglePvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster, CMuxPlayerSkill* pSkill, const PlayerID& orgTargetID,  EScopeFlag scopeFlag, const GMUseSkill* pUseSkill );
	bool SingleCvcWrapper( CMonster* pUseSkillMonster, CMonster* pTargetMonster, CMuxSkillProp* pSkillProp, const PlayerID& orgTargetID, EScopeFlag scopeFlag );
	bool SingleCvpWrapper( CMonster* pUseSkillMonster, CPlayer* pTargetPlayer, CMuxSkillProp* pSkillProp, const PlayerID& orgTargetID, EScopeFlag scopeFlag );
	bool OnPlayerSkillGetAoeTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster ); 
	bool OnMonsterSkillGetAoeTarget( CMonster* pUSeSkillMonster, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster );
	bool OnPlayerSkillGetTeamTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer );
	bool SingleItemPvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster, CMuxSkillProp* pSkillProp, const PlayerID& orgTargetID, EScopeFlag scopeFlag );
	bool SingleItemPvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer, CMuxSkillProp* pSkillProp, const PlayerID& orgTargetID, EScopeFlag scopeFlag );

private:
	//通知相应块攻击结果
	bool NotifyResBlockAttInfo(bool isSplitSkill, bool isLastMsg, AreaOfInterest& attackAoe, MGSplitAttack& splitAttack, MGNormalAttack& normalAttack, BYTE& validTgtNum);

private:
	SubHpInfoArr m_subHpArr;

public:
	void OnActivityStart( unsigned int actID );
	void OnActivityEnd( unsigned int actID );
	unsigned int GetCounterCounter();		//获得地图计数器的数字
	
 };//CMuxMap;

#endif //MUX_MAPBASE_H
