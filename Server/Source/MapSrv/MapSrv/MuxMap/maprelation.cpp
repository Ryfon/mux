﻿#include "maprelation.h"
#include "string.h"
#include "../../../Base/Utility.h"
#include "../../../Base/XmlManager.h"


std::vector<CMapRelation::strMapRelationData> CMapRelation::m_mapRels;

bool CMapRelation::InitMapRelation(void)
{
	TRY_BEGIN;

	const char *pszFileName = "config/mapconfig/maprelation.xml";
	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR("地图关系文件%s加载出错\n", pszFileName);
		return false;
	}

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("地图关系文件%s可能为空\n", pszFileName);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);
	const char *pTemp = NULL;
	CElement *pElement = NULL;

	for(std::vector<CElement *>::iterator i = childElements.begin(); i != childElements.end(); i++)
	{	
		pElement = *i;
		pTemp = pElement->Name();
		if( (pTemp != NULL) && (strncmp(pTemp, "Item", 4) == 0) )
		{
			strMapRelationData mr;
			const char * pOuter = pElement->GetAttr("Outer");
			const char * pInner = pElement->GetAttr("Inner");
			mr.outerMapID = (NULL == pOuter) ? 0 : ACE_OS::atoi(pOuter);
			mr.innerMapID = (NULL == pInner) ? 0 : ACE_OS::atoi(pInner);
			if((0 != mr.outerMapID) && (0 != mr.innerMapID))
			{
				if(mr.outerMapID == mr.innerMapID)
				{
					D_ERROR("配置文件%s内容出错", pszFileName);
					return false;
				}
					
				m_mapRels.push_back(mr);
			}
		}
	}

	return true;
	TRY_END;
	return false;
}

bool CMapRelation::GetMapRelation(unsigned short mapID, CMapRelation::eMapRelatType &relatonType, unsigned short &associateMapId)
{
	for(std::vector<CMapRelation::strMapRelationData>::iterator iter = m_mapRels.begin(); m_mapRels.end() != iter; ++iter)
	{
		if(iter->outerMapID == mapID)
		{	
			relatonType = OUTER_MAP;
			associateMapId = iter->innerMapID;
			return true;
		}

		if(iter->innerMapID == mapID)
		{	
			relatonType = INNER_MAP;
			associateMapId = iter->outerMapID;
			return true;
		}
	}

	return false;
}
