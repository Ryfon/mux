﻿#ifndef MAP_RELATION_H
#define MAP_RELATION_H

#include <vector>
#include <time.h>


//内外城地图关系
class CMapRelation
{
public:
	enum eMapRelatType
	{
		INNER_MAP = 1,
		OUTER_MAP = 2
	};

	struct strMapRelationData
	{
		unsigned short innerMapID;
		unsigned short outerMapID;
	};

public:
	static bool InitMapRelation(void);

	static bool GetMapRelation(unsigned short mapID, eMapRelatType &val, unsigned short &associateMapId);

private:
	static std::vector<strMapRelationData> m_mapRels;
};

//关卡攻城战附加信息
struct MapWarAdditionalInfo
{
	CMapRelation::eMapRelatType mapRelationType;//本地图关系类型：外城/内城?
	unsigned short associatedMapId;//关联地图编号
	unsigned short warRebirthRace;//本地图复活点归属种族
	unsigned short warSwitchRace;//本地图跳转点归属种族
	bool warStartFlag;//攻城战开启标记
	unsigned int warStartTime;//攻城战开启时间
	
	MapWarAdditionalInfo(void)
	{
		mapRelationType = CMapRelation::OUTER_MAP;
		associatedMapId = 0;
		warRebirthRace = 0;
		warSwitchRace = 0;
		warStartFlag = false;
		warStartTime = 0;
	}

	void SetWarRebirthRace(unsigned short race)
	{
		warRebirthRace = Convert(race);
	}

	void SetWarSwitchRace(unsigned short race)
	{
		warSwitchRace = Convert(race);
	}

	void SetWarStart(void)
	{
		warStartFlag = true;
		warStartTime = (unsigned int)time(NULL);
	}

	void SetWarEnd(void)
	{
		warStartFlag = false;
		warStartTime = 0;
	}

	unsigned short Convert(unsigned short inputVal)
	{
		if(0 == inputVal)
			return 0;

		unsigned short outputVal = 0;
		do
		{
			unsigned short temp = inputVal % 10;
			inputVal = inputVal / 10;
			if(temp > 0)
				outputVal |= (1 << (temp-1));

		}while(0 != inputVal);

		return outputVal;
	}

	bool RebirthRaceEnabled(unsigned int race)
	{
		if(0 == race)
			return false;

		return (warRebirthRace & (1 << (race-1))) != 0;
	}

	bool SwitchRaceEnabled(unsigned int race)
	{
		if(0 == race)
			return false;

		return (warSwitchRace  & (1 << (race-1))) != 0;
	}
};


#endif/*MAP_RELATION_H*/
