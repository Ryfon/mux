﻿/**
* @file MapBlock.cpp
* @brief 定义地图小块
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MapBlock.cpp
* 摘    要: 定义地图小块
* 作    者: dzj
* 开始日期: 2008.07.02
* 
*/

#include "MapBlock.h"

#include "muxmapbase.h"
#include "muxnormalmap.h"
#include "muxcopy.h"

#include "../Player/Player.h"
#include "../monster/monster.h"

#include "../NpcSkill.h"
#include "../ItemSkill.h"

#include "../RideState.h"

#include "../monster/monsterwrapper.h"
#include "../../../Base/Utility.h"
#include "../../../Base/PkgProc/CliProtocol.h"
#include "../BattleRule.h"
#include "../Lua/BattleScript.h"

#include "../Player/FightPet.h"

//#ifdef STAGE_MAP_SCRIPT
//#include "mapcomponent.h"
//#endif //STAGE_MAP_SCRIPT

#include <algorithm>

bool MuxMapSpace::g_bIsViewDebug = false;

#ifdef ITEM_NPC 
bool operator <(const MapItemNpc& itemNpc1, const MapItemNpc& itemNpc2 )
{
	return (itemNpc1.itemNpcSeq < itemNpc2.itemNpcSeq );
}
#endif //ITEM_NPC

////暂时不用集聚通知, by dzj, 09.05.08,MGOtherPlayerInfo MuxMapBlock::m_arrBlockPlayerInfo[100];

///属于副本的地图块时钟;
void MuxMapBlock::BlockCopyTimer()
{
	if ( blockMonsters.empty() )
	{
		return;
	}

	vector<CMonster*> toExpMonsters;//遍历过程中可能会影响块上怪物，因此必须另行拷贝
	CMonster* pMonster = NULL;
	for ( list<CMonster*>::iterator iter=blockMonsters.begin(); iter!=blockMonsters.end(); ++iter )
	{
		pMonster = *iter;
		if ( NULL != pMonster )
		{
			toExpMonsters.push_back( pMonster );
		}
	}

	for ( vector<CMonster*>::iterator iter=toExpMonsters.begin(); iter!=toExpMonsters.end(); ++iter )
	{
		pMonster = *iter;
		if ( NULL != pMonster )
		{
			pMonster->SlowTimerProc();
		}
	}

	return;
}

bool MuxMapBlock::FillGroupInfoWithSelfRideTeamExecpt(CGroupNotiInfo& groupNotiInfo, const PlayerID& exceptTeamLeaderID, const PlayerID& exceptTeamAssistID, bool& isSomeOneAdd )
{
	TRY_BEGIN;

	isSomeOneAdd = false;

	if ( blockPlayers.empty() )
	{
		return true;
	}

	CPlayer* pBlockPlayer = NULL;//块上玩家；
	PlayerID blockPlayerID;
	for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
	{
		pBlockPlayer = *iter;
		if ( NULL == pBlockPlayer )
		{
			D_ERROR( "FillGroupInfoWithSelfPlayer,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
			continue;
		}

		if ( pBlockPlayer->IsLeaveDelay() )
		{
			//玩家已离线，除向DB的更新消息外，其余消息不再发送；
			continue;
		}

		blockPlayerID = pBlockPlayer->GetFullID();

		if ( ( exceptTeamLeaderID.dwPID == blockPlayerID.dwPID ) && ( exceptTeamLeaderID.wGID == blockPlayerID.wGID ) ) 
		{
			//被排除对象(队长)；
			continue;
		}

		if ( ( exceptTeamAssistID.dwPID == blockPlayerID.dwPID ) && ( exceptTeamAssistID.wGID == blockPlayerID.wGID ) ) 
		{
			//被排除对象(队员)；
			continue;
		}

		//D_DEBUG( "FillGroupInfoWithSelfPlayer，添加玩家%s(%d),block(%d,%d)\n", pBlockPlayer->GetAccount(), pBlockPlayer->GetPID(), GetBlockX(), GetBlockY() );
		if ( !( groupNotiInfo.AddGroupNotiPlayerID(blockPlayerID) ) )
		{
			return false;
		}
		isSomeOneAdd = true;
	}

	return true;
	TRY_END;
	return false;	
}

///根据块上玩家信息填充广播对象，但不通知特定人物；
bool MuxMapBlock::FillGroupInfoWithSelfPlayerExcept( CGroupNotiInfo& groupNotiInfo, const PlayerID& exceptID, bool& isSomeOneAdd )
{
	TRY_BEGIN;

	isSomeOneAdd = false;

	if ( blockPlayers.empty() )
	{
		return true;
	}

	CPlayer* pBlockPlayer = NULL;//块上玩家；
	PlayerID blockPlayerID;
	for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
	{
		pBlockPlayer = *iter;
		if ( NULL == pBlockPlayer )
		{
			D_ERROR( "FillGroupInfoWithSelfPlayer,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
			continue;
		}

		if ( pBlockPlayer->IsLeaveDelay() )
		{
			//玩家已离线，除向DB的更新消息外，其余消息不再发送；
			continue;
		}

		blockPlayerID = pBlockPlayer->GetFullID();

		if ( ( exceptID.dwPID == blockPlayerID.dwPID ) && ( exceptID.wGID == blockPlayerID.wGID ) ) 
		{
			//被排除对象；
			continue;
		}

		//D_DEBUG( "FillGroupInfoWithSelfPlayer，添加玩家%s(%d),block(%d,%d)\n", pBlockPlayer->GetAccount(), pBlockPlayer->GetPID(), GetBlockX(), GetBlockY() );
		if ( !( groupNotiInfo.AddGroupNotiPlayerID(blockPlayerID) ) )
		{
			return false;
		}
		isSomeOneAdd = true;
	}

	return true;
	TRY_END;
	return false;	
}




bool MuxMapBlock::FillGroupInfoWithSelfPlayer( CGroupNotiInfo& groupNotiInfo, bool& isSomeOneAdd/*是否添加了通知对象*/ )
{
	TRY_BEGIN;

	isSomeOneAdd = false;
	if ( blockPlayers.empty() )
	{
		return true;
	}

	CPlayer* pBlockPlayer = NULL;//块上玩家；
	for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
	{
		pBlockPlayer = *iter;
		if ( NULL == pBlockPlayer )
		{
			D_ERROR( "FillGroupInfoWithSelfPlayer,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
			continue;
		}

		if ( pBlockPlayer->IsLeaveDelay() )
		{
			//玩家已离线，除向DB的更新消息外，其余消息不再发送；
			continue;
		}

		if ( !( groupNotiInfo.AddGroupNotiPlayerID(pBlockPlayer->GetFullID()) ) )
		{
			return false;
		}
		isSomeOneAdd = true;//的确添加了一些人；
	}

	return true;
	TRY_END;
	return false;	
}

//暂时不用集聚通知, by dzj, 09.05.08///集聚情形下的块玩家信息定时通知；
//MGOtherPlayerInfo* MuxMapBlock::BlockConcentPlayerNotiMsg( unsigned int& blockPlayerNum)
//{
//	blockPlayerNum = 0;
//	if ( blockPlayers.empty() )
//	{
//		return NULL;
//	}
//
//	//准备组包；
//	blockPlayerNum = (unsigned int) (blockPlayers.size());
//	unsigned int curarrpos = 0u;
//	CPlayer* pBlockPlayer = NULL;//块上玩家；
//	const unsigned int arrsize = (unsigned int) ( sizeof(m_arrBlockPlayerInfo) / sizeof(m_arrBlockPlayerInfo[0]) );
//	if ( blockPlayerNum > arrsize )
//	{
//		D_WARNING( "块(%d,%d)玩家数%d超过%d，通知信息可能丢失\n", blockPlayerNum, arrsize  );
//		blockPlayerNum = arrsize;
//	}
//
//	for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
//	{
//		if ( curarrpos >= blockPlayerNum )
//		{
//			break;
//		}
//
//		pBlockPlayer = *iter;
//		if ( NULL == pBlockPlayer )
//		{
//			D_ERROR( "PlayerMoveNewBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
//			continue;
//		}
//		//1.通知本玩家block玩家信息与移动信息？
//		m_arrBlockPlayerInfo[curarrpos].otherPlayerID = pBlockPlayer->GetFullID();
//		pBlockPlayer->GetPlayerLoginInfo(m_arrBlockPlayerInfo[curarrpos].playerInfoLogin);
//		m_arrBlockPlayerInfo[curarrpos].nCurHp = pBlockPlayer->GetCurrentHP();
//		m_arrBlockPlayerInfo[curarrpos].nCurMagic = pBlockPlayer->GetCurrentMP();
//		m_arrBlockPlayerInfo[curarrpos].fSpeed = pBlockPlayer->GetSpeed();
//		m_arrBlockPlayerInfo[curarrpos].fTargetX = pBlockPlayer->GetFloatTargetX();
//		m_arrBlockPlayerInfo[curarrpos].fTargetY = pBlockPlayer->GetFloatTargetY();
//		m_arrBlockPlayerInfo[curarrpos].nErrCode = MGOtherPlayerInfo::ISERR_OK;
//		m_arrBlockPlayerInfo[curarrpos].nMaxHp = pBlockPlayer->GetMaxHP();
//		m_arrBlockPlayerInfo[curarrpos].nMaxMagic = pBlockPlayer->GetMaxMP();
//		m_arrBlockPlayerInfo[curarrpos].bInTeam = pBlockPlayer->IsInTeam();
//		m_arrBlockPlayerInfo[curarrpos].bTeamCaptain = pBlockPlayer->IsTeamCaptain();
//		m_arrBlockPlayerInfo[curarrpos].bRogue = pBlockPlayer->IsRogueState();
//		m_arrBlockPlayerInfo[curarrpos].sGoodEvilPoint = pBlockPlayer->GetGoodEvilPoint();
//		++curarrpos;
//	}
//
//	return &(m_arrBlockPlayerInfo[0]);
//}


///怪物移动新进入视野块，相关通知；
void MuxMapBlock::MonsterMoveNewBlockNotify( CMonster* pMoveMonster )
{
	if ( !pMoveMonster )
		return;

	//1、通知块上玩家怪物移动；
	//2、通知移动怪物:本block玩家新进入视野；

	CPlayer* pBlockPlayer = NULL;//块上玩家；
	CMonster* pBlockMonster = NULL;//块上怪物；

	if ( !blockPlayers.empty() )
	{
//#ifndef GROUP_NOTI
//		MGMonsterMove moveMsg;
//		moveMsg.fSpeed = pMoveMonster->GetSpeed();
//		moveMsg.lMonsterID = pMoveMonster->GetMonsterID();
//		moveMsg.nNewMapPosX = pMoveMonster->GetTargetX();
//		moveMsg.nNewMapPosY = pMoveMonster->GetTargetY();
//		moveMsg.nOrgMapPosX = pMoveMonster->GetPosX();
//		moveMsg.nOrgMapPosY = pMoveMonster->GetPosY();
//#endif //GROUP_NOTI

		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
		{
			pBlockPlayer = *iter;
			if ( NULL == pBlockPlayer )
			{
				D_ERROR( "MonsterMoveNewBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}

//#ifndef GROUP_NOTI
//	   	 	pBlockPlayer->SendPkg<MGMonsterMove>( pMoveMonster->GetMonsterMove() ); //通知玩家怪物移动；
//#endif //GROUP_NOTI
			//D_DEBUG("通知玩家 怪物位置(%d,%d) 目标位置(%d,%d)",pMoveMonster->GetPosX(),pMoveMonster->GetPosY(),pMoveMonster->GetTargetX(), pMoveMonster->GetTargetY() );

			//if( pBlockPlayer->GetCurrentHP() != 0 )
			{
				//D_DEBUG( "AI匹配5，通知怪物%d，玩家%d进入\n", pMoveMonster->GetMonsterID(), pBlockPlayer->GetFullID().dwPID );
				pMoveMonster->OnPlayerDetected( pBlockPlayer ); //通知怪物玩家进入视野；
			}
		}
	}

	//互相通知：块上怪物<-->移动怪物，怪物新进入视野；
	//////////////////////////////////////////////////////////////////////////////
	//情况1 如果是特殊NPC需要通知所有块内NPC的情况
	bool isBlockHaveSpecialNpc = (specialNpcNum > 0);
	bool isMoveMonsteterSpecicalNpc = pMoveMonster->NeedNotiOtherMonster();
	if( isBlockHaveSpecialNpc || isMoveMonsteterSpecicalNpc )
	{
		if ( !blockMonsters.empty() )
		{
			for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
			{
				pBlockMonster = *iter;
				if ( NULL == pBlockMonster )
				{
					D_ERROR( "MonsterMoveNewBlockNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
					continue;
				}
				if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
				{
					///怪物待删；
					continue;
				}

				if(pBlockMonster->GetNpcType() == 17 )
				{
					continue;
				}

				if( pMoveMonster == pBlockMonster )
				{
					continue;
				}

				if( isBlockHaveSpecialNpc && pBlockMonster->NeedNotiOtherMonster() )
				{
					pBlockMonster->OnMonsterEnterView( pMoveMonster );
				}
				if( isMoveMonsteterSpecicalNpc )
				{
					pMoveMonster->OnMonsterEnterView( pBlockMonster );
				}
			}
		}
	} 
}


void MuxMapBlock::PtGetScopeCreature( TYPE_POS posX, TYPE_POS posY, int range, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster )
{
	CMonster* pTmpMonster = NULL;
	TYPE_POS targetPosX = 0, targetPosY = 0;
	if ( !blockMonsters.empty() )
	{
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pTmpMonster = *iter;
			if( NULL == pTmpMonster )
			{
				D_ERROR( "GetInPtRangeTargetAoe,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pTmpMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pTmpMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}

			if( pTmpMonster->GetNpcType() == 17 )
			{
				//采集NPC
				continue;
			}
			
			pTmpMonster->GetPos( targetPosX, targetPosY );
			if( IsInVagueAttDistance( posX, posY, targetPosX, targetPosY, 0, range, true ) )
			{
				vecMonster.push_back( pTmpMonster );
			}
		}
	}

	CPlayer* pTmpPlayer = NULL;
	if( !blockPlayers.empty() )
	{
		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
		{
			pTmpPlayer = *iter;
			if( NULL == pTmpPlayer )
			{
				D_ERROR( "GetInPtRangeTargetAoe,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
		
			pTmpPlayer->GetPos( targetPosX, targetPosY );
			if( IsInVagueAttDistance( posX, posY, targetPosX, targetPosY, 0, range, true ) )
			{
				vecPlayer.push_back( pTmpPlayer );
			}
		}
	}
}


void MuxMapBlock::NotifyMonsterAddHpEvent( const PlayerID& addStarter, const PlayerID& addTarget, unsigned int addVal )
{
	CMonster* pTmpMonster = NULL;
	if ( !blockMonsters.empty() )
	{
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pTmpMonster = *iter;
			if( NULL == pTmpMonster )
			{
				D_ERROR( "GetInPtRangeTargetAoe,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pTmpMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pTmpMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}


			if( pTmpMonster->GetNpcType() == 17 )
			{
				//采集NPC
				continue;
			}
			
			pTmpMonster->OnPlayerAddHp( addStarter, addTarget, addVal );
		}
	}
}


void MuxMapBlock::PtPlayerGetSkillTarget( TYPE_POS posX, TYPE_POS posY, unsigned int range, CPlayer* pUseSkillPlayer, CBattleScript* pScript, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster )
{
	if ( NULL == pUseSkillPlayer )
	{
		D_ERROR( "MuxMapBlock::PtPlayerGetSkillTarget，NULL == pUseSkillPlayer，块(%d,%d)\n", GetBlockX(), GetBlockY() );
		return;
	}

	if ( NULL == pScript )
	{
		D_ERROR( "MuxMapBlock::PtPlayerGetSkillTarget，NULL == pScript，块(%d,%d)\n", GetBlockX(), GetBlockY() );
		return;
	}

	CMonster* pTmpMonster = NULL;
	TYPE_POS targetPosX = 0, targetPosY = 0;
	unsigned int ret = 0;
	if ( !blockMonsters.empty() )
	{
		//对BLOCK上的怪进行攻击
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pTmpMonster = *iter;
			if( NULL == pTmpMonster )
			{
				D_ERROR( "MuxMapBlock::PtPlayerGetSkillTarget, NULL == pTmpMonster, 块(%d,%d)\n", GetBlockX(), GetBlockY() );
				continue;
			}

			pScript->FnCheckPveBattleSecondStage( pUseSkillPlayer, pTmpMonster, ret );
			if( ret != 0 )
			{
				continue;
			}
			
			pTmpMonster->GetPos( targetPosX, targetPosY );
			if( IsInVagueAttDistance( posX, posY, targetPosX, targetPosY, 0, range, true ) )
			{
				vecMonster.push_back( pTmpMonster );
			}
		}
	}// if ( !blockMonsters.empty() )

	//对BLOCK上的人进行攻击
	CPlayer* pTmpPlayer = NULL;
	if ( !blockPlayers.empty() )
	{
		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
		{
			pTmpPlayer = *iter;
			if( NULL == pTmpPlayer )
			{
				D_ERROR( "MuxMapBlock::PtPlayerGetSkillTarget, NULL == pTmpPlayer,块(%d,%d)\n", GetBlockX(), GetBlockY() );
				continue;
			}
			
			pScript->FnCheckPvpBattleSecondStage( pUseSkillPlayer, pTmpPlayer, ret );
			if( ret != 0 )
			{
				continue;
			}

		
			pTmpPlayer->GetPos( targetPosX, targetPosY );
			if( IsInVagueAttDistance( posX, posY, targetPosX, targetPosY, 0, range, true ) )
			{
				vecPlayer.push_back( pTmpPlayer );
			}
		}
	}// if (!blockPlayer.empty())

	return;
}


void MuxMapBlock::PtMonsterGetSkillTarget( TYPE_POS posX, TYPE_POS posY, unsigned int range, CMonster* pUseSkillMonster, vector<CPlayer*>& vecPlayer, vector<CMonster*> vecMonster )
{
	CMonster* pTmpMonster = NULL;
	TYPE_POS targetPosX = 0, targetPosY = 0;
	unsigned int ret = 0;
	if ( !blockMonsters.empty() )
	{
		//对BLOCK上的怪进行攻击
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pTmpMonster = *iter;
			if( NULL == pTmpMonster )
				continue;

			if ( pTmpMonster->IsMonsterDie() )
			{
				continue;//已死怪物不受攻击；
			}

			if( !CBattleRule::NpcCheckNpc( pUseSkillMonster, pTmpMonster ) )
			{
				continue;
			}
			
			pTmpMonster->GetPos( targetPosX, targetPosY );
			if( IsInVagueAttDistance( posX, posY, targetPosX, targetPosY, 0, range, true ) )
			{
				vecMonster.push_back( pTmpMonster );
			}
		}
	}

	//对BLOCK上的人进行攻击
	CPlayer* pTmpPlayer = NULL;
	if( !blockPlayers.empty() )
	{
		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
		{
			pTmpPlayer = *iter;
			if( NULL == pTmpPlayer )
			{
				D_ERROR( "GetInPtRangeTargetAoe,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}

			if ( pTmpPlayer->IsDying() )
			{
				continue;//已死玩家不再受攻击;
			}
			
			if( !CBattleRule::NpcCheckPlayer( pUseSkillMonster, pTmpPlayer ) )
			{
				continue;
			}

			pTmpPlayer->GetPos( targetPosX, targetPosY );
			if( IsInVagueAttDistance( posX, posY, targetPosX, targetPosY, 0, range, true ) )
			{
				vecPlayer.push_back( pTmpPlayer );
			}
		}
	}
}


void MuxMapBlock::PtPlayerSkillGetTeamTarget( TYPE_POS posX, TYPE_POS posY, unsigned int range, CPlayer* pUseSkillPlayer, CBattleScript* pScript, vector<CPlayer*>& vecPlayer )
{
	if ( NULL == pUseSkillPlayer )
	{
		D_ERROR( "MuxMapBlock::PtPlayerSkillGetTeamTarget, NULL == pUseSkillPlayer，块(%d,%d)\n", GetBlockX(), GetBlockY() );
		return;
	}
	if ( NULL == pScript )
	{
		D_ERROR( "MuxMapBlock::PtPlayerSkillGetTeamTarget，NULL == pScript，块(%d,%d)\n", GetBlockX(), GetBlockY() );
		return;
	}

	//对BLOCK上的人进行攻击
	CPlayer* pTmpPlayer = NULL;
	TYPE_POS targetPosX = 0, targetPosY = 0;
	if ( blockPlayers.empty() )
	{
		//原逻辑修改过来；
		return;
	}

	for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
	{
		pTmpPlayer = *iter;
		if( NULL == pTmpPlayer )
		{
			D_ERROR( "GetInPtRangeTargetAoe,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
			continue;
		}

		if( pTmpPlayer->IsDying() || pTmpPlayer->GetGroupTeam() != pUseSkillPlayer->GetGroupTeam() )
			continue;

		unsigned int ret = 0;
		pScript->FnCheckPvpBattleSecondStage( pUseSkillPlayer, pTmpPlayer, ret );
		if( ret != 0 )
		{
			continue;
		}

		pTmpPlayer->GetPos( targetPosX, targetPosY );
		if( IsInVagueAttDistance( posX, posY, targetPosX, targetPosY, 0, range, true ) )
		{
			vecPlayer.push_back( pTmpPlayer );
		}
	}

	return;
}

//
//bool MuxMapBlock::GetBlockMonsterID( ScopeMonsterInfo* monsterArr, int maxSize, int& arrSize )
//{
//	if ( NULL == monsterArr )
//	{
//		D_ERROR( "MuxMapBlock::GetBlockMonsterID, 输入参数空\n" );
//		return false;
//	}
//
//	CMonster* pTmpMonster = NULL;
//	if ( !blockMonsters.empty() )
//	{
//		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
//		{
//			pTmpMonster = *iter;
//			if( NULL == pTmpMonster )
//			{
//				D_ERROR( "GetInPtRangeTargetAoe,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
//				continue;
//			}
//			if ( pTmpMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pTmpMonster->IsToDel() )
//			{
//				///怪物待删；
//				continue;
//			}
//
//			if( pTmpMonster->GetNpcType() == 17 )
//			{
//				//采集NPC
//				continue;
//			}
//			if( arrSize >= maxSize )
//				return false;
//
//			monsterArr[arrSize].monsterID = pTmpMonster->GetMonsterID();
//			monsterArr[arrSize].posX = pTmpMonster->GetPosX();
//			monsterArr[arrSize].posY = pTmpMonster->GetPosY();
//			arrSize++;
//		}
//	}
//	return true;
//}
//
//
//bool MuxMapBlock::GetBlockPlayerInfo( ScopePlayerInfo* playerArr, int maxSize, int& arrSize )
//{
//	if ( NULL == playerArr )
//	{
//		D_ERROR( "MuxMapBlock::GetBlockPlayerInfo, 输入参数空\n" );
//		return false;
//	}
//
//	if( arrSize >= maxSize )
//		return false;
//
//	CPlayer* pTmpPlayer = NULL;
//	if ( !blockPlayers.empty() )
//	{
//		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
//		{
//			pTmpPlayer = *iter;
//			if( NULL == pTmpPlayer )
//			{
//				D_ERROR( "GetInPtRangeTargetAoe,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
//				continue;
//			}
//
//			if( pTmpPlayer->GetCurrentHP() == 0 )
//				continue;
//		
//			if( arrSize >= maxSize )
//				return false;
//
//			playerArr[arrSize].playerID = pTmpPlayer->GetFullID();
//			playerArr[arrSize].posX = pTmpPlayer->GetPosX();
//			playerArr[arrSize].posY = pTmpPlayer->GetPosY();
//			playerArr[arrSize].playerLevel = pTmpPlayer->GetLevel();
//			arrSize++;
//		}
//	}
//	return true;
//}

bool MuxMapBlock::IsEnabelAddTower(TYPE_POS gridX, TYPE_POS gridY)
{
	CMonster* pMonster = NULL;
	for ( list<CMonster*>::iterator iter=blockMonsters.begin(); iter!=blockMonsters.end(); ++iter )
	{
		pMonster = *iter;
		if ( NULL != pMonster )
		{
			if (pMonster->GetNpcType() == 23)
			{
				TYPE_POS mX = pMonster->GetPosX();
				TYPE_POS mY = pMonster->GetPosY();
				if (abs(mX-gridX) <= TOWER_RADIUS*2 && abs(mY-gridY) <= TOWER_RADIUS*2)
				{
					return false;
				}
			}
			else
			{
				TYPE_POS mX = pMonster->GetPosX();
				TYPE_POS mY = pMonster->GetPosY();
				if (abs(mX-gridX) <= TOWER_RADIUS && abs(mY-gridY) <= TOWER_RADIUS)
				{
					return false;
				}
			}
		}
	}
	return true;
}

void MuxMapBlock::GetPlayersInRect(TYPE_POS gridLeft, TYPE_POS gridTop, TYPE_POS gridRight, TYPE_POS gridBottom, vector<CPlayer *>& vecPlayer)
{
	TRY_BEGIN;

	if(!blockPlayers.empty())
	{
		TYPE_POS gridMinX=0, gridMinY=0, gridMaxX=0, gridMaxY=0;
		if(gridLeft < gridRight)
		{
			gridMinX = gridLeft;
			gridMaxX = gridRight;
		}
		else
		{
			gridMinX = gridRight;
			gridMaxX = gridLeft;
		}

		if(gridTop < gridBottom)
		{
			gridMinY = gridTop;
			gridMaxY = gridBottom;
		}
		else
		{
			gridMinY = gridBottom;
			gridMaxY = gridTop;
		}

		TYPE_POS posX = 0, posY = 0;
		CPlayer *pPlayer = NULL;
		for(list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter)
		{
			pPlayer = *iter;
			if(NULL == pPlayer)
			{
				D_ERROR( "GetPlayersInRect,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}

			pPlayer->GetPos(posX, posY);
			if((posX >= gridMinX) && (posX <= gridMaxX) && (posY >= gridMinY) && (posY <= gridMaxY))
				vecPlayer.push_back(pPlayer);
		}
	}

	return ;
	TRY_END;
	return ;
}

///////////////////////////////////////////////////////////
//copy...
///怪物出生影响块，相关通知；
void MuxMapBlock::MonsterBornBlockNotify( CMonster* pBornMonster )
{
	if ( NULL == pBornMonster )
	{
		D_ERROR( "MuxMapBlock::MonsterBornBlockNotify, NULL == pBornMonster，块(%d,%d)\n", GetBlockX(), GetBlockY() );
		return;
	}

	if ( !blockPlayers.empty() )
	{
		CPlayer* pBlockPlayer = NULL;//块上玩家；
		MGMonsterBorn bornMsg;
		bornMsg.lMonsterID = pBornMonster->GetMonsterID();
		bornMsg.lMonsterTypeID = pBornMonster->GetClsID();
		bornMsg.nMapPosX = pBornMonster->GetPosX();
		bornMsg.nMapPosY = pBornMonster->GetPosY();
		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
		{
			pBlockPlayer = *iter;
			if( NULL == pBlockPlayer )
			{
				D_ERROR( "MonsterBornBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			//1、通知怪物:周围玩家出现；
			//if( pBlockPlayer->GetCurrentHP() != 0 )
			{
				//D_DEBUG( "AI匹配6，通知怪物%d，玩家%d进入\n", pBornMonster->GetMonsterID(), pBlockPlayer->GetFullID().dwPID );
				pBornMonster->OnPlayerDetected( pBlockPlayer );
			}

			//2、通知周围玩家:该怪物出现；
			pBlockPlayer->SendPkg<MGMonsterBorn>( &bornMsg );
		}
	}

	CMonster* pBlockMonster = NULL;
	//互相通知：块上怪物<-->移动怪物，怪物新进入视野；
	bool isBornMonsterSpecialNpc = pBornMonster->NeedNotiOtherMonster();
	bool isBlockHaveSpecialNpc = ( specialNpcNum > 0 );
	if ( (!isBornMonsterSpecialNpc) && (!isBlockHaveSpecialNpc) )
	{
		//按原逻辑改成，specialNPC是需要通知其它怪物的特别怪物？
		return;
	}
	if ( blockMonsters.empty() )
	{
		//按原逻辑改成，如果块上无怪物，则不必进行怪物间互相通知；
		return;
	}

	for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
	{
		pBlockMonster = *iter;
		if ( NULL == pBlockMonster )
		{
			D_ERROR( "MonsterMoveNewBlockNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
			continue;
		}
		if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
		{
			///怪物待删；
			continue;
		}

		if(pBlockMonster->GetNpcType() == 17 )
		{
			continue;
		}

		if( pBlockMonster == pBornMonster )
		{
			continue;
		}

		if( isBlockHaveSpecialNpc && pBlockMonster->NeedNotiOtherMonster() )
		{
			pBlockMonster->OnMonsterEnterView( pBornMonster );
		}
		if( isBornMonsterSpecialNpc )
		{
			pBornMonster->OnMonsterEnterView( pBlockMonster );
		}
	}// for blockMonsters;

	return;
}


void MuxMapBlock::MonsterLeaveViewNotify( CMonster* pLeaveViewMonster )
{
	if ( NULL == pLeaveViewMonster )
	{
		D_ERROR( "MuxMapBlock::MonsterLeaveViewNotify, 输入参数空\n" );
		return;
	}

	//互相通知：块上怪物<-->移动怪物，怪物新进入视野；
	CMonster* pBlockMonster = NULL;
	if ( specialNpcNum > 0 )
	{
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pBlockMonster = *iter;
			if ( NULL == pBlockMonster )
			{
				D_ERROR( "MonsterMoveOrgBlockNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}

			if( pBlockMonster->NeedNotiOtherMonster() )
			{
				pBlockMonster->OnMonsterLeaveView( pLeaveViewMonster );
			}
		}
	}

	return;
}

///怪物移动离开视野块，相关通知；
void MuxMapBlock::MonsterMoveOrgBlockNotify( CMonster* pMoveMonster )
{
	if ( NULL == pMoveMonster )
	{
		return;
	}
	CPlayer* pBlockPlayer = NULL;
	CMonster* pBlockMonster = NULL;

	if ( !blockPlayers.empty() )
	{
		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
		{
			pBlockPlayer = *iter;
			if ( NULL == pBlockPlayer )
			{
				D_ERROR( "MonsterMoveOrgBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			//通知移动怪物:本block玩家离开视野；
			//D_DEBUG( "AI匹配1，通知怪物%d，玩家%d离开\n", pMoveMonster->GetMonsterID(), pBlockPlayer->GetFullID().dwPID );
			pMoveMonster->OnPlayerLeaveView( pBlockPlayer, AILR_LEAVEVIEW );
		}
	}

	//互相通知：块上怪物<-->移动怪物，怪物新进入视野；
	bool isMoveMonsterSpcialNpc = pMoveMonster->NeedNotiOtherMonster();
	bool isBlockHaveSpecialNpc = ( specialNpcNum > 0 );
	if( isMoveMonsterSpcialNpc || isBlockHaveSpecialNpc )
	{
		if ( !blockMonsters.empty() )
		{
			for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
			{
				pBlockMonster = *iter;
				if ( NULL == pBlockMonster )
				{
					D_ERROR( "MonsterMoveOrgBlockNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
					continue;
				}
				if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
				{
					///怪物待删；
					continue;
				}

				if( pMoveMonster == pBlockMonster )
				{
					continue;
				}

				if( isBlockHaveSpecialNpc && pBlockMonster->NeedNotiOtherMonster() )
				{
					pBlockMonster->OnMonsterLeaveView( pMoveMonster );
				}
				if( isMoveMonsterSpcialNpc )
				{
					pMoveMonster->OnMonsterLeaveView( pBlockMonster );
				}
			}
		}
	}
}
//
/////怪物出生影响块，相关通知；
//void MuxMapBlock::MonsterBornBlockNotify( CMonster* pBornMonster )
//{
//	if ( blockPlayers.empty() )
//	{
//		return;
//	}
//
//	CPlayer* pBlockPlayer = NULL;//块上玩家；
//    MGMonsterBorn bornMsg;
//	bornMsg.lMonsterID = pBornMonster->GetMonsterID();
//	bornMsg.lMonsterTypeID = pBornMonster->GetClsID();
//	bornMsg.nMapPosX = pBornMonster->GetPosX();
//	bornMsg.nMapPosY = pBornMonster->GetPosY();
//	for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
//	{
//		pBlockPlayer = *iter;
//		if( NULL == pBlockPlayer )
//		{
//			D_ERROR( "MonsterBornBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
//			continue;
//		}
//		//1、通知怪物:周围玩家出现；
//		if( pBlockPlayer->GetCurrentHP() != 0 )
//		{
//			pBornMonster->OnPlayerDetected( pBlockPlayer );
//		}
//
//		//2、通知周围玩家:该怪物出现；
//		pBlockPlayer->SendPkg<MGMonsterBorn>( &bornMsg );
//
//	}
//}
/////怪物移动离开视野块，相关通知；
//void MuxMapBlock::MonsterMoveOrgBlockNotify( CMonster* pMoveMonster )
//{
//	CPlayer* pBlockPlayer = NULL;
//	CMonster* pBlockMonster = NULL;
//
//	if ( !blockPlayers.empty() )
//	{
//		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
//		{
//			pBlockPlayer = *iter;
//			if ( NULL == pBlockPlayer )
//			{
//				D_ERROR( "MonsterMoveOrgBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
//				continue;
//			}
//			//通知移动怪物:本block玩家离开视野；
//			pMoveMonster->OnPlayerLeaveView( pBlockPlayer, AILR_LEAVEVIEW );
//		}
//	}
//
//	//互相通知：块上怪物<-->移动怪物，怪物新进入视野；
//	if ( !blockMonsters.empty() )
//	{
//		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
//		{
//			pBlockMonster = *iter;
//			if ( NULL == pBlockMonster )
//			{
//				D_ERROR( "MonsterMoveOrgBlockNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
//				continue;
//			}
//			if ( pBlockMonster->IsToDel() )
//			{
//				///怪物待删；
//				continue;
//			}
//			pBlockMonster->OnMonsterLeaveView( pMoveMonster );
//			pMoveMonster->OnMonsterLeaveView( pBlockMonster );
//		}
//	}
//
//}
//...copy
///////////////////////////////////////////////////////////


//移动玩家跨块时通知玩家自身其周围玩家的信息(但暂时不通知该玩家周边该玩家的移动，因为在SRV定时刷玩家位置时已通知过了)；
void MuxMapBlock::PlayerBeyondBlockSelfNotify( CPlayer* pBeyondPlayer )
{
	if ( NULL == pBeyondPlayer )
	{
		return;
	}

	if ( !(blockPlayers.empty()) )
	{
		MGBuffData blockBuffInfo;
		MGPetinfoStable*  petInfo = NULL;
		MGSimplePlayerInfo* blockPlayerInfo = NULL;
		MGStartCast blockStartCast;
		CPlayer* pBlockPlayer = NULL;//块上玩家；
		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
		{
			pBlockPlayer = *iter;
			if ( NULL == pBlockPlayer )
			{
				D_ERROR( "PlayerMoveNewBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			//1.通知本玩家block玩家信息与移动信息？
			blockPlayerInfo = pBlockPlayer->GetSimplePlayerInfo();
			if( NULL != blockPlayerInfo )
			{
				pBeyondPlayer->SendPkg<MGSimplePlayerInfo>( blockPlayerInfo );
			}

			CUnion *pBlockPlayerUnion = pBlockPlayer->Union();
			if(NULL != pBlockPlayerUnion)
			{
				MGUnionMemberShowInfo *pBlockPlayerShowInfo = pBlockPlayerUnion->GetUnionMemberShowInfo(pBlockPlayer);
				pBeyondPlayer->SendPkg<MGUnionMemberShowInfo>( pBlockPlayerShowInfo);
			}

			if( pBlockPlayer->IsHaveBuff() )
			{
				blockBuffInfo.buffData = (*pBlockPlayer->GetBuffData());
				blockBuffInfo.targetID = pBlockPlayer->GetFullID();
				pBeyondPlayer->SendPkg<MGBuffData>( &blockBuffInfo );
			}
			if( pBlockPlayer->IsSummonPet() )
			{
				petInfo = pBlockPlayer->GetSurPetNotiInfo();
				if ( NULL != petInfo )
				{
					pBeyondPlayer->SendPkg< MGPetinfoStable > ( petInfo );
				}
			}
			if ( pBlockPlayer->GetCastManager().IsInCast() )
			{
				blockStartCast.req.playerID = pBlockPlayer->GetFullID();
				blockStartCast.req.skillID = pBlockPlayer->GetCastManager().GetCastSkillID();
				blockStartCast.req.castTime = (USHORT)(pBlockPlayer->GetCastManager().GetCastTime() - (GetTickCount()/1000-pBlockPlayer->GetCastManager().GetCastStartTime()));
				pBeyondPlayer->SendPkg<MGStartCast>( &blockStartCast );
			}
		}
	}

	if ( !(blockMonsters.empty()) )
	{
		
		MGBuffData monsterBuff;
		CMonster* pBlockMonster = NULL;//块上怪物；
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pBlockMonster = *iter;
			if ( NULL == pBlockMonster )
			{
				D_ERROR( "PlayerMoveNewBlockNotify,块(%d,%d)上的玩物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}
			//3.通知本玩家block怪物信息与怪物移动信息；
#ifdef ITEM_NPC
			if( !pBlockMonster->IsItemNpc() )  //不是物件NPC才发
			{
#endif //ITEM_NPC
	
				pBeyondPlayer->SendPkg<MGMonsterInfo>( pBlockMonster->GetMonsterInfo() );

				if( pBlockMonster->IsHaveBuff() )
				{
					monsterBuff.buffData = pBlockMonster->GetBuffData();
					monsterBuff.targetID.dwPID = pBlockMonster->GetMonsterID();
					monsterBuff.targetID.wGID = MONSTER_GID;
					pBeyondPlayer->SendPkg<MGBuffData>( &monsterBuff );
				}
#ifdef ITEM_NPC
			}
#endif //ITEM_NPC
		}
	}

	if ( !(blockRideTeams.empty()) )
	{
		NoticeRideTeamAppear( pBeyondPlayer );
	}

	if ( !(blockItemPkgs.empty()) )
	{
		NoticeItemPkgsAppear( pBeyondPlayer );
	}

	return;
}

///玩家移动新进入视野块，相关通知；
void MuxMapBlock::PlayerMoveNewBlockNotify( CPlayer* pMovePlayer )
{
	if ( NULL == pMovePlayer )
	{
		return;
	}
	
	if ( !(blockPlayers.empty()) )
	{
//#ifndef GROUP_NOTI
//		MGOtherPlayerInfo* movPlayerInfo = NULL;
//		movPlayerInfo = pMovePlayer->GetOtherPlayerInfo();
//
//		MGUnionMemberShowInfo *pMovePlayerShowInfo;
//		CUnion *pMovePlayerUnion = pMovePlayer->Union();
//		if(NULL != pMovePlayerUnion)
//		{
//			pMovePlayerShowInfo = pMovePlayerUnion->GetUnionMemberShowInfo(pMovePlayer);
//		}
//	
//		MGPetinfoStable*  movePetInfo = NULL;
//		MGBuffData moveBuffInfo;
//		if( pMovePlayer->IsHaveBuff() )
//		{
//			moveBuffInfo.buffData = (*pMovePlayer->GetBuffData());
//			moveBuffInfo.targetID = pMovePlayer->GetFullID();
//		}
//
//		if( pMovePlayer->IsSummonPet() )
//		{
//			movePetInfo = pMovePlayer->GetSurPetNotiInfo();
//		}
//#endif //GROUP_NOTI
		MGBuffData blockBuffInfo;
		MGPetinfoStable* blockPetInfo = NULL;
		MGSimplePlayerInfo* blockPlayerInfo = NULL;
		MGStartCast blockStartCast;
		CPlayer* pBlockPlayer = NULL;//块上玩家；
		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
		{
			pBlockPlayer = *iter;
			if ( NULL == pBlockPlayer )
			{
				D_ERROR( "PlayerMoveNewBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}

//#ifndef GROUP_NOTI
//			//1.通知block玩家本玩家信息与移动信息？
//			pBlockPlayer->SendPkg<MGOtherPlayerInfo>( &movPlayerInfo );
//			if( pMovePlayer->IsHaveBuff() )
//			{
//				pBlockPlayer->SendPkg<MGBuffData>( &moveBuffInfo );
//			}
//
//			if(NULL != pMovePlayerUnion)
//			{
//				pBlockPlayer->SendPkg<MGUnionMemberShowInfo> (*pMovePlayerShowInfo);
//			}
//
//			if( pMovePlayer->IsSummonPet() )
//			{
//				if ( NULL != movePetInfo )
//				{
//					pBlockPlayer->SendPkg<MGPetinfoStable>( movePetInfo );
//				}
//			}
//#endif //GROUP_NOTI

			//2.通知本玩家block玩家信息与移动信息？
			blockPlayerInfo = pBlockPlayer->GetSimplePlayerInfo();
			if( NULL != blockPlayerInfo )
			{
				pMovePlayer->SendPkg<MGSimplePlayerInfo>( blockPlayerInfo );
			}
			
			CUnion *pBlockPlayerUnion = pBlockPlayer->Union();
			if(NULL != pBlockPlayerUnion)
			{
				MGUnionMemberShowInfo *pBlockPlayerShowInfo = pBlockPlayerUnion->GetUnionMemberShowInfo(pBlockPlayer);
				pMovePlayer->SendPkg<MGUnionMemberShowInfo>(pBlockPlayerShowInfo);
			}

			if( pBlockPlayer->IsHaveBuff() )
			{
				blockBuffInfo.buffData = (*pBlockPlayer->GetBuffData());
				blockBuffInfo.targetID = pBlockPlayer->GetFullID();
				pMovePlayer->SendPkg<MGBuffData>( &blockBuffInfo );
			}

			if( pBlockPlayer->IsSummonPet() )
			{
				blockPetInfo = pBlockPlayer->GetSurPetNotiInfo();
				if ( NULL != blockPetInfo )
				{
					pMovePlayer->SendPkg<MGPetinfoStable>( blockPetInfo );
				}
			}
			if ( pBlockPlayer->GetCastManager().IsInCast() )
			{
				blockStartCast.req.playerID = pBlockPlayer->GetFullID();
				blockStartCast.req.skillID = pBlockPlayer->GetCastManager().GetCastSkillID();
				blockStartCast.req.castTime = (USHORT)(pBlockPlayer->GetCastManager().GetCastTime() - (GetTickCount()/1000-pBlockPlayer->GetCastManager().GetCastStartTime()));
				pMovePlayer->SendPkg<MGStartCast>( &blockStartCast );
			}
		}
	}

	if ( !(blockMonsters.empty()) )
	{
		MGBuffData monsterBuff;
		CMonster* pBlockMonster = NULL;//块上怪物；
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pBlockMonster = *iter;
			if ( NULL == pBlockMonster )
			{
				D_ERROR( "PlayerMoveNewBlockNotify,块(%d,%d)上的玩物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}

#ifdef ITEM_NPC
			if( !pBlockMonster->IsItemNpc() )
			{
#endif //ITEM_NPC
				
				//3.通知本玩家block怪物信息与怪物移动信息；
				pMovePlayer->SendPkg<MGMonsterInfo>( pBlockMonster->GetMonsterInfo() );

				if( pBlockMonster->IsHaveBuff() )
				{
					monsterBuff.buffData = pBlockMonster->GetBuffData();
					monsterBuff.targetID.dwPID = pBlockMonster->GetMonsterID();
					monsterBuff.targetID.wGID = MONSTER_GID;
					pMovePlayer->SendPkg<MGBuffData>( &monsterBuff );
				}


				//D_DEBUG("探测到怪物 ID:%d 位置(%d,%d)\n", monsterMsg.lMonsterID, monsterMsg.nMapPosX, monsterMsg.nMapPosY );
				//4.通知block怪物本玩家信息与移动信息；
				//if( pMovePlayer->GetCurrentHP() != 0 )
				{
					//D_DEBUG( "AI匹配7，通知怪物%d，玩家%d进入\n", pBlockMonster->GetMonsterID(), pMovePlayer->GetFullID().dwPID );
					pBlockMonster->OnPlayerDetected( pMovePlayer );
				}			
#ifdef ITEM_NPC
			}
#endif //ITEM_NPC
		}
	}

	if ( !(blockRideTeams.empty()) )
	{
		NoticeRideTeamAppear( pMovePlayer );
	}

	if ( !(blockItemPkgs.empty()) )
	{
		NoticeItemPkgsAppear( pMovePlayer );
	}

	return;
}

///玩家移动状态改变通知块内怪物
void MuxMapBlock::PlayerMovChgBlockMonsterNotify( CPlayer* pMovChgPlayer )
{
	if ( NULL == pMovChgPlayer )
	{
		D_WARNING( "PlayerMovChgBlockMonsterNotify，输入pMovChgPlayer指针空" );
		return;
	}

	//1.通知块上怪物本玩家离开；
	CMonster* pBlockMonster = NULL;//块上怪物；
	if ( !(blockMonsters.empty()) )
	{
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pBlockMonster = *iter;
			if ( NULL == pBlockMonster )
			{
				D_ERROR( "PlayerMovChgBlockMonsterNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}
			pBlockMonster->OnSurPlayerMovChg( pMovChgPlayer );
		}
	}
}


///玩家死亡影响到该块，相关通知;
void MuxMapBlock::PlayerDieBlockNotify( CPlayer* pDiePlayer )
{
	if ( NULL == pDiePlayer )
	{
		D_WARNING( "PlayerDieBlockNotify，输入pDiePlayer指针空" );
		return;
	}

	//1.通知块上怪物本玩家离开；
	CMonster* pBlockMonster = NULL;//块上怪物；
	if ( !(blockMonsters.empty()) )
	{
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pBlockMonster = *iter;
			if ( NULL == pBlockMonster )
			{
				D_ERROR( "PlayerDieBlockNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}
			////通知block怪物本玩家离开信息,不需要？由怪物AI自己去计算，或者还是这里通知，省去AI的计算量？
			//D_DEBUG( "AI匹配2，通知怪物%d，玩家%d离开\n", pBlockMonster->GetMonsterID(), pDiePlayer->GetFullID().dwPID );
			//pBlockMonster->OnPlayerLeaveView( pDiePlayer, AILR_DIE );
			pBlockMonster->OnPlayerDie( pDiePlayer );
		}
	}
}

///玩家离开影响到该块，相关通知；
void MuxMapBlock::PlayerLeaveBlockNotify( CPlayer* pLeavePlayer )
{
	if ( !pLeavePlayer )
		return;


	//1.通知块上怪物本玩家离开；
	CMonster* pBlockMonster = NULL;//块上怪物；
	if ( !(blockMonsters.empty()) )
	{
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pBlockMonster = *iter;
			if ( NULL == pBlockMonster )
			{
				D_ERROR( "PlayerLeaveBlockNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}
			//通知block怪物本玩家离开信息,不需要？由怪物AI自己去计算，或者还是这里通知，省去AI的计算量？
			//D_DEBUG( "AI匹配3，通知怪物%d，玩家%d离开\n", pBlockMonster->GetMonsterID(), pLeavePlayer->GetFullID().dwPID );

			pBlockMonster->OnPlayerLeaveView( pLeavePlayer, AILR_LEAVEMAP );
		}
	}

//#ifndef GROUP_NOTI
//	//2.通知块上玩家本玩家离开;
//	if ( !(blockPlayers.empty()) )
//	{
//		MGPlayerLeave returnMsg;
//		returnMsg.lPlayerID = pLeavePlayer->GetFullID();
//		returnMsg.lMapCode = pLeavePlayer->GetMapID();
//		CPlayer* pBlockPlayer = NULL;
//		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
//		{
//			pBlockPlayer = *iter;
//			if ( NULL == pBlockPlayer )
//			{
//				D_ERROR( "PlayerLeaveBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
//				continue;
//			}
//			//通知block玩家本玩家离开；
//			if ( pBlockPlayer != pLeavePlayer )//不通知自身离开;
//			{
//				pBlockPlayer->SendPkg<MGPlayerLeave>( &returnMsg );
//			}
//		}
//	}
//#endif //GROUP_NOTI

}

///玩家移动离开视野块，相关通知；
void MuxMapBlock::PlayerMoveOrgBlockNotify( CPlayer* pMovePlayer )
{
	if ( NULL == pMovePlayer )
	{
		D_ERROR( "PlayerMoveOrgBlockNotify，输入玩家指针空\n" );
		return;
	}

	//通知块上怪物玩家离开；
	CMonster* pBlockMonster = NULL;//块上怪物；
	if ( !(blockMonsters.empty()) )
	{
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pBlockMonster = *iter;
			if ( NULL == pBlockMonster )
			{
				D_ERROR( "PlayerMoveOrgBlockNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}
			//D_DEBUG( "AI匹配4，通知怪物%d，玩家%d离开\n", pBlockMonster->GetMonsterID(), pMovePlayer->GetFullID().dwPID );
			pBlockMonster->OnPlayerLeaveView( pMovePlayer, AILR_LEAVEVIEW );
		}
	}

	////2.通知块上玩家本玩家移出视野,通知本玩家块上玩家移出视野，
	//if ( !(blockPlayers.empty()) )
	//{
	//	MGLeaveView returnMsg;
	//	returnMsg.leaveMonsterID = 0;
	//	returnMsg.leavePlayerID = pMovePlayer->GetFullID();
	//	CPlayer* pBlockPlayer = NULL;
	//	MGLeaveView blockplayerLeave;
	//	blockplayerLeave.leaveMonsterID = 0;
	//	for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
	//	{
	//		pBlockPlayer = *iter;

	//		//1.通知block玩家本玩家移出视野；
	//		pBlockPlayer->SendPkg<MGLeaveView>( &returnMsg );

	//		//2.通知本玩家块上玩家移出视野；
	//		blockplayerLeave.leavePlayerID = pBlockPlayer->GetFullID();
	//		pMovePlayer->SendPkg<MGLeaveView>( &blockplayerLeave);
	//	}
	//}

	////3.通知本玩家块上怪物移出视野;
	//if ( !(blockMonsters.empty()) )
	//{
	//	MGLeaveView monsterLeave;
	//	monsterLeave.leavePlayerID.wGID  = 0;
	//	monsterLeave.leavePlayerID.dwPID = 0;
	//	CMonster* pBlockMonster = NULL;
	//	for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
	//	{
	//		pBlockMonster = *iter;
	//		monsterLeave.leaveMonsterID = pBlockMonster->GetMonsterID();
	//		pMovePlayer->SendPkg<MGLeaveView>( &monsterLeave );
	//	}
	//}


	////玩家移动离开视野时的回调函数
	//CRideState& rideState = pMovePlayer->GetSelfStateManager().GetRideState();
	////仅当骑乘状态激活时，且玩家时队长时
	//if ( rideState.IsActive() &&  rideState.GetPlayerRideType() == CRideState::E_LEADER )
	//{
	//	RideTeam* pTeam = rideState.GetRideTeam();
	//	if ( !pTeam)
	//	{
	//		D_ERROR("出错,处于骑乘状态但是竟然找不到骑乘队伍指针\n");
	//		return;
	//	}

	//	CPlayer* pMember = pTeam->GetRideMember();
	//	if ( pMember )
	//		PlayerMoveOrgBlockNotify( pMember );
	//}
	//pMovePlayer->OnMoveOutOrgBlockNotify( this );
}

///玩家出现影响到该格，相关通知；
void MuxMapBlock::PlayerAppearBlockNotify( CPlayer* pAppearPlayer, bool isNotiSelf/*是否通知出现者本人*/ )
{
	if ( NULL == pAppearPlayer )
	{
		D_ERROR( "PlayerAppearBlockNotify，输入玩家指针空\n" );
		return;
	}

	CPlayer* pBlockPlayer = NULL;//块上玩家；
	CMonster* pBlockMonster = NULL;//块上怪物；

//#ifndef GROUP_NOTI
//	if ( !(blockPlayers.empty()) )
//	{
//		//通知块上玩家本玩家信息；
//		MGOtherPlayerInfo* movPlayerInfo = NULL;
//		movPlayerInfo = pAppearPlayer->GetOtherPlayerInfo();
//		
//		MGUnionMemberShowInfo *pMovePlayerShowInfo = NULL;
//		CUnion *pMovePlayerUnion = pAppearPlayer->Union();
//		if(NULL != pMovePlayerUnion)
//		{
//			pMovePlayerShowInfo = pMovePlayerUnion->GetUnionMemberShowInfo(pAppearPlayer);
//		}
//
//		MGBuffData moveBuff;
//		if( pAppearPlayer->IsHaveBuff() )
//		{
//			moveBuff.buffData = (*pAppearPlayer->GetBuffData());
//			moveBuff.targetID = pAppearPlayer->GetFullID();
//		}
//
//		MGPetinfoStable* movePetInfo = NULL;
//		if( pAppearPlayer->IsSummonPet() )
//		{
//			movePetInfo = pAppearPlayer->GetSurPetNotiInfo();
//		}
//
//		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
//		{
//			pBlockPlayer = *iter;
//			if ( NULL == pBlockPlayer )
//			{
//				D_ERROR( "PlayerAppearBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
//				continue;
//			}
//
//			if ( !isNotiSelf )
//			{
//				//不通知出现者本人；
//				if ( pBlockPlayer == pAppearPlayer )
//				{
//					continue;
//				}
//			}
//			//通知block玩家本玩家信息；
//			if ( NULL != movPlayerInfo )
//			{
//				pBlockPlayer->SendPkg<MGOtherPlayerInfo>( movPlayerInfo );
//			}
//
//			if ( NULL != pMovePlayerUnion )
//			{
//				pBlockPlayer->SendPkg<MGUnionMemberShowInfo>(pMovePlayerShowInfo);
//			}
//
//			if( pAppearPlayer->IsHaveBuff() )
//			{
//				pBlockPlayer->SendPkg<MGBuffData>( &moveBuff );
//			}
//
//			if( pAppearPlayer->IsSummonPet() )
//			{
//				if ( NULL != movePetInfo )
//				{
//					pBlockPlayer->SendPkg< MGPetinfoStable >( movePetInfo );
//				}
//			}
//
//		}
//	}
//#endif //GROUP_NOTI

	if ( !(blockPlayers.empty()) )
	{
		MGBuffData blockBuff;
		MGPetinfoStable* blockPetInfo = NULL;
		MGSimplePlayerInfo* blockPlayerInfo = NULL;

		for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
		{
			pBlockPlayer = *iter;
			if ( NULL == pBlockPlayer )
			{
				D_ERROR( "PlayerAppearBlockNotify,块(%d,%d)上的玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}

			if ( !isNotiSelf )
			{
				//不通知出现者本人；
				if ( pBlockPlayer == pAppearPlayer )
				{
					continue;
				}
			}

			//通知本玩家block玩家信息；	
			blockPlayerInfo = pBlockPlayer->GetSimplePlayerInfo();
			if( NULL != blockPlayerInfo )
			{
				pAppearPlayer->SendPkg<MGSimplePlayerInfo>( blockPlayerInfo );
			}

			CUnion *pBlockPlayerUnion = pBlockPlayer->Union();
			if(NULL != pBlockPlayerUnion)
			{
				MGUnionMemberShowInfo *pBlockPlayerShowInfo = pBlockPlayerUnion->GetUnionMemberShowInfo(pBlockPlayer);
				pAppearPlayer->SendPkg<MGUnionMemberShowInfo>(pBlockPlayerShowInfo);
			}

			if( pBlockPlayer->IsHaveBuff() )
			{
				blockBuff.buffData = (*pBlockPlayer->GetBuffData());
				blockBuff.targetID = pBlockPlayer->GetFullID();
				pAppearPlayer->SendPkg<MGBuffData>( &blockBuff );
			}

			if( pBlockPlayer->IsSummonPet() )
			{
				blockPetInfo = pBlockPlayer->GetSurPetNotiInfo();
				if ( NULL != blockPetInfo )
				{
					pAppearPlayer->SendPkg< MGPetinfoStable >( blockPetInfo ); 
				}				
			}
		}
	}

	if ( !(blockMonsters.empty()) )
	{
		//D_DEBUG( "进入块块(%d,%d)遍历怪物\n", GetBlockX(), GetBlockY() );
		
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			pBlockMonster = *iter;
			if ( NULL == pBlockMonster )
			{
				D_ERROR( "PlayerAppearBlockNotify,块(%d,%d)上的怪物列表元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
			if ( pBlockMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pBlockMonster->IsToDel() )
			{
				///怪物待删；
				continue;
			}

#ifdef ITEM_NPC
			if( !pBlockMonster->IsItemNpc() ) //不是物件NPC
			{
#endif //ITEM_NPC
				
				pAppearPlayer->SendPkg<MGMonsterInfo>( pBlockMonster->GetMonsterInfo() );
				if( pBlockMonster->GetClsID() == 120400 )
				{
					D_INFO("%s进入块 通知了怪物ClsID=%d\n", pAppearPlayer->GetNickName(), pBlockMonster->GetClsID() );
				}

				if( pBlockMonster->IsHaveBuff() )
				{
					MGBuffData srvmsg;
					srvmsg.buffData = pBlockMonster->GetBuffData();
					srvmsg.targetID.dwPID = pBlockMonster->GetMonsterID();
					srvmsg.targetID.wGID = MONSTER_GID;
					pAppearPlayer->SendPkg<MGBuffData>( &srvmsg );
				}

				//D_INFO("appear notify发送怪物信息 ID:%d\n", pBlockMonster->GetMonsterID() );
				//通知block怪物本玩家信息；
				//if( pAppearPlayer->GetCurrentHP() != 0 )
				{
					//D_DEBUG( "AI匹配8，通知怪物%d，玩家%d进入\n", pBlockMonster->GetMonsterID(), pAppearPlayer->GetFullID().dwPID );
					pBlockMonster->OnPlayerDetected( pAppearPlayer );
				}
#ifdef ITEM_NPC
			}
#endif //ITEM_NPC
		}
	}

	if ( !(blockRideTeams.empty()) )
	{
		NoticeRideTeamAppear( pAppearPlayer );
	}

	if ( !(blockItemPkgs.empty()) )
	{
		NoticeItemPkgsAppear( pAppearPlayer );
	}

	return;
}

///////////////////////////////////////////////////////////////
//原MuxMapBlock成员，现搬过来做MapBlock成员

///玩家加入块
void MuxMapBlock::AddPlayerToBlock(CPlayer* pPlayer)
{
	if ( NULL == pPlayer )
	{
		return;
	}

	//,???,邓子建,移动日志，D_DEBUG( "玩家%s加入块(%d,%d)\n", pPlayer->GetAccount(), blockX, blockY );
	//D_DEBUG( "玩家%s加入块(%d,%d)\n", pPlayer->GetAccount(), GetBlockX(), GetBlockY() );
	blockPlayers.push_back(pPlayer);

	CRideState& rideState = pPlayer->GetSelfStateManager().GetRideState();
	//仅当骑乘状态激活时，且玩家时队长时
	if ( rideState.IsActive() &&  rideState.GetPlayerRideType() == CRideState::E_LEADER )
	{
		RideTeam* pTeam = rideState.GetRideTeam();
		if ( !pTeam)
		{
			D_ERROR("出错,处于骑乘状态但是竟然找不到骑乘队伍指针\n");
			return;
		}

		//从该块删除
		AddRideTeam( pTeam );
	}
}

///玩家从块上删去
bool MuxMapBlock::DeletePlayerFromBlock(CPlayer* pPlayer)
{
	TRY_BEGIN;

	if ( !pPlayer )
		return false;

	//D_DEBUG( "玩家%s从块(%d,%d)删除\n", pPlayer->GetAccount(), GetBlockX(), GetBlockY() );
	if ( blockPlayers.empty() )
	{
		D_ERROR("从块中删除玩家%s时，块上玩家列表空，此时玩家(%d,%d), 块(%d,%d)\n", pPlayer->GetAccount(), pPlayer->GetPosX(), pPlayer->GetPosY(), blockX, blockY );
		return false;
	}

	bool isFound = false;
	for( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter )
	{
		if(*iter == pPlayer)
		{
			isFound = true;
			blockPlayers.erase(iter);
			break;
		}
	}

	if ( !isFound )
	{
		D_ERROR("从块中删除时找不到玩家%s 此时玩家(%d,%d), 块(%d,%d)\n", pPlayer->GetAccount(), pPlayer->GetPosX(), pPlayer->GetPosY(), blockX, blockY );
		return false;
	}

	CRideState& rideState = pPlayer->GetSelfStateManager().GetRideState();
	//仅当骑乘状态激活时，且玩家时队长时
	if ( rideState.IsActive() &&  rideState.GetPlayerRideType() == CRideState::E_LEADER )
	{
		RideTeam* pTeam = rideState.GetRideTeam();
		if ( !pTeam)
		{
			D_ERROR("出错,处于骑乘状态但是竟然找不到骑乘队伍指针\n");
			return false;
		}
		//从该块删除
		DeleteRideTeam( pTeam );
	}

	return true;
	TRY_END;
	return false;
}

///怪物加入块
bool MuxMapBlock::AddMonsterToBlock(CMonster* pMonster)
{
	if ( NULL == pMonster )
	{
		D_ERROR( "MuxMapBlock::AddMonsterToBlock，块(%d,%d)，输入怪物空\n", GetBlockX(), GetBlockY() );
		return false;
	}

	blockMonsters.push_back(pMonster);
	//是特殊NPC
	if( pMonster->NeedNotiOtherMonster() )
	{
		specialNpcNum++;
	}
	return true;
}

///怪物从块上删去
/////////////////////////////////////////////////////////////
//copy...
bool MuxMapBlock::DeleteMonsterFromBlock(CMonster* pMonster)
{
	TRY_BEGIN;

	if ( NULL == pMonster )
	{
		D_ERROR( "MuxMapBlock::DeleteMonsterFromBlock, NULL == pMonster，块(%d,%d)\n", GetBlockX(), GetBlockY() );
		return false;
	}

	bool isDeleteSuccess = false;
	if ( !(blockMonsters.empty()) )
	{
		for( list<CMonster*>::iterator iter = blockMonsters.begin(); iter != blockMonsters.end(); ++iter )
		{
			if(*iter == pMonster)
			{
				//D_DEBUG( "从块(%d,%d)删去怪物%d\n", GetBlockX(), GetBlockY(), pMonster->GetMonsterID() );
				blockMonsters.erase(iter);
				isDeleteSuccess = true;
				break;
			}
		}
	}

	if( pMonster->NeedNotiOtherMonster() )
	{
		specialNpcNum--;
	}

	if( !isDeleteSuccess )
	{
		D_ERROR("从块中删除时找不到怪物 此时怪物(%d,%d), 块(%d,%d)\n", pMonster->GetPosX(), pMonster->GetPosY(), blockX, blockY );
	}

	return isDeleteSuccess;
	TRY_END;
	return false;
}


void MuxMapBlock::AddItemsPkg(CItemsPackage *itemsPkg)
{
	TRY_BEGIN;

	if ( NULL != itemsPkg )
	{
		blockItemPkgs.push_back( itemsPkg );
	}


	TRY_END;
}


//查找道具包的函数对象
class ItemPkgFindFunc:public std::binary_function< CItemsPackage *,int,bool >
{
public:
	bool operator()( CItemsPackage * pItemPkg, int pkgIndex ) const 
	{
		if ( !pItemPkg )
			return false;

		if ( pItemPkg->GetItemsPackageIndex() == (unsigned int)pkgIndex )
			return true;

		return false;
	}
};

//@brief:删除道具包
bool MuxMapBlock::DeleteItemsPkg(CItemsPackage *itemsPkg)
{
	TRY_BEGIN;

	if ( !itemsPkg )
		return false;

	int pgkIndex = itemsPkg->GetItemsPackageIndex();
	list<CItemsPackage *>::iterator lter = std::remove_if(
		blockItemPkgs.begin(),
		blockItemPkgs.end(),
		bind2nd( ItemPkgFindFunc(),pgkIndex )
		);

	blockItemPkgs.erase( lter, blockItemPkgs.end() );

	TRY_END;

	return true;

}

void MuxMapBlock::AddRideTeam(RideTeam* pRideTeam )
{
	TRY_BEGIN;

	if ( !pRideTeam )
		return;

	//D_INFO("(%d,%d)MapBlock加入骑乘队伍 %d\n",GetBlockX(),GetBlockY(), pRideTeam->GetRideTeamID() );

	blockRideTeams.push_back( pRideTeam );

	TRY_END;
}

class RideTeamFindFunc:public std::binary_function< RideTeam *,int,bool >
{
public:
	bool operator()( RideTeam * pRideTeam, int teamIndex ) const 
	{
		if ( !pRideTeam )
			return false;

		if ( pRideTeam->GetRideTeamID() == (unsigned long)teamIndex )
			return true;

		return false;
	}
};


void MuxMapBlock::DeleteRideTeam(RideTeam* pRideTeam )
{
	TRY_BEGIN;

	if ( !pRideTeam )
		return;

	unsigned long teamID = pRideTeam->GetRideTeamID();

	list<RideTeam*>::iterator iter = std::find_if( blockRideTeams.begin(),
		blockRideTeams.end(),
		bind2nd( RideTeamFindFunc(), teamID)
		);

	blockRideTeams.erase(iter,blockRideTeams.end());

	//D_INFO("(%d,%d)MapBlock移除骑乘队伍 %d\n",GetBlockX(),GetBlockY(),pRideTeam->GetRideTeamID() );

	TRY_END;
}



RideTeam* MuxMapBlock::GetRideTeamByID(long teamID)
{
	TRY_BEGIN;

	list<RideTeam*>::iterator iter = std::find_if( blockRideTeams.begin(),
		blockRideTeams.end(),
		bind2nd( RideTeamFindFunc(), teamID)
	);

	if ( iter != blockRideTeams.end() )
		return *iter;

	TRY_END;

	return NULL;
}

void MuxMapBlock::NoticeItemPkgsToSurroundPlayer( CItemsPackage* pItemPkg )
{
	if ( !pItemPkg )
		return;

	if ( !blockPlayers.empty() )
	{
		MGItemPackAppear pkgAppearMsg;
		pkgAppearMsg.px =  pItemPkg->GetItemPosX();
		pkgAppearMsg.py =  pItemPkg->GetItemPosY();
		pkgAppearMsg.pkgID = pItemPkg->GetItemsPackageIndex();
		pkgAppearMsg.dropOwnerID = pItemPkg->GetDropOwnerID();
		pkgAppearMsg.OwnerID = pItemPkg->GetOwner();
		
		//包裹的发光类型
		int itempkgType = (int)pItemPkg->GetItemPkgType();

		std::vector<CItemPkgEle>& itemEle = pItemPkg->GetItemPkgItemEleVec();
		for ( size_t i = 0; i<itemEle.size(); ++i)
		{
			pkgAppearMsg.itemInfo = itemEle[i].GetItemEleInfo();
			pkgAppearMsg.itemIndex = pItemPkg->FindItemIndex(itemEle[i]);
			for ( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter  )
			{
				CPlayer* pTmpPlayer  = *iter;
				if ( NULL == pTmpPlayer )
				{
					D_ERROR( "NoticeItemPkgsToSurroundPlayer，块(%d,%d)，块上玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
					continue;
				}

				//根据包裹的具体情况,来定义发送给客户端的包裹颜色
				if ( pItemPkg->IsPlayerCanPickItemPkg(pTmpPlayer) )
					pkgAppearMsg.type = (1<<16)|itempkgType;
				else
					pkgAppearMsg.type = itempkgType;

				//D_DEBUG("NoticeItemPkgsToSurroundPlayer %d包裹通知周围玩家%s 能否被拾取：%d\n" ,pItemPkg->GetItemsPackageIndex(),pTmpPlayer->GetAccount(),pkgAppearMsg.type   );
				pTmpPlayer->SendPkg<MGItemPackAppear>( &pkgAppearMsg );
			}
		}
	}
}

void MuxMapBlock::NoticeItemPkgFirstAppear(CItemsPackage* pItemPkg )
{
	if ( !pItemPkg )
		return;

	if ( !blockPlayers.empty() )
	{
		MGItemPackAppear pkgAppearMsg;
		pkgAppearMsg.px =  pItemPkg->GetItemPosX();
		pkgAppearMsg.py =  pItemPkg->GetItemPosY();
		pkgAppearMsg.pkgID = pItemPkg->GetItemsPackageIndex();
		pkgAppearMsg.dropOwnerID = pItemPkg->GetDropOwnerID();
		pkgAppearMsg.OwnerID = pItemPkg->GetOwner();

		//包裹的发光类型
		int itempkgType = (int)pItemPkg->GetItemPkgType();

		std::vector<CItemPkgEle>& itemEle = pItemPkg->GetItemPkgItemEleVec();
		for ( size_t i = 0; i<itemEle.size(); ++i)
		{
			pkgAppearMsg.itemInfo = itemEle[i].GetItemEleInfo();
			pkgAppearMsg.itemIndex = pItemPkg->FindItemIndex(itemEle[i]);
			for ( list<CPlayer*>::iterator iter = blockPlayers.begin(); iter != blockPlayers.end(); ++iter  )
			{
				CPlayer* pTmpPlayer  = *iter;
				if ( NULL == pTmpPlayer )
				{
					D_ERROR( "NoticeItemPkgsToSurroundPlayer，块(%d,%d)，块上玩家列表元素指针空\n", GetBlockX(), GetBlockY() );
					continue;
				}

				//根据包裹的具体情况,来定义发送给客户端的包裹颜色
				if ( pItemPkg->IsPlayerCanPickItemPkg(pTmpPlayer) )
					pkgAppearMsg.type = (1<<17)|(1<<16)|itempkgType;
				else
					pkgAppearMsg.type = (1<<17)|itempkgType;

				pTmpPlayer->SendPkg<MGItemPackAppear>( &pkgAppearMsg );
			}
		}
	}
}

//将该块所拥有的道具包信息通知玩家
void MuxMapBlock::NoticeItemPkgsAppear( CPlayer* pPlayer )
{
	TRY_BEGIN;

	if ( !pPlayer )
		return;

	if ( !blockItemPkgs.empty() )
	{
		for ( std::list<CItemsPackage *>::iterator iter = blockItemPkgs.begin();
			iter!= blockItemPkgs.end();
			++iter )
		{
			CItemsPackage* pItemPkg = *iter;
			if ( NULL == pItemPkg )
			{
				D_ERROR( "NoticeItemPkgsAppear，块(%d,%d)，blockItemPkgs元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}

			pItemPkg->NoticePlayerItemPkgAppear( pPlayer );
		}
	}

	TRY_END;
}

//将块所拥有的骑乘队伍信息通知玩家
void MuxMapBlock::NoticeRideTeamAppear( CPlayer* pPlayer )
{
	TRY_BEGIN;

	if ( !pPlayer )
		return;

	if( blockRideTeams.size() )
	{
		for(list<RideTeam*>::iterator iter = blockRideTeams.begin(); iter != blockRideTeams.end(); ++iter)
		{
			RideTeam* pTeam = *iter;
			if ( NULL == pTeam )
			{
				D_ERROR( "NoticeRideTeamAppear，块(%d,%d)，blockRideTeams元素指针空\n", GetBlockX(), GetBlockY() );
				continue;
			}
/*#ifdef _DEBUG
			D_DEBUG("MuxBlock:(%d,%d) 通知骑乘队伍%d 出现\n", GetBlockX(),GetBlockY(), pTeam->GetRideTeamID() );
#endif	*/	
			pTeam->NoticeRideTeamInfoToOtherPlayer( pPlayer);
		}
	} 

	TRY_END;
}

////////////////////////////////////////////////////////////////
//战斗宠添加删除...
//战斗宠添加至块；
bool MuxMapBlock::AddFightPetToBlock( CFightPet* fightPet )
{
	//vector<CFightPet*> blockFightPets;//该节点上的战斗宠
	if ( NULL == fightPet )
	{
		D_ERROR( "AddFightPetToBlock， NULL == fightPet\n" );
		return false;
	}

	//for debug, to delete later;
	for ( vector<CFightPet*>::iterator iter=blockFightPets.begin(); iter!=blockFightPets.end(); ++iter )
	{
		if ( *iter == fightPet )
		{
			D_ERROR( "AddFightPetToBlock，重复在块(%d,%d)中添加同一战斗宠%d\n", GetBlockX(), GetBlockY(), fightPet->GetFightPetID() );
			return false;
		}
	}

	blockFightPets.push_back( fightPet );

	return true;
}

//战斗宠从块删除；
bool MuxMapBlock::DelFightPetFromBlock( CFightPet* fightPet )
{
	if ( NULL == fightPet )
	{
		D_ERROR( "DelFightPetFromBlock, 块(%d,%d), NULL == fightPet\n", GetBlockX(), GetBlockY() );
		return false;
	}

	if ( blockFightPets.empty() )
	{
		D_ERROR( "DelFightPetFromBlock, 块(%d,%d), blockFightPets.empty()\n", GetBlockX(), GetBlockY() );
		return false;
	}

	bool isFound = false;
	for ( vector<CFightPet*>::iterator iter=blockFightPets.begin(); iter!=blockFightPets.end(); ++iter )
	{
		if ( *iter == fightPet )
		{
			isFound = true;
			//与最末元素对调然后删去最末元素；
			blockFightPets[iter-blockFightPets.begin()] = *(blockFightPets.rbegin());
			blockFightPets.pop_back();
			break;
		}
	}

	if ( !isFound )
	{
		D_ERROR( "DelFightPetFromBlock, 块(%d,%d), 块上找不到该战斗宠%d\n", GetBlockX(), GetBlockY(), fightPet->GetFightPetID() );
		return false;
	}

	return true;
}
//...战斗宠添加删除
////////////////////////////////////////////////////////////////



//原MuxMapBlock成员，现搬过来做MapBlock成员
///////////////////////////////////////////////////////////////


