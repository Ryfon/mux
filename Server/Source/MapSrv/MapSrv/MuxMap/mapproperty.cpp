﻿/**
* @file mapproperty.cpp
* @brief 定义地图属性
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: mapproperty.cpp
* 摘    要: 定义地图属性，将地图静态属性从地图动态信息中分离出来，for 副本;
* 作    者: dzj
* 开始日期: 2009.09.10
* 
*/

#include "mapproperty.h"
#include "XmlManager.h"

void PlayerCopyInfo::InitPlayerCopyInfo()
{
	m_isIssueSelfLeaveHF = false;
	m_isIssueSelfLeaveLF = false;
	m_IssueSelfLeaveTime = 0;
	m_isCurInCopy = false;
	m_isPCopy     = false;

	m_curScrollType = m_toUseScrollType = 0;
	//m_pCopy       = NULL;

	m_isCurCopyInfoSet = false;//当前副本信息是否已设；

	m_curCopyInfo.ClearCopyInfoEle();//当前所在副本信息,；
}

///清欲进副本与当前副本信息；
bool PlayerCopyInfo::ClearCopyInfo()
{
	if ( m_isCurCopyInfoSet )
	{
		m_curCopyInfo.ClearCopyInfoEle();
		return true;
	}

	return false;		
}

#ifdef HAS_PATH_VERIFIER
std::vector<CBlockQuadManager::BQData> CBlockQuadManager::bqVec;
bool CBlockQuadManager::InitBlockQuards(void)
{
	TRY_BEGIN;

	const char *pszFileName = "config/mapconfig/BlockQuadInfo.xml";
	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR("地图碰撞数据%s加载出错\n", pszFileName);
		return false;
	}

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("地图碰撞数据%s可能为空\n", pszFileName);
		return false;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(2, childElements);
	CElement *pElement = NULL;
	const char *pElementName= NULL;

	for(size_t i = 0; i < childElements.size(); i++)
	{	
		pElement = childElements[i];
		pElementName = pElement->Name();
		if( (pElementName != NULL) && (strncmp(pElementName, "Region", 6) == 0) )
		{
			BQData bq;
			bq.regionId = STRING_TO_NUM(pElement->GetAttr("ID"));
			bq.minX = STRING_TO_NUM(pElement->GetAttr("MinX"));
			bq.minY = STRING_TO_NUM(pElement->GetAttr("MinY"));
			bq.maxX = STRING_TO_NUM(pElement->GetAttr("MaxX"));
			bq.maxY = STRING_TO_NUM(pElement->GetAttr("MaxY"));

			bqVec.push_back(bq);
		}
	}

	return true;
	TRY_END;
	return false;
}

void CBlockQuadManager::GetBlockQuardByMapId(unsigned short mapId, std::vector<CBlockQuadManager::BQData> &bqs)
{
	for(size_t i = 0; i < bqVec.size(); i++)
	{
		if(bqVec[i].regionId/100 > mapId)
			break;
		if(bqVec[i].regionId/100 == mapId)
			bqs.push_back(bqVec[i]);
	}

	return;
}

CBlockQuadManager::BQData* CBlockQuadManager::GetBlockRanage(int regionId)
{
	std::vector<CBlockQuadManager::BQData> bqs;
	GetBlockQuardByMapId(regionId/100, bqs);
	
	size_t ssize = bqs.size();
	if(ssize > 0)
	{
		for(size_t i = 0; i < ssize; i++)
		{
			if(bqs[i].regionId == regionId)
				return &bqs[i];
		}
	}

	return NULL;
}
#endif/*HAS_PATH_VERIFIER*/


CMuxMapProperty::CMuxMapProperty() : m_ID(0), m_IsCopyMap(false), m_pMapScript(NULL), m_gridWidth(0), m_gridHeight(0)
                                , m_blockWidth(0), m_blockHeight(0), m_arrMapBaseInfo(NULL), m_arrMapDetailInfo(NULL)
{
#ifdef HAS_PATH_VERIFIER
	m_arrMapBaseInfoBackup = NULL;
	m_arrAccessReginMapInfo = NULL;
#endif

	for ( int i=0; i<ARRAY_SIZE(m_mapAreaFsmsArr); ++i )
	{
		m_mapAreaFsmsArr[i] = NULL;
	}

	m_vecNewMapRebirthPts.clear();//复活点配置，据此填充复活点;

	///本地图的可用复活点；
	m_vecAvailRebirthPt.clear();					//普通复活点
	m_vecAvailCityAttackRebirthPt.clear();		//攻城战进攻者所用复活点
	m_vecAvailCityDefendRebirthPt.clear();		//攻城战防守者所用复活点
};

CMuxMapProperty::~CMuxMapProperty() 
{
	if ( !(m_vecNewMapRebirthPts.empty()) )
	{
		for( vector< MapRebirthPt* >::iterator iter=m_vecNewMapRebirthPts.begin(); iter!=m_vecNewMapRebirthPts.end(); ++iter )
		{
			delete *iter; *iter = NULL;
		}
		m_vecNewMapRebirthPts.clear();
	}

	for ( vector< MuxPoint* >::iterator iter=m_vecAvailRebirthPt.begin(); iter!=m_vecAvailRebirthPt.end(); ++iter )
	{
		delete *iter; *iter = NULL;
	}
	m_vecAvailRebirthPt.clear();//复活点配置，据此填充复活点;

	for ( vector< MuxPoint* >::iterator iter=m_vecAvailCityAttackRebirthPt.begin(); iter!=m_vecAvailCityAttackRebirthPt.end(); ++iter )
	{
		delete *iter; *iter = NULL;
	}
	m_vecAvailCityAttackRebirthPt.clear();//复活点配置，据此填充复活点;

	for ( vector< MuxPoint* >::iterator iter=m_vecAvailCityDefendRebirthPt.begin(); iter!=m_vecAvailCityDefendRebirthPt.end(); ++iter )
	{
		delete *iter; *iter = NULL;
	}
	m_vecAvailCityDefendRebirthPt.clear();//复活点配置，据此填充复活点;

	if ( NULL != m_arrMapBaseInfo )
	{
		for( int y=0; y<m_gridHeight; ++y)
		{
			delete [] m_arrMapBaseInfo[y]; m_arrMapBaseInfo[y] = NULL;
		}
		delete [] m_arrMapBaseInfo; m_arrMapBaseInfo = NULL;
	}

	if ( NULL != m_arrMapDetailInfo )
	{
		for ( int y = 0 ; y<m_gridHeight; ++y )
		{
			delete[] m_arrMapDetailInfo[y];m_arrMapDetailInfo[y] = NULL;
		}
		delete[] m_arrMapDetailInfo;m_arrMapDetailInfo = NULL;
	}

#ifdef HAS_PATH_VERIFIER
	if ( NULL != m_arrMapBaseInfoBackup )
	{
		for ( int y = 0 ; y<m_gridHeight; ++y )
		{
			delete[] m_arrMapBaseInfoBackup[y];m_arrMapBaseInfoBackup[y] = NULL;
		}
		delete[] m_arrMapBaseInfoBackup;m_arrMapBaseInfoBackup = NULL;
	}

	if ( NULL != m_arrAccessReginMapInfo )
	{
		for ( int y = 0 ; y<m_gridHeight; ++y )
		{
			delete[] m_arrAccessReginMapInfo[y];m_arrAccessReginMapInfo[y] = NULL;
		}
		delete[] m_arrAccessReginMapInfo;m_arrAccessReginMapInfo = NULL;
	}
#endif/*HAS_PATH_VERIFIER*/
};

void CMuxMapProperty::SetToReadMapInfo(ToReadMapInfo& info)
{
	m_ToReadMapInfo.SetToReadMapInfo(info);
}
///从.map文件中读入地图基本信息；
bool CMuxMapProperty::ReadMapInfo(const char *pszFile)
{
	TRY_BEGIN;

	if ( NULL == pszFile )
	{
		return false;
	}
	FILE* fp = NULL;

	// @step0 打开文件
#ifdef ACE_WIN32
	errno_t openrst = fopen_s( &fp, pszFile, "rb" );
	if ( ( 0 != openrst ) || ( NULL == fp ) )
#else  //ACE_WIN32
	fp = fopen( pszFile,"rb" );
	if ( NULL == fp )
#endif //ACE_WIN32	
	{
		D_WARNING( "打开地图文件%s失败", pszFile );
		return false;
	}

	// @step1 写版本号
	unsigned long dwVersion = 0ul;
	fread( &dwVersion, sizeof( unsigned long ), 1, fp );
	
	// @step1.5 写地图编号
	fread( &m_ID, sizeof( unsigned long ), 1, fp );
	
	// @step2 写X,Y
	int nTotalX = 0,nTotalY = 0; //无所谓,服务器用不到
	fread( &nTotalX, sizeof( int ), 1, fp );
	fread( &nTotalY, sizeof( int ), 1, fp );

	// @step3 格子的数目,即地图宽高
	int tempWidth, tempHeight;//必须按int读；
	fread( &tempWidth, sizeof( int ), 1, fp );
	fread( &tempHeight, sizeof( int ), 1, fp );
	if ( (tempWidth>1024) || (tempWidth<=0)
		 || (tempHeight>1024) || (tempHeight<=0)
		 )
	{
		D_WARNING( "地图%s大小信息不正确，读地图失败", pszFile );
		fclose( fp );
		return false;
	}

	///copy map fix size: 256*256;
	if ( IsCopyMap() )
	{
		if ( ( tempWidth != 256 ) || ( tempHeight != 256 ) )
		{
			D_ERROR( "copy map %d error size (%d, %d)\n", m_ID, tempWidth, tempHeight );
			fclose( fp );
			return false;
		}
	}

	m_gridWidth        = (unsigned short)tempWidth;//保存地图的宽高；
	m_gridHeight       = (unsigned short)tempHeight;

	// @step4 写地形属性
	m_arrMapBaseInfo = NEW int*[m_gridHeight];
	for( unsigned short y=0; y<m_gridHeight; ++y)
	{
		m_arrMapBaseInfo[y] = NEW int[m_gridWidth];
	}

	m_arrMapDetailInfo = NEW int*[m_gridHeight];
	for ( unsigned short y = 0 ; y< m_gridHeight;++y)
	{
		m_arrMapDetailInfo[y] = NEW int[m_gridWidth];
	}

#ifdef HAS_PATH_VERIFIER
	m_arrMapBaseInfoBackup = NEW unsigned int*[m_gridHeight];
	for( unsigned short y=0; y<m_gridHeight; ++y)
	{
		m_arrMapBaseInfoBackup[y] = NEW unsigned int[m_gridWidth];
	}
#endif

	for( unsigned short y=0; y<m_gridHeight; ++y)
	{
		for ( unsigned short x=0; x<m_gridWidth; ++x )
		{
			fread( &m_arrMapBaseInfo[y][x], sizeof( int ), 1, fp );
			//地图文件中1表示障碍物，0为可通行；
#ifdef HAS_PATH_VERIFIER
			m_arrMapBaseInfoBackup[y][x] = (unsigned int)m_arrMapBaseInfo[y][x];
#endif

			//21 22位同时为1代表不可通行
			bool isDNTPASS = ( ( m_arrMapBaseInfo[y][x] & (1<<20) ) 
				             && ( m_arrMapBaseInfo[y][x] & (1<<21) ) );
			if( isDNTPASS )
			{
				m_arrMapBaseInfo[y][x] = PT_DNTPASS;
				m_arrMapDetailInfo[y][x] = 0;
			}
			else
			{
				unsigned int mapInfo     = m_arrMapBaseInfo[y][x];
				m_arrMapDetailInfo[y][x] = GetMapAreaInfo( mapInfo );
				m_arrMapBaseInfo[y][x]   = PT_CANPASS;
			}
		}

	}

	//计算地图包含的块数；
	m_blockWidth     = m_gridWidth / MAPBLOCK_SIZE + 1;
	m_blockHeight    = m_gridHeight / MAPBLOCK_SIZE + 1;

	// @step5 关闭文件
	fclose( fp );

#ifdef STAGE_MAP_SCRIPT
	LoadMapScript();//装入关卡地图；
#endif //STAGE_MAP_SCRIPT

#ifdef HAS_PATH_VERIFIER
	const char* oldStr= "terrainProperty";
	const char* newStr= "prePath";
	std::string accessRegionFile(pszFile);
	accessRegionFile.replace(accessRegionFile.find(oldStr), strlen(oldStr), newStr);
	ReadAcceRegionMapInfo(accessRegionFile.c_str());
#endif

	return true;
	TRY_END;
	return false;
}

///读相应区域信息；
int CMuxMapProperty::GetMapAreaInfo( int areaInfo )
{

#define  BLOCKID_MASK 25//blockID的值 
#define  PKAREA_MASK 1<<24 //是否可PK区域
#define  FIGHTAREA_MASK 1<<23 //是否是战斗区域
#define  DUNGEON_PREAREA_MASK 1<<22 //是否是副本前置区域

	int tmpAreaInfo = 0;

	//如果该区域编号属于区域任务编号 ( 0 ,50 )
	tmpAreaInfo |= areaInfo>>BLOCKID_MASK ;
	if ( tmpAreaInfo < ARRAY_SIZE(m_mapAreaFsmsArr)  && m_mapAreaFsmsArr[tmpAreaInfo] == NULL )
	{
		LoadAreaTaskScript(  tmpAreaInfo );
	}

	tmpAreaInfo = tmpAreaInfo << 16;
	bool isPKAREA = ((areaInfo & PKAREA_MASK) != 0)  ;
	if ( isPKAREA )
	{
		tmpAreaInfo |= 1;
	}

	bool isFIGHTAREA = (( areaInfo & FIGHTAREA_MASK) != 0);
	if ( isFIGHTAREA )
	{
		tmpAreaInfo |= 1<<1;
	}

	bool isDUNGEONAREA = ((areaInfo & DUNGEON_PREAREA_MASK) != 0);
	if ( isDUNGEONAREA )
	{
		tmpAreaInfo |= 1<<2;
	}
	return tmpAreaInfo;
}

///装载指定区域脚本；
bool CMuxMapProperty::LoadAreaTaskScript(unsigned int areaID)
{
	if ( areaID>=ARRAY_SIZE(m_mapAreaFsmsArr) || areaID==0 )
	{
		return false;
	}

	if ( m_mapAreaFsmsArr[areaID] == NULL )
	{
		CNormalScript* pScript = CNormalScriptManager::FindScripts( areaID );
		if ( pScript )
		{
			if ( pScript->IsAreaScript() )
			{
				m_mapAreaFsmsArr[areaID] = pScript;
			} else {
				D_ERROR( "LoadAreaTaskScript,脚本%d非区域脚本，实际类型%d\n", areaID, pScript->GetScriptType() );
			}
			
			return true;
		}
	}

	return false;
}

#ifdef STAGE_MAP_SCRIPT
///装入关卡地图；
void CMuxMapProperty::LoadMapScript()
{
	m_pMapScript = CMapScriptManager::FindScripts( GetPropertyMapNo() );
	return;
}
#endif //STAGE_MAP_SCRIPT

///添加一个本地图的复活点;
bool CMuxMapProperty::AddRebirthRgn( MapRebirthPt* pRebirthPt ) 
{ 
	if ( NULL == pRebirthPt )
	{
		return false;
	}

	if ( pRebirthPt->rptMapID != GetPropertyMapNo() )
	{
		return false;
	}

	m_vecNewMapRebirthPts.push_back( pRebirthPt );

	return true;
};

//初始化地图的复活点，目前只处理第一个复活区域； //现加入更多的复活点
bool CMuxMapProperty::FillRebirthPt() 
{ 
	if ( !(m_vecNewMapRebirthPts.empty()) )
	{
		for( size_t i = 0; i< m_vecNewMapRebirthPts.size(); ++i )
		{
			for ( int y=m_vecNewMapRebirthPts[i]->rptLeftTop.nPosY; y<m_vecNewMapRebirthPts[i]->rptRightBottom.nPosY; ++y )
			{
				for ( int x=m_vecNewMapRebirthPts[i]->rptLeftTop.nPosX; x<m_vecNewMapRebirthPts[i]->rptRightBottom.nPosX; ++x )
				{
					if ( IsGridCanPass(x,y) )
					{
						MuxPoint* pMuxPoint = NEW MuxPoint;
						if ( NULL == pMuxPoint )
						{
							D_ERROR( "InitRebirthPt，分配内存失败\n" );
							return false;
						}
						pMuxPoint->nPosX = x;
						pMuxPoint->nPosY = y;

						if( m_vecNewMapRebirthPts[i]->rptType == MRPT_COMMON )
						{
							m_vecAvailRebirthPt.push_back( pMuxPoint );
						}else if( m_vecNewMapRebirthPts[i]->rptType == MRPT_CITY )
						{
							m_vecAvailCityDefendRebirthPt.push_back( pMuxPoint );
						}else if( m_vecNewMapRebirthPts[i]->rptType == MRPT_FIELD )
						{
							m_vecAvailCityAttackRebirthPt.push_back( pMuxPoint );
						}
					}
				}
			}
		}
	} else {
		m_vecAvailRebirthPt.clear();
	}
	return true;
};

#ifdef HAS_PATH_VERIFIER
bool CMuxMapProperty::ReadAcceRegionMapInfo(const char *pszFile)
{
	TRY_BEGIN;

	if ( NULL == pszFile )
	{
		return false;
	}

	FILE* fp = NULL;

	// @step0 打开文件
#ifdef ACE_WIN32
	errno_t openrst = fopen_s( &fp, pszFile, "rb" );
	if ( ( 0 != openrst ) || ( NULL == fp ) )
#else  //ACE_WIN32
	fp = fopen( pszFile,"rb" );
	if ( NULL == fp )
#endif //ACE_WIN32	
	{
		D_WARNING( "打开地图区域文件%s失败\n", pszFile );
		return false;
	}

	// @step1 写版本号
	unsigned long dwVersion = 0ul;
	fread( &dwVersion, sizeof( unsigned long ), 1, fp );

	// @step1.5 写地图编号
	unsigned long mapID = 0;
	fread( &mapID, sizeof( unsigned long ), 1, fp );
	if(mapID != m_ID)
	{
		D_ERROR( "地图编号信息不一致(%d:%d)\n", mapID, m_ID );
		return false;
	}

	// @step2 写X,Y
	int nTotalX = 0,nTotalY = 0; //无所谓,服务器用不到
	fread( &nTotalX, sizeof( int ), 1, fp );
	fread( &nTotalY, sizeof( int ), 1, fp );

	// @step3 格子的数目,即地图宽高
	int tempWidth, tempHeight;//必须按int读；
	fread( &tempWidth, sizeof( int ), 1, fp );
	fread( &tempHeight, sizeof( int ), 1, fp );
	if ( (tempWidth>1024) || (tempWidth<=0) || (tempWidth != m_gridWidth)
		|| (tempHeight>1024) || (tempHeight<=0) ||(tempHeight != m_gridHeight)
		)
	{
		D_WARNING( "地图%s大小信息不正确，读地图失败", pszFile );
		fclose( fp );
		return false;
	}

	///copy map fix size: 256*256;
	if ( IsCopyMap() )
	{
		if ( ( tempWidth != 256 ) || ( tempHeight != 256 ) )
		{
			D_ERROR( "copy map %d error size (%d, %d)\n", m_ID, tempWidth, tempHeight );
			fclose( fp );
			return false;
		}
	}

	// @step4 写地形属性
	m_arrAccessReginMapInfo = NEW unsigned int*[tempHeight];
	for( unsigned short y=0; y<tempHeight; ++y)
	{
		m_arrAccessReginMapInfo[y] = NEW unsigned int[tempHeight];
	}

	for( unsigned short y=0; y<tempHeight; ++y)
	{
		for ( unsigned short x=0; x<tempWidth; ++x )
		{
			fread( &m_arrAccessReginMapInfo[y][x], sizeof( unsigned int ), 1, fp );
		}

	}

	// @step5 关闭文件
	fclose( fp );
	
	return true;
	TRY_END;
	return false;
}

void CMuxMapProperty::GetBackupAndAccessRegionInfo(unsigned int** &backupedBaseInfo, unsigned int** &accessRegionInfo)
{
	backupedBaseInfo = m_arrMapBaseInfoBackup;
	accessRegionInfo = m_arrAccessReginMapInfo;
	return;
}
#endif/*HAS_PATH_VERIFIER*/

///取普通复活点；
bool CMuxMapProperty::GetNormalRebirthPt( MuxPoint& rebirthPt )
{
	rebirthPt.nPosX = 0;
	rebirthPt.nPosY = 0;

	int availNum = (int)(m_vecAvailRebirthPt.size());
	if ( availNum <=0 )
	{
		D_ERROR( "CMuxMap::GetNormalRebirthPt, 地图%d没有可用的复活点\n",  m_ID);
		return false;
	}

	int pos = RAND % availNum;
	rebirthPt.nPosX = m_vecAvailRebirthPt[pos]->nPosX;
	rebirthPt.nPosY = m_vecAvailRebirthPt[pos]->nPosY;

	return true;
};

///取攻城攻击方复活点；
bool CMuxMapProperty::GetCityAttackRebirthPt( MuxPoint& rebirthPt )
{
	rebirthPt.nPosX = 0;
	rebirthPt.nPosY = 0;

	int availNum = (int)(m_vecAvailCityAttackRebirthPt.size());
	if ( availNum <=0 )
	{
		//如果没有配攻城复活点，则返回一个普通复活点；
		return GetNormalRebirthPt( rebirthPt );
	}

	int pos = RAND % availNum;
	rebirthPt.nPosX = m_vecAvailCityAttackRebirthPt[pos]->nPosX;
	rebirthPt.nPosY = m_vecAvailCityAttackRebirthPt[pos]->nPosY;

	return true;
};

///取攻城防守方复活点;
bool CMuxMapProperty::GetCityDefendRebirthPt( MuxPoint& rebirthPt )
{
	rebirthPt.nPosX = 0;
	rebirthPt.nPosY = 0;

	int availNum = (int)(m_vecAvailCityDefendRebirthPt.size());
	if ( availNum <=0 )
	{
		//如果没有配攻城复活点，则返回一个普通复活点；
		return GetNormalRebirthPt( rebirthPt );
	}

	int pos = RAND % availNum;
	rebirthPt.nPosX = m_vecAvailCityDefendRebirthPt[pos]->nPosX;
	rebirthPt.nPosY = m_vecAvailCityDefendRebirthPt[pos]->nPosY;

	return true;
};

bool CMuxMapProperty::GetWarRebirthPt( unsigned short &mapId, unsigned int &posX, unsigned int &posY )
{
	int availNum = (int)(m_vecAvailCityDefendRebirthPt.size());
	if ( availNum <=0 )
	{
		D_ERROR("地图%d没有配置攻城战复活点\n", m_ID);
		return false;
	}

	int pos = RAND % availNum;
	posX = m_vecAvailCityDefendRebirthPt[pos]->nPosX;
	posY = m_vecAvailCityDefendRebirthPt[pos]->nPosY;
	mapId = m_ID;

	return true;
}

 