﻿/**
* @file MapBlock.h
* @brief 定义地图小块
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MapBlock.h
* 摘    要: 定义地图小块
* 作    者: dzj
* 开始日期: 2008.07.02
* 
*/

#pragma once

#include "../Player/Player.h"
#include "../monster/monster.h"

#include "../../../Base/dscontainer/dshashtb.h"

#include "MapCommonHead.h"
#include "../BattleUtility.h"
#include "../Lua/MapScript.h"
#include "mapcomponent.h"

using namespace MuxMapSpace;

#define TOWER_RADIUS 3

class CBattleScript;

class CGroupNotiInfo
{
public:
	CGroupNotiInfo()
	{
		ResetGroupInfo();
	};
	~CGroupNotiInfo(){};

public:

#define MAX_GATE_NUM 32

public:
	//void DebugInnerID()
	//{
	//	for ( int i=0; i<MAX_GATE_NUM; ++i )
	//	{
	//		if ( m_GroupNotiGatePlayerID[i].nCurNum > 0 )
	//		{
	//			D_DEBUG( "本次通知数：%d，序号:%d\n", m_GroupNotiGatePlayerID[i].nCurNum, m_GroupNotiGatePlayerID[i].testseqID );
	//			for ( int j=0; j<m_GroupNotiGatePlayerID[i].nCurNum; ++j )
	//			{
	//				D_DEBUG( "内部玩家ID号:%d\n", m_GroupNotiGatePlayerID[i].arrPlayerPID[j] );
	//			}
	//		}
	//	}

	//	return;
	//}

	///通知各个gate组播消息,每次发完信息都必须重置通知群；；
	template <typename T_Msg>
	void NotiGroup( T_Msg* srvMsg )
	{
		TRY_BEGIN;

		if ( NULL == srvMsg )
		{
			D_ERROR( "NotiGroup，输入待发信息为空\n" );
			return;
		}

		for ( int i=0; i<MAX_GATE_NUM; ++i )
		{
			srvMsg->lNotiPlayerID.wGID = i;
			srvMsg->lNotiPlayerID.dwPID = 0;

			if ( i >= ARRAY_SIZE(m_GroupNotiGatePlayerID) )
			{
				D_ERROR( "NotiGroup, i(%d) >= ARRAY_SIZE(m_GroupNotiGatePlayerID)\n", i );
				return;
			}
			if ( m_GroupNotiGatePlayerID[i].nCurNum > 0 )
			{
				if ( (unsigned int)(m_GroupNotiGatePlayerID[i].nCurNum) > ARRAY_SIZE(m_GroupNotiGatePlayerID[i].arrPlayerPID) )
				{
					D_ERROR( "NotiGroup, 广播信息非法, wCmd:%x, nCurNum:%d, arrPlayerPID size:%d\n"
						, T_Msg::wCmd, m_GroupNotiGatePlayerID[i].nCurNum, ARRAY_SIZE(m_GroupNotiGatePlayerID[i].arrPlayerPID)  );
					return;
				}
				CGateSrv* pGateSrv = CManG_MProc::FindProc( i );
				if( NULL == pGateSrv )
				{
					D_ERROR( "NotiGroupPre 找不到GateSrv%d 不能发送消息%x\n", i, T_Msg::wCmd );
					continue;
				}
				MsgToPut* pIDMsg = CreateSrvMGTPkg( pGateSrv, m_GroupNotiGatePlayerID[i] );
				pGateSrv->SendPkgToGateServer( pIDMsg );//先通b知广播ID号；
				MsgToPut* pNewMsg = CreateSrvPkg( T_Msg, pGateSrv, *srvMsg );
				pGateSrv->SendPkgToGateServer( pNewMsg );//再通知广播内容；
			}
		}

		return;
		TRY_END;
		return;
	}

public:
	bool DebugIDIfAny()
	{
		for ( int i=0; i<MAX_GATE_NUM; ++i )
		{
			if ( i >= ARRAY_SIZE(m_GroupNotiGatePlayerID) )
			{
				D_ERROR( "DebugIDIfAny, i%d >= ARRAY_SIZE(m_GroupNotiGatePlayerID)\n", i );
				return false;
			}
			if ( m_GroupNotiGatePlayerID[i].nCurNum > 0 )
			{
				D_DEBUG( "DebugIDIfAny, 给gate%d的ID\n", i );
				for ( int j=0; j<m_GroupNotiGatePlayerID[i].nCurNum; ++j )
				{
					if ( j >= ARRAY_SIZE(m_GroupNotiGatePlayerID[i].arrPlayerPID) )
					{
						D_ERROR( "DebugIDIfAny, j%d >= ARRAY_SIZE(m_GroupNotiGatePlayerID[i].arrPlayerPID\n", j );
						return false;
					}
					D_DEBUG( "%d\n", m_GroupNotiGatePlayerID[i].arrPlayerPID[j] );
				}
				return true;
			}
		}
		return false;
	}

	void ResetGroupInfo()
	{
		for ( int i=0; i<MAX_GATE_NUM; ++i )
		{
			if ( i >= ARRAY_SIZE(m_GroupNotiGatePlayerID) )
			{
				D_ERROR( "ResetGroupInfo, i%d >= ARRAY_SIZE(m_GroupNotiGatePlayerID)\n", i );
				return;
			}
			m_GroupNotiGatePlayerID[i].nCurNum = 0;
			//++ (m_GroupNotiGatePlayerID[i].testseqID);
		}
		m_allPlayerIDVec.clear();
	}

	bool AddGroupNotiPlayerID( const PlayerID& inPlayerID )
	{
		USHORT 	wGID = inPlayerID.wGID;
		TYPE_ID dwPID = inPlayerID.dwPID;		
		if ( wGID >= MAX_GATE_NUM )
		{
			//不可能有这么多gate，错误的inPlayerID;
			D_ERROR( "AddGroupNotiPlayerID,不可能的wGID:%d\n", wGID );
			return false;
		}

		if ( wGID >= ARRAY_SIZE(m_GroupNotiGatePlayerID) )
		{
			D_ERROR( "AddGroupNotiPlayerID, wGID(%d) >= ARRAY_SIZE(m_GroupNotiGatePlayerID)\n", wGID );
			return false;
		}
		unsigned long m_CurPlayerNum = m_GroupNotiGatePlayerID[wGID].nCurNum;//该gate当前的通知人数；
		if ( m_CurPlayerNum >= MAX_NOTIPLAYER_NUM_PERGATE-1 )
		{
			//单Gate通知人数超限；
			D_ERROR( "AddGroupNotiPlayerID,当前通知人数%d已超限\n", m_CurPlayerNum );
			return false;
		}

		if ( m_CurPlayerNum >= ARRAY_SIZE(m_GroupNotiGatePlayerID[wGID].arrPlayerPID) )
		{
			D_ERROR( "AddGroupNotiPlayerID, m_curPlayerNum(%d) >= ARRAY_SIZE(m_GroupNotiGatePlayerID[wGID].arrPlayerPID)\n", m_CurPlayerNum );
			return false;
		}
		m_GroupNotiGatePlayerID[wGID].arrPlayerPID[m_CurPlayerNum] = dwPID;//存储待通知玩家的ID号；
		++(m_GroupNotiGatePlayerID[wGID].nCurNum);

		m_allPlayerIDVec.push_back( inPlayerID );

		return true;
	}

	const std::vector<PlayerID>& GetAllGroupPlayerIDVec() { return m_allPlayerIDVec; } 

private:
	///删除广播组内的某个通知对象，只用于副本，因为普通地图内每次通知完毕后都要清所有广播组，而副本人员相对固定，因此只在玩家进入/离开副本时修改广播组；
	friend class CMuxCopyInfo;
	bool DelGroupNotiPlayerID( const PlayerID& inPlayerID )
	{
		USHORT 	wGID = inPlayerID.wGID;
		TYPE_ID dwPID = inPlayerID.dwPID;		
		if ( wGID >= MAX_GATE_NUM )
		{
			//不可能有这么多gate，错误的inPlayerID;
			D_ERROR( "DelGroupNotiPlayerID,不可能的wGID:%d\n", wGID );
			return false;
		}

		if ( wGID >= ARRAY_SIZE(m_GroupNotiGatePlayerID) )
		{
			D_ERROR( "DelGroupNotiPlayerID, wGID(%d) >= ARRAY_SIZE(m_GroupNotiGatePlayerID)\n", wGID );
			return false;
		}
		unsigned long curPlayerNum = m_GroupNotiGatePlayerID[wGID].nCurNum;//该gate当前的通知人数；
		if ( curPlayerNum <= 0 )
		{
			D_ERROR( "DelGroupNotiPlayerID,当前通知列表空,wGID:%d\n", wGID );
			return false;
		}

		if ( curPlayerNum >= MAX_NOTIPLAYER_NUM_PERGATE-1 )
		{
			//单Gate通知人数超限；
			D_ERROR( "DelGroupNotiPlayerID,不可能错误，当前通知人数%d已超限\n", curPlayerNum );
			return false;
		}

		bool isFound = false;
		for ( unsigned int i=0; i<curPlayerNum; ++i )
		{
			if ( i >= ARRAY_SIZE(m_GroupNotiGatePlayerID[wGID].arrPlayerPID) )
			{
				D_ERROR( "DelGroupNotiPlayerID, i(%d) >= ARRAY_SIZE(m_GroupNotiGatePlayerID[wGID].arrPlayerPID)\n", i );
				return false;
			}
			if ( dwPID == m_GroupNotiGatePlayerID[wGID].arrPlayerPID[i] )
			{
				//找到了待删playerID;
				isFound = true;
				//最末pid放入当前pid，然后把最后id删去；
				if ( curPlayerNum-1 >= ARRAY_SIZE(m_GroupNotiGatePlayerID[wGID].arrPlayerPID) )
				{
					D_ERROR( "DelGroupNotiPlayerID, curPlayerNum(%d)-1 >= ARRAY_SIZE(m_GroupNotiGatePlayerID[wGID].arrPlayerPID)\n", curPlayerNum );
					return false;
				}
				m_GroupNotiGatePlayerID[wGID].arrPlayerPID[i] = m_GroupNotiGatePlayerID[wGID].arrPlayerPID[curPlayerNum-1];
				--m_GroupNotiGatePlayerID[wGID].nCurNum;
				break;
			}
		}
		
		for ( std::vector<PlayerID>::iterator lter = m_allPlayerIDVec.begin()
			  ;lter!= m_allPlayerIDVec.end(); 
			  lter++ )
		{
			PlayerID& tmpID = *lter;
			if ( ( tmpID.dwPID == inPlayerID.dwPID ) && ( tmpID.wGID == inPlayerID.wGID ) )
			{
				m_allPlayerIDVec.erase( lter );
				break;
			}
		}

		return isFound;
	}

private:	 
	///各gate分到的通知玩家ID号，由于wGID在各个gate中都一致，因此只存储dwPID;
	MGBroadCast   m_GroupNotiGatePlayerID[MAX_GATE_NUM];
	std::vector<PlayerID> m_allPlayerIDVec;
};


#ifdef ITEM_NPC 
struct MapItemNpc
{
	unsigned int itemNpcSeq;		//序列号
	unsigned int initStatus;		//初始状态
	unsigned int curStatus;			//当前状态
	unsigned int resMonsterId;		//对应的怪物ID

	MapItemNpc():itemNpcSeq(0),initStatus(0),curStatus(0),resMonsterId(0)
	{}
};

bool operator <(const MapItemNpc& itemNpc1, const MapItemNpc& itemNpc2 );

#endif //ITEM_NPC

struct MuxMapBlock
{
public:
	MuxMapBlock(){};

	///初始化，新建块时调用；
	void InitMapBlock( unsigned short inBlockX, unsigned short inBlockY )
	{
		PoolObjInit();
		blockX = inBlockX;
		blockY = inBlockY;
	}

	~MuxMapBlock()
	{
		PoolObjInit();
	}

	unsigned short GetBlockX(){ return blockX; }
	unsigned short GetBlockY(){ return blockY; }

private:
	unsigned short blockX; //块在地图中的位置X；
	unsigned short blockY; //块在地图中的位置Y；

public:
	///根据块上玩家信息填充广播对象；
    bool FillGroupInfoWithSelfPlayer( CGroupNotiInfo& groupNotiInfo, bool& isSomeOneAdd/*是否添加了通知对象*/ );

	///根据块上玩家信息填充广播对象，但不通知特定人物；
    bool FillGroupInfoWithSelfPlayerExcept( CGroupNotiInfo& groupNotiInfo, const PlayerID& exceptID, bool& isSomeOneAdd );

	//根据块上玩家信息填充广播对象，但不通知组队
	bool FillGroupInfoWithSelfRideTeamExecpt( CGroupNotiInfo& groupNotiInfo, const PlayerID& exceptTeamLeaderID, const PlayerID& exceptTeamAssistID, bool& isSomeOneAdd );

private:
	////暂时不用集聚通知, by dzj, 09.05.08,static MGOtherPlayerInfo m_arrBlockPlayerInfo[100];
	bool           m_bConcenNotiSelfBlock;//集聚通知，是否通知本块内的玩家；
	unsigned int   m_concentLastNotiBlock;//集聚通知，上次通知的块标识；
	ACE_Time_Value m_concentLastNotiTime;//集聚通知，上次通知的时间（通知时间间隔自动调整）；

public:
	void PlayerMovChgBlockMonsterNotify( CPlayer* pMovChgPlayer );

	///玩家死亡影响到该块，相关通知;
	void PlayerDieBlockNotify( CPlayer* pDiePlayer );

	///玩家离开影响到该块，相关通知；
	void PlayerLeaveBlockNotify( CPlayer* pLeavePlayer );

	///玩家出现影响到该格，相关通知；
    void PlayerAppearBlockNotify( CPlayer* pAppearPlayer, bool isNotiAppearer/*是否通知出现者本人*/ );

	///玩家移动新进入视野块，相关通知；
	void PlayerMoveNewBlockNotify( CPlayer* pMovePlayer );

	///移动玩家跨块时通知玩家自身其周围玩家的信息(但暂时不通知该玩家周边该玩家的移动，因为在SRV定时刷玩家位置时已通知过了)；
	void PlayerBeyondBlockSelfNotify( CPlayer* pBeyondPlayer );

	///玩家移动离开视野块，相关通知；
	void PlayerMoveOrgBlockNotify( CPlayer* pMovePlayer );

	///怪物移动新进入视野块，相关通知；
	void MonsterMoveNewBlockNotify( CMonster* pMoveMonster );

	///怪物移动离开视野块，相关通知；
	void MonsterMoveOrgBlockNotify( CMonster* pMoveMonster );

	///怪物出生影响块，相关通知；
    void MonsterBornBlockNotify( CMonster* pBornMonster );

	void MonsterLeaveViewNotify( CMonster* pLeaveViewMonster );

	//获取该块与某点在一定范围内的对象（包括人和怪物）
	void PtPlayerGetSkillTarget( TYPE_POS posX, TYPE_POS posY, unsigned int range, CPlayer* pUseSkillPlayer, CBattleScript* pScript, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster );
	void PtMonsterGetSkillTarget( TYPE_POS posX, TYPE_POS posY, unsigned int range, CMonster* pUseSkillMonster, vector<CPlayer*>& vecPlayer, vector<CMonster*> vecMonster );
	void PtPlayerSkillGetTeamTarget( TYPE_POS posX, TYPE_POS posY, unsigned int range, CPlayer* pUseSkillPlayer, CBattleScript* pScript, vector<CPlayer*>& vecPlayer );

	void PtGetScopeCreature( TYPE_POS posX, TYPE_POS posY, int range, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster );
	void NotifyMonsterAddHpEvent( const PlayerID& addStarter, const PlayerID& addTarget, unsigned int addVal );

	//检查地图上某点是否可以添加塔
	bool IsEnabelAddTower(TYPE_POS gridX, TYPE_POS gridY);

	//块内且处于指定矩形区域内的玩家列表
	void GetPlayersInRect(TYPE_POS gridLeft, TYPE_POS gridTop, TYPE_POS gridRight, TYPE_POS gridBottom, vector<CPlayer *>& vecPlayer);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//原MuxMapBlock成员...
public:
	PoolFlagDefine()
	{
		blockX = 0u;
		blockY = 0u;
		blockPlayers.clear();
		blockMonsters.clear();
		specialNpcNum = 0;
		blockRideTeams.clear();
		blockItemPkgs.clear();
		m_bConcenNotiSelfBlock = false;//集聚通知，不通知本块玩家；
		m_concentLastNotiBlock = 0u;//集聚通知，上次通知的玩家位置；
	}

public:
	inline unsigned int GetBlockPlayerNum() { return (unsigned int)(blockPlayers.size()); }
	inline unsigned int GetBlockMonsterNum() { return (unsigned int)(blockMonsters.size()); };
	///玩家加入块
	void AddPlayerToBlock( CPlayer* pPlayer );
	///玩家从块上删去
	bool DeletePlayerFromBlock( CPlayer* pPlayer );
	///怪物加入块
	bool AddMonsterToBlock(CMonster* pMonster);
	///怪物从块上删去
	bool DeleteMonsterFromBlock(CMonster* pMonster);

	////////////////////////////////////////////////////////////////
	///@brief:添加道具包 2008.05.02
	void AddItemsPkg( CItemsPackage* itemsPkg ); 
	///@brief:删除道具包
	bool DeleteItemsPkg( CItemsPackage* itemsPkg );
	//在该节点加入骑乘队伍
	void AddRideTeam( RideTeam* pRideTeam );
	//在该节点删除骑乘队伍
	void DeleteRideTeam( RideTeam* pRideTeam );
	//依据ID号获取骑乘队伍
	RideTeam* GetRideTeamByID( long teamID );
	//通知骑乘队伍出现
	void NoticeRideTeamAppear( CPlayer* pPlayer );
	//将该包裹存在的信息通知给包裹周围的玩家
	void NoticeItemPkgsToSurroundPlayer( CItemsPackage* pItemPkg );
	//将该BLOCK中的包裹数据通知给该玩家
	void NoticeItemPkgsAppear( CPlayer* pPlayer );
	//包裹掉落的第一出现
	void NoticeItemPkgFirstAppear( CItemsPackage* pItemPkg );
	////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////
	//战斗宠添加删除...
	//战斗宠添加至块(激活或随玩家出现，移动进入)；
	bool AddFightPetToBlock( CFightPet* fightPet );
	//战斗宠从块删除(收回或随玩家消息，移动离开)；
	bool DelFightPetFromBlock( CFightPet* fightPet );
	//...战斗宠添加删除
	////////////////////////////////////////////////////////////////

public:
	///属于副本的地图块时钟;
	void BlockCopyTimer();

private:
	list<CPlayer*>  blockPlayers;   ///该节点的玩家列表
	list<CMonster*> blockMonsters;	 ///该节点的怪物列表
	int specialNpcNum;
	list<RideTeam*> blockRideTeams; ///该节点上的骑乘队伍
	list<CItemsPackage*> blockItemPkgs;///该节点的所有道具包

	vector<CFightPet*> blockFightPets;//该节点上的战斗宠
	//原MuxMapBlock成员...
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
};

#define  MAX_AREATASK_COUNT 50

///无生成器怪物补充请求；
struct NoCreatorNpcSupplyInfo
{
private:
	NoCreatorNpcSupplyInfo();

public:
	NoCreatorNpcSupplyInfo( unsigned long inCallerID, unsigned long inNpcType, TYPE_POS inPosX, TYPE_POS inPosY, const PlayerID& inRewardID, unsigned int inAIFlag ) : callerID(inCallerID),
		npcType(inNpcType),
		posX(inPosX),
		posY(inPosY),
		rewardID( inRewardID ), 
		aiFlag( inAIFlag )
		{};

public:
	inline unsigned long GetCallerID() { return callerID; };
	inline unsigned long GetNpcType() { return npcType; };
	inline TYPE_POS GetPosX() { return posX; };
	inline TYPE_POS GetPosY() { return posY; };
	inline unsigned int GetAiFlag() { return aiFlag; }
	const PlayerID& GetRewardID() { return rewardID; }

private:
	unsigned long callerID;
	unsigned long npcType;
	TYPE_POS posX;
	TYPE_POS posY;
	unsigned int aiFlag;
	PlayerID rewardID;
};



