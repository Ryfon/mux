﻿/**
* @file mapproperty.h
* @brief 定义地图属性
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: mapproperty.h
* 摘    要: 定义地图属性，将地图静态属性从地图动态信息中分离出来，for 副本;
* 作    者: dzj
* 开始日期: 2009.09.10
* 
*/

#pragma once

#ifndef MAP_PROPERTY_H
#define MAP_PROPERTY_H

#include "MapCommonHead.h"
#include "../Lua/MapScript.h"
#include "../Lua/NpcChatScript.h"

#define  MAX_AREATASK_COUNT 50

#ifndef HAS_PATH_VERIFIER
#define HAS_PATH_VERIFIER
#endif

#ifdef HAS_PATH_VERIFIER
#include "../PathVerifier/AccessRegionPathVerifier.h"
#endif

class CMuxCopyInfo;

//玩家副本相关信息(当前所在副本信息)
struct PlayerCopyInfo
{
	//副本相关信息(欲进副本或当前所在副本信息)，若未设，则这些信息全为0(false)
	struct PlayerCopyInfoEle
	{
	public:
		PlayerCopyInfoEle() 
		{
			ClearCopyInfoEle();
		}
		~PlayerCopyInfoEle()
		{
			ClearCopyInfoEle();
		}

	public:
		void SetCopyInfoEle( unsigned int copyID/*副本ID号*/, unsigned short copyMapID/*副本地图号*/, unsigned short outMapID/*离开副本后要去的位置,地图号*/
			, unsigned short outMapPosX/*离开副本后要去的位置,坐标X*/, unsigned short outMapPosY/*离开副本后要去的位置,坐标Y*/ )
		{
			m_curCopyID = copyID;//所在副本ID号；
			m_curCopyMapID = copyMapID;//所在副本地图ID号；

			m_outMapID = outMapID;//离开副本后要去的位置,地图号;
			m_outMapPosX = outMapPosX;//离开副本后要去的位置,坐标X;
			m_outMapPosY = outMapPosY;//离开副本后要去的位置,坐标Y;
		}

		void ClearCopyInfoEle()
		{
			m_outMapID = 0;//离开副本后要去的位置,地图号;
			m_outMapPosX = 0;//离开副本后要去的位置,坐标X;
			m_outMapPosY = 0;//离开副本后要去的位置,坐标Y;

			m_curCopyMapID = 0;//所在副本地图ID号；
			m_curCopyID = 0;//所在副本ID号；
		}

	public:
		inline bool SetOutPosInfo( unsigned short& outMapID, unsigned short& outPosX, unsigned short& outPosY )
		{
			m_outMapID = outMapID;
			m_outMapPosX = outPosX;
			m_outMapPosY = outPosY;
			return true;
		}

		inline bool GetOutPosInfo( unsigned short& outMapID, unsigned short& outPosX, unsigned short& outPosY )
		{
			outMapID = m_outMapID;
			outPosX  = m_outMapPosX;
			outPosY  = m_outMapPosY;
			return true;;
		}
		inline unsigned short GetCopyMapID() { return m_curCopyMapID; }
		inline unsigned int GetCopyID() { return m_curCopyID; }

	private:
		unsigned short m_outMapID;//离开副本后要去的位置,地图号;
		unsigned short m_outMapPosX;//离开副本后要去的位置,坐标X;
		unsigned short m_outMapPosY;//离开副本后要去的位置,坐标Y;

		unsigned short m_curCopyMapID;//所在副本地图ID号；
		unsigned int   m_curCopyID;//所在副本ID号；
	};

public:
	PlayerCopyInfo() 
	{
		InitPlayerCopyInfo();
	};

	~PlayerCopyInfo() {};

private:
	void InitPlayerCopyInfo();

public:
	///清欲进副本与当前副本信息；
	bool ClearCopyInfo();

public:
	///是否已发起自身离开副本；
	inline bool IsIssueLeaveHF() { return m_isIssueSelfLeaveHF; }
	inline bool IsIssueLeaveLF() { return m_isIssueSelfLeaveLF; }
	///发起离开副本后，真正开始离开副本的条件是否已满足
	inline bool IsIssueLeaveTimeFulfil()
	{
		if ( IsIssueLeaveHF() )
		{
			if ( GetTickCount() - m_IssueSelfLeaveTime > 60*1000/*60秒*/ )
			{
				return true;
			}
		}
		return false;
	}
	///设置已发起离开副本请求
	inline void SetIssueLeaveHF() 
	{ 
		m_isIssueSelfLeaveHF = true; 
		m_IssueSelfLeaveTime = GetTickCount();
	}
	inline void SetIssueLeaveLF()
	{
		m_isIssueSelfLeaveLF = true;
	}

public:
	inline bool IsCurInCopy() { return m_isCurInCopy; }
	inline bool SetCurInCopy() { m_isCurInCopy = true; return true; }
	inline bool ClearCurInCopy() { m_isCurInCopy = false; return true; }

	inline bool IsCurPCopy() { return m_isPCopy; }
	inline bool SetCurPCopy() { m_isPCopy = true; return true; }
	inline bool ClearCurPCopy() { m_isPCopy = false; return true; }

	//inline CMuxCopyInfo* GetCurCopy()
	//{
	//	if ( !IsCurInCopy() )
	//	{
	//		return NULL;
	//	}
	//	return m_pCopy;
	//}

public:
	inline bool IsCurCopyInfoSet() { return m_isCurCopyInfoSet; }

public:
	///设置玩家的outmapinfo;
	inline bool SetOutMapInfo( unsigned short mapID, unsigned short posX, unsigned short posY )
	{
		m_curCopyInfo.SetOutPosInfo( mapID, posX, posY );
		return true;
	}

	///设置欲使用的卷轴信息；
	inline void SetToUseScrollType( unsigned int toUseScrollType )
	{
		m_toUseScrollType = toUseScrollType;//欲使用的卷轴类型；
		return;
	}

	///设置欲使用的卷轴信息；
	inline void SetCurScrollType( unsigned int curScrollType )
	{
		m_curScrollType = curScrollType;//欲使用的卷轴类型；
		return;
	}

	///设置欲进副本相关信息；
	void SetCopyInfo( bool isPCopy, unsigned int copyID/*副本ID号*/, unsigned short copyMapID/*副本地图号*/, unsigned short outMapID/*离开副本后要去的位置,地图号*/
		, unsigned short outMapPosX/*离开副本后要去的位置,坐标X*/, unsigned short outMapPosY/*离开副本后要去的位置,坐标Y*/ )
	{
		SetCurInCopy();
		if ( isPCopy )
		{
			SetCurPCopy();
		}
		m_isCurCopyInfoSet = true;		
		return m_curCopyInfo.SetCopyInfoEle( copyID, copyMapID, outMapID, outMapPosX, outMapPosY );
	}

	inline void ClearCurCopyInfo()
	{
		m_isCurInCopy = false;
		m_isPCopy     = false;
		//m_pCopy = NULL;
		m_curCopyInfo.ClearCopyInfoEle();
		m_isCurCopyInfoSet = false;
	}

public:
	///取欲使用的卷轴信息；
	inline bool GetToUseScrollType( unsigned int& toUseScrollType ) { toUseScrollType = m_toUseScrollType; return true; }

	///取欲使用的卷轴信息；
	inline bool GetCurScrollType( unsigned int& curScrollType ) { curScrollType = m_curScrollType; return true; }

	///取当前副本ID号与副本地图ID号；
	inline bool GetCurCopyMapInfo( unsigned int& copyID, unsigned short& copyMapID )
	{
		if ( !m_isCurCopyInfoSet )
		{
			return false;
		}

		copyID = m_curCopyInfo.GetCopyID();
		copyMapID = m_curCopyInfo.GetCopyMapID();

		return true;
	}

	inline bool GetOutPosInfo( unsigned short& outMapID, unsigned short& outPosX, unsigned short& outPosY )
	{
		if ( m_isCurCopyInfoSet ) {
			m_curCopyInfo.GetOutPosInfo( outMapID, outPosX, outPosY );
			return true;
		} else {
			return false;
		}

		return false;
	}

private:
	bool        m_isIssueSelfLeaveHF;//是否已发此玩家离开副本请求，上半部，主要为了防止重复请求离开；
	unsigned int m_IssueSelfLeaveTime;//发起自身离开请求的时刻，用于给客户端倒计时；
	bool        m_isIssueSelfLeaveLF;//是否已发此玩家离开副本请求，下半部，主要为了防止重复请求离开；

	bool        m_isCurInCopy;
	bool        m_isCurCopyInfoSet;//当前副本信息是否已设；
	bool        m_isPCopy;//(如果当前在副本中)，伪副本/真副本
	//CMuxCopyInfo*   m_pCopy;//当前所在Copy;	
	PlayerCopyInfoEle m_curCopyInfo;//当前所在副本信息,；
	unsigned int   m_toUseScrollType;//本次进入欲使用的卷轴类型(注意不一定是真正的卷轴类型，因为如果之前进过同一副本实例的话，会以之前的扣除卷轴为准)；
	unsigned int   m_curScrollType;
};

#ifdef HAS_PATH_VERIFIER
class CBlockQuadManager
{
public:
	struct BQData
	{
		int regionId;
		int minX;
		int minY;
		int maxX;
		int maxY;
	};
public:
	static bool InitBlockQuards(void);
	static void GetBlockQuardByMapId(unsigned short mapId, std::vector<BQData> &bqs);
	static BQData* GetBlockRanage(int regionId);
private:
	static std::vector<BQData> bqVec;
};
#endif/*HAS_PATH_VERIFIER*/

///待读取的地图信息(是否副本，地图文件名)
struct ToReadMapInfo
{
public:
	ToReadMapInfo():isCopy(false),mapFileName("") {};
	ToReadMapInfo(bool inIsCopy, const char* inMapFileName):isCopy(inIsCopy),mapFileName(inMapFileName) {};

	void	SetToReadMapInfo(ToReadMapInfo& info)
	{
		isCopy = info.isCopy;
		mapFileName = info.mapFileName;
		nSceneIndex = info.nSceneIndex;
		memcpy(info.szSceneName,szSceneName,sizeof(szSceneName));
		nSceneType = info.nSceneType; 
		dwStartTime = info.isCopy;
		dwRunTime = info.dwRunTime;
		vEnterConditions.assign(info.vEnterConditions.begin(),info.vEnterConditions.end());
		vLeaveConditions.assign(info.vLeaveConditions.begin(),info.vLeaveConditions.end());
		vSceneBuff.assign(info.vSceneBuff.begin(),info.vSceneBuff.end());
		vSceneFlag.assign(info.vSceneFlag.begin(),info.vSceneFlag.end());
	}
public:
	bool isCopy;
	string mapFileName;

	///场景索引编号
	UINT nSceneIndex; 
	///场景名
	char szSceneName[MAX_PATH];
	///场景类型 主城(1),野外(2),副本(3)
	UINT nSceneType; 
	///场景开启时间
	DWORD dwStartTime;
	///场景开启持续时间
	DWORD dwRunTime;
	struct SCondition 
	{
		UINT nType;
		UINT nValue;
	};
	//进入条件(属性)  类型,数值;类型,数值
	vector<SCondition> vEnterConditions;
	//离开条件(属性)  类型,数值;类型,数值
	vector<SCondition> vLeaveConditions;

	//进入时肯定要附加的BUFF，同时离开时也是必须要删除的BUFF
	vector<UINT> vSceneBuff;	
	//场景标记(限制)-禁止战斗，禁止PK，禁止摆摊，禁止使用传送道具（传出），不可骑乘，不可寻
	vector<UINT> vSceneFlag;	
};
///地图静态属性，由所有同类地图的不同实例共享；
class CMuxMapProperty
{
public:
	CMuxMapProperty();

	~CMuxMapProperty();

public:
	inline unsigned int GetPropertyMapNo() { return m_ID; };
	inline const char* GetMapName() { return ""; };

	////两点之间是否可通视（可直线通行）;
	//inline bool IsPassOK( float sPosX, float sPosY, float tPosX, float tPosY ) 
	//{
	//	return true;
	//}

	inline bool IsGridCanPass( unsigned int gridX, unsigned int gridY ) 
	{ 
		if ( (gridX>=m_gridWidth) || (gridY>=m_gridHeight ) )
		{
			return false;
		}

		return ( PT_DNTPASS != m_arrMapBaseInfo[gridY][gridX] );
	};

	inline bool GetPosProperty( unsigned short x, unsigned short y, unsigned int& areaID, bool& bPK, bool& bFight, bool& bPreArea )
	{
		if ( !IsGridCanPass( x, y ) )
		{
			return false;
		}

		areaID = 0;
		bPK = bFight = true;
		bPreArea = false;

		int mapInfo = m_arrMapDetailInfo[y][x];

		if ( 0 == mapInfo )
		{
			return false;
		}

		areaID   = mapInfo>>16;
		bPK     = (mapInfo & 1 )?false:true;
		bFight   = (mapInfo & 1<<1)?false:true;
		bPreArea = (mapInfo & 1<<2 )?true:false;

		return true;
	}


public:
	///从.map文件中读入地图基本信息；
	bool ReadMapInfo(const char *pszFile);

	///设置为copy map;
	inline void SetIsCopyMap() { m_IsCopyMap = true; }
	///是否copy map;
	inline bool IsCopyMap() { return m_IsCopyMap; }

	///添加一个复活点配置信息（用于填充真正复活点）;
	bool AddRebirthRgn( MapRebirthPt* pRebirthPt );

	///初始化地图的复活点，目前只处理第一个复活区域； //现加入更多的复活点
	bool FillRebirthPt();

#ifdef HAS_PATH_VERIFIER
	//读取地图分区信息
	bool ReadAcceRegionMapInfo(const char *pszFile);

	//获取地图基本信息和分区信息
	void GetBackupAndAccessRegionInfo(unsigned int** &backupedBaseInfo, unsigned int** &accessRegionInfo);
#endif

private:
	///读相应区域信息；
	int GetMapAreaInfo( int areaInfo );
	///装载指定区域脚本；
	bool LoadAreaTaskScript( unsigned int areaID );
#ifdef STAGE_MAP_SCRIPT
	///装入关卡地图；
	void LoadMapScript();
#endif //STAGE_MAP_SCRIPT

public:
	///相关地图实例启动，执行脚本初始动作；
	inline CMapScript* GetMapScript()
	{
		return m_pMapScript;
	}

	CNormalScript* GetAreaScript( unsigned int areaID )
	{
		if ( areaID >= ARRAY_SIZE(m_mapAreaFsmsArr) )
		{
			return NULL;
		}

		return m_mapAreaFsmsArr[areaID];
	}

	///获取地图上指定范围内的可用刷怪点(矩形)
	bool GetMultiRefreshMonsterPtRect( TYPE_POS gridLeft, TYPE_POS gridTop, TYPE_POS gridRight, TYPE_POS gridBottom, vector<MuxPoint>& vecOutPts )
	{ 
		TRY_BEGIN;

		TYPE_POS loopTmpX= 0, loopTmpY=0;
		TYPE_POS gridMinX=0, gridMinY=0, gridMaxX=0, gridMaxY=0;
		GetValidScopeRectGrid( gridLeft, gridTop, gridRight, gridBottom, gridMinX, gridMaxX, gridMinY, gridMaxY);  /*获取边界*/

		for( loopTmpY=gridMinY; loopTmpY<gridMaxY; ++loopTmpY )
		{
			for( loopTmpX=gridMinX; loopTmpX<gridMaxX; ++loopTmpX)
			{
				if ( IsGridCanPass( loopTmpX, loopTmpY ) )
				{
					vecOutPts.push_back( MuxPoint( loopTmpX, loopTmpY) );
				}
			}
		}
		return true;
		TRY_END;
		return false;
	};

	///取有效范围1;
	void GetValidScopeRectGrid( TYPE_POS gridLeft, TYPE_POS gridTop, TYPE_POS gridRight, TYPE_POS gridBottom, TYPE_POS& gridMinX
		, TYPE_POS& gridMaxX, TYPE_POS& gridMinY, TYPE_POS& gridMaxY)
	{
		if ( gridLeft > gridRight )
		{
			TYPE_POS tmp = gridLeft;
			gridLeft = gridRight;
			gridRight = tmp;
		}
		if ( gridTop > gridBottom )
		{
			TYPE_POS tmp = gridTop;
			gridTop = gridBottom;
			gridBottom = tmp;
		}

		gridMinX = (gridLeft <= 0) ? 0:gridLeft;
		gridMaxX = (gridRight > m_gridWidth-1) ? m_gridWidth-1:gridRight;
		gridMinY = (gridTop <= 0) ? 0:gridTop;
		gridMaxY = (gridBottom > m_gridHeight-1) ? m_gridHeight-1:gridBottom;

		return; 
	}

	bool GetRandPointByRanage( TYPE_POS gridLeft, TYPE_POS gridTop, TYPE_POS gridRight, TYPE_POS gridBottom, TYPE_POS &randPosX, TYPE_POS &randPosY)
	{
		TRY_BEGIN;

		vector<MuxPoint> vecOutPts;
		if(!GetMultiRefreshMonsterPtRect(gridLeft, gridTop, gridRight, gridBottom, vecOutPts))
			return false;

		unsigned int vecSize = (unsigned int)vecOutPts.size();
		if(0 == vecSize)
			return false;

		unsigned int randIndex = RAND % vecSize;
		randPosX = vecOutPts[randIndex].nPosX;
		randPosY = vecOutPts[randIndex].nPosY;

		return true;
		TRY_END;
		return false;
	}

public:
	///取普通复活点；
	bool GetNormalRebirthPt( MuxPoint& rebirthPt );

	///取攻城攻击方复活点；
	bool GetCityAttackRebirthPt( MuxPoint& rebirthPt );

	///取攻城防守方复活点;
	bool GetCityDefendRebirthPt( MuxPoint& rebirthPt );

	///获取攻城战复活点
	bool GetWarRebirthPt( unsigned short &mapId, unsigned int &posX, unsigned int &posY );

private:
	unsigned int     m_ID;//地图ID号；
	bool             m_IsCopyMap;//是否副本地图；

private:
	CMapScript*      m_pMapScript;//地图脚本；
	CNormalScript*   m_mapAreaFsmsArr[MAX_AREATASK_COUNT];//地图区域脚本

public:
	inline unsigned short GetGridWidth() { return m_gridWidth; }
	inline unsigned short GetGridHeight() { return m_gridHeight; }
	inline unsigned short GetBlockWidth() { return m_blockWidth; }
	inline unsigned short GetBlockHeight() { return m_blockHeight; }

private://地图属性；
	unsigned short   m_gridWidth;//地图宽度（以格子计）；
	unsigned short   m_gridHeight;//地图高度（以格子计）；
	unsigned short   m_blockWidth;//地图宽度（以小块计）；
	unsigned short   m_blockHeight;//地图高度（以小块计）；

	int**            m_arrMapBaseInfo;//地图各点，目前只包含障碍信息,PT_DNTPASS为障碍，PT_CANPASS为可通行；
	int**            m_arrMapDetailInfo;

#ifdef HAS_PATH_VERIFIER
	unsigned int**   m_arrMapBaseInfoBackup;//地图基本信息备份，用于初始化CAccessRegionPathVerifier
	unsigned int**   m_arrAccessReginMapInfo;//地图区域信息
#endif

private:
	///本地图的复活点配置信息，将据此填充各类可用复活点；
	vector< MapRebirthPt* > m_vecNewMapRebirthPts;

	///本地图的可用复活点；
	vector<MuxPoint*> m_vecAvailRebirthPt;					//普通复活点
	vector<MuxPoint*> m_vecAvailCityAttackRebirthPt;		//攻城战进攻者所用复活点
	vector<MuxPoint*> m_vecAvailCityDefendRebirthPt;		//攻城战防守者所用复活点

public:
	//
	void SetToReadMapInfo(ToReadMapInfo& info);
	ToReadMapInfo	m_ToReadMapInfo;
};

#endif //MAP_PROPERTY_H


