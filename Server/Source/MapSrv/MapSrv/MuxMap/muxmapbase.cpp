﻿/**
* @file muxmapbase.cpp
* @brief 
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: muxmapbase.cpp
* 摘    要: 
* 作    者: dzj
* 开始日期: 2009.10.07
* 
*/

#include "muxmapbase.h"
#include "../Lua/BattleScript.h"

#include "../Player/Player.h"
#include "../monster/monster.h"
#include "../Item/CItemPackage.h"
#include "../monster/manmonstercreator.h"
#include "../GMCmdManager.h"

#include "../ItemSkill.h"

#include "../Lua/FightCalScript.h"
#include "../Lua/FightPreScript.h"

#include "../BattleRule.h"

#include "../Rank/rankeventmanager.h"
#include "../Activity/WarTaskManager.h"
#include "../Item/CItemPackage.h"
#include "../Union/UnionManager.h"
#include "../NpcStandRule.h"
#include "../AISys/RuleAIEngine.h"
#include "mannormalmap.h"
#include "../MuxSkillConfig.h"

#include "manmuxcopy.h"
#include "../CallMemberSkill.h"

/*
AreaOfInterest区域示意：

ext0   ext1  ext2  ext3  ext4
ext15   0     1     2    ext5
ext14   7     8     3    ext6
ext13   6     5     4    ext7
ext12  ext11 ext10 ext9  ext8

11 = 0x3; 111 = 0x7; 11111 = 1+2+4+8+16 = 0x1f
0: (111<<0 | 11<<14), 1: 111<<1, 2: 11111<<2, 3: 111<<5, 4: 11111<<6
5: 111<<9, 6: 11111<<10, 7: 111<<13
*/

const unsigned int CMuxMap::AreaOfInterest::posMap[9] = { 0, 1, 2, 7, 8, 3, 6, 5, 4 };//三横三竖静态对应位置
const int CMuxMap::AreaOfInterest::extRevMap[16][2] = { {-2, -2}, {-2,-1}, {-2,0}, {-2,1}, {-2,2}, {-1,2}, {0,2}, {1,2}, {2,2}, {2,1}, {2,0}, {2,-1}, {2,-2}, {1,-2}, {0,-2}, {-1,-2} };//ext位置对应的块偏移值
const int CMuxMap::AreaOfInterest::extExtRevMap[24][2] = { {-3,-3},{-3,-2},{-3,-1},{-3,0},{-3,1},{-3,2},{-3,3},{-2,3},{-1,3},{0,3},{1,3},{2,3},{3,3},{3,2},{3,1},{3,0},{3,-1},{3,-2},{3,-3},{2,-3},{1,-3},{0,-3},{-1,-3},{-2,-3} };

//取对应扩展区域
vector<MuxMapBlock*>* CMuxMap::AreaOfInterest::GetExtArea( CMuxMap* pResMap )
{
	if ( NULL == pResMap )
	{
		D_ERROR( "AreaOfInterest::GetExtArea, NULL == pResMap\n" );
		return NULL;
	}
	m_vecExtArea.clear();//清之前填充信息

	MuxMapBlock* pBlock = NULL;

	//基本区域压入输出数组
	for (int y=-1; y<=1; ++y)
	{
		for (int x=-1; x<=1; ++x)
		{
			pBlock = pResMap->GetMapBlockByBlock( m_cenX+x, m_cenY+y );
			if (NULL != pBlock)
			{
				m_vecExtArea.push_back(pBlock);
			}	    
		}
	}

	unsigned int tmpmask = extmask;
	unsigned int chkpos = 0;
	unsigned int loopnum = 0;

	while ( 0!=tmpmask )
	{
		++ loopnum;
		if (loopnum>16)
		{
			D_ERROR( "AreaOfInterest::GetExtArea, loopnum>16\n" );	    
			break;
		}
		if ( !GetLogValAsm( tmpmask, chkpos ) )
		{
			D_ERROR( "AreaOfInterest::GetExtArea, GetLogVal失败\n" );
			break;
		}

		if (chkpos >= 16)
		{
			D_ERROR( "AreaOfInterest::GetExtArea, GetLogVal返回值%d错误\n", chkpos );
			break;
		}

		tmpmask &= (~(0x1<<chkpos));//已检查位，置0
		pBlock = pResMap->GetMapBlockByBlock( m_cenX+extRevMap[chkpos][1], m_cenY+extRevMap[chkpos][0] );
		if (NULL != pBlock)
		{
			m_vecExtArea.push_back(pBlock);
		}
		if ( 0 == tmpmask )
		{
			break;
		}
	}

	tmpmask = extextmask;
	chkpos = 0;
	loopnum = 0;
	while ( 0!=tmpmask )
	{
		++ loopnum;
		if (loopnum>24)
		{
			D_ERROR( "AreaOfInterest::GetExtArea, loopnum>24\n" );	    
			break;
		}
		if ( !GetLogValAsm( tmpmask, chkpos ) )
		{
			D_ERROR( "AreaOfInterest::GetExtArea, GetLogVal失败\n" );
			break;
		}

		if (chkpos >= 24)
		{
			D_ERROR( "AreaOfInterest::GetExtArea, GetLogVal返回值%d错误\n", chkpos );
			break;
		}

		tmpmask &= (~(0x1<<chkpos));//已检查位，置0
		pBlock = pResMap->GetMapBlockByBlock( m_cenX+extExtRevMap[chkpos][1], m_cenY+extExtRevMap[chkpos][0] );
		if (NULL != pBlock)
		{
			m_vecExtArea.push_back(pBlock);
		}	    
	}

	return &m_vecExtArea;
} //vector<MuxMapBlock*>* AreaOfInterest::GetExtArea( CMuxMap* pResMap )

CMuxMap::CMuxMap( bool isNormalOrCopy, unsigned short width, unsigned short height ) : 
m_pMapProperty(NULL),
m_pNormalMapInfo(NULL), m_pCopyMapInfo(NULL)
// m_ID(0), m_gridWidth(0), m_gridHeight(0), m_arrMapBaseInfo(NULL), m_arrMapDetailInfo(NULL), m_blockWidth(0), m_blockHeight(0), m_arrMapBlocks(NULL)
//m_arrMapBlocks(NULL)
#ifdef STAGE_MAP_SCRIPT
, m_pTimerOfMap(NULL), m_pCounterOfMap(NULL)/*,m_pCityInfo( NULL )*/
#endif //STAGE_MAP_SCRIPT
{
	if ( isNormalOrCopy )
	{
		//普通地图；
		m_AIMapInfo.isNormalOrCopy = true;//普通地图；
		m_AIMapInfo.mapID          = 0;//初始无效的地图ID号；
		m_NormalOrCopy = MUX_NORMAL_MAP;
		m_pNormalMapInfo = NEW CMuxNormalMapInfo( width, height );
	} else {
		//副本地图；
		m_AIMapInfo.isNormalOrCopy = false;//副本；
		m_AIMapInfo.mapID = 0;//无效的副本号；
		m_NormalOrCopy = MUX_COPY_MAP;
		m_pCopyMapInfo = NEW CMuxCopyInfo();
	}

	//by dzj, 代码规范，初始化部分成员...
	m_MovingPlayers.clear();

	m_lastSupplyTime = ACE_OS::gettimeofday() + ACE_Time_Value( 0, (RAND%50)*1000 );
	m_LastEPos = 0;
	m_pWarAdditionInfo = NULL;

	//memset(m_mapAreaFsmsArr, 0x0,sizeof(CNormalScript *)*MAX_AREATASK_COUNT );
	m_vecMonsterCreators.clear();
	m_vecNoCreatorNpcSupplyQueue.clear();
	m_vecCreatorNpcSuppplyQueue.clear();
#ifdef ITEM_NPC
	StructMemSet( m_itemNpcChangedInfo, 0, sizeof(m_itemNpcChangedInfo) );
	StructMemSet( m_pArrMapItemNpc, 0, sizeof( m_pArrMapItemNpc ) );
#endif //ITEM_NPC

#ifdef STAGE_MAP_SCRIPT
	m_pTimerOfMap = NEW CTimerOfMap;
	if ( NULL != m_pTimerOfMap )
	{
		m_pTimerOfMap->InitTimerOfMap( this );//地图计时器；
	}
	m_pCounterOfMap = NEW CCounterOfMap;
	if ( NULL != m_pCounterOfMap )
	{
		m_pCounterOfMap->InitCounterOfMap( this );//地图计数器；
	}
	m_pDetailTimerOfMap = NEW CDetailTimerOfMap();
	if( NULL != m_pDetailTimerOfMap )
	{
		m_pDetailTimerOfMap->InitTimerOfMap( this );
	}
	m_warTimerManager.AttachOwnerMap( this );
	m_warCounterManager.AttachOwnerMap( this );
#endif //STAGE_MAP_SCRIPT

#ifdef HAS_PATH_VERIFIER
	m_pBlockQuards = NULL;
	m_blockQuadNum = 0;
	m_pPathVerifier = NULL;
#endif

	// [1/5/2010 shenchenkai]
	m_tInvade = ACE_Time_Value::zero;
	m_tAttackNpc = ACE_Time_Value::zero;
	//  [1/5/2010 shenchenkai]
};

CMuxMap::~CMuxMap() 
{
	ReleaseMuxMap();

#ifdef STAGE_MAP_SCRIPT
	if ( NULL != m_pTimerOfMap )
	{
		delete m_pTimerOfMap; m_pTimerOfMap = NULL;
	}
    if ( NULL != m_pCounterOfMap )
	{
		delete m_pCounterOfMap; m_pCounterOfMap = NULL;
	}
	if( NULL != m_pDetailTimerOfMap )
	{
		delete m_pDetailTimerOfMap; m_pDetailTimerOfMap = NULL;
	}
#endif //STAGE_MAP_SCRIPT

	if ( NULL != m_pNormalMapInfo )
	{
		delete m_pNormalMapInfo; m_pNormalMapInfo = NULL;
	}

	if ( NULL != m_pCopyMapInfo )
	{
		delete m_pCopyMapInfo; m_pCopyMapInfo = NULL;
	}

	if(NULL != m_pWarAdditionInfo)
	{
		delete m_pWarAdditionInfo; m_pWarAdditionInfo = NULL;
	}
	return;
};

///释放地图内存,???,邓子建检查；
void CMuxMap::ReleaseMuxMap()
{
	TRY_BEGIN;

	m_MovingPlayers.clear();

#ifdef STAGE_MAP_SCRIPT
	if ( NULL != m_pTimerOfMap )
	{
		m_pTimerOfMap->InitTimerOfMap( this );//地图计时器；
	}
	if ( NULL != m_pCounterOfMap )
	{
		m_pCounterOfMap->InitCounterOfMap( this );//地图计数器；
	}
	if( NULL != m_pDetailTimerOfMap )
	{
		m_pDetailTimerOfMap->InitTimerOfMap( this );
	}
	m_warTimerManager.InitWarTimer( this );
#endif //STAGE_MAP_SCRIPT

#ifdef ITEM_NPC
	StructMemSet( m_itemNpcChangedInfo, 0, sizeof(m_itemNpcChangedInfo) );
	for( int i =0; i< ARRAY_SIZE(m_pArrMapItemNpc); ++i )
	{
		if( NULL != m_pArrMapItemNpc[i] )
		{
			delete m_pArrMapItemNpc[i]; m_pArrMapItemNpc[i] = NULL;
		}
	}
#endif //ITEM_NPC

	if ( NULL != m_pNormalMapInfo )
	{
		m_pNormalMapInfo->ResetNormalMapInfo();
	}

	if ( NULL != m_pCopyMapInfo )
	{
		m_pCopyMapInfo->ResetCopyMapInfo();
	}

	m_vecNoCreatorNpcSupplyQueue.clear();
	m_vecCreatorNpcSuppplyQueue.clear();

#ifdef HAS_PATH_VERIFIER
	m_blockQuadNum = 0;
	delete []m_pBlockQuards; m_pBlockQuards = NULL;

	delete m_pPathVerifier ; m_pPathVerifier = NULL;
#endif

	CManMonsterCreatorProp::ClearMapCreators( this );//因为之前地图清怪物要用到相关生成器，所以放在最后；

	TRY_END;
}

///使用地图属性初始化地图;
bool CMuxMap::InitMapProperty( CMuxMapProperty* pMapProperty )
{
	if ( NULL == pMapProperty )
	{
		return false;
	}

	m_MapScriptTarget = 0;

	m_pMapProperty = pMapProperty;

	m_vecToDelMonsterType.clear();

	m_isPCopyAndClose = false;//是否为伪副本&&已关闭，两项为真时才为真；

#ifdef HAS_PATH_VERIFIER
	SetPathVerifier();//设置路径校验器
#endif

	if ( m_pMapProperty->IsCopyMap() )
	{
		//副本地图；
		if ( m_AIMapInfo.isNormalOrCopy )
		{
			D_ERROR( "InitMapProperty，使用副本地图%d属性初始化普通地图\n", pMapProperty->GetPropertyMapNo() );
			return false;
		}
	} else {
		//普通地图；
		if ( !m_AIMapInfo.isNormalOrCopy )
		{
			D_ERROR( "InitMapProperty，使用普通地图%d属性初始化副本地图\n", pMapProperty->GetPropertyMapNo() );
			return false;
		}
		m_AIMapInfo.mapID          = m_pMapProperty->GetPropertyMapNo();

		//设置地图关系（内外城）
		InitWarMapRelation();
	}

	return true;
}

///初始化副本地图的副本相关信息；
bool CMuxMap::InitMapCopyInfo( unsigned int copyid )
{
	if ( NULL == m_pMapProperty )
	{
		return false;
	}

	if ( !(IsInsCopyMap()) )
	{
		//非副本地图调用副本初始化;
		D_ERROR( "非副本地图调用副本初始化,输入的副本号%d\n", copyid );
		return false;
	}

	//副本地图；
	m_AIMapInfo.mapID = copyid;//真正的副本号；	
	if ( !(m_pCopyMapInfo->InitCopy( GetMapNo(), copyid ) ) )
	{
		D_ERROR( "副本初始化失败,输入的副本号%d\n", copyid );
		delete m_pCopyMapInfo; m_pCopyMapInfo = NULL;
		return false;
	}

	return true;
}

/// add rideteam to map;
bool CMuxMap::AddRideTeamWithLeader( RideTeam* pTeam, CPlayer* pPlayer )
{
	if ( ( NULL == pTeam ) || ( NULL == pPlayer ) )
	{
		D_ERROR( "CMuxMap::AddRideTeamWithLeader, 输入参数空\n" );
		return false;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::AddRideTeamWithLeader, NULL == m_pCopyMapInfo!" );
			return false;
		}
		if ( !(m_pCopyMapInfo->AddRideteamToCopyMap( pTeam, pPlayer->GetPosX(), pPlayer->GetPosY() )) )
		{
			D_ERROR( "AddRideteadToCopyMap fail!" );
			return false;
		}
		return true;
	} else {
		//普通地图
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::AddRideTeamWithLeader, NULL == m_pNormalMapInfo!" );
			return false;
		}
		if ( !(m_pNormalMapInfo->AddRideteamToNormalMap( pTeam, pPlayer->GetPosX(), pPlayer->GetPosY() )) )
		{
			D_ERROR( "AddRideteadToNormalMap fail!" );
			return false;
		}
		return true;
	}

	return false;
}

///delete rideteam from map;
bool CMuxMap::DelRideTeamWithLeader( RideTeam* pTeam, CPlayer* pPlayer )
{
	if ( ( NULL == pTeam ) || ( NULL == pPlayer ) )
	{
		D_ERROR( "CMuxMap::DelRideTeamWithLeader, 输入参数空\n" );
		return false;
	}

	if ( IsInsCopyMap() )
	{
		//副本;
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::DelRideTeamWithLeader, NULL == m_pCopyMapInfo!" );
			return false;
		}
		if ( !(m_pCopyMapInfo->DelRideteamFromCopyMap( pTeam, pPlayer->GetPosX(), pPlayer->GetPosY() )) )
		{
			D_ERROR( "DelRideteamFromCopyMap fail!" );
			return false;
		}
		return true;
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::DelRideTeamWithLeader, NULL == m_pNormalMapInfo!" );
			return false;
		}
		if ( !(m_pNormalMapInfo->DelRideteamFromNormalMap( pTeam, pPlayer->GetPosX(), pPlayer->GetPosY() )) )
		{
			D_ERROR( "DelRideteamFromNormalMap fail!" );
			return false;
		}
		return true;
	}

	return false;
}

///notify hp add event;
void CMuxMap::NotifySurMonsterPlayerHpAdd( PlayerID addStarter, PlayerID addTarget, int hpAdd, unsigned short centerX, unsigned short centerY )
{
	if ( IsInsCopyMap() )
	{
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::NotifySurMonsterPlayerHpAdd, NULL == m_pCopyMapInfo!" );
			return ;
		}

		m_pCopyMapInfo->NotifySurMonsterPlayerHpAdd( addStarter, addTarget, hpAdd, centerX, centerY );
	}
	else
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::NotifySurMonsterPlayerHpAdd, NULL == m_pNormalMapInfo!" );
			return ;
		}

		m_pNormalMapInfo->NotifySurMonsterPlayerHpAdd( addStarter, addTarget, hpAdd, centerX, centerY );
	}


	return;
}

/////temp for monster wrapper;
//bool CMuxMap::GetScopeMonsters( unsigned short centerX, unsigned short centerY, ScopeMonsterInfo* scopeMonstrArr, int inArrSize, int& arrSize )
//{
//	if ( NULL == scopeMonstrArr )
//	{
//		D_ERROR( "CMuxMap::GetScopeMonsters, 输入参数空\n" );
//		return false;
//	}
//
//	if ( IsInsCopyMap() )
//	{
//		//副本；
//		if ( NULL == m_pCopyMapInfo )
//		{
//			arrSize = 0;
//			D_ERROR( "CMuxMap::GetScopeMonsters, NULL == m_pCopyMapInfo\n" );
//			return false;
//		}
//		return m_pCopyMapInfo->GetScopeMonsters( centerX, centerY, scopeMonstrArr, inArrSize, arrSize );
//	} else {
//		//普通地图；
//		if ( NULL == m_pNormalMapInfo )
//		{
//			arrSize = 0;
//			D_ERROR( "CMuxMap::GetScopeMonsters, NULL == m_pNormalMapInfo\n" );
//			return false;
//		}
//		return m_pNormalMapInfo->GetScopeMonsters( centerX, centerY, scopeMonstrArr, inArrSize, arrSize );
//	}
//
//	return false;
//}
//
/////temp for monster wrapper;
//bool CMuxMap::GetScopePlayers( unsigned short centerX, unsigned short centerY, ScopePlayerInfo* scopePlayerArr, int inArrSize, int& arrSize )
//{
//	if ( NULL == scopePlayerArr )
//	{
//		D_ERROR( "CMuxMap::GetScopePlayers, 输入参数空\n" );
//		return false;
//	}
//
//	if ( IsInsCopyMap() )
//	{
//		//副本；
//		if ( NULL == m_pCopyMapInfo )
//		{
//			arrSize = 0;
//			D_ERROR( "CMuxMap::GetScopePlayers, NULL == m_pCopyMapInfo\n" );
//			return false;
//		}
//		return m_pCopyMapInfo->GetScopePlayers( centerX, centerY, scopePlayerArr, inArrSize, arrSize );
//	} else {
//		//普通地图；
//		if ( NULL == m_pNormalMapInfo )
//		{
//			arrSize = 0;
//			D_ERROR( "CMuxMap::GetScopePlayers, NULL == m_pNormalMapInfo\n" );
//			return false;
//		}
//		return m_pNormalMapInfo->GetScopePlayers( centerX, centerY, scopePlayerArr, inArrSize, arrSize );
//	}
//
//	return false;
//}


/// exec when map start, etc. when copy created
bool CMuxMap::OnMapStart()
{
	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnStageInit( this );

	return true;
}

///取一个可用点，用于机器人随机出生与复活；
bool CMuxMap::GetOneAvailPt( MuxPoint& availPt ) 
{ 
	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			availPt.nPosX = 0;
			availPt.nPosY = 0;
			D_ERROR( "CMuxMap::GetOneAvailPt, NULL == m_pCopyMapInfo\n" );
		} else {
			availPt.nPosX = (unsigned short) ( RAND % COPY_GRID_WIDTH );
			availPt.nPosY = (unsigned short) ( RAND % COPY_GRID_HEIGHT );
		}
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			availPt.nPosX = 0;
			availPt.nPosY = 0;
			D_ERROR( "CMuxMap::GetOneAvailPt, NULL == m_pNormalMapInfo\n" );
		} else {
			availPt.nPosX = (unsigned short) ( RAND % m_pNormalMapInfo->GetGridWidth() );
			availPt.nPosY = (unsigned short) ( RAND % m_pNormalMapInfo->GetGridHeight() );
		}
	}

	return true;
	//不再使用此成员，机器人出生复活点随便选，因为他们的移动本来也不考虑障碍，by dzj, 09.10.12unsigned int availSize = (unsigned int)(m_vecAvailPt.size());
};

///取一个可用复活点;
bool CMuxMap::GetOneRebirthPt( unsigned long& mapID, MuxPoint& rebirthPt, unsigned char rebirthRace ) 
{ 
	if ( NULL == m_pMapProperty )
	{
		rebirthPt.nPosX = 0;
		rebirthPt.nPosY = 0;
		D_ERROR( "CMuxMap::GetOneRebirthPt, NULL == m_pMapProperty,  地图编号%d\n",  GetMapNo());
		return false;
	}

	return m_pMapProperty->GetNormalRebirthPt( rebirthPt ); 
};

bool CMuxMap::GetDefaultRebirthPt(unsigned long& mapID, MuxPoint& rebirthPt)
{
	mapID = GetMapNo();
	rebirthPt.nPosX = 0;
	rebirthPt.nPosY = 0;

	if ( NULL == m_pMapProperty )
	{
		return false;
	}

	return m_pMapProperty->GetNormalRebirthPt( rebirthPt );
}

///读入所有属于本地图的怪物生成器;
void CMuxMap::FillMapMonsterCreator() 
{
	return CManMonsterCreatorProp::FillMapCreators( this );		
};

///添加一个本map的怪物生成器；
bool CMuxMap::PushMapCreator( CCreatorIns* pCreatorIns )
{
	if ( NULL == pCreatorIns )
	{
		return false;
	}
	if ( pCreatorIns->GetOwnerMap() != this )
	{
		return false;
	}

	m_vecMonsterCreators.push_back( pCreatorIns );

	return true;
}

///弹出一个本map的怪物生成器，如果本地图已无生成器，则返回NULL；
CCreatorIns* CMuxMap::PopMapCreator()
{
	if ( m_vecMonsterCreators.empty() )
	{
		return NULL;
	}

	CCreatorIns* pToReturn = m_vecMonsterCreators[m_vecMonsterCreators.size()-1];//返回最末元素；
	m_vecMonsterCreators.pop_back();

	return pToReturn;
}


bool CMuxMap::SetNpcCreatorActiveFlag( unsigned int creatorID, bool isActive )
{
	if ( m_vecMonsterCreators.empty() )
	{
		return false;
	}

	for( unsigned int i=0; i<m_vecMonsterCreators.size(); ++i )
	{
		if( creatorID == m_vecMonsterCreators[i]->GetCreatorID() )
		{
			m_vecMonsterCreators[i]->SetActiveFlag( isActive );	//激活
			if( isActive )
			{
				m_vecMonsterCreators[i]->TryRefreshMonster();		//再进行第一次的刷怪
			}
			return true;
		}
	}

	return false;
}

///第一次刷怪
void CMuxMap::FirstRefreshMonster()
{
	TRY_BEGIN;

	CCreatorIns* pCreator = NULL;
	for ( vector< CCreatorIns* >::iterator tmpiter = m_vecMonsterCreators.begin();
		tmpiter != m_vecMonsterCreators.end(); ++ tmpiter )
	{
		pCreator = *tmpiter;
		if ( NULL == pCreator )
		{
			continue;
		}
		pCreator->TryRefreshMonster();//使用生成器在本地图上刷怪；
	}

	OnMapStart();//地图初始化操作，执行脚本中的初始化操作；

	return;
	TRY_END;
	return;
};

///添加一个无生成器NPC生成请求至队列；
bool CMuxMap::AddOneNoCreatorNpc( unsigned long callerID, unsigned long npcType, TYPE_POS posX, TYPE_POS posY, const PlayerID& rewardInfo, unsigned int aiFlag )
{
	TRY_BEGIN;

	m_vecNoCreatorNpcSupplyQueue.push_back( NoCreatorNpcSupplyInfo( callerID, npcType, posX, posY, rewardInfo, aiFlag ) );

	return true;
	TRY_END;
	return false;
}

///添加一个待补充的怪物；
bool CMuxMap::AddToSupplyCreator( CCreatorIns* pCreatorIns )
{
	if ( NULL == pCreatorIns )
	{
		D_ERROR( "CMuxMap::AddToSupplyCreator, NULL == pCreatorIns\n" );
		return false;
	}

	if ( this != pCreatorIns->GetOwnerMap() )
	{
		D_ERROR( "CMuxMap::AddToSupplyCreator, this != pCreatorIns->m_pOwner\n" );
		return false;
	}

	pCreatorIns->SetInSupply();//设置其当前在补充队列中；
	m_vecCreatorNpcSuppplyQueue.push_back( pCreatorIns );

	return true;
}

///副本时钟循环；
bool CMuxMap::MapCopyTimerProc( const ACE_Time_Value& curTime )
{
	if ( NULL == m_pCopyMapInfo )
	{
		D_ERROR( "MapCopyTimerProc, NULL == m_pCopyInfo\n" );
		return false;//副本地图此信息不应为空，删去此副本地图；
	} 

	if ( !(m_pCopyMapInfo->CopyTimerProc()) )//副本相关信息时间；
	{
		return false;
	}

	MapCommonTimerProc( curTime );//一般地图时钟(普通地图与副本地图都要调用)；
	return true;
}

///普通地图时钟循环；
bool CMuxMap::MapNormalTimerProc( const ACE_Time_Value& curTime )
{
	//normalmap目前没有特殊需要执行的时钟，只需执行地图共用时钟
	MapCommonTimerProc( curTime );
	return true;
}

///普通地图与副本地图共用时钟
void CMuxMap::MapCommonTimerProc( const ACE_Time_Value& curTime )
{

	if ( (curTime-m_lastSupplyTime).msec() >= 511 )
	{
#ifdef STAGE_MAP_SCRIPT
		if ( NULL != m_pTimerOfMap )
		{
			///地图计时器检测；
			m_pTimerOfMap->MapTimerCheck( curTime );
		}

		if( NULL != m_pDetailTimerOfMap )
		{
			m_pDetailTimerOfMap->MapDetailTimerCheck();
		}

		m_warTimerManager.WarTimerProcess();
#endif //STAGE_MAP_SCRIPT
		MapTrySupplyMonster();
		m_lastSupplyTime = curTime;
	}

	TimerCheckToDelTypeMonster();

	MovingPlayerTimer();

	return;
}

///尝试补充怪
void CMuxMap::MapTrySupplyMonster()
{
	TRY_BEGIN;

	if ( !m_vecNoCreatorNpcSupplyQueue.empty() )
	{
		CMonster* pNoCreatorMonster = NULL;
		for ( vector<NoCreatorNpcSupplyInfo>::iterator iter=m_vecNoCreatorNpcSupplyQueue.begin(); iter!=m_vecNoCreatorNpcSupplyQueue.end(); ++iter )
		{
			pNoCreatorMonster = NULL;
			CCreatorIns::SinglePtRefreshNoCreator( iter->GetNpcType(), this, iter->GetPosX(), iter->GetPosY(), 0, pNoCreatorMonster, NULL );
			if ( NULL != pNoCreatorMonster )
			{
				if( pNoCreatorMonster )
				{
					CRuleAIEngine::Instance()->OnCallMonsterBorn( pNoCreatorMonster->GetMonsterID(), iter->GetAiFlag() );
					pNoCreatorMonster->SetRewardInfo( iter->GetRewardID() );
					//  [1/25/2010 shenchenkai]
					if ( iter->GetRewardID().wGID == 0 && iter->GetRewardID().dwPID == 0)
					{
						pNoCreatorMonster->ClearRewardInfo();
					}
				}
			} else {
				D_WARNING( "MapTrySupplyMonster，地图%d，无生成器怪物生成怪物失败(%d, %d, %d)\n", GetMapNo(), iter->GetNpcType(), iter->GetPosX(), iter->GetPosY() );
			}
		}
		m_vecNoCreatorNpcSupplyQueue.clear();
	}

	CCreatorIns* pCreator = NULL;
	bool isNextNeedSupply = false;
	for ( vector< CCreatorIns* >::iterator tmpiter = m_vecCreatorNpcSuppplyQueue.begin(); tmpiter != m_vecCreatorNpcSuppplyQueue.end(); )
	{
		isNextNeedSupply = false;
		pCreator = *tmpiter;
		if ( NULL == pCreator )
		{
			continue;
		}
		pCreator->TrySupplyMonster( isNextNeedSupply );
		if ( !isNextNeedSupply )
		{
			pCreator->ClearInSupply();//设置其当前已不在补充队列中；
			tmpiter = m_vecCreatorNpcSuppplyQueue.erase( tmpiter );			
		} else {
			++tmpiter;
		}
	}

	return;
	TRY_END;
	return;
};

bool CMuxMap::IsEnabelAddTower(TYPE_POS gridX, TYPE_POS gridY)
{
	bool res = false;
	if ( IsInsCopyMap() )
	{
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::IsEnabelAddTower, NULL == m_pCopyMapInfo\n" );
			return false;
		}
		res = m_pCopyMapInfo->IsEnabelAddTower( gridX, gridY );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::IsEnabelAddTower, NULL == m_pNormalMapInfo\n" );
			return false;
		}

		res = m_pNormalMapInfo->IsEnabelAddTower( gridX, gridY );
	}
	return res;
}

bool CMuxMap::GetRandPointByRanage( TYPE_POS gridLeft, TYPE_POS gridTop, TYPE_POS gridRight, TYPE_POS gridBottom, TYPE_POS &randPosX, TYPE_POS &randPosY)
{
	TRY_BEGIN;

	if ( NULL == m_pMapProperty )
	{
		return false;
	}

	return m_pMapProperty->GetRandPointByRanage( gridLeft, gridTop, gridRight, gridBottom, randPosX, randPosY );
	TRY_END;
	return false;
}

///检查玩家是否在地图内(玩家指针不知是否仍有效)；
bool CMuxMap::IsDoubtPlayerPtrInMap( CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CMuxMap::IsDoubtPlayerPtrInMap, 输入参数空\n" );
		return false;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::IsDoubtPlayerPtrInMap, NULL == m_pCopyMapInfo\n" );
			return false;
		}
		return m_pCopyMapInfo->IsDoubtPlayerPtrInMap( pPlayer );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::IsDoubtPlayerPtrInMap, NULL == m_pNormalMapInfo\n" );
			return false;
		}
		return m_pNormalMapInfo->IsDoubtPlayerPtrInMap( pPlayer );
	}

	return false;
}

#ifdef USE_SELF_HASH
///检查怪物是否在地图内；
bool CMuxMap::IsDoubtMonsterPtrInMap( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		return false;
	}
	return (NULL != GetMonsterPtrInMap( pMonster->GetMonsterID() ));
}

///取地图内的怪物指针；
CMonster* CMuxMap::GetMonsterPtrInMap( unsigned long monsterID )
{
	if ( IsInsCopyMap() )
	{
		//副本地图；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::GetMonsterPtrInMap, NULL == m_pCopyMapInfo\n" );
			return NULL;
		}
		return m_pCopyMapInfo->GetMonsterPtrInCopy( monsterID );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::GetMonsterPtrInMap, NULL == m_pNormalMapInfo\n" );
			return NULL;
		}
		return m_pNormalMapInfo->GetMonsterPtrInMap( monsterID );
	}

	return NULL;
}
#else  //USE_SELF_HASH
///检查怪物是否在地图内；
bool CMuxMap::IsDoubtMonsterPtrInMap( CMonster* pMonster )
{
	set<CMonster*>::iterator iter = m_allMonster.find( pMonster );
	if ( iter != m_allMonster.end() )
	{
		if ( !( pMonster->IsToDel() ) )
		{
			return true;
		}
	}
	return false;
}
#endif //USE_SELF_HASH

///取地图内玩家总数；
unsigned int CMuxMap::GetMapPlayerNum()
{
	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::GetMapPlayerNum, NULL == m_pCopyMapInfo\n" );
			return 0;
		}
		return m_pCopyMapInfo->GetCopyPlayerNum();
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::GetMapPlayerNum, NULL == m_pNormalMapInfo\n" );
			return 0;
		}
		return m_pNormalMapInfo->GetMapPlayerNum();
	}

	return 0;
}

//添加一个玩家到地图上，同时加至块上
bool CMuxMap::AddPlayerToMap( CPlayer* pPlayer, unsigned short gridX, unsigned short gridY ) 
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CMuxMap::AddPlayerToMap，玩家指针空\n" );
		return false;
	}

	if ( !IsGridValid( gridX, gridY ) )
	{
		//点超出范围，添加失败;
		D_ERROR( "CMuxMap::AddPlayerToMap，玩家的地图出现点%d,%d越界\n", gridX, gridY );
		return false;
	}

	bool isOK = false;

	if ( IsInsCopyMap() )
	{
		//副本地图；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::AddPlayerToMap，NULL == m_pCopyMapInfo\n" );
			return false;
		}

		isOK = m_pCopyMapInfo->PlayerEnterCopy( pPlayer, gridX, gridY );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::AddPlayerToMap，NULL == m_pNormalMapInfo\n" );
			return false;
		}

		isOK = m_pNormalMapInfo->AddPlayerToNormalMap( pPlayer, gridX, gridY );
	}

	//去除种族后废弃，by dzj, 10.07.30;////  [1/5/2010 shenchenkai]
	//if (isOK)
	//{
	//	OnCheckPlayerEnterMapNotify(pPlayer);
	//}

	return isOK; 
};

///怪物加入细节地图(normalmap/copymap)
bool CMuxMap::AddMonsterToExtMap( CMonster* pMonster, unsigned short gridX, unsigned short gridY )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMuxMap::AddMonsterToExtMap, 输入参数空\n" );
		return false;
	}

	if ( IsInsCopyMap() )
	{
		//副本
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::AddMonsterToExtMap, NULL == m_pCopyMapInfo\n" );
			return false;
		}
		if ( !(m_pCopyMapInfo->AddMonsterToCopyMap( pMonster, gridX, gridY )) )
		{
			D_ERROR( "CMuxMap::AddMonsterToExtMap, AddMonsterToCopyMap fail\n" );
			return false;
		}
		//m_pCopyMapInfo->MonsterBornNotify( pMonster );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::AddMonsterToExtMap，NULL == m_pNormalMapInfo\n" );		
			return false;
		}

		if ( !(m_pNormalMapInfo->AddMonsterToNormalMap( pMonster, gridX, gridY ) ) )
		{
			D_ERROR( "CMuxMap::AddMonsterToExtMap，AddMonsterToNormalMap fail\n" );		
			return false;
		}
		//m_pNormalMapInfo->MonsterBornNotify( pMonster );
	}

#ifdef AI_SUP_CODE
	pMonster->OnSelfBorn(); //先通知其自身出生；
#endif //AI_SUP_CODE

	//再通知其周围其它信息；
	if ( IsInsCopyMap() )
	{
		//副本
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::AddMonsterToExtMap, NULL == m_pCopyMapInfo\n" );
			return false;
		}
		m_pCopyMapInfo->MonsterBornNotify( pMonster );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::AddMonsterToExtMap，NULL == m_pNormalMapInfo\n" );		
			return false;
		}

		m_pNormalMapInfo->MonsterBornNotify( pMonster );
	}

	return true;
}

//添加一个怪物到地图上，同时加至块上
bool CMuxMap::AddMonsterToMap( CMonster* pMonster, unsigned short gridX, unsigned short gridY ) 
{ 
	if ( NULL == pMonster )
	{
		D_ERROR( "CMuxMap::AddMonster，怪物指针空\n" );
		return false;
	}

	//物件NPC不需要去考虑障碍点的规则2009.9.9
	if( pMonster->GetNpcType() != 21 )
	{
		if ( !IsGridCanPass( gridX, gridY ) )
		{
			D_ERROR( "CMuxMap::AddMonster，怪物的地图出现点%d,%d can not pass, fail!\n", gridX, gridY );		
			return false;
		}
	}
	
	AddMonsterToExtMap( pMonster, gridX, gridY );	

	if ( pMonster->IsFollowing() )
	{
		//通知AI，怪物开始跟随；
		pMonster->OnStartFollowing( 0 );
		//////////////////////////////////////////////////////////////////////////////////////////////////
		//全部由AI去处理, by dzj, 08.12.18；
		////如果怪物为跟随怪物，且其跟随的玩家正在移动，则通知周围该怪物移动；
		//CPlayer* pFollowingPlayer = pMonster->GetFollowingPlayer();
		//if ( NULL != pFollowingPlayer )
		//{
		//	if ( pFollowingPlayer->IsMoving() )
		//	{
		//		//创建跟随NPC时，玩家正在移动，则马上通知周围人该怪物移动；
		//		CMonsterWrapper::MonsterStartMove( pMonster, pMonster->GetPosX(), pMonster->GetPosY()
		//			, pFollowingPlayer->GetGridTargetX(), pFollowingPlayer->GetGridTargetY(), pMonster->GetSpeed()/*这个速度就用初始速度*/ );
		//	}
		//}
		//全部由AI去处理, by dzj, 08.12.18；
		//////////////////////////////////////////////////////////////////////////////////////////////////
	}

	return true; 
};

//从地图上删去玩家，同时从块中删去
bool CMuxMap::DeletePlayerFromMap( CPlayer* pPlayer, bool isSwitchMap ) 
{ 
	if ( NULL == pPlayer )
	{
		D_ERROR( "CMuxMap::DeletePlayerFromMap，玩家指针空\n" );
		return false;
	}

	//2.从移动列表中删去, bug修改，原做法先从地图上删去，再重设移动状态，会导致重置移动的移动从原点添加失败，然后又将player加入新点，这是一个bug，导致块上残留一个已回收的玩家指针；
	if ( pPlayer->IsInMovingList() )
	{
		DelPlayerFromMovingList( pPlayer );//由于与Timer串行执行，因此不会有(见(09.01.05,FLAG0001))问题，唯一调用DelPlayerFromMovingList的地方；
	}
	pPlayer->ResetMoveStat();

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::DeletePlayerFromMap，NULL == m_pCopyMapInfo\n" );
			return false;
		}
		m_pCopyMapInfo->PlayerLeaveCopy( pPlayer );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::DeletePlayerFromMap，NULL == m_pNormalMapInfo\n" );
			return false;
		}
		m_pNormalMapInfo->DelPlayerFromNormalMap( pPlayer );
	}

	CManMonsterFsm::OnPlayerLeaveMap( pPlayer->GetFullID().wGID, pPlayer->GetFullID().dwPID, isSwitchMap );//通知AI，玩家离开地图；

	return true; 
};

///检测是否为伪副本且已关闭，若已关闭，应发起踢此刚进入者；
bool CMuxMap::CheckIfPCopyClose( CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		return false;
	}

	if ( m_isPCopyAndClose )
	{
		pPlayer->IssuePlayerLeaveCopyHF( true );//发起踢此玩家；
		return true;
	}

	return false;

}

///脚本开启伪副本；
bool CMuxMap::OnPCopyOpen()
{
	//真副本不可能执行到此
	if ( IsInsCopyMap() )
	{
		D_ERROR( "CMuxMap::OnPCopyOpen, 真副本不应执行到此\n" );
		return false;
	}

	if ( NULL == m_pNormalMapInfo )
	{
		D_ERROR( "CMuxMap::OnPCopyOpen, NULL == m_pNormalMapInfo\n" );
		return false;
	}

	m_isPCopyAndClose = false;//是否为伪副本&&已关闭，两项为真时才为真；

	CManMuxCopy::OnPCopyOpen( GetMapNo() );//副本开启

	return true;
}

///脚本关闭伪副本，默认先传地图内所有玩家出副本；
bool CMuxMap::OnPCopyClose()
{
	//真副本不可能执行到此
	if ( IsInsCopyMap() )
	{
		D_ERROR( "CMuxMap::OnPCopyClose, 真副本不应执行到此\n" );
		return false;
	}

	if ( NULL == m_pNormalMapInfo )
	{
		D_ERROR( "CMuxMap::OnPCopyClose, NULL == m_pNormalMapInfo\n" );
		return false;
	}

	m_isPCopyAndClose = true;//是否为伪副本&&已关闭，两项为真时才为真；

	m_pNormalMapInfo->IssueAllPlayerLeavePCopyHF( false );//发起踢所有人上半部，且内部不通知每个人消息，在下面使用广播通知；

	///通知所有人踢出倒计时窗口；
	MGTimeLimit timeLimit;
	timeLimit.limitTime.timeLimit = 60;//副本倒计时永远60秒；
	timeLimit.limitTime.limitReason = 1;//预备踢出副本；
	NotifyAllUseBrocast<MGTimeLimit>( &timeLimit );

	CManMuxCopy::OnPCopyClose( GetMapNo() );//副本关闭，没有人可以再进

	return true;
}

///怪物从细节地图(normalmap/copymap)删除;
bool CMuxMap::DeletePlayerFromExtMap( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMuxMap::DeletePlayerFromExtMap, 输入参数空\n" );
		return false;
	}

	if ( !(pMonster->IsToDel()) )
	{
		D_ERROR( "DeletePlayerFromExtMap，未设置怪物%x的待删标记\n", (unsigned int)pMonster );
		return false;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::DeletePlayerFromExtMap，NULL == m_pCopyMapInfo\n" );
			return false;
		}
		if ( !(m_pCopyMapInfo->DelMonsterFromCopyMap( pMonster )) )
		{
			D_ERROR( "CMuxMap::DeletePlayerFromExtMap，删怪物失败\n" );
			return false;
		}
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::DeletePlayerFromExtMap，NULL == m_pNormalMapInfo\n" );
			return false;
		}
		if ( !(m_pNormalMapInfo->DelMonsterFromNormalMap( pMonster )) )
		{
			D_ERROR( "CMuxMap::DeletePlayerFromExtMap，删怪物失败\n" );
			return false;
		}
	}

	return true;
}

///从地图上删去怪物，同时从块中删去，之前的OnDisappear已调用，此处只删怪物；
bool CMuxMap::DeleteMonsterFromMap( CMonster* pMonster ) 
{ 
	if ( NULL == pMonster )
	{
		D_ERROR( "CMuxMap::DeleteMonster，怪物指针空\n" );
		return false;
	}

	DeletePlayerFromExtMap( pMonster );

	return true; 
};


///取当前地图的移动列表大小；
int CMuxMap::GetMovingPlayerNum()
{
	return (int) ( m_MovingPlayers.size() );
}

bool CMuxMap::GetPosProperty( unsigned short x, unsigned short y, unsigned int& areaID, bool& bPK, bool& bFight, bool& bPreArea )
{
	if ( NULL == m_pMapProperty )
	{
		return false;
	}

	return m_pMapProperty->GetPosProperty( x, y, areaID, bPK, bFight, bPreArea );
}

void CMuxMap::PlayerMoveToNewPos(CPlayer *pMovePlayer, unsigned int orgX, unsigned int orgY, unsigned int newX, unsigned int newY)
{
	if ( !pMovePlayer )
		return;


	//新的点不能被穿越,则无须判断区域属性是否改变了
	if( !IsGridCanPass( newX, newY ) )
	{
		//D_DEBUG("新点(%d,%d)不可通行\n",newX ,newY );
		return;
	}

	unsigned int orgAreaID = 0;
	bool orgbPK = pMovePlayer->GetSelfStateManager().IsCanPvP();
	bool orgbFight = pMovePlayer->GetSelfStateManager().IsCanFight();
	bool orgbPre = pMovePlayer->GetSelfStateManager().IsInDunGeonPreArea(); ;
	GetPosProperty( orgX, orgY, orgAreaID, orgbPK, orgbFight, orgbPre );

	unsigned int newAreaID = 0;
	bool newbPK = orgbPK;
	bool newbFight = orgbFight;
	bool newbPre = orgbPre;
	GetPosProperty( newX, newY, newAreaID, newbPK, newbFight, newbPre );

	//2. 判断是否进入一个新的区域,可能触发区域任务
	if ( orgAreaID != newAreaID )
	{
		if ( orgAreaID < MAX_AREATASK_COUNT )
		{
			pMovePlayer->OnLeaveOldTaskArea();
			OnHandleScriptLeaveArea( orgAreaID, pMovePlayer );
		}

		//如果区域编号是否属于区域任务编号的范围
		if ( newAreaID < MAX_AREATASK_COUNT )
		{
			CNormalScript* pScript = NULL;
			if ( NULL != m_pMapProperty )
			{
				pScript = m_pMapProperty->GetAreaScript(newAreaID);
			}
			
			if ( NULL == pScript )
			{
				pMovePlayer->OnEnterNewTaskArea( NULL );//reset area script;
			} else {
				pMovePlayer->OnEnterNewTaskArea( pScript );
			}

			OnHandleScriptEnterArea( newAreaID, pMovePlayer );
		}
	}

	if ( orgbPK != newbPK )
	{
		//原来是不可PK区域，现在是可PK区域，则状态发生变化
		if ( orgbPK == false && newbPK == true )
			pMovePlayer->OnEnterPKArea();
		else
			pMovePlayer->OnExitPKArea();
	}

	if ( orgbFight != newbFight )
	{
		//如果原来为不可战斗区域，而现在为可战斗区域,状态变化
		if ( orgbFight == false && newbFight == true )
		{
			pMovePlayer->OnEnterFightArea();
		}
		else
		{
			pMovePlayer->OnExitFightArea();
		}
	}

	if ( orgbPre != newbPre )
	{
		if ( orgbPre == false && newbPre == true )
			pMovePlayer->OnEnterDunGeonPreArea();
		else
			pMovePlayer->OnExitDunGeonPreArea();
	}

}

///通知GM当前地图中的当前移动玩家；
void CMuxMap::NotifyGMMovingPlayerInfo( CPlayer* pGMPlayer )
{
	if ( NULL == pGMPlayer )
	{
		return;
	}

	CPlayer* pTmpPlayer = NULL;
	bool isFound = false;

	stringstream syschat;
	syschat << "移动玩家数量:" << m_MovingPlayers.size();

	for ( list<CPlayer*>::iterator iter=m_MovingPlayers.begin(); iter!=m_MovingPlayers.end(); ++iter/*汗，死循环*/ )
	{
		pTmpPlayer = *iter;
		if ( pGMPlayer == pTmpPlayer )
		{
			isFound = true;
			break;
		}
	}
	if ( isFound )
	{
		syschat << ",本玩家在移动列表中"; 
	} else {
		syschat << ",本玩家不在移动列表";
	}
	syschat << "\n";

	pGMPlayer->SendSystemChat( syschat.str().c_str() );

	syschat.str("");

	return;
}

///将玩家从移动玩家列表中删去；
bool CMuxMap::DelPlayerFromMovingList( CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		return false;
	}

	CPlayer* pMovingPlayer = NULL;
	for ( list<CPlayer*>::iterator iter=m_MovingPlayers.begin(); iter!=m_MovingPlayers.end(); ++iter/*汗，死循环*/ )
	{
		pMovingPlayer = *iter;
		if ( pPlayer == pMovingPlayer )
		{
			pMovingPlayer->SetNotInMovingList();//即时从移动列表中删去；
			m_MovingPlayers.erase( iter );
			--m_LastEPos;
			if ( m_LastEPos <= 0 )
			{
				m_LastEPos = 0;
			}
			return true;
		}
	}

	return false;
}


///将玩家加至移动玩家列表；
bool CMuxMap::AddPlayerToMovingList( CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		D_WARNING( "AddPlayerToMovingList,玩家指针空\n", pPlayer->GetAccount() );
		return false;
	}

	if ( pPlayer->IsInMovingList() )
	{
		return true;//玩家已在移动列表；
	}

	//if ( !( pPlayer->m_bIsRobot ) )
	//{
	//	D_DEBUG( "AddPlayerToMovingList,将玩家%s加入移动列表\n", pPlayer->GetAccount() );
	//}		

	m_MovingPlayers.push_back( pPlayer );
	pPlayer->SetInMovingList();

	return true;
};

/////增加对应八方向的新块计算函数，输入：旧块X，旧块Y，新块X， 新块Y，输出：新视野块X，新视野块Y（最多5组，不允许跳块，即，输入必须为邻块）,???,贺轶男检查;
//bool CMuxMap::GetNewViewportBlocks( unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY
//						  ,  bool& isJump/*是否跳块，输入非相邻块*/, unsigned short& viewportBlockNum/*新视野块数目*/, unsigned short outBlockX[5], unsigned short outBlockY[5] )
//{
//	/////跨块移动新视野块,???,贺轶男检查；
//	////1、检查新块与旧块之间的范围是否为1，超过1则出错返回false，因为此时输入的数组大小已经不够，如果特定情况下需要跨块，则需要特殊处理，例如GM命令瞬移时；
//	////2、计算新视野块，并将新块坐标填入输出数组；
//
//	//预先做个判断
//	if( !IsGridValid(orgBlockX, orgBlockY) )
//	{
//		D_WARNING("越界的Org点位 (%d,%d)\n",orgBlockX, orgBlockY );
//		return false;
//	}
//
//	if( !IsGridValid(newBlockX, newBlockY)  )
//	{
//		D_WARNING("越界的new点位 (%d,%d)\n", newBlockX, newBlockY );
//		return false;
//	}
//
//	isJump = true;//初始假设为跳块；
//	viewportBlockNum = 0;
//
//	int nMinX = 0, nMaxX = 0, nMinY = 0, nMaxY = 0;
//	bool bUpdateX = false ,bUpdateY = false;
//	int dx = orgBlockX - newBlockX;
//	int dy = orgBlockY - newBlockY;
//	if( abs(dx) > 1 || abs(dy) > 1)
//	{
//		//D_ERROR( "GetNewViewportBlocks移动跳块, 原块(%d,%d)-->新块(%d,%d)\n", orgBlockX, orgBlockY, newBlockX, newBlockY );
//		return false;
//	}
//
//	isJump = false;//非跳块；
//
//	//8个方向 8种情况
//	if( (dy > 0) 
//		&& ( newBlockY != 0 ) )  //既新格不是最上面一格
//	{
//		bUpdateY = true;
//	}
//
//	if( (dy < 0)
//		&& ( newBlockY != m_blockHeight - 1 ) ) //新格不是最下面一格
//	{
//		bUpdateY = true;
//	}
//
//	if( (dx> 0)
//		&& newBlockX != 0 )		//新格不是最左面的格
//	{
//		bUpdateX = true;
//	}
//
//	if( (dx<0)
//		&& newBlockX !=  m_blockWidth - 1 )
//	{
//		bUpdateX = true;
//	}
//
//	GetValidBlock( newBlockX, newBlockY, 1, nMinX, nMaxX, nMinY, nMaxY );
//
//	//8个方向
//	int i = 0;
//	if( dy == 1 && dx == 0)  //正北
//	{
//		if( bUpdateY )
//		{
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMinY;
//				viewportBlockNum++;
//			}
//		}
//	}
//	else if( dy == -1 && dx == 0)  //正南
//	{
//		if( bUpdateY )
//		{
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMaxY;
//				viewportBlockNum++;
//			}
//		}
//	}
//	else if( dy == 0 && dx == 1)  //正西
//	{
//		if( bUpdateX )
//		{
//			for(unsigned short y = nMinY ; y <= nMaxY; ++i, ++y)
//			{
//				outBlockX[i] = nMinX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//		}
//	}
//	else if( dy == 0 && dx == -1 ) //正东
//	{
//		if( bUpdateX )
//		{
//			for(unsigned short y = nMinY ; y <= nMaxY; ++i, ++y)
//			{
//				outBlockX[i] = nMaxX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//		}
//	}
//	else if( dx == 1 && dy == 1)  //西北
//	{
//		if( bUpdateX && bUpdateY)  //2边都能更新
//		{
//			//先X3座标
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMinY;
//				viewportBlockNum++;
//			}
//
//			//再Y2座标
//			for(unsigned short y = nMinY + 1; y <= nMaxY; ++i, ++y)
//			{
//				outBlockX[i] = nMinX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//
//		}
//		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
//		{
//			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
//			{
//				outBlockX[i] = nMinX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//		}
//		else  //只能更新X轴
//		{
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMinY;
//				viewportBlockNum++;
//			}
//		}
//	}
//	else if( dx == 1 && dy == -1) //西南
//	{
//		if( bUpdateX && bUpdateY)  //2边都能更新
//		{
//			//再更新X轴3个
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMaxY;
//				viewportBlockNum++;
//			}
//
//			//再更新Y轴2个
//			for(unsigned short y = nMinY; y <= nMaxY - 1; ++i, ++y)
//			{
//				outBlockX[i] = nMinX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//		}
//		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
//		{
//			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
//			{
//				outBlockX[i] = nMinX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//		}
//		else  //只能更新X轴
//		{
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMaxY;
//				viewportBlockNum++;
//			}
//		}
//	}
//	else if( dx == -1 && dy == -1) //东南
//	{
//		if( bUpdateX && bUpdateY)  //2边都能更新
//		{
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMaxY;
//				viewportBlockNum++;
//			}
//
//			for(unsigned short y = nMinY; y <= nMaxY - 1; ++i, ++y)
//			{
//				outBlockX[i] = nMaxX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//		}
//		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
//		{
//			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
//			{
//				outBlockX[i] = nMaxX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//		}
//		else  //只能更新X轴
//		{
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMaxY;
//				viewportBlockNum++;
//			}
//		}
//	}
//	else if( dx == -1 && dy == 1) //东北
//	{
//		if( bUpdateX && bUpdateY)  //2边都能更新
//		{
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMinY;
//				viewportBlockNum++;
//			}
//
//			for(unsigned short y = nMinY+1; y <= nMaxY; ++i, ++y)
//			{
//				outBlockX[i] = nMaxX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//		}
//		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
//		{
//			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
//			{
//				outBlockX[i] = nMaxX;
//				outBlockY[i] = y;
//				viewportBlockNum++;
//			}
//		}
//		else  //只能更新X轴
//		{
//			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
//			{
//				outBlockX[i] = x;
//				outBlockY[i] = nMinY;
//				viewportBlockNum++;
//			}
//		}
//	}
//	
//	return true; 
//};

/////取指定块周边块的坐标范围；
//bool CMuxMap::GetBlockNearbyScope( unsigned short blockX, unsigned short blockY, unsigned short& minBlockX, unsigned short& minBlockY, unsigned short& maxBlockX, unsigned short& maxBlockY )
//{
//	if ( !IsBlockValid( blockX, blockY ) )
//	{
//		//点的块坐标不在地图内；
//		return false;
//	}
//
//	minBlockX=blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
//	if ( 0!=blockX )
//	{
//		minBlockX = blockX - 1;
//	}
//	if ( 0!=blockY )
//	{
//		minBlockY = blockY -1;
//	}
//	if ( blockX < m_blockWidth-1 )
//	{
//		maxBlockX = blockX + 1;
//	}
//	if ( blockY < m_blockHeight-1 )
//	{
//		maxBlockY = blockY + 1;
//	}
//
//	return true;
//}

//调整玩家所在块
void CMuxMap::AdjustPlayerBlock( bool isManualSet/*是否手动调，手动调时必须周围全通知到而不是只通知新视野*/, CPlayer* pPlayer, unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY )
{
	if ( NULL == pPlayer )
	{
		D_WARNING( "AdjustPlayerBlock，输入玩家指针空\n" );
		return;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::AdjustPlayerBlock, NULL == m_pCopyMapInfo\n" );
			return;
		}
		return m_pCopyMapInfo->AdjustPlayerBlock( isManualSet, pPlayer, orgBlockX, orgBlockY, newBlockX, newBlockY );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::AdjustPlayerBlock, NULL == m_pNormalMapInfo\n" );
			return;
		}
		return m_pNormalMapInfo->AdjustPlayerBlock( isManualSet, pPlayer, orgBlockX, orgBlockY, newBlockX, newBlockY );
	}

	return;
}

///玩家移动一次，返回值：玩家是否到达目标点(或是否要从移动列表中删去)；
void CMuxMap::PlayerMoveOnce( CPlayer* pMovingPlayer, bool isForceCal/*是否强制计算，而不管是否跨格*/, bool& isAchieve, bool& isNeedNoti )
{
	TRY_BEGIN;

	if ( NULL == pMovingPlayer )
	{
		//不可能到此；
		D_WARNING( "CMuxMap::PlayerMoveOnce，不可能错误\n" );
		return;
	} else {
		//if ( !pMovingPlayer->m_bIsRobot )
		//{
		//	D_DEBUG( "PlayerMoveOnce, 玩家%s\n", pMovingPlayer->GetAccount() );
		//}
	}

	bool isReallyMoveGrid  = false;//移动是否跨格；
	bool isReallyMoveBlock = false;//移动是否跨块；
	isAchieve         = false;//是否到达目标点；

	unsigned short orgBlockX=0, orgBlockY=0, newBlockX=0, newBlockY=0;

 //   unsigned short viewportBlockNum;/*新视野块数目*/
	//unsigned short viewBlockX[5];//新视野块X坐标数组；
	//unsigned short viewBlockY[5];//新视野块Y坐标数组；
	//MuxMapBlock*   pNewBlock = NULL;//新视野临时块；
	//MuxMapBlock*   pOrgBlock = NULL;//旧视野临时块； 

	//unsigned short newGridX=0, newGridY=0;

	bool isInMovingState = pMovingPlayer->TryMove( isNeedNoti/*最迟移动通知算法*/, isReallyMoveGrid, isReallyMoveBlock, isAchieve, orgBlockX, orgBlockY, newBlockX, newBlockY, isForceCal );
	if ( !isInMovingState )
	{
		return;
	}

	if ( isReallyMoveGrid )
	{
		// 是否移动影响交易状态？2008.8.5
		pMovingPlayer->MoveAffectTrade();

		////更新玩家格子坐标；
		//MuxMapSpace::FloatToGridPos( pMovingPlayer->GetFloatPosX(), pMovingPlayer->GetFloatPosY(), newGridX, newGridY );
		////,???,邓子建,移动日志，D_WARNING( "玩家%s从(%d,%d)移动至(%d,%d)\n", pMovingPlayer->GetAccount(), pMovingPlayer->GetPosX(), pMovingPlayer->GetPosY(), newGridX, newGridY );
		//pMovingPlayer->SetPlayerPos( newGridX, newGridY, false );//TryMove时已更新浮点坐标，此处不再重置
		////CMonster* pMovingMonster = pMovingPlayer->GetFollowingMonster();
		////if ( NULL != pMovingMonster )
		////{
		////	//有跟随怪物；
		////	CMonsterWrapper::MonsterBeyondGrid( pMovingMonster, pMovingMonster->GetPosX(), pMovingMonster->GetPosY()
		////		, newGridX, newGridY );
		////}

		////判断是否是骑乘状态移动,
		//if ( pMovingPlayer->IsRideState() )
		//{
		//	pMovingPlayer->CheckIsRideStateMove();
		//}
	}

	//if ( isReallyMoveBlock ) //玩家有跨块移动，进行移动处理；
	//{
	//	AdjustPlayerBlock( pMovingPlayer, orgBlockX, orgBlockY, newBlockX, newBlockY );
	//	////将玩家从旧块上删去；
	//	//pOrgBlock = GetMapBlockByBlock( orgBlockX, orgBlockY );
	//	//if ( NULL != pOrgBlock )
	//	//{
	//	//	pOrgBlock->DeletePlayerFromBlock( pMovingPlayer );
	//	//} else{
	//	//	D_WARNING( "不可能错误，取不到移动玩家%s原来所在块", pMovingPlayer->GetAccount() );
	//	//}

	//	//pNewBlock = GetMapBlockByBlock( newBlockX, newBlockY );
	//	//if ( NULL != pNewBlock )
	//	//{
	//	//	pNewBlock->AddPlayerToBlock( pMovingPlayer );
	//	//} else {
	//	//	D_WARNING( "不可能错误，取不到移动玩家%s新点(%d,%d)所在块(%d,%d)", pMovingPlayer->GetAccount(), newGridX, newGridY, newBlockX, newBlockY );
	//	//}

	//	////将玩家加入新块；
	//	////玩家移动中的定时更新只通知边缘玩家；
	//	////此处为定时更新，因此：
	//	//// 1.找到前方新进入视野小块玩家；
	//	//// 2.通知这些玩家本玩家的移动消息，同时通知本玩家这些新玩家的相应信息；
	//	//// 3.通知前方新进入视野小块怪物，同时通知本玩家这些新怪物的信息；
	//	//if(  !GetNewViewportBlocks( orgBlockX, orgBlockY, newBlockX, newBlockY, viewportBlockNum, viewBlockX, viewBlockY ) )//新进入视野块；
	//	//{
	//	//	D_WARNING("PlayerMoveOnce 输入点非法\n");
	//	//	return;
	//	//}
	//
	//	//for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
	//	//{
	//	//	pNewBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
	//	//	if ( NULL != pNewBlock )
	//	//	{
	//	//		pNewBlock->PlayerMoveNewBlockNotify( pMovingPlayer );//新进入视野块处理；
	//	//	}
	//	//}


	//	//if( !GetNewViewportBlocks( newBlockX, newBlockY, orgBlockX, orgBlockY, viewportBlockNum, viewBlockX, viewBlockY ) )//旧离开视野块；
	//	//{
	//	//	D_WARNING("PlayerMoveOnce 输入点非法\n");
	//	//	return;
	//	//}

	//	//for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
	//	//{
	//	//	pNewBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
	//	//	if ( NULL != pNewBlock )
	//	//	{
	//	//		pNewBlock->PlayerMoveOrgBlockNotify( pMovingPlayer );//旧离开视野块处理；
	//	//	}
	//	//}
	//}

	return;
	TRY_END;
	return;
}

///定时更新移动玩家的位置；
void CMuxMap::MovingPlayerTimer()
{
	TRY_BEGIN;

	CPlayer* pMovingPlayer = NULL;
	bool isAchieve         = false;//是否到达目标点；

	if ( m_MovingPlayers.empty() )
	{
		return;
	}

	bool isNeedNotiMove = false;

	//若为副本服务器，则直接遍历全部移动玩家，不再执行小循环自适应，该自适应由副本房间执行；
	if ( CLoadSrvConfig::IsCopyMapSrv() )
	{
		//副本中玩家本来就不多，因此直接全部刷新，但遍历哪个副本，由调用者决定；
		for ( list<CPlayer*>::iterator iter = m_MovingPlayers.begin(); iter!=m_MovingPlayers.end(); )
		{
			isNeedNotiMove = false;
			pMovingPlayer = *iter;
			if ( NULL == pMovingPlayer )
			{
				D_ERROR( "副本移动玩家遍历，NULL == pMovingPlayer\n" );
				iter = m_MovingPlayers.erase( iter );
				continue;
			}

			//有效的移动玩家；
			if ( !(pMovingPlayer->IsMoving()) )
			{
				//玩家不处于移动状态，两次循环间停止了移动，例如被打死了，因为被打死有可能发生在循环中，因此不能在打死时立刻从移动列表中删去，以免破坏迭代器，而只是清移动信息，等待下次遍历此列表时安全删去。
				pMovingPlayer->SetNotInMovingList();//从移动列表中删去；
				pMovingPlayer->ResetMoveStat();
				iter = m_MovingPlayers.erase( iter );
				continue;
			}

			PlayerMoveOnce( pMovingPlayer, false/*不跨格就不更新玩家位置*/, isAchieve, isNeedNotiMove );
#ifdef LATEST_MOVE_NOTI //只有使用了最迟移动通知，才可能会在本Timer中检测是否需要广播；
			if ( isNeedNotiMove )
			{
				PlayerNewMovePlayerBrocast( pMovingPlayer, false/*修改相关标志，用于后续通知算法*/ );//最迟移动通知算法，两个可能通知点:1新移动时；2移动时钟(玩家发来移动跨块触发同样计算)发现新旧点计算差距太大时;
			}
#endif //LATEST_MOVE_NOTI

			if ( isAchieve )
			{
				//D_DEBUG( "玩家%s到达(%d,%d)\n", pMovingPlayer->GetAccount(), pMovingPlayer->GetPosX(), pMovingPlayer->GetPosY() );
				pMovingPlayer->SetNotInMovingList();//即时从移动列表中删去；
				iter = m_MovingPlayers.erase( iter );
				continue;
			}

			++iter;//刷新下一移动玩家的位置；
		}//for ( list<MovingPlayer*>...
		return; //副本移动玩家遍历；
	}

	//以下普通地图移动玩家遍历；

	if ( m_LastEPos >= (int)(m_MovingPlayers.size()) )
	{
		m_LastEPos = 0;
	}

#ifdef AUTO_EXP_UNIT
	static ACE_Time_Value lastBigLoopTime = ACE_OS::gettimeofday();
	static int            smallLoopNum = 1;//各小循环的每次遍历数，自适应调整，以小值开始启动；
	static int            bigLoopExped = 0;//大循环中已逝去小循环的已遍历数；
	int passedmsec = (ACE_OS::gettimeofday() - lastBigLoopTime).msec();//大循环已逝去毫秒数；
	int numpere = smallLoopNum;//本次遍历数，将在AutoExpNumCal中被修改；
	bool isUpdateLastBigLoopTime = false;//是否更新大循环起始时刻；
	AutoExpNumCal( passedmsec, (const int)m_MovingPlayers.size(), smallLoopNum, bigLoopExped, numpere, isUpdateLastBigLoopTime );		
	if ( isUpdateLastBigLoopTime )
	{
		lastBigLoopTime = ACE_OS::gettimeofday();
	}
#else //AUTO_EXP_UNIT
	int numpere = (int)(m_MovingPlayers.size()) / EXPLORE_UNIT_PROP + 1;//每次遍历怪物总数的1/4;
#endif //AUTO_EXP_UNIT
	int numcure = 0;//当前遍历的玩家数；

	list<CPlayer*>::iterator iter = m_MovingPlayers.begin();
	advance( iter, m_LastEPos );	
	if ( iter == m_MovingPlayers.end() )
	{
		iter = m_MovingPlayers.begin();
		m_LastEPos = 0;
	}

	//DWORD stTick = GetTickCount();

	for ( ; iter!=m_MovingPlayers.end(); )
	{
		if ( numcure >= numpere )
		{
			break;//已遍历超过1/4
		}

		++numcure;//本次已遍历的移动玩家数；
		++m_LastEPos;//最后遍历的元素位置；

		isNeedNotiMove = false;
		pMovingPlayer = *iter;
		if ( NULL == pMovingPlayer )
		{
			D_ERROR( "普通地图移动玩家遍历，NULL == pMovingPlayer\n" );
			iter = m_MovingPlayers.erase( iter );
			continue;
		}
		//if ( !(pMovingPlayer->m_bIsRobot) )
		//{
		//	static ACE_Time_Value dzjlast = ACE_OS::gettimeofday();
		//	int dzjmsec = (ACE_OS::gettimeofday() - dzjlast).msec();//大循环已逝去毫秒数；
		//	dzjlast = ACE_OS::gettimeofday();
		//	D_DEBUG( "遍历间隔时间:%d\n", dzjmsec );
		//}

		if ( !(pMovingPlayer->IsMoving()) )
		{
			//玩家不处于移动状态，两次循环间停止了移动，例如被打死了，因为被打死有可能发生在循环中，因此不能在打死时立刻从移动列表中删去，以免破坏迭代器，而只是清移动信息，等待下次遍历此列表时安全删去。
			pMovingPlayer->SetNotInMovingList();//从移动列表中删去；
			pMovingPlayer->ResetMoveStat();
			iter = m_MovingPlayers.erase( iter );
			--m_LastEPos;
			if ( m_LastEPos <= 0 )
			{
				m_LastEPos = 0;
			}
			continue;
		}

		PlayerMoveOnce( pMovingPlayer, false/*不跨格就不更新玩家位置*/, isAchieve, isNeedNotiMove );

#ifdef LATEST_MOVE_NOTI //只有使用了最迟移动通知，才可能会在本Timer中检测是否需要广播；
		if ( isNeedNotiMove )
		{
			PlayerNewMovePlayerBrocast( pMovingPlayer, false/*修改相关标志，用于后续通知算法*/ );//最迟移动通知算法，两个可能通知点:1新移动时；2移动时钟(玩家发来移动跨块触发同样计算)发现新旧点计算差距太大时;
		}
#endif //LATEST_MOVE_NOTI

		if ( isAchieve )
		{
			//已经到达目标点；
			//if ( !( pMovingPlayer->m_bIsRobot ) )
			//{
			//	D_DEBUG( "MovingPlayerTimer,将玩家%s从移动列表中删去\n", pMovingPlayer->GetAccount() );
			//}	
			//D_DEBUG( "玩家%s到达(%d,%d)\n", pMovingPlayer->GetAccount(), pMovingPlayer->GetPosX(), pMovingPlayer->GetPosY() );
			pMovingPlayer->SetNotInMovingList();//即时从移动列表中删去；
			iter = m_MovingPlayers.erase( iter );
			--m_LastEPos;
			if ( m_LastEPos <= 0 )
			{
				m_LastEPos = 0;
			}
			//,???,邓子建,移动日志，D_WARNING( "MovingPlayerTimer，玩家%s到达目标点，从movinglist中删去\n", pMovingPlayer->GetAccount() );
			continue;
		}

		++iter;//刷新下一移动玩家的位置；
	}//for ( list<MovingPlayer*>...

	//if ( GetTickCount() - stTick > 61 )
	//{
	//	D_DEBUG( "移动时钟费时%d\n", GetTickCount()-stTick );		
	//	__asm int 3;		
	//}
	//if ( GetTickCount() - stTick > 60 )
	//{
	//	D_DEBUG( "玩家移动时钟费时%d\n", GetTickCount() - stTick );
	//}

	return;
	TRY_END;
	return;
}

///玩家试图移动，可以移动时返回真，不可移动返回假;
bool CMuxMap::PlayerTryNewMove( CPlayer* pMovePlayer, float fCurX, float fCurY, float fTargetX, float fTargetY )
{
	TRY_BEGIN;

	if ( NULL == pMovePlayer )
	{
		D_ERROR( "CMuxMap::PlayerTryNewMove, NULL == pMovePlayer\n" );
		return false;
	}

	pMovePlayer->CheckInvincible();

	if ( pMovePlayer->GetSpeed() <= 0 )
	{
		D_WARNING( "玩家%s的移动速度(%f)小于等于0，移动失败\n", pMovePlayer->GetAccount(), pMovePlayer->GetSpeed() );
		return false;
	}

	if ( pMovePlayer->GetCastManager().IsInCast() )
	{
		pMovePlayer->GetCastManager().OnStop();
	}

	//1、玩家是否有效?由于调用之前在dealpkg中已经有了判断，因此这里不再判断；

	//2、新位置是否有效?验证新位置的可靠性,消息中的nOrgMapPos是玩家本次移动移至的当前点；
	unsigned short orgTargetGridX=0, orgTargetGridY=0;
	MuxMapSpace::FloatToGridPos( pMovePlayer->GetFloatTargetX(), pMovePlayer->GetFloatTargetY(), orgTargetGridX, orgTargetGridY );
	
	unsigned short gridX=0, gridY=0;
	MuxMapSpace::FloatToGridPos( fTargetX, fTargetY, gridX, gridY );
	if ( !IsGridValid( gridX, gridY ) )
	{
		D_WARNING("玩家移动PlayerTryNewMove目标点无效 (%d,%d) \n", gridX, gridY );
		return false;
	}

	//如果新目标点所在格与原格相同，则不处理本次移动；
	if ( ( orgTargetGridX==gridX )
		&& ( orgTargetGridY==gridY )
		)
	{
		D_INFO( "PlayerTryNewMove，%s本次移动目标点(%f,%f)与原目标点(%f,%f)在同一格，忽略\n", pMovePlayer->GetAccount(), fTargetX, fTargetY, pMovePlayer->GetFloatTargetX(), pMovePlayer->GetFloatTargetY() );
		return false;
	}

	bool isAchieve = false;
	bool invalidIsNeedNoti = false;//无用的通知标记，因为肯定不会在此时通知;
	if ( pMovePlayer->IsMoving() )
	{
		PlayerMoveOnce( pMovePlayer, true, isAchieve, invalidIsNeedNoti );//移动之前，先把上次的移动完，计算下准确的最新位置，并更新最近移动时间；
	}

	if ( isAchieve )
	{
		//,???,邓子建,移动日志，D_WARNING( "PlayerTryNewMove，玩家%s到达目标点，马上有新移动，不从movinglist中删去\n", pMovePlayer->GetAccount() );
	}

	//3、如果指定的出发点与玩家当前位置有差异，则拉玩家，拉玩家时必须通知到原位置以及新位置周围的玩家，具体算法,???,邓子建检查；
	float errX = fabs( fCurX - pMovePlayer->GetFloatPosX() );
	float errY = fabs( fCurY - pMovePlayer->GetFloatPosY() );
	float curError = errX*errX + errY*errY;

	if ( curError > 3*3 )
	{
		//拉玩家(纠正玩家位置)；
		if ( curError > 2*3*3 ) //如果客户端连续点地图，有时会出现一定偏差，此时纠正下服务器以尽量保持同步。该现象的具体原因待查，但无论如何不应该相差太大，相差太大时打错误日志
		{
			D_WARNING( "cli%s(%f,%f)与服务器(%f,%f)位置差异大于根号3\n", pMovePlayer->GetAccount(), fCurX, fCurY, pMovePlayer->GetFloatPosX(), pMovePlayer->GetFloatPosY() );
			//差距超过一定程度时应返回失败，防止被利用加速；
		}
		pMovePlayer->ForceSetFloatPos( true/*因为是强制拉*/, fCurX, fCurY );
	}

#ifdef HAS_PATH_VERIFIER
	if (!IsPassOK(fCurX, fCurY, fTargetX, fTargetY))
	{
		D_ERROR( "IsPassOK()检测失败, 玩家%s移动路径(%d,%d)-->(%d,%d)上有障碍点,忽略此移动\n"
			, pMovePlayer->GetNickName(), fCurX, fCurY, fTargetX, fTargetY );
		pMovePlayer->ResetMoveStat();
		pMovePlayer->ForceSetFloatPos( true/*移动失败，停在当前点，需要通知全部周围人移动状态改变*/, pMovePlayer->GetFloatPosX(), pMovePlayer->GetFloatPosY() );
		return false;
	}

	if(!BlockTestOK(fCurX, fCurY, fTargetX, fTargetY))
	{
		D_ERROR( "BlockTestOK()检测失败, 玩家%s移动路径(%d,%d)-->(%d,%d),忽略此移动\n"
			, pMovePlayer->GetNickName(), fCurX, fCurY, fTargetX, fTargetY );
		unsigned long mapID;
		MuxPoint pt;
		bool isOK = GetOneRebirthPt(mapID, pt, (unsigned char)pMovePlayer->GetRace());
		if(isOK)
		{
			OnPlayerInnerJump( pMovePlayer, pt.nPosX, pt.nPosY );
		}
		else
		{
			D_ERROR("PlayerTryNewMove,玩家%s获取新位置失败\n", pMovePlayer->GetAccount());
			return false;
		}
	}
#endif/*HAS_PATH_VERIFIER*/

	//4、新的目标点是否与原目标点足够一致，如果相差不大则不作任何处理直接返回，此处的算法,???,邓子建检查

	//5、为玩家赋新的移动信息；

	bool isNeedNotiMove = false;//最迟移动通知算法；

	//位置调试，D_DEBUG( "玩家新移动:(%f,%f)-->(%f,%f),速度:%f\n", pMovePlayer->GetFloatPosX(), pMovePlayer->GetFloatPosY(), fTargetX, fTargetY, pMovePlayer->GetSpeed() );

	bool isMoveOk = pMovePlayer->SetMoveInfo( pMovePlayer->GetSpeed(), fTargetX, fTargetY, isNeedNotiMove );
	if ( !isMoveOk )
	{
		D_WARNING( "玩家%s SetMoveInfo失败\n", pMovePlayer->GetAccount() );
		//如果这时玩家已经achieve，则本来应该从moving list中删去，但实示上没有关系，因为会在下次move循环时删去，或者玩家下线时强行删去
		return false;
	}

#ifndef LATEST_MOVE_NOTI
	isNeedNotiMove = true; //如果不使用最迟移动通知，则新移动时永远需要立刻广播;
#endif //LATEST_MOVE_NOTI

	//7、通知玩家周围8小块，本玩家移动消息；
	if ( isNeedNotiMove )
	{
		//如果!isNeedNotiMove，则肯定玩家之前已在移动列表中，无需再加入一次;
		AddPlayerToMovingList( pMovePlayer );//将玩家加入移动玩家列表，以便定时更新此玩家位置；
		PlayerNewMovePlayerBrocast( pMovePlayer, false/*修改相应标记，用于后续移动通知算法*/ );//最迟移动通知算法，两个可能通知点:1新移动时；2移动时钟(玩家发来移动跨块触发同样计算)发现新旧点计算差距太大时;		
	} else {
		//if ( !pMovePlayer->m_bIsRobot )
		//{
		//	D_DEBUG( "%s try 新移动，isNeedNotiMove为false，不通知该新移动\n", pMovePlayer->GetAccount() );
		//}
	}

	//9、玩家移动事件通知怪物,不需要，在玩家位置真正发生变化时通知, 
	//    modified by dzj, 09.01.12，对怪物总是在开始移动时通知一次，并在以后每次跨块时通知（邻块与当前块），而不延后通知；
	PlayerNewMoveMonsterBrocast( pMovePlayer );

	return true;
	TRY_END;
	return false;
}

///向周围怪物广播玩家的新移动消息；
void CMuxMap::PlayerNewMoveMonsterBrocast( CPlayer* pMovePlayer )
{
	TRY_BEGIN;

	if ( NULL == pMovePlayer )
	{
		D_ERROR( "CMuxMap::PlayerNewMoveMonsterBrocast, 输入参数空\n" );
		return;
	}

	if ( IsInsCopyMap() )
	{
		//副本
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::PlayerNewMoveMonsterBrocast, NULL == m_pCopyMapInfo\n" );
			return;
		}
		m_pCopyMapInfo->PlayerNewMoveMonsterBrocast( pMovePlayer );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::PlayerNewMoveMonsterBrocast, NULL == m_pNormalMapInfo\n" );
			return;
		}
		m_pNormalMapInfo->PlayerNewMoveMonsterBrocast( pMovePlayer );
	}

	return;
	TRY_END;
	return;
}

///向周围玩家广播玩家的新移动消息；
void CMuxMap::PlayerNewMovePlayerBrocast( CPlayer* pMovePlayer, bool isPureNotify/*纯粹通知，不作任何标志位的修改*/ )
{
	TRY_BEGIN;

	if ( NULL == pMovePlayer )
	{
		return;
	}

	if ( !isPureNotify )
	{
		//如果!isNeedNotiMove,则之前怪物肯定也已经得到了玩家的移动消息，无需再通知一次；
		CMonster* pMovingMonster = pMovePlayer->GetFollowingMonster();//每次新通知周围玩家时都再通知一次跟随的怪物，???,邓子建检查,以后修改。
		if ( NULL != pMovingMonster )
		{
			//有跟随怪物；
			pMovingMonster->OnFollowPlayerStartMoving();
		}
	}

	MGRun returnMsg;
	returnMsg.fCurWorldPosX = pMovePlayer->GetFloatPosX();//原浮点坐标；
	returnMsg.fCurWorldPosY = pMovePlayer->GetFloatPosY();
	returnMsg.fTargetWorldPosX = pMovePlayer->GetFloatTargetX();//此处是否需要通知原坐标，因为客户端显示不能跳跃，如果位置发生了偏差，则客户端会自动转向将玩家拉过去,???,邓子建检查；
	returnMsg.fTargetWorldPosY = pMovePlayer->GetFloatTargetY();
	returnMsg.lPlayerID = pMovePlayer->GetFullID();
	returnMsg.fSpeed = pMovePlayer->GetSpeed();
	returnMsg.nErrCode = GCRun::RUNRST_OK;
	NotifySurrounding<MGRun>( &returnMsg, pMovePlayer->GetPosX(), pMovePlayer->GetPosY() );

	if ( !isPureNotify )
	{
		pMovePlayer->SetMoveNotifiedStat();//保存已通知的移动信息；
	}

	//D_INFO( "广播,玩家%s(%f,%f),目标(%f,%f)\n\n", pMovePlayer->GetAccount(), returnMsg.fCurWorldPosX, returnMsg.fCurWorldPosY, returnMsg.fTargetWorldPosX, returnMsg.fTargetWorldPosY );

	return;
	TRY_END;
	return;
}

/////怪物出生,调用时怪物已加入地图；
//bool CMuxMap::MonsterBornNotify( CMonster* pBornMonster, unsigned short bornGridX, unsigned short bornGridY )
//{
//	TRY_BEGIN;
//
//	if ( NULL == pBornMonster )
//	{
//		D_ERROR( "CMuxMap::MonsterBornNotify，monster指针空\n" );
//		return false;
//	}
//
//	if ( !IsGridValid( bornGridX, bornGridY ) )
//	{
//		D_ERROR( "CMuxMap::MonsterBornNotify，monster出生点%d,%d，超出范围或为不可移动点\n", bornGridX, bornGridY );
//		return false;
//	}
//
//	unsigned short bornBlockX=0, bornBlockY=0;
//	MuxMapSpace::GridToBlockPos( bornGridX, bornGridY, bornBlockX, bornBlockY );//怪物出现点块坐标；
//	MuxMapBlock* pBornBlock = GetMapBlockByBlock( bornBlockX, bornBlockY );
//	if ( NULL == pBornBlock )
//	{
//		D_ERROR( "CMuxMap::MonsterBornNotify，不可能错误，monster出生点%d,%d无对应地图块\n", bornGridX, bornGridY );
//		return false;
//	}
//
//	//2、相关影响块处理；
//	unsigned short minBlockX = bornBlockX, minBlockY=bornBlockY, maxBlockX=bornBlockX, maxBlockY=bornBlockY;
//	if ( !GetBlockNearbyScope( bornBlockX, bornBlockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	{		
//		return false;//输入点为无效点；
//	}
//
//	MuxMapBlock* pTmpBlock = NULL;
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pTmpBlock = GetMapBlockByBlock( x, y );
//			if ( NULL != pTmpBlock )
//			{
//				pTmpBlock->MonsterBornBlockNotify( pBornMonster );
//			}			
//		}
//	}
//
//	return true;
//	TRY_END;
//	return false;
//}

///真正从地图上，地图块内删去怪物，由地图时钟调用；
bool CMuxMap::FinalDelMonster( CMonster* pMonsterToDel )
{
	TRY_BEGIN;

	if ( NULL == pMonsterToDel )
	{
		return false;
	}

	CCreatorIns* pCreator = pMonsterToDel->GetOwnCreator();
	if ( NULL != pCreator ) 
	{
		//如果生成器非空，则检测补充
		pCreator->OnMonsterDelSupply( pMonsterToDel );
	}

	DeleteMonsterFromMap( pMonsterToDel );//将怪物从地图上删去；

	return true;
	TRY_END;
	return false;
}

///怪物在地图上消失
bool CMuxMap::OnMonsterDisappear( CMonster* pDisappearMonster, GCMonsterDisappear::E_DIS_TYPE disType )
{
	TRY_BEGIN;

	if ( NULL == pDisappearMonster )
	{
		return false;
	}

	if ( pDisappearMonster->IsToDel() )
	{
		D_ERROR( "重复OnMonsterDisappear, 怪物%x, disType:%d\n", (unsigned int)pDisappearMonster, disType );
		return false;
	}

	if ( pDisappearMonster->GetMap() != this )
	{
		D_ERROR( "OnMonsterDisappear, 欲删怪物%x不属于本地图(mapID:%d)\n", (unsigned long)pDisappearMonster, GetMapNo() );
		return false;
	}

	unsigned short nPosX,nPosY;
	pDisappearMonster->GetPos( nPosX,nPosY );

	//通知周围玩家:怪物从地图上消失；
	MGMonsterDisappear disappearMsg;
	disappearMsg.lMonsterID = pDisappearMonster->GetMonsterID();
	disappearMsg.disType = disType;
	NotifySurrounding<MGMonsterDisappear>( &disappearMsg, nPosX, nPosY );

	MonsterLeaveViewNotify( pDisappearMonster );

	bool isUseManMonster = !(IsInsCopyMap());//是否使用CManMonster,普通地图使用，副本不使用;
	if ( isUseManMonster )
	{
		CManMonster::ReleaseManedMonster( pDisappearMonster );
	} else {
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::OnMonsterDisappear, 副本地图 NULL == m_pCopyMapInfo\n" );
			return false;
		}
		m_pCopyMapInfo->IssueMonsterDelete( pDisappearMonster );
	}

	return true;
	TRY_END;
	return false;
}

//怪物开始移动
bool CMuxMap::OnMonsterStartMove( CMonster* pMoveMonster, TYPE_POS gridCurX, TYPE_POS gridCurY, TYPE_POS gridTargetX, TYPE_POS gridTargetY, float fSpeed )
{
	TRY_BEGIN;

	if ( NULL == pMoveMonster )
	{
		D_WARNING( "CMuxMap::OnMonsterStartMove，传入怪物指针空\n" );
		return false;
	}

	//先检查地图里是否有这个怪物	
	if( !IsValidMonsterPtrInMap( pMoveMonster ) )
	{
		D_WARNING("CMuxMap::OnMonsterStartMove，地图上找不到欲移动的怪物，ID:%x\n", pMoveMonster->GetMonsterID());
		return false;
	}

	unsigned short orgGridPosX = 0, orgGridPosY = 0;
	pMoveMonster->GetPos( orgGridPosX, orgGridPosY );

	if ( ( orgGridPosX != gridCurX ) 
		|| ( orgGridPosY != gridCurY )
		)
	{
		D_WARNING( "OnMonsterStartMove，取得当前点(%d,%d)不等于传入当前点(%d,%d)\n", orgGridPosX, orgGridPosY, gridCurX, gridCurY );
	}

	//检查怪物是否为相邻格移动；
	int dx = abs( gridTargetX - orgGridPosX );
	int dy = abs( gridTargetY - orgGridPosY );
	
	if ( dx + dy <= 0 )//同一格内不需要移动；
	{
		D_WARNING( "CMuxMap::OnMonsterStartMove 怪物在同一格内移动\n" );
		//return false;
	}

	//如果怪物原来已经在动，则检查新目标点是否==原目标点，如果是，则忽略此移动；
	if ( pMoveMonster->IsMonsterMoving() )
	{
		if ( ( gridTargetX == pMoveMonster->GetTargetX() )
			&& ( gridTargetY == pMoveMonster->GetTargetY() )
			)
		{
			D_WARNING( "怪物移动目标点与原目标点相同，忽略此次移动\n" );
			return false;
		}
	}

	//位置调试，D_DEBUG( "怪物开始移动：(%d,%d)-->(%d,%d)，速度:%f\n", orgGridPosX, orgGridPosY, gridTargetX, gridTargetY, fSpeed );

	//更新怪物身上的移动信息； 
	pMoveMonster->SetSpeed( fSpeed );
	pMoveMonster->SetMonsterMovInfo( gridTargetX, gridTargetY );

	NotifySurrounding<MGMonsterMove>( pMoveMonster->GetMonsterMove(), orgGridPosX, orgGridPosY );

	return true;
	TRY_END;
	return false;
}

//怪物移动中跨格(以MonsterStartMove为前提)
bool CMuxMap::OnMonsterBeyondGrid( CMonster* pMoveMonster, TYPE_POS gridOrgX, TYPE_POS gridOrgY, TYPE_POS gridNewX, TYPE_POS gridNewY
								  , bool isForceAndNotNotiMove/*是否强制置位置且不通知周围此移动*/ )
{
	TRY_BEGIN;

	if ( NULL == pMoveMonster )
	{
		D_WARNING( "CMuxMap::OnMonsterBeyondGrid，传入怪物指针空\n" );
		return false;
	}

	//先检查地图里是否有这个怪物
	if( !IsValidMonsterPtrInMap( pMoveMonster ) )
	{
		D_WARNING("CMuxMap::OnMonsterBeyondGrid，地图上找不到欲移动的怪物，ID:%x\n", pMoveMonster->GetMonsterID());
		return false;
	}

	if ( !IsGridValid( gridNewX, gridNewY ) )
	{
		//don't check if grid can pass, monster can pass every valid grid;
		D_WARNING( "OnMonsterBeyondGrid，monster:%d, map:%d, target grid(%d,%d) is not valid，忽略此次移动\n"
			, pMoveMonster->GetCreatureID().dwPID, GetMapNo(), gridNewX, gridNewY );
		return false;
	}

	unsigned short orgGridPosX = 0, orgGridPosY = 0;
	pMoveMonster->GetPos( orgGridPosX, orgGridPosY );

	if ( !isForceAndNotNotiMove )
	{
		//检查怪物的移动状态
		if ( !(pMoveMonster->IsMonsterMoving()) )
		{
			D_WARNING( "怪物跨格时，不处于移动状态，忽略此次移动\n" );
			return false;
		}

		if ( ( orgGridPosX != gridOrgX ) 
			|| ( orgGridPosY != gridOrgY )
			)
		{
			D_WARNING( "OnMonsterBeyondGrid，取得当前点(%d,%d)不等于传入当前点(%d,%d)\n", orgGridPosX, orgGridPosY, gridOrgX, gridOrgY );
		}

		//检查怪物是否为相邻格移动；
		int dx = abs( gridNewX - orgGridPosX );
		int dy = abs( gridNewY - orgGridPosY );

		if ( dx + dy <= 0 )//同一格内不需要移动；
		{
			D_WARNING( "CMuxMap::OnMonsterBeyondGrid 怪物在同一格内移动\n" );
			return false;
		}		
		if ( dx+dy>6 )
		{
			D_WARNING( "怪物移动跨格太多：(%d,%d)-->(%d,%d)\n", orgGridPosX, orgGridPosY, gridNewX, gridNewY );
		}
	}

	//位置调试，D_DEBUG( "怪物跨格：(%d,%d)-->(%d,%d)，速度:%f\n", orgGridPosX, orgGridPosY, gridNewX, gridNewY, pMoveMonster->GetSpeed() );

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::OnMonsterBeyondGrid，NULL == m_pCopyMapInfo\n" );
			return false;
		}
		return m_pCopyMapInfo->AdjustMonsterPos( pMoveMonster, orgGridPosX, orgGridPosY, gridNewX, gridNewY, isForceAndNotNotiMove );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::OnMonsterBeyondGrid，NULL == m_pNormalMapInfo\n" );
			return false;
		}
		return m_pNormalMapInfo->AdjustMonsterPos( pMoveMonster, orgGridPosX, orgGridPosY, gridNewX, gridNewY, isForceAndNotNotiMove );
	}

	return true;
	TRY_END;
	return false;
}

//怪物停止移动(以MonsterStartMove为前提)
bool CMuxMap::OnMonsterStopMove( CMonster* pStopMonster )
{
	TRY_BEGIN;

	if ( NULL == pStopMonster )
	{
		D_WARNING( "CMuxMap::OnMonsterStopMove，传入怪物指针空\n" );
		return false;
	}

	//先检查地图里是否有这个怪物
	if( !IsValidMonsterPtrInMap( pStopMonster ) )
	{
		D_WARNING("CMuxMap::OnMonsterStopMove，地图上找不到欲停止的怪物，ID:%x\n", pStopMonster->GetMonsterID());
		return false;
	}

	//检查怪物的移动状态
	if ( !( pStopMonster->IsMonsterMoving() ) )
	{
		D_WARNING( "怪物停止时，不处于移动状态，忽略此次移动\n" );
		return false;
	}

	unsigned short orgGridPosX = 0, orgGridPosY = 0;
	pStopMonster->GetPos( orgGridPosX, orgGridPosY );

	//位置调试，D_DEBUG( "怪物停止移动：(%d,%d)\n", orgGridPosX, orgGridPosY );

	//重置怪物身上的移动信息； 
	pStopMonster->ResetMonsterMovInfo();	

	NotifySurrounding<MGMonsterMove>( pStopMonster->GetMonsterMove(),orgGridPosX, orgGridPosY  );

	return true;
	TRY_END;
	return false;
}

/////怪物移动处理，由AI调用；
//bool CMuxMap::OnMonsterMove(CMonster* pMoveMonster,int newGridX, int newGridY, int tgtGridX, int tgtGridY ) 
//{ 
//	TRY_BEGIN;
//
//	if ( NULL == pMoveMonster )
//	{
//		D_WARNING( "CMuxMap::OnMonsterMove，传入怪物指针空\n" );
//		return false;
//	}
//
//	//先检查地图里是否有这个怪物
//	if( ! IsMonsterInMap( pMoveMonster ) )
//	{
//		D_WARNING("CMuxMap::OnMonsterMove，地图上找不到欲移动的怪物，ID:%x\n", pMoveMonster->GetMonsterID());
//		return false;
//	}
//
//	unsigned short orgGridPosX = 0, orgGridPosY = 0;
//	pMoveMonster->GetPos( orgGridPosX, orgGridPosY );
//
//	MuxMapBlock* pOrgBlock = GetMapBlockByGrid( orgGridPosX, orgGridPosY );
//	if ( !IsGridCanPass( newGridX, newGridY ) )
//	{
//		D_WARNING( "怪物移动目标点X:%d,Y:%d不可行走\n", newGridX, newGridY );
//		return false;
//	}
//	MuxMapBlock* pNewBlock = GetMapBlockByGrid( newGridX, newGridY );
//	if ( NULL == pNewBlock )
//	{
//		D_WARNING( "怪物移动目标点X:%d,Y:%d块指针空\n", newGridX, newGridY );
//		return false;
//	}
//
//	//检查怪物是否为相邻格移动；
//	int dx = abs( newGridX - orgGridPosX );
//	int dy = abs( newGridY - orgGridPosY );
//	
//	if ( dx + dy <= 0 )//同一格内不需要移动；
//	{
//		D_WARNING( "怪物在同一格内移动\n" );
//		return false;
//	}		
//	if ( dx>1 || dy>1 )//不可跨格移动；
//	{
//		D_WARNING( "怪物跨格移动\n" );
//		return false;
//	}
//
//	//更新怪物身上保存的位置信息； 
//	pMoveMonster->SetPos( newGridX, newGridY );
//
//	if ( pOrgBlock != pNewBlock ) //怪物跨块移动；
//	{		
//		pOrgBlock->DeleteMonsterFromBlock( pMoveMonster );//将怪物从旧块上删去；
//		pNewBlock->AddMonsterToBlock( pMoveMonster );//怪物加入新块；
//
//		unsigned short viewportBlockNum;/*新视野块数目*/
//		unsigned short viewBlockX[5];//新视野块X坐标数组；
//		unsigned short viewBlockY[5];//新视野块Y坐标数组；
//		MuxMapBlock* pTmpBlock = NULL;
//		//处理新进入视野块；
//		GetNewViewportBlocks( pOrgBlock->GetBlockX(), pOrgBlock->GetBlockY(), pNewBlock->GetBlockX(), pNewBlock->GetBlockY(), viewportBlockNum, viewBlockX, viewBlockY );//新进入视野块；
//		for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
//		{
//			pTmpBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
//			if ( NULL != pNewBlock )
//			{
//				pTmpBlock->MonsterMoveNewBlockNotify( pMoveMonster );
//			}
//		}
//		//处理旧离开视野块；
//		GetNewViewportBlocks( pNewBlock->GetBlockX(), pNewBlock->GetBlockY(), pOrgBlock->GetBlockX(), pOrgBlock->GetBlockY(), viewportBlockNum, viewBlockX, viewBlockY );//旧离开视野块；
//		for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
//		{
//			pTmpBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
//			if ( NULL != pNewBlock )
//			{
//				pTmpBlock->MonsterMoveOrgBlockNotify( pMoveMonster );
//			}
//		}
//	}
//
//	return true;
//	TRY_END;
//	return false;
//};

//玩家发来跨块消息
bool CMuxMap::OnPlayerBeyondBlock( CPlayer* pPlayer, const GMPlayerBeyondBlock* beyondMsg )
{
	TRY_BEGIN;

	if ( NULL == pPlayer )
	{
		return false;
	}

	if( NULL == beyondMsg)
	{	
		return false;
	}

	//pre.确保玩家在本地图内；
	if ( !IsValidPlayerPtrInMap( pPlayer ) )
	{
		D_ERROR( "OnPlayerBeyondBlock, 玩家%s(%d:%d)地图信息不正确，踢去此玩家\n", pPlayer->GetAccount(), pPlayer->GetGID(), pPlayer->GetPID() );
		//有的话踢这个玩家下线
		CGateSrv* pGateSrv = CManG_MProc::FindProc( pPlayer->GetGID() );
		if ( NULL == pGateSrv )
		{
			D_ERROR( "OnPlayerBeyondBlock，找不到玩家(%d:%d)对应的gatesrv实例\n", pPlayer->GetGID(), pPlayer->GetPID() );
			return false;
		}
		pGateSrv->NotifyGateSrvKickPlayer( pPlayer->GetFullID() );
		//MGError errMsg;
		//errMsg.leavePlayerID = pPlayer->GetFullID();
		//errMsg.nErrCode = MGError::
		//SafeStrCpy(errMsg.szLeaveAccount, pPlayer->GetAccount());
		//pPlayer->SendPkg<MGError>(&errMsg);
		return false;
	}

	if ( pPlayer->IsMoving() )
	{
		bool isAchieve = false, isNeedNotiMove = false;
		PlayerMoveOnce( pPlayer, false/*不跨格就不更新玩家位置*/, isAchieve, isNeedNotiMove );

#ifdef LATEST_MOVE_NOTI //只有使用了最迟移动通知，才可能会在本Timer中检测是否需要广播；
		if ( isNeedNotiMove )
		{
			PlayerNewMovePlayerBrocast( pPlayer, false/*修改相关标志，用于后续通知算法*/ );//最迟移动通知算法，两个可能通知点:1新移动时；2移动时钟(玩家发来移动跨块触发同样计算)发现新旧点计算差距太大时;
		}
#endif //LATEST_MOVE_NOTI

		if ( isAchieve )
		{
			//见(09.01.05,FLAG0001)if ( pPlayer->IsInMovingList() )
			//{
			//	DelPlayerFromMovingList( pPlayer );
			//}
			pPlayer->ResetMoveStat();
		}
	} else {
		//有可能进到此处，因为SRV先到达目标点并已停止移动，稍后玩家才跨块并停止移动；
	}


	//校验玩家跨块信息是否准确；	
	float fSrvPosX = pPlayer->GetFloatPosX();
	float fSrvPosY = pPlayer->GetFloatPosY();

	float fErr = fabs( fSrvPosX-beyondMsg->fCurX ) + fabs( fSrvPosY-beyondMsg->fCurY );
	if ( fErr > 10 )
	{
		D_WARNING( "OnPlayerBeyondBlock，玩家%s主动跨块时与SRV位置差距%f过大\n", pPlayer->GetNickName(), fErr );
	}

	/*
        跨块的同时player moveonce？如果SRV当前已跨块，则忽略，否则SRV也跨块？？
		如果这样做了，则后续时钟刷新玩家位置时本可以不通知的，但也要考虑SRV已经先跨块并已先通知的情形，
		或者即使SRV已经先跨块也不先通知，而只等moving player自己发来跨块消息时才通知，
		这样新视野玩家就不会根据SRV的计算看到一个移动者，而是确实在移动者自己跨块时才看到他，这会有一定的延迟，但比较保险？？
		但这样的话，SRV的先跨块玩家将不会出现在新视野玩家的视野内，如果此时该先跨块玩家有任何动作广播到新视野玩家，则会导致混乱？
	*/	

	//通知跨块玩家其周围环境情形（与定时刷新玩家位置时的通知有一定重复）
	PlayerBeyondBlockNotify( pPlayer );

	return true;
	TRY_END;
	return false;
}

//玩家移动处理;
bool CMuxMap::OnPlayerNewMove( CPlayer* pMovePlayer,const GMRun* pRunBuf )
{
	if ( NULL == pMovePlayer || NULL == pRunBuf )
	{
		D_ERROR( "CMuxMap::OnPlayerNewMove, NULL == pMovePlayer || NULL == pRunBuf\n" );
		return false;
	}
	return PlayerTryNewMove( pMovePlayer, pRunBuf->fCurWorldPosX, pRunBuf->fCurWorldPosY, pRunBuf->fTargetWorldPosX, pRunBuf->fTargetWorldPosY );
}

///玩家离开地图
bool CMuxMap::OnPlayerLeave( CPlayer* pLeavePlayer, bool isSwitchMapLeave )
{
	TRY_BEGIN;

	if ( NULL == pLeavePlayer )
	{
		return false;
	}

	//pre.确保玩家在本地图内；
	if ( !IsValidPlayerPtrInMap( pLeavePlayer ) )
	{
		D_ERROR( "CMuxMap::OnPlayerLeave, GsrvID:%d  Player ID:%x is not in the map\n", pLeavePlayer->GetGID(), pLeavePlayer->GetPID() );
		//无需通知gate此错误，让gate按普通流程将玩家删去，by dzj, 09.10.10，MGError errMsg;
		//errMsg.leavePlayerID = pLeavePlayer->GetFullID();
		//errMsg.nErrCode = MGError::LEAVE_ERR_MAPDISMATCH;
		//SafeStrCpy(errMsg.szLeaveAccount, pLeavePlayer->GetAccount());
		//pLeavePlayer->SendPkg<MGError>(&errMsg);
		return false;
	}

	//2.离线通知周围人以及周围怪物；
    PlayerLeaveNotify( pLeavePlayer );

	//3.将玩家从块以及地图上删去;
	DeletePlayerFromMap( pLeavePlayer, isSwitchMapLeave );

	CMonster* pFollowMonster = pLeavePlayer->GetFollowingMonster();
	if ( NULL != pFollowMonster )
	{
		pFollowMonster->OnMonsterDie( NULL /*没有任何人杀此NPC*/, GCMonsterDisappear::M_VANISH );//玩家离开地图时，跟随它的NPC自然死亡；
	}

	pLeavePlayer->SetMap( NULL ); //已经离开这个地图 没必要再保存该地图的指针

	//todo wcj 2011.03.05 删除地图BUFF 
	for (int i=0; i<m_pMapProperty->m_ToReadMapInfo.vSceneBuff.size(); ++i)
	{ 
		if(!pLeavePlayer->DeleteMuxBuff(m_pMapProperty->m_ToReadMapInfo.vSceneBuff[i]))
		{
			printf("删除无效的地图BUFF：%d",m_pMapProperty->m_ToReadMapInfo.vSceneBuff[i]); 
		}
		else
		{
			printf("删除地图BUFF：%d",m_pMapProperty->m_ToReadMapInfo.vSceneBuff[i]);
		}
	}


	return true;
	TRY_END;
	return false;
}

/////玩家移动状态改变通知周边怪物(新称动，打人，打怪)，应AI要求添加；
//void CMuxMap::PlayerMovChgMonsterNotify( CPlayer* pMovChgPlayer )
//{
//	TRY_BEGIN;
//
//	if ( NULL == pMovChgPlayer )
//	{
//		D_WARNING( "PlayerMovChgNotify, 输入指针空\n" );
//		return;
//	}

	//unsigned short blockX = 0, blockY = 0;
	//unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;


	//MuxMapSpace::GridToBlockPos( pMovChgPlayer->GetPosX(), pMovChgPlayer->GetPosY(), blockX, blockY );
	//if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	//{
	//	//输入点为无效点；
	//	return;
	//}
	//MuxMapBlock* pBlock = NULL;
	//for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	//{
	//	for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
	//	{
	//		pBlock = GetMapBlockByBlock( x,y );
	//		if ( NULL != pBlock )
	//		{
	//			pBlock->PlayerMovChgBlockMonsterNotify( pMovChgPlayer );
	//		}
	//	}
	//}

//	return;
//	TRY_END;
//	return;
//}

///玩家死亡通知；
void CMuxMap::PlayerDieNotify( CPlayer* pDiePlayer )
{
	TRY_BEGIN;

	if ( NULL == pDiePlayer )
	{
		D_WARNING( "CMuxMap::PlayerDieNotify, 输入指针空\n" );
		return;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_WARNING( "CMuxMap::PlayerDieNotify, NULL == m_pCopyMapInfo\n" );
			return;
		}
		m_pCopyMapInfo->PlayerDieNotify( pDiePlayer );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_WARNING( "CMuxMap::PlayerDieNotify, NULL == m_pNormalMapInfo\n" );
			return;
		}
		m_pNormalMapInfo->PlayerDieNotify( pDiePlayer );
	}

	return;
	TRY_END;
	return;
}

///玩家同地图内跳转；
bool CMuxMap::OnPlayerInnerJump( CPlayer* pMovPlayer, TYPE_POS targetGridX, TYPE_POS targetGridY ) 
{ 
	if ( NULL == pMovPlayer )
	{
		D_WARNING( "OnPlayerInnerJump，玩家地图内跳转，pMovPlayer指针空\n" );
		return false;
	}

	//pre.验证玩家新位置；
	if ( !IsGridCanPass( targetGridX, targetGridY ) )
	{
		D_WARNING( "玩家地图内跳转，目标点(%d,%d)不可移动\n", targetGridX, targetGridY );
		return false;
	}

	if ( pMovPlayer->IsMoving() )
	{
		D_WARNING( "OnPlayerInnerJump，玩家%s移动中地图内跳转，跳转失败\n", pMovPlayer->GetAccount() );
		return false;
	}

	CRideState& ridestate = pMovPlayer->GetSelfStateManager().GetRideState();
	if ( ridestate.IsActive() )
	{
		if( ridestate.GetPlayerRideType() != CRideState::E_LEADER )//如果不是骑乘队长，不能控制跳地图
		{
			pMovPlayer->SendSystemChat("您不是骑乘队长,不能地图内跳转");
			return false;
		}
	}
	
#ifdef HAS_PATH_VERIFIER
	if(!BlockTestOK(pMovPlayer->GetFloatPosX(), pMovPlayer->GetFloatPosY(), (float)targetGridX, (float)targetGridY))
	{
		D_WARNING("OnPlayerInnerJump，玩家%s移动中地图内跳转,碰撞检测为通过\n", pMovPlayer->GetAccount());
		return false;
	}
#endif/*HAS_PATH_VERIFIER*/

	pMovPlayer->ResetMoveStat();

	pMovPlayer->ForceSetFloatPos( true/*非移动状态下重设位置*/, targetGridX+0.5f, targetGridY+0.5f );

	return true; 
};

/////玩家出现块相关通知；
//void CMuxMap::PlayerAppearNotifyByBlockPos( CPlayer* pAppearPlayer, bool isNotiSelf/*是否通知出现者本人*/, unsigned short blockX, unsigned short blockY )
//{
//	if ( NULL == pAppearPlayer )
//	{
//		D_WARNING( "PlayerAppearNotifyByBlockPos，输入玩家指针空\n" );
//		return;
//	}
//
//	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
//
//	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	{
//		//输入点为无效点；
//		return;
//	}
//
//#ifdef GROUP_NOTI
//	bool isNeedNoti = false;
//	bool isSomeOneAdd = false;
//#endif //GROUP_NOTI
//
//	MuxMapBlock* pBlock = NULL;
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pBlock = GetMapBlockByBlock( x,y );
//			if ( NULL != pBlock )
//			{
//				pBlock->PlayerAppearBlockNotify( pAppearPlayer, isNotiSelf );
//			}
//		}
//	}
//
//#ifdef GROUP_NOTI
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pBlock = GetMapBlockByBlock( x,y );
//			if ( NULL != pBlock )
//			{
//				if ( !isNotiSelf )
//				{
//					//不通知自己；
//					pBlock->FillGroupInfoWithSelfPlayerExcept( m_GroupNotiInfo, pAppearPlayer->GetFullID(), isSomeOneAdd );
//				} else {
//					//通知自己；
//					pBlock->FillGroupInfoWithSelfPlayer( m_GroupNotiInfo, isSomeOneAdd );
//				}
//				if ( isSomeOneAdd )
//				{
//					isNeedNoti = true;
//				}
//			}
//		}
//	}
//
//	if ( isNeedNoti )
//	{
//		//通知块上玩家本玩家出现信息；
//		MGOtherPlayerInfo* movPlayerInfo = NULL;
//		movPlayerInfo = pAppearPlayer->GetOtherPlayerInfo();
//		if ( NULL != movPlayerInfo )
//		{
//			m_GroupNotiInfo.NotiGroup<MGOtherPlayerInfo>( movPlayerInfo );
//		}
//		
//		MGUnionMemberShowInfo* pMovePlayerShowInfo = NULL;
//		CUnion* pMovePlayerUnion = pAppearPlayer->Union();
//		if ( NULL != pMovePlayerUnion )
//		{
//			pMovePlayerShowInfo = pMovePlayerUnion->GetUnionMemberShowInfo(pAppearPlayer);
//			if ( NULL != pMovePlayerShowInfo )
//			{
//				m_GroupNotiInfo.NotiGroup<MGUnionMemberShowInfo>( pMovePlayerShowInfo );
//			}
//		}
//
//		MGBuffData moveBuff;
//		if ( pAppearPlayer->IsHaveBuff() )
//		{
//			moveBuff.buffData = (*pAppearPlayer->GetBuffData());
//			moveBuff.targetID = pAppearPlayer->GetFullID();
//			m_GroupNotiInfo.NotiGroup<MGBuffData>( &moveBuff );
//		}
//
//		MGPetinfoStable* movePetInfo = NULL;
//		if( pAppearPlayer->IsSummonPet() )
//		{
//			movePetInfo = pAppearPlayer->GetSurPetNotiInfo();
//			if ( NULL != movePetInfo )
//			{
//				m_GroupNotiInfo.NotiGroup<MGPetinfoStable>( movePetInfo );
//			}
//		}
//
//		m_GroupNotiInfo.ResetGroupInfo();
//	}
//#endif //GROUP_NOTI
//
//	return;
//}

/////玩家跨块相关通知；
//void CMuxMap::PlayerBeyondBlockNotifyByBlockPos( CPlayer* pBeyondPlayer, unsigned short blockX, unsigned short blockY )
//{
//	if ( NULL == pBeyondPlayer )
//	{
//		D_WARNING( "PlayerBeyondBlockNotifyByBlockPos，输入玩家指针空\n" );
//		return;
//	}
//
//	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
//
//	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	{
//		//输入点为无效点；
//		return;
//	}
//
//	MGEnvNotiFlag notiFlag;
//	notiFlag.gcNotiFlag.notiStOrEnd = true;//通知开始；
//	pBeyondPlayer->SendPkg<MGEnvNotiFlag>( &notiFlag );
//
//	MuxMapBlock* pBlock = NULL;
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pBlock = GetMapBlockByBlock( x,y );
//			if ( NULL != pBlock )
//			{
//				pBlock->PlayerBeyondBlockSelfNotify( pBeyondPlayer );
//			}
//		}
//	}
//
//	notiFlag.gcNotiFlag.notiStOrEnd = false;//通知结束；
//	pBeyondPlayer->SendPkg<MGEnvNotiFlag>( &notiFlag );
//
//	return;
//}

/////玩家离开块相关通知；
//void CMuxMap::PlayerLeaveNotifyByBlockPos( CPlayer* pLeavePlayer, unsigned short blockX, unsigned short blockY )
//{
//	if ( NULL == pLeavePlayer )
//	{
//		D_WARNING( "PlayerLeaveNotifyByBlockPos，输入玩家指针空\n" );
//		return;
//	}
//
//	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
//
//	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	{
//		//输入点为无效点；
//		return;
//	}
//
//#ifdef GROUP_NOTI
//	bool isNeedNoti = false;
//	bool isSomeOneAdd = false;
//#endif //GROUP_NOTI
//
//	MuxMapBlock* pBlock = NULL;
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pBlock = GetMapBlockByBlock( x,y );
//			if ( NULL != pBlock )
//			{
//				pBlock->PlayerLeaveBlockNotify( pLeavePlayer );
//			}
//		}
//	}
//
//#ifdef GROUP_NOTI
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pBlock = GetMapBlockByBlock( x,y );
//			if ( NULL != pBlock )
//			{
//				pBlock->FillGroupInfoWithSelfPlayerExcept( m_GroupNotiInfo, pLeavePlayer->GetFullID(), isSomeOneAdd );
//				if ( isSomeOneAdd )
//				{
//					isNeedNoti = true;
//				}
//			}
//		}
//	}
//
//	if ( isNeedNoti )
//	{
//		MGPlayerLeave returnMsg;
//		returnMsg.lPlayerID = pLeavePlayer->GetFullID();
//		returnMsg.lMapCode = pLeavePlayer->GetMapID();
//
//		m_GroupNotiInfo.NotiGroup<MGPlayerLeave>( &returnMsg );//通知新进入视野块自身信息；
//		m_GroupNotiInfo.ResetGroupInfo();
//	}
//#endif //GROUP_NOTI
//
//	return;
//}

////////////////////////////////////////////////////////////
//copy...
//通知周围格特殊怪物 离开或者进入视野
void CMuxMap::MonsterLeaveViewNotify( CMonster* pMonster )
{
	if( NULL == pMonster )
	{
		D_ERROR( "CMuxMap::MonsterLeaveViewNotify, 输入参数空\n" );
		return;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::MonsterLeaveViewNotify，NULL == m_pCopyMapInfo\n" );
			return;
		}
		return m_pCopyMapInfo->MonsterLeaveViewNotify( pMonster );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::MonsterLeaveViewNotify，NULL == m_pNormalMapInfo\n" );
			return;
		}
		return m_pNormalMapInfo->MonsterLeaveViewNotify( pMonster );
	}

	return;
}
//...copy
////////////////////////////////////////////////////////////

///玩家离开当前位置相关通知；
void CMuxMap::PlayerBeyondBlockNotify( CPlayer* pBeyondPlayer )
{
	if ( NULL == pBeyondPlayer )
	{
		D_WARNING( "PlayerBeyondBlockNotify，输入玩家指针空\n" );
		return;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::PlayerBeyondBlockNotify，NULL == m_pCopyMapInfo\n" );
			return;
		}
		return m_pCopyMapInfo->PlayerBeyondBlockNotifyByBlockPos( pBeyondPlayer );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::PlayerBeyondBlockNotify，NULL == m_pNormalMapInfo\n" );
			return;
		}
		return m_pNormalMapInfo->PlayerBeyondBlockNotifyByBlockPos( pBeyondPlayer );
	}

	return;
}

///玩家离开当前位置相关通知；
void CMuxMap::PlayerLeaveNotify( CPlayer* pLeavePlayer )
{
	if ( NULL == pLeavePlayer )
	{
		D_WARNING( "PlayerLeaveNotify，输入玩家指针空\n" );
		return;
	}

	unsigned short blockX = 0, blockY = 0;

	MuxMapSpace::GridToBlockPos( pLeavePlayer->GetPosX(), pLeavePlayer->GetPosY(), blockX, blockY );

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::PlayerLeaveNotify, NULL == m_pCopyMapInfo\n" );
			return;
		}
		m_pCopyMapInfo->PlayerLeaveNotifyByBlockPos( pLeavePlayer, blockX, blockY );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::PlayerLeaveNotify, NULL == m_pNormalMapInfo\n" );
			return;
		}
		m_pNormalMapInfo->PlayerLeaveNotifyByBlockPos( pLeavePlayer, blockX, blockY );
	}

	return;
}

///玩家出现相关通知；
void CMuxMap::PlayerAppearNotify( CPlayer* pAppearPlayer, bool isNotiSelf/*是否通知出现者本人*/ )
{
	if ( NULL == pAppearPlayer )
	{
		D_WARNING( "PlayerAppearNotify，输入玩家指针空\n" );
		return;
	}

	unsigned short blockX = 0, blockY = 0;

	MuxMapSpace::GridToBlockPos( pAppearPlayer->GetPosX(), pAppearPlayer->GetPosY(), blockX, blockY );

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "PlayerAppearNotify，NULL == m_pCopyMapInfo\n" );
			return;
		}
		m_pCopyMapInfo->PlayerAppearNotifyByBlockPos( pAppearPlayer, isNotiSelf, blockX, blockY );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "PlayerAppearNotify，NULL == m_pNormalMapInfo\n" );
			return;
		}
		m_pNormalMapInfo->PlayerAppearNotifyByBlockPos( pAppearPlayer, isNotiSelf, blockX, blockY );
	}


	return;
}

///玩家出现在地图上
bool CMuxMap::OnPlayerAppear( CPlayer* pAppearPlayer )
{
	TRY_BEGIN;

	if ( NULL == pAppearPlayer )
	{
		D_WARNING( "OnPlayerAppear，输入玩家指针空\n" );
		return false;
	}

	if ( ! AddPlayerToMap( pAppearPlayer, pAppearPlayer->GetPosX(), pAppearPlayer->GetPosY() ) )
	{
		//地图添加玩家失败；
		D_ERROR( "AddPlayerToMap失败，玩家%s\n", pAppearPlayer->GetAccount() );
		return false;
	}

	pAppearPlayer->PlayerDbInfoNotify();//玩家存盘信息通知；

	//登陆玩家BUFF的初始化
	if( !pAppearPlayer->InitMuxBuff() )
	{
		D_ERROR("BUFF初始化失败\n");
		//return false;
	}

	//新上线玩家不应该处于休息状态
	if (pAppearPlayer->IsInRest())
	{
		pAppearPlayer->ActiveEndRestBuff();
	}

	//todo wcj 2011.03.05 在此添加场景BUFF
	for (int i=0; i<m_pMapProperty->m_ToReadMapInfo.vSceneBuff.size(); ++i)
	{
		if(!pAppearPlayer->CreateMuxBuffByID(m_pMapProperty->m_ToReadMapInfo.vSceneBuff[i],0, pAppearPlayer->GetCreatureID()))
		{
			printf("添加无效的地图BUFF：%d",m_pMapProperty->m_ToReadMapInfo.vSceneBuff[i]);  
		}
		else
		{
			printf("添加场景BUFF：%d",m_pMapProperty->m_ToReadMapInfo.vSceneBuff[i]);
		}
	}
	

	//6.玩家新出现相关通知（通知周围人与周围怪物）；
	PlayerAppearNotify( pAppearPlayer, true );

	pAppearPlayer->OnRookieStateCheck( true );

	return true;
	TRY_END;
	return false;
}


bool CMuxMap::OnReqRoleInfo(CPlayer* pPlayer, CPlayer* pNoticePlayer)
{
	if ( ( NULL == pPlayer ) || ( NULL == pNoticePlayer ) )
	{
		return false;
	}

	if( !IsValidPlayerPtrInMap(pPlayer) )
	{
		D_WARNING("OnReqRoleInfo, 所请求玩家%d不在地图%d\n", pPlayer->GetPID(), GetMapNo() );
		return false;
	}

	if( !IsValidPlayerPtrInMap(pNoticePlayer) )
	{
		D_WARNING("OnReqRoleInfo, 请求玩家%d不在地图%d\n", pNoticePlayer->GetPID(), GetMapNo() );
		return false;
	}

	D_DEBUG( "玩家%s主动请求%s的外观信息\n", pNoticePlayer->GetAccount(), pPlayer->GetAccount() );
	
	MGSimplePlayerInfo* otherPlayerInfo = pPlayer->GetSimplePlayerInfo();
	if( NULL != otherPlayerInfo )
	{
		pNoticePlayer->SendPkg<MGSimplePlayerInfo>( otherPlayerInfo );
	}

	MGUnionMemberShowInfo *pShowInfo;
	CUnion *pUnion = pPlayer->Union();
	if(NULL != pUnion)
	{
		pShowInfo = pUnion->GetUnionMemberShowInfo(pPlayer);
		pNoticePlayer->SendPkg<MGUnionMemberShowInfo>(pShowInfo);
	}

	if( pPlayer->IsHaveBuff() )
	{
		MGBuffData srvmsg;
		srvmsg.buffData = *(pPlayer->GetBuffData());
		srvmsg.targetID = pPlayer->GetFullID();
		pNoticePlayer->SendPkg<MGBuffData>(&srvmsg );
	}

	if( pPlayer->IsSummonPet() )
	{
		MGPetinfoStable* movePetInfo = pPlayer->GetSurPetNotiInfo();
		if ( NULL != movePetInfo )
		{
			pNoticePlayer->SendPkg<MGPetinfoStable>( movePetInfo );//通知新进入视野块自身信息；
		}
	}
	return true;
}


bool CMuxMap::GetScopeCreature( TYPE_POS posX, TYPE_POS posY, int range, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster )
{
	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::GetScopeCreature，NULL == m_pCopyMapInfo\n" );
			return false;
		}
		return m_pCopyMapInfo->GetScopeCreature( posX, posY, range, vecPlayer, vecMonster );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::GetScopeCreature，NULL == m_pNormalMapInfo\n" );
			return false;
		}
		return m_pNormalMapInfo->GetScopeCreature( posX, posY, range, vecPlayer, vecMonster );
	}

	return false;

}


bool CMuxMap::OnReqMonsterInfo(CPlayer* pNoticePlayer, const GMReqMonsterInfo* pReqMonsterInfo)
{
	if ( NULL == pNoticePlayer )
	{
		return false;
	}

	if ( NULL == pReqMonsterInfo )
	{
		return false;
	}

	if( !IsValidPlayerPtrInMap(pNoticePlayer) )
	{
		//D_WARNING("CMuxMap::OnReqMonsterInfo,地图上找不到该玩家\n");
		return false;
	}
	
	CMonster* pMonster = GetMonsterPtrInMap( pReqMonsterInfo->lMonsterID );
	if ( NULL == pMonster )
	{
		D_ERROR("CMuxMap::OnReqMonsterInfo,找不到请求的怪物: %x\n",  pReqMonsterInfo->lMonsterID);
		return false;
	}

#ifdef ITEM_NPC
	if( pMonster->IsItemNpc() )
	{
		D_WARNING( "%s请求物件NPC monsterinfo\n", pNoticePlayer->GetAccount() );
		return false;
	}
#endif //ITEM_NPC

	pNoticePlayer->SendPkg<MGMonsterInfo>( pMonster->GetMonsterInfo() );

	return true;
}


bool CMuxMap::HandlePlayerChat( CPlayer* pPlayer, const GMChat* pChat)
{
	TRY_BEGIN;

	if ( NULL == pPlayer )
	{
		return false;
	}

	if ( NULL == pChat )
	{
		return false;
	}

	if( !IsValidPlayerPtrInMap(pPlayer) )
	{
		D_WARNING("HandlePlayerChat, 玩家%d不在地图%d\n", pPlayer->GetPID(), GetMapNo() );
		return false;
	}

	///验证玩家发来的聊天类型，玩家不能主动发系统类消息；
	if ( ( pChat->nType != CT_SCOPE_CHAT ) 
		//私聊应发center，&& ( pChat->nType != CT_PRIVATE_CHAT ) 
		&& ( pChat->nType != CT_RACE_BRO_CHAT ) 
		&& ( pChat->nType != CT_WORLD_CHAT ) 
		//组队聊天应直接发relation，&& ( pChat->nType != CT_TEAM_CHAT ) 
		//IRC聊天不可能发到这里来，&& ( pChat->nType != CT_PRIVATE_IRC_CHAT ) 
		//IRC聊天不可能发到这里来，&& ( pChat->nType != CT_WORLD_IRC_CHAT ) 
		//IRC聊天不可能发到这里来，&& ( pChat->nType != CT_UNION_IRC_CHAT ) 
		)
	{
		D_WARNING( "玩家%s发来非法的聊天类型消息，本应在gate就被拦住\n", pPlayer->GetAccount() );
		return false;
	}

	string strCmd;	//gm指令的字符串
	vector<string> cmdArgs;	//gm指令的参数
	//string strChat;
	bool isGMCmd = CGMCmdManager::IsGmCmd( (const char*) pChat->strChat, strCmd, cmdArgs );

	if ( isGMCmd )
	{
		int ret = CGMCmdManager::DealGMCmd( strCmd.c_str(), cmdArgs, pPlayer );
		if ( GM_CMD_SUCCESS == ret )
		{
			return true;
		} 
		else 
		{
			if( GM_TRANSFER == ret ) 
			{
				if( cmdArgs.size() == 0 )  //申请转发的GM命令必须有一个参来表示目标玩家的姓名，参数列表不可为空
				{
					return false;
				}
				MGGmCmdRequest gateMsg;
				SafeStrCpy( gateMsg.strChat, pChat->strChat ); 
				gateMsg.requestPlayerID = pPlayer->GetFullID();
				SafeStrCpy( gateMsg.targetName, cmdArgs[0].c_str() ); //默认第一个为目标玩家的姓名，以后需要转发的参数格式必须按照这个格式来统一
				pPlayer->SendPkg<MGGmCmdRequest>( &gateMsg );
				return true;
			} 
			else if ( GM_CMD_ERROR == ret ) 
			{
				D_DEBUG( "玩家%s执行GM指令%s失败\n", pPlayer->GetAccount(), pChat->strChat );
			} 
			else 
			{
				D_WARNING( "HandlePlayerChat，意料之外的GM指令%s处理返回值%d\n", strCmd.c_str(), ret );
			}
			return false;
		}
	}

	//以下为讲话消息；

	//1、普通周边聊天
	//2、检查是否世界聊天
	//3、检查是否地图内广播
	if ( pChat->nType == CT_SCOPE_CHAT ) 
	{
		//普通周边聊天；
		MGChat returnChatMsg;
		returnChatMsg.sourcePlayerID = pChat->sourcePlayerID;
		SafeStrCpy( returnChatMsg.strChat, pChat->strChat );
		returnChatMsg.chatType = pChat->nType;
		returnChatMsg.extInfo = 0;

		//象周围格子所有人发送
		NotifySurrounding<MGChat>(&returnChatMsg, pPlayer->GetPosX(), pPlayer->GetPosY());		
		return true;
	}

	if ( pChat->nType == CT_RACE_BRO_CHAT ) 
	{
		//种族聊天，检查玩家的包裹里的相应道具；
		if ( pPlayer->CheckAndSubItem( pChat->usedItemID, LITTLE_BUGLE_TYPE ) )
		{
			MGChat returnChatMsg;
			returnChatMsg.sourcePlayerID = pChat->sourcePlayerID;
			SafeStrCpy( returnChatMsg.strChat, pChat->strChat );
			returnChatMsg.chatType = pChat->nType;
			returnChatMsg.extInfo = pPlayer->GetRace();//种族填入额外信息；

			pPlayer->SendPkg<MGChat>( &returnChatMsg );
			return true;
		} else {
			D_WARNING( "玩家%s使用聊天道具%d地图内喊话，实际检查此道具失败\n", pPlayer->GetAccount(), pChat->usedItemID );
			return false;
		}
	}

	if ( pChat->nType == CT_WORLD_CHAT ) 
	{
		//世界聊天，检查玩家的包裹里的相应道具；
		if ( pPlayer->CheckAndSubItem( pChat->usedItemID, BIG_BUGLE_TYPE ) )
		{
			MGChat returnChatMsg;
			returnChatMsg.sourcePlayerID = pChat->sourcePlayerID;
			SafeStrCpy( returnChatMsg.strChat, pChat->strChat );
			returnChatMsg.chatType = pChat->nType;
			returnChatMsg.extInfo = 0;

			//世界聊天，将聊天消息发回gate，由gate转发centersrv;
			pPlayer->SendPkg<MGChat>( &returnChatMsg );
			return true;
		} else {
			D_WARNING( "玩家%s使用聊天道具%d世界喊话，实际检查此道具失败\n", pPlayer->GetAccount(), pChat->usedItemID );
			return false;
		}
	}

	D_ERROR( "玩家%s发来的聊天消息%s，类型%d不可识别\n", pPlayer->GetAccount(), pChat->strChat, pChat->nType );

	return false;
	TRY_END;
	return false;
}

bool CMuxMap::HandlePlayerLevelUp(CPlayer* pPlayer, int levelUpType)
{
	TRY_BEGIN;

	if ( NULL == pPlayer )
	{
		return false;
	}

	if( !IsValidPlayerPtrInMap(pPlayer) )
	{
		D_WARNING( "[Handle LevelUp]玩家%s不在对应地图%d\n", pPlayer->GetAccount(), GetMapNo() );
		return false;
	}

	TYPE_POS nPlayerX,nPlayerY;
	pPlayer->GetPos( nPlayerX, nPlayerY);

	//当处于排行榜中的玩家等级改变时
	//RankEventManagerSingle::instance()->OnPlayerLevelChange( pPlayer );
	RankEventManager::OnPlayerLevelChange( pPlayer );

	//如果自身的等级更新了,那么通知RelationSrv
	MGFriendInfoUpdate friendUpMsg;
	friendUpMsg.level = pPlayer->GetLevel();
	pPlayer->SendPkg<MGFriendInfoUpdate>( &friendUpMsg );


	MGPlayerLevelUp levelUpMsg;
	levelUpMsg.playerID = pPlayer->GetFullID();
	levelUpMsg.nNewLevel = pPlayer->GetLevel();
	levelUpMsg.levelUpType = levelUpType;
	NotifySurrounding< MGPlayerLevelUp >( &levelUpMsg, nPlayerX, nPlayerY);

	MGSimplePlayerInfo* pOtherInfo = pPlayer->GetSimplePlayerInfo();
	NotifySurrounding<MGSimplePlayerInfo>( pOtherInfo, nPlayerX, nPlayerY );

	pPlayer->NoticePlayerLevelUpdate( pPlayer->GetLevel() );
	pPlayer->OnRookieStateCheck( false );
	return true;

	TRY_END;
	return false;
}


bool CMuxMap::IsPlayerInMonsterSight(CMonster* pMonster, CPlayer* pPlayer, int nDist)
{
	if(NULL == pMonster)
		return false;

	if(NULL == pPlayer)
		return false;

	int nSight = nDist;
	TYPE_POS nMonsterPosX = 0, nMonsterPosY = 0, nPlayerPosX = 0, nPlayerPosY = 0;
	pMonster->GetPos( nMonsterPosX, nMonsterPosY );
	pPlayer->GetPos( nPlayerPosX, nPlayerPosY );
	return (abs(nMonsterPosX- nPlayerPosX) + abs(nMonsterPosY - nPlayerPosY)) <= nSight;
}

bool CMuxMap::HandleItemPkgFirstAppear(CItemsPackage* pItemPkg )
{
	TRY_BEGIN;

	if ( NULL == pItemPkg )
	{
		D_ERROR( "HandleItemPkgFirstAppear, NULL == pItemPkg\n" );
		return false;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "HandleItemPkgFirstAppear, NULL == m_pCopyMapInfo\n" );
			return false;
		}
		if ( !(m_pCopyMapInfo->AddItemPkgToCopyMap( pItemPkg )) )
		{
			D_ERROR( "HandleItemPkgFirstAppear, AddItemPkgToCopyMap fail!\n" );
			return false;
		}
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "HandleItemPkgFirstAppear, NULL == m_pNormalMapInfo\n" );
			return false;
		}
		if ( !(m_pNormalMapInfo->AddItemPkgToNormalMap( pItemPkg )) )
		{
			D_ERROR( "HandleItemPkgFirstAppear, AddItemPkgToNormalMap fail!\n" );
			return false;
		}
	}

	NotifyItemPkgFirstAppearEx( pItemPkg );

	return true;
	TRY_END;
	return false;
}

void CMuxMap::NotifyItemPkgsAppearEx( CItemsPackage* pItemPkg )
{
	if ( NULL == pItemPkg )
	{
		D_ERROR( "NotifyItemPkgsAppearEx, NULL == pItemPkg\n" );
		return;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::NotifyItemPkgsAppearEx，NULL == m_pCopyMapInfo\n" );
			return;
		}
		return m_pCopyMapInfo->NotifyItemPkgsAppearEx( pItemPkg );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::NotifyItemPkgsAppearEx，NULL == m_pNormalMapInfo\n" );
			return;
		}
		return m_pNormalMapInfo->NotifyItemPkgsAppearEx( pItemPkg );
	}

	return;
}

void CMuxMap::NotifyItemPkgFirstAppearEx(CItemsPackage* pItemPkg )
{
	if ( NULL == pItemPkg )
	{
		D_ERROR( "NotifyItemPkgFirstAppearEx, NULL == pItemPkg\n" );
		return;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::NotifyItemPkgFirstAppearEx，NULL == m_pCopyMapInfo\n" );
			return;
		}
		return m_pCopyMapInfo->NotifyItemPkgFirstAppearEx( pItemPkg );
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::NotifyItemPkgFirstAppearEx，NULL == m_pNormalMapInfo\n" );
			return;
		}
		return m_pNormalMapInfo->NotifyItemPkgFirstAppearEx( pItemPkg );
	}

	return;
}

bool CMuxMap::HandleItemsPkgDisAppear(CItemsPackage *pItemPkg)
{
	TRY_BEGIN;

	if ( NULL == pItemPkg )
	{
		D_ERROR( "CMuxMap::HandleItemsPkgDisAppear, NULL == pItemPkg\n" );
		return false;
	}

	if ( IsInsCopyMap() )
	{
		//副本；
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "HandleItemsPkgDisAppear, NULL == m_pCopyMapInfo\n" );
			return false;
		}
		if ( !(m_pCopyMapInfo->DelItemPkgFromCopyMap( pItemPkg )) )
		{
			D_ERROR( "HandleItemsPkgDisAppear, DelItemPkgFromCopyMap fail!\n" );
			return false;
		}
	} else {
		//普通地图；
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "HandleItemsPkgDisAppear, NULL == m_pNormalMapInfo\n" );
			return false;
		}
		if ( !(m_pNormalMapInfo->DelItemPkgFromNormalMap( pItemPkg )) )
		{
			D_ERROR( "HandleItemsPkgDisAppear, DelItemPkgFromNormalMap fail!\n" );
			return false;
		}
	}

	MGItemPackDisAppear pkgDisappear;
	pkgDisappear.pkgID = pItemPkg->GetItemsPackageIndex();
	NotifySurrounding<MGItemPackDisAppear>( &pkgDisappear, pItemPkg->GetItemPosX(),pItemPkg->GetItemPosY() );

	D_DEBUG("%d 包裹消失,通知client\n" , pkgDisappear.pkgID );

	return true;
	TRY_END;
	return false;
}

bool CMuxMap::HandleQueryItemsPkg(CPlayer *pPlayer, unsigned long pkgIndex, unsigned int queryType )
{
	TRY_BEGIN;

	if ( !pPlayer)
		return false;

	CItemsPackage* pItemPkg = CItemPackageManagerSingle::instance()->GetItemPackage( pkgIndex );
	if ( pItemPkg )
	{
		//@brief: 返回包中的所有信息，如果包中有武器或者道具，它们的随机属性也会返回
		pItemPkg->OnQueryItemPkgDetailInfo( pPlayer, queryType );
		return true;
	}
	else
	{
		D_ERROR("玩家%s查询道具包信息时，找不到对应的道具包%d信息!\n", pPlayer->GetAccount(), pkgIndex );
	}

	return false;
	TRY_END;
	return false;
}


bool CMuxMap::OnPlayerUseAssistSkillToPlayer(CPlayer* pUseSkillPlayer,const PlayerID& targetID, TYPE_ID skillID, bool isUnionSkill )
{
	//if( !pUseSkillPlayer )
	//	return false;

	////如果是非工会辅助技能
	//if( !isUnionSkill )
	//{
	//	CPlayer* pTargetPlayer = NULL;
	//	CPlayerSkill* pSkill = NULL;

	//	EBattleResult errorCode = CheckPvp( pUseSkillPlayer, targetID, skillID, pSkill, pTargetPlayer );
	//	if( errorCode != BATTLE_SUCCESS )
	//	{
	//		MGUseAssistSkill errMsg;
	//		errMsg.skillID = skillID;
	//		errMsg.nErrorCode = errorCode;
	//		errMsg.useSkillPlayer = pUseSkillPlayer->GetFullID();
	//		errMsg.bPlayer = true;
	//		pUseSkillPlayer->SendPkg<MGUseAssistSkill>( &errMsg );
	//		return false;
	//	}

	//	if( !pTargetPlayer )
	//		return false;

	//	if( !pSkill )
	//		return false;

	//	CMuxPlayerSkill* pMuxSkill = pUseSkillPlayer->FindMuxSkill( skillID );
	//	if( NULL == pMuxSkill )
	//	{
	//		return false;
	//	}

	//	//pTargetPlayer->CreateMuxBuff( pMuxSkill->GetBuffProp(),  pUseSkillPlayer->GetFullID() );
	//	
	//	////现在暂时不处理范围
	//	//CREATE_BUFFARG( pSkill, buffArg );
	//	//_HandleBuffProcessPvp( &buffArg, pUseSkillPlayer, pTargetPlayer );



	//	//扣魔 扣SP +sp经验 +sp技能经验
	//	pUseSkillPlayer->SubMP( pSkill->GetUllageMp() );
	//	if( pSkill->IsSpSkill() )
	//	{
	//		pUseSkillPlayer->SubSpPower( pSkill->GetUllageSp() * CSpSkillManager::GetPowerPerBean() );
	//		if( pUseSkillPlayer->GetFullID() == targetID && pSkill->GetTargetType() == 1 )  //技能是自己施放的类型
	//		{
	//			pUseSkillPlayer->AddSpExp( 10 );  //对自己放有经验
	//			pSkill->AddSpExp( 1 );			  //对自己放有经验
	//		}
	//	}

	//	//如果技能是不利技能,触发流氓状态
	//	if( !pSkill->IsGoodSkill() )
	//	{
	//		pUseSkillPlayer->FightStartCheckRogueState( pTargetPlayer );

	//		//当玩家被释放坏的技能时
	//		pTargetPlayer->GetSelfStateManager().OnPlayerBeAttack();
	//	}

	//	MGUseAssistSkill successMsg;
	//	successMsg.nErrorCode = errorCode;
	//	successMsg.bPlayer = true;
	//	successMsg.nUseSkillPlayerSkillExp = pSkill->GetSpExp();
	//	successMsg.nUseSkillPlayerSpExp = pUseSkillPlayer->GetSpExp();
	//	successMsg.nUseSkillPlayerSpPower = pUseSkillPlayer->GetSpPower();
	//	successMsg.skillID = skillID;
	//	successMsg.targetPlayer = targetID;
	//	successMsg.useSkillPlayer = pUseSkillPlayer->GetFullID();
	//	successMsg.skillCurseTime = pSkill->GetCurseTime();
	//	successMsg.skillCooldownTime = pSkill->GetCoolDown();
	//	successMsg.nUseSkillPlayerMp = pUseSkillPlayer->GetCurrentMP();
	//	NotifySurrounding<MGUseAssistSkill>( &successMsg, pTargetPlayer->GetPosX(), pTargetPlayer->GetPosY() );
	//}else
	//{
	//	UnionSkillArg* pSkill = NULL;
	//	CPlayer* pTargetPlayer = NULL;
	//	EBattleResult errorCode = CheckUnionPvp( pUseSkillPlayer, targetID, skillID, pSkill, pTargetPlayer );
	//	if( errorCode != BATTLE_SUCCESS )
	//	{
	//		MGUseAssistSkill errMsg;
	//		errMsg.skillID = skillID;
	//		errMsg.nErrorCode = errorCode;
	//		errMsg.useSkillPlayer = pUseSkillPlayer->GetFullID();
	//		errMsg.bPlayer = true;
	//		pUseSkillPlayer->SendPkg<MGUseAssistSkill>( &errMsg );
	//		return false;
	//	}

	//	BuffArg buffArg;
	//	buffArg.buffProp = CManBuffProp::FindObj( pSkill->linkID );
	//	buffArg.buffTime = pSkill->periodOfTime;
	//	buffArg.assistObject = 1;
	//	buffArg.linkRate = pSkill->linkRate;
	//	buffArg.isTeamBuff = ( pSkill->target == 2 );
	//	buffArg.teamRange = pSkill->attDistMax;
	//	_HandleBuffProcessPvp( &buffArg, pUseSkillPlayer, pTargetPlayer );

	//	//扣魔 扣SP +sp经验 +sp技能经验
	//	pUseSkillPlayer->SubMP( pSkill->ullageMp );
	//	MGUseAssistSkill successMsg;
	//	successMsg.nErrorCode = errorCode;
	//	successMsg.bPlayer = true;
	//	successMsg.nUseSkillPlayerSkillExp = 0;
	//	successMsg.nUseSkillPlayerSpExp = 0;
	//	successMsg.nUseSkillPlayerSpPower = pUseSkillPlayer->GetSpPower();
	//	successMsg.skillID = skillID;
	//	successMsg.targetPlayer = targetID;
	//	successMsg.useSkillPlayer = pUseSkillPlayer->GetFullID();
	//	successMsg.skillCurseTime = pSkill->curseTime;
	//	successMsg.skillCooldownTime = pSkill->CoolDown;
	//	successMsg.nUseSkillPlayerMp = pUseSkillPlayer->GetCurrentMP();
	//	NotifySurrounding<MGUseAssistSkill>( &successMsg, pTargetPlayer->GetPosX(), pTargetPlayer->GetPosY() );
	//}

	
	return true;
}


bool CMuxMap::OnPlayerUseAssistSkillToMonster(CPlayer* pUseSkillPlayer, TYPE_ID monsterID, TYPE_ID skillID, bool isUnionSkill )
{
	//if( !pUseSkillPlayer )
	//	return false;

	////工会技能不能对怪物释放
	//if( isUnionSkill )
	//{
	//	MGUseAssistSkill failMsg;
	//	failMsg.nErrorCode = CUSTOMIZE_ERROR;
	//	failMsg.bPlayer = false;
	//	failMsg.nUseSkillPlayerSkillExp = 0;
	//	failMsg.nUseSkillPlayerSpExp = 0;
	//	failMsg.nUseSkillPlayerSpPower = 0;
	//	failMsg.skillID = skillID;
	//	failMsg.targetPlayer.dwPID = monsterID;
	//	failMsg.targetPlayer.wGID = 1000;
	//	failMsg.useSkillPlayer = pUseSkillPlayer->GetFullID();
	//	failMsg.skillCooldownTime = 0;
	//	failMsg.skillCurseTime = 0;
	//	failMsg.nUseSkillPlayerMp = 0;
	//	pUseSkillPlayer->SendPkg<MGUseAssistSkill>( &failMsg );

	//	pUseSkillPlayer->SendSystemChat("工会技能不能对怪物施放");
	//	return true;
	//}

	//
	//CMonster* pMonster = NULL;
	//CPlayerSkill* pSkill = NULL;

	//EBattleResult errorCode = CheckPvc( pUseSkillPlayer, monsterID, skillID, pSkill, pMonster );
	//if( errorCode != BATTLE_SUCCESS )
	//{
	//	MGUseAssistSkill errMsg;
	//	errMsg.skillID = skillID;
	//	errMsg.nErrorCode = errorCode;
	//	errMsg.bPlayer = false;
	//	errMsg.useSkillPlayer = pUseSkillPlayer->GetFullID();
	//	pUseSkillPlayer->SendPkg<MGUseAssistSkill>( &errMsg );
	//	return false;
	//}

	//if( !pMonster )
	//	return false;

	//if( !pSkill )
	//	return false;

	//if( pSkill->GetTargetType() == 2 )
	//{
	//	CREATE_BUFFARG( pSkill, buffArg );
	//	_HandleTeamBuff( &buffArg, pUseSkillPlayer );
	//	return true;
	//}
	//
	////现在暂时不处理范围
	//if( RandRate( pSkill->GetLinkRate() ) )
	//{
	//	pMonster->CreateBuff( pSkill->GetLinkID(), pSkill->GetPeriodTime(), pUseSkillPlayer->GetFullID(), CREATE_PLAYER, pSkill->GetSkillID() );
	//	if( !pSkill->IsGoodSkill() )
	//	{
	//		pMonster->OnBeAttackedByPlayer( pUseSkillPlayer, 0, true, 0 );
	//	}
	//}


	////扣魔 扣SP +sp经验 +sp技能经验
	//pUseSkillPlayer->SubMP( pSkill->GetUllageMp() );
	//if( pSkill->IsSpSkill() )
	//{
	//	pUseSkillPlayer->SubSpPower( pSkill->GetUllageSp() * CSpSkillManager::GetPowerPerBean() );
	//	pUseSkillPlayer->AddSpExp( 10 );
	//	pSkill->AddSpExp( 1 );
	//}

	//MGUseAssistSkill successMsg;
	//successMsg.nErrorCode = errorCode;
	//successMsg.bPlayer = false;
	//successMsg.nUseSkillPlayerSkillExp = pSkill->GetSpExp();
	//successMsg.nUseSkillPlayerSpExp = pUseSkillPlayer->GetSpExp();
	//successMsg.nUseSkillPlayerSpPower = pUseSkillPlayer->GetSpPower();
	//successMsg.skillID = skillID;
	//successMsg.targetPlayer.dwPID = monsterID;
	//successMsg.targetPlayer.wGID = 1000;
	//successMsg.useSkillPlayer = pUseSkillPlayer->GetFullID();
	//successMsg.skillCooldownTime = pSkill->GetCoolDown();
	//successMsg.skillCurseTime = pSkill->GetCurseTime();
	//successMsg.nUseSkillPlayerMp = pUseSkillPlayer->GetCurrentMP();
	//NotifySurrounding<MGUseAssistSkill>( &successMsg, pMonster->GetPosX(), pMonster->GetPosY() );
	return true;
	
}


bool CMuxMap::BattleModeCheckPvc( CPlayer* pAttackPlayer, CMonster* pMonster, bool bGoodSkill )
{
	if( !pAttackPlayer )
		return false;

	if( !pMonster )
		return false;

	if( !bGoodSkill )
	{
		if( pMonster->GetNpcType() == 12 )  //12为商店ＮＰＣ任何战斗模式下都不能进行攻击
			return false;
	}


	BATTLE_MODE battleMode = pAttackPlayer->GetBattleMode();
	int usNpcRaceAttribute = pMonster->GetRaceAttribute();
	if( battleMode == DEFAULT_MODE )
	{
		if( !bGoodSkill )
		{
			if( pAttackPlayer->GetRace() == usNpcRaceAttribute )
			{
				return false;
			}
			return true;
		}else{
			if( pAttackPlayer->GetRace() == usNpcRaceAttribute )
			{
				return true;
			}		
			return false;
		}
	}else if( battleMode == FULL_FIELD_MODE ){
		if( bGoodSkill )						//有利的BUFF不能对怪物使用
		{
			return false;
		}
		return true;	//全域几乎全能打	    	
	}else if( battleMode == GUILD_MODE )
	{
		if( pMonster->GetRaceAttribute() == NPC_RACE_MONSTER )
			return true;

	}

	return true;
}


bool CMuxMap::BattleModeCheckPvp( CPlayer* pAttackPlayer, CPlayer* pTargetPlayer, bool bGoodSkill )
{
	if( !pAttackPlayer )
		return false;

	if( !pTargetPlayer )
		return false;

	//如果使用对象是自己
	if( pAttackPlayer == pTargetPlayer )
	{
		if( bGoodSkill )
			return true;
		else
			return false;
	}

	short attackEvilPoint = pAttackPlayer->GetGoodEvilPoint();
	short targetEvilPoint = pTargetPlayer->GetGoodEvilPoint();
	BATTLE_MODE battleMode = pAttackPlayer->GetBattleMode();

	if( battleMode == DEFAULT_MODE )
	{
		if( pAttackPlayer->GetRace() == pTargetPlayer->GetRace() )
		{
			//目标为魔头状态 或者 流氓状态的时候
			if( targetEvilPoint < 0 || pTargetPlayer->IsRogueState() )
			{
				//流氓状态 和 魔头可以对其施放任何技能
				if( pAttackPlayer->IsRogueState() || attackEvilPoint < 0)
					return true;
				
				//其他状态的人,不能对魔头 和 流氓玩家释放有利技能
				if( bGoodSkill )
				{
					return false;
				}
				return true;
			}

			//目标为普通英雄
			if( targetEvilPoint >= 0 )
			{
				//流氓 魔头在常规模式中不能对同种族的任何玩家使用技能
				if( pAttackPlayer->IsRogueState() || attackEvilPoint < 0)
				{
					return false;
				}

				if( bGoodSkill )
				{
					return true;
				}

				//攻击技能不能用
				return false;
			}
		}else
		{
			if( bGoodSkill )
			{
				return false;
			}else
			{
				return true;
			}
		}
	}else if( battleMode == FULL_FIELD_MODE ){
		if( bGoodSkill )
			return false;	
	
		return true;
	}else if( battleMode == GUILD_MODE )
	{
		CUnion* pAttackUnion = pAttackPlayer->Union();
		CUnion* pTargetUnion = pTargetPlayer->Union();
		//首先判断是否异种族
		if( pAttackPlayer->GetRace() != pTargetPlayer->GetRace() )
		{
			if( bGoodSkill )
				return false;
			return true;
		}

		//同种族情况下。。。
		if( NULL == pAttackUnion )			//如果攻击者无工会，类似全域模式
		{
			D_WARNING("警告,%s无工会却在使用战盟模式\n", pAttackPlayer->GetNickName() );
			if( bGoodSkill )
				return false;	
	
			return true;
		}

		if( pAttackUnion == pTargetUnion )			//如果是同一工会的，同一工会的人必定是同一种族的人
		{
			if( !bGoodSkill )
				return false;
			return true;
		}

		if( bGoodSkill )
		{
			//释放者为普通和英雄状态可以对同种族的普通和英雄释放
			if( attackEvilPoint < 0 || pAttackPlayer->IsRogueState() )
			{
				if( targetEvilPoint < 0 || pTargetPlayer->IsRogueState() )
					return true;
				return false;
			}else
			{
				if( targetEvilPoint < 0 || pTargetPlayer->IsRogueState() )
					return false;
				return true;
			}
		}else
		{
			return true;	//非同工会的都能打
		}
	}

	return true;
}


bool CMuxMap::RookieProtect( CPlayer* pAttackPlayer,  CPlayer* pTargetPlayer, bool bGoodSkill, bool bInScopeAttack )
{
	//注，此判断必须要在BattleModeCheckPvp以后判断

	if( NULL == pAttackPlayer || NULL == pTargetPlayer )
		return false;

	
	bool bDifferentRace = (pAttackPlayer->GetRace() != pTargetPlayer->GetRace()); 
	if( bGoodSkill )
	{
		return !bDifferentRace; 
	}

	unsigned short tgtLevel = pTargetPlayer->GetLevel();
	//流氓状态就是新手保护失效时间
	if( pTargetPlayer->IsRogueState() )
		return true;

	if( tgtLevel <= 30 )
	{
		if( !bInScopeAttack )
		{
			pAttackPlayer->SendSystemChat("严禁欺负弱小否则必受天谴！");
		}
		return false;
	}
		
	return true;
}

///删除某类怪物；
bool CMuxMap::DelTypeMonster( unsigned int npcTypeId )
{
	AddToDelMonsterType( npcTypeId );
	return true;
}

///将某类怪物添加到删除列表；
void CMuxMap::AddToDelMonsterType( unsigned int monsterClsID )
{
	if( !IsInsCopyMap() )
	{	
		if( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "AddToDelMonsterType, NULL == m_pNormalMapInfo\n" );
			return;
		}
		m_pNormalMapInfo->AddToDelMonsterType( monsterClsID );
	}else{
		if( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "AddToDelMonsterType, NULL == m_pCopyMapInfo\n" );
			return;
		}
		m_pCopyMapInfo->AddToDelMonsterType( monsterClsID );
	}
	if ( m_vecToDelMonsterType.empty() )
	{
		m_vecToDelMonsterType.push_back( monsterClsID );//待删类型新加入待删列表；
		return;
	} else {
		for ( vector<unsigned int>::iterator iter=m_vecToDelMonsterType.begin(); iter!=m_vecToDelMonsterType.end(); ++iter )
		{
			if ( *iter == monsterClsID )
			{
				return;//待删类型已加入待删列表；
			}
		}
		m_vecToDelMonsterType.push_back( monsterClsID );//待删类型新加入待删列表；
		return;
	}
}

///定时检查是否有某类怪物需要被删除；
bool CMuxMap::TimerCheckToDelTypeMonster()
{
	if ( m_vecToDelMonsterType.empty() )
	{
		return true;
	}

	vector<unsigned int> tmptodelvec;//暂存到目前为止的待删类型，用于防止在以下的调用中，向m_vecToDelMonsterType中添加新元素；
	tmptodelvec.clear();
	tmptodelvec.swap( m_vecToDelMonsterType );
	m_vecToDelMonsterType.clear();

	if( !IsInsCopyMap() )
	{	
		if( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "TimerCheckToDelTypeMonster, NULL == m_pNormalMapInfo\n" );
			return false;
		}
		for ( vector<unsigned int>::iterator iter=tmptodelvec.begin(); iter!=tmptodelvec.end(); ++iter )
		{
			m_pNormalMapInfo->DelTypeMonster( *iter );
		}
	}else{
		if( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "TimerCheckToDelTypeMonster, NULL == m_pCopyMapInfo\n" );
			return false;
		}
		for ( vector<unsigned int>::iterator iter=tmptodelvec.begin(); iter!=tmptodelvec.end(); ++iter )
		{
			m_pCopyMapInfo->DelTypeMonster( *iter );
		}
	}

	return true;
}

#ifdef STAGE_MAP_SCRIPT

///计数器添加
bool CMuxMap::MapAddCounterOfMap( unsigned int counterClsId/*编号，用于脚本标识*/
								 , unsigned int counterType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount )
{
	if ( NULL == m_pCounterOfMap )
	{
		return false;
	}

	m_pCounterOfMap->AddCounterOfMap( counterClsId, counterType, counterIdMin, counterIdMax, targetCount );
	return true;
}

///地图计时器添加
bool CMuxMap::MapInsertMapTimer( unsigned int timerClsID/*类型号，用于脚本标识*/, unsigned int resTime/*到期时刻，单位:分钟*/ )
{
	if ( NULL == m_pTimerOfMap )
	{
		return false;
	}

	ACE_Time_Value expireTime = ACE_OS::gettimeofday();
	expireTime += ACE_Time_Value( resTime*60 );

	m_pTimerOfMap->InsertMapTimer( timerClsID, expireTime );
	return true;
}

bool CMuxMap::MapDeleteMapTimer( unsigned int timerClsID )
{
	if ( NULL == m_pTimerOfMap )
	{
		return false;
	}

	return m_pTimerOfMap->DeleteMapTimer( timerClsID );
}

bool CMuxMap::MapIsHaveMapTimer( unsigned int timerClsID )
{
	if ( NULL == m_pTimerOfMap )
	{
		return false;
	}
	return m_pTimerOfMap->IsHaveMapTimer( timerClsID );
}


bool CMuxMap::MapInsertDetailTimer( unsigned int timerClsId, unsigned int hour, unsigned int minute )		//增加固定时间的定时器
{
	if( NULL == m_pDetailTimerOfMap )
	{
		return false;
	}
	m_pDetailTimerOfMap->InsertDetailMapTimer( timerClsId, hour, minute );
	return true;
}


bool CMuxMap::MapDeleteDetailTimer( unsigned int timerClsId )
{
	if( NULL == m_pDetailTimerOfMap )
	{
		return false;
	}
	
	return m_pDetailTimerOfMap->DeleteDetailMapTimer( timerClsId );
}

///向地图内的所有玩家广播
bool CMuxMap::MapBrocastChatMsg( const char* notimsg )
{
	MGChat sysMsg;

	SafeStrCpy( sysMsg.strChat, notimsg );
	sysMsg.chatType = CT_SYS_EVENT_NOTI;
	sysMsg.extInfo = 0;

	NotifyAllUseBrocast<MGChat>( &sysMsg );

	return true;
}

///玩家进入地图，回调脚本事件处理函数；
bool CMuxMap::OnMapPlayerEnterScriptHandle( CPlayer* pEnterPlayer )
{
	if ( NULL == pEnterPlayer )
	{
		D_ERROR( "OnMapPlayerEnterScriptHandle，输入的进入玩家空\n" );
		return false;
	}

	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnPlayerEnter( this, pEnterPlayer );
	m_warTimerManager.OnPlayerEnterMap( pEnterPlayer );
	return true;
}

///玩家离开地图，回调脚本事件处理函数；
bool CMuxMap::OnMapPlayerLeaveScriptHandle( CPlayer* pLeavePlayer )
{
	if ( NULL == pLeavePlayer )
	{
		D_ERROR( "OnMapPlayerEnterScriptHandle，输入的离开玩家空\n" );
		return false;
	}

	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnPlayerLeave( this, pLeavePlayer );

	return true;
}

///地图计时器到，回调脚本事件处理函数；
bool CMuxMap::OnMapTimerScriptHandle( unsigned int mapTimerClsID /*到期的时钟号，传递给脚本以便作相应处理*/ ) 
{
	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnTimerMeet( this, mapTimerClsID );

	return true;
};

///地图计数器到，回调脚本事件处理函数；
bool CMuxMap::OnMapCounterScriptHandle( unsigned int mapCounterClsID /*计数满的计数器号，传递给脚本以便作相应处理*/ ) 
{
	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnCounterMeet( this, mapCounterClsID );

	return true;
};


bool CMuxMap::OnDetailTimerScriptHandle( unsigned int timerId )	 /*到期的定时器号*/
{
	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnDetailTimerMeet( this, timerId );

	return true;
}


bool CMuxMap::OnNpcDeadScriptHandle( CPlayer* pKiller, unsigned int npcTypeId )   /*npc死亡*/
{
	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnNpcDead( this, pKiller, npcTypeId );

	return true;
}


bool CMuxMap::OnPlayerDieScriptHandle( CPlayer* pDiePlayer )
{
	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnPlayerDie( this, pDiePlayer );

	return true;
}


bool CMuxMap::OnHandleScriptLeaveArea( unsigned int areaID, CPlayer* pMovePlayer )
{
	if(NULL == pMovePlayer)
		return false;

	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}
		
	pMapScript->OnPlayerLeaveArea( this, pMovePlayer, areaID );
	return true;
}


bool CMuxMap::OnHandleScriptNotifyLevelMapInfo(unsigned int count)
{
	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}
		
	pMapScript->OnNotifyLevelMapInfo( this, count );
	return true;
}


bool CMuxMap::OnHandleScriptEnterArea( unsigned int areaID, CPlayer* pMovePlayer )
{
	if(NULL == pMovePlayer)
		return false;

	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnPlayerEnterArea( this, pMovePlayer, areaID );
	return true;
}


bool CMuxMap::OnHandleScriptNpcFirstBeAttacked( unsigned int monsterID, CPlayer* pAttacker )
{
	CMapScript* pMapScript = GetMapScript();
	if ( NULL == pMapScript )
	{
		return false;
	}

	pMapScript->OnNpcFirstBeAttacked( this, pAttacker, monsterID );
	return true;
}

#endif //STAGE_MAP_SCRIPT


bool CMuxMap::CreateMapBuff( unsigned int race, unsigned int buffID, unsigned int buffTime )
{
	if( IsInsCopyMap() )
	{
		if( m_pCopyMapInfo )
		{
			m_pCopyMapInfo->CreateMapBuff( race, buffID, buffTime );
		}
	}else{
		if( m_pNormalMapInfo )
		{
			m_pNormalMapInfo->CreateMapBuff( race, buffID, buffTime );
		}
	}
	return true;
}


bool CMuxMap::OnNpcAttackNpc( CMonster* pAtkMonster, CMonster* pTargetMonster, CNpcSkill* pNpcSkill, EScopeFlag scopeFlag, const PlayerID& orgTargetID )
{
	if( NULL == pAtkMonster || NULL == pTargetMonster || NULL == pNpcSkill )
		return false;

	if( pAtkMonster == pTargetMonster )
		return false;

	if( scopeFlag == SCOPE_ATTACK )
	{
		if( !CBattleRule::NpcCheckNpc( pAtkMonster, pTargetMonster ) )
			return false;
	}

	
	CFightCalScript* pScript = CFightCalScriptManager::FindScripts( FCAL_SCRIPT_NAME );
	if( NULL == pScript )
	{
		D_ERROR("CMuxMap::OnNpcAttackNpc,找不到战斗计算的脚本\n");
		return false;
	}

	vector<int> vecRst;
	pScript->OnCvcCal( pAtkMonster, pTargetMonster, pNpcSkill, &vecRst); 
	if ( vecRst.size() != 3)
	{
		D_ERROR("CMuxMap::OnNpcAttackNpc,怪物打斗脚本有错\n");
		return false;
	}
	EBattleFlag byHit = ( vecRst[1] == 0) ? HIT : MISS;
	bool bCritical = (vecRst[0] == 2) ? true : false;
	if ( vecRst[2] < 0 )
	{
		///by dzj, 09.03.26，代码规范修改；
		D_ERROR( "OnNpcAttackNpc，nDamage 小于 0\n" );
		vecRst[2] = 0;
	}
	unsigned int nDamage = (unsigned int) ( vecRst[2] );
	if( bCritical )
	{
		byHit = CRITICAL;
	}

	if ( (nDamage <= 0) && (byHit != MISS) )
	{
		D_WARNING( "OnNpcAttackNpc，OnCvcCal, 伤血为0但返回为非MISS\n" );
	}

	if( !pTargetMonster->GetAttackFlag() || pTargetMonster->GetNpcType() == 13 )
	{
		nDamage = 0;
		byHit = MISS;
	}
	if( byHit == MISS )
	{
		nDamage = 0;
	}
	pTargetMonster->SubMonsterHp( nDamage, NULL, false, 0 );
	pTargetMonster->OnBeAttacckByNpc( pAtkMonster, nDamage );

	unsigned int buffID = pNpcSkill->m_nLinkID;
	float linkRate = pNpcSkill->m_fLinkRate;
	unsigned int assistObject = pNpcSkill->m_nAssistObject;
	if( buffID != 0 )
	{
		CBuffProp* pBuffProp = CManBuffProp::FindObj( buffID );
		if( pBuffProp )
		{
			if( RandRate( linkRate ) )
			{
				if( assistObject == 1)
				{
					//pTargetMonster->CreateBuff( buffID , pNpcSkill->m_nPeriodTime, pAtkMonster->GetCreatureID(), CREATE_MONSTER );
				}else{
					//pAtkMonster->CreateBuff( buffID, pNpcSkill->m_nPeriodTime, pAtkMonster->GetCreatureID(), CREATE_PLAYER );
				}
			}
		}
	}
	
	MGPlayerAttackTarget battleMsg;
	battleMsg.battleFlag = byHit;
	battleMsg.scopeFlag = scopeFlag;
	battleMsg.lPlayerID = pAtkMonster->GetCreatureID();
	battleMsg.targetID = pTargetMonster->GetCreatureID();
	battleMsg.lHpLeft = pTargetMonster->GetHp();
	battleMsg.lCooldownTime = pNpcSkill->m_nCoolDown;
	battleMsg.consumeItemTypeID = 0;
	battleMsg.lCurseTime = pNpcSkill->m_nCurseTime;
	battleMsg.lHpSubed = nDamage;
	battleMsg.mainTargetID = orgTargetID;
	battleMsg.nAtkPlayerHP = pAtkMonster->GetHp();
	battleMsg.skillID = pNpcSkill->m_lNpcSkillID;
	battleMsg.nErrCode = BATTLE_SUCCESS;
	NotifySurrounding<MGPlayerAttackTarget>( &battleMsg, pTargetMonster->GetPosX(),pTargetMonster->GetPosY() );
	return true;
}




#ifdef ITEM_NPC 
void CMuxMap::FillChangedItemNpcInfo()
{
	StructMemSet( m_itemNpcChangedInfo, 0, sizeof( m_itemNpcChangedInfo ) );

	unsigned int arrSize = ARRAY_SIZE( m_itemNpcChangedInfo.itemNpc.buildNpcArr ), count = 0;;

	for( size_t i = 0; i< ARRAY_SIZE( m_pArrMapItemNpc ); ++i )
	{
		if( count >= arrSize )
		{
			D_WARNING("变化的数量超出了预计的数量%d\n", arrSize );
			break;
		}

		if( NULL == m_pArrMapItemNpc[i] )
		{
			continue;
		}

		if( m_pArrMapItemNpc[i]->initStatus != m_pArrMapItemNpc[i]->curStatus )
		{
			m_itemNpcChangedInfo.itemNpc.buildNpcArr[count].buildSeq = m_pArrMapItemNpc[i]->itemNpcSeq;
			m_itemNpcChangedInfo.itemNpc.buildNpcArr[count].curStatus = m_pArrMapItemNpc[i]->curStatus;
			count++;
		}
	}
	m_itemNpcChangedInfo.itemNpc.buildNpcCount = count;
}

bool CMuxMap::IsNeedNotifyItemNpc()
{
	return (m_itemNpcChangedInfo.itemNpc.buildNpcCount != 0);
}


void CMuxMap::NotifyAllChangedItemNpcStatus( CPlayer* pPlayer )
{
	if( NULL == pPlayer )
		return;

	pPlayer->SendPkg<MGItemNpcStatusInfo>( &m_itemNpcChangedInfo );
}


bool CMuxMap::SetMapItemNpcState( unsigned int itemNpcSeq, unsigned int curStatus )
{
	if( itemNpcSeq >= (unsigned int)ARRAY_SIZE( m_pArrMapItemNpc ) )
	{
		D_ERROR("SetMapItemNpcMonsterId itemNpcSeq:%d超出了m_pArrMapItemNpc的界限\n", itemNpcSeq );
		return false;
	}

	if( NULL == m_pArrMapItemNpc[itemNpcSeq] )
	{
		D_ERROR("SetMapItemNpcState itemNpcSeq:%d在m_pArrMapItemNpc还没有生成实例\n", itemNpcSeq );
		return false;
	}


	if( m_pArrMapItemNpc[itemNpcSeq]->resMonsterId == 0 )
	{
		D_ERROR("当前序列%d 还未填充怪物,不能设置状态\n", itemNpcSeq );
		return false;
	}

	if( m_pArrMapItemNpc[itemNpcSeq]->curStatus == curStatus )
	{
		D_WARNING("物件NPC已经是此状态%d 没必要设置\n", curStatus);
		return false;
	}

	m_pArrMapItemNpc[itemNpcSeq]->curStatus = curStatus;
	//单个物件NPC变化状态的全服通知(针对副本地图)
	MGItemNpcStatusInfo info;
	info.itemNpc.buildNpcCount = 1;
	info.itemNpc.buildNpcArr[0].curStatus = m_pArrMapItemNpc[itemNpcSeq]->curStatus;
	info.itemNpc.buildNpcArr[0].buildSeq = itemNpcSeq;
	NotifyAllUseBrocast<MGItemNpcStatusInfo>( &info );

	//同时改变所有变化NPC状态的缓存
	FillChangedItemNpcInfo();

	return true;
}

bool CMuxMap::SetItemNpc( unsigned int itemSeq, MapItemNpc* pMapItemNpc )
{
	if( itemSeq >= ARRAY_SIZE( m_pArrMapItemNpc ) )
	{
		D_ERROR("SetItemNpc itemSeq 大于m_pArrMapItemNpc数组SIZE\n");
		return false;
	}

	if(NULL == pMapItemNpc)
	{
		return false;
	}

	m_pArrMapItemNpc[itemSeq] = pMapItemNpc;
	return true;
}

//void CMuxMap::ItemNpcSort()
//{
//	sort( m_vecMapItemNpc.begin(), m_vecMapItemNpc.end() );
//}


bool CMuxMap::SetMapItemNpcMonsterId( unsigned int itemNpcSeq, unsigned int monsterId )
{
	if( itemNpcSeq >= (unsigned int)ARRAY_SIZE( m_pArrMapItemNpc ) )
	{
		D_ERROR("SetMapItemNpcMonsterId itemNpcSeq:%d超出了itemnpc.xml的界限\n", itemNpcSeq );
		return false;
	}

	if( NULL == m_pArrMapItemNpc[itemNpcSeq] )
	{
		D_ERROR("SetMapItemNpcMonsterId itemNpcSeq:%d在m_pArrMapItemNpc还没有生成实例\n", itemNpcSeq );
		return false;
	}

	m_pArrMapItemNpc[itemNpcSeq]->resMonsterId = monsterId;
	return true;
}

bool CMuxMap::GetCharaterIDByItemSeq( unsigned int itemSeq, PlayerID& characterID )
{
	if( itemSeq >= (unsigned int)ARRAY_SIZE( m_pArrMapItemNpc ) )
	{
		D_ERROR("GetCharaterIDByItemSeq itemNpcSeq:%d超出了itemnpc.xml的界限\n", itemSeq );
		return false;
	}

	if( NULL == m_pArrMapItemNpc[itemSeq] )
	{
		D_ERROR("GetCharaterIDByItemSeq itemNpcSeq:%d在m_pArrMapItemNpc还没有生成实例\n", itemSeq );
		return false;
	}

	characterID.dwPID = m_pArrMapItemNpc[itemSeq]->resMonsterId;
	characterID.wGID = MONSTER_GID;
	return true;
}


bool CMuxMap::IsItemNpcChange( unsigned int itemSeq, unsigned int& changedStatus )
{
	if( itemSeq >= (unsigned int)ARRAY_SIZE( m_pArrMapItemNpc ) )
	{
		D_ERROR("IsItemNpcChange itemNpcSeq:%d超出了itemnpc.xml的界限\n", itemSeq );
		return false;
	}

	if( NULL == m_pArrMapItemNpc[itemSeq] )
	{
		D_ERROR("IsItemNpcChange itemNpcSeq:%d在m_pArrMapItemNpc还没有生成实例\n", itemSeq );
		return false;
	}

	if( m_pArrMapItemNpc[itemSeq]->curStatus != m_pArrMapItemNpc[itemSeq]->initStatus )
	{
		changedStatus = m_pArrMapItemNpc[itemSeq]->curStatus;
		return true;
	}
	return false;
}


bool CMuxMap::IsHaveItemNpc( unsigned int itemSeq )
{
	if( itemSeq >= (unsigned int)ARRAY_SIZE( m_pArrMapItemNpc ) )
	{
		D_ERROR("IsHaveItemNpc itemNpcSeq:%d超出了m_pArrMapItemNpc的界限\n", itemSeq );
		return false;
	}

	return ( NULL != m_pArrMapItemNpc[itemSeq] );
}

#endif //ITEM_NPC

///技能攻击目标检测;
bool CMuxMap::SkillAttackTargetCheck( CPlayer* pAttackPlayer, const PlayerID& targetID
									 , unsigned int& checkRet, CPlayer*& pTgtPlayer, CMonster*& pTgtMonster, TYPE_POS& targetPosX, TYPE_POS& targetPosY, bool& isPvc )
{
	checkRet = -1;
	pTgtPlayer = NULL;
	pTgtMonster = NULL;
	targetPosX = targetPosY = 0;
	isPvc = true;

	if ( NULL == pAttackPlayer )
	{
		D_ERROR("SkillAttackTargetCheck, NULL == pAttackPlayer\n");
		return false;
	}

	if ( targetID.wGID > MONSTER_GID )
	{
		D_ERROR("SkillAttackTargetCheck, targetID.wGID > MONSTER_GID\n");
		return false;
	}

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("SkillAttackTargetCheck，找不到战斗脚本,使用技能失败\n");
		return false;
	}

	if ( MONSTER_GID == targetID.wGID ) //对怪攻击
	{			
		pTgtMonster = GetMonsterPtrInMap( targetID.dwPID );
		if( NULL == pTgtMonster )
		{
			//错误返回
			checkRet = TARGET_INVALID_ERROR;
			D_ERROR("SkillAttackTargetCheck，找不到欲攻击的目标怪物%d\n", targetID.dwPID);
			return false;
		}

		if ( pTgtMonster->IsMonsterDie() )
		{
			checkRet = TARGET_INVALID_ERROR;
			D_ERROR("SkillAttackTargetCheck，目标怪物%d已死亡\n", targetID.dwPID);
			return false;
		}

		if( !pScript->FnCheckPveBattleSecondStage( pAttackPlayer, pTgtMonster, checkRet ) )
		{
			////脚本调用错误
			checkRet = OTHER_ERROR;
			D_WARNING("SkillAttackTargetCheck，第二阶段检测目标怪物%d失败\n", targetID.dwPID);
			return false;
		}
		if( checkRet != 0 )
		{
			D_WARNING("SkillAttackTargetCheck，检测目标怪物%d失败，其它错误%d\n", targetID.dwPID, checkRet);
			return false;
		}

		targetPosX = pTgtMonster->GetPosX();
		targetPosY = pTgtMonster->GetPosY();
	} else { //对人攻击
		CGateSrv* pGateSrv = CManG_MProc::FindProc( targetID.wGID );
		if( NULL == pGateSrv )
		{
			//错误返回
			checkRet = TARGET_INVALID_ERROR;
			D_ERROR("SkillAttackTargetCheck，找不到欲攻击玩家%d:%d所在gate\n", targetID.wGID, targetID.dwPID);
			return false;
		}
		pTgtPlayer = pGateSrv->FindPlayer( targetID.dwPID );
		if( NULL == pTgtPlayer )
		{
			//错误返回
			checkRet = TARGET_INVALID_ERROR;
			D_ERROR("SkillAttackTargetCheck，相应gate找不到欲攻击玩家%d:%d\n", targetID.wGID, targetID.dwPID);
			return false;
		}

		if ( pTgtPlayer->IsDying() )
		{
			checkRet = TARGET_INVALID_ERROR;
			D_ERROR("SkillAttackTargetCheck，欲攻击玩家%s(%d:%d)已死亡\n", pTgtPlayer->GetAccount(), targetID.wGID, targetID.dwPID);
			return false;
		}

		if( !pScript->FnCheckPvpBattleSecondStage( pAttackPlayer, pTgtPlayer, checkRet ) )
		{
			checkRet = OTHER_ERROR;
			D_WARNING("SkillAttackTargetCheck，第二阶段检测目标玩家%d:%d失败\n", targetID.wGID, targetID.dwPID);
			return false;
		}

		if( checkRet != 0 )
		{
			D_WARNING("SkillAttackTargetCheck，第二阶段检测目标玩家%d:%d，其它错误\n", targetID.wGID, targetID.dwPID);
			return false;
		}

		isPvc = false;
		targetPosX = pTgtPlayer->GetPosX();
		targetPosY = pTgtPlayer->GetPosY();
	}

	return true;
} //CMuxMap::SkillAttackTargetCheck

//战斗判断，消耗脚本以及分割技能攻击者位置重设
bool CMuxMap::PlayerUseSkillCommonCheck( CPlayer* pUseSkillPlayer, const GMUseSkill2* pUseSkillInfo
										, EBattleResult& ret/*技能使用结果*/, unsigned int& logSkillID/*用于记日志（在使用技能成功时才记日志，但是在判断的过程中技能编号有可能会变成新技能编号，因此保存旧的技能编号）*/
										, bool& isSplitSkill/*是否分割技能*/, CMuxPlayerSkill* pSkill, bool& isAoe/*是否aoe*/, bool& isTeamAoe/*是否组队aoe*/, bool& isNormalAoe/*是否普通aoe*/)
{
	if ( ( NULL == pUseSkillPlayer )
		|| ( NULL == pUseSkillInfo )
		|| ( NULL == pSkill )
		)
	{
		D_ERROR( "CMuxMap::PlayerUseSkillCommonCheck, 输入参数空\n" );
		return false;
	}

	CBattleScript* pBattleScript = CBattleScriptManager::GetScript();
	if( NULL == pBattleScript )
	{
		D_ERROR("CMuxMap::PlayerUseSkillCommonCheck，找不到战斗脚本,使用技能失败\n");
		return false;
	}

	//CT时间判断
	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	if( pUseSkillPlayer->GetNextActionTime() > currentTime )
	{
		D_WARNING( "玩家%s使用技能%d，其处于CT时间内\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
		ret = SKILL_IN_CT_TIME_ERROR;
		return false;
	}//以下不立刻重设CT时间，因为本次攻击还不一定成功，直至第二阶段判断通过后才设；

	//if( UNION_SKILL == pUseSkillInfo->useSkill.skillType )
	//{
	//	pSkill = pUseSkillPlayer->FindMuxSkill( pUseSkillInfo->useSkill.skillID, false );
	//}else{
	//	pSkill = pUseSkillPlayer->FindMuxSkill( pUseSkillInfo->useSkill.skillID );
	//}
	//if( NULL == pSkill )
	//{
	//	//错误返回
	//	D_WARNING( "玩家%s使用技能%d，不拥有该技能\n", pUseSkillPlayer->GetAccount(), pUseSkillInfo->useSkill.skillID );
	//	ret = INVALID_SKILL_ID_ERROR;
	//	return false;
	//}

	//非有益技能，只能在非骑乘态下使用；
	if ( !pSkill->IsGoodSkill() )
	{
		if ( pUseSkillPlayer->IsRideState() )
		{
			D_WARNING( "玩家%s使用技能%d，处于骑乘态，不能使用该技能\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
			ret = PLAYER_STATE_ERROR;
			return false;
		}
	}

	//休息时不允许施放技能
	if( pUseSkillPlayer->IsInRest() )
	{
		D_WARNING( "玩家%s使用技能%d，处于休息态，不能使用该技能\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
		ret = PLAYER_STATE_ERROR;
		return false;
	}

#ifdef HAS_PATH_VERIFIER
	//分割技能阻挡判定
	if(pUseSkillInfo->useSkill.skillType == PARTION_SKILL)/*如果是动作分割技能，判断是否通过阻挡验证[zsw: 20091217] */
	{
		if(!BlockTestOK(pUseSkillPlayer->GetFloatPosX(), pUseSkillPlayer->GetFloatPosY()
			, (float)pUseSkillInfo->useSkill.splitPosX/10/*客户端发来单位0.1*/, (float)pUseSkillInfo->useSkill.splitPosY/10/*客户端发来单位0.1*/))
		{
			D_WARNING( "玩家%s使用分割技能%d，阻挡判定失败\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
			ret = PLAYER_POS_ERROR;
			return false;
		}
	}
#endif /*HAS_PATH_VERIFIER*/

	//第一阶段判断
	logSkillID = pSkill->GetSkillID();//保存技能编号
	unsigned int tmpret = 0;
	if( !pBattleScript->FnCheckBattleFirstStage( pUseSkillPlayer, pSkill->GetEffectiveSkillID(), tmpret ) )
	{
		D_WARNING( "玩家%s使用技能%d，第一阶段判定失败\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
		ret = OTHER_ERROR;
		return false;
	}

	ret = (EBattleResult) tmpret;

	if( BATTLE_SUCCESS != ret )
	{
		//第一阶段前检测失败;
		D_WARNING( "玩家%s使用技能%d，其它判定失败%d\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID(), ret );
		return false;
	}

	isAoe = pSkill->IsAoeSkill();
	if ( isAoe )
	{
		//aoe，判断是普通aoe还是组队aoe;
		if( pSkill->IsTeamSkill() )
		{
			isTeamAoe = true;
		} else {
			isNormalAoe = true;
		}
	} else {
		//非aoe，目标数不能大于1;
		if (pUseSkillInfo->useSkill.targetNum>1)
		{
			D_ERROR("玩家%s使用非aoe技能%d，但目标数大于1\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
			ret = TARGET_INVALID_ERROR;
			return false;
		}
	}

	//以下为直接攻击目标判定；
	if ( 0 == pUseSkillInfo->useSkill.targetNum ) //空放
	{
		//空放，判断该技能是否可空放;
		if (!pSkill->IsNoTargetValid())
		{
			D_WARNING( "玩家%s空放技能%d，该技能不允许空放\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
			ret = TARGET_INVALID_ERROR;
			return false;
		}

		pUseSkillPlayer->SetNextActionTime( ACE_OS::gettimeofday() + ACE_Time_Value( 0, pSkill->GetCurseTime() * 1000) );
		if( !pBattleScript->FnConsumeStagePvp( pUseSkillPlayer, tmpret ) )		//减去消耗
		{
			ret = (EBattleResult) tmpret;
			//脚本调用错误
			D_ERROR( "玩家%s空放攻击，调用消耗脚本失败\n", pUseSkillPlayer->GetNickName() );
			if (BATTLE_SUCCESS == ret)
			{
				ret = OTHER_ERROR;
			}
			return false;
		}
	} else if ( 1 == pUseSkillInfo->useSkill.targetNum ) { //单目标
		CPlayer* pTgtPlayer = NULL;
		CMonster* pTgtMonster = NULL;
		TYPE_POS targetPosX=0, targetPosY=0;
		bool isPvc = false;
		//单个目标，则进行此目标的攻击判定；
		if (!SkillAttackTargetCheck( pUseSkillPlayer, pUseSkillInfo->useSkill.arrTarget[0], tmpret, pTgtPlayer, pTgtMonster, targetPosX, targetPosY, isPvc ))
		{
			ret = (EBattleResult) tmpret;
			D_WARNING( "玩家%s施放技能%d，目标%d判定失败\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID(), pUseSkillInfo->useSkill.arrTarget[0].dwPID );
			ret = TARGET_INVALID_ERROR;
			return false;;
		}
		pUseSkillPlayer->SetNextActionTime( ACE_OS::gettimeofday() + ACE_Time_Value( 0, pSkill->GetCurseTime() * 1000) );//经过了第二阶段判断，CT时间重设

		if( !pBattleScript->FnConsumeStagePvc( pUseSkillPlayer, tmpret ) )		//减去消耗
		{
			ret = (EBattleResult) tmpret;
			//脚本调用错误
			D_ERROR( "玩家%s攻击单个目标，调用消耗脚本失败\n", pUseSkillPlayer->GetNickName() );
			if (BATTLE_SUCCESS == ret){
				ret = OTHER_ERROR;
			}
			return false;
		}
	} else { //多目标
		//多个目标，aoe技能，不进行攻击目标判定
		pUseSkillPlayer->SetNextActionTime( ACE_OS::gettimeofday() + ACE_Time_Value( 0, pSkill->GetCurseTime() * 1000) );
		if( !pBattleScript->FnConsumeStagePvp( pUseSkillPlayer, tmpret ) )		//减去消耗
		{
			ret = (EBattleResult) tmpret;
			//脚本调用错误
			D_ERROR( "玩家%s范围攻击，调用消耗脚本失败\n", pUseSkillPlayer->GetNickName() );
			if (BATTLE_SUCCESS == ret){
				ret = OTHER_ERROR;
			}
			return false;
		}
	}

	//0、若为动作分割技能，进行位置重设
	if( pSkill->GetSpiltProp() && (PARTION_SKILL == pUseSkillInfo->useSkill.skillType) )
	{
		isSplitSkill = true;
		//验证设置的点和target之间的关系,应该不能相差太大
		int dis = abs( (int)(pUseSkillInfo->useSkill.splitPosX/10/*客户端发来单位0.1*/ - pUseSkillPlayer->GetPosX()) );
		dis += abs( (int)(pUseSkillInfo->useSkill.splitPosY/10/*客户端发来单位0.1*/ - pUseSkillPlayer->GetPosY()) );
		if( dis >= 30 )
		{
			D_ERROR("玩家%s使用动作分割技能%d，目标和当前位置间的距离相差太大\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
			ret = TARGET_INVALID_ERROR;
			return false;
		}
		//设置攻击者新位置
		pUseSkillPlayer->ForceSetFloatPos( false/*作移动处理，视野之内不再通知，留给客户端播动作时间*/, pUseSkillInfo->useSkill.splitPosX/(float)10/*客户端发来单位0.1*/, pUseSkillInfo->useSkill.splitPosY/(float)10/*客户端发来单位0.1*/ );
	}	

	return true;
} // bool CMuxMap::PlayerUseSkillCommonCheck

//空放技能成功处理
bool CMuxMap::SucEmptyUseProc( CPlayer* pUseSkillPlayer, const GMUseSkill2* pUseSkillInfo, CMuxPlayerSkill* pSkill, bool isSplitSkill, EBattleResult ret )
{
	if ( ( NULL == pUseSkillPlayer )
		|| ( NULL == pUseSkillInfo )
		|| ( NULL == pSkill )
		)
	{
		D_ERROR( "CMuxMap::SucEmptyUseProc, 输入参数空\n" );
		return false;
	}

	CBattleScript* pBattleScript = CBattleScriptManager::GetScript();
	if( NULL == pBattleScript )
	{
		D_ERROR("CMuxMap::SucEmptyUseProc，找不到战斗脚本,使用技能失败\n");
		return false;
	}

	MGRstToAtker rstToAtker;
	rstToAtker.gcRstToAtker.atkCommonInfo.seqID = pUseSkillInfo->useSkill.attackSeq;
	rstToAtker.gcRstToAtker.atkCommonInfo.cliID = pUseSkillInfo->useSkill.cliID;
	rstToAtker.gcRstToAtker.atkCommonInfo.isLastMsg = pUseSkillInfo->useSkill.isSeqLast;
	rstToAtker.gcRstToAtker.atkCommonInfo.attackerID = pUseSkillPlayer->GetFullID();
	rstToAtker.gcRstToAtker.atkCommonInfo.battleFlag = HIT;//正常应由战斗脚本计算出来，现在空放未到那一步
	rstToAtker.gcRstToAtker.atkCommonInfo.skillID = pSkill->GetEffectiveSkillID();
	rstToAtker.gcRstToAtker.atkCommonInfo.orgTargetID.wGID = 0;
	rstToAtker.gcRstToAtker.atkCommonInfo.orgTargetID.dwPID = 0;
	rstToAtker.gcRstToAtker.atkCommonInfo.skillType = pUseSkillInfo->useSkill.skillType;
	rstToAtker.gcRstToAtker.infoToAtker.errCode = ret;
	rstToAtker.gcRstToAtker.infoToAtker.curseTime = pSkill->GetCurseTime();
	rstToAtker.gcRstToAtker.infoToAtker.cooldownTime = pSkill->GetCoolDown();
	rstToAtker.gcRstToAtker.infoToAtker.attackHP = pUseSkillPlayer->GetCurrentHP();
	rstToAtker.gcRstToAtker.infoToAtker.attackMP = pUseSkillPlayer->GetCurrentMP();
	rstToAtker.gcRstToAtker.splitPosX = pUseSkillInfo->useSkill.splitPosX;
	rstToAtker.gcRstToAtker.splitPosY = pUseSkillInfo->useSkill.splitPosY;

	pUseSkillPlayer->SendPkg<MGRstToAtker>( &rstToAtker );

	//通知周边空放成功；
	if ( isSplitSkill )
	{
		//检查技能类型和其他参数类型,动作分割最后个消息也要赋
		unsigned short arrRst[5];
		StructMemSet(arrRst, 0, sizeof(arrRst));
		if ( !pBattleScript->FnCheckSkillSpilt( arrRst ) )
		{
			//调用错误
			D_ERROR("玩家%s使用分割技能%d成功，但FnCheckSkillSplit失败\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID());
			return false;
		}
		if ( 0 == arrRst[0] )
		{
			D_ERROR("2:玩家%s使用分割技能%d成功，但FnCheckSkillSplit返回结果错误\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID());
			return false;
		}

		MGSplitAttack splitAttack;
		StructMemSet( splitAttack, 0, sizeof(splitAttack));
		splitAttack.gcSplitAttack.atkCommonInfo = rstToAtker.gcRstToAtker.atkCommonInfo;
		splitAttack.gcSplitAttack.splitPosX = pUseSkillInfo->useSkill.splitPosX;
		splitAttack.gcSplitAttack.splitPosY = pUseSkillInfo->useSkill.splitPosY;
		splitAttack.gcSplitAttack.splitArg1 = arrRst[1];
		splitAttack.gcSplitAttack.splitArg2 = arrRst[2];
		splitAttack.gcSplitAttack.splitArg3 = arrRst[3];
		splitAttack.gcSplitAttack.splitArg4 = arrRst[4];
		splitAttack.gcSplitAttack.tgtNum = 0;
		NotifySurrounding<MGSplitAttack>( &splitAttack, pUseSkillPlayer->GetPosX(), pUseSkillPlayer->GetPosY() );
	} else {
		MGNormalAttack normalAttack;
		StructMemSet( normalAttack, 0, sizeof(normalAttack));
		normalAttack.gcNormalAttack.atkCommonInfo = rstToAtker.gcRstToAtker.atkCommonInfo;
		normalAttack.gcNormalAttack.tgtNum = 0;
		NotifySurrounding<MGNormalAttack>( &normalAttack, pUseSkillPlayer->GetPosX(), pUseSkillPlayer->GetPosY() );
	}

	return true;
}// bool CMuxMap::SucEmptyUseProc( CPlayer* pUseSkillPlayer )

//玩家使用技能，6.30版本矩形攻击及相应范围攻击修改
bool CMuxMap::OnPlayerUseSkill2( CPlayer* pUseSkillPlayer, const GMUseSkill2* pUseSkillInfo )
{
	if( NULL == pUseSkillPlayer || NULL == pUseSkillInfo )
	{
		D_ERROR( "OnPlayerUseSkill, NULL == pUseSkillPlayer || NULL == pUseSkillInfo\n" );
		return false;
	}

#ifdef _DEBUG
	D_INFO("玩家%s使用技能 ID=%d, isSeqLast:%s\n", pUseSkillPlayer->GetNickName(), pUseSkillInfo->useSkill.skillID, pUseSkillInfo->useSkill.isSeqLast?"true":"false" );
#endif //_DEBUG

	pUseSkillPlayer->ResetMoveStat();//重置玩家移动状态；

	if ( pUseSkillInfo->useSkill.targetNum > ARRAY_SIZE(pUseSkillInfo->useSkill.arrTarget) )
	{
		D_ERROR( "GMUseSkill2消息，越界的targetNum(%d)\n", pUseSkillInfo->useSkill.targetNum );
		return false;
	}

	CMuxPlayerSkill* pSkill = NULL;
	{
		//无论成败，固定扣蓝;
		if( UNION_SKILL == pUseSkillInfo->useSkill.skillType )
		{
			pSkill = pUseSkillPlayer->FindMuxSkill( pUseSkillInfo->useSkill.skillID, false );
		}else{
			pSkill = pUseSkillPlayer->FindMuxSkill( pUseSkillInfo->useSkill.skillID );
		}
		if( NULL == pSkill )
		{
			//错误返回
			D_WARNING( "玩家%s使用技能%d，不拥有该技能\n", pUseSkillPlayer->GetAccount(), pUseSkillInfo->useSkill.skillID );
			return false;
		}

		CMuxSkillProp* pSkillProp = pSkill->GetSkillProp();
		if (NULL == pSkillProp)
		{
			D_WARNING( "玩家%s使用技能%d，找不到该技能属性\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
			return false;
		}

		if ( pSkillProp->m_castTime != 0 )
		{//该技能需要读条
			if ( pUseSkillPlayer->GetCastManager().GetCastSkillID() != pSkillProp->m_skillID )
			{
				D_WARNING("玩家%s使用需要读条技能%d，但玩家不处于读条状态\n", pUseSkillPlayer->GetNickName(),pSkillProp->m_skillID);
				pUseSkillPlayer->GetCastManager().OnStop();
				return false;
			}
			unsigned int current = GetTickCount();
			if ((int)current - (int)pUseSkillPlayer->GetCastManager().GetCastStartTime() < pUseSkillPlayer->GetCastManager().GetCastTime()*1000 - 1000/*稍微放宽1秒*/ )
			{
				D_WARNING("玩家%s使用需要读条技能%d，但玩家读条时间还未到\n", pUseSkillPlayer->GetNickName(),pSkillProp->m_skillID);
				pUseSkillPlayer->GetCastManager().OnStop();
				return false;
			}
		}

		if ( pUseSkillPlayer->GetCastManager().IsInCast() )
		{//如果处于读条状态，则打断
			pUseSkillPlayer->GetCastManager().OnStop();
		}

		//扣MP;
		if ( !(pUseSkillPlayer->SubPlayerMp( pSkillProp->m_nUllageMp, pSkillProp->m_nUllageMpPer )) )
		{
			//MP不足;
			D_WARNING( "玩家%s使用技能%d，玩家当前MP%d不足(%d,%f)\n"
				, pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID(), pUseSkillPlayer->GetCurrentMP(), pSkillProp->m_nUllageMp, pSkillProp->m_nUllageMpPer );
			return false;
		}

		//技能使用基本属性点要求判定
		if ( !(pUseSkillPlayer->UseSkillBaseAttCheck( pSkillProp )) )
		{
			D_WARNING( "玩家%s使用技能%d，玩家当前基本属性点不足\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
			return false;
		}
	}

	if ( NULL == pSkill )
	{
		D_ERROR( "OnPlayerUseSkill2, NULL == pSkill 2\n" );
		return false;
	}

    if ( pUseSkillInfo->useSkill.targetNum > ARRAY_SIZE(pUseSkillInfo->useSkill.arrTarget) )
	{
		D_ERROR( "OnPlayerUseSkill2, 玩家攻击消息非法, tgtNum:%d，数组大小:%d\n", pUseSkillInfo->useSkill.targetNum, ARRAY_SIZE(pUseSkillInfo->useSkill.arrTarget) );
		return false;
	}

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("OnPlayerUseSkill2，找不到战斗脚本,使用技能失败\n");
		return false;
	}

	EBattleResult ret = BATTLE_SUCCESS;//技能使用结果；
	unsigned int logSkillID = 0;//用于记日志（在使用技能成功时才记日志，但是在判断的过程中技能编号有可能会变成新技能编号，因此保存旧的技能编号）
	bool         isSplitSkill = false;//是否分割技能；
	bool isAoe = false;//是否aoe;
	bool isTeamAoe = false;//是否组队aoe;
	bool isNormalAoe = false;//是否普通aoe;
	if ( ! PlayerUseSkillCommonCheck( pUseSkillPlayer, pUseSkillInfo, ret, logSkillID, isSplitSkill, pSkill, isAoe, isTeamAoe, isNormalAoe ) )
	{
		D_ERROR("OnPlayerUseSkill2，PlayerUseSkillCommonCheck失败，玩家%s, 技能%d\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
	}

	if (pSkill->GetType() == CALL_SKILL)
	{//挪移术
		bool isOK = CallMemberManagerSingle::instance()->StartCallMember(pUseSkillPlayer);
		if (!isOK)
		{
			ret = OTHER_ERROR;
		}
	}

	if ( BATTLE_SUCCESS != ret ) //攻击校验失败返回相关消息
	{
		//攻击失败，只通知给攻击者
		MGRstToAtker rstToAtker;
		rstToAtker.gcRstToAtker.atkCommonInfo.seqID = pUseSkillInfo->useSkill.attackSeq;
		rstToAtker.gcRstToAtker.atkCommonInfo.cliID = pUseSkillInfo->useSkill.cliID;
		rstToAtker.gcRstToAtker.atkCommonInfo.isLastMsg = pUseSkillInfo->useSkill.isSeqLast;
		rstToAtker.gcRstToAtker.atkCommonInfo.attackerID = pUseSkillPlayer->GetFullID();
		D_DEBUG( "玩家%s使用技能%d攻击，CommonCheck失败，返回MISS\n", pUseSkillPlayer->GetAccount(), pUseSkillInfo->useSkill.skillID );
		rstToAtker.gcRstToAtker.atkCommonInfo.battleFlag = MISS;//正常应由战斗脚本计算出来，现在未到那一步
		rstToAtker.gcRstToAtker.atkCommonInfo.skillID = pUseSkillInfo->useSkill.skillID;
		rstToAtker.gcRstToAtker.atkCommonInfo.orgTargetID.wGID = 0;
		rstToAtker.gcRstToAtker.atkCommonInfo.orgTargetID.dwPID = 0;
		rstToAtker.gcRstToAtker.atkCommonInfo.skillType = pUseSkillInfo->useSkill.skillType;
		rstToAtker.gcRstToAtker.infoToAtker.errCode = ret;
		rstToAtker.gcRstToAtker.infoToAtker.curseTime = 0;
		rstToAtker.gcRstToAtker.infoToAtker.cooldownTime = 0;//如果是调消耗脚本失败，则由于服务器计算了cooldown，而客户端没有，因此可能客户端稍后的攻击消息会cooldown验证失败
		rstToAtker.gcRstToAtker.infoToAtker.attackHP = pUseSkillPlayer->GetCurrentHP();
		rstToAtker.gcRstToAtker.infoToAtker.attackMP = pUseSkillPlayer->GetCurrentMP();
		rstToAtker.gcRstToAtker.splitPosX = pUseSkillInfo->useSkill.splitPosX;
		rstToAtker.gcRstToAtker.splitPosY = pUseSkillInfo->useSkill.splitPosY;
		pUseSkillPlayer->SendPkg<MGRstToAtker>( &rstToAtker );
		return false;
	}

	//以下攻击成功
	if (NULL == pSkill)
	{
		D_ERROR("不可能错误，玩家%s使用技能%d成功，但NULL == pSkill\n", pUseSkillPlayer->GetAccount(), pUseSkillInfo->useSkill.skillID);
		return false;
	}

	//以下技能施放成功；
	pUseSkillPlayer->UseSkillTimeAffectWear();	//使用技能影响的耐久

	if ( 0 == pUseSkillInfo->useSkill.targetNum ) //空放
	{
		//空放成功处理；
		bool isemptyok = SucEmptyUseProc( pUseSkillPlayer, pUseSkillInfo, pSkill, isSplitSkill, ret );
		D_DEBUG("玩家%s空放技能%d，结果%d\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID(), isemptyok?1:0);
		return isemptyok;
	} // if ( 0 == pUseSkillInfo->useSkill.targetNum ) //空放成功；

	//以下为成功施放的非空放技能；
	CLogManager::DoUseSkill(pUseSkillPlayer, pSkill->GetSkillType(), logSkillID, pSkill->IsSpSkill());//记日志

	//2、以下为攻击消息中指明的目标依次进行校验攻击(省去原来的各攻击目标查找), 记录被攻击者位置(添加最终通知块)

	MGRstToAtker rstToAtker;
	rstToAtker.gcRstToAtker.atkCommonInfo.seqID = pUseSkillInfo->useSkill.attackSeq;
	rstToAtker.gcRstToAtker.atkCommonInfo.cliID = pUseSkillInfo->useSkill.cliID;
	rstToAtker.gcRstToAtker.atkCommonInfo.isLastMsg = pUseSkillInfo->useSkill.isSeqLast;
	rstToAtker.gcRstToAtker.atkCommonInfo.attackerID = pUseSkillPlayer->GetFullID();
	rstToAtker.gcRstToAtker.atkCommonInfo.battleFlag = HIT;//正常应由战斗脚本计算出来，现在空放未到那一步 ???
	rstToAtker.gcRstToAtker.atkCommonInfo.skillID = pSkill->GetEffectiveSkillID();
	rstToAtker.gcRstToAtker.atkCommonInfo.orgTargetID = pUseSkillInfo->useSkill.arrTarget[0];
	rstToAtker.gcRstToAtker.atkCommonInfo.skillType = pUseSkillInfo->useSkill.skillType;
	rstToAtker.gcRstToAtker.infoToAtker.errCode = BATTLE_SUCCESS;
	rstToAtker.gcRstToAtker.infoToAtker.curseTime = pSkill->GetCurseTime();
	rstToAtker.gcRstToAtker.infoToAtker.cooldownTime = pSkill->GetCoolDown();
	rstToAtker.gcRstToAtker.infoToAtker.attackHP = pUseSkillPlayer->GetCurrentHP(); // ???
	rstToAtker.gcRstToAtker.infoToAtker.attackMP = pUseSkillPlayer->GetCurrentMP(); // ???
	rstToAtker.gcRstToAtker.splitPosX = pUseSkillInfo->useSkill.splitPosX;
	rstToAtker.gcRstToAtker.splitPosY = pUseSkillInfo->useSkill.splitPosY;

	pUseSkillPlayer->SendPkg<MGRstToAtker>( &rstToAtker ); //先通知攻击者攻击成功

	MGSplitAttack splitAttack; //广播准备用分割攻击消息
	StructMemSet( splitAttack, 0, sizeof(splitAttack));
	splitAttack.gcSplitAttack.atkCommonInfo = rstToAtker.gcRstToAtker.atkCommonInfo;
	splitAttack.gcSplitAttack.tgtNum = 0;// ???
	if ( isSplitSkill )
	{
		unsigned short arrRst[5];
		StructMemSet(arrRst, 0, sizeof(arrRst));
		if ( !pScript->FnCheckSkillSpilt( arrRst ) )
		{
			//调用错误
			D_ERROR( "玩家%s使用分割技能%d攻击多目标成功，但FnCheckSkillSplit失败\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
			return false;
		}
		if ( 0 == arrRst[0] )
		{
			D_ERROR( "2:玩家%s使用分割技能%d攻击多目标成功，但FnCheckSkillSplit返回结果错误\n", pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID() );
			return false;
		}
		splitAttack.gcSplitAttack.splitPosX = pUseSkillInfo->useSkill.splitPosX;
		splitAttack.gcSplitAttack.splitPosY = pUseSkillInfo->useSkill.splitPosY;
		splitAttack.gcSplitAttack.splitArg1 = arrRst[1];
		splitAttack.gcSplitAttack.splitArg2 = arrRst[2];
		splitAttack.gcSplitAttack.splitArg3 = arrRst[3];
		splitAttack.gcSplitAttack.splitArg4 = arrRst[4];
	}

	MGNormalAttack normalAttack;//广播准备用普通攻击消息
	StructMemSet( normalAttack, 0, sizeof(normalAttack));
	normalAttack.gcNormalAttack.atkCommonInfo = rstToAtker.gcRstToAtker.atkCommonInfo;
	normalAttack.gcNormalAttack.tgtNum = 0;// ???

	AttackInfoPerTgt* tgtInfo = NULL;
	int maxTgtNum = 0;
	BYTE validTgtNum = 0;//normalAttack.gcNormalAttack.tgtNum;	
	if ( isSplitSkill ) 
	{
		//validTgtNum = splitAttack.gcSplitAttack.tgtNum;
		tgtInfo = &(splitAttack.gcSplitAttack.tgtInfo[0]);
		maxTgtNum = ARRAY_SIZE(splitAttack.gcSplitAttack.tgtInfo);
	} else {
		//validTgtNum = normalAttack.gcNormalAttack.tgtNum;
		tgtInfo = &(normalAttack.gcNormalAttack.tgtInfo[0]);
		maxTgtNum = ARRAY_SIZE(normalAttack.gcNormalAttack.tgtInfo);
	}

	CPlayer* pTgtPlayer = NULL;
	CMonster* pTgtMonster = NULL;
	AreaOfInterest attackAoe;
	m_subHpArr.ResetSubHpInfoArr();

	//以下依次处理客户端发来的各目标，对每一目标首先进行位置与范围校验，然后传战斗脚本处理;
	unsigned short aoeTgtX=pUseSkillPlayer->GetPosX(), aoeTgtY=pUseSkillPlayer->GetPosY();//初始化为自身所在点；
	int aoeRange = pSkill->GetAoeRange() + 5;//范围稍为放宽些
	bool firstAoeTgt = true;//第一个目标是否为aoe中心点目标
	if ( isAoe )
	{	
		if ( 1 == pSkill->GetAoeShape() || 3 == pSkill->GetAoeShape() )
		{
			firstAoeTgt = false;//目标以自身为圆心aoe，在稍后的目标遍历中无需重新确定中心；
		} 
	}

	for (int i=0; i<pUseSkillInfo->useSkill.targetNum; ++i)
	{
 		if ( validTgtNum >= maxTgtNum )
		{
			if ( isSplitSkill ) //填充消息中的目标人数;
			{
				splitAttack.gcSplitAttack.tgtNum = validTgtNum;
			} else {
				normalAttack.gcNormalAttack.tgtNum = validTgtNum;
			}
			//待发消息已填满，先向周围广播一次(同时内部清空resblocks和相应的广播消息)；
			NotifyResBlockAttInfo( isSplitSkill, false, attackAoe, splitAttack, normalAttack, validTgtNum);
			if ( validTgtNum >= maxTgtNum )
			{
				D_ERROR("不可能错误, validTgtNum >= maxTgtNum\n");
				return false;
			}
		}

		pTgtPlayer = NULL;
		pTgtMonster = NULL;
		if ( i >= ARRAY_SIZE(pUseSkillInfo->useSkill.arrTarget) ) 
		{
			D_ERROR("不可能错误，经过前面判断不可能到此处\n");
			return false;
		}

		if ( MONSTER_GID == pUseSkillInfo->useSkill.arrTarget[i].wGID ) //对怪攻击
		{
			pTgtMonster = GetMonsterPtrInMap( pUseSkillInfo->useSkill.arrTarget[i].dwPID );
			if( NULL == pTgtMonster )
			{
				D_WARNING( "玩家%s使用技能%d，攻击目标怪物之一%d:%d找不到\n"
					, pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID(), pUseSkillInfo->useSkill.arrTarget[i].wGID, pUseSkillInfo->useSkill.arrTarget[i].dwPID );
				continue;
			}
			unsigned int ret = 0;
			if (!pScript->FnCheckPveBattleSecondStage(pUseSkillPlayer,pTgtMonster,ret))
			{
				D_WARNING("FnCheckPveBattleSecondStage 执行脚本失败\n");
				continue;
			}
			if (ret != 0)
			{
				continue;
			}
			if ( 0 == i ) //取aoe的范围中心；
			{
				if ( firstAoeTgt )
				{
					pTgtMonster->GetPos( aoeTgtX, aoeTgtY );
					if ( ( (abs((int)aoeTgtX-pUseSkillPlayer->GetPosX())) > 20 )
						|| ( (abs((int)aoeTgtY-pUseSkillPlayer->GetPosY())) > 20 )
						)
					{
						D_WARNING( "客户端%s使用技能%d，超距离, self(%d,%d),tgt%d(%d,%d)\n"
							, pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID()
							, pUseSkillPlayer->GetPosX(), pUseSkillPlayer->GetPosY(), pUseSkillInfo->useSkill.arrTarget[i].dwPID, aoeTgtX, aoeTgtY );
						return false;
					}
				}
			}

			if ( pTgtMonster->IsMonsterDie() )
			{
				D_DEBUG( "CMuxMap::OnPlayerUseSkill2, 怪物(%d:%d)已死，不再受%s攻击\n", pTgtMonster->GetCreatureID().wGID, pTgtMonster->GetCreatureID().dwPID, pUseSkillPlayer->GetAccount() );
				continue;
			}

			//以下目标检测
			if ( isTeamAoe )
			{
				D_ERROR("客户端错误的攻击消息，组队目标技能，实际目标为怪物%d\n", pUseSkillInfo->useSkill.arrTarget[i].dwPID );
				continue;
			}
			if ( (abs((int)aoeTgtX-pTgtMonster->GetPosX()) > aoeRange)
				|| (abs((int)aoeTgtY-pTgtMonster->GetPosY()) > aoeRange)
				)
			{
				D_WARNING( "客户端技能攻击怪物，攻击距离超范围, cen(%d,%d),tgt%d(%d,%d)\n"
					, aoeTgtX, aoeTgtY, pUseSkillInfo->useSkill.arrTarget[i].dwPID, pTgtMonster->GetPosX(), pTgtMonster->GetPosY() );
				continue;
			}

			if ( ( i >= ARRAY_SIZE(pUseSkillInfo->useSkill.arrNewPosX) )
				|| ( i >= ARRAY_SIZE(pUseSkillInfo->useSkill.arrNewPosY) )
				)
			{
				D_ERROR( "CMuxMap::OnPlayerUseSkill2, i(%d)越界2, pUseSkillInfo->useSkill.targetNum(%d)\n", i, pUseSkillInfo->useSkill.targetNum );
				break;
			}

			bool isKickBack = false;
			TYPE_POS kickNewPosX=pUseSkillInfo->useSkill.arrNewPosX[i];
			TYPE_POS kickNewPosY=pUseSkillInfo->useSkill.arrNewPosY[i];

			if ( pTgtMonster->IsBossMonster() )
			{
				//由于boss不能被击退，所以直接设置无击退新位置;
				kickNewPosX = 0;
				kickNewPosY = 0;
			}

			if ( ( 0 != kickNewPosX ) || ( 0 != kickNewPosY ) )
			{
				//击退处理；
				isKickBack = true;				
				//击退距离判断
				if ( (abs((int)kickNewPosX-pTgtMonster->GetPosX()*10) > (int)(pSkill->GetKickBackRange()) )
					|| (abs((int)kickNewPosY-pTgtMonster->GetPosY()*10) > (int)(pSkill->GetKickBackRange()) )
					)
				{
					D_WARNING( "客户端技能攻击怪物，击退距离超范围, orgpos(%d,%d),怪物id %d,newpos(%d,%d)"
						, pTgtMonster->GetPosX()*10, pTgtMonster->GetPosY()*10, pUseSkillInfo->useSkill.arrTarget[i].dwPID, kickNewPosX, kickNewPosY );
					isKickBack = false;//按老黄说法，即使击退判定失败不击退，攻击也还是要成功，by dzj, 10.06.02;
					kickNewPosX = 0;
					kickNewPosY = 0;
				}
			}

			SinglePvcWrapper( pUseSkillPlayer, pTgtMonster, pSkill, (0==i), tgtInfo[validTgtNum]
			    , isKickBack, kickNewPosX, kickNewPosY
				, attackAoe, m_subHpArr );

		} else { //对人攻击
			CGateSrv* pGateSrv = CManG_MProc::FindProc( pUseSkillInfo->useSkill.arrTarget[i].wGID );
			if( NULL == pGateSrv )
			{
				D_WARNING( "玩家%s使用技能%d，攻击目标玩家%d:%d所在gate找不到\n"
					, pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID(), pUseSkillInfo->useSkill.arrTarget[i].wGID, pUseSkillInfo->useSkill.arrTarget[i].dwPID );
				continue;
			}
			pTgtPlayer = pGateSrv->FindPlayer( pUseSkillInfo->useSkill.arrTarget[i].dwPID );
			if( NULL == pTgtPlayer )
			{
				D_WARNING( "玩家%s使用技能%d，找不到攻击目标玩家%d:%d\n"
					, pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID(), pUseSkillInfo->useSkill.arrTarget[i].wGID, pUseSkillInfo->useSkill.arrTarget[i].dwPID );
				continue;
			}
			unsigned int ret = 0;
			if (!pScript->FnCheckPvpBattleSecondStage(pUseSkillPlayer,pTgtPlayer,ret))
			{
				D_WARNING("FnCheckPvpBattleSecondStage 执行脚本失败\n");
				continue;
			}
			if (ret != 0)
			{
				continue;
			}
			if ( 0 == i ) //取aoe的范围中心；
			{
				if ( firstAoeTgt )
				{
					pTgtPlayer->GetPos( aoeTgtX, aoeTgtY );
					if ( ( (abs((int)aoeTgtX-pUseSkillPlayer->GetPosX())) > 20 )
						|| ( (abs((int)aoeTgtY-pUseSkillPlayer->GetPosY())) > 20 )
						)
					{
						D_WARNING( "客户端%s使用技能%d，超距离, self(%d,%d),tgt%d(%d,%d)"
							, pUseSkillPlayer->GetAccount(), pSkill->GetEffectiveSkillID()
							, pUseSkillPlayer->GetPosX(), pUseSkillPlayer->GetPosY(), pUseSkillInfo->useSkill.arrTarget[i].dwPID, aoeTgtX, aoeTgtY );
						return false;
					}
				}
			}
			if ( pTgtPlayer->IsDying() )
			{
				D_DEBUG( "CMuxMap::OnPlayerUseSkill2, 玩家%s已死，不再受%s攻击\n", pTgtPlayer->GetAccount(), pUseSkillPlayer->GetAccount() );
				continue;
			}
			//以下目标检测
			if ( isTeamAoe )
			{
				if ( pUseSkillPlayer->GetGroupTeam() != pTgtPlayer->GetGroupTeam() )
				{
					D_ERROR("客户端错误的攻击消息，组队目标技能，实际目标%d与其不在同一队", pUseSkillInfo->useSkill.arrTarget[i].dwPID );
					continue;
				}
			}
			if ( (abs((int)aoeTgtX-pTgtPlayer->GetPosX()) > aoeRange)
				|| (abs((int)aoeTgtY-pTgtPlayer->GetPosY()) > aoeRange)
				)
			{
				D_WARNING( "客户端技能攻击玩家，超范围, cen(%d,%d),tgt%d(%d,%d)"
					, aoeTgtX, aoeTgtY, pUseSkillInfo->useSkill.arrTarget[i].dwPID, pTgtPlayer->GetPosX(), pTgtPlayer->GetPosY() );
				continue;
			}

			if ( ( i >= ARRAY_SIZE(pUseSkillInfo->useSkill.arrNewPosX) )
				|| ( i >= ARRAY_SIZE(pUseSkillInfo->useSkill.arrNewPosY) )
				)
			{
				D_ERROR( "客户端技能攻击玩家，i(%d)越界, pUseSkillInfo->useSkill.targetNum(%d)\n", i, pUseSkillInfo->useSkill.targetNum );
				break;
			}

			if ( ( 0 != pUseSkillInfo->useSkill.arrNewPosX[i] ) || ( 0 != pUseSkillInfo->useSkill.arrNewPosY[i] ) )
			{
				//玩家不可被击退
				D_WARNING( "客户端%s技能试图击退x玩家%s，玩家不可被击退", pUseSkillPlayer->GetAccount(), pTgtPlayer->GetAccount() );
				continue;
			}

			SinglePvpWrapper( pUseSkillPlayer, pTgtPlayer, pSkill, (0==i), tgtInfo[validTgtNum], attackAoe, m_subHpArr );
		}

		++ validTgtNum;
	}// 逐个攻击各目标，for (int i=0; i<pUseSkillInfo->useSkill.targetNum; ++i)

	if ( isSplitSkill ) //填充消息中的目标人数;
	{
		splitAttack.gcSplitAttack.tgtNum = validTgtNum;
	} else {
		normalAttack.gcNormalAttack.tgtNum = validTgtNum;
	}
	//3、向最终通知块中的各玩家发送本次攻击消息
	NotifyResBlockAttInfo( isSplitSkill, pUseSkillInfo->useSkill.isSeqLast, attackAoe, splitAttack, normalAttack, validTgtNum);

	//4、对各有效攻击目标一一减血;
	m_subHpArr.SubArrTgtsHp( pUseSkillPlayer, pSkill );

	return true;
} //bool CMuxMap::OnPlayerUseSkill2( CPlayer* pUseSkillPlayer, const GMUseSkill2* pUseSkillInfo)

//通知相应块攻击结果
bool CMuxMap::NotifyResBlockAttInfo(bool isSplitSkill, bool isLastMsg, AreaOfInterest& attackAoe, MGSplitAttack& splitAttack, MGNormalAttack& normalAttack, BYTE& validTgtNum)
{
	vector<MuxMapBlock*>* vecBlocks = attackAoe.GetExtArea( this );

	if ( isSplitSkill ) 
	{
		splitAttack.gcSplitAttack.atkCommonInfo.isLastMsg = isLastMsg;
		NotifyBlocks( &splitAttack, vecBlocks );
	} else {
		if ( normalAttack.gcNormalAttack.tgtNum > ARRAY_SIZE(normalAttack.gcNormalAttack.tgtInfo) )
		{
			D_ERROR( "NotifyResBlockAttInfo, tgtNum(%d)越界\n", normalAttack.gcNormalAttack.tgtNum );
			return false;
		}
		normalAttack.gcNormalAttack.atkCommonInfo.isLastMsg = isLastMsg;
		for ( int i=0; i<normalAttack.gcNormalAttack.tgtNum; ++i )
		{
			D_DEBUG("广播攻击结果，攻击目标总数%d-%d，目标:%d, hpSub%d, hpLeft%d\n"
				, normalAttack.gcNormalAttack.tgtNum, i
				, normalAttack.gcNormalAttack.tgtInfo[i].targetID.dwPID, normalAttack.gcNormalAttack.tgtInfo[i].hpSubed, normalAttack.gcNormalAttack.tgtInfo[i].hpLeft );
		}
		NotifyBlocks( &normalAttack, vecBlocks );
	}

	attackAoe.ResetRegion();

	validTgtNum = 0;

	return true;
}

bool CMuxMap::OnPlayerUseSkill( CPlayer* pUseSkillPlayer, const GMUseSkill* pUseSkillInfo)
{
	if( NULL == pUseSkillPlayer || NULL == pUseSkillInfo )
	{
		D_ERROR( "OnPlayerUseSkill, NULL == pUseSkillPlayer || NULL == pUseSkillInfo\n" );
		return false;
	}

#ifdef _DEBUG
	D_INFO("玩家%s使用旧攻击消息，技能 ID=%d\n", pUseSkillPlayer->GetNickName(), pUseSkillInfo->useSkill.skillID );
#endif //_DEBUG
	return false;
//废弃旧的攻击消息，by dzj, 10.06.23;
//	if ( NULL != pUseSkillPlayer )
//	{
//		//即使NULL==pUseSkillInfo也要停下，因为客户端发此消息已经停下来了；
//		pUseSkillPlayer->ResetMoveStat();
//	}
//
//	CBattleScript* pScript = CBattleScriptManager::GetScript();
//	if( NULL == pScript )
//	{
//		D_ERROR("找不到战斗脚本,使用技能失败\n");
//		return false;
//	}
//
//	unsigned int ret = 0;
//	CMuxPlayerSkill* pSkill = NULL;
//	TYPE_POS targetPosX = 0, targetPosY = 0;
//	CMonster* pTargetMonster = NULL;
//	CPlayer* pTargetPlayer = NULL;
//	bool isPvc = false;
//	unsigned int logSkillID = 0;//用于记日至（在使用技能成功时才记日至，但是在判断的过程中技能编号有可能会变成新技能编号，因此保存旧的技能编号）
//	do
//	{
//#ifdef HAS_PATH_VERIFIER
//		if(pUseSkillInfo->useSkill.skillType == PARTION_SKILL)/*如果是动作分割技能，判断是否通过阻挡验证[zsw: 20091217] */
//		{
//			if(!BlockTestOK(pUseSkillPlayer->GetFloatPosX(), pUseSkillPlayer->GetFloatPosY(), (float)pUseSkillInfo->useSkill.arg1, (float)pUseSkillInfo->useSkill.arg2))
//			{
//				//错误返回
//				ret = PLAYER_POS_ERROR;
//				break;
//			}
//		}
//#endif/*HAS_PATH_VERIFIER*/
//
//		if( pUseSkillInfo->useSkill.skillType == UNION_SKILL )
//		{
//			pSkill = pUseSkillPlayer->FindMuxSkill( pUseSkillInfo->useSkill.skillID, false );
//		}else{
//			pSkill = pUseSkillPlayer->FindMuxSkill( pUseSkillInfo->useSkill.skillID );
//		}
//		if( NULL == pSkill )
//		{
//			//错误返回
//			D_WARNING( "玩家%s发来非战斗类技能%d\n", pUseSkillPlayer->GetAccount(), pUseSkillInfo->useSkill.skillID );
//			ret = INVALID_SKILL_ID_ERROR;
//			break;
//		}
//
//		if ( !pSkill->IsGoodSkill() )
//		{
//			if ( pUseSkillPlayer->IsRideState() )
//			{
//				ret = PLAYER_STATE_ERROR;
//				break;
//			}
//		}
//
//		logSkillID = pSkill->GetSkillID();//保存技能编号
//		if( !pScript->FnCheckBattleFirstStage( pUseSkillPlayer, pUseSkillInfo->useSkill.skillID, ret ) )
//		{
//			//脚本调用错误
//			ret = OTHER_ERROR;
//			break;
//		}
//		if( pUseSkillPlayer->IsInRest() )
//		{
//			ret = PLAYER_STATE_ERROR;
//			break;
//		}
//		if( ret != 0 )
//		{
//			break;
//		}
//
//		//是AOE技能,无对象的就没必要检查对象了
//		if( pSkill->IsTeamSkill() )
//		{
//			break;
//		}
//
//		if( pUseSkillInfo->useSkill.targetID.wGID == MONSTER_GID )
//		{			
//			pTargetMonster = GetMonsterPtrInMap( pUseSkillInfo->useSkill.targetID.dwPID );
//			if( NULL == pTargetMonster )
//			{
//				//错误返回
//				ret = TARGET_INVALID_ERROR;
//				break;
//			}
//			if( !pScript->FnCheckPveBattleSecondStage( pUseSkillPlayer, pTargetMonster, ret ) )
//			{
//				////脚本调用错误
//				ret = OTHER_ERROR;
//				break;
//			}
//			if( ret != 0 )
//			{
//				break;
//			}
//
//			ACE_Time_Value currentTime = ACE_OS::gettimeofday();
//			if( pUseSkillPlayer->GetNextActionTime() > currentTime )
//			{
//				ret = SKILL_IN_CT_TIME_ERROR;
//				break;
//			}
//			pUseSkillPlayer->SetNextActionTime( currentTime+ ACE_Time_Value( 0, pSkill->GetCurseTime() * 1000) );
//		
//
//			if( !pScript->FnConsumeStagePvc( pUseSkillPlayer, ret ) )		//减去消耗
//			{
//				//脚本调用错误
//				return false;
//			}
//			isPvc = true;
//			targetPosX = pTargetMonster->GetPosX();
//			targetPosY = pTargetMonster->GetPosY();
//		}else{
//			CGateSrv* pGateSrv = CManG_MProc::FindProc( pUseSkillInfo->useSkill.targetID.wGID );
//			if( NULL == pGateSrv )
//			{
//				//错误返回
//				ret = OTHER_ERROR;
//				break;
//			}
//			pTargetPlayer = pGateSrv->FindPlayer( pUseSkillInfo->useSkill.targetID.dwPID );
//			if( NULL == pTargetPlayer )
//			{
//				//错误返回
//				ret = TARGET_INVALID_ERROR;
//				break;
//			}
//
//			if( !pScript->FnCheckPvpBattleSecondStage( pUseSkillPlayer, pTargetPlayer, ret ) )
//			{
//				ret = OTHER_ERROR;
//				break;
//			}
//			if( ret != 0 )
//			{
//				break;
//			}
//
//			ACE_Time_Value currentTime = ACE_OS::gettimeofday();
//			if( pUseSkillPlayer->GetNextActionTime() > currentTime )
//			{
//				ret = SKILL_IN_CT_TIME_ERROR;
//				break;
//			}
//			pUseSkillPlayer->SetNextActionTime( currentTime+ ACE_Time_Value( 0, pSkill->GetCurseTime() * 1000) );
//
//			if( !pScript->FnConsumeStagePvp( pUseSkillPlayer, ret ) )		//减去消耗
//			{
//				//脚本调用错误
//				return false;
//			}
//			isPvc = false;
//			targetPosX = pTargetPlayer->GetPosX();
//			targetPosY = pTargetPlayer->GetPosY();
//		}
//
//	
//	}while( false );
//
//	if( ret != 0 )
//	{
//		//错误返回
//		MGUseSkillRst err;
//		StructMemSet( err, 0, sizeof( err ) );
//		err.skillRst.skillID = pUseSkillInfo->useSkill.skillID;
//		err.skillRst.sequenceID = pUseSkillInfo->useSkill.sequenceID;
//		err.skillRst.targetID = pUseSkillInfo->useSkill.targetID;
//		err.skillRst.attackID = pUseSkillPlayer->GetFullID();
//		err.skillRst.orgTargetID = pUseSkillInfo->useSkill.targetID;
//		err.skillRst.skillType = pUseSkillInfo->useSkill.skillType;
//		err.skillRst.nErrCode = (EBattleResult)ret;
//		err.skillRst.attackHP = pUseSkillPlayer->GetCurrentHP();
//		err.skillRst.attackMP = pUseSkillPlayer->GetCurrentMP();
//		err.skillRst.causeBuffID = 0;
//	    pUseSkillPlayer->SendPkg( &err );
//		return false;
//	}
//
//	if ( NULL == pSkill || NULL == pUseSkillInfo ) //前面的判空离的太远看不清，因此这里再做下判断
//	{
//		D_ERROR( "CMuxMap::OnPlayerUseSkill, NULL == pSkill || NULL == pUseSkillInfo\n" );
//		return false;
//	}
//
//	//如果是动作分割技能,设置玩家位置
//	if( pSkill->GetSpiltProp() && pUseSkillInfo->useSkill.skillType == PARTION_SKILL )
//	{
//		//验证设置的点和target之间的关系,应该不能相差太大
//		int dis = abs( int(pUseSkillInfo->useSkill.arg1 - targetPosX) );
//		dis += abs( int(pUseSkillInfo->useSkill.arg2 - targetPosY) );
//		if( dis >= 6 )
//		{
//			D_ERROR("动作分割技能的目标和target之间的距离相差太大\n");
//			return false;
//		}
//		//设置动作分割技能的位置
//		pUseSkillPlayer->ForceSetFloatPos( false/*作移动处理，视野之内不再通知，留给客户端播动作时间*/, (float)(pUseSkillInfo->useSkill.arg1), (float)(pUseSkillInfo->useSkill.arg2) );
//
//		//10.03.09，移动bug查找修改，此处：只设标记，不直接从list中删，统一由timer去删//从移动列表中删去；
//		//if ( pUseSkillPlayer->IsInMovingList() )
//		//{
//		//	DelPlayerFromMovingList( pUseSkillPlayer );
//		//}
//		//10.03.09，移动bug查找修改,此处：只要发攻击消息就停下，而不只是攻击技能，pUseSkillPlayer->ResetMoveStat();
//	}	
//
//	CLogManager::DoUseSkill(pUseSkillPlayer, pSkill->GetSkillType(), logSkillID, pSkill->IsSpSkill());//记日志
//	pUseSkillPlayer->UseSkillTimeAffectWear();	//使用技能影响的耐久
//
//	TYPE_POS aoePosX = 0, aoePosY = 0;
//	vector<CPlayer*> vecTargetPlayer;
//	vector<CMonster*> vecTargetMonster;
//	bool isAoe = pSkill->IsAoeSkill();
//	if( isAoe )
//	{
//		//如果不是队伍技能
//		if( pSkill->GetAoeShape() == 1 )
//		{
//			pUseSkillPlayer->GetPos( aoePosX, aoePosY );
//		}else
//		{
//			aoePosX = targetPosX; aoePosY = targetPosY; 
//		}
//	
//		if( !pSkill->IsTeamSkill() )
//		{
//			OnPlayerSkillGetAoeTarget( pUseSkillPlayer, pSkill->GetAoeRange(), aoePosX, aoePosY, vecTargetPlayer, vecTargetMonster );
//		}else
//		{
//			OnPlayerSkillGetTeamTarget( pUseSkillPlayer, pSkill->GetAoeRange(), aoePosX, aoePosY, vecTargetPlayer );
//			//小队技能只会对人使用，故用pvp
//			if( !pScript->FnConsumeStagePvp( pUseSkillPlayer, ret ) )		//减去消耗
//			{
//				//脚本调用错误
//				return false;
//			}
//		}
//	}else{
//		if( isPvc )
//		{
//			D_ERROR("使用旧消息攻击怪物，忽略\n");
//			//SinglePvcWrapper( pUseSkillPlayer, pTargetMonster, pSkill, pUseSkillInfo->useSkill.targetID, SINGLE_ATTACK, pUseSkillInfo );
//		}else{
//			D_ERROR("使用旧消息攻击玩家，忽略\n");
//			//SinglePvpWrapper( pUseSkillPlayer, pTargetPlayer, pSkill, pUseSkillInfo->useSkill.targetID, SINGLE_ATTACK, pUseSkillInfo );
//		}
//		MGInfoUpdate infoUp;
//		infoUp.nAtkPlayerExp = pUseSkillPlayer->GetExp();
//		infoUp.nAtkPlayerHP = pUseSkillPlayer->GetCurrentHP();
//		infoUp.nAtkPlayerMagExp = pUseSkillPlayer->GetMagExp();
//		infoUp.nAtkPlayerMoney = pUseSkillPlayer->GetMoney();
//		infoUp.nAtkPlayerMP = pUseSkillPlayer->GetCurrentMP();
//		infoUp.nAtkPlayerPhyExp = pUseSkillPlayer->GetPhyExp();
//		infoUp.nAtkPlayerSpPower = pUseSkillPlayer->GetSpPower();
//		infoUp.nAtkSkillID = pSkill->GetSkillID();
//		infoUp.nAtkSkillSpExp = pSkill->GetSpExp();
//		infoUp.nAtkSpExp = pUseSkillPlayer->GetSpExp();
//		pUseSkillPlayer->SendPkg<MGInfoUpdate>( &infoUp );
//		return true;
//	}
//
//	CPlayer* pTmpPlayer = NULL;
//	unsigned int vecSize = (unsigned int)vecTargetPlayer.size();
//	for( unsigned int i =0; i< vecSize; ++i )
//	{
//		pTmpPlayer = vecTargetPlayer[i];
//		if( !pTmpPlayer )
//			continue;
//		D_ERROR("使用旧消息攻击玩家，忽略\n");		
//		//SinglePvpWrapper( pUseSkillPlayer, pTmpPlayer, pSkill, pUseSkillInfo->useSkill.targetID, SCOPE_ATTACK, pUseSkillInfo );
//	}
//
//	CMonster* pTmpMonster = NULL;
//	vecSize = (unsigned int)vecTargetMonster.size();
//	for( unsigned int i =0; i< vecTargetMonster.size(); ++i )
//	{
//		pTmpMonster = vecTargetMonster[i];
//		if( !pTmpMonster )
//			continue;
//		D_ERROR("使用旧消息攻击怪物，忽略\n");	
//		//SinglePvcWrapper( pUseSkillPlayer, pTmpMonster, pSkill, pUseSkillInfo->useSkill.targetID, SCOPE_ATTACK, pUseSkillInfo );
//	}
//	
//	//scope end msg
//	MGUseSkillRst rst;
//	rst.skillRst.attackID = pUseSkillPlayer->GetFullID();
//	rst.skillRst.attackHP = pUseSkillPlayer->GetCurrentHP();
//	rst.skillRst.attackMP = pUseSkillPlayer->GetCurrentMP();
//	rst.skillRst.battleFlag = HIT;
//	rst.skillRst.causeBuffID = 0;
//	rst.skillRst.lCooldownTime = pSkill->GetCoolDown();
//	rst.skillRst.lCurseTime = pSkill->GetCurseTime();
//	rst.skillRst.lHpLeft = 0;
//	rst.skillRst.lHpSubed = 0;
//	rst.skillRst.nErrCode = BATTLE_SUCCESS;
//	rst.skillRst.orgTargetID = pUseSkillInfo->useSkill.targetID;
//	rst.skillRst.scopeFlag = SCOPE_ATTACK_END;
//	rst.skillRst.skillType = pUseSkillInfo->useSkill.skillType;
//	rst.skillRst.skillID = pSkill->GetSkillID();
//	rst.skillRst.sequenceID = pUseSkillInfo->useSkill.sequenceID;
//
//	//检查技能类型和其他参数类型,动作分割最后个消息也要赋
//	unsigned short arrRst[5];
//	StructMemSet(arrRst, 0, sizeof(arrRst));
//	if( pUseSkillInfo->useSkill.skillType == PARTION_SKILL )
//	{
//		if( !pScript->FnCheckSkillSpilt( arrRst ) )
//		{
//			//调用错误
//			return false;
//		}
//
//		if ( 0 == arrRst[0] )
//		{
//			D_ERROR("1:玩家%s使用分割技能%d成功，但FnCheckSkillSplit返回结果错误\n", pUseSkillPlayer->GetAccount(), pUseSkillInfo->useSkill.skillID);
//			return false;
//		}
//
//		rst.skillRst.arg1 = pUseSkillInfo->useSkill.arg1;
//		rst.skillRst.arg2 = pUseSkillInfo->useSkill.arg2;
//		rst.skillRst.arg3 = arrRst[1];
//		rst.skillRst.arg4 = arrRst[2];
//		rst.skillRst.arg5 = arrRst[3];
//		rst.skillRst.arg6 = arrRst[4];
//	}
//
//	CMuxMap* pMap = pUseSkillPlayer->GetCurMap();
//	if( pMap )
//	{
//		pMap->NotifySurrounding<MGUseSkillRst>( &rst, pUseSkillPlayer->GetPosX(), pUseSkillPlayer->GetPosY() );
//	}
//
//	//self info update
//	MGInfoUpdate infoUp;
//	infoUp.nAtkPlayerExp = pUseSkillPlayer->GetExp();
//	infoUp.nAtkPlayerHP = pUseSkillPlayer->GetCurrentHP();
//	infoUp.nAtkPlayerMagExp = pUseSkillPlayer->GetMagExp();
//	infoUp.nAtkPlayerMoney = pUseSkillPlayer->GetMoney();
//	infoUp.nAtkPlayerMP = pUseSkillPlayer->GetCurrentMP();
//	infoUp.nAtkPlayerPhyExp = pUseSkillPlayer->GetPhyExp();
//	infoUp.nAtkPlayerSpPower = pUseSkillPlayer->GetSpPower();
//	infoUp.nAtkSkillID = pSkill->GetSkillID();
//	infoUp.nAtkSkillSpExp = pSkill->GetSpExp();
//	infoUp.nAtkSpExp = pUseSkillPlayer->GetSpExp();
//	pUseSkillPlayer->SendPkg<MGInfoUpdate>( &infoUp );
//
//	return true;
}//bool CMuxMap::OnPlayerUseSkill( CPlayer* pUseSkillPlayer, const GMUseSkill* pUseSkillInfo)


EBattleResult CMuxMap::CheckNewBattlePvc( CPlayer* pUseSkillPlayer, const PlayerID& targetID, TYPE_ID skillID, CMuxPlayerSkill*& pSkill,CMonster*& pTargetMonster )
{
	if(NULL == pUseSkillPlayer )
		return OTHER_ERROR;

	if( !CBattleRule::PlayerStateCheck( pUseSkillPlayer ) )
	{
		return CUSTOMIZE_ERROR;
	}

	//step 3 判断玩家是否拥有该技能
	pSkill = pUseSkillPlayer->FindMuxSkill( skillID );
	if( NULL == pSkill )
	{
		D_ERROR("CMuxMap::CheckPvc,玩家%s技能列表里并不拥有此技能%d\n", pUseSkillPlayer->GetAccount(), skillID );
		return INVALID_SKILL_ID_ERROR;
	}
	
	//step 2 查看怪物是否在本地图上
	pTargetMonster = GetMonsterPtrInMap( targetID.dwPID );
	if ( NULL == pTargetMonster )
	{
		D_WARNING( "CMuxMap::CheckPvc,找不到被攻击的怪物%x\n", targetID.dwPID );
		return TARGET_INVALID_ERROR;
	}
	if( !CBattleRule::PvcTargetCheck( pUseSkillPlayer, pTargetMonster, pSkill->IsGoodSkill(), false ) )
	{
		return TARGET_INVALID_ERROR;
	}

	unsigned int targetSize = pTargetMonster->GetMonsterSize();
	TYPE_POS targetPosX =0, targetPosY = 0;
	pTargetMonster->GetPos( targetPosX, targetPosY );
	unsigned int minRange = pSkill->GetAttDistMin();
	unsigned int maxRange = pSkill->GetAttDistMax();
	if( !CBattleRule::DistanceCheck( pUseSkillPlayer, targetPosX, targetPosY, minRange, maxRange, targetSize ))
	{
		return NOT_IN_ATK_RANGE_ERROR;	
	}
	
	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	ACE_Time_Value nextActionTime(curTime + ACE_Time_Value( 0, pSkill->GetCurseTime() * 1000) );
	pUseSkillPlayer->SetNextActionTime( nextActionTime );
	return BATTLE_SUCCESS;
}


EBattleResult CMuxMap::CheckNewBattlePvp( CPlayer* pUseSkillPlayer, const PlayerID& targetID, TYPE_ID skillID, CMuxPlayerSkill*& pSkill,CPlayer*& pTargetPlayer )
{
	if ( NULL == pUseSkillPlayer )
	{
		return PLAYER_STATE_ERROR;
	}
	
	if( !CBattleRule::PlayerStateCheck( pUseSkillPlayer ) )
	{
		return PLAYER_STATE_ERROR;
	}

	//step4 判断玩家是否拥有该技能
	pSkill = pUseSkillPlayer->FindMuxSkill( skillID );
	if( NULL == pSkill )
	{
		D_ERROR( "CMuxMap::CheckNewBattlePvp,玩家%s技能列表里并不拥有此技能%d\n", pUseSkillPlayer->GetAccount(), skillID );
		return INVALID_SKILL_ID_ERROR;
	}
	if( !CBattleRule::NewSkillCheck( pUseSkillPlayer, pSkill ) )
	{
		return CUSTOMIZE_ERROR;
	}

	//被攻击者可能不是此pOwner上的成员，所以必须在它本身的owener上寻找  05.04 hyn
	CGateSrv* pProc = CManG_MProc::FindProc( targetID.wGID );
	if( NULL == pProc )
	{
		D_ERROR( "CMuxMap::CheckNewBattlePvp 被攻击玩家的gatesrv找不到\n" );
		return TARGET_INVALID_ERROR;
	}
	pTargetPlayer = pProc->FindPlayer( targetID.dwPID );
	if ( NULL == pTargetPlayer )
	{
		D_WARNING("CMuxMap::CheckNewBattlePvp 收到玩家攻击玩家消息,但在本mapsrv上找不到被攻击者:%x\n", targetID.dwPID );
		return TARGET_INVALID_ERROR;
	}
	
	pUseSkillPlayer->CheckInvincible();
	
	if(pTargetPlayer->CheckInvincible(false))
	{
		return PLAYER_STATE_ERROR;
	}

	if( !CBattleRule::PvpTargetCheck( pUseSkillPlayer, pTargetPlayer, pSkill->IsGoodSkill(), false ) )
	{
		return CUSTOMIZE_ERROR;
	}
	
	CMonster* pFollowingMonster = pTargetPlayer->GetFollowingMonster();
	if( NULL != pFollowingMonster && !pSkill->IsGoodSkill() )
	{
		pFollowingMonster->OnFollowPlayerBeAttack( pUseSkillPlayer->GetFullID(), 0 );
	}

	unsigned int minRange = pSkill->GetAttDistMin();
	unsigned int maxRange = pSkill->GetAttDistMax();
	unsigned int targetSize = 0;

	if( !CBattleRule::DistanceCheck( pUseSkillPlayer, pTargetPlayer->GetPosX(), pTargetPlayer->GetPosY(), minRange, maxRange, targetSize) )
	{
		return NOT_IN_ATK_RANGE_ERROR;
	}

	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	ACE_Time_Value nextActionTime(curTime + ACE_Time_Value( 0, pSkill->GetCurseTime() * 1000) );
	pUseSkillPlayer->SetNextActionTime( nextActionTime );
	return BATTLE_SUCCESS;
}


//改用脚本后已废弃?void CMuxMap::AfterCheckProcess( CPlayer* pUseSkillPlayer, CMuxPlayerSkill* pSkill, bool isPvc, unsigned int targetLevel, GMUseSkill* pUseSkillInfo )
//{
//	if( NULL == pUseSkillPlayer || NULL == pSkill || NULL == pUseSkillInfo )
//		return;
//
//	//记录技能的使用时间
//	unsigned short attackLevel = pUseSkillPlayer->GetLevel();
//	bool bAddExp = ( attackLevel - targetLevel) > 5 ? false : true;
//	unsigned int skillAddExp = pSkill->GetAddExp();
//
//	//关于增加经验的，对人是不长的
//	if( isPvc && bAddExp )
//	{
//		if( pSkill->GetType() == TYPE_PHYSICS )
//		{
//			//D_DEBUG("增加物理经验 %d\n", pSkill->GetAddExp());
//			pUseSkillPlayer->AddPhyExp( skillAddExp );
//			pUseSkillPlayer->AddSpPower( SP_POWER_PER_ATTACK );	//使用其他技能增加SP POWER
//		}
//		else if( pSkill->GetType() == TYPE_MAGIC  )
//		{
//			//D_DEBUG("增加魔法经验 %d\n", pSkill->GetAddExp());
//			pUseSkillPlayer->AddMagExp( skillAddExp );
//			pUseSkillPlayer->AddSpPower( SP_POWER_PER_ATTACK );	//使用其他技能增加SP POWER
//		}
//
//		if( pSkill->IsSpSkill() )
//		{
//			pUseSkillPlayer->AddSpExp( 10 );	//使用SP技能增加SP经验
//			pSkill->AddSpExp( 1 );
//		}
//	}
//
//	//扣除技能的消耗 暂时只做MP 3.25
//	pUseSkillPlayer->SubMP( pSkill->GetUllageMp() );
//	if( pSkill->IsSpSkill() )
//	{
//		pUseSkillPlayer->SubSpPower( pSkill->GetUllageSp() * CSpSkillManager::GetPowerPerBean() );  
//	}
//
//	//动作分割技能设置位置
//	if( pSkill->IsSpiltSkill() && pUseSkillInfo->useSkill.skillType == PARTION_SKILL )
//	{
//		//具体可以再加判定
//		pUseSkillPlayer->ForceSetFloatPos( false/*强制重拉玩家*/, pUseSkillInfo->useSkill.arg1+0.5f, pUseSkillInfo->useSkill.arg2+0.5f );
//	}
//}


//bool CMuxMap::SinglePvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer, CMuxPlayerSkill* pSkill, const PlayerID& orgTargetID,  EScopeFlag scopeFlag, const GMUseSkill* pUseSkill )
//{
//	if( NULL == pUseSkillPlayer || NULL == pTargetPlayer || NULL == pSkill || NULL == pUseSkill )
//		return false;
//
//	vector<int> vecRst;
//	CBattleScript* pScript = CBattleScriptManager::GetScript();
//	if( NULL == pScript )
//	{
//		return false;
//	}
//
//	//step1 先判定buff
//
//	bool isMiss = false;
//	bool isTargetRogue = pTargetPlayer->IsRogueState();
//
//	unsigned int buffID = 0;
//	MuxBuffProp* pBuffProp = pSkill->GetBuffProp();
//	unsigned int buffTime = 0;
//	unsigned int assistObject = 0;
//	if( NULL != pBuffProp )
//	{
//		if( !pScript->FnAddBuffPvp( pUseSkillPlayer, pTargetPlayer, pBuffProp->mID, &vecRst ) )
//		{
//			return false;
//		}
//
//		if( vecRst.size() != 3 )
//		{
//			D_ERROR("FnAddBuffPvp 返回值数量错误\n");
//			return false;
//		}
//
//		buffID = vecRst[0];
//		buffTime = vecRst[1];
//		assistObject = vecRst[2];
//	}
//	//未命中不加buff，故这段代码搬到下面来
//	//	if( buffID != 0 )
//	//	{
//	//		if( assistObject == 0 )
//	//		{
//	//			pUseSkillPlayer->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() ); 
//	//		}else{
//	//			pTargetPlayer->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() );
//	//		}
//	//	}
//	//}	
//
//	//解除类型技能
//	if( pSkill->GetRelieveID() != 0 )
//	{
//		if( !pScript->FnRelievePvp( pUseSkillPlayer, pTargetPlayer, pSkill->GetRelieveID(), &vecRst ) )
//		{
//			return false;
//		}
//	}
//	
//	MGUseSkillRst rst;		//先声明发送结构,先填充部分信息
//	
//	//关于伤害和加血//
//	if( NULL != pSkill->GetDamageProp() )
//	{
//		if( !pScript->FnPvpDamage( pUseSkillPlayer, pTargetPlayer, pSkill->GetSkillID(), &vecRst ) )
//		{
//			//调用脚本错误
//			return false;
//		}
//
//		if( vecRst.size() != 2 )
//		{
//			D_ERROR("FnPvpDamage 返回值错误\n");
//			return false;
//		}
//
//		unsigned int pvpDamage = vecRst[0];
//		EBattleFlag pvpFlag = (EBattleFlag)vecRst[1];
//		rst.skillRst.battleFlag = pvpFlag;
//		rst.skillRst.lHpSubed = pvpDamage;
//			
//		EBattleFlag battleFlag = (EBattleFlag)vecRst[1];
//		unsigned int nDamage = vecRst[0];
//
//		if (battleFlag == MISS)
//		{
//			isMiss = true;
//		}
//
//		if( nDamage != 0 )
//		{
//			//减血
//			pTargetPlayer->SubHP( nDamage, pUseSkillPlayer, pUseSkillPlayer->GetFullID() );
//			pTargetPlayer->GetSelfStateManager().OnPlayerBeAttack();
//			pTargetPlayer->BeInjuredAffectWear();
//			RankEventManagerSingle::instance()->MontiorPlayerBeDemage( pTargetPlayer, nDamage  );		//排行榜增加
//			RankEventManagerSingle::instance()->MonitorPlayerDemage( pUseSkillPlayer, nDamage  );
//
//			//AI通知
//			CMonster* pFollowingMonster = pTargetPlayer->GetFollowingMonster();
//			if( NULL != pFollowingMonster )
//			{
//				pFollowingMonster->OnFollowPlayerBeAttack( pUseSkillPlayer->GetCreatureID(), nDamage );
//			}
//		
//	    
//			//关于伤害反弹
//			if( !pScript->FnPvpDamageRebound( pUseSkillPlayer, pTargetPlayer, nDamage, battleFlag, &vecRst ) )
//			{
//				return false;
//			}
//			if( vecRst.size() != 2 )
//			{
//				D_ERROR("FnPvpDamageRebound 返回值错误\n");
//				return false;
//			}
//			
//			unsigned int damReb = vecRst[0];
//			EBattleFlag damRebFlag = (EBattleFlag)vecRst[1];
//			if( damReb != 0 )
//			{
//				pUseSkillPlayer->NotifyDamageRebound( pSkill->GetSkillID(), damReb, damRebFlag );
//				pUseSkillPlayer->SubHP(damReb,pTargetPlayer,pTargetPlayer->GetFullID());
//				pUseSkillPlayer->GetSelfStateManager().OnPlayerBeAttack();
//				pUseSkillPlayer->BeInjuredAffectWear();
//				RankEventManagerSingle::instance()->MontiorPlayerBeDemage( pUseSkillPlayer, damReb  );		//排行榜增加
//				RankEventManagerSingle::instance()->MonitorPlayerDemage( pTargetPlayer, damReb  );
//			}
//
//			if( pTargetPlayer->GetFullID() == orgTargetID )
//			{
//				WeaponSkillPvpWrapper( pUseSkillPlayer, pTargetPlayer );
//			}
//		}
//	}
//
//	//关于加血
//	if( NULL != pSkill->GetItemSkillProp() )
//	{
//		if( !pScript->FnPvpHealNum( pUseSkillPlayer, pTargetPlayer, pSkill->GetSkillID(), &vecRst ) )
//		{
//			return false;
//		}
//
//		if( vecRst.size() != 4 )
//		{
//			D_ERROR("FnPvpHealNum 返回值错误\n");
//			return false;
//		}
//		
//		unsigned int healDamage = vecRst[0];
//		unsigned int mp = vecRst[1];
//		unsigned int spBean = vecRst[2];
//    	EBattleFlag healFlag = (EBattleFlag)vecRst[3];
//
//		if (healFlag == MISS)
//		{
//			isMiss = true;
//		}
//
//	    if( healDamage != 0 )
//		{
//			pTargetPlayer->AddPlayerHp( healDamage );
//			pTargetPlayer->NotifySurroundingMonsterHpAdd( pUseSkillPlayer->GetFullID(), healDamage );
//		}
//
//		if( mp )
//		{
//			pTargetPlayer->AddPlayerMp( mp );
//		}
//
//		if( spBean )
//		{
//			pTargetPlayer->AddSpBean( spBean );
//		}
//
//		rst.skillRst.skillType = ITEM_SKILL;
//		rst.skillRst.battleFlag = healFlag;
//		rst.skillRst.lHpSubed = healDamage;
//	}
//
//	unsigned short arrRst[5];
//	memset(arrRst, 0, sizeof(arrRst));		
//	//检查技能类型和其他参数类型
//	if( pUseSkill->useSkill.skillType == PARTION_SKILL )
//	{
//		if( !pScript->FnCheckSkillSpilt( arrRst ) )
//		{
//			//调用错误
//			return false;
//		}
//
//		if ( 0 == arrRst[0] )
//		{
//			D_ERROR("SinglePvpWrapper:玩家%s使用分割技能%d成功，但FnCheckSkillSplit返回结果错误\n", pUseSkillPlayer->GetAccount());
//			return false;
//		}
//
//		rst.skillRst.arg1 = pUseSkill->useSkill.arg1;
//		rst.skillRst.arg2 = pUseSkill->useSkill.arg2;
//		rst.skillRst.arg3 = arrRst[1];
//		rst.skillRst.arg4 = arrRst[2];
//		rst.skillRst.arg5 = arrRst[3];
//		rst.skillRst.arg6 = arrRst[4];
//	}
//
//	if ( !isMiss )
//	{
//		if( buffID != 0 )
//		{
//			if( assistObject == 0 )
//			{
//				pUseSkillPlayer->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() ); 
//			}else{
//				pTargetPlayer->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() );
//			}
//		}
//	}
//
//	//rst.skillRst.causeBuffID = buffID;	//从上面的buff流程来
//	isMiss ? rst.skillRst.causeBuffID = 0 : rst.skillRst.causeBuffID = buffID;
//	rst.skillRst.attackID = pUseSkillPlayer->GetFullID();
//	rst.skillRst.attackHP = pUseSkillPlayer->GetCurrentHP();
//	rst.skillRst.attackMP = pUseSkillPlayer->GetCurrentMP();
//	rst.skillRst.nErrCode = BATTLE_SUCCESS;
//	rst.skillRst.lCooldownTime = pSkill->GetCoolDown();
//	rst.skillRst.lCurseTime = pSkill->GetCurseTime();
//	rst.skillRst.lHpLeft = pTargetPlayer->GetCurrentHP();
//	rst.skillRst.orgTargetID = orgTargetID;
//	rst.skillRst.scopeFlag = scopeFlag;
//	rst.skillRst.skillID = pSkill->GetSkillID();
//	rst.skillRst.skillType = pUseSkill->useSkill.skillType;
//	rst.skillRst.nErrCode = BATTLE_SUCCESS;
//	rst.skillRst.targetID = pTargetPlayer->GetFullID();
//	rst.skillRst.targetRewardID.dwPID = 0;
//	rst.skillRst.targetRewardID.wGID = 0;
//	rst.skillRst.skillType = pUseSkill->useSkill.skillType;
//	rst.skillRst.sequenceID = pUseSkill->useSkill.sequenceID;
//
//	//零散,流氓状态,下马
//	if( !pSkill->IsGoodSkill() ) 
//	{
//		pUseSkillPlayer->FightStartCheckRogueState( pTargetPlayer, isTargetRogue );
//	}
//
//	CMuxMap* pMap = pTargetPlayer->GetCurMap();
//	if( pMap )
//	{
//		pMap->NotifySurrounding<MGUseSkillRst>( &rst, pTargetPlayer->GetPosX(), pTargetPlayer->GetPosY() );
//	}	
// 
//	return true;
//}

//bool CMuxMap::SinglePvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster, CMuxPlayerSkill* pSkill, const PlayerID& orgTargetID,  EScopeFlag scopeFlag, const GMUseSkill* pUseSkill )
//{
//	if ( NULL == pUseSkillPlayer || NULL == pTargetMonster || NULL == pSkill || NULL == pUseSkill )
//	{
//		return false;
//	}
//
//	vector<int> vecRst;
//	CBattleScript* pScript = CBattleScriptManager::GetScript();
//	if( NULL == pScript )
//	{
//		return false;
//	}
//
//	bool isMiss = false;
//
//	unsigned int buffID = 0;
//	MuxBuffProp* pBuffProp = pSkill->GetBuffProp();
//	unsigned int buffTime = 0;
//	unsigned int assistObj = 0;
//	if( NULL != pBuffProp )
//	{
//		if( !pScript->FnAddBuffPvc( pUseSkillPlayer, pTargetMonster, pBuffProp->mID, &vecRst ) )
//		{
//			return false;
//		}
//
//		if( vecRst.size() != 3 )
//		{
//			D_ERROR("FnAddBuffPvc 返回值数量错误\n");
//			return false;
//		}
//
//		buffID = vecRst[0];
//		buffTime = vecRst[1];
//		assistObj = vecRst[2];
//	}
//
//	
//	//事先填充
//	MGUseSkillRst rst;
//
//	//伤害
//	if( NULL != pSkill->GetDamageProp() )
//	{
//		if( !pScript->FnPvcDamage( pUseSkillPlayer, pTargetMonster, pSkill->GetSkillID(), &vecRst ) )
//		{
//			//调用脚本错误
//			return false;
//		}
//
//		if( vecRst.size() != 2 )
//		{
//			D_ERROR("FnPvcDamage返回值数量错误\n");
//			return false;
//		}
//		
//		EBattleFlag battleFlag = (EBattleFlag)vecRst[1];
//		unsigned int nDamage = vecRst[0];
//
//		//怪物减血
//		pTargetMonster->SubMonsterHp( nDamage, pUseSkillPlayer, false, pSkill->GetCurseTime() );
//		RankEventManagerSingle::instance()->MonitorPlayerDemage( pUseSkillPlayer, nDamage );
//	
//		if (battleFlag == MISS)
//		{
//			isMiss = true;
//		}
//
//		rst.skillRst.battleFlag = battleFlag;
//		//rst.skillRst.causeBuffID = buffID;
//		isMiss ? rst.skillRst.causeBuffID = 0 : rst.skillRst.causeBuffID = buffID;
//		rst.skillRst.lHpSubed = nDamage;
//
//		//关于伤害反弹
//		if( nDamage != 0 )
//		{
//			//伤害反弹
//			if( !pScript->FnPvcDamageRebound( pUseSkillPlayer, pTargetMonster, nDamage, battleFlag, &vecRst ) )
//			{
//				return false;
//			}
//
//			if( vecRst.size() != 2 )
//			{
//				D_ERROR("FnPvcDamageRebound 返回值数量错误\n");
//				return false;
//			}
//
//			unsigned int damReb = (unsigned int)vecRst[0];
//			EBattleFlag drFlag = (EBattleFlag)vecRst[1];
//			
//			if( damReb != 0 )
//			{
//				pUseSkillPlayer->NotifyDamageRebound( pSkill->GetSkillID(), damReb, drFlag );
//				pUseSkillPlayer->SubHP(damReb,NULL,pTargetMonster->GetCreatureID());
//				pUseSkillPlayer->GetSelfStateManager().OnPlayerBeAttack();
//				pUseSkillPlayer->BeInjuredAffectWear();
//				RankEventManagerSingle::instance()->MontiorPlayerBeDemage( pUseSkillPlayer, damReb  );		//排行榜增加
//			}
//
//			if( pTargetMonster->GetCreatureID() == orgTargetID )
//			{
//				WeaponSkillPvcWrapper( pUseSkillPlayer, pTargetMonster );
//			}
//		}
//	}
//
//	//关于加血和伤害互斥
//	MuxItemSkillProp* pHealProp = pSkill->GetItemSkillProp();
//	if( NULL !=  pHealProp )
//	{
//		if( !pScript->FnPvcHealNum( pUseSkillPlayer, pTargetMonster, pHealProp->mID, &vecRst ) )
//		{
//			return false;
//		}
//
//		if( vecRst.size() != 4 )
//		{
//			D_ERROR("FnPvcHealNum 返回值数量错误\n");
//			return false;
//		}
//
//		unsigned int hp = vecRst[0];
//		unsigned int mp = vecRst[1];
//		unsigned int spBean = vecRst[2];
//		EBattleFlag healFlag = (EBattleFlag)vecRst[3];
//		
//		if( hp != 0 )
//		{
//			pTargetMonster->AddHp( hp );
//		}
//
//		if (healFlag == MISS)
//		{
//			isMiss = true;
//		}
//		//怪物不考虑mp和sp
//
//		rst.skillRst.skillType = ITEM_SKILL;		//加血的type
//		rst.skillRst.lHpSubed = hp;
//		rst.skillRst.lHpLeft = pTargetMonster->GetHp();
//		rst.skillRst.battleFlag = healFlag;
//	}
//
//	unsigned short arrRst[5];
//	memset(arrRst, 0, sizeof(arrRst));	
//	//检查技能类型和其他参数类型
//	if( pUseSkill->useSkill.skillType == PARTION_SKILL )
//	{
//		if( !pScript->FnCheckSkillSpilt( arrRst ) )
//		{
//			//调用错误
//			return false;
//		}
//
//		if ( 0 == arrRst[0] )
//		{
//			D_ERROR("SinglePvcWrapper:玩家%s使用分割技能%d成功，但FnCheckSkillSplit返回结果错误\n", pUseSkillPlayer->GetAccount());
//			return false;
//		}
//
//		rst.skillRst.arg1= pUseSkill->useSkill.arg1;
//		rst.skillRst.arg2 = pUseSkill->useSkill.arg2;
//		rst.skillRst.arg3 = arrRst[1];
//		rst.skillRst.arg4 = arrRst[2];
//		rst.skillRst.arg5 = arrRst[3];
//		rst.skillRst.arg6 = arrRst[4];
//	}
//
//	if ( !isMiss )
//	{
//		if( buffID != 0 )
//		{
//			if( assistObj == 0  )
//			{
//				pUseSkillPlayer->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() );
//			}else
//			{
//				pTargetMonster->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() );
//			}
//		}
//	}
//
//
//	rst.skillRst.attackID = pUseSkillPlayer->GetFullID();
//	rst.skillRst.attackHP = pUseSkillPlayer->GetCurrentHP();
//	rst.skillRst.attackMP = pUseSkillPlayer->GetCurrentMP();
//	rst.skillRst.lHpLeft = pTargetMonster->GetHp();
//	rst.skillRst.lCooldownTime = pSkill->GetCoolDown();
//	rst.skillRst.lCurseTime = pSkill->GetCurseTime();
//	rst.skillRst.lHpLeft = pTargetMonster->GetHp();
//	rst.skillRst.nErrCode = BATTLE_SUCCESS;
//	rst.skillRst.orgTargetID = orgTargetID;
//	rst.skillRst.scopeFlag = scopeFlag;
//	rst.skillRst.skillType = pUseSkill->useSkill.skillType;
//	rst.skillRst.skillID = pSkill->GetSkillID();
//	rst.skillRst.targetID = pTargetMonster->GetCreatureID();
//	rst.skillRst.targetRewardID = pTargetMonster->GetRewardInfo().rewardPlayerID;
//	rst.skillRst.sequenceID = pUseSkill->useSkill.sequenceID;
//	//rst.skillRst.causeBuffID = buffID;
//	isMiss ? rst.skillRst.causeBuffID = 0 : rst.skillRst.causeBuffID = buffID;
//
//
//	CMuxMap* pMap = pTargetMonster->GetMap();
//	if( pMap )
//	{
//		pMap->NotifySurrounding<MGUseSkillRst>( &rst, pTargetMonster->GetPosX(), pTargetMonster->GetPosY() );
//	}	
//
//	return true;
//}

/////取指定点视野内玩家与怪物数量；
//bool CMuxMap::GetPosSurPlayerMonsterNum( unsigned short posX, unsigned short posY, unsigned int& playerNum, unsigned int& monsterNum )
//{
//	if ( IsInsCopyMap() )
//	{
//		//副本；
//		if ( NULL == m_pCopyMapInfo )
//		{
//			D_ERROR( "CMuxMap::GetPosSurPlayerMonsterNum, NULL == m_pCopyMapInfo\n" );
//			return false;
//		}
//		return m_pCopyMapInfo->GetPosSurPlayerMonsterNum( posX, posY, playerNum, monsterNum );
//	} else {
//		//普通地图；
//		if ( NULL == m_pNormalMapInfo )
//		{
//			D_ERROR( "CMuxMap::GetPosSurPlayerMonsterNum, NULL == m_pNormalMapInfo\n" );
//			return false;
//		}
//		return m_pNormalMapInfo->GetPosSurPlayerMonsterNum( posX, posY, playerNum, monsterNum );
//	}
//
//	return false;
//}

bool CMuxMap::SinglePvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer, CMuxPlayerSkill* pSkill, bool isOrgTgt, AttackInfoPerTgt& outAtkInfoPerTgt
							   , AreaOfInterest& attackAoe, SubHpInfoArr& subHpInfoArr )
{
	if( NULL == pUseSkillPlayer || NULL == pTargetPlayer || NULL == pSkill/* || NULL == pUseSkill */)
	{
		D_ERROR("SinglePvpWrapper，输入指针空\n");
		return false;
	}

	outAtkInfoPerTgt.causeBuffID = 0;//初始值，因为后续如果没有扣血，则不会再赋此值
	outAtkInfoPerTgt.hpSubed = 0;
	outAtkInfoPerTgt.hpLeft = pTargetPlayer->GetCurrentHP();//初始值，因为后续如果没有扣血，则不会再赋此值

	if ( pTargetPlayer->IsDying() )
	{
		D_ERROR("CMuxMap::SinglePvpWrapper, 目标玩家%s已死亡，不再受%s攻击\n", pTargetPlayer->GetAccount(), pUseSkillPlayer->GetAccount() );
		return false;
	}

	//相关影响块更新；
	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( pTargetPlayer->GetPosX(), pTargetPlayer->GetPosY(), blockX, blockY );//假设在此过程中玩家位置发生变化，则仍需以原始点计算影响块

	vector<int> vecRst;
	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("SinglePvpWrapper，战斗脚本指针空\n");
		return false;
	}

	//判定buff

	bool isMiss = false;
	bool isTargetRogue = pTargetPlayer->IsRogueState();

	unsigned int buffID = 0;
	MuxBuffProp* pBuffProp = pSkill->GetBuffProp();
	unsigned int buffTime = 0;
	unsigned int assistObj = 0;//加buff对象，0给攻击者，否则给目标，如果真是0的话，则对于aoe技能，则会在一次攻击中给自己上多个buff?? 10.05.10
	if( NULL != pBuffProp )
	{
		if( !pScript->FnAddBuffPvp( pUseSkillPlayer, pTargetPlayer, pBuffProp->mID, &vecRst ) )
		{
			D_ERROR("SinglePvpWrapper，战斗脚本指针空，FnAddBuffPvp执行失败\n");
			return false;
		}

		if( vecRst.size() != 3 )
		{
			D_ERROR("SinglePvpWrapper FnAddBuffPvp返回值数量错误\n");
			return false;
		}

		buffID = vecRst[0];
		buffTime = vecRst[1];
		assistObj = vecRst[2];
	}

	//解除类型技能
	if( pSkill->GetRelieveID() != 0 )
	{
		if( !pScript->FnRelievePvp( pUseSkillPlayer, pTargetPlayer, pSkill->GetRelieveID(), &vecRst ) )
		{
			D_ERROR("SinglePvpWrapper FnRelievePvp执行失败\n");
			return false;
		}
	}
	
	//关于伤害
	if( NULL != pSkill->GetDamageProp() )
	{
		if( !pScript->FnPvpDamage( pUseSkillPlayer, pTargetPlayer, pSkill->GetSkillID(), &vecRst ) )
		{
			//调用脚本错误
			D_ERROR("SinglePvpWrapper, FnPvpDamage失败\n");
			return false;
		}

		if( vecRst.size() != 2 )
		{
			D_ERROR("SinglePvpWrapper，FnPvpDamage 返回值错误\n");
			return false;
		}

		unsigned int nDamage = vecRst[0];			
		EBattleFlag battleFlag = (EBattleFlag)vecRst[1];
		if ( (nDamage <= 0) && (battleFlag != MISS) )
		{
			D_WARNING( "SinglePvpWrapper，FnPvpDamage, 伤血为0但返回为非MISS, skillID:%d\n", pSkill->GetSkillID() );
		}

		//新修改中去除每目标的battleFlag，若需要时再添加，10.05.10，outAtkInfoPerTgt.battleFlag = battleFlag;
		if ( MISS == battleFlag )
		{
			isMiss = true;
		}

		if( 0 != nDamage )
		{
			//通知AI此事件发生
			CMonster* pFollowingMonster = pTargetPlayer->GetFollowingMonster();
			if( NULL != pFollowingMonster )
			{
				pFollowingMonster->OnFollowPlayerBeAttack( pUseSkillPlayer->GetCreatureID(), nDamage );
			}
		}//if( 0 != nDamage ) //造成了伤害

		outAtkInfoPerTgt.causeBuffID = isMiss ? 0:buffID;
		outAtkInfoPerTgt.hpSubed = nDamage;
		outAtkInfoPerTgt.hpLeft = (pTargetPlayer->GetCurrentHP()>nDamage) ? (pTargetPlayer->GetCurrentHP()-nDamage):0;
		D_DEBUG( "SinglePvPWrapper1, playerid:%d, hpSubed%d, hpLeft%d\n", pTargetPlayer->GetFullID().dwPID, outAtkInfoPerTgt.hpSubed, outAtkInfoPerTgt.hpLeft );
		subHpInfoArr.AddOneSubInfo( (nDamage>0)&&(isOrgTgt)/*有伤害且为原始目标则附带武器技能*/, pTargetPlayer, NULL, nDamage
			, pSkill->GetCurseTime(), battleFlag, (0==assistObj)/*buff目标：攻击者自身或攻击目标*/, outAtkInfoPerTgt.causeBuffID, buffTime );//准备稍后减血

	}//if( NULL != pSkill->GetDamageProp() ) 伤害技能

	//关于加血，？只有道具技能加血？10.05.10
	if ( NULL != pSkill->GetItemSkillProp() )
	{
		if( !pScript->FnPvpHealNum( pUseSkillPlayer, pTargetPlayer, pSkill->GetSkillID(), &vecRst ) )
		{
			D_ERROR("SinglePvpWrapper，FnPvpHealNum执行失败\n");
			return false;
		}

		if( vecRst.size() != 4 )
		{
			D_ERROR("SinglePvpWrapper，FnPvpHealNum返回值错误\n");
			return false;
		}
		
		unsigned int healDamage = vecRst[0];
		unsigned int mp = vecRst[1];
		unsigned int spBean = vecRst[2];
    	EBattleFlag healFlag = (EBattleFlag)vecRst[3];

		if (healFlag == MISS)
		{
			isMiss = true;
		}

	    if( 0 != healDamage )
		{
			pTargetPlayer->AddPlayerHp( healDamage );
			pTargetPlayer->NotifySurroundingMonsterHpAdd( pUseSkillPlayer->GetFullID(), healDamage );
		}

		if( 0 != mp )
		{
			pTargetPlayer->AddPlayerMp( mp );
		}

		if( 0 != spBean )
		{
			pTargetPlayer->AddSpBean( spBean );
		}

		//新修改后去除，by dzj, 10.05.10，rst.skillRst.skillType = ITEM_SKILL;
		//新修改中去除每目标的battleFlag，若需要时再添加，outAtkInfoPerTgt.battleFlag = healFlag;
		outAtkInfoPerTgt.hpSubed = healDamage;
		outAtkInfoPerTgt.hpLeft = pTargetPlayer->GetCurrentHP();
		D_DEBUG( "SinglePvPWrapper2, playerid:%d, hpSubed%d, hpLeft%d\n", pTargetPlayer->GetFullID().dwPID, outAtkInfoPerTgt.hpSubed, outAtkInfoPerTgt.hpLeft );

	}// if ( NULL != pSkill->GetItemSkillProp() ) 关于加血，？只有道具技能加血？10.05.10

	//每目标的分割技能相关计算去除;

	//以下根据miss与否创建可能有的buff;
	if ( !isMiss )
	{
		//附带buff添加
		if( buffID != 0 )
		{
			outAtkInfoPerTgt.causeBuffID = buffID;
		}
	}

	outAtkInfoPerTgt.targetID = pTargetPlayer->GetFullID();
	outAtkInfoPerTgt.tgtRewardID.dwPID = 0;//攻击目标为人，没有目标所属玩家
	outAtkInfoPerTgt.tgtRewardID.wGID = 0;

	//零散,流氓状态
	if( !pSkill->IsGoodSkill() ) 
	{
		pUseSkillPlayer->FightStartCheckRogueState( pTargetPlayer, isTargetRogue );
	}

    attackAoe.AddOneKernelBlock( blockX, blockY, blockX, blockY );
 
	return true;
} //bool CMuxMap::SinglePvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer, CMuxPlayerSkill* pSkill, const PlayerID& orgTargetID,  EScopeFlag scopeFlag, const GMUseSkill* pUseSkill )

bool CMuxMap::SubHpInfo::WeaponSkillPvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster )
{
	if( NULL == pUseSkillPlayer || NULL == pTargetMonster )
		return false;

	CBattleScript *pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
		return false;

	CMuxMap* pMap = pUseSkillPlayer->GetCurMap();
	if( NULL == pMap )
		return false;

	//关于武器技能
	int weaponSkillID = 0, weaponSkillRate = 0;
	if( pUseSkillPlayer->GetWeaponSkill( weaponSkillID, weaponSkillRate ) && pTargetMonster->GetHp() != 0 )
	{	
		if( weaponSkillID != 0 && RandRate( weaponSkillRate / 100.f) )
		{
			CMuxSkillProp* pWeaponSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( weaponSkillID );
			if( NULL == pWeaponSkillProp )
				return false;

			//武器技能有伤害和buff
			vector<int> vecRst;
			MuxNpcDamageProp* pDamageProp = pWeaponSkillProp->m_pNpcDamageProp;
			if( NULL != pDamageProp )
			{
				if( !pScript->FnItemDamagePvc( pUseSkillPlayer, pTargetMonster, pDamageProp->mID, &vecRst ) )
				{
					return false;				
				}

				if( vecRst.size() != 2 )
				{
					return false;
				}

				unsigned int damage = (unsigned int)vecRst[0];
				EBattleFlag batFlag = (EBattleFlag)vecRst[1];

				if ( (damage <= 0) && (batFlag != MISS) )
				{
					D_WARNING( "WeaponSkillPvcWrapper，FnItemDamagePvc, 伤血为0但返回为非MISS, skillID:%d\n", pDamageProp->mID );
				}
				
				pTargetMonster->SubMonsterHp( damage, pUseSkillPlayer, false, 0 );

				MGTargetGetDamage tarDam;
				tarDam.battleFlag = batFlag;
				tarDam.damage = damage;
				tarDam.targetID = pTargetMonster->GetCreatureID();
				tarDam.skillID = weaponSkillID;
				tarDam.targetLeftHp = pTargetMonster->GetHp();
				tarDam.noticeID = pUseSkillPlayer->GetFullID();
				pMap->NotifySurrounding<MGTargetGetDamage>( &tarDam, pUseSkillPlayer->GetPosX(), pUseSkillPlayer->GetPosY() );
			}

			////////////////////////////buff//////////////////////////
			MuxBuffProp* pBuffProp = pWeaponSkillProp->m_pBuffProp;
			if( NULL != pBuffProp )
			{
				if( !pScript->FnAddBuffPvc( pUseSkillPlayer, pTargetMonster, pBuffProp->mID, &vecRst ) )
				{
					return false;
				}

				if( vecRst.size() != 3 )
				{
					return false;
				}

				unsigned int buffID = vecRst[0];
				unsigned int buffTime = vecRst[1];
				
				pTargetMonster->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() );
			}
		}
	}

	return true;	
}// bool CMuxMap::SubHpInfo::WeaponSkillPvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster )

bool CMuxMap::SubHpInfo::WeaponSkillPvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer )
{
	if( NULL == pUseSkillPlayer || NULL == pTargetPlayer )
		return false;

	CBattleScript *pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
		return false;

	CMuxMap* pMap = pUseSkillPlayer->GetCurMap();
	if( NULL == pMap )
		return false;

	//关于武器技能
	int weaponSkillID = 0, weaponSkillRate = 0;
	if( pUseSkillPlayer->GetWeaponSkill( weaponSkillID, weaponSkillRate ) && pTargetPlayer->GetCurrentHP() != 0 )
	{	
		if( weaponSkillID != 0 && RandRate( weaponSkillRate / 100.f) )
		{
			CMuxSkillProp* pWeaponSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( weaponSkillID );
			if( NULL == pWeaponSkillProp )
				return false;

			//武器技能有伤害和buff
			vector<int> vecRst;
			MuxNpcDamageProp* pDamageProp = pWeaponSkillProp->m_pNpcDamageProp;
			if( NULL != pDamageProp )
			{
				if( !pScript->FnItemDamagePvp( pUseSkillPlayer, pTargetPlayer, pDamageProp->mID, &vecRst ) )
				{
					return false;				
				}

				if( vecRst.size() != 2 )
				{
					return false;
				}

				unsigned int damage = (unsigned int)vecRst[0];
				EBattleFlag batFlag = (EBattleFlag)vecRst[1];

				if ( (damage <= 0) && (batFlag != MISS) )
				{
					D_WARNING( "WeaponSkillPvpWrapper，FnItemDamagePvp, 伤血为0但返回为非MISS, skillID:%d\n", pDamageProp->mID );
				}
				
				pTargetPlayer->SubPlayerHp( damage, pUseSkillPlayer, pUseSkillPlayer->GetFullID() );

				MGTargetGetDamage tarDam;
				tarDam.battleFlag = batFlag;
				tarDam.damage = damage;
				tarDam.targetID = pTargetPlayer->GetCreatureID();
				tarDam.skillID = weaponSkillID;
				tarDam.targetLeftHp = pTargetPlayer->GetCurrentHP();
				tarDam.noticeID = pUseSkillPlayer->GetFullID();
				pMap->NotifySurrounding<MGTargetGetDamage>( &tarDam, pUseSkillPlayer->GetPosX(), pUseSkillPlayer->GetPosY() );
			}

			////////////////////////////buff//////////////////////////
			MuxBuffProp* pBuffProp = pWeaponSkillProp->m_pBuffProp;
			if( NULL != pBuffProp )
			{
				if( !pScript->FnAddBuffPvp( pUseSkillPlayer, pUseSkillPlayer, pBuffProp->mID, &vecRst ) )
				{
					return false;
				}

				if( vecRst.size() != 3 )
				{
					return false;
				}

				unsigned int buffID = vecRst[0];
				unsigned int buffTime = vecRst[1];
				
				if( buffID != 0 )
				{
					pTargetPlayer->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() );
				}
			}
		}
	}

	return true;
} // bool CMuxMap::SubHpInfo::WeaponSkillPvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer )

//攻击目标数组中的一个目标减血；
bool CMuxMap::SubHpInfo::SubOneTgtHp( CPlayer* pAttacker, CMuxPlayerSkill* pSkill, CBattleScript* pBattleScript )
{
	if ( ( NULL == pAttacker )
		|| ( NULL == pSkill )
		|| ( NULL == pBattleScript )
		)
	{
		D_ERROR( "CMuxMap::SubHpInfo::SubOneTgtHp, 输入参数空\n" );
		return false;
	}

	vector<int>  vecRst;

	if ( NULL != pTgtMonster )
	{
		if ( pTgtMonster->IsMonsterDie() )
		{
			D_WARNING( "CMuxMap::SubHpInfo::SubOneTgtHp, 怪物%d已死亡，忽略减血\n", pTgtMonster->GetCreatureID().dwPID );
			return true;
		}
		//怪物减血...
		pTgtMonster->SubMonsterHp( hpSubed, pAttacker, false, pSkill->GetCurseTime() );
		//RankEventManagerSingle::instance()->MonitorPlayerDemage( pAttacker, hpSubed );
		RankEventManager::MonitorPlayerDamage( pAttacker, hpSubed );

		//关于伤害反弹以及武器技能
		if( hpSubed != 0 )
		{
			if ( pAttacker->IsDying() )
			{
				D_WARNING( "CMuxMap::SubHpInfo::SubOneTgtHp, 玩家%s已死亡，忽略伤害反弹\n", pAttacker->GetAccount() );
				return true;
			}
			//伤害反弹
			if( !pBattleScript->FnPvcDamageRebound( pAttacker, pTgtMonster, hpSubed, battleFlag, &vecRst ) )
			{
				D_ERROR("CMuxMap::SubHpInfo::SubOneTgtHp, FnPvcDamageRebound执行错误\n");
				return false;
			}

			if( vecRst.size() != 2 )
			{
				D_ERROR("CMuxMap::SubHpInfo::SubOneTgtHp, FnPvcDamageRebound返回值数量错误\n");
				return false;
			}

			unsigned int damReb = (unsigned int)vecRst[0];
			EBattleFlag drFlag = (EBattleFlag)vecRst[1];

			if( damReb != 0 )
			{
				pAttacker->NotifyDamageRebound( pSkill->GetSkillID(), damReb, drFlag );
				pAttacker->SubPlayerHp(damReb, NULL, pTgtMonster->GetCreatureID());
				pAttacker->GetSelfStateManager().OnPlayerBeAttack();
				pAttacker->BeInjuredAffectWear();
				//RankEventManagerSingle::instance()->MontiorPlayerBeDemage( pAttacker, damReb  );		//排行榜增加
				//被伤害排行去除，RankEventManager::MontiorPlayerBeDemage( pAttacker, damReb  );		//排行榜增加
			}
		}//if( hpSubed != 0 )

		if( isCheckWeaponSkill )
		{
			if ( pAttacker->IsDying() )
			{
				D_WARNING( "CMuxMap::SubHpInfo::SubOneTgtHp, 玩家%s已死亡，忽略附带武器技能判定\n", pAttacker->GetAccount() );
				return true;
			}

			if ( pTgtMonster->IsMonsterDie() )
			{
				D_WARNING( "CMuxMap::SubHpInfo::SubOneTgtHp, 怪物%d已死亡，忽略附带武器技能判定\n", pTgtMonster->GetCreatureID().dwPID );
				return true;
			}
			WeaponSkillPvcWrapper( pAttacker, pTgtMonster ); //直接目标附带武器技能, 注释添加 by dzj, 10.05.15;
		}

		if( buffID != 0 )
		{
			if( isBuffSelf  )
			{
				pAttacker->CreateMuxBuffByID( buffID, buffTime, pAttacker->GetFullID() );
			} else {
				pTgtMonster->CreateMuxBuffByID( buffID, buffTime, pAttacker->GetFullID() );
			}
		}
		//...怪物减血；
		return true;
	} else if ( NULL != pTgtPlayer ) {
		//玩家减血...
		//减血
		pTgtPlayer->SubPlayerHp( hpSubed, pAttacker, pAttacker->GetFullID() );
		pTgtPlayer->GetSelfStateManager().OnPlayerBeAttack();
		pTgtPlayer->BeInjuredAffectWear();
		//RankEventManagerSingle::instance()->MontiorPlayerBeDemage( pTgtPlayer, hpSubed  );		//排行榜增加
		//被伤害排行去除，RankEventManager::MontiorPlayerBeDemage( pTgtPlayer, hpSubed  );		//排行榜增加
		//RankEventManagerSingle::instance()->MonitorPlayerDemage( pAttacker, hpSubed  );
		RankEventManager::MonitorPlayerDamage( pAttacker, hpSubed  );

		if( hpSubed != 0 )
		{
			//伤害反弹计算
			if( !pBattleScript->FnPvpDamageRebound( pAttacker, pTgtPlayer, hpSubed, battleFlag, &vecRst ) )
			{
				D_ERROR("SinglePvpWrapper，FnPvpDamageRebound失败\n");
				return false;
			}
			if( vecRst.size() != 2 )
			{
				D_ERROR("SinglePvpWrapper，FnPvpDamageRebound 返回值错误\n");
				return false;
			}

			unsigned int damReb = vecRst[0];
			EBattleFlag damRebFlag = (EBattleFlag)vecRst[1];
			if( damReb != 0 )
			{
				pAttacker->NotifyDamageRebound( pSkill->GetSkillID(), damReb, damRebFlag );
				pAttacker->SubPlayerHp(damReb,pTgtPlayer,pTgtPlayer->GetFullID());
				pAttacker->GetSelfStateManager().OnPlayerBeAttack();
				pAttacker->BeInjuredAffectWear();
				//RankEventManagerSingle::instance()->MontiorPlayerBeDemage( pAttacker, damReb  );		//排行榜增加
				//被伤害排行去除，RankEventManager::MontiorPlayerBeDemage( pAttacker, damReb  );		//排行榜增加
				//RankEventManagerSingle::instance()->MonitorPlayerDemage( pTgtPlayer, damReb  );
				RankEventManager::MonitorPlayerDamage( pTgtPlayer, damReb  );
			}
		}

		//有伤害时，同时检测武器附带伤害，根据原hyn代码修改而来；
		if( isCheckWeaponSkill )
		{
			WeaponSkillPvpWrapper( pAttacker, pTgtPlayer );
		}

		if( buffID != 0 )
		{
			if( isBuffSelf  )
			{
				pAttacker->CreateMuxBuffByID( buffID, buffTime, pAttacker->GetFullID() ); 
			}else{
				pTgtPlayer->CreateMuxBuffByID( buffID, buffTime, pAttacker->GetFullID() );
			}
		}
		//...玩家减血
		return true;
	} else {
		D_ERROR( "CMuxMap::SubHpInfo::SubOneTgtHp， 目标空\n" );
		return false;
	}//if ( NULL != pTgtMonster )

	D_ERROR( "之前的if路径全，不可能到此处\n" );
	return false;
} //bool CMuxMap::SubHpInfo::SubOneTgtHp( CPlayer* pAttacker, CSkill* pSkill, CBattleScript* pBattleScript )

//对数组中各目标进行减血；
bool CMuxMap::SubHpInfoArr::SubArrTgtsHp( CPlayer* pAttacker, CMuxPlayerSkill* pSkill )
{
	if ( ( NULL == pAttacker ) || ( NULL == pSkill ) )
	{
		D_ERROR( "CMuxMap::SubHpInfoArr::SubArrTgtsHp,输入参数空\n" );
		return false;
	}

	if (curTgtNum > ARRAY_SIZE(m_SubHpArr) )
	{
		D_ERROR( "CMuxMap::SubHpInfoArr::SubTgtsHp，目标数%d超限\n", curTgtNum );
		return false;
	}

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if ( NULL == pScript )
	{
		D_ERROR( "CMuxMap::SubHpInfoArr::SubTgtsHp，NULL == pScript\n" );
		return false;
	}

	for ( unsigned int i=0; i<curTgtNum; ++i )
	{
		m_SubHpArr[i].SubOneTgtHp( pAttacker, pSkill, pScript );
	}

	ResetSubHpInfoArr();

	return true;
}// bool CMuxMap::SubHpInfoArr::SubArrTgtsHp( CPlayer* pAttacker, CSkill* pSkill )

bool CMuxMap::SinglePvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster, CMuxPlayerSkill* pSkill
							   , bool isOrgTgt, AttackInfoPerTgt& outAtkInfoPerTgt, bool isKickBack, TYPE_POS kickPosX, TYPE_POS kickPosY
							   , AreaOfInterest& attackAoe, SubHpInfoArr& subHpInfoArr )
{
	if ( NULL == pUseSkillPlayer || NULL == pTargetMonster || NULL == pSkill/* || NULL == pUseSkill */)
	{
		D_ERROR( "SinglePvcWrapper, 输入指针空，非法\n" );
		return false;
	}

	outAtkInfoPerTgt.causeBuffID = 0;//初始值，因为后续如果没有扣血，则不会再赋此值
	outAtkInfoPerTgt.hpSubed = 0;
	outAtkInfoPerTgt.hpLeft = pTargetMonster->GetHp();//初始值，因为后续如果没有扣血，则不会再赋此值

	if ( pTargetMonster->IsMonsterDie() )
	{
		D_ERROR( "CMuxMap::SinglePvcWrapper, 怪物(%d)已死，不再受玩家%s攻击\n", pTargetMonster->GetCreatureID().dwPID, pUseSkillPlayer->GetAccount() );
		return false;
	}
	//D_DEBUG( "SinglePvcWrapper, monsterid:%d, prefight HP%d\n", pTargetMonster->GetCreatureID().dwPID, pTargetMonster->GetHp() );

	////相关影响块更新；
	unsigned short blockX=0, blockY=0;//保存怪物当前位置，因为在这过程中怪物可能被击退，而如果本攻击成功，则仍需以怪物原始点作为影响范围
	MuxMapSpace::GridToBlockPos( pTargetMonster->GetPosX(), pTargetMonster->GetPosY(), blockX, blockY );

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR( "SinglePvcWrapper, NULL == pScript \n" );
		return false;
	}

	vector<int> vecRst;
	bool isMiss = false;
	unsigned int buffID = 0;
	unsigned int buffTime = 0;
	unsigned int assistObj = 0;

	do {
		MuxBuffProp* pBuffProp = pSkill->GetBuffProp();
		if( NULL != pBuffProp )
		{
			if( !pScript->FnAddBuffPvc( pUseSkillPlayer, pTargetMonster, pBuffProp->mID, &vecRst ) )
			{
				D_ERROR( "SinglePvcWrapper, FnAddBuffPvc执行失败\n" );
				return false;
			}

			if( vecRst.size() != 3 )
			{
				D_ERROR( "SinglePvcWrapper, FnAddBuffPvc 返回值数量错误\n" );
				return false;
			}

			buffID = vecRst[0];
			buffTime = vecRst[1];
			assistObj = vecRst[2];
		}

		//伤害
		if( NULL != pSkill->GetDamageProp() )
		{
			if( !pScript->FnPvcDamage( pUseSkillPlayer, pTargetMonster, pSkill->GetSkillID(), &vecRst ) )
			{
				//调用脚本错误
				D_ERROR( "SinglePvcWrapper, FnPvcDamage执行错误\n" );
				return false;
			}

			if( vecRst.size() != 2 )
			{
				D_ERROR("SinglePvcWrapper, FnPvcDamage返回值数量错误\n");
				return false;
			}

			EBattleFlag battleFlag = (EBattleFlag)vecRst[1];
			unsigned int nDamage = vecRst[0];

			if ( (nDamage <= 0) && (battleFlag != MISS) )
			{
				D_WARNING( "SinglePvcWrapper，FnPvcDamage, 伤血为0但返回为非MISS, skillID:%d\n", pSkill->GetSkillID() );
			}

			{//不攻击处在障碍点内的怪物，以免怪物直接在障碍物内反击；
				unsigned short monsterposx = pTargetMonster->GetPosX();
				unsigned short monsterposy = pTargetMonster->GetPosY();
				if ( !IsGridCanPass(monsterposx, monsterposy) )
				{
					battleFlag = MISS;//如果怪物在障碍物内，则按MISS处理;
					//MISS情形下不会通知AI，如果以后其它MISS改为通知AI(以便怪物作出反击)，则在此处应立刻返回, return false;
				}
			}

			if ( battleFlag == MISS ) 
			{
				isMiss = true;
				if ( nDamage > 0 )
				{
					D_ERROR("SinglePvcWrapper, 技能%d, miss但nDamage%d大于0\n", pSkill->GetSkillID(), nDamage);
					return false;
				}
				outAtkInfoPerTgt.hpSubed = 0;
				outAtkInfoPerTgt.hpLeft = pTargetMonster->GetHp();
				break;
			}

			if ( isKickBack )
			{
				if ( nDamage >= pTargetMonster->GetHp() )
				{
					//死亡时根据技能属性配置决定是否击退;
					if ( !pSkill->IsDeadKickBack() )
					{
						isKickBack = false;
					}
				}
			}

			if ( isKickBack )//加血不可能同时击退，所以在加血处理中不添加击退相关代码，只在对怪物造成伤害时才进行此判断，by dzj, 10.05.15;
			{
				//以下攻击成功，调整怪物位置(客户端消息中的怪物位置单位为0.1格)
				bool iskickbackok = OnMonsterBeyondGrid( pTargetMonster, pTargetMonster->GetPosX(), pTargetMonster->GetPosY(), kickPosX/10, kickPosY/10, true/*仍然要通知新区域人员，同时新区域人员不发送攻击消息*//*isForceAndNotNotiMove*/ );//内部相关通知废去，由本攻击消息来通知
				if ( iskickbackok )
				{
					pTargetMonster->OnBeKickBack( kickPosX, kickPosY );
					outAtkInfoPerTgt.newPosX = kickPosX;
					outAtkInfoPerTgt.newPosY = kickPosY;
				} else {
					D_WARNING( "SinglePvcWrapper, 玩家%s技能%d, 试图击退怪物%d(%d,%d)至新位置(%d,%d,单位0.1)失败\n", pUseSkillPlayer->GetAccount(), pSkill->GetSkillID()
						, pTargetMonster->GetCreatureID().dwPID, pTargetMonster->GetPosX(), pTargetMonster->GetPosY(), kickPosX, kickPosY );
				}
			}

			//新修改中去除每目标的battleFlag，若需要时再添加，utAtkInfoPerTgt.battleFlag = battleFlag;
			outAtkInfoPerTgt.causeBuffID = isMiss ? 0:buffID;
			outAtkInfoPerTgt.hpSubed = nDamage;
			outAtkInfoPerTgt.hpLeft = (pTargetMonster->GetHp()>nDamage) ? (pTargetMonster->GetHp()-nDamage):0;
			D_DEBUG( "SinglePvcWrapper1, monsterid:%d, hpSubed%d, hpLeft%d\n", pTargetMonster->GetCreatureID().dwPID, outAtkInfoPerTgt.hpSubed, outAtkInfoPerTgt.hpLeft );
			subHpInfoArr.AddOneSubInfo( (nDamage>0)&&(isOrgTgt)/*有伤害且为原始目标则附带武器技能*/, NULL, pTargetMonster, nDamage
				, pSkill->GetCurseTime(), battleFlag, (0==assistObj)/*buff目标：攻击者自身或攻击目标*/, outAtkInfoPerTgt.causeBuffID, buffTime );//准备稍后减血

			break;//发生伤害时，真正减血稍后进行，以便先通知周边攻击成功以及对应的位置改变，再通知减血引起的其它消息，防止客户端因为这种先后顺序而引发表现错误 。
			      //同时，按贺轶男之前代码看伤害技能与加血技能不能同时拥有（否则它们填充攻击结果消息时会互相覆盖），因此，这里可以直接break;

		}//if( NULL != pSkill->GetDamageProp() )

		//关于加血和伤害互斥
		MuxItemSkillProp* pHealProp = pSkill->GetItemSkillProp();
		if( NULL !=  pHealProp ) //加血不可能同时击退，所以在加血处理中不添加击退相关代码，by dzj, 10.05.15;
		{
			if( !pScript->FnPvcHealNum( pUseSkillPlayer, pTargetMonster, pHealProp->mID, &vecRst ) )
			{
				D_ERROR("SinglePvcWrapper, FnPvcHealNum执行错误\n");
				return false;
			}

			if( vecRst.size() != 4 )
			{
				D_ERROR("SinglePvcWrapper, FnPvcHealNum返回值数量错误\n");
				return false;
			}

			unsigned int hp = vecRst[0];
			unsigned int mp = vecRst[1];
			unsigned int spBean = vecRst[2];
			EBattleFlag healFlag = (EBattleFlag)vecRst[3];

			if( hp != 0 )
			{
				pTargetMonster->AddHp( hp );
			}

			if ( MISS == healFlag )
			{
				isMiss = true;
			}

			//怪物不考虑mp和sp

			//新修改后去除，by dzj, 10.05.10,rst.skillRst.skillType = ITEM_SKILL;		//加血的type
			outAtkInfoPerTgt.hpSubed = hp;
			outAtkInfoPerTgt.hpLeft = pTargetMonster->GetHp();
			D_DEBUG( "SinglePvcWrapper2, monsterid:%d, hpSubed%d, hpLeft%d\n", pTargetMonster->GetCreatureID().dwPID, outAtkInfoPerTgt.hpSubed, outAtkInfoPerTgt.hpLeft );

			//新修改中去除每目标的battleFlag，若需要时再添加，outAtkInfoPerTgt.battleFlag = healFlag;
		}
	} while (false);

	//分割技能相关检查去除；

	if ( !isMiss )
	{
		if( buffID != 0 )
		{
			outAtkInfoPerTgt.causeBuffID = buffID;//通知附带buff;
		}
	}

	outAtkInfoPerTgt.targetID = pTargetMonster->GetCreatureID();
	outAtkInfoPerTgt.tgtRewardID = pTargetMonster->GetRewardInfo().rewardPlayerID;

	attackAoe.AddOneKernelBlock( blockX, blockY, blockX, blockY );//攻击成功，以原始位置计算影响块；

	return true;
}// bool CMuxMap::SinglePvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster, CMuxPlayerSkill* pSkill, const PlayerID& orgTargetID,  EScopeFlag scopeFlag, const GMUseSkill* pUseSkill )

void CMuxMap::InitWarMapRelation(void)
{
	if(NULL != m_pMapProperty)
	{
		CMapRelation::eMapRelatType relaType;//关系类型
		unsigned short associMapID;//关联地图

		if(CMapRelation::GetMapRelation((unsigned short)m_pMapProperty->GetPropertyMapNo(), relaType, associMapID))
		{
			if(NULL == m_pWarAdditionInfo)
				m_pWarAdditionInfo = NEW MapWarAdditionalInfo;

			if(NULL != m_pWarAdditionInfo)
			{
				m_pWarAdditionInfo->mapRelationType = relaType;
				m_pWarAdditionInfo->associatedMapId = associMapID;
				m_pWarAdditionInfo->warRebirthRace = 0;
				m_pWarAdditionInfo->warSwitchRace = 0;
				m_pWarAdditionInfo->warStartFlag = false;
				m_pWarAdditionInfo->warStartTime = 0;
			}
		}
	}

	return;
}

bool CMuxMap::SetWarStart(void)
{
	if(NULL != m_pWarAdditionInfo)
	{
		m_pWarAdditionInfo->SetWarStart();
		return true;
	}

	return false;
}


//攻城战结束
bool CMuxMap::SetWarEnd()
{
	if(NULL != m_pWarAdditionInfo)
	{
		m_pWarAdditionInfo->SetWarEnd();
		return true;
	}
	return false;
}

bool CMuxMap::InWarState(void)
{ 
	if((NULL != m_pWarAdditionInfo) && m_pWarAdditionInfo->warStartFlag)
		return true;

	return false;
}

unsigned int CMuxMap::GetWarStartTime(void)
{
	if((NULL != m_pWarAdditionInfo) && InWarState())
		return m_pWarAdditionInfo->warStartTime;

	return 0;
}

bool CMuxMap::GetWarInnerRebirthPt(unsigned short race, unsigned short &mapId, unsigned int &posX, unsigned int &posY, unsigned char &err)
{
	bool retcode = false;//缺省返回值
	err = 3;//缺省错误(err: 1:种族不匹配, 2:没有找到复活点,3:其他错误)	

	if(NULL != m_pWarAdditionInfo)
	{
		if(m_pWarAdditionInfo->mapRelationType == CMapRelation::INNER_MAP)//当前是内城地图
		{
			if(!m_pWarAdditionInfo->RebirthRaceEnabled(race))	
				err = 1;
			else if(!m_pMapProperty->GetWarRebirthPt(mapId, posX, posY))
				err = 2;
			else
				retcode = true;
		}
		else
		{
			CMuxMap *pAssociatedMap = CManNormalMap::FindMap(m_pWarAdditionInfo->associatedMapId);
			if(NULL != pAssociatedMap)
			{
				MapWarAdditionalInfo *pAssociatedAddtionMapInfo = pAssociatedMap->GetWarAdditionalInfo();
				if(NULL != pAssociatedAddtionMapInfo)
				{
					if(!pAssociatedAddtionMapInfo->RebirthRaceEnabled(race))
						err = 1;
					else
					{
						CMuxMapProperty *pAssociatedMapProperty = pAssociatedMap->GetMapProperty();
						if(NULL != pAssociatedMapProperty)
						{
							if(!pAssociatedMapProperty->GetWarRebirthPt(mapId, posX, posY))
								err = 2;
							else
								retcode = true;
						}
					}
				}
			}
		}
	}
	
	return retcode;
}

bool CMuxMap::GetWarOuterRebirthPt(unsigned short race, unsigned short &mapId, unsigned int &posX, unsigned int &posY, unsigned char &err)
{
	bool retcode = false;//缺省返回值
	err = 3;//缺省错误(err: 1:种族不匹配, 2:没有找到复活点,3:其他错误)	

	if(NULL != m_pWarAdditionInfo)
	{
		if(m_pWarAdditionInfo->mapRelationType == CMapRelation::OUTER_MAP)//当前是外城地图
		{
			if(!m_pWarAdditionInfo->RebirthRaceEnabled(race))
				err = 1;
			else if( !m_pMapProperty->GetWarRebirthPt(mapId, posX, posY))
				err = 2;
			else
				retcode = true;
		}
		else
		{
			CMuxMap *pAssociatedMap = CManNormalMap::FindMap(m_pWarAdditionInfo->associatedMapId);
			if(pAssociatedMap != NULL)
			{
				MapWarAdditionalInfo *pAssociatedAddtionMapInfo = pAssociatedMap->GetWarAdditionalInfo();
				if(NULL != pAssociatedAddtionMapInfo)
				{
					if(!pAssociatedAddtionMapInfo->RebirthRaceEnabled(race))
						err = 1;
					else
					{
						CMuxMapProperty *pAssociateMapProperty = pAssociatedMap->GetMapProperty();
						if(NULL != pAssociateMapProperty)
						{	
							if(!pAssociateMapProperty->GetWarRebirthPt(mapId, posX, posY))
								err = 2;
							else
								retcode = true;
						}
					}
				}
			}
		}
	}

	return retcode;
}

bool CMuxMap::GetWarDefaultRebirthPt(unsigned short race, unsigned short &mapId, unsigned int &posX, unsigned int &posY, unsigned char &err)
{
	bool retcode = false;//缺省返回值
	err = 3;//缺省错误(err: 1:种族不匹配, 2:没有找到复活点,3:其他错误)

	if(NULL != m_pWarAdditionInfo)
	{	
		if(m_pWarAdditionInfo->mapRelationType == CMapRelation::OUTER_MAP)//当前是外城地图
		{
			MuxPoint rebirthPt;
			if(!m_pMapProperty->GetNormalRebirthPt(rebirthPt))
				err = 2;
			else
			{
				mapId = m_pMapProperty->GetPropertyMapNo();
				posX = rebirthPt.nPosX;
				posY = rebirthPt.nPosY;

				retcode = true;
			}
		}
		else
		{
			CMuxMap *pAssociatedMap = CManNormalMap::FindMap(m_pWarAdditionInfo->associatedMapId);
			if(pAssociatedMap != NULL)
			{
				CMuxMapProperty *pAssociateMapPropert = pAssociatedMap->GetMapProperty();
				if(NULL != pAssociateMapPropert)
				{
					MuxPoint rebirthPt;
					if(!pAssociateMapPropert->GetNormalRebirthPt(rebirthPt))
						err = 2;
					else
					{
						mapId = pAssociateMapPropert->GetPropertyMapNo();
						posX = rebirthPt.nPosX;
						posY = rebirthPt.nPosY;

						retcode = true;
					}
				}
			}
		}
	}

	return retcode;
}

bool CMuxMap::ModityWarRebirthRace(unsigned short race)
{ 
	if(NULL != m_pWarAdditionInfo)
	{	
		m_pWarAdditionInfo->SetWarRebirthRace(race);
		return true;
	}

	return false;
}

bool CMuxMap::ModityWarSwitchRace(unsigned short race)
{ 
	if(NULL != m_pWarAdditionInfo)
	{
		m_pWarAdditionInfo->SetWarSwitchRace(race);
		return true;
	}
	
	return false; 
}


//杀死所有地图上的玩家
bool CMuxMap::KillAllPlayerByRace( unsigned int race )
{
	if( !IsInsCopyMap() )
	{
		if( m_pNormalMapInfo )
		{
			m_pNormalMapInfo->KillAllPlayerByRace( race );
			return true;
		}
	}else{
		if( m_pCopyMapInfo )
		{
			//m_pCopyMapInfo->KillAllPlayerByRace   如果需要的话？
		}
	}
	return false;
}


//传送玩家
bool CMuxMap::SwitchMapPlayer( unsigned int race, unsigned int mapID, unsigned int posX1, unsigned int posY1, unsigned int posX2, unsigned int posY2 )
{
	if( !IsInsCopyMap() )
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::SwitchMapPlayer, NULL == m_pNormalMapInfo\n" );
			return false;
		}
		m_pNormalMapInfo->SwitchMapPlayer( race, mapID, posX1, posY1, posX2, posY2 );
		return true;
	}else{
		D_ERROR( "脚本错误：在副本地图中使用SwitchMapPlayer，应使用设置玩家副本外位置，然后踢玩家的方法\n" );
		return false;
	}

	return false;
}

//去除种族后废弃，by dzj, 10.07.30;//检测是否有异族进入地图并通知
////  [1/5/2010 shenchenkai]
//void CMuxMap::OnCheckPlayerEnterMapNotify(CPlayer * pPlayer)
//{
//	if (NULL == pPlayer)
//	{
//		return;
//	}
//	CityInfo *pCityInfo = GetCityInfo();
//	if (NULL == pCityInfo)
//	{
//		return;
//	}
//	if (!InWarState())
//	{
//		return;
//	}
//	if (pCityInfo->ucRace != pPlayer->GetRace())
//	{//进入地图的是异族
//		ACE_Time_Value diff = ACE_OS::gettimeofday() - m_tInvade;
//		if (diff.sec() >= 5*60)
//		{//五分钟内多次进入不发通知
//			CRaceManager * pRaceManager = AllRaceMasterManagerSingle::instance()->GetRaceManagerByRace(pCityInfo->ucRace);
//			if (NULL != pRaceManager)
//			{
//				pRaceManager->NotifyInvade(pPlayer->GetRace());
//			}
//			m_tInvade = ACE_OS::gettimeofday();
//		}
//	}
//}

//种族废弃后去除，by dzj, 10.07.30////检测是否有异族攻击NPC并通知
////  [1/5/2010 shenchenkai]
//void CMuxMap::OnCheckPlayerAttackNpcNotify(CPlayer * pPlayer)
//{
//	if (NULL == pPlayer)
//	{
//		return;
//	}
//	CityInfo *pCityInfo = GetCityInfo();
//	if (NULL == pCityInfo)
//	{
//		return;
//	}
//	if (!InWarState())
//	{
//		return;
//	}
//	if (pCityInfo->ucRace != pPlayer->GetRace())
//	{//攻打npc的是异族
//		ACE_Time_Value diff = ACE_OS::gettimeofday() - m_tAttackNpc;
//		if (diff.sec() >= 5*60)
//		{//五分钟内多次攻打不发通知
//			CRaceManager * pRaceManager = AllRaceMasterManagerSingle::instance()->GetRaceManagerByRace(pCityInfo->ucRace);
//			if (NULL != pRaceManager)
//			{
//				pRaceManager->NotifyAttackNpc(pPlayer->GetRace());
//			}
//			m_tAttackNpc = ACE_OS::gettimeofday();
//		}
//	}
//}

void CMuxMap::OnActivityStart( unsigned int actID )
{
	CMapScript* pMapScript = GetMapScript();
	if( NULL == pMapScript )
	{
		//
		return;
	}

	pMapScript->OnActivityStart( this, actID );
	//SetWarStart(); //脚本中调用MSWarRealStart中才真正开始(20100205.zsw)
}


void CMuxMap::OnActivityEnd( unsigned int actID )
{
	CMapScript* pMapScript = GetMapScript();
	if( NULL == pMapScript )
	{
		//
		return;
	}

	pMapScript->OnActivityEnd( this, actID );
	SetWarEnd();
}


unsigned int CMuxMap::GetCounterCounter()		//获得地图计数器的数字
{
	if( NULL == m_pCounterOfMap )
	{
		return 0;
	}

	return 0;
}


bool CMuxMap::OnMonsterUseSkill( CMonster* pUseSkillMonster, unsigned int skillID, const PlayerID& targetID )
{
	if( NULL == pUseSkillMonster )
	{
		D_ERROR( "CMuxMap::OnMonsterUseSkill, NULL == pUseSkillMonster\n" );
		return false;
	}

	CMuxSkillProp* pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( skillID );
	if( NULL == pSkillProp )
	{
		D_ERROR("找不到怪物所使用的技能 编号:%d\n", skillID );
		return false;
	}
	
	CMonster* pTargetMonster = NULL;
	CPlayer* pTargetPlayer = NULL;
	bool pvc = false;
	TYPE_POS targetPosX = 0, targetPosY = 0;
	if( targetID.wGID == MONSTER_GID )
	{
		pTargetMonster = GetMonsterPtrInMap( targetID.dwPID );
		if( NULL == pTargetMonster )
		{
			D_ERROR("CMuxMap::OnMonsterUseSkill, 找不到被攻击的目标怪物 %d\n", targetID.dwPID );
			return false;
		}
		pvc = true;
		pTargetMonster->GetPos( targetPosX, targetPosY );
	}else{
		CGateSrv* pGateSrv = CManG_MProc::FindProc( targetID.wGID );
		if( NULL == pGateSrv )
		{
			D_ERROR("CMuxMap::OnMonsterUseSkill, 被攻击的目标玩家(%d:%d)所在gatesrv找不到\n", targetID.wGID, targetID.dwPID );
			return false;
		}
		pTargetPlayer = pGateSrv->FindPlayer( targetID.dwPID );
		if( NULL == pTargetPlayer )
		{
			D_ERROR("CMuxMap::OnMonsterUseSkill, 找不到被攻击的目标玩家(%d:%d)\n", targetID.wGID, targetID.dwPID );
			return false;
		}
		pvc = false;
		pTargetPlayer->GetPos( targetPosX, targetPosY );
		if ( pTargetPlayer->IsDying() )
		{
			D_DEBUG( "怪物%d攻击，原始目标玩家%s，玩家已死亡\n", pUseSkillMonster->GetCreatureID().dwPID, pTargetPlayer->GetAccount() );
			return false;
		}
	}

	if( !pSkillProp->IsAoeSkill() )
	{
		if( pvc )
		{
			SingleCvcWrapper( pUseSkillMonster, pTargetMonster, pSkillProp, targetID, SINGLE_ATTACK );
		}else{
			SingleCvpWrapper( pUseSkillMonster, pTargetPlayer, pSkillProp, targetID, SINGLE_ATTACK );
		}
		//D_DEBUG( "怪物%d使用单体技能%d\n", pUseSkillMonster->GetCreatureID().dwPID, skillID );
		return true;	
	}

	D_DEBUG( "怪物%d使用AOE技能%d\n", pUseSkillMonster->GetCreatureID().dwPID, skillID );	
	vector<CMonster*> vecTargetMonster;
	vector<CPlayer*> vecTargetPlayer;
	TYPE_POS aoePosX = 0, aoePosY =0;
	if( pSkillProp->m_shape == 1 )
	{
		pUseSkillMonster->GetPos( aoePosX, aoePosY );
	}else
	{
		aoePosX = targetPosX; aoePosY = targetPosY; 
	}
	OnMonsterSkillGetAoeTarget( pUseSkillMonster, pSkillProp->GetAoeRange(), aoePosX, aoePosY, vecTargetPlayer, vecTargetMonster );
	
	CPlayer* pTmpPlayer = NULL;
	for( unsigned int i =0; i< vecTargetPlayer.size(); ++i )
	{
		pTmpPlayer = vecTargetPlayer[i];
		if( !pTmpPlayer )
			continue;
		
		SingleCvpWrapper( pUseSkillMonster, pTmpPlayer, pSkillProp, targetID, SCOPE_ATTACK );
	}

	CMonster* pTmpMonster = NULL;
	for( unsigned int i =0; i< vecTargetMonster.size(); ++i )
	{
		pTmpMonster = vecTargetMonster[i];
		if( !pTmpMonster )
			continue;
		
		SingleCvcWrapper( pUseSkillMonster, pTmpMonster, pSkillProp, targetID, SCOPE_ATTACK );
	}
	
	//scope end msg
	MGUseSkillRst rst;
	rst.skillRst.attackID = pUseSkillMonster->GetCreatureID();
	rst.skillRst.attackHP = pUseSkillMonster->GetHp();
	rst.skillRst.attackMP = 0;
	rst.skillRst.battleFlag = HIT;
	rst.skillRst.causeBuffID = 0;
	rst.skillRst.lCooldownTime = 0;
	rst.skillRst.lCurseTime = 0;
	rst.skillRst.lHpLeft = 0;
	rst.skillRst.lHpSubed = 0;
	rst.skillRst.nErrCode = BATTLE_SUCCESS;
	rst.skillRst.orgTargetID = targetID;
	rst.skillRst.scopeFlag = SCOPE_ATTACK_END;
	rst.skillRst.skillType = COMMON_SKILL;
	rst.skillRst.skillID = skillID;
	rst.skillRst.sequenceID = 0;
	CMuxMap* pMap = pUseSkillMonster->GetMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGUseSkillRst>( &rst, pUseSkillMonster->GetPosX(), pUseSkillMonster->GetPosY() );
	}


	return true;
}


bool CMuxMap::SingleCvcWrapper( CMonster* pUseSkillMonster, CMonster* pTargetMonster, CMuxSkillProp* pSkillProp, const PlayerID& orgTargetID, EScopeFlag scopeFlag )
{
	if( NULL == pUseSkillMonster || NULL == pTargetMonster || NULL == pSkillProp )
	{
		D_ERROR( "CMuxMap::SingleCvcWrapper，输入参数空\n" );
		return false;
	}

	if ( pTargetMonster->IsMonsterDie() )
	{
		D_ERROR( "怪物%d受到攻击时已死，忽略此次攻击\n", pTargetMonster->GetCreatureID().dwPID );
		return false;
	}

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("找不到战斗脚本\n");
		return false;
	}
	
	vector<int> vecRst;

	bool isMiss = false;
	//未命中不加buff，故这段代码搬到下面来
	unsigned int buffID = 0;
	MuxBuffProp* pBuffProp = pSkillProp->m_pBuffProp;
	unsigned int buffTime = 0;
	unsigned int assistObj = 0;
	if( NULL != pBuffProp )
	{
		if( !pScript->FnAddBuffCvc( pUseSkillMonster, pTargetMonster, pBuffProp->mID, &vecRst ) )
		{
			return false;
		}

		if( vecRst.size() != 3 )
		{
			D_ERROR("FnAddBuffCvc 返回值数量不等于3\n");
			return false;
		}

		buffID = vecRst[0];
		buffTime = vecRst[1];
		assistObj = vecRst[2];
	}

	//	if( buffID != 0 )
	//	{
	//		if( assistObj == 0 )
	//		{
	//			pUseSkillMonster->CreateMuxBuffByID( buffID, buffTime, pUseSkillMonster->GetCreatureID() );
	//		}else
	//		{
	//			pTargetMonster->CreateMuxBuffByID( buffID, buffTime,  pUseSkillMonster->GetCreatureID() );
	//		}
	//	}
	//}


	MGUseSkillRst rst;
	//关于伤害
	if( NULL != pSkillProp->m_pNpcDamageProp )
	{
		if( !pScript->FnCvcDamage( pUseSkillMonster, pTargetMonster, pSkillProp->m_skillID, &vecRst ) )
		{
			//脚本调用错误
			return false;
		}

		if( vecRst.size() != 2 )
		{
			D_ERROR("FnCvcDamage返回值数量错误\n");
			return false;
		}

		//转换返回的结果
		unsigned int nDamage = vecRst[0];
		EBattleFlag npcFlag = (EBattleFlag)vecRst[1];

		if ( (nDamage <= 0) && (npcFlag != MISS) )
		{
			D_WARNING( "SingleCvcWrapper，FnCvcDamage, 伤血为0但返回为非MISS, skillID:%d\n", pSkillProp->m_skillID );
		}

		rst.skillRst.lHpSubed = nDamage;
		rst.skillRst.battleFlag = npcFlag;
		rst.skillRst.skillType = COMMON_SKILL;
		
		pTargetMonster->SubMonsterHp( nDamage, NULL, false, pSkillProp->m_nCurseTime );
		pTargetMonster->OnBeAttackedByNpc( pUseSkillMonster, nDamage, false, pSkillProp->m_nCurseTime );
		
		//关于伤害反弹
		if( nDamage != 0 )
		{
			if( !pScript->FnCvcDamageRebound( pUseSkillMonster, pTargetMonster, nDamage, npcFlag, &vecRst ) )
			{
				return false;
			}

			if( vecRst.size() != 2) 
			{
				D_ERROR("FnCvcDamageRebound 返回伤害\n");
				return false;
			}

			unsigned int damReb = (unsigned int)vecRst[0];
			EBattleFlag drFlag = (EBattleFlag)vecRst[1];

			if ( damReb != 0 )
			{
				pUseSkillMonster->NotifyDamageRebound( pSkillProp->m_skillID, damReb, drFlag );
				pUseSkillMonster->SubMonsterHp( damReb, NULL, false, 0 );
				pUseSkillMonster->OnBeAttackedByNpc( pTargetMonster, damReb, false, 0 );
			}
		}

		if (npcFlag == MISS)
		{
			isMiss = true;
		}
	}


	//加血
	MuxItemSkillProp* pHealProp = pSkillProp->m_pMuxItemSkillProp;
	if( NULL != pHealProp )
	{
		if( !pScript->FnCvcHealNum( pUseSkillMonster, pTargetMonster, pHealProp->mID, &vecRst ) )
		{
			return false;
		}

		if( vecRst.size() != 4 )
		{
			D_ERROR("FnCvcHealNum 返回值数量错误\n");
			return false;
		}

		unsigned int healVal = vecRst[0];
		EBattleFlag healFlag = (EBattleFlag)vecRst[3];

		//加血
		pTargetMonster->AddHp( healVal );

		if ( (healVal <= 0) && (healFlag != MISS) )
		{
			D_WARNING( "SingleCvcWrapper，FnCvcHealNum, 伤血为0但返回为非MISS, mID:%d\n", pHealProp->mID );
		}

		rst.skillRst.lHpSubed = healVal;
		rst.skillRst.battleFlag = healFlag;
		rst.skillRst.skillType = ITEM_SKILL;

		if (healFlag == MISS)
		{
			isMiss = true;
		}
	}

	if ( !isMiss )
	{
		if( buffID != 0 )
		{
			if( assistObj == 0 )
			{
				pUseSkillMonster->CreateMuxBuffByID( buffID, buffTime, pUseSkillMonster->GetCreatureID() );
			}else
			{
				pTargetMonster->CreateMuxBuffByID( buffID, buffTime,  pUseSkillMonster->GetCreatureID() );
			}
		}
	}

	rst.skillRst.attackID = pUseSkillMonster->GetCreatureID();
	rst.skillRst.attackHP = pUseSkillMonster->GetHp();
	rst.skillRst.lHpLeft = pTargetMonster->GetHp();
	rst.skillRst.attackMP = 0;
	//rst.skillRst.causeBuffID = buffID;
	isMiss ? rst.skillRst.causeBuffID = 0 : rst.skillRst.causeBuffID = buffID;
	rst.skillRst.lCooldownTime = pSkillProp->m_nCoolDown;
	rst.skillRst.lCurseTime = pSkillProp->m_nCurseTime;
	rst.skillRst.nErrCode = BATTLE_SUCCESS;
	rst.skillRst.orgTargetID = orgTargetID;
	rst.skillRst.scopeFlag = scopeFlag;
	rst.skillRst.skillID = pSkillProp->m_skillID;
	rst.skillRst.targetID = pTargetMonster->GetCreatureID();
	rst.skillRst.targetRewardID = pTargetMonster->GetRewardInfo().rewardPlayerID;
	rst.skillRst.sequenceID = 0;

	CMuxMap* pMap = pTargetMonster->GetMap();
	if( NULL != pMap )
	{
		pMap->NotifySurrounding<MGUseSkillRst>( &rst, pTargetMonster->GetPosX(), pTargetMonster->GetPosY() );
	}
	return true;
}


bool CMuxMap::SingleCvpWrapper( CMonster* pUseSkillMonster, CPlayer* pTargetPlayer, CMuxSkillProp* pSkillProp, const PlayerID& orgTargetID, EScopeFlag scopeFlag )
{
	if( NULL == pUseSkillMonster || NULL == pTargetPlayer || NULL == pSkillProp )
	{
		D_ERROR( "SingleCvpWrapper，输入参数空\n" );
		return false;
	}

	if ( pTargetPlayer->IsDying() )
	{
		D_ERROR( "玩家%s已死，不再受怪物(%d)攻击\n", pTargetPlayer->GetAccount(), pUseSkillMonster->GetCreatureID().dwPID );
		return false;//目标玩家已死，不再进行攻击;
	}

	//怪物打人 初始的无敌状态
	if( pTargetPlayer->CheckInvincible(false) )
	{
		return false;
	}

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("找不到战斗脚本\n");
		return false;
	}

	vector<int> vecRst;
	bool isMiss = false;
	//未命中不加buff，故这段代码搬到下面来
	//buff 流程
	unsigned int buffID = 0;
	MuxBuffProp* pBuffProp = pSkillProp->m_pBuffProp;
	unsigned int buffTime = 0;
	unsigned int assistObj = 0;
	if( NULL != pBuffProp )
	{
		if( !pScript->FnAddBuffCvp( pUseSkillMonster, pTargetPlayer, pBuffProp->mID, &vecRst ) )
		{
			return false;
		}

		if( vecRst.size() != 3 )
		{
			D_ERROR("FnAddBuffCvp 返回值错误\n");
			return false;
		}

		buffID = vecRst[0];
		buffTime = vecRst[1];
		assistObj = vecRst[2];
	}

	//	if( buffID != 0 )
	//	{
	//		if( assistObj == 0 )
	//		{
	//			pUseSkillMonster->CreateMuxBuffByID( buffID, buffTime, pUseSkillMonster->GetCreatureID() );
	//		}else{
	//			pTargetPlayer->CreateMuxBuffByID( buffID, buffTime, pUseSkillMonster->GetCreatureID() );
	//		}
	//	}
	//}

	MGUseSkillRst rst;
	
	//关于伤害
	if( NULL != pSkillProp->m_pNpcDamageProp )
	{
		if( !pScript->FnCvpDamage( pUseSkillMonster, pTargetPlayer,  pSkillProp->m_skillID, &vecRst ) )
		{
			//脚本调用错误
			return false;
		}

		if( vecRst.size() != 2 )
		{
			D_ERROR("FnCvpDamage 返回值错误\n");
			return false;
		}

		unsigned int npcDamage = vecRst[0];
		EBattleFlag batFlg = (EBattleFlag)vecRst[1];

		if (batFlg == MISS)
		{
			isMiss = true;
		}
		
		//人物减血
		pTargetPlayer->SubPlayerHp( npcDamage, NULL, pUseSkillMonster->GetCreatureID() );
		//RankEventManagerSingle::instance()->MontiorPlayerBeDemage( pTargetPlayer, npcDamage  );		//排行榜增加
		//被伤害排行去除，RankEventManager::MontiorPlayerBeDemage( pTargetPlayer, npcDamage  );		//排行榜增加

		pTargetPlayer->GetSelfStateManager().OnPlayerBeAttack();
		pTargetPlayer->BeInjuredAffectWear();
	   
		rst.skillRst.lHpSubed = npcDamage;
		rst.skillRst.skillType = COMMON_SKILL;
		rst.skillRst.battleFlag = batFlg;

		if ( (npcDamage <= 0) && (batFlg != MISS) )
		{
			D_WARNING( "SingleCvpWrapper，FnCvpDamage, 伤血为0但返回为非MISS, skillID:%d\n", pSkillProp->m_skillID );
		}

		//AI
		CMonster* pFollowingMonster = pTargetPlayer->GetFollowingMonster();
		if( NULL != pFollowingMonster )
		{
			pFollowingMonster->OnFollowPlayerBeAttack( pUseSkillMonster->GetCreatureID(), npcDamage );
		}
		
		//关于伤害反弹
		if( npcDamage != 0 )
		{
			if( !pScript->FnCvpDamageRebound( pUseSkillMonster, pTargetPlayer, npcDamage, batFlg, &vecRst ))
			{
				return false;
			}

			if( vecRst.size() != 2 )
			{
				D_ERROR("FnCvpDamageRebound返回值错误\n");
				return false;
			}

			unsigned int damReb = vecRst[0];
			EBattleFlag damFlag = (EBattleFlag)vecRst[1];
			
			if( damReb != 0 )
			{
				pUseSkillMonster->NotifyDamageRebound( pSkillProp->m_skillID, damReb, damFlag );
				pUseSkillMonster->SubMonsterHp( damReb, pTargetPlayer, false, 0 );
				//RankEventManagerSingle::instance()->MonitorPlayerDemage( pTargetPlayer, damReb );
				RankEventManager::MonitorPlayerDamage( pTargetPlayer, damReb );
			}
		}
	}


	//关于加血
	MuxItemSkillProp* pHealProp = pSkillProp->m_pMuxItemSkillProp;
	if( NULL != pHealProp )
	{
		if( !pScript->FnCvpHealNum( pUseSkillMonster, pTargetPlayer, pHealProp->mID,  &vecRst ) )
		{
			return false;
		}

		if( vecRst.size() != 4 )
		{
			D_ERROR("FnCvpHealNum 返回值数量错误\n");
			return false;
		}

		unsigned int healVal = vecRst[0];
		unsigned int mp = vecRst[1];
		unsigned int spBean = vecRst[2];
		EBattleFlag healFlag = (EBattleFlag)vecRst[3];

		if ( (healVal <= 0) && (healFlag != MISS) )
		{
			D_WARNING( "SingleCvpWrapper，FnCvpHealNum, 伤血为0但返回为非MISS, mID:%d\n", pHealProp->mID );
		}
		
		if( healVal != 0 )
		{
			pTargetPlayer->AddPlayerHp( healVal );
		}

		if( mp )
		{
			pTargetPlayer->AddPlayerMp( mp );
		}

		if( spBean )
		{
			pTargetPlayer->AddSpBean( spBean );
		}

		rst.skillRst.lHpSubed = healVal;
		rst.skillRst.skillType = ITEM_SKILL;
		rst.skillRst.battleFlag = healFlag;

		if (healFlag == MISS)
		{
			isMiss = true;
		}
	}

	if ( !isMiss )
	{
		if( buffID != 0 )
		{
			if( assistObj == 0 )
			{
				pUseSkillMonster->CreateMuxBuffByID( buffID, buffTime, pUseSkillMonster->GetCreatureID() );
			}else{
				pTargetPlayer->CreateMuxBuffByID( buffID, buffTime, pUseSkillMonster->GetCreatureID() );
			}
		}
	}

	rst.skillRst.attackID = pUseSkillMonster->GetCreatureID();
	rst.skillRst.attackHP = pUseSkillMonster->GetHp();
	rst.skillRst.attackMP = 0;
	rst.skillRst.lHpLeft = pTargetPlayer->GetCurrentHP();
	//rst.skillRst.causeBuffID = buffID;
	isMiss ? rst.skillRst.causeBuffID = 0 : rst.skillRst.causeBuffID = buffID;
	rst.skillRst.lCooldownTime = pSkillProp->m_nCoolDown;
	rst.skillRst.lCurseTime = pSkillProp->m_nCurseTime;
	rst.skillRst.nErrCode = BATTLE_SUCCESS;
	rst.skillRst.orgTargetID = orgTargetID;
	rst.skillRst.scopeFlag = scopeFlag;
	rst.skillRst.skillID = pSkillProp->m_skillID;
	rst.skillRst.skillType = COMMON_SKILL;
	rst.skillRst.targetID = pTargetPlayer->GetFullID();
	rst.skillRst.targetRewardID.dwPID = 0;
	rst.skillRst.targetRewardID.wGID = 0;
	rst.skillRst.sequenceID = 0;
	
	CMuxMap* pMap = pTargetPlayer->GetCurMap();
	if( NULL != pMap )
	{
		pMap->NotifySurrounding<MGUseSkillRst>( &rst, pTargetPlayer->GetPosX(), pTargetPlayer->GetPosY() );
	}

	//零散
	if( !pSkillProp->IsGoodSkill() )
	{
		pTargetPlayer->GetSelfStateManager().OnPlayerBeAttack();
	}
	return true;
}


bool CMuxMap::SingleItemPvcWrapper( CPlayer* pUseSkillPlayer, CMonster* pTargetMonster, CMuxSkillProp* pSkillProp, const PlayerID& orgTargetID, EScopeFlag scopeFlag )
{
	if( NULL == pUseSkillPlayer || NULL == pTargetMonster || NULL == pSkillProp )
		return false;

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("找不到战斗脚本\n");
		return false;
	}

	vector<int> vecRst;
	//Buff
	MuxBuffProp* pBuffProp = pSkillProp->m_pBuffProp;
	if( NULL != pBuffProp )
	{
		if( !pScript->FnAddBuffPvc( pUseSkillPlayer, pTargetMonster, pBuffProp->mID, &vecRst ))
		{
			return false;
		}

		if( vecRst.size() != 3 )
		{
			return false;
		}

		unsigned int buffID = vecRst[0];
		unsigned int buffTime = vecRst[1];
		
		if( buffID != 0 )
		{
			pTargetMonster->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() );
		}
	}

	MuxNpcDamageProp* pDamageProp = pSkillProp->m_pNpcDamageProp;
	if( NULL != pDamageProp )
	{
		if( !pScript->FnItemDamagePvc( pUseSkillPlayer, pTargetMonster, pDamageProp->mID, &vecRst ) )
		{
			return false;
		}

		if( vecRst.size() != 2 )
		{
			return false;
		}

		unsigned int damage = vecRst[0];
		EBattleFlag batFlag = (EBattleFlag)vecRst[1];

		pTargetMonster->SubMonsterHp( damage, pUseSkillPlayer, false, pSkillProp->m_nCurseTime );
		//RankEventManagerSingle::instance()->MonitorPlayerDemage( pUseSkillPlayer, damage );
		RankEventManager::MonitorPlayerDamage( pUseSkillPlayer, damage );

		if ( (damage <= 0) && (batFlag != MISS) )
		{
			D_WARNING( "SingleItemPvcWrapper，FnItemDamagePvc, 伤血为0但返回为非MISS, skillID:%d\n", pDamageProp->mID );
		}

		MGUseSkillRst rst;
		rst.skillRst.attackHP = pUseSkillPlayer->GetCurrentHP();
		rst.skillRst.attackID = pUseSkillPlayer->GetFullID();
		rst.skillRst.attackMP = pUseSkillPlayer->GetCurrentMP();
		rst.skillRst.causeBuffID = 0;
		rst.skillRst.battleFlag = batFlag;
		rst.skillRst.lCooldownTime = pSkillProp->m_nCoolDown;
		rst.skillRst.lCurseTime = pSkillProp->m_nCurseTime;
		rst.skillRst.lHpLeft = pTargetMonster->GetHp();
		rst.skillRst.lHpSubed = damage;
		rst.skillRst.nErrCode = BATTLE_SUCCESS;
		rst.skillRst.orgTargetID = orgTargetID;
		rst.skillRst.scopeFlag = scopeFlag;
		rst.skillRst.skillID = pSkillProp->m_skillID;
		rst.skillRst.skillType = COMMON_SKILL;
		rst.skillRst.targetID = pTargetMonster->GetCreatureID();
		rst.skillRst.targetRewardID = pTargetMonster->GetRewardInfo().rewardPlayerID;
		CMuxMap* pMap = pUseSkillPlayer->GetCurMap();
		if( pMap )
		{
			pMap->NotifySurrounding<MGUseSkillRst>( &rst, pTargetMonster->GetPosX(), pTargetMonster->GetPosY() );
		}
	}
	return true;
}


bool CMuxMap::SingleItemPvpWrapper( CPlayer* pUseSkillPlayer, CPlayer* pTargetPlayer, CMuxSkillProp* pSkillProp, const PlayerID& orgTargetID, EScopeFlag scopeFlag )
{
	if( NULL == pUseSkillPlayer || NULL == pTargetPlayer || NULL == pSkillProp )
		return false;

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("找不到战斗脚本\n");
		return false;
	}

	bool isTargetRogue = pTargetPlayer->IsRogueState();
	//buff
	vector<int> vecRst;
	MuxBuffProp* pBuffProp = pSkillProp->m_pBuffProp;
	if( NULL != pBuffProp )
	{
		if( !pScript->FnAddBuffPvp( pUseSkillPlayer, pTargetPlayer, pBuffProp->mID, &vecRst ))
		{
			return false;
		}

		if( vecRst.size() != 3 )
		{
			return false;
		}

		unsigned int buffID = vecRst[0];
		unsigned int buffTime = vecRst[1];
		
		pTargetPlayer->CreateMuxBuffByID( buffID, buffTime, pUseSkillPlayer->GetFullID() );
	}

	//伤害
	MuxNpcDamageProp* pDamageProp = pSkillProp->m_pNpcDamageProp;
	if( NULL != pDamageProp )
	{
		if( !pScript->FnItemDamagePvp( pUseSkillPlayer, pTargetPlayer, pDamageProp->mID, &vecRst ) )
		{
			return false;
		}

		if( vecRst.size() != 2 )
		{
			return false;
		}

		unsigned int damage = vecRst[0];
		EBattleFlag batFlag = (EBattleFlag)vecRst[1];

		pTargetPlayer->SubPlayerHp( damage, pUseSkillPlayer, pUseSkillPlayer->GetFullID() );
		//RankEventManagerSingle::instance()->MonitorPlayerDemage( pUseSkillPlayer, damage  );
		RankEventManager::MonitorPlayerDamage( pUseSkillPlayer, damage  );
		//RankEventManagerSingle::instance()->MontiorPlayerBeDemage( pTargetPlayer, damage  );
		//被伤害排行去除，RankEventManager::MontiorPlayerBeDemage( pTargetPlayer, damage  );

		if ( (damage <= 0) && (batFlag != MISS) )
		{
			D_WARNING( "SingleItemPvpWrapper，FnItemDamagePvp, 伤血为0但返回为非MISS, skillID:%d\n", pDamageProp->mID );
		}

		MGUseSkillRst rst;
		rst.skillRst.attackHP = pUseSkillPlayer->GetCurrentHP();
		rst.skillRst.attackID = pUseSkillPlayer->GetFullID();
		rst.skillRst.attackMP = pUseSkillPlayer->GetCurrentMP();
		rst.skillRst.causeBuffID = 0;
		rst.skillRst.battleFlag = batFlag;
		rst.skillRst.lCooldownTime = pSkillProp->m_nCoolDown;
		rst.skillRst.lCurseTime = pSkillProp->m_nCurseTime;
		rst.skillRst.lHpLeft = pTargetPlayer->GetCurrentHP();
		rst.skillRst.lHpSubed = damage;
		rst.skillRst.nErrCode = BATTLE_SUCCESS;
		rst.skillRst.orgTargetID = orgTargetID;
		rst.skillRst.scopeFlag = scopeFlag;
		rst.skillRst.skillID = pSkillProp->m_skillID;
		rst.skillRst.skillType = COMMON_SKILL;
		rst.skillRst.targetID = pTargetPlayer->GetFullID();
		CMuxMap* pMap = pUseSkillPlayer->GetCurMap();
		if( pMap )
		{
			pMap->NotifySurrounding<MGUseSkillRst>( &rst, pTargetPlayer->GetPosX(), pTargetPlayer->GetPosY() );
		}
	}

	//零散,流氓状态,下马
	if( !pSkillProp->IsGoodSkill() ) 
	{
		pUseSkillPlayer->FightStartCheckRogueState( pTargetPlayer, isTargetRogue );
		pTargetPlayer->GetSelfStateManager().OnPlayerBeAttack();
	}
	return true;
}



bool CMuxMap::OnPlayerSkillGetAoeTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster ) 
{
	if( IsInsCopyMap() )
	{
		if( m_pCopyMapInfo )
		{
			return m_pCopyMapInfo->OnPlayerSkillGetAoeTarget( pUseSkillPlayer, range, aoePosX, aoePosY, vecTargetPlayer, vecMonster );
		}
	}else{
		if( m_pNormalMapInfo )
		{
			m_pNormalMapInfo->OnPlayerSkillGetAoeTarget( pUseSkillPlayer, range, aoePosX, aoePosY, vecTargetPlayer, vecMonster );
		}
	}
	return true;
}


bool CMuxMap::OnPlayerSkillGetTeamTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer )
{
	if ( NULL == pUseSkillPlayer )
	{
		D_ERROR( "CMuxMap::OnPlayerSkillGetTeamTarget, NULL == pUseSkillPlayer\n" );
		return false;
	}
	if( IsInsCopyMap() )
	{
		if( m_pCopyMapInfo )
		{
			return m_pCopyMapInfo->OnPlayerSkillGetTeamTarget( pUseSkillPlayer, range, aoePosX, aoePosY, vecTargetPlayer );
		}
	}else{
		if( m_pNormalMapInfo )
		{
			m_pNormalMapInfo->OnPlayerSkillGetTeamTarget( pUseSkillPlayer, range, aoePosX, aoePosY, vecTargetPlayer );
		}
	}
	return true;
}


bool CMuxMap::OnMonsterSkillGetAoeTarget( CMonster* pUSeSkillMonster, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster ) 
{
	if( IsInsCopyMap() )
	{
		if( m_pCopyMapInfo )
		{
			return m_pCopyMapInfo->OnMonsterSkillGetAoeTarget( pUSeSkillMonster, range, aoePosX, aoePosY, vecTargetPlayer, vecMonster );
		}
	}else{
		if( m_pNormalMapInfo )
		{
			m_pNormalMapInfo->OnMonsterSkillGetAoeTarget( pUSeSkillMonster, range, aoePosX, aoePosY, vecTargetPlayer, vecMonster );
		}
	}
	return true;
}

#ifdef HAS_PATH_VERIFIER
void CMuxMap::SetPathVerifier(void)
{
	if(NULL != m_pMapProperty)
	{
		unsigned int** backupedBaseInfo = NULL;
		unsigned int** accessRegionInfo = NULL;
		m_pMapProperty->GetBackupAndAccessRegionInfo(backupedBaseInfo, accessRegionInfo);
		if((NULL != backupedBaseInfo) && (NULL != accessRegionInfo))
		{	
			int iMapID = (int)m_pMapProperty->GetPropertyMapNo();
			int gridWidth = m_pMapProperty->GetGridWidth(); 
			int gridHeight = m_pMapProperty->GetGridHeight(); 
			//创建路径校验对象
			m_pPathVerifier = NEW CAccessRegionPathVerifier(iMapID, gridWidth, gridHeight, backupedBaseInfo, accessRegionInfo);

			//初始化碰撞信息
			SetBlockQuardInfo();
		}
	}
}

void CMuxMap::SetBlockQuardInfo(void)
{
	if(NULL != m_pPathVerifier)
	{
		if(NULL != m_pBlockQuards)
		{
			delete []m_pBlockQuards;
			m_pBlockQuards = NULL;
		}

		std::vector<CBlockQuadManager::BQData> bqVec;
		CBlockQuadManager::GetBlockQuardByMapId((unsigned short)m_pMapProperty->GetPropertyMapNo(), bqVec);

		m_blockQuadNum  = (int)bqVec.size();
		if(m_blockQuadNum > 0)
		{
			if ( m_blockQuadNum > 15 )//修正为最大值
			{
				m_blockQuadNum = 15;
			}

			m_pBlockQuards = NEW CPathVerifier::CBlockQuad[m_blockQuadNum];
			if(NULL != m_pBlockQuards)
			{
				for(int i = 0; i < m_blockQuadNum ; i++)	
				{
					if ( (size_t)i >= bqVec.size() )
					{
						D_ERROR( "SetBlockQuardInfo, i(%d)越界，bqVec's size(%d)\n", i, bqVec.size() );
						break;
					}
					m_pBlockQuards[i].Set(bqVec[i].regionId, bqVec[i].minX, bqVec[i].minY, bqVec[i].maxX, bqVec[i].maxY);
				}

				m_pPathVerifier->SetBlockQuads(m_blockQuadNum, m_pBlockQuards);
			}
		}
	}

	return;
}

void CMuxMap::GetEnableBlockQuads(std::vector<int> &blockQuads)
{
	if ( NULL == m_pBlockQuards )
	{
		D_ERROR( "GetEnableBlockQuads, NULL == m_pBlockQuards, 地图号:%d\n", GetMapNo() );
		return;
	}
	for(int i = 0; i < m_blockQuadNum; i++)
	{
		if(m_pBlockQuards[i].Enable())
		{
			blockQuads.push_back(m_pBlockQuards[i].GetID());
		}
	}

	return;
}

void CMuxMap::SetBlockQuard(int regionId, bool val)
{
	if(NULL != m_pPathVerifier)
	{	
		m_pPathVerifier->SetBlockQuadEnable(regionId, val);

		MGBlockQuadStateChange msg;
		msg.stateChange.regionID = regionId;
		msg.stateChange.val = val;
		NotifyAllUseBrocast<MGBlockQuadStateChange>(&msg);

		if(val)//碰撞有效使该块的玩家死亡
		{
			OnBlockQuadEnable(regionId);
		}
	}
}

bool CMuxMap::IsPassOK( float sPosX, float sPosY, float tPosX, float tPosY ) 
{
	if(NULL != m_pPathVerifier)
	{
		unsigned short startGridX=0, startGridY=0;
		unsigned short targetGridX=0, targetGridY=0;
		MuxMapSpace::FloatToGridPos( sPosX, sPosY, startGridX, startGridY );
		MuxMapSpace::FloatToGridPos( tPosX, tPosY, targetGridX, targetGridY );
		if ( !IsGridCanPass( targetGridX, targetGridY ) )
		{
			//终点为障碍点，判为不可通行;
			//  之所以加这一条，是因为怪物行走使用了较粗略的同区域判定，而同一区域中可能会有小块的障碍区
			//  有这一条后，虽然怪物仍可能经过障碍区，但永远不可能停在障碍区内，这样造成的错误后果
			return false;
		}
		if ( !IsGridCanPass( startGridX, startGridY ) )
		{
			//起点为障碍点，终点为非障碍点，判为可通行；
			//  本情形只可能出现在怪物身上，因为玩家不可能进入障碍点
			//  而对于怪物，主要用于使其在特殊情形下走出障碍(例如被击退至障碍物内，或者在经过障碍时因某种原因停在障碍内)，
			//  由于怪物行走在服务器端由AI控制，因此暂时来看没什么逻辑漏洞，不会被玩家利用于穿墙，由于怪物初始的行走路径受限于区域，因此怪物也不可能穿墙。
			return true;
		}
		return m_pPathVerifier->VerifiyWalkable((int)startGridX, (int)startGridY, (int)targetGridX, (int)targetGridY);
	}

	return true;
}

inline bool CMuxMap::BlockTestOK( float sPosX, float sPosY, float tPosX, float tPosY )
{
	if(NULL != m_pPathVerifier)
	{
		unsigned short startGridX=0, startGridY=0;
		unsigned short targetGridX=0, targetGridY=0;
		MuxMapSpace::FloatToGridPos( sPosX, sPosY, startGridX, startGridY );
		MuxMapSpace::FloatToGridPos( tPosX, tPosY, targetGridX, targetGridY );
		return m_pPathVerifier->BlockQuadTest((int)startGridX, (int)startGridY, (int)targetGridX, (int)targetGridY);
	}

	return true;
}

void CMuxMap::OnBlockQuadEnable(int regionId)
{
	if ( IsInsCopyMap() )//副本
	{
		if ( NULL == m_pCopyMapInfo )
		{
			D_ERROR( "CMuxMap::OnBlockQuadEnable, NULL == m_pCopyMapInfo\n" );
			return ;
		}

		m_pCopyMapInfo->OnBlockQuadEnable(regionId);
	} 
	else//普通地图 
	{
		if ( NULL == m_pNormalMapInfo )
		{
			D_ERROR( "CMuxMap::OnBlockQuadEnable, NULL == m_pNormalMapInfo\n" );
			return ;
		}

		m_pNormalMapInfo->OnBlockQuadEnable(regionId);
	}

	return;
}
#endif/*HAS_PATH_VERIFIER*/

