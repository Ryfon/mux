﻿/**
* @file muxcopy.h
* @brief 副本类
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: muxcopy.h
* 摘    要: 副本类
* 作    者: dzj
* 开始日期: 2009.09.14
* 
*/

#ifndef MUX_COPY_H
#define MUX_COPY_H

#include "mapproperty.h"
#include "MapBlock.h"
#include "../../../Base/dscontainer/dshashtb.h"

class CPlayer;
class CMonster;
class CMuxCopyInfo;

static const unsigned short COPY_GRID_WIDTH = 256;//副本地图宽(以格计)；
static const unsigned short COPY_GRID_HEIGHT = 256;//副本地图高(以格计)；
static const unsigned short COPY_BLOCK_WIDTH   = COPY_GRID_WIDTH / MAPBLOCK_SIZE + 1; //副本地图宽(以块计)；
static const unsigned short COPY_BLOCK_HEIGHT  = COPY_GRID_HEIGHT / MAPBLOCK_SIZE + 1; //副本地图高(以块计)；
static const unsigned int   COPY_PLAYER_MAXNUM = 7; //副本最大人数；

///活动块坐标信息；
struct ActiveBlockInfo
{
	unsigned short blockX;
	unsigned short blockY;
};

//注释已被替换活动块算法，by dzj, 10.06.21;///副本地图活动块信息；
//class CopyActiveBlocks
//{
//	static const unsigned int ACTIVE_FLAG_ROWS = COPY_BLOCK_HEIGHT;//活动标记行数；
//	static const unsigned int ACTIVE_FLAG_COLS = COPY_BLOCK_WIDTH;//活动标记列数；
//	static const unsigned int MAX_ACTIVE_NUM = COPY_PLAYER_MAXNUM/*人数*/*9/*每个人的视野范围*/;//可能的最大活动块数----所有人的视野都无阻碍且无相互交叉时；	
//
//public:
//	CopyActiveBlocks()
//	{
//		InitCopyActiveBlocks();
//	}
//
//	///清所有活动块标记；
//	void InitCopyActiveBlocks()
//	{
//		StructMemSet( m_activeFlags, 0, sizeof(m_activeFlags) );
//		m_activeBlockNum = 0;
//		StructMemSet( m_activeBlockInfos, 0, sizeof(m_activeBlockInfos) );
//	}
//
//public:
//	///取活动块信息；
//	inline bool GetActiveBlockInfo( unsigned int& activeBlockNum/*活动块数目*/, ActiveBlockInfo*& activeBlockInfos/*返回的活动块信息数组*/ )
//	{
//		activeBlockNum = m_activeBlockNum;
//		activeBlockInfos = &(m_activeBlockInfos[0]);
//		return true;
//	}
//
//	///设置当前的各活动中心块坐标(副本内玩家的坐标);
//	inline bool SetActiveCenterBlocks( unsigned int activeCenterNum, const ActiveBlockInfo activeCenterBlockInfo[COPY_PLAYER_MAXNUM] )
//	{
//		InitCopyActiveBlocks();
//
//		if ( ( activeCenterNum <= 0 )
//			|| ( NULL == activeCenterBlockInfo )
//			)
//		{
//			return false;
//		}
//
//		for ( unsigned int i=0; i<activeCenterNum; ++i )
//		{
//			if ( i >= ARRAY_SIZE(activeCenterBlockInfo) )
//			{
//				D_ERROR( "CopyActiveBlocks::SetActiveCenterBlocks, i(%d)越界, activeCenterNum:%d\n", i, activeCenterNum );
//				return false;
//			}
//			SetOneActiveCenterBlocks( activeCenterBlockInfo[i].blockX, activeCenterBlockInfo[i].blockY );
//		}
//
//		//生成m_activeBlockInfos;
//		GenActiveBlockInfos();
//		
//		return true;
//	}
//
//	///根据某个中心块位置，将其周围范围内块都置为活动；
//    inline void SetOneActiveCenterBlocks( unsigned short blockX, unsigned short blockY )
//	{
//		if ( ( blockX > ACTIVE_FLAG_ROWS-1 ) 
//			 || ( blockY > ACTIVE_FLAG_COLS-1 )
//			)
//		{
//			//无效的输入块坐标；
//			return;
//		}
//
//		unsigned short leftX=blockX, upY=blockY, rightX=blockX, downY=blockY;
//		if ( blockX > 0 ) {	--leftX; }
//		if ( blockY > 0 ) {	--upY; }
//		if ( blockX < ACTIVE_FLAG_COLS-1 ) { ++rightX; }
//		if ( blockY < ACTIVE_FLAG_ROWS-1 ) { ++downY; }
//		for ( unsigned short y=upY; y<=downY; ++y )
//		{
//			for ( unsigned short x=leftX; x<=rightX; ++x )
//			{
//				m_activeFlags[y] |= (0x01<<x);
//			}
//		}
//		return;
//	}
//
//	inline void GenActiveBlockInfos()
//	{
//		m_activeBlockNum = 0;
//		unsigned short posY = 0;
//		unsigned short posX = 0;
//
//		for ( unsigned short row=0; row<ACTIVE_FLAG_ROWS; ++row )
//		{
//			posY = row;
//			if ( row >= ARRAY_SIZE(m_activeFlags) )
//			{
//				D_ERROR( "GenActiveBlockInfos, row(%d)越界\n", row );
//				return;
//			}
//			I32 tmprow = m_activeFlags[row];//本行所有已置标记；
//			if ( 0 != tmprow )//本行有活动块；
//			{
//				do {
//					//取本行活动块;
//					I32 tmpPos = ( tmprow & (~(tmprow-1)) );
//					if ( 0 == tmpPos )
//					{
//						break;
//					}
//					posX = GetActivePos( tmpPos );
//					if ( (posX>=ACTIVE_FLAG_COLS) )
//					{
//						break;
//					}
//					tmprow &= (~(0x00000001 << posX));
//					if ( m_activeBlockNum >= ARRAY_SIZE(m_activeBlockInfos) )
//					{
//						D_ERROR( "活动块数超过m_activeBlockInfos数组大小\n" );
//						break;
//					}
//					m_activeBlockInfos[m_activeBlockNum].blockX = posX;
//					m_activeBlockInfos[m_activeBlockNum].blockY = posY;
//					++m_activeBlockNum;					
//				}while ( 0 != tmprow );//本行还有活动块；
//			}
//		}
//		
//		return;
//	}
//
//private:
//	///取输入数置位，例:0x00000001返回0(第0位被置位)
//	inline unsigned short GetActivePos( unsigned int inVal )
//	{
//		switch ( inVal )
//		{
//		case (0x00000001<<0):
//			return 0;
//			break;
//		case (0x00000001<<1):
//			return 1;
//			break;
//		case (0x00000001<<2):
//			return 2;
//			break;
//		case (0x00000001<<3):
//			return 3;
//			break;
//		case (0x00000001<<4):
//			return 4;
//			break;
//		case (0x00000001<<5):
//			return 5;
//			break;
//		case (0x00000001<<6):
//			return 6;
//			break;
//		case (0x00000001<<7):
//			return 7;
//			break;
//		case (0x00000001<<8):
//			return 8;
//			break;
//		case (0x00000001<<9):
//			return 9;
//			break;
//		case (0x00000001<<10):
//			return 10;
//			break;
//		case (0x00000001<<11):
//			return 11;
//			break;
//		case (0x00000001<<12):
//			return 12;
//			break;
//		case (0x00000001<<13):
//			return 13;
//			break;
//		case (0x00000001<<14):
//			return 14;
//			break;
//		case (0x00000001<<15):
//			return 15;
//			break;
//		case (0x00000001<<16):
//			return 16;
//			break;
//		case (0x00000001<<17):
//			return 17;
//			break;
//		case (0x00000001<<18):
//			return 18;
//			break;
//		case (0x00000001<<19):
//			return 19;
//			break;
//		case (0x00000001<<20):
//			return 20;
//			break;
//		case (0x00000001<<21):
//			return 21;
//			break;
//		case (0x00000001<<22):
//			return 22;
//			break;
//		case (0x00000001<<23):
//			return 23;
//			break;
//		case (0x00000001<<24):
//			return 24;
//			break;
//		case (0x00000001<<25):
//			return 25;
//			break;
//		case (0x00000001<<26):
//			return 26;
//			break;
//		default:
//			{
//				D_ERROR( "CopyActiveBlocks::GetActivePos，输入值%x错误\n", inVal );
//				return 0xffff;
//			}
//		}
//	}
//
//private:
//	I32 m_activeFlags[ACTIVE_FLAG_ROWS];//行列形式的活动块标记；
//
//	unsigned int m_activeBlockNum;//m_activeBlockInfos中的有效信息数目;
//	ActiveBlockInfo m_activeBlockInfos[MAX_ACTIVE_NUM];//数组形式的当前各活动块的块坐标；
//};//class CopyActiveBlocks

////此工作已改至center,///曾进过本副本的玩家信息；
//class CopyPlayerInfos
//{
//	struct CopyPlayerEle
//	{
//	private:
//		CopyPlayerEle& operator = ( const CopyPlayerEle& );//屏蔽这个操作，因为有string成员；
//
//	public:
//		CopyPlayerEle() { InitCopyPlayerEle(); };
//		void InitCopyPlayerEle()
//		{
//			playerID.wGID = 0;
//			playerID.dwPID = 0;
//			scrollType = 1;//默认需要扣副本券
//			//playerRoleName = "";
//		}
//
//		void SetCopyPlayerEleVal( const PlayerID& inPlayerID, unsigned int inScrollType/*, const char* inRoleName*/ )
//		{
//			playerID = inPlayerID;
//			scrollType = inScrollType;
//			//playerRoleName = inRoleName;
//		}
//
//		PlayerID     playerID;
//		unsigned int scrollType;//玩家使用的副本券信息；
//		//string       playerRoleName;//玩家昵称,由于使用次数较少，因此直接使用string，而不使用const char*；
//	};
//
//public:
//	CopyPlayerInfos() { InitCopyPlayerInfos(); }
//
//	inline void InitCopyPlayerInfos() 
//	{
//		playerInfoNum = 0;
//		for ( int i=0; i<ARRAY_SIZE(playerInfos); ++i )
//		{
//			playerInfos[i].InitCopyPlayerEle();
//		}
//		return;
//	}
//
//public:
//OnPlayerLeaveTeamDelInfo	///玩家离开组队删去该玩家之前保存的进副本信息；
//	bool OnPlayerLeaveTeamDelInfo( const PlayerID& playerID )
//	{
//		if ( playerInfoNum <=  0 )
//		{
//			return false;
//		}
//
//		bool isFound = false;
//		unsigned int  foundPos = 0;
//		for ( unsigned int i=0; i<playerInfoNum; ++i )
//		{
//			if ( playerID == playerInfos[i].playerID )
//			{
//				isFound = true;
//				foundPos = i;
//				break;
//			}
//		}
//
//		if ( !isFound )
//		{
//			return false;
//		}
//
//		//已找到该玩家的初始进入信息，删去；
//		playerInfos[foundPos].SetCopyPlayerEleVal( playerInfos[playerInfoNum-1].playerID
//			, playerInfos[playerInfoNum-1].scrollType/*, playerInfos[playerInfoNum-1].playerRoleName.c_str()*/ );
//		--playerInfoNum;
//
//		return true;
//	}
//
//	//玩家进入副本时的检测(之前是否进过此副本，如果进过，是用何种卷轴进入的等等) 
//	bool PlayerEnterCheck( const PlayerID& playerID/*, const char* roleName*/, unsigned int& scrollType/*in_out scrollType*/, bool& isOrgEnterThis/*之前是否进过此副本*/ )
//	{
//		bool isFound = false;
//		unsigned int  foundPos = 0;
//		for ( unsigned int i=0; i<playerInfoNum; ++i )
//		{
//			if ( playerID == playerInfos[i].playerID )
//			{
//				isFound = true;
//				foundPos = i;
//				break;
//			}
//		}
//
//		if ( isFound )
//		{
//			//该玩家原来进过此副本；
//			isOrgEnterThis = true;
//			scrollType = playerInfos[foundPos].scrollType;
//		} else {
//			//该玩家原来未曾进过此副本；
//			isOrgEnterThis = false;
//			if ( playerInfoNum >= ARRAY_SIZE( playerInfos ) )
//			{
//				//超过预定数量的玩家曾进入单一副本；
//				D_ERROR( "超过预定数量的玩家曾进入单一副本\n" );
//				return false;
//			}
//			playerInfos[playerInfoNum].playerID = playerID;
//			playerInfos[playerInfoNum].scrollType = scrollType;
//			//playerInfos[playerInfoNum].playerRoleName = roleName;
//			++playerInfoNum;
//		}
//
//		return true;
//	}
//
//private:
//	unsigned int  playerInfoNum;//当前有效的曾进过本副本的玩家信息；
//	CopyPlayerEle playerInfos[COPY_PLAYER_MAXNUM];
//};


///副本地图活动块信息；
class NewCopyActiveBlocks
{
	static const unsigned int ACTIVE_FLAG_ROWS = COPY_BLOCK_HEIGHT;//活动标记行数；
	static const unsigned int ACTIVE_FLAG_COLS = COPY_BLOCK_WIDTH;//活动标记列数；
	static const unsigned int MAX_ACTIVE_NUM = COPY_PLAYER_MAXNUM/*人数*/*9/*每个人的视野范围*/;//可能的最大活动块数----所有人的视野都无阻碍且无相互交叉时；	

public:
	NewCopyActiveBlocks()
	{
		InitCopyActiveBlocks();
	}

	///清所有活动块标记；
	void InitCopyActiveBlocks()
	{
		StructMemSet( m_activeBlockFactors, 0, sizeof(m_activeBlockFactors) );
		m_activeBlockNum = 0;
		StructMemSet( m_activeBlockInfos, 0, sizeof(m_activeBlockInfos) );
	}

public:
	///取活动块信息；
	inline bool GetActiveBlockInfo( unsigned int& activeBlockNum/*活动块数目*/, ActiveBlockInfo*& activeBlockInfos/*返回的活动块信息数组*/ )
	{
		if ( m_activeBlockNum >= ARRAY_SIZE(m_activeBlockInfos) )
		{
			D_ERROR( "GetActiveBlockInfo, m_activeBlockNum(%d)越界\n", m_activeBlockNum );
			return false;
		}
		activeBlockNum = m_activeBlockNum;
		activeBlockInfos = &(m_activeBlockInfos[0]);
		return true;
	}

public:
	///新的活动中心块加入；
	inline bool NewActiveCenterBlocksAdd( unsigned short blockX, unsigned short blockY )
	{
		if ( ( blockX > ACTIVE_FLAG_ROWS-1 ) 
			|| ( blockY > ACTIVE_FLAG_COLS-1 )
			)
		{
			//无效的输入块坐标；
			return false;
		}

		unsigned short leftX=blockX, upY=blockY, rightX=blockX, downY=blockY;
		if ( blockX > 0 ) {	--leftX; }
		if ( blockY > 0 ) {	--upY; }
		if ( blockX < ACTIVE_FLAG_COLS-1 ) { ++rightX; }
		if ( blockY < ACTIVE_FLAG_ROWS-1 ) { ++downY; }
		for ( unsigned short y=upY; y<=downY; ++y )
		{
			for ( unsigned short x=leftX; x<=rightX; ++x )
			{
				bool isOrgSet = m_activeBlockFactors[y][x] > 0;
				++m_activeBlockFactors[y][x];
				if ( !isOrgSet )
				{
					AddOneActiveBlockInfo( x, y );//过去非活动，现在活动，新活动块，添加活动块信息;				
				}
			}
		}

		return true;
	}

	///旧的活动中心块删去；
	inline bool OrgActiveCenterBlocksDel( unsigned short blockX, unsigned short blockY )
	{
		if ( ( blockX > ACTIVE_FLAG_ROWS-1 ) 
			|| ( blockY > ACTIVE_FLAG_COLS-1 )
			)
		{
			//无效的输入块坐标；
			return false;
		}

		unsigned short leftX=blockX, upY=blockY, rightX=blockX, downY=blockY;
		if ( blockX > 0 ) {	--leftX; }
		if ( blockY > 0 ) {	--upY; }
		if ( blockX < ACTIVE_FLAG_COLS-1 ) { ++rightX; }
		if ( blockY < ACTIVE_FLAG_ROWS-1 ) { ++downY; }
		for ( unsigned short y=upY; y<=downY; ++y )
		{
			for ( unsigned short x=leftX; x<=rightX; ++x )
			{
				bool isOrgSet = m_activeBlockFactors[y][x] > 0;
				if ( !isOrgSet )
				{
					D_ERROR( "OrgActiveCenterBlocksDel，块(%d,%d)上影响因子小于等于0，不可能错误\n", x, y );
					continue;
				}
				--m_activeBlockFactors[y][x];
				if ( 0 == m_activeBlockFactors[y][x] )
				{
					//块影响因子已为0，清活动标记；
					DelOneActiveBlockInfo( x, y );//必定为过去活动块；					
				}
			}
		}

		return true;
	}

	///旧的活动中心块移动
	inline bool OrgCenterBlockMove( unsigned short orgX, unsigned short orgY, unsigned short newX, unsigned newY )
	{
		return ( OrgActiveCenterBlocksDel( orgX, orgY ) && NewActiveCenterBlocksAdd( newX, newY ) );
	}

private:
	///在m_activeBlockInfo中添加一个活动块信息；
	bool AddOneActiveBlockInfo( unsigned short blockX, unsigned short blockY )
	{
		if ( (unsigned int)m_activeBlockNum >= ARRAY_SIZE(m_activeBlockInfos) )
		{
			D_ERROR( "AddOneActiveBlockInfo,活动块数超限\n" );
			return false;
		}
		m_activeBlockInfos[m_activeBlockNum].blockX = blockX;
		m_activeBlockInfos[m_activeBlockNum].blockY = blockY;
		++m_activeBlockNum;
		return true;
	}

	///从m_activeBlockInfo中删去一个活动块信息；
	bool DelOneActiveBlockInfo( unsigned short blockX, unsigned short blockY )
	{
		if ( m_activeBlockNum <= 0 )
		{
			D_ERROR( "DelOneActiveBlockInfo,删活动块信息时，原数组空\n" );
			return false;
		}

		bool isFound = false;
		int  foundPos = 0;
		for ( int i=0; i<m_activeBlockNum; ++i )
		{
			if ( i >= ARRAY_SIZE(m_activeBlockInfos) )
			{
				D_ERROR( "DelOneActiveBlockInfo, i(%d)越界，m_activeBlockNum:%d\n", i, m_activeBlockNum );
				return false;
			}
			if ( ( m_activeBlockInfos[i].blockX == blockX ) && ( m_activeBlockInfos[i].blockY == blockY ) )
			{
				isFound = true;
				foundPos = i;
				break;
			}
		}

		if ( !isFound )
		{
			D_ERROR( "DelOneActiveBlockInfo，删活动块信息时在原数组中找不到欲删元素\n" );
			return false;
		}

		m_activeBlockInfos[foundPos].blockX = m_activeBlockInfos[m_activeBlockNum-1].blockX;
		m_activeBlockInfos[foundPos].blockY = m_activeBlockInfos[m_activeBlockNum-1].blockY;
		--m_activeBlockNum;
		if ( m_activeBlockNum < 0 )
		{
			D_ERROR( "DelOneActiveBlockInfo，活动块信息数小于0\n" );
			return false;
		}
		return true;
	}

private:
	int m_activeBlockFactors[ACTIVE_FLAG_ROWS][ACTIVE_FLAG_COLS];//各个块的活动系数；

	int m_activeBlockNum;//m_activeBlockInfos中的有效信息数目;
	ActiveBlockInfo m_activeBlockInfos[MAX_ACTIVE_NUM];//数组形式的当前各活动块的块坐标；
};//class CopyActiveBlocks


///地图副本信息；
class CMuxCopyInfo
{
public:
	//调试进出副本，static const unsigned int   ISSUE_DEL_INTERVAL = 30; //发起删除到真正删除的时间间隔，一般副本空时即发起删除，但若副本中又进人，则发起删除过程cancel;
	static const unsigned int   ISSUE_DEL_INTERVAL = 30*60*1000; //30分钟，单位毫秒，发起删除到真正删除的时间间隔，一般副本空时即发起删除，但若副本中又进人，则发起删除过程cancel;

public:
	CMuxCopyInfo();

	~CMuxCopyInfo();

	PoolFlagDefine()
	{
		MuxCopyInit();
	}

	///副本初始化，由PoolObjInit调用；
	void MuxCopyInit();

	///重置本副本地图信息,副本回池时调用；
    bool ResetCopyMapInfo();

public:
	///使用地图ID号初始化副本(InitCopy-->PoolObjInit-->MuxCopyInit)；
	bool InitCopy( unsigned int mapID, unsigned int copyID );

	///取本副本对应的地图ID号；
	unsigned short GetCopyMapID() { return m_mapID; }
	///取本副本的唯一ID号；
	unsigned int GetCopyID() { return m_copyID; }

	bool SetTDFlag() { m_IsTDMap = true; return true; }

public:
	///副本时钟循环；
	bool CopyTimerProc();

public:
	unsigned int m_lastExpTick;//上次CopyTimerProc遍历时刻；

private:
	///遍历活动块怪物；
	bool ExploreActiveBlockMonster();
	///遍历所有怪物(用于TD脚本)；
	bool ExploreAllMonster();

	///对每一怪物执行ExploreAllMonster时，元素被遍历时执行，由hash表内部调用；
	static bool OnMonsterEleBeExplored( HashTbPtrEle<CMonster>* beExploredEle, void* pParam );

private:
	///删副本逻辑检测,如果删副本逻辑条件满足(例如副本空闲达到一定时间)，则准备发起删除（踢所有人，向centersrv发nomoreplayer标记，向centersrv发删自身请求等等）
	bool CopyDelLogic();//若需发起删除，则返回真，否则返回假；

public:
	///取地图内的怪物指针；
	CMonster* GetMonsterPtrInCopy( unsigned long monsterID );
	///检查玩家是否在地图内(玩家指针不知是否仍有效)；
	bool IsDoubtPlayerPtrInMap( const CPlayer* pPlayer );

public:
	///怪物出生相关通知；
	bool MonsterBornNotify( CMonster* pBornMonster );
	///玩家出现块相关通知；
	void PlayerAppearNotifyByBlockPos( CPlayer* pAppearPlayer, bool isNotiSelf/*是否通知出现者本人*/, unsigned short blockX, unsigned short blockY );
	///玩家离开块相关通知；
	void PlayerLeaveNotifyByBlockPos( CPlayer* pLeavePlayer, unsigned short blockX, unsigned short blockY );
	///向周围怪物广播玩家的新移动消息；
	void PlayerNewMoveMonsterBrocast( CPlayer* pMovePlayer );
	///玩家死亡通知；
	void PlayerDieNotify( CPlayer* pDiePlayer );
	///通知怪物离开视野（对象：可能的特殊怪物）
	void MonsterLeaveViewNotify( CMonster* pMonster );
	///玩家跨块相关通知；
	void PlayerBeyondBlockNotifyByBlockPos( CPlayer* pBeyondPlayer );
	///notify monster player hp add event;
	void NotifySurMonsterPlayerHpAdd( PlayerID addStarter, PlayerID addTarget, int hpAdd, unsigned short centerX, unsigned short centerY );
	///@brief:将该道具包的信息,发送给周围的玩家
	void NotifyItemPkgsAppearEx( CItemsPackage* pItemPkg );
	///notify itempkf first appear;
	void NotifyItemPkgFirstAppearEx( CItemsPackage* pItemPkg );
	//检查地图上某点是否可以添加塔
	bool IsEnabelAddTower(TYPE_POS gridX, TYPE_POS gridY);

public:
	/////副本内的玩家离开组队时的处理（应发起将其从副本中踢出去）;
	//bool OnPlayerLeaveSTeamProc( const PlayerID& playerID );

	///新玩家进入副本；
	bool PlayerEnterCopy( CPlayer* pEnterPlayer, unsigned short gridX, unsigned short gridY );

	///原副本玩家离开副本；
	bool PlayerLeaveCopy( CPlayer* pLeavePlayer );

	///怪物加入副本；
	bool AddMonsterToCopyMap( CMonster* pMonster, unsigned short gridX, unsigned short gridY );

	///delete monster to map block;
	bool DelMonsterFromCopyMap( CMonster* pMonster );

	//add pkg to map block;
	bool AddItemPkgToCopyMap( CItemsPackage* pItemPkg );

	//delete pkg to map block;
	bool DelItemPkgFromCopyMap( CItemsPackage* pItemPkg );

	///发起副本怪物删除；
	bool IssueMonsterDelete( CMonster* pMonster );

	///真正删去所有待删怪物；
	bool DeleteAllToDelMonster();

	///调整玩家所在块
	void AdjustPlayerBlock( bool isManualSet/*是否手动调，手动调时必须周围全通知到而不是只通知新视野*/, CPlayer* pPlayer, unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY );

	///调整怪物所在块
	bool AdjustMonsterPos( CMonster* pMoveMonster, unsigned short orgGridX, unsigned short orgGridY, unsigned short newGridX, unsigned short newGridY
		, bool isForceAndNotNotiMove/*是否强制设位置且不通知周围此移动，怪物移动状态下改变位置为false，非移动状态下改变位置为true(目前只有被击退一种情形)*/ );

	///add rideteam to map block;
	bool AddRideteamToCopyMap( RideTeam* pTeam, unsigned short gridX, unsigned short gridY );

	///delete rideteam from map block;
	bool DelRideteamFromCopyMap( RideTeam* pTeam, unsigned short gridX, unsigned short gridY );

public:
	///取副本中的玩家数；
	inline unsigned int GetCopyPlayerNum() { return m_curPlayerNum; }

	/////temp for monster wrapper;
	//bool GetScopeMonsters( unsigned short centerX, unsigned short centerY, ScopeMonsterInfo* scopeMonstrArr, int inArrSize, int& arrSize );

	/////temp for monster wrapper;
	//bool GetScopePlayers( unsigned short centerX, unsigned short centerY, ScopePlayerInfo* scopePlayerArr, int inArrSize, int& arrSize );

	//获取范围内的玩家和怪物
	bool GetScopeCreature( TYPE_POS posX, TYPE_POS posY, int range, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster );

	//获取技能对象
	bool OnPlayerSkillGetAoeTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster );
	bool OnMonsterSkillGetAoeTarget( CMonster* pUseSkillMonster, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster );
	bool OnPlayerSkillGetTeamTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer );

public:
	///创建一个新的指定类型NPC；
	static bool CreateNewMonster( unsigned long dwMonsterClsID, CMonster*& pOutMonster );
	///释放已分配的怪物对象;
	static bool ReleaseOldMonster( CMonster* pToReleaseMonster );

private:
	///块坐标是否有效；
	inline bool IsBlockValid( unsigned short blockX, unsigned short blockY ) { return (blockX<COPY_BLOCK_WIDTH) && (blockY<COPY_BLOCK_HEIGHT); }

	///格坐标是否有效；
	inline bool IsGridValid( unsigned short gridX, unsigned short gridY ) {	return (gridX<COPY_GRID_WIDTH) && (gridY<COPY_GRID_WIDTH); }

public:
	///用块坐标取块；
	inline MuxMapBlock* GetMapBlockByBlock( unsigned short inBlockX, unsigned short inBlockY )
	{
		if ( IsBlockValid( inBlockX, inBlockY ) )
		{
			return &(m_arrCopyBlocks[inBlockY][inBlockX]);
		} else {
			return NULL;
		}
	}

private:
	///用格坐标取块；
	inline MuxMapBlock* GetMapBlockByGrid( unsigned short inGridX, unsigned short inGridY )
	{
		unsigned short blockX=0, blockY=0;

		MuxMapSpace::GridToBlockPos( inGridX, inGridY, blockX, blockY );

		return GetMapBlockByBlock( blockX, blockY );
	}

	///取范围内有效块；
	inline void GetValidBlock( int nX, int nY, int nScope, int& nMinX, int& nMaxX, int& nMinY, int& nMaxY )
	{
		int tmpX = nX - nScope;
		nMinX = (tmpX<0) ? 0:tmpX;
		tmpX = nX + nScope;
		nMaxX = (tmpX >= COPY_BLOCK_WIDTH) ? (COPY_BLOCK_WIDTH-1):tmpX;

		int tmpY = nY - nScope;
		nMinY = (tmpY<0) ? 0:tmpY;
		tmpY = nY + nScope;
		nMaxY = (tmpY >= COPY_BLOCK_HEIGHT) ? ( COPY_BLOCK_HEIGHT-1 ) :tmpY;
	}

	///取指定块周边块的坐标范围；
	bool GetBlockNearbyScope( unsigned short blockX, unsigned short blockY, unsigned short& minBlockX, unsigned short& minBlockY, unsigned short& maxBlockX, unsigned short& maxBlockY );

	///增加对应八方向的新块计算函数，输入：旧块X，旧块Y，新块X， 新块Y，输出：新视野块X，新视野块Y（最多5组，不允许跳块，即，输入必须为邻块）,???,贺轶男检查;
	bool GetNewViewportBlocks( unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY
		,  bool& isJump/*是否跳块，输入非相邻块*/, unsigned short& viewportBlockNum/*新视野块数目*/, unsigned short outBlockX[5], unsigned short outBlockY[5] );


public:
    template< typename T_Msg >
	void NotifyBlocks( T_Msg* pMsg )
	{
		NotifyAllUseBrocast( pMsg );
	}

	///向周边玩家广播；
	template< typename T_Msg >
	void NotifySurrounding( T_Msg* pMsg, int nPosX, int nPosY ) 
	{
		ACE_UNUSED_ARG( nPosX );
		ACE_UNUSED_ARG( nPosY );
		NotifyAllUseBrocast( pMsg );
	};

	template< typename T_Msg >
	void NotifyAllUseBrocast( T_Msg* pMsg )
	{
		if ( m_curPlayerNum > 0 )
		{
			m_GroupNotiInfo.NotiGroup<T_Msg>( pMsg );
		}		
		return; 
	}

	template< typename T_Msg>
	void NotifySurroundingExceptPlayer( T_Msg* pMsg, int nPosX, int nPosY , const PlayerID& exceptPlayerID )
	{
		if ( !pMsg )
			return;

		ACE_UNUSED_ARG( nPosX );
		ACE_UNUSED_ARG( nPosY );
		ACE_UNUSED_ARG( exceptPlayerID );
		NotifyAllUseBrocast<T_Msg>( pMsg );

		//副本中，该信息也通知自身，省得每次都要重组tmpnoticegroup，实际上现在只有消息MGRoleattUpdate使用此方式通知, by dzj, 10.03.17;CGroupNotiInfo tmpnoticegroup;
		//tmpnoticegroup.ResetGroupInfo();
		//const std::vector<PlayerID>& allplayeridvec = m_GroupNotiInfo.GetAllGroupPlayerIDVec();
		//for ( std::vector<PlayerID>::const_iterator iter = allplayeridvec.begin();
		//	iter != allplayeridvec.end();
		//	++iter )
		//{
		//	const PlayerID&  tmpID = *iter;
		//	if ( ( tmpID.dwPID == exceptPlayerID.dwPID ) && ( tmpID.wGID == exceptPlayerID.wGID ) )
		//	{
		//		continue;
		//	}
		//	tmpnoticegroup.AddGroupNotiPlayerID( tmpID );
		//}
		//tmpnoticegroup.NotiGroup<T_Msg>( pMsg );//通知新进入视野块自身信息；
	}

	bool DelTypeMonster( unsigned int monsterClsID );
	bool AddToDelMonsterType( unsigned int monsterClsID );
	//地图上给该种族玩家一个buff
	void CreateMapBuff( unsigned int race, unsigned int buffID, unsigned int buffTime );


#ifdef HAS_PATH_VERIFIER
	void OnBlockQuadEnable(int regionId);
#endif	


private:
	MuxMapBlock**    m_arrCopyBlocks;//地图各小块；

public:
	///发起删除副本；
	void OnIssueDelCopyQuest()
	{
		D_DEBUG( "副本%d，收到请求删自身消息，执行删除准备操作\n", GetCopyID() );
		IssueNomorePlayerQuest();//通知centersrv不要让更多的人进入；
		IssueKickAllCopyPlayers();//设置删所有玩家标记；
		IssueDetectDelCond();//设置可删条件(副本中玩家数0)检测
		return;
	}

private:
	///是否已发nomoreplayer quest;
	inline bool IsNomorePlayerIssued() { return m_isNomorePlayerIssued; }
	///设置已发nomoreplayer quest;
	inline void SetNomorePlayerIssued() { m_isNomorePlayerIssued = true; }
	///发出nomoreplayer请求给center;
	void IssueNomorePlayerQuest();
	///是否已置m_isDetectDelCond
	inline bool IsDetectDelCondFlag() { return m_isDetectDelCond; }
	///设置m_isDetectDelCond
	inline void SetDetectDelCondFlag() { m_isDetectDelCond = true; }
	///发起设置m_isDetectDelCond，此过程一旦开始，则必定会不断踢副本中玩家，直至踢完玩家后通知centersrv删自身；
	void IssueDetectDelCond();
	///是否已发delselfcopy quest;
	inline bool IsDelSelfCopyIssued() { return m_isDelCopyIssued; }
	///设置已发delselfcopy quest;
	inline void SetDelSelfCopyIssued() { m_isDelCopyIssued = true; }
	///发出delselfcopy请求给center;
	void IssueDelSelfCopyQuest();
	///是否标记删除所有玩家;
	inline bool IsKickAllCopyPlayerIssued() { return m_isKickAllPlayer; }
	///设置删除所有玩家标记;
	inline void SetKickAllCopyPlayerIssued() { m_isKickAllPlayer = true; }
	///置踢所有副本玩家标记
	void IssueKickAllCopyPlayers();
	///发出踢所有玩家指令
	void KickAllCopyPlayers();
	//杀死所有地图玩家
	void KillAllCopyPlayers( unsigned int race );

public:
	///副本地图设置所有玩家的outmapinfo;
	bool SetAllPlayerOutPos( unsigned short mapid, unsigned short posX, unsigned short posY, unsigned short extent );

	////玩家进入副本时的检测(之前是否进过此副本，如果进过，是用何种卷轴进入的等等) 
	//inline bool PlayerEnterCheck( const PlayerID& playerID/*, const char* roleName*/, unsigned int& scrollType/*in_out scrollType*/, bool& isOrgEnterThis/*之前是否进过此副本*/ )
	//{
	//	return m_copyPlayerInfos.PlayerEnterCheck( playerID/*, roleName*/, scrollType, isOrgEnterThis );
	//}

private:
	unsigned int   m_delCondFufilTime;//删除条件的满足时刻；

private:
	bool           m_isNomorePlayerIssued;//是否已发不允许更多玩家进入请求(用于准备踢所有副本玩家，以及删副本)；
	bool           m_isDetectDelCond;//是否检测可删条件(如果条件满足，则发删除自身请求，置m_isDelCopyIssued)；
	bool           m_isDelCopyIssued;//是否已发删除自身请求；
	bool           m_isKickAllPlayer;//是否已发删除所有副本玩家指令；

	unsigned short m_mapID;//地图ID号；
	unsigned int   m_copyID;//副本ID号；

	int m_curPlayerNum;//当前副本中的玩家数；
	CPlayer*            m_CopyPlayers[COPY_PLAYER_MAXNUM];//副本中的玩家，第一个为房主（队长）；
    NewCopyActiveBlocks m_ActiveBlocks;//活动块信息
	//ActiveBlockInfo     m_PlayerPos[COPY_PLAYER_MAXNUM];//对应m_CopyPlayers数组相应玩家的玩家位置，用于向CopyActiveBlocks传递；
	//CopyActiveBlocks    m_ActiveBlocks;//活动块标记；

	int m_curMonsterNum;//当前副本中的怪物数；
	DsHashTable< HashTbPtrEle<CMonster>, 256, 2 > m_CopyMonsters;//副本中的怪物；//不再使用hash宏了，直接使用；

	vector<CMonster*> m_vecToDelMonster;//待删怪物；

//private:
//	CopyPlayerInfos m_copyPlayerInfos;//曾进入此副本的玩家信息；

private:
	bool m_IsLogicToDelSelf;//是否逻辑已检测到要删除自身，当副本空时即发起删除，过ISSUE_DEL_INTERVAL时间后仍未有新变化，则真正删除；
	unsigned int m_LogicDelIssueTick;//逻辑删除发起时刻，距此时刻ISSUE_DEL_INTERVAL TICK后删本副本;
	bool m_IsLogicDelIssued;//是否已发起逻辑删除；

private:
	CGroupNotiInfo  m_GroupNotiInfo;    //组通知玩家信息；
	bool m_IsTDMap;//是否TD地图；

};

#endif //MUX_COPY_H
