﻿#include "mannormalmap.h"

#include "manmapproperty.h"

map<long, CNewMap*> CManNormalMap::m_mapList;

///根据配置读入所有地图并进行初始化, by dzj,07.12.28；
bool CManNormalMap::InitAllNormalMap()
{		
	TRY_BEGIN;

	//首先读入本mapsrv需要管理的地图；

	vector<unsigned short> tomanedmaps;
	CManMapProperty::GetAllNormalMapID( tomanedmaps );

	CMuxMapProperty* pMapProperty = NULL;
	for ( vector<unsigned short>::iterator iter=tomanedmaps.begin(); iter!=tomanedmaps.end(); ++iter )
	{
		pMapProperty = CManMapProperty::FindMapProperty( *iter );
		if ( NULL == pMapProperty )
		{
			D_ERROR( "InitAllNormalMap, can not find map property %d\n", *iter );
			continue;
		}
		///暂时生成的map
		CNewMap* pNewMap = NEW CNewMap( true/*normal map*/, pMapProperty->GetGridWidth(), pMapProperty->GetGridHeight() );
		if ( NULL == pNewMap )
		{
			continue;
		}
		pNewMap->InitMapProperty( pMapProperty );
		pNewMap->FillMapMonsterCreator();//读属于本地图的怪物生成器, by dzj, 07.12.27；
		m_mapList.insert( make_pair(pNewMap->GetMapNo(), pNewMap) );
		D_DEBUG( "read map : %d\n", pNewMap->GetMapNo() );		
	}

	//为读入的各地图刷怪；
	FirstRefreshMonster();

	return true;
	TRY_END;
	return false;
}

void CManNormalMap::ReleaseAllNormalMap(void)
{
	TRY_BEGIN;

	for( map<long,CNewMap*>::iterator iter = m_mapList.begin(); iter != m_mapList.end(); iter++)
	{
		if ( NULL != iter->second )
		{
			delete (iter->second);
		}
	}
	m_mapList.clear();

	return;
	TRY_END;
	return;
}

///第一次刷所有地图的所有初始怪物, by dzj, 07.12.28;
void CManNormalMap::FirstRefreshMonster()
{
	TRY_BEGIN;

	CNewMap* pTmpMap = NULL;
	for( map<long,CNewMap*>::iterator iter = m_mapList.begin(); iter != m_mapList.end(); iter++)
	{
		pTmpMap = iter->second;
		if ( NULL != pTmpMap )
		{
			pTmpMap->FirstRefreshMonster();
		}
	}

	return;
	TRY_END;
	return;
}

///地图管理器慢时钟;
void CManNormalMap::MapManagerSlowTimer( const ACE_Time_Value& curTime )
{
	TRY_BEGIN;

	CNewMap* pTmpMap = NULL;
	for( map<long,CNewMap*>::iterator iter = m_mapList.begin(); iter != m_mapList.end(); iter++)
	{
		pTmpMap = iter->second;
		if ( NULL != pTmpMap )
		{
			pTmpMap->MapNormalTimerProc( curTime );
		}
	}

	return;
	TRY_END;
	return;
}

///获取该地图ID在地图管理器里的指针，如果无，返回NULL
CNewMap* CManNormalMap::FindMap(long lMapID)
{
	TRY_BEGIN;

	map<long,CNewMap*>::iterator iter = m_mapList.find(lMapID);

	if( iter == m_mapList.end() )
	{
		return NULL;
	}

	return (iter->second);

	TRY_END;
	return NULL;
}

