﻿/**
* @file mapcomponent.cpp
* @brief 地图用组件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: mapcomponent.cpp
* 摘    要: 地图用组件(例如：地图计数器，地图计时器)
* 作    者: dzj
* 完成日期: 2008.03.25
*
*/

#include "mapcomponent.h"
#include "muxmapbase.h"
#include <stdlib.h>
#include <fstream>
#include "../Player/Player.h"


CCounterOfMap::~CCounterOfMap() 
{
	TRY_BEGIN;

	InitCounterOfMap( NULL );

	return;
	TRY_END;
	return;
};

///计数器添加
bool CCounterOfMap::AddCounterOfMap( unsigned int counterClsId/*编号，用于脚本标识*/
									, unsigned int counterType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount )
{
	TRY_BEGIN;

	if ( counterType >= CT_INVALID )
	{
		//无效的计数器类型
		D_ERROR( "AddCounterOfMap，无效的计数器类型%d\n", counterType );
		return false;
	}

	MapCounterInfo* pCounterInfo = NEW MapCounterInfo( counterClsId, counterType, counterIdMin, counterIdMax, targetCount );
	if ( NULL == pCounterInfo )
	{
		D_ERROR( "AddCounterOfMap，NEW MapCounterInfo失败\n" );
		return false;
	}

	if ( counterType >= ARRAY_SIZE(m_arrMapCounters) )
	{
		D_ERROR( "AddCounterOfMap，counterType(%d) >= ARRAY_SIZE(m_arrMapCounters)\n", counterType );
		return false;
	}

	m_arrMapCounters[counterType].push_back( pCounterInfo );

	return true;
	TRY_END;
	return false;	
}

///计数事件检测（计数事件到达时回调关卡地图相应接口）
bool CCounterOfMap::TryMapCounterEvent( COUNTER_TARGET_TYPE ctType, unsigned int reasonObjID, unsigned int reasonObjNum )
{
	TRY_BEGIN;

	if ( NULL == m_pOwnerMap )
	{
		D_ERROR( "TryMapCounterEvent，所属地图空\n" );
		return false;
	}

	if ( ctType >= ARRAY_SIZE(m_arrMapCounters) )
	{
		D_ERROR( "TryMapCounterEvent，ctType(%d) >= ARRAY_SIZE(m_arrMapCounters)\n", ctType );
		return false;
	}

	if ( m_arrMapCounters[ctType].empty() )
	{
		return true;
	}

	vector<MapCounterInfo*> expireCounter;
	MapCounterInfo* pCounter = NULL;
	for ( vector<MapCounterInfo*>::iterator iter=m_arrMapCounters[ctType].begin(); iter!=m_arrMapCounters[ctType].end();  )
	{
		pCounter = *iter;
		if ( NULL != pCounter )
		{
			if ( pCounter->IsCountFinish( ctType, reasonObjID, reasonObjNum ) )
			{
				expireCounter.push_back( pCounter );
				iter = m_arrMapCounters[ctType].erase( iter );
				continue;
			}				
		}
		++iter;
	}

	if ( !(expireCounter.empty()) )
	{
		//调用计数满处理，删除计数满计数器；
		for ( vector<MapCounterInfo*>::iterator iter=expireCounter.begin(); iter!=expireCounter.end(); ++iter )
		{
			pCounter = *iter;
			if ( NULL != pCounter )
			{
#ifdef STAGE_MAP_SCRIPT
				m_pOwnerMap->OnMapCounterScriptHandle( pCounter->GetCounterClsID() );//调用地图计数器处理；
#endif //STAGE_MAP_SCRIPT
			}				
		}

		for ( vector<MapCounterInfo*>::iterator iter=expireCounter.begin(); iter!=expireCounter.end(); ++iter )
		{
			pCounter = *iter;
			if ( NULL != pCounter )
			{
				delete pCounter; pCounter = NULL;
			}
		}

		expireCounter.clear();
	}	

	return true;
	TRY_END;
	return false;
}

CTimerOfMap::~CTimerOfMap() 
{
	TRY_BEGIN;

	InitTimerOfMap( NULL );

	return;
	TRY_END;
	return;
};

///地图计时器添加，保持排序顺序；
bool CTimerOfMap::InsertMapTimer( unsigned int timerClsID/*类型号，用于脚本标识*/, const ACE_Time_Value& expireTime/*到期时刻*/ )
{
	TRY_BEGIN;

	MapTimerInfo* pTimerInfo = NEW MapTimerInfo( timerClsID, expireTime );
	if ( NULL == pTimerInfo )
	{
		D_ERROR( "InsertMapTimer，NEW MapTimerInfo失败\n" );
		return false;
	}

	bool isInsert = false;//是否已插入；
	MapTimerInfo* pIterInfo = NULL;
	for ( list<MapTimerInfo*>::iterator iter=m_listTimer.begin(); iter!=m_listTimer.end(); ++iter )
	{
		pIterInfo = *iter;
		if ( NULL != pIterInfo )
		{
			if ( pIterInfo->DeterTimeOrder( expireTime ) )
			{
				D_DEBUG( "插入计时器1:%d　到期时间%d\n", timerClsID, expireTime.sec() );
				m_listTimer.insert( iter, pTimerInfo );
				isInsert = true;
				break;
			}												
		}				
	}

	if ( !isInsert )
	{  
		D_DEBUG( "插入计时器2:%d 到期时间:%d\n", timerClsID, expireTime.sec() );
		m_listTimer.push_back( pTimerInfo );
	}

	return true;
	TRY_END;
	return false;
}


bool CTimerOfMap::DeleteMapTimer( unsigned int timerClsID )
{
	MapTimerInfo* pIterInfo = NULL;
	for ( list<MapTimerInfo*>::iterator iter=m_listTimer.begin(); iter!=m_listTimer.end(); ++iter )
	{
		pIterInfo = *iter;
		if ( NULL != pIterInfo )
		{
			if ( pIterInfo->GetTimerClsID() == timerClsID )
			{
				delete pIterInfo; pIterInfo = NULL;
				m_listTimer.erase( iter );
				return true;
			}												
		}				
	}
	return false;
}

bool CTimerOfMap::IsHaveMapTimer( unsigned int timerClsID )
{	
	MapTimerInfo* pIterInfo = NULL;
	for ( list<MapTimerInfo*>::iterator iter=m_listTimer.begin(); iter!=m_listTimer.end(); ++iter )
	{
		pIterInfo = *iter;
		if ( NULL != pIterInfo )
		{
			if ( pIterInfo->GetTimerClsID() == timerClsID )
			{
				return true;
			}												
		}				
	}
	return false;
}

///地图计时器循环检测；
bool CTimerOfMap::MapTimerCheck( const ACE_Time_Value& curTime )
{
	TRY_BEGIN;

	if ( NULL == m_pOwnerMap )
	{
		return false;
	}

	vector<MapTimerInfo*> expireTimer;
	MapTimerInfo* pTimerInfo = NULL;

	if ( m_listTimer.empty() )
	{
		return true;
	}

	for ( list<MapTimerInfo*>::iterator iter=m_listTimer.begin(); iter!=m_listTimer.end(); )
	{
		pTimerInfo = *iter;
		if ( NULL != pTimerInfo )
		{
			if ( pTimerInfo->IsTimeExpire( curTime ) )
			{
				expireTimer.push_back( pTimerInfo );
				iter = m_listTimer.erase( iter );//释放已到期时钟；
				continue;
			} 
			else 
			{
				break;//由于时钟是按序排列的，因此只要有一个没有到期，则后续时钟肯定也没有到期，无需继续检测；
			}
		} 
		else 
		{
			D_ERROR( "CTimerOfMap::MapTimerCheck，队列中的计时器空!\n" );
			++iter;
		}
	}

	if ( !(expireTimer.empty()) )
	{
		//调用时钟到期处理，删除到期时钟；
		for ( vector<MapTimerInfo*>::iterator iter=expireTimer.begin(); iter!=expireTimer.end(); ++iter )
		{
			pTimerInfo = *iter;
			if ( NULL != pTimerInfo )
			{
				D_DEBUG( "计时器到达:%d  该计时器的到期时间%d\n", pTimerInfo->GetTimerClsID(), pTimerInfo->GetExpireTime().sec() );
#ifdef STAGE_MAP_SCRIPT				
				m_pOwnerMap->OnMapTimerScriptHandle( pTimerInfo->GetTimerClsID() );//调用地图时钟处理；
#endif //STAGE_MAP_SCRIPT
			}				
		}

		for ( vector<MapTimerInfo*>::iterator iter=expireTimer.begin(); iter!=expireTimer.end(); ++iter )
		{
			pTimerInfo = *iter;
			if ( NULL != pTimerInfo )
			{
				delete pTimerInfo; pTimerInfo = NULL;
			}
		}

		expireTimer.clear();
	}	

	return true;
	TRY_END;
	return false;
}



bool CDetailTimerOfMap::InsertDetailMapTimer( unsigned int timerClsID/*类型号，用于脚本标识*/, const unsigned int hour, const unsigned int minute )  //到期小时，到期分钟
{
	m_listDetailTimer.push_back( MapDetailTimerInfo( timerClsID, hour, minute ) );
	return true;
}


bool CDetailTimerOfMap::DeleteDetailMapTimer( unsigned int timerClsID )
{
	for( list<MapDetailTimerInfo>::iterator iter = m_listDetailTimer.begin(); iter != m_listDetailTimer.end(); ++iter )
	{
		if( iter->GetTimerId() == timerClsID )
		{
			m_listDetailTimer.erase( iter );
			return true;
		}
	}
	return false;
}


bool CDetailTimerOfMap::MapDetailTimerCheck()
{
	time_t now = ACE_OS::time();
	tm nowTM;
	ACE_OS::localtime_r(&now, &nowTM);

	vector<MapDetailTimerInfo> expireTimer;
	for( list<MapDetailTimerInfo>::iterator iter = m_listDetailTimer.begin(); iter != m_listDetailTimer.end();)
	{
		if( (nowTM.tm_hour == (int)iter->GetHour())
			&& (nowTM.tm_min == (int)iter->GetMinute()) )
		{
			expireTimer.push_back( *iter );
			iter = m_listDetailTimer.erase( iter );
			continue;
		}
		iter++; 
	}

	for( size_t i = 0; i<expireTimer.size(); ++i )
	{
		m_pOwnerMap->OnDetailTimerScriptHandle( expireTimer[i].GetTimerId() );
	}
	return true;
}


/////////////////////////////////////////////////////////WarCounterManager////////////////////////////////////////////////////////////////////////////

void CWarCounterManager::AddWarCounter( unsigned int counterID, unsigned int counterNum, const char* szCounterInfo )
{
	WarCounterInfo* pCounter = NEW WarCounterInfo;
	pCounter->InitWarCounter( counterID, counterNum, szCounterInfo );
	m_vecWarCounter.push_back( pCounter );
}

unsigned int CWarCounterManager::GetCounterNum( unsigned int counterID )
{
	if( m_vecWarCounter.empty() )
		return 0;

	WarCounterInfo* pCounter = NULL;
	for( vector<WarCounterInfo*>::iterator iter = m_vecWarCounter.begin(); iter != m_vecWarCounter.end(); iter++)
	{
		pCounter = *iter;
		if( NULL == pCounter )
			continue;

		if( pCounter->counterID == counterID )
		{
			return pCounter->GetCounterNum();
		}
	}

	return 0;
}


void CWarCounterManager::SetCounterNum( unsigned int counterID, unsigned int counterNum )
{
	if( m_vecWarCounter.empty() )
		return;

	WarCounterInfo* pCounter = NULL;
	for( vector<WarCounterInfo*>::iterator iter = m_vecWarCounter.begin(); iter != m_vecWarCounter.end(); iter++)
	{
		pCounter = *iter;
		if( NULL == pCounter )
			continue;

		if( pCounter->counterID == counterID )
		{
			pCounter->counterNum = counterNum;
			return;
		}
	}
}


bool CWarCounterManager::DelWarCounter( unsigned int counterID )
{
	if( m_vecWarCounter.empty() )
		return false;

	WarCounterInfo* pCounter = NULL;
	for( vector<WarCounterInfo*>::iterator iter = m_vecWarCounter.begin();iter != m_vecWarCounter.end(); iter++)
	{
		pCounter = *iter;
		if( NULL == pCounter )
			continue;

		if( pCounter->counterID == counterID )
		{
			m_vecWarCounter.erase( iter );
			delete pCounter; pCounter = NULL;
			return true;
		}
	}
	return false;
}

//向地图上所有人显示计数器信息
void CWarCounterManager::MapDisplayCounterInfo( unsigned int counterID )
{
	if( m_vecWarCounter.empty() || NULL == m_pOwnerMap )
		return;

	WarCounterInfo* pCounter = NULL;
	for( vector<WarCounterInfo*>::iterator iter = m_vecWarCounter.begin();iter != m_vecWarCounter.end(); iter++)
	{
		pCounter = *iter;
		if( NULL == pCounter )
			continue;

		if( pCounter->counterID == counterID )
		{
			MGDisplayWarCounter dpWc;
			dpWc.gcCounter.counterID = pCounter->counterID;
			dpWc.gcCounter.couterNum = pCounter->counterNum;
			SafeStrCpy( dpWc.gcCounter.couterString, pCounter->szCounterInfo );
			dpWc.gcCounter.couterStringLen = (unsigned int)strlen( dpWc.gcCounter.couterString );

			m_pOwnerMap->NotifyAllUseBrocast<MGDisplayWarCounter>( &dpWc );
		}
	}
}


//向单人发送计数信息
void CWarCounterManager::OnPlayerDisplayCounterInfo( unsigned int counterID, CPlayer* pPlayer )
{
	if( m_vecWarCounter.empty() || NULL == pPlayer )
		return;

	WarCounterInfo* pCounter = NULL;
	for( vector<WarCounterInfo*>::iterator iter = m_vecWarCounter.begin();iter != m_vecWarCounter.end(); iter++)
	{
		pCounter = *iter;
		if( NULL == pCounter )
			continue;

		if( pCounter->counterID == counterID )
		{
			MGDisplayWarCounter dpWc;
			dpWc.gcCounter.counterID = pCounter->counterID;
			dpWc.gcCounter.couterNum = pCounter->counterNum;
			SafeStrCpy( dpWc.gcCounter.couterString, pCounter->szCounterInfo );
			dpWc.gcCounter.couterStringLen = (unsigned int)strlen( dpWc.gcCounter.couterString );

			pPlayer->SendPkg<MGDisplayWarCounter>( &dpWc );
		}
	}	
}


///////////////////////////////////////////////////WarTimerManager//////////////////////////////////////////////////////////
void CWarTimerManager::WarTimerProcess()
{
	if( m_vecWarTimer.empty() )
		return;

	WarTimerInfo* pTempTimer = NULL;
	for( vector<WarTimerInfo*>::iterator iter = m_vecWarTimer.begin(); iter != m_vecWarTimer.end(); )
	{
		pTempTimer = *iter;
		if( NULL == pTempTimer )
		{
			D_ERROR( "CWarTimerManager::WarTimerProcess, NULL == pTempTimer\n" );
			iter = m_vecWarTimer.erase( iter );
			continue;
		}

		unsigned int ret = pTempTimer->WarTimerProcess();
		if( ret == 0)
		{
			iter = m_vecWarTimer.erase( iter );
			delete pTempTimer; pTempTimer = NULL;
			continue;
		}
		
		//返回结果为2 全地图通知
		if( ret == 2 )
		{
			MapDisplayWarTimer( pTempTimer );		
		}
		iter++;
	}
}


void CWarTimerManager::AddWarTimer( unsigned int inTimerID, unsigned int time, unsigned int stInfoTime, unsigned int isClockWise, bool isInformClient, const char* pTimerInfo )
{
	WarTimerInfo* pTimer = NEW WarTimerInfo;
	pTimer->InitWarTimer( inTimerID, time, stInfoTime, isClockWise, isInformClient, pTimerInfo );
	m_vecWarTimer.push_back( pTimer );
}

bool CWarTimerManager::DeleteWarTimer( unsigned int timerID )
{
	//D_INFO("CWarTimerManager::DeleteWarTimer before m_vecWarTimer.empty()\n");
	if( m_vecWarTimer.empty() )
	{
		//D_INFO("CWarTimerManager::DeleteWarTimer m_vecWarTimer is empty\n");
		return false;
	}

	//D_INFO("CWarTimerManager::DeleteWarTimer after m_vecWarTimer.empty()\n");

	WarTimerInfo* pTempTimer = NULL;
	bool rs = false;
	//D_INFO("m_vecWarTimer.size() = %d\n",m_vecWarTimer.size());
	for( vector<WarTimerInfo*>::iterator iter = m_vecWarTimer.begin(); iter != m_vecWarTimer.end(); )
	{
		pTempTimer = *iter;
		//D_INFO("pTempTimer = *iter;\n");
		if ( NULL == pTempTimer )
		{
			//D_INFO("NULL == pTempTimer\n");
			++iter;
			//D_INFO("++iter;\n");
			continue;
		}
		if( pTempTimer->timerID == timerID )
		{
			//D_INFO("pTempTimer->timerID == timerID\n");
			iter = m_vecWarTimer.erase( iter );
			//D_INFO("iter = m_vecWarTimer.erase( iter );\n");
			delete pTempTimer; pTempTimer = NULL;
			//D_INFO("delete pTempTimer; pTempTimer = NULL;\n");
			rs = true;
			//return true;
		}
		else
		{
			//D_INFO("else\n");
			++iter;
			//D_INFO("++iter;\n");
		}
	}
	return rs;
}

void CWarTimerManager::OnPlayerEnterMap( CPlayer* pPlayer )
{
	if( NULL == pPlayer )
		return;

	if( m_vecWarTimer.empty() )
		return;

	WarTimerInfo* pTempTimer = NULL;
	for( vector<WarTimerInfo*>::iterator iter = m_vecWarTimer.begin(); iter != m_vecWarTimer.end(); ++iter )
	{
		pTempTimer = *iter;
		if (NULL == pTempTimer)
		{
			continue;
		}
		if( pTempTimer->isInformClient && pTempTimer->isStartInform )
		{
			pTempTimer->OnDisplayWarTimer( pPlayer );
			break;
		}
	}
}


//全地图通知该warTimer的信息
void CWarTimerManager::MapDisplayWarTimer( WarTimerInfo* pWarTimer )
{
	if( NULL == pWarTimer || NULL == m_pOwnerMap)
		return;

	MGDisplayWarTimer dpWt;
	dpWt.gcTimer.isClockWise = pWarTimer->isClockWise;
	dpWt.gcTimer.leftSecond = pWarTimer->leftMiliSecond / 1000;
	dpWt.gcTimer.timerID = pWarTimer->timerID;
	SafeStrCpy( dpWt.gcTimer.timerString, pWarTimer->timerInfo );
	dpWt.gcTimer.timerStringLen = (unsigned int)strlen( dpWt.gcTimer.timerString );
	m_pOwnerMap->NotifyAllUseBrocast<MGDisplayWarTimer>( &dpWt );
}


void WarTimerInfo::OnDisplayWarTimer( CPlayer* pNotifyPlayer )
{
	if( NULL == pNotifyPlayer )
		return;

	MGDisplayWarTimer dpWt;
	dpWt.gcTimer.isClockWise = isClockWise;
	dpWt.gcTimer.leftSecond = leftMiliSecond / 1000;
	dpWt.gcTimer.timerID = timerID;
	SafeStrCpy( dpWt.gcTimer.timerString, timerInfo );
	dpWt.gcTimer.timerStringLen = (unsigned int)strlen( dpWt.gcTimer.timerString );
	pNotifyPlayer->SendPkg<MGDisplayWarTimer>( &dpWt );
}


void WarCounterInfo::OnDisplayCounterInfo( CPlayer* pPlayer )
{
	if( NULL == pPlayer )
		return;

	MGDisplayWarCounter dpWc;
	dpWc.gcCounter.counterID = counterID;
	SafeStrCpy( dpWc.gcCounter.couterString, szCounterInfo );
	dpWc.gcCounter.couterStringLen = (unsigned int)strlen( dpWc.gcCounter.couterString );
	pPlayer->SendPkg<MGDisplayWarCounter>( &dpWc );
}