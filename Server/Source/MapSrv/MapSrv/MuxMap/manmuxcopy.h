﻿/**
* @file manmuxcopy.h
* @brief 定义副本管理器
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: manmuxcopy.h
* 摘    要: 管理活动副本;
* 作    者: dzj
* 开始日期: 2009.09.14
* 
*/

#ifndef MAN_MUX_COPY_H
#define MAN_MUX_COPY_H

#include "muxmapbase.h"

#include "LoadConfig/LoadSrvConfig.h" //用于取自身ID号，以生成唯一副本ID；

class CManMuxCopy
{
private:
	CManMuxCopy();
	~CManMuxCopy();

public:
	static bool InitManMuxCopy()
	{
		//m_copyidGener = 1;		
		m_allCopys.clear();

		if ( CLoadSrvConfig::IsCopyMapSrv() )
		{
			m_copyMapPoolSize = 128;
		} else {
			m_copyMapPoolSize = 0;
		}

		if ( m_copyMapPoolSize > 0 )
		{
			m_arrCopyMapPool = NEW CNewMap*[m_copyMapPoolSize];
			for ( unsigned int i=0; i<m_copyMapPoolSize; ++i )
			{
				m_arrCopyMapPool[i] = NEW CNewMap( false/*copymap*/, COPY_GRID_WIDTH, COPY_GRID_HEIGHT );
			}
			m_copyMapPoolCurPos = m_copyMapPoolSize;			
		} else {
			m_arrCopyMapPool = NULL;
			m_copyMapPoolCurPos = 0;
		}

		StructMemSet( m_globePCopyInfos, 0, sizeof(m_globePCopyInfos) );

		return true;
	}

	static bool ReleaseManMuxCopy()
	{
		//释放管理的副本地图至池；
		if ( m_allCopys.size() > 0 )
		{
			for ( map<unsigned int, CNewMap*>::iterator iter=m_allCopys.begin(); iter!=m_allCopys.end(); ++iter )
			{
				if ( NULL != iter->second )
				{
					ReleaseCopyMapToPool( iter->second );
				}
			}
		}
		m_allCopys.clear();

		StructMemSet( m_globePCopyInfos, 0, sizeof(m_globePCopyInfos) );

		//释放副本池；
		if ( NULL != m_arrCopyMapPool ) 
		{
			for ( unsigned int i=0; i<m_copyMapPoolSize; ++i )
			{
				delete m_arrCopyMapPool[i]; m_arrCopyMapPool[i] = NULL;
			}
			delete [] m_arrCopyMapPool; m_arrCopyMapPool = NULL;
		}
		//m_copyidGener = 1;		
		return true;
	}	

public:
	///创建一个新副本；
	static bool CreateNewCopy( unsigned int mapid/*副本对应地图*/, unsigned int copyid/*副本ID号*/ );

	///通过副本ID找副本
	static bool FindCopyByCopyID( unsigned int copyid/*副本ID号*/, CNewMap*& pFoundCopy/*所找到的副本*/ );

public:
	///查询全局副本信息(本mapsrv管理的伪副本)
	static bool OnQueryGlobePCopyInfo( const PlayerID& playerID );
	///查询某伪副本是否可进
	static bool IsPCopyOpen( unsigned short pcopyMapID );

	///开启伪副本;
	static bool OnPCopyOpen( unsigned int pcopyMapID/*欲开启伪副本的mapid*/ );
	///关闭伪副本;
	static bool OnPCopyClose( unsigned int pcopyMapID/*欲开启伪副本的mapid*/ );

public:
	///centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
    static bool OnCenterSrvDelCopyQuest( const CopyManagerEle& copyInfo );
	///收到centersrv命令强制删某副本；
	static bool ForceDelCopy( const CopyManagerEle& copyInfo );

public:
	///执行所管理副本的时钟循环；
	static bool ManCopyTimerProc( const ACE_Time_Value& curTime );

	static bool OnDelWarTimer( unsigned int timerID )
	{
		for( map<unsigned int,CNewMap*>::iterator iter = m_allCopys.begin(); iter != m_allCopys.end(); ++iter)
		{
			if( NULL == iter->second )
				continue;
			CWarTimerManager& timerManager = iter->second->GetWarTimerManager();
			timerManager.DeleteWarTimer( timerID );
		}
		return true;
	}

public:
	/////取得一个新的副本ID号；
	//static unsigned int GetOneNewCopyID()
	//{
	//	++ m_copyidGener;
	//	if ( m_copyidGener <= 0 )
	//	{
	//		m_copyidGener = 1;
	//	} else if ( m_copyidGener >= 1000000/*百万*/ ) {
	//		m_copyidGener = 1;
	//	}
	//	
	//	return CLoadSrvConfig::GetSelfID()*10000000/*千万*/ + m_copyidGener;
	//}

private:
	//从池中分配副本地图；
	static CNewMap* GetNewCopyMapFromPool();
	//释放副本地图回池中；
	static void     ReleaseCopyMapToPool( CNewMap* pToReleaseMap );

private:
	static map<unsigned int, CNewMap*> m_allCopys;//本mapsrv管理的所有副本(副本ID号<-->副本指针)；
	//static unsigned int m_copyidGener;//副本号生成依据号，每生成一个新的副本+1;
	static MGGlobeCopyInfos            m_globePCopyInfos;//全局伪副本信息；

private:
	static unsigned int        m_copyMapPoolSize;//副本地图池大小
	static unsigned int        m_copyMapPoolCurPos;//当前的分配位置，每次都从末尾分配并释放回末尾
	static CNewMap**           m_arrCopyMapPool;//副本地图池

	static int                 m_LastEPos;//用于遍历副本房间；
};

#endif //MAN_MUX_COPY_H
