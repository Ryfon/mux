﻿/**
* @file MapCommonHead.h
* @brief 定义NPC
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MapCommonHead.h
* 摘    要: 声明部分地图公用函数
* 作    者: dzj
* 开始日期: 2008.07.02
* 
*/

#pragma once

#define NEW_MAPSLN //新地图方案(移动或节点信息管理)；

#include "Utility.h"

#include <math.h>
#include <list>
#include <vector>
#include <map>
#include <set>
#include "tcache/tcache.h"

#include "PkgProc/SrvProtocol.h"

using namespace std;
using namespace MUX_PROTO;

class CPlayer;
class CItemsPackage;
class CMonster;
class RideTeam;
class CItemsPackage;
class CPlayerAttSkillProp;
class CNpcSkill;
class CBaseItem;
class CCreatorIns;
struct MuxMapBlock;

class CManMonsterCreatorProp;
class CMuxMap;//by dzj, 07.12.27;

typedef CMuxMap CNewMap;

#define MAPBLOCK_SIZE 10 //每个地图小块的大小(单位：格子数)
#define METER_PERGRID 1  //每格的边长（单位米）

#define PT_CANPASS 1 //地图点可通行；
#define PT_DNTPASS 0 //地图点不可通行；

struct MuxPoint
{
	MuxPoint() : nPosX(0), nPosY(0) {}
	MuxPoint( unsigned short gridX, unsigned short gridY ) : nPosX(gridX), nPosY(gridY) {};
	unsigned short nPosX;
	unsigned short nPosY;
};

enum ELeaveType
{
	LEAVE_SWITCH_MAP = 0,
	LEAVE_OFFLINE = 1,
};

///复活点类型枚举；
enum MRPT_TYPE
{
	MRPT_COMMON  = 0,	 //0：正常用复活点（非城战用
	MRPT_CITY    = 1,	 //1：城镇复活点（城战用）
	MRPT_FIELD   = 2	 //2：野外复活点（城战用）
};

struct MapRebirthPt
{
	unsigned long rptID;//复活点编号；
	MRPT_TYPE     rptType;//复活点类型；
	unsigned long rptMapID;//复活点所在的地图编号；
	MuxPoint      rptLeftTop;//复活点左上角坐标；
	MuxPoint      rptRightBottom;//复活点右下角坐标；
};

namespace MuxMapSpace
{
	extern bool g_bIsViewDebug;

	bool GetViewDebug();

	void InverseViewDebug();
	
	///浮点坐标转成格子坐标；
	void FloatToGridPos( float fPosX, float fPosY, unsigned short& gridX, unsigned short& gridY );
    ///格子坐标转成小块坐标；
	void GridToBlockPos( unsigned short gridX, unsigned short gridY, unsigned short& blockX, unsigned short& blockY );
	///浮点坐标转成小块坐标；
	void FloatToBlockPos( float fPosX, float fPosY, unsigned short& blockX, unsigned short& blockY );

	///计算距离平方
	unsigned int GetSqrDistance( unsigned short gridX1, unsigned short gridY1, unsigned short gridX2, unsigned short gridY2 );
};

