﻿/**
* @file muxcopy.cpp
* @brief 副本实现
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: muxcopy.cpp
* 摘    要: 副本实现
* 作    者: dzj
* 开始日期: 2009.09.14
* 
*/

#include "muxcopy.h" 
#include "manmapproperty.h"

#include "MapBlock.h"

#include "muxmapbase.h" //FinalDelMonster使用；
#include "../ItemSkill.h"
#include "../Lua/BattleScript.h"

CMuxCopyInfo::CMuxCopyInfo() 
{
	m_arrCopyBlocks = NEW MuxMapBlock*[COPY_BLOCK_HEIGHT];
	for( unsigned short y=0; y<COPY_BLOCK_HEIGHT; ++y)
	{
		m_arrCopyBlocks[y] = NEW MuxMapBlock[COPY_BLOCK_WIDTH];
	}

	m_CopyMonsters.ClearAllEle();

	PoolObjInit();
}

CMuxCopyInfo::~CMuxCopyInfo()
{
	PoolObjInit();

	m_CopyMonsters.ClearAllEle();

	if ( NULL != m_arrCopyBlocks )
	{
		for( unsigned short y=0; y<COPY_BLOCK_HEIGHT; ++y)
		{
			if ( NULL != m_arrCopyBlocks[y] )
			{
				delete [] m_arrCopyBlocks[y]; m_arrCopyBlocks[y] = NULL;
			}
		}
		delete [] m_arrCopyBlocks; m_arrCopyBlocks = NULL;
	}
}

///重置本副本地图信息,副本回池时调用；
bool CMuxCopyInfo::ResetCopyMapInfo()
{
	MuxCopyInit();
	return true;
}

void CMuxCopyInfo::MuxCopyInit()
{
	m_lastExpTick = GetTickCount();
	m_IsTDMap = false;//初始非TD地图；
	m_IsLogicToDelSelf = false;
	m_IsLogicDelIssued = false;
	m_isNomorePlayerIssued = false;
	m_delCondFufilTime = 0;
	m_isDetectDelCond = false;
	m_isDelCopyIssued = false;
	m_isKickAllPlayer = false;
	m_curPlayerNum = 0;//当前副本中的玩家数；
	m_curMonsterNum = 0;//当前副本中的怪物数；

	m_mapID = 0;//地图ID号；
	m_copyID = 0;//副本ID号；

	m_vecToDelMonster.clear();

	//m_copyPlayerInfos.InitCopyPlayerInfos();

	m_ActiveBlocks.InitCopyActiveBlocks();

	m_GroupNotiInfo.ResetGroupInfo();
	
	if ( m_CopyMonsters.GetEleNum() > 0 )
	{
		m_CopyMonsters.InitExplore();
		HashTbPtrEle<CMonster>* tbPtr = m_CopyMonsters.ExploreNext();
		CMonster* pMonster = NULL;
		while ( NULL != tbPtr )
		{
			pMonster = tbPtr->GetInnerPtr();
			if ( NULL != pMonster )
			{
				pMonster->OnMonsterDie( NULL, GCMonsterDisappear::M_VANISH/*消失*/, false/*不掉东西*/ );//通知AI，副本中怪物消失，将怪物从本地图上删去，本待删怪物加入待删队列；
			}
			tbPtr = m_CopyMonsters.ExploreNext();
		}
	}

	DeleteAllToDelMonster();

	m_CopyMonsters.ClearAllEle();

	for ( unsigned int i=0; i<ARRAY_SIZE(m_CopyPlayers); ++i )
	{
		m_CopyPlayers[i] = NULL;
	}		

	//以下初始化各地图块；
	if ( NULL != m_arrCopyBlocks )
	{
		for( unsigned short y=0; y<COPY_BLOCK_HEIGHT; ++y )
		{
			if ( NULL != m_arrCopyBlocks[y] )
			{
				for ( unsigned short x=0; x<COPY_BLOCK_WIDTH; ++x )
				{
					m_arrCopyBlocks[y][x].InitMapBlock( x, y );
				}					
			}
		}

	}
}

///使用地图ID号初始化副本(InitCopy-->PoolObjInit-->MuxCopyInit)；
bool CMuxCopyInfo::InitCopy( unsigned int mapID, unsigned int copyID )
{
	PoolObjInit();

	m_mapID = mapID;//地图ID号；
	m_copyID = copyID;//副本ID号；

	return true;
}

///增加对应八方向的新块计算函数，输入：旧块X，旧块Y，新块X， 新块Y，输出：新视野块X，新视野块Y（最多5组，不允许跳块，即，输入必须为邻块）,???,贺轶男检查;
bool CMuxCopyInfo::GetNewViewportBlocks( unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY
						  ,  bool& isJump/*是否跳块，输入非相邻块*/, unsigned short& viewportBlockNum/*新视野块数目*/, unsigned short outBlockX[5], unsigned short outBlockY[5] )
{
	/////跨块移动新视野块,???,贺轶男检查；
	////1、检查新块与旧块之间的范围是否为1，超过1则出错返回false，因为此时输入的数组大小已经不够，如果特定情况下需要跨块，则需要特殊处理，例如GM命令瞬移时；
	////2、计算新视野块，并将新块坐标填入输出数组；

	//预先做个判断
	if( !IsGridValid(orgBlockX, orgBlockY) )
	{
		D_WARNING("越界的Org点位 (%d,%d)\n",orgBlockX, orgBlockY );
		return false;
	}

	if( !IsGridValid(newBlockX, newBlockY)  )
	{
		D_WARNING("越界的new点位 (%d,%d)\n", newBlockX, newBlockY );
		return false;
	}

	isJump = true;//初始假设为跳块；
	viewportBlockNum = 0;

	int nMinX = 0, nMaxX = 0, nMinY = 0, nMaxY = 0;
	bool bUpdateX = false ,bUpdateY = false;
	int dx = orgBlockX - newBlockX;
	int dy = orgBlockY - newBlockY;
	if( abs(dx) > 1 || abs(dy) > 1)
	{
		//D_ERROR( "GetNewViewportBlocks移动跳块, 原块(%d,%d)-->新块(%d,%d)\n", orgBlockX, orgBlockY, newBlockX, newBlockY );
		return false;
	}

	isJump = false;//非跳块；

	//8个方向 8种情况
	if( (dy > 0) 
		&& ( newBlockY != 0 ) )  //既新格不是最上面一格
	{
		bUpdateY = true;
	}

	if( (dy < 0)
		&& ( newBlockY != COPY_BLOCK_HEIGHT - 1 ) ) //新格不是最下面一格
	{
		bUpdateY = true;
	}

	if( (dx> 0)
		&& newBlockX != 0 )		//新格不是最左面的格
	{
		bUpdateX = true;
	}

	if( (dx<0)
		&& newBlockX !=  COPY_BLOCK_WIDTH - 1 )
	{
		bUpdateX = true;
	}

	GetValidBlock( newBlockX, newBlockY, 1, nMinX, nMaxX, nMinY, nMaxY );

	//8个方向
	unsigned int i = 0;
	if( dy == 1 && dx == 0)  //正北
	{
		if( bUpdateY )
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 正北, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}
		}
	}
	else if( dy == -1 && dx == 0)  //正南
	{
		if( bUpdateY )
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 正南, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}
		}
	}
	else if( dy == 0 && dx == 1)  //正西
	{
		if( bUpdateX )
		{
			for(unsigned short y = nMinY ; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 正西, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
	}
	else if( dy == 0 && dx == -1 ) //正东
	{
		if( bUpdateX )
		{
			for(unsigned short y = nMinY ; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 正东, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
	}
	else if( dx == 1 && dy == 1)  //西北
	{
		if( bUpdateX && bUpdateY)  //2边都能更新
		{
			//先X3座标
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西北X3, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}

			//再Y2座标
			for(unsigned short y = nMinY + 1; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西北Y2, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}

		}
		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
		{
			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西北Y轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else  //只能更新X轴
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西北X轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}
		}
	}
	else if( dx == 1 && dy == -1) //西南
	{
		if( bUpdateX && bUpdateY)  //2边都能更新
		{
			//再更新X轴3个
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西南X3, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}

			//再更新Y轴2个
			for(unsigned short y = nMinY; y <= nMaxY - 1; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西南Y2, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
		{
			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西南Y轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else  //只能更新X轴
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西南X轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}
		}
	}
	else if( dx == -1 && dy == -1) //东南
	{
		if( bUpdateX && bUpdateY)  //2边都能更新
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东南X3, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}

			for(unsigned short y = nMinY; y <= nMaxY - 1; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东南Y2, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
		{
			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东南Y轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else  //只能更新X轴
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东南X轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}
		}
	}
	else if( dx == -1 && dy == 1) //东北
	{
		if( bUpdateX && bUpdateY)  //2边都能更新
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东北X3, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}

			for(unsigned short y = nMinY+1; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东北Y2, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
		{
			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东北Y轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else  //只能更新X轴
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东北X轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}
		}
	}
	
	return true; 
};


bool CMuxCopyInfo::DelTypeMonster( unsigned int monsterClsID )
{
	HashTbPtrEle<CMonster>* pFound = NULL;
	CMonster* pMonster = NULL;
	m_CopyMonsters.InitExplore();		
	while ( NULL != (pFound=m_CopyMonsters.ExploreNext()) )
	{
		pMonster = pFound->GetInnerPtr();
		if ( NULL == pMonster )
		{
			continue;
		}

		//地图内跳转 
		if( pMonster->GetClsID() == monsterClsID && pMonster->IsToDelAll())
		{
			pMonster->SetDelAllFlag(false);
			if ( pMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ )
			{
				D_ERROR( "真副本：跟踪重复删怪物bug，已设删除标记怪物%d重复死亡\n", pMonster->GetClsID() );
				continue;
			}
			pMonster->OnMonsterDie( NULL, GCMonsterDisappear::M_VANISH, false );			//没人杀死,不掉包
		}
	}
	return true;
	TRY_END;
	return false;
}

bool CMuxCopyInfo::AddToDelMonsterType( unsigned int monsterClsID )
{
	HashTbPtrEle<CMonster>* pFound = NULL;
	CMonster* pMonster = NULL;
	m_CopyMonsters.InitExplore();		
	while ( NULL != (pFound=m_CopyMonsters.ExploreNext()) )
	{
		pMonster = pFound->GetInnerPtr();
		if ( NULL == pMonster )
		{
			continue;
		}

		if( pMonster->GetClsID() == monsterClsID )
		{
			pMonster->SetDelAllFlag(true);
		}
	}
	return true;
	TRY_END;
	return false;
}


//地图上给该种族玩家一个buff
void CMuxCopyInfo::CreateMapBuff( unsigned int race, unsigned int buffID, unsigned int buffTime )
{
	CPlayer* pPlayer = NULL;
	PlayerID createID;
	createID.dwPID = 0;
	createID.wGID = 0;

	for ( int i=0; i<m_curPlayerNum; ++i )
	{
		if ( i >= ARRAY_SIZE(m_CopyPlayers) )
		{
			D_ERROR( "CMuxCopyInfo::CreateMapBuff, 越界, m_curPlayerNum(%d)\n", m_curPlayerNum );
			return;
		}
		pPlayer = m_CopyPlayers[i];
		if( NULL == pPlayer )
			continue;

		if ( pPlayer->GetRace() == race)
		{
			pPlayer->CreateMuxBuffByID( buffID, buffTime, createID );
		}
	}
}


///取指定块周边块的坐标范围；
bool CMuxCopyInfo::GetBlockNearbyScope( unsigned short blockX, unsigned short blockY, unsigned short& minBlockX, unsigned short& minBlockY, unsigned short& maxBlockX, unsigned short& maxBlockY )
{
	if ( !IsBlockValid( blockX, blockY ) )
	{
		//点的块坐标不在地图内；
		return false;
	}

	minBlockX=blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( 0!=blockX )
	{
		minBlockX = blockX - 1;
	}
	if ( 0!=blockY )
	{
		minBlockY = blockY -1;
	}
	if ( blockX < COPY_BLOCK_WIDTH-1 )
	{
		maxBlockX = blockX + 1;
	}
	if ( blockY < COPY_BLOCK_HEIGHT-1 )
	{
		maxBlockY = blockY + 1;
	}

	return true;
}

///删副本逻辑检测,如果删副本逻辑条件满足(例如副本空闲达到一定时间)，则准备发起删除（踢所有人，向centersrv发nomoreplayer标记，向centersrv发删自身请求等等）
bool CMuxCopyInfo::CopyDelLogic()
{
	if ( m_IsLogicDelIssued )
	{
		return false;//已经发起过逻辑删除，不再重复发；
	}

	//若需发起删除，则返回真，否则返回假；
	if ( m_curPlayerNum > 0 )
	{
		m_IsLogicToDelSelf = false;//一旦有人，则取消可能已经发起的删除;
	}

	if ( m_IsLogicToDelSelf )
	{
		unsigned int passed = GetTickCount() - m_LogicDelIssueTick;
		if ( passed > (ISSUE_DEL_INTERVAL) )
		{
			D_DEBUG( "CMuxCopyInfo::CopyDelLogic，空闲超过%d毫秒，副本%d(mapid:%d)已满足删除条件\n", ISSUE_DEL_INTERVAL, GetCopyID(), GetCopyMapID() );
			m_IsLogicDelIssued = true;
			return true;//已到本副本删除时刻；
		} else {
			return false;//还未到本副本删除时刻；
		}
	} else {
		//当前未设删除标记
		//1、检查人数，如果为0，检查是否需要删去本副本；
		if ( m_curPlayerNum <= 0 )
		{
			m_IsLogicToDelSelf = true;//是否要删除自身，当副本空时即发起删除，过ISSUE_DEL_INTERVAL时间后仍未有新变化，则真正删除；
			m_LogicDelIssueTick = GetTickCount();//删除发起时刻，距此时刻ISSUE_DEL_INTERVAL TICK后删本副本;
			return false;//发起删除后返回；
		}
	}

	return false;
}

///副本时钟循环，若本副本需删去，则返回false，否则返回true；
bool CMuxCopyInfo::CopyTimerProc()
{
	if ( GetTickCount() - m_lastExpTick < 512 )
	{
		//以下代码500毫秒执行一次；
		return true;
	}

	m_lastExpTick = GetTickCount();

	if ( CopyDelLogic() )
	{
		OnIssueDelCopyQuest();//如果删副本逻辑条件满足(例如副本空闲达到一定时间)，则准备发起删除（踢所有人，向centersrv发nomoreplayer标记，向centersrv发删自身请求等等）
	}

	if ( m_curPlayerNum > 0 )
	{
		if ( IsKickAllCopyPlayerIssued() )
		{
			//D_DEBUG( "副本%d，执行踢所有玩家(对每一玩家发离开请求)\n", GetCopyID() );
			KickAllCopyPlayers();//当前副本中还有人，且已设踢所有玩家标记，则踢仍在副本中的玩家；
		}
		m_delCondFufilTime = 0;//重置条件满足时刻；
	} else {
		//副本中当前玩家已为0;
		if ( IsDetectDelCondFlag() )
		{
			//如果当前在检测可删条件；
			if ( 0 == m_delCondFufilTime )
			{
				m_delCondFufilTime = GetTickCount();//条件满足的开始时刻；
			} else {
				//mapsrv向center发删除quest后，center收到nomore标记之前，发来的玩家进入信息，可能会导致稍后center的real del cmd到来时，副本中仍有玩家的逻辑错误出现。
				//为防止出现此种情况，设置以下的条件满足延续时间，如果时此间满足，则上述情况的概率会大大减小。
				//同时，只要是在IssueDelSelfCopyQuest之前到来的玩家，都仍然让其进入，并在稍后的时钟中删去。
				//极端情况下，如果还出现已发删除quest之后，收到玩家进入请求，则应将请求进入玩家踢下线。
				if ( GetTickCount() - m_delCondFufilTime > 5000 ) //距离删条件满足已过去了5秒，这样应该不会再存在时间窗内centersrv发来的新玩家进入消息(后续不会有玩家试图进此副本了，可以安全发IssueDelSelf消息)
				{
					IssueDelSelfCopyQuest();//删除条件已满足，且已延续一段时间，可以安全发起删除；
				}
			}
		}
	}

	DeleteAllToDelMonster();//这里集中清理怪物容器，删去待删怪物；

	if ( !m_IsTDMap )
	{
		//非TD地图遍历活动块；
		return ExploreActiveBlockMonster();
	} else {
		//TD地图遍历所有怪物；
		return ExploreAllMonster();
	}
}

///遍历活动块怪物；
bool CMuxCopyInfo::ExploreActiveBlockMonster()
{
	unsigned int activeBlockNum = 0;/*活动块数目*/
	ActiveBlockInfo* activeBlockInfos = NULL;/*返回的活动块信息数组*/
	if ( !(m_ActiveBlocks.GetActiveBlockInfo( activeBlockNum, activeBlockInfos )) )
	{
		D_ERROR( "副本%d，取活动块失败\n", GetCopyID() );
		return true;
	}

	if ( ( activeBlockNum <= 0 ) 
		|| ( NULL == activeBlockInfos )
		)
	{
		//D_DEBUG( "副本%d，当前无活动块，副本当前人数%d\n", GetCopyID(), m_curPlayerNum );
		return true;
	}

	MuxMapBlock*   pBlock = NULL;//旧视野临时块； 
	for ( unsigned int i=0; i<activeBlockNum; ++i )
	{
		pBlock = GetMapBlockByBlock( activeBlockInfos[i].blockX, activeBlockInfos[i].blockY );
		if ( NULL == pBlock )
		{
			D_ERROR( "不可能错误，遍历副本活动块，输入的活动块坐标(%d,%d)非法\n", activeBlockInfos[i].blockX, activeBlockInfos[i].blockX );
			continue;
		}
		pBlock->BlockCopyTimer();//属于副本的地图块时钟;
	}

	return true;
}

///遍历所有怪物(用于TD脚本)；
bool CMuxCopyInfo::ExploreAllMonster()
{
	if ( m_CopyMonsters.GetEleNum() <= 0 )
	{
		return true;
	}

	m_CopyMonsters.ExploreProcess( &(CMuxCopyInfo::OnMonsterEleBeExplored), NULL );	
	return true;
}

///对每一怪物执行ExploreAllMonster时，元素被遍历时执行，由hash表内部调用；
bool CMuxCopyInfo::OnMonsterEleBeExplored( HashTbPtrEle<CMonster>* beExploredEle, void* pParam )
{
	ACE_UNUSED_ARG( pParam );
	if ( NULL == beExploredEle )
	{
		return false;
	}

	CMonster* pMonster = beExploredEle->GetInnerPtr();
	if ( NULL == pMonster )
	{
		return false;
	}
	pMonster->SlowTimerProc();//遍历过程中不会影响hash表，因此不必另行拷贝

	return true;
}

///检查玩家是否在地图内(玩家指针不知是否仍有效)；
bool CMuxCopyInfo::IsDoubtPlayerPtrInMap( const CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		return false;
	}

	for ( int i=0; i<m_curPlayerNum; ++i )
	{
		if ( i >= ARRAY_SIZE(m_CopyPlayers) )
		{
			D_ERROR( "CMuxCopyInfo::IsDoubtPlayerPtrInMap, m_curPlayerNum(%d)越界\n", m_curPlayerNum );
			return false;
		}
		if ( pPlayer == m_CopyPlayers[i] )
		{
			return true;
		}
	}

	return false;

}

///取地图内的怪物指针；
CMonster* CMuxCopyInfo::GetMonsterPtrInCopy( unsigned long monsterID )
{
	if ( m_CopyMonsters.GetEleNum() <= 0 )
	{
		return NULL;
	}

	HashTbPtrEle<CMonster>* pFound = NULL;
	bool isFound = m_CopyMonsters.FindEle( (unsigned int)monsterID, pFound );
	if ( isFound )
	{
		if ( NULL != pFound )
		{
			return pFound->GetInnerPtr();
		}
	}

	return NULL;
}

//调整玩家所在块
void CMuxCopyInfo::AdjustPlayerBlock( bool isManualSet/*是否手动调，手动调时必须周围全通知到而不是只通知新视野*/, CPlayer* pPlayer, unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY )
{
	if ( NULL == pPlayer )
	{
		D_WARNING( "CMuxCopyInfo::AdjustPlayerBlock，输入玩家指针空\n" );
		return;
	}

    unsigned short viewportBlockNum;/*新视野块数目*/
	unsigned short viewBlockX[5];//新视野块X坐标数组；
	unsigned short viewBlockY[5];//新视野块Y坐标数组；
	MuxMapBlock*   pNewBlock = NULL;//新视野临时块；
	MuxMapBlock*   pOrgBlock = NULL;//旧视野临时块； 

	//将玩家从旧块上删去；
	pOrgBlock = GetMapBlockByBlock( orgBlockX, orgBlockY );
	if ( NULL != pOrgBlock )
	{
		pOrgBlock->DeletePlayerFromBlock( pPlayer );
	} else{
		D_ERROR( "CMuxCopyInfo::AdjustPlayerBlock，不可能错误，取不到玩家%s原来所在块\n", pPlayer->GetAccount() );
	}

	pNewBlock = GetMapBlockByBlock( newBlockX, newBlockY );
	if ( NULL != pNewBlock )
	{
		pNewBlock->AddPlayerToBlock( pPlayer );
		pNewBlock->PlayerMovChgBlockMonsterNotify( pPlayer );//应AI要求添加，进入与怪物相同的同一块时再通知一次怪物(进入相邻格时已通知过一次)；
	} else {
		D_ERROR( "CMuxCopyInfo::AdjustPlayerBlock，不可能错误，取不到玩家%s(%d,%d)所在块(%d,%d)\n", pPlayer->GetAccount(), pPlayer->GetPosX(), pPlayer->GetPosY(), newBlockX, newBlockY );
	}

	//m_PlayerPos[pPlayer->GetPlayerPosInCopy()].blockX = newBlockX;
	//m_PlayerPos[pPlayer->GetPlayerPosInCopy()].blockY = newBlockY;
	//m_ActiveBlocks.SetActiveCenterBlocks( m_curPlayerNum, m_PlayerPos );
	m_ActiveBlocks.OrgCenterBlockMove( orgBlockX, orgBlockY, newBlockX, newBlockY );

	//将玩家加入新块；
	//玩家移动中的定时更新只通知边缘玩家；
	//此处为定时更新，因此：
	// 1.找到前方新进入视野小块玩家；
	// 2.通知这些玩家本玩家的移动消息，同时通知本玩家这些新玩家的相应信息；
	// 3.通知前方新进入视野小块怪物，同时通知本玩家这些新怪物的信息；

	if ( isManualSet/*是否手动调，手动调时必须周围全通知到而不是只通知新视野*/ )
	{
		PlayerLeaveNotifyByBlockPos( pPlayer, orgBlockX, orgBlockY );
		PlayerAppearNotifyByBlockPos( pPlayer, false, newBlockX, newBlockY );

		return;
	}

	bool isJumpBlock = false;
	if (  !GetNewViewportBlocks( orgBlockX, orgBlockY, newBlockX, newBlockY, isJumpBlock, viewportBlockNum, viewBlockX, viewBlockY ) )//新进入视野块；
	{
		if ( isJumpBlock )
		{
			//D_WARNING( "AdjustPlayerBlock，玩家%s移动跳块\n", pPlayer->GetAccount() );
			//跳块，直接通知旧点周围离开，新点周围出现，唯一跳过PlayerLeaveNotify和PlayerAppearNotify直接通知的地方；
			PlayerLeaveNotifyByBlockPos( pPlayer, orgBlockX, orgBlockY );
			PlayerAppearNotifyByBlockPos( pPlayer, false, newBlockX, newBlockY );
		}
		return;
	}

	for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
	{
		if ( ( blockid >= ARRAY_SIZE(viewBlockX) )
			|| ( blockid >= ARRAY_SIZE(viewBlockY) )
			)
		{
			D_ERROR( "CMuxCopyInfo::AdjustPlayerBlock, 越界, viewportBlockNum(%d)\n", viewportBlockNum );
			return;
		}
		pNewBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
		if ( NULL != pNewBlock )
		{
			pNewBlock->PlayerMoveNewBlockNotify( pPlayer );//新进入视野块处理；
		}
	}

	if ( m_curPlayerNum > 0 )
	{
		MGSimplePlayerInfo* movPlayerInfo = NULL;
		movPlayerInfo = pPlayer->GetSimplePlayerInfo();
		if( NULL != movPlayerInfo )
		{
			m_GroupNotiInfo.NotiGroup<MGSimplePlayerInfo>( movPlayerInfo );//通知新进入视野块自身信息；
		}

		MGUnionMemberShowInfo *pMovePlayerShowInfo;
		CUnion *pMovePlayerUnion = pPlayer->Union();
		if(NULL != pMovePlayerUnion)
		{
			pMovePlayerShowInfo = pMovePlayerUnion->GetUnionMemberShowInfo(pPlayer);
			m_GroupNotiInfo.NotiGroup<MGUnionMemberShowInfo>(pMovePlayerShowInfo);
		}

		MGBuffData moveBuffInfo;
		if( pPlayer->IsHaveBuff() )
		{
			moveBuffInfo.buffData = (*pPlayer->GetBuffData());
			moveBuffInfo.targetID = pPlayer->GetFullID();
			m_GroupNotiInfo.NotiGroup<MGBuffData>( &moveBuffInfo );//通知新进入视野块自身信息；
		}

		MGPetinfoStable* movePetInfo = NULL;
		if( pPlayer->IsSummonPet() )
		{
			movePetInfo = pPlayer->GetSurPetNotiInfo();
			if ( NULL != movePetInfo )
			{
				m_GroupNotiInfo.NotiGroup<MGPetinfoStable>( movePetInfo );//通知新进入视野块自身信息；
			}
		}
	}

	if( !GetNewViewportBlocks( newBlockX, newBlockY, orgBlockX, orgBlockY, isJumpBlock, viewportBlockNum, viewBlockX, viewBlockY ) )//旧离开视野块；
	{
		if ( isJumpBlock )
		{
			D_ERROR("CMuxCopyInfo::AdjustPlayerBlock， 跳块2，不可能错误\n");//因为前面应该已处理并已返回；
		}		
		return;
	}

	for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
	{
		if ( ( blockid >= ARRAY_SIZE(viewBlockX) )
			|| ( blockid >= ARRAY_SIZE(viewBlockY) )
			)
		{
			D_ERROR( "CMuxCopyInfo::AdjustPlayerBlock, 越界2, viewportBlockNum(%d)\n", viewportBlockNum );
			return;
		}
		pNewBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
		if ( NULL != pNewBlock )
		{
			pNewBlock->PlayerMoveOrgBlockNotify( pPlayer );//旧离开视野块处理；
		}
	}

	return;
}

//调整怪物所在块
bool CMuxCopyInfo::AdjustMonsterPos( CMonster* pMoveMonster, unsigned short orgGridX, unsigned short orgGridY, unsigned short newGridX, unsigned short newGridY
									, bool isForceAndNotNotiMove/*是否强制设位置且不通知周围此移动，怪物移动状态下改变位置为false，非移动状态下改变位置为true(目前只有被击退一种情形)*/	)
{
	TRY_BEGIN;

	if ( NULL == pMoveMonster )
	{
		D_WARNING( "CMuxCopyInfo::AdjustMonsterPos，传入怪物指针空\n" );
		return false;
	}

	//更新怪物身上保存的位置信息； 
	pMoveMonster->SetPos( newGridX, newGridY );

	MuxMapBlock* pOrgBlock = GetMapBlockByGrid( orgGridX, orgGridY );
	if ( NULL == pOrgBlock )
	{
		D_WARNING( "CMuxCopyInfo::AdjustMonsterPos,怪物移动原所在点X:%d,Y:%d块指针空\n", orgGridX, orgGridY );
		return false;
	}

	MuxMapBlock* pNewBlock = GetMapBlockByGrid( newGridX, newGridY );
	if ( NULL == pNewBlock )
	{
		D_WARNING( "CMuxCopyInfo::AdjustMonsterPos,怪物移动目标点X:%d,Y:%d块指针空\n", newGridX, newGridY );
		return false;
	}

	if ( pOrgBlock == pNewBlock )
	{
		return true;//未跨块，直接返回
	}

	//怪物跨块移动；
	pOrgBlock->DeleteMonsterFromBlock( pMoveMonster );//将怪物从旧块上删去；
	pNewBlock->AddMonsterToBlock( pMoveMonster );//怪物加入新块；

	unsigned short viewportBlockNum;/*新视野块数目*/
	unsigned short viewBlockX[5];//新视野块X坐标数组；
	unsigned short viewBlockY[5];//新视野块Y坐标数组；
	MuxMapBlock* pTmpBlock = NULL;
	//处理新进入视野块；
	bool isJumpBlock = false;
	if( !GetNewViewportBlocks( pOrgBlock->GetBlockX(), pOrgBlock->GetBlockY(), pNewBlock->GetBlockX(), pNewBlock->GetBlockY(), isJumpBlock, viewportBlockNum, viewBlockX, viewBlockY ) )//新进入视野块；
	{
		if ( isJumpBlock )
		{
			D_WARNING("CMuxCopyInfo::AdjustMonsterPos,怪物跳块移动\n");				
		} else {
			D_WARNING("CMuxCopyInfo::AdjustMonsterPos,输入点非法\n");
		}

		return false;
	}

	for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
	{
		if ( ( blockid >= ARRAY_SIZE(viewBlockX) )
			|| ( blockid >= ARRAY_SIZE(viewBlockY) )
			)
		{
			D_ERROR( "CMuxCopyInfo::AdjustMonsterPos, 越界, viewportBlockNum(%d)\n", viewportBlockNum );
			return false;
		}
		pTmpBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
		if ( NULL != pTmpBlock )
		{
			pTmpBlock->MonsterMoveNewBlockNotify( pMoveMonster );
		}
	}

	//据客户端意见，仍然通知新视野，而攻击消息不通知新视野，by dzj, 10.05.24,if ( !isForceAndNotNotiMove ) //正常移动，需要通知新视野
	{
		if ( m_curPlayerNum > 0 )
		{
			//D_DEBUG( "CMuxCopyInfo::::AdjustMonsterPos，通知怪物移动\n" );
			m_GroupNotiInfo.NotiGroup<MGMonsterMove>( pMoveMonster->GetMonsterMove() );
		}
	}

	//处理旧离开视野块；
	if( !GetNewViewportBlocks( pNewBlock->GetBlockX(), pNewBlock->GetBlockY(), pOrgBlock->GetBlockX(), pOrgBlock->GetBlockY(), isJumpBlock, viewportBlockNum, viewBlockX, viewBlockY ) )//旧离开视野块
	{
		if ( isJumpBlock )
		{
			D_WARNING("CMuxCopyInfo::AdjustMonsterPos，怪物跳块移动2\n");				
		} else {
			D_WARNING("CMuxCopyInfo::AdjustMonsterPos 输入点非法2\n");
		}
		return false;
	}

	for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
	{
		if ( ( blockid >= ARRAY_SIZE(viewBlockX) )
			|| ( blockid >= ARRAY_SIZE(viewBlockY) )
			)
		{
			D_ERROR( "CMuxCopyInfo::AdjustMonsterPos, 越界2, viewportBlockNum(%d)\n", viewportBlockNum );
			return false;
		}
		pTmpBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );			
		if ( NULL != pNewBlock )
		{
			pTmpBlock->MonsterMoveOrgBlockNotify( pMoveMonster );
		}
	}

	return true;
	TRY_END;
	return false;
}

//真正删去所有待删怪物；
bool CMuxCopyInfo::DeleteAllToDelMonster()
{
	//删除副本中的待删怪物；
	CMonster* pToDelMonster = NULL;
	CNewMap* pMap = NULL;
	for ( vector<CMonster*>::iterator iter=m_vecToDelMonster.begin(); iter!=m_vecToDelMonster.end(); ++iter )//待删怪物；
	{
		pToDelMonster = *iter;
		if ( NULL != pToDelMonster )
		{
			//无效的monster对象，应该删去；
			pMap = pToDelMonster->GetMap();
			if ( NULL == pMap )
			{
				D_ERROR( "CopyTimerProc, pToDelMonster所属的地图空!\n" );
				return false;
			}
			pMap->FinalDelMonster( pToDelMonster );
		}
	}
	m_vecToDelMonster.clear();

	return true;
}

//发起副本怪物删除；
bool CMuxCopyInfo::IssueMonsterDelete( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		return false;
	}
	if ( NULL == GetMonsterPtrInCopy( pMonster->GetMonsterID() ) )
	{
		D_ERROR( "IssueMonsterDelete, monster %x 不属于本副本 \n", pMonster->GetMonsterID() );
		return false;
	}
	if ( pMonster->IsToDel() )
	{
		D_ERROR( "CMuxCopyInfo::IssueMonsterDelete, 重复删除怪物%x", (unsigned int)pMonster );
		return false;
	}
	pMonster->SetDelFlag();//置删除标记；
	m_vecToDelMonster.push_back( pMonster );
	return true;
}

///释放已分配的怪物对象;
bool CMuxCopyInfo::ReleaseOldMonster( CMonster* pToReleaseMonster )
{
	if ( NULL == pToReleaseMonster )
	{
		return false;
	}
	
	delete pToReleaseMonster; pToReleaseMonster = NULL;
	return true;
}

///创建一个新的指定类型NPC；
bool CMuxCopyInfo::CreateNewMonster( unsigned long dwMonsterClsID, CMonster*& pOutMonster )
{
	TRY_BEGIN;

	if ( NULL == CManNPCProp::FindObj( dwMonsterClsID ) )
	{
		D_WARNING( "CMuxCopyInfo::CreateNewMonster，找不到怪物类型%d的对应属性\n", dwMonsterClsID );
		pOutMonster = NULL;
		return false;
	}

	CMonster* pNewMonster = NEW CMonster;//只从池中分配而不新NEW，以便任何时候都可以访问pMonster的唯一ID号;
	if ( NULL == pNewMonster )
	{
		D_ERROR( "CMuxCopyInfo::CreateNewMonster，从m_poolMonster中分配CMonster实例失败\n" );
		pOutMonster = NULL;
		return false;
	}
	
	if ( !(pNewMonster->InitMonster( dwMonsterClsID )) )
	{
		D_WARNING( "怪物类型%d属性初始化失败\n", dwMonsterClsID );
		delete pNewMonster; pNewMonster = NULL;
		return false;
	}
	pOutMonster = pNewMonster;

	return true;
	TRY_END;
	return false;
}

///notify itempkf first appear;
void CMuxCopyInfo::NotifyItemPkgFirstAppearEx( CItemsPackage* pItemPkg )
{
	TRY_BEGIN;

	if ( NULL == pItemPkg )
	{
		D_ERROR( "CMuxCopyInfo::NotifyItemPkgFirstAppearEx, NULL == pItemPkg\n" );
		return;
	}

	unsigned short nPosX = pItemPkg->GetItemPosX();
	unsigned short nPosY = pItemPkg->GetItemPosY();
	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( nPosX, nPosY, blockX, blockY );

	//获取该点周围的Block节点，
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			//通告给者
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )//将该包裹的信息，通知给该节点的客户端
			{
				pBlock->NoticeItemPkgFirstAppear( pItemPkg );
			}
		}
	}

	return;
	TRY_END;
	return;
}

bool CMuxCopyInfo::IsEnabelAddTower(TYPE_POS gridX, TYPE_POS gridY)
{
	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( gridX, gridY, blockX, blockY );

	//获取该点周围的Block节点，
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				if (!pBlock->IsEnabelAddTower(gridX,gridY))
				{
					return false;
				}
			}
		}
	}
	return true;
}

///@brief:将该道具包的信息,发送给周围的玩家
void CMuxCopyInfo::NotifyItemPkgsAppearEx( CItemsPackage* pItemPkg )
{
	TRY_BEGIN;

	if ( NULL == pItemPkg )
	{
		D_ERROR( "CMuxCopyInfo::NotifyItemPkgsAppearEx, NULL == pItemPkg\n" );
		return;
	}

	unsigned short nPosX = pItemPkg->GetItemPosX();
	unsigned short nPosY = pItemPkg->GetItemPosY();
	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( nPosX, nPosY, blockX, blockY );

	//获取该点周围的Block节点，
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			//通告给者
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )//将该包裹的信息，通知给该节点的客户端
			{
				pBlock->NoticeItemPkgsToSurroundPlayer( pItemPkg );
			}
		}
	}

	return;
	TRY_END;
	return;

}

///玩家出现块相关通知；
void CMuxCopyInfo::PlayerAppearNotifyByBlockPos( CPlayer* pAppearPlayer, bool isNotiSelf/*是否通知出现者本人*/, unsigned short blockX, unsigned short blockY )
{
	if ( NULL == pAppearPlayer )
	{
		D_WARNING( "PlayerAppearNotifyByBlockPos，输入玩家指针空\n" );
		return;
	}

	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerAppearBlockNotify( pAppearPlayer, isNotiSelf );
			}
		}
	}

	if ( m_curPlayerNum > 0 )
	{
		//通知块上玩家本玩家出现信息；
		MGSimplePlayerInfo* movPlayerInfo = NULL;
		movPlayerInfo = pAppearPlayer->GetSimplePlayerInfo();
		if ( NULL != movPlayerInfo )
		{
			m_GroupNotiInfo.NotiGroup<MGSimplePlayerInfo>( movPlayerInfo );
		}
		
		MGUnionMemberShowInfo* pMovePlayerShowInfo = NULL;
		CUnion* pMovePlayerUnion = pAppearPlayer->Union();
		if ( NULL != pMovePlayerUnion )
		{
			pMovePlayerShowInfo = pMovePlayerUnion->GetUnionMemberShowInfo(pAppearPlayer);
			if ( NULL != pMovePlayerShowInfo )
			{
				m_GroupNotiInfo.NotiGroup<MGUnionMemberShowInfo>( pMovePlayerShowInfo );
			}
		}

		MGBuffData moveBuff;
		if ( pAppearPlayer->IsHaveBuff() )
		{
			moveBuff.buffData = (*pAppearPlayer->GetBuffData());
			moveBuff.targetID = pAppearPlayer->GetFullID();
			m_GroupNotiInfo.NotiGroup<MGBuffData>( &moveBuff );
		}

		MGPetinfoStable* movePetInfo = NULL;
		if( pAppearPlayer->IsSummonPet() )
		{
			movePetInfo = pAppearPlayer->GetSurPetNotiInfo();
			if ( NULL != movePetInfo )
			{
				m_GroupNotiInfo.NotiGroup<MGPetinfoStable>( movePetInfo );
			}
		}
	}

	return;
}

///玩家跨块相关通知；
void CMuxCopyInfo::PlayerBeyondBlockNotifyByBlockPos( CPlayer* pBeyondPlayer )
{
	if ( NULL == pBeyondPlayer )
	{
		return;
	}

	unsigned short blockX = 0, blockY = 0;
	MuxMapSpace::GridToBlockPos( pBeyondPlayer->GetPosX(), pBeyondPlayer->GetPosY(), blockX, blockY );
	unsigned short minBlockX=blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{		
		return;//输入点为无效点；
	}

	MGEnvNotiFlag notiFlag;
	notiFlag.gcNotiFlag.notiStOrEnd = true;//通知开始；
	pBeyondPlayer->SendPkg<MGEnvNotiFlag>( &notiFlag );

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerBeyondBlockSelfNotify( pBeyondPlayer );
			}
		}
	}

	notiFlag.gcNotiFlag.notiStOrEnd = false;//通知结束；
	pBeyondPlayer->SendPkg<MGEnvNotiFlag>( &notiFlag );

	return;
}

///notify monster player hp add event;
void CMuxCopyInfo::NotifySurMonsterPlayerHpAdd( PlayerID addStarter, PlayerID addTarget, int hpAdd, unsigned short centerX, unsigned short centerY )
{
	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( centerX, centerY, blockX, blockY );
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x, y );
			if ( NULL != pBlock )
			{
				pBlock->NotifyMonsterAddHpEvent( addStarter, addTarget, hpAdd );
			}
		}
	}

	return;
}

///通知怪物离开视野（对象：可能的特殊怪物）
void CMuxCopyInfo::MonsterLeaveViewNotify( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMuxCopyInfo::MonsterLeaveViewNotify, 输入参数空\n" );
		return;
	}

	unsigned short blockX = 0, blockY = 0, minBlockX = 0, minBlockY =0, maxBlockX = 0, maxBlockY = 0;
	MuxMapSpace::GridToBlockPos( pMonster->GetPosX(), pMonster->GetPosY(), blockX, blockY );
	
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x, y );
			if ( NULL != pBlock )
			{
				pBlock->MonsterLeaveViewNotify( pMonster );   //通知进入视野
			}
		}
	}

	return;
}

///玩家死亡通知；
void CMuxCopyInfo::PlayerDieNotify( CPlayer* pDiePlayer )
{
	TRY_BEGIN;

	if ( NULL == pDiePlayer )
	{
		D_WARNING( "CMuxNormalMapInfo::PlayerDieNotify, 输入指针空\n" );
		return;
	}

	unsigned short blockX = 0, blockY = 0;
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	MuxMapSpace::GridToBlockPos( pDiePlayer->GetPosX(), pDiePlayer->GetPosY(), blockX, blockY );
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerDieBlockNotify( pDiePlayer );
			}
		}
	}

	return;
	TRY_END;
	return;
}

///向周围怪物广播玩家的新移动消息；
void CMuxCopyInfo::PlayerNewMoveMonsterBrocast( CPlayer* pMovePlayer )
{
	TRY_BEGIN;

	if ( NULL == pMovePlayer )
	{
		D_WARNING( "CMuxCopyInfo::PlayerNewMoveMonsterBrocast, 输入指针空\n" );
		return;
	}

	unsigned short blockX = 0, blockY = 0;
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;


	MuxMapSpace::GridToBlockPos( pMovePlayer->GetPosX(), pMovePlayer->GetPosY(), blockX, blockY );
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerMovChgBlockMonsterNotify( pMovePlayer );
			}
		}
	}

	return;
	TRY_END;
	return;
}

///玩家离开块相关通知；
void CMuxCopyInfo::PlayerLeaveNotifyByBlockPos( CPlayer* pLeavePlayer, unsigned short blockX, unsigned short blockY )
{
	if ( NULL == pLeavePlayer )
	{
		D_WARNING( "PlayerLeaveNotifyByBlockPos，输入玩家指针空\n" );
		return;
	}

	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerLeaveBlockNotify( pLeavePlayer );
			}
		}
	}

	if ( m_curPlayerNum > 0 )
	{
		//修改BUG 196872:[SAT] 玩家B在移动时同意双人骑乘邀请，玩家A显示玩家B状态错误
		//因为服务器多发了PlayerLeave消息
		bool     isNeedExceptRideTeam = false;
		PlayerID leadID, memberID;
		leadID.dwPID = leadID.wGID = 0;
		memberID.dwPID = memberID.wGID = 0;
		if(   pLeavePlayer->IsRideState() )//如果在马上,则两个玩家则作为一个整体,不通知，则一起不通知
		{
			isNeedExceptRideTeam = true;
			pLeavePlayer->GetSelfStateManager().GetRideState().GetTeamAllMemberID( leadID, memberID );
		}

		if ( isNeedExceptRideTeam )
		{
			CGroupNotiInfo tmpnoticegroup;
			tmpnoticegroup.ResetGroupInfo();
			const std::vector<PlayerID>& allplayeridvec = m_GroupNotiInfo.GetAllGroupPlayerIDVec();
			for ( std::vector<PlayerID>::const_iterator iter = allplayeridvec.begin();
				iter != allplayeridvec.end();
				++iter )
			{
				const PlayerID&  tmpID = *iter;
				if ( ( tmpID.dwPID == leadID.dwPID ) && ( tmpID.wGID == leadID.wGID ) )
				{
					continue;
				}

				if ( ( tmpID.dwPID == memberID.dwPID ) && ( tmpID.wGID == memberID.wGID ) )
				{
					continue;
				}
				tmpnoticegroup.AddGroupNotiPlayerID( tmpID );
			}

			MGPlayerLeave returnMsg;
			returnMsg.lPlayerID = pLeavePlayer->GetFullID();
			returnMsg.lMapCode = pLeavePlayer->GetMapID();
			tmpnoticegroup.NotiGroup<MGPlayerLeave>( &returnMsg );//通知新进入视野块自身信息；
		}
		else
		{
			MGPlayerLeave returnMsg;
			returnMsg.lPlayerID = pLeavePlayer->GetFullID();
			returnMsg.lMapCode = pLeavePlayer->GetMapID();
			m_GroupNotiInfo.NotiGroup<MGPlayerLeave>( &returnMsg );//通知新进入视野块自身信息；
		}
	}

	return;
}

///怪物出生相关通知；
bool CMuxCopyInfo::MonsterBornNotify( CMonster* pBornMonster )
{
	TRY_BEGIN;

	if ( NULL == pBornMonster )
	{
		D_ERROR( "CMuxCopyInfo::MonsterBornNotify，monster指针空\n" );
		return false;
	}

	unsigned short bornGridX = pBornMonster->GetPosX();
	unsigned short bornGridY = pBornMonster->GetPosY();

	if ( !IsGridValid( bornGridX, bornGridY ) )
	{
		D_ERROR( "CMuxCopyInfo::MonsterBornNotify，monster出生点%d,%d，超出范围或为不可移动点\n", bornGridX, bornGridY );
		return false;
	}

	unsigned short bornBlockX=0, bornBlockY=0;
	MuxMapSpace::GridToBlockPos( bornGridX, bornGridY, bornBlockX, bornBlockY );//怪物出现点块坐标；
	MuxMapBlock* pBornBlock = GetMapBlockByBlock( bornBlockX, bornBlockY );
	if ( NULL == pBornBlock )
	{
		D_ERROR( "CMuxCopyInfo::MonsterBornNotify，不可能错误，monster出生点%d,%d无对应地图块\n", bornGridX, bornGridY );
		return false;
	}

	//2、相关影响块处理；
	unsigned short minBlockX = bornBlockX, minBlockY=bornBlockY, maxBlockX=bornBlockX, maxBlockY=bornBlockY;
	if ( !GetBlockNearbyScope( bornBlockX, bornBlockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{		
		return false;//输入点为无效点；
	}

	MuxMapBlock* pTmpBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pTmpBlock = GetMapBlockByBlock( x, y );
			if ( NULL != pTmpBlock )
			{
				pTmpBlock->MonsterBornBlockNotify( pBornMonster );
			}			
		}
	}

	return true;
	TRY_END;
	return false;
}

//add pkg to map block;
bool CMuxCopyInfo::AddItemPkgToCopyMap( CItemsPackage* pItemPkg )
{
	if ( NULL == pItemPkg )
	{
		D_ERROR( "CMuxCopyInfo::AddItemPkgToCopyMap, NULL == pItemPkg\n" );
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( pItemPkg->GetItemPosX(), pItemPkg->GetItemPosY() );
	if ( NULL == pBlock )
	{
		D_ERROR( "AddItemPkgToCopyMap, can not find res Block, (%d, %d)", pItemPkg->GetItemPosX(), pItemPkg->GetItemPosY() );
		return false;
	}

	//将生成的道具包加入该节点
	pBlock->AddItemsPkg( pItemPkg );

	return true;
}

//delete pkg to map block;
bool CMuxCopyInfo::DelItemPkgFromCopyMap( CItemsPackage* pItemPkg )
{
	if ( NULL == pItemPkg )
	{
		D_ERROR( "CMuxCopyInfo::DelItemPkgFromCopyMap, NULL == pItemPkg\n" );
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( pItemPkg->GetItemPosX(), pItemPkg->GetItemPosY() );
	if ( NULL == pBlock )
	{
		D_ERROR( "DelItemPkgFromCopyMap, can not find res Block, (%d, %d)", pItemPkg->GetItemPosX(), pItemPkg->GetItemPosY() );
		return false;
	}

	return pBlock->DeleteItemsPkg( pItemPkg );
}

///怪物加入副本；
bool CMuxCopyInfo::AddMonsterToCopyMap( CMonster* pMonster, unsigned short gridX, unsigned short gridY )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMuxCopyInfo::AddMonsterToCopyMap, NULL == pMonster\n" );
		return false;
	}

	if ( m_curMonsterNum >= 200 )
	{
		D_ERROR( "CMuxCopyInfo::AddMonsterToCopyMap, m_curMonsterNum >= 200\n" );
		return false;
	}

	if ( !IsGridValid( gridX, gridY ) )
	{
		//点超出范围，添加失败;
		D_ERROR( "AddMonsterToCopyMap，怪物的地图出现点%d,%d越界\n", gridX, gridY );		
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( gridX, gridY );
	if ( NULL == pBlock )
	{
		//找不到对应块指针，不可能错误；
		D_ERROR( "AddMonsterToCopyMap，地图出现点%d,%d对应块空\n", gridX, gridY );		
		return false;
	}

	pMonster->SetBornPt( gridX, gridY ); //设置怪物出生点	
	pMonster->SetPos( gridX, gridY );

	pBlock->AddMonsterToBlock( pMonster );//怪物加入块；

	m_CopyMonsters.PushEle( HashTbPtrEle<CMonster>( pMonster->GetMonsterID(), pMonster ) );

	++ m_curMonsterNum;

	return true;
}

//delete monster to map block;
bool CMuxCopyInfo::DelMonsterFromCopyMap( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "DelMonsterFromCopyMap，怪物指针空\n" );
		return false;
	}

	if ( !(pMonster->IsToDel()) )
	{
		D_ERROR( "CMuxCopyInfo::DelMonsterFromCopyMap，未设置删怪物%x标记\n", (unsigned int)pMonster );
		return false;
	}

	unsigned short gridX=pMonster->GetPosX(), gridY=pMonster->GetPosY();
	if ( !IsGridValid( gridX, gridY ) )
	{
		//点超出范围，删除失败;
		D_ERROR( "DelMonsterFromCopyMap，怪物所在的地图点%d,%d越界\n", gridX, gridY );		
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( gridX, gridY );
	if ( NULL == pBlock )
	{
		//找不到对应块指针，不可能错误；
		D_ERROR( "DelMonsterFromCopyMap，怪物所在的地图点%d,%d对应块空\n", gridX, gridY );		
		return false;
	}

	pBlock->DeleteMonsterFromBlock( pMonster );

	m_CopyMonsters.DelEle( pMonster->GetMonsterID() );

	ReleaseOldMonster( pMonster );

	-- m_curMonsterNum;

	return true;
}
//
//此工作已改至center,///副本内的玩家离开组队时的处理（应发起将其从副本中踢出去）;
//bool CMuxCopyInfo::OnPlayerLeaveSTeamProc( const PlayerID& playerID )
//{
//	////先删去该玩家的第一次进副本信息，然后发起踢起玩家；
//	//if ( !(m_copyPlayerInfos.OnPlayerLeaveTeamDelInfo( playerID )) )
//	//{
//	//	D_ERROR( "CMuxCopyInfo::OnPlayerLeaveSTeamProc, 清玩家(%d,%d)的曾进副本信息失败，当前副本ID号%d\n", playerID.wGID, playerID.dwPID, GetCopyID() );
//	//	return false;
//	//}
//
//	//找到playerID所指玩家，检查玩家当前是否在此副本内，若在此副本，则发起踢此玩家；
//	CGateSrv* pGateSrv = CManG_MProc::FindProc( playerID.wGID );
//	if ( NULL == pGateSrv )
//	{
//		D_ERROR( "CMuxCopyInfo::OnPlayerLeaveSTeamProc,找不到GateSrv%d\n", playerID.wGID );
//		return false;
//	}
//	CPlayer* pLeaveSTeamPlayer = pGateSrv->FindPlayer( playerID.dwPID );
//	if( NULL == pLeaveSTeamPlayer )
//	{
//		D_DEBUG( "CMuxCopyInfo::OnPlayerLeaveSTeamProc,离开副本组队的玩家(%d,%d)不在本mapsrv上\n", playerID.wGID, playerID.dwPID );
//		return true;
//	}
//
//	if ( !(pLeaveSTeamPlayer->IsPlayerCurInCopy()) )
//	{
//		D_DEBUG( "CMuxCopyInfo::OnPlayerLeaveSTeamProc,离开副本组队的玩家(%d,%d)当前不在副本中\n", playerID.wGID, playerID.dwPID );
//		return true;
//	}
//
//	if ( pLeaveSTeamPlayer->IsPlayerCurPCopy() )
//	{
//		D_DEBUG( "CMuxCopyInfo::OnPlayerLeaveSTeamProc,离开副本组队的玩家(%d,%d)当前在伪副本中\n", playerID.wGID, playerID.dwPID );
//		return true;
//	}
//
//	//该玩家当前在真副本中；	
//
//	unsigned int copyID=0;
//	unsigned short copyMapID=0;
//	if ( !pLeaveSTeamPlayer->GetCurCopyMapInfo( copyID, copyMapID ) )
//	{
//		D_ERROR( "CMuxCopyInfo::OnPlayerLeaveSTeamProc, 玩家%s在副本中，但取其当前副本ID号失败\n", pLeaveSTeamPlayer->GetAccount() );
//		return false;
//	}
//
//	if ( copyID == GetCopyID() )
//	{
//		//玩家当前在本副本地图内；
//		D_DEBUG( "玩家%s离开副本组队时，发起踢出组队所拥有的副本%d\n", pLeaveSTeamPlayer->GetAccount(), copyID );
//		pLeaveSTeamPlayer->IssuePlayerLeaveCopyHF();//发起踢此玩家出副本；
//	}
//
//	return true;
//}

///新玩家进入副本；
bool CMuxCopyInfo::PlayerEnterCopy( CPlayer* pEnterPlayer, unsigned short gridX, unsigned short gridY )
{
	if ( NULL == pEnterPlayer )
	{
		D_ERROR( "PlayerEnterCopy，副本%d，输入玩家空\n", GetCopyID() );
		return false;
	}

	if ( m_curPlayerNum>=ARRAY_SIZE(m_CopyPlayers) )
	{
		D_ERROR( "玩家%s欲进副本%d时，副本中人数超过了队伍人数上限\n", pEnterPlayer->GetAccount(), GetCopyID() );
		return false;
	}

	if ( !IsGridValid( gridX, gridY ) )
	{
		//点超出范围，添加失败;
		D_ERROR( "CMuxCopyInfo::PlayerEnterCopy，玩家%s的副本地图出现点%d,%d越界\n", pEnterPlayer->GetAccount(), gridX, gridY );
		return false;
	}

	if ( IsDelSelfCopyIssued() )
	{
		D_WARNING( "PlayerEnterCopy,小概率事件,copyid:%d 向center发del self quest之后，收到玩家%s进入请求!\n", GetCopyID(), pEnterPlayer->GetAccount() );
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( gridX, gridY );
	if ( NULL == pBlock )
	{
		//找不到对应块指针，不可能错误；
		D_ERROR( "CMuxCopyInfo::PlayerEnterCopy，玩家%s地图出现点%d,%d对应块空\n", pEnterPlayer->GetAccount(), gridX, gridY );
		return false;
	}

	pBlock->AddPlayerToBlock( pEnterPlayer );//玩家加入块；

	pEnterPlayer->SetPlayerPosInCopy( m_curPlayerNum );
	//MuxMapSpace::GridToBlockPos( pEnterPlayer->GetPosX()
	//	, pEnterPlayer->GetPosY(), m_PlayerPos[m_curPlayerNum].blockX, m_PlayerPos[m_curPlayerNum].blockY );//置相应的m_PlayerPos;
	m_CopyPlayers[m_curPlayerNum] = pEnterPlayer;
	++ m_curPlayerNum;//副本中玩家数++;
	//m_ActiveBlocks.SetActiveCenterBlocks( m_curPlayerNum, m_PlayerPos );
	m_ActiveBlocks.NewActiveCenterBlocksAdd( pBlock->GetBlockX(), pBlock->GetBlockY() );

	D_DEBUG( "玩家%s进入副本(uid:%d),副本中玩家数%d\n", pEnterPlayer->GetAccount(), m_copyID, m_curPlayerNum );
	m_GroupNotiInfo.AddGroupNotiPlayerID( pEnterPlayer->GetFullID() );//副本中广播人员固定，不必每次在周围寻找广播对象；
	pEnterPlayer->OnPlayerEnterCurCopy( this );

	//待添加,09.09.14
	//0、检查
	//1、检查玩家身上是否保存了本副本类型的阶段信息，如有，则将玩家刷至相应阶段点；
	//2、检查玩家身上是否保存了本id副本的进度信息，如有，则将玩家刷至上次退出点；
	//3、调用副本相应的玩家进入函数；
	//4、通知所有副本内玩家及周边怪物，本玩家进入副本；

	return true;
}

///原副本玩家离开副本；
bool CMuxCopyInfo::PlayerLeaveCopy( CPlayer* pLeavePlayer )
{
	if ( NULL == pLeavePlayer )
	{
		D_ERROR( "CMuxCopyInfo::PlayerLeaveCopy, 副本中删玩家时，输入玩家指针空\n" );
		return false;
	}

	//副本中的玩家数
	if ( m_curPlayerNum <= 0 )
	{
		D_ERROR( "CMuxCopyInfo::PlayerLeaveCopy, 副本中删玩家%s时，发现副本中玩家数为%d\n", pLeavePlayer->GetAccount(), m_curPlayerNum );
		return false;
	}

	unsigned short gridX=pLeavePlayer->GetPosX(), gridY=pLeavePlayer->GetPosY();
	if ( !IsGridValid( gridX, gridY ) )
	{
		//点超出范围，删除失败;
		D_ERROR( "CMuxCopyInfo::PlayerLeaveCopy, 玩家所在的地图点%d,%d越界\n", gridX, gridY );		
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( gridX, gridY );
	if ( NULL == pBlock )
	{
		//找不到对应块指针，不可能错误；
		D_ERROR( "CMuxCopyInfo::PlayerLeaveCopy, 玩家所在的地图点%d,%d对应块空\n", gridX, gridY );		
		return false;
	}
	
	pBlock->DeletePlayerFromBlock( pLeavePlayer );

	bool isFound = false;
	int foundpos = -1;
	for ( int i=0; i<m_curPlayerNum; ++i )
	{
		if ( i >= ARRAY_SIZE(m_CopyPlayers) )
		{
			D_ERROR( "PlayerLeaveCopy, 越界, m_curPlayerNum(%d)\n", m_curPlayerNum );
			return false;
		}
		if ( m_CopyPlayers[i] == pLeavePlayer )
		{
			foundpos = i;
			isFound = true;
			break;
		}
	}

	if ( ( !isFound ) || ( foundpos < 0 ) )
	{
		D_ERROR( "副本中删玩家时，发现副本中无该玩家\n" );
		return false;//副本中无此玩家；
	}

	if ( ( m_curPlayerNum-1 < 0) || ( m_curPlayerNum-1 >= ARRAY_SIZE(m_CopyPlayers) )
		|| ( foundpos >= ARRAY_SIZE(m_CopyPlayers) )
		)
	{
		D_ERROR( "副本中删玩家时，越界，m_curPlayerNum(%d)-1或foundpos(%d)\n", m_curPlayerNum, foundpos );
		return false;
	}

	//使用最尾玩家覆盖发现位置玩家，然后删去尾部玩家，以保证有效玩家总在数组的前列；
	m_CopyPlayers[m_curPlayerNum-1]->SetPlayerPosInCopy( foundpos );//玩家新的数组位置;
	m_CopyPlayers[foundpos] = m_CopyPlayers[m_curPlayerNum-1/*最大玩家位置=玩家数-1*/];
	//MuxMapSpace::GridToBlockPos( m_CopyPlayers[foundpos]->GetPosX()
	//	, m_CopyPlayers[foundpos]->GetPosY(), m_PlayerPos[foundpos].blockX, m_PlayerPos[foundpos].blockY );//置相应的m_PlayerPos;
	m_CopyPlayers[m_curPlayerNum-1] = NULL;
	--m_curPlayerNum;//当前副本中玩家数-1;
	//m_ActiveBlocks.SetActiveCenterBlocks( m_curPlayerNum, m_PlayerPos );
	m_ActiveBlocks.OrgActiveCenterBlocksDel( pBlock->GetBlockX(), pBlock->GetBlockY() );

	D_DEBUG( "玩家%s离开副本(uid:%d), 当前副本中人数%d\n", pLeavePlayer->GetAccount(), m_copyID, m_curPlayerNum );
	pLeavePlayer->OnPlayerLeaveCurCopy();//设置玩家当前已离开副本；
	if ( !(m_GroupNotiInfo.DelGroupNotiPlayerID( pLeavePlayer->GetFullID() )) )
	{
		D_ERROR( "将玩家(%d:%d)从副本(%d:%d)中广播信息中删去时，找不到该玩家\n"
			, pLeavePlayer->GetGID(), pLeavePlayer->GetPID(), GetCopyID(), GetCopyMapID() );
	}

	//待添加，09.09.14
	//如果离开的是队长，做队长特殊处理；
	//通知所有玩家及周边怪物该玩家离开；

	return true;

}

///add rideteam to map block;
bool CMuxCopyInfo::AddRideteamToCopyMap( RideTeam* pTeam, unsigned short gridX, unsigned short gridY )
{
	if ( NULL == pTeam )
	{
		D_ERROR( "CMuxCopyInfo::AddRideteamToCopyMap, 输入参数空\n" );
		return false;
	}

	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( gridX, gridY, blockX, blockY );
	MuxMapBlock* pBlock = GetMapBlockByBlock( blockX, blockY );
	if ( NULL == pBlock )
	{
		return false;
	}

	pBlock->AddRideTeam( pTeam );

	return true;
}

///delete rideteam from map block;
bool CMuxCopyInfo::DelRideteamFromCopyMap( RideTeam* pTeam, unsigned short gridX, unsigned short gridY )
{
	if ( NULL == pTeam )
	{
		D_ERROR( "CMuxCopyInfo::DelRideteamFromCopyMap, 输入参数空\n" );
		return false;
	}

	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( gridX, gridY, blockX, blockY );
	MuxMapBlock* pBlock = GetMapBlockByBlock( blockX, blockY );
	if ( NULL == pBlock )
	{
		return false;
	}

	pBlock->DeleteRideTeam( pTeam );

	return true;
}

/////temp for monster wrapper;
//bool CMuxCopyInfo::GetScopeMonsters( unsigned short centerX, unsigned short centerY, ScopeMonsterInfo* scopeMonstrArr, int inArrSize, int& arrSize )
//{
//	if ( NULL == scopeMonstrArr )
//	{
//		D_ERROR( "CMuxCopyInfo::GetScopeMonsters, 输入参数空\n" );
//		return false;
//	}
//
//	arrSize = 0;
//
//	TYPE_POS blockX,blockY;
//	MuxMapSpace::GridToBlockPos( centerX, centerY, blockX, blockY );
//	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
//	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	{
//		//输入点为无效点；
//		return false;
//	}
//	MuxMapBlock* pBlock = NULL;
//	int tempArrSize = 0;
//	//int nRange = pSkill->GetRange();
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pBlock = GetMapBlockByBlock( x, y );
//			if ( NULL != pBlock )
//			{
//				if( !pBlock->GetBlockMonsterID( scopeMonstrArr, inArrSize, tempArrSize ) )
//				{
//					return true;
//				}
//			}
//		}
//	}
//
//	return true;
//}
//
/////temp for monster wrapper;
//bool CMuxCopyInfo::GetScopePlayers( unsigned short centerX, unsigned short centerY, ScopePlayerInfo* scopePlayerArr, int inArrSize, int& arrSize )
//{
//	if ( NULL == scopePlayerArr )
//	{
//		D_ERROR( "CMuxMap::GetScopePlayers, 输入参数空\n" );
//		return false;
//	}
//
//	arrSize = 0;
//
//	TYPE_POS blockX,blockY;
//	MuxMapSpace::GridToBlockPos( centerX, centerY, blockX, blockY );
//	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
//	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	{
//		//输入点为无效点；
//		return false;
//	}
//	MuxMapBlock* pBlock = NULL;
//	int tempArrSize = 0;
//	//int nRange = pSkill->GetRange();
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pBlock = GetMapBlockByBlock( x, y );
//			if ( NULL != pBlock )
//			{
//				if( !pBlock->GetBlockPlayerInfo( scopePlayerArr, inArrSize, tempArrSize ) )
//				{
//					return true;
//				}
//			}
//		}
//	}
//
//	return true;
//}

bool CMuxCopyInfo::OnPlayerSkillGetAoeTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster )
{
	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("找不到战斗脚本\n");
		return false;
	}

	vecTargetPlayer.clear();
	vecMonster.clear();
	TYPE_POS blockX = 0, blockY = 0;

	MuxMapSpace::GridToBlockPos( aoePosX, aoePosY, blockX, blockY );
	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PtPlayerGetSkillTarget( aoePosX, aoePosY, range, pUseSkillPlayer, pScript, vecTargetPlayer, vecMonster );
			}
		}
	}
	return true;
}


bool CMuxCopyInfo::OnMonsterSkillGetAoeTarget( CMonster* pUseSkillMonster, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster )
{
	vecTargetPlayer.clear();
	vecMonster.clear();
	TYPE_POS blockX = 0, blockY = 0;

	MuxMapSpace::GridToBlockPos( aoePosX, aoePosY, blockX, blockY );
	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PtMonsterGetSkillTarget( aoePosX, aoePosY, range, pUseSkillMonster, vecTargetPlayer, vecMonster );
			}
		}
	}
	return true;
}


bool CMuxCopyInfo::OnPlayerSkillGetTeamTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer )
{
	if ( NULL == pUseSkillPlayer )
	{
		D_ERROR( "CMuxCopyInfo::OnPlayerSkillGetTeamTarget, NULL == pUseSkillPlayer\n" );
		return false;
	}
	vecTargetPlayer.clear();
	if( pUseSkillPlayer->GetGroupTeam() == NULL )
	{
		vecTargetPlayer.push_back( pUseSkillPlayer );
		return true;
	}

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("找不到战斗脚本\n");
		return false;
	}

	vecTargetPlayer.clear();
	TYPE_POS blockX = 0, blockY = 0;

	MuxMapSpace::GridToBlockPos( aoePosX, aoePosY, blockX, blockY );
	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PtPlayerSkillGetTeamTarget( aoePosX, aoePosY, range, pUseSkillPlayer, pScript, vecTargetPlayer );
			}
		}
	}
	return true;
}





//获取范围内的玩家和怪物
bool CMuxCopyInfo::GetScopeCreature( TYPE_POS posX, TYPE_POS posY, int range, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster )
{
	vecPlayer.clear();
	vecMonster.clear();

	unsigned short blockX = 0, blockY = 0;
	MuxMapSpace::GridToBlockPos( posX, posY, blockX, blockY );
	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PtGetScopeCreature( posX, posY, range, vecPlayer, vecMonster );
			}
		}
	}
	return true;
}





///发出nomoreplayer请求给center;
void CMuxCopyInfo::IssueNomorePlayerQuest()
{
	if ( IsNomorePlayerIssued() )
	{
		D_WARNING( "IssueNomorePlayerQuest,重复issue nomoreplayer, copyid:%d\n", GetCopyID() );
		return;
	}

	D_DEBUG( "副本%d，通知center，no more player，IssueNomorePlayerQuest\n", GetCopyID() );

	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("IssueNomorePlayerQuest 找不到合适的GateSrv用于发送消息\n" );
		return;
	}

	MGCopyNomorePlayer nomorePlayer;
	nomorePlayer.copyid = GetCopyID();
	MsgToPut* pNewMsg = CreateSrvPkg( MGCopyNomorePlayer, pGateSrv, nomorePlayer );
	if ( NULL == pNewMsg )
	{
		D_ERROR("IssueNomorePlayerQuest CreateSrvPkg 失败\n" );
		return;
	}

	pGateSrv->SendPkgToGateServer( pNewMsg );

	SetNomorePlayerIssued();

	return;
}

///发起设置m_isDetectDelCond
void CMuxCopyInfo::IssueDetectDelCond() 
{ 
	if ( IsDetectDelCondFlag() )
	{
		D_WARNING( "IssueDetectDelCond,重复issue detect del cond, copyid:%d\n", GetCopyID() );
		return;
	}

	D_DEBUG( "副本%d，设置检测删除标记，IssueDetectDelCond\n", GetCopyID() );

	SetDetectDelCondFlag();
	m_delCondFufilTime = 0;//重置条件满足时刻；

	return;
};

///发出delselfcopy请求给center;
void CMuxCopyInfo::IssueDelSelfCopyQuest()
{
	if ( IsDelSelfCopyIssued() )
	{
		//D_WARNING( "IssueDelSelfCopyQuest,重复issue del self copy, copyid:%d\n", GetCopyID() );
		return;
	}

	D_DEBUG( "副本%d，删除条件已满足，向center发删除请求，IssueDelSelfCopyQuest\n", GetCopyID() );

	CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
	if( NULL == pGateSrv )
	{
		D_ERROR("IssueDelSelfCopyQuest 找不到合适的GateSrv用于发送消息，副本%d\n", GetCopyID() );
		return;
	}

	MGDelCopyQuest delCopyQuest;
	delCopyQuest.copyid = GetCopyID();
	MsgToPut* pNewMsg = CreateSrvPkg( MGDelCopyQuest, pGateSrv, delCopyQuest );
	if ( NULL == pNewMsg )
	{
		D_ERROR("IssueDelSelfCopyQuest CreateSrvPkg 失败，副本%d\n", GetCopyID() );
		return;
	}

	pGateSrv->SendPkgToGateServer( pNewMsg );

	SetDelSelfCopyIssued();

	return;
}

///置踢所有副本玩家标记
void CMuxCopyInfo::IssueKickAllCopyPlayers()
{
	if ( IsKickAllCopyPlayerIssued() )
	{
		D_WARNING( "IssueKickAllCopyPlayers,重复标记删除所有玩家, copyid:%d\n", GetCopyID() );
		return;
	}

	//全放到时钟中去Issue请求,KickAllCopyPlayers();
	D_DEBUG( "副本%d，发起踢所有副本中玩家，IssueKickAllCopyPlayers\n", GetCopyID() );

	SetKickAllCopyPlayerIssued();

	return;
}

///发出踢所有玩家指令
void CMuxCopyInfo::KickAllCopyPlayers()
{
	CPlayer* pPlayer = NULL;
	for ( int i=0; i<m_curPlayerNum; ++i )
	{
		if ( i >= ARRAY_SIZE(m_CopyPlayers) )
		{
			D_ERROR( "CMuxCopyInfo::KickAllCopyPlayers, 越界, m_curPlayerNum(%d)\n", m_curPlayerNum );
			return;
		}
		pPlayer = m_CopyPlayers[i];
		if ( NULL != pPlayer )
		{
			pPlayer->IssuePlayerLeaveCopyHF();
		}
	}

	return;
}


//杀死所有地图玩家
void CMuxCopyInfo::KillAllCopyPlayers( unsigned int race )
{
	CPlayer* pPlayer = NULL;
	PlayerID attackID;
	attackID.dwPID = 0;
	attackID.wGID = 0;
	for ( int i=0; i<m_curPlayerNum; ++i )
	{
		if ( i >= ARRAY_SIZE(m_CopyPlayers) )
		{
			D_ERROR( "CMuxCopyInfo::KillAllCopyPlayers, 越界, m_curPlayerNum(%d)\n", m_curPlayerNum );
			return;
		}
		pPlayer = m_CopyPlayers[i];
		if ( NULL != pPlayer )
		{
			pPlayer->SubPlayerHp( pPlayer->GetCurrentHP(), NULL, attackID );
		}
	}

	return;
}


///副本地图设置所有玩家的outmapinfo;
bool CMuxCopyInfo::SetAllPlayerOutPos( unsigned short mapid, unsigned short posX, unsigned short posY, unsigned short extent )
{
	CPlayer* pPlayer = NULL;
	for ( int i=0; i<m_curPlayerNum; ++i )
	{
		if ( i >= ARRAY_SIZE(m_CopyPlayers) )
		{
			D_ERROR( "CMuxCopyInfo::SetAllPlayerOutPos, 越界, m_curPlayerNum(%d)\n", m_curPlayerNum );
			return false;
		}
		pPlayer = m_CopyPlayers[i];
		if ( NULL != pPlayer )
		{
			pPlayer->SetPlayerOutPos( mapid, posX, posY, extent );
		}
	}

	return true;
}

#ifdef HAS_PATH_VERIFIER
void CMuxCopyInfo::OnBlockQuadEnable(int regionId)
{
	CBlockQuadManager::BQData *pBlockQuad = CBlockQuadManager::GetBlockRanage(regionId);
	if(NULL == pBlockQuad)
		return;

	unsigned short minBlockX, minBlockY, maxBlockX, maxBlockY;
	minBlockX = minBlockY = maxBlockX = maxBlockY = 0;
	MuxMapSpace::GridToBlockPos(pBlockQuad->minX, pBlockQuad->minY, minBlockX, minBlockY);
	MuxMapSpace::GridToBlockPos(pBlockQuad->maxX, pBlockQuad->maxY, maxBlockX, maxBlockY);

	MuxMapBlock* pBlock = NULL;
	std::vector<CPlayer*> vecPlayer;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->GetPlayersInRect(pBlockQuad->minX, pBlockQuad->minY, pBlockQuad->maxX, pBlockQuad->maxY, vecPlayer);
			}
		}
	}

	//让块内玩家死亡
	size_t ssize = vecPlayer.size();
	if(ssize > 0)
	{
		CPlayer *pPlayer = NULL;
		for(int i = 0; i < ssize; i++)
		{
			pPlayer = vecPlayer[i];

			if((NULL != pPlayer) && (pPlayer->IsAlive()))
				pPlayer->SubPlayerHp( pPlayer->GetCurrentHP(), NULL, pPlayer->GetFullID() );
		}
	}

	return;
}
#endif/*HAS_PATH_VERIFIER*/

