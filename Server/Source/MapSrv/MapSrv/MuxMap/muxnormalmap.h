﻿/**
* @file muxnormalmap.h
* @brief 
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: muxnormalmap.h
* 摘    要: 
* 作    者: dzj
* 开始日期: 2009.10.07
* 
*/

#ifndef MUX_NORMALMAP_H
#define MUX_NORMALMAP_H

#include "MapBlock.h"

class CMuxMap;

class CMuxNormalMapInfo
{
	static const int MAX_PLAYERNUM_INMAP = 3600;//同地图最多可拥有的玩家数量；
	friend class CMuxMap;

private:
	CMuxNormalMapInfo();

public:
	CMuxNormalMapInfo( unsigned short gridWidth, unsigned gridHeight );

	~CMuxNormalMapInfo();

	PoolFlagDefine()
	{
		MuxNormalMapInfoInit();
	}

	///重置本普通地图信息,普通地图回池时调用(实际普通地图没有使用池管理，因此实际内容空)；
	bool ResetNormalMapInfo();

	///普通地图初始化，由PoolObjInit调用((实际普通地图没有使用池管理，因此实际内容空))；
	void MuxNormalMapInfoInit();

private:
	///向周围怪物广播玩家的新移动消息；
	void PlayerNewMoveMonsterBrocast( CPlayer* pMovePlayer );
	///玩家死亡通知；
	void PlayerDieNotify( CPlayer* pDiePlayer );
	///玩家出现块相关通知；
	void PlayerAppearNotifyByBlockPos( CPlayer* pAppearPlayer, bool isNotiSelf/*是否通知出现者本人*/
		, unsigned short blockX, unsigned short blockY );

	//通知周围格特殊怪物 离开或者进入视野
	void MonsterLeaveViewNotify( CMonster* pMonster );//copy...

	///玩家跨块相关通知；
	void PlayerBeyondBlockNotifyByBlockPos( CPlayer* pBeyondPlayer );
   
	///notify monster player hp add event;
	void NotifySurMonsterPlayerHpAdd( PlayerID addStarter, PlayerID addTarget, int hpAdd, unsigned short centerX, unsigned short centerY );

	///玩家离开块相关通知；
	void PlayerLeaveNotifyByBlockPos( CPlayer* pLeavePlayer, unsigned short blockX, unsigned short blockY );

	///怪物出生,调用时怪物已加入地图；
	bool MonsterBornNotify( CMonster* pBornMonster );

	///@brief:将该道具包的信息,发送给周围的玩家
	void NotifyItemPkgsAppearEx( CItemsPackage* pItemPkg );

	///notify itempkf first appear;
	void NotifyItemPkgFirstAppearEx( CItemsPackage* pItemPkg );

	//检查地图上某点是否可以添加塔
	bool IsEnabelAddTower(TYPE_POS gridX, TYPE_POS gridY);

private:
	///用块坐标取块；
	inline MuxMapBlock* GetMapBlockByBlock( unsigned short inBlockX, unsigned short inBlockY )
	{
		if ( IsBlockValid( inBlockX, inBlockY ) )
		{
			return &(m_arrMapBlocks[inBlockY][inBlockX]);
		} else {
			return NULL;
		}
	}

	///用格坐标取块；
	inline MuxMapBlock* GetMapBlockByGrid( unsigned short inGridX, unsigned short inGridY )
	{
		unsigned short blockX=0, blockY=0;

		MuxMapSpace::GridToBlockPos( inGridX, inGridY, blockX, blockY );

		return GetMapBlockByBlock( blockX, blockY );
	}

	///取指定块周边块的坐标范围；
	bool GetBlockNearbyScope( unsigned short blockX, unsigned short blockY
		, unsigned short& minBlockX, unsigned short& minBlockY, unsigned short& maxBlockX, unsigned short& maxBlockY );

	///块坐标是否有效；
	inline bool IsBlockValid( unsigned short blockX, unsigned short blockY ) 
	{
		return (blockX<GetBlockWidth()) && (blockY<GetBlockHeight());
	}

	///格坐标是否有效；
	inline bool IsGridValid( unsigned short gridX, unsigned short gridY ) 
	{
		return (gridX<GetGridWidth()) && (gridY<GetGridHeight());
	}

private:
	///累计块内玩家怪物数量；
	void CalBlockPlayerMonsterNum( unsigned short blockX, unsigned blockY, unsigned int& toAddPlayerNum, unsigned int& toAddMonsterNum );

	/////取指定点视野内玩家与怪物数量；
	//bool GetPosSurPlayerMonsterNum( unsigned short posX, unsigned short posY, unsigned int& playerNum, unsigned int& monsterNum );

	//获取范围内的玩家和怪物
	bool GetScopeCreature( TYPE_POS posX, TYPE_POS posY, int range, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster );

	/////temp for monster wrapper;
	//bool GetScopeMonsters( unsigned short centerX, unsigned short centerY, ScopeMonsterInfo* scopeMonstrArr, int inArrSize, int& arrSize );

	/////temp for monster wrapper;
	//bool GetScopePlayers( unsigned short centerX, unsigned short centerY, ScopePlayerInfo* scopePlayerArr, int inArrSize, int& arrSize );

	///增加对应八方向的新块计算函数，输入：旧块X，旧块Y，新块X， 新块Y，输出：新视野块X，新视野块Y（最多5组，不允许跳块，即，输入必须为邻块）,???,贺轶男检查;
	bool GetNewViewportBlocks( unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY
		,  bool& isJump/*是否跳块，输入非相邻块*/, unsigned short& viewportBlockNum/*新视野块数目*/, unsigned short outBlockX[5], unsigned short outBlockY[5] );

private:
	///调整玩家所在块
	void AdjustPlayerBlock( bool isManualSet/*是否手动调，手动调时必须周围全通知到而不是只通知新视野*/, CPlayer* pPlayer, unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY );

	///调整怪物所在块
	bool AdjustMonsterPos( CMonster* pMonster, unsigned short orgGridX, unsigned short orgGridY, unsigned short newGridX, unsigned short newGridY
		, bool isForceAndNotNotiMove/*是否强制设位置且不通知周围此移动，怪物移动状态下改变位置为false，非移动状态下改变位置为true(目前只有被击退一种情形)*/ );

private:
	///add player to map block;
	bool AddPlayerToNormalMap( CPlayer* pPlayer, unsigned short gridX, unsigned short gridY );

	///add monster to map block;
	bool AddMonsterToNormalMap( CMonster* pMonster, unsigned short gridX, unsigned short gridY );

	///delete player from map block;
	bool DelPlayerFromNormalMap( CPlayer* pPlayer );

	///delete monster from map block;
	bool DelMonsterFromNormalMap( CMonster* pMonster );

	///add pkg to map block;
	bool AddItemPkgToNormalMap( CItemsPackage* pitemPkg );

	///delete pkg from map block;
	bool DelItemPkgFromNormalMap( CItemsPackage* pitemPkg );

	///add rideteam to map block;
	bool AddRideteamToNormalMap( RideTeam* pTeam, unsigned short gridX, unsigned short gridY );

	///delete rideteam from map block;
	bool DelRideteamFromNormalMap( RideTeam* pTeam, unsigned short gridX, unsigned short gridY );

	///发起踢所有人上半部，且内部不通知每个人消息，在下面使用广播通知；
	bool IssueAllPlayerLeavePCopyHF( bool isNotiPlayerThis/*函数内部是否直接通知player或稍后使用广播通知*/ );

	/////对每一玩家执行PlayerLeaveHF时，元素被遍历时执行，由hash表内部调用；
	//static bool OnPlayerEleIssueLeaveHFExplored( HashTbPtrEle<CPlayer>* beExploredEle, void* pParam );

	bool OnPlayerSkillGetAoeTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster );
	bool OnMonsterSkillGetAoeTarget( CMonster* pUseSkillMonster, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster );
	bool OnPlayerSkillGetTeamTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer );


private:
	inline unsigned short GetGridWidth()  { return m_gridWidth; };
	inline unsigned short GetGridHeight() { return m_gridHeight; };
	inline unsigned short GetBlockWidth() { return m_blockWidth; };
	inline unsigned short GetBlockHeight(){ return m_blockHeight; };
	inline void GetValidBlock( int nX, int nY, int nScope, int& nMinX, int& nMaxX, int& nMinY, int& nMaxY )
	{
		int tmpX = nX - nScope;
		nMinX = (tmpX<0) ? 0:tmpX;
		tmpX = nX + nScope;
		nMaxX = (tmpX >= m_blockWidth) ? (m_blockWidth-1):tmpX;

		int tmpY = nY - nScope;
		nMinY = (tmpY<0) ? 0:tmpY;
		tmpY = nY + nScope;
		nMaxY = (tmpY >= m_blockHeight) ? ( m_blockHeight-1 ) :tmpY;
	}

private:
	///检查玩家是否在地图内(玩家指针不知是否仍有效)；
	bool IsDoubtPlayerPtrInMap( CPlayer* pPlayer );

	///取地图内的怪物指针；
	CMonster* GetMonsterPtrInMap( unsigned long monsterID );

	inline unsigned int GetMapPlayerNum() { return m_curMapPlayerNum; }

private:
	void SetCityInfo( CityInfo* pCityInfo );

	void AddCityExp( unsigned int addExp );

	unsigned char GetCityRace();

	bool IsHaveCityInfo();

	CityInfo* GetCityInfo();

	void ClearCityInfo();

private:
	template< typename T_Msg >
	void NotifyAllUseBrocast( T_Msg* pMsg )
	{
		TRY_BEGIN;

		if ( ( m_curMapPlayerNum<0 )
			|| ( m_curMapPlayerNum>MAX_PLAYERNUM_INMAP )
			)
		{
			D_ERROR( "NotifyAllUseBrocast,CMD:%d,地图内当前人数%d不对\n", T_Msg::wCmd, m_curMapPlayerNum );
			return;
		}

		bool isNeedNoti = false;
		CPlayer* pPlayer = NULL;

		for ( int i=0; i<m_curMapPlayerNum; ++i )
		{
			pPlayer = m_arrMapPlayers[i];
			if ( NULL == pPlayer )
			{
				D_ERROR( "NotifyAllUseBrocast,CMD:%d,地图玩家数组位置%d元素空\n", T_Msg::wCmd, i );
				continue;
			}
			if ( m_GroupNotiInfo.AddGroupNotiPlayerID( pPlayer->GetFullID() ) )
			{				
				//添加一个发送目标成功；
				isNeedNoti = true;
				continue;
			} else {
				//达到单个gate发送目标数上限，先发送完现有目标；
				m_GroupNotiInfo.NotiGroup<T_Msg>( pMsg );
				m_GroupNotiInfo.ResetGroupInfo();
				isNeedNoti = false;
				bool isok = m_GroupNotiInfo.AddGroupNotiPlayerID( pPlayer->GetFullID() );//再次尝试添加发送目标；
				if ( !isok )
				{
					D_ERROR( "不可能错误，NotifyAllUseBrocast,CMD:%d,无法添加发送目标\n", T_Msg::wCmd );
					m_GroupNotiInfo.ResetGroupInfo();
					return;
				}
				isNeedNoti = true;
			}
		}

		//最后一批待发送目标；
		if ( isNeedNoti )
		{
			m_GroupNotiInfo.NotiGroup<T_Msg>( pMsg );
			m_GroupNotiInfo.ResetGroupInfo();
		}

		return;
		TRY_END;
		return;
	}

    template< typename T_Msg >
	void NotifyBlocks( T_Msg* pMsg, vector<MuxMapBlock*>& vecBlocks )
	{
#ifdef GROUP_NOTI
		GroupNotifyBlocks<T_Msg>( pMsg, vecBlocks );
#else //GROUP_NOTI
		D_ERROR( "GroupNotifyBlocks, macro GROUP_NOTI not defined\n" );
#endif //GROUP_NOTI
	}

	///向周边玩家广播；
	template< typename T_Msg >
	void NotifySurrounding( T_Msg* pMsg, int nPosX, int nPosY ) 
	{
		unsigned short blockX=0, blockY=0;

		MuxMapSpace::GridToBlockPos( nPosX, nPosY, blockX, blockY );
#ifdef GROUP_NOTI
		GroupSurNotify<T_Msg>( pMsg, blockX, blockY );
#else //GROUP_NOTI
		D_ERROR( "NotifySurrounding, macro GROUP_NOTI not defined\n" );
#endif //GROUP_NOTI

		return; 
	};

	template<typename T_Msg>
	void NotifySurroundingExceptPlayer( T_Msg* pMsg, int nPosX, int nPosY , const PlayerID& exceptPlayerID )
	{
		unsigned short blockX=0, blockY=0;

		MuxMapSpace::GridToBlockPos( nPosX, nPosY, blockX, blockY );
#ifdef GROUP_NOTI
		GroupSurNotifyExceptPlayer<T_Msg>( pMsg, blockX, blockY,exceptPlayerID );
#else //GROUP_NOTI
		D_ERROR( "NotifySurrounding, macro GROUP_NOTI not defined\n" );
#endif //GROUP_NOTI
		return; 
	}

	template<typename T_Msg>
	void GroupSurNotifyExceptPlayer( T_Msg* pMsg, unsigned short blockX, unsigned short blockY,const PlayerID& exceptPlayerID ) 
	{
		TRY_BEGIN;

		unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

		if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
		{
			//输入点为无效点；
			return;
		}

		MuxMapBlock* pBlock = NULL;
		bool isNeedNoti = false;
		bool isSomeOneAdd = false;
		for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
		{
			for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
			{
				pBlock = GetMapBlockByBlock( x, y );
				if ( NULL != pBlock )
				{
					pBlock->FillGroupInfoWithSelfPlayerExcept( m_GroupNotiInfo, exceptPlayerID, isSomeOneAdd );
					if ( isSomeOneAdd )
					{
						isNeedNoti = true;//有需要通知的人；
					}
				}
			}
		}

		if ( isNeedNoti )
		{
			m_GroupNotiInfo.NotiGroup<T_Msg>( pMsg );
			m_GroupNotiInfo.ResetGroupInfo();
		}

		return;
		TRY_END;
		return;
	};

    template< typename T_Msg >
	void GroupNotifyBlocks( T_Msg* pMsg, vector<MuxMapBlock*>& vecBlocks )
	{
		TRY_BEGIN;

		if (vecBlocks.empty())
		{
			return;//欲广播块空；
		}

		MuxMapBlock* pBlock = NULL;
		bool isNeedNoti = false;
		bool isSomeOneAdd = false;
		for ( vector<MuxMapBlock*>::iterator iter=vecBlocks.begin(); iter!=vecBlocks.end(); ++iter )
		{
			pBlock = *iter;
			if ( NULL != pBlock )
			{
				pBlock->FillGroupInfoWithSelfPlayer( m_GroupNotiInfo, isSomeOneAdd );
				if ( isSomeOneAdd )
				{
					isNeedNoti = true;//有需要通知的人；
				}
			}
		}

		if ( isNeedNoti )
		{
			m_GroupNotiInfo.NotiGroup<T_Msg>( pMsg );
			m_GroupNotiInfo.ResetGroupInfo();
		}

		return;
		TRY_END;
		return;
	}// void GroupNotifyBlocks( T_Msg* pMsg, vector<MuxMapBlock*>& vecBlocks )

	///使用组播向周边块上的玩家广播；
	template< typename T_Msg >
	void GroupSurNotify( T_Msg* pMsg, unsigned short blockX, unsigned short blockY ) 
	{
		TRY_BEGIN;

		unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

		if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
		{
			//输入点为无效点；
			return;
		}

		//if ( T_Msg::wCmd == MGMonsterMove::wCmd )
		//{
		//	m_GroupNotiInfo.DebugIDIfAny();
		//}

		MuxMapBlock* pBlock = NULL;
		bool isNeedNoti = false;
		bool isSomeOneAdd = false;
		for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
		{
			for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
			{
				pBlock = GetMapBlockByBlock( x, y );
				if ( NULL != pBlock )
				{
					pBlock->FillGroupInfoWithSelfPlayer( m_GroupNotiInfo, isSomeOneAdd );
					if ( isSomeOneAdd )
					{
						isNeedNoti = true;//有需要通知的人；
					}
				}
			}
		}

		//if ( T_Msg::wCmd == MGMonsterMove::wCmd )
		//{
		//	m_GroupNotiInfo.DebugIDIfAny();
		//}
		if ( isNeedNoti )
		{
			//if ( T_Msg::wCmd == MGMonsterMove::wCmd )
			//{
			//	D_DEBUG( "GroupSurNotify,通知monstermove\n" );
			//}
			m_GroupNotiInfo.NotiGroup<T_Msg>( pMsg );
			m_GroupNotiInfo.ResetGroupInfo();
		}

		return;
		TRY_END;
		return;
	};

	//杀死地图内所有玩家
	void KillAllPlayerByRace( unsigned int race );
	//传送地图内玩家
	void SwitchMapPlayer( unsigned int race, unsigned int mapID, unsigned int posX1, unsigned int posY1, unsigned int posX2, unsigned int posY2 );
	//删除地图上的怪物
	bool DelTypeMonster( unsigned int monsterClsID );
	bool AddToDelMonsterType( unsigned int monsterClsID );
	//创建地图上玩家的buff
	void CreateMapBuff( unsigned int race, unsigned int buffID, unsigned int buffTime );


#ifdef HAS_PATH_VERIFIER
	void OnBlockQuadEnable(int regionId);
#endif	

private:
	unsigned short   m_gridWidth;//width in grid;
	unsigned short   m_gridHeight;//height in grid;

	unsigned short   m_blockWidth;//地图宽度（以小块计）；
	unsigned short   m_blockHeight;//地图高度（以小块计）；
	MuxMapBlock**    m_arrMapBlocks;//地图各小块；

private:
#ifdef USE_SELF_HASH
	///地图里的怪物与玩家列表
	CPlayer**        m_arrMapPlayers;//地图玩家指针数组(方便遍历、添加与删除，但查找某个PlayerID号玩家有困难，此种查找应通过gatesrv实例的相应接口)；
	int              m_curMapPlayerNum;//地图当前拥有的玩家数量(玩家指针数组的有效位置)

	DsHashTable< HashTbPtrEle<CMonster>, 3000, 2 >* m_pMapMonsters;//怪物可使用hash，因为怪物ＩＤ在同地图不重；
#endif //USE_SELF_HASH

private:
	CityInfo*      m_pCityInfo;			//关卡地图信息

private:
	CGroupNotiInfo  m_GroupNotiInfo;    //组通知玩家信息；

};

#endif //MUX_NORMALMAP_H
