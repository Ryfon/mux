﻿/**
* @file muxnormalmap.cpp
* @brief 
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: muxnormalmap.cpp
* 摘    要: 
* 作    者: dzj
* 开始日期: 2009.10.07
* 
*/

#include "muxnormalmap.h" 
#include "../ItemSkill.h"
#include "../Lua/BattleScript.h"
#include "muxmapbase.h"

CMuxNormalMapInfo::CMuxNormalMapInfo( unsigned short gridWidth, unsigned gridHeight ) 
{
	m_gridWidth = gridWidth;//width in grid;
	m_gridHeight = gridHeight;//height in grid;
	MuxMapSpace::GridToBlockPos( m_gridWidth, m_gridHeight, m_blockWidth, m_blockHeight );
	m_blockWidth += 1;
	m_blockHeight += 1;
	
	m_arrMapBlocks = NEW MuxMapBlock*[m_blockHeight];
	for ( int y=0; y<m_blockHeight; ++y )
	{
		m_arrMapBlocks[y] = NEW MuxMapBlock[m_blockWidth];
		for ( int x=0; x<m_blockWidth; ++x )
		{
			m_arrMapBlocks[y][x].InitMapBlock( x, y );
		}
	}

	m_pCityInfo = NULL;

	m_arrMapPlayers = NEW CPlayer*[MAX_PLAYERNUM_INMAP];//每地图最多可有这么多玩家;
	m_curMapPlayerNum = 0;
	for ( int i=0; i<MAX_PLAYERNUM_INMAP; ++i )
	{
		m_arrMapPlayers[i] = NULL;//初始地图中无任何玩家；
	}
	m_pMapMonsters = NEW DsHashTable< HashTbPtrEle<CMonster>, 3000, 2 >;
	if ( NULL == m_pMapMonsters )
	{
		D_ERROR( "CMuxMap(), NEW m_pMapMonsters失败\n" );
	}

	m_GroupNotiInfo.ResetGroupInfo();
	PoolObjInit();
}

CMuxNormalMapInfo::~CMuxNormalMapInfo()
{
    m_GroupNotiInfo.ResetGroupInfo();
	PoolObjInit();

	if ( NULL != m_pMapMonsters )
	{
		delete m_pMapMonsters; m_pMapMonsters = NULL;
	}

	m_curMapPlayerNum = 0;
	if ( NULL != m_arrMapPlayers )
	{
		delete [] m_arrMapPlayers; m_arrMapPlayers = NULL;
	}

	for ( int y=0; y<m_blockHeight; ++y )
	{
		delete [] m_arrMapBlocks[y]; m_arrMapBlocks[y] = NULL;
	}
	delete [] m_arrMapBlocks; m_arrMapBlocks = NULL;

	if ( NULL != m_pCityInfo )
	{
		delete m_pCityInfo;
		m_pCityInfo = NULL;
	}

	return;
}

///重置本普通地图信息,普通地图回池时调用(实际普通地图没有使用池管理，因此会在删地图对象前调用)；
bool CMuxNormalMapInfo::ResetNormalMapInfo()
{
	return true;
}

void CMuxNormalMapInfo::MuxNormalMapInfoInit()
{
	return;
}

///检查玩家是否在地图内(玩家指针不知是否仍有效)；
bool CMuxNormalMapInfo::IsDoubtPlayerPtrInMap( CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		D_ERROR( "CMuxNormalMapInfo::IsDoubtPlayerPtrInMap，输入指针空\n" );
		return false;
	}

	//前提：CPlayer在池中分配，因此永远可以安全地取其在数组中的位置；
	int playerarrpos = pPlayer->GetPlayerPosInMapArr();
	if ( ( playerarrpos>=m_curMapPlayerNum ) 
		|| ( playerarrpos<0 )
		|| ( m_curMapPlayerNum<=0 )
		)
	{
		//该玩家已离开地图，且其原来所在位置已无效；
		return false;
	}

	if ( playerarrpos >= MAX_PLAYERNUM_INMAP )
	{
		D_ERROR( "IsDoubtPlayerPtrInMap,位置%d超过数组大小%d\n", playerarrpos, MAX_PLAYERNUM_INMAP );
		return false;
	}

	if ( m_arrMapPlayers[playerarrpos] != pPlayer )
	{
		//该玩家已离开地图，其原来位置存放的是其它玩家；
		return false;
	}

	return true;//该位置元素仍为希望的元素；
}

///取地图内的怪物指针；
CMonster* CMuxNormalMapInfo::GetMonsterPtrInMap( unsigned long monsterID )
{
	if ( NULL == m_pMapMonsters )
	{
		D_ERROR( "CMuxMap::GetMonsterPtrInMap， NULL == m_pMapMonsters\n" );
		return NULL;
	}

	HashTbPtrEle<CMonster>* pFound = NULL;
	bool isFound = m_pMapMonsters->FindEle( (unsigned int)monsterID, pFound );
	if ( isFound )
	{
		if ( NULL != pFound )
		{
			return pFound->GetInnerPtr();
		}
	}

	return NULL;
}

void CMuxNormalMapInfo::SetCityInfo( CityInfo* pCityInfo )
{
	if ( NULL != m_pCityInfo )
	{
		delete m_pCityInfo;
		m_pCityInfo = NULL;
	}
	m_pCityInfo = pCityInfo;
}


void CMuxNormalMapInfo::AddCityExp( unsigned int addExp )
{
	if( IsHaveCityInfo() )
	{
		m_pCityInfo->cityExp += addExp;
	}
}


unsigned char CMuxNormalMapInfo::GetCityRace()
{
	if( !IsHaveCityInfo() )
		return 0;

	return m_pCityInfo->ucRace;
}


bool CMuxNormalMapInfo::IsHaveCityInfo()
{
	return ( m_pCityInfo != NULL ); 
}


CityInfo* CMuxNormalMapInfo::GetCityInfo()
{
	return m_pCityInfo;
}


void CMuxNormalMapInfo::ClearCityInfo()
{
	if( NULL != m_pCityInfo )
	{
		delete m_pCityInfo;	m_pCityInfo = NULL;
	}
}


///累计块内玩家怪物数量；
void CMuxNormalMapInfo::CalBlockPlayerMonsterNum( unsigned short blockX, unsigned blockY, unsigned int& toAddPlayerNum, unsigned int& toAddMonsterNum )
{
	if ( IsBlockValid(blockX, blockY ) )
	{
		MuxMapBlock* pBlock = GetMapBlockByBlock(blockX, blockY);
		if ( NULL != pBlock )
		{
			D_DEBUG( "块(%d,%d),玩家数:%d,怪物数:%d\n", blockX, blockY, pBlock->GetBlockPlayerNum(), pBlock->GetBlockMonsterNum()  );
			toAddPlayerNum += pBlock->GetBlockPlayerNum();
			toAddMonsterNum += pBlock->GetBlockMonsterNum();
		}
	}
	return;
}

/////temp for monster wrapper;
//bool CMuxNormalMapInfo::GetScopeMonsters( unsigned short centerX, unsigned short centerY, ScopeMonsterInfo* scopeMonstrArr, int inArrSize, int& arrSize )
//{
//	if ( NULL == scopeMonstrArr )
//	{
//		D_ERROR( "CMuxNormalMapInfo::GetScopeMonsters, 输入参数空\n" );
//		return false;
//	}
//
//	arrSize = 0;
//
//	TYPE_POS blockX,blockY;
//	MuxMapSpace::GridToBlockPos( centerX, centerY, blockX, blockY );
//	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
//	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	{
//		//输入点为无效点；
//		return false;
//	}
//	MuxMapBlock* pBlock = NULL;
//	int tempArrSize = 0;
//	//int nRange = pSkill->GetRange();
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pBlock = GetMapBlockByBlock( x, y );
//			if ( NULL != pBlock )
//			{
//				if( !pBlock->GetBlockMonsterID( scopeMonstrArr, inArrSize, tempArrSize ) )
//				{
//					return true;
//				}
//			}
//		}
//	}
//
//	return true;
//}
//
/////temp for monster wrapper;
//bool CMuxNormalMapInfo::GetScopePlayers( unsigned short centerX, unsigned short centerY, ScopePlayerInfo* scopePlayerArr, int inArrSize, int& arrSize )
//{
//	if ( NULL == scopePlayerArr )
//	{
//		D_ERROR( "CMuxNormalMapInfo::GetScopePlayers, 输入参数空\n" );
//		return false;
//	}
//
//	arrSize = 0;
//
//	TYPE_POS blockX,blockY;
//	MuxMapSpace::GridToBlockPos( centerX, centerY, blockX, blockY );
//	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
//	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
//	{
//		//输入点为无效点；
//		return false;
//	}
//	MuxMapBlock* pBlock = NULL;
//	int tempArrSize = 0;
//	//int nRange = pSkill->GetRange();
//	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
//	{
//		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
//		{
//			pBlock = GetMapBlockByBlock( x, y );
//			if ( NULL != pBlock )
//			{
//				if( !pBlock->GetBlockPlayerInfo( scopePlayerArr, inArrSize, tempArrSize ) )
//				{
//					return true;
//				}
//			}
//		}
//	}
//
//	return true;
//}

//获取范围内的玩家和怪物
bool CMuxNormalMapInfo::GetScopeCreature( TYPE_POS posX, TYPE_POS posY, int range, vector<CPlayer*>& vecPlayer, vector<CMonster*>& vecMonster )
{
	vecPlayer.clear();
	vecMonster.clear();

	unsigned short blockX = 0, blockY = 0;
	MuxMapSpace::GridToBlockPos( posX, posY, blockX, blockY );
	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PtGetScopeCreature( posX, posY, range, vecPlayer, vecMonster );
			}
		}
	}
	return true;
}

/////取指定点视野内玩家与怪物数量；
//bool CMuxNormalMapInfo::GetPosSurPlayerMonsterNum( unsigned short posX, unsigned short posY, unsigned int& playerNum, unsigned int& monsterNum )
//{
//	playerNum=0, monsterNum=0;
//	unsigned short orgBlockX=0, orgBlockY=0;
//	MuxMapSpace::GridToBlockPos( posX, posY, orgBlockX, orgBlockY );
//
//	unsigned short tmpX = orgBlockX-1, tmpY = orgBlockY-1;
//	CalBlockPlayerMonsterNum( tmpX, tmpY, playerNum, monsterNum );
//
//	tmpX = orgBlockX-1, tmpY = orgBlockY;
//	CalBlockPlayerMonsterNum( tmpX, tmpY, playerNum, monsterNum );
//
//	tmpX = orgBlockX-1, tmpY = orgBlockY+1;
//	CalBlockPlayerMonsterNum( tmpX, tmpY, playerNum, monsterNum );
//
//	tmpX = orgBlockX, tmpY = orgBlockY-1;
//	CalBlockPlayerMonsterNum( tmpX, tmpY, playerNum, monsterNum );
//
//	tmpX = orgBlockX, tmpY = orgBlockY;
//	CalBlockPlayerMonsterNum( tmpX, tmpY, playerNum, monsterNum );
//
//	tmpX = orgBlockX, tmpY = orgBlockY+1;
//	CalBlockPlayerMonsterNum( tmpX, tmpY, playerNum, monsterNum );
//
//	tmpX = orgBlockX+1, tmpY = orgBlockY-1;
//	CalBlockPlayerMonsterNum( tmpX, tmpY, playerNum, monsterNum );
//
//	tmpX = orgBlockX+1, tmpY = orgBlockY;
//	CalBlockPlayerMonsterNum( tmpX, tmpY, playerNum, monsterNum );
//
//	tmpX = orgBlockX+1, tmpY = orgBlockY+1;
//	CalBlockPlayerMonsterNum( tmpX, tmpY, playerNum, monsterNum );
//
//	return true;
//}

//调整玩家所在块
void CMuxNormalMapInfo::AdjustPlayerBlock( bool isManualSet/*是否手动调，手动调时必须周围全通知到而不是只通知新视野*/, CPlayer* pPlayer, unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY )
{
	if ( NULL == pPlayer )
	{
		D_WARNING( "AdjustPlayerBlock，输入玩家指针空\n" );
		return;
	}

    unsigned short viewportBlockNum;/*新视野块数目*/
	unsigned short viewBlockX[5];//新视野块X坐标数组；
	unsigned short viewBlockY[5];//新视野块Y坐标数组；
	MuxMapBlock*   pNewBlock = NULL;//新视野临时块；
	MuxMapBlock*   pOrgBlock = NULL;//旧视野临时块； 

	//将玩家从旧块上删去；
	pOrgBlock = GetMapBlockByBlock( orgBlockX, orgBlockY );
	if ( NULL != pOrgBlock )
	{
		pOrgBlock->DeletePlayerFromBlock( pPlayer );
	} else{
		D_ERROR( "AdjustPlayerBlock，不可能错误，取不到玩家%s原来所在块\n", pPlayer->GetAccount() );
	}

	pNewBlock = GetMapBlockByBlock( newBlockX, newBlockY );
	if ( NULL != pNewBlock )
	{
		pNewBlock->AddPlayerToBlock( pPlayer );
		pNewBlock->PlayerMovChgBlockMonsterNotify( pPlayer );//应AI要求添加，进入与怪物相同的同一块时再通知一次怪物(进入相邻格时已通知过一次)；
	} else {
		D_ERROR( "AdjustPlayerBlock，不可能错误，取不到玩家%s(%d,%d)所在块(%d,%d)\n", pPlayer->GetAccount(), pPlayer->GetPosX(), pPlayer->GetPosY(), newBlockX, newBlockY );
	}

	//将玩家加入新块；
	//玩家移动中的定时更新只通知边缘玩家；
	//此处为定时更新，因此：
	// 1.找到前方新进入视野小块玩家；
	// 2.通知这些玩家本玩家的移动消息，同时通知本玩家这些新玩家的相应信息；
	// 3.通知前方新进入视野小块怪物，同时通知本玩家这些新怪物的信息；

	if ( isManualSet/*是否手动调，手动调时必须周围全通知到而不是只通知新视野*/ )
	{
		PlayerLeaveNotifyByBlockPos( pPlayer, orgBlockX, orgBlockY );
		PlayerAppearNotifyByBlockPos( pPlayer, false, newBlockX, newBlockY );

		return;
	}

	bool isJumpBlock = false;
	if(  !GetNewViewportBlocks( orgBlockX, orgBlockY, newBlockX, newBlockY, isJumpBlock, viewportBlockNum, viewBlockX, viewBlockY ) )//新进入视野块；
	{
		if ( isJumpBlock )
		{
			//D_WARNING( "AdjustPlayerBlock，玩家%s移动跳块\n", pPlayer->GetAccount() );
			//跳块，直接通知旧点周围离开，新点周围出现，唯一跳过PlayerLeaveNotify和PlayerAppearNotify直接通知的地方；
			PlayerLeaveNotifyByBlockPos( pPlayer, orgBlockX, orgBlockY );
			PlayerAppearNotifyByBlockPos( pPlayer, false, newBlockX, newBlockY );
		}
		return;
	}

#ifdef GROUP_NOTI
	//m_GroupNotiInfo.ResetGroupInfo();
	bool isNeedNoti = false;
	bool isSomeOneAdd = false;
#endif //GROUP_NOTI	

	for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
	{
		pNewBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
		if ( NULL != pNewBlock )
		{
			pNewBlock->PlayerMoveNewBlockNotify( pPlayer );//新进入视野块处理；
		}
	}

#ifdef GROUP_NOTI
	for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
	{
		pNewBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
		if ( NULL != pNewBlock )
		{
			pNewBlock->FillGroupInfoWithSelfPlayer( m_GroupNotiInfo, isSomeOneAdd );
			if ( isSomeOneAdd )
			{
				isNeedNoti = true;
			}
		}
	}
	if ( isNeedNoti )
	{
		MGSimplePlayerInfo* movPlayerInfo = NULL;
		movPlayerInfo = pPlayer->GetSimplePlayerInfo();
		if( NULL != movPlayerInfo )
		{
			m_GroupNotiInfo.NotiGroup<MGSimplePlayerInfo>( movPlayerInfo );//通知新进入视野块自身信息；
		}

		MGUnionMemberShowInfo *pMovePlayerShowInfo;
		CUnion *pMovePlayerUnion = pPlayer->Union();
		if(NULL != pMovePlayerUnion)
		{
			pMovePlayerShowInfo = pMovePlayerUnion->GetUnionMemberShowInfo(pPlayer);
			m_GroupNotiInfo.NotiGroup<MGUnionMemberShowInfo>(pMovePlayerShowInfo);
		}

		MGBuffData moveBuffInfo;
		if( pPlayer->IsHaveBuff() )
		{
			moveBuffInfo.buffData = (*pPlayer->GetBuffData());
			moveBuffInfo.targetID = pPlayer->GetFullID();
			m_GroupNotiInfo.NotiGroup<MGBuffData>( &moveBuffInfo );//通知新进入视野块自身信息；
		}

		MGPetinfoStable* movePetInfo = NULL;
		if( pPlayer->IsSummonPet() )
		{
			movePetInfo = pPlayer->GetSurPetNotiInfo();
			if ( NULL != movePetInfo )
			{
				m_GroupNotiInfo.NotiGroup<MGPetinfoStable>( movePetInfo );//通知新进入视野块自身信息；
			}
		}

		m_GroupNotiInfo.ResetGroupInfo();
	}
#endif //GROUP_NOTI

	if( !GetNewViewportBlocks( newBlockX, newBlockY, orgBlockX, orgBlockY, isJumpBlock, viewportBlockNum, viewBlockX, viewBlockY ) )//旧离开视野块；
	{
		if ( isJumpBlock )
		{
			D_ERROR("AdjustPlayerBlock， 跳块2，不可能错误\n");//因为前面应该已处理并已返回；
		}		
		return;
	}

	for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
	{
		pNewBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
		if ( NULL != pNewBlock )
		{
			pNewBlock->PlayerMoveOrgBlockNotify( pPlayer );//旧离开视野块处理；
		}
	}

	return;
}

//调整怪物所在块
bool CMuxNormalMapInfo::AdjustMonsterPos( CMonster* pMoveMonster, unsigned short orgGridX, unsigned short orgGridY, unsigned short newGridX, unsigned short newGridY
										 , bool isForceAndNotNotiMove/*是否强制设位置且不通知周围此移动，怪物移动状态下改变位置为false，非移动状态下改变位置为true(目前只有被击退一种情形)*/ )
{
	TRY_BEGIN;

	if ( NULL == pMoveMonster )
	{
		D_WARNING( "CMuxMap::AdjustMonsterPos，传入怪物指针空\n" );
		return false;
	}

	//更新怪物身上保存的位置信息； 
	pMoveMonster->SetPos( newGridX, newGridY );

	MuxMapBlock* pOrgBlock = GetMapBlockByGrid( orgGridX, orgGridY );
	if ( NULL == pOrgBlock )
	{
		D_WARNING( "CMuxMap::AdjustMonsterPos,怪物移动原所在点X:%d,Y:%d块指针空\n", orgGridX, orgGridY );
		return false;
	}

	MuxMapBlock* pNewBlock = GetMapBlockByGrid( newGridX, newGridY );
	if ( NULL == pNewBlock )
	{
		D_WARNING( "CMuxMap::AdjustMonsterPos,怪物移动目标点X:%d,Y:%d块指针空\n", newGridX, newGridY );
		return false;
	}

	if ( pOrgBlock != pNewBlock ) //怪物跨块移动；
	{		
		pOrgBlock->DeleteMonsterFromBlock( pMoveMonster );//将怪物从旧块上删去；
		pNewBlock->AddMonsterToBlock( pMoveMonster );//怪物加入新块；

		unsigned short viewportBlockNum;/*新视野块数目*/
		unsigned short viewBlockX[5];//新视野块X坐标数组；
		unsigned short viewBlockY[5];//新视野块Y坐标数组；
		MuxMapBlock* pTmpBlock = NULL;
		//处理新进入视野块；
		bool isJumpBlock = false;
		if( !GetNewViewportBlocks( pOrgBlock->GetBlockX(), pOrgBlock->GetBlockY(), pNewBlock->GetBlockX(), pNewBlock->GetBlockY(), isJumpBlock, viewportBlockNum, viewBlockX, viewBlockY ) )//新进入视野块；
		{
			if ( isJumpBlock )
			{
				D_WARNING("CMuxMap::AdjustMonsterPos，怪物跳块移动\n");				
			} else {
				D_WARNING("CMuxMap::AdjustMonsterPos, 输入点非法\n");
			}

			return false;
		}

		/*
		*/

#ifdef GROUP_NOTI
		//m_GroupNotiInfo.ResetGroupInfo();
		//m_GroupNotiInfo.DebugIDIfAny();
		bool isNeedNoti = false;
		bool isSomeOneAdd = false;
#endif //GROUP_NOTI

		for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
		{
			pTmpBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
			if ( NULL != pTmpBlock )
			{
				pTmpBlock->MonsterMoveNewBlockNotify( pMoveMonster );
			}
		}

#ifdef GROUP_NOTI
		//据客户端意见，仍然通知新视野，而攻击消息不通知新视野，by dzj, 10.05.24,if ( !isForceAndNotNotiMove ) //正常移动，需要通知新视野
		{
			for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
			{
				pTmpBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );
				if ( NULL != pTmpBlock )
				{
					pTmpBlock->FillGroupInfoWithSelfPlayer( m_GroupNotiInfo, isSomeOneAdd );
					if ( isSomeOneAdd )
					{
						isNeedNoti = true;
					}
				}
			}
			if ( isNeedNoti )
			{
				//D_DEBUG( "CMuxNormalMapInfo::AdjustMonsterPos，通知怪物停止移动\n" );
				m_GroupNotiInfo.NotiGroup<MGMonsterMove>( pMoveMonster->GetMonsterMove() );
				m_GroupNotiInfo.ResetGroupInfo();
			}
		}
#endif //GROUP_NOTI

		//处理旧离开视野块；
		if( !GetNewViewportBlocks( pNewBlock->GetBlockX(), pNewBlock->GetBlockY(), pOrgBlock->GetBlockX(), pOrgBlock->GetBlockY(), isJumpBlock, viewportBlockNum, viewBlockX, viewBlockY ) )//旧离开视野块
		{
			if ( isJumpBlock )
			{
				D_WARNING("CMuxMap::AdjustMonsterPos，怪物跳块移动2\n");				
			} else {
				D_WARNING("CMuxMap::AdjustMonsterPos 输入点非法2\n");
			}
			return false;
		}

		for ( unsigned short blockid=0; blockid<viewportBlockNum; ++blockid )
		{
			pTmpBlock = GetMapBlockByBlock( viewBlockX[blockid], viewBlockY[blockid] );			
			if ( NULL != pNewBlock )
			{
				pTmpBlock->MonsterMoveOrgBlockNotify( pMoveMonster );
			}
		}
	}

	return true;
	TRY_END;
	return false;
}

///取指定块周边块的坐标范围；
bool CMuxNormalMapInfo::GetBlockNearbyScope( unsigned short blockX, unsigned short blockY, unsigned short& minBlockX, unsigned short& minBlockY, unsigned short& maxBlockX, unsigned short& maxBlockY )
{
	if ( !IsBlockValid( blockX, blockY ) )
	{
		//点的块坐标不在地图内；
		return false;
	}

	minBlockX=blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( 0!=blockX )
	{
		minBlockX = blockX - 1;
	}
	if ( 0!=blockY )
	{
		minBlockY = blockY -1;
	}
	if ( blockX < m_blockWidth-1 )
	{
		maxBlockX = blockX + 1;
	}
	if ( blockY < m_blockHeight-1 )
	{
		maxBlockY = blockY + 1;
	}

	return true;
}

///增加对应八方向的新块计算函数，输入：旧块X，旧块Y，新块X， 新块Y，输出：新视野块X，新视野块Y（最多5组，不允许跳块，即，输入必须为邻块）,???,贺轶男检查;
bool CMuxNormalMapInfo::GetNewViewportBlocks( unsigned short orgBlockX, unsigned short orgBlockY, unsigned short newBlockX, unsigned short newBlockY
						  ,  bool& isJump/*是否跳块，输入非相邻块*/, unsigned short& viewportBlockNum/*新视野块数目*/, unsigned short outBlockX[5], unsigned short outBlockY[5] )
{
	/////跨块移动新视野块,???,贺轶男检查；
	////1、检查新块与旧块之间的范围是否为1，超过1则出错返回false，因为此时输入的数组大小已经不够，如果特定情况下需要跨块，则需要特殊处理，例如GM命令瞬移时；
	////2、计算新视野块，并将新块坐标填入输出数组；

	//预先做个判断
	if( !IsGridValid(orgBlockX, orgBlockY) )
	{
		D_WARNING("越界的Org点位 (%d,%d)\n",orgBlockX, orgBlockY );
		return false;
	}

	if( !IsGridValid(newBlockX, newBlockY)  )
	{
		D_WARNING("越界的new点位 (%d,%d)\n", newBlockX, newBlockY );
		return false;
	}

	isJump = true;//初始假设为跳块；
	viewportBlockNum = 0;

	int nMinX = 0, nMaxX = 0, nMinY = 0, nMaxY = 0;
	bool bUpdateX = false ,bUpdateY = false;
	int dx = orgBlockX - newBlockX;
	int dy = orgBlockY - newBlockY;
	if( abs(dx) > 1 || abs(dy) > 1)
	{
		//D_ERROR( "GetNewViewportBlocks移动跳块, 原块(%d,%d)-->新块(%d,%d)\n", orgBlockX, orgBlockY, newBlockX, newBlockY );
		return false;
	}

	isJump = false;//非跳块；

	//8个方向 8种情况
	if( (dy > 0) 
		&& ( newBlockY != 0 ) )  //既新格不是最上面一格
	{
		bUpdateY = true;
	}

	if( (dy < 0)
		&& ( newBlockY != m_blockHeight - 1 ) ) //新格不是最下面一格
	{
		bUpdateY = true;
	}

	if( (dx> 0)
		&& newBlockX != 0 )		//新格不是最左面的格
	{
		bUpdateX = true;
	}

	if( (dx<0)
		&& newBlockX !=  m_blockWidth - 1 )
	{
		bUpdateX = true;
	}

	GetValidBlock( newBlockX, newBlockY, 1, nMinX, nMaxX, nMinY, nMaxY );

	//8个方向
	unsigned int i = 0;
	if( dy == 1 && dx == 0)  //正北
	{
		if( bUpdateY )
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 正北, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}
		}
	}
	else if( dy == -1 && dx == 0)  //正南
	{
		if( bUpdateY )
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 正南, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}
		}
	}
	else if( dy == 0 && dx == 1)  //正西
	{
		if( bUpdateX )
		{
			for(unsigned short y = nMinY ; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 正西, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
	}
	else if( dy == 0 && dx == -1 ) //正东
	{
		if( bUpdateX )
		{
			for(unsigned short y = nMinY ; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 正东, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
	}
	else if( dx == 1 && dy == 1)  //西北
	{
		if( bUpdateX && bUpdateY)  //2边都能更新
		{
			//先X3座标
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西北X3, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}

			//再Y2座标
			for(unsigned short y = nMinY + 1; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西北Y2, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}

		}
		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
		{
			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西北Y轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else  //只能更新X轴
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西北X轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}
		}
	}
	else if( dx == 1 && dy == -1) //西南
	{
		if( bUpdateX && bUpdateY)  //2边都能更新
		{
			//再更新X轴3个
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西南X3, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}

			//再更新Y轴2个
			for(unsigned short y = nMinY; y <= nMaxY - 1; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西南Y2, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
		{
			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西南Y轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMinX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else  //只能更新X轴
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 西南X轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}
		}
	}
	else if( dx == -1 && dy == -1) //东南
	{
		if( bUpdateX && bUpdateY)  //2边都能更新
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东南X3, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}

			for(unsigned short y = nMinY; y <= nMaxY - 1; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东南Y2, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
		{
			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东南Y轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else  //只能更新X轴
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东南X轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMaxY;
				viewportBlockNum++;
			}
		}
	}
	else if( dx == -1 && dy == 1) //东北
	{
		if( bUpdateX && bUpdateY)  //2边都能更新
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东北X3, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}

			for(unsigned short y = nMinY+1; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东北Y2, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else if( bUpdateX && !bUpdateY )  //只能更新Y轴
		{
			for(unsigned short y = nMinY; y <= nMaxY; ++i, ++y)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东北Y轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = nMaxX;
				outBlockY[i] = y;
				viewportBlockNum++;
			}
		}
		else  //只能更新X轴
		{
			for(unsigned short x = nMinX; x <= nMaxX; ++i, ++x)
			{
				if ( i >= 5 )
				{
					D_ERROR( "CMuxCopyInfo::GetNewViewportBlocks, 东北X轴, 越界(%d,%d,%d,%d), 输入(%d,%d,%d,%d)\n"
						, nMinX, nMaxX, nMinY, nMaxY, orgBlockX, orgBlockY, newBlockX, newBlockY );
					return false;
				}
				outBlockX[i] = x;
				outBlockY[i] = nMinY;
				viewportBlockNum++;
			}
		}
	}
	
	return true; 
};

//add player to map block;
bool CMuxNormalMapInfo::AddPlayerToNormalMap( CPlayer* pPlayer, unsigned short gridX, unsigned short gridY )
{
	if ( NULL == pPlayer )
	{
		return false;
	}

	if ( m_curMapPlayerNum < 0 )
	{
		D_ERROR( "玩家%s进地图时，发现地图当前人数(%d)非法1\n", pPlayer->GetNickName(), m_curMapPlayerNum );
		return false;
	}
	if ( m_curMapPlayerNum >= MAX_PLAYERNUM_INMAP )
	{
		D_WARNING( "地图的进入人数超过了最大上限，玩家%s进入地图失败\n", pPlayer->GetNickName() );
		return false;
	}

	if ( !IsGridValid( gridX, gridY ) )
	{
		//点超出范围，添加失败;
		D_ERROR( "CMuxNormalMapInfo::AddPlayerToNormalMap，玩家的地图出现点%d,%d越界\n", gridX, gridY );
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( gridX, gridY );
	if ( NULL == pBlock )
	{
		//找不到对应块指针，不可能错误；
		D_ERROR( "CMuxNormalMapInfo::AddPlayerToNormalMap，地图出现点%d,%d对应块空\n", gridX, gridY );
		return false;
	}

	pBlock->AddPlayerToBlock( pPlayer );//玩家加入块；

	if ( m_curMapPlayerNum < 0 )
	{
		D_ERROR( "玩家%s进地图时，发现地图当前人数(%d)非法2\n", pPlayer->GetNickName(), m_curMapPlayerNum );
		return false;
	}
	if ( m_curMapPlayerNum >= MAX_PLAYERNUM_INMAP )
	{
		D_ERROR( "进入人数超过了最大上限，函数最开始的判断没有拦住，玩家%s\n", pPlayer->GetNickName() );
		return false;
	}
	m_arrMapPlayers[m_curMapPlayerNum] = pPlayer;
	pPlayer->SetPlayerPosInMapArr( m_curMapPlayerNum );//记录玩家在数组中的位置；
	++m_curMapPlayerNum;
	D_DEBUG( "将玩家%s加入地图数组的位置%d\n", pPlayer->GetNickName(), m_curMapPlayerNum-1 );

	return true;
}

//add monster to map block;
bool CMuxNormalMapInfo::AddMonsterToNormalMap( CMonster* pMonster, unsigned short gridX, unsigned short gridY )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMuxNormalMapInfo::AddMonsterToNormalMap, NULL == pMonster\n" );
		return false;
	}

	if ( !IsGridValid( gridX, gridY ) )
	{
		//点超出范围，添加失败;
		D_ERROR( "CMuxMap::AddMonster，怪物的地图出现点%d,%d越界\n", gridX, gridY );		
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( gridX, gridY );
	if ( NULL == pBlock )
	{
		//找不到对应块指针，不可能错误；
		D_ERROR( "CMuxMap::AddMonster，地图出现点%d,%d对应块空\n", gridX, gridY );		
		return false;
	}

	pMonster->SetBornPt( gridX, gridY ); //设置怪物出生点	
	pMonster->SetPos( gridX, gridY );

	pBlock->AddMonsterToBlock( pMonster );//怪物加入块；

#ifdef USE_SELF_HASH
	if ( NULL == m_pMapMonsters )
	{
		D_ERROR( "CMuxMap::AddMonsterToMap， NULL == m_pMapMonsters\n" );
		return false;
	}
	m_pMapMonsters->PushEle( HashTbPtrEle<CMonster>( pMonster->GetMonsterID(), pMonster ) );
#else //USE_SELF_HASH
	m_allMonster.insert( pMonster );//怪物加入地图;
#endif //USE_SELF_HASH

	return true;
}

//delete player to map block;
bool CMuxNormalMapInfo::DelPlayerFromNormalMap( CPlayer* pPlayer )
{
	if ( NULL == pPlayer )
	{
		return false;
	}

	unsigned short gridX=pPlayer->GetPosX(), gridY=pPlayer->GetPosY();
	if ( !IsGridValid( gridX, gridY ) )
	{
		//点超出范围，删除失败;
		D_ERROR( "DelPlayerFromNormalMap，玩家所在的地图点%d,%d越界\n", gridX, gridY );		
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( gridX, gridY );
	if ( NULL == pBlock )
	{
		//找不到对应块指针，不可能错误；
		D_ERROR( "DelPlayerFromNormalMap，玩家所在的地图点%d,%d对应块空\n", gridX, gridY );		
		return false;
	}
	
	pBlock->DeletePlayerFromBlock( pPlayer );

	int todelpos = pPlayer->GetPlayerPosInMapArr();
	if ( ( todelpos>=m_curMapPlayerNum ) 
		|| ( todelpos<0 )
		|| ( m_curMapPlayerNum<=0 )
		)
	{
		D_ERROR( "DelPlayerFromNormalMap,玩家%s在数组中的位置%d非法，当前人数-数组大小(%d-%d)\n"
			, pPlayer->GetNickName(), todelpos, m_curMapPlayerNum, MAX_PLAYERNUM_INMAP );
		return false;
	}
	if ( todelpos >= MAX_PLAYERNUM_INMAP )
	{
		D_ERROR( "DelPlayerFromNormalMap,玩家%s在数组中的位置%d超过数组大小%d，当前人数%d\n"
			, pPlayer->GetNickName(), todelpos, MAX_PLAYERNUM_INMAP, m_curMapPlayerNum );
		return false;
	}
	if ( m_arrMapPlayers[todelpos] != pPlayer )
	{
		D_ERROR( "DelPlayerFromNormalMap,玩家%s，数组中相应位置%d非欲删者，数组大小%d，当前人数%d\n"
			, pPlayer->GetNickName(), todelpos, MAX_PLAYERNUM_INMAP, m_curMapPlayerNum );
		return false;
	}

	//以下待删元素校验通过；
	m_arrMapPlayers[todelpos] = m_arrMapPlayers[m_curMapPlayerNum-1];//最末元素覆盖当前元素；
	if ( NULL == m_arrMapPlayers[todelpos] )
	{
		D_ERROR( "DelPlayerFromNormalMap,玩家%s，交换过来的最末元素为空，交换位置%d，数组大小%d，当前人数%d\n"
			, pPlayer->GetNickName(), todelpos, MAX_PLAYERNUM_INMAP, m_curMapPlayerNum );
		return false;
	}
	m_arrMapPlayers[todelpos]->SetPlayerPosInMapArr( todelpos );//重设交换过来元素的位置
	pPlayer->SetPlayerPosInMapArr( -1 );//重置删去玩家的数组中位置(如果只有一个玩家，则这里是一个重复)；
	m_arrMapPlayers[m_curMapPlayerNum-1] = NULL;//最末元素删去；
	--m_curMapPlayerNum;//地图中的玩家数--;
	D_DEBUG( "将玩家%s从地图数组的位置%d删去\n", pPlayer->GetNickName(), todelpos );

	return true;
}

//delete monster to map block;
bool CMuxNormalMapInfo::DelMonsterFromNormalMap( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "DelMonsterFromNormalMap，怪物指针空\n" );
		return false;
	}

	unsigned short gridX=pMonster->GetPosX(), gridY=pMonster->GetPosY();
	if ( !IsGridValid( gridX, gridY ) )
	{
		//点超出范围，删除失败;
		D_ERROR( "DelMonsterFromNormalMap，怪物所在的地图点%d,%d越界\n", gridX, gridY );		
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( gridX, gridY );
	if ( NULL == pBlock )
	{
		//找不到对应块指针，不可能错误；
		D_ERROR( "DelMonsterFromNormalMap，怪物所在的地图点%d,%d对应块空\n", gridX, gridY );		
		return false;
	}

	pBlock->DeleteMonsterFromBlock( pMonster );

#ifdef USE_SELF_HASH
	if ( NULL == m_pMapMonsters )
	{
		D_ERROR( "CMuxMap::DeleteMonsterFromMap， NULL == m_pMapMonsters\n" );
		return false;
	}
	m_pMapMonsters->DelEle( pMonster->GetMonsterID() );
#else //USE_SELF_HASH
	m_allMonster.erase( pMonster );
#endif //USE_SELF_HASH

	return true;
}

//add pkg to map block;
bool CMuxNormalMapInfo::AddItemPkgToNormalMap( CItemsPackage* pItemPkg )
{
	if ( NULL == pItemPkg )
	{
		D_ERROR( "CMuxNormalMapInfo::AddItemPkgToNormalMap, NULL == pItemPkg\n" );
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( pItemPkg->GetItemPosX(), pItemPkg->GetItemPosY() );
	if ( NULL == pBlock )
	{
		D_ERROR( "AddItemPkgToNormalMap, can not find res Block, (%d, %d)", pItemPkg->GetItemPosX(), pItemPkg->GetItemPosY() );
		return false;
	}

	//将生成的道具包加入该节点
	pBlock->AddItemsPkg( pItemPkg );

	return true;
}

//delete pkg to map block;
bool CMuxNormalMapInfo::DelItemPkgFromNormalMap( CItemsPackage* pItemPkg )
{
	if ( NULL == pItemPkg )
	{
		D_ERROR( "CMuxNormalMapInfo::DelItemPkgFromNormalMap, NULL == pItemPkg\n" );
		return false;
	}

	MuxMapBlock* pBlock = GetMapBlockByGrid( pItemPkg->GetItemPosX(), pItemPkg->GetItemPosY() );
	if ( NULL == pBlock )
	{
		return false;
	}

	return pBlock->DeleteItemsPkg( pItemPkg );
}

///add rideteam to map block;
bool CMuxNormalMapInfo::AddRideteamToNormalMap( RideTeam* pTeam, unsigned short gridX, unsigned short gridY )
{
	if ( NULL == pTeam )
	{
		D_ERROR( "CMuxNormalMapInfo::AddRideteamToNormalMap, NULL == pTeam\n" );
		return false;
	}

	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( gridX, gridY, blockX, blockY );
	MuxMapBlock* pBlock = GetMapBlockByBlock( blockX, blockY );
	if ( NULL == pBlock )
	{
		return false;
	}

	pBlock->AddRideTeam( pTeam );

	return true;
}

///delete rideteam from map block;
bool CMuxNormalMapInfo::DelRideteamFromNormalMap( RideTeam* pTeam, unsigned short gridX, unsigned short gridY )
{
	if ( NULL == pTeam )
	{
		D_ERROR( "CMuxNormalMapInfo::DelRideteamFromNormalMap, NULL == pTeam\n" );
		return false;
	}

	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( gridX, gridY, blockX, blockY );
	MuxMapBlock* pBlock = GetMapBlockByBlock( blockX, blockY );
	if ( NULL == pBlock )
	{
		return false;
	}

	pBlock->DeleteRideTeam( pTeam );

	return true;
}

///发起踢所有人上半部，且内部不通知每个人消息，在下面使用广播通知；
bool CMuxNormalMapInfo::IssueAllPlayerLeavePCopyHF( bool isNotiPlayerThis/*函数内部是否直接通知player或稍后使用广播通知*/ )
{
	if ( ( m_curMapPlayerNum<0 )
		|| ( m_curMapPlayerNum>MAX_PLAYERNUM_INMAP )
		)
	{
		D_ERROR( "CMuxNormalMapInfo::IssueAllPlayerLeavePCopyHF，地图内当前人数%d不对\n", m_curMapPlayerNum );
		return false;
	}

	CPlayer* pPlayer = NULL;
	for ( int i=0; i<m_curMapPlayerNum; ++i )
	{
		pPlayer = m_arrMapPlayers[i];
		if ( NULL == pPlayer )
		{
			D_ERROR( "CMuxNormalMapInfo::IssueAllPlayerLeavePCopyHF，地图玩家数组位置%d元素空\n", i );
			continue;
		}
		pPlayer->IssuePlayerLeaveCopyHF( isNotiPlayerThis );
	}

	return true;
}

/////对每一玩家执行PlayerLeaveHF时，元素被遍历时执行，由hash表内部调用；
//bool CMuxNormalMapInfo::OnPlayerEleIssueLeaveHFExplored( HashTbPtrEle<CPlayer>* beExploredEle, void* pParam )
//{
//	if ( NULL == beExploredEle )
//	{
//		return false;
//	}
//
//	if ( NULL == pParam )
//	{
//		return false;
//	}
//
//	CPlayer* pPlayer = beExploredEle->GetInnerPtr();
//	if ( NULL == pPlayer )
//	{
//		return false;
//	}
//
//	pPlayer->IssuePlayerLeaveCopyHF( *(bool*)pParam );
//
//	return true;
//}

bool CMuxNormalMapInfo::OnPlayerSkillGetAoeTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster )
{
	if( NULL == pUseSkillPlayer )
	{
		D_ERROR( "CMuxNormalMapInfo::OnPlayerSkillGetAoeTarget, NULL == pUseSkillPlayer || NULL == pMuxSkill\n" );
		return false;
	}

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("找不到战斗脚本\n");
		return false;
	}

	vecTargetPlayer.clear();
	vecMonster.clear();
	TYPE_POS blockX = 0, blockY = 0;

	MuxMapSpace::GridToBlockPos( aoePosX, aoePosY, blockX, blockY );
	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PtPlayerGetSkillTarget( aoePosX, aoePosY, range, pUseSkillPlayer, pScript, vecTargetPlayer, vecMonster );
			}
		}
	}
	return true;
}


bool CMuxNormalMapInfo::OnMonsterSkillGetAoeTarget( CMonster* pUseSkillMonster, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer, vector<CMonster*>& vecMonster )
{
	vecTargetPlayer.clear();
	vecMonster.clear();
	TYPE_POS blockX = 0, blockY = 0;

	MuxMapSpace::GridToBlockPos( aoePosX, aoePosY, blockX, blockY );
	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PtMonsterGetSkillTarget( aoePosX, aoePosY, range, pUseSkillMonster, vecTargetPlayer, vecMonster );
			}
		}
	}
	return true;
}


bool CMuxNormalMapInfo::OnPlayerSkillGetTeamTarget( CPlayer* pUseSkillPlayer, unsigned int range, TYPE_POS aoePosX, TYPE_POS aoePosY, vector<CPlayer*>& vecTargetPlayer )
{
	if ( NULL == pUseSkillPlayer )
	{
		D_ERROR( "CMuxNormalMapInfo::OnPlayerSkillGetTeamTarget, NULL == pUseSkillPlayer\n" );
		return false;
	}

	vecTargetPlayer.clear();
	if( pUseSkillPlayer->GetGroupTeam() == NULL )
	{
		vecTargetPlayer.push_back( pUseSkillPlayer );
		return true;
	}

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("找不到战斗脚本\n");
		return false;
	}

	TYPE_POS blockX = 0, blockY = 0;
	MuxMapSpace::GridToBlockPos( aoePosX, aoePosY, blockX, blockY );
	TYPE_POS minBlockX, minBlockY, maxBlockX, maxBlockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PtPlayerSkillGetTeamTarget( aoePosX, aoePosY, range, pUseSkillPlayer, pScript, vecTargetPlayer );
			}
		}
	}
	return true;
}


///向周围怪物广播玩家的新移动消息；
void CMuxNormalMapInfo::PlayerNewMoveMonsterBrocast( CPlayer* pMovePlayer )
{
	TRY_BEGIN;

	if ( NULL == pMovePlayer )
	{
		D_WARNING( "CMuxNormalMapInfo::PlayerNewMoveMonsterBrocast, 输入指针空\n" );
		return;
	}

	unsigned short blockX = 0, blockY = 0;
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;


	MuxMapSpace::GridToBlockPos( pMovePlayer->GetPosX(), pMovePlayer->GetPosY(), blockX, blockY );
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerMovChgBlockMonsterNotify( pMovePlayer );
			}
		}
	}

	return;
	TRY_END;
	return;
}

///玩家死亡通知；
void CMuxNormalMapInfo::PlayerDieNotify( CPlayer* pDiePlayer )
{
	TRY_BEGIN;

	if ( NULL == pDiePlayer )
	{
		D_WARNING( "CMuxNormalMapInfo::PlayerDieNotify, 输入指针空\n" );
		return;
	}

	unsigned short blockX = 0, blockY = 0;
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	MuxMapSpace::GridToBlockPos( pDiePlayer->GetPosX(), pDiePlayer->GetPosY(), blockX, blockY );
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}
	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerDieBlockNotify( pDiePlayer );
			}
		}
	}

	return;
	TRY_END;
	return;

}

///玩家出现块相关通知；
void CMuxNormalMapInfo::PlayerAppearNotifyByBlockPos( CPlayer* pAppearPlayer, bool isNotiSelf/*是否通知出现者本人*/
													 , unsigned short blockX, unsigned short blockY )
{
	if ( NULL == pAppearPlayer )
	{
		D_WARNING( "PlayerAppearNotifyByBlockPos，输入玩家指针空\n" );
		return;
	}

	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

#ifdef GROUP_NOTI
	bool isNeedNoti = false;
	bool isSomeOneAdd = false;
#endif //GROUP_NOTI

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerAppearBlockNotify( pAppearPlayer, isNotiSelf );
			}
		}
	}

#ifdef GROUP_NOTI
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				if ( !isNotiSelf )
				{
					//不通知自己；
					pBlock->FillGroupInfoWithSelfPlayerExcept( m_GroupNotiInfo, pAppearPlayer->GetFullID(), isSomeOneAdd );
				} else {
					//通知自己；
					pBlock->FillGroupInfoWithSelfPlayer( m_GroupNotiInfo, isSomeOneAdd );
				}
				if ( isSomeOneAdd )
				{
					isNeedNoti = true;
				}
			}
		}
	}

	if ( isNeedNoti )
	{
		//通知块上玩家本玩家出现信息；
		MGSimplePlayerInfo* movPlayerInfo = NULL;
		movPlayerInfo = pAppearPlayer->GetSimplePlayerInfo();
		if ( NULL != movPlayerInfo )
		{
			m_GroupNotiInfo.NotiGroup<MGSimplePlayerInfo>( movPlayerInfo );
		}
		
		MGUnionMemberShowInfo* pMovePlayerShowInfo = NULL;
		CUnion* pMovePlayerUnion = pAppearPlayer->Union();
		if ( NULL != pMovePlayerUnion )
		{
			pMovePlayerShowInfo = pMovePlayerUnion->GetUnionMemberShowInfo(pAppearPlayer);
			if ( NULL != pMovePlayerShowInfo )
			{
				m_GroupNotiInfo.NotiGroup<MGUnionMemberShowInfo>( pMovePlayerShowInfo );
			}
		}

		MGBuffData moveBuff;
		if ( pAppearPlayer->IsHaveBuff() )
		{
			moveBuff.buffData = (*pAppearPlayer->GetBuffData());
			moveBuff.targetID = pAppearPlayer->GetFullID();
			m_GroupNotiInfo.NotiGroup<MGBuffData>( &moveBuff );
		}

		MGPetinfoStable* movePetInfo = NULL;
		if( pAppearPlayer->IsSummonPet() )
		{
			movePetInfo = pAppearPlayer->GetSurPetNotiInfo();
			if ( NULL != movePetInfo )
			{
				m_GroupNotiInfo.NotiGroup<MGPetinfoStable>( movePetInfo );
			}
		}

		m_GroupNotiInfo.ResetGroupInfo();
	}
#endif //GROUP_NOTI

	return;
}

///@brief:将该道具包的信息,发送给周围的玩家
void CMuxNormalMapInfo::NotifyItemPkgsAppearEx( CItemsPackage* pItemPkg )
{
	TRY_BEGIN;

	if ( NULL == pItemPkg )
	{
		D_ERROR( "CMuxNormalMapInfo::NotifyItemPkgsAppearEx, NULL == pItemPkg\n" );
		return;
	}

	unsigned short nPosX = pItemPkg->GetItemPosX();
	unsigned short nPosY = pItemPkg->GetItemPosY();
	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( nPosX, nPosY, blockX, blockY );

	//获取该点周围的Block节点，
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			//通告给者
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )//将该包裹的信息，通知给该节点的客户端
			{
				pBlock->NoticeItemPkgsToSurroundPlayer( pItemPkg );
			}
		}
	}

	return;
	TRY_END;
	return;

}

///notify itempkf first appear;
void CMuxNormalMapInfo::NotifyItemPkgFirstAppearEx( CItemsPackage* pItemPkg )
{
	TRY_BEGIN;

	if ( NULL == pItemPkg )
	{
		D_ERROR( "CMuxNormalMapInfo::NotifyItemPkgFirstAppearEx, NULL == pItemPkg\n" );
		return;
	}

	unsigned short nPosX = pItemPkg->GetItemPosX();
	unsigned short nPosY = pItemPkg->GetItemPosY();
	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( nPosX, nPosY, blockX, blockY );

	//获取该点周围的Block节点，
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			//通告给者
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )//将该包裹的信息，通知给该节点的客户端
			{
				pBlock->NoticeItemPkgFirstAppear( pItemPkg );
			}
		}
	}

	return;
	TRY_END;
	return;
}

bool CMuxNormalMapInfo::IsEnabelAddTower(TYPE_POS gridX, TYPE_POS gridY)
{
	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( gridX, gridY, blockX, blockY );

	//获取该点周围的Block节点，
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return false;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				if (!pBlock->IsEnabelAddTower(gridX,gridY))
				{
					return false;
				}
			}
		}
	}
	return true;
}

///怪物出生,调用时怪物已加入地图；
bool CMuxNormalMapInfo::MonsterBornNotify( CMonster* pBornMonster )
{
	TRY_BEGIN;

	if ( NULL == pBornMonster )
	{
		D_ERROR( "CMuxNormalMapInfo::MonsterBornNotify，monster指针空\n" );
		return false;
	}

	unsigned short bornGridX = pBornMonster->GetPosX();
	unsigned short bornGridY = pBornMonster->GetPosY();

	if ( !IsGridValid( bornGridX, bornGridY ) )
	{
		D_ERROR( "CMuxNormalMapInfo::MonsterBornNotify，monster出生点%d,%d，超出范围或为不可移动点\n", bornGridX, bornGridY );
		return false;
	}

	unsigned short bornBlockX=0, bornBlockY=0;
	MuxMapSpace::GridToBlockPos( bornGridX, bornGridY, bornBlockX, bornBlockY );//怪物出现点块坐标；
	MuxMapBlock* pBornBlock = GetMapBlockByBlock( bornBlockX, bornBlockY );
	if ( NULL == pBornBlock )
	{
		D_ERROR( "CMuxNormalMapInfo::MonsterBornNotify，不可能错误，monster出生点%d,%d无对应地图块\n", bornGridX, bornGridY );
		return false;
	}

	//2、相关影响块处理；
	unsigned short minBlockX = bornBlockX, minBlockY=bornBlockY, maxBlockX=bornBlockX, maxBlockY=bornBlockY;
	if ( !GetBlockNearbyScope( bornBlockX, bornBlockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{		
		return false;//输入点为无效点；
	}

	MuxMapBlock* pTmpBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pTmpBlock = GetMapBlockByBlock( x, y );
			if ( NULL != pTmpBlock )
			{
				pTmpBlock->MonsterBornBlockNotify( pBornMonster );
			}			
		}
	}

	return true;
	TRY_END;
	return false;
}

///玩家离开块相关通知；
void CMuxNormalMapInfo::PlayerLeaveNotifyByBlockPos( CPlayer* pLeavePlayer, unsigned short blockX, unsigned short blockY )
{
	if ( NULL == pLeavePlayer )
	{
		D_WARNING( "PlayerLeaveNotifyByBlockPos，输入玩家指针空\n" );
		return;
	}

	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

#ifdef GROUP_NOTI
	bool isNeedNoti = false;
	bool isSomeOneAdd = false;
#endif //GROUP_NOTI

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerLeaveBlockNotify( pLeavePlayer );
			}
		}
	}

#ifdef GROUP_NOTI
	bool     isNeedExceptRideTeam = false;
	PlayerID leadID, memberID;
	leadID.dwPID = leadID.wGID = 0;
	memberID.dwPID = memberID.wGID = 0;
	if(   pLeavePlayer->IsRideState() )//如果在马上,则两个玩家则作为一个整体,不通知，则一起不通知
	{
		isNeedExceptRideTeam = true;
		pLeavePlayer->GetSelfStateManager().GetRideState().GetTeamAllMemberID( leadID, memberID );
	}

	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				//修改BUG 196872:[SAT] 玩家B在移动时同意双人骑乘邀请，玩家A显示玩家B状态错误
				//因为服务器多发了PlayerLeave消息
				if ( isNeedExceptRideTeam )
				{
					pBlock->FillGroupInfoWithSelfRideTeamExecpt( m_GroupNotiInfo, leadID, memberID, isSomeOneAdd );
				}
				else
				{
					pBlock->FillGroupInfoWithSelfPlayerExcept( m_GroupNotiInfo, pLeavePlayer->GetFullID(), isSomeOneAdd );
				}

				if ( isSomeOneAdd )
				{
					isNeedNoti = true;
				}
			}
		}
	}

	if ( isNeedNoti )
	{
		MGPlayerLeave returnMsg;
		returnMsg.lPlayerID = pLeavePlayer->GetFullID();
		returnMsg.lMapCode = pLeavePlayer->GetMapID();

		m_GroupNotiInfo.NotiGroup<MGPlayerLeave>( &returnMsg );//通知新进入视野块自身信息；
		m_GroupNotiInfo.ResetGroupInfo();
	}
#endif //GROUP_NOTI

	return;
}

/////////////////////////////////////////////////////////////////////////////////////////////
//copy...
//通知周围格特殊怪物 离开或者进入视野
void CMuxNormalMapInfo::MonsterLeaveViewNotify( CMonster* pMonster )
{
	if ( NULL == pMonster )
	{
		D_ERROR( "CMuxNormalMapInfo::MonsterLeaveViewNotify，输入指针空\n" );
		return;
	}

	unsigned short blockX = 0, blockY = 0, minBlockX = 0, minBlockY =0, maxBlockX = 0, maxBlockY = 0;
	MuxMapSpace::GridToBlockPos( pMonster->GetPosX(), pMonster->GetPosY(), blockX, blockY );
	
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x, y );
			if ( NULL != pBlock )
			{
				pBlock->MonsterLeaveViewNotify( pMonster );   //通知进入视野
			}
		}
	}

	return;
}
//...copy
/////////////////////////////////////////////////////////////////////////////////////////////


///玩家跨块相关通知；
void CMuxNormalMapInfo::PlayerBeyondBlockNotifyByBlockPos( CPlayer* pBeyondPlayer )
{
	if ( NULL == pBeyondPlayer )
	{
		return;
	}

	unsigned short blockX = 0, blockY = 0;
	MuxMapSpace::GridToBlockPos( pBeyondPlayer->GetPosX(), pBeyondPlayer->GetPosY(), blockX, blockY );
	unsigned short minBlockX=blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;
	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{		
		return;//输入点为无效点；
	}

	MGEnvNotiFlag notiFlag;
	notiFlag.gcNotiFlag.notiStOrEnd = true;//通知开始；
	pBeyondPlayer->SendPkg<MGEnvNotiFlag>( &notiFlag );

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->PlayerBeyondBlockSelfNotify( pBeyondPlayer );
			}
		}
	}

	notiFlag.gcNotiFlag.notiStOrEnd = false;//通知结束；
	pBeyondPlayer->SendPkg<MGEnvNotiFlag>( &notiFlag );

	return;
}

///notify monster player hp add event;
void CMuxNormalMapInfo::NotifySurMonsterPlayerHpAdd( PlayerID addStarter, PlayerID addTarget, int hpAdd, unsigned short centerX, unsigned short centerY )
{
	unsigned short blockX=0, blockY=0;
	MuxMapSpace::GridToBlockPos( centerX, centerY, blockX, blockY );
	unsigned short minBlockX = blockX, minBlockY=blockY, maxBlockX=blockX, maxBlockY=blockY;

	if ( !GetBlockNearbyScope( blockX, blockY, minBlockX, minBlockY, maxBlockX, maxBlockY ) )
	{
		//输入点为无效点；
		return;
	}

	MuxMapBlock* pBlock = NULL;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x, y );
			if ( NULL != pBlock )
			{
				pBlock->NotifyMonsterAddHpEvent( addStarter, addTarget, hpAdd );
			}
		}
	}

	return;
}


void CMuxNormalMapInfo::SwitchMapPlayer( unsigned int race, unsigned int mapID, unsigned int posX1, unsigned int posY1, unsigned int posX2, unsigned int posY2  )
{
	if ( ( m_curMapPlayerNum<0 )
		|| ( m_curMapPlayerNum>MAX_PLAYERNUM_INMAP )
		)
	{
		D_ERROR( "CMuxNormalMapInfo::SwitchMapPlayer,地图内当前人数%d不对\n", m_curMapPlayerNum );
		return;
	}

	if ( NULL == m_arrMapPlayers[0] )
	{
		D_ERROR( "CMuxNormalMapInfo::SwitchMapPlayer,地图内第一个玩家指针空\n" );
		return;
	}
	unsigned int curMapID = (unsigned int)(m_arrMapPlayers[0]->GetMapID());
	CPlayer* pPlayer = NULL;
	int finalPosX=0, finalPosY=0;
	if ( curMapID == mapID )
	{
		//跳当前地图；
		D_DEBUG("CMuxNormalMapInfo::SwitchMapPlayer 跳当前地图\n");
		for ( int i=0; i<m_curMapPlayerNum; ++i )
		{
			pPlayer = m_arrMapPlayers[i];
			if ( NULL == pPlayer )
			{
				D_ERROR( "CMuxNormalMapInfo::SwitchMapPlayer,地图玩家数组位置%d元素空\n", i );
				continue;
			}
			if( pPlayer->GetRace() != race )
			{
				continue;
			}

			finalPosX = RandNumber( posX1, posX2 );
			finalPosY = RandNumber( posY1, posY2 );

			CMuxMap* pMap = pPlayer->GetCurMap();
			if ( NULL == pMap )
			{
				D_ERROR( "CMuxNormalMapInfo::SwitchMapPlayer,玩家%s当前地图空\n", pPlayer->GetAccount() );
				continue;
			}
			if ( pPlayer->IsMoving() )
			{
				pPlayer->ResetMoveStat();
			}
			pMap->OnPlayerInnerJump( pPlayer, finalPosX, finalPosY );
		}//for;
	} else {
		//跳其它地图；
		D_DEBUG("CMuxNormalMapInfo::SwitchMapPlayer 跳其它地图\n");
		for ( int i=0; i<m_curMapPlayerNum; ++i )
		{
			pPlayer = m_arrMapPlayers[i];
			if ( NULL == pPlayer )
			{
				D_ERROR( "CMuxNormalMapInfo::SwitchMapPlayer,地图玩家数组位置%d元素空\n", i );
				continue;
			}
			if( pPlayer->GetRace() != race )
			{
				continue;
			}

			finalPosX = RandNumber( posX1, posX2 );
			finalPosY = RandNumber( posY1, posY2 );

			pPlayer->IssueSwitchMap( mapID, finalPosX, finalPosY );
		}//for;
	}


	return;
	TRY_END;
	return;
}


bool CMuxNormalMapInfo::DelTypeMonster( unsigned int monsterClsID )
{
	if ( NULL == m_pMapMonsters )
	{
		D_ERROR( "CMuxMap::DelTypeMonster， NULL == m_pMapMonsters\n" );
		return false;
	}

	HashTbPtrEle<CMonster>* pFound = NULL;
	CMonster* pMonster = NULL;
	m_pMapMonsters->InitExplore();		
	while ( NULL != (pFound=m_pMapMonsters->ExploreNext()) )
	{
		pMonster = pFound->GetInnerPtr();
		if ( NULL == pMonster )
		{
			continue;
		}

		//地图内跳转 
		if( pMonster->GetClsID() == monsterClsID && pMonster->IsToDelAll() )
		{
			pMonster->SetDelAllFlag(false);
			if ( pMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ )
			{
				D_ERROR( "普通地图或伪副本：跟踪重复删怪物bug，已设删除标记怪物%d重复死亡\n", pMonster->GetClsID() );
				continue;
			}
			pMonster->OnMonsterDie( NULL, GCMonsterDisappear::M_VANISH, false );			//没人杀死,不掉包
		}
	}
	return true;
	TRY_END;
	return false;
}

bool CMuxNormalMapInfo::AddToDelMonsterType( unsigned int monsterClsID )
{
	if ( NULL == m_pMapMonsters )
	{
		D_ERROR( "CMuxMap::AddToDelMonsterType， NULL == m_pMapMonsters\n" );
		return false;
	}

	HashTbPtrEle<CMonster>* pFound = NULL;
	CMonster* pMonster = NULL;
	m_pMapMonsters->InitExplore();		
	while ( NULL != (pFound=m_pMapMonsters->ExploreNext()) )
	{
		pMonster = pFound->GetInnerPtr();
		if ( NULL == pMonster )
		{
			continue;
		}

		if( pMonster->GetClsID() == monsterClsID )
		{
			pMonster->SetDelAllFlag(true);
		}
	}
	return true;
	TRY_END;
	return false;
}


//创建地图上玩家的buff
void CMuxNormalMapInfo::CreateMapBuff( unsigned int race, unsigned int buffID, unsigned int buffTime )
{
		if ( ( m_curMapPlayerNum<0 )
		|| ( m_curMapPlayerNum>MAX_PLAYERNUM_INMAP )
		)
	{
		D_ERROR( "CMuxNormalMapInfo::CreateMapBuff,地图内当前人数%d不对\n", m_curMapPlayerNum );
		return;
	}

	if ( NULL == m_arrMapPlayers[0] )
	{
		D_ERROR( "CMuxNormalMapInfo::CreateMapBuff,地图内第一个玩家指针空\n" );
		return;
	}
	
	CPlayer* pPlayer = NULL;
	PlayerID createID;
	createID.dwPID = 0;
	createID.wGID = 0;
	//跳当前地图；
	for ( int i=0; i<m_curMapPlayerNum; ++i )
	{
		pPlayer = m_arrMapPlayers[i];
		if ( NULL == pPlayer )
		{
			D_ERROR( "CMuxNormalMapInfo::CreateMapBuff,地图玩家数组位置%d元素空\n", i );
			continue;
		}

		if( pPlayer->GetRace() != race )
		{
			continue;
		}

		pPlayer->CreateMuxBuffByID( buffID, buffTime, createID );
	}//for;
	
	return;
	TRY_END;
	return;
}


///杀地图上特定种族的玩家；
void CMuxNormalMapInfo::KillAllPlayerByRace( unsigned int race )
{
	if ( ( m_curMapPlayerNum<0 )
		|| ( m_curMapPlayerNum>MAX_PLAYERNUM_INMAP )
		)
	{
		D_ERROR( "CMuxNormalMapInfo::KillAllPlayerByRace,地图内当前人数%d不对\n", m_curMapPlayerNum );
		return;
	}

	PlayerID attackID;
	attackID.dwPID = 0;
	attackID.wGID = 0;
	CPlayer* pPlayer = NULL;
	for ( int i=0; i<m_curMapPlayerNum; ++i )
	{
		pPlayer = m_arrMapPlayers[i];
		if ( NULL == pPlayer )
		{
			D_ERROR( "CMuxNormalMapInfo::KillAllPlayerByRace,地图玩家数组位置%d元素空\n", i );
			continue;
		}
		if( pPlayer->GetRace() != race )
		{
			continue;
		}

		pPlayer->SubPlayerHp( pPlayer->GetCurrentHP(), NULL, attackID );
	}

	return;
	TRY_END;
	return;
}

#ifdef HAS_PATH_VERIFIER
void CMuxNormalMapInfo::OnBlockQuadEnable(int regionId)
{
	CBlockQuadManager::BQData *pBlockQuad = CBlockQuadManager::GetBlockRanage(regionId);
	if(NULL == pBlockQuad)
		return;

	unsigned short minBlockX, minBlockY, maxBlockX, maxBlockY;
	minBlockX = minBlockY = maxBlockX = maxBlockY = 0;
	MuxMapSpace::GridToBlockPos(pBlockQuad->minX, pBlockQuad->minY, minBlockX, minBlockY);
	MuxMapSpace::GridToBlockPos(pBlockQuad->maxX, pBlockQuad->maxY, maxBlockX, maxBlockY);

	MuxMapBlock* pBlock = NULL;
	std::vector<CPlayer*> vecPlayer;
	for ( unsigned short y=minBlockY; y<=maxBlockY; ++y )
	{
		for ( unsigned short x=minBlockX; x<=maxBlockX; ++x )
		{
			pBlock = GetMapBlockByBlock( x,y );
			if ( NULL != pBlock )
			{
				pBlock->GetPlayersInRect(pBlockQuad->minX, pBlockQuad->minY, pBlockQuad->maxX, pBlockQuad->maxY, vecPlayer);
			}
		}
	}

	//让块内玩家死亡
	size_t ssize = vecPlayer.size();
	if(ssize > 0)
	{
		CPlayer *pPlayer = NULL;
		for(int i = 0; i < ssize; i++)
		{
			pPlayer = vecPlayer[i];

			if((NULL != pPlayer) && (pPlayer->IsAlive()))
			{
				pPlayer->SubPlayerHp( pPlayer->GetCurrentHP(), NULL, pPlayer->GetFullID() );
			}
		}
	}

	return;
}
#endif/*HAS_PATH_VERIFIER*/


