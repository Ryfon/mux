﻿/**
* @file mapsercer.cpp
* @brief 地图服务器的主逻辑
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: mapserver.cpp
* 摘    要: 
* 作    者: hyn
* 完成日期: 
*
*/

#if defined(_WIN32) || defined(_WIN64)
#define  _SECURE_SCL_THROWS 1
#endif

#include <vector>
#include <list>
#include <iostream>

#include "LoadConfig/LoadSrvConfig.h"
#include "PlayerSkillProp.h"

#include "MuxMap/manmapproperty.h"
#include "MuxMap/manmuxcopy.h"
#include "MuxMap/maprelation.h"

/*缓冲队列*/ 
#include "BufQueue/BufQueue.h"
#include "BufQueue/BufQueueImp.h"
#include "PkgProc/MsgToPut.h"  /*包结构*/
#include "MuxMap/mannormalmap.h"   /*地图管理类*/
/*网络模块*/
//#include "NetTask.h"
#include "NetTaskEx.h"
//#include "HandlerT.h"
//#include "CacheMessageBlock.h"
/*收包组包模块*/
#include "PkgProc/RcvPkgHandler.h"	 //收包处理器(CRcvPkgHandler<CServer>)
#include "PkgProc/CManCommHandler.h"  //所有连接管理器
#include "DealG_MPkg.h"
/*对象池*/
#include "tcache/tcache.h"

//crash模块
#ifdef ACE_WIN32
#include "CrashLib/CrashLib.h"
#endif

/*怪物*/
#include "monster/monster.h"
#include "monster/manmonstercreator.h"
/*玩家与怪物攻击技能*/
#include "ManPlayerProp.h"
#include "GMCmdManager.h"
#include "NpcSkill.h"
#include "BuffProp.h"
#include "SpSkillManager.h"

#include "Lua/FightPreScript.h" //战斗判断脚本管理器；
#include "Lua/FightCalScript.h" //战斗计算脚本管理器；
#ifdef LEVELUP_SCRIPT
	#include "Lua/LevelUpScript.h"
#endif

#ifdef AI_SUP_CODE
#include "AISys/MonsterSkillSet.h"
#include "AISys/RuleAIEngine.h"
#include "AISys/MonsterSkillSet.h"
#endif

#include "Lua/NpcChatScript.h"

#include "Item/CItemPackage.h"
#include "Item/LoadDropItemXML.h"
#include "Item/RollItemManager.h"
#include "GroupTeamManager.h"
#include "GoodEvilControl.h"
#include "Item/CItemPropManager.h"
#include "Item/LoadDropItemXML.h"
#include "Item/RepairWearRule.h"
#include "Item/ItemUpdateConfig.h"
#include "Rank/rankeventmanager.h"
#include "CreatePtSet.h"
#include "npcshop/npcshop.h"
#include "Activity/WarTaskManager.h"
#include "ItemSkill.h"
#include "IBuff.h"
#include "Activity/PunishmentManager.h"
#include "MapPublicNoticeManager.h"
#ifdef OPEN_PET		//是否开启宠物
	#include "Player/PetManager.h"
#endif //OPEN_PET

#include "Item/ReadRandProps.h"

#ifdef USE_DSIOCP
#include "iocp/dsiocp.h"
#endif //USE_DSIOCP

#include "../../Test/testthreadque_wait/sigexception/sigexception.h"

#include "MuxSkillConfig.h" //copy...,
#include "Lua/BattleScript.h"
#include "Lua/CalSecondProperty.h"

#ifdef USE_DSIOCP
TCache< CDsSocket >* g_poolDsSocket = NULL;
TCache< UniqueObj< CDsSocket > >* g_poolUniDssocket = NULL;
TCache< PerIOData >* g_poolPerIOData = NULL;
TCache< DsIOBuf >*   g_poolDsIOBuf = NULL;
#endif //USE_DSIOCP

#ifdef WAIT_QUEUE
#include "WaitQueue.h"
#endif //WAIT_QUEUE

using namespace std;

///声明全局变量
TCache< MsgToPut >* g_poolMsgToPut;
CManCommHandler< CRcvPkgHandler<CSrvConnection> >* g_ManCommHandler;
CPoolManager* g_poolManager; 
const unsigned char SELF_PROTO_TYPE = 0x05;//自身为MapSrv，定义见srvProtocol.h;

extern unsigned int g_maxMapSrvPlayerNum;

#ifdef PRINT_PKGNUM //测试集聚情形；
		//统计发包数量；
       unsigned int g_playerInfoNum = 0;
	   unsigned int g_monsterInfoNum = 0;
#endif //PRINT_PKGNUM //测试集聚情形；


// 全局小缓存池
//CCacheMessageBlock *g_poolSmallMB;

///声明静态量
IBufQueue* g_pPkgSender;
//map<unsigned short, PFN_PKG> CDealG_MPkg::m_mapDeals;

#ifdef GATE_SENDV
void VeryFastTimer( const ACE_Time_Value& curTime )
{
	CManG_MProc::CheckSend( curTime );
}
#endif //GATE_SENDV

//void FastTimer()
//{
//	CManMonster::MonsterFastTimer();//每次快时钟执行怪物管理器的快时钟一次；
//}

void MonsterUpdateTimer()
{
	CManMonster::MonsterSlowTimer();//每次慢时钟执行怪物管理器的慢时钟一次；
	return;
}

void NormalMapUpdateTimer( const ACE_Time_Value& curTime )
{
	CManNormalMap::MapManagerSlowTimer( curTime );//执行地图慢时钟；
	return;
}

void ShopSupplyTimer( const ACE_Time_Value& curTime )
{
	CNpcShopManager::NpcShopManTimerProc( curTime );
	return;
}


#ifdef USE_SIG_TRY
bool ExceptionCheck()
{
	if( g_isException )
	{
		MGExceptionNotify expMsg;
		expMsg.exceptionReason = 0;
		expMsg.srvId = CLoadSrvConfig::GetSelfID();
		CGateSrv* pGateSrv = CManG_MProc::FindRandGateSrv();
		if( NULL != pGateSrv )
		{
			MsgToPut* pNewMsg = CreateSrvPkg( MGExceptionNotify, pGateSrv, expMsg );
			pGateSrv->SendPkgToGateServer( pNewMsg );
		}
	}
	return true;
}
#endif //USE_SIG_TRY

void SlowTimer()
{
#ifdef PRINT_PKGNUM //测试集聚情形；
	static ACE_Time_Value debugSt = ACE_OS::gettimeofday();
	if ( (ACE_OS::gettimeofday() - debugSt).sec() > 5 )
	{
		debugSt = ACE_OS::gettimeofday();
		D_DEBUG( "5秒内，通知个人信息数%d, 总信息数:%d\n", g_playerInfoNum, g_monsterInfoNum );
		g_playerInfoNum = 0;
		g_monsterInfoNum = 0;
	}
#endif //PRINT_PKGNUM

#ifdef WAIT_QUEUE
	static ACE_Time_Value waitQueSt = ACE_OS::gettimeofday();
	if ( (ACE_OS::gettimeofday() - waitQueSt).sec() > 5 )
	{
		waitQueSt = ACE_OS::gettimeofday();
		CWaitQueue::WaitQueTimeProcess();
	}
#endif //WAIT_QUEUE

    CManG_MProc::ManGateSrvTimer();//通知gate自身是否可进入；

	CPlayerManager::PlayerSlowTimer();//执行玩家慢时钟;

	CBuffManager::TimeProc();

	//@brief:每六分钟刷新道具
	CItemPackageManagerSingle::instance()->MonitorItemPkgLive();

	//监视每个Roll点的状况
	RollItemManagerSingle::instance()->MonitorRollItemStatus();

	//每半小时，检验一次是否该更换掉落规则
	DropRuleStateSingle::instance()->MonitorCheckTime();

	//更新每个玩家的恶魔在线时间
	CEvilTimeControlManagerSingle::instance()->UpdateEvilTime();

	//更新每个人的限时任务的时间
	CLimitTimeTaskManagerSingle::instance()->MonitorLimitTimeTask();

	//更新排行榜信息
	//RankEventManagerSingle::instance()->UpdateRankCharts();
	//RankEventManager::UpdateRankCharts();

	//已取消种族，by dzj, 10.07.30，AllRaceMasterManagerSingle::instance()->MontiorRaceMasterTimer();

	//察看是否需要发布公告
	MapPublicNoticeManager::SendMapPublicNotice();
}

///时钟检测函数，循环执行以确定是否需要执行快时钟或慢时钟；
void TimerCheck()
{
	static ACE_Time_Value veryLastTime = ACE_OS::gettimeofday();
	static ACE_Time_Value monsterLastTime = ACE_OS::gettimeofday();
	static ACE_Time_Value mapLastTime = ACE_OS::gettimeofday() + ACE_Time_Value( 0, (RAND%57)*1000 );
	static ACE_Time_Value slowLastTime = ACE_OS::gettimeofday();
	static ACE_Time_Value shopLastTime = ACE_OS::gettimeofday();
	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	ACE_Time_Value veryPastTime = curTime - veryLastTime;
	ACE_Time_Value monsterPastTime = curTime - monsterLastTime;
	ACE_Time_Value mapPastTime = curTime - mapLastTime;
	ACE_Time_Value slowPastTime = curTime - slowLastTime;
	ACE_Time_Value shopPastTime = curTime - shopLastTime;

	//天谴的更新每10秒1次
#ifdef OPEN_PUNISHMENT
	static ACE_Time_Value punishmentLastTime = ACE_OS::gettimeofday();
	if( curTime.sec() - punishmentLastTime.sec() >= 10 )
	{
		CPunishmentManager::TimeProc();
		CWarTaskManager::CityWarTimer();
		punishmentLastTime = curTime;
	}
#endif 
	
#ifdef GATE_SENDV
	if ( veryPastTime.msec() > 10 )
	{
		veryLastTime = curTime;
		VeryFastTimer( curTime );
	}
#endif //GATE_SENDV

#ifndef AUTO_EXP_UNIT
	const unsigned int interbias = (unsigned int) (500/EXPLORE_UNIT_PROP);
	if ( monsterPastTime.msec() > interbias )
#endif //AUTO_EXP_UNIT
	{
		monsterLastTime = curTime;
		//float testf = 1.234;
		//for ( int i=0; i<1000000; ++i )
		//{
		//	testf = testf*testf;
		//	if ( testf > 20000 )
		//	{
		//		testf = 1.234;
		//	}
		//}
		//if ( 0==RAND % 10 )
		//{
		//	printf( "%f", testf );
		//}
		MonsterUpdateTimer();	    
	}

#ifndef AUTO_EXP_UNIT
	if ( mapPastTime.msec() > interbias )
#endif //AUTO_EXP_UNIT
	{
		mapLastTime = curTime;
		if ( !CLoadSrvConfig::IsCopyMapSrv() )
		{
			//普通地图管理器时钟；
			NormalMapUpdateTimer( curTime );
		} else {
			//副本地图管理器时钟；
			CManMuxCopy::ManCopyTimerProc( curTime );
		}
	}

	//if ( fastPastTime.msec() > 361 ) //361毫秒；
	//{
	//	//激活快时钟；
	//	fastLastTime = curTime;
	//	FastTimer();
	//} 
	if ( slowPastTime.msec() > 517 ) //517毫秒；
	{
		//激活慢时钟；
		slowLastTime = curTime;
		SlowTimer();
	}

	if ( shopPastTime.msec() > 100163 ) //100秒；
	{
		//激活慢时钟；
		shopLastTime = curTime;
		ShopSupplyTimer( curTime );
	}


#ifdef USE_SIG_TRY
	static ACE_Time_Value exceptionCheckLastTime = ACE_OS::gettimeofday();
	ACE_Time_Value exceptionPastTime = curTime - exceptionCheckLastTime;
	if( exceptionPastTime.sec() >= 10 )   //每10秒检测一次
	{
		exceptionCheckLastTime = curTime;
		ExceptionCheck();
	}
#endif //USE_SIG_TRY
	return;
}

//加载服务器的XML数据
void LoadItemXML()
{
	ReadItemRandomProps();//随机属性集管理；
	MoneyGeneratorSingle::instance()->LoadXML("config/item/dropmoney.xml");
	DropCommonItemRuleSingle::instance()->LoadXML("config/item/dropcommon.xml");
	MonsterDropItemSetSingle::instance()->LoadXML("config/item/dropset.xml");
	DropStandByItemRuleSingle::instance()->LoadXML("config/item/dropstandby.xml");
	DropSpecialItemRuleSingle::instance()->LoadXML("config/item/dropspecial.xml");
	//RankUpdateTimeManagerSingle::instance()->ReadUpdateTimeConfig();
	//RankChartPropManagerSingle::instance()->LoadXML("config/rank/ranklist.xml");
	CWeaponPropertyManagerSingle::instance()->LoadXML("config/item/weapon.xml");
	CEquipPropertyManagerSingle::instance()->LoadXML("config/item/equip.xml");
	CFunctionPropertyManagerSingle::instance()->LoadXML("config/item/functionitem.xml");
	CAxiologyPropertyManagerSingle::instance()->LoadXML("config/item/axiology.xml");
	CSkillBookPropertyManagerSingle::instance()->LoadXML("config/item/skillbook.xml");
	CConsumePropertyManagerSingle::instance()->LoadXML("config/item/expendable.xml");
	CTaskPropertyManagerSingle::instance()->LoadXML("config/item/taskitem.xml");
	BulePrintManagerSingle::instance()->LoadXML("config/item/buleprint.xml");
	CDropTaskItemSetSingle::instance()->LoadXML("config/item/droptask.xml");
	CRepairWairRuleManagerSingle::instance()->LoadXML("config/item/wear.xml");
	CFashionPropertyManagerSingle::instance()->LoadXML("config/item/clothes.xml");
	CMountPropertyManagerSingle::instance()->LoadXML("config/item/mount.xml");
	MountLevelUpPropManagerSingle::instance()->LoadXML("config/item/mountadd.xml");
	ItemUpdateConfig::LoadItemUpdateConfig();
	DropRuleStateSingle::instance()->CheckRuleState();
}

/////////////////////////////////////////////////////////////////////
//copy...
void LoadSkillXML()
{
	PlayerDamPropSingleton::instance()->LoadSkillPropT("config/skill/PlayerDam.xml");
	NpcDamPropSingleton::instance()->LoadSkillPropT("config/skill/MonsterDam.xml");
	ItemSkillPropSingleton::instance()->LoadSkillPropT("config/skill/Heal.xml");
	BuffPropSingleton::instance()->LoadSkillPropT("config/skill/Buff.xml");
	SpiltPropSingleton::instance()->LoadSkillPropT("config/skill/Split.xml");
	MuxSkillPropSingleton::instance()->LoadSkillPropT("config/skill/skill.xml");
	CManSkillExp::Init();
}
//...copy
/////////////////////////////////////////////////////////////////////

#ifndef ACE_WIN32
void catchpipe(int signo)
{
	D_WARNING( "收到SIGPIPE信号%d，忽略\n", signo );
	exit( 0 );
}
#endif  //ACE_WIN32

static void sigIntHandler(int signo)
{
	D_DEBUG("收到%d信号", signo);
	return;
}

#ifdef ACE_WIN32
#include "Mmsystem.h"
#endif //ACE_WIN32

///应用程序主函数；
int ACE_TMAIN(int argc, ACE_TCHAR* argv[])
{
#ifdef ACE_WIN32
	CCrashLib CrashHandler;
#endif
	
	TRY_BEGIN;

    MainInitTls();

	int jjj = sizeof(FullPlayerInfo);

#ifdef ACE_WIN32
	timeBeginPeriod(1);
#endif //ACE_WIN32

//#ifdef ACE_WIN32
//	//g_pfnPreSignalHandler = signal( SIGINT, SignalHandler);
//#else //ACE_WIN32
//	static struct sigaction act;
//	act.sa_handler = catchpipe;
//	sigfillset( &(act.sa_mask) );
//	sigaction( SIGPIPE, &act, NULL );
//#endif //ACE_WIN32

	//@brief:MapServer的Debug情况,

	srand( (unsigned int)time(0) );  //随即种

	if( Parse_args( argc, argv ) == -1 )
		return 0;

	D_INFO( "最大容许人数%d\n", g_maxMapSrvPlayerNum );

	if (  g_iLogType <= 2 || g_iLogType >= 0 )
	{
		SetLog("Debug_log.txt",g_iLogType );
	}
	else
	{
		return 0;
	}

	SetTSTP();//TSTP信号终止程序添加

	// 捕捉SIGINT信号
	ACE_Sig_Action handleSigInt(reinterpret_cast <ACE_SignalHandler>(sigIntHandler), SIGINT);
	ACE_UNUSED_ARG(handleSigInt);

	// 忽略SIGPIPE信号
	ACE_Sig_Action noSigPipe((ACE_SignalHandler)SIG_IGN, SIGPIPE);
	ACE_UNUSED_ARG(noSigPipe);

	/*先生成POOL 切记!!!!*/
	g_poolManager = NEW CPoolManager;
	g_poolManager->Intialize();	
	g_poolMsgToPut = NEW TCache< MsgToPut >(200000);  ///测试用30条,以后改

#ifdef USE_DSIOCP
	CDsSocket::Init();
    g_poolPerIOData = NEW TCache< PerIOData >( 50000 );//尽量分配足够；
    g_poolUniDssocket = NEW TCache< UniqueObj< CDsSocket > >( 50000 );//尽量分配足够；
	g_poolDsSocket = NEW TCache< CDsSocket>( 256 );//全局DsSocket对象池，必须分配足够，因为只能使用Retrieve而不能使用RetrieveOrCreate；
	g_poolDsIOBuf = NEW TCache< DsIOBuf >( 2560 );//因为服务器端连接数少，虽然通信量大；
#else //USE_DSIOCP
	//// 创建全局小缓冲块池
	//g_poolSmallMB = NEW CCacheMessageBlock(10, 50*1024);
	//if(g_poolSmallMB == NULL)
		return -1;
	//// 自动销毁
	//auto_ptr< CCacheMessageBlock > poolAutoCleanSmall(g_poolSmallMB);
#endif //USE_DSIOCP

	//ACE_UNUSED_ARG( argc );
	//ACE_UNUSED_ARG( argv );

	//读服务配置信息；
	if ( ! CLoadSrvConfig::LoadConfig() )
	{
		D_WARNING( "服务配置错！服务启动失败\n" );
		return 0;
	}
	D_INFO("读取服务配置成功\n");

	if( !CBattleScriptManager::Init() )
	{
		D_ERROR("初始化战斗脚本管理器失败\n");
		return 0;	
	}
	D_INFO("读取战斗脚本成功\n");

	if( !CCalSecondPropertyScriptManager::Init() )
	{
		D_ERROR("二级属性脚本管理器失败\n");
		return 0;
	}
	D_INFO("读取二级属性计算脚本成功\n");

    D_INFO( "服务类型MapSrv，序号:%d\n", CLoadSrvConfig::GetSelfID() );

	if(!CMapRelation::InitMapRelation())
	{
		D_ERROR("地图关系文件读取失败\n");
		return 0;
	}
	D_INFO("读取地图关系文件成功\n");

	if( !CWarTaskManager::InitWarTaskManager() )
	{
		D_ERROR("读取warconfig.xml失败\n");
		return 0;
	}
	D_INFO("读取warconfig.xml成功\n");

#ifdef HAS_PATH_VERIFIER
	if(!CBlockQuadManager::InitBlockQuards())
	{
		D_ERROR("地图碰撞信息读取失败\n");
		return 0;
	}
	D_INFO("读取地图碰撞信息成功\n");
#endif/*HAS_PATH_VERIFIER*/

    if ( !CManMapProperty::InitAllMapProperty() )
	{
		D_ERROR( "地图属性读取失败\n" );
		return 0;
	}
	D_INFO("读取地图属性成功\n");

	if ( !CManMuxCopy::InitManMuxCopy() )
	{
		D_ERROR( "副本管理器初始化失败\n" );
		return 0;
	}
	D_INFO("读取副本管理器成功\n");

	if ( !CNpcShopManager::NpcShopManagerInit( "config/shop/npcshop.xml" ) )
	{
		D_WARNING( "读取NPC商店配置文件失败\n" );
		return -1;
	}
	D_INFO("读取NPC商店配置文件成功\n");

	if ( !CNpcShopManager::NpcSkillShopManagerInit( "config/shop/skillshop.xml" ) )
	{
		D_WARNING( "读取NPC技能商店配置文件失败\n" );
		return -1;
	}
	D_INFO("读取NPC技能商店配置文件成功\n");

	srand( ACE_OS::gettimeofday().usec() );

	LoadItemXML();
	D_INFO("LoadItemXML成功\n");

	LoadSkillXML();//copy...
	D_INFO("LoadSkillXML成功\n");

#ifdef AI_SUP_CODE
	string ErrorCode;

	bool bRes = CRuleAIEngine::Instance()->LoadData("config/scripts/ruleai.lua", ErrorCode);
	if(bRes == false)
	{
		string str1 = "'RuleAI.lua'中数据有错,原因:";
		string msg = str1 + ErrorCode + "\n";
		//D_ERROR( msg.c_str() );
		printf(msg.c_str());
		return -1;
	}
	D_INFO("读取config/scripts/ruleai.lua成功\n");

	bRes = MonsterSkillSet::Instance()->Load(CRuleAIEngine::Instance()->GetLuaState(), 
		"config/scripts/monsterskillset.lua", ErrorCode);
	if(bRes == false)
	{
		string str1 = "'monsterskillset.lua'中数据有错,原因:";
		string msg = str1 + ErrorCode + "\n";
		//D_ERROR( msg.c_str() );
		printf(msg.c_str());
		return -1;
	}
	D_INFO("读取config/scripts/monsterskillset.lua成功\n");

#endif //AI_SUP_CODE

	//unsigned short SELF_SRV_ID = (unsigned short) CLoadSrvConfig::GetSelfID();	
	if( !ReadVersion("../version.txt") )  //与张涛商定为上一级的目录的version.txt
	{
		D_ERROR("读取版本信息失败\n");
		return -1;
	}
	D_INFO("读取版本信息成功\n");

	if( !CManBuffProp::Init() )
	{
		D_WARNING("读取BUFF属性失败\n");
		return -1;
	}
	D_DEBUG("读取BUFF属性成功\n");

	if ( !CNormalScriptManager::Init() )
	{
		D_ERROR( "读取一般普通脚本失败\n" );
		return -1;
	}
	D_INFO("读取一般普通脚本成功\n");

#ifdef LEVELUP_SCRIPT
	if( !CLevelUpScriptManager::Init() )
	{
		D_ERROR("读取升级脚本失败\n");
		return -1;
	}
	D_INFO("读取升级脚本成功\n");
#endif

#ifdef OPEN_PET
	if( !CPetManager::Init() )
	{
		D_ERROR("读取宠物配置失败\n");
		return -1;
	}
	D_INFO("读取宠物配置成功\n");
#endif //OPEN_PET



	//CNormalCounterPropManager::Init(); //暂时不判断返回值，因为现在相关的配置文件还没有配；
	CNormalTaskPropManager::Init(); //暂时不判断返回值，因为现在相关的配置文件还没有配；

#ifdef OPEN_PUNISHMENT
	if( !CPunishmentManager::Init() )
	{
		D_ERROR("读取天谴活动配置文件失败\n");
		return -1;
	}
	D_INFO("读取天谴活动配置文件成功\n");
#endif

	if( !CManPlayerProp::Init() )  //玩家职业基础属性配置初始化 3.6 hyn
	{
		D_ERROR( "读取职业属性配置失败\n" );
		return -1;
	}
	D_INFO("读取职业属性配置成功\n");

	if( !CSpSkillManager::Init() )
	{
		D_ERROR("读取SP配置失败\n");
		return -1;
	}
	D_INFO("读取SP配置成功\n");


	CGMCmdManager::GMCmdInit(); //3.21 hyn GM命令初始化
	
	if( ! CManNpcSkill::Init() )
	{
		D_ERROR( "读取npc技能配置失败\n" );
		return -1;
	}
	D_INFO("读取npc技能配置成功\n");
	
	if( !CItemSkillManager::LoadFromXML("config/common/itemskill.xml") )
	{
		D_ERROR("LoadItemSkill 失败\n");
		//return false;
	}
	D_INFO("LoadItemSkill成功\n");

	if( !CManNPCProp::Init() ) //初始化怪物属性  04.08 hyn
	{
		D_ERROR( "读取NPC属性配置失败\n" );
		return -1;
	}
	D_INFO("读取NPC属性配置成功\n");

	/*因为怪物要载入状态机,所以状态机要在怪物生成前先进行初始化,但是状态机又是根据属性确定组合，所以要在CManMonsterProp后面生成!!!*/  //updated by hyn 1.17
	if( !CManMonsterCreatorProp::InitAllMonsterProp() ) //初始化怪生成器管理器;
	{
		D_ERROR( "怪物生成器读取配置文件出错\n" );
		return -1;
	}
	D_INFO("读取怪物生成器读取配置文件成功\n");

	CManMonster::Init();//初始化怪物管理器；

	if ( !CPlayerManager::Init() )
	{
		D_ERROR( "初始化玩家管理器失败\n" );
		return -1;
	}
	D_INFO("初始化玩家管理器失败成功\n");

	if(CUnionConfigSingle::instance()->Init())
	{
		return -1;
	}
	
	if(CUnionSkillManagerSingle::instance()->Init())
	{
		return -1;
	}

	
	CCreatePtSetManager::Init();

	////////////////////////////////////////////////////////////////////////////////////////
	//战斗判定脚本；
	if ( ! CFightPreScriptManager::Init() )//战斗判断脚本管理器；
	{
		D_ERROR( "初始化战斗判断脚本管理器失败\n" );
		return -1;
	}
	D_INFO("读取初始化战斗判断脚本管理器成功\n");

	if ( ! CFightCalScriptManager::Init() )//战斗计算脚本管理器；
	{
		D_ERROR( "初始化战斗公式计算脚本管理器失败\n" );
		return -1;
	}
	D_INFO("读取初始化战斗判断脚本管理器成功\n");


	if( !CManNormalMap::InitAllNormalMap() )
	{
		D_ERROR( "读取的地图文件找不到\n");
		return -1;
	}
	D_INFO("读取的地图文件成功\n");

	CDealG_MPkg::Init();   //Gateserver的包处理器初始化


	IBufQueue* pReadNetMsgQueue = NEW CBufQueue;	//从此队列读网络消息
	IBufQueue* pSendNetMsgQueue = NEW CBufQueue;	//向此队列写网络消息
	g_pPkgSender = pSendNetMsgQueue;

	
	g_ManCommHandler = NEW CManCommHandler< CRcvPkgHandler<CSrvConnection> >;  ///句柄管理类
	g_ManCommHandler->SetNetQueue( pReadNetMsgQueue );//收包句柄管理，置收包来源(收包队列)
	g_ManCommHandler->SetHandlePoolSize( 256 );//收包句柄管理，设置收包句柄对象池

	//CGateSrv::SetPkgSender( pSendNetMsgQueue );	//置发包执行者(发包队列) //静态设置

#ifdef USE_DSIOCP
	CDsIocp< IBufQueue, IBufQueue, CDsSocket >* srvIocp = NEW CDsIocp< IBufQueue, IBufQueue, CDsSocket >;
	srvIocp->Init( 128, pReadNetMsgQueue, pSendNetMsgQueue );
	srvIocp->AddListen( CLoadSrvConfig::GetListenPort() );
	D_INFO( "mapsrv(IsCopyMapSrv-%d):监听地址：%s 端口：%d\n",CLoadSrvConfig::IsCopyMapSrv(),CLoadSrvConfig::GetListenAddr(),CLoadSrvConfig::GetListenPort() );
#else //USE_DSIOCP
#ifdef USE_NET_TASK
	///开创网络事件线程
	CNetTask<CHandlerT> myTask(pReadNetMsgQueue, pSendNetMsgQueue);
	///初始化并激活线程
	ACE_INET_Addr cliListenAddr( CLoadSrvConfig::GetListenPort() );
	//ACE_INET_Addr cliListenAddr(6801);
	if(myTask.open( &cliListenAddr ) == -1)
	{
		D_DEBUG("mytask activate error ,d=%d\n", 1);
		return 0;
	}
#else
	///开创网络事件线程
	CNetTaskEx<CHandlerEx, ACE_Thread_Mutex> myTask(pReadNetMsgQueue, pSendNetMsgQueue);
	///初始化并激活线程
	ACE_INET_Addr cliListenAddr( CLoadSrvConfig::GetListenPort() );
	//ACE_INET_Addr cliListenAddr(6801);
	if(myTask.Open( &cliListenAddr ) == -1)
	{
		D_DEBUG("mytask activate error ,d=%d\n", 1);
		return 0;
	}
#endif/*USE_NET_TASK*/
#endif //USE_DSIOCP

	////TODO:在此开创网络和DB线程,并把消息队列作参共享

	///主线程序的循环
	//以下主线程继续工作，其间通过pWriteQueue、pReadQueue与网络线程通信；
	ACE_Time_Value stTime = ACE_OS::gettimeofday();
	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	ACE_Time_Value pastTime;
	ACE_Time_Value connectMapsrvTime = ACE_OS::gettimeofday();
	ACE_Time_Value connectMapsrvpassed = ACE_OS::gettimeofday() - connectMapsrvTime;

	D_INFO( "mapsrv:运行中...\n" );

	const unsigned long runTime = CLoadSrvConfig::GetRunTime();
	while (!g_isGlobeStop)
	{
		curTime = ACE_OS::gettimeofday();
		pastTime = curTime - stTime;
		if ( ( runTime>0 )  //runTime == 0 不中断运行；
			&& ( pastTime.sec()>(int)runTime )
			)
		{
#ifndef USE_DSIOCP
			myTask.Quit();
			g_pPkgSender->SetReadEvent( true );//通知网络模块发送；
#endif //USE_DSIOCP
			D_DEBUG( "runTime时刻到，退出\n" );
			break;
		}

		ST_SIG_CATCH {

			TimerCheck();

			g_pPkgSender->SetReadEvent( false );//通知网络模块发送；				

			g_ManCommHandler->CommTimerProc();  ///里面任务为接受所有消息并处理

			//g_pPkgSender->SetReadEvent();//通知网络模块发送；

			//ACE_Time_Value tmpTime;//等待一会；
			//tmpTime.sec(0);
			//tmpTime.usec( 1000 );
			//ACE_OS::sleep( tmpTime );//???,邓子建检查,08.08.19;

			///DB 操作

			///接下来是NPC的操作,用定时器进行时间片的管理,暂时先不启动
			/*NPC*/


			///最后物品管理,整个逻辑主循环一次结束
		} END_SIG_CATCH;
	}

#ifndef USE_DSIOCP
#ifdef USE_NET_TASK
	myTask.wait();  ///等待线程结束
#else
	myTask.Wait();
#endif/*USE_NET_TASK*/
#endif //USE_DSIOCP

	D_DEBUG( "服务器正常结束\n" );

	if( NULL != g_ManCommHandler)
	{
		delete g_ManCommHandler;g_ManCommHandler = NULL;
	}

	CPlayerManager::Release();

#ifdef USE_DSIOCP
	delete srvIocp; srvIocp = NULL;
#endif //USE_DSIOCP

	if( pReadNetMsgQueue != NULL)
	{
		delete pReadNetMsgQueue;pReadNetMsgQueue = NULL;
	}
	if( pSendNetMsgQueue != NULL)
	{
		delete pSendNetMsgQueue;pSendNetMsgQueue = NULL;
	}

#ifdef USE_DSIOCP
	//删DsSocket对象池；
	if ( NULL != g_poolDsSocket )
	{
		delete g_poolDsSocket; g_poolDsSocket = NULL;
	}
    if ( NULL != g_poolDsIOBuf )
	{
		delete g_poolDsIOBuf; g_poolDsIOBuf = NULL;
	}
	//删唯一对象对象池；
	if ( NULL != g_poolUniDssocket )
	{
		delete g_poolUniDssocket; g_poolUniDssocket = NULL;
	}
	if ( NULL != g_poolPerIOData )
	{
		delete g_poolPerIOData; g_poolPerIOData = NULL;
	}
#endif //USE_DSIOCP

	
	CFightPreScriptManager::Release();//战斗判断脚本管理器；
	CFightCalScriptManager::Release();//战斗计算脚本管理器；
	CCreatePtSetManager::Release();
	CBattleScriptManager::Release();
	CCalSecondPropertyScriptManager::Release();
	CItemSkillManager::Release();
	CManNormalMap::ReleaseAllNormalMap();   //释放地图 written by hyn 12.29
	CManMuxCopy::ReleaseManMuxCopy();
	CManPlayerProp::ReleaseManedObj();  //释放职业玩家属性 3.6 hyn
	CManNPCProp::ReleaseManedObj();//释放管理的NPCProp 08.04.15 by dzj;
	CManNpcSkill::ReleaseManedObj();//释放管理的NPCProp 08.04.15 by dzj;
	CManSkillExp::ReleaseManedObj();	//3.20 hyn 技能经验配置释放
	CManMonster::ReleaseAllManedMonster();//释放所有管理的怪物；
	CManMonsterCreatorProp::ReleaseAllMonsterProp();//释放管理的怪物生成器属性；
	CManBuffProp::ReleaseManedObj();
	CWarTaskManager::Release();
#ifdef LEVELUP_SCRIPT
	CLevelUpScriptManager::Release();
#endif

	CNormalTaskPropManager::Release();
	//CNormalCounterPropManager::Release();

	CNormalScriptManager::Release();

#ifdef STAGE_MAP_SCRIPT
	CMapScriptManager::Release();
#endif //STAGE_MAP_SCRIPT

	CNpcShopManager::NpcShopManagerRelease();
	
    CManMapProperty::ReleaseAllMapProperty();

	///对象pool一定要最后释放！！！！！切记！！！
	if ( NULL != g_poolMsgToPut )
	{
		delete g_poolMsgToPut; g_poolMsgToPut = NULL;
	}
	if(NULL != g_poolManager)
	{
		delete g_poolManager;g_poolManager = NULL;
	}

#ifdef ACE_WIN32
	timeEndPeriod(1);
#endif //ACE_WIN32

	MainEndTls();

	return 0;
	TRY_END;
	return 0;
}
