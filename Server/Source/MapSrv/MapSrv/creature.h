﻿/**
* @file creature.h
* @brief 定义生物接口
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: creature.h
* 摘    要: 定义生物接口，应晓强要求添加
* 作    者: dzj
* 完成日期: 2008.09.01
*
*/
#pragma once

class CMuxCreature
{
public:
  enum CREATURE_TYPE 
  {
     CREA_TYPE_PLAYER = 0,
     CREA_TYPE_MONSTER
  };

public:
  virtual ~CMuxCreature() {};

public:
  virtual CREATURE_TYPE GetType() = 0;

  virtual bool GetPos( TYPE_POS& x, TYPE_POS& y ) = 0;

  virtual const PlayerID GetCreatureID() = 0;
};

