﻿/********************************************************************
	created:	2008/10/08
	created:	8:10:2008   16:01
	file:		TaskRecordManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef  TASKRECORDMANAGER_H_
#define  TASKRECORDMANAGER_H_

#include <cassert>

#include "../../Base/PkgProc/SrvProtocol.h"
using namespace MUX_PROTO;

class CPlayer;
class CNormalTask;

#define  COMMON_TASKRD_COUNT 20
#define  BACKGROUND_TASKRD_COUNT 30

class TaskRecordManager
{
public:
	TaskRecordManager():m_AttachPlayer(NULL),commonTaskRdArr(NULL),timeExpireTaskRdArr(NULL)
	{
	}

	~TaskRecordManager(){}

public:
	void AttachPlayer( CPlayer* pPlayer ); 

	TaskRecordInfo* GetTaskRecordInfoInCommon( unsigned int TaskID  );

	//更新任务信息给玩家
	void UpdateTaskRecordInfoToPlayer( const TaskRecordInfo&  taskRdInfo , char taskIndexType = 0 );

	//当接到新任务时回调
	TaskRecordInfo*  AddNewTaskRecord( CNormalTask* pNormalTask );

	//当接到DB发来的任务存盘信息时
	void OnRecvTaskInfoRecord( const TaskRecordInfo* taskInfoArr );

	//当完成一个任务时，回调
	void OnFinishTask( CNormalTask* pTask );

	//当任务的状态改变时
	void OnTaskStateChange(CNormalTask* pTask ,ETaskStatType taskStat );

	//计数器的个数改变时
	void OnTaskCounterNumChange( unsigned int taskID, unsigned counterNum );

	//删除任务信息
	void OnDeleteTask( unsigned int taskID );

	//找到任务存盘的的位置
	int FindCommonTaskRecordIndex( unsigned int taskID );

	int FindTimeExpireTaskRecordIndex( unsigned int taskID );

	//检查每日任务是不是过期了
	bool CheckEveryDayTaskExpire( unsigned int taskID );

	//获取新的过期任务位置
	int GetExpireTimeEmptyTaskRdIndex( unsigned int taskID );

	//获取普通任务列表
	TaskRecordInfo* GetCommonTaskRecordArr() { return commonTaskRdArr; }

	//获取时间过期任务列表
	TaskRecordInfo* GetTimeExpireTaskRecordArr() { return timeExpireTaskRdArr; }

	//当完成一个每日任务时回调
	void OnFinishEveryDayTask( unsigned int taskID );

private:

	//清除任务存盘信息
	void ClearTaskRecord(  TaskRecordInfo& taskInfo );

private:
	CPlayer* m_AttachPlayer;
	TaskRecordInfo* commonTaskRdArr;//记录和客户端相同的任务信息
	TaskRecordInfo* timeExpireTaskRdArr;//记录任务的后台信息
};

#endif
