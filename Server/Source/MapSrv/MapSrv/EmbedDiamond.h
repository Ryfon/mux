﻿/********************************************************************
	created:	2010/05/11
	created:	11:5:2010   10:39
	file:		EmbedDiamond.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef EMBED_DIAMOND_H
#define EMBED_DIAMOND_H

class CPlayer;
#include "../../Base/PkgProc/SrvProtocol.h"
#include <vector>

struct EmbedDiamondItemCSInfo
{
	unsigned int ownerItemUID;  
	unsigned int embidDiamondNum;
	EmbedDiamondInfo embidDiamondsArr[3];

	EmbedDiamondItemCSInfo():ownerItemUID(0),embidDiamondNum(0){};

	EmbedDiamondItemCSInfo& operator=( const EmbedDiamondItemCSInfo& embedDiamondItem )
	{
		if ( this == &embedDiamondItem )
			return *this;

		ownerItemUID    = embedDiamondItem.ownerItemUID;
		embidDiamondNum = embedDiamondItem.embidDiamondNum;
		embidDiamondsArr[0] = embedDiamondItem.embidDiamondsArr[0];
		embidDiamondsArr[1] = embedDiamondItem.embidDiamondsArr[1];
		embidDiamondsArr[2] = embedDiamondItem.embidDiamondsArr[2];

		return *this;
	}

	EmbedDiamondItemCSInfo(  const EmbedDiamondItemCSInfo& embedDiamondItem )
	{
		if ( this == &embedDiamondItem )
			return;

		ownerItemUID    = embedDiamondItem.ownerItemUID;
		embidDiamondNum = embedDiamondItem.embidDiamondNum;
		embidDiamondsArr[0] = embedDiamondItem.embidDiamondsArr[0];
		embidDiamondsArr[1] = embedDiamondItem.embidDiamondsArr[1];
		embidDiamondsArr[2] = embedDiamondItem.embidDiamondsArr[2];
	}

	void AffectPlayer( CPlayer* pPlayer );

	void UnAffectPlayer( CPlayer* pPlayer );
};

class EmbedDiamondManager
{
public:
	EmbedDiamondManager():m_pAttachPlayer(NULL)
	{
	}

	void RecvPlayerEmbedDiamonds( EmbedDiamondInfo* diamondsArr,  int arrlen );

	void NoticeEmbedDiamondsToPlayer();

	void UpdateEmbedDiamondToDB();

	void AttachPlayer( CPlayer* pPlayer )
	{
		m_pAttachPlayer = pPlayer;
	}

public:
	//当玩家丢弃道具时
	void OnPlayerDropItem( unsigned int itemUID );

	//当玩家装备道具时
	void OnPlayerEquipItem( unsigned int itemUID );

	//当玩家脱下装备时
	void OnPlayerUnEquipItem( unsigned int itemUID );

private:
	EmbedDiamondItemCSInfo* GetEmbedDiamondItemByItemUID( unsigned int itemuid  )
	{
		if ( embedDiamondItemVec.size() == 0 )
			return NULL;

		for ( unsigned int i = 0; i< embedDiamondItemVec.size(); i++ )
		{
			if( embedDiamondItemVec[i].ownerItemUID == itemuid )
			{
				return &embedDiamondItemVec[i];
			}
		}
		return NULL;
	}

private:
	std::vector<EmbedDiamondItemCSInfo> embedDiamondItemVec;  
	CPlayer* m_pAttachPlayer;

};


#endif