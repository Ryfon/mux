﻿#include "BattleRule.h"
#include "Player/Player.h"
#include "monster/monster.h"
#include "MuxMap/muxmapbase.h"
#include "MuxSkillConfig.h"

CBattleRule::CBattleRule(void)
{
}

CBattleRule::~CBattleRule(void)
{
}


bool CBattleRule::BattleModeCheckPvc( CPlayer* pAttackPlayer, CMonster* pMonster, bool bGoodSkill )
{
	if( !pAttackPlayer )
		return false;

	if( !pMonster )
		return false;

	if( !bGoodSkill )
	{
		if( pMonster->GetNpcType() == 12 )  //12为商店ＮＰＣ任何战斗模式下都不能进行攻击
			return false;
	}


	BATTLE_MODE battleMode = pAttackPlayer->GetBattleMode();
	int usNpcRaceAttribute = pMonster->GetRaceAttribute();
	if( battleMode == DEFAULT_MODE )
	{
		if( !bGoodSkill )
		{
			if( pAttackPlayer->GetRace() == usNpcRaceAttribute )
			{
				return false;
			}
			return true;
		}else{
			if( pAttackPlayer->GetRace() == usNpcRaceAttribute )
			{
				return true;
			}		
			return false;
		}
	}else if( battleMode == FULL_FIELD_MODE ){
		if( bGoodSkill )						//有利的BUFF不能对怪物使用
		{
			return false;
		}
		return true;	//全域几乎全能打	    	
	}else if( battleMode == GUILD_MODE )
	{
		if( pMonster->GetRaceAttribute() == NPC_RACE_MONSTER )
			return true;

	}

	return true;
}


bool CBattleRule::BattleModeCheckPvp( CPlayer* pAttackPlayer, CPlayer* pTargetPlayer, bool bGoodSkill )
{
	if( !pAttackPlayer )
		return false;

	if( !pTargetPlayer )
		return false;

	//如果使用对象是自己
	if( pAttackPlayer == pTargetPlayer )
	{
		if( bGoodSkill )
			return true;
		else
			return false;
	}

	short attackEvilPoint = pAttackPlayer->GetGoodEvilPoint();
	short targetEvilPoint = pTargetPlayer->GetGoodEvilPoint();
	BATTLE_MODE battleMode = pAttackPlayer->GetBattleMode();

	if( battleMode == DEFAULT_MODE )
	{
		if( pAttackPlayer->GetRace() == pTargetPlayer->GetRace() )
		{
			//目标为魔头状态 或者 流氓状态的时候
			if( targetEvilPoint < 0 || pTargetPlayer->IsRogueState() )
			{
				//流氓状态 和 魔头可以对其施放任何技能
				if( pAttackPlayer->IsRogueState() || attackEvilPoint < 0)
					return true;
				
				//其他状态的人,不能对魔头 和 流氓玩家释放有利技能
				if( bGoodSkill )
				{
					return false;
				}
				return true;
			}

			//目标为普通英雄
			if( targetEvilPoint >= 0 )
			{
				//流氓 魔头在常规模式中不能对同种族的任何玩家使用技能
				if( pAttackPlayer->IsRogueState() || attackEvilPoint < 0)
				{
					return false;
				}

				if( bGoodSkill )
				{
					return true;
				}

				//攻击技能不能用
				return false;
			}
		}else
		{
			if( bGoodSkill )
			{
				return false;
			}else
			{
				return true;
			}
		}
	}else if( battleMode == FULL_FIELD_MODE ){
		if( bGoodSkill )
			return false;	
	
		return true;
	}else if( battleMode == GUILD_MODE )
	{
		CUnion* pAttackUnion = pAttackPlayer->Union();
		CUnion* pTargetUnion = pTargetPlayer->Union();
		//首先判断是否异种族
		if( pAttackPlayer->GetRace() != pTargetPlayer->GetRace() )
		{
			if( bGoodSkill )
				return false;
			return true;
		}

		//同种族情况下。。。
		if( NULL == pAttackUnion )			//如果攻击者无工会，类似全域模式
		{
			D_WARNING("警告,%s无工会却在使用战盟模式\n", pAttackPlayer->GetNickName() );
			if( bGoodSkill )
				return false;	
	
			return true;
		}

		if( pAttackUnion == pTargetUnion )			//如果是同一工会的，同一工会的人必定是同一种族的人
		{
			if( !bGoodSkill )
				return false;
			return true;
		}

		if( bGoodSkill )
		{
			//释放者为普通和英雄状态可以对同种族的普通和英雄释放
			if( attackEvilPoint < 0 || pAttackPlayer->IsRogueState() )
			{
				if( targetEvilPoint < 0 || pTargetPlayer->IsRogueState() )
					return true;
				return false;
			}else
			{
				if( targetEvilPoint < 0 || pTargetPlayer->IsRogueState() )
					return false;
				return true;
			}
		}else
		{
			return true;	//非同工会的都能打
		}
	}

	return true;
}


bool CBattleRule::RookieProtect( CPlayer* pAttackPlayer,  CPlayer* pTargetPlayer, bool bGoodSkill, bool bInScopeAttack )
{
	//注，此判断必须要在BattleModeCheckPvp以后判断

	if( NULL == pAttackPlayer || NULL == pTargetPlayer )
		return false;

	
	bool bDifferentRace = (pAttackPlayer->GetRace() != pTargetPlayer->GetRace()); 
	if( bGoodSkill )
	{
		return !bDifferentRace; 
	}

	CMuxMap* pMap = pAttackPlayer->GetCurMap();
	if( NULL == pMap )
		return false;

	//看本地图是否开启了城战开启时没有任何新手保护机制

	unsigned short tgtLevel = pTargetPlayer->GetLevel();
	//流氓状态就是新手保护失效时间
	if( pTargetPlayer->IsRookieBreak() || pTargetPlayer->IsRogueState() )
		return true;

	if( tgtLevel <= 30 )
	{
		if( !bInScopeAttack )
		{
			pAttackPlayer->SendSystemChat("严禁欺负弱小否则必受天谴！");
		}
		return false;
	}
		
	return true;
}


bool CBattleRule::PvcTargetCheck( CPlayer* pAttackPlayer,  CMonster* pMonster, bool bGoodSkill, bool isScopeAttack )
{
	if( NULL == pAttackPlayer || NULL == pMonster )
		return false;

	if ( pMonster->IsMonsterDie()/*因为通知脚本时ToDel标记可能还未置但死亡标记已置，为了避免嵌套调用*/ || pMonster->IsToDel() )
	{
		///怪物待删；
		return false;
	}
	
	if( pMonster->GetNpcType() == 17 )
	{
		//采集系
		return false;
	}

	if( pMonster->GetHp() == 0 )
	{
		//死亡怪物
		return false;
	}

	if( !pMonster->IsCanBeAttacked() )
	{
		//不能攻击的NPC
		return false;
	}

	if( !CBattleRule::BattleModeCheckPvc( pAttackPlayer, pMonster, bGoodSkill ) )
	{
		//战斗模式规则不符合
		return false;
	}

	//NPC立场
	bool bNpcStand = false;
	if( bGoodSkill )
	{
		bNpcStand = CNpcStandRule::OnUseAssistSkillToNpc( pAttackPlayer, pMonster );
	}else{
		bNpcStand = CNpcStandRule::OnAttackNpc( pAttackPlayer, pMonster );
	}
	if( !bNpcStand )
	{
		return false;
	}

	return true;
}


bool CBattleRule::PvpTargetCheck( CPlayer* pAttackPlayer,  CPlayer* pTargetPlayer, bool isGoodSkill, bool isScopeAttack )
{
	if( NULL == pAttackPlayer || NULL == pTargetPlayer )
		return false;

	bool bGoodSkill = isGoodSkill;
	if ( !bGoodSkill )
	{
		//玩家如果处于非战斗区域,不能进行攻击
		if ( !pAttackPlayer->GetSelfStateManager().IsCanFight() )
		{
			D_DEBUG("玩家%s处于非战斗区域,不能攻击其他玩家\n",pAttackPlayer->GetNickName());
			pAttackPlayer->SendSystemChat("您处于非战斗区域,不能攻击其他玩家");
			return false;
		}

		if ( !pTargetPlayer->GetSelfStateManager().IsCanFight() )
		{
			D_DEBUG("被攻击玩家处于非战斗区域,不能被攻击\n");
			pAttackPlayer->SendSystemChat("被攻击玩家处于非战斗区域,不能被攻击");
			return false;
		}

		//玩家处于非PK区域,不能攻击同种族玩家
		if ( pAttackPlayer->GetRace() == pTargetPlayer->GetRace() )
		{
			if (  !pAttackPlayer->GetSelfStateManager().IsCanPvP()  )
			{
				D_DEBUG("玩家%s处于非PK区域,不能攻击同种族玩家\n",pAttackPlayer->GetNickName());
				pAttackPlayer->SendSystemChat("您处于非PK区域,不能攻击同种族玩家");
				return false;
			}

			if ( !pTargetPlayer->GetSelfStateManager().IsCanPvP() )
			{
				D_DEBUG("玩家%s处于非PK区域,不能被同种族的玩家攻击\n",pTargetPlayer->GetNickName());
				pAttackPlayer->SendSystemChat("被攻击玩家处于非PK区域,不能被同种族的玩家攻击");
				return false;
			}
		}
	}

	if( !BattleModeCheckPvp( pAttackPlayer, pTargetPlayer, bGoodSkill ) )
	{
		D_WARNING("战斗模式下不能攻击该目标\n");
		pAttackPlayer->SendSystemChat("此战斗模式下不允许使用技能");
		return false;
	}

	if( !RookieProtect( pAttackPlayer, pTargetPlayer, bGoodSkill )  )
	{
		D_WARNING("战斗规则修正不符合\n");
		return false;
	}

	/*if( pSkill->GetType() == 2 )
	{
		if( pSkill->GetTargetType() != 2 )
		{
			CBuffProp* pBuffProp = pSkill->GetBuffProp();
			if( NULL == pBuffProp )
			{
				return false;
			}

			unsigned nBuffIndex = pBuffProp->GetBuffIndex();
			unsigned int outBuffIndex = 0;
			if( !pTargetPlayer->IsCanAddBuff( pBuffProp, nBuffIndex, pAttackPlayer->GetFullID(), outBuffIndex ) )
			{
				pAttackPlayer->SendSystemChat("目标身上已有一个更强大的法术");
				return false;
			}
		}
	}*/

	return true;
}

bool CBattleRule::HandlePlayerSkill( CPlayer* pAttackPlayer, GMUseSkill* pUseSkill )
{
	if( NULL == pAttackPlayer || NULL == pUseSkill )
		return false;

	bool isPvp = ( pUseSkill->useSkill.targetID.wGID != MONSTER_GID );
	return true;
}


bool CBattleRule::PlayerStateCheck( CPlayer* pAttackPlayer )
{
	if( NULL == pAttackPlayer )
		return false;

	if( !pAttackPlayer->IsAlive() )
	{
		D_WARNING("CBattleRule::PlayerStateCheck,非活着的玩家不能攻击怪物\n");
		pAttackPlayer->SendSystemChat("死亡状态时您不能使用技能");
		return false;
	}

	if( pAttackPlayer->IsFaint() )
	{
		D_WARNING("CBattleRule::PlayerStateCheck,晕眩的时候不能攻击\n");
		pAttackPlayer->SendSystemChat("晕眩状态时玩家不能攻击怪物");
		return false;
	}

	if ( pAttackPlayer->IsWaitToDel() )
	{
		D_ERROR( "CBattleRule::PlayerStateCheck 玩家处于待删状态，忽略除leave之外的其它任何消息\n" );
		return false;
	}

	if( pAttackPlayer->GetSelfStateManager().GetRideState().IsActive() )
	{
		D_WARNING("CBattleRule::PlayerStateCheck,骑乘玩家不能攻击怪物\n");
		pAttackPlayer->SendSystemChat("骑乘玩家不能攻击怪物");
		return false;
	}

	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	if( curTime <= pAttackPlayer->GetNextActionTime() )
	{
		D_WARNING("CMuxMap::CheckPvc,CT时间内不允许使用其他技能\n");
		pAttackPlayer->SendSystemChat("CT时间中您不能使用技能");
		return false;
	}

	return true;
}


bool CBattleRule::DistanceCheck( CPlayer* pAttackPlayer, TYPE_POS targetPosX, TYPE_POS targetPosY, unsigned int minRange, unsigned int maxRange, unsigned int targetSize )
{
	if( NULL == pAttackPlayer )
		return false;

	TYPE_POS atkPosX = 0, atkPosY = 0;
	pAttackPlayer->GetPos( atkPosX, atkPosY );

	unsigned int maxDistance = maxRange;
	maxDistance += pAttackPlayer->GetBuffAtkDistance();
	maxDistance += targetSize;
	
	if( !IsInVagueAttDistance(atkPosX, atkPosY, targetPosX, targetPosY, minRange, maxDistance, true ) )
	{
		D_WARNING( "DistanceCheck，玩家%s使用技能打怪失败\n", pAttackPlayer->GetAccount() );
		return false ;
	}

	return true;
}


bool CBattleRule::NpcCheckNpc( CMonster* pAtkMonster, CMonster* pTargetMonster )
{
	if( NULL == pAtkMonster || NULL == pTargetMonster )
		return false;

	bool res = false;
	unsigned short atkRace_ = pAtkMonster->GetRaceAttribute();
	unsigned short tgtRace  = pTargetMonster->GetRaceAttribute();

	switch(atkRace_)
	{
	case NPC_RACE_NEUTRAL:
		break;
	case NPC_RACE_HUMAN:
	case NPC_RACE_ERKA:
	case NPC_RACE_ELF:
		{
			bool beAttacked = pTargetMonster->IsCanBeAttacked();
			if (tgtRace == NPC_RACE_HUMAN || tgtRace == NPC_RACE_ERKA || tgtRace == NPC_RACE_ELF)
			{
				if (atkRace_ != tgtRace && beAttacked)
					res = true;
			}
			else if (tgtRace == NPC_RACE_MONSTER)
			{
				if (beAttacked)
					res = true;
			}
		}
		break;
	case NPC_RACE_MONSTER:
		{
			if (tgtRace == NPC_RACE_HUMAN || tgtRace == NPC_RACE_ERKA || tgtRace == NPC_RACE_ELF)
				res = pTargetMonster->IsCanBeAttacked();
		}
		break;
		
	};

	return res;
}


bool CBattleRule::NpcCheckPlayer( CMonster* pAtkMonster, CPlayer* pTargetPlayer )
{
	if( NULL == pAtkMonster || NULL == pTargetPlayer )
		return false;


	bool res = false;
	unsigned short atkRace_ = pAtkMonster->GetRaceAttribute();
	unsigned short tgtRace  = pTargetPlayer->GetRace();

	switch(atkRace_)
	{
	case NPC_RACE_NEUTRAL:
		{
			res = pTargetPlayer->IsEvil();
			if (!res)
				res = pTargetPlayer->IsRogueState();
		}
		break;
	case NPC_RACE_HUMAN:
	case NPC_RACE_ERKA:
	case NPC_RACE_ELF:
		{
			if (atkRace_ != tgtRace)
			{
				res = true;
			}
			else
			{
				res = pTargetPlayer->IsEvil();
				if (!res)
					res = pTargetPlayer->IsRogueState();
					
				if (res)
					res = ( pAtkMonster->GetInherentStand() == 1) ? true : false;
			}
		}
		break;
	case NPC_RACE_MONSTER:
		{
			res = true;
		}
		break;
		
	};

	return res;
}


bool CBattleRule::NewSkillCheck( CPlayer* pUseSkillPlayer, CMuxPlayerSkill* pSkill )
{
	if( NULL == pUseSkillPlayer || NULL == pSkill )
		return false;

	if( !pSkill->IsNormalAttack() )
	{
		if( !pUseSkillPlayer->IsEquipWeapon() )
		{
			D_ERROR("CBattleRule::PSkillCheck 在没有装备武器的情况下使用非普通攻击\n");
			pUseSkillPlayer->SendSystemChat("没有武器您不能使用技能");
			return false;
		}
	}
	
	//攻击技能非战斗区域不能使用
	if( !pSkill->IsGoodSkill() )
	{
		if ( !pUseSkillPlayer->GetSelfStateManager().IsCanFight() )
		{
			D_DEBUG("CBattleRule::PSkillCheck 玩家%s处于非战斗区域 不能攻击怪物\n", pUseSkillPlayer->GetNickName() );
			pUseSkillPlayer->SendSystemChat("非战斗区域中不能进行攻击");
			return false;
		}
	}

	//玩家处于无敌状态不能使用有害技能
	if( pUseSkillPlayer->IsInvicibleState() )
	{
		if( !pSkill->IsGoodSkill() )
		{
			pUseSkillPlayer->SendSystemChat("无敌状态下不能使用有害技能");
			return false;
		}
	}

	//step 4 判断是否在COOLDOWN中
	ACE_Time_Value cdTime( 0, pSkill->GetCoolDown() * 1000 );
	if( !pUseSkillPlayer->CheckCd( pSkill->GetPublicCdType(), cdTime ) ) 
	{
		D_WARNING("CBattleRule::PSkillCheck PSkillCheck 玩家CD时间\n");
		pUseSkillPlayer->SendSystemChat("该技能还在cooldown中");
		return false;
	}

	//还需判断是否技能需要消耗的东西足够
	if( pSkill->GetUllageMp() > (int)pUseSkillPlayer->GetCurrentMP() )
	{
		D_WARNING("CBattleRule::PSkillCheck, 消耗的MP不够\n");
		pUseSkillPlayer->SendSystemChat("您没有足够的MP");
		return false;
	}

	//消耗的SP不足
	if( pSkill->IsSpSkill() )
	{
		if( pSkill->GetUllageSp() * CSpSkillManager::GetPowerPerBean() > pUseSkillPlayer->GetSpPower() )
		{
			D_WARNING("CBattleRule::PSkillCheck,消耗的SP不够\n");
			pUseSkillPlayer->SendSystemChat("您没有足够的SP能量");
			return false;
		}
	}

	return true;
}
