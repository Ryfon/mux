﻿#include "ItemSkill.h"
#include "IBuff.h"


map<int, CItemSkill*> CItemSkillManager::m_mapItemSkill;

CItemSkill::CItemSkill(void)
{
	m_nID = 0;					  	
	m_skillType = ITEM_INVALIDE;   //道具技能的类型
	m_damageMethod = 0;			//0 定植 1按百分比
	m_nNpcType = 0;		//攻击Npc判定用的类型
	m_nNpcRank = 0;		//攻击npc判定用的rank
	m_iMaxTargetLevel = 0;	//对npc使用时可适用的最高等级
	m_iMinTargetLevel = 0;	//对npc使用时可适用的最低等级
	m_iMaxVal = 0;			//伤害 或者 恢复的最大数值
	m_iMinVal = 0;			//伤害 或者 恢复的最小数值
	m_iMaxAttDist = 0;		//使用伤害技能 最大的
	m_iMinAttDist = 0;		//使用伤害技能 最小的
	m_nTarget = 0;			//技能的作用对象
	m_nShape = 0;			//范围伤害技能的形状
	m_nRange = 0;			//范围伤害的范围
	m_nPeriodTime = 0;		//BUFF持续时间
	m_nLinkID = 0;			//LINK的
	m_nLinkRate = 0;		//LINK的概率
	m_nAssistObject = 0;	//BUFF类道具的作用对象
	m_nCurseTime = 0;			//item技能的动作施放时间
	m_nPublicCdType = 0;		//公共CD的类型
	m_cdTime = 0;				//此类型技能的cd时间
	m_nHealType = 0;			//回复技能回复的类型
	m_buffProp = NULL;		//LINK的BUFF属性
}

CItemSkill::~CItemSkill(void)
{
}


bool CItemSkillManager::LoadFromXML(const char* szFileName)
{
	TRY_BEGIN;
	if ( NULL == szFileName )
	{
		D_ERROR( "CSkillExp::LoadFromSkillLevelXML，input file name NULL\n" );
		return false;
	}

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(szFileName))
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if ( elementSet.empty() )
	{
		return false;
	}
	CElement* tmpEle = NULL;

	

	unsigned int rootLevel =  pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		CItemSkill* pItemSkill = NEW CItemSkill;	//技能等级经验
		tmpEle = *tmpiter;
		int curpos = (int) (tmpiter-elementSet.begin());
		if ( tmpEle->Level() == rootLevel+1 )
		{
			if ( !IsXMLValValid( tmpEle, "nID", curpos ) )
			{
				return false;
			}		
			pItemSkill->m_nID = atoi( tmpEle->GetAttr("nID") );

			if ( !IsXMLValValid( tmpEle, "SkillType", curpos ) )
			{
				return false;
			}		
			pItemSkill->m_skillType = (EItemSkillType)atoi( tmpEle->GetAttr("SkillType") );
		

			if ( !IsXMLValValid( tmpEle, "DamageMethod", curpos ) )
			{
				return false;
			}		
			pItemSkill->m_damageMethod = atoi( tmpEle->GetAttr("DamageMethod") )	;
			

			if ( !IsXMLValValid( tmpEle, "NpcType", curpos ) )
			{
				return false;
			}
			pItemSkill->m_nNpcType = atoi( tmpEle->GetAttr("NpcType") );

			//if ( !IsXMLValValid( tmpEle, "NpcRank", curpos ) )
			//{
			//	return false;
			//}	

			pItemSkill->m_nNpcRank = 0/*atoi( tmpEle->GetAttr("NpcRank") )*/;

			if ( !IsXMLValValid( tmpEle, "MaxTargetLevel", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_iMaxTargetLevel = atoi( tmpEle->GetAttr("MaxTargetLevel") );

			if ( !IsXMLValValid( tmpEle, "MinTargetLevel", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_iMinTargetLevel = atoi( tmpEle->GetAttr("MinTargetLevel") );

			if ( !IsXMLValValid( tmpEle, "MinTargetLevel", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_iMinTargetLevel = atoi( tmpEle->GetAttr("MinTargetLevel") );

			if ( !IsXMLValValid( tmpEle, "HealType", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nHealType = atoi( tmpEle->GetAttr("HealType") );

			if ( !IsXMLValValid( tmpEle, "MaxHeal", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_iMaxVal = atoi( tmpEle->GetAttr("MaxHeal") );

			if ( !IsXMLValValid( tmpEle, "MinHeal", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_iMinVal = atoi( tmpEle->GetAttr("MinHeal") );

			if ( !IsXMLValValid( tmpEle, "nAttDistMin", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_iMinAttDist = atoi( tmpEle->GetAttr("nAttDistMin") );

			if ( !IsXMLValValid( tmpEle, "nAttDistMax", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_iMaxAttDist = atoi( tmpEle->GetAttr("nAttDistMax") );

			if ( !IsXMLValValid( tmpEle, "nShape", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nShape = atoi( tmpEle->GetAttr("nShape") );

			if ( !IsXMLValValid( tmpEle, "nRange", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nRange = atoi( tmpEle->GetAttr("nRange") );

			if ( !IsXMLValValid( tmpEle, "nTarget", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nTarget = atoi( tmpEle->GetAttr("nTarget") );

			//if ( !IsXMLValValid( tmpEle, "nRange", curpos ) )
			//{
			//	return false;
			//}	
			//pItemSkill->m_nShape = atoi( tmpEle->GetAttr("nRange") );

			if ( !IsXMLValValid( tmpEle, "nPeriodOfTime", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nPeriodTime = atoi( tmpEle->GetAttr("nPeriodOfTime") );

			if ( !IsXMLValValid( tmpEle, "nLinkID", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nLinkID = atoi( tmpEle->GetAttr("nLinkID") );
			if( pItemSkill->m_nLinkID != 0 )
			{
				CBuffProp* pTmpProp = CManBuffProp::FindObj( pItemSkill->m_nLinkID );
				if( NULL == pTmpProp )
				{
					D_ERROR("ITEMSKILL读取找不到LINK的BUFF属性\n");
					return false;
				}
				pItemSkill->m_buffProp = pTmpProp;
			}


			if ( !IsXMLValValid( tmpEle, "nLinkRate", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nLinkRate = atoi( tmpEle->GetAttr("nLinkRate") );

			if ( !IsXMLValValid( tmpEle, "AssistObject", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nAssistObject = atoi( tmpEle->GetAttr("AssistObject") );

			if ( !IsXMLValValid( tmpEle, "PublicCDType", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nPublicCdType = atoi( tmpEle->GetAttr("PublicCDType") );

			if ( !IsXMLValValid( tmpEle, "nCoolDown", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_cdTime = atoi( tmpEle->GetAttr("nCoolDown") );

			if ( !IsXMLValValid( tmpEle, "nCurseTime", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_nCurseTime = atoi( tmpEle->GetAttr("nCurseTime") );

			if ( !IsXMLValValid( tmpEle, "targetSet", curpos ) )
			{
				return false;
			}	
			pItemSkill->m_targetSet = atoi( tmpEle->GetAttr("targetSet") );
		}
		m_mapItemSkill.insert( make_pair( pItemSkill->m_nID, pItemSkill) ); //加入配置类
	}
	return true;
	TRY_END;
	return false;
}


CItemSkill* CItemSkillManager::FindItemSkill( int skillID )
{
	map<int, CItemSkill*>::iterator iter = m_mapItemSkill.find( skillID );
	if( iter == m_mapItemSkill.end() )
	{
		return NULL;
	}else
	{
		return iter->second;
	}
}


bool CItemSkillManager::IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
{
	if ( NULL == pEle || NULL == attName )
	{
		return false;
	}
	if ( NULL == pEle->GetAttr( attName ) )
	{
		D_ERROR( "玩家技能经验XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
		return false;
	}
	return true;
}


void CItemSkillManager::Release()
{
	CItemSkill* pItemSkill = NULL;
	for( map<int, CItemSkill*>::iterator iter = m_mapItemSkill.begin(); iter != m_mapItemSkill.end(); ++iter )
	{
		pItemSkill = iter->second;
		if ( NULL != pItemSkill )
		{
			delete pItemSkill;
		}
	}
}
