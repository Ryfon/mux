﻿#pragma once
#include <map>
#include "MuxSkillProp.h"
#include "XmlManager.h"
#include "Utility.h"
using namespace std;

//NEW SKILL SYSTEM

template <typename T>
class MuxSkillConfig
{

friend class ACE_Singleton< MuxSkillConfig, ACE_Null_Mutex>;	

public:
	MuxSkillConfig(void)
	{}

	virtual ~MuxSkillConfig(void)
	{
		T* pTemplate = NULL;
		for( typename map<unsigned int, T*>::iterator iter = m_mapMuxSkillPropT.begin(); iter != m_mapMuxSkillPropT.end(); ++iter )
		{
			pTemplate = iter->second;
			if( NULL != pTemplate )
			{
				delete pTemplate; pTemplate = NULL;
			}
		}
		m_mapMuxSkillPropT.clear();
	}

public:
	virtual bool LoadSkillPropT( const char* filename );

	virtual int FindSpSkill( unsigned short spLevel, unsigned int job, vector<CMuxSkillProp*>& vecMuxSkillProp )
	{
		return 0;
	}

	std::map<unsigned int, T*>& SkillProps(void)
	{
		return m_mapMuxSkillPropT;
	}
	
	T* FindTSkillProp( unsigned int id )
	{
		typename map<unsigned int, T*>::iterator iter = m_mapMuxSkillPropT.find( id );
		if( iter != m_mapMuxSkillPropT.end() )
		{
			return iter->second;
		}
		return NULL;
	}

private:
	bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord );
	
private:
	map<unsigned int, T*> m_mapMuxSkillPropT;
};

typedef ACE_Singleton< MuxSkillConfig<MuxPlayerDamageProp>, ACE_Null_Mutex > PlayerDamPropSingleton;
typedef ACE_Singleton< MuxSkillConfig<MuxNpcDamageProp>, ACE_Null_Mutex > NpcDamPropSingleton;
typedef ACE_Singleton< MuxSkillConfig<MuxItemSkillProp>, ACE_Null_Mutex > ItemSkillPropSingleton;
typedef ACE_Singleton< MuxSkillConfig<MuxBuffProp>, ACE_Null_Mutex > BuffPropSingleton;
typedef ACE_Singleton< MuxSkillConfig<MuxSpiltProp>, ACE_Null_Mutex > SpiltPropSingleton;
typedef ACE_Singleton< MuxSkillConfig<CMuxSkillProp>, ACE_Null_Mutex > MuxSkillPropSingleton;

