﻿#ifndef CSORTITEMPKG_H
#define CSORTITEMPKG_H

#include "../../Base/PkgProc/SrvProtocol.h"
#include "Utility.h"
#include <set>

using namespace MUX_PROTO;

class CPlayer;	//声明

class CSortPkgItem
{
public:
	CSortPkgItem() {m_pPlayer = NULL; SetInit();}
	~CSortPkgItem() {}
	void OnBegin(char sortpage);
	void OnEnd();
	void OnRecvItemInfo(const ItemUidPosition* itemInfo, unsigned int size );
	void SetPlayer(CPlayer* pPlayer){ m_pPlayer = pPlayer; }
private:
	void SetInit();//回到初始化
	void OnSortFail();

private:
	CPlayer * m_pPlayer;
	bool m_isActive;//是否处于排序中
	int	 m_itemInfo[PACKAGE_SIZE];	//客户端排好序的背包,值为itemUID
	ItemInfo_i m_itemInfoBak[PACKAGE_SIZE];	//原来的背包
	std::set<int> m_uidSet;//set<itemUID> 原来背包内道具的UID
	std::map<unsigned int,ItemInfo_i*> m_itemInfoMap;//map<itemUID,ItemInfo_i*>
	unsigned int m_begin;
	unsigned int m_end;
};

#endif
