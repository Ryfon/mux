﻿/**
* @file DealPkg.h
* @brief 处理包头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.h(copy from dzj)
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: hyn
* 完成日期: 2007.12.17
*
*/

#pragma once
#include <map>
#include <string>
#include "PkgProc/DealPkgBase.h"
using namespace std;

class CGateSrv;  //针对GATESRV发包过来的处理类的声明
/*特别注意，当一个协议涉及到2个以上玩家时，需区分哪个是发包过来的玩家！！！因为别的玩家可能并不是发包过来的gatesrv
所以先要通过wGID来寻找别的玩家所在的gatesrv，然后再找玩家,不然在多gatesrv下可能会出现错误*/

class CDealG_MPkg : public IDealPkg<CGateSrv>
{
public:
	virtual ~CDealG_MPkg()
	{	};

protected:
	CDealG_MPkg() {};
	
public:
	//初始化，注册处理函数
	static void Init();

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
public:
	//DB信息开始;	//Register( G_M_DBGET_PRE, &OnDbGetPre );
	static bool OnDbGetPre( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//DB信息结束(与G_M_DBGET_PRE一一对应);	//Register( G_M_DBGET_POST, &OnDbGetPost );
	static bool OnDbGetPost( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家离开地图(在G_M_DBGET_POST之后发送);	//Register( G_M_PLAYER_ENTER_MAP, &OnPlayerEnterMap );
	static bool OnPlayerEnterMap( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//DB取到的信息；	//Register( G_M_DBGET_INFO, &OnDbGetInfo );
	static bool OnDbGetInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//#define G_M_NDSMR_INFO                //no db, switch map reserve信息；	Register( G_M_NDSMR_INFO, &OnNDSMRInfo );
	static bool OnNDSMRInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public:

	//玩家跑动的处理
	static bool OnPlayerRun( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家跑动中跨块；
	static bool OnPlayerBeyondBlock( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( G_M_USE_MONEY_TYPE, &OnGMUseMoneyType );//玩家设置所使用的钱币；
	static bool OnGMUseMoneyType( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( G_M_HONOR_MASK, &OnGMHonorMask );//玩家选择显示的称号；	
	static bool OnGMHonorMask( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( G_M_BIAS_INFO, &OnGMBiasInfo );//排行榜阈值相关信息；
	static bool OnGMBiasInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家上线的处理
	//static bool OnPlayerAppear( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家离线的处理
	static bool OnPlayerLeave( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	////收到玩家包裹信息的处理
	//static bool OnPlayerPkg( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家连击消息的处理
	//static bool OnPlayerCombo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家获取玩家信息请求的处理
	static bool OnReqRoleInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家获取怪物信息请求的处理
	static bool OnReqMonsterInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家攻击怪物处理；
	static bool OnPlayerAttackMonster(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );


	//玩家聊天包的处理
	static bool OnPlayerChat(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//gatesrv请求此mapsrv所管理的地图
	static bool OnReqMannedMap(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//地图跳转处理
	//移至gatesrvstatic bool OnSwitchMap(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家与脚本对话
	static bool OnPlayerScriptChat(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///玩家攻击玩家消息
	static bool OnPlayerAttackPlayer(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//玩家的完整信息
	static bool OnFullPlayerInfo(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	///普通地图FullInfo后处理，与CopyMapAfterFullInfoProc对应；
	static bool NormalMapAfterFullInfoProc( CGateSrv* pGateSrv, CPlayer* pPlayer );

	///副本FullInfo后处理，与NormalMapAfterFullInfoProc对应；
	static bool CopyMapAfterFullInfoProc( CGateSrv* pGateSrv, CPlayer* pPlayer );

	//玩家倒计时时间到，或者玩家主动选择至复活点复活;
	static bool OnDefaultRebirth(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	////玩家复活点复活准备完毕，收到后SRV应将玩家加入地图，并向周围人广播;
	//static bool OnRebirthReady(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//当玩家查询道具包信息时 ItemDebug
	static bool OnQueryItemPkgInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//当玩家拾取包中道具时
	static bool OnPickItemFromPkg( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//当玩家拾取包中的所有道具时
	static bool OnPickAllItemFromPkg( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//MapServer开启时，获取最新的ItemID号 2008/05/04
	static bool OnQueryNewItemRange( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//@装备道具
	static bool OnUseItem(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//@brief:装备可装备道具
	static bool OnEquipItem(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//@玩家不使用道具了
	static bool OnUnUseItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//@玩家交换道具
	static bool OnSwapItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//@brief:当玩家丢弃道具时
	static bool OnDropItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//@brief:分离道具
	static bool OnSplitItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//使用上马操作
	static bool OnActiveMountItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//玩家同意了其他玩家的邀请骑乘请求
	static bool OnAgreeBeInviteMount( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//玩家离开了队伍的请求
	static bool OnLeaveMountTeam( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//邀请玩家加入组队队伍
	static bool OnInvitePlayerJoinMountTeam( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//当玩家进入新地图时,设置玩家的骑乘状态
	static bool OnNewMapSetPlayerRideState(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//当玩家队长切换地图时
	static bool OnRideLeaderSwitchMap(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnReqBuffEnd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPlayerUseAssistSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

//////////////////////////////////////////////////////////////////////////
	//当接受到组队的详细信息时
	static bool OnRecvTeamDetialInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//当玩家更新在组队中的信息
	static bool OnPlayerUpdateSelfTeamInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//接到玩家是否同意Roll点
	static bool OnRecvIsAgreeRollItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//切换组队队长
	static bool OnRecvChangeTeamLeader( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );
	//切换组队的道具分享模式
	static bool OnRecvChangeItemShareMode( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//切换组队的经验分享模式
	static bool OnRecvChangeExpreShareMode( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//切换组队的Roll物品等级
	static bool OnRecvChangeRollLevelItemMode( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//组队中踢出玩家
	static bool OnRecvKickPlayerOnTeam( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//组队解散
	static bool OnRecvDisBandTeam( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnTeamMemberGetNewItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( G_M_ITEM_UPGRADE_PLATE, &OnPlayerReqItemUpPlate);//玩家请求装备升级托盘
	static bool OnPlayerReqItemUpPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	//玩家往托盘中加物品
	static bool OnPlayerAddSthToPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	//玩家从托盘中取物品
	static bool OnPlayerTakeSthFromPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	//玩家尝试执行托盘操作
	static bool OnPlayerTryPlateExec( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );	
	//玩家关闭托盘
	static bool OnPlayerClosePlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 发送交易请求 
	static bool OnSendTradeReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	// 请求交易的结果（ 同意或拒绝）
	static bool OnSendTradeReqResult( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	// 增加或删除一个物品到交易栏
	static bool OnAddOrDeleteItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	// 交易金钱
	static bool OnSetTradeMoney( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	// 取消交易
	static bool OnCancelTrade( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	// 锁定交易
	static bool OnLockTrade( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	// 确认交易
	static bool OnConfirmTrade( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//修复耐久度
	static bool OnRepairWearByItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//时间型耐久度为0
	static bool OnTimedWearIsZero( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//添加SP SKILL的请求
	static bool OnAddSpSkillReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//升级战斗技能的请求
	static bool OnUpgradeSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnDeleteTask( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//npc维修耐久度
	static bool OnRepairWearByNpc( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//查询商店物品数量
    static bool OnQueryShop( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//请求购买商店物品
    static bool OnShopBuyReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

    //请求售出包裹物品
	static bool OnItemSoldReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//更改战斗模式
	static bool OnChangeBattleMode( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//请求添加活点, //GMAddActivePtReq
	static bool OnAddAptReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//查询恶魔时间
	static bool OnQueryEvilTime(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//使用伤害型消耗道具
	static bool OnUseDamageConsumeItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//查询维修的道具的花费
	static bool OnQueryRepairItemCost( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//交换装备栏的道具时
	static bool OnSwapEquipItem(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//查询道具包中对应页的道具信息
	static bool OnQueryItemPkgPage(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );


#ifdef OPEN_PUNISHMENT
	//天谴相关
	static bool OnPunishmentStart( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPunishmentEnd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPunishPlayerInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
#endif /*OPEN_PUNISHMENT*/

#ifdef OPEN_PET
	//宠物相关
	static bool OnSummonPet( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnClosePet( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnChangePetName( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnChangePetImage( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnChangePetSkillState( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
#endif //OPEN_PET

	//排行相关
	static bool OnRecvRankChartInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	////当接到移除排行榜玩家时
	//static bool OnRecvPlayerDeleteRole( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//当接到删除排行榜玩家时
	//static bool OnRecvRemoveRankChartPlayer( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

#ifdef WAIT_QUEUE
	//GateSrv发来玩家退出等待队列
	static bool OnRemoveWaitPlayer( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
#endif /*WAIT_QUEUE*/

	// 注册工会成员
	static bool OnRegisterUnionMember( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 通告工会技能
	static bool OnReportUnionSkills( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	// 通告工会等级
	static bool OnReportUnionLevel( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//取消工会成员注册
	static bool OnUnregisterUnionMember( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//发布任务成功
	static bool OnIssueUnionTask( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//通知工会活跃度改变
	static bool OnChanageUnionActive( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//通知工会威望改变
	static bool OnChanageUnionPrestige( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//通知修改徽章
	static bool OnChanageUnionBadge( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//GM工具相关
	static bool OnGmCmdRequest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//攻城相关
	static bool OnNotifyWarTaskState( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//攻城开始 结束
	static bool OnAttackWarBegin( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	static bool OnAttackWarEnd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//使用UseSkill2后废弃，原ID号由GMMouseTipReq使用//动作分割
	//static bool OnUsePartionSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	//Register( G_M_MOUSETIP_REQ, OnGMMouseTipReq );//鼠标停留信息查询(HP|MP)
	static bool OnGMMouseTipReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//判断包裹是否有空闲空间
	static bool OnCheckPkgFreeSpace( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	
	//领取vip道具
	static bool OnFetchVipItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	
	//领取非vip道具
	static bool OnFetchNonVipItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//领取邮件的道具
	static bool OnPickMailItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//领取邮件的金钱
	static bool OnPickMailMoney( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//右键察看玩家属性
	static bool OnReqPlayerDetailInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	
	//获取套装信息
	static bool OnQuerySuitInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//清除套装信息
	static bool OnClearSuitInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//保存套装信息
	static bool OnSaveSuitInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//一键换装
	static bool OnChanageSuit( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//放弃采集道具
	static bool OnDiscardCollectItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//拾取采集道具
	static bool OnPickCollectItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//拾取所有采集道具
	static bool OnPickAllCollectItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//装备态/时装态切换
	static bool OnEquipFashionSwitch( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//停止变形
	static bool OnEndAnamorphic( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//放入道具到保护托盘
	static bool OnPutItemToProtectPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//执行保护托盘
	static bool OnConfirmProtectPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//释放掉道具的特殊保护状态
	static bool OnReleaseItemSpecialProtect(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//取消特殊保护,保护道具进入特殊保护时间
	static bool OnCancelItemUnSpecialProtectWaitTime( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//把道具从保护托盘中拿出来
	static bool OnTakeItemFromProtectPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//获得当前管理地图的关卡地图信息
	static bool OnCityInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//更新排行的玩家信息
	//static bool OnRcvUpdateRankPlayerRankInfo(CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

#ifdef ANTI_ADDICTION
	//获得当前的游戏状态
	static bool OnGameStateNotify( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
#endif

	//通知当前的Relation数量
	static bool OnNotifyRelationNumber( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ); 

	//新技能　使用一个技能
	static bool OnUseSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ); 

	//Register( G_M_USE_SKILL_2, &OnUseSkill2 );  //使用技能2，6.30版本矩形攻击相应调整所有范围攻击
	static bool OnUseSkill2( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen ); 

	//通知工会活跃度或威望n倍
	static bool OnUnionMultiNtf(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//关闭仓库的托盘
	static bool OnCloseStoragePlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//改变仓库的锁定状态
	static bool OnChangeStorageLockState( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//从仓库中取出物品
	static bool OnTakeItemFromStorage( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//从仓库的指定位置移动到包裹的指定位置
	static bool OnTakeItemFromStorageIndexToPkgIndex( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//将东西放入仓库
	static bool OnPutItemToStorage( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//将东西从包裹的指定位置放到仓库的指定位置
	static bool OnPutItemToStorageIndexFromPkgIndex( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//设置仓库的密码
	static bool OnSetNewStoragePassWord( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//重新设置密码
	static bool OnResetStoragePassWord( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//取出物品时的密码检查
	static bool OnTakeItemCheckPassWord( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//改变仓库状态的密码检查
	static bool OnChangeStorageLockStateCheckPsd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//当扩充仓库
	static bool OnExtendStorage( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//排序仓库
	static bool OnSortStorage( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	//接受仓库的道具信息
	static bool OnRecvStorageItemInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//接受仓库的安全信息
	static bool OnRecvStorageSafeInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//接受仓库的禁止信息
	static bool OnRecvStorageForbiddenInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//交换仓库中的道具
	static bool OnSwapStorageItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//排序包裹中的道具
	static bool OnSortPkgItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//开始排序
	static bool OnSortPkgItemBegin( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//结束排序
	static bool OnSortPkgItemEnd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Gm查询
	static bool OnGMQueryReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//一键贱卖
	static bool OnBatchSellItemsReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//回购
	static bool OnRepurchaseItemReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//关闭在线测试界面
	static bool OnCloseOLTestPlate( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//回答问题
	static bool OnAnswerQuestion( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//放弃答题
	static bool OnDropOLTest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//重置排行榜
	//static bool OnRefreshRankChart( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//获取单体玩家队伍ID 
	static bool OnRecvSinglePlayerTeamID( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

#ifdef ITEM_NPC
	//收到查询ITEMNPC消息
	static bool OnItemNpcReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
#endif //ITEM_NPC

	///收到创建新副本指令
	static bool OnNewCopyCmd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///收到删除旧副本指令
	static bool OnDelCopyCmd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///centersrv发起删除副本请求(可能是副本中太久无人)(centerdelquest-->mapsrvdelallplayer-->mapsrvdelquest-->centerdelcmd-->mapsrvdel operation)
	static bool OnDelCopyQuest( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家离开副本组队通知；
	static bool OnLeaveSTeamNoti( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
	///玩家向mapsrv查询全局副本信息(该mapsrv管理的全部伪副本)
	static bool OnQueryGlobeCopyInfos( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//Register( G_M_TIMELIMIT_RES, &OnPlayerTimeLimitRes );//玩家点击倒计时框；
	static bool OnPlayerTimeLimitRes( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//获取回购列表
	static bool OnRepurchaseItemsListReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//选择攻城战复活点
	static bool OnSelectWarRebirthPtOption( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterSetNewNotice( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnRaceMasterTimeInfoChange(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterTimeInfoExpire( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterDeclareWarInfoChange( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnRecvRaceMasterDetialInfo( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRecvNewRaceMasterBorn( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterSelectOpType( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterDeclareWarToOtherRace( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnRaceMasterSetNewPublicMsg( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnLoadRaceMasterInfoRst( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnForbiddenRaceMasterAuthority( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPublicNoticeAdd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnPublicNoticeRemove( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPublicNoticeModify( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnMarqueueNoticeAdd( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnTempNoticeBroadcast( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnParamNoticeBroadcast(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnDiamondToPrestigeNtf(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnGMAccountNtf(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnForwardUnionCmd(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnCreateUnionErrNtf(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerStartNewAuction( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerBidItem( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnPlayerBidItemErrorAddMoney( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnManualUpgradeReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnAllocatePointReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnModEffectiveSkill( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnFriendGainNtf( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnQueryExpOfLevelUpReq( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnReplyCallMember( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnStartCast( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnStopCast( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen );
};
