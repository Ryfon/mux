﻿/********************************************************************
	created:	2010/05/06
	created:	6:5:2010   11:18
	file:		ItemAttribute.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef  ITEM_ATTRIBUTE_H
#define  ITEM_ATTRIBUTE_H

#include <vector>
#include "XmlManager.h"
#include "aceall.h"


struct ItemRandAttribute
{
	int levelrank;
	int gearArmType;
	int AttributeID;
	int Min;
	int Max;

	ItemRandAttribute():levelrank(0),gearArmType(0),AttributeID(0),Min(0),Max(0)
	{}
};

class ItemAttributeManager
{
public:
	void LoadXML( const char* pxmlfilepath )
	{
		CXmlManager xmlManager;
		if(!xmlManager.Load(pxmlfilepath)){
			return;
		} 

		CElement *pRoot = xmlManager.Root();
		if(NULL == pRoot)
			return;

		std::vector<CElement *> childElements;
		xmlManager.FindSameLevelElements(1,childElements);
		for ( std::vector<CElement*>::iterator lter = childElements.begin(); lter != childElements.end(); ++ lter )
		{
			CElement* pElement = *lter;
			if ( pElement )
			{

				const char* pLevel   = pElement->GetAttr("Level");
				const char* pGearArmType = pElement->GetAttr("GearArmType");
				const char* pMin = pElement->GetAttr("Min");
				const char* pMax = pElement->GetAttr("Max");
				const char* pAttribute = pElement->GetAttr("Attribute");


				if ( !pLevel || !pGearArmType || !pMin || !pMax || !pAttribute  )
					continue;

				ItemRandAttribute randattribute;
				randattribute.levelrank   = atoi( pLevel );
				randattribute.gearArmType = atoi( pGearArmType );
				randattribute.AttributeID = atoi( pAttribute );
				randattribute.Max = atoi( pMax );
				randattribute.Min = atoi( pMin );
				mExcellentAttributeVec.push_back( randattribute );
			}
		}
	}

	bool GetExcellentAttribute( unsigned int level , int gearArmtype, int attribute, ItemRandAttribute& randAttribute )
	{
		std::vector<ItemRandAttribute>::iterator iter = mExcellentAttributeVec.begin();
		for ( ;iter != mExcellentAttributeVec.end(); ++iter )
		{
			randAttribute = *iter;
			if ( randAttribute.levelrank == level && randAttribute.gearArmType == gearArmtype && randAttribute.AttributeID == attribute  )
			{
				return true;
			}
		}
		return false;
	}

private:
	std::vector<ItemRandAttribute>  mExcellentAttributeVec;
};


class ExcellentRandAttributeManager:public ItemAttributeManager
{
	friend class ACE_Singleton<ExcellentRandAttributeManager, ACE_Null_Mutex>;
};
typedef ACE_Singleton<ExcellentRandAttributeManager, ACE_Null_Mutex> ExcellentRandAttributeManagerSingle;


class LuckyRandAttributeManager:public ItemAttributeManager
{
	friend class ACE_Singleton<LuckyRandAttributeManager, ACE_Null_Mutex>;
};
typedef ACE_Singleton<LuckyRandAttributeManager, ACE_Null_Mutex>    LuckyRandAttributeManagerSingle;



#endif