﻿#include "TaskIndexManager.h"
#include "XmlManager.h"
#include "../../Base/Utility.h"
#include "Player/Player.h"


void TaskIndexInfoManager::LoadAllTaskXML()
{
	for ( unsigned short isex = 1; isex < 3; isex++ )
	{
		for ( unsigned short irace = 1; irace< 4; irace++ )
		{
			for ( unsigned short iclass = 1; iclass < 7; iclass++ )
			{
				LoadXML( isex, irace, iclass );
			}
		}
	}
}

void TaskIndexInfoManager::LoadXML( unsigned int nsex ,unsigned int nrace, unsigned int nclass )
{

	char szxmlFileFormat[] = {"config/task/%d_%d_%d.xml"};
	char pszxmlFile[128] = { 0 };
	ACE_OS::snprintf(pszxmlFile,
		128,
		szxmlFileFormat,
		nsex,
		nrace,
		nclass
		);
	if ( !pszxmlFile )
		return;

	D_DEBUG("记载任务索引LoadXML:%s\n", pszxmlFile);

	TRY_BEGIN;

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszxmlFile))
	{
		D_ERROR( "玩家初始化文件XML加载出错,找不到文件%s\n", pszxmlFile );
		return;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("%s配置文件可能为空\n",pszxmlFile);
		return;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindSameLevelElements(1,childElements);

	TaskIndexInfo* pTaskInfo = NEW TaskIndexInfo;
	pTaskInfo->sex = nsex;
	pTaskInfo->nrace = nrace;
	pTaskInfo->nclass = nclass;

	if ( pTaskInfo )
	{
		for ( std::vector<CElement *>::iterator lter = childElements.begin();
			lter!= childElements.end();
			++lter )
		{
			PushTaskIndexEle( *lter, pTaskInfo );
		}

		mTaskIndexInfoVec.push_back( pTaskInfo );
	}

	
	TRY_END;
}

void TaskIndexInfoManager::PushTaskIndexEle(CElement* pxmlElement, TaskIndexInfo* pTaskIndexInfo )
{
	if ( pxmlElement && pTaskIndexInfo )
	{
		
		unsigned int taskID = ACE_OS::atoi( pxmlElement->GetAttr("TaskID") );
		unsigned int index  = ACE_OS::atoi( pxmlElement->GetAttr("Index") );

		pTaskIndexInfo->mTaskIndexMap.insert( std::pair<unsigned int, unsigned int>(taskID, index) );
	}
}

int TaskIndexInfoManager::FindTaskIndex(unsigned int sex,  unsigned int nrace , unsigned int nclass, unsigned int taskID )
{
	if ( mTaskIndexInfoVec.empty() )
		return -1;

	std::vector<TaskIndexInfo *>::iterator lter = mTaskIndexInfoVec.begin();
	for ( ; lter != mTaskIndexInfoVec.end(); ++lter )
	{
		TaskIndexInfo* pTaskInfo = *lter;
		if ( pTaskInfo )
		{
			if ( pTaskInfo->sex == sex 
				&& pTaskInfo->nrace == nrace 
				&& pTaskInfo->nclass == nclass )
			{
				std::map<unsigned int,unsigned int>::iterator findIter = pTaskInfo->mTaskIndexMap.find( taskID );
				if ( findIter != pTaskInfo->mTaskIndexMap.end() )
				{
					return findIter->second;
				}
			}
		}
	}

	return -1;
}


void NewTaskRecordDelegate::RecvPlayerTaskRecordArr(unsigned int *taskRdArr, unsigned int pageIndex)
{
	if ( taskRdArr == NULL )
		return;

	if ( pageIndex>= 1 )
	{
		D_ERROR("接受任务存盘错误 NewTaskRecordDelegate::RecvPlayerTaskRecordArr pageIndex = %d >=1 \n", pageIndex );
		return;
	}

	if ( pageIndex == 0 )
	{
		StructMemSet( m_TaskRdArr, 0x0, sizeof(m_TaskRdArr) );
	}

	StructMemCpy( m_TaskRdArr, taskRdArr, sizeof(m_TaskRdArr) );

	if ( m_pAttachPlayer )
	{
		//通知client
		MGNotifyNewTaskRecord newTaskRecord;
		newTaskRecord.lNotiPlayerID = m_pAttachPlayer->GetFullID();
		newTaskRecord.newTaskRecord.taskArrSize = 100;
		StructMemCpy( newTaskRecord.newTaskRecord.taskArr, taskRdArr, sizeof( newTaskRecord.newTaskRecord.taskArr ));
		m_pAttachPlayer->SendPkg<MGNotifyNewTaskRecord>( &newTaskRecord );
	}
}

unsigned int NewTaskRecordDelegate::GetTaskRdState(unsigned int taskMask)
{
	unsigned int taskSegment = (taskMask - 1) / 32 ;
	if (taskSegment >= ARRAY_SIZE(m_TaskRdArr))
	{
		D_ERROR("NewTaskRecordDelegate::GetTaskRdState,taskSegment(%d) >= ARRAY_SIZE(m_TaskRdArr)\n",taskSegment);
		return 0;
	}
	unsigned int taskRdInfo = m_TaskRdArr[taskSegment];
	unsigned int taskIndex = (taskMask -1) % 32;

	if (  taskRdInfo &  ( 1<<taskIndex ) )
		return 1;

	return 0;
}

void NewTaskRecordDelegate::SetTaskRdState(unsigned int taskMask ,unsigned int taskState )
{
	unsigned int  taskSegment = (taskMask - 1) / 32;
	if (taskSegment >= ARRAY_SIZE(m_TaskRdArr))
	{
		D_ERROR("NewTaskRecordDelegate::SetTaskRdState,taskSegment(%d) >= ARRAY_SIZE(m_TaskRdArr)\n",taskSegment);
		return;
	}
	unsigned int& taskRdInfo  = m_TaskRdArr[taskSegment];
	unsigned int  taskIndex   = (taskMask -1) % 32;

	if ( taskState == 1 )
		taskRdInfo |=   1<<taskIndex; 
	else
		taskRdInfo &= ~(1<<taskIndex);

	if ( m_pAttachPlayer )
	{
		MGUpdateBackTaskInfo srvmsg;
		srvmsg.lNotiPlayerID = m_pAttachPlayer->GetFullID();
		srvmsg.taskUpdate.taskIndex     = taskSegment;
		srvmsg.taskUpdate.taskInfo      = taskRdInfo;
		m_pAttachPlayer->SendPkg<MGUpdateBackTaskInfo>( &srvmsg );
	}

}


bool NewTaskRecordDelegate::IsHaveDoneTask( char sex, unsigned int race, unsigned int nclass , unsigned int taskID )
{
	if ( !m_pAttachPlayer )
		return false;

	int taskMask = TaskIndexInfoManagerSingle::instance()->FindTaskIndex( sex, race, nclass , taskID );
	if ( taskMask != -1 )
	{
		unsigned int taskStat = GetTaskRdState( taskMask );
		if( taskStat  == 1 )
		{
			D_DEBUG("玩家%s查询任务状态，已完成任务 %d ,索引位:%d\n ", m_pAttachPlayer->GetNickName(), taskID,taskMask );
			return true;
		}
		else
		{
			D_DEBUG("玩家%s查询任务状态，未完成任务 %d ,索引位:%d\n ", m_pAttachPlayer->GetNickName(), taskID,taskMask );
		}
	}
	
	return false;
}


void NewTaskRecordDelegate::RecordDoneTask( char sex, unsigned int race, unsigned int nclass , unsigned int taskID )
{
	int taskMask = TaskIndexInfoManagerSingle::instance()->FindTaskIndex( sex, race, nclass , taskID );
	if ( taskMask != -1 )
	{
		SetTaskRdState( taskMask, 1 ); 
		D_DEBUG("记录任务状态，记录任务 %d 完成,索引位:%d\n", taskID, taskMask );

		UpdateTaskRdInfoToDB();
	}
}

void NewTaskRecordDelegate::UpdateTaskRdInfoToDB()
{
	if ( !m_pAttachPlayer )
		return;

	MGDbSaveInfo saveinfo;
	saveinfo.infoType = DI_TASKRD;
	saveinfo.playerID = m_pAttachPlayer->GetFullID();
	for ( unsigned int i = 0 ; i < 1; i++ )
	{
		unsigned* taskRdIndex = m_TaskRdArr + i * TASK_RD_PAGE_SIZE; //偏移

		StructMemCpy( saveinfo.taskRdInfo.mTaskRdIArr, taskRdIndex, sizeof(saveinfo.taskRdInfo.mTaskRdIArr) );
		saveinfo.taskRdInfo.mCount = i;
		saveinfo.taskRdInfo.isEnd  =  ( i == 0 );

		m_pAttachPlayer->ForceGateSend<MGDbSaveInfo>( &saveinfo );
	}
}


