﻿#ifndef LOG_MANAGER_H
#define LOG_MANAGER_H

#include <time.h>
#include "../../Base/PkgProc/SrvProtocol.h"


class CPlayer;
class CMonster;


//记录日至条件:
//1.没有条件(缺省:全部记录)
//2.指定物品(在物品属性里面扩展一个日至属性,用来标记是否记日至)
//3.指定玩家(在玩家身上关联日至选项,所有选项在CLogManager中枚举)
//4.指定怪物(根据策划意见,除部分类型的怪物不记外,所有的都要记录)
//5.指定字段超过临界值(作为类CLogManager的数据成员成员)

//日至策略应用:
//1.针对日至文档,需要日至条件判断的,在函数前面进行适当判断.
//2.没有条件的,全部记录


class CLogManager
{
	enum eBindPlayerOption//针对记录日至条件3
	{
		
	};

public:
	//玩家升级
	static void DoPlayerLevelUp(CPlayer *pPlayer, unsigned short oldLevel);
	
	//技能升级
	static void DoSkillLevelUp(CPlayer *pPlayer, unsigned char skillType, unsigned int skillID, unsigned int oldLevel, unsigned int newLevel);

	//获取物品
	static void DoGetItem(CPlayer *pPlayer, unsigned char ownType, ItemInfo_i &item);

	//使用物品
	static void DoUseItem(CPlayer *pPlayer, ItemInfo_i &item);

	//丢弃物品
	static void DoDropItem(CPlayer *pPlayer, ItemInfo_i &item);

	//任务变化
	static void DoTaskChanage(CPlayer *pPlayer, unsigned int taskId, unsigned char state);

	//角色死亡
	static void DoPlayerDead(CPlayer *pPlayer, unsigned char killerType, unsigned int killerID, unsigned int dropExp, unsigned int dropCount, ItemInfo_i items[]);

	//地图切换
	static void DoSwitchMap(CPlayer *pPlayer, unsigned int oldMapID, unsigned int newMapID);

	//交易
	static void DoTradeItem(CPlayer *pPlayer, CPlayer *pPeerPlayer, ItemInfo_i ownerItems[], unsigned int ownItemCount, ItemInfo_i peerItems[], unsigned int peerItemCount, unsigned int ownerMoney = 0, unsigned int peerMoney = 0);

	//sp豆变化
	static void DoSpPowerChange(CPlayer *pPlayer, unsigned char changeType, unsigned char changeNum);
	
	//物品属性变化
	static void DoItemPropertyChange(CPlayer *pPlayer, unsigned char propertyType,  unsigned int itemUId, unsigned itemTypeID, unsigned int oldValue, unsigned newValue);

	//金钱变化
	static void DoMoneyChange(CPlayer *pPlayer, unsigned char changeType, unsigned char reasonType, unsigned char moneyType, unsigned int moneyCount);

	//宠物增加	
	static void DoPetAdd(CPlayer *pPlayer, unsigned int petID);

	//宠物升级
	static void DoPetLevelUp(CPlayer *pPlayer, unsigned int petID, unsigned int oldValue, unsigned int newValue);

	//宠物获取技能
	static void DoPetGainSkill(CPlayer *pPlayer, unsigned int petID, unsigned int skillID);

	//宠物状态变化
	static void DoPetStateChange(CPlayer *pPlayer, unsigned int petID, unsigned char stateType);

	//使用技能
	static void DoUseSkill(CPlayer *pPlayer, unsigned char skillType, unsigned int skillID, bool isSpSkill = false);

	//杀死玩家
	static void DoKillPlayer(CPlayer *pPlayer, CPlayer *pBeKilledPlayer);
	
	//玩家复活
	static void DoPlayerRebirth(CPlayer *pPlayer, unsigned int mapID, unsigned int posX, unsigned int posY);

	//商店购买
	static void DoNpcShopPurchase(CPlayer *pPlayer,  unsigned int shopID, unsigned char purchaseType, unsigned int money, unsigned int itemType, unsigned int itemNum);

	//修复物品
    static void DoRepairItem(CPlayer *pPlayer, unsigned int shopID, unsigned int money, unsigned int itemUid, unsigned int itemTypeID);
	
	//怪物死亡
	static void DoMonsterDead(CPlayer *pPlayer, CMonster *pMonster);

	//副本变化
	static void DoInstanceChange(unsigned int instanceID, unsigned int worldID, unsigned char changeType);

	//仓库打开
	static void DoWarehouseOpen(CPlayer *pPlayer, unsigned int warehouseID, unsigned char hasPasswd);

	//从仓库中存取物品
	static void DoWarehouseOperateItem(CPlayer *pPlayer, unsigned int warehouseID, unsigned char operateType, unsigned int itemUid, unsigned int itemTypeId, unsigned itemCount);

	//从仓库中存取金钱
	static void DoWarehouseOperateMoney(CPlayer *pPlayer, unsigned int warehouseID, unsigned char operateType, unsigned int money);

	//仓库扩展
	static void DoWarehouseExtend(CPlayer *pPlayer, unsigned int warehouseID, unsigned int num);

	//仓库保护
	static void DoWareHousePrivate(CPlayer *pPlayer, unsigned int warehouseID, unsigned char operateType, unsigned char result);

private:
	//物品信息内部转换
	static void CopyItemInfo(LOG_SVC_NS::ItemInfo_i &dst, MUX_PROTO::ItemInfo_i &src);

	//获取当前时间
	static unsigned int Now(void){ return (unsigned int)time(NULL); }

private:
	//物品需要记日至(针对记录日至条件2)
	static bool NeedLogItem(unsigned int itemTypeID){ return true;}

	//玩家需要记日至(针对记录日至条件3)
	static bool NeedLogPlayer(CPlayer *pPlayer, unsigned int option){return  true;}

	//怪物需要记日至(针对记录日至条件4)
	static bool NeedLogMonster(CMonster *pMonster){return true;}

private:
	//临界值字段(针对记录日至条件5)
	static unsigned int lcfMoeny;//金钱
	static unsigned int lcfSpPowerBean;//sp豆

private:
	
};

#endif/*LOG_MANAGER_H*/
