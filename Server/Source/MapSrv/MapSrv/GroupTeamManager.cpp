﻿#include "GroupTeamManager.h"
#include "../../Base/Utility.h"
#include "Player/Player.h"
#include "Item/RollItemManager.h"


GroupTeamManager::~GroupTeamManager()
{
	std::map<unsigned long,CGroupTeam *>::iterator lter = mGroupTeamMan.begin();
	for ( ; lter!=mGroupTeamMan.end(); ++lter )
	{
		if ( NULL != lter->second )
		{
			delete lter->second;
		}
	}
	mGroupTeamMan.clear();
}

CGroupTeam* GroupTeamManager::CreateGroupTeam( unsigned long teamID, ITEM_SHAREMODE itemShareMode,EXPERIENCE_SHAREMODE expreShareMode )
{
	CGroupTeam* pGroupTeam = NEW CGroupTeam( teamID, itemShareMode, expreShareMode );
	if ( !pGroupTeam )
		return NULL;

	mGroupTeamMan.insert( std::pair<unsigned long,CGroupTeam *>(teamID,pGroupTeam) );

	return pGroupTeam;
}

CGroupTeam* GroupTeamManager::GetGroupTeamByID(unsigned long teamID )
{
	if ( mGroupTeamMan.empty() )
		return NULL;

	std::map<unsigned long,CGroupTeam *>::iterator lter = mGroupTeamMan.find( teamID );
	if ( lter != mGroupTeamMan.end() )
		return lter->second;

	return NULL;
}

void GroupTeamManager::OnGroupPlayerSwichMap(CPlayer* pMember  )
{
	if ( !pMember )
		return;

	//是不是在组队状态,处理组队状态
	if ( pMember->GetSelfStateManager().IsGroupState() )
	{
		//获取当前组队指针
		CGroupTeam* pTeam =  pMember->GetSelfStateManager().GetGroupState().GetGroupTeam();
		//assert( pTeam!= NULL );
		if ( NULL == pTeam )
		{
			D_ERROR( "GroupTeamManager::OnGroupPlayerSwichMap，NULL == pTeam\n" );
			return;
		}
		if ( pTeam )
		{
			if( pTeam->OnSwitchMapExitGroupTeam( pMember, pMember->GetMapID() ) )
				RecoverGroupTeam( pTeam );
		}
	}

	//玩家跳地图影响Roll点
	RollItemManagerSingle::instance()->OnPlayerLeaveSrv( pMember );
}

void GroupTeamManager::OnGroupPlayerExitMap( CPlayer* pMember  )
{
	if ( !pMember )
		return;

	//是不是在组队状态
	if ( pMember->GetSelfStateManager().IsGroupState() )
	{
		CGroupTeam* pTeam =  pMember->GetSelfStateManager().GetGroupState().GetGroupTeam();
		//assert( pTeam!= NULL );
		if ( NULL == pTeam )
		{
			D_ERROR( "GroupTeamManager::OnGroupPlayerExitMap，NULL==pTeam\n" );
			return;
		}
		if ( pTeam )
		{
			pTeam->ExitGroupTeam( pMember->GetFullID() );
			if ( pTeam->IsMemberEmpty() )//如果组队为空了
				RecoverGroupTeam( pTeam );
		}
	}

	//玩家跳地图影响Roll点
	RollItemManagerSingle::instance()->OnPlayerLeaveSrv( pMember );
}

void GroupTeamManager::RecoverGroupTeam(CGroupTeam *pGroupTeam)
{
	if ( !pGroupTeam )
		return;

	mGroupTeamMan.erase( pGroupTeam->GetGroupTeamID() );
	delete pGroupTeam;
	pGroupTeam = NULL;
}


//玩家切换地图时
//玩家创建组队时
void GroupTeamManager::OnRecvTeamDetialInfo(unsigned teamID,const PlayerID * playerArr, const int* memberSeqArr, int count, 
											ITEM_SHAREMODE itemShareMode,
											EXPERIENCE_SHAREMODE expreShareMode, 
											ROLLITEM_MODE itemRollLevel, 
											int leaderIndex )
{
	if ( !playerArr )
		return;

	if ( !memberSeqArr )
		return;

	if( count <=0 || count > MAXTEAMMEMBER )
	{
		D_ERROR("GroupTeamManager::OnRecvTeamDetialInfo 组队的玩家个数大于允许的最大个数\n");
		return;
	}

	if ( itemShareMode >= E_ITEMMODE_MAX  )
	{
		D_ERROR("组队的物品分配模式非法:%d\n",(unsigned int)itemShareMode );
		return;
	}

	if( itemShareMode == E_TURN_MODE)//轮流模式已经取消（zsw/2010.8.13）
	{
		D_ERROR("组队的物品分配模式不能为轮流模式:%d\n",(unsigned int)itemShareMode );
		return;
	}

	if ( expreShareMode >= E_EXPERIENCE_MAX)
	{
		D_ERROR("组队的经验分配模式非法:%d\n",(unsigned int)expreShareMode );
		return;
	}


//#ifdef _DEBUG
//	D_DEBUG("开始接受组队的详细信息:一共%d个玩家 物品分配模式为:%d ,经验分配模式为:%d\n",count ,itemShareMode, expreShareMode );
//#endif
	
	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
	if ( !pTeam )
		pTeam = CreateGroupTeam( teamID, itemShareMode, expreShareMode );

	//assert( pTeam != NULL );
	if ( NULL == pTeam )
	{
		D_ERROR( "GroupTeamManager::OnRecvTeamDetialInfo, NULL == pTeam\n" );
		return;
	}


#ifdef _DEBUG
	D_DEBUG("获取组队队伍成功,队伍ID:%d\n",teamID );
#endif

	//更新组队的队伍的信息
	pTeam->RecvGroupMemberInfo( playerArr, memberSeqArr,count, leaderIndex );

	pTeam->UpdateGroupTeamInfo( itemShareMode, expreShareMode,itemRollLevel );

	/*std::vector<CGroupTeam::TeamPlayer>& TeamPlayerVec = pTeam->GetTeamMemberVec();
	TeamPlayerVec.clear();
	for ( unsigned int i = 0 ; i< (unsigned)count; ++i )
	{
		CGroupTeam::TeamPlayer tp;
		tp.memberSeq = memberSeqArr[i];
		tp.playerID =  playerArr[i];
		CGateSrv* pGateSrv = CManG_MProc::FindProc( tp.playerID.wGID );
		if ( !pGateSrv )
			D_ERROR("依据GateSrv传来的组队玩家的PlayerID,找不到组队的玩家的GateSrv:%d\n",tp.playerID.wGID );
		else
		{
			tp.pPlayer = pGateSrv->FindPlayer( tp.playerID.dwPID );
		}
		TeamPlayerVec.push_back(tp);
	}*/
}

////玩家进入组队时
//void GroupTeamManager::OnRecvPlayerEnterGroupTeam( unsigned teamID, const PlayerID& playerID ,int memberSeq )
//{
//	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
//	if ( !pTeam )
//	{
//		D_ERROR("玩家加入组队，但是找不到组队信息\n");
//		return;
//	}
//
//	//只插入不存在的玩家
//	if ( pTeam->IsPlayerExists(playerID) )
//		return;
//
//	CPlayer* player = NULL;
//	CGateSrv* pGateSrv = CManG_MProc::FindProc( playerID.wGID );
//	if ( !pGateSrv )
//		D_ERROR("找不到组队的玩家的GateSrv\n");
//	else
//	{
//		player = pGateSrv->FindPlayer( playerID.dwPID );
//	}
//
//	pTeam->NewPlayerEnterGroupTeam( player ,memberSeq , playerID);
//
//}

////玩家离开组队
//void GroupTeamManager::OnRecvPlayerLeaveGroupTeam(unsigned int teamID, const PlayerID &playerID, int memberSeq)
//{
//	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
//	if ( !pTeam )
//	{
//		D_ERROR("玩家离开组队，但是找不到组队信息\n");
//		return;
//	}
//
//	//只有当玩家存在在组队中时，才能接到该消息
//	assert( pTeam->IsPlayerExists( playerID ) == true );
//
//#ifdef _DEBUG
//	CPlayer* player = NULL;
//	CGateSrv* pGateSrv = CManG_MProc::FindProc( playerID.wGID );
//	if ( !pGateSrv )
//		D_ERROR("找不到组队的玩家的GateSrv\n");
//	else
//	{
//		player = pGateSrv->FindPlayer( playerID.dwPID );
//		if ( player )//玩家在组队中，则组队状态必须为激活
//			assert( player->GetSelfStateManager().GetGroupState().IsActive() == true );
//	}
//#endif
//
//	pTeam->ExitGroupTeam( memberSeq );	
//
//	//如果退队成功,判断组队是否为空,如果为空，回收组队内存
//	if ( pTeam->IsMemberEmpty() || !pTeam->IsHasExistPlayer() )
//		RecoverGroupTeam( pTeam );
//}

void GroupTeamManager::OnRecvPlayerIsAgreeRollItem(unsigned rollItemID ,const PlayerID& playerID, bool isAgree )
{
	RollItem* pRollItem = RollItemManagerSingle::instance()->GetRollItem( rollItemID );
	if ( pRollItem )
	{
		CPlayer* player = NULL;
		CGateSrv* pGateSrv = CManG_MProc::FindProc( playerID.wGID );
		if ( !pGateSrv )
			D_ERROR("找不到组队的玩家的GateSrv\n");
		else
		{
			player = pGateSrv->FindPlayer( playerID.dwPID );
			//assert( player!= NULL );
			if ( NULL == player )
			{
				D_ERROR( "GroupTeamManager::OnRecvPlayerIsAgreeRollItem, NULL == player\n" );
				return;
			}
		}

		//如果接到反馈后，开始了Roll物品的过程
		pRollItem->RecvRollReq( player, isAgree );
	}
	else
	{
		D_ERROR("玩家Roll点的回应，但是找不到对应的RollItem规则\n");
	}

}

void GroupTeamManager::OnRecvChangeTeamLeader(unsigned int teamID, const MUX_PROTO::PlayerID &playerID)
{

	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
	if ( !pTeam )
	{
		D_ERROR("组队错误OnRecvChangeTeamLeader\n");
		return;
	}

	pTeam->SetGroupTeamLeaderIndex( playerID );
}

void GroupTeamManager::OnRecvChangeTeamItemShareMode(unsigned teamID,ITEM_SHAREMODE shareMode )
{
	D_DEBUG("改变组队%d的道具拾取模式 %d\n", teamID, shareMode );

	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
	if ( !pTeam )
	{
		D_ERROR("组队错误:OnRecvChangeTeamItemShareMode\n");
		return;
	}
	pTeam->UpdateGroupTeamItemShareMode( shareMode );
}

void GroupTeamManager::OnRecvChangeTeamExpreShareMode(unsigned teamID, EXPERIENCE_SHAREMODE expreShareMode )
{
	D_DEBUG("改变了组队%d的经验分享模式 %d\n", teamID, expreShareMode );

	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
	if ( !pTeam )
	{
		D_ERROR("组队错误OnRecvChangeTeamExpreShareMode\n");
		return;
	}
	pTeam->UpdateGroupTeamExpreShareMode( expreShareMode );
}

void GroupTeamManager::OnRecvChangeTeamRollItemLevel(unsigned int teamID, unsigned int rollItemLevel)
{
	D_DEBUG("改变组队%d的Roll物品等级 %d\n", teamID,rollItemLevel );

	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
	if ( !pTeam )
	{
		D_ERROR("组队错误OnRecvChangeTeamExpreShareMode\n");
		return;
	}

	pTeam->UpdateGroupTeamRollItemLevel( (ITEM_MASSLEVEL)(rollItemLevel + 2));
}

void GroupTeamManager::OnRecvKickPlayerOnTeam(unsigned teamID, const PlayerID& playerID )
{
	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
	if ( !pTeam )
	{
		D_ERROR("组队错误OnRecvBanPlayerOnTeam\n");
		return;
	}

	//CGateSrv* pGateSrv = CManG_MProc::FindProc( playerID.wGID );
	//if ( NULL != pGateSrv )
	//{
	//	CPlayer* pLeaveTeamPlayer = pGateSrv->FindPlayer( playerID.dwPID );
	//	if ( NULL != pLeaveTeamPlayer )
	//	{
	//		//离开组队的玩家在本mapsrv上，检测其是否在副本中，若在副本中，则发起离开副本操作，并重置副本中该玩家的初始进入信息；
	//		pLeaveTeamPlayer->OnLeaveTeamCopyProcCheck();
	//	}
	//}

	pTeam->KickPlayerOnTeam( playerID );

}

void GroupTeamManager::OnRecvDisBandTeam(unsigned teamID )
{
	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
	if ( !pTeam )
	{
		D_ERROR("组队错误OnRecvDisBandTeam\n");
		return;
	}
	pTeam->OnDisBandGroupTeam();
}


void GroupTeamManager::OnRecvTeamPlayerGetNewItem(unsigned int teamID, unsigned int itemTypeID, const PlayerID& pickPlayerID )
{
	CGroupTeam* pTeam = GetGroupTeamByID( teamID );
	if ( pTeam )
		pTeam->NoticeTeamMemberGetNewItem( pickPlayerID, itemTypeID );
}

