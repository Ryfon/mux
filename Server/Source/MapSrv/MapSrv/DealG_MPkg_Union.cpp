﻿/**
* @file DealPkg.cpp
* @brief 处理包实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: DealPkg.cpp
* 摘    要: 根据通信参与方以及消息中的命令字处理收到的网络消息
* 作    者: hyn
* 完成日期: 2007.12.17
*
*/

#include "DealG_MPkg.h"

#include "MuxMap/mannormalmap.h"

bool CDealG_MPkg::OnRegisterUnionMember( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMRegisterUnionMember );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerId.dwPID );
	if( NULL == pPlayer)
		return false;

	if((NULL != pPlayer->Union()) || (0 == pMsg->unionID) ||(0 == pMsg->unionLevel) )
		return false;

	CUnion *pUnion = CUnionManagerSingle::instance()->RetrieveUnion(pMsg->unionID);
	if(NULL == pUnion)
		return false;

	//关联工会信息
	pUnion->RegisterPlayer(pPlayer, pMsg->unionName, pMsg->ownerUiid, pMsg->unionActive, pMsg->unionLevel);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnReportUnionSkills( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMReportUnionSkills );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerId.dwPID );
	if(NULL == pPlayer)
		return false;

	CUnion *pUnion = pPlayer->Union();
	if(NULL == pUnion)
		return false;

	pUnion->SetSkills(&(pMsg->unionSkills[0]));
	pUnion->InitUnionAttributeEffect();

	return true;
	TRY_END;
	return false;
}


bool CDealG_MPkg::OnReportUnionLevel( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMReportUnionLevel );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if(NULL == pPlayer)
		return false;

	CUnion *pUnion = pPlayer->Union();
	if(NULL == pUnion)
		return false;

	pUnion->SetLevel(pMsg->unionLevel);

	return true;
	TRY_END;

	return false;
}
bool CDealG_MPkg::OnUnregisterUnionMember( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMUnregisterUnionMember );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerId.dwPID );
	if(NULL != pPlayer)
	{
		CUnion *pUnion= pPlayer->Union();
		if(NULL != pUnion)
		{
			//取消战盟注册
			pUnion->UnregisterPlayer(pPlayer, true);

			//血值魔值更新
			pPlayer->NoticeHPMPChange();
		}

		//如果已经在工会模式，通知客户端强制改称常规模式
		if( pPlayer->GetBattleMode() == GUILD_MODE )
		{
			pPlayer->ChangeBattleMode( DEFAULT_MODE );

			MGChangeBattleMode battleMode;
			battleMode.mode = DEFAULT_MODE;
			battleMode.bSuccess = true;
			pPlayer->SendPkg<MGChangeBattleMode>( &battleMode );
		}

	}

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnIssueUnionTask( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMIssueUnionTasks );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerId.dwPID );
	if( NULL == pPlayer)
		return false;

	//查找对应工会
	CUnion *pUnion = pPlayer->Union();
	if(NULL == pUnion)
		return false;

	pUnion->SetIssuedTasks(pMsg->taskEndTime);
	pUnion->SetActive(pMsg->costActive);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnChanageUnionActive( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeUnionActive );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerId.dwPID );
	if( NULL == pPlayer)
		return false;

	//查找对应工会
	CUnion *pUnion = pPlayer->Union();
	if(NULL == pUnion)
		return false;

	pUnion->SetActive(pMsg->newValue);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnChanageUnionPrestige( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChangeUnionPrestige );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerId.dwPID );
	if( NULL == pPlayer)
		return false;

	//查找对应工会
	CUnion *pUnion = pPlayer->Union();
	if(NULL == pUnion)
		return false;

	pUnion->SetPrestige(pMsg->newValue);

	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnChanageUnionBadge( CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMChanageUnionBadge );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->playerId.dwPID );
	if( NULL == pPlayer)
		return false;

	//查找对应工会
	CUnion *pUnion = pPlayer->Union();
	if(NULL == pUnion)
		return false;

	pUnion->SetBadge(pMsg->newBadge);

	//通知角色要显示的工会信息，因为GMChanageUnionBadge的处理晚于玩家在地图上出现的消息。
	CMuxMap* pMap = pPlayer->GetCurMap();
	if( NULL != pMap )
	{
		MGUnionMemberShowInfo* pShowInfo = pUnion->GetUnionMemberShowInfo(pPlayer);
		if( NULL != pShowInfo )
		{
			pMap->NotifySurrounding<MGUnionMemberShowInfo>( pShowInfo, pPlayer->GetPosX(), pPlayer->GetPosY() );
		}
	}

	return true;
	TRY_END;
	return false;
}
bool CDealG_MPkg::OnForwardUnionCmd(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMForwardUnionCmd );

	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	if( NULL == pPlayer)
		return false;

	MGForwardUnionCmdRst rst;
	StructMemSet( rst, 0x00, sizeof(rst));

	rst.flag = pPlayer->HaveUnionNpcChat((int)pMsg->cmdID);
	rst.cmdID = pMsg->cmdID;
	switch(pMsg->cmdID)
	{
	case GCUnionUISelectCmd::CREATE_UNION:
		{
			do 
			{
				UnionCostArg &costArg = CUnionConfigSingle::instance()->GetCreateUnionCost();//创建工会的消耗
				MGUnionCostArg costArgInfo;
				costArgInfo.arg = costArg;
				pPlayer->SendPkg<MGUnionCostArg>(&costArgInfo);

				bool moneycheck = false;
				if ( pPlayer->IsUseGoldMoney() )
				{
					//使用金币
					moneycheck = (pPlayer->GetGoldMoney() < costArg.money);
				} else {
					//使用银币
					moneycheck = (pPlayer->GetSilverMoney() < costArg.money);
				}

				if ( moneycheck )//是否有足够的金钱,//双币种判断
				{
					D_WARNING("创建工会时玩家%s金钱不足\n", pPlayer->GetNickName());
					return true;
				}

				if(pPlayer->CounterItemByID(costArg.itemID) < (int)costArg.itemCount)//是否有相应的物品
				{
					D_WARNING("创建工会时玩家%s所需物品不足\n", pPlayer->GetNickName());
					return true;
				}

				//预扣除金钱和道具
				if ( pPlayer->IsUseGoldMoney() )
				{
					//使用金币
					pPlayer->SubGoldMoney(costArg.money);//双币种判断
				} else {
					//使用银币
					pPlayer->SubSilverMoney(costArg.money);//双币种判断
				}

				pPlayer->OnDropItemByType(costArg.itemID, costArg.itemCount);
			} while(0);

			rst.createUnion = pMsg->createUnion;
			break;
		}

	case GCUnionUISelectCmd::ADVANCE_UNION_LEVEL:
		{
			rst.advanceUnionLevel = pMsg->advanceUnionLevel;
			break;
		}

	case GCUnionUISelectCmd::ISSUE_UNION_TASK:
		{
			rst.issueTask = pMsg->issueTask;
			break;
		}

	case GCUnionUISelectCmd::LEARN_UNION_SKILL:
		{
			rst.learnUnionSkill = pMsg->learnUnionSkill;
			break;
		}

	case GCUnionUISelectCmd::DESIGN_UNION_BADGE:
		{
			rst.modifyUnionBadge = pMsg->modifyUnionBadge;
			break;
		}
	}

	pPlayer->SendPkg<MGForwardUnionCmdRst>( &rst );
	return true;
	TRY_END;
	return false;
}

bool CDealG_MPkg::OnCreateUnionErrNtf(  CGateSrv* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if ( NULL == pOwner )
	{
		return false;
	}
	if ( NULL == pPkg )
	{
		return false;
	}

	TRY_BEGIN;

	PKG_CONVERT( GMCreateUnionErrNtf );

	UnionCostArg &costArg = CUnionConfigSingle::instance()->GetCreateUnionCost();//创建工会的消耗
	CPlayer* pPlayer = pOwner->FindPlayer( pMsg->lNotiPlayerID.dwPID );
	bool needMailNtf = false;//是否需要邮件通知
	do 
	{
		if( NULL == pPlayer)//没有找到对应玩家？
		{
			needMailNtf = true;
			break;
		}

		if((costArg.money > 0) && (pPlayer->GetSilverMoney() + costArg.money > MAX_MONEY))//金钱还原后是否越界？//双币种判断？
		{
			needMailNtf = true;
			break;
		}

		unsigned int itemInfoArr[2] = {0,0};
		itemInfoArr[0] =  costArg.itemID;
		itemInfoArr[1] =  costArg.itemCount;
		if(!pPlayer->NpcCanGiveItem( itemInfoArr, 2 ))//包裹中是否有足够空间？
		{
			needMailNtf = true;
			break;
		}

		//还原相关数据
		if(!needMailNtf)
		{
			pPlayer->AddSilverMoney(costArg.money);//双币种判断
			/*GetMailItem( unsigned int itemTypeID, unsigned int itemCount, unsigned int itemLevel, unsigned int randSet, unsigned int luckySeed, unsigned int excelSeed );*/
			pPlayer->GetMailItem( costArg.itemID, costArg.itemCount, 0/*等级*/, 0/*返还物品随机集*/, 0, 0);//随机相关属性要加入到costArg中去，目前暂时清掉;
		}
	} while(0);

	if(needMailNtf)
	{
		MGSendSysMailToPlayer sysMail;
		StructMemSet( sysMail, 0x00, sizeof(sysMail));

		sysMail.lNotiPlayerID = pMsg->lNotiPlayerID;
		SafeStrCpy(sysMail.newSysMail.recvPlayerName, pMsg->recvPlayerName);
		SafeStrCpy(sysMail.newSysMail.sendMailTitle, "创建工会错误");
		SafeStrCpy(sysMail.newSysMail.sendMailContent, "");
		sysMail.newSysMail.attachItem[0].usItemTypeID = costArg.itemID;
		sysMail.newSysMail.attachItem[0].ucCount = costArg.itemCount;
		sysMail.newSysMail.silverMoney = costArg.money;//双币种判断
		sysMail.newSysMail.goldMoney = 0;//双币种判断

		if(NULL != pPlayer)
		{
			pPlayer->SendPkg<MGSendSysMailToPlayer>(&sysMail);
		}
		else
		{
			CGateSrv* pGateSrv = CManG_MProc::FindProc( pMsg->lNotiPlayerID.wGID );
			if(NULL == pGateSrv)
			{
				D_ERROR("处理玩家%s创建工会出错消息时，不能找到对应的gateserver\n", pMsg->recvPlayerName);
				return false;
			}
			else
			{
				MsgToPut* pNewMsg = CreateSrvPkg( MGSendSysMailToPlayer, pGateSrv, sysMail );
				pGateSrv->SendPkgToGateServer( pNewMsg );
			}
		}
	}

	return true;
	TRY_END
		return false;
}