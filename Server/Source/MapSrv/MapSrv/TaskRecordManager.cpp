﻿#include "TaskRecordManager.h"
#include "Item/NormalTask.h"
#include "Player/Player.h"
#include <time.h>

#include "TaskIndexManager.h"

#define  NEW_TASKID_LOGIC

TaskRecordInfo* TaskRecordManager::GetTaskRecordInfoInCommon(unsigned int TaskID )
{
	if ( !commonTaskRdArr )
		return NULL;

	for ( unsigned int i = 0 ; i< COMMON_TASKRD_COUNT; i++ )
	{
		TaskRecordInfo* pTask = &commonTaskRdArr[i];
		if ( pTask && (unsigned int)pTask->taskID  == TaskID )
			return pTask;
	}
	return NULL;
}

void TaskRecordManager::UpdateTaskRecordInfoToPlayer(const TaskRecordInfo& taskRdInfo , char taskIndexType )
{
	//更新客户端保存的任务信息

	if ( !m_AttachPlayer )
		return;

	MGPlayerTaskRecordUpdate taskRdInfoMsg;
	taskRdInfoMsg.taskType = taskIndexType;
	taskRdInfoMsg.taskRdInfo.completeCount = taskRdInfo.completeCount;
	taskRdInfoMsg.taskRdInfo.processData = taskRdInfo.processData;
	taskRdInfoMsg.taskRdInfo.taskID = taskRdInfo.taskID;
	taskRdInfoMsg.taskRdInfo.taskTime = taskRdInfo.taskTime;
	taskRdInfoMsg.taskRdInfo.taskState = taskRdInfo.taskState;
	m_AttachPlayer->SendPkg<MGPlayerTaskRecordUpdate>( &taskRdInfoMsg );

}

TaskRecordInfo* TaskRecordManager::AddNewTaskRecord(CNormalTask *pNormalTask)
{
	if ( !pNormalTask )
		return NULL;

	if ( !commonTaskRdArr )
		return NULL;

	//不应该存在任务标号一样的现存任务
	unsigned int i = 0;
	for (  ; i< COMMON_TASKRD_COUNT; i++ )
	{
		TaskRecordInfo& taskInfo = commonTaskRdArr[i];
		if ( (unsigned int)taskInfo.taskID == pNormalTask->GetTaskID() )
			return NULL;
	}

	for ( i = 0 ; i< COMMON_TASKRD_COUNT; i++ )
	{
		TaskRecordInfo& taskInfo = commonTaskRdArr[i];
		if ( taskInfo.taskID == 0 )
		{
			taskInfo.taskID = pNormalTask->GetTaskID();
			taskInfo.completeCount = taskInfo.processData = 0;
			taskInfo.taskState = TASK_PROCESS;
			taskInfo.taskTime  = (unsigned)pNormalTask->GetTaskStartTime();
			return &commonTaskRdArr[i];
		}
	}

	return NULL;
}

void TaskRecordManager::OnRecvTaskInfoRecord(const TaskRecordInfo* taskInfoArr )
{
	TRY_BEGIN;

	if ( !taskInfoArr )
		return;

	if ( !m_AttachPlayer)
		return;

	if ( !commonTaskRdArr || !timeExpireTaskRdArr )
		return;

	//身上的现存未完成的任务
	unsigned int taskIndex = 0;
	for ( unsigned int commonIndex = 0 ; commonIndex < COMMON_TASKRD_COUNT; commonIndex ++ ,taskIndex ++ )
	{
		TaskRecordInfo& taskRd = commonTaskRdArr[commonIndex];
		taskRd = taskInfoArr[taskIndex];

		//如果是0，则代表该位置没有存有任务
		if( taskRd.taskID == 0 )
			continue;

		//给玩家增加个任务
		if( m_AttachPlayer->LoadTask( taskRd.taskID ) == true )
		{
			//如果原来任务就是失败的状态，通知client该任务失败
			if ( taskRd.taskState == TASK_FAIL )
				m_AttachPlayer->SetTaskStat( taskRd.taskID, NTS_FAIL, false );

			//如果原任务完成,还未提交,通知client
			if ( taskRd.taskState == TASK_ACHIEVE )
				m_AttachPlayer->SetTaskStat( taskRd.taskID, NTS_TGT_ACHIEVE, false );

			CNormalTask* pTask =  m_AttachPlayer->FindPlayerTask( taskRd.taskID );
			if ( pTask  == NULL )
			{
				D_ERROR("根据任务ID，找不到玩家的任务指针.任务ID号:%d\n",taskRd.taskID  );
				continue;
			}

			//如果是限时的任务
			if ( pTask->IsLimitTimeTask() )
			{
				//设置任务开始时间
				pTask->SetTaskStartTime( taskRd.taskTime );
				CLimitTimeTaskManagerSingle::instance()->PushLimitTimeTask( pTask );
			}

			//根据任务信息，给玩家加入一个计数器
			NormalTaskProp* pProp = pTask->GetTaskProp();
			if ( pProp && pProp->GetCountType() != CT_INVALID )
			{
				if ( pProp->m_targetMaxID != -1 && pProp->m_targetMinID != -1 )
				{
					m_AttachPlayer->AddUnFinishCounter( pProp->GetCountType(), pProp->m_targetMinID, pProp->m_targetMaxID ,pProp->m_targetCount, taskRd.taskID, taskRd.processData );
				}
			}

			//并更新客户端的信息
			UpdateTaskRecordInfoToPlayer( taskRd );
		}
	}


	//通知玩家身上的已经完成的每日任务
	MGPlayerTaskHistory taskHistory;
	StructMemSet( taskHistory, 0x0, sizeof(taskHistory) );

	unsigned int expiretaskcount = 0;
	for ( unsigned int i = 0 ; i< BACKGROUND_TASKRD_COUNT; i++ )
	{
		TaskRecordInfo& taskRdInfo =  timeExpireTaskRdArr[i];
		if ( taskRdInfo.taskID > 0 && taskRdInfo.taskTime > 0 )
		{ 
			taskHistory.gcTaskHistory.ArrTaskHistory[expiretaskcount] = taskRdInfo;
			expiretaskcount++;
		}
	}

	if ( expiretaskcount > 0 )
	{
		taskHistory.gcTaskHistory.arrSize = expiretaskcount;
		m_AttachPlayer->SendPkg<MGPlayerTaskHistory>( &taskHistory );
	}
	

	TRY_END;
}


void TaskRecordManager::AttachPlayer(CPlayer *pPlayer)
{
	TRY_BEGIN;

	if ( !pPlayer )
		return;

	commonTaskRdArr = timeExpireTaskRdArr = NULL;

	m_AttachPlayer = pPlayer ; 
	FullPlayerInfo * playerInfo = pPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("TaskRecordManager::AttachPlayer, NULL == GetFullPlayerInfo()\n");
		return;
	}
	TaskRecordInfo* taskInfoArr = playerInfo->taskInfo;

	commonTaskRdArr        =   taskInfoArr;
	timeExpireTaskRdArr    = ( taskInfoArr + COMMON_TASKRD_COUNT );


	//接到任务的信息
	OnRecvTaskInfoRecord( playerInfo->taskInfo );

	TRY_END;
}


void TaskRecordManager::ClearTaskRecord(TaskRecordInfo& taskInfo )
{
	taskInfo.taskID    = taskInfo.completeCount = taskInfo.processData = taskInfo.taskTime =  0;
	taskInfo.taskState = TASK_NONE;
}


void TaskRecordManager::OnTaskCounterNumChange( unsigned int taskID, unsigned counterNum )
{
	if ( !m_AttachPlayer )
		return;

	TaskRecordInfo* ptaskRdInfo = GetTaskRecordInfoInCommon(taskID );
	if ( ptaskRdInfo != NULL )
	{
		ptaskRdInfo->processData = counterNum;

		//通知客户端改变了完成数据
		UpdateTaskRecordInfoToPlayer( *ptaskRdInfo );
	}
}

void TaskRecordManager::OnDeleteTask(unsigned int taskID )
{
	if ( !m_AttachPlayer )
		return;

	TaskRecordInfo* ptaskRdInfo = GetTaskRecordInfoInCommon(taskID);
	if ( ptaskRdInfo!=NULL )
	{
		//找到存盘的位置
		int rdIndex = FindCommonTaskRecordIndex( taskID );

		ClearTaskRecord( *ptaskRdInfo );

		if (  rdIndex != -1 )
			m_AttachPlayer->NoticePlayerTaskRdUpdate( false, *ptaskRdInfo , rdIndex );

	} else {
		//assert(false);
		D_ERROR( "TaskRecordManager::OnDeleteTask\n" );
	}
}

int TaskRecordManager::FindCommonTaskRecordIndex(unsigned int taskID )
{
	for ( unsigned int i = 0 ; i < COMMON_TASKRD_COUNT ; i++ )//在普通存盘中寻找
	{
		TaskRecordInfo* pTaskRdInfo = (TaskRecordInfo*)&commonTaskRdArr[i];
		if ( pTaskRdInfo && pTaskRdInfo->taskID == (int)taskID  )
			return i;
	}
	return -1;
}

int TaskRecordManager::FindTimeExpireTaskRecordIndex(unsigned int taskID )
{
	for ( unsigned int i = COMMON_TASKRD_COUNT; i < MAX_TASKRD_SIZE; i++ )//
	{
		TaskRecordInfo* pTaskRdInfo = (TaskRecordInfo*)&timeExpireTaskRdArr[i];//遍历时间过期型任务列表
		if ( pTaskRdInfo && pTaskRdInfo->taskID == taskID )
		{
			return i;
		}
	}
	return -1;
}

bool TaskRecordManager::CheckEveryDayTaskExpire(unsigned int taskID )
{
	if ( !m_AttachPlayer )
		return false;

	int rdIndex  = FindTimeExpireTaskRecordIndex( taskID );
	if( rdIndex != -1 && rdIndex >= COMMON_TASKRD_COUNT )
	{
		TaskRecordInfo* pTaskRdInfo = (TaskRecordInfo*)&timeExpireTaskRdArr[rdIndex];
		if ( pTaskRdInfo )
		{
			time_t curtime     = time(NULL);
			struct tm cur_tm  = *ACE_OS::localtime( &curtime );

			time_t expiretime  = (time_t)pTaskRdInfo->taskTime;
			struct tm expr_tm = *ACE_OS::localtime( &expiretime );

			if ( cur_tm.tm_year != expr_tm.tm_year 
				|| cur_tm.tm_mon != expr_tm.tm_mon 
				|| cur_tm.tm_mday != expr_tm.tm_mday )
			{
				return true;//已经过期了
			}
			else
				return false;//还没过期
		}
	}
	return true;
}


void TaskRecordManager::OnFinishEveryDayTask(unsigned int taskID )
{
	if ( !m_AttachPlayer )
		return;

	int  rdIndex = FindTimeExpireTaskRecordIndex( taskID );//找这个任务是否完成过没有
	if ( rdIndex == -1 )
	{
		 rdIndex = GetExpireTimeEmptyTaskRdIndex( taskID );//如果没有完成过,获取这个任务的位置
	}

	if ( rdIndex  != -1 && rdIndex >= COMMON_TASKRD_COUNT )
	{
		TaskRecordInfo* pTaskRdInfo = (TaskRecordInfo*)&timeExpireTaskRdArr[rdIndex];
		if ( pTaskRdInfo )
		{
			pTaskRdInfo->taskID   =  taskID;
			pTaskRdInfo->taskTime = (unsigned int)time(NULL);

			//通知DB更新
			m_AttachPlayer->NoticePlayerTaskRdUpdate( true, *pTaskRdInfo, rdIndex );

			//通知玩家存盘信息
			UpdateTaskRecordInfoToPlayer( *pTaskRdInfo , 1 );
		}
	}
}

int TaskRecordManager::GetExpireTimeEmptyTaskRdIndex( unsigned int taskID )
{
	for ( unsigned int i = COMMON_TASKRD_COUNT; i < MAX_TASKRD_SIZE; i++ )//
	{
		TaskRecordInfo* pTaskRdInfo = (TaskRecordInfo*)&timeExpireTaskRdArr[i];//遍历时间过期型任务列表
		if ( pTaskRdInfo )
		{
			if ( pTaskRdInfo->taskID == 0 )
			{
				return i;
			}
		}
	}
	return -1;
}

void TaskRecordManager::OnTaskStateChange( CNormalTask* pTask ,ETaskStatType taskStat )
{
	if ( !pTask )
		return;

	if ( !m_AttachPlayer )
		return;

	TaskRecordInfo* taskInfo = GetTaskRecordInfoInCommon( pTask->GetTaskID() );
	ETaskState TaskRdState = TASK_PROCESS;
	if ( taskStat == NTS_FINISH  )
	{
		TaskRdState  = TASK_FINISH;
	}
	else if( taskStat == NTS_TGT_ACHIEVE)
	{
		TaskRdState = TASK_ACHIEVE;//设置为未提交状态
	}
	else if( taskStat == NTS_FAIL )
	{
		TaskRdState  = TASK_FAIL; 
	}

	if ( taskInfo )
		taskInfo->taskState = TaskRdState;

	//普通人物的状态变化了,需要即时存盘
	int  taskRdIndex  = FindCommonTaskRecordIndex( pTask->GetTaskID() );
	if ( taskRdIndex != -1 )
		m_AttachPlayer->NoticePlayerTaskRdUpdate( false, *taskInfo, taskRdIndex );

	//如果该任务做完了，进入做完处理流程
	if ( taskStat == NTS_FINISH )
	{
		if( !pTask->GetTaskProp()->IsTaskCanRepeat() )//任务是否能重复接,如果不可重复接,则记录任务已完成
			m_AttachPlayer->GetNewTaskRecordDelegate().RecordDoneTask( m_AttachPlayer->GetSex(), m_AttachPlayer->GetRace(), m_AttachPlayer->GetClass(), pTask->GetTaskID() );
		
		if( pTask->IsEveryDayTask() )//如果完成的是每日任务
		{
			OnFinishEveryDayTask( pTask->GetTaskID() );
		}

		OnFinishTask( pTask );
	}		
}

void TaskRecordManager::OnFinishTask( CNormalTask* pTask )
{
	if ( !pTask )
		return;

	if ( !m_AttachPlayer )
		return;

	if ( !commonTaskRdArr )
		return;

	unsigned int taskID        = pTask->GetTaskID();

	if(pTask->IsUnionTask())//工会任务完成改变工会活跃度和威望
	{
		m_AttachPlayer->ChanageUnionActive((unsigned short)CUnionConfig::DAILY_TASK_FININSH, 0);
		m_AttachPlayer->ChanageUnionPrestige((unsigned short)CUnionConfig::DAILY_TASK_FINISH_PRES, 0);
	}

	m_AttachPlayer->DeleteTask( taskID );
}

