﻿/********************************************************************
	created:	2009/11/12
	created:	12:11:2009   16:37
	file:		MapPublicNoticeManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef MAP_PUBLIC_NOTICE_MANAGER_H
#define MAP_PUBLIC_NOTICE_MANAGER_H

#include <string>
#include <vector>
#include <map>


struct MapNotices
{
	unsigned int hour1;
	unsigned int min1;
	unsigned int hour2;
	unsigned int min2;
	unsigned int intervaltime;
	std::string  noticeMsg;
	time_t lastupdatetime;

	MapNotices():hour1(0),min1(0),hour2(0),min2(0),intervaltime(0),lastupdatetime(0)
	{
	}
};

struct MapNoticeVec
{
	std::vector<MapNotices> noticeVec;
	
	unsigned int mapID;

	MapNoticeVec():mapID(0)
	{}

	bool CheckNeedPublicNotice( time_t curtime );
};

class MapPublicNoticeManager
{
public:
	static void LoadNoticeConfig();

	static void SendMapPublicNotice();

	static MapNoticeVec* GetMapNoticeVec( unsigned int mapID );
	
	static void SendPublicNotice( unsigned int mapID, const char* notice );

private:
	static std::map<unsigned int, MapNoticeVec> mMapNoticeMap;

	static time_t mLastUpdateTime;
};


#endif