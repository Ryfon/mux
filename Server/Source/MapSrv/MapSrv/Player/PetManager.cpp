﻿#include "PetManager.h"
#include "LoadConfig/LoadSrvConfig.h"
#include "Player.h"

map<unsigned long, unsigned long> CPetManager::m_mapLevelNeedExp;				//每一级升级需要的宠物经验
map<unsigned long, PetSkillInfo> CPetManager::m_mapSkillInfo;			//每学一个宠物技能需要的宠物等级
UINT CPetManager::m_maxLevel;			//最大等级
UINT CPetManager::m_maxHungerDegree;		//最大宠物饥饿度
UINT CPetManager::m_commonConsumeHunger;	//每2分钟消耗的饥饿度
UINT CPetManager::m_notGetExpHunger;		//不增加经验的饥饿度界限
UINT CPetManager::m_imageSkillPerHunger;	//每次变身技能需要消耗的饥饿度

CPetManager::CPetManager(void)
{
}

CPetManager::~CPetManager(void)
{
}


bool CPetManager::FindLevelNeedExp( unsigned long level, unsigned long& needExp )		//通过等级查找需要升级的经验
{
	map<unsigned long, unsigned long>::iterator iter = m_mapLevelNeedExp.find( level );
	if( iter != m_mapLevelNeedExp.end() )
	{
		needExp = iter->second;
		return true;
	}
	return false;
}

///宠物是否可以学习指定技能；
bool CPetManager::GetPetSkillLearnCond( EPetSkillMask petSkillFlag, UINT& needLevel, UINT& needMoney )
{
	needLevel = 99999;
	needMoney = 0xfffffff;

	map<unsigned long, PetSkillInfo>::iterator iter = m_mapSkillInfo.find( petSkillFlag );
	if( iter != m_mapSkillInfo.end() )
	{
		needLevel = iter->second.skillNeedLevel;
		needMoney = iter->second.skillNeedMoney;
		return true;
	}

	D_ERROR( "SubSkillUseHunger，找不到对应宠物技能%d的相关信息\n", petSkillFlag );
	return false;
}

///使用技能扣除宠物饥饿度，如果饥饿度足够扣，则返回成功，否则返回失败；
bool CPetManager::SubSkillUseHunger( EPetSkillMask petSkillFlag, UINT& toSubDegree ) 
{ 
	map<unsigned long, PetSkillInfo>::iterator iter = m_mapSkillInfo.find( petSkillFlag );
	if( iter != m_mapSkillInfo.end() )
	{
		if ( toSubDegree >= iter->second.skillComsumeHunger )
		{
			toSubDegree -= iter->second.skillComsumeHunger;
			return true;
		} else {
			return false;
		}
	}

	D_ERROR( "SubSkillUseHunger，找不到对应宠物技能%d的相关信息\n", petSkillFlag );
	return false;
};


bool CPetManager::GetSkillFlagConsumeHunger( EPetSkillMask petSkillFlag, UINT& consumeHunger )
{
	map<unsigned long, PetSkillInfo>::iterator iter = m_mapSkillInfo.find( petSkillFlag );
	if( iter != m_mapSkillInfo.end() )
	{
		consumeHunger = iter->second.skillComsumeHunger;
		return true;
	}

	D_ERROR( "SubSkillUseHunger，找不到对应宠物技能%d的相关信息\n", petSkillFlag );
	return false;
}

///召唤减饥饿度；
bool CPetManager::SummonSubHunger( UINT& inHunger )
{
	if ( inHunger >= m_commonConsumeHunger )
	{
		inHunger -= m_commonConsumeHunger;
		return true;
	} else {
		inHunger = 0;
		return false;
	}
}

UINT CPetManager::GetMaxPetLevel()
{
	return m_maxLevel;
}


UINT CPetManager::GetMaxPetHungerDegree()
{
	return m_maxHungerDegree;
}


UINT CPetManager::GetNotGetExpHunger()
{
	return m_notGetExpHunger;
}


UINT CPetManager::GetImageSkillPerHunger()	//每次使用变身技能需要消耗的饥饿度
{
	return m_imageSkillPerHunger;
}


UINT CPetManager::GetCommonConsumeHunger()
{
	return m_commonConsumeHunger;
}

bool CPetManager::LoadPetXml(const char* szFilePath )		//读取XML
{
	TRY_BEGIN;

	if ( NULL == szFilePath )
	{
		D_ERROR( "CPetManager::LoadPetXml，input file name NULL\n" );
		return false;
	}

	CXmlManager xmlLoader;
	if( !xmlLoader.Load(szFilePath) )
	{
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		return false;
	}

	//取该XML所有第二层的ELEMNET
	vector<CElement*> elementSet;
	xmlLoader.FindChildElements( pRoot, elementSet );
	
	if ( elementSet.empty() )
	{
		return false;
	}

	CElement* tmpEle = NULL;
	int curpos = 0;
	unsigned long level =0,needExp = 0;
	//编译警告去除，by dzj, 09.07.08, unsigned int rootLevel = pRoot->Level();
	for ( vector<CElement*>::iterator tmpiter = elementSet.begin();
		tmpiter != elementSet.end(); ++tmpiter )
	{
		tmpEle = *tmpiter;
		if( strcmp(tmpEle->Name(), "PetExp" ) == 0 ) //宠物经验值经验	
		{
			if ( !IsXMLValValid( tmpEle, "Level", curpos ) )
			{
				return false;
			}		
			level = atoi( tmpEle->GetAttr("Level") );

			if ( !IsXMLValValid( tmpEle, "NeedExp", curpos ) )
			{
				return false;
			}		
			needExp = atoi( tmpEle->GetAttr("NeedExp") );
			m_mapLevelNeedExp.insert( make_pair(level,needExp) );
		}

		if( strcmp( tmpEle->Name(),"MaxPetLevel") == 0)
		{
			if ( !IsXMLValValid( tmpEle, "MaxLevel", curpos ) )
			{
				return false;
			}		
			m_maxLevel = atoi( tmpEle->GetAttr("MaxLevel") );
		}

		if( strcmp( tmpEle->Name(),"PetHunger") == 0)
		{
			if ( !IsXMLValValid( tmpEle, "MaxHunger", curpos ) )
			{
				return false;
			}		
			m_maxHungerDegree = atoi( tmpEle->GetAttr("MaxHunger") );

			if ( !IsXMLValValid( tmpEle, "NotGetExpHunger", curpos ) )
			{
				return false;
			}		
			m_notGetExpHunger = atoi( tmpEle->GetAttr("NotGetExpHunger") );

			if ( !IsXMLValValid( tmpEle, "ConsumeHunger", curpos ) )
			{
				return false;
			}		
			m_commonConsumeHunger = atoi( tmpEle->GetAttr("ConsumeHunger") );


			if ( !IsXMLValValid( tmpEle, "ImageSkillPerHunger", curpos ) )
			{
				return false;
			}		
			m_imageSkillPerHunger = atoi( tmpEle->GetAttr("ImageSkillPerHunger") );
		}

		if( strcmp( tmpEle->Name(),"PetSkill") == 0)
		{
			PetSkillInfo petSkillInfo;
			if ( !IsXMLValValid( tmpEle, "NeedLevel", curpos ) )
			{
				return false;
			}		
			petSkillInfo.skillNeedLevel = atoi( tmpEle->GetAttr("NeedLevel") );

			if ( !IsXMLValValid( tmpEle, "ConsumeMethod", curpos ) )
			{
				return false;
			}		
			petSkillInfo.consumeMethod = atoi( tmpEle->GetAttr("ConsumeMethod") );
			
			if ( !IsXMLValValid( tmpEle, "NeedMoney", curpos ) )
			{
				return false;
			}		
			petSkillInfo.skillNeedMoney = atoi( tmpEle->GetAttr("NeedMoney") );

			if ( !IsXMLValValid( tmpEle, "ConsumeHunger", curpos ) )
			{
				return false;
			}		
			petSkillInfo.skillComsumeHunger = atoi( tmpEle->GetAttr("ConsumeHunger") );

			if ( !IsXMLValValid( tmpEle, "ID", curpos ) )
			{
				return false;
			}		
			petSkillInfo.skillID = atoi( tmpEle->GetAttr("ID") );
			if ( petSkillInfo.skillID >= 10 )
			{
				D_ERROR( "宠物基本技能号%d超限\n", petSkillInfo.skillID );
				return false;
			}
        	petSkillInfo.skillID = ( EPetSkillMask ) (0x00000001 << petSkillInfo.skillID);
			m_mapSkillInfo.insert( make_pair(petSkillInfo.skillID, petSkillInfo ) );
		}
		curpos++;
	}

	return true;
	TRY_END;
	return false;
}


bool CPetManager::IsXMLValValid( CElement* pEle, const char* attName, int nRecord )
{
	if ( NULL == pEle || NULL == attName )
	{
		return false;
	}
	if ( NULL == pEle->GetAttr( attName ) )
	{
		D_ERROR( "玩家技能属性XML配置文件第%d条记录属性%s失败\n", nRecord, attName );
		return false;
	}
	return true;
}


bool CPetManager::Init()
{
	return LoadPetXml("config/pet.xml");
}

