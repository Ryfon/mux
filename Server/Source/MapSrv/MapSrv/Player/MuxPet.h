﻿


#ifndef MUXPET_H
#define MUXPET_H

#include "../../../Base/PkgProc/SrvProtocol.h"
#include "Utility.h"

#include "PetManager.h"

#include <string>
using namespace MUX_PROTO;

#define MAX_PETSKILL_SZIE 10
#define NEW_PET_UPDATE

class CPlayer;	//声明

class CMuxPet
{
	friend class CPlayer;

public:
	CMuxPet(void);
	~CMuxPet(void);

private:
	///重置宠物相关属性
	void PetInfoReset();

private:
	bool IsActivate();		//查询是否激活过宠物系统
	bool PetActivate( CPlayer* pPlayer );		//初始化宠物信息

private:
	void EnableSummonState();		//设置成召唤状态
	void DisableSummonState();		//废除召唤状态
	bool IsSummoned() { return m_petInfoStable.petInfoStable.isSummoned; }; //宠物是否已被召出；

	///在唤回与下次召出间保存宠物时钟；
	inline void SavePetTimer( ACE_Time_Value& inTimer, const ACE_Time_Value& curTime )
	{
		inTimer = curTime - inTimer;//保存目前为止的已逝时间
	}

	///在唤回与下次召出间恢复宠物时钟；
	inline void RestorePetTimer( ACE_Time_Value& inTime, const ACE_Time_Value& curTime )
	{
		//由于饥饿度已先扣去，因此不必再重复计算之前消耗的时间, by dzj, 09.08.14；inTime = curTime - inTimer;//将时钟的开始时间往前推一点，以包含上次的已逝时间；
		inTime = curTime;
	}


private:
	void PetTimeProcess();//定时调用

	bool SummonPet();//唤出宠物

	bool ClosePet();//唤回宠物

	bool SetPetName( const char* petName );//设置宠物的姓名

	bool SetPetImagePos( unsigned int imagePos );//设置宠物的外形ID，值为256当中的序号；

	bool SetPetSkillState( unsigned long newSkillState );//设置正在使用的持续性技能

	bool FeedPet( unsigned long foodNum );//给宠物喂食;

private:
	void PetExpAdd( unsigned long addExp );//增加经验

	bool SetHunger( unsigned long setHunger);

public:
	UINT GetHunger();			//获取饥饿度

	void GetDetailInfo(MGPetinfoStable *&pPetStable, MGPetinfoInstable *&pPetInstable, MGPetAbility *&pPetAbility)
	{
		pPetStable = &m_petInfoStable;
		pPetInstable = &m_petInfoInstable;	
		pPetAbility = &m_petAbility;
		return;
	}

	void OnPlayerSwitchMap();//当玩家切换地图时

private:
	//通知宠物主人不稳定信息改变
	void NotifyInstableInfo();

	//通知宠物主人宠物能力改变
	void NotifyAbilityInfo();

	//通知宠物主人稳定信息改变
	void NotifyStableInfo();		

	//获得用于向周围人广播的宠物信息；
	MGPetinfoStable* GetSurPetNotiInfo();

	inline MGPetinfoInstable* GetPetInfoInstable()
	{
		return &m_petInfoInstable;
	}

	//宠物状态变化时，通知周围人此变化信息；
	void NotifySurPetInfo( bool isOnlySelf /*是否只通知自己*/);

	//通知宠物主人宠物完整信息；
	bool NotifySelfPetInfo();

	void UpdatePetInfoToDB();

private:
	//根据输入位置，每位置大小，最大位置数得到输入位置对应为第outUnitPos位置的第outUnitBitPos位；
	inline bool GetUnitBitPos( unsigned int inVal, unsigned int unitSize/*单位bit*/, unsigned int maxUnit/*outUnitPos的可能最大值*/, unsigned int& outUnitPos, unsigned int& outUnitBitPos )
	{
		if ( unitSize<=0 )
		{
			return false;
		}
		outUnitPos = inVal/unitSize;//第几个char;
		outUnitBitPos = inVal%unitSize;//第charpos中字符的第几个位置;
		if ( outUnitPos >= maxUnit )
		{
			return false;//超出了可学变身技能最大标号值；
		}
		return true;
	}

	///基本技能习得;
	bool StudyPetSkill( unsigned int petSkillFlag );

	///设置宠物技能时，检测处理，返回值：是否已设置使用该技能，无论是早先的设置，还是本次的设置；
	bool SkillSetCheckOneFlag( unsigned long skillState, EPetSkillMask petSkillFlag, const ACE_Time_Value& curTime, ACE_Time_Value& skillStTime/*技能使用开始时间*/ );
	
	///返回值，是否在使用指定技能，并且已到扣饥饿度时间；
	inline bool SkillUseTimeCheck( EPetSkillMask toCheckFlag, unsigned int checkInterval, const ACE_Time_Value& curTime, ACE_Time_Value& lastTime, bool isNeedNotify )
	{
		if ( IsPetSkillInUse( toCheckFlag ) 
			&& ( (curTime - lastTime).sec() >= (int)checkInterval )
			)
		{
			isNeedNotify = true;
			if ( CPetManager::SubSkillUseHunger( toCheckFlag, m_petInfoInstable.petInfoInstable.petHunger ) )
			{
				lastTime += ACE_Time_Value( checkInterval );
			} else {
				m_petInfoInstable.petInfoInstable.petHunger = 0;
				ClosePet();
			}			
		}
		return false;
	}

	///是否使用了变身技能;
	inline bool IsTransLook() { return ( 2 != m_petInfoStable.petInfoStable.lookPos ) && ( 1 != m_petInfoStable.petInfoStable.lookPos ); };

	///是否有任何基本技能当前被置为开启状态；
	inline bool IsAnySkillInUse() { return ( IS_ACTIVATE_MASK != m_petInfoInstable.petInfoInstable.petSkillSet ); }

	///是否拥有此技能；
	inline bool IsPetSkillLearned( EPetSkillMask petSkillFlag ) { return ( (m_petAbility.petAbility.skillValid & petSkillFlag) != 0 ); }

	///是否已设置为使用此技能
	inline bool IsPetSkillInUse( EPetSkillMask petSkillFlag ) { return ( ( m_petInfoInstable.petInfoInstable.petSkillSet & petSkillFlag ) != 0 ); }

	///是否可以设置此技能状态（输入参数中的设置位必须为已学习技能）
	inline bool CanSkillStateSet( unsigned long skillState );

	inline void SetSkillUseFlag( EPetSkillMask petSkillFlag ) 
	{ 
		m_petInfoInstable.petInfoInstable.petSkillSet |= petSkillFlag;
		UINT consumeHunger = 0;
		if( CPetManager::GetSkillFlagConsumeHunger( petSkillFlag, consumeHunger ) )
		{
			m_petInfoInstable.petInfoInstable.skillComsumeHunger += consumeHunger;
		}
	};
	inline void ClearSkillUseFlag( EPetSkillMask petSkillFlag )
	{
		m_petInfoInstable.petInfoInstable.petSkillSet &= (~petSkillFlag);
		UINT consumeHunger = 0;
		if( CPetManager::GetSkillFlagConsumeHunger( petSkillFlag, consumeHunger ) )
		{
			if( m_petInfoInstable.petInfoInstable.skillComsumeHunger < consumeHunger )
			{
				m_petInfoInstable.petInfoInstable.skillComsumeHunger = 0;
			}else{
				m_petInfoInstable.petInfoInstable.skillComsumeHunger -= consumeHunger;
			}
		}
	};
	inline void ClearAllSkillUseFlag() 
	{
		m_petInfoInstable.petInfoInstable.petSkillSet = IS_ACTIVATE_MASK; 
		m_petInfoInstable.petInfoInstable.skillComsumeHunger = CPetManager::GetCommonConsumeHunger();
	};

	///检测宠物是否可以学习此技能；
	bool CheckAndLearnPetSkill( EPetSkillMask petSkillFlag );

	//增加学习到的变身技能(256种当中的一种)
	bool StudyBodytran( unsigned int imagePos );

	//是否已学习过指定变身技能；
	bool IsBodytranLearned( unsigned int imagePos );	

private:
	///填充DB存盘信息，以便存盘；
	bool FillPlayerPetInfo( MGDbSaveInfo& dbSaveInfo ) ;

	bool FillPetTimer( PetTimer& petTimer );

	///使用从DB取到的信息初始化宠物；
	bool InitPlayerPetInfo( CPlayer* pOwner, const DbPetInfo* petInfo, bool isSwitchMap );

	bool InitPetTimer( const PetTimer* pPetTimer );

	bool GmGetAllPetSkill();

private:
	MGPetinfoInstable m_petInfoInstable;//宠物易变信息(只宠物主人知晓)
	MGPetAbility      m_petAbility;//宠物能力(只宠物主人知晓)
	MGPetinfoStable   m_petInfoStable;//宠物稳定信息(宠物主人及周边人知晓)

	unsigned long m_needExp;		//宠物当前级别升级所需要的经验值

	CPlayer* m_pPetOwner;				//所属于哪个玩家

	//各种时钟；
	ACE_Time_Value m_summonTimer;		//召唤时间的timer	
	ACE_Time_Value m_hungerTimer;		//用于计算召唤饥饿度的timer
	ACE_Time_Value m_aiPickTimer;		//自动拾取的timer
	ACE_Time_Value m_aiAttackTimer;		//自动攻击的timer
	ACE_Time_Value m_aiDrugsTimer;		//自动喝药的timer
	ACE_Time_Value m_aiAttackUpgradeTimer;	//自动攻击升级版的Timer
	ACE_Time_Value m_aiRepairEquipTimer;	//自动修理装备的Timer
	ACE_Time_Value m_aiMpTimer;
};

#endif
