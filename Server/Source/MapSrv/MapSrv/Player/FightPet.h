﻿/*
战斗宠
by dzj, 10.09.15
*/


#ifndef FIGHTPET_H
#define FIGHTPET_H

#include "../monster/monsterbase.h"

class CPlayer;	//声明
class FightPetItemExtInfo;

class CFightPet
{
	friend class CPlayer;

	CFightPet( const CFightPet& inFightPet );  //屏蔽这两个操作；
	CFightPet& operator = ( const CFightPet& inFightPet );//屏蔽这两个操作；

public:
	CFightPet(void);
	~CFightPet(void);

public:
	///使用战斗宠道具初始化宠物信息
	bool FightPetFromItem( FightPetItemExtInfo* petItemExtInfo );

	///收回战斗宠(反激活)
	bool CloseFightPet();

	///战斗宠状态变化时，通知周围人此变化信息；
	void NotifySurFightPetInfo( bool isOnlySelf /*是否只通知自己*/ );

	///更新宠物对应的道具信息
	void UpdateResFightPetItem();

public:
	unsigned int GetFightPetID()
	{
		return m_petInfoPublic.selfID;
	};

private:
	///重置战斗宠相关属性
	void FightPetReset();

	//获得用于向周围人广播的战斗宠信息；
	MGFightPetPublic* GetSurFightPetNotiInfo();

	//通知战斗宠主人，战斗宠的完整信息；
	bool NotifySelfFightPetInfo();

	//通知战斗宠主人，战斗宠不稳定信息改变
	void NotifyFightPetInstableInfo();

	//通知战斗宠主人，战斗宠稳定信息改变
	void NotifyFightPetStableInfo();

private:
	MGFightPetPrivate  m_petInfoPrivate;//战斗宠私有信息(只宠物主人知晓)
	MGFightPetPublic   m_petInfoPublic;//战斗宠公共信息(宠物主人及周边人知晓)
	CPlayer* m_pPetOwner;				//战斗宠主人
	FightPetItemExtInfo*    m_resExtItemInfo;//相应道具的额外信息

	////////////////////////////////////////////////////////////////////////////////////////////
	/*                          以下从原CMonster下移的部分接口                                */
	////////////////////////////////////////////////////////////////////////////////////////////

///////////////////初始化/////////////////////////////////////////
public:

	//对象池的初始化
	PoolFlagDefine() {}

	//初始化怪物的战斗信息
	void InitTempInfo();

public:
	//生成怪物的时候的初始化！
	bool InitFightPet( unsigned long dwFightPetClsID );
///////////////////初始化////////////////////////////////////////

//////////////////////////////获取设置属性////////////////////////
public:
	///取怪物类别号；
	unsigned long GetClsID();

	void SetPos( int posX, int posY );

	const PlayerID GetCreatureID() ;

	bool GetPos( TYPE_POS& posX, TYPE_POS& posY );

	unsigned short GetPosX();
	unsigned short GetPosY();

	///取怪物HP；
	unsigned long GetHp();

	void SetHp( unsigned long monsterHp );;

	inline CNewMap* GetMap();

	unsigned long GetMapIDOfMonster();

	//引用自身的对象管理
	void SetDelFlag();
	void ClearDelFlag();
	bool IsToDel();

	bool IsToDelAll();//这两个函数再看看什么意思；
	void SetDelAllFlag(bool m_b);//这两个函数再看看什么意思；


//////////////////怪物逻辑////////////////////
public:
	///怪物减血；注!!!!OnMonsterDie会让自身失效！！！！！！！
	unsigned long SubMonsterHp( int inHp, CPlayer* pKiller/*杀NPC者*/, bool bCreatedByDebuff /*是否为DEBUFF引起的伤害*/, int reactTime );

	unsigned long AddHp(unsigned int addHp);

public:
	///快时钟到达时执行的操作；
	void FastTimerProc();

	///慢时钟到达时执行的操作；
	void SlowTimerProc();

public:
	bool IsMonsterMoving();

	///取目标点浮点坐标X；
    float GetFloatTargetX();
	///取目标点浮点坐标Y；
	float GetFloatTargetY();

	///取怪物移动目标点X，如果怪物静止，则返回怪物当前点位置；
	unsigned short GetTargetX();

	///取怪物移动目标点Y，如果怪物静止，则返回怪物当前点位置；
	unsigned short GetTargetY();

	void SetSpeed( float inSpeed );

	///取怪物移动速度，如果怪物静止，则返回0；
	float GetSpeed();

	void SetMonsterMovInfo( unsigned short inTargetGridX, unsigned short inTargetGridY );

	void ResetMonsterMovInfo();

public:
	///////////////////////////////////////////////////////////////////
	///取NPC相关的一些属性，by dzj, 09.03.27,代码规范，公有成员去除...
	inline int GetAttackInterval();

	inline float GetNpcResistFaint();

	inline float GetResistBondage();
	///...取NPC相关的一些属性，by dzj, 09.03.27,代码规范，公有成员去除；
	///////////////////////////////////////////////////////////////////


	 //获取怪物等级 08.3.09 hyn
	 unsigned long GetLevel(void);

	 //获取目标集
	 unsigned int GetTargetSet();
	
	 //获得怪物的经验值 03.20 hyn
	 unsigned long GetExp(void);

	 unsigned short GetNpcType();

	  //获取怪物的物理防御
	 int GetPhyDef( void );

	 //获取怪物的魔法防御
	 int GetMagDef( void );

	 unsigned short GetRank(void);

	 //是否BOSS怪物
	 bool IsBossMonster();

	 //获取怪物的物理回避
	 float GetPhyDodge( void );

	 //获取怪物的魔法回避
	 float GetMagDodge( void);

	 MONSTER_STAT GetState(); //NEWFSM
	 void SetState( const MONSTER_STAT& inState); //NEWFSM
	 
	unsigned long GetMonsterDropSetID(void);//调落集；

	bool IsCanBeAttacked();

	void SetCanBeAttacked( bool bAttackable );

	bool IsCanBeAttackedPvc();

	void SetCanBeAttackedPvc( bool bAttackable );

	void SetRewardInfo( const PlayerID& rewardID );

	void ClearRewardInfo();

	const RewardInfo& GetRewardInfo(void);

	unsigned short GetRaceAttribute();//来自npcprop，

	//怪物死亡
	void OnMonsterDie( CPlayer* pKiller/*杀NPC者*/, GCMonsterDisappear::E_DIS_TYPE disType=GCMonsterDisappear::M_DIE, bool isDropItem = true );

private:
	inline void SetMonsterDieFlag();
	inline void ClearMonsterDieFlag();
public:
	inline bool IsMonsterDie();

	void OnMonsterBuffStart( BUFF_TYPE buffType, int buffVal );

	void OnMonsterBuffEnd( BUFF_TYPE buffType, int buffVal );

	//怪物受到自爆伤害
	void OnBeExplosion( CPlayer *pExplosionPlayer, CMuxSkillProp* pSkillProp, unsigned int exArg );

	///玩家与NPC对话，nOption:玩家所选择的选项，==0表示初始选择NPC；
	bool OnChatOption( CPlayer* pOwner, int nOption );

	//设置怪物是否能被攻击
	void SetAttackFlag(bool bCanBeAttacked);

	unsigned int GetMaxHp();

public:
	/////////////////////////////////////BUFF/////////////////////////////////
	bool CreateMuxBuff( MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID );
	bool CreateMuxBuffByID( unsigned int buffID, unsigned int buffTime, const PlayerID& createID );

	//新buff
	void AddMuxBuffEffect( MuxBuffProp* pBuffProp );
	void AddMuxSingleBuffEffect( MuxBuffProp* pBuffProp );
	void DelMuxBuffEffect( MuxBuffProp* pBuffProp, unsigned int buffLevel );
	void DelMuxSingleBuffEffect( MuxBuffProp* pBuffProp, unsigned int buffLevel );

	bool IsDamageRebound();

	//计算更新战斗属性
	void CaluculateTempInfo();

	//清除所有BUFF
	void ClearAllBuff();

	MonsterBuffAffect* GetMonsterBuffAffect();
	
	//关于晕眩和束缚状态的查询
	bool IsFaint();

	void SetFaintFlag(bool bFlag);

	bool IsBondage();

	void SetBondageFlag(bool bFlag);

	//伤害反弹
	void SetDamageReboundInfo( bool bFlag, unsigned int buffID );

	unsigned int GetDrArg();

	unsigned int GetAbsorbDamage();

	int GetBuffAoeRange();

	int GetBuffPhyCriDamAdd();

	int GetBuffMagCriDamAdd();

	float GetAddPhyDamageRate();

	float GetAddMagDamageRate();

	float GetAddPhyCriDmgRate();

	float GetAddMagCriDmgRate();

	float GetPhyJoukRate();

	float GetMagJoukRate();

	//是否变羊
	bool IsSheep();

	//是否睡眠
	bool IsInSleep();

	int GetMonsterSize();

	void AddBuffNum();

	void SubBuffNum();
	
	bool IsHaveBuff();

	MGMonsterInfo* GetMonsterInfo();

	MGMonsterMove* GetMonsterMove();

public:
	void SetDropItemFlag( bool bFlag );
	bool IsDropItem();

#ifdef ITEM_NPC
	public:
		bool IsItemNpc();

		unsigned int GetItemNpcSeq();

		void SetItemNpcFlag( bool isItemNpc );

		void SetItemNpcSeq( unsigned int itemNpcSeq );
#endif //ITEM_NPC

public:
	bool NeedNotiOtherMonster();

public:
	unsigned int GetIsAggressive();

	void BuffProcess();

	const PlayerInfo_5_Buffers& GetBuffData();

	bool RelieveBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve/*是否只是探测可能性*/ );

public:
	void NotifyDamageRebound( unsigned int skillID, unsigned int reboundDamage, EBattleFlag btFlag );

	////////////////////////////////////////////////////////////////////////////////////////////
	/*                          以上从原CMonster下移的部分接口                                */
	////////////////////////////////////////////////////////////////////////////////////////////

}; //class CFightPet

//玩家战斗宠相关存盘信息，存为玩家FullPlayerInfo的一部分；
class PlayerFightPetDBInfo
{
	static const unsigned int FP_MAX_EQUIPNUM = 5;//战斗宠最大可装备数目；

public:
	//取当前激活的宠物ItemInfo_i;
	ItemInfo_i* GetActiveItem() const;

public:
	//玩家在包裹位置itemPkgPos与装备位置equipPos之间移动战斗宠道具;
	bool ExchangeFightPetItem( unsigned int itemPkgPos, unsigned int itemID, unsigned int equipPos, bool isEquip/*true:装备,false:卸载*/ );
	//选择某个战斗宠道具;
	bool SelectFightPetItem( unsigned int equipPos, unsigned int itemID );
	//在激活/非激活状态之间切换;
	bool ToggleActive( bool isActive );

private:
	bool         m_isPetActived;//所选宠物是否激活；
	unsigned int m_selectPet;//所选中的战斗宠；
	ItemInfo_i   m_equipedPetItem[FP_MAX_EQUIPNUM];//所装备的战斗宠道具
};//class PlayerFightPet

#endif //FIGHTPET_H
