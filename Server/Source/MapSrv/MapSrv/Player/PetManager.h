﻿
#ifndef PET_MANAGER_H
#define PET_MANAGER_H

#include <map>
#include "Utility.h"
#include "PkgProc/SrvProtocol.h"
using namespace std;

typedef struct StrPetSkillInfo
{
	unsigned long skillID;
	unsigned long skillNeedLevel;			             
	unsigned long skillNeedMoney;			//技能需要的金钱
	unsigned long skillComsumeHunger;		//技能消耗的饥饿度
	int consumeMethod;						//消耗的方式
}PetSkillInfo;

class CElement;
class CPlayer;

class CPetManager
{

public:
	static bool Init();
	static bool FindLevelNeedExp( unsigned long level, unsigned long& needExp );		//通过等级查找需要升级的经验

	static UINT GetMaxPetLevel();		//获得宠物最大的等级
	static UINT GetMaxPetHungerDegree();	//获得最大的宠物饥饿度
	static UINT GetCommonConsumeHunger();
	static UINT GetNotGetExpHunger();		//不获得经验的饥饿平衡点 
	static UINT GetImageSkillPerHunger();	//每次使用变身技能需要消耗的饥饿度

	///召唤减饥饿度；
	static bool SummonSubHunger( UINT& inHunger );

	///使用技能扣除宠物饥饿度，如果饥饿度足够扣，则返回成功，否则返回失败；
	static bool SubSkillUseHunger( EPetSkillMask petSkillFlag, UINT& toSubDegree );	
	static bool GetSkillFlagConsumeHunger( EPetSkillMask petSkillFlag, UINT& consumeHunger );

	///宠物是否可以学习指定技能；
	static bool GetPetSkillLearnCond( EPetSkillMask petSkillFlag, UINT& needLevel, UINT& needMoney );

private:
	CPetManager(void);		//静态类屏蔽
	~CPetManager(void);

private:
	static bool LoadPetXml(const char* szFilePath );		//读取XML
	static bool IsXMLValValid( CElement* pEle, const char* attName, int nRecord );

private:
	static map<unsigned long, unsigned long> m_mapLevelNeedExp;				//每一级升级需要的宠物经验
	static map<unsigned long, PetSkillInfo> m_mapSkillInfo;			//每学一个宠物技能需要的宠物等级
	static UINT m_maxLevel;			//最大等级
	static UINT m_maxHungerDegree;		//最大宠物饥饿度
	static UINT m_commonConsumeHunger;	//每2分钟消耗的饥饿度
	static UINT m_notGetExpHunger;		//不增加经验的饥饿度界限
	static UINT m_imageSkillPerHunger;	//每次变身技能使用的饥饿度
};

#endif //PET_MANAGER_H

