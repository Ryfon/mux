﻿#include "MuxPet.h"
#include "Player.h"

#include "../MuxMap/muxmapbase.h"
#include "../LogManager.h"

CMuxPet::CMuxPet(void)
{
	PetInfoReset();
}

CMuxPet::~CMuxPet(void)
{
}

///重置宠物相关属性
void CMuxPet::PetInfoReset()
{
	StructMemSet( m_petInfoInstable, 0, sizeof(m_petInfoInstable) );//宠物易变信息(只宠物主人知晓)
	StructMemSet( m_petAbility, 0, sizeof(m_petAbility) );//宠物能力信息
	StructMemSet( m_petInfoStable, 0, sizeof(m_petInfoStable) );//宠物稳定信息
	m_petInfoStable.petInfoStable.isNewSummoned = false;//在宠物内部永远认为是新召出，只在移动广播时置此值为false;

    m_needExp = 0;		//宠物当前级别升级所需要的经验值

	m_pPetOwner = NULL;				//所属于哪个玩家

	//以下时钟必须初始化为0，这样后续的时钟恢复与保存工作才能正常进行；
	m_summonTimer = ACE_Time_Value::zero;
	m_hungerTimer = ACE_Time_Value::zero;
	m_aiPickTimer = ACE_Time_Value::zero;
	m_aiAttackTimer = ACE_Time_Value::zero;
	m_aiDrugsTimer = ACE_Time_Value::zero;
}

bool CMuxPet::IsActivate()		//查询是否激活过宠物系统
{
	return ( m_petInfoInstable.petInfoInstable.petSkillSet & IS_ACTIVATE_MASK );
}


bool CMuxPet::PetActivate( CPlayer* pPlayer )		//初始化宠物信息
{
	if( NULL == pPlayer )
	{
		D_ERROR("CMuxPet::Intialize 所属player为空\n");
		return false;
	}

	m_pPetOwner = pPlayer;

	if ( IsActivate() )
	{
		m_pPetOwner->SendSystemChat( "您已经有了宠物，获取新宠物失败\n" );
		return false;
	}

	unsigned int tmpImagePos = pPlayer->GetSex();

	m_petInfoInstable.petInfoInstable.petExp = 0;//宠物易变信息(只宠物主人知晓)
	m_petInfoInstable.petInfoInstable.petHunger = CPetManager::GetMaxPetHungerDegree();//初始饥饿度, test,int jjj;
	m_petInfoInstable.petInfoInstable.petLevel = 1;//初始等级
	m_petInfoInstable.petInfoInstable.skillComsumeHunger = CPetManager::GetCommonConsumeHunger();

	CPetManager::FindLevelNeedExp( m_petInfoInstable.petInfoInstable.petLevel, m_needExp );		//获取初始等级所需要的经验
	ClearAllSkillUseFlag();//初始技能设置；
	StructMemSet( m_petAbility.petAbility.lookValid, 0, sizeof(m_petAbility.petAbility.lookValid) );//宠物能力(只宠物主人知晓)
	m_petAbility.petAbility.lookValidLen = ARRAY_SIZE(m_petAbility.petAbility.lookValid);
	m_petAbility.petAbility.skillValid = m_petAbility.petAbility.skillValid | AUTOMATIC_USE_MP_DRUGS_MASK;		//初始有自动攻击技能
	m_petInfoStable.petInfoStable.isSummoned = false;//宠物稳定信息(宠物主人及周边人知晓)
	m_petInfoStable.petInfoStable.lookPos = tmpImagePos;
	if( m_pPetOwner->GetSex() == 1 )
	{
		SafeStrCpy( m_petInfoStable.petInfoStable.petName, "小恶魔" );
	}else
	{
		SafeStrCpy( m_petInfoStable.petInfoStable.petName, "小天使" );
	}
	m_petInfoStable.petInfoStable.petNameLen = (UINT) strlen( m_petInfoStable.petInfoStable.petName ) + 1;
	m_petInfoStable.petInfoStable.petOwnerID = pPlayer->GetFullID();	

	StudyBodytran( tmpImagePos );//学会初始外形；
	m_petInfoStable.petInfoStable.lookPos = tmpImagePos;//初始外形设为当前外形;
	//SetPetImagePos( tmpImagePos );

	NotifySelfPetInfo();

	//更新Pet的信息到DB 
#ifdef NEW_PET_UPDATE
	UpdatePetInfoToDB();
#endif
	
	CLogManager::DoPetAdd(m_pPetOwner, 0);//记日至
	return true;
}

//获得用于向周围人广播的宠物信息；
MGPetinfoStable* CMuxPet::GetSurPetNotiInfo()
{
	if ( !IsActivate() )
	{
		return NULL;
	}

	return &m_petInfoStable;
}

//宠物状态变化时，通知周围人此变化信息；
void CMuxPet::NotifySurPetInfo( bool isOnlySelf /*是否只通知自己*/ )
{
	if ( !IsActivate() )
	{
		D_WARNING( "NotifySurPetInfo，未激活宠物" );
		return;
	}

	if ( NULL == m_pPetOwner )
	{
		//宠物不属于任何玩家;
		D_ERROR( "NotifySurPetInfo，宠物所属玩家空" );
		return;
	}

	CMuxMap* pPlayerMap = m_pPetOwner->GetCurMap();
	if ( NULL == pPlayerMap )
	{
		return;
	}

	m_petInfoStable.petInfoStable.isNewSummoned = true;//内部通知永远为新召出，只有外部移动通知时才为旧宠物；
	if ( isOnlySelf )
	{
		m_pPetOwner->SendPkg< MGPetinfoStable >( &m_petInfoStable );
	} else {
		pPlayerMap->NotifySurrounding< MGPetinfoStable >( &m_petInfoStable, m_pPetOwner->GetPosX(), m_pPetOwner->GetPosY() );
	}
	m_petInfoStable.petInfoStable.isNewSummoned = false;//内部通知永远为新召出，只有外部移动通知时才为旧宠物；

	return;
}

void CMuxPet::EnableSummonState()		//设置成召唤状态
{
	if ( !IsActivate() )
	{
		D_ERROR( "允许未激活宠物的召唤状态\n" );
		return;
	}

	if ( IsSummoned() )//已经是召唤状态；
	{
		return;
	}

	if ( ! CPetManager::SummonSubHunger( m_petInfoInstable.petInfoInstable.petHunger ) )
	{
		if ( NULL != m_pPetOwner )
		{
			m_pPetOwner->SendSystemChat( "宠物饥饿度将为0，无法呼出\n" );
		}
		return;
	}

	m_petInfoStable.petInfoStable.isSummoned = true;

	ACE_Time_Value curTime = ACE_OS::gettimeofday();

	RestorePetTimer( m_summonTimer, curTime );
	RestorePetTimer( m_hungerTimer, curTime );
	RestorePetTimer( m_aiPickTimer, curTime );
	RestorePetTimer( m_aiAttackTimer, curTime );
	RestorePetTimer( m_aiDrugsTimer, curTime );

	NotifyInstableInfo();//由于饥饿度发生了变化，因此通知自身，不稳定信息改变；

	NotifySurPetInfo( false/*通知周围人*/ );

	return;
}


void CMuxPet::DisableSummonState()
{
	if ( !IsActivate() )
	{
		D_ERROR( "禁止未激活宠物的召唤状态\n" );
		return;
	}

	if ( !IsSummoned() )//已经是未召唤状态；
	{
		return;
	}

	m_petInfoStable.petInfoStable.isSummoned = false;

	ClearAllSkillUseFlag();//清玩家已设置的宠物使用技能

	ACE_Time_Value curTime = ACE_OS::gettimeofday();

	SavePetTimer( m_summonTimer, curTime );
	SavePetTimer( m_hungerTimer, curTime );
	SavePetTimer( m_aiPickTimer, curTime );
	SavePetTimer( m_aiAttackTimer, curTime );
	SavePetTimer( m_aiDrugsTimer, curTime );

	NotifySurPetInfo( false/*通知周围人*/ );

	return;
}

//设置宠物的姓名
bool CMuxPet::SetPetName( const char* petName )  
{	
	if ( !IsActivate() )
	{
		D_ERROR( "不可设置未激活宠物名字\n" );
		return false;
	}

	StructMemSet( m_petInfoStable.petInfoStable.petName, 0, sizeof( m_petInfoStable.petInfoStable.petName ) );
	SafeStrCpy( m_petInfoStable.petInfoStable.petName, petName );
	m_petInfoStable.petInfoStable.petNameLen = (UINT) strlen( m_petInfoStable.petInfoStable.petName ) + 1;

	bool isNotiOnlySelf = !IsSummoned();/*如果召当前召出来了，则不只通知自己，否则如果没召出来，则只通知自己*/
	NotifySurPetInfo( isNotiOnlySelf );

	return true;
}

//设置宠物的外形ID，值为256当中的序号；
bool CMuxPet::SetPetImagePos( TYPE_ID imagePos )		
{
	if ( NULL == m_pPetOwner )
	{
		return false;
	}

	if ( !IsActivate() )
	{
		D_ERROR( "不可设置未激活宠物外形\n" );
		return false;
	}

	//首先判断是否已学此变身技能，如果已学，则设置，否则设置失败;
	if ( ! IsBodytranLearned(imagePos) )
	{
		m_pPetOwner->SendSystemChat( "宠物尚未学会此变身技能\n" );
		return false;//尚未学习此技能；
	}

	if ( IsAnySkillInUse() )
	{
		//只要有一种基本技能在使用状态，就不可以改变宠物外观；
		m_pPetOwner->SendSystemChat( "使用基本技能时，不可设置宠物外观\n" );
		return false;
	}

	//变身需要的饥饿度是否够
	if( CPetManager::GetImageSkillPerHunger() >= GetHunger() )
	{
		m_pPetOwner->SendSystemChat("变身技能需要的饥饿度的饥饿度不足");
		return false;
	}

	m_petInfoStable.petInfoStable.lookPos = imagePos;
	if ( m_petInfoInstable.petInfoInstable.petHunger >= CPetManager::GetImageSkillPerHunger() )
	{
		m_petInfoInstable.petInfoInstable.petHunger -= CPetManager::GetImageSkillPerHunger();
	}
	else
	{
		m_petInfoInstable.petInfoInstable.petHunger = 0;
	}
	//通知饥饿度的变更
	NotifyInstableInfo();

	bool isNotiOnlySelf = !IsSummoned();/*如果召当前召出来了，则不只通知自己，否则如果没召出来，则只通知自己*/
	NotifySurPetInfo( isNotiOnlySelf );

#ifdef NEW_PET_UPDATE
	UpdatePetInfoToDB();
#endif

	return true;
}

///基本技能习得;
bool CMuxPet::StudyPetSkill( unsigned int petSkillID )
{
	if ( petSkillID >= MAX_PETSKILL_SZIE )
	{
		D_WARNING( "宠物技能号%d超出范围\n", petSkillID );
		return false;
	}
	EPetSkillMask petSkillFlag = ( EPetSkillMask ) (0x00000001 << petSkillID);

	if ( NULL == m_pPetOwner )
	{
		D_ERROR( "StudyPetSkill, 宠物所属玩家空\n" );
		return false;
	}

	if ( !IsActivate() )
	{
		D_ERROR( "玩家%s, 未激活宠物不可学习技能\n", m_pPetOwner->GetAccount() );
		return false;
	}
	if ( IsPetSkillLearned( petSkillFlag ) )
	{
		D_WARNING( "玩家%s重复学习相同的技能\n", m_pPetOwner->GetAccount() );
		m_pPetOwner->SendSystemChat("已经拥有该技能");
		return false;
	}

	if( !CheckAndLearnPetSkill( petSkillFlag ))
		return false;

	CLogManager::DoPetGainSkill(m_pPetOwner, 0, petSkillID);//记日至
	return true;
}

///检测宠物是否可以学习此技能；
bool CMuxPet::CheckAndLearnPetSkill( EPetSkillMask petSkillFlag )
{
	if ( NULL == m_pPetOwner )
	{
		D_ERROR( "CheckAndLearnPetSkill, 宠物所属玩家空\n" );
		return false;
	}

	UINT needLevel = 99999;
	UINT needMoney = 0xfffffff;
	if ( !CPetManager::GetPetSkillLearnCond( petSkillFlag, needLevel, needMoney ) )
	{
		D_ERROR( "CheckAndLearnPetSkill，找不到学习技能%d所需的信息\n", (int)petSkillFlag );
		m_pPetOwner->SendSystemChat( "非法技能，不可学习\n" );
		return false;
	}
	if ( m_petInfoInstable.petInfoInstable.petLevel < needLevel )
	{
		m_pPetOwner->SendSystemChat( "您的宠物等级不够，不可学习此技能\n" );
		return false;
	}

#ifndef PET_SKILL_SHOP //使用技能商店后，在商店里进行此类判断；
	if ( m_pPetOwner->IsUseGoldMoney() )
	{
		//使用金币
		if ( m_pPetOwner->GetGoldMoney() < needMoney )
		{
			m_pPetOwner->SendSystemChat( "您的金币不够，不可学习此技能\n" );
			return false;
		}
		m_pPetOwner->SubGoldMoney( needMoney );
	} else {
		//使用银币
		if ( m_pPetOwner->GetSilverMoney() < needMoney )
		{
			m_pPetOwner->SendSystemChat( "您的银币不够，不可学习此技能\n" );
			return false;
		}
		m_pPetOwner->SubSilverMoney( needMoney );
	}
#endif //PET_SKILL_SHOP

	m_petAbility.petAbility.skillValid |= petSkillFlag;	

	NotifyAbilityInfo();

	//更新PetInfo信息到 db
#ifdef NEW_PET_UPDATE
	UpdatePetInfoToDB();
#endif

	return true;
}

//增加学习到的变身技能(256种当中的一种)
bool CMuxPet::StudyBodytran( unsigned int imagePos )
{
	if ( NULL == m_pPetOwner )
	{
		D_ERROR( "StudyBodytran, 宠物所属玩家空\n" );
		return false;
	}

	if ( !IsActivate() )
	{
		D_ERROR( "未激活宠物不可学习变身技能\n" );
		return false;
	}

	if ( IsBodytranLearned( imagePos ) )
	{
		//已学会此技能，无需再学习
		m_pPetOwner->SendSystemChat( "已学会此变身技能，无需再学\n" );
		return false;
	}

	unsigned int charpos = 0, bitpos = 0;
	if ( !GetUnitBitPos( imagePos, 8, ARRAY_SIZE(m_petAbility.petAbility.lookValid), charpos, bitpos ) )
	{
		return false;
	}
	//将相应位置为已学；
	if ( charpos >= ARRAY_SIZE(m_petAbility.petAbility.lookValid) )
	{
		return false;
	}

	m_petAbility.petAbility.lookValid[charpos] |= (0x01 << bitpos);

	NotifyAbilityInfo();

	return true;
}

//是否已学习过指定变身技能；
bool CMuxPet::IsBodytranLearned( unsigned int imagePos )
{
	if ( !IsActivate() )
	{
		D_ERROR( "未激活宠物无变身技能\n" );
		return false;
	}

	unsigned int charpos = 0, bitpos = 0;
	if ( !GetUnitBitPos( imagePos, 8, ARRAY_SIZE(m_petAbility.petAbility.lookValid), charpos, bitpos ) )
	{
		return false;
	}
	if ( charpos >= ARRAY_SIZE(m_petAbility.petAbility.lookValid) )
	{
		return false;
	}

	return ( 0x00 != (m_petAbility.petAbility.lookValid[charpos] & (0x01<<bitpos)) );	
}


//唤出宠物
bool CMuxPet::SummonPet()						
{
	if ( NULL == m_pPetOwner )
	{
		D_ERROR( "SummonPet, 宠物所属玩家空\n" );
		return false;
	}

	if( !IsActivate() )
	{
		//未激活，不能召唤；
		D_WARNING( "SummonPer, 召唤未激活宠物，玩家%s\n", m_pPetOwner->GetAccount() );
		m_pPetOwner->SendSystemChat( "您尚未获得宠物\n" );
		return false;
	}

	if ( IsSummoned() ) //已召唤；
	{
		return true;
	}

	if( m_petInfoInstable.petInfoInstable.petHunger <= 0 )  //饥饿度为0时不能召唤
	{
		D_WARNING( "SummonPer, 召唤饥饿宠物，玩家%s\n", m_pPetOwner->GetAccount() );
		m_pPetOwner->SendSystemChat( "宠物饥饿度为0，无法呼出\n" );
		return false;					
	}

	EnableSummonState();

	CLogManager::DoPetStateChange(m_pPetOwner, 0, LOG_SVC_NS::PST_ACTIVE);//记日至

	return true;
}


//唤回宠物
bool CMuxPet::ClosePet()						
{
	if ( NULL == m_pPetOwner )
	{
		D_ERROR( "ClosePet, 宠物所属玩家空\n" );
		return false;
	}

	if( !IsActivate() )
	{
		//未激活，不能召回；
		D_WARNING( "ClosePet, 召回未激活宠物，玩家%s\n", m_pPetOwner->GetAccount() );
		return false;
	}

	DisableSummonState();

	CLogManager::DoPetStateChange(m_pPetOwner, 0, LOG_SVC_NS::PST_SLEEP);//记日至

	return true;
}

//增加经验
void CMuxPet::PetExpAdd( unsigned long addExp )	
{
	if ( NULL == m_pPetOwner )
	{
		D_ERROR( "CMuxPet::PetExpAdd, 宠物所属玩家空\n" );
		return;
	}

	if( !IsActivate() )
	{
		//未激活，不能召唤；
		D_WARNING( "CMuxPet::PetExpAdd, 未激活宠物不可加经验，玩家%s\n", m_pPetOwner->GetAccount() );
		return;
	}

	//如果所需要升级经验为0
	if( m_needExp == 0 )
	{
		D_INFO("宠物下级经验为0,跳出\n");
		return;
	}

	if( m_petInfoInstable.petInfoInstable.petExp + addExp > m_needExp )
	{
		if ( (m_petInfoInstable.petInfoInstable.petLevel) >= m_pPetOwner->GetLevel() )
		{
			//宠物等级不可以大于人物等级；
			D_INFO( "玩家%s的宠物等级已与人物等级相当，无法再升级\n", m_pPetOwner->GetAccount() );
			m_petInfoInstable.petInfoInstable.petExp = m_needExp;
			return;
		}
		++ (m_petInfoInstable.petInfoInstable.petLevel);
		m_petInfoInstable.petInfoInstable.petExp = 0;

		bool bRet = CPetManager::FindLevelNeedExp( m_petInfoInstable.petInfoInstable.petLevel, m_needExp );
		if( !bRet )
		{
			m_needExp = 0;
		}

		NotifyInstableInfo();

#ifdef NEW_PET_UPDATE
		UpdatePetInfoToDB();
#endif

		CLogManager::DoPetLevelUp(m_pPetOwner, 0, m_petInfoInstable.petInfoInstable.petLevel -1, m_petInfoInstable.petInfoInstable.petLevel);//记日至
		return;
	}

	m_petInfoInstable.petInfoInstable.petExp += addExp;

	return;
}


bool CMuxPet::SetHunger( unsigned long setHunger)
{
	if( setHunger > 999 )
	{
		return false;
	}
	m_petInfoInstable.petInfoInstable.petHunger = setHunger;
	return true;
}


UINT CMuxPet::GetHunger()			//获取饥饿度
{
	return 	m_petInfoInstable.petInfoInstable.petHunger;
}


bool CMuxPet::FeedPet( unsigned long foodNum )//给宠物喂食;
{
	if ( NULL == m_pPetOwner )
	{
		D_ERROR( "CMuxPet::PetExpAdd, 宠物所属玩家空\n" );
		return false;
	}

	if( !IsActivate() )
	{
		//未激活，不能召唤；
		D_WARNING( "CMuxPet::PetExpAdd, 未激活宠物不可加经验，玩家%s\n", m_pPetOwner->GetAccount() );
		m_pPetOwner->SendSystemChat( "无法使用，您还未获得宠物\n" );
		return false;
	}

	unsigned int maxHunger = CPetManager::GetMaxPetHungerDegree();
	if ( m_petInfoInstable.petInfoInstable.petHunger >= maxHunger )
	{
		m_pPetOwner->SendSystemChat( "宠物饥饿度满，无法使用宠物食品\n" );
		return false; 
	}

	m_petInfoInstable.petInfoInstable.petHunger += foodNum;
	if ( m_petInfoInstable.petInfoInstable.petHunger > maxHunger )
	{
		m_petInfoInstable.petInfoInstable.petHunger = maxHunger;
	}

	NotifyInstableInfo();
	return true;
}

void CMuxPet::OnPlayerSwitchMap()
{
	if ( !IsActivate() )
	{
		return;
	}

	if ( !IsSummoned())
	{
		return;
	}

	//跳转地图时关闭宠物的自动攻击技能
	EPetSkillMask closeSkill = AUTOMATIC_ATTACK_MASK;
	if (IsPetSkillInUse(closeSkill))
	{
		ClearSkillUseFlag(closeSkill);
		NotifyInstableInfo();
	}
}

//设置正在使用的持续性技能
bool CMuxPet::SetPetSkillState( unsigned long newSkillState )		
{
	if ( NULL == m_pPetOwner )
	{
		D_ERROR( "CMuxPet::SetPetSkillState, 宠物所属玩家空\n" );
		return false;
	}

	if( !IsActivate() )
	{
		D_WARNING( "CMuxPet::SetPetSkillState, 未激活宠物不可设置技能，玩家%s\n", m_pPetOwner->GetAccount() );
		return false;
	}

	newSkillState &= ~IS_ACTIVATE_MASK;//清此位；

	//通过petState进行判断
	if( !(CanSkillStateSet( newSkillState )) )
	{
		D_WARNING( "SetPetSkillState，%s所设技能%d超出了宠物当前具备的技能%d\n", m_pPetOwner->GetAccount(), newSkillState, m_petAbility.petAbility.skillValid );
		m_pPetOwner->SendSystemChat( "所设置的宠物使用技能超出了您宠物的能力范围，设置失败\n" );
		return false;
	}
	
	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	//注释此句，以便后续的SkillSetCheckOneFlag能够使用旧的技能设置进行判断，ClearAllSkillUseFlag();//重置技能使用状态；

	SkillSetCheckOneFlag( newSkillState, AUTOMATIC_ATTACK_MASK, curTime, m_aiAttackTimer );

	SkillSetCheckOneFlag( newSkillState, AUTOMATIC_USE_HP_DRUGS_MASK, curTime, m_aiDrugsTimer );

	SkillSetCheckOneFlag( newSkillState, AUTOMATIC_PICK_MASK, curTime, m_aiPickTimer );

	SkillSetCheckOneFlag( newSkillState, AUTOMATIC_REPAIR_WEAPON, curTime, m_aiRepairEquipTimer );

	SkillSetCheckOneFlag( newSkillState, AUTOMATIC_LEVELUP_ATTACK_MASK, curTime, m_aiAttackUpgradeTimer );
	SkillSetCheckOneFlag( newSkillState, AUTOMATIC_USE_MP_DRUGS_MASK, curTime, m_aiMpTimer );
	NotifyInstableInfo();

	return true;
}

///是否可以设置此技能状态（输入参数中的设置位必须为已学习技能）
bool CMuxPet::CanSkillStateSet( unsigned long skillState ) 
{ 
	if ( NULL == m_pPetOwner )
	{
		return false;
	}

	if ( IS_ACTIVATE_MASK != skillState )
	{
		//if ( IsTransLook() )
		//{
		//	//变身技能下任何基本技能都不可使用；
		//	m_pPetOwner->SendSystemChat( "变身状态下不可使用基本技能\n" );
		//	return false;
		//}

		if ( ( skillState&WEAPON_LEVELUP_MASK ) && ( skillState!=WEAPON_LEVELUP_MASK ) )
		{
			//武器升级技能只能单独使用；
			m_pPetOwner->SendSystemChat( "武器升级技能只能单独使用\n" );
			return false;
		}
		if ( ( skillState&OFFLINE_LEVELUP_MASK ) && ( skillState!=OFFLINE_LEVELUP_MASK ) )
		{
			//离线升级技能只能单独使用；
			m_pPetOwner->SendSystemChat( "离线升级技能只能单独使用\n" );
			return false;
		}
	}
	return ( ( m_petAbility.petAbility.skillValid & skillState ) == skillState ); 
}

///设置宠物技能时，检测处理，返回值：是否已设置使用该技能，无论是早先的设置，还是本次的设置；
bool CMuxPet::SkillSetCheckOneFlag( unsigned long skillState, EPetSkillMask petSkillFlag, const ACE_Time_Value& curTime, ACE_Time_Value& skillStTime/*技能使用开始时间*/ )
{
	if ( NULL == m_pPetOwner )
	{
		return false;
	}
	if (skillState & petSkillFlag)
	{
		//新设置中包含了此技能；
		if ( IsPetSkillInUse( petSkillFlag ) )
		{
			//新设置与原设置都有使用，不作任何处理
			return true;
		} else {
			if ( !IsPetSkillLearned( petSkillFlag ) )
			{
				m_pPetOwner->SendSystemChat( "还未学会此宠物技能，不可使用\n" );
				return false;
			}
			//新设置使用，原设置没有使用，扣饥饿度，同时设置技能开始使用时刻;
			if ( CPetManager::SubSkillUseHunger( petSkillFlag, m_petInfoInstable.petInfoInstable.petHunger ) )
			{
				skillStTime = curTime;
				SetSkillUseFlag( petSkillFlag );
				return true;
			} else {
				D_WARNING( "SetPetSkillState，%s使用宠物技能%d，宠物饥饿度太低，使用失败\n", m_pPetOwner->GetAccount(), petSkillFlag );
				m_pPetOwner->SendSystemChat( "宠物饥饿度太低，技能未能全部使用成功\n" );
				return false;
			}
		}
	} else {
		//新设置中不包含此技能；
		if ( IsPetSkillInUse( petSkillFlag ) )
		{
			//新设置没有使用，但原设置有使用，停用之；
			ClearSkillUseFlag( petSkillFlag );
			skillStTime = ACE_Time_Value::zero;
		} else {
			//新设置与原设置都没有使用，不作任何处理；
		}
		return false;
	}
}



//通知宠物主人宠物完整信息；
bool CMuxPet::NotifySelfPetInfo()
{
	if ( !IsActivate() )
	{
		return true;//没有宠物，无需通知；
	}

	NotifyAbilityInfo();
	NotifyInstableInfo();
	NotifyStableInfo();

	return true;
}

void CMuxPet::UpdatePetInfoToDB()
{
	if ( m_pPetOwner )
	{
		MGDbSaveInfo saveInfo;
		saveInfo.playerID = m_pPetOwner->GetFullID();
		m_pPetOwner->FillPlayerPetInfo( saveInfo );
		m_pPetOwner->ForceGateSend<MGDbSaveInfo>( &saveInfo );
	}
}


//通知宠物主人不稳定信息改变
void CMuxPet::NotifyInstableInfo()					
{
	if( NULL == m_pPetOwner )
	{
		D_ERROR("CMuxPet::NotifyInstableInfo m_pPetOwner为空\n");
		return;
	}

	m_pPetOwner->SendPkg<MGPetinfoInstable>( &m_petInfoInstable );
}

//通知宠物主人宠物能力改变
void CMuxPet::NotifyAbilityInfo()					
{
	if( NULL == m_pPetOwner )
	{
		D_ERROR("CMuxPet::NotifyAbilityInfo m_pPetOwner为空\n");
		return;
	}

	m_pPetOwner->SendPkg<MGPetAbility>( &m_petAbility );
}

//通知宠物主人稳定信息改变
void CMuxPet::NotifyStableInfo()					
{
	if( NULL == m_pPetOwner )
	{
		D_ERROR("CMuxPet::NotifyStableInfo m_pPetOwner为空\n");
		return;
	}

	//已召出，则通知周围人；
	CMuxMap* pMap = m_pPetOwner->GetCurMap();
	if( NULL == pMap )
	{
		D_ERROR("CMuxPet::NotifyStableInfo pMap为空\n");
		return;
	}
	m_petInfoStable.petInfoStable.isNewSummoned = true;//内部通知永远为新召出，只有外部移动通知时才为旧宠物；
	pMap->NotifySurrounding<MGPetinfoStable>( &m_petInfoStable, m_pPetOwner->GetPosX(), m_pPetOwner->GetPosY() );
	m_petInfoStable.petInfoStable.isNewSummoned = false;//内部通知永远为新召出，只有外部移动通知时才为旧宠物；
}

void CMuxPet::PetTimeProcess()			//定时调用
{
	if ( NULL == m_pPetOwner )
	{
		D_ERROR( "CMuxPet::PetTimeProcess, 宠物所属玩家空\n" );
		return;
	}

	if( !IsActivate() )
	{
		//未激活宠物，无需调用时钟；
		return;
	}

	if ( !IsSummoned() )
	{
		//未召唤宠物，无需调用时钟；
		return;
	}

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();

	bool isNeedNotify = false;

	if( currentTime.sec() - m_summonTimer.sec() >= 600 )
	{
		if( m_petInfoInstable.petInfoInstable.petHunger >= CPetManager::GetNotGetExpHunger() )				//饥饿度大于配置才能增加经验
		{
			PetExpAdd( 1 );
			m_summonTimer += ACE_Time_Value( 600 );
			isNeedNotify = true;
		}
	}
	
	//看是否到了summon的2分钟,每2分钟消耗1点饥饿度
	if ( currentTime.sec() - m_hungerTimer.sec() >= 120 )
	{
		isNeedNotify = true;
		if ( CPetManager::SummonSubHunger( m_petInfoInstable.petInfoInstable.petHunger ) )
		{
			m_hungerTimer += ACE_Time_Value( 120 );
		} else {
			ClosePet();		
		}
	}

	SkillUseTimeCheck( AUTOMATIC_PICK_MASK, 120, currentTime, m_aiPickTimer, isNeedNotify );
	SkillUseTimeCheck( AUTOMATIC_ATTACK_MASK, 120, currentTime, m_aiAttackTimer, isNeedNotify );
	SkillUseTimeCheck( AUTOMATIC_USE_HP_DRUGS_MASK, 120, currentTime, m_aiDrugsTimer, isNeedNotify );
	SkillUseTimeCheck( AUTOMATIC_REPAIR_WEAPON, 120, currentTime, m_aiRepairEquipTimer, isNeedNotify );
	SkillUseTimeCheck( AUTOMATIC_LEVELUP_ATTACK_MASK, 120, currentTime, m_aiAttackUpgradeTimer, isNeedNotify );

	if ( isNeedNotify )
	{
		NotifyInstableInfo();
	}

	return;
}

///填充DB存盘信息，以便存盘；
bool CMuxPet::FillPlayerPetInfo( MGDbSaveInfo& dbSaveInfo ) 
{ 
	dbSaveInfo.infoType = DI_PET;
	StructMemCpy( (dbSaveInfo.petInfo.petInfoInstable), &(m_petInfoInstable), sizeof(dbSaveInfo.petInfo.petInfoInstable) );
	StructMemCpy( (dbSaveInfo.petInfo.petInfoAbility), &(m_petAbility), sizeof(dbSaveInfo.petInfo.petInfoAbility) );
	StructMemCpy( (dbSaveInfo.petInfo.petInfoStable), &(m_petInfoStable), sizeof(dbSaveInfo.petInfo.petInfoStable) );
	return true; 
};


bool CMuxPet::FillPetTimer( PetTimer& petTimer )
{
	//传输每个状态持续了多少时间 
	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	petTimer.summonTimer = (unsigned int)( currentTime - m_summonTimer ).sec();		
	petTimer.aiAttackTimer = (unsigned int)( currentTime - m_aiAttackTimer ).sec();
	petTimer.aiDrugsTimer = (unsigned int)( currentTime - m_aiDrugsTimer ).sec();
	petTimer.aiPickTimer = (unsigned int)( currentTime - m_aiPickTimer ).sec();
	petTimer.hungerTimer = (unsigned int)( currentTime - m_hungerTimer ).sec();
	petTimer.aiRepairEquipTimer = (unsigned int)( currentTime - m_aiRepairEquipTimer).sec();
	petTimer.aiAttackUpgradeTimer = (unsigned int)( currentTime - m_aiAttackUpgradeTimer).sec();
	return true;
}

///使用从DB取到的信息初始化宠物；
bool CMuxPet::InitPlayerPetInfo( CPlayer* pOwner, const DbPetInfo* petInfo, bool isSwitchMap )
{	
	if ( NULL == pOwner )
	{
		return false;
	}

	StructMemCpy( m_petInfoInstable, &(petInfo->petInfoInstable), sizeof(m_petInfoInstable) );
	StructMemCpy( m_petAbility, &(petInfo->petInfoAbility), sizeof(m_petAbility) );
	StructMemCpy( m_petInfoStable, &(petInfo->petInfoStable), sizeof(m_petInfoStable) );
	
	if ( !isSwitchMap )
	{
		m_petInfoStable.petInfoStable.isSummoned = false;
	}

	m_petInfoStable.petInfoStable.isNewSummoned = false;

	//部分需要上线重置的信息；
	m_pPetOwner = pOwner;
	m_petInfoStable.petInfoStable.petOwnerID = m_pPetOwner->GetFullID();
	if( !CPetManager::FindLevelNeedExp( m_petInfoInstable.petInfoInstable.petLevel, m_needExp ) )		//获取初始等级所需要的经验
	{
		m_needExp = 0;
	}

	//由于跳地图时需要保存此信息，因此改为下线时召回。m_petInfoStable.petInfoStable.isSummoned = false;

	return true;
}


bool CMuxPet::InitPetTimer( const PetTimer* pPetTimer )
{
	if( NULL == pPetTimer )
		return false;
	
	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	m_summonTimer = m_hungerTimer = m_aiPickTimer = m_aiDrugsTimer = m_aiAttackTimer = currentTime;

	m_summonTimer -= pPetTimer->summonTimer;
	m_hungerTimer -= pPetTimer->hungerTimer;
	m_aiPickTimer -= pPetTimer->aiPickTimer;
	m_aiDrugsTimer -= pPetTimer->aiDrugsTimer;
	m_aiAttackTimer -= pPetTimer->aiAttackTimer;
	m_aiRepairEquipTimer -= pPetTimer->aiRepairEquipTimer;
	m_aiAttackUpgradeTimer -= pPetTimer->aiAttackUpgradeTimer;
	return true;
}


bool CMuxPet::GmGetAllPetSkill()  //GM调试用
{
	m_petAbility.petAbility.skillValid = 0;
	m_petAbility.petAbility.skillValid |= IS_ACTIVATE_MASK;
	m_petAbility.petAbility.skillValid |= AUTOMATIC_USE_MP_DRUGS_MASK;
	m_petAbility.petAbility.skillValid |= AUTOMATIC_ATTACK_MASK;
	m_petAbility.petAbility.skillValid |= AUTOMATIC_USE_HP_DRUGS_MASK;
	m_petAbility.petAbility.skillValid |= AUTOMATIC_PICK_MASK;
	m_petAbility.petAbility.skillValid |= WEAPON_LEVELUP_MASK;
	m_petAbility.petAbility.skillValid |= OFFLINE_LEVELUP_MASK;
	m_petAbility.petAbility.skillValid |= AUTOMATIC_REPAIR_WEAPON;
	m_petAbility.petAbility.skillValid |= AUTOMATIC_LEVELUP_ATTACK_MASK;

	NotifyAbilityInfo();
	return true;
}

