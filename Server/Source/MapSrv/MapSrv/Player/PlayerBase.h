﻿/*
*
* @file CPlayer.h
* @brief 定义玩家信息
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: CPlayer.h
* 摘    要: 定义玩家有关信息
* 作    者: hyn
* 完成日期: （待扩展）
*
*/
#ifndef PLAYER_BASE_H
#define PLAYER_BASE_H

#include "tcache/tcache.h"
#include "PkgProc/PacketBuild.h"
#include "../G_MProc.h"
#include "../Fsm/IFsm.h"
#include "../PoolManager.h"
#include "../BuffProp.h"

#include "../TradeInfo.h"
#include "../SpSkillManager.h"
#include "../ScampBuff.h"
#include "../MuxMap/MapCommonHead.h"
#include "../MuxMap/mapproperty.h"

#include "../plate/plate.h"

#include "../Item/SuitDelegate.h"
#include "../Item/ItemDelegate.h"
#include "../Item/Storage.h"
#include "../Item/ProtectItemPlate.h"
#include "../ChatInfo.h"
#include "../GoodEvilControl.h"
#include "../TaskRecordManager.h"
#include "../TaskIndexManager.h"
#include "../Item/NormalTask.h"
#include "../PlayerStateManager.h"
#include "../OLTestPlate.h"

#include "../creature.h"
#include "../CooldownManager.h"
#include "MuxPet.h"
#include "../Union/UnionManager.h"
#include "../BattleUtility.h"
#include "../RacePrestigeManager.h"

#include "../PlayerBuffManager.h"
#ifdef ANTI_ADDICTION
	#include "../AntiAddictionManager.h"
#endif
#include "../Activity/PunishTask.h"

#include "../MuxPlayerSkill.h" 
#include "../LogManager.h"
#include "../Item/AuctionManager.h"
#include "../EmbedDiamond.h"
#include "../CSortItem.h"
#include "../CastManager.h"

extern CPlayer* g_debugPlayer;

void EQUIP_ITEM_MASK( char& levelInfo );
char GET_ITEM_LEVELUP( char levelInfo );
void RESET_ITEM_LEVEL( char& levelInfo);
void SET_ITEM_LEVEL( char& levelInfo, unsigned level ) ;
bool ITEM_IS_EQUIPED( char levelInfo );

class CFightPet;
class CMonster;
class CBaseItem;
class CClassLevelProp;
class CItemSkill;
class CMuxPet;
class CWarTask;

class CLevelUpScript;

class CMuxCopyInfo;

#define LATEST_MOVE_NOTI //使用最迟移动通知；
#define MAX_MONEY 2100000000   //最大金钱最多为21亿


#define EVIL_UPDATE_TIME 3600//每1小时变化一点 

#define NORMAL_PKG_SIZE 88
#define NEW_SKILL_UPDATE 

#ifdef PRINT_PKGNUM //测试集聚情形；
		//统计发包数量；
       extern unsigned int g_playerInfoNum;
	   extern unsigned int g_monsterInfoNum;
#endif //PRINT_PKGNUM //测试集聚情形；


	  struct RestSkillInfo
	  {
		  RestSkillInfo()
		  {
			  clear();
		  }	
		
		  void clear()
		  {
			  isHavePassiveSkill = false;
			  hp = 0;
			  mp = 0;
			  perHp = 0.0f;
			  perMp = 0.0f;
		  }


		  bool isHavePassiveSkill;
		  unsigned int hp;
		  unsigned int mp;
		  float perHp;
		  float perMp;
	  };

	  struct PlayerSecondProp
	  {
		  PlayerSecondProp()
		  {
			  StructMemSet( *this, 0, sizeof(*this) );
		  }
		  TYPE_UI32                	nMagicAttack;                               	//魔法攻击力
		  TYPE_UI32                	nMagicDefend;                               	//魔法防御力
		  TYPE_UI32                	nPhysicsAttack;                             	//物理攻击力
		  TYPE_UI32                	nPhysicsDefend;                             	//物理防御力
		  TYPE_UI32                	nPhysicsHit;                                	//物理命中
		  TYPE_UI32                	nMagicHit;                                  	//魔法命中
		  TYPE_UI32                	nNormalState;                               	//平时状态(位的概念)
		  TYPE_UI32                	lMaxHP;                                     	//最大HP
		  TYPE_UI32                 lHpRecover;                                  //HP回复
		  TYPE_UI32                	lMaxMP;                                     	//最大MP
		  TYPE_UI32                 lMpRecover;                                  //HP回复
		  TYPE_UI32                	lNextLevelExp;                              	//升到下一级需要的经验值
		  TYPE_UI32                	lLastLevelExp;                              	//上一级升级的经验
		  TYPE_UI32                	lNextPhyLevelExp;                           	//下一级物理等级需要的经验
		  TYPE_UI32                	lLastPhyLevelExp;                           	//上一级物理等级需要的经验
		  TYPE_UI32                	lNextMagLevelExp;                           	//下一级魔法等级需要的经验
		  TYPE_UI32                	lLastMagLevelExp;                           	//上一级魔法等级需要的经验
		  TYPE_UI32                	lNextPhyPointExp;                           	//下一级物理技能点需要的经验
		  TYPE_UI32                	lLastPhyPointExp;                           	//上一级物理技能点需要的经验
		  TYPE_UI32                	lNextMagPointExp;                           	//下一级魔法技能点需要的经验
		  TYPE_UI32                	lLastMagPointExp;                           	//上一级魔法技能点需要的经验
		  USHORT                   	currentLevelMaxPhyPoint;                    	//当前等级可获得最大物理技能点
		  USHORT                   	currentLevelMaxMagPoint;                    	//当前等级可获得最大魔法技能点
		  TYPE_UI32                	maxLimitPhyExp;                             	//最大可获得的物理技能经验
		  TYPE_UI32                	maxLimitMagExp;                             	//最大可获得的魔法技能经验
		  USHORT                   	phySkillTitleSize;                          	//物理技能称号的长度
		  CHAR                     	phySkillTitle[16];                            	//物理技能等级称号
		  USHORT                   	magSkillTitleSize;                          	//魔法技能称号的长度
		  CHAR                     	magSkillTitle[16];                            	//魔法技能等级称号
		  FLOAT                    	fSpeed;                                     	//根据人物的速度档位得出的人物速度
		  TYPE_UI32                	nPhysicsJouk;                               	//物理躲闪
		  TYPE_UI32                	nMagicJouk;                                 	//魔法躲闪
		  TYPE_UI32                	Excellent;                                  	//卓越一击
		  TYPE_UI32                	Critical;                                   	//会心一击
	  };

///最迟移动通知算法，保存最后通知的移动信息;
class CNotifiedMoveInfo
{
public:
	CNotifiedMoveInfo() 
	{
		ResetNotifiedMovInfo();
	}
	~CNotifiedMoveInfo()
	{
		ResetNotifiedMovInfo();
	}

public:
	//初始化本结构时，以及玩家实际移动到达目标点时设置；
	void ResetNotifiedMovInfo()
	{
		m_isNotifiedInfoOK = false;
	}

	///计算是否需要通知新目标信息，每次移动过程中收到新移动请求，或者移动过程中计算得到自身新位置时调用；
	///调用本函数时，如果true==isNeedNoti，则立刻将新移动信息广播，同时调用SetNotifiedMovInfo设置已通知移动信息；
	///广播新移动信息时注意区分移动者本人与周边其它人,???,邓子建检查；
	bool IsNeedNoti( float curX, float curY/*当前位置*/, float speedX, float speedY/*往目标点位置的移动速度*/, float tgtX, float tgtY/*目标点*/ )
	{
		if ( !m_isNotifiedInfoOK )
		{
			//没有通知过旧移动信息，必定需要通知;
			return true;
		}

		if ( fabs(tgtX-m_oldTgtX) + fabs(tgtY-m_oldTgtY) < 1 )
		{
			//新旧目标点非常近，没必要通知；
			return false;
		}

		float passedTime = (float)( ( ACE_OS::gettimeofday() - m_lastNotiTime ).msec() / 1000.) + 1;//预测未来一秒后的位置差;

		float calX = 0, calY = 0;
		if ( ( passedTime >= m_oldExpectTime ) 
			|| (m_oldExpectTime<=0)
			)
		{
			//已经到达旧目标点；
			calX = m_oldTgtX, calY = m_oldTgtY;
		} else {
			float passedpcent = passedTime / m_oldExpectTime;
			calX = m_oldStPtX + passedpcent * ( m_oldTgtX-m_oldStPtX );//按旧目标点计算出来的新位置X
			calY = m_oldStPtY + passedpcent * ( m_oldTgtY-m_oldStPtY );//按旧目标点计算出来的新位置X
		}

		//当前点一秒后的位置-按旧目标点计算出来的一秒后位置，差距大于预定值
		return( ( fabs(curX+speedX-calX) + fabs(curY+speedY-calY) ) > 1 );
	}

	///每次通知移动信息后调用，以设置已通知移动信息
	bool SetNotifiedMovInfo( float curX, float curY, float tgtX, float tgtY, float inSpeed )
	{
		m_isNotifiedInfoOK = true;

		m_oldStPtX = curX;
		m_oldStPtY = curY;
		m_oldTgtX  = tgtX;
		m_oldTgtY  = tgtY;

		float tmpoffsetX = (m_oldTgtX-m_oldStPtX);
		float tmpoffsetY = (m_oldTgtY-m_oldStPtY);
		float tmpbase = sqrtf( tmpoffsetX*tmpoffsetX + tmpoffsetY*tmpoffsetY );
		if ( ( tmpbase <= 0.1 ) || ( inSpeed <= 0 ) )
		{
			//移动距离过小；
			m_isNotifiedInfoOK = false;
			return false;
		}

		m_oldExpectTime = tmpbase / inSpeed;//预计到达所通知的目标点需要花费的时间;
		m_lastNotiTime = ACE_OS::gettimeofday();//最后通知时间；

		return true;
	}

private:
	bool  m_isNotifiedInfoOK;
    float m_oldStPtX;
	float m_oldStPtY;
	float m_oldTgtX;
	float m_oldTgtY; /*旧位置、旧目标*/
	float m_oldExpectTime; /*最近一次通知时，预计还要多久才到达目标点*/

	ACE_Time_Value m_lastNotiTime; /*最近一次通知的时刻*/
};

//玩家身上的移动信息；
struct MoveInfo
{
public:
	MoveInfo() : isMoving(false) 
	{
		ResetMoveStat( 0, 0 );
	};
	~MoveInfo() {};

public:
	///最迟移动通知算法,置已通知状态；
	void SetNotifiedMovInfo( float inCurX, float inCurY )
	{
		//设置移动信息，如果isNeedNoti==true，则本函数的调用者必须立即广播给周围人，因此立刻重设已广播的移动信息结构；
		notifiedInfo.SetNotifiedMovInfo( inCurX, inCurY, fTargetX, fTargetY, fSpeed );
	}

	///最迟移动通知算法,设置移动信息，如果isNeedNoti==true，则调用者必须立即广播给周围人，否则，可以暂时不广播移动消息；
	bool SetMoveInfo( float inSpeed, float inCurX, float inCurY, float inTargetX, float inTargetY, bool& isNeedNoti/*最迟移动通知算法*/ )

	{
		float tmpoffsetX = (inTargetX-inCurX);
		float tmpoffsetY = (inTargetY-inCurY);
		float tmpbase = sqrtf( tmpoffsetX*tmpoffsetX + tmpoffsetY*tmpoffsetY );
		if ( ( tmpbase <= 0.1 ) )
		{
			//移动距离过小；
			return false;
		}
		fTargetX  = inTargetX;
		fTargetY  = inTargetY;
		fSpeed    = inSpeed;
		fSpeedX   = inSpeed * tmpoffsetX / tmpbase;
		fSpeedY   = inSpeed * tmpoffsetY / tmpbase;
		expectDis = tmpbase;//初始化时刻距目标点的距离，用于判断是否已到达目标点；

		isNeedNoti = true;//默认置新移动信息时是要通知的，除非玩家本身在移动，且旧的移动目标与新移动目标计算出的下一点相同时，才不需要通知；

#ifdef LATEST_MOVE_NOTI
		if ( isMoving )
		{
			isNeedNoti = notifiedInfo.IsNeedNoti( inCurX, inCurY, fSpeedX, fSpeedY, fTargetX, fTargetY );//最迟移动通知算法，是否需要广播移动信息
		}
#endif //LATEST_MOVE_NOTI
		//if ( !isMoving )
		//{
			startMoveTime = lastMoveTime = ACE_OS::gettimeofday();
			isMoving  = true;
		//}
		return true;
	};

	void ResetMoveStat( float curPosX, float curPosY )
	{
		isMoving = false;
		fTargetX = curPosX;
		fTargetY = curPosY;
		fSpeed = 0;
		fSpeedX = 0;
		fSpeedY = 0;
		expectDis = 0;//初始化时刻距目标点的距离，用于判断是否已到达目标点；
		startMoveTime = lastMoveTime = ACE_OS::gettimeofday();
		notifiedInfo.ResetNotifiedMovInfo();
	}
	float GetFloatPosX()
	{
		return fTargetX;
	}
	float GetFloatPosY()
	{
		return fTargetY;
	}

public:
	///当前在否在移动；
	bool IsMoving()
	{
		return isMoving;
	}

	///向目标点移动，如果当前确实处于移动状态，则返回真，否则返回假；
	bool MoveToTarget( float& fCurX, float& fCurY			
		, bool& isNeedNoti/*最迟移动通知算法*/, bool& isMoved/*本次浮点坐标是否有改变*/, bool& isReallyMoveGrid/*是否真正移动了(距离超过一格)*/, bool& isReallyMoveBlock/*是否真正移动了(距离超过一格)*/, bool& isAchieve/*是否到达了目标点*/
		, unsigned short& orgBlockX, unsigned short& orgBlockY, unsigned short& newBlockX, unsigned short& newBlockY, bool isForceCal/*是否强制计算（不满一格也计算）*/ );

private: //用于判断是否已到达目标点；
	bool           isMoving;//当前是否在移动；
	ACE_Time_Value startMoveTime;//开始移动时刻；
	float          expectDis;//初始化时计算出的到目标点距离；

private:

#ifdef LATEST_MOVE_NOTI
	CNotifiedMoveInfo notifiedInfo;//已通知的移动信息；
#endif //LATEST_MOVE_NOTI

private://用于定时更新玩家位置；
	ACE_Time_Value lastMoveTime;//最近移动时间；
	float fTargetX;//目标点浮点X坐标；
	float fTargetY;//目标点浮点Y坐标；
	float fSpeed;//速度
	float fSpeedX;//X方向速度;
	float fSpeedY;//Y方向速度;
};

struct RelationInfo
{
	RelationInfo():friendNum( 0 ), enemyNum( 0 )
	{}

	unsigned int GetFriendNumber()
	{
		return friendNum;
	}

	unsigned int GetEnemyNumber()
	{
		return enemyNum;
	}

	void AddFriendNumber( unsigned addNum )
	{
		friendNum += addNum;
	}


	void AddEnemyNumber( unsigned addNum )
	{
		enemyNum += addNum;
	}

	bool SubFriendNumber( unsigned int subNum )
	{
		if( subNum > friendNum )
		{
			D_WARNING("SubFriendNumber subNum>friendNum\n");
			return false;
		}
		friendNum -= subNum;
		return true;
	}

	bool SubEnemyNumber( unsigned int subNum )
	{
		if( subNum > enemyNum )
		{
			D_WARNING("SubFriendNumber subNum>enemyNum\n");
			return false;
		}
		enemyNum -= subNum;
		return true;
	}

	void SetFriendNum( unsigned int setNum )
	{
		friendNum = setNum;	
	}

	void SetEnemyNum( unsigned int setNum )
	{
		enemyNum = setNum;
	}

private:
	unsigned int friendNum;	//朋友数量
	unsigned int enemyNum;	//敌人数量
};


struct CollectInfo
{
	CollectInfo():collectMoney(0)
	{
		mCollectItemEleVec.clear();
	}

	vector<CItemPkgEle> mCollectItemEleVec;	//采集道具的VECTOR
	unsigned int collectMoney;					//采集的钱
};


using namespace MUX_PROTO;


class CGateSrv;

//BUFF影响的数值

class UniqueGatePtr
{
private:
	UniqueGatePtr( const UniqueGatePtr& );  //屏蔽这两个操作；
	UniqueGatePtr& operator = ( const UniqueGatePtr& );//屏蔽这两个操作；

public:
	UniqueGatePtr() : m_pProc(NULL), m_procUniqueID(0) {};
	~UniqueGatePtr()
	{
		m_pProc = NULL;
		m_procUniqueID = 0;
	}

public:
	void InitUniGatePtr( CGateSrv* pGate )
	{
		if ( NULL == pGate )
		{
			m_pProc = NULL;
			m_procUniqueID = 0;
			return;
		}
		m_pProc = pGate;
		m_procUniqueID = m_pProc->GetUniqueFlag();
		return;
	}

	inline CGateSrv* GetInnerGateSrvPtr()
	{
		if ( NULL == m_pProc )
		{
			return NULL;
		}
		if ( m_pProc->GetUniqueFlag() != m_procUniqueID )
		{
			return NULL;
		}
		return m_pProc;
	}

private:
	CGateSrv* m_pProc;	//表明该玩家的所属处理器，发送时直接调用这个
	unsigned int m_procUniqueID;//所属处理器的唯一ID号；
};

///玩家所设的活点信息；
struct PlayerAptInfo
{
public:
	PlayerAptInfo() : reqID(0), pAptPlayer(NULL), isAptSet(false)
	{
		aptPlayerID.wGID = 0;
		aptPlayerID.dwPID = 0;
	};
	~PlayerAptInfo() 
	{
		reqID = 0;
		pAptPlayer        = NULL;
		aptPlayerID.wGID  = 0;
		aptPlayerID.dwPID = 0;
		isAptSet = false;
	}

public:
	bool SetAptPlayer( unsigned int inReqID, CPlayer* pInPlayer );

	inline bool ResetAptPlayer()
	{
		reqID = 0;
		pAptPlayer = NULL;
		aptPlayerID.wGID = 0;
		aptPlayerID.dwPID = 0;
		isAptSet = false;
		return true;
	}

	inline bool IsAptPlayerSet()
	{
		return isAptSet;
	}

	inline unsigned int GetReqID()
	{
		return reqID;
	}

	inline CPlayer* GetAptPlayerPtr()
	{
		if ( !isAptSet )
		{
			return NULL;
		} else {
			return pAptPlayer;
		}
	}

	inline const PlayerID& GetAptPlayerID()
	{
		return aptPlayerID;
	}

private:
	unsigned int reqID;//客户端发来的创建活点时的请求号；    
	CPlayer* pAptPlayer;//活点玩家指针；
	PlayerID aptPlayerID;//活点玩家ID号；
	bool     isAptSet;//是否为有效的APT信息；
};//struct PlayerAptInfo

struct KilledInfo
{
#define GET_PROFIT_COUNT 5

	KilledInfo()
	{
		roleUID = 0;
		killCount = 0;
	}

	void SetKilledCount(unsigned int inRoleUID )
	{
		if( roleUID == inRoleUID )
		{
			killCount++;
		}else
		{
			roleUID = inRoleUID;
			killCount = 1;
		}
	}

	unsigned int GetKillCount()
	{
		return killCount;
	}


	bool CanGetProfit()		//是否能在击杀时获得利益
	{
		return (killCount <= GET_PROFIT_COUNT);
	}

	void SetKillInfo( const KillInfo& killInfo )
	{
		roleUID = killInfo.roleUID;
		killCount = killInfo.killCount;
	}

	unsigned int GetRoleUID()
	{
		return roleUID;
	}


private:
	unsigned int  roleUID;				//最近一次所杀的角色ID
	unsigned short killCount;			//杀死该角色的连续次数,如果连续杀同一角色,超过5次,则会有收益惩罚
};//KilledInfo

struct CachedRepurchaseInfo
{
public:
	struct RepurchaseInfo_i
	{
		ItemInfo_i itemInfo;
		unsigned int money;
	};

	CachedRepurchaseInfo(void)
	{
		Clear();
	}
public:
	void Clear(void)
	{
		cachedItems.clear();
	}

	void InsertItem(ItemInfo_i &item, unsigned int money)
	{
		RepurchaseInfo_i repurchase;
		repurchase.itemInfo = item;
		repurchase.money = money;

		int size = (int)cachedItems.size();
		if(0 == size)//为空则直接插入
		{
			cachedItems.push_back(repurchase);
			return;
		}

		//查找第一个为空的位置
		int emptyPos = -1;
		int flagPos = 0;
		for(int i = 0; i < size; i++)
		{
			if(0 == cachedItems[i].itemInfo.uiID)
			{	
				emptyPos = i;
				break;
			}
		}

		if(emptyPos == -1)
		{
			cachedItems.push_back(cachedItems[0]);
			flagPos = (int)cachedItems.size() -1;
		}
		else
		{
			flagPos = emptyPos;
		}

		//移动
		for(int i = flagPos; i > 0; i--)
			cachedItems[i] = cachedItems[i-1];

		//插到第一个位置
		cachedItems[0] = repurchase;

		//只缓存MAX_REPURCHASE_COUNT个
		if(cachedItems.size() > MAX_REPURCHASE_COUNT)
			cachedItems.erase(cachedItems.begin()+MAX_REPURCHASE_COUNT, cachedItems.end());

		return;
	}

	void DeleteItem(unsigned short index)
	{
		if(index < MAX_REPURCHASE_COUNT && index < cachedItems.size())
			StructMemSet( cachedItems[index], 0x00, sizeof(cachedItems[index]));

		return;
	}

	RepurchaseInfo_i *GetItem(unsigned short index)
	{
		if(index < cachedItems.size())
			return &cachedItems[index];

		return NULL;
	}

	std::vector<RepurchaseInfo_i>& CachedList(void)
	{
		return cachedItems;
	}

private:
	std::vector<RepurchaseInfo_i> cachedItems;
};

struct PlayerTeamCopyFlag
{
	int copyteamID;
	unsigned int copyplayerflag;
	unsigned int copyteamflag;
	PlayerTeamCopyFlag():copyteamID(0),copyplayerflag(0),copyteamflag(0){}

	void ResetTeamCopyFlag()
	{
		copyteamID = copyplayerflag = copyteamflag = 0;
	}

	unsigned int GetTeamFlag() { return copyteamflag; }
	unsigned int GetPlayerFlag() { return copyplayerflag; }
	void SetTeamCopyFlag( unsigned int teamID, unsigned int playerflag, unsigned int teamflag )
	{
		copyteamID = teamID;
		copyplayerflag =  playerflag;
		copyteamflag = teamflag;
	} 
};


#endif /*PLAYER_BASE_H*/
