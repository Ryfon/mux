﻿/*
*
* @file CPlayer.h
* @brief 定义玩家信息
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: CPlayer.h
* 摘    要: 定义玩家有关信息
* 作    者: hyn
* 完成日期: （待扩展）
*
*/
#ifndef PLAYER_H
#define PLAYER_H


#include "PlayerBase.h"

using namespace MUX_PROTO;


class CPlayer : public CMuxCreature
{
private:
	CPlayer( const CPlayer& );  //屏蔽这两个操作；
	CPlayer& operator = ( const CPlayer& );//屏蔽这两个操作；

public:
	CREATURE_TYPE GetType() { return CREA_TYPE_PLAYER; };//应晓强要求添加，08.09.01

public:
	inline bool IsRobot() { return m_bIsRobot; }

private:
	bool m_bIsRobot; //临时，机器人作特殊处理;

public:
	///置本玩家在地图玩家数组中的位置；
	inline bool SetPlayerPosInMapArr( int posInMapArr )
	{
		if ( posInMapArr < 0 )
		{
			m_posInMapArr = -1;
			return false;
		}
		m_posInMapArr = posInMapArr;
		return true;
	}

	///取本玩家在地图玩家数组中的位置；
	inline int GetPlayerPosInMapArr()
	{
		return m_posInMapArr;
	}

private:
	int  m_posInMapArr;//在地图玩家数组中的位置，用于快速查找删除本玩家；

public:
	//记录被杀次数++;
	inline bool IncBeKillTimes( unsigned int beKillRankSeq )
	{
		return m_fullPlayerInfo.stateInfo.IncBeKillTimes( beKillRankSeq );
	}

	//取被杀次数记录；
	inline unsigned int GetBeKillTimes( unsigned int beKillRankSeq )
	{
		return m_fullPlayerInfo.stateInfo.GetBeKillTimes( beKillRankSeq );
	}

	//记录击杀次数++;
	inline bool IncKillTimes( unsigned int killRankSeq )
	{
		return m_fullPlayerInfo.stateInfo.IncKillTimes( killRankSeq );
	}

	//取击杀次数记录；
	inline unsigned int GetKillTimes( unsigned int killRankSeq )
	{
		return m_fullPlayerInfo.stateInfo.GetKillTimes( killRankSeq );
	}

	//可能的新的最大输出伤害
	inline bool NewOutDamage( unsigned int curRankSeq, unsigned int curOutDamage )
	{
		return m_fullPlayerInfo.stateInfo.NewOutDamage( curRankSeq, curOutDamage );
	}

	//取最大输出伤害；
	inline unsigned int GetMaxOutDamage( unsigned int curRankSeq )
	{
		return m_fullPlayerInfo.stateInfo.GetMaxOutDamage( curRankSeq );
	}

public:
	CPlayer()
	{
		m_nTradeInfo = 0;
	}

	virtual ~CPlayer()
	{
	}

	PoolFlagDefine()
	{
		m_pFightPet = NULL;
		m_UseMoneyType = 0;//0:银币，1:金币，初始上线时默认为使用银币
		m_isSimplePlayerInfoValid = false;//初始用于广播的外观信息未初始化，无效；
		m_isNeedNotiTeamMembers = false;//此时信息与simpleplayerinfo一样，无效；

		m_ndmsFollowingMonsterID = 0;
		m_posInMapArr = -1;//初始化为无效位置；
        ResetCopyStageInfos();//重置玩家所有的副本阶段信息；
		SetPlayerPosInCopy( 0 );//重置玩家在副本中的位置（该位置只有玩家当前处于副本中时有意义）；
		SetLeaveUpdateInfoToDB( true );//离线是需要更新自身信息至DB(除非gate发来GMPlayerLeave::NOT_EXIST)；
		SetIsInDelayDelQue( false );//初始不在延迟删队列中；

        ClearFollowingMonster();
		m_bIsInMovingList = false;
        ResetMoveStat();

		m_isLeaveDelay = false;

		//09.04.28,取消工会任务新实现,ClearCanSelectRandomTask();

		m_bIsRobot = false;//初始非机器人;
//        m_PlayerNearby.Clear();//清附近玩家；

		m_bPreFight = true;
		m_bIsInSwitchMap = false;
		m_bIsWaitToDel = false;
		m_bIsFullInfoOk = false;

		StructMemSet( m_fullPlayerInfo, 0, sizeof( FullPlayerInfo ) );
		m_fullPlayerInfo.stateInfo.ResetPlayerStateInfo();//这个先后顺序不要乱；

		m_pMap  = NULL;
		m_uniGatePtr.InitUniGatePtr( NULL );

		m_nextActionTime = ACE_Time_Value::zero;
		m_RegisterInvincibleTime = ACE_Time_Value::zero;
		m_updatePosTime  = ACE_OS::gettimeofday();
		m_lastSaveTime   = ACE_OS::gettimeofday();
		m_lastOLTestCheckTime = ACE_OS::gettimeofday();
		
		m_npcChatInfo.Init();
		m_itemChatInfo.Init();
		m_areaChatInfo.Init();

		StructMemSet( m_TmpProp, 0, sizeof(m_TmpProp) );
		//bug修改，memset( &m_playerSkill, 0, sizeof(m_playerSkill) );
		//memset( &m_itemTotalProp,0x0, sizeof(ItemAddtionProp) );//新的装备增加属性
		m_itemTotalProp.ResetItemAddtionProp();//重置玩家身上的装备添加属性;
		m_bEnableAttack = true; //表示可以攻击
		m_bFaint = false;      
		m_bBondage = false;
		m_bRide = false;
		mForgingItem = 0;
		m_battleMode = DEFAULT_MODE;
		m_pRogueBuff = NULL;
		m_bRookieBreak = false;
		m_bRookieState = false;
		m_pAreaScript = NULL;
		maxItemSpeed = 0 ;
		maxRideSpeed = 0 ;
#ifdef LEVELUP_SCRIPT
		m_pLevelUpScript = NULL;
#endif 
       ClearCounters();
		ClearTasks();
		m_spInfo.usSpLevel = 0;
		m_spInfo.nextLevelExp = 0;

//#ifdef AI_SUP_CODE
//		m_vecRegedMonsters.clear();
//#endif //AI_SUP_CODE
		m_recvFromPlayer.dwPID = 0;
		m_recvFromPlayer.wGID = 0;
		DelTradeInfo();
		m_nTradeInfo = 0;
		m_pUnion = NULL;
		m_anamorphic = 0;
		friendGroupGainAddition = 0;
		m_isGmAccount = false;
		m_unionChatCmd = -1;
		m_cachedRepurchase.Clear();
		m_copyTeamFlag.ResetTeamCopyFlag();
		m_wearpointchecktime = time(NULL);
		SetDebugMonsterID(0);
		m_bActRestFlag = false;
		ReleaseMuxSkill();
		m_isNeedNotifyHpChange = false;
		m_isNeedNotifyMpChange = false;
		m_lastNotifyHpMpChange = ACE_OS::gettimeofday();

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
		//帐号与角色名，新登录/DB信息流程添加
		StructMemSet( m_accountName, 0, sizeof(m_accountName) );
		StructMemSet( m_roleName, 0, sizeof(m_roleName) );
		///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

public:
	//排行榜缓存方式修改//是否曾经进过本mapsrv的排行榜数据缓存
	//inline bool IsEverInRankCache() { return m_bIsEverInRankCache; };
	////设置曾经进过本mapsrv的排行榜数据缓存
	//inline bool SetEverInRankCache() { m_bIsEverInRankCache = true; return true;};
	//取历史进地图序号，用于区分排行榜数据的记录时刻
	unsigned int GetEnterMapSeq() { return m_fullPlayerInfo.stateInfo.m_enterMapSeq; }

private:
	unsigned int m_ndmsFollowingMonsterID;//跨地图时需要跟随的怪物信息；

private:
	///根据fullplayerinfo中的PlayerFightPetDBInfo以及创建战斗宠;
	bool InitFightPet();
	///传递给PlayerFightPetDBInfo,玩家在包裹位置itemPkgPos与装备位置equipPos之间移动战斗宠道具;
	bool ExchangeFightPetItem( unsigned int itemPkgPos, unsigned int itemID, unsigned int equipPos, bool isEquip/*true:装备,false:卸载*/ );
	///传递给PlayerFightPetDBInfo,选择某个战斗宠道具;
	bool SelectFightPetItem( unsigned int equipPos, unsigned int itemID );
	///传递给PlayerFightPetDBInfo,在激活/非激活状态之间切换;
	bool ToggleActive( bool isActive );
	///玩家战斗宠移动(开始，移动中跨块，停止移动三种时刻发来消息)
	bool OnFightPetMove( unsigned int moveType/*0:停止,1:移动,2:跨块*/, unsigned short curPosX, unsigned short curPosY, unsigned short tgtPosX, unsigned short tgtPosY );

private:
	CFightPet* m_pFightPet;//玩家战斗宠；

public:
	////取视野内玩家以及怪物数量；
	//bool GetSurPlayerMonsterNum( unsigned int& mapPlayerNum, unsigned int& surPlayerNum, unsigned int& monsterNum );

	///取玩家当前浮点坐标X;
	float GetFloatPosX()
	{
		return m_fCurX;
	}
	///取玩家当前浮点坐标Y;
	float GetFloatPosY()
	{
		return m_fCurY;
	}
	float GetFloatTargetX()
	{
		return ( m_MoveInfo.IsMoving() ? m_MoveInfo.GetFloatPosX() : m_fCurX );
	}
	float GetFloatTargetY()
	{
		return ( m_MoveInfo.IsMoving() ? m_MoveInfo.GetFloatPosY() : m_fCurY );
	}
	TYPE_POS GetGridTargetX()
	{
		return (TYPE_POS) ( GetFloatTargetX() );
	}
	TYPE_POS GetGridTargetY()
	{
		return (TYPE_POS) ( GetFloatTargetY() );
	}
	///置玩家移动信息；
	bool SetMoveInfo( float inSpeed, float inTargetX, float inTargetY, bool& isNeedNoti/*最迟移动通知算法*/ );
	///最迟移动通知算法,置已通知状态，每次广播一个新移动目标后设置；
	void SetMoveNotifiedStat()
	{
		m_MoveInfo.SetNotifiedMovInfo( GetFloatPosX(), GetFloatPosY() );
	}

	///玩家是否在移动；
	bool IsMoving()
	{
		return m_MoveInfo.IsMoving();
	}
	void InitMovingInfo()//临时，待玩家浮点坐标存盘后去除；
	{
		m_bIsInMovingList = false;
		m_fCurX = (float)(1.0*GetPosX() + 0.5);
		m_fCurY = (float)(1.0*GetPosY() + 0.5);
	}

	///重置玩家的移动状态，玩家下线或死亡时必须调用；
	void ResetMoveStat();

public:
	///玩家请求出售物品；
	bool SoldPkgItem( const GMItemSoldReq& itemSoldReq );

	// 获取灰色物品列表
	unsigned int GetAllGreyItems(std::map<int, unsigned int>& greyItemList);

	///批量卖出灰色物品
	bool SoldGreyPkgItems( const GMBatchSellItemsReq& itemSoldReq);

	///回购物品
	bool RepurchaseItem( const GMRepurchaseItemReq& itemReq);

	//更新回购列表
	void RepurchaseListNtf(void);

	const PlayerID GetCreatureID() 
	{
		return m_lPlayerID;
	}

public:

	//重设玩家位置；
	void ForceSetFloatPos( bool isManualSet/*是否手动重置(非移动)*/, float newFloatX, float newFloatY );

private:
	///单个玩家重置位置，置位置的最小单位，仅供ForceSetFloatPos调用，对直接调用者，以及调用者的骑乘乘员分别调用一次；
	void OnePlayerForceSetFloatPos( bool isManualSet/*是否手动重置(非移动)*/, float newFloatX, float newFloatY, bool& isResetBlock/*是否重设了块*/ );

public:
	///向目标点移动，如果当前确实处于移动状态，则返回真，否则返回假；
	bool TryMove( bool& isNeedNoti/*最迟移动通知算法*/, bool& isReallyMoveGrid/*移动是否跨格*/, bool& isReallyMoveBlock/*移动是否跨块*/, bool& isAchieve
		, unsigned short& orgBlockX, unsigned short& orgBlockY, unsigned short& newBlockX, unsigned short& newBlockY
		, bool isForceCal/*是否强制计算，而不管是否跨格*/ );

	///是否在移动列表中；
	bool IsInMovingList()
	{
		return m_bIsInMovingList;
	}
	///设置玩家已加入移动列表；
	void SetInMovingList()
	{
		m_bIsInMovingList = true;
	}
	///设置玩家已不在移动列表；
	void SetNotInMovingList()
	{
		m_bIsInMovingList = false;
	}

	///////////////////////////////////////////////////////////////////////////////
	//活点相关。。。
public:
	///添加活点玩家；
	void OnAddAPT( unsigned int reqID, const char* aptRoleName );
private:
	///定时检测玩家所设APT点的有效性，如果有效且活点仍在地图上，则通知玩家该活点的位置信息；
	void TimeCheckUpdateAptInfo();
private:
	PlayerAptInfo m_aptInfo;//玩家所设的活点信息；
	//活点相关。。。
	///////////////////////////////////////////////////////////////////////////////

private:
	MoveInfo m_MoveInfo;//玩家移动信息；
	bool     m_bIsInMovingList;
	float    m_fCurX;//玩家当前X浮点坐标；
	float    m_fCurY;//玩家当前Y浮点坐标；

public:
	///////////////////////////////////////////////////////////////
	//托盘相关...
	///通知客户端显示一个托盘；
	bool ShowPlateCmd( EPlateType plateType );
	///设置本次托盘的判断条件ID
	void SetPlateCondition( unsigned int conditionID );
	///客户端向托盘中添加物品；
	bool OnAddSthToPlate( EPlateType plateType, ItemInfo_i sthInfo, /*EPlatePos platePos*/unsigned int platePos );
	///客户端从托盘中取走物品；
	bool OnTakeSthFromPlate( ItemInfo_i sthInfo, unsigned int platePos ) ;
	///客户端发命令尝试托盘操作
	bool OnExecPlate();
	///服务器通知客户端关托盘
	void ClosePlateCmd();
	///客户端发命令尝试托盘操作
	bool OnClosePlate();
	//死亡时关闭托盘
	bool OnDieClosePlate();
	//获取托盘的引用
	CCommonPlate& GetCommonPlate() { return m_playerPlate; }

	void SetTryExecPlateMoney( unsigned int money ) { return m_playerPlate.SetConsumeMoney(money); }

	unsigned int GetTryExecPlateMoney() { return m_playerPlate.GetConsumeMoney(); }

	CProtectItemPlate& GetProtectItemPlate() { return m_protectItemPlate; }

	CProtectItemManager& GetProtectItemManager() { return m_protectItemMan; }

	NewTaskRecordDelegate& GetNewTaskRecordDelegate() { return m_taskRecordDelegate; }

	//...托盘相 关
	///////////////////////////////////////////////////////////////

private:
	CCommonPlate m_playerPlate;//玩家身上的托盘；

	CProtectItemPlate m_protectItemPlate;//玩家保护道具的托盘

	CProtectItemManager m_protectItemMan;//玩家保护道具的管理

	//PlayerTeamCopyFlag  m_teamCopyFlag;

public:

	/////通知客户端最终复活完成（在地图上出现）
 //   void SendFinalRebirthSuc( TYPE_ID sequenceID = 0);

	///通知客户端进入地图(地图服务器)成功；
	void SendEnterMapRstSuc();

	void UpdateGateGoodEvilPoint();		//发送消息更新Gate上的善恶值

	//初次上线调用；
	void OnlineProc( unsigned long& dyingleft )
	{
		m_fullPlayerInfo.stateInfo.OnlineProc( this, dyingleft );//上线置玩家活状态；
	}

	///下线调用相关处理；
	void OfflineProc( EAppearType nReason, bool isClearCopyInfo/*是否清副本信息，只有跳进副本的情况下才不清副本信息，其余任何情形均清副本信息*/ );

	inline bool IsWaitingClient()
	{
		return ( MAP_PS_WAITING_CLIENT == m_fullPlayerInfo.stateInfo.m_CurBaseState );
	}

	EPlayerBaseState GetBaseState()
	{
		return m_fullPlayerInfo.stateInfo.m_CurBaseState;
	}

	inline bool IsDying()
	{
		return ( MAP_PS_DIE == m_fullPlayerInfo.stateInfo.m_CurBaseState );
	}

	//虚弱状态也算活着
	inline bool IsAlive()
	{
		return ( MAP_PS_ALIVE == m_fullPlayerInfo.stateInfo.m_CurBaseState || MAP_PS_WEAK == m_fullPlayerInfo.stateInfo.m_CurBaseState );
	}

	//取玩家装配的武器，有装配武器时返回true，没装配武器时返回false;
	bool GetEquipWeaposType( unsigned int& weaponType1, unsigned int& weaponType2 );
	//该玩家是否装备武器
	bool IsEquipWeapon();
	//取玩家身上装配的某型装备,不可用本函数取单手装备，因为其ItemInfo_i有两个；
	bool GetPlayerGearEquip( ITEM_GEARARM_TYPE gearType, ItemInfo_i*& pOutItemInfoI );
	
	//初始化生成一个玩家
	bool InitPlayer( CNewMap* pMap);

	//更新信息至DB 04.23
	bool UpdatePlayerFullInfo( EAppearType infoType );

	//设置地图
	void SetMap(CNewMap* pMap);

	///无lNotiPlayerID填写的强制发包函数，用于DB存盘相关包的发送；
	template< typename T_Msg >
	void ForceGateSend( T_Msg* pMsg )
	{
		TRY_BEGIN;

		CGateSrv* pGate = GetProc();

		if ( NULL == pGate )
		{
			return;
		}

#ifdef GATE_SENDV
		bool isNewPkg = false;
		MsgToPut* pNewMsg = CreateGateSrvPkg( T_Msg, pGate, *pMsg, isNewPkg );
		if ( isNewPkg )
		{
			pGate->SendOldRsvNew( pNewMsg );
		}
#else  //GATE_SENDV
		MsgToPut* pNewMsg = CreateSrvPkg( T_Msg, pGate, *pMsg );
		pGate->SendPkgToGateServer(pNewMsg);
#endif //GATE_SENDV

		return;
		TRY_END;
		return;
	}

	///发包函数；
	template< typename T_Msg >
	void SendPkg( T_Msg* pMsg )
	{
		TRY_BEGIN;

		if ( IsLeaveDelay() )
		{
			///离线保留期间，除向DB发送的玩家信息保存消息之外，不向玩家发送任何其它消息；
			if ( T_Msg::wCmd != MGFullPlayerInfo::wCmd )
			{
				return;
			}
		}

		CGateSrv* pGate = GetProc();

		if ( NULL == pGate )
		{
			return;
		}
		pMsg->lNotiPlayerID = m_lPlayerID;//所有发给玩家的消息结构中都会包含通知玩家的ID号；

#ifdef PRINT_PKGNUM //测试集聚情形；
		//统计发包数量；
	   if ( MGOtherPlayerInfo::wCmd == T_Msg::wCmd )
	   {
		   ++g_playerInfoNum;
	   }
	   //else if ( MGMonsterInfo::wCmd == T_Msg::wCmd ) 
	   {
	       ++g_monsterInfoNum;
	   }
#endif //PRINT_PKGNUM //测试集聚情形；

#ifdef GATE_SENDV
		bool isNewPkg = false;
		MsgToPut* pNewMsg = CreateGateSrvPkg( T_Msg, pGate, *pMsg, isNewPkg );
		if ( isNewPkg )
		{
			pGate->SendOldRsvNew( pNewMsg );
		}
#else  //GATE_SENDV
		MsgToPut* pNewMsg = CreateSrvPkg( T_Msg, pGate, *pMsg );
		pGate->SendPkgToGateServer(pNewMsg);
#endif //GATE_SENDV

		return;
		TRY_END;
		return;
	}

	void SendPkg( MGTmpInfoUpdate* pMsg )
	{
		D_DEBUG( "已废弃旧消息MGTmpInfoUpdate，忽略发送, 10.05.25\n" );
		return;
	}
	
public:
	//置玩家位置；
	void SetPlayerPos( unsigned short nX, unsigned short nY, bool isSetFloatPos/*是否同时置浮点坐标*/ )
	{
		OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(位置)

		m_fullPlayerInfo.baseInfo.iPosX = nX;
		m_fullPlayerInfo.baseInfo.iPosY = nY;
		if ( isSetFloatPos )
		{
			InitMovingInfo();
		}
	}


	//10.09.20废弃//获取角色的模样编号
	//unsigned short GetRoleNo()
	//{
	//	return m_fullPlayerInfo.baseInfo.usPortrait;
	//}

	//获取玩家的完整ID
	inline const PlayerID& GetFullID()
	{
		return m_lPlayerID;
	}

	//获取玩家在GATESRV上的ID
	unsigned long GetPID()
	{
		return m_lPlayerID.dwPID;
	}

	//获取DB ID
	unsigned int GetDBID()
	{
		return m_fullPlayerInfo.baseInfo.uiID;
	}

	//获取玩家的GATESRV上的ID
	unsigned short GetGID()
	{
		return m_lPlayerID.wGID;
	}

	//取玩家当前点位置
	bool GetPos( TYPE_POS& nPosX, TYPE_POS& nPosY )
	{
		TRY_BEGIN;
		nPosX = m_fullPlayerInfo.baseInfo.iPosX;
		nPosY = m_fullPlayerInfo.baseInfo.iPosY;
		return true;
		TRY_END;
		return false;
	}

	int GetPosX()
	{
		return m_fullPlayerInfo.baseInfo.iPosX;
	}

	int GetPosY()
	{
		//return m_DBProp.m_nMapY;
		return m_fullPlayerInfo.baseInfo.iPosY;
	}
	
	//取玩家当前所在地图；
	CNewMap* GetCurMap();

	//取地图号；
	unsigned short GetMapID()
	{
		return m_fullPlayerInfo.baseInfo.usMapID;
	}

	////获取姓名
	//char* GetAccount()
	//{
	//	return m_fullPlayerInfo.baseInfo.szAccount
	//}

	//取shFlag(性别|称号掩码)
	BYTE GetSHFlag()
	{
		return m_fullPlayerInfo.baseInfo.shFlag;
	}

	//设置玩家称号显示标记；
	bool SetHonorMask( unsigned char inHonorMask )
	{
		SetHMaskToSHFlag( inHonorMask, m_fullPlayerInfo.baseInfo.shFlag );
		return true;
	}

	unsigned char GetHonorMask()
	{
		return GetHMaskFromSHFlag( m_fullPlayerInfo.baseInfo.shFlag );
	}

	//获得性别
	BYTE GetSex(void)
	{
		return GetSexFromSHFlag(m_fullPlayerInfo.baseInfo.shFlag);
	}

	//获取当前经验值
	unsigned long GetExp()
	{
		return m_fullPlayerInfo.baseInfo.dwExp;
	}

	//获取级别
	unsigned short GetLevel(void)
	{

		return m_fullPlayerInfo.baseInfo.usLevel;
	}

	unsigned int GetAbsorbDamage()
	{
		return m_buffManager.GetAbsorbDamage();
	}

	unsigned char GetClass(void)
	{
		return m_fullPlayerInfo.baseInfo.usClass;
	}

	//获取种族
	unsigned short GetRace(void)
	{
		return m_fullPlayerInfo.baseInfo.ucRace;
	}

	//获取力量
	unsigned short GetStrength(void)
	{
		int value = m_fullPlayerInfo.baseInfo.strength;
		value += GetItemAddtionProp().strength;
		return value;
	}

	//获取体质
	unsigned short GetVitality(void)
	{
		int value = m_fullPlayerInfo.baseInfo.vitality;
		value += GetItemAddtionProp().physique;
		return value;
	}

	//获取敏捷
	unsigned short GetAgility(void)
	{
		int value = m_fullPlayerInfo.baseInfo.agility;
		value += GetItemAddtionProp().agility;
		return value;
	}
	
	//获取精神
	unsigned short GetSpirit(void)
	{
		int value = m_fullPlayerInfo.baseInfo.spirit;
		value += GetItemAddtionProp().spirit;
		return value;
	}

	//获取智力
	unsigned short GetIntelligence(void)
	{
		int value = m_fullPlayerInfo.baseInfo.intelligence;
		value += GetItemAddtionProp().intelligence;
		return value;
	}

	//获取未分配点数
	unsigned short GetUnallocatPoint(void)
	{
		return m_fullPlayerInfo.baseInfo.unallocatePoint;
	}

	unsigned int GetTotalExp(void)
	{
		return m_fullPlayerInfo.baseInfo.totalExp;
	}

	//获取物攻
	unsigned int GetPhyAttack(void)
	{
		AttributeEffect* pUnionEffect = GetUnionEffect();
		int finalPhyAtk = m_TmpProp.nPhysicsAttack;
		finalPhyAtk = (int)(m_TmpProp.nPhysicsAttack * (1 + GetFriendGroupGainAddi()));//好友度带来的攻防加成

		finalPhyAtk += m_itemTotalProp.PhysicsAttack;
		finalPhyAtk += GetBuffPhyAtk();
		if( pUnionEffect )
		{
			finalPhyAtk += pUnionEffect->nPhyAttack;
		}
		return ( finalPhyAtk < 0) ? 0 : finalPhyAtk;
	}
	

	//获取魔攻
	unsigned int GetMagAttack(void)
	{
		AttributeEffect* pUnionEffect = GetUnionEffect();
		int nFinalMagicAttack = m_TmpProp.nMagicAttack;
		nFinalMagicAttack = (int)(m_TmpProp.nMagicAttack * ( 1 + GetFriendGroupGainAddi()));//好友度带来的攻防加成

		nFinalMagicAttack += m_itemTotalProp.MagicAttack;
		nFinalMagicAttack += GetBuffMagAtk();
		if( NULL != pUnionEffect )
		{
			nFinalMagicAttack += pUnionEffect->nMagAttack;
		}
		return ( nFinalMagicAttack < 0) ? 0 : nFinalMagicAttack;
	}

	//获取物防,装备 技能 初始都有的
	unsigned int GetPhyDefend(void)
	{
		int nFinalPhyDefend = m_TmpProp.nPhysicsDefend;
		nFinalPhyDefend = (int)(m_TmpProp.nPhysicsDefend * (1 + GetFriendGroupGainAddi()));//好友度带来的攻防加成
		
		nFinalPhyDefend	+= m_itemTotalProp.PhysicsDefend;
		nFinalPhyDefend += GetBuffPhyDef();
		AttributeEffect* pUnionEffect = GetUnionEffect();
		if( NULL != pUnionEffect )
		{
			nFinalPhyDefend += pUnionEffect->nPhyDefend;
		}
		return ( nFinalPhyDefend < 0) ? 0 : nFinalPhyDefend;
	}

	//获取魔防,装备 技能 初始都有的
	unsigned int GetMagDefend(void)
	{
		AttributeEffect* pUnionEffect = GetUnionEffect();
		int nFinalMagicDefend = m_TmpProp.nMagicDefend;
		nFinalMagicDefend = (int)(m_TmpProp.nMagicDefend * (1 + GetFriendGroupGainAddi()));//好友度带来的攻防加成
		
		nFinalMagicDefend += m_itemTotalProp.MagicDefend;
		nFinalMagicDefend += GetBuffMagDef();
		if( NULL != pUnionEffect )
		{
			nFinalMagicDefend += pUnionEffect->nMagDefend;
		}
		return (nFinalMagicDefend < 0) ? 0 : nFinalMagicDefend;
	}

	//获取物命 物命是受初始值 装备  物理
	unsigned int GetPhyHit(void)
	{
		AttributeEffect* pUnionEffect = GetUnionEffect();
		int nFinalPhyHit = m_TmpProp.nPhysicsHit;
		nFinalPhyHit += m_itemTotalProp.PhysicsHit;
		nFinalPhyHit += GetBuffPhyHit();
		if( NULL != pUnionEffect )
		{
			nFinalPhyHit += pUnionEffect->nPhyHitLevel;
		}
		return ( nFinalPhyHit < 0) ? 0 : nFinalPhyHit;
	}


	//获取魔命 魔命是受初始值 装备  物理
	unsigned int GetMagHit(void)
	{
		AttributeEffect* pUnionEffect = GetUnionEffect();
		int nFinalMagHit = m_TmpProp.nMagicHit;
		nFinalMagHit += m_itemTotalProp.MagicHit;
		nFinalMagHit += GetBuffMagHit();
		if( NULL != pUnionEffect )
		{
			nFinalMagHit += pUnionEffect->nMagHitLevel;
		}
		return ( nFinalMagHit < 0) ? 0 : nFinalMagHit;
	}

	//获取物闪
	unsigned int GetPhyJouk(void)
	{
		int nFinalPhyJouk = m_TmpProp.nPhysicsJouk;
		nFinalPhyJouk += m_itemTotalProp.PhysicsJouk;
		return ( nFinalPhyJouk < 0) ? 0 : nFinalPhyJouk;
	}

	//获取魔闪
	unsigned int GetMagJouk(void)
	{
		int nFinalMagJouk = m_TmpProp.nMagicJouk;
		nFinalMagJouk += m_itemTotalProp.MagicJouk;
		return ( nFinalMagJouk < 0) ? 0 : nFinalMagJouk;
	}

	//获取卓越一击
	unsigned int GetExcellent(void)
	{
		int nFinalExcellent = m_TmpProp.Excellent;
		nFinalExcellent += m_itemTotalProp.excellentbiff;
		return ( nFinalExcellent < 0) ? 0 : nFinalExcellent;
	}

	//获取会心一击
	unsigned int GetCritical(void)
	{
		int nFinalCritical = m_TmpProp.Critical;
		nFinalCritical += m_itemTotalProp.luckybiff;
		return ( nFinalCritical < 0) ? 0 : nFinalCritical;
	}

	//获取最大HP, 装备,技能
	unsigned int GetMaxHP(void)
	{
		unsigned int finalMaxHp = m_TmpProp.lMaxHP; //基础的
		finalMaxHp = (unsigned int)(finalMaxHp * ( 1 + GetBuffBaseHpUp() ));
		finalMaxHp += m_itemTotalProp.Hp;
		finalMaxHp += GetBuffMaxHpUp();
		AttributeEffect* pUnionEffect = GetUnionEffect();
		if( NULL != pUnionEffect )
		{
			finalMaxHp += pUnionEffect->nMaxHpUp;
		}
		return finalMaxHp;
	}

	unsigned int GetBaseMaxHp()
	{
		return m_TmpProp.lMaxHP;
	}

	//获取最大MP
	unsigned int GetMaxMP(void)
	{
		return m_TmpProp.lMaxMP + m_itemTotalProp.Mp;
	}

	unsigned int GetBaseMaxMp()
	{
		return m_TmpProp.lMaxMP;
	}

	//10.09.20已废弃//获取临时信息中的状态位
	//unsigned int GetTempInfoState(void)
	//{
	//	return m_TmpProp.nNormalState;
	//}

	//下一级的经验
	unsigned long GetNextLevelExp(void)
	{
		return m_TmpProp.lNextLevelExp;
	}

	//保存登陆时的物品信息
	void SavePkgInfo(const PlayerInfo_3_Pkgs& pkgData)
	{
		m_fullPlayerInfo.pkgInfo = pkgData;			
	}

	//获取包裹信息
	const PlayerInfo_3_Pkgs* GetPkgInfo()
	{
	
		return &m_fullPlayerInfo.pkgInfo;
	}

	//获取装备
	const PlayerInfo_2_Equips* GetEquip(void)
	{
		return &m_fullPlayerInfo.equipInfo;
	}

	//取玩家身上的装备
	ItemInfo_i* GetEquipByPos( unsigned int equipPos )
	{
		if ( equipPos >= ARRAY_SIZE( m_fullPlayerInfo.equipInfo.equips ) )
		{
			return NULL;
		}
		return &(m_fullPlayerInfo.equipInfo.equips[equipPos]);
	}

	//获取技能
	const PlayerInfo_4_Skills* GetSkill(void)
	{
		return &m_fullPlayerInfo.skillInfo;
	}

	//获取BUFF
	const PlayerInfo_5_Buffers* GetBuffData(void)
	{
		return &m_fullPlayerInfo.bufferInfo;
	}

	//获取魔法等级
	unsigned int GetMagLevel(void)
	{
		return m_fullPlayerInfo.baseInfo.usMagicLevel;
	}

	//获取物理等级 
	unsigned int GetPhyLevel(void)
	{
		return m_fullPlayerInfo.baseInfo.usPhysicsLevel;
	}

	//获取物理技能可分配点数
	unsigned short GetPhyAllPoint(void)
	{
		return m_fullPlayerInfo.baseInfo.usPhysicsAllotPoint;
	}

	//获取魔法技能可分配点数
	unsigned short GetMagAllPoint(void)
	{
		return m_fullPlayerInfo.baseInfo.usMagicAllotPoint;
	}

	//获取物理技能分配的总点数
	unsigned short GetPhyTotalPoint(void)
	{
		return m_fullPlayerInfo.baseInfo.usPhyTotalGetPoint;
	}

	//获取魔法技能总点数
	unsigned short GetMagTotalPoint(void)
	{
		return m_fullPlayerInfo.baseInfo.usMagTotalGetPoint;
	}

	unsigned char GetMaxPhyLevelLimit(void)
	{
		return m_fullPlayerInfo.baseInfo.ucMaxPhyLevelLimit;
	}


	unsigned char GetMaxMagLevelLimit(void)
	{
		return m_fullPlayerInfo.baseInfo.ucMaxMagLevelLimit;
	}

	//获取物理技能的经验
	unsigned int GetPhyExp(void)
	{
		return m_fullPlayerInfo.baseInfo.dwPhysicsExp;
	}
	
	//获取魔法技能的经验
	unsigned int GetMagExp(void)
	{
		return m_fullPlayerInfo.baseInfo.dwMagicExp;
	}

	//获取脸
	UINT GetFace()
	{
		return m_fullPlayerInfo.baseInfo.usFace;	
	}


	UINT GetHair()
	{
		return m_fullPlayerInfo.baseInfo.usHair;
	}

	//获取头发颜色
	unsigned short GetHairColor()
	{
		return m_fullPlayerInfo.baseInfo.usHairColor;
	}


	//获取肖像
	UINT GetPortrait()
	{
		return m_fullPlayerInfo.baseInfo.usPortrait;
	}

	//获取善恶值
	short GetGoodEvilPoint();

	//是否是流氓状态
	bool IsRogueState()
	{
		return IsHaveRogueState();
	}

	//初始化OtherPlayerInfo的简明信息
	//void InitOtherPlayerConciseInfo();

	//装备道具时影响了OtherPlayerInfo
	//void EquipItemAffectOtherPlayerInfo( const ItemInfo_i& itemInfo , unsigned int equipIndex );

	//卸载道具时影响了OtherPlayerInfo信息
	//void UnEquipItemAffectOtherPlayerInfo( unsigned int unequipIndex );

	//获取玩家的LOGIN信息
	bool GetPlayerLoginInfo( PlayerInfoLogin& playerInfoLogin )
	{
		playerInfoLogin.uiID    = GetDBID();
		playerInfoLogin.usLevel = GetLevel();
		playerInfoLogin.ucRace  = GetRace();
		playerInfoLogin.usClass = GetClass();
		
		playerInfoLogin.equipInfo = (*GetEquip());
		if (IsFashionState())//时装态，用时装替换对应部位的装备
		{
			if ( ( E_BODY >= ARRAY_SIZE(playerInfoLogin.equipInfo.equips) )
				|| ( E_BODY >= ARRAY_SIZE(m_fashions.fashionData) )
				|| ( E_HAND >= ARRAY_SIZE(playerInfoLogin.equipInfo.equips) )
				|| ( E_HAND >= ARRAY_SIZE(m_fashions.fashionData) )
				|| ( E_FOOT >= ARRAY_SIZE(playerInfoLogin.equipInfo.equips) )
				|| ( E_FOOT >= ARRAY_SIZE(m_fashions.fashionData) )
				)
			{
				D_ERROR( "GetPlayerLoginInfo, E_BODY或E_HAND或E_FOOT越界\n" );
				return false;
			}
			//E_BODY,E_HEAD,E_FOOT的值与EQUIPTYPE_POS取出的值完全对应，可以不转，而且时装好象是要去掉，因此就保留原样吧，by dzj, 10.07.05;
			playerInfoLogin.equipInfo.equips[E_BODY] = m_fashions.fashionData[E_BODY];
			playerInfoLogin.equipInfo.equips[E_HAND] = m_fashions.fashionData[E_HAND];
			playerInfoLogin.equipInfo.equips[E_FOOT] = m_fashions.fashionData[E_FOOT];
		}
		
		playerInfoLogin.usMapID = GetMapID();
		playerInfoLogin.iPosX = GetPosX();
		playerInfoLogin.iPosY = GetPosY();
		playerInfoLogin.shFlag = GetSHFlag();
		playerInfoLogin.usFace = GetFace();
		playerInfoLogin.usHair = GetHair();
		playerInfoLogin.usHairColor = GetHairColor();
		playerInfoLogin.usPortrait = GetPortrait();
		playerInfoLogin.nameSize = (UINT)strlen( m_fullPlayerInfo.baseInfo.szNickName )+1;
		SafeStrCpy(playerInfoLogin.szNickName, m_fullPlayerInfo.baseInfo.szNickName ); //姓名
		return true;
	}

	bool GetPlayerChangeFrequentlyInfo( PlayerInfoLogin& playerInfoLogin )
	{
		playerInfoLogin.usLevel   = GetLevel();
		playerInfoLogin.usMapID   = GetMapID();
		playerInfoLogin.iPosX     = GetPosX();
		playerInfoLogin.iPosY     = GetPosY();
		playerInfoLogin.equipInfo = (*GetEquip());
		if(IsFashionState())//时装态，用时装替换对应部位的装备
		{
			if ( ( E_BODY >= ARRAY_SIZE(playerInfoLogin.equipInfo.equips) )
				|| ( E_BODY >= ARRAY_SIZE(m_fashions.fashionData) )
				|| ( E_HAND >= ARRAY_SIZE(playerInfoLogin.equipInfo.equips) )
				|| ( E_HAND >= ARRAY_SIZE(m_fashions.fashionData) )
				|| ( E_FOOT >= ARRAY_SIZE(playerInfoLogin.equipInfo.equips) )
				|| ( E_FOOT >= ARRAY_SIZE(m_fashions.fashionData) )
				)
			{
				D_ERROR( "GetPlayerChangeFrequentlyInfo, E_BODY或E_HAND或E_FOOT越界\n" );
				return false;
			}
			//E_BODY,E_HEAD,E_FOOT的值与EQUIPTYPE_POS取出的值完全对应，可以不转，而且时装好象是要去掉，因此就保留原样吧，by dzj, 10.07.05;
			playerInfoLogin.equipInfo.equips[E_BODY] = m_fashions.fashionData[E_BODY];
			playerInfoLogin.equipInfo.equips[E_HAND] = m_fashions.fashionData[E_HAND];
			playerInfoLogin.equipInfo.equips[E_FOOT] = m_fashions.fashionData[E_FOOT];
		}
		return true;
	}

	//玩家的PLAYERINFO
	const PlayerInfo_1_Base* GetPlayerInfo(void)
	{
		return &m_fullPlayerInfo.baseInfo;
	}

	//获取当前HP
	unsigned int GetCurrentHP()
	{
		return m_fullPlayerInfo.baseInfo.uiHP;
	}

	//获取当前MP
	unsigned int GetCurrentMP()
	{
		return m_fullPlayerInfo.baseInfo.uiMP;
	}

	unsigned int GetUIStr()
	{
		int nFinalStr = GetBuffStr() + GetStrength();
		return (nFinalStr < 0 ) ? 0 : nFinalStr;	
	}


	//应策划特殊要求,脚本里人物初始STR不需要提供
	int GetScriptStr()
	{
		return GetBuffStr();
	}

	
	//获取当前智力，影响智力的只有本身属性和技能BUFF
	unsigned int GetUIInt()
	{
		int nFinalInt = GetBuffInt() + GetIntelligence();
		return (nFinalInt<0) ? 0 : nFinalInt;	
	}

	int GetScriptInt()
	{
		return GetBuffInt();
	}
	
	//获取当前敏捷,影响敏捷的只有本身属性和技能BUFF
	unsigned int GetUIAgi()
	{
		int nFinalAgi = GetBuffAgi() + GetAgility();
		return ( nFinalAgi < 0 ) ? 0 : nFinalAgi;	
	}

	int GetScriptAgi()
	{
		return GetBuffAgi();
	}

	//获取当前体质
	unsigned int GetUIVit()
	{
		return GetVitality();//630版本，基本属性不再受技能影响;
	}

	//获取当前精神
	unsigned int GetUISpi()
	{
		return GetSpirit();//630版本，基本属性不再受技能影响;
	}

	unsigned int GetDrArg()
	{
		return m_buffManager.GetDrArg();	
	}

	float GetSpeed()
	{
		float spEffect = 0.0f;
		if( GetBuffSpeedAffect() > 0 )
		{
			spEffect = max( GetBuffSpeedAffect(), GetItemMaxSpeed() / 100.f ); 
		}else
		{
			spEffect = GetItemMaxSpeed() / 100.f + GetBuffSpeedAffect(); 
		}
		float finalSpeed = m_TmpProp.fSpeed * ( 1 +  spEffect );
		return ( finalSpeed > 10.0f )? 10.0f : finalSpeed;
	}

	bool SetSpeed( float fSpeed);  //此接口只作GM命令使用

	//增加技能，增加的技能肯定为有效的
	bool AddSkillByProperty(CMuxSkillProp* pSkillProp);
	
	//学习技能
	bool LearnSkillByID( TYPE_ID skillID )
	{
		CMuxSkillProp* pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( skillID );
		if( NULL == pSkillProp )
		{
			D_ERROR("要学习的技能属性找不到 找不到的技能属性ID号:%d\n", skillID );
			return false;
		}

		if ( pSkillProp->m_upgradeExp > GetExp() ||
			pSkillProp->m_upgradeMoney > GetSilverMoney() || //双币种判断，按姚斌：所有条件判定都使用银币
			pSkillProp->m_upgradeStr > GetStrength() ||
			pSkillProp->m_upgradeVit > GetVitality() ||
			pSkillProp->m_upgradeAgi > GetAgility() ||
			pSkillProp->m_upgradeSpi > GetSpirit() ||
			pSkillProp->m_upgradeInt > GetIntelligence())
		{
			D_DEBUG("人物属性未达到学习技能要求\n");
			return false;
		}
		return AddSkillByProperty( pSkillProp );
	}

	bool AddSkillByID( TYPE_ID skillID );
	void SetSkillData( unsigned int nIndex, TYPE_ID skillID, unsigned int uiExp );
	
	unsigned int AddSpPower(unsigned int nAddPower )
	{
		unsigned int addSpVal = 0;
		unsigned int spPowerPerBean = CSpSkillManager::GetPowerPerBean();
		unsigned int spPowerBean = (0 == spPowerPerBean) ? 0 : nAddPower/spPowerPerBean;
		CLogManager::DoSpPowerChange(this, LOG_SVC_NS::CT_PLUS, spPowerBean);//记日至

		if( IsSpPowerSpeedUp() )
		{
			nAddPower = (unsigned int)(nAddPower * GetBuffSpPowerUpRate() );
		}
		m_fullPlayerInfo.baseInfo.usSpPower += nAddPower;
		addSpVal = nAddPower;
		if( m_fullPlayerInfo.baseInfo.usSpPower > m_fullPlayerInfo.baseInfo.ucMaxSpLimit * CSpSkillManager::GetPowerPerBean() )
		{
			addSpVal -= (m_fullPlayerInfo.baseInfo.usSpPower - m_fullPlayerInfo.baseInfo.ucMaxSpLimit * CSpSkillManager::GetPowerPerBean());
			m_fullPlayerInfo.baseInfo.usSpPower = m_fullPlayerInfo.baseInfo.ucMaxSpLimit * CSpSkillManager::GetPowerPerBean();
		}

		MGRoleattUpdate roleUpdate;
		roleUpdate.bFloat = false;
		roleUpdate.changeTime = 0;
		roleUpdate.lChangedPlayerID = GetFullID();
		roleUpdate.nNewValue = this->GetSpPower();
		roleUpdate.nType = RT_SP;
		roleUpdate.nAddType = MGRoleattUpdate::TYPE_SET;
		SendPkg<MGRoleattUpdate>( &roleUpdate );
		return addSpVal;
	}



	//设置无敌
	inline void SetAttackFlag(bool bEnableAttack)
	{
		m_bEnableAttack = bEnableAttack;
	}

	//获取是否能攻击
	inline bool GetAttackFlag(void)
	{
		return m_bEnableAttack;
	}

	inline FullPlayerInfo* GetFullPlayerInfo(void)
	{
		return &m_fullPlayerInfo;
	}

	//取骑乘装备位置装备对应ItemInfo_i，没有装备时返回空
	inline ItemInfo_i* GetValidDriveEquip()
	{
		ItemInfo_i* pItemInfoI = NULL;

		//取玩家身上装配的某型装备,不可用本函数取单手装备，因为其ItemInfo_i有两个；
		bool isGetOK = GetPlayerGearEquip( E_DRIVE, pItemInfoI );
		if ( ( !isGetOK )
			|| ( NULL == pItemInfoI )
			)
		{
			D_ERROR( "GetValidDriveEquip, GetPlayerGearEquip失败，玩家%s\n", GetAccount() );
			return false;			
		}
		if ( PlayerInfo_2_Equips::INVALIDATE == pItemInfoI->uiID )
		{
			return NULL;//该位置无骑乘装备；
		}
		return pItemInfoI;
	}

	//取骑乘装备位置装备对应ItemInfo_i，可能会没有装备
	inline ItemInfo_i* GetPossiDriveEquip()
	{
		ItemInfo_i* pItemInfoI = NULL;

		//取玩家身上装配的某型装备,不可用本函数取单手装备，因为其ItemInfo_i有两个；
		bool isGetOK = GetPlayerGearEquip( E_DRIVE, pItemInfoI );
		if ( ( !isGetOK )
			|| ( NULL == pItemInfoI )
			)
		{
			D_ERROR( "GetValidDriveEquip, GetPossiDriveEquip失败，玩家%s\n", GetAccount() );
			return NULL;			
		}
		return pItemInfoI;
	}

	inline AttributeEffect* GetBuffEffect(void)
	{
		return m_buffManager.GetBuffEffect();
	}

public:
	///是否已离开（保留期）
	inline bool IsLeaveDelay() { return m_isLeaveDelay; }
	///设置玩家为已离开进入保留期
	inline void SetLeaveDelay() { m_isLeaveDelay = true; }

private:
	bool m_isLeaveDelay;//玩家是否已离开本mapsrv，当前处于保留期，若是，则向该玩家发送的一切无关消息都应屏蔽，但save playerinfo应发出；	

public:
	//#ifdef AI_SUP_CODE
	//	///怪物将自己注册到CPlayer,以便接收该玩家的后续事件；
	//	bool RegisterMonster( CMonster* pMonster );
	//
	//	///怪物将自己反注册，表示以后不需要再接收该玩家的后续事件；
	//	bool UnRegisterMonster( CMonster* pMonster )
	//	{
	//		if ( NULL == pMonster )
	//		{
	//			return false;
	//		}
	//		for ( vector< MonsterEventReged >::iterator iter=m_vecRegedMonsters.begin();
	//			iter != m_vecRegedMonsters.end(); ++ iter )
	//		{
	//			if ( iter->GetMonster() == pMonster )
	//			{
	//				iter->Invalidate();//置为无效，待后删除；
	//				break;
	//			}
	//		}
	//		return true;
	//	}
	//
	//	///检查怪物是否已注册，是否要向怪物通知相关事件；
	//	bool IsMonsterReged( CMonster* pMonster )
	//	{
	//		if ( NULL == pMonster )
	//		{
	//			return false;
	//		}
	//		for ( vector< MonsterEventReged >::iterator iter=m_vecRegedMonsters.begin();
	//			iter != m_vecRegedMonsters.end(); ++ iter )
	//		{
	//			if ( iter->GetMonster() == pMonster )
	//			{
	//				return iter->IsValid();
	//			}
	//		}
	//		return false;
	//	}
	//
	//	///通知已注册玩家相应事件发生；
	//    bool NotifyRegedMonsterAiEvent( MONSTER_EVENT aiEvent );
	//#endif //AI_SUP_CODE


	//计算游戏中的非存盘属性
	bool CalculateTmpProp();

	//10.09.20已废弃//根据等级计算属性
	//bool ReloadLevelProp();
	
	//10.09.20已废弃//根据物理经验计算相关属性
	//bool ReloadPhyProp();

	//10.09.20已废弃//根据魔法经验计算相关属性
	//bool ReloadMagProp();

	//重新计算二级属性
	bool ReloadSecondProp();

	///设置玩家HP;
	void SetPlayerMp( unsigned int inMp )
	{
		OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(MP)
		SetNeedNotiTeamMember();

		m_fullPlayerInfo.baseInfo.uiMP = inMp;
		if ( m_fullPlayerInfo.baseInfo.uiMP > GetMaxMP() )
		{
			m_fullPlayerInfo.baseInfo.uiMP = GetMaxMP();
		}

		return;
	}

	//增加MP
	unsigned int AddPlayerMp(unsigned int inMP )
	{
		OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(MP)
		SetNeedNotiTeamMember();

		unsigned int addVal = 0;
		if( m_fullPlayerInfo.baseInfo.uiMP + inMP > GetMaxMP() )
		{
			int val = GetMaxMP() - GetCurrentMP();
			addVal = (val<=0)?0:val;
			SetPlayerMp( GetMaxMP() );
			return addVal;
		}

		m_fullPlayerInfo.baseInfo.uiMP += inMP;		
		return inMP; 
	}


	///设置玩家HP;
	void SetPlayerHp( unsigned int inHp )
	{
		OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(HP)
		SetNeedNotiTeamMember();

		m_fullPlayerInfo.baseInfo.uiHP = inHp;
		if ( m_fullPlayerInfo.baseInfo.uiHP > GetMaxHP() )
		{
			m_fullPlayerInfo.baseInfo.uiHP = GetMaxHP();
		}

		return;
	}

	//增加HP
	unsigned int AddPlayerHp(unsigned int uiHp )
	{
		OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(HP)
		SetNeedNotiTeamMember();

		unsigned int addVal = 0;
		if( m_fullPlayerInfo.baseInfo.uiHP + uiHp > GetMaxHP() )
		{
			int val = GetMaxHP() - GetCurrentHP();
			addVal = (val<=0)?0:val;
			SetPlayerHp( GetMaxHP() );
			return addVal;
		}
		m_fullPlayerInfo.baseInfo.uiHP += uiHp;

		return uiHp; 
	}

	//增加人物的经验值
	void AddExp(unsigned int nExp, int addType, bool isExpCritical/*是否经验*/ );		 
	//10.09.20已废弃////增加物理的经验值
	//void AddPhyExp(unsigned int nExp);
	////增加魔法的经验值
	//void AddMagExp(unsigned int nExp);

	//10.09.20已废弃//bool CleanPhySkillPoint();   //清洗物理技能
	//bool CleanMagSkillPoint();	 //清洗魔法技能
	//10.09.20已废弃//bool CleanSinglePhySkill( CMuxPlayerSkill* pSkill );
	//bool CleanSingleMagSkill( CMuxPlayerSkill* pSkill );

	unsigned int GetRacePrestige()  {   return m_fullPlayerInfo.baseInfo.racePrestige;  }
	void AddRacePrestige( unsigned int addValue )	 
	{
		m_racePrestigeManager.AddRacePrestige( addValue );
	}

	bool SubRacePrestige( unsigned int subValue )
	{
		return m_racePrestigeManager.SubRacePrestige( subValue );
	}

	TYPE_ID GetPlayerUID()
	{
		return m_fullPlayerInfo.baseInfo.uiID;
	}

	///死亡处理；
	void DieProcess( CPlayer* pKiller, const PlayerID& killerPlayerID );

	//减少HP
	void SubPlayerHp( int nSubHP, CPlayer* pKiller, const PlayerID& attackID );

	//减玩家MP，若减成功返回true，否则返回false;
	bool SubPlayerMp( int subVal/*减MP绝对值*/, float subPercent/*减MP百分比*/ );


	///重置玩家HPMP，只在异常情况下，由OnlineProc调用；
	inline void ResetHPMP()
	{
		SetPlayerHp( 0 );
		SetPlayerMp( 0 );
	}


	//杀掉别人的处理
	void KillerOther( CPlayer* pBeKiller );
	//当杀死NPC的时候
	void OnKillNpc( CMonster* pDeadNpc );

	//战斗开始,检测是否进入流氓状态
	void FightStartCheckRogueState( CPlayer* pTarget, bool isRogue );
	//取得玩家的善恶值类型
	GOODEVIL_MODE GetGoodEvilMode();
	//开始流氓状态
	void StartRogueState();
	//结束流氓状态
	void EndRogueState();
	//被杀的处理
	void BeKilled( CPlayer* pKiller );

	////减少MP
	//void SubMP(unsigned int nSubMP)
	//{
	//	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(MP)
	//	SetNeedNotiTeamMember();

	//	if( nSubMP > GetCurrentMP() )
	//	{
	//		//如果大于则什么也不做返回
	//		SetPlayerMp( 0 );
	//		return;
	//	}

	//	m_fullPlayerInfo.baseInfo.uiMP -= nSubMP;	//减去MP
	//	return;		
	//}

	///获取银币数
	unsigned int GetSilverMoney(void)
	{
		return m_fullPlayerInfo.baseInfo.dwSilverMoney;
	}

	///获取金币数
	unsigned int GetGoldMoney()
	{
		return m_fullPlayerInfo.baseInfo.dwGoldMoney;
	}

	///是否使用金币
	bool IsUseGoldMoney()
	{
		return (0 != m_UseMoneyType);
	}

	///设置使用的钱币类型
	bool SetUseMoneyType( unsigned char moneyType /*0:银币，1:金币*/)
	{
		m_UseMoneyType = moneyType;
		return true;
	}
	
	///减银币
	bool SubSilverMoney( unsigned int moneyNum );
	bool SubGoldMoney( unsigned int moneyNum );

	///加银币
	bool AddSilverMoney( unsigned int uiMoney, int addType = 0 );		//0表示普通获得(需要防沉迷验证) 1表示交易获得
	bool AddGoldMoney( unsigned int uiMoney, int addType = 0 );

	///设置银币数目
	bool SetSilverMoney( unsigned int moneyCount );
	bool SetGoldMoney( unsigned int moneyCount );

	///通知银币数量
	bool NotifySilverMoney();
	///通知金币数量
	bool NotifyGoldMoney();

private:
	unsigned char m_UseMoneyType;/*0:银币，1:金币*/

public:

	PlayerInfo_1_Base& GetDBProp(void)
	{
		return m_fullPlayerInfo.baseInfo;
	}
	
	PlayerSecondProp& GetTmpProp(void)
	{
		return m_TmpProp;
	}

	//获得处理器
	inline CGateSrv* GetProc(void)
	{
		return m_uniGatePtr.GetInnerGateSrvPtr();
	}	

	///发送NPC对话界面消息，由CNormalScript调用；
	void SendNpcUiInfo( const char* szChat );

	///发送Item对话界面消息，由CNormalScript调用；
	void SendItemUiInfo( const char* szChat );

	///发送Area对话界面消息,由CNormalScript调用；
	void SendAreaUiInfo( const char* szChat );

	//09.04.28,取消工会任务新实现,///发送Task对话界面消息，由CNormalScript调用；
	//void SendTaskUiInfo( const char* szChat );

	///发送系统消息
	void SendSystemChat( const char* szChat );

	///发送玩家死亡倒计时消息
	void SendDyingPkg( unsigned long duraSec );

	//void NotifyOtherPlayerInfo();

	///上次下线时已过复活操作提示时间（由于下线保护时间），再次上线时直接将玩家置到复活点而不再与客户端交流复活相关消息；
	void OnlineDirectRebirth();

	///选择一个攻城战复活点复活
	void OnSelectWarRebirthOption(BYTE option = 4/*内城:1, 外城:2, 缺省:4*/);

	///死于当前攻城战(玩家死亡时的那次攻城战)?
	bool DiedInCurrentWar(void);

	//获取攻城战复活点选项
	BYTE GetWarRebirthOption(void);

	/////发送玩家重生点信息；
 //   void SendRebirthInfoPkg( TYPE_ID sequenceID );

	///新的直接复活，省去中间步骤；
	void NewDirectRebirth();

	///检查玩家是否死亡，如果已到死亡复活时间，则复活，如果检查过后玩家仍为死亡状态，则返回真，否则返回假；
    bool CheckDie(); 

	/////玩家复活至默认复活点,但暂时不真正加入复活点，只在玩家身上记录新点信息，以便稍后OnRebirthReady时在地图上出现，同时通知玩家复活点信息，以便玩家加载地图;
	//void OnDefaultRebirth( TYPE_ID sequenceID )
	//{
	//	//玩家要求复活至默认复活点;
	//	SendRebirthInfoPkg( sequenceID );
	//}

	/////玩家真正加入默认复活点
	//void OnRebirthReady( TYPE_ID sequenceID );

	//是否是队长
	bool IsTeamCaptain();

	//是否有组
	bool IsInTeam();

	//获取组队
	CGroupTeam* GetGroupTeam();

	//通知HP的变化
	void NotifyHpChange();

	//最大血量发生变化
	void NotifyMaxHPChange();

	//最大魔量发生变化
	void NotifyMaxMPChange();

	//通知MP的变化
	void NotifyMpChange();

	//通知速度的变化
	void NotifySpeedChange();

	//通知下一级需要经验
	void NotifyNextLevelExp();

//09.04.28,取消工会任务新实现,	//////////////////////////////////////////////////////////////////////////////////////
//	//玩家随机任务对话相关...
//private:
//	//设置玩家可以使用工会任务对话标记，确保玩家通过合法的路径到达工会任务选择，防止玩家绕过NPC对话，直接使用工会任务选择
//	//  当然，玩家可以在某一时刻到达工会任务选择，然后不选工会任务，直至稍后其觉得合适时再选择工作任务，若需要防止此种情况，则需另做判断；
//	bool m_bCanSelectRandomTask;
//
//public:
//	void SetCanSelectRandomTask()
//	{
//		m_bCanSelectRandomTask = true;
//	}
//	void ClearCanSelectRandomTask()
//	{
//		m_bCanSelectRandomTask = false;
//	}
//	bool CanSelectRandomTask()
//	{
//		return m_bCanSelectRandomTask;
//	}
//	///玩家随机任务对话，nOption为玩家的对话选择；
//	bool OnChatToTask( unsigned int taskID, int nOption, bool isSrvSet = false/*是否服务器端主动调用，还是客户端发来消息导致调用*/ )
//	{
//		if ( !(m_taskChatInfo.SetTaskChatTarget( taskID, nOption, isSrvSet )) )
//		{
//			D_WARNING( "%s不处于任务对话状态而试图使用任务对话\n", GetAccount() );
//			return false;
//		}
//		return m_taskChatInfo.OnTaskChatOption( this, nOption );				
//	}
//
//	///设玩家的交谈阶段，初始点击NPC之时设为0，其余时候由脚本调用；
//	void SetTaskChatStage( unsigned long curStage )
//	{
//		m_taskChatInfo.SetTaskChatStage( curStage );
//	}
//
//	///取玩家的交谈阶段，由脚本引擎调用；
//	unsigned long GetTaskChatStage()
//	{
//		return m_taskChatInfo.GetTaskChatStage();
//	}
//	//...玩家随机任务对话相关
//	//////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////
	//玩家与NPC对话相关...
	///玩家与NPC对话，nOption为玩家的对话选择；
	bool OnChatToNpc( CMonster* pMonster, int nOption )
	{
		m_npcChatInfo.SetNpcChatTarget( pMonster, nOption );
		return m_npcChatInfo.OnNpcChatOption( this, nOption );				
	}

	bool OnChatToNpc2( CMonster* pMonster ,int nOption, const char* szChat );

	///设玩家的交谈阶段，初始点击NPC之时设为0，其余时候由脚本调用；
	void SetNpcChatStage( unsigned long curStage )
	{
		m_npcChatInfo.SetNpcChatStage( curStage );
	}

	///取玩家的交谈阶段，由脚本引擎调用；
	unsigned long GetNpcChatStage()
	{
		return m_npcChatInfo.GetNpcChatStage();
	}

	///取玩家的交谈对象；
	CMonster* GetNpcChatTarget()
	{
		return m_npcChatInfo.GetNpcChatTarget();
	}
	//...玩家与NPC对话相关
	//////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////
	//玩家与Item对话相关...
	///玩家与Item对话，nOption为玩家的对话选择；
	bool OnChatToItem( CBaseItem* pItem, int nOption )
	{
		m_itemChatInfo.SetItemChatTarget( pItem, nOption );
		return m_itemChatInfo.OnItemChatOption( this, nOption );				
	}

	///设玩家的交谈阶段，初始点击NPC之时设为0，其余时候由脚本调用；
	void SetItemChatStage( unsigned long curStage )
	{
		m_itemChatInfo.SetItemChatStage( curStage );
	}

	///取玩家的交谈阶段，由脚本引擎调用；
	unsigned long GetItemChatStage()
	{
		return m_itemChatInfo.GetItemChatStage();
	}

	///取玩家的交谈对象；
	CBaseItem* GetItemChatTarget()
	{
		return m_itemChatInfo.GetItemChatTarget();
	}
	//...玩家与Item对话相关
	//////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	//玩家与Area对话相关
	void SetAreaChatStage( unsigned long curStage )
	{
		m_areaChatInfo.SetAreaTaskStage( curStage );
	}

	bool OnChatToArea( int nOption )
	{
		m_areaChatInfo.SetAreaChatTarget( nOption );
		return m_areaChatInfo.OnAreaTaskOption( this, nOption );
	}

	//取玩家的交谈阶段,由脚本引擎调用
	unsigned long GetAreaChatStage()
	{
		return m_areaChatInfo.GetAreaTaskState();
	}

	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////
	//玩家逻辑出错时的处理相关。。。
	void SetWaitToDelFlag()
	{
		m_bIsWaitToDel = true;
	}
	bool IsWaitToDel()
	{
		return m_bIsWaitToDel;
	}
	//玩家逻辑出错时的处理相关。。。
	//////////////////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////////////////
	//战斗开始相关。。。
		/*
		//玩家此时还未真正进入战斗，从现在开始到向客户端返回appear消息之前，如果有任意一步失败,
		//则:向gatesrv返回MGError消息的同时，要同时将pPlayer实例删去，因为后续gatesrv不会再发来leave消息了；
		*/
	void SetPreFightFlag()
	{
		m_bPreFight = true;
	}
	void ResetPreFightFlag()
	{
		m_bPreFight = false;
	}
	bool IsPreFight()
	{
		return m_bPreFight;
	}
	/////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////
	////跳地图相关设置...；
	//void SetSwitchMapFlag()
	//{
	//	m_bIsInSwitchMap = true;
	//}
	//void ResetSwitchMapFlag()
	//{
	//	m_bIsInSwitchMap = false;
	//}
	//bool IsInSwitchMap()
	//{
	//	return m_bIsInSwitchMap;
	//}
	////...跳地图相关设置；
	////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////
	//玩家full info完整性相关...;
	void SetFullInfoFalse()
	{
		m_bIsFullInfoOk = false;
	}

	void SetFullInfoTrue()
	{
		m_bIsFullInfoOk = true;
		//一旦信息收完整，就置玩家简略信息中的不变部分
		SetUnchangeSimpleInfo();
		//一旦信息收完整，就置需要通知队友标记
		SetNeedNotiTeamMember();
	}

	bool IsInfoFull()
	{
		return m_bIsFullInfoOk;
	}
	//...玩家full info完整性相关;
	//////////////////////////////////////////////////////////////////////////////////////
	const ACE_Time_Value& GetNextActionTime()
	{
		return m_nextActionTime;
	}

	void SetNextActionTime( const ACE_Time_Value& nextTime)
	{
		m_nextActionTime = nextTime;
	}

	/// 玩家交易
	PlayerID RecvFromPlayer(void)
	{
		return m_recvFromPlayer;
	}

	void RecvFromPlayer(PlayerID  playerID)
	{
		m_recvFromPlayer = playerID;
	}

	CTradeInfo * TradeInfo(void)
	{
		return CTradeInfoManager::GetTradeInfo(m_nTradeInfo);
	}

	void TradeInfo(unsigned int TradeID)
	{
		m_nTradeInfo = TradeID;
	}

	void DelTradeInfo()
	{
		CTradeInfoManager::DelTradeInfo(m_nTradeInfo);
	}

	CUnion * Union(void)
	{
		return m_pUnion;
	}

	void Union(CUnion *pUnion)
	{
		m_pUnion = pUnion;
	}


	AttributeEffect* GetUnionEffect()
	{
		if( NULL == m_pUnion )
		{
			return NULL;
		}
		return m_pUnion->GetUnionEffect();
	}

	void MakeUnfinishUnionTaskFail(void)
	{
		for(vector<CNormalTask*>::iterator iter=m_arrTasks.begin(); iter!=m_arrTasks.end(); ++iter)
		{
			if((NULL != *iter) && 
				((*iter)->GetTaskStat() != NTS_FINISH) && 
				((*iter)->IsUnionTask()))
			{	
				//只标志任务失败，其他走任务删除流程
				this->SetTaskStat((*iter)->GetTaskID(), NTS_FAIL);
			}
		}

		return;
	}

	bool IsUnionLeader(void)
	{
		if(NULL == m_pUnion)
			return false;

		return m_pUnion->IsUnionLeader(m_fullPlayerInfo.baseInfo.uiID);
	}

	void WithdrawalUnionFashion(void);

public:
	///定时执行的玩家相关事件；
	void PlayerTimerProc();

	//上线初试化新的buff
	bool InitMuxBuff();

	//通过一个ID来创建BUFF
	bool CreateMuxBuffByID( unsigned int buffID, unsigned int buffTime, const PlayerID& createID );
	bool CreateMuxBuff( MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID );
	bool DeleteMuxBuff( unsigned int buffID );

	//设置当个BUFF的存储数据
	bool SetSingleBuffData( unsigned buffIndex, unsigned int buffID, unsigned buffTime );

	///清玩家身上的计数器；
	void ClearCounters()
	{
		CNormalCounter* pCounter = NULL;
		for ( int i=0; i<CT_INVALID+1; ++i )
		{
			for ( vector<CNormalCounter*>::iterator iter=m_arrCounters[i].begin(); iter!=m_arrCounters[i].end(); ++iter )
			{
				pCounter = *iter;
				if ( NULL != pCounter )
				{
					delete pCounter; pCounter = NULL;
				}			
			}
			m_arrCounters[i].clear();
		}
	}

	///给玩家身上挂一个计数器；
	bool AddCounter( unsigned int counterIdType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount, unsigned int taskID )
	{
		if(  FindPlayerTask( taskID )!= NULL )
		{
			CNormalCounter* pCounter = NEW CNormalCounter;
			if ( ! (pCounter->Init( counterIdType, counterIdMin, counterIdMax, targetCount, taskID )) )
			{
				//建指定类型计数器失败；
				delete pCounter; pCounter = NULL;
				return false;
			}

			COUNTER_TARGET_TYPE ctType = pCounter->GetCounterType();
			m_arrCounters[ctType].push_back( pCounter );//加到相应类型计数器队列中去；

			if ( ctType == CT_COLLECT )
				pCounter->CheckItemPkgCounterNum( this );
			
			return true;
		}
		return false;
	}

	/*bool RefreshCollectCounterNum()
	{
		std::vector<CNormalCounter *>& counterVec = m_arrCounters[CT_COLLECT];

		if ( counterVec.empty() )
			return false;

		for ( std::vector<CNormalCounter *>::iterator lter = counterVec.begin(); lter!= counterVec.end(); ++lter )
		{
			CNormalCounter* pCounter = *lter;
			if ( pCounter )
				pCounter->CheckItemPkgCounterNum( this );
		}

		return true;
	}*/

	//增加原来一个未完成的计数器,并恢复计数到原来的数目
	bool AddUnFinishCounter( unsigned int counterIdType, unsigned int counterIdMin, unsigned int counterIdMax, unsigned int targetCount, unsigned int taskID, unsigned int readyFinishCount )
	{
		CNormalCounter* pCounter = NEW CNormalCounter;
		if ( ! (pCounter->Init( counterIdType, counterIdMin, counterIdMax, targetCount, taskID )) )
		{
			//建指定类型计数器失败；
			delete pCounter; pCounter = NULL;
			return false;
		}

		COUNTER_TARGET_TYPE ctType = pCounter->GetCounterType();
		m_arrCounters[ctType].push_back( pCounter );//加到相应类型计数器队列中去；

		if ( ctType == CT_COLLECT )
		{
			//如果是收集的,直接检查包裹中的道具个数
			pCounter->CheckItemPkgCounterNum( this );//恢复保存的计数个数,
			
		}
		else//恢复保存的计数个数,
			pCounter->SetCurCount( readyFinishCount );

		//检查是否达到了完成状态
		pCounter->CheckCounterIsFinish( this );
		return true;
	}

	//组队时，组队成员杀死了怪物，NPC计数器改变了
	bool TeamMemberKillNpcEvent( unsigned int reasonObjID/*事件相关物体类型*/, unsigned int reasonObjNum/*事件相关物体数量*/ );

	///特定事件发生时，更新玩家身上所挂计数器；
	bool TryCounterEvent( COUNTER_TARGET_TYPE ctType/*发生的事件类型*/, unsigned int reasonObjID/*事件相关物体类型*/, unsigned int reasonObjNum/*事件相关物体数量*/ );
	
	//当丢弃道具时，触发道具搜集计数器
	bool OnDropItemCounterEvent( const ItemInfo_i& dropItem );

	//获取任务ID对应的计数器
	CNormalCounter * GetTaskCounter( unsigned int taskID,COUNTER_TARGET_TYPE ctType );

	///清玩家身上的任务；
	void ClearTasks()
	{
		CNormalTask* pTask = NULL;
		for ( vector<CNormalTask*>::iterator iter=m_arrTasks.begin(); iter!=m_arrTasks.end(); ++iter )
		{
			pTask = *iter;
			if ( NULL != pTask )
			{
				delete pTask; pTask = NULL;
			}			
		}
		m_arrTasks.clear();
	}

	///给玩家身上挂一个任务；
	bool AddTask( unsigned int taskID );

	///加载一个已存在的任务
	bool LoadTask( unsigned int taskID );

	///通知客户端任务失败倒计时；
	bool NotifyTaskFailTime( UINT taskID, UINT failTime );

	///将玩家身上指定类型的任务置为特定状态(,???,如果玩家可以接同类任务多次怎么办?,邓子建检查)
	bool SetTaskStat( unsigned int taskID, ETaskStatType inStat, bool logFlag = true/*是否记日志标记.此函数被多个函数调用,但有些函数调用不用记日至,如LoadTask*/ );

	///NPC检查玩家身上特定任务的状态；
	ETaskStatType GetTaskStatus( unsigned int taskID ) 
	{ 
		CNormalTask* pTask = FindPlayerTask( taskID );
		if ( NULL == pTask )
		{
			//玩家身上没有该类型任务；
			return NTS_NOACCEPT;
		}
		return pTask->GetTaskStat();
	};

	///玩家身上是否挂有某任务；
	CNormalTask* FindPlayerTask( unsigned int taskID )
	{
		for ( vector<CNormalTask*>::iterator iter=m_arrTasks.begin(); iter!=m_arrTasks.end(); ++iter )
		{
			if ( ( NULL != *iter ) 
				&& ( taskID  == (*iter)->GetTaskID() )
				)
			{
				return *iter;
			}
		}
		return NULL;
	}

	//玩家请求删除任务
	bool DeleteTask( unsigned int taskID );


	//更新BUFF的停表时间
	bool BuffDataArrayCheck( unsigned int index );

	//关于晕眩和束缚状态的查询
	bool IsFaint()
	{
		return m_buffManager.IsFaint();
	}

	void SetFaintFlag(bool bFlag)
	{
		m_buffManager.SetFaint( bFlag );
	}

	bool IsBondage()
	{
		return m_buffManager.IsBondage();
	}

	void SetBondageFlag(bool bFlag)
	{
		m_buffManager.SetBondage( bFlag );
	}

	bool IsSelfExplosion()
	{
		return m_buffManager.IsSelfExplosion();
	}

	unsigned int GetSelfExplosionArg()
	{
		return m_buffManager.GetSelfExplosionArg();
	}

	void SetSelfExplosionInfo( bool bFlag, unsigned int arg )
	{
		m_buffManager.SetSelfExplosionInfo( bFlag, arg );
	}


	void SetSpeedDown( bool bFlag );	//设置减速标志
	bool IsSpeedDown();		//是否被减速
	
	//玩家自爆
	bool OnSelfExplosion();
	//被别的玩家自爆
	void OnBeExplosion( CPlayer *pExplosionPlayer, CMuxSkillProp* pSkillProp, unsigned int exArg );

public:
	inline void SetNeedNotiTeamMember() { m_isNeedNotiTeamMembers = true; };//设置需要通知队友标记；
	inline bool IsNeedNotiTeamMember() { return m_isNeedNotiTeamMembers; }//是否需要通知队友更新信息；
private:
	void SendSelfInfoToOtherMembers();//更新信息通知队友；

private:
	bool m_isNeedNotiTeamMembers;//是否有更新信息需要通知队友

public:
	//新版buff
	void AddMuxBuffEffect( MuxBuffProp* pBuffProp );
	void AddSingleMuxBuffEffect( MuxBuffProp* pBuffProp );
	void DelMuxBuffEffect( MuxBuffProp* pBuffPro, unsigned int buffLevel );
	void DelSingleMuxBuffEfect( MuxBuffProp* pBuffProp, unsigned int buffLevel );

	int GetBuffStr()
	{
		return m_buffManager.GetBuffStr();
	}

	int GetBuffInt()
	{
		return m_buffManager.GetBuffInt();
	}

	int GetBuffAgi()
	{
		return m_buffManager.GetBuffAgi();
	}
		
	int GetBuffAoeRange()
	{
		return m_buffManager.GetBuffAoeRange();
	}

	int GetBuffPhyAtk()
	{
		return m_buffManager.GetBuffPhyAtk();
	}

	int GetBuffMagAtk()
	{
		return m_buffManager.GetBuffMagAtk();
	}

	int GetBuffAtkDistance()
	{
		return m_buffManager.GetBuffAtkDistance();
	}

	float GetDotDmgUp()
	{
		return m_buffManager.GetDotDmgUp();
	}
	
	int GetBuffPhyHit()
	{
		return m_buffManager.GetBuffPhyHit();
	}

	int GetBuffMagHit()
	{
		return m_buffManager.GetBuffMagHit();
	}

	int GetBuffPhyDef()
	{
		return m_buffManager.GetBuffPhyDef();
	}

	int GetBuffMagDef()
	{
		return m_buffManager.GetBuffMagDef();
	}

	int GetBuffMaxHpUp()
	{
		return m_buffManager.GetBuffMaxHpUp();
	}

	float GetBuffBaseHpUp()
	{
		return m_buffManager.GetBuffBaseHpUp();
	}

	int GetBuffPhyCriLevel()
	{
		return m_buffManager.GetBuffPhyCriLevel();
	}

	int GetBuffMagCriLevel()
	{
		return m_buffManager.GetBuffMagCriLevel();
	}

	float GetBuffSpeedAffect()
	{
		return m_buffManager.GetBuffSpeedAffect();
	}

	float GetBuffSpPowerUpRate()
	{
		return m_buffManager.GetBuffSpPowerUpRate();
	}

	int GetBuffPhyCriDamAdd()
	{
		return m_buffManager.GetBuffPhyCriDamAdd();
	}

	int GetBuffMagCriDamAdd()
	{
		return m_buffManager.GetBuffMagCriDamAdd();
	}

	//是否伤害反弹
	bool IsDamageRebound()
	{
		return m_buffManager.IsDamageRebound();
	}

	void AddMaxHp(int nAddMaxHp)
	{
		AddPlayerHp( nAddMaxHp );

		MGRoleattUpdate updateHpMsg;
		updateHpMsg.bFloat = false;
		updateHpMsg.changeTime = 0;
		updateHpMsg.nAddType = MGRoleattUpdate::TYPE_SET;
		updateHpMsg.nNewValue = GetCurrentHP();
		updateHpMsg.lChangedPlayerID = GetFullID();
		updateHpMsg.nType = RT_HP;
		SendPkg<MGRoleattUpdate>( &updateHpMsg );
	}

	void SubMaxHp( int nSubMaxHp )
	{
		//if( (unsigned int)nSubMaxHp >= GetMaxHP() )
		//{
		//	m_TmpProp.lMaxHP = 1;
		//	SetPlayerHp( 1 );
		//	return;
		//}
		
		if( GetCurrentHP() > GetMaxHP() )
		{
			SetPlayerHp( GetMaxHP() );
		
			MGRoleattUpdate updateMsg;
			updateMsg.bFloat = false;
			updateMsg.changeTime = 0;
			updateMsg.nAddType = MGRoleattUpdate::TYPE_SET;
			updateMsg.nNewValue = GetCurrentHP();
			updateMsg.lChangedPlayerID = GetFullID();
			updateMsg.nType = RT_HP;
			SendPkg<MGRoleattUpdate>( &updateMsg );
		}
	}
	
	
	void AddSpExp(unsigned int uiSpExp);
	

	TYPE_UI32 GetSpExp()
	{
		return m_fullPlayerInfo.baseInfo.usSpExp;
	}


	TYPE_UI16 GetSpPower()
	{
		return m_fullPlayerInfo.baseInfo.usSpPower;
	}

	USHORT GetSpLevel()
	{
		return m_spInfo.usSpLevel;
	}



	void SubSpPower(unsigned int uiSpExp )
	{
		unsigned int spPowerPerBean = CSpSkillManager::GetPowerPerBean();
		unsigned int spPowerBean = (0 == spPowerPerBean) ? 0 : uiSpExp/spPowerPerBean;
		CLogManager::DoSpPowerChange(this, LOG_SVC_NS::CT_MINUS, spPowerBean);//记日至

		if( m_fullPlayerInfo.baseInfo.usSpPower < uiSpExp )
		{
			D_WARNING("当前的SP能量不足 现在的SP能量:%d 要消耗的SP能量:%d\n",m_fullPlayerInfo.baseInfo.usSpPower, uiSpExp );
			return;
		}
		m_fullPlayerInfo.baseInfo.usSpPower -= uiSpExp;
	}

	//物暴击等级
	int GetPhyCriLevel()
	{
		int nFinalLevel = m_itemTotalProp.PhysicsCritical + GetBuffPhyCriLevel();
		return ( nFinalLevel < 0)? 0: nFinalLevel;
	}

	//魔暴等级
	int GetMagCriLevel()
	{
		int nFinalLevel = m_itemTotalProp.MagicCritical + GetBuffMagCriLevel();
		return ( nFinalLevel < 0)? 0: nFinalLevel;
	}

	int GetPhyCriDamage()
	{
		int nFinalDamage = m_itemTotalProp.PhysicsCriticalDamage + GetBuffPhyCriDamAdd();
		return ( nFinalDamage < 0)? 0: nFinalDamage;
	}

	int GetMagCriDamage()
	{
		int nFinalDamage = m_itemTotalProp.MagicCriticalDamage + GetBuffMagCriDamAdd();
		return ( nFinalDamage < 0)? 0: nFinalDamage;
	}

	void InitSpInfo()
	{
		CSpSkillManager::FindSpInfo( m_fullPlayerInfo.baseInfo.usSpExp, m_spInfo ); 
	}

	///给玩家增加一个跟随怪物；
	bool AddFollowingMonster( unsigned int monsterType, unsigned int taskID );

	///设置跟随本玩家的怪物；
	void SetFollowingMonster( CMonster* pFollowingMonster );

	void ClearFollowingMonster()
	{
		m_pFollowingMonster = NULL;
	}

	CMonster* GetFollowingMonster()
	{
		return m_pFollowingMonster;	
	}

	///检查是否有特定类型NPC跟随玩家；
	bool CheckFollowNpc( unsigned int  monsterType );
	///删去跟随玩家的特定类型NPC，如果有此种类型NPC，则返回true，否则返回false;
    bool DelFollowNpc( unsigned int monsterType );

	float GetSkillAddFaintRate();   //获得增加的击晕几率
	float GetSkillAntiFaintRate();	//获得技能抗晕几率

	int GetEquipAddFaintLevel();  //获得装备增加击晕等级
	int GetEquipAntiFaintLevel();	//获得装备抗晕等级
	
	int GetEquipAddBondageLevel();	//获得装备抗束缚等级
	int GetEquipAntiBondageLevel();	 //获得装备抗束缚几率

	float GetSkillAddBondageRate();		//获得技能增加束缚几率
	float GetSkillAntiBondageRate();	//获得技能抗束缚几率

	bool UseItemSkill( TYPE_ID itemTypeID, TYPE_ID itemSkillID );

	bool UseDamageItem( TYPE_ID itemTypeID, TYPE_ID itemSkillID, TYPE_POS attackPosX, TYPE_POS attackPosY, PlayerID targetID );

	bool IsHaveControlBuff();	//是否拥有控制类的不利BUFF

	void NotifySurroundingMonsterHpAdd( PlayerID addStarter, int addVal );

	bool ChangeBattleMode( BATTLE_MODE changeMode )
	{
		if( changeMode == GUILD_MODE )
		{
			if( m_pUnion == NULL )
			{
				SendSystemChat("您还没有工会,不能使用工会战斗模式");
				return false;
			}
		}
		m_battleMode = changeMode;
		return true;
	}

	BATTLE_MODE GetBattleMode()
	{
		return m_battleMode;
	}
	
	bool AbsorbCheck( unsigned int& damage );
	
	void EndAbsorbBuff();

	//是否是免疫状态
	bool IsMoveImmunityState()
	{
		return m_buffManager.IsMoveImmunity();
	}

	void SetMoveImmunity( bool bFlag )
	{
		m_buffManager.SetMoveImmunity( bFlag );
	}

	bool IsInvicibleState()
	{
		return m_buffManager.IsInvicible();
	}


	bool IsCanOnMount()
	{
		return m_buffManager.IsCanOnMount();
	}

	void SetInvicibleState (bool bFlag )
	{
		m_buffManager.SetInvicible( bFlag );
	}
	
	bool IsSpPowerSpeedUp()
	{
		return m_buffManager.IsSpPowerSpeedUp();
	}

	void SetSpPowerSpeedUp( bool bFlag )
	{
		m_buffManager.SetSpPowerSpeedUp( bFlag );
	}

	void SetSpeedUp( bool bFlag )
	{
		m_buffManager.SetSpeedUp( bFlag );
	}


	bool IsSpeedUp()
	{
		return m_buffManager.IsSpeedUp();
	}
	
	//删除加速buff
	bool EndSpeedUpBuff();

	//删除控制buff
	void EndControlBuff();

	//删除所有debuff
	void EndAllDebuff();

	//是否变羊
	bool IsSheep()
	{
		return m_buffManager.IsSheep();
	}

	//是否睡眠
	bool IsInSleep()
	{
		return m_buffManager.IsInSleep();
	}

	bool EndSheepDebuff()
	{
		return m_buffManager.EndSheepDebuff();
	}

	//获得经验值倍数
	unsigned int GetTimesExp()
	{
		return m_buffManager.GetTimesExp();
	}

	//获得熟练度倍数
	unsigned int GetTimesPhyMagExp()
	{
		return m_buffManager.GetTimesPhyMagExp();
	}

	void DieDropExp(void);

	//注册无敌
	void RegisterInvincible(void)
	{
		this->SetAttackFlag( false );
		m_RegisterInvincibleTime = ACE_OS::gettimeofday();

		return;
	}

	//检查是否处于无敌
	bool CheckInvincible(bool active = true)
	{
		if(!this->GetAttackFlag() && m_RegisterInvincibleTime.sec() > 0)//无敌状态
		{
			if(!active)//玩家被攻击
			{
				ACE_Time_Value internalTime = ACE_OS::gettimeofday() - m_RegisterInvincibleTime;
				if(internalTime.sec() < 15)//15秒保护内
				{
					//D_ERROR("在保护时间内无敌，activie =%d\n", active);
					return true;
				}
			}

			//D_ERROR("无敌状态解除，activie =%d\n", active);
			
			this->SetAttackFlag( true );
			m_RegisterInvincibleTime.sec(0);
			m_RegisterInvincibleTime.msec(0);
		}

		return false;
	}

	bool IsInAbsorbState()
	{
		return m_buffManager.IsInAbsorbState();
	}


	void SetAbsorbDamage( unsigned int damage )
	{
		m_buffManager.SetAbsorbDamage( damage );
	}

private:
	//更新人物等级的经验
	bool UpdateLevelExp(int nLevel);

public:
	//获取该BUFF的栏位
	int FindBuffPos( CBuffProp* pBuffProp);

	bool IsEvil()
	{
		return ( GetGoodEvilPoint() < 0 );
	}

#ifdef ANTI_ADDICTION
	void SetAddictionState( UINT state )
	{
		m_antiAddictionManager.SetState( state );
	}

	bool IsTriedState()
	{
		return m_antiAddictionManager.IsTriedState();
	}


	bool IsTriedOrAddictionState()
	{
		return m_antiAddictionManager.IsTriedOrAddictionState();
	}


	UINT GetAddictionState()
	{
		return m_antiAddictionManager.GetState();
	}


	void TriedStateProfitCheck( unsigned int& addVal, ProfitType addType )
	{
		m_antiAddictionManager.TriedStateProfitCheck( addVal, addType );
	}
#endif /*ANTI_ADDICTION*/
	
private:
	CPlayerBuffManager m_buffManager;	//BUFF状态管理器
#ifdef ANTI_ADDICTION
	CAntiAddictionManager m_antiAddictionManager;		//防沉迷的管理器
#endif

private:
	vector<CNormalCounter*>   m_arrCounters[CT_INVALID+1];//玩家身上所挂的计数器
	vector<CNormalTask*>      m_arrTasks;//玩家身上所挂的任务；

    //PlayerStateInfo m_StateInfo;//玩家状态信息，包括该状态的开始与结束时间等；

	NpcChatInfo   m_npcChatInfo; //当前玩家的对话信息，包括玩家的对话对象以及玩家的对话阶段；
	ItemChatInfo  m_itemChatInfo; //当前玩家的对话信息，包括玩家的对话对象以及玩家的对话阶段；
	AreaTaskChatInfo m_areaChatInfo;//当前玩家的对话信息,包括玩家的对话对象以及玩家的对话阶段
	//09.04.28,取消工会任务新实现,TaskChatInfo  m_taskChatInfo;//当前玩家的对话信息,包括玩家的对话对象以及玩家的对话阶段

	//PlayerTmpInfo m_TmpProp;	//游戏中的不存盘属性
	PlayerSecondProp m_TmpProp;//玩家二级属性；

	FullPlayerInfo m_fullPlayerInfo;  //注：包含了stateInfo，但是关于m-stateinfo没改

	bool m_bIsFullInfoOk;//当前保存的full info是否完整；

	bool m_bIsWaitToDel;//是否处于待删状态，这是一个本mapsrv相关的玩有特殊状态，在该玩家相关逻辑处理出错时被设置，该状态下除了leave消息之外不再处理其它任何消息；

	bool m_bIsInSwitchMap;//玩家是否处于跳地图状态；

	bool m_bPreFight;//玩家是否处于战前状态（还未向客户端返回玩家已成功进入地图消息，在gatesrv上玩家还未置FIGHTING状态);

	bool m_bFaint;   //看这个玩家是否晕旋

	bool m_bBondage;  //看这个玩家是否束缚

	bool m_bRide;//玩家是否骑乘

	PlayerID m_lPlayerID;

	SpInfo m_spInfo;		//SP的结构体

	unsigned int m_uiSpNextLevelExp;	//下一级的SP升级的经验值

	CNewMap* m_pMap;	//所属的地图，保存当前的地图

    UniqueGatePtr m_uniGatePtr;	//表明该玩家的所属处理器，发送时直接调用这个

	CMonster* m_pFollowingMonster; //跟随本玩家的怪物；

	//AttributeEffect m_buffAffect;	//BUFF影响的属性

	bool m_bEnableAttack;	//是否可以攻击的标志

	//int m_itemCountOnPkgs;//玩家包裹包中的道具个数 

	BATTLE_MODE m_battleMode;

	ACE_Time_Value m_nextActionTime;	//下次可行动的时间,由于人物在CT时间内不能做任何事,所以这个一定要保存 5月23日

	ACE_Time_Value m_RegisterInvincibleTime; //无敌开启时间

	CRacePrestigeManager m_racePrestigeManager;		//种族声望

	ACE_Time_Value m_lastSaveTime;//最后一次存盘DB的时间
	static const unsigned long m_updateSaveDBTime = 60000;//每次DB存盘时间,每10分钟一次存盘
	ACE_Time_Value m_lastOLTestCheckTime;//最后一次检查答题的时间
	static const unsigned long m_updateOLTestTime = 1000;//一秒钟误差
	PlayerTeamCopyFlag m_copyTeamFlag;//玩家副本的相关信息
	bool	m_isNeedNotifyHpChange;//是否需要通知自动回血，如果一直处于满血状态，则不需要通知
	bool	m_isNeedNotifyMpChange;//是否需要通知自动回魔，如果一直处于满魔状态，则不需要通知
	ACE_Time_Value m_lastNotifyHpMpChange;//上次自动回复通知时间

//#ifdef AI_SUP_CODE
//	vector< MonsterEventReged > m_vecRegedMonsters;	//关注该玩家事件的怪物；
//#endif //AI_SUP_CODE

public:
	///获取自身基本信息
	MGOtherPlayerInfo* GetDetailPlayerInfo();		//获取自身基本信息

	//获取玩家简略(外观)信息
	MGSimplePlayerInfo* GetSimplePlayerInfo();     //获取玩家简略(外观)信息

	inline void OnSimpleInfoChange() { m_isSimplePlayerInfoValid = false; };//设置玩家简略信息改变标记；

private:
	///设置玩家简略信息中的不变部分
	void SetUnchangeSimpleInfo();

	///设置玩家简略信息中的可变部分(以后可以细化为各个部分，例如：血量，装备等等)
	void SetChangeSimpleInfo();

	///设置玩家简略信息中的标记值
	void SetSimpleInfoFlag();

	///设置玩家简略信息中的装备
	void SetSimpleInfoEquip();

	///取玩家身上流光值
	unsigned char GetEquipLightVal();

private:
	MGOtherPlayerInfo  m_selfPlayerInfo;		//自身信息，在客户端请求时返回

	bool               m_isSimplePlayerInfoValid;//自身简略信息是否有效，每次取信息时，若简略信息已无效，则重新填充简略信息；
	MGSimplePlayerInfo m_simplePlayerInfo;  //自身简略(外观)信息，用于向周围广播

public:
	//////////////////////////////////////////////////////////////////////////
	//获取现在包裹中所含有的道具数目
	unsigned GetItemCountOnPkg() { return m_itemMan.GetItemCountOnPkgs(); }
	//玩家的包裹是否已经满了
	bool IsItemPkgFull() { return  GetFreePkgIndex(2) == -1; }
	//拆分道具
	bool SplitItem( unsigned int itemUID, unsigned int num );
	//@当玩家不使用道具时,通知周围的玩家
	void  OnUnUseItemNotifyNearByPlayer( unsigned long itemTypeID );
	//当丢弃道具时
	bool OnDropItem( unsigned int itemUID );
	//获取玩家所增加的属性
	ItemAddtionProp& GetItemAddtionProp() { return m_itemTotalProp; }
	//获取玩家状态对象管理器
	CPlayerStateManager& GetSelfStateManager() { return m_stateManager; }
	//当NPC给与玩家道具时//itemID 道具的编号 itemCount 本次创建道具的数目 
	bool OnNpcGiveItem( unsigned int itemTypeID ,int itemCount = 1 );
	//NPC取走玩家道具;
	bool OnNpcRemoveItem( unsigned int itemTypeID ,int itemCount = 1 );
	//取走玩家触发当前脚本的道具
	bool OnNpcRemoveScriptItem();
	//取走这个人物身上的Count个类型道具
	bool OnDropItemByType( unsigned int itemTypeID ,int itemCount );
	//取走这个人身上所有该类型道具
	bool OnDropAllByType( unsigned int itemTypeID );
	//取走属于该材料的物品
	bool OnDropItemByConditionID( unsigned int conditionType, int itemCount );
	//获得背包中同类型材料的个数
	int OnGetItemCountByConditionID( unsigned int conditionID );
	//玩家能否得到道具
	bool CanGetItem( unsigned int itemTypeID, unsigned int itemCount );
	//获得一个空的包裹位置
	int GetFreePkgIndex(int type/*0普通背包，1任务背包，2全部背包*/,unsigned int width = 1, unsigned int height = 1);
	//清除背包中该uid的道具
	void ClearPkgItem( unsigned int itemUID );
	//清除背包中一定范围的道具
	void ClearPkgItem(unsigned int begin, unsigned int end);
	//填充背包中的道具
	bool SetPkgItem(const ItemInfo_i& itemInfo, unsigned int pkgIndex);
	//背包位置是否空
	bool IsFreePkgIndex(unsigned int pkgIndex, unsigned int width = 1, unsigned int height = 1);
	//该道具位置是否位于左上角
	bool IsLeftUpPkgIndex(unsigned int pkgIndex, unsigned int itemUID);

	////玩家获得新的道具
	//bool GetNewItem( unsigned int itemTypeID, unsigned int itemCount , unsigned int itemLevel = 0, unsigned int itemaddidrandSet = 0, unsigned int randSet = 0 );

	//玩家获得新道具
	bool GetNormalItem( unsigned int itemTypeID, unsigned int itemCount , unsigned int itemLevel = 0 );

	//给玩家邮件物品
	bool GetMailItem( unsigned int itemTypeID, unsigned int itemCount, unsigned int itemLevel, unsigned int randSet, unsigned int luckySeed, unsigned int excelSeed );

private:
	//给玩家物品
	bool InnerGetItem( unsigned int itemTypeID, unsigned int itemCount , unsigned int itemLevel, bool isNewRand, unsigned int randSet, unsigned int luckySeed, unsigned int excelSeed );

public:
	//依照道具包中的位置,检查道具信息的合法性
	bool CheckItemValidByPkgPos( unsigned int itemTypeID, unsigned int itemCount, unsigned int itemPos, unsigned int& itemUID );
	//依照道具包中的位置,扣除对应位置上一定数目的道具
	bool SubItemCountByPkgPos( unsigned int itemPos, unsigned int itemTypeID, unsigned int itemCount );
	//查询道具的价钱
	unsigned int QueryItemPrice( unsigned int itemTypeID );
	//拥有的itemTypeID类型的道具数目是否达到了预定目标
	bool ItemNumReachTargetNum( unsigned int itemTypeID, unsigned int targerNum );
	//依据ID号统计在道具在包裹中的个数
	int CounterItemByID( unsigned int itemTypeID );
	//是否含有这个类型的道具
	bool IsPkgHave( unsigned int itemTypeID );
	//NPC是否将对应类型和数量的物品给玩家
	bool NpcCanGiveItem(  unsigned int itemArr[], unsigned int arrSize );
	///通知玩家任务的有关信息
	bool NoticeTaskUiInfo( unsigned int infoNo );
	//判断能否给道具给玩家
	bool GiveNewItemToPlayer( const ItemInfo_i& newItem ,unsigned taskID ,int& errNo, int pkgIndex = -1 );
	//创建一个新的道具指针
	CBaseItem* CreateNewItem( ItemInfo_i& newItem, unsigned int pkgIndex, unsigned int taskID );
	//当玩家移动时,检测是否处于骑乘状态
	bool CheckIsRideStateMove();
	//进入新地图时,设置玩家的骑乘信息
	void OnEnterNewMapSetRideState( ERIDESTATE eRideState ,long attachInfo );
	//骑乘队长切换地图时
	void OnRideLeaderSwitchMap( const GMRideLeaderSwitchMap* pMsg );
	//玩家是否被邀请骑乘
	bool IsInvitingRideState() { return GetSelfStateManager().GetRideState().IsInviteState(); }
	//玩家对经验值的处理
	void OnKillMonsterAddExp( unsigned long expre,unsigned int monsterLevel, bool isBossMonster, unsigned long monsterClsID );
	////当block节点删除玩家的时候
	//void OnDeletePlayerFromBlock( MuxMapBlock* pMapBlcok );
	////当block节点加入加入玩家的时候
	//void OnAddPlayerToNewBlcok( MuxMapBlock* pMapBlcok );
	////当玩家移动离开视野块
	//void OnMoveOutOrgBlockNotify( MuxMapBlock* pMapBlock );
	//获取套装的代理
	SuitDelegate& GetSuitDelegate(){ return m_suitMan;}
	//通知客户端包裹里对应位置的物品信息改变了
	void NoticePkgItemChange( unsigned int index );
    //通知客户端装备栏对应位置的物品信息改变了
	void NoticeEquipItemChange( unsigned int equipIndex );
	//玩家是否在锻造道具(升级，改造,追加)
	bool IsPlayerForgingItem();
	// 移动是否影响交易状态？
	void MoveAffectTrade(void);
	// 离开是否影响交易状态?
	void LeaveAffectTrade(void);
	/// 死亡影响耐久度
	void DieAffectWear(void);
	/// 使用道具影响耐久度
	bool UseItemAffectWear(ItemInfo_i *pItem);
	/// 受伤害影响耐久度
	void BeInjuredAffectWear(void);
	/// 使用技能次数影响耐久度
	void UseSkillTimeAffectWear(void);
	/// 时间影响耐久度
	void TimeAffectWear(void);
	/// 耐久度损耗(返回值：-1:出错,0:耐久度无变化,1:耐久度有变化,2:物品被删除)
	int DoWear(ItemInfo_i *pItem, unsigned int uiFlag);
	//当耐久度将为0时调用该函数，处理因为耐久度将为0时的属性变化
	void OnItemWearPointEmpty( ItemInfo_i* itemWear );
	//装备道具时
	void OnEquipItem( const ItemInfo_i& item, unsigned int equipIndex );
	//卸载道具时
	void OnUnEquipItem( const ItemInfo_i& item, unsigned int unequipIndex );
	//装备时装
	void OnFashionItem( const ItemInfo_i& item, CBaseItem *pBase, unsigned int equipIndex );
	//卸载时装
	void OffFashionItem( const ItemInfo_i& item, CBaseItem *pBase, unsigned int unequipIndex );
	//获取任务管理器
	TaskRecordManager& GetTaskRecordManager() { return m_taskRdManager; }
	//是否曾经做过这个任务
	bool IsHasDoneTask( unsigned int taskID );
	//当在交易时,控制交换互相的物品的函数
	int OnTradeSwapItem( CPlayer* pTargetPlayer,int sourceItemIndex[], int targetItem[], unsigned int sourceMoney, unsigned int targetMoney);
	//获取了道具，通知其他相关的玩家
	void GetNewItemNoticeOtherTeamMember( const ItemInfo_i& itemEle );
	//激活骑乘状态
	void ActiveRideState(){ m_bRide = true; }
	//取消骑乘
	void CancelRideState() { m_bRide = false; }
	//是不是在骑乘状态
	bool IsRideState(){ return m_bRide; }
	//GM更新善恶值
	void OnGMUpdateGoodEvilPoint( int point );
	//玩家使用技能，校验基本属性点
	bool UseSkillBaseAttCheck( CMuxSkillProp* pSkillProp );
	//辅助技能的升级
	bool UpgradeAssistSkill( TYPE_ID skillID); 
	//检测物品是否能被修理
	int CheckItemCanRepair(ItemInfo_i *pItem, unsigned int &uiLeftMoeny, unsigned int &newWear);
	//查询维修道具会花费多少钱
	void QueryRepairAllItemCost();
	//获得道具代理
	ItemDelegate& GetItemDelegate() { return m_itemMan; }
	//是否可与玩家交易
	bool IsCanTradeWithPlayer( unsigned int itemUID , unsigned int& errType );
	//是否死亡掉落
	bool IsCanDeadDrop( unsigned int itemUID );
	//是否能丢弃
	bool IsCanDrop( unsigned int itemUID );
	//是否可与NPC交易
	bool IsCanTradeWithNPC( unsigned int itemUID );
	//是否可升级,追加,道具
	bool IsCanUpdateItem( unsigned int itemUID );
	//更新DB端对应的道具
	void NoticePlayerItemUpdate( bool isEquip , const  ItemInfo_i& itemInfo, unsigned int itemIndex );
	//更新DB端玩家的等级
	void NoticePlayerLevelUpdate( unsigned int level );
	//更新玩家的可变的基础信息
	void NoticePlayerBaseChange();
	//更新DB断的任务记录
	void NoticePlayerTaskRdUpdate( bool isbkTask, const TaskRecordInfo& taskRdInfo ,unsigned int rdIndex );
	//当将物品放入仓库时
	bool OnPutItemToStorage( unsigned int itemUID );
	//当从仓库中取出物品时
	bool OnGetItemFromStorage( const ItemInfo_i& itemInfo );
	//跳转到目标地图
	void IssueSwitchMap( unsigned newMapID, unsigned int targetPosX, unsigned int targetPosY );
	//是否进入流氓状态
	bool IsHaveRogueState() { return (m_pRogueBuff != NULL); }
	//清除流氓状态
	void ClearRogueState() {
		OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(流氓状态)
		m_pRogueBuff = NULL; 

	}
	//创建流氓状态
	void CreateRogueState( unsigned int rogueTime = 20);
	//获取流氓状态的时间 
	unsigned int GetRogueStateTime();	
	//创建虚弱BUFF
	void CreateWeakBuff( unsigned int weakTime = 600);
	//是否在虚弱状态
	bool IsWeak();
	//返回恶魔时间
	unsigned int GetEvilTime() { return m_goodEvilMan.GetEvilTime();}
	//获取武器的技能ID和技能概率,成功返回true 否则返回false
	bool GetWeaponSkill( int& skillID, int& skillRatio );
	//交换装备栏上的道具
	void SwapEquipItem( char from, char to );
	//查询道具包中对应的页数信息
	void QueryItemPkgPage( unsigned char page );
	//检查能否激活骑马状态
	bool CheckCanActiveRideState();
	//当装备的道具被升级,追加,改造时
	void OnUpdateEquipItemInfo( const ItemInfo_i& oldItemInfo,const ItemInfo_i& newItemInfo );
	//随机从装备的物品中选一件升级
	void RandUpdateOneEquipItem();
	//获取在数据库中的玩家唯一编号
	unsigned int GetDBUID()
	{
		FullPlayerInfo * playerInfo = GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CPlayer::GetDBUID, NULL == GetFullPlayerInfo()\n");
			return 0;
		}
		return playerInfo->baseInfo.uiID; 
	}
	//获取装备在身上的道具的最大速度
	int GetEquipedItemMaxSpeed( unsigned int exceptItemUID );
	//装备道具改变了玩家的速度
	void EquipItemChangeSpeed( int speed );
	//设置道具最大速度 
	void SetEquipMaxSpeed( int speed ) { maxItemSpeed = speed; }
	//获取道具最大速度
	int GetEquipMaxSpeed() { return maxItemSpeed; }
	//设置骑乘装备的速度
	void SetRideMaxSpeed( int speed ) { maxRideSpeed = speed ; }
	//获取骑乘装备的速度
	int GetRideMaxSpeed() { return maxRideSpeed; }
	//获取装备的最大值
	int GetItemMaxSpeed();
	//通知客户端速度改变
	void NoticeSpeedChange();
	//维修道具，增加道具属性
	void RepaireEquipItemAddProp( const ItemInfo_i& itemInfo ,unsigned int equipIndex );
	//显示邮箱
	void ShowMailBox();
	//更新技能信息到DB
	void UpdateSkillInfoToDB();
	//通知玩家的血和魔的数据改变了
	void NoticeHPMPChange();
	//根据ItemUID获得道具的索引
	ItemInfo_i* GetItemInfoByItemUID( unsigned int itemUID,int& itemIndex, bool& isEquip );
	//根据包裹index获得道具的索引
	bool GetItemInfoByPkgIndex( unsigned int pkgIndex, ItemInfo_i& itemInfo, unsigned int& leftUpIndex );
	//获得玩家的仓库管理
	CPlayerStorage& GetPlayerStorage() { return m_storageMan; }
	//函数用于排序道具
	bool CompareSortItem( const ItemInfo_i& itemInfo1 , const ItemInfo_i& itemInfo2 );
	//在线测试托盘
	OLTestPlate& GetPlayerOLTestPlate() { return m_oltestMan; }
	//设置玩家副本的相关信息
	void SetCopyTeamInfo( unsigned int TeamID, unsigned int playerflag, unsigned int teamflag ) { m_copyTeamFlag.SetTeamCopyFlag( TeamID, playerflag, teamflag );  }
	//检测玩家的时间耐久度
	void OnCheckTimeWearPoint();
	//检查并且扣除道具
	bool CheckAndSubItem( unsigned int itemid, unsigned int checkitemtype );
	//随机移动
	bool RandomMove();
	//显示玩家拍卖行托盘 
	bool ShowAuctionPlate();
	//获取玩家道具拍卖管理器
	AuctionManager& GetAuctionManager() { return m_auctionManager; }

	////根据属性ＩＤ号获取道具属性对应的指针
	//int* GetItemAttributeByAttriID( unsigned int attributeID );
	//取玩家装备基本属性加成项;
	int& GetNewItemAddBaseAtt( ItemBaseAddAtt attrID );
	//取玩家装备属性加成项;
	int& GetNewItemAddRandAtt( ItemRandAddAtt attrID );

	//道具的宝石管理对象
	EmbedDiamondManager& GetItemEmbedDiamondManager() { return m_embedDiamondsManager; }
	//获取玩家对应副本的flag
	unsigned int GetPlayerCopyFlag()
	{
		return m_copyTeamFlag.GetTeamFlag();
	}

	EUpgradeSkillResult CheckUpgradeBattleSkill(TYPE_ID skillID,CMuxPlayerSkill*& pOutSkill, CMuxSkillProp*& pOutNewProp);

	bool UpgradeBattleSkill( TYPE_ID skillID); 

	void SetDebugMonsterID( TYPE_ID monsterID )
	{
		m_debugMonsterID = monsterID;
	}


	TYPE_ID GetDebugMonsterID()
	{
		return m_debugMonsterID;
	}

	//注册删除；
	void RegisterDeferredOffline( bool isQuickDel/*是否快速删除，若gate已不存在此玩家，则应快速删除而不必延迟等待*/ );

	///尝试执行延迟删除，如果确实执行了删除操作，则返回真，以便调用者不再遍历，否则反之；
	bool ProcessDeferredOffline( bool isForceOffline, const ACE_Time_Value& currentTime );

	inline bool SetLeaveUpdateInfoToDB( bool isUpdate )
	{
		m_bIsLeaveUpdateInfoToDB = isUpdate;
		return true;
	}

	///设置是否已在延迟删除队列中，防止gatesrv可能发来多个不同原因的下线消息时，重复加入延迟删队列；
	inline bool SetIsInDelayDelQue( bool isInDelayDelQue )
	{
		m_bIsInDelayDelQue = isInDelayDelQue;
		return true;
	}

	///是否已在延迟删除队列中，防止gatesrv可能发来多个不同原因的下线消息时，重复加入延迟删队列；
	inline bool IsInDelDelQue() { return m_bIsInDelayDelQue; }

private:
	bool m_bIsLeaveUpdateInfoToDB;//玩家离开本mapsrv时，是否要向DB(gate)更新自身fullinfo?
	bool m_bIsInDelayDelQue;//是否已在延迟删除队列中；

public:

	bool CheckCd( TYPE_ID publicCdType, const ACE_Time_Value& cdTime );

	bool IsHaveBuff()
	{
		return m_buffManager.IsHaveBuff();
	}

	void GetSwitchMapBuffData( MGTempBuffData& bfData )
	{
		m_buffManager.GetSwitchMapBuffData( bfData );
	}

	inline void AddBuffNum()
	{
		m_buffManager.AddBuffNum();
	}

	inline void SubBuffNum()
	{
		m_buffManager.SubBuffNum();
	}

	CMuxPet* GetPet()
	{
		return &m_pet;
	}

	void SelectUnionCmd(unsigned int cmdID)
	{
		m_unionChatCmd = (int)cmdID;//保存最后一次工会对话选择命令

		MGDisplayUnionIssuedTasks unionCmd;//协议文件被锁，暂时用该协议
		StructMemSet( unionCmd, 0x00, sizeof(unionCmd));

		unionCmd.taskEndTime = cmdID;
		SendPkg<MGDisplayUnionIssuedTasks>(&unionCmd);
		
		return;
	}

	void InitUnionAttributeEffect();

	void SetKilledCount( unsigned int roleUID )
	{
		m_killedInfo.SetKilledCount( roleUID );
	}

	unsigned int GetKillCount()
	{
		return m_killedInfo.GetKillCount();
	}

	void SetKillInfo( const KillInfo& killInfo )
	{
		m_killedInfo.SetKillInfo( killInfo );
	}

	unsigned int GetKillRoleUID()
	{
		return m_killedInfo.GetRoleUID();
	}

	bool CanGetKillProfit( unsigned short beKilledLevel )
	{
		unsigned short playerLevel = GetLevel();
		if( ( (short)playerLevel - (short)beKilledLevel ) <= 30 && m_killedInfo.CanGetProfit() )
			return true;
		return false;
	}


	vector<CItemPkgEle>& GetCollectItemVec()
	{
		return m_collectInfo.mCollectItemEleVec;
	}

	void ClearCollectInfo()
	{
		m_collectInfo.mCollectItemEleVec.clear();
		m_collectInfo.collectMoney = 0;
	}

	//是否在技能施放时间内
	bool IsInActionTime();

	//当杀死采集系NPC时
	void OnKillCollectNpc( CMonster* pDeadNpc );

	//通知客户端种族声望变更
	void OnNotifyRacePrestige( int updateVal );

	bool OnPickOneCollectItem( unsigned int index );		//拾取一个采集道具
	bool RemoveCollectItemEle( CItemPkgEle &itemEle );		//从采集道具集中删除一个采集道具
	bool OnPickAllCollectItem();							//拾取所有采集道具
	bool CallMonster( unsigned int num, unsigned int range); //吸引周围怪物

	void AllocatePoint(unsigned short toStr, unsigned short toVit, unsigned short toAgi, unsigned short toSpi, unsigned short toInt);
	void ManualUpgradeLevel(void);
	void ModifyEffectiveSkill(unsigned int skillID, bool incFlag);

	CSortPkgItem& GetSortPkgItem() {return m_sortPkgItem;}
	CastManager& GetCastManager() {return m_castManager;}
	
private:
	//玩家的总的附加道具的属性
	ItemAddtionProp m_itemTotalProp;
	//玩家的状态管理
	CPlayerStateManager m_stateManager;
	//玩家的任务信息管理
	TaskRecordManager   m_taskRdManager;
	//用以记录玩家在锻造(升级，改造，追加)道具的标志位
	unsigned mForgingItem;
	//套装管理
	SuitDelegate m_suitMan;
	//道具管理
	ItemDelegate m_itemMan;
	//善恶值管理
	CGoodEviControl m_goodEvilMan;
	//玩家的区域脚本指针，如果玩家触发了脚本，则为非NULL，否则一般情况为NULL
	CNormalScript* m_pAreaScript;
	//玩家的流氓状态Buff
	CScampBuff* m_pRogueBuff;
	//玩家任务存盘的记录
	NewTaskRecordDelegate m_taskRecordDelegate;
	//玩家的仓库管理
	CPlayerStorage m_storageMan;
	//在线测试托盘
	OLTestPlate m_oltestMan;
	//拍卖管理
	AuctionManager m_auctionManager;
	//道具镶嵌宝石管理
	EmbedDiamondManager  m_embedDiamondsManager;

	int maxItemSpeed;//最大的道具速度

	int maxRideSpeed;//最大的骑乘速度

	time_t m_wearpointchecktime;//最后的耐久度检查点

	//背包整理
	CSortPkgItem m_sortPkgItem;

	//读条管理
	CastManager m_castManager;


	//当玩家往道具包添加道具时
	bool CheckItemCanAdd( ItemInfo_i& AddItemInfo, ItemInfo_i itemPkgs[] , int changeIndexArr[], unsigned int &uiCount , unsigned searchBegin, unsigned searchEnd );

	TYPE_ID m_debugMonsterID;	            //调试怪的ID
	ACE_Time_Value m_updatePosTime;		    //上次更新组队位置的时间
	static const unsigned long m_updateTime = 3000;//每次组队更新的时间,现在设置为3000毫秒更新一次
	CClassLevelProp* m_pClassLevelProp;		//对应该玩家职业的属性配置表指针

	//物品交易
	PlayerID m_recvFromPlayer;//发送交易请求的玩家ID，如果不为空标志"已经收到交易请求"
	//CTradeInfo *m_pTradeInfo;//交易信息,如果不为空标志"交易中"
	unsigned int m_nTradeInfo;//交易信息,如果不为0"交易中"

	ACE_Time_Value m_offlineTime;
	CCooldownManager m_cdManager;
	CUnion *m_pUnion;//工会信息
	KilledInfo m_killedInfo;
	CollectInfo m_collectInfo;

#ifdef OPEN_PUNISHMENT
public:
	bool GivePunishTask( int killCouner = 0 );		//给予天谴任务

	int FinishPunishTask();		//天谴任务设置为完成

	bool GiveUpPunishTask();		//放弃天谴任务

	bool IsHavePunishTask();	//是否拥有天谴任务

	void SetPunishTask( CPunishTask* pTask );	//设置天谴任务

	int GetKillCounter();
	//玩家定时调用
	void CheckPunishTask();
private:
	CPunishTask m_punishTask;		//天谴任务
#endif //OPEN_PUNISHMENT


public:
	///玩家获得宠物;
	bool PlayerGainPet()
	{
		return m_pet.PetActivate( this );
	}

	bool PlayerFeedPet( unsigned int foodNum )
	{
		return m_pet.FeedPet( foodNum );
	}

	inline MGPetinfoStable* GetSurPetNotiInfo()
	{
		return m_pet.GetSurPetNotiInfo();
	}

	inline MGPetinfoInstable* GetPetInfoInstable()
	{
		return m_pet.GetPetInfoInstable();
	}

	///玩家宠物是否激活；
	inline bool IsActivePet()
	{
		return ( m_pet.IsActivate() );
	}

	inline bool IsSummonPet()
	{
		return ( m_pet.IsSummoned() );
	}

	inline bool SummonPet()
	{
		return m_pet.SummonPet();
	}

	inline bool ClosePet()
	{
		return m_pet.ClosePet();
	}

	inline bool SetPetName( const char* petName )
	{
		return m_pet.SetPetName( petName );
	}

	inline void AddPetExp( unsigned long addVal )
	{
		return m_pet.PetExpAdd( addVal );
	}

	inline bool SetPetHunger( unsigned int setVal )
	{
		return m_pet.SetHunger( setVal );
	}

	//设置宠物的外形ID
	inline bool SetPetImagePos( TYPE_ID imageID )
	{
		return m_pet.SetPetImagePos( imageID );
	}

	//设置正在使用的持续性技能
	inline bool SetPetSkillState( unsigned long newSkillState )
	{
		return m_pet.SetPetSkillState( newSkillState );
	}

	//学习宠物基本技能；
	inline bool PetSkillLearn( unsigned int petSkillID )
	{
		return m_pet.StudyPetSkill( petSkillID );
	}

	//增加学习到的变身技能(256种当中的一种)
	inline bool StudyBodytran( unsigned int imagePos )
	{
		return m_pet.StudyBodytran( imagePos );
	}

	//宠物
    inline bool FillPlayerPetInfo( MGDbSaveInfo& dbSaveInfo ) 
	{ 
		return m_pet.FillPlayerPetInfo( dbSaveInfo ); 
	};


	inline bool FillPetTimer( PetTimer& petTimer )
	{
		return m_pet.FillPetTimer( petTimer );
	}

	inline bool InitPlayerPetInfo( const DbPetInfo* petInfo, bool isSwitchMap )
	{
		return m_pet.InitPlayerPetInfo( this, petInfo, isSwitchMap );
	}


	inline bool InitPetTimer( const PetTimer* pPetTimer )
	{
		return m_pet.InitPetTimer( pPetTimer );
	}

	//套装
	inline bool InitPlayerSuitInfo( const PlayerInfo_Suits* pSuitInfo )
	{
		StructMemCpy(m_suits, pSuitInfo, sizeof(m_suits));
		return true;
	}
	
	inline bool FillPlayerSuitInfo(MGDbSaveInfo & dbSaveInfo)
	{
		dbSaveInfo.infoType = DI_SUIT;
		dbSaveInfo.playerID = m_lPlayerID;
		StructMemCpy(dbSaveInfo.suitInfo, &m_suits, sizeof(dbSaveInfo.suitInfo));
		return true;
	}

	//时装服饰
	inline bool InitPlayerFashionInfo( const PlayerInfo_Fashions* pFashionInfo )
	{
		StructMemCpy(m_fashions, pFashionInfo, sizeof(m_fashions));
		return true;
	}

	//保护道具
	bool InitPlayerProtectItemInfo( const ProtectItemInfo& protectItemInfo );

	//玩家任务记录
	bool InitPlayerNewTaskRecordInfo( const TaskRdInfo& newTaskRdInfo);

	inline bool FillPlayerFashionInfo( MGDbSaveInfo & dbSaveInfo )
	{
		dbSaveInfo.infoType = DI_FASHION;
		dbSaveInfo.playerID = m_lPlayerID;
		StructMemCpy(dbSaveInfo.fashionInfo, &m_fashions, sizeof(dbSaveInfo.fashionInfo));
		return true;
	}

	SuitInfo_i * Suit(BYTE index)
	{
		if(index >= MAX_SUIT_COUNT)
			return NULL;

		return  &(m_suits.suitData[index]);
	}

	bool Suit(BYTE index, SuitInfo_i &suit)
	{
		if(index >= MAX_SUIT_COUNT)
			return false;

		StructMemCpy(suit, &(m_suits.suitData[index]), sizeof(suit));
		return true;
	}

	int IsValidSuit(SuitInfo_i *pSuit, CBaseItem *pBase[], bool &isFull);

	bool Suit(BYTE index, SuitInfo_i *pSuit, bool clearFlag)
	{
		if(index >= MAX_SUIT_COUNT)
			return false;

		if(clearFlag)//重置
		{
			StructMemSet( (m_suits.suitData[index]), 0x00, sizeof(m_suits.suitData[index]));
		}
		else//保存
		{
			CBaseItem *pBase[EQUIP_SIZE] ={0};
			bool isFull = true;
			if(IsValidSuit(pSuit, pBase, isFull) == -1)
				return false;

			if(!isFull)
				return false;

			StructMemCpy((m_suits.suitData[index]), pSuit, sizeof(m_suits.suitData[index]));
		}

		return true;
	}

	PlayerInfo_Suits& Suits(void)
	{
		return m_suits;
	}

	PlayerInfo_Fashions& Fashions(void)
	{
		return m_fashions;
	}

	bool IsAnamorphicState(void)
	{
		return (m_anamorphic != 0);
	}

	void SetAnamorphic(unsigned int val)
	{
		OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(变形态)
		m_anamorphic = val;
		return;
	}

	float GetFriendGroupGainAddi(void)
	{
		return friendGroupGainAddition / 100.0f;
	}

	void SetFriendGroupGainAddi(unsigned int friendGoupAddi)
	{
		if(friendGoupAddi >= 100)
			friendGroupGainAddition = 0;
		else
			friendGroupGainAddition = friendGoupAddi;

		return;
	}

	bool IsGmAccount(void)
	{
		return m_isGmAccount;
	}
	
	void SetGmAccount(void)
	{
		m_isGmAccount = true;
		return;
	}

	bool HaveUnionNpcChat(int npcChatCmd)
	{
		return ((m_unionChatCmd >= 0) && (m_unionChatCmd == npcChatCmd));
	}

	bool IsFashionState(void)
	{
		return ACE_BIT_ENABLED(m_fullPlayerInfo.baseInfo.tag, PlayerInfo_1_Base::IN_FASHION_STATE);
	}

	//装备态/时装态切换
	void EquipFashionSwitch(void);

	void EndAnamorphic(void)
	{
		if(m_anamorphic > 0)
		{
			OnUnUseItemNotifyNearByPlayer( m_anamorphic );
			SetAnamorphic( 0 );
		}

		return;
	}

	void ChanageUnionActive(unsigned short gainMode, int changeValue/*gainMode=0时gm修改的值*/);

	void ChanageUnionPrestige(unsigned short gainMode, int chanageValue/*gainMode=0时gm修改的值*/);

	void SetUnionActOrPresTimes(bool activeFlag, BYTE times, unsigned int expireTime);

	void SaveRebirthPt(unsigned short leftOfRanage, unsigned short topOfRanage,unsigned short rightOfRanage, unsigned short bottomOfRanage);

	void NotifyDefaultRebirthPt(void);

	void CheckWeakState( void );

	CachedRepurchaseInfo& CachedRepurchase(void){return m_cachedRepurchase;}

	//下线更新走时间流逝的物品的耐久度
	void UpdateExpireTimeItems(const ACE_Time_Value& onLineTV/*本次上线时间*/, const ACE_Time_Value& offLineTV/*本次离线时间*/);

private:
	inline bool NotifySelfPetInfo()
	{
		return m_pet.NotifySelfPetInfo();
	}

private:
	CMuxPet m_pet;		//玩家的宠物
	PlayerInfo_Suits m_suits;//玩家套装
	PlayerInfo_Fashions m_fashions;//时装服饰

	unsigned int m_anamorphic; //变形对应的模型ID(不等于0时处于变形态)

	unsigned int friendGroupGainAddition;//好友组队收益加成
	bool m_isGmAccount;//gm账号，可以使用gm指令
	int m_unionChatCmd;//公会对话备份信息，因为此时npc对话已经结束

	CachedRepurchaseInfo m_cachedRepurchase;//缓存的回购信息

//09.04.28,取消工会任务新实现,public:
//	///发送工会任务信息；
//	void SendUnionTaskInfo();

public:
	//进入可PK区域
	void OnEnterPKArea();
	//离开PK区域
	void OnExitPKArea();
	//进入战斗区域
	void OnEnterFightArea();
	//离开战斗区域
	void OnExitFightArea();
	//进入副本前置区域
	void OnEnterDunGeonPreArea();
	//离开副本前置区域
	void OnExitDunGeonPreArea();
	//玩家进入areaID编号的区域
	void OnEnterNewTaskArea( CNormalScript* pScript );
	//玩家离开areaID编号的区域
	void OnLeaveOldTaskArea();
	//登陆地图时，获取登陆点的地表属性
	void GetEnterMapPosProperty();

	CNormalScript* GetAreaTaskScript() { return m_pAreaScript; }

#ifdef HAS_PATH_VERIFIER
	void NotifyBlockQuadInfo(void);
#endif

	////玩家新进入视野；
	//void PlayerEnterView( CPlayer* pPlayer )
	//{
	//	if ( pPlayer == this )
	//	{
	//		return;
	//	}		
	//	m_PlayerNearby.NewPlayerEntering( pPlayer );
	//	//D_DEBUG( "将%s加到%s的附近玩家列表\n", pPlayer->GetAccount(), GetAccount() );
	//}

	////玩家新离开视野；
	//void PlayerLeaveView( CPlayer* pPlayer )
	//{
	//	if ( pPlayer == this )
	//	{
	//		return;
	//	}		
	//	m_PlayerNearby.OrgPlayerLeaving( pPlayer );
	//	//D_DEBUG( "将%s从%s的附近玩家列表中删去\n", pPlayer->GetAccount(), GetAccount() );
	//}

	////整理玩家身上的附近玩家，将新进入视野的玩家加入PN_NEARBY，同时将PN_ORG_LEAVING玩家从PN_NEARBY中删去；
	//void RefreshNearby()
	//{
	//	m_PlayerNearby.RefreshManedPlayer();
	//	//D_DEBUG( "整理%s的附近玩家列表，整理后其附近玩家总数为%d\n", GetAccount(), m_PlayerNearby.GetNearbyPlayerNum() );
	//};

	////离开地图前清身上的附近列表；
	//   void LeaveMapClearNearby();

	////向玩家附近玩家发包；
	//template< typename T_Msg >
	//void NotifyPlayerNearby( PLAYER_NEARBY_TYPE nType, T_Msg* pMsg )
	//{
	//	TRY_BEGIN;

	//	UniqueObj<CPlayer>* pUniquePlayer = NULL;
	//	CPlayer* pPlayer = NULL;
	//	set< UniqueObj<CPlayer>* >* notiPlayers = NULL;

	//	if ( PN_NEW_ENTERING == nType )//新近进入视野的玩家；
	//	{
	//		notiPlayers = m_PlayerNearby.GetNewEntering();
	//	} else if ( PN_ORG_LEAVING == nType ) {//新近离开视野的玩家；
	//		notiPlayers = m_PlayerNearby.GetOrgLeaving();
	//	} else if ( PN_NEARBY == nType ) {//附近玩家；
	//		notiPlayers = m_PlayerNearby.GetNearby();
	//	}

	//	
	//	////如果SET里面只有一个玩家就不要用群发
	//	//if( notiPlayers->size() == 1)
	//	//{		
	//	//	set< UniqueObj<CPlayer>* >::iterator iter	= notiPlayers->begin();
	//	//	pUniquePlayer = *iter;
	//	//	if ( NULL == pUniquePlayer )
	//	//	{
	//	//		notiPlayers->erase( iter++ );
	//	//		continue;
	//	//	}
	//	//	pPlayer = pUniquePlayer->GetUniqueObj();
	//	//	if ( NULL == pPlayer )
	//	//	{
	//	//		g_poolManager->Release( pUniquePlayer );//释放UniqueObj<CPlayer>*;
	//	//		notiPlayers->erase( iter++ );
	//	//		continue;
	//	//	}
	//	//	pPlayer->SendPkg<T_Msg>( pMsg );
	//	//	return;
	//	//}


	//	//通知相应玩家；
	//	/*set< CGateSrv* > setProc;
	//	CGateSrv* pTempProc = NULL;*/
	//	if ( ( NULL != notiPlayers ) && ( !(notiPlayers->empty()) ) )
	//	{
	//		for( set< UniqueObj<CPlayer>* >::iterator iter = notiPlayers->begin(); iter != notiPlayers->end(); )
	//		{
	//			pUniquePlayer = *iter;
	//			if ( NULL == pUniquePlayer )
	//			{
	//				notiPlayers->erase( iter++ );
	//				continue;
	//			}
	//			pPlayer = pUniquePlayer->GetUniqueObj();
	//			if ( NULL == pPlayer )
	//			{
	//				g_poolManager->Release( pUniquePlayer );//释放UniqueObj<CPlayer>*;
	//				notiPlayers->erase( iter++ );
	//				continue;
	//			}
	//			//pTempProc = pPlayer->GetProc();
	//			//if( NULL == pTempProc )
	//			//{
	//			//	continue;
	//			//}
	//			////如果添加失败，则是因为一次的BUFF已满，需要先发掉
	//			//if( !pTempProc->AddPlayerID( pPlayer->GetFullID() ) )
	//			//{
	//			//	pTempProc->SendBroadcastPkg<T_Msg>( pMsg );
	//			//	pTempProc->ResetBuff();  //清空了
	//			//	pTempProc->AddPlayerID( pPlayer->GetFullID() );
	//			//}
	//			//setProc.insert( pTempProc );
	//			pPlayer->SendPkg<T_Msg>( pMsg );
	//			++iter;
	//		}
	//	}

	//	
	///*	for( set< CGateSrv* >::iterator iter = setProc.begin(); iter != setProc.end(); ++iter )
	//	{
	//		pTempProc = *iter;
	//		pTempProc->SendBroadcastPkg<T_Msg>( pMsg );
	//		pTempProc->ResetBuff();
	//	}*/

	//	return;
	//	TRY_END;
	//	return;
//}

private:
	//	PlayerNearbyPlayer m_PlayerNearby;//附近玩家；

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//副本相关信息...
public:
	///设置玩家在副本数组中的位置；
	inline void SetPlayerPosInCopy( unsigned int posInCopy ) { m_posInCopy = posInCopy; }
	///取玩家在副本数组中的位置；
	inline unsigned int GetPlayerPosInCopy() { return m_posInCopy; }

private:
	unsigned int  m_posInCopy;//玩家在副本数组中的位置，只有玩家在副本地图中时才有意义；

public:
	//初始化玩家副本阶段信息(使用DB传来数据)；
    inline bool InitPlayerCopyStageInfos( const PlayerStageInfos& copyStageInfos )
	{
		StructMemCpy( m_playerStageInfo, &copyStageInfos, sizeof(m_playerStageInfo) );
		return true;
	}

	//填充玩家副本阶段信息(准备发往gate并最终存往DB)
    inline bool FillPlayerCopyStageInfo( MGDbSaveInfo& dbSaveInfo ) 
	{
		dbSaveInfo.infoType = DI_COPYSTAGE;
		StructMemCpy( (dbSaveInfo.copyStageInfos), &m_playerStageInfo, sizeof(dbSaveInfo.copyStageInfos) );
		return true; 
	};

	inline void ResetCopyStageInfos() { m_playerStageInfo.ResetCopyStageInfos(); };
	///检查玩家身上是否有指定副本地图的阶段信息；
	inline bool GetIfHasStageInfo( unsigned short mapid, unsigned short& stagenum, unsigned short& copytype, unsigned int& timeinfo )
	{
		return m_playerStageInfo.GetIfHasStageInfo( mapid, stagenum, copytype, timeinfo );
	}

	///设置玩家完整的副本阶段信息；
	inline bool SetStageInfo( unsigned short mapid, unsigned short stagenum, unsigned short copytype, unsigned int timeinfo )
	{		
		return m_playerStageInfo.SetStageInfo( mapid, stagenum, copytype, timeinfo );
	}

	///设置玩家的副本阶段号；
	inline bool SetStageInfoStageNum( unsigned short stagenum )
	{
		return m_playerStageInfo.SetStageInfoStageNum( GetMapID(), stagenum );
	}

private:
	PlayerStageInfos m_playerStageInfo;

public:
	///使用gate发来的消息初始化副本相关信息；
	inline bool InitCopyInfoFromCommCopyInfo( const CommCopyInfo& copyInfo )
	{
		if ( !(copyInfo.m_isNextCopy) )
		{
			return false;
		}

		m_CopyInfo.SetCopyInfo( copyInfo.m_isPCopy, copyInfo.m_curCopyID, copyInfo.m_curCopyMapID, copyInfo.m_outMapID, copyInfo.m_outMapPosX, copyInfo.m_outMapPosY );
		m_CopyInfo.SetToUseScrollType( copyInfo.m_toUseScrollType );

		return true;
	}

	///取玩家欲使用的卷轴类型
	inline bool GetPlayerToUseScrollType( unsigned int& toUseScrollType )
	{
		if ( !(m_CopyInfo.IsCurInCopy()) )
		{
			return false;
		}

		return m_CopyInfo.GetToUseScrollType( toUseScrollType );
	}

	///设置玩家当前使用的副本券类型
	inline void SetPlayerCurScrollType( unsigned int curScrollType )
	{
		if ( !(m_CopyInfo.IsCurInCopy()) )
		{
			return;
		}

		return m_CopyInfo.SetCurScrollType( curScrollType );
	}

	///取玩家当前使用的副本券类型;
	inline bool GetPlayerCurScrollType( unsigned int& curScrollType )
	{
		if ( !(m_CopyInfo.IsCurInCopy()) )
		{
			return false;
		}

		return m_CopyInfo.GetCurScrollType( curScrollType );
	}

	inline bool SetPlayerOutPos( unsigned short mapID, unsigned short posX, unsigned short posY, unsigned short extent )
	{
		ACE_UNUSED_ARG( extent );

		if ( !(m_CopyInfo.IsCurInCopy()) )
		{
			return false;
		}

		m_CopyInfo.SetOutMapInfo( mapID, posX, posY );

		MGPlayerOutMapInfo outMapInfo;
		outMapInfo.mapID = mapID;
		outMapInfo.posX = posX;
		outMapInfo.posY = posY;
		SendPkg< MGPlayerOutMapInfo >( &outMapInfo );//通知gate;
		
		return true;
	}

	/////离开组队的玩家在本mapsrv上，检测其是否在副本中，若在副本中，则发起离开副本操作，并重置副本中该玩家的初始进入信息；
	//bool OnLeaveTeamCopyProcCheck();

	///玩家当前是否在副本中
	inline bool IsPlayerCurInCopy()
	{
		return m_CopyInfo.IsCurInCopy();
	}

	///玩家当前是否在伪副本中(若IsPlayerCurInCopy==true)
	inline bool IsPlayerCurPCopy()
	{
		return m_CopyInfo.IsCurPCopy();
	}

	///发起玩家离开副本上半部
	bool IssuePlayerLeaveCopyHF( bool isNotiPlayerThis=true/*函数内部是否直接通知player或稍后使用广播通知*/ );

	///发起玩家离开副本下半部
	bool IssuePlayerLeaveCopyLF();

	///取离开副本后的目标位置信息；
	inline bool GetCopyOutMapInfo( unsigned short& outMapID, unsigned short& outPosX, unsigned short& outPosY )
	{
		return m_CopyInfo.GetOutPosInfo( outMapID, outPosX, outPosY );
	}

	///清玩家副本信息；
	inline bool ClearPlayerCopyInfo()
	{
		m_CopyInfo.ClearCopyInfo();
		return true;
	}

	inline bool IsCurPlayerCopyInfoSet()
	{
		return m_CopyInfo.IsCurCopyInfoSet();
	}

	inline bool GetCurCopyMapInfo( unsigned int& copyID, unsigned short& copyMapID )
	{
		return m_CopyInfo.GetCurCopyMapInfo( copyID, copyMapID );
	}

	///若玩家当前在副本中，则恢复玩家出副本后的位置；
	inline bool RestoreOutMapInfo()
	{
		if ( !IsCurPlayerCopyInfoSet() )
		{
			return false;
		}

		unsigned short outmapid=0, outposx=0, outposy = 0;
		GetCopyOutMapInfo( outmapid, outposx, outposy );
		//若玩家在副本中，则恢复玩家的原地图信息
		D_DEBUG( "玩家%s:%d(%d,%d)副本标记已置，将玩家副本外位置%d(%d,%d)设入其fullinfo\n", GetAccount()
			, m_fullPlayerInfo.baseInfo.usMapID, m_fullPlayerInfo.baseInfo.iPosX, m_fullPlayerInfo.baseInfo.iPosY
			, outmapid, outposx, outposy );
		m_fullPlayerInfo.baseInfo.usMapID = outmapid;
		m_fullPlayerInfo.baseInfo.iPosX = outposx;
		m_fullPlayerInfo.baseInfo.iPosY = outposy;

		return true;
	}

	//inline CMuxCopyInfo* GetPlayerCurCopy()
	//{
	//	return m_CopyInfo.GetCurCopy();
	//}

	///请求传送玩家进副本;
	bool TransPlayerToCopyQuest( unsigned short copymapid, unsigned short scrollType );

	///请求传送玩家进副本;
	bool TransPlayerToPCopyQuest( unsigned short copymapid, unsigned short scrollType );

	///玩家真正进入copy;
	bool OnPlayerEnterCurCopy( CMuxCopyInfo* pCopy );

	///玩家离开copy;
	bool OnPlayerLeaveCurCopy()
	{
		unsigned short outmapid = 0, outposx = 0, outposy = 0;
		GetCopyOutMapInfo( outmapid, outposx, outposy );

		D_DEBUG( "玩家%s:%d(%d,%d)离开副本，将玩家副本外位置%d(%d,%d)设入其fullinfo\n", GetAccount()
			, m_fullPlayerInfo.baseInfo.usMapID, m_fullPlayerInfo.baseInfo.iPosX, m_fullPlayerInfo.baseInfo.iPosY
			, outmapid, outposx, outposy );
		m_fullPlayerInfo.baseInfo.usMapID = outmapid;
		m_fullPlayerInfo.baseInfo.iPosX = outposx;
		m_fullPlayerInfo.baseInfo.iPosY = outposy;

		m_CopyInfo.ClearCurCopyInfo();

		return true;
	}

private:
	PlayerCopyInfo m_CopyInfo;//欲进副本与当前所在副本信息；
	//...副本相关信息
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
public:
	///新DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	///玩家非存盘信息，但是需要跨地图保存者，通知Gate;
	void PlayerNDSMRInfoSend();

	///gate发来的玩家NDSMR信息处理
	void OnNDSMRInfo( const GMNDSMRInfo* pNdsmrInfo );

	///玩家信息存盘
	void PlayerDbSave();

	///新玩家收到的玩家存盘信息；
	bool OnDbGetInfo( const GMDbGetInfo* pDbGetInfo );

	///玩家相关存盘信息通知（是在mapsrv发起，还是gate直接通知？？待决）
	bool PlayerDbInfoNotify()
	{
		return NotifySelfPetInfo();//目前只有宠物;
	}

	//增加好友动态
    void AddFriendTweet(unsigned short tweetType, const void* tweetInfo, unsigned short tweetInfoLen);

	//增加好友度
	void AddFriendDegree(unsigned int friendId, unsigned int friendDegree);

	//增加经验影响好友收益
	void AddExpAffectFriendGain(unsigned int exp, bool isBoss);

	// 收到额外的好友分享收益（组队：攻防加成；非组队：经验分享）
	void OnRecvAdditionalFriendGain(bool inTeam, unsigned int expOrNot);

public:
	inline bool InitNameInfo( CGateSrv* pGate, const PlayerID& playerID, const char* accountName, const char* roleName )
	{
		m_uniGatePtr.InitUniGatePtr( pGate );//所属gate;
		m_lPlayerID = playerID;
		SafeStrCpy( m_accountName, accountName );
		SafeStrCpy( m_roleName, roleName );
		return true;
	}

	inline const char* GetAccount() { return m_accountName; }
	inline const char* GetNickName() { return m_roleName; }

private:
	char m_accountName[MAX_NAME_SIZE];
	char m_roleName[MAX_NAME_SIZE];
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public:
//by dzj, 6.30版本修改为二级属性直接由脚本计算，因此这一块应去除//UI数值界面。。。。
//	//获取UI物攻MIN 
//	unsigned int GetUIPhyAtkMin()
//	{
//		unsigned int basicAtk = m_TmpProp.nPhysicsAttack;
//		basicAtk += m_itemTotalProp.PhysicsAttack;
//		basicAtk += GetBuffPhyAtk();
//		AttributeEffect* pUnionEffect = GetUnionEffect();
//		if( NULL != pUnionEffect )
//		{
//			basicAtk += pUnionEffect->nPhyAttack;
//		}
//		basicAtk += (int)(GetBuffStr() * GetPropertyK( GetClass() - 1, 0 ) );
//		basicAtk = (unsigned int)(basicAtk * ( 1 + m_itemTotalProp.PhysicsAttackPercent/100.f ));
//		basicAtk += m_itemTotalProp.AttackPhyMin;
//		return basicAtk;
//	}
//
//	//获取UI最小魔攻
//	unsigned int GetUIMagAtkMin()
//	{
//		unsigned int basicAtk = m_TmpProp.nMagicAttack;
//		basicAtk += m_itemTotalProp.MagicAttack;
//		basicAtk += GetBuffMagAtk();
//		AttributeEffect* pUnionEffect = GetUnionEffect();
//		if( NULL != pUnionEffect )
//		{
//			basicAtk += pUnionEffect->nMagAttack;
//		}
//		basicAtk += (int)(GetBuffInt() * GetPropertyK( GetClass() - 1, 3 ) );
//		basicAtk = (unsigned int)( basicAtk * ( 1 + m_itemTotalProp.MagicAttackPercent/100.f ) );
//		basicAtk += m_itemTotalProp.AttackMagMin;
//		return basicAtk;
//	}
//	
//
//	//获取UI上的物攻MAX
//	unsigned int GetUIPhyAtkMax()
//	{
//		unsigned int basicAtk = m_TmpProp.nPhysicsAttack;
//		basicAtk += m_itemTotalProp.PhysicsAttack;
//		basicAtk += GetBuffPhyAtk();
//		AttributeEffect* pUnionEffect = GetUnionEffect();
//		if( NULL != pUnionEffect )
//		{
//			basicAtk += pUnionEffect->nPhyAttack;
//		}
//		basicAtk += (int)(GetBuffStr() * GetPropertyK( GetClass() - 1, 0 ) );
//		basicAtk = (unsigned int)( basicAtk * ( 1 + m_itemTotalProp.PhysicsAttackPercent/100.f ) );
//		basicAtk += m_itemTotalProp.AttackPhyMax;
//		return basicAtk;
//	}
//
//
//	//获取UI上的魔攻MAX
//	unsigned int GetUIMagAtkMax()
//	{
//		unsigned int basicAtk = m_TmpProp.nMagicAttack;
//		basicAtk += m_itemTotalProp.MagicAttack;
//		basicAtk += GetBuffMagAtk();
//		AttributeEffect* pUnionEffect = GetUnionEffect();
//		if( NULL != pUnionEffect )
//		{
//			basicAtk += pUnionEffect->nMagAttack;
//		}
//		basicAtk += (int)(GetBuffInt() * GetPropertyK( GetClass() - 1, 3 ) );
//		basicAtk = (unsigned int)( basicAtk * ( 1 + m_itemTotalProp.MagicAttackPercent/100.f ) );
//		basicAtk += m_itemTotalProp.AttackMagMax;
//		return basicAtk;
//	}
//
//	//获取UI物防。。。
//	unsigned int GetUIPhyDef()
//	{
//		unsigned int basicDef = m_TmpProp.nPhysicsDefend;
//		basicDef += m_itemTotalProp.PhysicsDefend;
//		basicDef += GetBuffPhyDef();
//		AttributeEffect* pUnionEffect = GetUnionEffect();
//		if( NULL != pUnionEffect )
//		{
//			basicDef += pUnionEffect->nPhyDefend;
//		}
//		basicDef += (unsigned int)( GetBuffStr() * GetPropertyK( GetClass() - 1, 1 ) );
//		basicDef = (unsigned int)( basicDef * ( 1 + m_itemTotalProp.PhysicsDefendPercent/100.f ) );
//		return basicDef;
//	}
//
//	//获取UI魔防
//	unsigned int GetUIMagDef()
//	{
//		unsigned int basicDef = m_TmpProp.nMagicDefend;
//		basicDef += m_itemTotalProp.MagicDefend;
//		basicDef += GetBuffMagDef();
//		AttributeEffect* pUnionEffect = GetUnionEffect();
//		if( NULL != pUnionEffect )
//		{
//			basicDef += pUnionEffect->nMagDefend;
//		}
//		basicDef += (unsigned int) (GetBuffStr() * GetPropertyK( GetClass() - 1, 4 ) );
//		basicDef = (unsigned int)( basicDef * ( 1 + m_itemTotalProp.MagicDefendPercent/100.f ) );
//		return basicDef;
//	}
//
//
//	//获取UI物闪。。。
//	unsigned int GetUIPhyDodge()
//	{
//		return m_itemTotalProp.PhysicsJouk;
//	}
//
//
//	//获取UI魔闪
//	unsigned int GerUIMagDodge()
//	{
//		return m_itemTotalProp.MagicJouk;
//	}
//
//	
//	//获取UI物命
//	unsigned int GetUIPhyHit()
//	{
//		unsigned int basicPhyHit = GetPhyHit();
//		return basicPhyHit;
//	}
//
//	//获取UI魔命
//	unsigned int GetUIMagHit()
//	{
//		unsigned int basicMagHit = GetMagHit();
//		return basicMagHit;
//	}
//
//	//获取UI物暴
//	int GetUIPhyCri()
//	{
//		int basicPhyCri = m_itemTotalProp.PhysicsCritical;
//		basicPhyCri += GetBuffPhyCriLevel();
//		basicPhyCri += (int)( GetBuffStr() * GetPropertyK( GetClass() - 1, 2 ) );
//		return basicPhyCri;
//	}
//
//
//	//获取UI魔暴
//	int GetUIMagCri()
//	{
//		int basicMagCri = m_itemTotalProp.MagicCritical;
//		basicMagCri += GetBuffMagCriLevel();
//		basicMagCri += (int)( GetBuffInt() * GetPropertyK( GetClass() - 1, 5 ) );
//		return basicMagCri;
//	}

///////////////////////////////////////////////////////////////////////////////////////

////关于升级脚本的接口
	public:
#ifdef LEVELUP_SCRIPT
//升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, //设置最大MaxHp
//void SetBaseMaxHp( unsigned int maxHp);
////设置最大MaxMp
//void SetBaseMaxMp( unsigned int maxMp );
////设置基础力量
//void SetBaseStr( unsigned int baseStr);
////设置基础敏捷
//void SetBaseAgi( unsigned int baseAgi);
////设置基础智力
//void SetBaseInt( unsigned int baseInt );
////设置基础物攻
//void SetBasePhyAtk( unsigned int basePhyAtk );
////设置基础物防
//void SetBasePhyDef( unsigned int basePhyDef );
////设置基础魔攻
//void SetBaseMagAtk( unsigned int baseMagAtk );
////设置基础魔防
//void SetBaseMagDef( unsigned int baseMagDef );
////设置基础物理命中
//void SetBasePhyHit( unsigned int basePhyHit);
////设置基础魔法命中
//void SetBaseMagHit( unsigned int baseMagHit );
////设置下级经验值
//void SetNextLevelExp( unsigned int nextLevelExp );
//增加SP豆
unsigned int AddSpBean( unsigned int beanNumber );

private:
	CLevelUpScript* m_pLevelUpScript;
#endif  //LEVELUP_SCRIPT

//////////////////////////////////////////////////BUFF//////////////////////////////////////////////
public:
	//清除所有BUFF
	void OnBuffLeave();

	//人物死亡关于BUFF的处理
	void OnBuffDie();	
//////////////////////////////////////////关于仇人 好友变化的接口////////////////////
private:
	RelationInfo m_relationInfo;

public:
	//设置好友人数
	void SetFriendNum( unsigned int setNum )
	{
		m_relationInfo.SetFriendNum( setNum );		
	}

	//设置仇人人数
	void SetEnemyNum( unsigned int setNum )
	{
		m_relationInfo.SetEnemyNum( setNum );
	}

	unsigned int GetFriendNum()
	{
		return m_relationInfo.GetFriendNumber();
	}

	unsigned int GetEnemyNum()
	{
		return m_relationInfo.GetEnemyNumber();
	}

	void AddFriendNum( unsigned addNum )
	{
		m_relationInfo.AddFriendNumber( addNum );
	}

	void AddEnemyNum( unsigned int addNum )
	{
		m_relationInfo.AddEnemyNumber( addNum );
	}

	bool SubFriendNum( unsigned int subNum )
	{
		return m_relationInfo.SubFriendNumber( subNum );
	}

	bool SubEnemyNum( unsigned int subNum )
	{
		return m_relationInfo.SubEnemyNumber( subNum );
	}

	//////////////////////通知活动状态//////
	void NotifyActivityState();

	/////////////////////////新手保护机制/////////////
	bool IsRookieState();
	
	bool IsRookieBreak();

	//察看新手是否能被攻击 
	bool IsRookieCanBeAttack(); 

	void CreateRookieBreak( const ACE_Time_Value& = ACE_OS::gettimeofday() );
	
	void EndRookieBreak();
	
	void RookieBreakCheck( unsigned int rookieBreakTime = 20 );
	
	void OnRookieStateCheck( bool isOnline );	//上线和升级的时候调用
	void UpdateRookieState();


	bool IsRaceMaster();
		
private:
	bool m_bRookieBreak;
	bool m_bRookieState;
	ACE_Time_Value m_rookieBreakTime;

	
	public:
		bool InitMuxCommonSkill( const PlayerInfo_4_Skills& skillInfo ); 
		bool InitGuildSkill();
		CMuxPlayerSkill* FindMuxSkill( unsigned int skillID, bool isNormalSkill = true );
		void ReleaseMuxSkill();		//释放MuxSkill

	private:
		vector<CMuxPlayerSkill*> m_vecSkillList;		//技能列表
				
public:
	void GmGetPetAllPetSkill()
	{
		m_pet.GmGetAllPetSkill();
	}

	public:
		void SetMark( unsigned int mark )
		{
			m_mark = mark;
		}

		bool CheckMark( unsigned int mark )
		{
			return ( mark == m_mark );
		}

		//检查技能属性是否为被动休息技能是的话 增加被动休息技能的属性
		void RestSkillCheck( CMuxSkillProp* pSkillProp );

		bool IsInRest()
		{
			return m_bActRestFlag;	
		}

		void SetRestFlag( bool bFlag )
		{
			m_bActRestFlag = bFlag;
		}

	private:
		unsigned int m_mark;
		RestSkillInfo m_restSkillInfo;
		bool m_bActRestFlag;		//主动休息标志

		void AddRestSkillInfo( MuxBuffProp* pBuffProp );
		void AtomRestCheck( MuxBuffProp* pBuffProp );

		ACE_Time_Value m_lastBuffUpdate;
		ACE_Time_Value m_lastHpMpUpdate;

public:
	void ClearBuffData();
	void UpdateBuffData( unsigned int saveType  ); // -1 离线时处理 -0 平时的处理

	void BuffProcess();
	void RestSkillProcess();
	bool ActiveEndBuff( unsigned int buffIndex );  //主动结束buff
	bool ActiveEndRestBuff();	//结束休息技能


	unsigned int GetTeamID();

	//技能相关
	void NotifyDamageRebound( unsigned int skillID, unsigned int reboundDamage, EBattleFlag batFlg );
		
	
	///////////////////////////Weak Buff Process//////////////////////////////////////
	public:
		//EndWeakBuff
		bool EndWeakBuff()
		{
			return m_buffManager.EndWeakBuff();
		}

		void SetAlive()
		{
			OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(置活导致状态改变)
			m_fullPlayerInfo.stateInfo.SetAlive();	//将玩家状态置为活；
		}

		bool RelieveBuff( unsigned int relieveType, unsigned int relieveLevel, unsigned int relieveNum, bool isRealRelieve )
		{
			return m_buffManager.RelieveBuff( relieveType, relieveLevel, relieveNum, isRealRelieve );
		}
};

#endif /*PLAYER_H*/
