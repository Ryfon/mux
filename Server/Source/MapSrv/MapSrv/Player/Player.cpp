﻿#include "Player.h"
#include "../MuxMap/mannormalmap.h"
#include "../ManPlayerProp.h"
#include "../PlayerLevelConfig.h"
#include "PkgProc/SrvProtocol.h"
#include "../Item/ItemBuilder.h"
#include "../RideState.h"
#include "../Item/CEquipItemManager.h"
#include "../Item/CItemPackage.h"
#include "../GroupTeamManager.h"
#include "../monster/monster.h"
#include "../Item/RepairWearRule.h"
#include "../ItemSkill.h"
#include "../BattleUtility.h"
#include "../Item/LoadDropItemXML.h"
#include "../Item/ItemLevelUpAttribute.h"
#include "../Item/ItemLeavelUpCri.h"
#include "../SpSkillManager.h"
#include "../RaceManager.h"
#include "../MuxSkillConfig.h"
#include "../PlayerSkillProp.h"
#include "../AISys/RuleAIEngine.h"
#include "../Lua/BattleScript.h"
#include "../Union/UnionManager.h"
#include "../GMCmdManager.h"
#include "../ItemAttributeHelper.h"

#ifdef LEVELUP_SCRIPT
	#include "../Lua/LevelUpScript.h"
#endif
#include "../Lua/CalSecondProperty.h"

#include "../monster/manmonstercreator.h"

//套装计算
#include "../Item/SuitAttributeManager.h"
//#include "../Rank/RankChartManager.h"
#ifdef OPEN_PUNISHMENT
	#include "../Activity/PunishmentManager.h"
#endif //OPEN_PUNISHMENT
#include <string>
#include "../MuxSkillConfig.h" 

//#include "../Rank/rankcache.h"
#include "../Rank/rankeventmanager.h"

using namespace ITEM_GLOBAL;

CPlayer* g_debugPlayer = NULL;
extern CPoolManager* g_poolManager;

#define CheckValue( Value )\
	if( Value < 0 )\
{\
	Value = 0;\
}\

void EQUIP_ITEM_MASK( char& levelInfo ) {  levelInfo |= 0x80; }
char GET_ITEM_LEVELUP( char levelInfo ) {  return levelInfo & 0x7f ; }
void RESET_ITEM_LEVEL( char& levelInfo) {  levelInfo &= 0x80; }
void SET_ITEM_LEVEL( char& levelInfo, unsigned level ) { levelInfo =  ( levelInfo & 0x80 | level ); }
bool ITEM_IS_EQUIPED( char levelInfo )  {  return ( levelInfo & 0x80 )!=0; }

//#define HPMP_RECOVERY

namespace MUX_PROTO
{
	///上线初始化;
	void PlayerStateInfo::OnlineProc( CPlayer* pPlayer, unsigned long& dyingleft )
	{
		if ( NULL == pPlayer )
		{
			D_ERROR( "PlayerStateInfo::OnlineProc， NULL==pPlayer\n" );
			return;
		}

		//排行榜缓存方式修改//检测玩家是否进过排行榜数据缓存，打上相应标记；
		//if ( CRankCacheManager::CheckIfOnlinePlayerInCache( pPlayer->GetPlayerUID() ) )
		//{
		//	pPlayer->SetEverInRankCache();//置曾进缓存标记，以加快后续的排行榜缓存更新相关操作；
		//}

		dyingleft = 0;
		m_OnlineTick = time(NULL);
		if ( MAP_PS_ALIVE == m_CurBaseState )
		{
			if ( pPlayer->GetCurrentHP() > 0 )
			{
				//正常情形；
				m_StateStTick = time(NULL);
				return;
			} else {
				//异常，玩家状态为活，但血值<=0，将玩家状态重置为死亡；
				D_ERROR( "玩家%s状态为活，但血值<=0，将玩家状态重置为死亡\n", pPlayer->GetNickName() );
				pPlayer->ResetHPMP();
				m_CurBaseState = MAP_PS_DIE;
				m_StateStTick = m_OnlineTick;
				m_StateEndTick = m_OnlineTick;
			}
		}

		if ( MAP_PS_DIE == m_CurBaseState )
		{
			if ( pPlayer->GetCurrentHP() > 0 )
			{
				//异常，玩家状态为死亡，但其血值非0，将其血值重设为0;
				D_WARNING( "玩家%s状态为死亡，但其血值%d非0，将其血值重设为0\n", pPlayer->GetNickName(), pPlayer->GetCurrentHP() );
				pPlayer->ResetHPMP();
				m_StateStTick = m_OnlineTick;
				m_StateEndTick = m_OnlineTick;
			}
			//重新计算状态结束时间；
			//上次状态原本应该持续的时间；
			unsigned long lastshouldduration = (unsigned long)((m_StateEndTick-m_StateStTick));				
			if ( ( m_StatePauseTick >= m_StateStTick ) )
			{
				//上次下线时已经经历的状态时间；
				unsigned long lastpast = (unsigned long)(( m_StatePauseTick/*上次下线时间*/ - m_StateStTick/*上次状态开始时间*/ ));
				if ( lastshouldduration <= lastpast ) 
				{
					//上次下线时就过了复活间隔，应该立刻复活至复活点;
					m_StateStTick = m_OnlineTick;
					m_StateEndTick = m_OnlineTick;
					//超时，转默认复活点;
					pPlayer->OnlineDirectRebirth();
					return;
				}
				//剩余的倒计时时间
				unsigned long leftduration = lastshouldduration - lastpast;
				//D_WARNING( "上次下线时已经经历的状态时间 = %d\n", lastpast );				
				//D_WARNING( "剩余持续时间 = %d\n", leftduration );
				m_StateEndTick = m_OnlineTick + leftduration;
				m_StateStTick = m_OnlineTick;
				//此处应该发送DIE通知消息以便客户端显示倒计时界面；
				//pPlayer->SendDyingPkg( leftduration );
				dyingleft = leftduration;
				//D_WARNING( "通知客户端剩余持续时间 = %d\n", (m_StateEndTick-m_StateStTick) );
			} else {
				//上次下线时间比状态开始时间早，错！
				m_StateStTick = m_OnlineTick;
				m_StateEndTick = m_OnlineTick;
				//超时，转默认复活点;
				pPlayer->OnlineDirectRebirth();
			}
		}
	}

	void PlayerStateInfo::CheckWeakBuff( CPlayer* pPlayer )
	{
		if( NULL == pPlayer )
			return;

		if( MAP_PS_WEAK == m_CurBaseState )
		{
			unsigned int weakTime = (unsigned int)(m_StateEndTick - m_StatePauseTick);
			pPlayer->CreateWeakBuff( weakTime );
		}
	}

	///置死亡状态
	void PlayerStateInfo::SetDie( CPlayer* pPlayer )
	{
		if ( NULL == pPlayer )
		{
			return;
		}
		m_CurBaseState = MAP_PS_DIE;
		m_StateStTick = time(NULL);
		//死亡时同时置状态结束时刻；
		m_StateEndTick = m_StateStTick + DIE_DURATION*60;
		//通知怪物反注册下，防止怪继续攻击 05.04
		//pPlayer->NotifyRegedMonsterAiEvent( MEVENT_PLAYER_LEAVEVIEW );
		//此处应该发送DIE通知消息以便客户端显示倒计时界面；
		CNewMap *pMap = pPlayer->GetCurMap();
		if(NULL != pMap)
			ForWarDie(pMap->InWarState());//是否为攻城战死亡?

		pPlayer->SendDyingPkg( (unsigned long)(m_StateEndTick-m_StateStTick) );
		SetOffline();
	}

	void PlayerStateInfo::SetWeak( unsigned int weakTime )
	{
		m_CurBaseState = MAP_PS_WEAK;
		m_StateStTick = time(NULL);	
		m_StateEndTick = m_StateStTick + weakTime;
	}

};

bool PlayerAptInfo::SetAptPlayer( unsigned int inReqID, CPlayer* pInPlayer )
{
	if ( NULL == pInPlayer )
	{
		return false;
	}
	reqID = inReqID;
	pAptPlayer = pInPlayer;
	aptPlayerID = pAptPlayer->GetFullID();
	isAptSet = true;
	return true;
}

//void NpcChatInfo::ClearNpcChatTarget()
//{
//	if ( NULL != pChatTarget )
//	{
//		//pChatTarget->NotifyRefDeled( this );
//		pChatTarget = NULL;
//	}
//}
//
/////设置交谈目标；
//void NpcChatInfo::SetNpcChatTarget( CMonster* pMonster, int nOption )
//{
//	//如果原来没有交谈对象，或者原来的交谈对象不是pMonster，则重置交谈对象并置stage为0;
//	if ( ( NULL == pChatTarget )
//		|| ( pChatTarget != pMonster )
//		|| ( nOption == 0 ) //0表示重新点了该NPC,因为有可能客户端在谈话途中关闭了对话,稍后再次点击同一NPC,如果不重设,则选项0会导致对话再也无法进行;
//		)
//	{
//		ClearNpcChatTarget();
//		pChatTarget = pMonster;//->GetRefPtr( this );
//		SetNpcChatStage( 0 );
//	}
//}
//
/////选项选择，nOption==0表示初始选择NPC；
//bool NpcChatInfo::OnNpcChatOption( CPlayer* pOwner, int nOption )
//{
//	if ( NULL == pChatTarget )
//	{
//		//交谈对象空；
//		return false;
//	}
//	return pChatTarget->OnChatOption( pOwner, nOption );
//}
//
//void ItemChatInfo::ClearItemChatTarget()
//{
//	if ( NULL != pChatTarget )
//	{
//		//pChatTarget->NotifyRefDeled( this );
//		pChatTarget = NULL;
//	}
//}
//
/////设置交谈目标；
//void ItemChatInfo::SetItemChatTarget( CBaseItem* pItem, int nOption )
//{
//	//如果原来没有交谈对象，或者原来的交谈对象不是pMonster，则重置交谈对象并置stage为0;
//	if ( ( NULL == pChatTarget )
//		|| ( pChatTarget != pItem )
//		|| ( nOption == 0 ) //0表示重新点了该NPC,因为有可能客户端在谈话途中关闭了对话,稍后再次点击同一NPC,如果不重设,则选项0会导致对话再也无法进行;
//		)
//	{
//		ClearItemChatTarget();
//		pChatTarget = pItem;
//		SetItemChatStage( 0 );
//	}
//}
//
/////选项选择，nOption==0表示初始选择NPC；
//bool ItemChatInfo::OnItemChatOption( CPlayer* pOwner, int nOption )
//{
//	if ( NULL == pChatTarget )
//	{
//		//交谈对象空；
//		return false;
//	}
//	return pChatTarget->OnChatOption( pOwner, nOption );
//}

//inline void UniquePlayer::Init( CPlayer* inPlayer )  
//{
//	pPlayer = inPlayer;
//	dwUID = 0;
//	if ( NULL != pPlayer )
//	{
//		dwUID = pPlayer->GetUID();
//	}
//}
//
//inline CPlayer* UniquePlayer::GetUniquePlayer()
//{
//	if ( NULL == pPlayer )
//	{
//		return NULL;
//	} else {
//		if ( pPlayer->IsStillValid( dwUID) )//前提，CPlayer对象只会回收到对象池，而不会真正释放；
//		{
//			return pPlayer;
//		} else {
//			return NULL;
//		}
//	}
//}

//PlayerNearbyPlayer::~PlayerNearbyPlayer()
//{
//	Clear();
//}
//
//void PlayerNearbyPlayer::Clear()
//{
//	for ( set< UniqueObj<CPlayer>* >::iterator iter=nearbyPlayers.begin(); iter!=nearbyPlayers.end(); ++iter )
//	{
//		g_poolManager->Release( *iter );
//	}
//	for ( set< UniqueObj<CPlayer>* >::iterator iter=newEntering.begin(); iter!=newEntering.end(); ++iter )
//	{
//		g_poolManager->Release( *iter );
//	}
//	for ( set< UniqueObj<CPlayer>* >::iterator iter=orgLeaving.begin(); iter!=orgLeaving.end(); ++iter )
//	{
//		g_poolManager->Release( *iter );
//	}
//	nearbyPlayers.clear();
//	newEntering.clear();
//	orgLeaving.clear();
//
//	return;
//}
//
/////新的玩家进入本玩家附近范围；
//void PlayerNearbyPlayer::NewPlayerEntering( CPlayer* pPlayer )
//{
//	UniqueObj<CPlayer>* pUniquePlayer = g_poolManager->RetriveUniquePlayer( pPlayer );
//	if ( NULL != pUniquePlayer )
//	{
//		newEntering.insert( pUniquePlayer );
//	}
//	return;	
//}
//
/////一个玩家离开附近范围；
//void PlayerNearbyPlayer::OrgPlayerLeaving( CPlayer* pPlayer )
//{
//	UniqueObj<CPlayer>* pUniquePlayer = g_poolManager->RetriveUniquePlayer( pPlayer );
//	if ( NULL != pUniquePlayer )
//	{
//		orgLeaving.insert( pUniquePlayer );
//	}
//	return;
//}
//
/////整理附近玩家列表，删去新近离开的玩家，添加新近进入的玩家；
//void PlayerNearbyPlayer::RefreshManedPlayer()
//{
//	if ( !(orgLeaving.empty()) )//有离开的玩家；
//	{
//		//删去离开玩家；
//		for ( set< UniqueObj<CPlayer>* >::iterator iter=orgLeaving.begin(); iter!=orgLeaving.end(); ++iter )
//		{
//			DelNearbyPlayer( *iter );
//			g_poolManager->Release( *iter );//及时释放UniquePlayer*;
//		}
//		orgLeaving.clear();//里面的元素已无效，清空；
//	}
//	if ( !(newEntering.empty()) )//有新进入的玩家；
//	{
//		//添加新进入玩家；
//		for ( set< UniqueObj<CPlayer>* >::iterator iter=newEntering.begin(); iter!=newEntering.end(); ++iter )
//		{
//			nearbyPlayers.insert( *iter );
//		}
//		newEntering.clear();//里面的元素已加入nearbyPlayers，本列表清空；
//	}
//}
//
////从附近玩家列表中删去指定玩家；
//void PlayerNearbyPlayer::DelNearbyPlayer( UniqueObj<CPlayer>* inUniquePlayer )
//{
//	UniqueObj<CPlayer>* pUniquePlayer = NULL;
//	CPlayer* pInPlayer = inUniquePlayer->GetUniqueObj();
//	if ( NULL == pInPlayer )
//	{
//		//输入的待删玩家已经无效；
//		return;
//	}
//	if ( nearbyPlayers.empty() )
//	{
//		//附近玩家列表空;
//		return;
//	}
//	//删除玩家；
//	for ( set< UniqueObj<CPlayer>* >::iterator iter=nearbyPlayers.begin(); iter!=nearbyPlayers.end(); )
//	{
//		pUniquePlayer = *iter;
//		if ( pInPlayer == pUniquePlayer->GetUniqueObj() )//nearbyPlayers中保存的玩家与输入的玩家一致，从内部列表中删去该玩家；
//		{
//			g_poolManager->Release( *iter );//释放UniqueObj<CPlayer>*;
//			nearbyPlayers.erase( iter++ );//从附近玩家列表中删去；
//			return;//同一玩家应该只在set中存在一次；
//		} else {
//			++iter;
//		}
//	}
//}

///下线调用相关处理；
void CPlayer::OfflineProc( EAppearType nReason, bool isClearCopyInfo/*是否清副本信息，只有跳进副本的情况下才不清副本信息，其余任何情形均清副本信息*/ )
{
	if ( true ) //为了使pMap局部化，防止在pMap空时外部调用它,09.23的修改，为了在玩家还未进地图的情况下也能将玩家正常删去，而不会在下次登录时出已在线错误；
	{
		//地图相关处理；
		CNewMap* pMap = GetCurMap();
		if ( NULL != pMap )
		{
			m_ndmsFollowingMonsterID = 0;
			if ( INFO_SWITCH_UPDATE == nReason ) //跳地图保存跟随怪物信息；
			{
				CMonster* pFollowMonster = GetFollowingMonster();
				if ( NULL != pFollowMonster )
				{
					//在OnPlayerLeave时会将following monster删去，因此在这里暂存下；
					m_ndmsFollowingMonsterID = pFollowMonster->GetClsID();//准备跳地图保存；
				}
			}
#ifdef STAGE_MAP_SCRIPT
			pMap->OnMapPlayerLeaveScriptHandle( this );//不能在复活的时候调用，因此不能合并至OnPlayerLeave；
#endif //STAGE_MAP_SCRIPT
			pMap->OnPlayerLeave( this, (INFO_SWITCH_UPDATE==nReason) );	
		} else {
			D_ERROR("CPlayer::OfflineProc,玩家%s没有对应地图，可能玩家尚未进地图，其应处位置:%d(%d,%d)\n"
				, GetAccount(), GetMapID(), GetPosX(), GetPosY() );
		}
	}

	RestoreOutMapInfo();//检查是否要恢复玩家位置信息（从副本中）;

	if ( isClearCopyInfo )
	{
		//下线清玩家副本信息，清副本信息两种可能：1、跳出副本地图；2、下线；
		ClearPlayerCopyInfo();		
	}

	m_fullPlayerInfo.stateInfo.SetOffline();

	//下线前计算时间型耐久度的最新值(最新的离线时间作为基值)
	UpdateExpireTimeItems(ACE_Time_Value(m_fullPlayerInfo.stateInfo.GetOnlineTick()), ACE_Time_Value(m_fullPlayerInfo.stateInfo.GetOffLineTick()));

	if ( m_bIsLeaveUpdateInfoToDB/*下线是否刷新玩家信息*/ )
	{
		PlayerDbSave();//保存玩家DB信息；
		UpdatePlayerFullInfo(  nReason );
	}

	CGateSrv* playerGate = GetProc();//m_uniGatePtr.GetInnerGateSrvPtr();
	if ( playerGate != NULL )
	{
		playerGate->GateDelPlayerFromContainer( GetPID() );
	}

	CPlayerManager::DeletePlayer( this );

	return;
}

//取玩家装配的武器，有装配武器时返回true，没装配武器时返回false;
bool CPlayer::GetEquipWeaposType( unsigned int& weaponType1, unsigned int& weaponType2 )
{
	//只需检测单手对应的两个位置是否有装备即可
	bool isSecOK = false;
	unsigned int pos1=0, pos2=0;
	bool isGetPosOK = GetDefaultEquipPos( E_SINGLEHAND, pos1, isSecOK, pos2 );
	if ( !isGetPosOK )
	{
		D_ERROR("CPlayer::GetEquipWeaposType, GetDefaultEquipPos失败\n");
		return false;
	}
	ItemInfo_i* pWeapon1 = GetEquipByPos( pos1 );
	ItemInfo_i* pWeapon2 = NULL;
	if ( isSecOK )
	{
		pWeapon2 = GetEquipByPos( pos2 );
	}

	bool isHasWeapon = false;

	if( NULL != pWeapon1 )
	{
		if ( PlayerInfo_2_Equips::INVALIDATE != pWeapon1->uiID )
		{
			weaponType1 = pWeapon1->usItemTypeID;
			isHasWeapon = true;
		}
	}

	if( NULL != pWeapon2 )
	{
		if ( PlayerInfo_2_Equips::INVALIDATE != pWeapon2->uiID )
		{
			weaponType2 = pWeapon2->usItemTypeID;
			isHasWeapon = true;
		}
	}

	if ( 0 == weaponType1 )
	{
		weaponType1 = weaponType2;//优先在第一个位置返回有效的武器类型号;
		weaponType2 = 0;
	}

	return true;
}

//玩家是否有装配武器
bool CPlayer::IsEquipWeapon()	//该玩家是否装备武器
{
	unsigned int footype1 = 0, footype2 = 0;
	return GetEquipWeaposType( footype1, footype2 );
}

//取玩家身上装配的某型装备,不可用本函数取单手装备，因为其ItemInfo_i有两个；
bool CPlayer::GetPlayerGearEquip( ITEM_GEARARM_TYPE gearType, ItemInfo_i*& pOutItemInfoI )
{
	pOutItemInfoI = NULL;
	bool isSecOK = false;
	unsigned int pos1=0, pos2=0;
	bool isGetPosOK = GetDefaultEquipPos( gearType, pos1, isSecOK, pos2 );
	if ( !isGetPosOK )
	{
		D_ERROR( "CPlayer::GetPlayerGearEquip, GetDefaultEquipPos失败,输入的gearType:%d\n", gearType );
		return false;
	}

	pOutItemInfoI = GetEquipByPos( pos1 );
	return (NULL != pOutItemInfoI);
}

//初始化生成一个玩家
bool CPlayer::InitPlayer( CNewMap* pMap)
{
	TRY_BEGIN;
	//改为在Appear之后执行，OnlineProc();//调用上线操作，目前为记录上线时间，如果玩家死亡，则重新计算倒计时时间并通知客户端

//#ifdef AI_SUP_CODE
//	m_vecRegedMonsters.clear();
//#endif //AI_SUP_CODE

	if ( NULL != m_pMap )
	{
		//初始化玩家前，玩家所在地图应为空
		D_ERROR( "CPlayer::InitPlayer, 玩家%s, NULL != m_pMap, cur mapid:%d\n", GetAccount(), m_pMap->GetMapNo() );
		return false;
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//临时，将机器人设到一个随机点，避免他们聚在一处...
	const char* playerAccount = GetAccount();
	const char* szrobot = "Robot";
	if ( strlen( playerAccount ) >= strlen(szrobot) )
	{
		//将机器人设到一个随机点；
		bool isRobot = true;
		for ( unsigned int i=0; i<strlen(szrobot); ++i )
		{
			if ( szrobot[i] != playerAccount[i] )
			{
				isRobot = false;
				break;
			}
		}
		if ( isRobot )
		{
			m_bIsRobot = true;
			MuxPoint tmpPt;
			if ( pMap->GetOneAvailPt( tmpPt ) )
			{
				SetPlayerPos( tmpPt.nPosX, tmpPt.nPosY, true );				
			}			
		}
	}
	//...临时，将机器人设到一个随机点，避免他们聚在一处
	////////////////////////////////////////////////////////////////////////////////////////////

	InitMovingInfo();
	m_npcChatInfo.Init();
	m_itemChatInfo.Init();
	m_areaChatInfo.Init();
	InitSpInfo();

	//保存存盘数据
	//m_DBProp = pAppear->playerInfo;
	m_pMap = pMap; //地图

	//m_itemCountOnPkgs = 0;//初始化道具包中的道具个数 //2008/05/26
	m_pClassLevelProp = CManPlayerProp::FindObj( GetClass() );
	if( NULL == m_pClassLevelProp )
	{
		D_ERROR("找不到职业属性属性配置表 职业为%d\n", GetClass() );
		return false;
	}

	//初始化技能
	if( !InitMuxCommonSkill( m_fullPlayerInfo.skillInfo ) )
	{
		D_ERROR("初始化技能失败\n");
		return false;//2010.5.22,技能失败则人物初始化失败
	}
	
	//通过配置属性算出游戏中的非存盘属性
	if( !CalculateTmpProp() )
	{
		return false;
	}
#ifdef LEVELUP_SCRIPT
	m_pLevelUpScript = CLevelUpScriptManager::FindScripts( GetClass() );
	if( NULL == m_pLevelUpScript )
	{
		D_ERROR("人物初始化时找不到对应的升级脚本\n");
		return false;
	}
	//m_pLevelUpScript->OnPlayerSetBaseProp( this );
#endif

	//套装管理
	m_suitMan.AttachPlayer( this );

	//道具管理
	m_itemMan.AttachPlayer( this );

	//任务记录管理器
	m_taskRdManager.AttachPlayer( this );

	//状态管理
	m_stateManager.AttachPlayer(this);

	//buff状态管理
	m_buffManager.AttachOwner( this );

#ifdef ANTI_ADDICTION
	m_antiAddictionManager.AttachOwner( this );
#endif /*ANTI_ADDICTION*/

	//玩家的托盘管理
	m_playerPlate.AttachPlayer( this );

	//玩家善恶值管理
	m_goodEvilMan.AttachPlayer( this );

	//玩家保护道具的托盘
	m_protectItemPlate.AttachPlayer( this );

	//玩家保护道具管理器
	m_protectItemMan.AttachPlayer( this );

	//天谴任务
	m_punishTask.AttachOwner( this );

	//玩家的任务存盘
	m_taskRecordDelegate.AttachPlayer( this );

	//玩家的仓库管理
	m_storageMan.AttachPlayer( this );

	//玩家的答题管理
	m_oltestMan.AttachPlayer( this );

	//玩家的拍卖管理
	m_auctionManager.AttachPlayer( this );

	//初始化玩家的OtherPlayerInfo信息
	//InitOtherPlayerConciseInfo();

	m_racePrestigeManager.AttachPlayer( this );

	m_sortPkgItem.SetPlayer(this);

	m_castManager.AttachPlayer(this);

	//检测是否是排行榜的玩家进入地图
	//RankEventManagerSingle::instance()->OnPlayerEnterMap( this );
	RankEventManager::OnPlayerEnterMap( this );
	//玩家进入该地图的种族管理器
	AllRaceMasterManagerSingle::instance()->OnPlayerEnterMap( this );

	//获取登陆地点的地表属性
	GetEnterMapPosProperty();

	//成功登陆后将玩家基本属性保存
	NoticePlayerBaseChange();

	return true;
	TRY_END;
	return false;
}

///////////////////////////////////////////////////////////////////////////////
//活点相关。。。
///添加活点玩家；
void CPlayer::OnAddAPT( unsigned int reqID, const char* aptRoleName )
{
	TRY_BEGIN;

    MGAddActivePtRst rstMsg;
	rstMsg.lNotiPlayerID = GetFullID();
	rstMsg.reqID = reqID;
	rstMsg.tgtPlayerID.wGID = 0;
	rstMsg.tgtPlayerID.dwPID = 0;
	rstMsg.reqRst = GCAddActivePtRst::ADDAPT_OTHERERROR;
	rstMsg.posX = 0;
	rstMsg.posY = 0;

	if ( NULL == aptRoleName )
	{
		D_WARNING( "CPlayer::OnAddAPT，玩家%s加活点，输入角色名空\n", GetAccount() );
		SendPkg< MGAddActivePtRst >( &rstMsg );//通知客户端加活点失败；
		m_aptInfo.ResetAptPlayer();//每次加活点失败都重置之前的活点信息；
		return;
	}

	CPlayer* pAptPlayer = CPlayerManager::FindPlayerByRoleName( aptRoleName );
	if ( NULL == pAptPlayer )
	{
		//本mapsrv找不到指定名字玩家；
		rstMsg.reqRst = GCAddActivePtRst::ADDAPT_NOPLAYER;
		SendPkg< MGAddActivePtRst >( &rstMsg );//通知客户端加活点失败；		
		m_aptInfo.ResetAptPlayer();//每次加活点失败都重置之前的活点信息；
		return;
	}

	CNewMap* pCurMap = GetCurMap();
	if ( NULL == pCurMap )
	{
		D_WARNING( "CPlayer::OnAddAPT，玩家%s加活点，当前所在地图空\n", GetAccount() );
		SendPkg< MGAddActivePtRst >( &rstMsg );//通知客户端加活点失败；
		m_aptInfo.ResetAptPlayer();//每次加活点失败都重置之前的活点信息；
		return;
	}

	bool isAptPlayerInMap = (pAptPlayer->GetMapID() == GetMapID() );//pCurMap->IsPlayerInMap( pAptPlayer );
	if ( !isAptPlayerInMap )
	{
		//本地图找不到指定玩家；
		rstMsg.reqRst = GCAddActivePtRst::ADDAPT_NOPLAYER;
		SendPkg< MGAddActivePtRst >( &rstMsg );//通知客户端加活点失败；		
		m_aptInfo.ResetAptPlayer();//每次加活点失败都重置之前的活点信息；
		return;
	}

	//本地图确实有该玩家，将该玩家设为当前活点；
	rstMsg.reqRst = GCAddActivePtRst::ADDAPT_SUCCESS;
	rstMsg.tgtPlayerID = pAptPlayer->GetFullID();
	rstMsg.posX = pAptPlayer->GetPosX();
	rstMsg.posY = pAptPlayer->GetPosY();
	SendPkg< MGAddActivePtRst >( &rstMsg );//通知客户端加活点成功；		
	m_aptInfo.SetAptPlayer( reqID, pAptPlayer );

	return;
	TRY_END;
	return;
}

///定时检测玩家所设APT点的有效性，如果有效且活点仍在地图上，则通知玩家该活点的位置信息；
void CPlayer::TimeCheckUpdateAptInfo()
{
	TRY_BEGIN;

	if ( !m_aptInfo.IsAptPlayerSet() )
	{
		///没有设APT玩家；
		return;
	}

	CPlayer* pAptPlayer = m_aptInfo.GetAptPlayerPtr();
	if ( NULL == pAptPlayer )
	{
		//不可能错误，APT玩家已设，但没有设APT玩家指针；
		D_ERROR( "玩家%s, APT玩家已设，但没有设APT玩家指针\n", GetAccount() );
		m_aptInfo.ResetAptPlayer();
		return;
	}

    MGAddActivePtRst rstMsg;
	rstMsg.lNotiPlayerID = GetFullID();
	rstMsg.reqRst = GCAddActivePtRst::ADDAPT_OTHERERROR;
	rstMsg.reqID = m_aptInfo.GetReqID();
	rstMsg.tgtPlayerID = m_aptInfo.GetAptPlayerID();
	rstMsg.posX = 0;
	rstMsg.posY = 0;

	CNewMap* pCurMap = GetCurMap();
	if ( NULL == pCurMap )
	{
		D_ERROR( "TimeCheckUpdateAptInfo, 不可能错误，玩家%s当前所在地图空\n", GetAccount() );
		SendPkg< MGAddActivePtRst >( &rstMsg );
		m_aptInfo.ResetAptPlayer();
		return;
	}

	bool isAptPlayerInMap = pCurMap->IsDoubtPlayerPtrInMap( pAptPlayer );
	if ( !isAptPlayerInMap )
	{
		D_WARNING( "玩家%s, 所设APT玩家已不在本地图\n", GetAccount() );
		rstMsg.reqRst = GCAddActivePtRst::ADDAPT_NOPLAYER;//玩家已下线；
		SendPkg< MGAddActivePtRst >( &rstMsg );
		m_aptInfo.ResetAptPlayer();
		return;
	}

	if ( ( m_aptInfo.GetAptPlayerID().wGID != pAptPlayer->GetFullID().wGID )
		|| ( m_aptInfo.GetAptPlayerID().dwPID != pAptPlayer->GetFullID().dwPID )
		)
	{
		//不可能错误，APT玩家已设，但没有设APT玩家指针；
		D_WARNING( "玩家%s, APT玩家的PlayerID校验失败,可能palyerID已被重用\n", GetAccount() );
		rstMsg.reqRst = GCAddActivePtRst::ADDAPT_NOPLAYER;//玩家已下线；
		SendPkg< MGAddActivePtRst >( &rstMsg );
		m_aptInfo.ResetAptPlayer();
		return;
	}

	//活点玩家仍然在线，将活点位置信息发给客户端；
	rstMsg.reqRst = GCAddActivePtRst::ADDAPT_SUCCESS;
	rstMsg.posX = pAptPlayer->GetPosX();
	rstMsg.posY = pAptPlayer->GetPosY();
	SendPkg< MGAddActivePtRst>( &rstMsg );

	return;
	TRY_END;
	return;
}
//活点相关。。。
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
//托盘相关...
///通知客户端显示一个托盘；
bool CPlayer::ShowPlateCmd( EPlateType plateType ) 
{
	if( mForgingItem!= 0 )//如果之前有托盘，则要重置上个托盘
	{
		m_playerPlate.ResetPlate();
		mForgingItem = 0;
	}

	mForgingItem |= 1<<((unsigned int)plateType);

	m_playerPlate.Init( GetFullID(), plateType );

	MGShowPlate showPlate;
	showPlate.lNotiPlayerID = GetFullID();
	showPlate.plateType = plateType;

	SendPkg<MGShowPlate>( &showPlate );

	return true; 
};

//设置本次托盘读取条件的ID
void CPlayer::SetPlateCondition(unsigned int conditionID )
{
	m_playerPlate.SetConditionID( conditionID );
}

bool CPlayer::SetSpeed(float fSpeed)
{
	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(速度)

	//不能设置超过8以上的属性
	if( fSpeed > 5 || fSpeed <= 0)
	{
		D_ERROR( "CPlayer::SetSpeed, 玩家%s, fSpeed值%d超限，设为默认速度\n", GetAccount(), fSpeed );
		m_TmpProp.fSpeed = ROLE_INIT_SPEED;//设为人物初始速度；
		return false;
	}
	
	m_TmpProp.fSpeed = fSpeed;	//修改人物的速度

	MGRoleattUpdate updateAttr;
	updateAttr.nNewValue = (int)(fSpeed * 100);
	updateAttr.lChangedPlayerID = GetFullID();
	updateAttr.nType = RT_SPEED;	//修改速度
	updateAttr.bFloat = true;
	updateAttr.changeTime = 0;
	SendPkg<MGRoleattUpdate>( &updateAttr );
	return true;
}

///客户端向托盘中添加物品；
bool CPlayer::OnAddSthToPlate( EPlateType plateType, ItemInfo_i sthInfo, unsigned int platePos ) 
{
	if ( m_playerPlate.GetPlateType() != plateType )
	{
		m_playerPlate.Init( this->GetFullID(), plateType );//初始化托盘；
	}

	bool isAddOK = false;

	int errNo = 0;
	//当platePos == 0 时，为主物品
	if ( 0 == platePos )
	{
		isAddOK = m_playerPlate.AddMainItem( this, sthInfo ,errNo );
		MGAddSthToPlateRst addRst;
		addRst.sthID = sthInfo;
		addRst.platePos = platePos;
		addRst.isFulfilCon = m_playerPlate.IsConFulfil();
		addRst.addRst = isAddOK ? GCAddSthToPlateRst::ADD_STH_OK :(GCAddSthToPlateRst::AddRstType) errNo;
		SendPkg<MGAddSthToPlateRst>( &addRst );
	} 
	else /*if ( PLATE_POS_ASST == platePos )*/
	{
		//检测辅助物品是否已经放入,不处理已经放入托盘的情况
		if( m_playerPlate.FindAssistItem( sthInfo.uiID ) == false )
		{
			isAddOK = m_playerPlate.AddAsstItem( this, sthInfo,platePos ,errNo );
			MGAddSthToPlateRst addRst;
			addRst.sthID = sthInfo;
			addRst.platePos = platePos;
			addRst.isFulfilCon = m_playerPlate.IsConFulfil();
			addRst.addRst = isAddOK ? GCAddSthToPlateRst::ADD_STH_OK :(GCAddSthToPlateRst::AddRstType) errNo;
			SendPkg<MGAddSthToPlateRst>( &addRst );
		}
	}

	return isAddOK; 
};

///客户端从托盘中取走物品；
bool CPlayer::OnTakeSthFromPlate( ItemInfo_i sthInfo, unsigned int platePos )
{
	bool isTakeOK = false;  //
	
	//如果是主要物品
	if ( platePos == 0 )
	{
		m_playerPlate.TakeMainItem( this );
		isTakeOK = true;
	}
	else
	{
		isTakeOK = m_playerPlate.TakeAsstItem( this,sthInfo,platePos );
	}

	/*if ( PLATE_POS_MAIN == platePos )
	{
		m_playerPlate.TakeMainItem();
		isTakeOK = true;
	} else if ( PLATE_POS_ASST == platePos ) {
		isTakeOK = m_playerPlate.TakeAsstItem( sthInfo );
	}*/

	MGTakeSthFromPlateRst takeRst;
	takeRst.sthID = sthInfo;
	takeRst.platePos = platePos;
	takeRst.isFulfilCon = m_playerPlate.IsConFulfil();
	takeRst.takeRst = isTakeOK ? GCTakeSthFromPlateRst::TAKE_STH_OK : GCTakeSthFromPlateRst::TAKE_STH_FAIL;

	SendPkg<MGTakeSthFromPlateRst>( &takeRst );

	return isTakeOK; 
};

///客户端发命令尝试托盘操作
bool CPlayer::OnExecPlate() 
{ 
	bool isok = m_playerPlate.TryPlateExec(); 

	MGPlateExecRst execRst;
	execRst.execRst = isok ? GCPlateExecRst::PLATE_EXEC_OK : GCPlateExecRst::PLATE_EXEC_FAIL;
	SendPkg<MGPlateExecRst>( &execRst );


	if ( m_playerPlate.GetPlateType() == PLATE_ADD )
	{
		mForgingItem = 0;
	}
	return isok;
};

///服务器通知客户端关托盘
void CPlayer::ClosePlateCmd() 
{ 
	MGClosePlate closePlate;
	SendPkg<MGClosePlate>( &closePlate );

	m_playerPlate.ResetPlate(); 
	mForgingItem = 0;

	return; 
};

///客户端发命令尝试托盘操作
bool CPlayer::OnClosePlate() 
{ 
	ClosePlateCmd();//通知客户端关托盘；
	return true; 
};

//死亡时关闭托盘
bool CPlayer::OnDieClosePlate()
{
	if ( m_playerPlate.GetPlateType() != INVALID_PLATE )
		OnClosePlate();

	return true;
}

//...托盘相关
///////////////////////////////////////////////////////////////

///玩家请求出售物品；
bool CPlayer::SoldPkgItem( const GMItemSoldReq& itemSoldReq )
{
	//1、检查玩家身上指定位置是否有指定物品；
	//2、计算该物品的售出价格，与玩家希望的卖价比较；
	//3、如果比较成功，则给玩家加金钱，将物品从玩家身上取走；
	MGItemSoldRst soldRst;
	/*
		unsigned long itemID;//物品类型ID号；
		unsigned long itemPos;//售出物品在包裹中的位置；
		unsigned long itemNum;//售出数量；
		unsigned long moneyNum;//本次售出希望获得的金钱数；
		GCItemSoldRst::SoldRstType soldRst;//售出结果；
	*/
	soldRst.itemID = itemSoldReq.itemID;
	soldRst.itemPos = itemSoldReq.itemPos;
	soldRst.itemNum = itemSoldReq.itemNum;
	soldRst.silverMoneyNum = itemSoldReq.silverMoneyNum;

	unsigned int itemUID = 0;
	//1、检查玩家身上指定位置是否有指定物品；
	bool ishave = CheckItemValidByPkgPos( itemSoldReq.itemID, itemSoldReq.itemNum, itemSoldReq.itemPos, itemUID );
	if ( !ishave )
	{
		soldRst.soldRst = GCItemSoldRst::NO_ITEM;
		SendPkg< MGItemSoldRst >( &soldRst );
		return false;
	}

	//如果不能与npc交易
	if ( !IsCanTradeWithNPC( itemUID )  )
	{
		soldRst.soldRst = GCItemSoldRst::PROTECT_ITEM_SOLD_ERR;//不可售;
		SendPkg< MGItemSoldRst >( &soldRst );
		return false;
	}

	//2,如果是放在任务背包中的物品，是无法被卖出的
	if ( itemSoldReq.itemPos >= NORMAL_PKG_SIZE )
	{
		soldRst.soldRst = GCItemSoldRst::CAN_NOT_SOLD;//不可售;
		SendPkg< MGItemSoldRst >( &soldRst );
		return false;
	}

	//3、计算该物品的售出价格，与玩家希望的卖价比较/*取指定ID号物品的价格 dzj 9999*/；
	unsigned long realprice = QueryItemPrice( itemSoldReq.itemID ) * itemSoldReq.itemNum;
	if ( 0 == realprice )
	{
		soldRst.soldRst = GCItemSoldRst::CAN_NOT_SOLD;//不可售;
		SendPkg< MGItemSoldRst >( &soldRst );
		return false;
	}

	if ( realprice != itemSoldReq.silverMoneyNum ) 
	{
		soldRst.soldRst = GCItemSoldRst::SOLD_COND_ERROR;//售出条件错，卖得的金钱数不一致;
		SendPkg< MGItemSoldRst >( &soldRst );
		return false;
	}

	soldRst.soldRst = GCItemSoldRst::SOLD_SUCCESS;//成功售出；
	SendPkg< MGItemSoldRst >( &soldRst );
	CLogManager::DoNpcShopPurchase(this, 0, LOG_SVC_NS::TT_SALE, itemSoldReq.silverMoneyNum, itemSoldReq.itemID, itemSoldReq.itemNum);//记日至

	//4、如果比较成功，则给玩家加金钱，将物品从玩家身上取走；
	if( !AddSilverMoney( itemSoldReq.silverMoneyNum, 1 ) )//双币种判断
	{
		return false;
	}

	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::SoldPkgItem, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	if ( (unsigned int)(itemSoldReq.itemPos) >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs) )
	{
		D_ERROR( "CPlayer::SoldPkgItem, (unsigned int)(itemSoldReq.itemPos(%d)) >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs)\n", itemSoldReq.itemPos );
		return false;
	}
	ItemInfo_i itemBackup = playerInfo->pkgInfo.pkgs[itemSoldReq.itemPos];//先备份，最后插入回购列表
	bool isdel = SubItemCountByPkgPos( itemSoldReq.itemPos, itemSoldReq.itemID, itemSoldReq.itemNum );
	if ( !isdel )
	{
		D_ERROR( "不可能错误，SoldPkgItem时取不走玩家身上物品，银币%d已给玩家%s\n", itemSoldReq.silverMoneyNum, GetAccount() );
		return false;
	}

	//加入回购列表并通知
	m_cachedRepurchase.InsertItem(itemBackup, itemSoldReq.silverMoneyNum);
	RepurchaseListNtf();

	return true;
}

unsigned int CPlayer::GetAllGreyItems(std::map<int, unsigned int>& greyItemList)
{
	unsigned int totalPrice = 0;//总价
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::GetAllGreyItems, NULL == GetFullPlayerInfo()\n");
		return 0;
	}
	PlayerInfo_3_Pkgs & pkgs = playerInfo->pkgInfo;
	for(int i = 0; i < NORMAL_PKG_SIZE; i++)
	{
		if(pkgs.pkgs[i].uiID > 0)
		{
			CItemPublicProp *pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(pkgs.pkgs[i].usItemTypeID);
			if(NULL != pPublicProp)
			{
				if(E_GRAY == pPublicProp->m_itemMassLevel)
				{
					unsigned int realprice = QueryItemPrice( pkgs.pkgs[i].usItemTypeID ) * pkgs.pkgs[i].ucCount;
					if ( 0 != realprice )
					{
						totalPrice = totalPrice + realprice;
						greyItemList[i] = realprice;		
					}
				}
			}
		}
	}

	return totalPrice;
}

bool CPlayer::SoldGreyPkgItems( const GMBatchSellItemsReq& itemSoldReq)
{
	MGBatchSellItemsResp resp;
	StructMemSet( resp, 0x00, sizeof(resp));
	resp.resp.totalItemCount = itemSoldReq.req.totalItemCount;
	resp.resp.totalSilverMoney = itemSoldReq.req.totalSilverMoney;

	do
	{
		//获取所有灰色物品的个数和价钱
		std::map<int, unsigned int> greyItemList;
		unsigned int totalPrice = GetAllGreyItems(greyItemList);

		size_t grayItemCount = greyItemList.size();
		if(grayItemCount != itemSoldReq.req.totalItemCount)
		{
			resp.resp.retcode = (int)GCBatchSellItemsResp::INCORRECTNESS_ITEM_COUNT;
			break;
		}

		if(totalPrice != itemSoldReq.req.totalSilverMoney)
		{
			resp.resp.retcode =(int)GCBatchSellItemsResp::INCORRECTNESS_ITEM_MONEY;
			break;
		}

		//更新金钱
		if(GetSilverMoney() + totalPrice > MAX_MONEY)
			totalPrice = MAX_MONEY - GetSilverMoney();
		
		AddSilverMoney( totalPrice, 1 );

		//更新物品
		FullPlayerInfo * playerInfo = GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CPlayer::SoldGreyPkgItems, NULL == GetFullPlayerInfo()\n");
			return false;
		}
		PlayerInfo_3_Pkgs &pkg = playerInfo->pkgInfo;
		for(std::map<int, unsigned int>::iterator iter = greyItemList.begin(); greyItemList.end() != iter; iter++)
		{
			if (iter->first >= ARRAY_SIZE(pkg.pkgs) || iter->first < 0)
			{
				D_ERROR("CPlayer::SoldGreyPkgItems, iter->first (%d) >= ARRAY_SIZE(pkg.pkgs)\n", iter->first);
				continue;
			}
			CLogManager::DoNpcShopPurchase(this, 0, LOG_SVC_NS::TT_SALE, iter->second, pkg.pkgs[iter->first].usItemTypeID, pkg.pkgs[iter->first].ucCount);//记日至

			//插入到回购列表
			m_cachedRepurchase.InsertItem(pkg.pkgs[iter->first], iter->second);

			SubItemCountByPkgPos( iter->first, pkg.pkgs[iter->first].usItemTypeID, pkg.pkgs[iter->first].ucCount );
		}

		//成功
		resp.resp.retcode =(int)GCBatchSellItemsResp::OK;
	}while(false);

	SendPkg<MGBatchSellItemsResp>(&resp);

	if((int)GCBatchSellItemsResp::OK == resp.resp.retcode)
	{
		//更新回购列表
		RepurchaseListNtf();
	}

	return true;
}

bool CPlayer::RepurchaseItem( const GMRepurchaseItemReq& itemReq)
{
	MGRepurchaseItemResp resp;
	StructMemSet( resp, 0x00, sizeof(resp));
	resp.resp.index = itemReq.req.index;
	resp.resp.itemUId = itemReq.req.itemUId;

	do 
	{
		//从回购列表中查找指定物品
		CachedRepurchaseInfo::RepurchaseInfo_i *pRepurchaseInfo = m_cachedRepurchase.GetItem(itemReq.req.index);
		if(NULL ==  pRepurchaseInfo)
		{
			resp.resp.retcode = GCRepurchaseItemResp::OTHER_ERROR;
			break;
		}

		if(itemReq.req.itemUId != pRepurchaseInfo->itemInfo.uiID)
		{
			resp.resp.retcode = GCRepurchaseItemResp::NO_ITEM;
			break;
		}

		if(pRepurchaseInfo->money > GetSilverMoney())
		{
			resp.resp.retcode = GCRepurchaseItemResp::NO_ENOUGH_MONEY;
			break;
		}

		if(!CanGetItem( pRepurchaseInfo->itemInfo.usItemTypeID, pRepurchaseInfo->itemInfo.ucCount ))
		{
			resp.resp.retcode = GCRepurchaseItemResp::NO_ENOUGH_SPACE;
			break;
		}

		CLogManager::DoNpcShopPurchase(this, 0, LOG_SVC_NS::TT_BUY, pRepurchaseInfo->money, pRepurchaseInfo->itemInfo.usItemTypeID, pRepurchaseInfo->itemInfo.ucCount);//记日至

		//更新金钱
		SubSilverMoney(pRepurchaseInfo->money);//双币种判断

		//更新物品
		int errNo = 0;
		GiveNewItemToPlayer(pRepurchaseInfo->itemInfo, 0, errNo);

		//从回购列表中删除
		m_cachedRepurchase.DeleteItem(itemReq.req.index);
	} while(false);

	SendPkg<MGRepurchaseItemResp>(&resp);
	RepurchaseListNtf();//通知回购列表
	
	return true;
}

void CPlayer::RepurchaseListNtf(void)
{
	MGRepurchaseItemListNtf notify;
	StructMemSet( notify, 0x00, sizeof(notify));

	std::vector<CachedRepurchaseInfo::RepurchaseInfo_i>& cacheList = m_cachedRepurchase.CachedList();
	size_t size = cacheList.size();//size <= MAX_REPURCHASE_COUNT
	if(size > 0)
	{
		for(size_t i = 0; i < size && i < ARRAY_SIZE(notify.ntf.repurchaseList); i++)
		{
			notify.ntf.repurchaseList[i] = cacheList[i].itemInfo.uiID;
		}

		notify.ntf.num = size;
	}

	SendPkg<MGRepurchaseItemListNtf>(&notify);
	return;
}

///重置玩家的移动状态，玩家下线或死亡时必须调用；
void CPlayer::ResetMoveStat()
{

	if ( !(m_MoveInfo.IsMoving()) )
	{
		return;
	}

	//10.03.09，移动bug查找修改,此处，每次停止时，计算出玩家的最新位置并更新
	CMuxMap* pMap = GetCurMap();
	if ( NULL == pMap )
	{
		D_ERROR( "CPlayer::ResetMoveStat, NULL == pMap\n" );
		return;
	}

	{
		//刷新位置
		bool isAchieve = false;
		bool isNeedNoti = true;//因为是停止移动，所以不会有新移动；
		pMap->PlayerMoveOnce( this, true, isAchieve, isNeedNoti );//移动之前，先把上次的移动完，计算下准确的最新位置，并更新最近移动时间；
	}

	m_MoveInfo.ResetMoveStat( m_fCurX, m_fCurY );

	return;
}

///置玩家移动信息；
bool CPlayer::SetMoveInfo( float inSpeed, float inTargetX, float inTargetY, bool& isNeedNoti/*最迟移动通知算法*/ )
{
	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(移动相关)

	return m_MoveInfo.SetMoveInfo( inSpeed, m_fCurX, m_fCurY, inTargetX, inTargetY, isNeedNoti );
}

//void CPlayer::LeaveMapClearNearby()
//{
//	TRY_BEGIN;
//
//	UniqueObj<CPlayer>* pUniquePlayer = NULL;
//	CPlayer* pPlayer = NULL;
//	set< UniqueObj<CPlayer>* >* notiPlayers = m_PlayerNearby.GetNearby();
//	//将自身从附近玩家的附近玩家列表中删去；
//	if ( ( NULL != notiPlayers ) 
//		&& ( !(notiPlayers->empty()) )
//		)
//	{
//		for( set< UniqueObj<CPlayer>* >::iterator iter = notiPlayers->begin(); iter != notiPlayers->end(); )
//		{
//			pUniquePlayer = *iter;
//			if ( NULL == pUniquePlayer )
//			{
//				notiPlayers->erase( iter++ );
//				continue;
//			}
//			pPlayer = pUniquePlayer->GetUniqueObj();
//			if ( NULL == pPlayer )
//			{
//				g_poolManager->Release( pUniquePlayer );//释放UniqueObj<CPlayer>*;
//				notiPlayers->erase( iter++ );
//				continue;
//			}
//			pPlayer->PlayerLeaveView( this );
//			pPlayer->RefreshNearby();//刷新pPlayer的附近玩家列表；
//			++iter;
//		}
//	}
//
//	m_PlayerNearby.Clear();//清自身的附近玩家列表；
//
//	return;
//	TRY_END;
//	return;
//}

//void CPlayer::SendFinalRebirthSuc( TYPE_ID sequenceID )
//{
//	MGFinalRebirthRst sendMsg;
//	sendMsg.sequenceID = sequenceID;
//	sendMsg.lMapCode = GetMapID();
//	sendMsg.lNotiPlayerID = GetFullID();
//	sendMsg.nErrCode = MGFinalRebirthRst::FINAL_REBIRTH_SUC;
//	sendMsg.nPosX = GetPosX();
//	sendMsg.nPosY = GetPosY();
//	SendPkg<MGFinalRebirthRst>( &sendMsg );
//}

void CPlayer::SendEnterMapRstSuc()
{
	MGEnterMapRst sendMsg;
	sendMsg.sequenceID = 0;
	sendMsg.lMapCode = GetMapID();
	sendMsg.lNotiPlayerID = GetFullID();
	sendMsg.nErrCode = MGEnterMapRst::ENTER_MAP_SUC;
	sendMsg.nPosX = GetPosX();
	sendMsg.nPosY = GetPosY();
	SendPkg<MGEnterMapRst>( &sendMsg );

	return;
}


void CPlayer::UpdateGateGoodEvilPoint()		//发送消息更新Gate上的善恶值
{
	MGGateUpdateGoodevilpoint updateMsg;
	updateMsg.goodevilPoint = GetGoodEvilPoint();
	SendPkg<MGGateUpdateGoodevilpoint>( &updateMsg );
}


//更新玩家全部的信息 04.23
bool CPlayer::UpdatePlayerFullInfo( EAppearType infoType )
{
	if ( !(IsInfoFull()) )
	{
		//玩家信息不完整，不能用mapsrv保存的玩家信息更新DB；
		//此时，如果是一般的appear或switch消息，则无需理会，
		//但如果是下线消息，则可能gatesrv还在等待玩家的full info以正确处理玩家下线信息保存，因此应该告知gatesrv;
		if ( ( infoType == INFO_OFFLINE_UPDATE )
			|| ( infoType == INFO_SWITCH_UPDATE )
			)
		{
			D_DEBUG( "通知gate玩家%s信息不完整\n", GetAccount() );//副本调试日志;
			MGFullPlayerInfo playerInfo;
			playerInfo.validFlag = MGFullPlayerInfo::MG_FULLINFO_INVALID;
			playerInfo.infoType = INFO_OFFLINE_UPDATE;
			playerInfo.ucIndex = 0;
			playerInfo.bEnd = true;
			playerInfo.lNotiPlayerID = GetFullID();
			playerInfo.playerID = GetFullID();
			SafeStrCpy( playerInfo.pszAccount, GetAccount() );
			SendPkg< MGFullPlayerInfo >( &playerInfo );			
		}
		return true;
	}

	D_DEBUG( "发送玩家%s的fullinfo给gate，位置%d(%d,%d)\n", GetAccount()
		, m_fullPlayerInfo.baseInfo.usMapID, GetPosX(), GetPosY() );//副本调试日志;
	
	vector<MGFullPlayerInfo> vecFullInfo;

	SendFullStr<FullPlayerInfo, MGFullPlayerInfo>( &m_fullPlayerInfo, vecFullInfo );
	for(vector<MGFullPlayerInfo>::iterator iter = vecFullInfo.begin(); iter != vecFullInfo.end(); ++iter)
	{
		iter->validFlag = MGFullPlayerInfo::MG_FULLINFO_OK;//信息有效；
		iter->playerID = GetFullID();
		iter->infoType = infoType;
		SendPkg< MGFullPlayerInfo >( &(*iter) );
	}

	return true;
}


CNewMap* CPlayer::GetCurMap()
{
	return m_pMap;
}


void CPlayer::SetMap(CNewMap* pMap)
{
	m_pMap = pMap;
	if( pMap != NULL )
		m_fullPlayerInfo.baseInfo.usMapID = (unsigned short)m_pMap->GetMapNo();
}


//计算游戏中的非存盘数据
bool CPlayer::CalculateTmpProp()
{
	CLevelProp* pLevelConfig = m_pClassLevelProp->FindObj( m_fullPlayerInfo.baseInfo.usLevel );
	if( NULL == pLevelConfig )
	{
		D_ERROR("读取等级属性失败 等级:%d\n",m_fullPlayerInfo.baseInfo.usLevel);
		return false;
	}

	StructMemSet( m_TmpProp, 0x0,sizeof(m_TmpProp) );

	//通过配置读取初始属性
	////等级属性配置中不再直接设2级属性，by dzj, 10.05.26, m_TmpProp.nPhysicsAttack = pLevelConfig->m_nPhyAttack;
	//m_TmpProp.nPhysicsDefend = pLevelConfig->m_nPhyDefend;
	//m_TmpProp.nMagicAttack = pLevelConfig->m_nMagAttack;
	//m_TmpProp.nMagicDefend = pLevelConfig->m_nMagDefend;
	//m_TmpProp.lMaxHP = pLevelConfig->lMaxHP;
	//m_TmpProp.lMaxMP = pLevelConfig->lMaxMP;
	//m_TmpProp.nPhysicsHit = pLevelConfig->m_nPhyHit;
	//m_TmpProp.nMagicHit = pLevelConfig->m_nMagHit;
	//不再包含此字段，by dzj, 10.05.26, m_TmpProp.nStrength = pLevelConfig->nStrength;
	//m_TmpProp.nAgility = pLevelConfig->nAgility;
	//m_TmpProp.nIntelligence = pLevelConfig->nIntelligence;
	m_TmpProp.lNextLevelExp = pLevelConfig->lNextLevelExp;
	SetSpeed( ROLE_INIT_SPEED );
	m_TmpProp.lLastLevelExp = 0;
	

	//还有关于技能等级 技能等级的显示
	SkillLevelExp outLevelExp;  //技能等级
	SkillPointExp outPointExp;  //点数配置
	
	//step1 技能等级
	CSkillExp* pSkillExp = CManSkillExp::FindObj( GetClass() );
	if( NULL == pSkillExp )
	{
		D_ERROR("CPlayer::CalculateTmpProp() 找不到职业的配置 该职业为%d\n", GetClass() );
		return false;
	}

	//物理技能等级经验 称号
	if( pSkillExp->FindLevelExp( GetPhyLevel(), outLevelExp ) )
	{
		m_TmpProp.lNextPhyLevelExp = outLevelExp.phyNextLevelExp;
		m_TmpProp.lLastPhyLevelExp = outLevelExp.phyLastLevelExp;
		SafeStrCpy( m_TmpProp.phySkillTitle, outLevelExp.szSkillTitle );
		m_TmpProp.phySkillTitleSize = (USHORT)strlen( m_TmpProp.phySkillTitle ) + 1;
	}

	//魔法技能等级经验 称号
	if( pSkillExp->FindLevelExp( GetMagLevel(), outLevelExp ) )
	{
		m_TmpProp.lNextMagLevelExp = outLevelExp.magNextLevelExp;
		m_TmpProp.lLastMagLevelExp = outLevelExp.magLastLevelExp;
		SafeStrCpy( m_TmpProp.magSkillTitle, outLevelExp.szSkillTitle );
		m_TmpProp.magSkillTitleSize = (USHORT)strlen( m_TmpProp.magSkillTitle ) + 1;
	}

	//物理技能点
	if( pSkillExp->FindPointExp( GetPhyTotalPoint(), outPointExp ) )
	{
		m_TmpProp.lNextPhyPointExp = outPointExp.phyNextPointExp;
		m_TmpProp.lLastPhyPointExp = outPointExp.phyLastPointExp;
	}

	//魔法技能点
	if( pSkillExp->FindPointExp( GetMagTotalPoint(), outPointExp ) )
	{
		m_TmpProp.lNextMagPointExp = outPointExp.magNextPointExp;
		m_TmpProp.lLastMagPointExp = outPointExp.magLastPointExp;
	}

	//接下来计算可获得的最大物理，魔法技能经验
	if( pSkillExp->FindLevelExp( GetMaxPhyLevelLimit(), outLevelExp ) )
	{
		m_TmpProp.maxLimitPhyExp = outLevelExp.phyNextLevelExp;
		m_TmpProp.currentLevelMaxPhyPoint = outLevelExp.usMaxGetPhyPoint;
	}

	if( pSkillExp->FindLevelExp( GetMaxMagLevelLimit(), outLevelExp) )
	{
		m_TmpProp.maxLimitMagExp = outLevelExp.magNextLevelExp;
		m_TmpProp.currentLevelMaxMagPoint = outLevelExp.usMaxGetMagPoint;
	}

	//ReloadSecondProp();
	ItemManagerSingle::instance()->CheckPlayerEquipItem(this);
	return true;
}

//10.09.20已废弃//根据等级计算属性
//bool CPlayer::ReloadLevelProp()
//{
//	CLevelProp* pLevelConfig = m_pClassLevelProp->FindObj( m_fullPlayerInfo.baseInfo.usLevel );
//	if( NULL == pLevelConfig )
//	{
//		D_ERROR("读取等级属性失败 等级:%d\n",m_fullPlayerInfo.baseInfo.usLevel);
//		return false;
//	}
//
//	//通过配置读取初始属性
//	//等级属性配置中不再直接设2级属性，by dzj, 10.05.26, m_TmpProp.nPhysicsAttack = pLevelConfig->m_nPhyAttack;
//	//m_TmpProp.nPhysicsDefend = pLevelConfig->m_nPhyDefend;
//	//m_TmpProp.nMagicAttack = pLevelConfig->m_nMagAttack;
//	//m_TmpProp.nMagicDefend = pLevelConfig->m_nMagDefend;
//	//m_TmpProp.lMaxHP = pLevelConfig->lMaxHP;
//	//m_TmpProp.lMaxMP = pLevelConfig->lMaxMP;
//	//m_TmpProp.nPhysicsHit = pLevelConfig->m_nPhyHit;
//	//m_TmpProp.nMagicHit = pLevelConfig->m_nMagHit;
//	//不再包含此属性，by dzj, 10.05.26, m_TmpProp.nStrength = pLevelConfig->nStrength;
//	//m_TmpProp.nAgility = pLevelConfig->nAgility;
//	//m_TmpProp.nIntelligence = pLevelConfig->nIntelligence;
//	m_TmpProp.lNextLevelExp = pLevelConfig->lNextLevelExp;
//	SetSpeed( ROLE_INIT_SPEED );
//	m_TmpProp.lLastLevelExp = 0;
//	return true;
//}

//10.09.20已废弃//根据物理经验计算相关属性
//bool CPlayer::ReloadPhyProp()
//{
//	//还有关于技能等级 技能等级的显示
//	SkillLevelExp outLevelExp;  //技能等级
//	SkillPointExp outPointExp;  //点数配置
//	
//	//step1 技能等级
//	CSkillExp* pSkillExp = CManSkillExp::FindObj( GetClass() );
//	if( NULL == pSkillExp )
//	{
//		D_ERROR("CPlayer::ReloadPhyProp() 找不到职业的配置 该职业为%d\n", GetClass() );
//		return false;
//	}
//
//	//物理技能等级经验 称号
//	if( pSkillExp->FindLevelExp( GetPhyLevel(), outLevelExp ) )
//	{
//		m_TmpProp.lNextPhyLevelExp = outLevelExp.phyNextLevelExp;
//		m_TmpProp.lLastPhyLevelExp = outLevelExp.phyLastLevelExp;
//		SafeStrCpy( m_TmpProp.phySkillTitle, outLevelExp.szSkillTitle );
//		m_TmpProp.phySkillTitleSize = (USHORT)strlen( m_TmpProp.phySkillTitle ) + 1;
//	}
//
//	//物理技能点
//	if( pSkillExp->FindPointExp( GetPhyTotalPoint(), outPointExp ) )
//	{
//		m_TmpProp.lNextPhyPointExp = outPointExp.phyNextPointExp;
//		m_TmpProp.lLastPhyPointExp = outPointExp.phyLastPointExp;
//	}
//
//	if( pSkillExp->FindLevelExp( GetMaxPhyLevelLimit(), outLevelExp ) )
//	{
//		m_TmpProp.maxLimitPhyExp = outLevelExp.phyNextLevelExp;
//		m_TmpProp.currentLevelMaxPhyPoint = outLevelExp.usMaxGetPhyPoint;
//	}
//	return true;
//}

//10.09.20已废弃//根据魔法经验计算相关属性
//bool CPlayer::ReloadMagProp()
//{
//	//还有关于技能等级 技能等级的显示
//	SkillLevelExp outLevelExp;  //技能等级
//	SkillPointExp outPointExp;  //点数配置
//	
//	//step1 技能等级
//	CSkillExp* pSkillExp = CManSkillExp::FindObj( GetClass() );
//	if( NULL == pSkillExp )
//	{
//		D_ERROR("CPlayer::ReloadMagProp() 找不到职业的配置 该职业为%d\n", GetClass() );
//		return false;
//	}
//
//	//魔法技能等级经验 称号
//	if( pSkillExp->FindLevelExp( GetMagLevel(), outLevelExp ) )
//	{
//		m_TmpProp.lNextMagLevelExp = outLevelExp.magNextLevelExp;
//		m_TmpProp.lLastMagLevelExp = outLevelExp.magLastLevelExp;
//		SafeStrCpy( m_TmpProp.magSkillTitle, outLevelExp.szSkillTitle );
//		m_TmpProp.magSkillTitleSize = (USHORT)strlen( m_TmpProp.magSkillTitle ) + 1;
//	}
//
//	//魔法技能点
//	if( pSkillExp->FindPointExp( GetMagTotalPoint(), outPointExp ) )
//	{
//		m_TmpProp.lNextMagPointExp = outPointExp.magNextPointExp;
//		m_TmpProp.lLastMagPointExp = outPointExp.magLastPointExp;
//	}
//
//	if( pSkillExp->FindLevelExp( GetMaxMagLevelLimit(), outLevelExp) )
//	{
//		m_TmpProp.maxLimitMagExp = outLevelExp.magNextLevelExp;
//		m_TmpProp.currentLevelMaxMagPoint = outLevelExp.usMaxGetMagPoint;
//	}
//	return true;
//}

//重新计算二级属性
bool CPlayer::ReloadSecondProp()
{
	CCalSecondPropertyScript * calScript = CCalSecondPropertyScriptManager::GetScript();
	if ( NULL == calScript )
	{
		D_ERROR( "CPlayer::ReloadSecondProp, NULL == calScript\n" );
		return false;
	}

	int value = 0;

	calScript->FnCalPhyAtk( this, value );
	m_TmpProp.nPhysicsAttack = value;
	calScript->FnCalPhyDef( this, value );
	m_TmpProp.nPhysicsDefend = value;
	calScript->FnCalPhyHit( this, value );
	m_TmpProp.nPhysicsHit = value;
	calScript->FnCalPhyJouk( this, value );
	m_TmpProp.nPhysicsJouk = value;
	calScript->FnCalMagAtk( this, value );
	m_TmpProp.nMagicAttack = value;
	calScript->FnCalMagDef( this, value );
	m_TmpProp.nMagicDefend = value;
	calScript->FnCalMagHit( this, value );
	m_TmpProp.nMagicHit = value;
	calScript->FnCalMagJouk( this, value );
	m_TmpProp.nMagicJouk = value;
	calScript->FnCalExcellent( this, value );
	m_TmpProp.Excellent = value;
	calScript->FnCalCritical( this, value );
	m_TmpProp.Critical = value;
	calScript->FnCalHP( this, value ); //最大血值
	m_TmpProp.lMaxHP = value;
	calScript->FnCalHPrecovery( this, value );//血值回复
	m_TmpProp.lHpRecover = value;
	calScript->FnCalMP( this, value ); //最大MP
	m_TmpProp.lMaxMP = value;
	calScript->FnCalMPrecovery( this, value );//MP回复
	m_TmpProp.lMpRecover = value;

	return true;
}

bool CPlayer::BuffDataArrayCheck( unsigned int index )
{
	if( index >= ARRAY_SIZE(m_fullPlayerInfo.bufferInfo.buffData) )
		return false;
	return true;
}


void CPlayer::SendSelfInfoToOtherMembers()
{
	m_isNeedNotiTeamMembers = false;//每次通知过后重设为false;

	if( !m_stateManager.IsGroupState() )
	{
		return;
	}

	MGTeamMemberDetailInfoUpdate detailInfo;
	detailInfo.mapID = GetMapID();
	detailInfo.nPosX = GetPosX();
	detailInfo.nPosY = GetPosY();
	detailInfo.uiHP = GetCurrentHP();
	detailInfo.uiMP = GetCurrentMP();
	detailInfo.maxHP = GetMaxHP();
	detailInfo.maxMP = GetMaxMP();
	detailInfo.teamMemberID = GetFullID();
	detailInfo.buffData = *GetBuffData();
	detailInfo.usLevel = GetLevel();
	detailInfo.goodevilPoint = GetGoodEvilPoint();

	CGroupTeam* pTeam = m_stateManager.GetGroupTeam();
	if( NULL == pTeam)
	{
		D_WARNING("CPlayer::SendSelfInfoToOtherMembers 组队状况下找不到队伍\n");
		return;
	}
	pTeam->SendTeamMemberDetailInfo( &detailInfo, this );
}

void CPlayer::SetSpeedDown( bool bFlag )	//设置减速标志
{
	m_buffManager.SetSpeedDown( bFlag );
}

bool CPlayer::IsSpeedDown()
{
	return m_buffManager.IsSpeedDown();
}


bool CPlayer::OnSelfExplosion()
{
	vector<CPlayer*> vecPlayer;
	vector<CMonster*> vecMonster;
	
	CMuxMap* pMap = GetCurMap();
	if( NULL == pMap )
		return false;

	CMuxSkillProp* pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( SELF_EXPLOSION_SKILL_ID );
	if( NULL == pSkillProp )
	{
		D_ERROR("找不到自爆技能的属性\n");
		return false;
	}

	//获取对象
	pMap->OnPlayerSkillGetAoeTarget( this, pSkillProp->m_range, GetPosX(), GetPosY(), vecPlayer, vecMonster );

	CPlayer* pTmpPlayer = NULL;
	for( size_t i = 0; i < vecPlayer.size(); ++i )
	{
		pTmpPlayer = vecPlayer[i];
		if( NULL == pTmpPlayer )
			continue;

		//自己不能自爆
		if( pTmpPlayer == this )
			continue;
		
		pTmpPlayer->OnBeExplosion( this, pSkillProp, GetSelfExplosionArg() );
	}

	CMonster* pTmpMonster = NULL;
	for( size_t i = 0; i < vecMonster.size(); ++i )
	{
		pTmpMonster = vecMonster[i];
		if( NULL == pTmpMonster )
			continue;

		pTmpMonster->OnBeExplosion( this, pSkillProp, GetSelfExplosionArg() );
	}

	MGUseSkillRst gcSkill;
	gcSkill.skillRst.skillID = SELF_EXPLOSION_SKILL_ID;
	gcSkill.skillRst.skillType = EXPLOSION_SKILL;
	gcSkill.skillRst.lCurseTime = pSkillProp->m_nCurseTime;
	gcSkill.skillRst.lCooldownTime = pSkillProp->m_nCoolDown;
	gcSkill.skillRst.scopeFlag = SCOPE_ATTACK_END;
	gcSkill.skillRst.nErrCode = BATTLE_SUCCESS;
	gcSkill.skillRst.attackID = GetFullID();


	pMap->NotifySurrounding<MGUseSkillRst>( &gcSkill, GetPosX(), GetPosY() );
	return true;
}


void CPlayer::OnBeExplosion( CPlayer *pExplosionPlayer, CMuxSkillProp* pSkillProp, unsigned int exArg )
{
	if( NULL == pExplosionPlayer || NULL == pSkillProp )
		return;

	CMuxMap* pMap = GetCurMap();
	if( NULL == pMap )
		return;


	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		return;
	}

	vector<int> vecRst;
	if( !pScript->FnExlosionPvp( pExplosionPlayer, this, exArg, &vecRst ) )
	{
		//脚本调用错误
		return;
	}

	if( vecRst.size() != 2 )
	{
		D_ERROR("FnExlosionPvp 返回值数量错误\n");
		return;
	}

	unsigned int damage = vecRst[0];
	EBattleFlag exFlag = (EBattleFlag)vecRst[1];

	if ( (damage <= 0) && (exFlag != MISS) )
	{
		D_WARNING( "CPlayer::OnBeExplosion，FnExlosionPvp, 战斗脚本伤血为0但返回为非MISS, exArg%d\n", exArg );
	}
	
	SubPlayerHp( damage, pExplosionPlayer, pExplosionPlayer->GetFullID() );

	MGUseSkillRst rst;
	rst.skillRst.attackHP = pExplosionPlayer->GetCurrentHP();
	rst.skillRst.attackID = pExplosionPlayer->GetFullID();
	rst.skillRst.attackMP = pExplosionPlayer->GetCurrentMP();
	rst.skillRst.battleFlag = exFlag;
	rst.skillRst.causeBuffID = 0;
	rst.skillRst.lCooldownTime = 0;
	rst.skillRst.lHpSubed = damage;
	rst.skillRst.lHpLeft = GetCurrentHP();
	rst.skillRst.nErrCode = BATTLE_SUCCESS;
	rst.skillRst.scopeFlag = SCOPE_ATTACK;
	rst.skillRst.skillID = SELF_EXPLOSION_SKILL_ID;
	rst.skillRst.skillType = EXPLOSION_SKILL;
	rst.skillRst.targetID = GetFullID();
	rst.skillRst.lCooldownTime = pSkillProp->m_nCoolDown;
	rst.skillRst.lCurseTime = pSkillProp->m_nCurseTime;
	rst.skillRst.sequenceID = 0;
	
	pMap->NotifySurrounding<MGUseSkillRst>( &rst, GetPosX(), GetPosY() );
}


void CPlayer::AddMuxBuffEffect( MuxBuffProp* pBuffProp )
{
	m_buffManager.AddMuxBuffEffect( pBuffProp );
}


void CPlayer::DelMuxBuffEffect( MuxBuffProp* pBuffProp, unsigned int buffLevel )
{
	m_buffManager.DelMuxBuffEffect( pBuffProp, buffLevel );
}

void CPlayer::DieDropExp(void)
{
	if ( GetGoodEvilPoint() < 0 )//红名玩家死亡时，需要扣身上的经验
	{
		int precent = 0 , minexp = 0 , maxexp = 0;
		DiePunishInfoSingle::instance()->GetEvilExpInfo( precent, minexp, maxexp );

		unsigned int oldExp = m_fullPlayerInfo.baseInfo.dwExp;
		//所有人现在都只扣%
		unsigned long nextLelveNeedExp = this->GetNextLevelExp() - m_TmpProp.lLastLevelExp;
		unsigned long currentExp = GetExp();
		unsigned long lostExp = (unsigned long)(precent / 100.0f * nextLelveNeedExp);
		if ( lostExp < minexp )
			lostExp = minexp;
		else if( lostExp > maxexp )
			lostExp = maxexp;

		if( lostExp >= currentExp )
		{
			m_fullPlayerInfo.baseInfo.dwExp = 1;
		}else{
			m_fullPlayerInfo.baseInfo.dwExp -= lostExp;
		}
		MGRoleattUpdate attUpdate;
		attUpdate.bFloat = false;
		attUpdate.changeTime = 0;
		attUpdate.nNewValue = GetExp();
		attUpdate.nAddType = 0;
		attUpdate.lChangedPlayerID = GetFullID();
		attUpdate.nType = RT_EXP;
		SendPkg<MGRoleattUpdate>(&attUpdate);

		ItemInfo_i items[1];
		CLogManager::DoPlayerDead(this, 2, 0, oldExp - m_fullPlayerInfo.baseInfo.dwExp, 0, items);//记日至
	}
}


void CPlayer::SetFollowingMonster( CMonster* pFollowingMonster )
{
	if ( NULL != m_pFollowingMonster )
	{
		m_pFollowingMonster->ClearFollowing();//清旧跟随怪物的跟随玩家信息；
	}
	m_pFollowingMonster = pFollowingMonster;
}

bool CPlayer::UpdateLevelExp(int nLevel)
{
	TRY_BEGIN;
	CClassLevelProp* pClassLevelProp = CManPlayerProp::FindObj( GetClass() );
	if( NULL == pClassLevelProp )
	{
		D_ERROR("CPlayer::UpdateLevelExp 找不到该职业的属性 职业为%d\n", GetClass() );
		return false;
	}

	CLevelProp* pLevelProp = pClassLevelProp->FindObj( nLevel);
	if( NULL == pLevelProp )
	{
		D_ERROR("CPlayer::UpdateLevelExp 找不到该职业等级配置 等级为%d\n", nLevel);
		return false;
	}
	m_TmpProp.lNextLevelExp = pLevelProp->lNextLevelExp;
	return true;
	TRY_END;
	return false;
}

//发送NPC对话界面消息，由CNpcChatScript调用；
void CPlayer::SendNpcUiInfo( const char* szChat )
{
	MGChat npcUiMsg;
	SafeStrCpy( npcUiMsg.strChat, szChat );
	npcUiMsg.chatType = CT_NPC_UIINFO;
	npcUiMsg.extInfo = 0;//无额外信息；
	npcUiMsg.sourcePlayerID.wGID = MONSTER_GID;
	CMonster* pMonster = m_npcChatInfo.GetNpcChatTarget();
	if ( NULL == pMonster )
	{
		D_WARNING( "CPlayer::SendNpcUiInfo, 所对话的NPC空\n" );
		return;
	}
	npcUiMsg.sourcePlayerID.dwPID = pMonster->GetMonsterID();
	SendPkg<MGChat>( &npcUiMsg );
	return;
}

//发送Item对话界面消息，由CItemChatScript调用；
void CPlayer::SendItemUiInfo( const char* szChat )
{
	MGChat npcUiMsg;
	SafeStrCpy( npcUiMsg.strChat, szChat );
	npcUiMsg.chatType = CT_ITEM_UIINFO;
	npcUiMsg.extInfo = 0;//无额外信息；
	npcUiMsg.sourcePlayerID.wGID = 0;
	CBaseItem* pItem = m_itemChatInfo.GetItemChatTarget();
	if ( NULL == pItem )
	{
		D_WARNING( "CPlayer::SendItemUiInfo, 所对话的Item空\n" );
		return;
	}
	npcUiMsg.sourcePlayerID.dwPID = (TYPE_ID) pItem->GetItemUID();
	SendPkg<MGChat>( &npcUiMsg );
	return;
}

//发送区域任务对话界面消息,由CNormalScript调用
void CPlayer::SendAreaUiInfo(const char* szChat )
{
	MGChat npcUiMsg;
	SafeStrCpy( npcUiMsg.strChat, szChat );
	npcUiMsg.chatType = CT_AREA_UIINFO;
	npcUiMsg.extInfo = 0;//无额外信息；
	npcUiMsg.sourcePlayerID.wGID = GetGID();
	npcUiMsg.sourcePlayerID.dwPID = (TYPE_ID)GetPlayerUID();
	SendPkg<MGChat>( &npcUiMsg );
	return;
}

//09.04.28,取消工会任务新实现,///发送Task对话界面消息，由CNormalScript调用；
//void CPlayer::SendTaskUiInfo( const char* szChat )
//{
//	//取消工会任务新方案，by dzj, 09.04.28，MGChat npcUiMsg;
//	//SafeStrCpy( npcUiMsg.strChat, szChat );
//	//npcUiMsg.nType = CT_NPC_UIINFO;
//	//npcUiMsg.sourcePlayerID.wGID = 0;
//	//npcUiMsg.sourcePlayerID.dwPID = (TYPE_ID)this->GetPlayerUID();
//	//SendPkg<MGChat>( &npcUiMsg );
//	return;
//}

bool CPlayer::OnChatToNpc2(CMonster* pMonster ,int nOption, const char* szChat )
{
	m_npcChatInfo.SetNpcChatTarget( pMonster, nOption );
	MGChat npcUiMsg;
	SafeStrCpy( npcUiMsg.strChat, szChat );
	npcUiMsg.chatType = CT_NPC_UIINFO;
	npcUiMsg.extInfo = 0;//无额外信息；
	npcUiMsg.sourcePlayerID.wGID = 0;
	npcUiMsg.sourcePlayerID.dwPID = pMonster->GetMonsterID();
	SendPkg<MGChat>( &npcUiMsg );
	return true;
}

///发送系统聊天消息；
void CPlayer::SendSystemChat(const char* szChat)
{
	MGChat sysMsg;
	SafeStrCpy( sysMsg.strChat, szChat );
	sysMsg.chatType = CT_SYS_EVENT_NOTI;
	sysMsg.extInfo = 0;//无额外信息；
	SendPkg<MGChat>( &sysMsg );
}

//是否是队长
bool CPlayer::IsTeamCaptain()
{
	//如果不是组队，肯定不是队长
	if( !IsInTeam() )
	{
		return false;
	}
	
	CGroupTeam* pTeam = GetGroupTeam();
	if( NULL == pTeam )
	{
		D_ERROR( "有组队却找不到队伍\n" );
		return false;
	}

	PlayerID tmpID;
	pTeam->GetGroupTeamLeaderID( tmpID );
	if ( tmpID == GetFullID() )
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool CPlayer::IsInTeam()
{
	return m_stateManager.IsGroupState();
}


CGroupTeam* CPlayer::GetGroupTeam()
{
	return m_stateManager.GetGroupTeam();
}


void CPlayer::NotifySpeedChange()
{
	MGRoleattUpdate updateMsg;
	updateMsg.bFloat = true;
	updateMsg.changeTime = 0;
	updateMsg.nAddType = MGRoleattUpdate::TYPE_SET;
	updateMsg.nNewValue = (int)(GetSpeed() * 100);
	updateMsg.nType = RT_SPEED;
	updateMsg.lChangedPlayerID = GetFullID();
	
	CMuxMap* pMap = GetCurMap();
	if( NULL == pMap )
		return;
	pMap->NotifySurrounding<MGRoleattUpdate>( &updateMsg, GetPosX(), GetPosY() );
}

void CPlayer::NotifyNextLevelExp()
{
	MGRoleattUpdate updatemsg;
	updatemsg.bFloat = false;
	updatemsg.changeTime = 0;
	updatemsg.lChangedPlayerID = GetFullID();
	updatemsg.nAddType = 0;
	updatemsg.nOldValue = 0;
	updatemsg.nNewValue = GetTmpProp().lNextLevelExp;
	updatemsg.nType = RT_NEXE_LEVEL_EXP;
	SendPkg<MGRoleattUpdate>(&updatemsg);//总的经验变化
}

//通知HP变化
void CPlayer::NotifyHpChange()
{
	MGRoleattUpdate updateMsg;
	updateMsg.bFloat = false;
	updateMsg.changeTime = 0;
	updateMsg.nAddType = MGRoleattUpdate::TYPE_SET;
	updateMsg.nNewValue = GetCurrentHP();
	updateMsg.nType = RT_HP;
	updateMsg.lChangedPlayerID = GetFullID();
	CMuxMap* pPlayerMap = GetCurMap();
	if ( pPlayerMap )
		pPlayerMap->NotifySurrounding<MGRoleattUpdate>( &updateMsg, GetPosX(),GetPosY() );
}

void CPlayer::NotifyMaxHPChange()
{
	MGRoleattUpdate updateMsg;
	updateMsg.bFloat = false;
	updateMsg.changeTime = 0;
	updateMsg.nAddType = MGRoleattUpdate::TYPE_SET;
	updateMsg.nNewValue = GetMaxHP();
	updateMsg.nType = RT_HPMAX;
	updateMsg.lChangedPlayerID = GetFullID();

	CMuxMap* pPlayerMap = GetCurMap();
	if ( NULL == pPlayerMap )
	{
		return;
	}
	pPlayerMap->NotifySurroundingExceptPlayer<MGRoleattUpdate>( &updateMsg, GetPosX(), GetPosY() , GetFullID() );
	//if ( pPlayerMap )
	//	pPlayerMap->NotifySurroundingPlayersExceptThisPlayer<MGRoleattUpdate>( &updateMsg, GetPosX(),GetPosY(), this );
}

void CPlayer::NotifyMaxMPChange()
{
	MGRoleattUpdate updateMsg;
	updateMsg.bFloat = false;
	updateMsg.changeTime = 0;
	updateMsg.nAddType = MGRoleattUpdate::TYPE_SET;
	updateMsg.nNewValue = GetMaxMP();
	updateMsg.nType = RT_MPMAX;
	updateMsg.lChangedPlayerID = GetFullID();

	CMuxMap* pPlayerMap = GetCurMap();
	if ( NULL == pPlayerMap )
	{
		return;
	}
	pPlayerMap->NotifySurroundingExceptPlayer<MGRoleattUpdate>( &updateMsg, GetPosX(), GetPosY() , GetFullID() );
	//if ( pPlayerMap )
	//	pPlayerMap->NotifySurroundingPlayersExceptThisPlayer<MGRoleattUpdate>( &updateMsg, GetPosX(),GetPosY(), this );
	return;
}

//通知MP变化了
void CPlayer::NotifyMpChange()
{
	MGRoleattUpdate updateMpMsg;
	updateMpMsg.bFloat = false;
	updateMpMsg.changeTime = 0;
	updateMpMsg.nAddType = MGRoleattUpdate::TYPE_SET;
	updateMpMsg.nNewValue = GetCurrentMP();
	updateMpMsg.nType = RT_MP;
	updateMpMsg.lChangedPlayerID = GetFullID();
	CMuxMap* pPlayerMap = GetCurMap();
	if ( pPlayerMap )
		pPlayerMap->NotifySurrounding<MGRoleattUpdate>( &updateMpMsg, GetPosX(),GetPosY() );
}

///检查是否有特定类型NPC跟随玩家；
bool CPlayer::CheckFollowNpc( unsigned int  monsterType )
{
	if ( NULL == m_pFollowingMonster )
	{
		return false;
	}
	if ( m_pFollowingMonster->GetClsID() != monsterType )
	{
		return false;
	}
	//的确为此种类型的跟随NPC，返回真；
	return true;
}

///删去跟随玩家的特定类型NPC，如果有此种类型NPC，则返回true，否则返回false;
bool CPlayer::DelFollowNpc( unsigned int monsterType )
{
	if ( NULL == m_pFollowingMonster )
	{
		return false;
	}
	if ( m_pFollowingMonster->GetClsID() != monsterType )
	{
		return false;
	}
	//的确为此种类型的跟随NPC，将此跟随NPC删去，同时返回真；
	m_pFollowingMonster->OnMonsterDie( NULL /*自然删去，没有人杀NPC*/, GCMonsterDisappear::M_VANISH );
	//ClearFollowingMonster();//其实，在OnMonsterDie内部已经调用了ClearFollowingMonster();
	return true;
}

//减玩家MP，若减成功返回true，否则返回false;
bool CPlayer::SubPlayerMp( int subVal/*减MP绝对值*/, float subPercent/*减MP百分比*/ )
{
	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(MP)
	SetNeedNotiTeamMember();

	if ( GetCurrentMP() <=0 )
	{
		//剩余MP已为0;
		if ( ( subVal > 0 ) || ( subPercent > 0 ) )
		{
			return false;
		} else {
			return true;//剩余MP虽为0，但本次的确不减MP，则也返回真
		}
	}

	if ( subVal > 0 )
	{
		if( subVal > GetCurrentMP() )
		{
			//MP扣光
			SetPlayerMp( 0 );
			return true;
		}
		m_fullPlayerInfo.baseInfo.uiMP -= subVal;	//减去MP
	} else if ( subPercent > 0 ) {
		if ( subPercent >= 1.0f )
		{
			//MP扣光
			SetPlayerMp( 0 );
			return true;
		}
		m_fullPlayerInfo.baseInfo.uiMP -= (unsigned int)(m_fullPlayerInfo.baseInfo.uiMP*subPercent);
	} else {
		//D_ERROR( "扣玩家%s的MP，所扣值小于等于0" );
		//return false;
	}

	return true;		
}

void CPlayer::SubPlayerHp(int nSubHP, CPlayer* pKiller,const PlayerID& attackID )
{
	if ( IsDying() )
	{
		D_DEBUG( "CPlayer::SubHP，玩家%s已死，不再受(%d:%d)攻击\n", GetAccount(), attackID.wGID, attackID.dwPID );
		return;//已死，不再重复减血;
	}

	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(HP)

	EndAnamorphic();//伤血结束变形状态

	if ( ( nSubHP >= (int)GetCurrentHP() ) //减血太多；
		|| ( GetCurrentHP() <= 0 ) //或者当前血量已经小于0;
		)
	{
		SetPlayerHp( 0 );
		SetPlayerMp( 0 );
		//以后可能还需要发送玩家死亡的消息
		
		if( NULL == pKiller )
		{
			CGateSrv* pGateSrv = CManG_MProc::FindProc( attackID.wGID );
			if( pGateSrv )
			{
				pKiller = pGateSrv->FindPlayer( attackID.dwPID );
			}
		}
		DieProcess( pKiller, attackID );
		return;
	}

	m_fullPlayerInfo.baseInfo.uiHP -= nSubHP;

	SetNeedNotiTeamMember();
	return;
}

void CPlayer::KillerOther(CPlayer* pBeKiller )
{
	if ( !pBeKiller )
		return;

	if ( pBeKiller->GetRace() != GetRace() )//不同种族才进行杀人计数
	{
		TryCounterEvent( CT_PK, pBeKiller->GetRace() , 1 );//检查杀人者身上的杀人计数器；
	}
	
	SetKilledCount( pBeKiller->GetDBID() );		//计算是否重复杀人
	m_goodEvilMan.OnKillerOtherPlayer( pBeKiller );
#ifdef OPEN_PUNISHMENT
	if( IsHavePunishTask() )
	{
		m_punishTask.OnKillPlayer( pBeKiller );	
	}
#endif //OPEN_PUNISHMENT
	m_racePrestigeManager.OnKillOther( pBeKiller );

	CLogManager::DoKillPlayer(this, pBeKiller);//记日至

	const char *beKilledPlayerName  = pBeKiller->GetNickName();
	AddFriendTweet(MUX_PROTO::TWEET_TYPE_KILL_PLAYER, (const void *)&beKilledPlayerName, (unsigned short)strlen(beKilledPlayerName));//记录好友动态
}


//当杀死NPC的时候
void CPlayer::OnKillNpc( CMonster* pDeadNpc )
{
	if( NULL == pDeadNpc )
		return;

	m_goodEvilMan.OnKillNpc( pDeadNpc );
	m_racePrestigeManager.OnKillNpc( pDeadNpc );

	CLogManager::DoMonsterDead(this, pDeadNpc);//记日至
}

void CPlayer::FightStartCheckRogueState( CPlayer* pTarget, bool isRogue )
{
	if ( !pTarget  )
		return;

	if( IsRookieState() )
	{
		CreateRookieBreak();
	}

	if ( pTarget->GetRace() != GetRace() )
		return;

	if( GetGoodEvilMode() == EVIL_MODE )
		return;

	//攻击处于恶魔状态的玩家,玩家不变为流氓状态
	GOODEVIL_MODE mode = pTarget->GetGoodEvilMode();
	if ( mode == EVIL_MODE )
		return;

	//如果目标处于流氓状态,则不将自身设为流氓状态
	if ( pTarget->IsRogueState() || isRogue )//可能攻击时对方是流氓状态，但被打死后流氓状态取消，所以此时判断不准确，增加传入被打前的状态isRogue判断
		return;

	//如果被攻击玩家不处于流氓状态(攻击处于流氓状态的玩家,不改变自身的流氓状态)
	CreateRogueState();
}

GOODEVIL_MODE CPlayer::GetGoodEvilMode()
{
	short goodEvilPoint = GetGoodEvilPoint();
	if ( goodEvilPoint == 0 )
		return NORMAL_MODE;

	return goodEvilPoint > 0? HERO_MODE:EVIL_MODE;
}

void CPlayer::StartRogueState()
{
	m_goodEvilMan.StartRogueState();
}

void CPlayer::EndRogueState()
{
	m_goodEvilMan.EndRogueState();
}

void CPlayer::BeKilled( CPlayer* pKiller )
{
	if ( !pKiller )
		return;
}

///死亡处理；
void CPlayer::DieProcess( CPlayer* pKiller, const PlayerID& attackID )
{
	TRY_BEGIN;

	D_DEBUG( "玩家%s死亡，攻击者:%d|%d\n", GetAccount(), attackID.wGID, attackID.dwPID );
	
	unsigned int killerID = (NULL == pKiller) ? attackID.dwPID : pKiller->GetPlayerUID();
	ItemInfo_i items[1];
	CLogManager::DoPlayerDead(this, 2, killerID, 0, 0, items);

	const char* name = GetNickName();
	AddFriendTweet(MUX_PROTO::TWEET_TYPE_DEAD, (const void *)&name, (unsigned short)strlen(name));//记录好友动态
	
#ifdef STAGE_MAP_SCRIPT
	{
		CMuxMap* pMap = GetCurMap();
		if( pMap )
		{
			pMap->OnPlayerDieScriptHandle( this );
		}
	}
#endif //STAGE_MAP_SCRIPT


	if( attackID.wGID != MONSTER_GID )
	{
		if ( NULL != pKiller )
		{
			//如果被其他玩家杀死，进入善恶值流程
			pKiller->KillerOther( this );
		}	

		if ( GetFullID() != attackID )
		{
			//发送添加仇人信息
			MGAddFoeInfo addmsg;
			addmsg.foePlayerID = attackID;
			addmsg.playerUID   = GetPlayerUID();
			addmsg.incidentPosX = GetPosX();
			addmsg.incidentPosY = GetPosY();
			CGateSrv* pGateSrv = CManG_MProc::FindProc( attackID.wGID );
			if ( pGateSrv )
			{
				MsgToPut* pNewMsg = CreateSrvPkg( MGAddFoeInfo, pGateSrv, addmsg );
				pGateSrv->SendPkgToGateServer( pNewMsg );
			}
		}
	}

	//如果在自爆状态,则对周围玩家产生自爆
	if( IsSelfExplosion() )
	{
		OnSelfExplosion();
	}

	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(死亡导致状态改变)
	m_fullPlayerInfo.stateInfo.SetDie( this );//将玩家状态设置为死亡

	//如果处于流氓状态，则结束流氓状态
	if( IsHaveRogueState() )
	{
		m_pRogueBuff->BuffEnd();
	}

	//死亡时自动把宠物召回
	if ( IsSummonPet() )
	{
		ClosePet();
	}

	OnDieClosePlate();//死亡时判断关闭托盘

	//死亡关闭答题托盘
	GetPlayerOLTestPlate().RefreshOLTest();

	OnBuffDie();  //消除BUFF

	LeaveAffectTrade(); //死亡影响交易状态

	DieAffectWear(); //死亡消耗耐久度

	DieDropExp();//死亡掉落经验

	GetSelfStateManager().OnPlayerDie();//死亡时解除玩家身上的状态

	CItemPackageManagerSingle::instance()->DiedPlayerDropItems( this );//死亡后掉落物品

	SetNeedNotiTeamMember();//通知组队的成员，玩家的信息发生变化

	EndAnamorphic();//变形状态终止

//#ifdef RANKCHART_OPEN
	//RankEventManagerSingle::instance()->MonitorPlayerDie( this );
	RankEventManager::MonitorPlayerDie( this );
//#endif //RANKCHART_OPEN

	//死亡做为关键事件，重新存盘
	UpdatePlayerFullInfo( INFO_NORMAL_UPDATE );


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// by dzj, 09.01.05,FLAG0001,此处不删，只是标记待后续遍历列表时删，以防止在遍历moving_player_list过程中，
	// AI杀死移动玩家导致的down机问题, 虽然葛清龙已做了修改只在update时进行攻击，但保险起见仍做此处理。...
	//if ( NULL != pMap )
	//{
	//	if ( IsInMovingList() )
	//	{
	//		pMap->DelPlayerFromMovingList( this );
	//	}
	//}
	// ...,by dzj, 09.01.05，此处不删，只是标记待后续遍历列表时删，以防止在遍历moving_player_list过程中，
	// AI杀死移动玩家导致的down机问题, 虽然葛清龙已做了修改只在update时进行攻击，但保险起见仍做此处理。
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	ResetMoveStat();

	CMuxMap* pMap = GetCurMap();
	if ( NULL != pMap )
	{
		//地图范围通知，玩家死亡；
		pMap->PlayerDieNotify( this );
	}		

	TRY_END;
}

///单个玩家重置位置，置位置的最小单位；
void CPlayer::OnePlayerForceSetFloatPos( bool isManualSet/*是否手动重置(非移动)*/, float newFloatX, float newFloatY, bool& isResetBlock/*是否重设了块*/ )
{
	isResetBlock = false;
	if ( NULL == m_pMap )
	{
		return;
	}

	//float orgFloatX   = GetFloatPosX();
	//float orgFloatY   = GetFloatPosY();
	TYPE_POS orgGridX = GetPosX();
	TYPE_POS orgGridY = GetPosY();
	unsigned short orgBlockX=0, orgBlockY=0;

	TYPE_POS newGridX=0, newGridY=0;
	unsigned short newBlockX=0, newBlockY=0;
	MuxMapSpace::GridToBlockPos( orgGridX,  orgGridY,  orgBlockX, orgBlockY );
	MuxMapSpace::FloatToGridPos( newFloatX, newFloatY, newGridX,  newGridY );
	MuxMapSpace::GridToBlockPos( newGridX,  newGridY,  newBlockX, newBlockY );		

	if ( !( m_pMap->IsGridValid( newGridX, newGridY ) ) )
	{
		D_WARNING( "ForceSetFloatPos, 新点(%f,%f)越界\n", newFloatX, newFloatY );
		return;
	}

	//新点合法，将玩家设至新点，并进行相应消息通知；
	m_fCurX = newFloatX;
	m_fCurY = newFloatY;
	SetPlayerPos( newGridX, newGridY, false );

	if ( ( orgBlockX != newBlockX )
		|| ( orgBlockY != newBlockY )
		|| isManualSet
		)
	{
		SetNeedNotiTeamMember();//块位置改变，需要通知队友；

		isResetBlock = true;

		if ( isManualSet )
		{
			//非移动状态下重置玩家位置，需要先通知客户端玩家位置改变，防止玩家先收到周围信息时，因为距离过远忽略(移动跨块时有可能客户端也会忽略进入ＮＰＣ的消息，但会在稍后的玩家自身跨块消息发来时再次得到正确消息)
			MGSetPlayerPos returnMsg;
			returnMsg.lNotiPlayerID = GetFullID();
			returnMsg.setpos.posX = (TYPE_POS)newFloatX;
			returnMsg.setpos.posY = (TYPE_POS)newFloatY;
			returnMsg.setpos.playerID = GetFullID();
			returnMsg.setpos.reason = POS_INNER_JUMP;//原因暂时都用跳地图；
			SendPkg<MGSetPlayerPos>( &returnMsg );
		}

		//块位置发生变化；
		m_pMap->AdjustPlayerBlock( isManualSet, this, orgBlockX, orgBlockY, newBlockX, newBlockY );
	}

	m_pMap->PlayerMoveToNewPos( this, orgGridX, orgGridY, newGridX, newGridY );

	return;
}

//重设玩家位置；
void CPlayer::ForceSetFloatPos( bool isManualSet/*是否手动重置(非移动)*/, float newFloatX, float newFloatY )
{
	bool isResetBlock = false;
	OnePlayerForceSetFloatPos( isManualSet, newFloatX, newFloatY, isResetBlock );

	CRideState& rideState = GetSelfStateManager().GetRideState();
	//仅当骑乘状态激活时，且玩家是队长时
	if ( rideState.IsActive() &&  rideState.GetPlayerRideType() == CRideState::E_LEADER )
	{
		RideTeam* pTeam = rideState.GetRideTeam();
		if ( !pTeam)
		{
			D_ERROR( "出错,%s处于骑乘状态但是竟然找不到骑乘队伍指针\n", GetAccount() );
			return;
		}

		if ( isResetBlock )
		{
			//通知周围的人，骑乘队伍出现
			pTeam->OnRideTeamMoveToNewBlock();
		}

		//获得骑乘队员
		CPlayer* pMember = pTeam->GetRideMember();
		if ( pMember )
		{
			bool dumpbool = false;
			pMember->OnePlayerForceSetFloatPos( isManualSet, newFloatX, newFloatY, dumpbool );
			pMember->MoveAffectTrade();//骑乘副座玩家移动，也要监视交易的状态
		}
	}

	return;
}

///向目标点移动，如果当前确实处于移动状态，则返回真，否则返回假；
bool CPlayer::TryMove( bool& isNeedNoti/*最迟移动通知算法*/, bool& isReallyMoveGrid/*移动是否跨格*/, bool& isReallyMoveBlock/*移动是否跨块*/, bool& isAchieve
					  , unsigned short& orgBlockX, unsigned short& orgBlockY, unsigned short& newBlockX, unsigned short& newBlockY
					  , bool isForceCal/*是否强制计算，而不管是否跨格*/ )
{
	TRY_BEGIN;

	float fFloatX = m_fCurX, fFloatY = m_fCurY;
	bool isMoved = false;
	bool isok = m_MoveInfo.MoveToTarget( fFloatX, fFloatY, isNeedNoti, isMoved, isReallyMoveGrid, isReallyMoveBlock, isAchieve, orgBlockX, orgBlockY, newBlockX, newBlockY, isForceCal );

	//if ( !m_bIsRobot )
	//{
	//	if ( !isok )
	//	{
	//		D_DEBUG( "TryMove,玩家%s当前不在移动状态，忽略本次移动\n", GetAccount() );
	//	}
	//}

	if ( isMoved )
	{
		//位置调试，D_DEBUG( "TryMove，玩家正常移动:(%f,%f)-->(%f,%f)\n", GetFloatPosX(), GetFloatPosY()
		//	, fFloatX, fFloatY );
		ForceSetFloatPos( false/*移动过程中改变位置，只通知新视野，大部分情况都应如此*/, fFloatX, fFloatY );
	}

	return isok;
	TRY_END;
	return false;
}


///向目标点移动，如果当前确实处于移动状态，则返回真，否则返回假；
bool MoveInfo::MoveToTarget( float& fCurX, float& fCurY			
						   , bool& isNeedNoti/*最迟移动通知算法*/, bool& isMoved/*本次浮点坐标是否有改变*/, bool& isReallyMoveGrid/*是否真正移动了(距离超过一格)*/, bool& isReallyMoveBlock/*是否真正移动了(距离超过一格)*/, bool& isAchieve/*是否到达了目标点*/
						   , unsigned short& orgBlockX, unsigned short& orgBlockY, unsigned short& newBlockX, unsigned short& newBlockY, bool isForceCal/*是否强制计算（不满一格也计算）*/ )
{
	if ( !isMoving )
	{
		//当前不在移动；
		//D_DEBUG( "当前不在移动状态，放弃本次移动\n" );
		return false;
	}

	//vCurPos = m_vOriginPos + m_vDir * m_fSpeed * m_fElaspedTime;
	isReallyMoveGrid = false;
	isReallyMoveBlock = false;

	TYPE_POS orgGridX=0, orgGridY=0;
	TYPE_POS newGridX=0, newGridY=0;

	isAchieve = false;

	ACE_Time_Value lastpassedTime = ACE_OS::gettimeofday() - lastMoveTime;
	float passed = lastpassedTime.msec() / (float)1000;//已经过去的秒数；
	//本次实际应该移动了多少米；
	float tmpRealDis = passed * fSpeed;
	//D_DEBUG( "MoveToTarget, passed:%f, fspeed:%f, fspeedx:%f, fspeedy:%f\n", passed, fSpeed, fSpeedX, fSpeedY );
	if ( ( tmpRealDis >= METER_PERGRID ) || isForceCal )
	{
		//移动距离超过两米;
		lastMoveTime = ACE_OS::gettimeofday();//更新最近移动时间；
		isMoved = true;

		MuxMapSpace::FloatToGridPos( fCurX, fCurY, orgGridX, orgGridY );
		MuxMapSpace::GridToBlockPos( orgGridX, orgGridY, orgBlockX, orgBlockY );//旧块位置；

		fCurX += passed*fSpeedX;//新位置浮点X；
		fCurY += passed*fSpeedY;//新位置浮点Y；

		MuxMapSpace::FloatToGridPos( fCurX, fCurY, newGridX, newGridY );
		MuxMapSpace::GridToBlockPos( newGridX, newGridY, newBlockX, newBlockY );//新块位置；

		isReallyMoveGrid = (orgGridX!=newGridX) || (orgGridY!=newGridY);//本次是否确实有跨格移动；
		isReallyMoveBlock = (orgBlockX!=newBlockX) || (orgBlockY!=newBlockY);//旧块位置与新块位置不一致；

		//检查是否到达目标点；
		ACE_Time_Value totalpassed = ACE_OS::gettimeofday() - startMoveTime; 
		float totalDis = totalpassed.msec() / (float)1000 * fSpeed;
		if ( totalDis >= expectDis )
		{
			fCurX = fTargetX;//到达目标点后需要修正位置，因为已经越过了目标点；
			fCurY = fTargetY;//到达目标点后需要修正位置，因为已经越过了目标点；
			MuxMapSpace::FloatToGridPos( fCurX, fCurY, newGridX, newGridY );
			MuxMapSpace::GridToBlockPos( newGridX, newGridY, newBlockX, newBlockY );//新块位置；

			isReallyMoveGrid = (orgGridX!=newGridX) || (orgGridY!=newGridY);//本次是否确实有跨格移动；
			isReallyMoveBlock = (orgBlockX!=newBlockX) || (orgBlockY!=newBlockY);//旧块位置与新块位置不一致；

			isAchieve = true;//已到达目标点；
#ifdef LATEST_MOVE_NOTI
			notifiedInfo.ResetNotifiedMovInfo();//重置已通知信息；
#endif //LATEST_MOVE_NOTI
			isMoving = false;//到达目标点后，状态变为停止移动；
		} else {
#ifdef LATEST_MOVE_NOTI
			//检查是否需要广播给周围人新移动消息
			isNeedNoti = notifiedInfo.IsNeedNoti( fCurX, fCurY, fSpeedX, fSpeedY, fTargetX, fTargetY );//最迟移动通知算法，是否需要广播移动信息
#endif //LATEST_MOVE_NOTI
		}
	}

	return true;//确实处于移动状态；
}

///上次下线时已过复活操作提示时间（由于下线保护时间），再次上线时直接将玩家置到复活点而不再与客户端交流复活相关消息；
void CPlayer::OnlineDirectRebirth()
{
	TRY_BEGIN;

	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(上线直接复活导致状态改变)

	//挑选一个复活点，并通知玩家；	

	//首先判断玩家是否处于死亡状态，如果不是，则直接返回，并记录错误；
	if ( !IsDying() )
	{
		//当前非死亡状态，不理会；
		D_ERROR( "OnlineDirectRebirth，发现玩家当前状态不是死亡，而是%d\n", m_fullPlayerInfo.stateInfo.m_CurBaseState );
		return;
	}

	if(m_fullPlayerInfo.stateInfo.ForWarDie())
	{
		OnSelectWarRebirthOption();
		return;
	}

	if( GetGoodEvilPoint() < 0 )
	{
		CreateWeakBuff();		//魔头创建虚弱BUFF
	} else {
		m_fullPlayerInfo.stateInfo.SetAlive();//将玩家状态置为活；
	}

	CNewMap* pMap = GetCurMap();
	if ( NULL == pMap )
	{
		D_ERROR( "OnlineDirectRebirth,找不到玩家所在地图\n" );
		return;
	}
	unsigned long rebirthMapID = pMap->GetMapNo();
	MuxPoint rebirthPt;

	bool isptgot = false;
	if ( IsRobot() )
	{
		isptgot = pMap->GetOneAvailPt( rebirthPt );//将机器人设到一个随机点复活；
	} else {
		unsigned short savedRebirthMapID = m_fullPlayerInfo.baseInfo.rebirthMapID;
		if(rebirthMapID != (unsigned long)savedRebirthMapID)
			isptgot = pMap->GetOneRebirthPt( rebirthMapID, rebirthPt, (unsigned char)GetRace() );
		else
		{
			rebirthPt.nPosX = m_fullPlayerInfo.baseInfo.rebirthPosX;
			rebirthPt.nPosY = m_fullPlayerInfo.baseInfo.rebirthPosY;
			isptgot = true;
		}
	}

	if ( !isptgot )
	{
		D_WARNING( "OnlineDirectRebirth, 找不到玩家%s复活点\n", GetAccount() );
		return;
	}

	//验证下复活点的可靠性
	if( !(pMap->IsGridCanPass( rebirthPt.nPosX, rebirthPt.nPosY )) )
	{
		//?随即选取不是障碍点的点复活？
		D_ERROR("OnlineDirectRebirth, 复活点为障碍点，复活失败\n");
		return;
	}

	/////////////////////////////////////////////////////////////////////////////////////
	//目前复活只在本地图，跨地图复活走跳转机制,by dzj, 09.10.15；
	//目前和地图跳转一样，暂时不处理复活地图不在本mapsrv上的情况；
	//获取目标地图及位置
	//CNewMap* pNewMap = CManNormalMap::FindMap( rebirthMapID );//在线直接复活必定为普通地图而不可能为某副本地图；
	//if(NULL == pNewMap)
	//{
	//	D_WARNING("CPlayer::SendRebirthInfoPkg，没有找到地图编号为%l的地图\n", rebirthMapID );
	//	return;
	//}

	////将玩家当前点当前地图设为复活点，但暂时不真正将玩家加入到复活点，而是等玩家的C_G_REBIRTH_READY来了后，再做加入地图操作；
	////这样设了后，下次真正appear时的所有相关操作才能取到正确信息,例如:InitPlayer等,跳地图时也要做类似操作，区别是不必修改血量，(参考:跳地图相关代码);
	//已修改为直接拉至复活点，不再等待玩家的read消息,注释补充, by dzj, 09.10.15;
	//SetMap( pNewMap );
	//目前复活只在本地图，跨地图复活走跳转机制,by dzj, 09.10.15；
	/////////////////////////////////////////////////////////////////////////////////////

	SetPlayerPos( rebirthPt.nPosX, rebirthPt.nPosY, true );
	SetPlayerHp( GetMaxHP() / 2 );//血值设为最大值的一半;
	SetPlayerMp( GetMaxMP() / 2 );
	UpdatePlayerFullInfo( INFO_NORMAL_UPDATE );//保存玩家的新地图与出生点；

	CLogManager::DoPlayerRebirth(this, rebirthMapID, rebirthPt.nPosX, rebirthPt.nPosY);//记日至

	return;
	TRY_END;
	return;
}

void CPlayer::OnSelectWarRebirthOption(BYTE option)
{
	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(war复活导致状态改变)

	if(m_fullPlayerInfo.stateInfo.m_StateStTick == m_fullPlayerInfo.stateInfo.m_StateEndTick)//直接复活，用于上线时候倒计时已到
	{
		//复活后血量恢复；
		SetPlayerHp( GetMaxHP() / 2 );//血值设为最大值的一半;
		SetPlayerMp( GetMaxMP() / 2 );

		if( GetGoodEvilPoint() < 0 )
		{
			CreateWeakBuff();
		}else{
			m_fullPlayerInfo.stateInfo.SetAlive();	//将玩家状态置为活；
		}

		UpdatePlayerFullInfo( INFO_NORMAL_UPDATE );//保存玩家的新地图与出生点；
		CLogManager::DoPlayerRebirth(this, m_fullPlayerInfo.baseInfo.usMapID, m_fullPlayerInfo.baseInfo.iPosX, m_fullPlayerInfo.baseInfo.iPosY);//记日至

		return;
	}

	//根据选项获得复活点
	if((1 != option) && (2 != option) && (4 != option))
		return;

	//当前地图
	if(NULL == m_pMap)
	{
		//地图信息为空不能复活
		D_ERROR("玩家%sCPlayer::OnSelectWarRebirthOption时地图信息为空\n", GetNickName());
		return;
	}

	bool retCode = false;
	unsigned short mapID = 0;
	unsigned int posX = 0, posY = 0;
	unsigned char err = 0;
	if((1 == option) || (2 == option))//内城复活或者外城复活
	{
		if(DiedInCurrentWar())
		{
			if(option == 1)
				retCode = m_pMap->GetWarInnerRebirthPt(GetRace(), mapID, posX, posY, err);
			else
				retCode = m_pMap->GetWarOuterRebirthPt(GetRace(), mapID, posX, posY, err);
		}
	}
	else
	{
		//是否外城绑定过复活点？
		MapWarAdditionalInfo* pWarAdditionInfo = m_pMap->GetWarAdditionalInfo();
		if(NULL != pWarAdditionInfo)
		{	
			if(pWarAdditionInfo->mapRelationType == CMapRelation::OUTER_MAP)//当前是外城地图
			{
				mapID = m_pMap->GetMapNo();
			}
			else
			{
				CMuxMap *pAssociatedMap = CManNormalMap::FindMap(pWarAdditionInfo->associatedMapId);
				if(pAssociatedMap != NULL)
				{
					mapID = pAssociatedMap->GetMapNo();
				}
			}

			if(mapID == m_fullPlayerInfo.baseInfo.rebirthMapID)
			{
				posX = m_fullPlayerInfo.baseInfo.rebirthPosX;
				posY = m_fullPlayerInfo.baseInfo.rebirthPosY;
				retCode = true;
			}
		}
	}

	if(!retCode)
		retCode = m_pMap->GetWarDefaultRebirthPt(GetRace(), mapID, posX, posY, err);//缺省复活点

	if(!retCode)//不成功则对应复活点不能复活
	{
		MGWarRebirthOptionErr optionErr;
		optionErr.lNotiPlayerID = GetFullID();
		optionErr.warRebirthError.rebirthOption = option;
		optionErr.warRebirthError.retCode = (BYTE)((err == 1) ? GCSelectWarRebirthOptionErr::RACE_NO_MATCH : GCSelectWarRebirthOptionErr::OTHER_ERROR);
		SendPkg<MGWarRebirthOptionErr>(&optionErr);

		return;
	}
	
	if(mapID == m_pMap->GetMapNo())//同地图跳转
	{
		//复活后血量恢复；
		SetPlayerHp( GetMaxHP() / 2 );//血值设为最大值的一半;
		SetPlayerMp( GetMaxMP() / 2 );

		if( GetGoodEvilPoint() < 0 )
		{
			CreateWeakBuff();
		}
		else
		{
			m_fullPlayerInfo.stateInfo.SetAlive();	//将玩家状态置为活；
		}

		MGFinalRebirthRst sendMsg;
		sendMsg.sequenceID = 123456;
		sendMsg.lMapCode = GetMapID();
		sendMsg.lNotiPlayerID = GetFullID();
		sendMsg.nErrCode = MGFinalRebirthRst::FINAL_REBIRTH_SUC;
		sendMsg.nPosX = posX;
		sendMsg.nPosY = posY;
		sendMsg.baseInfo = m_fullPlayerInfo.baseInfo;
		SendPkg<MGFinalRebirthRst>( &sendMsg );//通知客户端复活；

		m_pMap->OnPlayerInnerJump( this, posX, posY );
		SetNeedNotiTeamMember();   //死亡复活后也需要向玩家通知
		CLogManager::DoPlayerRebirth(this, mapID,posX, posY);//记日至
	}
	else// 先跳地图,后复活
	{	
		//修改玩家时间,以便在新地图上出现时走复活流程
		m_fullPlayerInfo.stateInfo.m_StateStTick = m_fullPlayerInfo.stateInfo.m_StateEndTick;

		//跳到不同地图
		IssueSwitchMap(mapID, posX, posY);
	}

	return;
}

bool CPlayer::DiedInCurrentWar(void)
{
	if((NULL != m_pMap) && (m_pMap->InWarState())//是否处于攻城战
		&& (m_fullPlayerInfo.stateInfo.ForWarDie())//是否攻城战中死亡
		&& (m_fullPlayerInfo.stateInfo.m_StateStTick > m_pMap->GetWarStartTime()))//是否本次攻城战中(玩家死亡时间晚于攻城开始时间)
	{
		return true;
	}
			
	return false;
}


BYTE CPlayer::GetWarRebirthOption(void)
{
	BYTE option = 0;

	if(NULL != m_pMap)
	{
		unsigned short mapID = 0;
		unsigned int posX = 0 , posY = 0;
		unsigned char err = 0;

		if(DiedInCurrentWar())
		{
			if(m_pMap->GetWarInnerRebirthPt(GetRace(), mapID, posX, posY, err))
				option |= 1;//内城可复活

			if(m_pMap->GetWarOuterRebirthPt(GetRace(), mapID, posX, posY, err))
				option |= 2;//外城可复活
		}

		if(m_pMap->GetWarDefaultRebirthPt(GetRace(), mapID, posX, posY, err))
			option |= 4;//缺省复活点
	}

	return option;
}

///检查玩家是否死亡，如果已到死亡复活时间，则复活，如果检查过后玩家仍为死亡状态，则返回真，否则返回假；
bool CPlayer::CheckDie()
{
	if ( !IsDying() )
	{
		//玩家非死亡状态，直接返回，否则检查是否到强制复活时刻；
		return false;
	}

	if ( time(NULL) < m_fullPlayerInfo.stateInfo.GetStateEndTick() )
	{
		//还未到强制复活时刻；
		return true;//死亡，返回true;
	}

	D_DEBUG( "玩家%s已到强制复活时刻，调用NewDirectRebitth\n", GetAccount() );
	
	if(m_fullPlayerInfo.stateInfo.ForWarDie())
	{
		OnSelectWarRebirthOption();
		return false;
	}

	//已到强制复活时刻，复活至复活点；
	NewDirectRebirth();

	return false;//已复活，返回true;
}

///新的直接复活，省去中间步骤；
void CPlayer::NewDirectRebirth()
{
	//首先判断玩家是否处于死亡状态，如果不是，则直接返回，并记录错误；
	if ( !IsDying() )
	{
		//当前非死亡状态，不理会；
		D_ERROR( "NewDirectRebirth,玩家当前状态不是死亡，而是%d\n", m_fullPlayerInfo.stateInfo.m_CurBaseState );
		return;
	}

    OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(复活导致状态改变)

	CNewMap* pMap = GetCurMap();
	if ( NULL == pMap )
	{
		D_ERROR( "NewDirectRebirth,找不到玩家所在地图\n" );
		return;
	}
	unsigned long rebirthMapID = pMap->GetMapNo();
	MuxPoint rebirthPt;

	bool isptgot = false;
	if ( IsRobot() )
	{
		isptgot = pMap->GetOneAvailPt( rebirthPt );//将机器人设到一个随机点复活；
	} else {
		unsigned short savedRebirthMapID = m_fullPlayerInfo.baseInfo.rebirthMapID;
		if(rebirthMapID != (unsigned long)savedRebirthMapID)
		{
			isptgot = pMap->GetOneRebirthPt( rebirthMapID, rebirthPt, (unsigned char)GetRace() );
		} else {
			rebirthPt.nPosX = m_fullPlayerInfo.baseInfo.rebirthPosX;
			rebirthPt.nPosY = m_fullPlayerInfo.baseInfo.rebirthPosY;
			isptgot = true;
		}
	}

	if ( !isptgot )
	{
		D_WARNING( "NewDirectRebirth,找不到玩家%s复活点，逻辑错误\n", GetAccount() );
		return;
	}

	if ( !IsRobot() )
	{
		//验证下复活点的可靠性
		if( !(pMap->IsGridCanPass( rebirthPt.nPosX, rebirthPt.nPosY )) )
		{
			//?随即选取不是障碍点的点复活？
			D_ERROR("NewDirectRebirth,复活点为障碍点，复活失败\n");
			return;
		}
	}


	//复活后血量恢复；
	SetPlayerHp( GetMaxHP() / 2 );//血值设为最大值的一半;
	SetPlayerMp( GetMaxMP() / 2 );

	if( GetGoodEvilPoint() < 0 )
	{
		CreateWeakBuff();
	}else{
		m_fullPlayerInfo.stateInfo.SetAlive();	//将玩家状态置为活；
	}

	MGFinalRebirthRst sendMsg;
	sendMsg.sequenceID = 123456;
	sendMsg.lMapCode = GetMapID();
	sendMsg.lNotiPlayerID = GetFullID();
	sendMsg.nErrCode = MGFinalRebirthRst::FINAL_REBIRTH_SUC;
	sendMsg.nPosX = rebirthPt.nPosX;
	sendMsg.nPosY = rebirthPt.nPosY;
	sendMsg.baseInfo = m_fullPlayerInfo.baseInfo;
	SendPkg<MGFinalRebirthRst>( &sendMsg );//通知客户端复活；

	OnRookieStateCheck( true );

	//MGTmpInfoUpdate tmpInfoUpdate;
	//tmpInfoUpdate.playerID = GetFullID();
	//tmpInfoUpdate.tmpInfo = GetTmpProp();
	//SendPkg< MGTmpInfoUpdate >(&tmpInfoUpdate);

	pMap->OnPlayerInnerJump( this, rebirthPt.nPosX, rebirthPt.nPosY );

	//ForceSetFloatPos( rebirthPt.nPosX+0.5f, rebirthPt.nPosY+0.5f ); //将玩家拉至复活点；

	//UpdatePlayerFullInfo( INFO_NORMAL_UPDATE );//保存玩家的新信息；

	//SendFinalRebirthSuc( sequenceID );

	//ResetPreFightFlag();

	//pMap->OnPlayerAppear( this );

	SetNeedNotiTeamMember();   //死亡复活后也需要向玩家通知

	CLogManager::DoPlayerRebirth(this, rebirthMapID, rebirthPt.nPosX, rebirthPt.nPosY);//记日至

	return;
}

///发送玩家死亡倒计时消息
void CPlayer::SendDyingPkg( unsigned long duraSec )
{
	MGRoleattUpdate dieMsg;
	dieMsg.bFloat = false;
	dieMsg.changeTime = 0;
	dieMsg.lChangedPlayerID = GetFullID();
	dieMsg.nAddType = MGRoleattUpdate::TYPE_SET;
	dieMsg.nNewValue = 0;
	dieMsg.nType = RT_HP;
	CMuxMap* pMap = GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGRoleattUpdate>(&dieMsg, GetPosX(), GetPosY() );
	}

	if(!m_fullPlayerInfo.stateInfo.ForWarDie() )
	{
		MGPlayerDying playerDying;
		playerDying.dyingPlayerID = GetFullID();
		playerDying.countDownSec = duraSec;
		//D_DEBUG("向玩家%s发送死亡倒计时%d秒\n", GetAccount(), duraSec );
		SendPkg< MGPlayerDying >( &playerDying );
	}
	else
	{
		//攻城战死亡
		MGPlayerWarDying playerDying;
		playerDying.lNotiPlayerID = GetFullID();
		playerDying.dying.dyingPlayerID = GetFullID();
		playerDying.dying.countDownSecond = duraSec;
		playerDying.dying.rebirthOption = GetWarRebirthOption();
		//D_DEBUG("向玩家%s发送攻城死亡倒计时%d秒\n", GetAccount(), duraSec );
		SendPkg<MGPlayerWarDying>( &playerDying);
	}

	return;
}

/////玩家真正加入默认复活点
//void CPlayer::OnRebirthReady( TYPE_ID sequenceID )
//{
//	TRY_BEGIN;
//
//	if ( !IsWaitingClient() )
//	{
//		//当前并没有在等待玩家的该消息，不予理会；
//       D_ERROR( "CPlayer::OnRebirthRead，发现玩家当前状态不是WaitingClinet，而是%d\n", m_fullPlayerInfo.stateInfo.m_CurBaseState );
//		return;
//	}
//
//	//if( GetGoodEvilPoint() < 0 )
//	//{
//	//	CreateWeakBuff();
//	//}else{
//	//	m_fullPlayerInfo.stateInfo.SetAlive();	//将玩家状态置为活；
//	//}
//
//	CNewMap* pMap = GetCurMap();
//
//	//参考：(地图跳转代码)
//	if ( NULL == pMap )
//	{
//		D_ERROR( "CPlayer::OnRebirthRead，玩家当前地图为空\n" );
//		return;
//	}
//
//	//D_DEBUG( "玩家%s复活至点%d,%d完成\n", GetAccount(), GetPosX(), GetPosY() );
//
//	SendFinalRebirthSuc( sequenceID );
//
//	MGTmpInfoUpdate tmpInfoUpdate;
//	tmpInfoUpdate.playerID = GetFullID();
//	tmpInfoUpdate.tmpInfo = GetTmpProp();
//	SendPkg< MGTmpInfoUpdate >(&tmpInfoUpdate);
//
//	//unsigned long dyingleft = 0;//此处不使用；
//	//OnlineProc( dyingleft );
//	ResetPreFightFlag();
//
//	pMap->OnPlayerAppear( this );
//
//	if( GetGoodEvilPoint() < 0 )
//	{
//		CreateWeakBuff();
//	}else{
//		m_fullPlayerInfo.stateInfo.SetAlive();	//将玩家状态置为活；
//	}
//
//	return;
//	TRY_END;
//	return;
//}

/////发送玩家重生点信息；
//void CPlayer::SendRebirthInfoPkg( TYPE_ID sequenceID )
//{
//	TRY_BEGIN;
//
//	//挑选一个复活点，并通知玩家；	
//
//	//首先判断玩家是否处于死亡状态，如果不是，则直接返回，并记录错误；
//	if ( !IsDying() )
//	{
//		//当前非死亡状态，不理会；
//		D_ERROR( "向玩家返回重生点信息时，发现玩家当前状态不是死亡，而是%d\n", m_fullPlayerInfo.stateInfo.m_CurBaseState );
//		return;
//	}
//
//	m_fullPlayerInfo.stateInfo.SetWaitClient();
//
//	CNewMap* pMap = GetCurMap();
//	if ( NULL == pMap )
//	{
//		D_ERROR( "CPlayer::SendRebirthInfoPkg,找不到玩家所在地图\n" );
//		return;
//	}
//	unsigned long rebirthMapID = pMap->GetMapNo();
//	MuxPoint rebirthPt;
//
//	bool isptgot = false;
//	if ( IsRobot() )
//	{
//		isptgot = pMap->GetOneAvailPt( rebirthPt );//将机器人设到一个随机点复活；
//	} else {
//		unsigned short savedRebirthMapID = m_fullPlayerInfo.baseInfo.rebirthMapID;
//		if(rebirthMapID != (unsigned long)savedRebirthMapID)
//			isptgot = pMap->GetOneRebirthPt( rebirthMapID, rebirthPt, (unsigned char)GetRace() );
//		else
//		{
//			rebirthPt.nPosX = m_fullPlayerInfo.baseInfo.rebirthPosX;
//			rebirthPt.nPosY = m_fullPlayerInfo.baseInfo.rebirthPosY;
//			isptgot = true;
//		}
//	}
//
//	if ( !isptgot )
//	{
//		D_WARNING( "找不到玩家%s复活点，逻辑错误\n", GetAccount() );
//		return;
//	}
//
//	//验证下复活点的可靠性
//	if( !(pMap->IsGridCanPass( rebirthPt.nPosX, rebirthPt.nPosY )) )
//	{
//		//?随即选取不是障碍点的点复活？
//		D_ERROR("复活点为障碍点，复活失败\n");
//		return;
//	}
//
//	MGRebirthInfo rebirthInfo;
//	rebirthInfo.sequenceID = sequenceID;
//	rebirthInfo.dyingPlayerID = GetFullID();
//	rebirthInfo.mapID = rebirthMapID;
//	rebirthInfo.nPosX = rebirthPt.nPosX;
//	rebirthInfo.nPosY = rebirthPt.nPosY;
//
//	SendPkg< MGRebirthInfo >( &rebirthInfo );//通知玩家复活点信息，收到后，玩家会开始装地图，装地图完毕玩家发C_G_REBIRTH_READY，SRV收到后广播Appear消息；
//
//	//D_DEBUG( "通知玩家%s复活至默认复活点，地图%d,点(%d,%d)\n", GetAccount(), rebirthMapID, rebirthPt.nPosX, rebirthPt.nPosY );
//
//	//将玩家移至新地图的新点；
//	//参考：(地图跳转代码)
//
//	//通知当前地图周围点的其它人玩家离开，此时pPlayer中还保存着玩家的点位置信息；
//	pMap->OnPlayerLeave( this ); //此函数内部应清玩家的所属地图以及点位置信息；		
//
//	//目前和地图跳转一样，暂时不处理复活地图不在本mapsrv上的情况；
//	//获取目标地图及位置
//	CNewMap* pNewMap = CNewMapManager::FindMap( rebirthMapID );
//	if(NULL == pNewMap)
//	{
//		D_WARNING("CPlayer::SendRebirthInfoPkg，没有找到地图编号为%l的地图\n", rebirthMapID );
//		return;
//	}
//
//	//将玩家当前点当前地图设为复活点，但暂时不真正将玩家加入到复活点，而是等玩家的C_G_REBIRTH_READY来了后，再做加入地图操作；
//	//这样设了后，下次真正appear时的所有相关操作才能取到正确信息,例如:InitPlayer等,跳地图时也要做类似操作，区别是不必修改血量，(参考:跳地图相关代码);
//	SetMap( pNewMap );
//	SetPlayerPos( rebirthPt.nPosX, rebirthPt.nPosY, true );
//	GetEnterMapPosProperty();//设置玩家的复活点信息
//	m_fullPlayerInfo.baseInfo.uiHP = GetMaxHP() / 2;//血值设为最大值的一半;
//	m_fullPlayerInfo.baseInfo.uiMP = GetMaxMP() / 2;
//	UpdatePlayerFullInfo( INFO_NORMAL_UPDATE );//保存玩家的新地图与出生点；
//	SendSelfInfoToOtherMembers();   //死亡复活后也需要向玩家通知
//
//	
//	return;
//	TRY_END;
//	return;
//}

void CPlayer::AddExp(unsigned int nExp, int addType, bool isExpCritical )
{
	TRY_BEGIN;

	//死亡的玩家不能获得经验值
	if( GetCurrentHP() == 0 )
	{
		return;
	}

	//如果已经是最高等级了无需再处理了
	if( NULL == m_pClassLevelProp )
	{
		D_WARNING( "CPlayer::AddExp, NULL == m_pClassLevelProp, player:%s\n", GetAccount() );
		return;
	}

	if( m_pClassLevelProp->GetMaxLevel() == GetLevel() )
	{
		//m_fullPlayerInfo.baseInfo.dwExp = 0;
		return;
	}

	nExp *= GetTimesExp();

#ifdef ANTI_ADDICTION
	TriedStateProfitCheck( nExp, TYPE_EXP );
#endif /*ANTI_ADDICTION*/	

	m_fullPlayerInfo.baseInfo.dwExp += nExp;
	m_fullPlayerInfo.baseInfo.totalExp += nExp;

	MGRoleattUpdate updatemsg;
	updatemsg.bFloat = false;
	updatemsg.changeTime = 0;
	updatemsg.lChangedPlayerID = GetFullID();
	updatemsg.nAddType = isExpCritical?1:0;
	updatemsg.nOldValue = GetTotalExp() - nExp;
	updatemsg.nNewValue = GetTotalExp();
	updatemsg.nType = RT_TOTALEXP;
	SendPkg<MGRoleattUpdate>(&updatemsg);//总的经验变化

	//如果升级的话...
	if( (m_fullPlayerInfo.baseInfo.dwExp  >= GetNextLevelExp()) && (GetLevel() < 30/*等级小于30的可以自动升级*/) )
	{
		unsigned short oldLevel = m_fullPlayerInfo.baseInfo.usLevel;
		unsigned short oldUnallocatePoint = m_fullPlayerInfo.baseInfo.unallocatePoint;
		do
		{
			++m_fullPlayerInfo.baseInfo.usLevel; //升级
			m_fullPlayerInfo.baseInfo.dwExp -= GetNextLevelExp();	//升级减去需要的经验值
			m_fullPlayerInfo.baseInfo.unallocatePoint += 500;//每升一级加5个属性点

			if ( !CalculateTmpProp() )//获取新等级的非存盘数据,也就是下一级的经验
			{
				D_DEBUG("获取非存盘数据时发生错误\n");
			}

#ifdef LEVELUP_SCRIPT	//脚本优先
			if( NULL != m_pLevelUpScript )
			{
				//m_pLevelUpScript->OnPlayerSetBaseProp( this );	//设置基础属性
				m_pLevelUpScript->OnPlayerLevelUp( this );		//设置升级时的特殊操作
			}
#endif //LEVELUP_SCRIPT

			/*if( GetLevel() == m_pClassLevelProp->GetMaxLevel() )
			{
				m_fullPlayerInfo.baseInfo.dwExp = 0;
				break;
			}*/
		}while((GetExp() >= GetNextLevelExp()) && (GetLevel() < 30/*等级小于30的可以自动升级*/));
		
		CLogManager::DoPlayerLevelUp(this, oldLevel);//记日至
		const unsigned int level = GetLevel();
		AddFriendTweet(MUX_PROTO::TWEET_TYPE_UPGRADE, (const void *)&level, sizeof(unsigned int));//记录好友动态

		//基本属性变化
		//MGRoleattUpdate updatemsg;
		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType = isExpCritical?1:0;
		updatemsg.nOldValue = nExp;
		updatemsg.nNewValue = GetExp();
		updatemsg.nType = RT_EXP;
		SendPkg<MGRoleattUpdate>(&updatemsg);//经验变化

		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType =  isExpCritical?1:0;
		updatemsg.nOldValue = oldUnallocatePoint;
		updatemsg.nNewValue = GetUnallocatPoint();
		updatemsg.nType = RT_UNALLOCATEPOINT;
		SendPkg<MGRoleattUpdate>(&updatemsg);//未分配属性点变化

		////临时属性变化
		////该消息已废除，因为相关属性已另行计算或通知, by dzj, 10.05.26, MGTmpInfoUpdate tmpInfoUpdate;
		//tmpInfoUpdate.playerID = GetFullID();
		//tmpInfoUpdate.tmpInfo = m_TmpProp;
		//tmpInfoUpdate.tmpInfo.fSpeed = GetSpeed();
		//SendPkg<MGTmpInfoUpdate>(&tmpInfoUpdate);

		//暂时废除，若仍需通知，则另取下一级需要经验，by dzj, 10.05.26, std::stringstream strSysChat;
		//strSysChat  << "下一级的经验" <<tmpInfoUpdate.tmpInfo.lNextLevelExp
		//<<"现在的经验:"  <<GetExp() <<endl;
		//SendSystemChat( strSysChat.str().c_str() );

		//升级要补满血和魔
		SetPlayerHp( GetMaxHP() );
		SetPlayerMp( GetMaxMP() );

		//通知下一等级需要经验
		NotifyNextLevelExp();

		if(m_pMap != NULL)
		{
			m_pMap->HandlePlayerLevelUp(this, addType );
		}
	}else
	{
		//MGRoleattUpdate updatemsg;
		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType = isExpCritical?1:0;
		updatemsg.nOldValue = nExp;
		updatemsg.nNewValue = GetExp();
		updatemsg.nType = RT_EXP;
		SendPkg<MGRoleattUpdate>(&updatemsg);
	}

		
	TRY_END;
}


//10.09.20已废弃//void CPlayer::AddPhyExp(unsigned int nExp)
//{
//	CSkillExp* pSkillExp = CManSkillExp::FindObj( GetClass() );
//	if( NULL == pSkillExp )
//	{
//		D_ERROR("AddPhyExp 找不到该职业的配置 %d\n", GetClass() );
//		return;
//	}
//
//	//到了最高等级,经验不添加
//	if( pSkillExp->GetMaxSkillLevel() == GetPhyLevel() && nExp != 0)
//	{
//		return;
//	}
//
//#ifdef ANTI_ADDICTION
//	TriedStateProfitCheck( nExp, PHYMAG_EXP );
//#endif //ANTI_ADDICTION	
//	
//	SkillLevelExp outLevelExp;
//	SkillPointExp outPointExp;
//	
//	//step 1 先检测超没超出等级
//	unsigned int nUpdateExp = GetPhyExp() + nExp;	
//	m_fullPlayerInfo.baseInfo.dwPhysicsExp = ( nUpdateExp >= m_TmpProp.maxLimitPhyExp) ? m_TmpProp.maxLimitPhyExp : nUpdateExp;
//
//	bool bLevelUp = false;
//	if( GetPhyExp() >= m_TmpProp.lNextPhyLevelExp && (GetPhyLevel() != m_fullPlayerInfo.baseInfo.ucMaxPhyLevelLimit) )
//	{
//		bLevelUp = true;
//		for( int i=0; i< 10; ++i )
//		{
//			if( GetPhyExp() >= m_TmpProp.lNextPhyLevelExp && (GetPhyLevel() != m_fullPlayerInfo.baseInfo.ucMaxPhyLevelLimit) )
//			{
//				m_fullPlayerInfo.baseInfo.usPhysicsLevel++;  //物理技能等级上升
//				if( pSkillExp->FindLevelExp( GetPhyLevel(), outLevelExp ) )
//				{
//					m_TmpProp.lNextPhyLevelExp = outLevelExp.phyNextLevelExp;
//					m_TmpProp.lLastPhyLevelExp = outLevelExp.phyLastLevelExp;
//					SafeStrCpy( m_TmpProp.phySkillTitle, outLevelExp.szSkillTitle );
//					m_TmpProp.phySkillTitleSize = (USHORT)strlen( m_TmpProp.phySkillTitle ) + 1;
//				}else{
//					D_WARNING("查找物理等级%d信息找不到\n", GetPhyLevel() );
//				}
//			}else{
//				break;
//			}
//		}
//
//		//告诉客户端技能等级提升
//		MGRoleattUpdate climsg;
//		climsg.bFloat = false;
//		climsg.lChangedPlayerID = GetFullID();
//		climsg.changeTime = 0;
//		climsg.nOldValue = m_fullPlayerInfo.baseInfo.usPhysicsLevel - 1;
//		climsg.nNewValue = GetPhyLevel();
//		climsg.nType = RT_PHYLEVEL;
//		SendPkg< MGRoleattUpdate >( &climsg );
//	}
//
//	//超过就获得技能点
//	if( GetPhyTotalPoint() >= m_TmpProp.currentLevelMaxPhyPoint )
//	{
//		return;
//	}
//
//	if( GetPhyExp() >= m_TmpProp.lNextPhyPointExp )
//	{
//		bLevelUp = true;
//		int nOldPoint = GetPhyAllPoint();
//		for(int i=0; i<90; ++i )
//		{
//			if( GetPhyTotalPoint() >= m_TmpProp.currentLevelMaxPhyPoint )
//			{
//				break;
//			}
//
//			if( GetPhyExp() >= m_TmpProp.lNextPhyPointExp )
//			{
//				m_fullPlayerInfo.baseInfo.usPhysicsAllotPoint++;		//增加可分配点数
//				m_fullPlayerInfo.baseInfo.usPhyTotalGetPoint++;			//增加总共分配点数
//				if( pSkillExp->FindPointExp( GetPhyTotalPoint() , outPointExp ) )
//				{
//					m_TmpProp.lNextPhyPointExp = outPointExp.phyNextPointExp;
//					m_TmpProp.lLastPhyPointExp = outPointExp.phyLastPointExp;
//				}else
//				{
//					D_WARNING("找不到当前物理点数需要的数据 点数:%d\n", GetPhyTotalPoint() );
//				}
//			}else{
//				break;
//			}
//		}
//		MGRoleattUpdate attrUpdate;
//		attrUpdate.nOldValue = nOldPoint;
//		attrUpdate.nNewValue = GetPhyAllPoint();
//		attrUpdate.nType = RT_PHYPOINT;
//		attrUpdate.changeTime = 0;
//		attrUpdate.nAddType = MGRoleattUpdate::TYPE_SET;	//直接设置
//		attrUpdate.bFloat = false;
//		attrUpdate.lChangedPlayerID = GetFullID();
//		SendPkg<MGRoleattUpdate>(&attrUpdate);
//	}
//	
//	if( bLevelUp )
//	{
//		////该消息已废除，因为相关属性已另行计算或通知, by dzj, 10.05.26, MGTmpInfoUpdate tempInfoUpdate;
//		//tempInfoUpdate.playerID = GetFullID();
//		//tempInfoUpdate.tmpInfo = m_TmpProp;
//		//SendPkg<MGTmpInfoUpdate>( &tempInfoUpdate );
//	}
//}
//
//
//void CPlayer::AddMagExp(unsigned int nExp)
//{
//	CSkillExp* pSkillExp = CManSkillExp::FindObj( GetClass() );
//	if( NULL == pSkillExp )
//	{
//		D_ERROR("AddPhyExp 找不到该职业的配置 %d\n", GetClass() );
//		return;
//	}
//
//	//到了最高等级,经验不添加
//	if( pSkillExp->GetMaxSkillLevel() == GetMagLevel() && nExp != 0)
//	{
//		return;
//	}
//
//#ifdef ANTI_ADDICTION
//	TriedStateProfitCheck( nExp, PHYMAG_EXP );
//#endif //ANTI_ADDICTION	
//
//	//step 1 先检测超没超出等级
//	unsigned int nUpdateExp = GetMagExp() + nExp;	
//	m_fullPlayerInfo.baseInfo.dwMagicExp = ( nUpdateExp >= m_TmpProp.maxLimitMagExp)? m_TmpProp.maxLimitMagExp : nUpdateExp;
//
//	bool bLevelUp = false;
//	if( GetMagExp() >= m_TmpProp.lNextMagLevelExp && (GetMagLevel() < m_fullPlayerInfo.baseInfo.ucMaxMagLevelLimit) )
//	{
//		bLevelUp = true;
//		SkillLevelExp outLevelExp;
//
//		for(int i=0; i<10; ++i)
//		{
//			if( GetMagExp() >= m_TmpProp.lNextMagLevelExp && ( GetMagLevel() < m_fullPlayerInfo.baseInfo.ucMaxMagLevelLimit ))
//			{
//				m_fullPlayerInfo.baseInfo.usMagicLevel++;  //魔法技能等级上升
//				if( pSkillExp->FindLevelExp( GetMagLevel(), outLevelExp ) )
//				{
//					m_TmpProp.lNextMagLevelExp = outLevelExp.magNextLevelExp;
//					m_TmpProp.lLastMagLevelExp = outLevelExp.magLastLevelExp;
//					SafeStrCpy( m_TmpProp.magSkillTitle, outLevelExp.szSkillTitle );
//					m_TmpProp.magSkillTitleSize = (USHORT)strlen( m_TmpProp.magSkillTitle ) + 1;
//				}else{
//					D_WARNING("找不到LEVEL %d的信息\n", GetMagLevel() );
//				}
//			}else{
//				break;
//			}
//		}
//		//告诉客户端技能等级提升
//		MGRoleattUpdate climsg;
//		climsg.bFloat = false;
//		climsg.lChangedPlayerID = GetFullID();
//		climsg.changeTime = 0;
//		climsg.nOldValue = m_fullPlayerInfo.baseInfo.usMagicLevel - 1;
//		climsg.nNewValue = GetMagLevel();
//		climsg.nType = RT_MAGLEVEL;
//		SendPkg< MGRoleattUpdate >( &climsg );
//	}
//
//	//超过就获得技能点
//	if( GetMagTotalPoint() >= m_TmpProp.currentLevelMaxMagPoint )
//	{
//		return;
//	}
//
//	if( GetMagExp() >= m_TmpProp.lNextMagPointExp )
//	{
//		bLevelUp = true;
//		int nOldPoint = GetMagAllPoint();
//		SkillPointExp outPointExp;
//		for(int i=0; i<90; ++i )
//		{
//			if( GetMagTotalPoint() >= m_TmpProp.currentLevelMaxMagPoint )
//			{
//				break;
//			}
//
//			if( GetMagExp() >= m_TmpProp.lNextMagPointExp )
//			{
//				m_fullPlayerInfo.baseInfo.usMagicAllotPoint++;		//增加可分配点数
//				m_fullPlayerInfo.baseInfo.usMagTotalGetPoint++;			//增加总共分配点数
//				if( pSkillExp->FindPointExp( GetMagTotalPoint() , outPointExp ) )
//				{
//					m_TmpProp.lNextMagPointExp = outPointExp.magNextPointExp;
//					m_TmpProp.lLastMagPointExp = outPointExp.magLastPointExp;
//				}else
//				{
//					D_WARNING("找不到当前物理点数需要的数据 点数:%d\n", GetMagTotalPoint() );
//				}
//			}else{
//				break;
//			}
//		}
//		MGRoleattUpdate attrUpdate;
//		attrUpdate.nOldValue = nOldPoint;
//		attrUpdate.nNewValue = GetMagAllPoint();
//		attrUpdate.nType = RT_MAGPOINT;
//		attrUpdate.changeTime = 0;
//		attrUpdate.nAddType = MGRoleattUpdate::TYPE_SET;	//直接设置
//		attrUpdate.bFloat = false;
//		attrUpdate.lChangedPlayerID = GetFullID();
//		SendPkg<MGRoleattUpdate>(&attrUpdate);
//	}
//	
//	if( bLevelUp )
//	{
//		////该消息已废除，因为相关属性已另行计算或通知, by dzj, 10.05.26, MGTmpInfoUpdate tempInfoUpdate;
//		//tempInfoUpdate.playerID = GetFullID();
//		//tempInfoUpdate.tmpInfo = m_TmpProp;
//		//SendPkg<MGTmpInfoUpdate>( &tempInfoUpdate );
//	}
//}


//10.09.20已废弃//bool CPlayer::CleanPhySkillPoint()   //清洗物理技能
//{
//	if( GetPhyTotalPoint() == 0)
//	{
//		return false;
//	}
//
//	if( GetPhyAllPoint() == GetPhyTotalPoint() )
//	{
//		return false;
//	}
//
//	bool bHavePhySkill = false;
//	for(int i=0; i<m_vecSkillList.size(); ++i)
//	{
//		if( NULL == m_vecSkillList[i] )
//			continue;
//
//		if( m_vecSkillList[i]->GetType() == TYPE_PHYSICS )
//		{
//			if( m_vecSkillList[i]->IsSpSkill() )
//				continue;
//			
//			if( m_vecSkillList[i]->IsNormalAttack() )
//				continue;
//
//			bHavePhySkill = true;
//			if( !CleanSinglePhySkill( m_vecSkillList[i] ) )
//			{
//				return false;
//			}
//		}
//	}
//
//
//	if( !bHavePhySkill )
//	{
//		return false;
//	}
//
//	m_fullPlayerInfo.baseInfo.usPhysicsAllotPoint = m_fullPlayerInfo.baseInfo.usPhyTotalGetPoint;	//洗点完可分配点数就是总点数
//
//
//	//全部技能都清洗完毕的话,就告知客户端
//	MGCleanSkillPoint cleanSkillMsg;
//	cleanSkillMsg.assignablePhyPoint = GetPhyAllPoint();
//	cleanSkillMsg.assignableMagPoint = GetMagAllPoint();
//	cleanSkillMsg.lNotiPlayerID = GetFullID();
//	StructMemCpy( cleanSkillMsg.skillData, &m_fullPlayerInfo.skillInfo.skillData, sizeof(cleanSkillMsg.skillData) );
//	cleanSkillMsg.skillSize = m_fullPlayerInfo.skillInfo.skillSize;
//	SendPkg<MGCleanSkillPoint>( &cleanSkillMsg );
//	return true;
//}
//
//
//bool CPlayer::CleanMagSkillPoint()   //清洗物理技能
//{
//	if( GetMagTotalPoint() == 0)
//	{
//		return false;
//	}
//
//	if( GetMagAllPoint() == GetMagTotalPoint() )
//	{
//		return false;
//	}
//	
//	bool bHaveMagSkill = false;
//	for(int i=0; i<m_vecSkillList.size(); ++i)
//	{
//		if( NULL == m_vecSkillList[i] )
//			continue;
//
//		if( m_vecSkillList[i]->GetType() == TYPE_MAGIC )
//		{
//			//是SP技能不用洗
//			if( m_vecSkillList[i]->IsSpSkill() )
//				continue;
//
//			if( m_vecSkillList[i]->IsNormalAttack() )
//				continue;
//
//			bHaveMagSkill = true;
//			if( !CleanSingleMagSkill( m_vecSkillList[i] ) )
//			{
//				return false;
//			}
//		}
//	}
//
//	if( !bHaveMagSkill )
//	{
//		return false;
//	}
//
//	m_fullPlayerInfo.baseInfo.usMagicAllotPoint = m_fullPlayerInfo.baseInfo.usMagTotalGetPoint;	//洗点完可分配点数就是总点数
//
//
//
//
//	//全部技能都清洗完毕的话,就告知客户端
//	MGCleanSkillPoint cleanSkillMsg;
//	cleanSkillMsg.assignablePhyPoint = GetPhyAllPoint();
//	cleanSkillMsg.assignableMagPoint = GetMagAllPoint();
//	cleanSkillMsg.lNotiPlayerID = GetFullID();
//	StructMemCpy( cleanSkillMsg.skillData, &m_fullPlayerInfo.skillInfo.skillData, sizeof(cleanSkillMsg.skillData) );
//	cleanSkillMsg.skillSize = m_fullPlayerInfo.skillInfo.skillSize;
//	SendPkg<MGCleanSkillPoint>( &cleanSkillMsg );
//	return true;
//}

//10.09.20已废弃//bool CPlayer::CleanSinglePhySkill( CMuxPlayerSkill* pSkill )
//{
//	if( !pSkill )
//		return false;
//
//	if( pSkill->GetType() != TYPE_PHYSICS )
//		return false;
//
//	unsigned int uiNewSkillID = pSkill->GetBasicSkillID();  
//	return pSkill->UpdateSkill( uiNewSkillID );
//}
//
//
//bool CPlayer::CleanSingleMagSkill( CMuxPlayerSkill* pSkill )
//{
//	if( !pSkill )
//		return false;
//
//	if( pSkill->GetType() != TYPE_MAGIC )
//		return false;
//
//
//	unsigned int uiNewSkillID = pSkill->GetBasicSkillID();  
//	return pSkill->UpdateSkill( uiNewSkillID );
//}

#ifdef AI_SUP_CODE

///定时执行的玩家相关事件；
void CPlayer::PlayerTimerProc()
{
	////清理已注册的接收玩家事件的怪物；
	//for ( vector< MonsterEventReged >::iterator iter=m_vecRegedMonsters.begin();
	//	iter != m_vecRegedMonsters.end(); )
	//{
	//	if ( iter->IsValid() )
	//	{
	//		++iter;
	//	} else {
	//		iter = m_vecRegedMonsters.erase(iter);
	//	}
	//}	

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	if( (currentTime - m_lastBuffUpdate).msec() >= 500 )
	{
		BuffProcess();
		m_lastBuffUpdate = currentTime;
	}

	if( (currentTime - m_lastHpMpUpdate).sec() >= CGMCmdManager::GetRsInterval() )
	{
		RestSkillProcess();
		m_lastHpMpUpdate = currentTime;
	}

	if ( (currentTime - m_lastOLTestCheckTime).msec()>= m_updateOLTestTime )
	{
		//检查答题是否过期
		if( GetPlayerOLTestPlate().IsTimeLimitAnswerNow() )
			GetPlayerOLTestPlate().CheckIfOutExpireTime();

		m_lastOLTestCheckTime = currentTime;
	}


	if( (currentTime - m_updatePosTime).msec() >=  m_updateTime )
	{
		m_updatePosTime = currentTime;

		//每隔10分钟，将玩家的基础信息存盘
		if ( (currentTime - m_lastSaveTime).msec() >= m_updateSaveDBTime )
		{
			NoticePlayerBaseChange();
			m_lastSaveTime = currentTime;
		}

		//是否在组队中，如果在组队，通知队伍中其他玩家信息　
		if( IsInTeam() )
		{
			if ( IsNeedNotiTeamMember() )
			{
				SendSelfInfoToOtherMembers();//只有中间信息发生过变化才通知队友
			}
		}

		if ( CheckDie() ) //检查玩家是否死亡，如果已到死亡复活时间，则复活，检查过后玩家为死，则返回真，为活，则返回假；
		{
			//如果玩家已死，则其余更新不进行;
			return;
		}

		////是否在组队中，如果在组队，通知队伍中其他玩家信息　
		//if( IsInTeam() )
		//{
		//	if ( IsNeedNotiTeamMember() )
		//	{
		//		SendSelfInfoToOtherMembers();//只有中间信息发生过变化才通知队友
		//	}
		//}

		TimeCheckUpdateAptInfo();//检测活点信息更新；

		m_pet.PetTimeProcess();//宠物定时处理；
		RookieBreakCheck();	

		//时间耐久度的道具是否消息
		OnCheckTimeWearPoint();

#ifdef OPEN_PUNISHMENT 
		CheckPunishTask();
#endif /*OPEN_PUNISHMENT*/

		if ( m_CopyInfo.IsIssueLeaveTimeFulfil() )
		{
			IssuePlayerLeaveCopyLF();//真正发起删除(通知center)
		}
	}

#ifdef HPMP_RECOVERY
	if (GetCurrentHP() != GetMaxHP())
		m_isNeedNotifyHpChange = true;
	if (GetCurrentMP() != GetMaxMP())
		m_isNeedNotifyMpChange = true;
	AddPlayerHp(m_TmpProp.lHpRecover);
	AddPlayerMp(m_TmpProp.lMpRecover);

	if ( (currentTime - m_lastNotifyHpMpChange).msec() >= 5000 )
	{
		m_lastNotifyHpMpChange = currentTime;
		if (m_isNeedNotifyHpChange)
			NotifyHpChange();
		if (m_isNeedNotifyMpChange)
			NotifyMpChange();
		m_isNeedNotifyHpChange = false;
		m_isNeedNotifyMpChange = false;
	}

#endif

};

/////怪物将自己注册到CPlayer,以便接收该玩家的后续事件；
//bool CPlayer::RegisterMonster( CMonster* pMonster )
//{
//	if ( NULL == pMonster )
//	{
//		return false;
//	}
//	if ( IsMonsterReged( pMonster ) )
//	{
//		D_DEBUG( "重复注册怪物%d\n", pMonster->GetMonsterID() );
//		return true;
//	}
//	m_vecRegedMonsters.push_back( MonsterEventReged(pMonster) );
//	return true;
//}
//
/////通知已注册玩家相应事件发生；
//bool CPlayer::NotifyRegedMonsterAiEvent( MONSTER_EVENT aiEvent )
//{
//	switch ( aiEvent )
//	{
//	case MEVENT_PLAYER_LEAVEVIEW:
//		for ( vector<MonsterEventReged>::iterator iter = m_vecRegedMonsters.begin(); 
//			iter != m_vecRegedMonsters.end(); ++iter )
//		{ 
//			if ( iter->IsValid() )
//			{
//				if ( NULL != iter->GetMonster() )
//				{
//					iter->GetMonster()->OnPlayerLeaveView( this, AILR_DIE );
//				}
//			}			
//		}
//		break;
//	default:
//		{}
//	}
//	return true;
//}
#endif //AI_SUP_CODE



bool CPlayer::CreateMuxBuff( MuxBuffProp* pBuffProp, unsigned int buffTime, const PlayerID& createID )
{
	if ( NULL == pBuffProp )
	{
		return false;
	}
	if ( IsDying() && pBuffProp->isDeathClear )
	{
		return false;
	}
	return m_buffManager.CreateMuxBuff( pBuffProp, buffTime, createID );
}


//通过一个ID来创建BUFF
bool CPlayer::CreateMuxBuffByID( unsigned int buffID, unsigned int buffTime, const PlayerID& createID )
{
	MuxBuffProp* pBuffProp = BuffPropSingleton::instance()->FindTSkillProp( buffID );
	if( NULL == pBuffProp )
	{
		D_WARNING( "CPlayer::CreateMuxBuffByID， 找不到欲给玩家%s添加的buff%d\n", GetAccount(), buffID );
		return false;
	}

	if ( IsDying() && pBuffProp->isDeathClear )
	{
		D_WARNING( "CPlayer::CreateMuxBuffByID， 玩家%s已死亡，忽略buff%d\n", GetAccount(), buffID );
		return false;
	}
	return m_buffManager.CreateMuxBuff( pBuffProp, buffTime, createID );
}

bool CPlayer::DeleteMuxBuff( unsigned int buffID )
{
	return m_buffManager.DeleteMuxBuff( buffID );
}


//离线前每个BUFF更新一下
void CPlayer::OnBuffLeave( )
{
	UpdateBuffData( 1 );
}


//死亡的话是一切BUFF消失，一切栏位包含数据库都要清除
void CPlayer::OnBuffDie()
{
	TRY_BEGIN;
	m_buffManager.OnBuffDie();
	TRY_END;
}


bool CPlayer::SetSingleBuffData( unsigned buffIndex, unsigned int buffID, unsigned buffTime )
{
	TRY_BEGIN;
	if( buffIndex >= ARRAY_SIZE(m_fullPlayerInfo.bufferInfo.buffData) )
	{
		D_ERROR("SetSingleBuffData 数组越界\n");
		return false;
	}
	m_fullPlayerInfo.bufferInfo.buffData[buffIndex].uiBuffID = buffID;
	m_fullPlayerInfo.bufferInfo.buffData[buffIndex].uiLeftDurSeconds = buffTime;
	return true;
	TRY_END;
	return false;
}

//上线初试化新的buff
bool CPlayer::InitMuxBuff()
{
	PlayerID createID;
	createID.dwPID = 0;
	createID.wGID = 0;
	for( int i=0; i< ARRAY_SIZE(m_fullPlayerInfo.bufferInfo.buffData); ++i)
	{
		 if( m_fullPlayerInfo.bufferInfo.buffData[i].uiBuffID == 0 )
			 continue;

		MuxBuffProp* pBuffProp = BuffPropSingleton::instance()->FindTSkillProp( m_fullPlayerInfo.bufferInfo.buffData[i].uiBuffID );
		if( NULL == pBuffProp )
			continue;

		CreateMuxBuff( pBuffProp, m_fullPlayerInfo.bufferInfo.buffData[i].uiLeftDurSeconds, createID );
	}
	return true;
}


void CPlayer::OnUnUseItemNotifyNearByPlayer( unsigned long itemTypeID )
{
	MGUnUseItem srvmsg;
	srvmsg.equipIndex = itemTypeID;
	srvmsg.playerID =   GetFullID();

	//NotifyPlayerNearby<MGUnUseItem> ( &srvmsg );
	CMuxMap* pPlayerMap = GetCurMap();
	if ( NULL != pPlayerMap )
	{
		pPlayerMap->NotifySurrounding<MGUnUseItem>( &srvmsg, GetPosX(), GetPosY() );
	}
}


bool CPlayer::OnDropItem(unsigned int itemUID)
{
	TRY_BEGIN;

	bool isSuccess = false;
	int  dropItemType  = -1;//装备栏的类型
	ItemInfo_i backItemInfo;
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::OnDropItem, NULL == GetFullPlayerInfo()\n");
		return false;
	}

	do 
	{
		if( !IsCanDrop( itemUID ) )//如果不能丢弃
		{
			D_WARNING( "OnDropItem，玩家%s丢弃道具(uid:%d)，该道具不可被丢弃\n", GetAccount(), itemUID );
			break;
		}

		bool bIsFind = false;

		PlayerInfo_3_Pkgs& pkgInfp = playerInfo->pkgInfo;
		for ( int i=0; i<PACKAGE_SIZE; i++ )
		{
			if ( i >= ARRAY_SIZE(pkgInfp.pkgs) )
			{
				D_ERROR( "OnDropItem，玩家%s丢弃道具(uid:%d)，i(%d) >= ARRAY_SIZE(pkgInfp.pkgs)，越界！\n", GetAccount(), itemUID, i );
				break;
			}
			ItemInfo_i& itemInfo = pkgInfp.pkgs[i];
			if ( itemInfo.uiID != itemUID )
			{
				continue;
			}

			backItemInfo = itemInfo;
			//在包裹中找到了欲丢弃道具；
			D_DEBUG("玩家%s丢弃包裹道具,道具索引:%d  道具类型:%d\n", GetAccount(), i, itemInfo.usItemTypeID );
			dropItemType = itemInfo.usItemTypeID;
			//从包裹管理器中删除该物品
			GetItemDelegate().DropPkgItem( itemUID );

			//丢弃道具可能会引发收集任务的数量改变
			OnDropItemCounterEvent( itemInfo  );

			ClearPkgItem(itemUID);
			bIsFind = true;

			//通知对应的包裹位更新
			NoticePlayerItemUpdate( false, itemInfo, i );
			break;
		}

		//@brief:如果过在包裹中没有找到
		if ( !bIsFind )
		{
			ItemInfo_i* pEquipItemInfo = NULL;
			for ( int index = 0; index<EQUIP_SIZE; index++ )
			{
				pEquipItemInfo = GetEquipByPos( index );
				if ( NULL == pEquipItemInfo )
				{
					D_ERROR( "玩家%s丢弃道具，在装备中查找该道具时GetEquipByPos(%d)失败\n", GetAccount(), index );
					break;
				}
				if ( pEquipItemInfo->uiID != itemUID )
				{
					continue;
				}

				//在装备中找到了欲丢弃道具；
				backItemInfo = *pEquipItemInfo;
				D_DEBUG( "玩家%s丢弃装备栏中的道具,道具索引:%d  道具类型:%d\n", GetAccount(), index, pEquipItemInfo->usItemTypeID );

				//如果在骑乘状态，并且卸载的是骑乘装备
				if ( IsDriveEquipPos(index) && GetSelfStateManager().IsOnRideState() )
				{
					D_ERROR( "玩家%s欲丢弃骑乘装备，其在骑乘状态，必须先下马才行\n", GetAccount() );
					break;
				}

				//如果装备栏有道具，则需要把装备栏的道具所加的属性减去
				OnUnEquipItem( *pEquipItemInfo, index );
				//不使用装备了,通知周围的玩家
				OnUnUseItemNotifyNearByPlayer( pEquipItemInfo->usItemTypeID );

				//从包裹管理器中删除，因为卸载了道具，道具跑到了包裹管理器中
				GetItemDelegate().DropPkgItem( itemUID );

				dropItemType = pEquipItemInfo->usItemTypeID;
				pEquipItemInfo->uiID = PlayerInfo_2_Equips::INVALIDATE;
				pEquipItemInfo->usItemTypeID  = 0;
				pEquipItemInfo->usWearPoint = 0;
				pEquipItemInfo->randSet/*usAddId*/ = 0;
				pEquipItemInfo->ucLevelUp = 0;
				pEquipItemInfo->ucCount = 0;
				bIsFind = true;

				NoticeEquipItemChange( index );

				NoticePlayerItemUpdate( true, *pEquipItemInfo,  index );
				break;
			}//for ( int index = 0; index<EQUIP_SIZE; index++ ) //如果在装备中没找到
		}// if ( !bIsFind ) //如果在包裹中没找到

		if ( !bIsFind )
		{
			PlayerInfo_Fashions& fashions = Fashions();
			for ( int index = 0 ; index < MAX_FASHION_COUNT; index++ )
			{
				if ( index >= ARRAY_SIZE(fashions.fashionData) )
				{
					D_DEBUG( "玩家%s丢弃道具%d,在时装栏中查找时%d越界\n", GetAccount(), itemUID, index );
					break;
				}
				ItemInfo_i& itemInfo = fashions.fashionData[index];
				if ( itemInfo.uiID != itemUID )
				{
					continue;
				}

				//在时装栏中找到；
				backItemInfo = itemInfo;
				D_DEBUG( "玩家%s丢弃时装栏中的道具,道具索引:%d  道具类型:%d\n", GetAccount(), index, itemInfo.usItemTypeID );

				CBaseItem* pUnEquipItemBase = GetItemDelegate().GetFashionItem( itemInfo.uiID );
				if ( NULL == pUnEquipItemBase )
				{
					break;
				}

				//如果装备栏有道具，则需要把装备栏的道具所加的属性减去
				OffFashionItem( itemInfo, pUnEquipItemBase, index );

				//不使用装备了,通知周围的玩家
				OnUnUseItemNotifyNearByPlayer( itemInfo.usItemTypeID );

				//从包裹管理器中删除，因为卸载了道具，道具跑到了包裹管理器中
				GetItemDelegate().DropPkgItem( itemUID );

				dropItemType = itemInfo.usItemTypeID;
				itemInfo.uiID = PlayerInfo_2_Equips::INVALIDATE;
				itemInfo.usItemTypeID  = 0;
				itemInfo.usWearPoint = 0;
				itemInfo.randSet/*usAddId*/ = 0;
				itemInfo.ucLevelUp = 0;
				itemInfo.ucCount = 0;
				bIsFind = true;

				MGDbSaveInfo saveInfo;
				FillPlayerFashionInfo( saveInfo );
				ForceGateSend<MGDbSaveInfo>( &saveInfo );
				break;
			}
		}

		if ( !bIsFind )
		{
			D_ERROR("在玩家%s的包裹和道具栏中根本找不到要卸载的道具%d!\n", GetAccount(), itemUID );
			break;
		}
		isSuccess = true;
		CLogManager::DoDropItem(this, backItemInfo);//记日至
	} while( false );

	MGDropItemResult srvmsg;
	srvmsg.result = isSuccess;
	srvmsg.uiItemID = itemUID;
	SendPkg<MGDropItemResult>( &srvmsg );

	return isSuccess;
	TRY_END;
	return false;
}

//当NPC给与玩家道具时
//@itemID 道具的编号
//@itemCount 本次创建道具的数目 
bool CPlayer::OnNpcGiveItem( unsigned int itemTypeID ,int itemCount /* = 1  */)
{
	bool isSuccess = false;

	TRY_BEGIN;

	isSuccess = GetNormalItem( itemTypeID, itemCount );

	TRY_END;

	return isSuccess;
}

void CPlayer::NoticePlayerItemUpdate(bool isEquip , const ItemInfo_i& itemInfo, unsigned int itemIndex )
{
	//通知对应的包裹位更新
	MGUpdatePlayerItemInfo updateItemInfo;
	updateItemInfo.isEquip = isEquip;
	updateItemInfo.itemIndex = itemIndex;
	updateItemInfo.itemInfo  = itemInfo;
	updateItemInfo.playerUID = GetDBUID();
	SendPkg<MGUpdatePlayerItemInfo>( &updateItemInfo );
}

void CPlayer::NoticePlayerLevelUpdate(unsigned int level )
{
	NoticePlayerBaseChange();
}


void CPlayer::NoticePlayerBaseChange()
{
	MGUpdatePlayerChangeBaseInfo baseInfo;
	StructMemSet( baseInfo, 0x0, sizeof(baseInfo) );
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::NoticePlayerBaseChange, NULL == GetFullPlayerInfo()\n");
		return;
	}
	baseInfo.baseChangeInfo.playerUID = GetDBUID();
	baseInfo.baseChangeInfo.dwExp     = GetExp();
	baseInfo.baseChangeInfo.dwMagicExp = playerInfo->baseInfo.dwMagicExp;
	baseInfo.baseChangeInfo.dwSilverMoney    = GetSilverMoney();//双币种判断
	baseInfo.baseChangeInfo.dwGoldMoney    = GetGoldMoney();//双币种判断
	baseInfo.baseChangeInfo.dwPhysicsExp = playerInfo->baseInfo.dwPhysicsExp;
	baseInfo.baseChangeInfo.iPosX   = GetPosX();
	baseInfo.baseChangeInfo.iPosY   = GetPosY();
	baseInfo.baseChangeInfo.racePrestige = GetRacePrestige();
	baseInfo.baseChangeInfo.sGoodEvilPoint = playerInfo->baseInfo.sGoodEvilPoint;
	baseInfo.baseChangeInfo.uiEvilTime  = playerInfo->baseInfo.uiEvilTime;
	baseInfo.baseChangeInfo.uiHP = GetCurrentHP();
	baseInfo.baseChangeInfo.uiMP = GetCurrentMP();
	baseInfo.baseChangeInfo.usLevel = playerInfo->baseInfo.usLevel;
	baseInfo.baseChangeInfo.usMagicAllotPoint = playerInfo->baseInfo.usMagicAllotPoint;
	baseInfo.baseChangeInfo.usMagicLevel = playerInfo->baseInfo.usMagicLevel;
	baseInfo.baseChangeInfo.usMagTotalGetPoint = playerInfo->baseInfo.usMagTotalGetPoint;
	baseInfo.baseChangeInfo.usMapID = GetMapID();
	baseInfo.baseChangeInfo.usPhysicsAllotPoint = playerInfo->baseInfo.usPhysicsAllotPoint;
	baseInfo.baseChangeInfo.usPhysicsLevel = playerInfo->baseInfo.usPhysicsLevel;
	baseInfo.baseChangeInfo.usPhyTotalGetPoint = playerInfo->baseInfo.usPhyTotalGetPoint;
	baseInfo.baseChangeInfo.strength = playerInfo->baseInfo.strength;
	baseInfo.baseChangeInfo.vitality = playerInfo->baseInfo.vitality;
	baseInfo.baseChangeInfo.agility = playerInfo->baseInfo.agility;
	baseInfo.baseChangeInfo.spirit = playerInfo->baseInfo.spirit;
	baseInfo.baseChangeInfo.intelligence = playerInfo->baseInfo.intelligence;
	baseInfo.baseChangeInfo.unallocatePoint = playerInfo->baseInfo.unallocatePoint;
	baseInfo.baseChangeInfo.totalExp = playerInfo->baseInfo.totalExp;
	SendPkg<MGUpdatePlayerChangeBaseInfo>( &baseInfo );
}

void CPlayer::NoticePlayerTaskRdUpdate(bool isbkTask, const TaskRecordInfo& taskRdInfo ,unsigned int rdIndex )
{
	MGUpdateTaskInfo updateTaskInfo;
	updateTaskInfo.isBackTask = isbkTask;
	updateTaskInfo.playerUID = GetDBUID();
	updateTaskInfo.lNotiPlayerID = GetFullID();
	updateTaskInfo.taskIndex = rdIndex;
	updateTaskInfo.taskInfo  = taskRdInfo;
	ForceGateSend<MGUpdateTaskInfo>(  &updateTaskInfo  );
}

//NPC取走玩家道具;
bool CPlayer::OnNpcRemoveItem( unsigned int itemTypeID ,int itemCount )
{
	bool isSuccess = false;

	TRY_BEGIN;

	isSuccess = OnDropItemByType( itemTypeID,itemCount );

	TRY_END;

	return isSuccess;
}

bool CPlayer::OnNpcRemoveScriptItem()
{
	CBaseItem* pItem = m_itemChatInfo.GetItemChatTarget();
	if ( pItem )
	{
		int itemIndex = -1;bool isEquip = false;
		ItemInfo_i* piteminfo = GetItemInfoByItemUID( pItem->GetItemUID(), itemIndex, isEquip );
		if ( piteminfo && itemIndex != -1 )
		{
			if ( piteminfo->ucCount == 1 )//如果就一个了,丢弃道具
			{
				return OnDropItem( piteminfo->uiID );
			}
			else if( piteminfo->ucCount>1 )//如果还有多个
			{
				piteminfo->ucCount--;
				if ( isEquip )
					NoticeEquipItemChange( itemIndex );
				else
					NoticePkgItemChange( itemIndex );

				return true;
			}
		}
		
	}
	return false;
}

void CPlayer::ShowMailBox()
{
	MGShowMailBox srvmsg;
	srvmsg.lNotiPlayerID = GetFullID();
	SendPkg<MGShowMailBox>( & srvmsg );
}

void CPlayer::UpdateSkillInfoToDB()
{
	MGDbSaveInfo saveInfo;
	saveInfo.playerID = GetFullID();
	saveInfo.infoType = DI_SKILL;
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::UpdateSkillInfoToDB, NULL == GetFullPlayerInfo()\n");
		return;
	}
	StructMemCpy( saveInfo.skillInfo,  &playerInfo->skillInfo,sizeof(saveInfo.skillInfo));
	ForceGateSend<MGDbSaveInfo>( &saveInfo );
}


void CPlayer::NoticeHPMPChange()
{
	if ( GetCurrentHP() > GetMaxHP() )
	{
		SetPlayerHp( GetMaxHP() );
		NotifyHpChange();
	}

	if ( GetCurrentMP() > GetMaxMP() )
	{
		SetPlayerMp( GetMaxMP() );
		NotifyMpChange();
	}

	NotifyMaxHPChange();
	NotifyMaxMPChange();
}

void CPlayer::OnCheckTimeWearPoint()
{
	time_t curtime = time(NULL);
	if ( difftime( curtime, m_wearpointchecktime ) > 60 )
	{
		FullPlayerInfo * playerInfo = GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CPlayer::OnCheckTimeWearPoint, NULL == GetFullPlayerInfo()\n");
			return;
		}
		PlayerInfo_3_Pkgs& pkg = playerInfo->pkgInfo;
		for ( unsigned int i = 0 ; i< PACKAGE_SIZE; i++ )
		{
			ItemInfo_i* itemInfo =  &pkg.pkgs[i];
			if ( itemInfo->uiID != PlayerInfo_3_Pkgs::INVALIDATE
				&& itemInfo->usWearPoint > 0 )
			{
				DoWear( itemInfo, CItemPublicProp::LAST_DAY );
			}
		}

		m_wearpointchecktime = curtime;
	}
}

bool CPlayer::ShowAuctionPlate()
{
	if ( !m_pMap )
		return false;

	MGShowAuctionPlate showAuctionPlate;
	SendPkg<MGShowAuctionPlate>( &showAuctionPlate );
	return true;
}

//
////装备实例属性
//enum ItemInsProp
//{
//	IIP_UNIID  = 400,   //单件装备唯一ID(实例生成时获得)
//	IIP_TYPEID = 300,   //装备类型ID(+基本XML决定所有需要基本XML决定的属性)
//	IIP_LEVEL = 301,    //装备等级(初始在实例时生成，以后随升级变化，+升级XML决定所有需要升级XML决定的属性)
//
//	IIP_randSet  = 500, //装备实例对应随机集(实例生成时获得，+luckySeed+excelSeed+随机集XML决定所有幸运与卓越属性)
//	IIP_luckySeed  = 501, //装备实例幸运号(实例生成时获得，见IIP_randSet)
//	IIP_excelSeed  = 502, //装备实例随机号(实例生成时获得，见IIP_randSet)
//
//	IIP_equipStr = 200, //装备所需力量(IIP_TYPEID+基本XML+升级XML属性决定)
//	IIP_equipAgi = 201, //装备所需敏捷(IIP_TYPEID+基本XML+升级XML属性决定)
//	IIP_equipInt = 202, //装备所需智力(IIP_TYPEID+基本XML+升级XML属性决定)
//	IIP_equipSpi = 203, //装备所需精神(IIP_TYPEID+基本XML+升级XML属性决定)
//	IIP_equipVit = 204, //装备所需体质(IIP_TYPEID+基本XML+升级XML属性决定)
//};//enum ItemInsProp
//
///*
//<!--各属性编码对应属性：
//基础属性
//
//          |  物理攻击力   （镶嵌提升）
//    武器  |  魔法攻击力   （镶嵌提升）
//          |  物理命中     （随武器固定）
//          |  魔法命中
//
//
//
//          |  物理防御力
//    防具  |  魔法防御力
//          |  移动速度
//
//
//升级只提升基础属性
//
//
//          | 1   力量
//          | 2   敏捷
//          | 3   智力
//          | 4   精神
//幸运      | 5   体质
//          | 6   HP上限
//          | 7   MP上限
//          | 8   幸运装备合成率
//          | 9   幸运一击率
//
//
//          | 10   击晕（几率）等级
//          | 11   束缚（几率）等级
//卓越      | 12   物理卓越一击伤害
//武器      | 13   魔法卓越一击伤害
//          | 14   攻击速度提升
//
//
//
//
//卓越      | 15   抗晕几率
//防具      | 16   抗束几率
//          | 17   物理闪避 
//          | 18   魔法闪避
//          | 19   抗物理卓越一击
//          | 20   抗魔法卓越一击
//
//
//          | 21   20%几率造成额外物理伤害
//          | 22   20%几率造成额外魔法伤害
//          | 23   抗晕（几率）等级
//          | 24   抗束缚（几率）等级
//未确定    | 25   物理闪躲率
//增加      | 26   魔法闪躲率
//          | 27   抗物理卓越一击率
//          | 28   抗魔法卓越一击率
//-->
//ItemAddtionProp 属性：
//		TYPE_SI32                	Hp;                                         	//
//		TYPE_SI32                	Mp;                                         	//
//		TYPE_SI32                	PhysicsAttack;                              	//
//		TYPE_SI32                	MagicAttack;                                	//
//		TYPE_SI32                	PhysicsDefend;                              	//
//		TYPE_SI32                	MagicDefend;                                	//
//		TYPE_SI32                	PhysicsHit;                                 	//
//		TYPE_SI32                	MagicHit;                                   	//
//		TYPE_SI32                	PhysicsCritical;                            	//
//		TYPE_SI32                	MagicCritical;                              	//
//		TYPE_SI32                	PhysicsAttackPercent;                       	//
//		TYPE_SI32                	MagicAttackPercent;                         	//
//		TYPE_SI32                	PhysicsCriticalDamage;                      	//
//		TYPE_SI32                	MagicCriticalDamage;                        	//
//		TYPE_SI32                	PhysicsDefendPercent;                       	//
//		TYPE_SI32                	MagicDefendPercent;                         	//
//		TYPE_SI32                	PhysicsCriticalDerate;                      	//
//		TYPE_SI32                	MagicCriticalDerate;                        	//
//		TYPE_SI32                	PhysicsCriticalDamageDerate;                	//
//		TYPE_SI32                	MagicCriticalDamageDerate;                  	//
//		TYPE_SI32                	PhysicsJouk;                                	//物理闪避
//		TYPE_SI32                	MagicJouk;                                  	//魔法闪避
//		TYPE_SI32                	PhysicsDeratePercent;                       	//
//		TYPE_SI32                	MagicDeratePercent;                         	//
//		TYPE_SI32                	Stun;                                       	// 击晕
//		TYPE_SI32                	Tie;                                        	// 束缚
//		TYPE_SI32                	Antistun;                                   	// 抗击晕
//		TYPE_SI32                	Antitie;                                    	// 抗束缚
//		TYPE_SI32                	PhysicsAttackAdd;                           	//
//		TYPE_SI32                	MagicAttackAdd;                             	//
//		TYPE_SI32                	PhysicsRift;                                	//
//		TYPE_SI32                	MagicRift;                                  	//
//		TYPE_SI32                	AddSpeed;                                   	//
//		TYPE_SI32                	AttackPhyMin;                               	//
//		TYPE_SI32                	AttackPhyMax;                               	//
//		TYPE_SI32                	AttackMagMin;                               	//
//		TYPE_SI32                	AttackMagMax;                               	//
//		TYPE_SI32                	strength;                                   	//力量
//		TYPE_SI32                	agility;                                    	//敏捷
//		TYPE_SI32                	intelligence;                               	//智力
//		TYPE_SI32                	spirit;                                     	//精神
//		TYPE_SI32                	physique;                                   	//体质
//		TYPE_SI32                	luckyCombindRatio;                          	//幸运装备合成率
//		TYPE_SI32                	luckyhitRatio;                              	//幸运一击率
//		TYPE_SI32                	phyexcellentdemage;                         	//物理卓越一击伤害
//		TYPE_SI32                	magexcellentdemage;                         	//魔法卓越一击伤害
//		TYPE_SI32                	attackspeed;                                	//攻击速率
//		TYPE_SI32                	ingorephydefend;                            	//无视物理防御
//		TYPE_SI32                	ingoremagdefend;                            	//无视魔法防御几率
//		TYPE_SI32                	movespeed;                                  	//移动速度
//		TYPE_SI32                	cureaddition;                               	//治疗法术加成
//		TYPE_SI32                	hatredaddition;                             	//仇恨值提升
//		TYPE_SI32                	enhancespecialskill;                        	//特殊技能等级提升
//*/

////装备对人物的基本属性加成枚举定义
//enum ItemBaseAddAtt
//{
//	IBA_phyAtk = 100, //物攻(IIP_TYPEID+基本XML+升级XML属性)
//	IBA_mgAtk = 101, //魔攻(IIP_TYPEID+基本XML+升级XML属性)
//	IBA_phyHitRatio = 102, //物理命中(IIP_TYPEID+基本XML属性)
//	IBA_mgHitRatio = 103,  //魔法命中(IIP_TYPEID+基本XML属性)
//	IBA_phyDef = 104, //物防(IIP_TYPEID+基本XML+升级XML属性)
//	IBA_mgDef = 105, //魔防(IIP_TYPEID+基本XML+升级XML属性)
//	IBA_spdAdd = 106 //移动速度提升(IIP_TYPEID+基本XML)
//}

//取玩家装备基本属性加成项;
int& CPlayer::GetNewItemAddBaseAtt( ItemBaseAddAtt attrID )
{
	ItemAddtionProp& itemAddition = GetItemAddtionProp();
	switch ( attrID )
	{
	case IBA_phyAtk:// = 100, //物攻
		return itemAddition.PhysicsAttack;
		break;
	case IBA_mgAtk:// = 101, //魔攻
		return itemAddition.MagicAttack;
		break;
	case IBA_phyHitRatio:// = 102, //物理命中
		return itemAddition.PhysicsHit;
		break;
	case IBA_mgHitRatio:// = 103, //魔法命中
		return itemAddition.MagicHit;
		break;
	case IBA_phyDef:// = 104, //物防
		return itemAddition.PhysicsDefend;
		break;
	case IBA_mgDef:// = 105, //魔防
		return itemAddition.MagicDefend;
		break;
	case IBA_spdAdd:// = 106, //移动速度提升
		return itemAddition.AddSpeed;
		break;
	default:
		{
			D_ERROR( "GetNewItemAddAttByID，不可识别属性%d\n", attrID );
		}
	}

	return itemAddition.dumpVal;
}

////装备对人物的随机属性加成对应ID号(对应策划配置文件中的配置号)
//enum ItemRandAddAtt
//{
//	//幸运属性，随机集确定
//    IRA_addStr = 1, //          | 1   力量
//    IRA_addAgi = 2, //          | 2   敏捷
//    IRA_addInt = 3, //          | 3   智力
//    IRA_addSpi = 4, //          | 4   精神
//    IRA_addVit = 5, //幸运      | 5   体质
//    IRA_addHpMax = 6, //          | 6   HP上限
//    IRA_addMpMax = 7, //          | 7   MP上限
//    IRA_addLuckComb = 8, //          | 8   幸运装备合成率
//    IRA_addLuckAtk = 9, //          | 9   幸运一击率
//
//	//卓越属性，随机集确定
//    IRA_addAtkStun = 10, //          | 10   击晕（几率）等级
//    IRA_addAtkTie = 11, //          | 11   束缚（几率）等级
//    IRA_addAtkPhExc = 12, //卓越      | 12   物理卓越一击伤害
//    IRA_addAtkMgExc = 13, //武器      | 13   魔法卓越一击伤害
//    IRA_addAtkSpd = 14, //          | 14   攻击速度提升
//
//    IRA_addAntiStun = 15, //卓越      | 15   抗晕几率
//    IRA_addAntiTie = 16, //防具      | 16   抗束几率
//    IRA_addPhyHide = 17, //          | 17   物理闪避 
//    IRA_addMgHide = 18, //          | 18   魔法闪避
//    IRA_addAntiPE = 19, //          | 19   抗物理卓越一击
//    IRA_addAntiME = 20, //          | 20   抗魔法卓越一击
//
//	//除幸运与卓越外的其它属性，由IIP_TYPEID+基本XML属性确定
//    IRA_add21 = 21, //           | 21   20%几率造成额外物理伤害
//    IRA_add22 = 22, //           | 22   20%几率造成额外魔法伤害
//    IRA_add23 = 23, //           | 23   抗晕（几率）等级
//    IRA_add24 = 24, //           | 24   抗束缚（几率）等级
//    IRA_add25 = 25, //未确定增加 | 25   物理闪躲率
//    IRA_add26 = 26, //基本XML确定| 26   魔法闪躲率
//    IRA_add27 = 27, //           | 27   抗物理卓越一击率
//    IRA_add28 = 28, //           | 28   抗魔法卓越一击率
//};//enum ItemAttAdd
//取玩家装备属性加成项;
int& CPlayer::GetNewItemAddRandAtt( ItemRandAddAtt attrID )
{
	ItemAddtionProp& itemAddition = GetItemAddtionProp();
	switch ( attrID )
	{
	case IRA_addStr:// = 1, //          | 1   力量
		return itemAddition.strength;
		break;
	case IRA_addAgi:// = 2, //          | 2   敏捷
		return itemAddition.agility;
		break;
	case IRA_addInt:// = 3, //          | 3   智力
		return itemAddition.intelligence;
		break;
	case IRA_addSpi:// = 4, //          | 4   精神
		return itemAddition.spirit;
		break;
	case IRA_addVit:// = 5, //幸运      | 5   体质
		return itemAddition.physique;
		break;
	case IRA_addHpMax:// = 6, //          | 6   HP上限
		return itemAddition.Hp;//??上限还是加血
		break;
	case IRA_addMpMax:// = 7, //          | 7   MP上限
		return itemAddition.Mp;//??上限还是加魔
		break;
	case IRA_addLuckComb:// = 8, //          | 8   幸运装备合成率
		return itemAddition.luckyCombindRatio;
		break;
	case IRA_addLuckAtk:// = 9, //          | 9   幸运一击率
		return itemAddition.luckyhitRatio;
		break;

	case IRA_addAtkStun:// = 10, //          | 10   击晕（几率）等级
		return itemAddition.Stun;
		break;
	case IRA_addAtkTie:// = 11, //          | 11   束缚（几率）等级
		return itemAddition.Tie;
		break;
	case IRA_addAtkPhExc:// = 12, //卓越      | 12   物理卓越一击伤害
		return itemAddition.phyexcellentdemage;
		break;
	case IRA_addAtkMgExc:// = 13, //武器      | 13   魔法卓越一击伤害
		return itemAddition.magexcellentdemage;
		break;
	case IRA_addAtkSpd:// = 14, //          | 14   攻击速度提升
		return itemAddition.attackspeed;
		break;

	case IRA_addAntiStun:// = 15, //卓越      | 15   抗晕几率
		return itemAddition.Antistun;
		break;
	case IRA_addAntiTie:// = 16, //防具      | 16   抗束几率
		return itemAddition.Antitie;
		break;
	case IRA_addPhyHide:// = 17, //          | 17   物理闪避 
		return itemAddition.PhysicsJouk;
		break;
	case IRA_addMgHide:// = 18, //          | 18   魔法闪避
		return itemAddition.MagicJouk;
		break;
	case IRA_addAntiPE:// = 19, //          | 19   抗物理卓越一击
		return itemAddition.antiPE;
		break;
	case IRA_addAntiME:// = 20, //          | 20   抗魔法卓越一击
		return itemAddition.antiME;
		break;

	default:
		{
			D_ERROR( "GetNewItemAddAttByID，不可识别属性%d\n", attrID );
		}
	}

	return itemAddition.dumpVal;
} //int* CPlayer::GetNewItemAddRandAtt( ItemRandAddAtt attrID )

//int* CPlayer::GetItemAttributeByAttriID(unsigned int attributeID )
//{
//	ItemAddtionProp& itemAddition = GetItemAddtionProp();
//	switch( attributeID )
//	{
//	case 1://物攻
//		return &itemAddition.PhysicsAttack;
//	case 2://魔攻
//		return &itemAddition.MagicAttack;
//	case 3://物防
//		return &itemAddition.PhysicsDefend;
//	case 4://魔防
//		return &itemAddition.MagicDefend;
//	case 5://HP上限
//		return &itemAddition.Hp;
//	case 6://MP
//		return &itemAddition.Mp;
//	case 7://物理命中
//		return &itemAddition.PhysicsHit;
//	case 8://魔法命中
//		return &itemAddition.MagicHit;
//	case 9://物理闪避等级
//		return &itemAddition.PhysicsJouk;
//	case 10://魔法闪避等级
//		return &itemAddition.MagicJouk;
//	case 11://物理会心一击等级
//		return &itemAddition.PhysicsCritical;
//	case 12://魔法会心一击等级
//		return &itemAddition.MagicCritical;
//	case 13://减免物理会心一击等级
//		return &itemAddition.PhysicsCriticalDerate;
//	case 14://减免魔法会心一击等级
//		return &itemAddition.MagicCriticalDerate;
//	case 15://物理攻击百分比
//		return &itemAddition.PhysicsAttackPercent;
//	case 16://魔法攻击百分比
//		return &itemAddition.MagicAttackPercent;
//	case 17://物理防御百分比
//		return &itemAddition.PhysicsDefendPercent;
//	case 18://魔法防御百分比
//		return &itemAddition.MagicDefendPercent;
//	case 19://物理会心一击伤害
//		return &itemAddition.PhysicsCriticalDamage;
//	case 20://魔法会心一击伤害
//		return &itemAddition.MagicCriticalDamage;
//	case 21://减免物理会心一击伤害
//		return &itemAddition.PhysicsCriticalDamageDerate;
//	case 22://减免魔法会心一击伤害
//		return &itemAddition.MagicCriticalDamageDerate;
//	case 25://物理伤害减免百分比
//		return &itemAddition.PhysicsDeratePercent;
//	case 26://魔法伤害减免百分比
//		return &itemAddition.MagicDeratePercent;
//	case 27://击晕（几率）等级
//		return &itemAddition.Stun;
//	case 28://束缚（几率）等级
//		return &itemAddition.Tie;
//	case 29://抗晕（几率）等级
//		return &itemAddition.Antistun;
//	case 30://抗束缚（几率）等级
//		return &itemAddition.Antitie;
//	case 33://最小物理
//		return &itemAddition.AttackPhyMin;
//	case 34://最大物理
//		return &itemAddition.AttackPhyMax;
//	case 35://最小魔法
//		return &itemAddition.AttackMagMin;
//	case 36://最大魔法
//		return &itemAddition.AttackMagMax;
//	case 37://力量
//		return &itemAddition.strength;
//	case 38://敏捷
//		return &itemAddition.agility;
//	case 39://智力
//		return &itemAddition.intelligence;
//	case 40://精神
//		return &itemAddition.spirit;
//	case 41://体质
//		return &itemAddition.physique;
//	case 42://幸运装备合成率
//		return &itemAddition.luckyCombindRatio;
//	case 43://幸运一击率
//		return &itemAddition.luckyhitRatio;
//	case 44://物理卓越一击伤害
//		return &itemAddition.phyexcellentdemage;
//	case 45://魔法卓越一击伤害
//		return &itemAddition.magexcellentdemage;
//	case 46://攻击速度提升
//		return &itemAddition.attackspeed;
//	case 47://无视物理防御几率
//		return &itemAddition.ingorephydefend;
//	case 48://无视魔法防御几率
//		return &itemAddition.ingoremagdefend;
//	case 49://移动速度
//		return &itemAddition.movespeed;
//	case 50://治疗法术加成
//		return &itemAddition.cureaddition;
//	case 51://仇恨值提升
//		return &itemAddition.hatredaddition;
//	case 52://特殊技能等级提升
//		return &itemAddition.enhancespecialskill;
//	default:
//		return NULL;
//	}
//	return NULL;
//}

bool CPlayer::RandomMove()
{
	if ( !m_pMap )
		return false;

	if ( IsPlayerCurInCopy() )
	{
		D_DEBUG( "玩家%s在副本中,不能随机移动\n", GetNickName() );
		return false;
	}

	int errNum = 0;
	while( errNum < 20 )
	{
		int posX = RandNumber( 0, 511 );
		int posY = RandNumber( 0, 511 );
		if( m_pMap->IsGridCanPass( posX, posY ) )
		{
			D_DEBUG("玩家%s使用随机移动道具 移动到%d %d点", GetNickName(), posX, posY );
			return m_pMap->OnPlayerInnerJump( this, posX, posY );
		}
		else
			errNum++;
	}

	D_DEBUG("玩家%s随机移动,尝试了10次依然没有找到合适的随机点\n", GetNickName() );
	return false;
}

bool CPlayer::CheckAndSubItem( unsigned int itemid, unsigned int checkitemtype )
{
	//获取包裹中的道具
	int  itemindex =-1;
	bool isEquip  = false;
	ItemInfo_i* pItemInfo = GetItemInfoByItemUID( itemid,itemindex,isEquip );
	if ( itemindex == -1 || !pItemInfo )
		return false;

	if ( pItemInfo->usItemTypeID != checkitemtype )
		return false;

	if ( pItemInfo->ucCount >0 )
	{
		if ( pItemInfo->ucCount == 1 )
		{
			OnDropItem( itemid );
		}
		else
		{
			pItemInfo->ucCount--;
			if ( !isEquip )
				NoticePkgItemChange( itemindex );
			else
				NoticeEquipItemChange( itemindex );
		}
		return true;
	}
	return false;
}


bool CPlayer::OnPutItemToStorage( unsigned int itemUID )
{
	//获取包裹中的道具
	CBaseItem* pItem = GetItemDelegate().GetPkgItem( itemUID );
	if ( pItem )
	{
		GetItemDelegate().RemovePkgItem( itemUID );
		
		MGDropItemResult dropItemRst;
		dropItemRst.uiItemID = itemUID;
		dropItemRst.result   = true;
		SendPkg<MGDropItemResult>( &dropItemRst );

		delete pItem;
		pItem = NULL;

		return true;
	}

	return false;
}

bool CPlayer::OnGetItemFromStorage( const ItemInfo_i& itemInfo )
{
	if ( itemInfo.uiID == 0 || itemInfo.usItemTypeID == 0 )
	{
		D_ERROR("%s从仓库中取出的物品错误,无法创建对应的信息 %d  %d\n", GetNickName() ,itemInfo.uiID, itemInfo.usItemTypeID );
		return false;
	}

	CBaseItem* pItem = CItemBuilderSingle::instance()->CreateItem( itemInfo.usItemTypeID );
	if ( pItem )
	{
		TaskProperty* pTaskProp = CTaskPropertyManagerSingle::instance()->GetSpecialPropertyT( itemInfo.usItemTypeID );
		if ( pTaskProp )
			pItem->SetTaskID( pTaskProp->TaskID );

		//插入全局道具管理
		pItem->SetItemUID(   itemInfo.uiID );
		pItem->SetItemOwner( GetFullID() );
		pItem->SetItemLevel(GET_ITEM_LEVELUP(itemInfo.ucLevelUp));

		//如果是放在包裹中的道具，那么加入玩家的包裹管理器
		GetItemDelegate().PushPkgItem( pItem );

		return true;
	}

	return false;
}


ItemInfo_i* CPlayer::GetItemInfoByItemUID(unsigned int itemUID,int& itemIndex, bool& isEquip )
{
	FullPlayerInfo* fullPlayerInfo   = GetFullPlayerInfo();
	if ( !fullPlayerInfo )
		return NULL;

	itemIndex = -1;
	isEquip = false;

	PlayerInfo_3_Pkgs&   pkgInfo     = fullPlayerInfo->pkgInfo;//包裹
	for ( unsigned int i = 0 ; i< PACKAGE_SIZE;  ++i )
	{
		ItemInfo_i& tmpItem = pkgInfo.pkgs[i];
		if ( tmpItem.uiID == itemUID )
		{
			itemIndex = i;
			return &pkgInfo.pkgs[i];
		}
	}

	if ( itemIndex == -1 )
	{
		PlayerInfo_2_Equips& equipInfo   = fullPlayerInfo->equipInfo;//装备栏
		for ( unsigned int i = 0 ; i< EQUIP_SIZE; ++i )
		{
			ItemInfo_i& itemInfo = equipInfo.equips[i];
			if ( itemInfo.uiID == itemUID )
			{
				isEquip   = true;
				itemIndex = i;
				return &equipInfo.equips[i]; 
			}
		}
	}

	if(itemIndex == -1)
	{
		for(unsigned int i = 0; i < MAX_FASHION_COUNT; ++i)
		{
			ItemInfo_i& itemInfo = m_fashions.fashionData[i];
			if(itemInfo.uiID == itemUID)
			{
				itemIndex = i;
				return  &m_fashions.fashionData[i];
			}
		}
	}

	return NULL;
}

bool CPlayer::GetItemInfoByPkgIndex(unsigned int pkgIndex, ItemInfo_i& itemInfo_out, unsigned int& leftUpIndex_out )
{
	FullPlayerInfo* fullPlayerInfo   = GetFullPlayerInfo();
	if ( !fullPlayerInfo )
		return false;

	PlayerInfo_3_Pkgs&   pkgInfo     = fullPlayerInfo->pkgInfo;//包裹
	if (pkgIndex >= ARRAY_SIZE(pkgInfo.pkgs))
	{
		return false;
	}
	ItemInfo_i & itemInfo = pkgInfo.pkgs[pkgIndex];
	if (PlayerInfo_3_Pkgs::INVALIDATE == itemInfo.uiID)
	{
		itemInfo_out = itemInfo;
		leftUpIndex_out = pkgIndex;
		return true;
	}
	else
	{//找实体模型的左上角
		int index = pkgIndex;
		while (index - 1 >= 0)
		{
			index -= 1;
			if (pkgInfo.pkgs[index].uiID != itemInfo.uiID)
			{
				index += 1;
				break;
			}
		}
		while (index - PKG_COLUMN >= 0)
		{
			index -= PKG_COLUMN;
			if (pkgInfo.pkgs[index].uiID != itemInfo.uiID)
			{
				index += PKG_COLUMN;
				break;
			}
		}
		itemInfo_out = pkgInfo.pkgs[index];
		leftUpIndex_out = index;
		return true;
	}
	return false;
}

#define MASK_8_BIT 10000000
#define MASK_7_BIT 1000000

void GetCompareSignInfo( unsigned short& compareSign , unsigned int itemType )
{
	unsigned short stype = itemType / MASK_8_BIT;

	if (  stype == 16 )//时装
		compareSign |= 1<<15;
	else if( stype == 23 )//功能性道具
		compareSign |= 1<<2;
	else if( stype == 20 ||  stype == 21 )//消耗品
		compareSign = 1<<1;
	else if( stype == 22 )
		compareSign = 1;//价值物
	else if( stype == 38 )
		compareSign = 0;//任务物品
	else
	{
		unsigned short ltype = itemType / MASK_7_BIT;

		if ( ltype  == 110)//坐骑
			compareSign |= 1<<14;
		else if ( ltype == 102 || ltype == 902 )//头
			compareSign |= 1<<13;
		else if ( ltype == 104 || ltype == 904 )//肩
			compareSign |= 1<<12;
		else if ( ltype == 105 || ltype == 905 )//胸
			compareSign |= 1<<11;
		else if ( ltype == 106 || ltype == 906 )//手
			compareSign |= 1<<10;
		else if ( ltype == 107 || ltype == 907 )//鞋
			compareSign |= 1<<9;
		else if ( ltype == 108 || ltype == 908 )//披风
			compareSign |= 1<<8;
		else if ( ltype == 109 || ltype == 909 )//翅膀
			compareSign |= 1<<7;
		else if ( ltype == 141 || ltype == 941 )//项链
			compareSign |= 1<<6;
		else if ( ltype == 142 || ltype == 942 )//戒指
			compareSign |= 1<<5;
		else if ( ltype == 143 || ltype == 943 )//耳环
			compareSign |= 1<<4;
		else if ( ltype == 144 || ltype == 944 )//手镯 
			compareSign |= 1<<3;
	}	
}

bool CPlayer::CompareSortItem(const ItemInfo_i& itemInfo1 , const ItemInfo_i& itemInfo2 )
{
	if ( itemInfo1.usItemTypeID == 0 && itemInfo2.usItemTypeID == 0 )
		return false;

	if ( itemInfo1.usItemTypeID == 0 && itemInfo2.usItemTypeID != 0 )
		return true;

	if ( itemInfo1.usItemTypeID != 0 && itemInfo2.usItemTypeID == 0 )
		return false;

	unsigned short compareSign1 = 0;
	GetCompareSignInfo( compareSign1 , itemInfo1.usItemTypeID );

	unsigned short compareSign2 = 0;
	GetCompareSignInfo( compareSign2 , itemInfo2.usItemTypeID );

	if ( compareSign1 == compareSign2 )//如果类型一致，则依靠ID号判别
	{
		if ( ( itemInfo2.usItemTypeID / MASK_8_BIT ==  38 )
			&& ( itemInfo1.usItemTypeID / MASK_8_BIT == 38 ) )
		{
			return itemInfo2.usItemTypeID < itemInfo1.usItemTypeID;
		}
		else
			return itemInfo2.usItemTypeID > itemInfo1.usItemTypeID;
	}

	return compareSign2 > compareSign1;
}

bool CPlayer::IsPkgHave( unsigned int itemTypeID )
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::IsPkgHave, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& pkgInfp = playerInfo->pkgInfo;
	for ( int i = 0 ; i< PACKAGE_SIZE ; i ++ )
	{
		ItemInfo_i& itemInfo = pkgInfp.pkgs[i];
		if ( itemInfo.usItemTypeID  == itemTypeID )
			return true;
	}
	return false;
}

bool CPlayer::NpcCanGiveItem( unsigned int itemArr[], unsigned int arrSize )
{
	ItemInfo_i pkgItemInfo[PACKAGE_SIZE];

	//ACE_OS::memcpy( pkgItemInfo, GetFullPlayerInfo()->pkgInfo.pkgs, sizeof(ItemInfo_i)*PACKAGE_SIZE );

	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::NpcCanGiveItem, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	if ( sizeof(pkgItemInfo) == sizeof(playerInfo->pkgInfo.pkgs) )
	{
		StructMemCpy( pkgItemInfo, playerInfo->pkgInfo.pkgs, sizeof(pkgItemInfo) );
	} else {
		D_ERROR( "NpcCanGiveItem，包裹信息类型错\n" );
		return false;
	}

	//放入普通包裹的变量
	int changeTarget[PACKAGE_SIZE] = { -1 };
	unsigned int uiTargetChangedCount = 0;

	if (arrSize%2)
	{
		D_ERROR("CPlayer::NpcCanGiveItem, arrSize(%d)不应该是奇数\n",arrSize);
		return false;
	}
	for( unsigned int i = 0; i< arrSize; i+=2 )
	{
		unsigned int itemType = itemArr[i];//道具类型
		unsigned int itemCount= itemArr[i+1];//道具个数
		if ( itemType ==0 || itemCount == 0 )
			break;

		ItemInfo_i tmpInfo = { 0, 0, 0 , 0 , 0 , 0 };
		tmpInfo.usItemTypeID = itemType;
		tmpInfo.ucCount = itemCount;

		//判断道具是否需要放入任务背包,否则放入普通背包
		TaskProperty* pTaskProp = CTaskPropertyManagerSingle::instance()->GetSpecialPropertyT( itemType );
		if ( pTaskProp )//如果是任务道具,放入任务背包
		{
			if( !CheckItemCanAdd( tmpInfo, pkgItemInfo,  changeTarget , uiTargetChangedCount , NORMAL_PKG_SIZE, PACKAGE_SIZE ) ) 
				return false;
		}
		else
		{
			if( !CheckItemCanAdd( tmpInfo, pkgItemInfo,  changeTarget , uiTargetChangedCount , 0, NORMAL_PKG_SIZE ) ) 
				return false;
		}
	}

	return true;
}

bool CPlayer::NoticeTaskUiInfo( unsigned int infoNo )
{
	MGPlayerShowTaskUI srvmsg;
	srvmsg.eTaskUIInfo = (ETaskUIMsgType)infoNo;

	SendPkg<MGPlayerShowTaskUI>( &srvmsg );
	return true;
}

bool CPlayer::OnDropItemByType(unsigned int itemTypeID ,int itemCount )
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::OnDropItemByType, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& pkgInfp = playerInfo->pkgInfo;
	bool bCountinue = true;
	for ( int i = 0 ; i< PACKAGE_SIZE && bCountinue; i ++ )
	{
		if ( i >= ARRAY_SIZE(pkgInfp.pkgs) )
		{
			D_ERROR( "OnDropItemByType， 玩家%s, itemTypeID%d, i(%d) >= ARRAY_SIZE(pkgInfp.pkgs)\n"
				, GetAccount(), itemTypeID, i);
			break;
		}
		ItemInfo_i& itemInfo = pkgInfp.pkgs[i];
		if ( itemInfo.usItemTypeID == itemTypeID )
		{
			if ( itemInfo.ucCount > itemCount )
			{
				itemInfo.ucCount -= itemCount;
				NoticePkgItemChange( i );

				ItemInfo_i dropItemInfo = itemInfo;
				dropItemInfo.ucCount = itemCount;
				OnDropItemCounterEvent( dropItemInfo );
				bCountinue = false;
			}
			else
			{
				itemCount -= itemInfo.ucCount;
				OnDropItem( itemInfo.uiID );
				if ( itemCount <= 0 )
					bCountinue =  false;
			}
		}
	}
	return true;
}


//取走这个人身上所有该类型道具
bool CPlayer::OnDropAllByType( unsigned int itemTypeID )
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::OnDropAllByType, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& pkgInfp = playerInfo->pkgInfo;
	for ( int i = 0 ; i< PACKAGE_SIZE; i ++ )
	{
		ItemInfo_i& itemInfo = pkgInfp.pkgs[i];
		if ( itemInfo.usItemTypeID == itemTypeID )
		{
			OnDropItem( itemInfo.uiID );
		}
	}
	return true;
}



int CPlayer::OnGetItemCountByConditionID( unsigned int conditionID )
{
	int itemCount = 0;
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::OnGetItemCountByConditionID, NULL == GetFullPlayerInfo()\n");
		return 0;
	}
	for ( int index = 0 ; index< PACKAGE_SIZE; index++  )//找到背包中的同类型的道具
	{
		if ( index >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs) )
		{
			D_ERROR( "OnGetItemCountByConditionID, 玩家%s，conditionID%d, index(%d) >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs)\n"
				, GetAccount(), conditionID, index );
			break;
		}
		const ItemInfo_i& itemInfo = playerInfo->pkgInfo.pkgs[index];
		if ( ( itemInfo.usItemTypeID / 10  ) == conditionID )
		{
			if (IsLeftUpPkgIndex(index,itemInfo.uiID))
			{
				itemCount+= itemInfo.ucCount;
			}
		}
	}
	return itemCount;
}


bool CPlayer::OnDropItemByConditionID( unsigned int conditionType, int itemCount )
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::OnDropItemByConditionID, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& pkgInfp = playerInfo->pkgInfo;
	bool bCountinue = true;
	for ( int i = 0 ; i< PACKAGE_SIZE && bCountinue; i ++ )
	{
		if ( i >= ARRAY_SIZE(pkgInfp.pkgs) )
		{
			D_ERROR( "OnDropItemByConditionID， 玩家%s, conditionTypeID%d, i(%d) >= ARRAY_SIZE(pkgInfp.pkgs)\n"
				, GetAccount(), conditionType, i);
			break;
		}
		ItemInfo_i& itemInfo = pkgInfp.pkgs[i];
		if ( ( itemInfo.usItemTypeID / 10 ) == conditionType )
		{
			if ( itemInfo.ucCount > itemCount )
			{
				itemInfo.ucCount -= itemCount;
				NoticePkgItemChange( i );

				ItemInfo_i dropItemInfo = itemInfo;
				dropItemInfo.ucCount = itemCount;
				OnDropItemCounterEvent( dropItemInfo );
				bCountinue = false;
			}
			else
			{
				itemCount -= itemInfo.ucCount;
				OnDropItem( itemInfo.uiID );
				if ( itemCount <= 0 )
					bCountinue =  false;
			}
		}
	}
	return true;
}

bool CPlayer::CanGetItem(unsigned int itemTypeID, unsigned int itemCount )
{
	ItemInfo_i tempInfo = { 0 , 0 , 0 , 0 , 0 , 0 };
	tempInfo.usItemTypeID = itemTypeID;
	tempInfo.ucCount  = itemCount;
	ItemInfo_i pkgs[PACKAGE_SIZE];
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::CanGetItem, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	StructMemCpy( pkgs, playerInfo->pkgInfo.pkgs, sizeof(ItemInfo_i)*PACKAGE_SIZE );

	int changeArr[PACKAGE_SIZE] = {-1};
	unsigned int changeCount = 0;

	return CheckItemCanAdd( tempInfo, pkgs, changeArr, changeCount , 0 , NORMAL_PKG_SIZE );
}

int CPlayer::GetFreePkgIndex(int type/*0普通背包，1任务背包，2全部背包*/,unsigned int width /* = 1 */, unsigned int height /* = 1 */)
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::GetFreePkgIndex, NULL == GetFullPlayerInfo()\n");
		return -1;
	}
	int freeIndex = -1;
	int begin = 0 , end = 0;
	switch(type)
	{
	case 0:
		begin = 0;
		end = NORMAL_PKG_SIZE;
		break;
	case 1:
		begin = NORMAL_PKG_SIZE;
		end = PACKAGE_SIZE;
		break;
	case 2:
		{
			for (int i = 0; i<2; ++i)
			{
				freeIndex = GetFreePkgIndex(i,width,height);
				if (freeIndex != -1)
				{
					return freeIndex;
				}
			}
			return freeIndex;
		}
		break;
	default:
		D_ERROR("CPlayer::GetFreePkgIndex type(%d)\n",type);
		return -1;
	}
	for (int i = begin; i<end; ++i)
	{
		if (PlayerInfo_2_Equips::INVALIDATE == playerInfo->pkgInfo.pkgs[i].uiID)
		{
			if (i/PKG_COLUMN != (i+width-1)/PKG_COLUMN)
			{//道具宽度超出改点至背包最右边的宽度
				continue;
			}
			bool isFree = true;
			for (unsigned int w = 0; w<width; ++w)
			{
				for (unsigned int h = 0; h<height; ++h)
				{
					unsigned int index = i+w+h*PKG_COLUMN;
					if (index < ARRAY_SIZE(playerInfo->pkgInfo.pkgs))
					{
						if (PlayerInfo_2_Equips::INVALIDATE != playerInfo->pkgInfo.pkgs[index].uiID)
						{
							isFree = false;
							break;
						}
					}
					else
					{
						isFree = false;
						break;
					}
				}
				if (!isFree)
				{
					break;
				}
			}
			if (isFree)
			{
				freeIndex = i;
				break;
			}
		}
	}
	return freeIndex;
}

//清除背包中该uid的道具
void CPlayer::ClearPkgItem( unsigned int itemUID )
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::ClearPkgItem, NULL == GetFullPlayerInfo()\n");
		return;
	}
	if (PlayerInfo_3_Pkgs::INVALIDATE == itemUID)
	{
		return;
	}
	for ( int i = 0; i<ARRAY_SIZE(playerInfo->pkgInfo.pkgs); ++i)
	{
		if (playerInfo->pkgInfo.pkgs[i].uiID == itemUID)
		{
			ItemInfo_i& itemInfo = playerInfo->pkgInfo.pkgs[i];

			itemInfo.uiID = PlayerInfo_3_Pkgs::INVALIDATE;
			itemInfo.usItemTypeID = 0;
			itemInfo.usWearPoint = 0;
			itemInfo.randSet/*usAddId*/ = 0;
			itemInfo.ucLevelUp = 0;
			itemInfo.ucCount = 0;
			NoticePlayerItemUpdate(false,itemInfo,i);
		}
	}
}

//清除背包中一定范围的道具
void CPlayer::ClearPkgItem(unsigned int begin, unsigned int end)
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::ClearPkgItem, NULL == GetFullPlayerInfo()\n");
		return;
	}
	if (begin > end)
	{
		return;
	}
	if (end >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs))
	{
		return;
	}
	for (unsigned int i = begin; i<end; ++i)
	{
		if (PlayerInfo_3_Pkgs::INVALIDATE != playerInfo->pkgInfo.pkgs[i].uiID)
		{
			ItemInfo_i& itemInfo = playerInfo->pkgInfo.pkgs[i];

			itemInfo.uiID = PlayerInfo_3_Pkgs::INVALIDATE;
			itemInfo.usItemTypeID = 0;
			itemInfo.usWearPoint = 0;
			itemInfo.randSet/*usAddId*/ = 0;
			itemInfo.ucLevelUp = 0;
			itemInfo.ucCount = 0;
			NoticePlayerItemUpdate(false,itemInfo,i);
		}
	}
}

//填充背包中的道具
bool CPlayer::SetPkgItem( const ItemInfo_i& itemInfo, unsigned int pkgIndex )
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::SetPkgItem, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	if (pkgIndex >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs))
	{
		D_ERROR("CPlayer::SetPkgItem pkgIndex(%d) >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs)\n",pkgIndex);
		return false;
	}
	if ( PlayerInfo_3_Pkgs::INVALIDATE == itemInfo.uiID )
	{
		playerInfo->pkgInfo.pkgs[pkgIndex] = itemInfo;
		NoticePlayerItemUpdate(false,itemInfo,pkgIndex);
		return true;
	}
	CBaseItem* pItem = GetItemDelegate().GetItemByItemUID( itemInfo.uiID );
	if (NULL == pItem)
	{
		D_ERROR("CPlayer::SetPkgItem NULL == pItem\n");
		return false;
	}
	unsigned int width = pItem->GetWidth();
	unsigned int height = pItem->GetHeight();
	for ( unsigned int w = 0; w < width; ++w )
	{
		for ( unsigned int h = 0; h < height; ++h )
		{
			unsigned int index = pkgIndex + w + h*PKG_COLUMN;
			if (index < ARRAY_SIZE(playerInfo->pkgInfo.pkgs))
			{
				playerInfo->pkgInfo.pkgs[index] = itemInfo;
				NoticePlayerItemUpdate(false,itemInfo,index);
			}
		}
	}
	return true;
}

//背包位置是否空
bool CPlayer::IsFreePkgIndex(unsigned int pkgIndex, unsigned int width /* = 1 */, unsigned int height /* = 1 */)
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::IsFreePkgIndex, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	unsigned int MaxIndex = 0;
	if (pkgIndex < NORMAL_PKG_SIZE)
	{
		MaxIndex = NORMAL_PKG_SIZE;
	}
	else
	{
		MaxIndex = ARRAY_SIZE(playerInfo->pkgInfo.pkgs);
	}
	if (pkgIndex/PKG_COLUMN != (pkgIndex+width-1)/PKG_COLUMN)
	{//道具宽度超出改点至背包最右边的宽度
		return false;
	}
	for (unsigned int w = 0; w<width; ++w)
	{
		for (unsigned int h = 0; h<height; ++h)
		{
			unsigned index = pkgIndex+w+h*PKG_COLUMN;
			if (index < MaxIndex)
			{
				if (PlayerInfo_2_Equips::INVALIDATE != playerInfo->pkgInfo.pkgs[index].uiID)
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	return true;
}

//该道具位置是否位于左上角
bool CPlayer::IsLeftUpPkgIndex(unsigned int pkgIndex, unsigned int itemUID)
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::IsLeftUpPkgIndex, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	
	PlayerInfo_3_Pkgs& pkgInfoArr = playerInfo->pkgInfo;
	if (pkgIndex >= ARRAY_SIZE(pkgInfoArr.pkgs))
	{
		D_ERROR("CPlayer::IsLeftUpPkgIndex pkgIndex(%d) >= ARRAY_SIZE(pkgInfoArr.pkgs)\n",pkgIndex);
		return false;
	}

	if ( PlayerInfo_3_Pkgs::INVALIDATE == pkgInfoArr.pkgs[pkgIndex].uiID )
	{
		return true;
	}

	if (pkgIndex>=1)
	{
		if (pkgInfoArr.pkgs[pkgIndex-1].uiID == pkgInfoArr.pkgs[pkgIndex].uiID)
		{
			return false;
		}
	}
	if (pkgIndex>=PKG_COLUMN)
	{
		if (pkgInfoArr.pkgs[pkgIndex-PKG_COLUMN].uiID == pkgInfoArr.pkgs[pkgIndex].uiID)
		{
			return false;
		}
	}
	return true;
}

//玩家获得新道具
bool CPlayer::GetNormalItem( unsigned int itemTypeID, unsigned int itemCount , unsigned int itemLevel )
{
	return InnerGetItem( itemTypeID, itemCount, itemLevel, true/*new rand*/, 0, 0, 0 );//需要重新随机属性
}

//给玩家邮件物品
bool CPlayer::GetMailItem( unsigned int itemTypeID, unsigned int itemCount, unsigned int itemLevel, unsigned int randSet, unsigned int luckySeed, unsigned int excelSeed )
{
	return InnerGetItem( itemTypeID, itemCount, itemLevel, false/*no new rand*/, randSet, luckySeed, excelSeed );//不需要重新随机属性
}

//给玩家物品
bool CPlayer::InnerGetItem( unsigned int itemTypeID, unsigned int itemCount , unsigned int itemLevel, bool isNewRand, unsigned int randSet, unsigned int luckySeed, unsigned int excelSeed )
{
	//该道具是否为合法道具
	CItemPublicProp* pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
	if ( !pItemPublicProp )
		return false;

	unsigned int taskID = 0;
	TaskProperty* pTaskProp = CTaskPropertyManagerSingle::instance()->GetSpecialPropertyT( itemTypeID );
	if ( pTaskProp )
	{
		taskID = pTaskProp->TaskID;
	}

	int errNo = 0;
	unsigned int maxAccount = pItemPublicProp->m_itemMaxAmount;
	if ( 0 == maxAccount )
	{
		D_ERROR( "CPlayer::InnerGetItem, 0 == maxAccount\n" );
		return false;
	}

	if ( 1 == maxAccount )
	{
		for ( unsigned int i = 0 ; i< itemCount ; i++ )
		{
			CItemPkgEle itemEle;
			//by dzj, 10.05.20, itemEle.SetItemEleProp( itemTypeID, 1, taskID , itemLevel, itemaddid, randSet );
			if ( isNewRand )
			{				
				itemEle.SetItemPropNewRand( itemTypeID, 1, taskID , itemLevel );
			} else {
				itemEle.SetItemPropExistRand( itemTypeID, 1, taskID , itemLevel, randSet, luckySeed, excelSeed );
			}
			
			if ( !GiveNewItemToPlayer( itemEle.GetItemEleInfo(), itemEle.GetTaskID(), errNo) )
			{
				return false;
			}
		}
	} else { //if ( 1 == maxAccount )
		unsigned int  cNum = itemCount/maxAccount;
		unsigned int  leftNum = itemCount%maxAccount;
		for ( unsigned int i = 0 ; i< cNum; i++ )
		{
			CItemPkgEle itemEle;
			//by dzj, 10.05.20, itemEle.SetItemEleProp( itemTypeID, maxAccount, taskID );
			if ( isNewRand )
			{				
				itemEle.SetItemPropNewRand( itemTypeID, maxAccount, taskID , itemLevel );
			} else {
				itemEle.SetItemPropExistRand( itemTypeID, maxAccount, taskID , itemLevel, randSet, luckySeed, excelSeed );
			}

			if ( !GiveNewItemToPlayer( itemEle.GetItemEleInfo(), itemEle.GetTaskID(), errNo ) )
			{
				return false;
			}
		}

		if ( leftNum != 0 )
		{
			CItemPkgEle itemEle;
			//by dzj, 10.05.20, itemEle.SetItemEleProp( itemTypeID, leftNum, taskID );
			if ( isNewRand )
			{				
				itemEle.SetItemPropNewRand( itemTypeID, leftNum, taskID , itemLevel );
			} else {
				itemEle.SetItemPropExistRand( itemTypeID, leftNum, taskID , itemLevel, randSet, luckySeed, excelSeed );
			}

			if ( !GiveNewItemToPlayer( itemEle.GetItemEleInfo(), itemEle.GetTaskID(),errNo ) )
			{
				return false;
			}
		}
	} //if ( 1 == maxAccount )
	return true;
}

//bool CPlayer::GetNewItem( unsigned int itemTypeID, unsigned int itemCount , unsigned int itemLevel /* = 0 */, unsigned int itemaddid /* = 0  */, unsigned int randSet )
//{
//	//该道具是否为合法道具
//	CItemPublicProp* pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
//	if ( !pItemPublicProp )
//		return false;
//
//	unsigned int taskID = 0;
//	TaskProperty* pTaskProp = CTaskPropertyManagerSingle::instance()->GetSpecialPropertyT( itemTypeID );
//	if ( pTaskProp )
//		taskID = pTaskProp->TaskID;
//
//	int errNo = 0;
//	unsigned int maxAccount = pItemPublicProp->m_itemMaxAmount;
//	if ( maxAccount ==  0 )
//		return false;
//
//	if ( maxAccount == 1 )
//	{
//		for ( unsigned int i = 0 ; i< itemCount ; i++ )
//		{
//			CItemPkgEle itemEle;
//			itemEle.SetItemEleProp( itemTypeID, 1, taskID , itemLevel, itemaddid, randSet );
//			if ( !GiveNewItemToPlayer( itemEle.GetItemEleInfo(), itemEle.GetTaskID(), errNo) )
//				return false;
//		}
//	}
//	else
//	{
//		unsigned int  cNum = itemCount/maxAccount;
//		unsigned int  leftNum = itemCount%maxAccount;
//		for ( unsigned int i = 0 ; i< cNum; i++ )
//		{
//			CItemPkgEle itemEle;
//			itemEle.SetItemEleProp( itemTypeID, maxAccount, taskID );
//			if ( !GiveNewItemToPlayer( itemEle.GetItemEleInfo(), itemEle.GetTaskID(), errNo ) )
//				return false;
//		}
//
//		if ( leftNum != 0 )
//		{
//			CItemPkgEle itemEle;
//			itemEle.SetItemEleProp( itemTypeID, leftNum, taskID );
//			if ( !GiveNewItemToPlayer( itemEle.GetItemEleInfo(), itemEle.GetTaskID(),errNo ) )
//				return false;
//		}
//	}
//	return true;
//}

bool CPlayer::ItemNumReachTargetNum(unsigned int itemTypeID, unsigned int targerNum)
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::ItemNumReachTargetNum, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	for ( int index = 0 ; index< PACKAGE_SIZE && targerNum> 0; index++  )
	{
		if ( index >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs) )
		{
			D_ERROR( "ItemNumReachTargetNum, 玩家%s, itemTypeID%d, index(%d)越界\n"
				, GetAccount(), itemTypeID, index );
			break;
		}
		ItemInfo_i& itemInfo = playerInfo->pkgInfo.pkgs[index];
		if ( ( itemInfo.uiID!= PlayerInfo_3_Pkgs::INVALIDATE ) && 
			 ( ( itemInfo.usItemTypeID/10 ) == itemTypeID )
			 )
		{
			if ( (unsigned int)itemInfo.ucCount >= targerNum )
				targerNum = 0;
			else
				targerNum -= itemInfo.ucCount;
		}
	}

	return  targerNum > 0 ?false:true;
}

bool CPlayer::CheckItemValidByPkgPos(unsigned int itemTypeID, unsigned int itemCount, unsigned int itemPos, unsigned int& itemUID )
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::CheckItemValidByPkgPos, NULL == GetFullPlayerInfo()\n");
		return false;
	}

	if ( itemPos >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs) )
	{
		D_ERROR( "CheckItemValidByPkgPos, itemPos(%d) >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs)\n", itemPos );
		return false;
	}

	 ItemInfo_i& itemInfo = playerInfo->pkgInfo.pkgs[itemPos];
	 if ( itemInfo.usItemTypeID == itemTypeID 
		 && (unsigned int)itemInfo.ucCount >= itemCount )
	 {
		 itemUID = itemInfo.uiID;
		 return true;
	 }

	 return false;
}

bool CPlayer::SubItemCountByPkgPos(unsigned int itemPos, unsigned int itemTypeID, unsigned int itemCount )
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::SubItemCountByPkgPos, NULL == GetFullPlayerInfo()\n");
		return false;
	}

	if ( itemPos >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs) )
	{
		D_ERROR( "SubItemCountByPkgPos, itemPos(%d) >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs)\n", itemPos );
		return false;
	}

	ItemInfo_i& itemInfo = playerInfo->pkgInfo.pkgs[itemPos];
	if ( itemInfo.usItemTypeID != itemTypeID )
		return false;

	if ( (unsigned int)itemInfo.ucCount < itemCount )
		return false;

	itemInfo.ucCount-= itemCount;
	
	//如果道具没有了,则扔掉该道具
	if ( itemInfo.ucCount == 0)
		OnDropItem( itemInfo.uiID );
	else
		NoticePkgItemChange( itemPos);

	return true;
}

unsigned int CPlayer::QueryItemPrice(unsigned int itemTypeID )
{
	CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( itemTypeID );
	if ( pPublicProp )
	{
		return pPublicProp->m_itemPrice;
	}

	return 0;
}


//每次移动检测是否处于骑乘状态
bool CPlayer::CheckIsRideStateMove()
{
	TRY_BEGIN;

	//CRideState& rideState = GetSelfStateManager().GetRideState();
	////仅当骑乘状态激活时，且玩家时队长时
	//if ( rideState.IsActive() &&  rideState.GetPlayerRideType() == CRideState::E_LEADER )
	//{
	//	RideTeam* pTeam = rideState.GetRideTeam();
	//	if ( !pTeam)
	//	{
	//		D_ERROR("出错,处于骑乘状态但是竟然找不到骑乘队伍指针\n");
	//		return false;
	//	}
	//	pTeam->ChangeTeamMemberPos();
	//}

	return false;
	TRY_END;
	return false;
}


//void CPlayer::OnDeletePlayerFromBlock( MuxMapBlock* pMapBlcok )
//{
//	if ( !pMapBlcok )
//		return;
//
//	CRideState& rideState = GetSelfStateManager().GetRideState();
//	//仅当骑乘状态激活时，且玩家时队长时
//	if ( rideState.IsActive() &&  rideState.GetPlayerRideType() == CRideState::E_LEADER )
//	{
//		RideTeam* pTeam = rideState.GetRideTeam();
//		if ( !pTeam)
//		{
//			D_ERROR("出错,处于骑乘状态但是竟然找不到骑乘队伍指针\n");
//			return;
//		}
//
//		if ( pTeam->IsTeamFull() )
//		{
//			CPlayer* pMember = pTeam->GetRideMember();
//			if ( pMember )
//				pMapBlcok->DeletePlayerFromBlock( pMember );
//		}
//		pMapBlcok->DeleteRideTeam( pTeam );
//	}
//}

//void CPlayer::OnAddPlayerToNewBlcok(MuxMapBlock* pMapBlcok )
//{
//	if ( !pMapBlcok )
//		return;
//
//	CRideState& rideState = GetSelfStateManager().GetRideState();
//	//仅当骑乘状态激活时，且玩家时队长时
//	if ( rideState.IsActive() &&  rideState.GetPlayerRideType() == CRideState::E_LEADER )
//	{
//		RideTeam* pTeam = rideState.GetRideTeam();
//		if ( !pTeam)
//		{
//			D_ERROR("出错,处于骑乘状态但是竟然找不到骑乘队伍指针\n");
//			return;
//		}
//
//		if ( pTeam->IsTeamFull() )
//		{
//			CPlayer* pMember = pTeam->GetRideMember();
//			if ( pMember )
//				pMapBlcok->AddPlayerToBlock( pMember );
//		}
//		pMapBlcok->AddRideTeam( pTeam );
//	}
//}

//void CPlayer::OnMoveOutOrgBlockNotify(MuxMapBlock *pMapBlock)
//{
//	if ( !pMapBlock )
//		return;
//
//	CRideState& rideState = GetSelfStateManager().GetRideState();
//	//仅当骑乘状态激活时，且玩家时队长时
//	if ( rideState.IsActive() &&  rideState.GetPlayerRideType() == CRideState::E_LEADER )
//	{
//		RideTeam* pTeam = rideState.GetRideTeam();
//		if ( !pTeam)
//		{
//			D_ERROR("出错,处于骑乘状态但是竟然找不到骑乘队伍指针\n");
//			return;
//		}
//
//		CPlayer* pMember = pTeam->GetRideMember();
//		if ( pMember )
//			pMapBlock->PlayerMoveOrgBlockNotify( pMember );
//	}
//}

void CPlayer::OnEnterNewMapSetRideState( ERIDESTATE eRideState ,long attachInfo )
{
	//如果设置的状态正确
	if ( eRideState != E_RIDESTATE_MAX && eRideState != E_UNRIDE  )
	{
		CPlayerStateManager& stateMan =GetSelfStateManager();

		//设置骑乘道具的状态,是道具还剩装备
		stateMan.GetRideState().SetRideItemState(eRideState);
		switch( eRideState )
		{
			//如果是消耗品
		case E_RIDE_CONSUME:
			{
				stateMan.GetRideState().SetRideItemType( attachInfo );
				stateMan.SetPlayerRideLeaderState();
			}
			break;
			//如果是装备
		case E_RIDE_EQUIP:
			{
				stateMan.SetPlayerRideLeaderState();
			}
			break;
		default:
			break;
		}

		//如果已经在副本下马区域了
		if ( stateMan.IsInDunGeonPreArea() )
		{
			RideTeam* pTeam = stateMan.GetRideState().GetRideTeam();
			if ( pTeam )
			{
				pTeam->DissolveRideTeam();
				RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam ); 
			}
		}
	}
}

void CPlayer::NoticePkgItemChange(unsigned int index )
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::NoticePkgItemChange, NULL == GetFullPlayerInfo()\n");
		return;
	}

	if ( index >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs) )
	{
		D_ERROR("CPlayer::NoticePkgItemChange, index(%d) >= ARRAY_SIZE(playerInfo->pkgInfo.pkgs)\n", index);
		return;
	}

	ItemInfo_i& pkgItem = playerInfo->pkgInfo.pkgs[index];

	if ( pkgItem.ucCount < 0  )
	{
		D_ERROR("玩家%s道具更新错误,更新位置%d\n", GetNickName(), index );
		return;
	}

	if ( !IsLeftUpPkgIndex(index,pkgItem.uiID) )
	{//只通知客户端道具实体模型的左上角

		//非左上角的位置在玩家看来是空的
		MGSetPkgItem setpkgItem;
		StructMemSet(setpkgItem.itemInfo,0,sizeof(setpkgItem.itemInfo));;
		setpkgItem.pkgIndex = index;
		SendPkg<MGSetPkgItem>( &setpkgItem );

		ItemInfo_i item;
		unsigned int pkgIndex = index;
		GetItemInfoByPkgIndex(pkgIndex,item,index);
	}

	MGSetPkgItem setpkgItem;
	setpkgItem.itemInfo = pkgItem;
	setpkgItem.pkgIndex = index;
	SendPkg<MGSetPkgItem>( &setpkgItem );

	NoticePlayerItemUpdate( false, pkgItem, index);
}


void CPlayer::NoticeEquipItemChange(unsigned int equipIndex )
{

	CNewMap* pCurMap = GetCurMap();
	if ( NULL == pCurMap )
	{
		D_ERROR( "CPlayer::NoticeEquipItemChange, NULL == pCurMap\n" );
		return;
	}

	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(装备，时装)
	
	ItemManagerSingle::instance()->CheckPlayerEquipItem(this);

	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::NoticeEquipItemChange, NULL == GetFullPlayerInfo()\n");
		return;
	}
	if ( equipIndex < EQUIP_SIZE )
	{
		ItemInfo_i& equipItem = playerInfo->equipInfo.equips[equipIndex];
		MGEquipItemUpdate equipItemUpdate;
		equipItemUpdate.equipIndex = equipIndex;
		equipItemUpdate.newEquipItem = equipItem;
		equipItemUpdate.changePlayerID = GetFullID();
		pCurMap->NotifySurrounding<MGEquipItemUpdate>( &equipItemUpdate, GetPosX() , GetPosY() );

		NoticePlayerItemUpdate( true, equipItem, equipIndex );
	}
	else
	{
		PlayerInfo_Fashions& playerFashion  = Fashions();
		int fashionIndex = equipIndex - EQUIP_SIZE;
		if ( fashionIndex >= MAX_FASHION_COUNT)
			return;

		MGEquipItemUpdate fashionUpdate;
		fashionUpdate.equipIndex     = equipIndex;
		fashionUpdate.changePlayerID = GetFullID();
		fashionUpdate.newEquipItem   = playerFashion.fashionData[fashionIndex];
		pCurMap->NotifySurrounding<MGEquipItemUpdate>( &fashionUpdate, GetPosX() , GetPosY() );

		MGDbSaveInfo saveInfo;
		FillPlayerFashionInfo( saveInfo );
		ForceGateSend<MGDbSaveInfo>( &saveInfo );
	}
}

//是否正在升级，追加，改造道具
bool CPlayer::IsPlayerForgingItem()
{
	if ( mForgingItem != 0 )
		return true;
	
	return false;
}

void CPlayer::OnRideLeaderSwitchMap( const GMRideLeaderSwitchMap* pMsg )
{
	if ( !pMsg )
		return;

	MGOnRideLeaderSwichMap swichMapMsg;
	swichMapMsg.iPosX = pMsg->iPosX;
	swichMapMsg.iPosY = pMsg->iPosY;
	swichMapMsg.usMapID = pMsg->usMapID;
	swichMapMsg.oldMapID = pMsg->oldMapID;

	this->SendPkg<MGOnRideLeaderSwichMap>(&swichMapMsg);
}

void CPlayer::OnKillMonsterAddExp( unsigned long expre, unsigned int monsterLevel, bool isBossMonster, unsigned long monsterClsID  )
{
	/*
	from 季显武：
	>设定：p0.level=a,m1.level=b,m1.exp=c,g
	>--------
	>if(a<=b) then              
	>y=c                        
	>elseif (a-b<10) then       
	>y=c*(1-(a-b)/10)           
	>else                       
	>y=c*1/100                  
	>end                    
	*/
	unsigned int playerLevel = GetLevel();
	if ( playerLevel > monsterLevel )
	{
		//playerLevel > monsterLevel
		unsigned int levDis = playerLevel - monsterLevel;
		if ( levDis < 10  )
		{
			//playerLevel超过怪物一点
			float tmpf = (float)(1 - levDis/10.0f);
			expre = (unsigned int)(expre*tmpf);
		} else {
			//playerLevel超过怪物太多
			expre = (unsigned int)(expre*0.01);
		}
	}

	//幸运系统加经验值,如果幸运暴击通过幸运系数增加经验值
	bool isExpCritical = false;
	if( RandRate( CSpSkillManager::GetProLuck() / 100.f ) )
	{
		float coe = 1 + CSpSkillManager::GetCoeLuck()/100.f;
		expre = (unsigned long)(expre * coe);
		isExpCritical = true;
	}

	//如果组队状态
	if ( GetSelfStateManager().IsGroupState())
	{
		CGroupTeam* pTeam = GetSelfStateManager().GetGroupState().GetGroupTeam();
		//assert( pTeam );
		if ( NULL == pTeam )
		{
			D_ERROR( "CPlayer::OnKillMonsterAddExp, NULL == pTeam\n" );
			return;
		}

		//启动组队经验规则
		if ( pTeam )
			pTeam->ShareExpreOnGroupTeam( this, expre, monsterLevel, isExpCritical, isBossMonster );
	}
	else
	{
		//新加经验公式后，5级经验0去除，by dzj, 10.06.21;//unsigned int level = GetLevel();
		//if ( level > monsterLevel &&  ( ( level - monsterLevel) > 5 ) )
		//	return;

		AddExp( expre, GCPlayerLevelUp::IN_BATTLE, isExpCritical );

		//增加经验会影响好友收益及好友度
		AddExpAffectFriendGain(expre, isBossMonster);
	}

	//击杀的是boss，添加好友动态
	if(isBossMonster)
		AddFriendTweet(MUX_PROTO::TWEET_TYPE_KILL_BOSS, (const void *)&monsterClsID, sizeof(unsigned long));

	return;
}


bool CPlayer::GiveNewItemToPlayer(const ItemInfo_i& newItem ,unsigned taskID ,int& errNo,int pkgIndex )
{
	errNo = 0;

	//该道具是否为合法道具
	CItemPublicProp* pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( newItem.usItemTypeID );
	if ( !pItemPublicProp )
	{
		errNo = E_PKG_ERROR;
		return false;
	}

	//计算时间型耐久度(上线时间作为基值)
	if(ACE_BIT_ENABLED(pItemPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY))
	{
		ACE_Time_Value onlineTV = ACE_Time_Value(m_fullPlayerInfo.stateInfo.GetOnlineTick());
		ACE_Time_Value nowTV = ACE_OS::gettimeofday();
		const_cast<ItemInfo_i &>(newItem).usWearPoint = (unsigned short)(nowTV.sec() - onlineTV.sec() + pItemPublicProp->m_maxWear * 60);
	}

	//判断是否是任务物品
	bool isTaskItem = taskID > 0 ?true:false;
	unsigned int pkgBegin = 0;
	unsigned int pkgEnd = NORMAL_PKG_SIZE;
	if ( isTaskItem )
	{
		pkgBegin = NORMAL_PKG_SIZE;//从第四页开始
		pkgEnd   = PACKAGE_SIZE;//到最后一个
	}

	//获取包裹引用
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::GiveNewItemToPlayer, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& itemPkgs = playerInfo->pkgInfo;
	//获取该物品可堆叠的最大个数
	unsigned int itemMaxCount = pItemPublicProp->m_itemMaxAmount;

	//如果为不可堆叠的物品
	if ( itemMaxCount  == 1 )
	{
		//如果包裹满了
		if( IsItemPkgFull() )
		{
			errNo = E_PKG_FULL;
			return false;
		}
		if ( pkgIndex == -1 )
		{//由服务器来找寻一个空位
			int type = isTaskItem ? 1 : 0;
			pkgIndex = GetFreePkgIndex(type,CBaseItem::GetWidth(newItem.usItemTypeID),CBaseItem::GetHeight(newItem.usItemTypeID));
			if ( -1 == pkgIndex )
			{
				errNo = E_PKG_FULL;
				return false;
			}
		}
		else
		{
			if (isTaskItem)
			{
				if (pkgIndex < NORMAL_PKG_SIZE)
				{
					errNo = E_PKG_ERROR;
					return false;
				}
			}
			else
			{
				if (pkgIndex >= NORMAL_PKG_SIZE)
				{
					errNo = E_PKG_ERROR;
					return false;
				}
			}
			if (!IsFreePkgIndex(pkgIndex,CBaseItem::GetWidth(newItem.usItemTypeID),CBaseItem::GetHeight(newItem.usItemTypeID)))
			{
				errNo = E_PKG_FULL;
				return false;
			}
		}

		//将该道具放进去
		ItemInfo_i itemInfo = newItem;
		if( CreateNewItem( itemInfo, pkgIndex, taskID ) == NULL )
		{
			errNo = E_PKG_ERROR;
			return false;
		}
		else
		{
			TryCounterEvent( CT_COLLECT, newItem.usItemTypeID , newItem.ucCount );
			GetNewItemNoticeOtherTeamMember( newItem );					
			return true;
		}

		////如果包裹没有空位放入道具
		//if ( index == pkgEnd )
		//{
		//	errNo = E_PKG_FULL;
		//	return false;
		//}
	}
	//如果是可堆叠物品
	else
	{		
		unsigned int fullPkgIndexArr[PACKAGE_SIZE] = {0};
		unsigned int fullPkgCountArr[PACKAGE_SIZE] = {0};
		unsigned int fullCount = 0;
		unsigned itemNum = newItem.ucCount;//本次所添加的道具数目
		int emptyIndex = -1;//标志位
		for ( unsigned int index = pkgBegin ; index < pkgEnd && itemNum > 0 ; ++ index  )
		{
			ItemInfo_i& itemInfo = itemPkgs.pkgs[index];
			if ( itemInfo.uiID != PlayerInfo_3_Pkgs::INVALIDATE )
			{
				//并且是同种类的，耐久度是一致的，并且该道具没有达到最大的堆叠个数
				if( itemInfo.usItemTypeID   == newItem.usItemTypeID 
					&& itemInfo.usWearPoint == newItem.usWearPoint
					&& itemInfo.excellentid == newItem.excellentid
					&& itemInfo.luckyid     == newItem.luckyid 
					&& (unsigned int)itemInfo.ucCount < itemMaxCount 
					&& IsLeftUpPkgIndex(index,newItem.uiID))
				{
					unsigned int subCount = itemMaxCount - itemInfo.ucCount;
					fullPkgIndexArr[index] = index;
					if ( subCount > itemNum )//如果剩余的个数>添加的个数
					{
						fullPkgCountArr[index] = ( itemInfo.ucCount + itemNum );
						itemNum = 0;
					}
					else//如果添加的个数大于剩余的个数
					{
						fullPkgCountArr[index] = itemMaxCount;
						itemNum -= subCount;
					}
					fullCount++;
				}
			}
			else if(  emptyIndex == -1 )//找到包裹中第一个空的位置
			{
				if (IsFreePkgIndex(index,CBaseItem::GetWidth(itemInfo.usItemTypeID),CBaseItem::GetHeight(itemInfo.usItemTypeID)))
				{
					emptyIndex = index;
				}
			}
		}

		//如果遍历了所有可堆叠的同类物品，而仍然有空的剩余物品无法堆叠，则找到第一个空位，将剩余道具放进去
		if ( itemNum > 0 )
		{
			//如果还有空闲的位置提供装备该物品
			if ( emptyIndex > -1 && emptyIndex < ARRAY_SIZE(itemPkgs.pkgs) )
			{
				ItemInfo_i& newAddItem =  itemPkgs.pkgs[emptyIndex];
				newAddItem = newItem;

				//检查是否超过了最大个数
				if ( itemNum > itemMaxCount )
					newAddItem.ucCount = itemMaxCount;
				else
					newAddItem.ucCount = itemNum;

				if( CreateNewItem( newAddItem, emptyIndex, taskID ) == NULL )
				{
					errNo = E_PKG_ERROR;
					return false;
				}
				//如果放入成功，道具个数计数设置为0，表示放入包裹成功
				itemNum = 0;
			}
		}

		//如果放入成功
		if( itemNum == 0 )
		{
			//如果产生了堆叠的情况，则依据堆叠影响的道具个数，依次改变
			if ( fullCount> 0 )
			{
				for ( unsigned int i = pkgBegin, j = 0  ; i< pkgEnd  && j < fullCount ; ++i )
				{
					unsigned int pkgIndex = fullPkgIndexArr[i];
					unsigned int pkgCount = fullPkgCountArr[i];
					
					if ( pkgCount == 0 )
						continue;

					if (pkgIndex >= ARRAY_SIZE(itemPkgs.pkgs))
					{
						D_ERROR("CPlayer::GiveNewItemToPlayer, pkgIndex(%d) >= ARRAY_SIZE(itemPkgs.pkgs)\n",pkgIndex);
						continue;
					}
					ItemInfo_i& itemInfo  = itemPkgs.pkgs[pkgIndex];
					itemInfo.ucCount = pkgCount ;
					NoticePkgItemChange( pkgIndex );
					++j;
				}
			}

			TryCounterEvent( CT_COLLECT, newItem.usItemTypeID , newItem.ucCount );
			GetNewItemNoticeOtherTeamMember( newItem ); 
			return true;
		}//包裹满了
		else
		{
			errNo = E_PKG_FULL;
			return false;
		}
	}
	return false;
}

CBaseItem* CPlayer::CreateNewItem( ItemInfo_i& newItem, unsigned int pkgIndex, unsigned int taskID )
{
	if ( pkgIndex >= PACKAGE_SIZE )
		return NULL;

	CBaseItem* pItem = CItemBuilderSingle::instance()->CreateItem( newItem.usItemTypeID );
	if ( !pItem )
		return NULL;

	//设为任务物品
	if ( taskID != 0 )
		pItem->SetTaskID( taskID );

	newItem.uiID    = ItemIDGeneratorSingle::instance()->NewItemID();//获取Item的唯一编号
	pItem->SetItemUID( newItem.uiID );//设置道具的唯一ID值
	pItem->SetItemOwner( GetFullID() );//设置道具的唯一玩家
	pItem->SetItemLevel(GET_ITEM_LEVELUP(newItem.ucLevelUp));
	//新创建的物品，放入包裹道具管理器中
	GetItemDelegate().PushPkgItem( pItem );

	SetPkgItem(newItem,pkgIndex);

	NoticePkgItemChange( pkgIndex );

	CLogManager::DoGetItem(this, 0, newItem);//记日至
	return pItem;
}

bool CPlayer::SplitItem(unsigned int itemUID, unsigned int num)
{
	if ( IsDying() )
		return false;

	if ( num == 0 )
		return false;

	TRY_BEGIN;

	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::SplitItem, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& pkg = playerInfo->pkgInfo;
	bool isFind = false;
	int  emptyIndex = -1;
	int  splitIndex = -1;
	//检查该道具是否在包裹中存在,且在包裹中能找到存放分解后道具的位置

	CBaseItem* pSplitItem = GetItemDelegate().GetPkgItem( itemUID );
	if ( !pSplitItem )
	{
		D_ERROR("%s分解道具失败，找不到对应的道具\n",GetNickName() );
		return false;
	}

	if ( GetItemDelegate().GetItemCountOnPkgs() >= (PACKAGE_SIZE/4) )//超过一页才开始计算拆分时是否背包满
	{
		//查找任务背包中有多少个任务道具
		unsigned int taskitemcount = 0;
		for ( unsigned int i = NORMAL_PKG_SIZE; i< PACKAGE_SIZE; i++ )
		{
			const ItemInfo_i& iteminfo = pkg.pkgs[i];
			if ( iteminfo.uiID != PlayerInfo_3_Pkgs::INVALIDATE  )//任务道具在任务背包中连续存放
				taskitemcount++;
		}

		bool isCheckRst = true;
		if ( !pSplitItem->IsTaskItem() )//如果非放入任务背包的道具
		{
			unsigned int normalitemcount = GetItemDelegate().GetItemCountOnPkgs() - taskitemcount;//非任务背包中的道具数目
			if (  normalitemcount >= NORMAL_PKG_SIZE )
				isCheckRst = false;
		}
		else//如果为放入任务背包的道具
		{
			if( taskitemcount >= (PACKAGE_SIZE/4) )
				isCheckRst = false;
		}

		if ( !isCheckRst )
		{
			MGSplitItemError errMsg;
			errMsg.errMsg.errNo = 1;
			SendPkg<MGSplitItemError>( &errMsg );
			return false;
		}
	}

	if( pSplitItem->GetMaxItemCount() == 1 )
	{
		D_ERROR("%s分解道具，分解的道具编号%d,为不可堆叠物品\n",GetNickName() ,pSplitItem->GetItemID() );
		return false;
	}

	bool isTaskItem = false;
	unsigned int splitBegin = 0;
	unsigned int splitEnd   = NORMAL_PKG_SIZE;
	if ( pSplitItem->IsTaskItem() )//如果是任务物品，则放入任务物品背包
	{
		splitBegin = NORMAL_PKG_SIZE;
		splitEnd   = PACKAGE_SIZE;
		isTaskItem = true;
	}
 
	for ( unsigned int i = splitBegin ; i < splitEnd; ++i )
	{
		if( pkg.pkgs[i].uiID == itemUID )
		{
			splitIndex = i;
			isFind = true;
		}

		if ( emptyIndex == -1 && 
			pkg.pkgs[i].uiID == PlayerInfo_3_Pkgs::INVALIDATE )
			emptyIndex = i;

		if ( emptyIndex > -1 &&  isFind )
			break;
	}

	//如果在包裹中能找到要分解的道具
	if ( isFind && emptyIndex > -1 )
	{
		ItemInfo_i& splitItem =  pkg.pkgs[splitIndex];
		if ( (unsigned int)splitItem.ucCount <= num )
		{
			D_ERROR("分解道具个数不对分解前%d 分解数目:%d\n",splitItem.ucCount,num );
			return false;
		}

		CBaseItem* pItem = CItemBuilderSingle::instance()->CreateItem( splitItem.usItemTypeID );
		if ( pItem  )
		{
			//将新分解的道具插入玩家背包
			unsigned int uID =  ItemIDGeneratorSingle::instance()->NewItemID();
			pItem->SetItemUID( uID );
			pItem->SetItemOwner( GetFullID() );
			pItem->SetItemLevel(GET_ITEM_LEVELUP(splitItem.ucLevelUp));

			if( isTaskItem )
				pItem->SetTaskID(  pSplitItem->GetTaskID() );

			//原来的道具分解的物品，放入包裹的，放入包裹道具管理器中
			GetItemDelegate().PushPkgItem( pItem );

			//包裹中空闲道具的位置的信息
			ItemInfo_i& newItem = pkg.pkgs[emptyIndex];
			newItem = splitItem;
			newItem.ucCount = num ;
			newItem.uiID = uID;

			//更新原来的个数
			splitItem.ucCount -= num;

			//通知客户端更新包裹中对应位置的道具变化
			NoticePkgItemChange( splitIndex );
			NoticePkgItemChange( emptyIndex );

			return true;
		}
		else
		{
			D_ERROR(" CPlayer::SplitItem 分离道具时出错,创建不出新分离的道具\n");
		}
	}

	TRY_END;

	return false;
}

//交易时交换物品
int CPlayer::OnTradeSwapItem( CPlayer* pTargetPlayer,int sourceItemIndexArr[], int targetItemIndexArr[], unsigned int sourceMoney, unsigned int targetMoney)
{
	if ( NULL == pTargetPlayer )
	{
		D_ERROR( "CPlayer::OnTradeSwapItem, NULL == pTargetPlayer\n" );
		return -1;
	}

	// 备份双方包裹信息到临时包裹信息，对临时包裹信息操作，剔除要交易的物品
	FullPlayerInfo* sourcePlayerInfo = GetFullPlayerInfo();
	if ( NULL == sourcePlayerInfo )
	{
		D_ERROR( "CPlayer::OnTradeSwapItem, NULL == sourcePlayerInfo\n" );
		return -1;
	}
	FullPlayerInfo* targetPlayerInfo = pTargetPlayer->GetFullPlayerInfo();
	if ( NULL == targetPlayerInfo )
	{
		D_ERROR( "CPlayer::OnTradeSwapItem, NULL == targetPlayerInfo\n" );
		return -1;
	}
	PlayerInfo_3_Pkgs& sourcePkg = sourcePlayerInfo->pkgInfo;
	PlayerInfo_3_Pkgs& targetPkg = targetPlayerInfo->pkgInfo;

	ItemInfo_i tmpSourcepkgs[PACKAGE_SIZE];//临时源包裹信息
	ItemInfo_i tmpSourceTradeItems[CTradeInfo::MAX_TRADE_NUM];//源交易物品列表
	StructMemCpy( tmpSourcepkgs, sourcePkg.pkgs, sizeof(ItemInfo_i)*PACKAGE_SIZE );

	unsigned int sourceCount = 0;
	for ( int i = 0 ; i< CTradeInfo::MAX_TRADE_NUM; i++)
	{
		int srItemIndex = sourceItemIndexArr[i];
		if ( (srItemIndex >= 0) && (srItemIndex < PACKAGE_SIZE) )
		{
			ItemInfo_i&  srItem = tmpSourcepkgs[srItemIndex];
			tmpSourceTradeItems[sourceCount] = srItem;//放到源交易物品列表
			srItem.uiID = srItem.usItemTypeID = srItem.ucCount = 0;//剔除
			
			sourceCount++;
		}
	}

	ItemInfo_i tmpTargetpkgs[PACKAGE_SIZE];
	ItemInfo_i tmpTargetTradeItems[CTradeInfo::MAX_TRADE_NUM];
	StructMemCpy( tmpTargetpkgs, targetPkg.pkgs, sizeof(ItemInfo_i)*PACKAGE_SIZE );

	unsigned int targetCount = 0;
	for ( int i = 0 ; i< CTradeInfo::MAX_TRADE_NUM; i++)
	{
		int trgItemIndex = targetItemIndexArr[i];
		if ( (trgItemIndex >= 0) && (trgItemIndex < PACKAGE_SIZE) )
		{
			ItemInfo_i&  trgItem = tmpTargetpkgs[trgItemIndex];
			tmpTargetTradeItems[targetCount] = trgItem;//放到对方交易物品列表
			trgItem.uiID = trgItem.usItemTypeID = trgItem.ucCount = 0;

			targetCount++;
		}
	}

	// 判断是否有足够空间放入对方交易的物品
	int changeTarget[PACKAGE_SIZE];
	StructMemSet(changeTarget, -1, sizeof(changeTarget));
	unsigned int uiTargetChangedCount = 0;
	for ( unsigned int j = 0 ; j< sourceCount ; j++  )
	{
		ItemInfo_i& sourceTmp = tmpSourceTradeItems[j];
		if ( !pTargetPlayer->CheckItemCanAdd( sourceTmp,tmpTargetpkgs ,changeTarget, uiTargetChangedCount , 0 ,NORMAL_PKG_SIZE ) )
		{
			return (int) MGConfirmTrade::PEER_PKG_SPACE_NOT_ENOUGH;//对方没有足够的空间
		}
	}

	int changeSource[PACKAGE_SIZE];
	StructMemSet(changeSource, -1, sizeof(changeSource));
	unsigned int uiSourceChangedCount = 0;
	for ( unsigned int j = 0 ; j< targetCount; j++ )
	{
		ItemInfo_i& targetTmp = tmpTargetTradeItems[j];
		if ( !CheckItemCanAdd( targetTmp,tmpSourcepkgs ,changeSource, uiSourceChangedCount , 0 ,NORMAL_PKG_SIZE ) )
		{
			return (int) MGConfirmTrade::SELF_PKG_SPACE_NOT_ENOUGH;//自己没有足够的空间
		}
	}

	//判断是否可以交换金钱
	bool bMoneyChanaged = false;
	FullPlayerInfo* pFullPlayerInfo = GetFullPlayerInfo();
	FullPlayerInfo* pPeerFullPlayerInfo = pTargetPlayer->GetFullPlayerInfo();
	if ( ( NULL == pFullPlayerInfo )
		|| ( NULL == pPeerFullPlayerInfo )
		)
	{
		D_ERROR( "CPlayer::OnTradeSwapItem, NULL == some FullPlayerInfo\n" );
		return -1;
	}

	if(sourceMoney != targetMoney)// 有变化
	{
		//交易前交易金额是否大于当前金额
		if ( (sourceMoney > GetGoldMoney()) //双币种判断，交易只可使用金币
			|| (targetMoney > pTargetPlayer->GetGoldMoney()) //双币种判断
			)
		{
			return -1;
		}

		// 交易后金额是否大于最大值？
		if ( (GetGoldMoney() - sourceMoney + targetMoney ) > MAX_MONEY )//双币种判断
		{
			return (int)MGConfirmTrade::SELF_MONEY_OVERFLOW;
		}
			
		if ( (pTargetPlayer->GetGoldMoney() + sourceMoney - targetMoney) > MAX_MONEY )
		{
			return (int)MGConfirmTrade::PEER_MONEY_OVERFLOW;
		}
		
		bMoneyChanaged = true;
	}

	//更新双方的金钱
	if (bMoneyChanaged)
	{
		SetGoldMoney( GetGoldMoney() - sourceMoney + targetMoney );//双币种判断
		pTargetPlayer->SetGoldMoney( pTargetPlayer->GetGoldMoney() - targetMoney + sourceMoney );//双币种判断

		//金币信息暂时不写日志，by dzj, 10.07.15,RankEventManagerSingle::instance()->MonitorPlayerMoneyChange( this, GetGoldMoney() );//双币种判断
		//金币信息暂时不写日志，by dzj, 10.07.15,RankEventManagerSingle::instance()->MonitorPlayerMoneyChange( pTargetPlayer, pTargetPlayer->GetGoldMoney() );//双币种判断

		NotifyGoldMoney();
		pTargetPlayer->NotifyGoldMoney();
	}

	// 通知交易后双方减少的物品
	for ( unsigned int  i = 0; i < sourceCount; i++ )
	{
		ItemInfo_i& sourceTmp = tmpSourceTradeItems[i];
		if ( sourceTmp.ucCount == 0 )//物理删除掉该物品（因为和其他的物品叠加）
		{
			OnDropItem( sourceTmp.uiID );
		} else {
			//只是在自己的包裹中删掉
			MGDropItemResult msg;
			msg.uiItemID = sourceTmp.uiID;
			msg.result = true;
			SendPkg<MGDropItemResult>(&msg);
		}
	}

	for ( unsigned int  i = 0 ;i < targetCount; i++ )
	{
		ItemInfo_i& targetTmp = tmpTargetTradeItems[i];
		if ( targetTmp.ucCount == 0 )//物理删除掉该物品（因为和其他的物品叠加）
		{
			pTargetPlayer->OnDropItem( targetTmp.uiID );
		} else {
			//只是在targetplayer的包裹中删掉
			MGDropItemResult msg;
			msg.uiItemID = targetTmp.uiID;
			msg.result = true;
			pTargetPlayer->SendPkg<MGDropItemResult>(&msg);
		}
	}

	//用临时包裹信息替换旧的包裹信息(**必须在通知删除后进行**)
	StructMemCpy(sourcePkg.pkgs, tmpSourcepkgs,sizeof(ItemInfo_i)*PACKAGE_SIZE  );
	StructMemCpy(targetPkg.pkgs, tmpTargetpkgs,sizeof(ItemInfo_i)*PACKAGE_SIZE  );
	CLogManager::DoTradeItem(this, pTargetPlayer, tmpSourceTradeItems, sourceCount, tmpTargetTradeItems, targetCount, sourceMoney, targetMoney);//记日至

	//通知玩家对应的包裹改变
	for ( unsigned int i = 0 ; i < sourceCount; i++  )
	{
		int srItemIndex = sourceItemIndexArr[i];
		if ( srItemIndex == -1  )
		{
			continue;
		}

		NoticePkgItemChange( srItemIndex );
	}

	//通知万家对应的包裹位改变
	for ( unsigned int i = 0 ; i< targetCount; i++ )
	{
		int trgItemIndex = targetItemIndexArr[i];
		if ( trgItemIndex == -1 )
		{
			continue;
		}

		pTargetPlayer->NoticePkgItemChange( trgItemIndex );
	}

	//通知交易后增加或变化的物品
	PlayerID playerID = this->GetFullID();
	for(int i = 0; i < PACKAGE_SIZE; i++)
	{
		int index = changeSource[i];
		if ( index == -1 )
		{
			break;
		}

		if((index >= 0) && (index < PACKAGE_SIZE))
		{
			this->NoticePkgItemChange(index);
			//如果物品是交易过来的，改变物品的所有者,并增加包裹中的道具数目
			CBaseItem* pItem   = pTargetPlayer->GetItemDelegate().GetPkgItem( sourcePkg.pkgs[index].uiID );
			if (NULL != pItem)
			{
				if(pItem->GetItemOwner() != playerID)
				{
					pItem->SetItemOwner(playerID);
					pItem->SetItemLevel(GET_ITEM_LEVELUP(sourcePkg.pkgs[index].ucLevelUp));
					//将从别人交易来的物品，放入自己的包裹管理器中
					GetItemDelegate().PushPkgItem( pItem );
					//从交换玩家包裹中拿掉
					pTargetPlayer->GetItemDelegate().RemovePkgItem( sourcePkg.pkgs[index].uiID );
				}
			} else {
				D_ERROR("交易的物品在玩家%s的包裹管理器中找不到%d\n",GetNickName(),sourcePkg.pkgs[index].uiID );
			}
		}
	}

	PlayerID targetPlayerID = pTargetPlayer->GetFullID();
	for ( int i=0; i<PACKAGE_SIZE; ++i )
	{
		int index = changeTarget[i];
		if (index == -1)
		{
			break;
		}

		if((index >= 0) && (index < PACKAGE_SIZE))
		{
			pTargetPlayer->NoticePkgItemChange(index);
			
			//如果物品是交易过来的，改变物品的所有者,并增加包裹中的道具数目
            CBaseItem* pItem   = GetItemDelegate().GetPkgItem( targetPkg.pkgs[index].uiID );
			if (NULL != pItem)
			{
				if(pItem->GetItemOwner() != targetPlayerID)
				{
					pItem->SetItemOwner(targetPlayerID);
					pItem->SetItemLevel(GET_ITEM_LEVELUP(targetPkg.pkgs[index].ucLevelUp));
					//将交易过去的物品，放入交换人的包裹管理器中
					pTargetPlayer->GetItemDelegate().PushPkgItem( pItem );
					//从包裹中拿掉
					GetItemDelegate().RemovePkgItem(  targetPkg.pkgs[index].uiID  );
				}
			} else {
				D_ERROR("交易的物品在玩家%s的包裹管理器中找不到%d\n",GetNickName(),targetPkg.pkgs[index].uiID );
			}
		}

	}

	return 0;
}

//通知银币数量
bool CPlayer::NotifySilverMoney()
{
	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_SILVER_MONEY;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = GetSilverMoney();
	updateAttr.lChangedPlayerID = GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	SendPkg<MGRoleattUpdate>( &updateAttr );
	return true;
}

//通知金币数量
bool CPlayer::NotifyGoldMoney()
{
	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_GOLD_MONEY;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = GetGoldMoney();
	updateAttr.lChangedPlayerID = GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	SendPkg<MGRoleattUpdate>( &updateAttr );
	return true;
}

bool CPlayer::CheckItemCanAdd(ItemInfo_i& AddItemInfo, ItemInfo_i itemPkgs[] , int changeIndexArr[], unsigned int &uiCount , unsigned searchBegin, unsigned searchEnd )
{
	CItemPublicProp* pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp( AddItemInfo.usItemTypeID );
	if ( !pPublicProp )
		return false;

	unsigned int uiMaxCount = pPublicProp->m_itemMaxAmount;
	if(uiMaxCount > 1)//可叠加
	{
		std::vector<ItemInfo_i> tmpItemInfoVec;
		if ( (unsigned int)AddItemInfo.ucCount >= uiMaxCount )
		{
			unsigned int maxpage   = AddItemInfo.ucCount / uiMaxCount;
			unsigned int leftcount = AddItemInfo.ucCount % uiMaxCount;
			for ( unsigned int i = 0 ; i < maxpage ; i++ )
			{
				ItemInfo_i tmpInfo = AddItemInfo;
				tmpInfo.ucCount = uiMaxCount;
				tmpItemInfoVec.push_back( tmpInfo );
			}
			if ( leftcount > 0 )
			{
				ItemInfo_i tmpInfo = AddItemInfo;
				tmpInfo.ucCount    = leftcount;
				tmpItemInfoVec.push_back( tmpInfo );
			}
		}
		else
		{
			tmpItemInfoVec.push_back( AddItemInfo );
		}

		unsigned int tmpItemCount = 0;
		unsigned int width = CBaseItem::GetWidth(AddItemInfo.usItemTypeID);
		unsigned int height = CBaseItem::GetHeight(AddItemInfo.usItemTypeID);
		for ( unsigned int addindex = 0 ; addindex < tmpItemInfoVec.size(); addindex++ ) 
		{
			int iFirstEmptyIndex = -1;//第一个为空的位置

			ItemInfo_i& addItem = tmpItemInfoVec[addindex];
			//从搜索的第一位开始
			for( unsigned i = searchBegin; i < searchEnd; i++)
			{
				if((iFirstEmptyIndex == -1) && ( itemPkgs[i].usItemTypeID == 0 )  )//记录第一个为空的位置
				{	
					bool isFree = true;
					if (i/PKG_COLUMN != (i+width-1)/PKG_COLUMN)
					{
						continue;
					}
					for ( unsigned w = 0; w<width; ++w)
					{
						for (unsigned h = 0; h<height; ++h)
						{
							unsigned int index = i+w+h*PKG_COLUMN;
							if (index < searchEnd)
							{
								if (itemPkgs[index].usItemTypeID != PlayerInfo_3_Pkgs::INVALIDATE)
								{
									isFree = false;
									break;
								}
							}
							else
							{
								isFree = false;
								break;
							}
						}
						if (!isFree)
						{
							break;
						}
					}
					if (isFree)
					{
						iFirstEmptyIndex = i;
						continue;
					}
				}

				if(itemPkgs[i].usItemTypeID == addItem.usItemTypeID)//同类物品
				{
					if((unsigned int)itemPkgs[i].ucCount < uiMaxCount)//可以继续叠加
					{
						//并且是同种类的，耐久度是一致的
						if( itemPkgs[i].usWearPoint == addItem.usWearPoint
							&& itemPkgs[i].excellentid == addItem.excellentid
							&& itemPkgs[i].luckyid     == addItem.luckyid )
						{
							unsigned char ucSpace = uiMaxCount - itemPkgs[i].ucCount;
							if( ucSpace >= addItem.ucCount)//足够放入
							{
								itemPkgs[i].ucCount +=  addItem.ucCount;
								changeIndexArr[uiCount++] = i;
								addItem.ucCount = 0;
								tmpItemCount++;
								break;
							}
							else
							{
								itemPkgs[i].ucCount = uiMaxCount;
								addItem.ucCount -= ucSpace;
								changeIndexArr[uiCount++] = i;
							}
						}
					}
				}
			}

			if ( addItem.ucCount > 0 )
			{
				if ( iFirstEmptyIndex != -1 )
				{
					itemPkgs[iFirstEmptyIndex] = addItem;
					changeIndexArr[uiCount++]  = iFirstEmptyIndex;
					addItem.ucCount = 0;
					for ( unsigned w = 0; w<width; ++w)
					{
						for (unsigned h = 0; h<height; ++h)
						{
							unsigned int index = iFirstEmptyIndex+w+h*PKG_COLUMN;
							if (index < searchEnd)
							{
								itemPkgs[index] = AddItemInfo;
								itemPkgs[index].ucCount = 0;
							}
						}
					}
					tmpItemCount++;
				}
				else
					return false;
			}
		}

		if ( tmpItemCount == (unsigned int)tmpItemInfoVec.size() )
			return true;
		else
			return false;
	}
	else//如果是不可堆叠道具
	{
		unsigned int width = CBaseItem::GetWidth(AddItemInfo.usItemTypeID);
		unsigned int height = CBaseItem::GetHeight(AddItemInfo.usItemTypeID);
		for( unsigned int i = 0 ; i < (unsigned int)AddItemInfo.ucCount; i++ )
		{
			int iFirstEmptyIndex = -1;//第一个为空的位置
			for( unsigned i = searchBegin; i < searchEnd; i++)
			{
				bool isFree = true;
				if((iFirstEmptyIndex == -1) && (itemPkgs[i].usItemTypeID == PlayerInfo_3_Pkgs::INVALIDATE))//记录第一个为空的位置
				{	
					if (i/PKG_COLUMN != (i+width-1)/PKG_COLUMN)
					{
						continue;
					}
					for ( unsigned w = 0; w<width; ++w)
					{
						for (unsigned h = 0; h<height; ++h)
						{
							unsigned int index = i+w+h*PKG_COLUMN;
							if (index < searchEnd)
							{
								if (itemPkgs[index].usItemTypeID != PlayerInfo_3_Pkgs::INVALIDATE)
								{
									isFree = false;
									break;
								}
							}
							else
							{
								isFree = false;
								break;
							}
						}
						if (!isFree)
						{
							break;
						}
					}
					if (isFree)
					{
						iFirstEmptyIndex = i;
						break;
					}
				}
			}

			//放在空的位置
			if( iFirstEmptyIndex != -1 )
			{
				itemPkgs[iFirstEmptyIndex] = AddItemInfo;
				itemPkgs[iFirstEmptyIndex].ucCount = 1;
				changeIndexArr[uiCount++] = iFirstEmptyIndex;
				for ( unsigned w = 0; w<width; ++w)
				{
					for (unsigned h = 0; h<height; ++h)
					{
						unsigned int index = iFirstEmptyIndex+w+h*PKG_COLUMN;
						if (index < searchEnd)
						{
							itemPkgs[index] = AddItemInfo;
							itemPkgs[index].ucCount = 1;
						}
					}
				}
			}
			else
				return false;
		}
		return true;
	}

	return false;
}

	


void CPlayer::GetNewItemNoticeOtherTeamMember( const ItemInfo_i& itemEle )
{
	//如果组队的话，通知组队玩家自己拾取了道具
	if ( GetSelfStateManager().IsGroupState() )
	{
		CGroupTeam* pTeam = GetSelfStateManager().GetGroupTeam();
		if ( pTeam )
			pTeam->GetNewItemNoticeOtherMembers( this, itemEle );
		else
			D_ERROR("处于组队状态，但找不到组队队伍\n");
	}
	else
	{
		//提示自己拾取了道具
		MGTeamMemberPickItem memberPickItem;
		memberPickItem.itemTypeID = itemEle.usItemTypeID;
		memberPickItem.pickPlayerID = GetFullID();
		SendPkg<MGTeamMemberPickItem>( &memberPickItem );
	}
}

//依据ID号获取道具在包裹中的个数
int CPlayer::CounterItemByID(unsigned int itemTypeID )
{
	int count = 0 ;
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::CounterItemByID, NULL == GetFullPlayerInfo()\n");
		return 0;
	}
	for ( int index = 0 ; index< PACKAGE_SIZE; index++  )//找到道具包中的空的位置
	{
		ItemInfo_i& itemInfo = playerInfo->pkgInfo.pkgs[index];
		if ( itemInfo.usItemTypeID == itemTypeID )
		{
			if (IsLeftUpPkgIndex(index,itemInfo.uiID))
			{
				count+= itemInfo.ucCount;
			}
		}
	}
	return count;
}

void CPlayer::MoveAffectTrade(void)
{
	TRY_BEGIN;

	CTradeInfo * pTradeInfo = TradeInfo();
	if ( NULL == pTradeInfo ) //by dzj, 08.08.05,???,张松伟检查;
		return;

	PlayerID peerPlayerID;
	CPlayer* pPeerPlayer = NULL;

	//确定对方所处gate
	if(pTradeInfo->InvitePlayerID() == m_lPlayerID)
		peerPlayerID = pTradeInfo->TargetPlayerID();
	else
		peerPlayerID = pTradeInfo->InvitePlayerID();

	if(peerPlayerID.wGID == m_lPlayerID.wGID)
	{
		CGateSrv* thisPlayerGate = GetProc();//m_uniGatePtr.GetInnerGateSrvPtr();
		if(NULL != thisPlayerGate)
			pPeerPlayer = thisPlayerGate->FindPlayer(peerPlayerID.dwPID);
	}
	else
	{
		CGateSrv* peerGate = CManG_MProc::FindProc( peerPlayerID.wGID );
		if(NULL != peerGate)
			pPeerPlayer = peerGate->FindPlayer(peerPlayerID.dwPID);
	}

	if(NULL != pPeerPlayer)
	{
		if((NULL != m_pMap) && !IsInAccurateDistance(GetPosX(), GetPosY(), pPeerPlayer->GetPosX(), pPeerPlayer->GetPosY(), 7/*客户端显示位置容许误差2*/))
		{
			PlayerID tempID;
			tempID.dwPID = 0;
			tempID.wGID = 0;

			//重置
			CTradeInfoManager::DelTradeInfo(m_nTradeInfo);


			this->RecvFromPlayer(tempID);
			this->TradeInfo(0);				  
			pPeerPlayer->RecvFromPlayer(tempID);
			pPeerPlayer->TradeInfo(0);

			//取消
			MGCancelTrade msg;
			this->SendPkg<MGCancelTrade>(&msg);
			pPeerPlayer->SendPkg<MGCancelTrade>(&msg);
		}
	}

	return;
	TRY_END;
	return;
}

void CPlayer::LeaveAffectTrade(void)
{
	PlayerID peerPlayerID;
	CPlayer *pPeerPlayer = NULL;

	CTradeInfo * pTradeInfo = TradeInfo();
	if ( NULL == pTradeInfo )//没有建立交易
	{
		peerPlayerID = RecvFromPlayer();
		if((peerPlayerID.wGID != 0) || (peerPlayerID.dwPID != 0) )//都到交易邀请
		{
			if(peerPlayerID.wGID == m_lPlayerID.wGID)
			{
				CGateSrv* thisPlayerGate = GetProc();//m_uniGatePtr.GetInnerGateSrvPtr();
				if(NULL != thisPlayerGate)
					pPeerPlayer = thisPlayerGate->FindPlayer(peerPlayerID.dwPID);
			}
			else
			{
				CGateSrv* peerGate = CManG_MProc::FindProc( peerPlayerID.wGID );
				if(NULL != peerGate)
					pPeerPlayer = peerGate->FindPlayer(peerPlayerID.dwPID);
			}

			if(NULL != pPeerPlayer)
			{
				m_recvFromPlayer.dwPID = 0;
				m_recvFromPlayer.wGID = 0;

				MGSendTradeReqResult msg;
				msg.lSelfPlayerID = m_lPlayerID;
				msg.tResult = (MGSendTradeReqResult::eTradeResult)(GCSendTradeReqResult::TRADE_REQ_REFUSE);

				pPeerPlayer->SendPkg<MGSendTradeReqResult>(&msg);
			}
		}
		return;
	}

	//确定对方所处gate
	if(pTradeInfo->InvitePlayerID() == m_lPlayerID)
		peerPlayerID = pTradeInfo->TargetPlayerID();
	else
		peerPlayerID = pTradeInfo->InvitePlayerID();

	if(peerPlayerID.wGID == m_lPlayerID.wGID)
	{
		CGateSrv* thisPlayerGate = GetProc();//m_uniGatePtr.GetInnerGateSrvPtr();
		if(NULL != thisPlayerGate)
			pPeerPlayer = thisPlayerGate->FindPlayer(peerPlayerID.dwPID);
	}
	else
	{
		CGateSrv* peerGate = CManG_MProc::FindProc( peerPlayerID.wGID );
		if(NULL != peerGate)
			pPeerPlayer = peerGate->FindPlayer(peerPlayerID.dwPID);
	}

	if(NULL != pPeerPlayer)
	{
		PlayerID tempID;
		tempID.dwPID = 0;
		tempID.wGID = 0;

		//重置
		this->DelTradeInfo();

		this->RecvFromPlayer(tempID);
		this->TradeInfo(0);
		pPeerPlayer->RecvFromPlayer(tempID);
		pPeerPlayer->TradeInfo(0);

		//取消
		MGCancelTrade msg;
		this->SendPkg<MGCancelTrade>(&msg);
		pPeerPlayer->SendPkg<MGCancelTrade>(&msg);
	}

	return;
}

void CPlayer::DieAffectWear(void)
{
	int iResult = 0;
	PlayerInfo_2_Equips &equipList = m_fullPlayerInfo.equipInfo;
	
	MGNotifyWearChange msg;
	StructMemSet( msg, 0x00, sizeof(msg));

	for(int i = 0; i < EQUIP_SIZE; i++)
	{
		if((equipList.equips[i].uiID > 0) && (equipList.equips[i].usWearPoint > 0))
		{
			//ItemInfo_i temp = equipList.equips[i];

			iResult = this->DoWear(&(equipList.equips[i]), CItemPublicProp::DIE);
			if(1 == iResult)//耐久度变化,但未失效
			{
				if (msg.itemSize >= ARRAY_SIZE(msg.items))
				{
					D_ERROR("CPlayer::DieAffectWear, msg.itemSize(%d) >= ARRAY_SIZE(msg.items)\n", msg.itemSize);
					break;
				}
				msg.items[msg.itemSize].itemID = equipList.equips[i].uiID;
				msg.items[msg.itemSize].newWear = equipList.equips[i].usWearPoint;

				msg.itemSize++;
			}
			//else if(2 == iResult)//耐久度失效且销毁
			//{
			//	MGDropItemResult msg1;
			//	msg1.uiItemID = temp.uiID;
			//	msg1.result = true;
			//	this->SendPkg<MGDropItemResult>(&msg1);
			//}
		}	
	}

	if(msg.itemSize > 0)
		SendPkg<MGNotifyWearChange>(&msg);

	return;
}

bool CPlayer::UseItemAffectWear(ItemInfo_i *pItem)
{
	if ( NULL == pItem )
	{
		D_ERROR( "CPlayer::UseItemAffectWear, NULL == pItem\n" );
		return false;
	}

	int iResult = 0;
	//ItemInfo_i temp = *pItem;

	MGNotifyWearChange msg;
	StructMemSet( msg, 0x00, sizeof(msg));

	iResult = DoWear(pItem, CItemPublicProp::USE_TIME);
	if(1 == iResult)//耐久度变化,但未失效
	{
		msg.itemSize = 1;
		msg.items[0].itemID = pItem->uiID;
		msg.items[0].newWear = pItem->usWearPoint;

		SendPkg<MGNotifyWearChange>(&msg);
	}
	//else if(2 == iResult)//耐久度失效且销毁
	//{
	//	MGDropItemResult msg1;
	//	msg1.uiItemID = temp.uiID;
	//	msg1.result = true;
	//	this->SendPkg<MGDropItemResult>(&msg1);
	//}
	else
	{
		return false;
	}

	return true;
}

void CPlayer::BeInjuredAffectWear(void)
{
	int iResult = 0;
	PlayerInfo_2_Equips &equipList = m_fullPlayerInfo.equipInfo;
	
	MGNotifyWearChange msg;
	StructMemSet( msg, 0x00, sizeof(msg));

	for(int i = 0; i < EQUIP_SIZE; i++)
	{
		if((equipList.equips[i].uiID > 0) && (equipList.equips[i].usWearPoint > 0))
		{
			//ItemInfo_i temp = equipList.equips[i];

			iResult = DoWear(&(equipList.equips[i]), CItemPublicProp::INJURED_TIME);
			if(1 == iResult)//耐久度变化,但未失效
			{
				if (msg.itemSize >= ARRAY_SIZE(msg.items))
				{
					D_ERROR("CPlayer::BeInjuredAffectWear, msg.itemSize(%d) >= ARRAY_SIZE(msg.items)\n", msg.itemSize);
					break;
				}
				msg.items[msg.itemSize].itemID = equipList.equips[i].uiID;
				msg.items[msg.itemSize].newWear = equipList.equips[i].usWearPoint;

				msg.itemSize++;
			}
			//else if(2 == iResult)//耐久度失效且销毁
			//{
			//	MGDropItemResult msg1;
			//	msg1.uiItemID = temp.uiID;
			//	msg1.result = true;
			//	this->SendPkg<MGDropItemResult>(&msg1);
			//}
		}
	}

	if(msg.itemSize > 0)
		SendPkg<MGNotifyWearChange>(&msg);

	return;
}

void CPlayer::UseSkillTimeAffectWear(void)
{
	int iResult = 0;
	PlayerInfo_2_Equips &equipList = m_fullPlayerInfo.equipInfo;
	
	MGNotifyWearChange msg;
	StructMemSet( msg, 0x00, sizeof(msg));

	for(int i = 0; i < EQUIP_SIZE; i++)
	{
		if((equipList.equips[i].uiID > 0) && (equipList.equips[i].usWearPoint > 0))
		{
			//ItemInfo_i temp = equipList.equips[i];

			iResult = DoWear(&(equipList.equips[i]), CItemPublicProp::USE_SKILL_TIME);
			if(1 == iResult)//耐久度变化,但未失效
			{
				if (msg.itemSize >= ARRAY_SIZE(msg.items))
				{
					D_ERROR("CPlayer::UseSkillTimeAffectWear, msg.itemSize(%d) >= ARRAY_SIZE(msg.items)\n", msg.itemSize);
					break;
				}
				msg.items[msg.itemSize].itemID = equipList.equips[i].uiID;
				msg.items[msg.itemSize].newWear = equipList.equips[i].usWearPoint;

				msg.itemSize++;
			}
			//else if(2 == iResult)//耐久度失效且销毁
			//{
			//	MGDropItemResult msg1;
			//	msg1.uiItemID = temp.uiID;
			//	msg1.result = true;
			//	this->SendPkg<MGDropItemResult>(&msg1);
			//}
		}
	}

	if(msg.itemSize > 0)
		SendPkg<MGNotifyWearChange>(&msg);

	return;
}

void CPlayer::TimeAffectWear(void)
{
	//int iResult = 0;
	
	time_t now;
	CItemPublicProp *pItemPublicProp = NULL;

	//遍历装备栏位
	PlayerInfo_2_Equips &equipList = m_fullPlayerInfo.equipInfo;
	for(int i = 0; i < EQUIP_SIZE; i++)
	{
		if((equipList.equips[i].uiID > 0) && (equipList.equips[i].usWearPoint > 0))
		{
			pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(equipList.equips[i].usItemTypeID);
			if(NULL == pItemPublicProp)
				continue;

			if(ACE_BIT_ENABLED(pItemPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY))
			{
				ACE_OS::time(&now);
				if( (unsigned int)now > equipList.equips[i].usWearPoint)
				{
					if(ACE_BIT_ENABLED(pItemPublicProp->m_wearOMode, CItemPublicProp::DESTROY))//耐久度为零时要求销毁,则销毁
						StructMemSet( (equipList.equips[i]), 0x00, sizeof(ItemInfo_i));
				}
			}
		}
	}

	//遍历包裹
	PlayerInfo_3_Pkgs &pkg = m_fullPlayerInfo.pkgInfo;
	for(int i = 0; i < PACKAGE_SIZE; i++)
	{
		if((pkg.pkgs[i].uiID > 0) && (pkg.pkgs[i].usWearPoint > 0))
		{
			pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(pkg.pkgs[i].usItemTypeID);
			if(NULL == pItemPublicProp)
				continue;
			
			if(ACE_BIT_ENABLED(pItemPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY))
			{
				ACE_OS::time(&now);
				if( (unsigned int)now > pkg.pkgs[i].usWearPoint)
				{
					if(ACE_BIT_ENABLED(pItemPublicProp->m_wearOMode, CItemPublicProp::DESTROY))//耐久度为零时要求销毁,则销毁
						StructMemSet( (pkg.pkgs[i]), 0x00, sizeof(ItemInfo_i));
				}
			}
		}
	}

	return;
}

int CPlayer::DoWear(ItemInfo_i *pItem, unsigned int uiFlag)
{
	//返回值意义(返回值：-1:出错,0:耐久度无变化,1:耐久度有变化,2:物品被删除)
	if(NULL == pItem)
		return -1;

	//获取物品的公共属性
	CItemPublicProp *pItemPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(pItem->usItemTypeID);
	if(NULL == pItemPublicProp)
		return -1;

	// 时间流逝型耐久度到期
	bool timedWearAndTimeout = false;

	//时间和次数消耗类型互斥
	if(!ACE_BIT_ENABLED(pItemPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY))//非时间型消耗
	{
		//是否和指定消耗类型匹配？
		if(!ACE_BIT_ENABLED(pItemPublicProp->m_wearConsumeMode, uiFlag))
			return 0;

		//耐久度损耗
		if(pItem->usWearPoint > 0)
		{
			if(CItemPublicProp::DIE == uiFlag)//消耗模式为死亡
			{
				unsigned char deadWearRate = CSpSkillManager::GetDeadWearRate();
				if(pItem->usWearPoint > (unsigned int )(pItemPublicProp->m_maxWear * deadWearRate /100))
					pItem->usWearPoint -= (unsigned int )(pItemPublicProp->m_maxWear * deadWearRate /100);
				else
					pItem->usWearPoint = 0;
			}
			else if(CItemPublicProp::USE_TIME == uiFlag)//消耗模式为使用
			{
				pItem->usWearPoint -= 1;
			}
			else if(CItemPublicProp::INJURED_TIME == uiFlag)//消耗模式为受到伤害
			{
				pItem->usWearPoint -= 1;
			}
			else if(CItemPublicProp::USE_SKILL_TIME == uiFlag)//消耗模式为使用技能
			{
				pItem->usWearPoint -= 1;
			}
			else
			{
				return 0;
			}
		}
	}
	else//时间型消耗
	{
		time_t now;

		ACE_OS::time(&now);
		if( (unsigned int)now > pItem->usWearPoint)
			timedWearAndTimeout = true;
	} 

	//耐久度为零时处理
	if((0 == pItem->usWearPoint) || timedWearAndTimeout)
	{	
		//物品影响属性变化
		OnItemWearPointEmpty(pItem);
		
		if(ACE_BIT_ENABLED(pItemPublicProp->m_wearOMode, CItemPublicProp::DESTROY))//耐久度为零时要求销毁,则销毁
		{
			D_DEBUG( "玩家%s的道具%d耐久为0或时间到期，DESTROY已设，销毁\n", GetAccount(), pItem->uiID );
			OnDropItem(pItem->uiID);
			return 2;
		}
	}

	return 1;
}

//耐久度降为0时，调用，扣除道具所加的属性
void CPlayer::OnItemWearPointEmpty( MUX_PROTO::ItemInfo_i *itemWear)
{
	if ( !itemWear )
		return;

	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::OnItemWearPointEmpty, NULL == GetFullPlayerInfo()\n");
		return;
	}
	unsigned int  equipIndex = 0;
	for ( equipIndex = 0 ; equipIndex < EQUIP_SIZE; equipIndex ++ )
	{
		ItemInfo_i& itemInfo = playerInfo->equipInfo.equips[equipIndex];
		if ( itemInfo.uiID == itemWear->uiID )
			break;
	}

	//如果不是装备，则不扣除加在玩家身上的属性
	if ( equipIndex == EQUIP_SIZE )
		return;

	D_DEBUG("玩家%s的装备%d的耐久度变为0,扣除所加的属性\n",GetNickName(), itemWear->usItemTypeID );

	//如果是玩家装备的物品,将道具的属性从玩家身上减去
	CBaseItem* pItem = GetItemDelegate().GetEquipItem( itemWear->uiID );
	if ( pItem )
	{
		//耐久度变化影响玩家的OtherPlayerInfo中的信息
		//EquipItemAffectOtherPlayerInfo(  *itemWear , equipIndex );

		float oldSpeed = GetSpeed();
		//pItem->SubItemPropToPlayer( this, *itemWear );
		ItemAttributeHelper::OnWearPointEmptyAffectPlayer( this,  *itemWear );


		//如果是套装,则判断需不需要把套装的属性从人身上移除
		int suitID = pItem->GetSuitID();
		if ( suitID > 0  )
			GetSuitDelegate().OnWearPointEmptySubSuitProp( suitID, equipIndex );

		//通知血，魔变化了
		NoticeHPMPChange();

		if ( oldSpeed != GetSpeed() )
		{
			NoticeSpeedChange();
		}
	}
	else
	{
		D_ERROR("玩家%s的装备耐久度为0，但在装备管理器中找不到该道具 %d\n",GetNickName(), itemWear->usItemTypeID );
	}
}

//装备道具时调用，加上道具的属性
void CPlayer::OnEquipItem(const MUX_PROTO::ItemInfo_i &item, unsigned int equipIndex)
{
	CBaseItem* pEquipItem =  GetItemDelegate().GetPkgItem( item.uiID );
	if ( !pEquipItem )
	{
		D_ERROR("从道具包装备的道具在玩家%s包裹道具管理器中找不到%d\n", GetNickName(), item.uiID );
		return;
	}

	float oldspeed = GetSpeed();

	//放入装备管理器
	GetItemDelegate().EquipItem( pEquipItem );

	//装备道具影响玩家的OtherPlayerInfo属性
	//EquipItemAffectOtherPlayerInfo( item, equipIndex );

	////耐久度>0的话，才把装备属性加上
	//if ( item.usWearPoint > 0 )
	//	pEquipItem->AddItemPropToPlayer( this, item );
	ItemAttributeHelper::OnEquipItemAffectPlayer( this, item );

	//玩家装备属于套装,则判断是否将套装的属性加给玩家
	if ( pEquipItem->GetSuitID() > 0)
		GetSuitDelegate().EquipSuitPart( pEquipItem->GetSuitID(), equipIndex, item.usWearPoint>0 );

	//ReloadSecondProp();
	//ItemManagerSingle::instance()->CheckPlayerEquipItem(this);

	//给其他人更新血量魔量消息
	NoticeHPMPChange();

	if ( oldspeed != GetSpeed() )
	{
		NoticeSpeedChange();
	}

	////加完以后更新组队成员信息
	//SendSelfInfoToOtherMembers();
}

//卸载装备时调用，减去道具的属性
void CPlayer::OnUnEquipItem(const ItemInfo_i& item, unsigned int unequipIndex )
{
	CBaseItem* pUnEquipItem =  GetItemDelegate().GetEquipItem( item.uiID );
	if ( !pUnEquipItem )
	{
		D_ERROR("玩家%s卸载的装备%d 在装备管理器中找不到\n",GetNickName(),item.usItemTypeID );
		return;
	}

	float oldSpeed = GetSpeed();

	////如果有耐久度，那么减去这个装备所加的属性
	//if ( item.usWearPoint > 0 )
	//	pUnEquipItem->SubItemPropToPlayer( this, item );
	if (GetItemDelegate().IsEquipPosValid(pUnEquipItem->GetGearArmType()))
	{//如果属性加成有效，才去除属性加成
		ItemAttributeHelper::OnUnEquipItemAffectPlayer( this, item );
	}

	//玩家装备属于套装,则判断是否将套装的属性扣除
	if ( pUnEquipItem->GetSuitID() >0  )
		GetSuitDelegate().UnEquipSuitPart( pUnEquipItem->GetSuitID(), unequipIndex );

	//从装备代理中删除,放入包裹代理
	GetItemDelegate().UnEquipItem( pUnEquipItem );

	//卸载道具，影响玩家的OtherPlayerInfo
	//UnEquipItemAffectOtherPlayerInfo( unequipIndex );

	//给其他人更新血量魔量消息
	NoticeHPMPChange();

	if ( oldSpeed != GetSpeed() )
	{
		//更新玩家的速度
		NoticeSpeedChange();
	}
	//ReloadSecondProp();
	//ItemManagerSingle::instance()->CheckPlayerEquipItem(this);

	////减完以后更新组队成员信息
	//SendSelfInfoToOtherMembers();
}

void CPlayer::OnFashionItem( const ItemInfo_i& item, CBaseItem *pBase, unsigned int equipIndex )
{
	ACE_UNUSED_ARG( item );
	ACE_UNUSED_ARG( equipIndex );

	//放入装备管理器
	GetItemDelegate().OnFashionItem( pBase );

	/*
	NoticeEquipItemChange( equipIndex );
	MGNoticeEquipItem srvmsg;
	srvmsg.equipItem   = item;
	srvmsg.equipIndex  = equipIndex;
	srvmsg.equipPlayer  = GetFullID();

	CMuxMap* pPlayerMap = GetCurMap();
	if ( NULL != pPlayerMap )
	{
		pPlayerMap->NotifySurrounding<MGNoticeEquipItem>( &srvmsg, GetPosX(), GetPosY() );
	}*/
}

void CPlayer::OffFashionItem( const ItemInfo_i& item, CBaseItem *pBase, unsigned int unequipIndex )
{
	ACE_UNUSED_ARG( item );
	ACE_UNUSED_ARG( unequipIndex );

	//从装备代理中删除,放入包裹代理
	GetItemDelegate().OffFashionItem( pBase );

	/*
	MGNoticeEquipItem srvmsg;
	memset( &srvmsg, 0x0, sizeof(MGNoticeEquipItem) );
	srvmsg.equipIndex   = unequipIndex;
	srvmsg.equipPlayer  = GetFullID();
	CMuxMap* pPlayerMap = GetCurMap();
	if ( NULL != pPlayerMap )
	{
		pPlayerMap->NotifySurrounding<MGNoticeEquipItem>( &srvmsg, GetPosX(), GetPosY() );
	}
	*/
}

////取视野内玩家以及怪物数量；
//bool CPlayer::GetSurPlayerMonsterNum( unsigned int& mapPlayerNum, unsigned int& surPlayerNum, unsigned int& monsterNum )
//{
//	mapPlayerNum = 0, surPlayerNum = 0; monsterNum = 0;
//	CMuxMap* pMap = GetCurMap();
//	if ( NULL == pMap )
//	{
//		return false;
//	}
//
//	mapPlayerNum = pMap->GetMapPlayerNum();
//	return pMap->GetPosSurPlayerMonsterNum( GetPosX(), GetPosY(), surPlayerNum, monsterNum );
//}

bool CPlayer::IsHasDoneTask(unsigned int taskID )
{
	//判断这个任务是否存过盘
	NormalTaskProp* pTaskProp = CNormalTaskPropManager::FindTaskProp( taskID );
	if ( pTaskProp  )
	{
		if( pTaskProp->GetTaskType() == 12 )//如果为每日任务
		{
			//如果还没过期，则已经做过了，如果已经过期，则没有做过这个任务
			return !GetTaskRecordManager().CheckEveryDayTaskExpire( taskID );
		}
		else
		{
			return GetNewTaskRecordDelegate().IsHaveDoneTask( GetSex() , GetRace(), GetClass(), taskID );
		}
	}

	return false;
}

bool CPlayer::AddTask(unsigned int taskID )
{
	if ( FindPlayerTask( taskID ) != NULL )
	{
		D_ERROR("给玩家 %s 加入已经存在的任务 %d\n", GetNickName(),taskID );
		return false;
	}

	CNormalTask* pTask = NEW CNormalTask();
	if ( NULL == pTask )
	{
		return false;
	}
	if ( ! (pTask->InitNormalTask( this, taskID )) )
	{
		//建指定类型任务失败；
		delete pTask; pTask = NULL;
		return false;
	}

	TaskRecordInfo* pRecordInfo = GetTaskRecordManager().AddNewTaskRecord( pTask );
	//增加任务记录
	if (  pRecordInfo == NULL )//如果加入任务存盘失败
	{
		delete pTask; pTask =NULL;
		return false;
	}
	
	CLogManager::DoTaskChanage(this, taskID, (BYTE)NTS_TGT_NOT_ACHIEVE);//记日至

	//加到任务队列中去；
	m_arrTasks.push_back( pTask );

	//检查该任务是否需要限时
	pTask->CheckIsLimitTimeTask();

	D_DEBUG("给玩家%s加入任务%d成功\n",GetNickName(),taskID );

	//通知玩家接到了新任务；
	MGPlayerNewTask newtaskmsg;
#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4311 ) //"type cast" pointer truncation from ??* to ??
	newtaskmsg.taskID = (unsigned long) pTask->GetTaskID();//任务ID号，以后下线任务状态存盘，则改为静态的全局唯一号, ???，邓子建检查；
#pragma warning( pop )
#else
	newtaskmsg.taskID = (unsigned long) pTask->GetTaskID();//任务ID号，以后下线任务状态存盘，则改为静态的全局唯一号, ???，邓子建检查；
#endif
	newtaskmsg.taskTypeID = taskID;//任务类型ID；
	SendPkg<MGPlayerNewTask>( &newtaskmsg );//通知客户端接到新任务；

	if( pRecordInfo->taskTime > 0 )//>0 证明是记时任务,通知client开始记时
		GetTaskRecordManager().UpdateTaskRecordInfoToPlayer( *pRecordInfo );

	int  recordIndex =  GetTaskRecordManager().FindCommonTaskRecordIndex( taskID );
	if ( recordIndex != -1  )
		NoticePlayerTaskRdUpdate( false, *pRecordInfo , recordIndex );

	return true;
}

bool CPlayer::LoadTask( unsigned int taskID )
{
	CNormalTask* pTask = NEW CNormalTask();
	if ( NULL == pTask )
	{
		return false;
	}
	if ( ! (pTask->InitNormalTask( this, taskID )) )
	{
		//建指定类型任务失败；
		delete pTask; pTask = NULL;
		return false;
	}

	m_arrTasks.push_back( pTask );//加到任务队列中去；
	//通知玩家接到了新任务；
	MGPlayerNewTask newtaskmsg;
#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4311 ) //"type cast" pointer truncation from ??* to ??
	newtaskmsg.taskID = (unsigned long) pTask->GetTaskID();//任务ID号，以后下线任务状态存盘，则改为静态的全局唯一号, ???，邓子建检查；
#pragma warning( pop )
#else
	newtaskmsg.taskID = (unsigned long) pTask->GetTaskID();//任务ID号，以后下线任务状态存盘，则改为静态的全局唯一号, ???，邓子建检查；
#endif
	newtaskmsg.taskTypeID = taskID;//任务类型ID；

	SendPkg<MGPlayerNewTask>( &newtaskmsg );//通知客户端接到新任务；
	
	return true;
}

bool CPlayer::TryCounterEvent( COUNTER_TARGET_TYPE ctType, unsigned int reasonObjID, unsigned int reasonObjNum )
{
	//如果玩家不在组队状态下
	CNormalCounter* pCounter = NULL;
	if ( !(m_arrCounters[ctType].empty()) )
	{
		for ( vector<CNormalCounter*>::iterator iter=m_arrCounters[ctType].begin(); iter!=m_arrCounters[ctType].end(); )
		{
			pCounter = *iter;
			if ( NULL != pCounter )
			{
				pCounter->OnCounterEvent( this, ctType, reasonObjID, reasonObjNum );
			}
			++iter;
		}
	}

	//如果玩家组队了,并且此次计数是杀怪计数，
	if ( ctType == CT_NPC_KILL )
	{
		CGroupTeam* pGroupTeam =  GetSelfStateManager().GetGroupTeam();
		if ( pGroupTeam )
			pGroupTeam->TeamPlayerKillNPCEvent( GetFullID(),GetMapID(), reasonObjID, reasonObjNum );
	}


	return true;
}

bool CPlayer::TeamMemberKillNpcEvent( unsigned int reasonObjID, unsigned int reasonObjNum )
{
	CNormalCounter* pCounter = NULL;
	if ( !(m_arrCounters[CT_NPC_KILL].empty()) )
	{
		for ( vector<CNormalCounter*>::iterator iter=m_arrCounters[CT_NPC_KILL].begin(); iter!=m_arrCounters[CT_NPC_KILL].end(); )
		{
			pCounter = *iter;
			if ( NULL != pCounter )
			{
				pCounter->OnCounterEvent( this, CT_NPC_KILL, reasonObjID, reasonObjNum );
			}
			++iter;
		}
	}
	return true;
}


bool CPlayer::OnDropItemCounterEvent( const ItemInfo_i& dropItem )
{
	CNormalCounter* pCounter = NULL;
	vector<CNormalCounter*> &collectCounterVec = m_arrCounters[CT_COLLECT];

	if ( !collectCounterVec.empty() )
	{
		for ( vector<CNormalCounter*>::iterator iter=collectCounterVec.begin(); iter!=collectCounterVec.end(); ++iter )
		{
			pCounter = *iter;
			if ( NULL != pCounter )
			{
				pCounter->OnDropItemCounterEvent( this, CT_COLLECT, dropItem.usItemTypeID, dropItem.ucCount );
			}
		}
	}
	return true;
}

CNormalCounter* CPlayer::GetTaskCounter(unsigned int taskID,COUNTER_TARGET_TYPE ctType )
{
	CNormalCounter* pCounter = NULL;
	vector<CNormalCounter*> &collectCounterVec = m_arrCounters[ctType];
	if ( !collectCounterVec.empty() )
	{
		for ( vector<CNormalCounter*>::iterator iter=collectCounterVec.begin(); iter!=collectCounterVec.end(); ++iter )
		{
			pCounter = *iter;
			if ( NULL != pCounter )
			{
				if( pCounter->GetOwnerTaskID() == taskID )
					return pCounter;
			}
		}
	}
	return NULL;
}

///通知客户端任务失败倒计时；
bool CPlayer::NotifyTaskFailTime( UINT taskID, UINT failTime )
{
	CNormalTask* pTask = FindPlayerTask( taskID );
	if ( pTask )
	{
		MGPlayerTaskRecordUpdate taskRdInfoMsg;
		StructMemSet( taskRdInfoMsg, 0x0, sizeof(taskRdInfoMsg) );
		taskRdInfoMsg.taskRdInfo.taskID   = taskID;
		taskRdInfoMsg.taskRdInfo.taskTime = (unsigned int)time(NULL) + failTime;
		taskRdInfoMsg.taskRdInfo.taskState = (ETaskState)pTask->GetTaskStat();
		SendPkg<MGPlayerTaskRecordUpdate>( &taskRdInfoMsg );
	}

//	CNormalTask* pTask = FindPlayerTask( taskID );
//	if ( NULL == pTask )
//	{
//		//玩家身上没有该类型任务；
//		D_ERROR( "通知玩家任务失败倒计时，玩家身上无此任务!\n" );
//		return false;
//	}
//
//	MGPlayerTaskStatus taskstatusmsg;
//#ifdef ACE_WIN32
//#pragma warning( push )
//#pragma warning( disable: 4311 ) //"type cast" pointer truncation from ??* to ??
//	taskstatusmsg.taskID = (unsigned long) taskID;//任务ID号，以后下线任务状态存盘，则改为静态的全局唯一号, ???，邓子建检查；
//#pragma warning( pop )
//#else 
//	taskstatusmsg.taskID = (unsigned long) taskID;//任务ID号，以后下线任务状态存盘，则改为静态的全局唯一号, ???，邓子建检查；
//#endif
//	taskstatusmsg.taskStat = pTask->GetTaskStat();
//	taskstatusmsg.failTime = failTime;
//
//	SendPkg<MGPlayerTaskStatus>( &taskstatusmsg );

	return true;
}

///将玩家身上指定类型的任务置为特定状态(,???,如果玩家可以接同类任务多次怎么办?,邓子建检查)
bool CPlayer::SetTaskStat(unsigned int taskID, ETaskStatType inStat, bool logFlag)
{
	CNormalTask* pTask = FindPlayerTask( taskID );
	if ( NULL == pTask )
	{
		//玩家身上没有该类型任务；
		return false;
	}
	pTask->SetTaskStat( inStat );

	if(logFlag)
		CLogManager::DoTaskChanage(this, taskID, (BYTE)inStat);//记日至

	//任务的状态改变了
	GetTaskRecordManager().OnTaskStateChange(pTask, inStat );

	MGPlayerTaskStatus taskstatusmsg;
#ifdef ACE_WIN32
#pragma warning( push )
#pragma warning( disable: 4311 ) //"type cast" pointer truncation from ??* to ??
	taskstatusmsg.taskID = (unsigned long) taskID;//任务ID号，以后下线任务状态存盘，则改为静态的全局唯一号, ???，邓子建检查；
#pragma warning( pop )
#else 
	taskstatusmsg.taskID = (unsigned long) taskID;//任务ID号，以后下线任务状态存盘，则改为静态的全局唯一号, ???，邓子建检查；
#endif
	taskstatusmsg.taskStat = inStat;
	taskstatusmsg.failTime = 0;

	SendPkg<MGPlayerTaskStatus>( &taskstatusmsg );

	return true;
}

bool CPlayer::DeleteTask( unsigned int taskID )
{
	//删除此时的数据
	GetTaskRecordManager().OnDeleteTask( taskID );

	//清除绑定的任务
	CNormalTask* pNormalTask = NULL;
	for ( vector<CNormalTask*>::iterator iter=m_arrTasks.begin(); iter!=m_arrTasks.end(); ++iter )
	{
		if ( ( NULL != *iter ) && ( taskID  == (*iter)->GetTaskID() ) )
		{
			pNormalTask = *iter;
			m_arrTasks.erase(iter);
			break;
		}
	}

	if ( !pNormalTask )
		return false;

	//如果是限时任务，完成功能后，把从任务从限时任务管理器中删除
	if ( pNormalTask->IsLimitTimeTask() )
		CLimitTimeTaskManagerSingle::instance()->RemoveLimitTimeTask( this,taskID );

	//如果是答题任务，删除任务则关闭答题过程
	if ( pNormalTask->GetTaskProp()->GetCountType() == CT_ANSWER )
		GetPlayerOLTestPlate().DropOLTest();

	//清除绑定的计数器
	COUNTER_TARGET_TYPE ctType = pNormalTask->GetTaskCountType();
	int taskType = (int)ctType;
	if ( taskType <0  || taskType >= CT_INVALID )
	{
		D_ERROR("获取%d 任务类型错误:%d\n", taskID, ctType);
	}
	else
	{
		CNormalCounter* pCounter = NULL;
		if ( !(m_arrCounters[ ctType ].empty()) )
		{
			for ( vector<CNormalCounter*>::iterator iter=m_arrCounters[ctType].begin(); iter!=m_arrCounters[ctType].end(); ++iter )
			{
				pCounter = *iter;
				if ( NULL != pCounter && pCounter->GetOwnerTaskID() == taskID )
				{
					delete pCounter; pCounter = NULL;
					iter = m_arrCounters[ctType].erase( iter );//已满足条件的计数器即时删去；
					break;
				}
			}
		}
	}
	delete pNormalTask; pNormalTask = NULL;

	
	//丢弃与任务相关的道具
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::DeleteTask, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_3_Pkgs& Pakage = playerInfo->pkgInfo;
	for ( unsigned int i = NORMAL_PKG_SIZE ; i< PACKAGE_SIZE; i++ )//在任务背包中检查
	{
		ItemInfo_i& itemInfo = Pakage.pkgs[i];
		if( itemInfo.uiID != PlayerInfo_3_Pkgs::INVALIDATE )
		{
			CBaseItem* pItem = GetItemDelegate().GetPkgItem( itemInfo.uiID );
			if ( pItem && pItem->GetTaskID() == taskID )
				OnDropItem( itemInfo.uiID );
		}
	}

	//检测是否为跟随任务
	CMonster* pMonster = GetFollowingMonster();
	if ( pMonster )//如果是跟随任务
	{
		pMonster->OnPlayerDropTask( taskID );//通知AI系统,丢弃任务
	}

	return true;
}

//玩家使用技能，校验基本属性点
bool CPlayer::UseSkillBaseAttCheck( CMuxSkillProp* pSkillProp )
{
	if ( NULL == pSkillProp )
	{
		D_DEBUG( "%s使用技能，输入技能属性指针空\n\n", GetAccount() );
		return false;
	}

	unsigned int skillID = pSkillProp->m_skillID;

	if ( GetStrength() < pSkillProp->m_upgradeStr )
	{
		D_DEBUG( "%s力量不符合要求,不能使用技能%d(拥有%d，需要%d)\n", GetAccount(), skillID, GetStrength(), pSkillProp->m_upgradeStr );
		return false;
	}
	if ( GetAgility() < pSkillProp->m_upgradeAgi )
	{
		D_DEBUG( "%s敏捷不符合要求,不能使用技能%d(拥有%d，需要%d)\n", GetAccount(), skillID, GetAgility(), pSkillProp->m_upgradeAgi );
		return false;
	}
	if (  GetIntelligence() < pSkillProp->m_upgradeInt )
	{
		D_DEBUG( "%s智力不符合要求,不能使用技能%d(拥有%d，需要%d)\n", GetAccount(), skillID, GetIntelligence(), pSkillProp->m_upgradeInt );
		return false;
	}
	if (  GetSpirit() < pSkillProp->m_upgradeSpi )
	{
		D_DEBUG( "%s精神不符合要求,不能使用技能%d(拥有%d，需要%d)\n", GetAccount(), skillID, GetSpirit(), pSkillProp->m_upgradeSpi );
		return false;
	}
	if ( GetVitality() < pSkillProp->m_upgradeVit )
	{
		D_DEBUG( "%s体质不符合要求,不能使用技能%d(拥有%d，需要%d)\n", GetAccount(), skillID, GetVitality(), pSkillProp->m_upgradeVit );
		return false;
	}

	//验证是否装备武器
	unsigned int weaponSkill = pSkillProp->m_weaponSkill;
	bool fooBool = false;
	unsigned int equipPos = 0, fooPos2 = 0;
	
	switch(weaponSkill)
	{
	case 0:
		break;
	case 3://case 3的情况下case 1和case 2都要走到
	case 1:
		{
			bool isEquipPosOK = GetDefaultEquipPos( E_FIRSTHAND, equipPos, fooBool, fooPos2 );
			if ( !isEquipPosOK )
			{
				D_ERROR("UseEquipRule::Execute失败\n");
				return false;
			}
			ItemInfo_i* pTmpItem = GetEquipByPos( equipPos );
			if ( NULL == pTmpItem )
			{
				return false;
			}
			if (PlayerInfo_3_Pkgs::INVALIDATE == pTmpItem->uiID )
			{
				return false;
			}
			if ( 1 == weaponSkill )
			{
				break;
			}
		}
	case 2:
		{
			bool isEquipPosOK = GetDefaultEquipPos( E_SECONDHAND, equipPos, fooBool, fooPos2 );
			if ( !isEquipPosOK )
			{
				D_ERROR("UseEquipRule::Execute失败\n");
				return false;
			}
			ItemInfo_i* pTmpItem = GetEquipByPos( equipPos );
			if ( NULL == pTmpItem )
			{
				return false;
			}
			if (PlayerInfo_3_Pkgs::INVALIDATE == pTmpItem->uiID )
			{
				return false;
			}
			break;
		}
	default:
		break;
	}

	return true;
}

bool CPlayer::UpgradeAssistSkill( TYPE_ID skillID) //
{
	CMuxPlayerSkill* pSkill = FindMuxSkill( skillID );
	if( NULL == pSkill )
	{
		D_WARNING("发送了错误的辅助技能升级ID %d\n", skillID);
		return false;
	}

	//step1 升级的不能是SP技能
	if( pSkill->IsSpSkill() )
	{
		D_WARNING("要升级的辅助技能是SP技能 ID:%d\n",skillID );
		return false;
	}

	//step2 升级的技能不能不是辅助技能
	if( !pSkill->IsAssistSkill() )
	{
		D_WARNING("要升级的辅助技能不能不是辅助技能 ID:%d\n", skillID);
		return false;
	}

	//step3 如果要升级的技能已经是满级,既没有后续技能也是不能升的
	TYPE_ID nextSkillID = pSkill->GetNextSkillID();
	if( nextSkillID == 0 )
	{
		D_WARNING("要升级的技能下一级技能无ID号 要升级的技能ID:%d\n", skillID );
		return false;
	}

	//step4 找到升级的技能属性
	CMuxSkillProp* pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( nextSkillID );
	if( NULL == pSkillProp )
	{
		D_ERROR("要升级的技能属性找不到 找不到的技能属性ID号:%d\n", nextSkillID );
		return false;
	}

	//step5 如果升级的属性是SP技能或者不是辅助技能,也是要失败的
	if( pSkillProp->IsSpSkill() || !pSkillProp->IsAssistSkill() )
	{
		D_WARNING("要升级的技能不符合要求 升级的技能ID号:%d\n", pSkillProp->m_skillID );
		return false;
	}

	//玩家是否达到升级条件
	if ( pSkillProp->m_upgradeExp > GetExp() ||
		pSkillProp->m_upgradeMoney > GetSilverMoney() || //双币种判断，按姚斌：所有条件判定都使用银币
		pSkillProp->m_upgradeStr > GetStrength() ||
		pSkillProp->m_upgradeVit > GetVitality() ||
		pSkillProp->m_upgradeAgi > GetAgility() ||
		pSkillProp->m_upgradeSpi > GetSpirit() ||
		pSkillProp->m_upgradeInt > GetIntelligence() ||
		pSkillProp->m_subExp > GetExp()
		)
	{
		D_DEBUG("人物属性未达到升级要求\n");
		return false;
	}
		
	//commented by shenchenkai
	////替换原来的技能属性
	//pSkill->SetMuxSkillProp( pSkillProp );
	//技能升级后保留原有技能
	if ( !AddSkillByID(nextSkillID) )
	{
		D_DEBUG("升级技能失败\n");
		return false;
	}
	
	//给客户端发送替换消息
	MGUpgradeSkillResult upgradeMsg;
	upgradeMsg.errorCode = UPGRADE_SUCCESS;
	upgradeMsg.oldSkillID = skillID;
	//upgradeMsg.newSkillID = pSkill->GetSkillID();
	upgradeMsg.newSkillID = nextSkillID;
	upgradeMsg.phySkillPoint = GetPhyAllPoint();
	upgradeMsg.magSkillPoint = GetMagAllPoint();
	SendPkg<MGUpgradeSkillResult>(&upgradeMsg );
	D_DEBUG("辅助技能升级成功 从%d 替换成 %d \n", skillID, upgradeMsg.newSkillID );
	return true;
}


EUpgradeSkillResult CPlayer::CheckUpgradeBattleSkill(TYPE_ID skillID,CMuxPlayerSkill*& pOutSkill, CMuxSkillProp*& outProp)
{
	CMuxPlayerSkill* pSkill = FindMuxSkill(skillID);
	if( NULL == pSkill )
	{
		D_WARNING("发送了错误的战斗技能升级ID %d\n", skillID);
		return INVALID_ID_ERROR;
	}

	//step1 升级的不能是SP技能
	if( pSkill->IsSpSkill() )
	{
		D_WARNING("要升级的战斗技能是SP技能 ID:%d\n",skillID );
		return INVALID_ID_ERROR;
	}

	//step2 升级的技能不能不是辅助技能
	unsigned int skillType = pSkill->GetSkillType();
	//非战斗技能也可升级，10.06.18,if( !(skillType == TYPE_PHYSICS || skillType == TYPE_MAGIC) )
	//{
	//	D_WARNING("要升级的辅助技能不能不是战斗技能 ID:%d\n", skillID);
	//	return INVALID_ID_ERROR;
	//}

	//step3 如果要升级的技能已经是满级,既没有后续技能也是不能升的
	TYPE_ID nextSkillID = pSkill->GetNextSkillID();
	if( nextSkillID == 0 )
	{
		D_WARNING("UpgradeBattleSkill 要升级的技能下一级技能无ID号 要升级的技能ID:%d\n", skillID );
		return MAX_LEVEL_SKILL_ERROR;
	}

	//step4 找到升级的技能属性
	CMuxSkillProp* pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( nextSkillID );
	if( NULL == pSkillProp )
	{
		D_ERROR("要升级的技能属性找不到 找不到的技能属性ID号:%d\n", nextSkillID );
		return INVALID_ID_ERROR;
	}

	//step5 如果升级的属性是SP技能或者不是辅助技能,也是要失败的
	if( pSkillProp->IsSpSkill() /*非战斗技能也可升级，10.06.18,|| !(pSkillProp->m_skillType == TYPE_PHYSICS || pSkillProp->m_skillType == TYPE_MAGIC)*/ )
	{
		D_WARNING("要升级的技能不符合要求 升级的技能ID号:%d\n", pSkillProp->m_skillID );
		return INVALID_ID_ERROR;
	}

	/*if( pSkillProp->m_upgradeLevel > GetLevel() )
	{
		D_WARNING("玩家还不到升级技能的等级\n");
		return NOT_ENOUGH_LEVEL;
	}*/

	//step6 看看玩家是否有足够的点数进行升级
	/*int upgradepoint = pSkill->GetUpgradePoints();
	int alloPt = 0;
	if( pSkillProp->m_skillType == TYPE_PHYSICS )
	{
		alloPt = GetPhyAllPoint();
	}else if( pSkillProp->m_skillType == TYPE_MAGIC )
	{
		alloPt = GetMagAllPoint();
	}
	if( alloPt < upgradepoint )
	{
		D_DEBUG("人物可分配技能点 %d  升级技能所需技能点 %d 不能升级\n",alloPt, upgradepoint );
		return NOT_ENOUGH_POINT_ERROR;
	}*/

	//玩家是否达到升级条件
	if ( pSkillProp->m_upgradeExp > GetExp() ||
		pSkillProp->m_upgradeMoney > GetSilverMoney() || //双币种判断，按姚斌：所有条件判定都使用银币
		pSkillProp->m_upgradeStr > GetStrength() ||
		pSkillProp->m_upgradeVit > GetVitality() ||
		pSkillProp->m_upgradeAgi > GetAgility() ||
		pSkillProp->m_upgradeSpi > GetSpirit() ||
		pSkillProp->m_upgradeInt > GetIntelligence())
	{
		D_DEBUG("人物属性未达到升级要求\n");
		return NOT_ENOUGH_LEVEL;
	}

	outProp = pSkillProp;
	pOutSkill = pSkill;
	return UPGRADE_SUCCESS;
}


bool CPlayer::UpgradeBattleSkill( TYPE_ID skillID)
{
	CMuxSkillProp* pSkillProp = NULL;
	CMuxPlayerSkill* pSkill = NULL;
	EUpgradeSkillResult errorCode = CheckUpgradeBattleSkill(skillID, pSkill, pSkillProp);

	if( errorCode != UPGRADE_SUCCESS )
	{
		MGUpgradeSkillResult errMsg;
		errMsg.errorCode = errorCode;
		errMsg.oldSkillID = skillID;
		SendPkg<MGUpgradeSkillResult>(&errMsg);
		return false;
	}
	
	if( NULL == pSkillProp || NULL == pSkill )
	{
		return false;
	}

	//先减去技能点
	/*if( pSkillProp->m_skillType == TYPE_PHYSICS )
	{
		m_fullPlayerInfo.baseInfo.usPhysicsAllotPoint -= pSkill->GetUpgradePoints();
	}else{
		m_fullPlayerInfo.baseInfo.usMagicAllotPoint -= pSkill->GetUpgradePoints();
	}*/

	//消耗金钱
	if ( IsUseGoldMoney() )
	{
		SubGoldMoney(pSkillProp->m_subMoney);//双币种判断,这里不看是否有足够的钱扣吗？？？by dzj, 10.07.15;
	} else {
		SubSilverMoney(pSkillProp->m_subMoney);//双币种判断
	}

	//消耗经验
	unsigned int oldExp = m_fullPlayerInfo.baseInfo.dwExp;
	m_fullPlayerInfo.baseInfo.dwExp -= pSkillProp->m_subExp;

	MGRoleattUpdate updatemsg;
	updatemsg.bFloat = false;
	updatemsg.changeTime = 0;
	updatemsg.lChangedPlayerID = GetFullID();
	updatemsg.nAddType = 0;
	updatemsg.nOldValue = oldExp;
	updatemsg.nNewValue = m_fullPlayerInfo.baseInfo.dwExp;
	updatemsg.nType = RT_EXP;
	SendPkg<MGRoleattUpdate>(&updatemsg);

	//替换原来的技能属性
	pSkill->SetMuxSkillProp( pSkillProp );

	////技能升级后保留原有技能
	//if ( !AddSkillByID(skillID) )
	//{
	//	D_DEBUG("升级技能失败\n");
	//	return false;
	//}

	MGUpgradeSkillResult upMsg;
	upMsg.errorCode = errorCode;
	upMsg.oldSkillID = skillID;
	upMsg.newSkillID = pSkill->GetSkillID();
	upMsg.phySkillPoint = GetPhyAllPoint();
	upMsg.magSkillPoint = GetMagAllPoint();
	SendPkg<MGUpgradeSkillResult>( &upMsg );

	CLogManager::DoSkillLevelUp(this, (pSkillProp->m_skillType == TYPE_PHYSICS)? LOG_SVC_NS::ST_PHYSICS:LOG_SVC_NS::ST_MAGIC, skillID, skillID, upMsg.newSkillID);//记日至

#ifdef NEW_SKILL_UPDATE
	UpdateSkillInfoToDB();
#endif

	return true;
}

int CPlayer::CheckItemCanRepair(ItemInfo_i *pItem, unsigned int &uiLeftMoeny, unsigned int &newWear)
{
	//返回值参见MGRepairWearByNPC::eRepairResult
	/*enum eRepairResult
	{
		TARGET_DO_NOT_REPAIR = 0,//不能修复或者可修复但耐久度满
		NO_ENOUGH_REPAIR_MONEY = 1,//没有足够的修复费用
		OTHER_ERROR = 2,//物品不存在或者其他错误
		OK = 3//修复成功
	};*/
	
	if(NULL == pItem)//参数无效
		return (int)MGRepairWearByNPC::OTHER_ERROR;

	CItemPublicProp *pTargetPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(pItem->usItemTypeID);
	if(NULL == pTargetPublicProp)//没有找到对应物品
	{
		std::stringstream sysBuf;		
		sysBuf <<"类型编号="  << pItem->usItemTypeID  <<" 的道具详细信息不存在\n";
		SendSystemChat( sysBuf.str().c_str() );
		return (int)MGRepairWearByNPC::OTHER_ERROR;
	}

	if(pTargetPublicProp->m_repairMode != 0 )//物品本身不可修复
		return (int)MGRepairWearByNPC::TARGET_DO_NOT_REPAIR;

	unsigned int uiRepairMoney = CRepairWairRuleManagerSingle::instance()->CalcRepairWearMoney(pTargetPublicProp->m_maxWear - pItem->usWearPoint,pTargetPublicProp->m_itemLevel, pTargetPublicProp->m_itemMassLevel, pItem->ucLevelUp);
	if(uiRepairMoney == 0)//物品耐久度满
		return (int)MGRepairWearByNPC::TARGET_DO_NOT_REPAIR;

	if(uiLeftMoeny < uiRepairMoney)//没有足够修复费用
		return (int)MGRepairWearByNPC::NO_ENOUGH_REPAIR_MONEY;

	uiLeftMoeny -=  uiRepairMoney;
	newWear = pTargetPublicProp->m_maxWear;

	return (int)MGRepairWearByNPC::OK;
}


void CPlayer::OnEnterPKArea()
{
	GetSelfStateManager().SetPKArea( true );
	SendSystemChat("进入PK区");
}

void CPlayer::OnExitPKArea()
{
	GetSelfStateManager().SetPKArea( false );
	SendSystemChat("离开PK区");
}

void CPlayer::OnEnterFightArea()
{
	GetSelfStateManager().SetFightArea( true );
	SendSystemChat("离开安全区");
}

void CPlayer::OnExitFightArea()
{
	GetSelfStateManager().SetFightArea( false );
	SendSystemChat("进入安全区");
}

void CPlayer::OnEnterDunGeonPreArea()
{
	if ( IsRideState() )
	{
		CRideState& rideState = GetSelfStateManager().GetRideState();
		//仅当骑乘状态激活时，且玩家是队长时
		if ( rideState.IsActive() &&  rideState.GetPlayerRideType() == CRideState::E_LEADER )
		{
			RideTeam* pTeam = rideState.GetRideTeam();
			if ( pTeam )
			{
				pTeam->DissolveRideTeam();
				RideTeamManagerSingle::instance()->RecoverRideTeam( pTeam ); 
			}
		}
	}
	GetSelfStateManager().SetDunGeonPreArea( true );

	SendSystemChat("进入不可骑乘区");
}

void CPlayer::OnExitDunGeonPreArea()
{
	GetSelfStateManager().SetDunGeonPreArea( false );

	SendSystemChat("离开不可骑乘区");	
}

void CPlayer::OnEnterNewTaskArea( CNormalScript* pScript )
{
	if ( NULL == pScript )
	{
		//enter area without script;
		m_pAreaScript = NULL;
		return;
	}

	m_pAreaScript = pScript;
	m_pAreaScript->OnAreaChat( this, 0 );

	return;
}

void CPlayer::OnLeaveOldTaskArea()
{
	if ( !m_pAreaScript )
	{
		//orgin area without script;
		return;
	}

	SetAreaChatStage(0);
	m_pAreaScript->ExtNpcCloseUiInfo();
	m_pAreaScript = NULL;//reset area script;

	return;
}


//进入地图时获取登陆点的地表属性
void CPlayer::GetEnterMapPosProperty()
{
	CMuxMap* pMap = GetCurMap();
	if ( pMap )
	{
		unsigned int dumpAreaID = 0;
		bool isPK = true;
		bool isFight = true;
		bool isPreArea = false;
		pMap->GetPosProperty( GetPosX(), GetPosY(), dumpAreaID, isPK, isFight, isPreArea );

		GetSelfStateManager().SetPKArea( isPK );
		GetSelfStateManager().SetFightArea( isFight );
		GetSelfStateManager().SetDunGeonPreArea( isPreArea );
	}
}

#ifdef HAS_PATH_VERIFIER
void CPlayer::NotifyBlockQuadInfo(void)
{
	if(NULL != m_pMap)
	{
		std::vector<int> blockQuads;
		m_pMap->GetEnableBlockQuads(blockQuads);

		size_t blockQuadNum = blockQuads.size();
		if(blockQuadNum > 0)
		{
			MGBlockQuadStateChange msg;
			for(size_t i = 0; i < blockQuadNum; i++)
			{
				msg.stateChange.regionID = blockQuads[i];
				msg.stateChange.val = true;

				SendPkg<MGBlockQuadStateChange>(&msg);
			}
		}
	}
}
#endif/*HAS_PATH_VERIFIER*/

void CPlayer::RegisterDeferredOffline( bool isQuickDel/*是否快速删除，若gate已不存在此玩家，则应快速删除而不必延迟等待*/ )
{
	if ( IsInDelDelQue() )
	{
		D_ERROR( "RegisterDeferredOffline,试图重复删玩家%s，忽略后一次删除!\n", GetAccount() );
		return;
	}

	//检测是否在交易中
	LeaveAffectTrade();
	//跳地图影响善恶值的监视
	CEvilTimeControlManagerSingle::instance()->RemoveGoodEvilControl( GetDBUID() );
	//玩家离线,检测是否需要退出骑乘队伍
	RideTeamManagerSingle::instance()->OnPlayerLeaveSrv( this );
	//玩家离线，检测是否需要退出组队
	GroupTeamManagerSingle::instance()->OnGroupPlayerExitMap( this );
	//移除存放在MapSrv中的道具数据
	ItemManagerSingle::instance()->RemovePlayerItem( this );
	//删除玩家存放在服务器的计时任务信息
	CLimitTimeTaskManagerSingle::instance()->OnPlayerLeaveSrv( this );
	//当种族玩家离开该地图的种族管理器
	AllRaceMasterManagerSingle::instance()->OnPlayerLeaveMap( this );
	//排行榜玩家离线
	//RankEventManagerSingle::instance()->OnPlayerLeaveMap( this );
	RankEventManager::OnPlayerLeaveMap( this );
	
#ifdef OPEN_PUNISHMENT
	CPunishmentManager::PunishPlayerOffline( this );		//天谴玩家下线的话需要及时通知在其他mapsrv
#endif //OPEN_PUNISHMENT

	SetLeaveDelay();//置玩家离开保留标记；

	m_offlineTime = ACE_OS::gettimeofday();

	if ( !isQuickDel )
	{
		//非快速删除；
		D_DEBUG( "非快速删除,%s\n", GetAccount() );
		CPlayerManager::RegisterDeferredOffline(this);
	} else {
		//快速删除；
		D_DEBUG( "快速删除,%s\n", GetAccount() );
		ACE_Time_Value currentTime = ACE_OS::gettimeofday();
		ProcessDeferredOffline( false, currentTime + ACE_Time_Value(11)/*以便调用能真正发挥作用*/ );
	}

	return;
}

///尝试执行延迟删除，如果确实执行了删除操作，则返回真，以便调用者不再遍历，否则反之；
bool CPlayer::ProcessDeferredOffline( bool isForceOffline, const ACE_Time_Value& currentTime)
{
	TRY_BEGIN;

	ACE_Time_Value internalTime = currentTime - m_offlineTime;
	if ( isForceOffline || ( internalTime.sec() >= 10 ) )
	{
		D_DEBUG( "CPlayer::ProcessDeferredOffline,延迟队列中%s被遍历删除\n", GetAccount() );

		if ( IsSummonPet() )
		{
			ClosePet();//下线召回宠物
		}

		/////////////////////////////////////////////////////////
		//copy...
		if ( NULL != m_pUnion )
		{
			m_pUnion->UnregisterPlayer(this);//取消工会注册
		}

		//当从gatesrv上都删掉的话表示玩家真正离线，这时记录他的BUFF  08。10。08
		OnBuffLeave();  
		//...copy
		/////////////////////////////////////////////////////////

		//离线更新DB端的信息
		OfflineProc( INFO_OFFLINE_UPDATE, true/*下线永远清副本信息*/ );
		
		return true;
	}
	
	return false;
	TRY_END;
	return false;
}

void CPlayer::CreateRogueState( unsigned int rogueTime)
{
#ifdef _DEBUG
	D_DEBUG("流氓状态BUFF开始 \n");
#endif //_DEBUG

	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(流氓状态)

	if( IsHaveRogueState() )
	{
		if( m_pRogueBuff )
			m_pRogueBuff->SetDelFlag();
	}

	if ( NULL == g_poolManager )
	{
		return;
	}
	CScampBuff* pStateBuff = g_poolManager->RetriveScampBuff();
	if ( NULL == pStateBuff )
	{
		return;
	}
	pStateBuff->Initialize( this, rogueTime );
	m_pRogueBuff = pStateBuff;
	m_pRogueBuff->BuffStart();
}


void CPlayer::CreateWeakBuff( unsigned int weakTime )
{
	if( IsWeak() )
	{
		EndWeakBuff();
	}

	PlayerID createID;
	createID.wGID = 0;
	createID.dwPID = 0;
	CreateMuxBuffByID( WEAK_BUFF_ID, weakTime, createID );

	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(虚弱状态)
	m_fullPlayerInfo.stateInfo.SetWeak( weakTime );
}


#ifdef OPEN_PUNISHMENT
bool CPlayer::GivePunishTask( int killCounter )
{
	//天谴任务没开始不能给予任务
	if( !CPunishmentManager::GetPunishState() )
	{
		return false;
	}

	if( IsHavePunishTask() )
	{
		D_ERROR("CPlayer::GivePunishTask() 已经拥有了天谴任务\n");
		return false;
	}

	//给客户端发消息
	MGPlayerNewTask taskMsg;
	taskMsg.taskID = PUNISH_TASK_ID;
	taskMsg.taskTypeID = PUNISH_TASK_ID;
	SendPkg<MGPlayerNewTask>( &taskMsg );

	
	//打死一个就算完成任务,状态
	if( killCounter >= 1)
	{
		MGPlayerTaskStatus gateMsg;
		gateMsg.taskID = PUNISH_TASK_ID;
		gateMsg.taskStat = NTS_TGT_ACHIEVE;
		gateMsg.failTime = 0;
		SendPkg<MGPlayerTaskStatus>( &gateMsg );
	}

	//计数
	MGPlayerTaskRecordUpdate updateCounter;
	StructMemSet( updateCounter, 0, sizeof(MGPlayerTaskRecordUpdate) );
	updateCounter.taskRdInfo.taskID = PUNISH_TASK_ID;
	updateCounter.taskRdInfo.processData = killCounter;
	SendPkg<MGPlayerTaskRecordUpdate>( &updateCounter );

	//设置天谴任务为接受状态
	m_punishTask.SetAcceptPunishTask( true );
	m_punishTask.SetKillCounter( killCounter );
	D_INFO("玩家%s获得天谴任务计数为%d\n",GetNickName(), killCounter );
	return true;
}


bool CPlayer::GiveUpPunishTask()		//放弃天谴任务
{
	//如果没有接天谴任务不能放弃
	if( !IsHavePunishTask() )
		return false;

	m_punishTask.Clear();
	return true;
}


int CPlayer::GetKillCounter()
{
	if( IsHavePunishTask() )
	{
		return m_punishTask.GetKillCounter();
	}
	return 0;
}


void CPlayer::CheckPunishTask()
{
	if( !IsHavePunishTask() )
		return;

	//如果天谴活动已经结束，表明天谴任务已经失败
	if( !CPunishmentManager::GetPunishState() )
	{
		MGPlayerTaskStatus gateMsg;
		gateMsg.taskID = PUNISH_TASK_ID;
		gateMsg.taskStat = NTS_FAIL;
		gateMsg.failTime = 0;
		SendPkg<MGPlayerTaskStatus>( &gateMsg );

		m_punishTask.Clear();
	}
}

bool CPlayer::IsHavePunishTask()	//是否拥有天谴任务
{
	return m_punishTask.IsAcceptPunishTask();
}


int CPlayer::FinishPunishTask()	//结束天谴任务
{
	if( !IsHavePunishTask() )
		return 0;

	
	MGPlayerTaskStatus gateMsg;
	gateMsg.taskID = PUNISH_TASK_ID;
	gateMsg.taskStat = NTS_FINISH;
	gateMsg.failTime = 0;
	SendPkg<MGPlayerTaskStatus>( &gateMsg );

	int killCounter = m_punishTask.GetKillCounter();
	m_punishTask.Clear();
	return killCounter;
}
#endif	//OPEN_PUNISHMENT


bool CPlayer::IsWeak()
{
	return (MAP_PS_WEAK == m_fullPlayerInfo.stateInfo.m_CurBaseState );
}


//获取流氓状态的时间 
unsigned int CPlayer::GetRogueStateTime()
{
	if( IsHaveRogueState() )
	{
		return (unsigned int)m_pRogueBuff->GetLeftSecond();
	}
	return 0;
}


bool CPlayer::CheckCd( TYPE_ID publicCdType, const ACE_Time_Value& cdTime )
{
	return m_cdManager.CheckCooldown( publicCdType, cdTime );
}


bool CPlayer::GetWeaponSkill(int& skillID, int& skillRatio )
{
	skillID = 0;
	skillRatio = 0;

	unsigned int weaponType = 0;
	unsigned int tmpType2 = 0;
	bool isHaveWeapon = GetEquipWeaposType( weaponType, tmpType2 );
	if ( !isHaveWeapon )
	{
		return false;
	}

	if ( 0 == weaponType )
	{
		D_ERROR( "CPlayer::GetWeaponSkill，GetEquipWeaposType返回有武器，但weaponType却为0，玩家%s\n", GetAccount() );
		return false;
	}

	WeaponProperty* pProp = CWeaponPropertyManagerSingle::instance()->GetSpecialPropertyT( weaponType );
	if ( pProp )
	{
		skillID = pProp->SkillID;
		skillRatio = pProp->SkillRatio;
		return true;
	}
	return false;
}


void CPlayer::InitUnionAttributeEffect()
{
	
}

void CPlayer::OnKillCollectNpc( CMonster* pDeadNpc )
{
	if( NULL == pDeadNpc )
	{
		D_ERROR( "CPlayer::OnKillCollectNpc, NULL == pDeadNpc\n" );
		return;
	}

	long money = 0;
	MonsterDropItemSetSingle::instance()->RunMonsterDropItemSet( this, pDeadNpc->GetMonsterDropSetID(), m_collectInfo.mCollectItemEleVec, money ); 
	
	//把采集道具信息发给客户端
	MGCollectTaskRst gateMsg;
	StructMemSet( gateMsg, 0, sizeof( MGCollectTaskRst ) );
	gateMsg.collectRst.silverMoney = money;//双币种判断
	gateMsg.collectRst.goldMoney = 0;//双币种判断
	gateMsg.collectRst.itemSize = (UINT)m_collectInfo.mCollectItemEleVec.size();
	for( size_t i =0; i < m_collectInfo.mCollectItemEleVec.size(); ++i )
	{
		if ( i >= ARRAY_SIZE(gateMsg.collectRst.ItemArr) )
		{
			D_ERROR( "CPlayer::OnKillCollectNpc, 生成的采集道具数量%d越过MGCollectTaskRst界(%d)\n"
				, m_collectInfo.mCollectItemEleVec.size(), ARRAY_SIZE(gateMsg.collectRst.ItemArr) );
			return;
		}
		gateMsg.collectRst.ItemArr[i] = m_collectInfo.mCollectItemEleVec[i].GetItemEleInfo();
		gateMsg.collectRst.ItemArr[i].uiID = (UINT)(i+1);
	}
	m_collectInfo.collectMoney = money;
	SendPkg<MGCollectTaskRst>( &gateMsg );
}


bool CPlayer::OnPickOneCollectItem( unsigned int index )
{
	if( index > m_collectInfo.mCollectItemEleVec.size() )
		return false;

	if( index == 0   )
	{
		if( m_collectInfo.collectMoney != 0  )
		{
			AddSilverMoney( m_collectInfo.collectMoney );//双币种判断
			m_collectInfo.collectMoney = 0;
			MGOnePlayerPickItem gateMsg;
			gateMsg.pkgIndex = 0;
			gateMsg.itemID = 0;
			gateMsg.bIsPickMoney = true;
			SendPkg<MGOnePlayerPickItem>( &gateMsg );
		}
		return true;
	}else
	{
		index--;						
	}

	int errNo = 0;
	bool bPickSuccess = true;
	//bool isCanPickTaskItem = true;
	do 
	{
		CItemPkgEle& itemEle =  m_collectInfo.mCollectItemEleVec[index];
		
		if ( itemEle.GetItemTypeID()  == 0 )
		{
			break;
		}

		//如果道具处于锁定状态时不能被拾取的
		if ( itemEle.IsLock() )
		{
			break;
		}

		//如果该物品已经有了所有者，比较所有者的ID是否正确
		if ( itemEle.IsHaveOwner() && itemEle.GetOwnerID() != GetFullID() )
		{
			break;
		}

		//如果是任务物品，必须身上有这个任务才能拾取道具
		unsigned taskID = itemEle.GetTaskID();
		if ( taskID != 0  )
		{
			bool isCanPickTaskItem = true;
			
			//根据任务获取对应的计数器
			CNormalCounter* pCounter = GetTaskCounter( itemEle.GetTaskID() ,CT_COLLECT );
			if ( pCounter == NULL )
			{
				isCanPickTaskItem = false;
			}

			//如果计数器满了，也无法拾取道具
			if ( pCounter && pCounter->IsReachMaxCounter() )
			{
				isCanPickTaskItem = false;
			}

			//如果拾取任务物品失败
			if ( !isCanPickTaskItem )
			{
				errNo = E_PKG_UNUSEITEM;

				//拾取任务道具失败
				MGPickItemFaild srvmsg;
				srvmsg.itemID = index + 1;
				srvmsg.result = (EPickItemResult)errNo;
				SendPkg<MGPickItemFaild>( &srvmsg );


				//如果不能拾取该物品的人拾取了，则该任务道具要消失掉
				MGOnePlayerPickItem pickMsg;
				pickMsg.itemID = index + 1;
				pickMsg.pkgIndex = 0;
				pickMsg.bIsPickMoney = false;
				SendPkg<MGOnePlayerPickItem>( &pickMsg );

				//删除道具
				RemoveCollectItemEle( itemEle );
				return false;
			}
		}
		
		bPickSuccess =  GiveNewItemToPlayer( itemEle.GetItemEleInfo(), itemEle.GetTaskID(), errNo );
		if ( bPickSuccess )
		{
			MGOnePlayerPickItem gateMsg;
			gateMsg.pkgIndex = 0;
			gateMsg.itemID = index + 1;
			gateMsg.bIsPickMoney = false;
			SendPkg<MGOnePlayerPickItem>( &gateMsg );

			D_INFO("拾取index为%d的采集道具,采集size为%d\n", index + 1, m_collectInfo.mCollectItemEleVec.size() );
			RemoveCollectItemEle( itemEle );//删除道具
		}else
		{
			MGPickItemFaild srvmsg;
			srvmsg.itemID = index + 1;
			srvmsg.result = (EPickItemResult)errNo;
			SendPkg<MGPickItemFaild>( &srvmsg );
			return false;
		}

	} while( false );

	return true;
}


bool CPlayer::OnPickAllCollectItem()							//拾取所有采集道具
{
	int collectSize = (int)m_collectInfo.mCollectItemEleVec.size();
	for( int i=0; i<= collectSize; ++i )
	{
		OnPickOneCollectItem( i );    //捡第一个道具道具数量次
	}
	return true;
}

bool CPlayer::CallMonster( unsigned int num, unsigned int range )
{
	TYPE_POS nPosX,nPosY;
	if (!GetPos(nPosX,nPosY))
	{
		return false;
	}
	CNewMap * pMap = GetCurMap();
	if (NULL == pMap)
	{
		return false;
	}
	vector<CMonster*> vecMonster;
	vector<CPlayer*> vecPlayer;
	if (pMap->GetScopeCreature(nPosX,nPosY,range,vecPlayer,vecMonster))
	{
		if (vecMonster.size() > num)
		{
			random_shuffle(vecMonster.begin(),vecMonster.end());
			vecMonster.erase(vecMonster.begin()+num,vecMonster.end());
		}
		for ( size_t i = 0; i<vecMonster.size(); ++i)
		{
			if ( NULL == vecMonster[i] )
			{
				continue;
			}
			vecMonster[i]->OnPlayerDetected(this);
		}
		return true;
	}
	return false;
}


bool CPlayer::RemoveCollectItemEle( CItemPkgEle &itemEle )
{
	bool isSuccess = false;

	TRY_BEGIN;

	std::vector<CItemPkgEle>::iterator lter = m_collectInfo.mCollectItemEleVec.begin();
	for ( ;lter!= m_collectInfo.mCollectItemEleVec.end() && !isSuccess ; )
	{
		if ( *lter ==  itemEle )
		{
			(*lter).Clear();
			isSuccess = true;
		}
		else
			++lter;
	}

	TRY_END;

	return isSuccess;
}


void CPlayer::OnNotifyRacePrestige( int updateVal )
{
	MGRoleattUpdate gateMsg;
	gateMsg.changeTime = 0;
	gateMsg.lChangedPlayerID = GetFullID();
	gateMsg.nOldValue = updateVal;
	gateMsg.nNewValue = (int)GetRacePrestige();
	gateMsg.nType = RT_RACE_PRESTIGE;
	gateMsg.nAddType = 0;
	SendPkg<MGRoleattUpdate>( &gateMsg );
}


bool CPlayer::IsInActionTime()
{
	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	if( curTime <= (GetNextActionTime()) )
	{
		D_WARNING("CMuxMap::CheckPvc,CT时间内不允许使用其他技能\n");
		return true;
	}
	return false;
}



//交换装备栏的道具
void CPlayer::SwapEquipItem(char from, char to )
{ 
	//2010.5.13 gcq 因为没有装备栏位上没有两个道具了， 不同位置的道具不能交换,所以这个函数不需要了
	//2010.7.05 dzj 单手装备可以放主手也可以放副本，所以还是需要此函数

	if ( from == to )
	{
		return;
	}

	if ( from >= EQUIP_SIZE )
	{
		return;
	}

	if ( to >=  EQUIP_SIZE  )
	{
		return;
	}

	//PlayerInfo_2_Equips& playerEquips =  GetFullPlayerInfo()->equipInfo;
	//ItemInfo_i& fromItem = playerEquips.equips[(unsigned)from];
	//ItemInfo_i& toItem   = playerEquips.equips[(unsigned)to];
	ItemInfo_i* pFromItemInfo = GetEquipByPos( (unsigned int)from );
	ItemInfo_i* pToItemInfo = GetEquipByPos( (unsigned int)to );
	if ( ( NULL == pFromItemInfo ) || ( NULL == pToItemInfo ) )
	{
		D_ERROR( "CPlayer::SwapEquipItem, 玩家%s, ( NULL == pFromItemInfo ) || ( NULL == pToItemInfo ), from(%d),to(%d)\n"
			, GetAccount(), from, to );
		return;
	}

	//验证交换道具是否合法，从玩家装备道具管理器中取出物品要被交换的物品
	CBaseItem* pFromItemIns = GetItemDelegate().GetEquipItem( pFromItemInfo->uiID  );
	if ( NULL == pFromItemIns )
	{
		D_ERROR("玩家%s交换装备，from道具%d不在玩家的装备管理器中\n", GetAccount(), pFromItemInfo->uiID );
		return;
	}

	//from位置道具是否能放至to位置
	if ( !IsEquipPosAccord( pFromItemIns->GetGearArmType(), to ) )
	{
		D_ERROR("玩家%s交换装备，from道具(type:%d,gearType:%d)不能放至位置%d\n"
			, GetAccount(), pFromItemInfo->usItemTypeID, pFromItemIns->GetGearArmType(), to );
		return;
	}

	if ( PlayerInfo_2_Equips::INVALIDATE != pToItemInfo->usItemTypeID )
	{
		//新位置原来有道具，检查to位置的道具是否能放至from位置；
		CBaseItem* pToItemIns = GetItemDelegate().GetEquipItem( pToItemInfo->uiID  );
		if ( NULL == pToItemIns )
		{
			D_ERROR("玩家%s交换装备，to道具%d不在玩家的装备管理器中\n", GetAccount(), pToItemInfo->uiID );
			return;
		}
		if ( !IsEquipPosAccord( pToItemIns->GetGearArmType(), from ) )
		{
			D_ERROR("玩家%s交换装备，to道具(type:%d,gearType:%d)不能放至位置%d\n"
				, GetAccount(), pToItemInfo->usItemTypeID, pToItemIns->GetGearArmType(), from );
			return;
		}
	}

	//交换这两个道具
	ItemInfo_i tmpInfo = *pToItemInfo;
	*pToItemInfo = *pFromItemInfo;
	*pFromItemInfo = tmpInfo;
	NoticeEquipItemChange( from );
	NoticeEquipItemChange( to );

	return;
}

void CPlayer::QueryItemPkgPage( unsigned char page )
{
	if ( page >= 4 )
		return;

	unsigned pageSize = PKG_PAGE_SIZE * 2;

	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::QueryItemPkgPage, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_3_Pkgs& pkgInfoArr = playerInfo->pkgInfo;
	//todo sck
	MGItemPkgConciseInfo srvpkgmsg;
	StructMemSet( srvpkgmsg, 0x0, sizeof(MGItemPkgConciseInfo) );
	unsigned pageIndex = page*2;

	//该页的起始地址
	ItemInfo_i* PageFirstStart = ( pkgInfoArr.pkgs + page*pageSize );
	bool bHaveItem =  false;
	for (  int i = 0 ; i< PKG_PAGE_SIZE; i++ )
	{
		const ItemInfo_i& item = PageFirstStart[i];
		if ( item.uiID != 0 )
		{
			bHaveItem = true;
			break;
		}
	}
	if ( bHaveItem )
	{
		//srvpkgmsg.pageIndex = pageIndex;
		for (int i = 0; i<ARRAY_SIZE(srvpkgmsg.pkgInfo); ++i)
		{
			srvpkgmsg.pkgInfo[i].itemIndex = page*pageSize+i;
			srvpkgmsg.pkgInfo[i].itemInfo = *(PageFirstStart+i);
		}
		//StructMemCpy( srvpkgmsg.pkgInfo, PageFirstStart, sizeof(ItemInfo_i)*PKG_PAGE_SIZE );
		SendPkg<MGItemPkgConciseInfo>( &srvpkgmsg );
	}
	
	//该页的下半页地址
	ItemInfo_i* PageSecondStart = ( PageFirstStart + PKG_PAGE_SIZE );
	bHaveItem = false;
	for ( int i =0 ; i < PKG_PAGE_SIZE; i++ )
	{
		const ItemInfo_i& item = PageSecondStart[i];
		if ( item.uiID != 0 )
		{
			bHaveItem = true;
			break;
		}
	}

	if ( bHaveItem )
	{
		//srvpkgmsg.pageIndex = pageIndex + 1;
		for (int i = 0; i<ARRAY_SIZE(srvpkgmsg.pkgInfo); ++i)
		{
			srvpkgmsg.pkgInfo[i].itemIndex = page*pageSize+PKG_PAGE_SIZE+i;
			srvpkgmsg.pkgInfo[i].itemInfo = *(PageSecondStart+i);
		}
		//StructMemCpy( srvpkgmsg.pkgInfo, PageSecondStart, sizeof(ItemInfo_i)*PKG_PAGE_SIZE );
		SendPkg<MGItemPkgConciseInfo>( &srvpkgmsg );
	}
}

bool CPlayer::UseItemSkill( TYPE_ID itemTypeID, TYPE_ID itemSkillID )
{
	ACE_UNUSED_ARG( itemTypeID );

	CMuxSkillProp* pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( itemSkillID );
	if( NULL == pSkillProp )
	{
		D_ERROR("UseItemSkill 找不到%d的属性\n", itemSkillID );
		return false;
	}


	//CD 检测
	if( !CheckCd( pSkillProp->m_publicCDType, ACE_Time_Value(0, pSkillProp->m_nCoolDown * 1000)) )
	{
		D_ERROR( "UseItemSkill CD错误, CD类型:%d，skillProp's m_nCoolDown:%d\n", pSkillProp->m_publicCDType, pSkillProp->m_nCoolDown );
		return false;
	}

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
	{
		D_ERROR("找不到 pScript = NULL\n");
		return false;
	}

	//给予这次战斗脚本一些必要的参数
	pScript->InitSkillProcessArg( true, itemSkillID );
	
	//计算buff
	vector<int> vecRst;
	MuxBuffProp* pBuffProp = pSkillProp->m_pBuffProp;
	if( NULL != pBuffProp )
	{
		if( !pScript->FnAddBuffPvp( this, this, pBuffProp->mID, &vecRst ) )
		{
			return false;
		}

		if( vecRst.size() != 3 )
		{
			D_ERROR("FnAddBuffPvp 返回值\n");
			return false;
		}

		unsigned int buffID = vecRst[0];
		unsigned int buffTime = vecRst[1];
		unsigned int assistObj = vecRst[2];

		if( buffID != 0 )
		{
			CreateMuxBuffByID( buffID, buffTime, GetFullID() );
		}
	}

	MuxItemSkillProp* pHealProp = pSkillProp->m_pMuxItemSkillProp;
	if( NULL != pHealProp )
	{
		if( !pScript->FnPvpHealNum( this, this, pHealProp->mID, &vecRst ) )
		{
			//脚本错误
			return false;
		}

		if( vecRst.size() != 4 )
		{
			D_ERROR("FnPvpHealNum 返回值数量限制\n");
			return false;
		}

		unsigned int hp = vecRst[0];
		unsigned int mp = vecRst[1];
		unsigned int spBean = vecRst[2];
		EBattleFlag healFlag = (EBattleFlag)vecRst[3];

		unsigned int addHpVal = 0;
		unsigned int addMpVal = 0;
		unsigned int addSpVal = 0;
		bool isHpAddOK = false, isMpAddOK = false, isSpAddOK = false;
		if( hp )
		{
			addHpVal = AddPlayerHp( hp );
			if (addHpVal == 0)
			{
				isHpAddOK = false;
			}
			else
			{
				isHpAddOK = true;
			}
		}
		else
		{
			isHpAddOK = true;
		}

		if( mp )
		{
			addMpVal = AddPlayerMp( mp );
			if (addMpVal == 0)
			{
				isMpAddOK = false;
			}
			else
			{
				isMpAddOK = true;
			}
		}
		else
		{
			isMpAddOK = true;
		}

		if( spBean )
		{
			addSpVal = AddSpBean( spBean );
			if (addSpVal == 0)
			{
				isSpAddOK = false;
			}
			else
			{
				isSpAddOK = true;
			}
		}
		else
		{
			isSpAddOK = true;
		}

		if (isHpAddOK && isMpAddOK && isSpAddOK)
		{
			MGPlayerUseHealItem mg;
			mg.gcHeal.itemSkillID = itemSkillID;
			mg.gcHeal.playerHp = GetCurrentHP();
			mg.gcHeal.playerMp = GetCurrentMP();
			mg.gcHeal.playerSp = GetSpPower();
			mg.gcHeal.useItem = itemTypeID;
			mg.gcHeal.addHp = addHpVal;
			mg.gcHeal.addMp = addMpVal;
			mg.gcHeal.playerID = GetFullID();

			CMuxMap* pMap = GetCurMap();
			if( pMap )
			{
				pMap->NotifySurrounding<MGPlayerUseHealItem>( &mg, GetPosX(), GetPosY() );
			}
		}
		else
		{
			return false;
		}
		
	}


	//解除型道具
	if( pSkillProp->m_relieveID )
	{
		if( !pScript->FnRelievePvp( this, this, pSkillProp->m_relieveID, &vecRst ) )
		{
			return false;
		}
	}
	return true;
}

//麻烦 道具技能不属于玩家技能范畴 导致很难用OnPlayerUseSkill流程
bool CPlayer::UseDamageItem( TYPE_ID itemTypeID, TYPE_ID itemSkillID, TYPE_POS attackPosX, TYPE_POS attackPosY, PlayerID targetID )
{
	CMuxSkillProp* pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( itemSkillID );
	if( NULL == pSkillProp )
		return false;

	CMuxMap* pMap = GetCurMap();
	if( NULL == pMap )
		return false;

	CBattleScript* pScript = CBattleScriptManager::GetScript();
	if( NULL == pScript )
		return false;

	pScript->InitSkillProcessArg( true, pSkillProp->m_skillID );

	//如果是地面技能
	vector<CPlayer*> vecPlayer;
	vector<CMonster*> vecMonster;
	//if( pSkillProp->m_target == 4 )
	if( pSkillProp->IsAoeSkill() || pSkillProp->m_target == 4)
	{
		//CD 检测
		if( !CheckCd( pSkillProp->m_publicCDType, ACE_Time_Value( 0, pSkillProp->m_nCoolDown * 1000 ) ) )
			return false;

		if (attackPosX == 0 && attackPosY == 0)
		{
			GetPos( attackPosX, attackPosY );
			if( targetID.wGID == MONSTER_GID )
			{
				CMonster* pMonster = pMap->GetMonsterPtrInMap( targetID.dwPID );
				if( NULL != pMonster )
				{
					if (attackPosX == 0 && attackPosY == 0)
					{
						if( pSkillProp->m_shape == 1 )
						{
							//GetPos( attackPosX, attackPosY );
						}else
						{
							pMonster->GetPos(attackPosX,attackPosY);
						}
					}
				}
			}
			else
			{
				CGateSrv* pGateSrv = CManG_MProc::FindProc( targetID.wGID );
				if( NULL != pGateSrv )
				{
					CPlayer* pTargetPlayer = pGateSrv->FindPlayer( targetID.dwPID  );
					if( NULL != pTargetPlayer )
					{
						if( pSkillProp->m_shape == 1 )
						{
							//GetPos( attackPosX, attackPosY );
						}else
						{
							pTargetPlayer->GetPos(attackPosX,attackPosY);
						}
					}
				}
			}
		}

		pMap->OnPlayerSkillGetAoeTarget( this, pSkillProp->m_range, attackPosX, attackPosY, vecPlayer, vecMonster ); 

		for( unsigned int i = 0; i < vecPlayer.size(); ++i )
		{
			pMap->SingleItemPvpWrapper( this, vecPlayer[i], pSkillProp, targetID, SCOPE_ATTACK );
		}

		for( unsigned int i = 0; i < vecMonster.size(); ++i )
		{
			pMap->SingleItemPvcWrapper( this, vecMonster[i], pSkillProp, targetID, SCOPE_ATTACK );
		}

		//EndMsg
		MGUseSkillRst rst;
		rst.skillRst.attackHP = GetCurrentHP();
		rst.skillRst.attackID = GetFullID();
		rst.skillRst.attackMP = GetCurrentMP();
		rst.skillRst.causeBuffID = 0;
		rst.skillRst.lCooldownTime = pSkillProp->m_nCoolDown;
		rst.skillRst.lCurseTime = pSkillProp->m_nCurseTime;
		rst.skillRst.nErrCode = BATTLE_SUCCESS;
		rst.skillRst.orgTargetID = targetID;
		rst.skillRst.scopeFlag = SCOPE_ATTACK_END;
		rst.skillRst.skillID = pSkillProp->m_skillID;
		rst.skillRst.skillType = COMMON_SKILL;
		pMap->NotifySurrounding<MGUseSkillRst>( &rst, GetPosX(), GetPosY() );
		return true;
	}else
	{
		unsigned int ret = 0;
		if( targetID.wGID == MONSTER_GID )
		{
			//CD 检测
			if( !CheckCd( pSkillProp->m_publicCDType, ACE_Time_Value( 0, pSkillProp->m_nCoolDown * 1000 ) ) )
				return false;

			CMonster* pTargetMonster = pMap->GetMonsterPtrInMap( targetID.dwPID );
			if( NULL == pTargetMonster )
				return false;

			if( !pScript->FnCheckPveBattleSecondStage( this, pTargetMonster, ret ) )
			{
				return false;
			}

			if( ret != 0 )
			{
				return false;
			}

			pMap->SingleItemPvcWrapper( this, pTargetMonster, pSkillProp, targetID, SINGLE_ATTACK );
		} else {
			//CD 检测
			if( !CheckCd( pSkillProp->m_publicCDType, ACE_Time_Value( 0, pSkillProp->m_nCoolDown * 1000 ) ) )
				return false;

			CGateSrv* pGateSrv = CManG_MProc::FindProc( targetID.wGID );
			if( NULL == pGateSrv )
			{
				return false;
			}

			CPlayer* pTargetPlayer = pGateSrv->FindPlayer( targetID.dwPID  );
			if( NULL == pTargetPlayer )
				return false;

			unsigned int ret = 0;
			if( !pScript->FnCheckPvpBattleSecondStage( this, pTargetPlayer, ret ) )
			{
				return false;
			}

	   	 	if( ret != 0)
			{
				return false;
			}

			pMap->SingleItemPvpWrapper( this, pTargetPlayer, pSkillProp, targetID, SINGLE_ATTACK );
		}
		return true;
	}
	return false;
}

bool CPlayer::IsHaveControlBuff()	//是否拥有控制类的不利BUFF
{
	return m_buffManager.IsHaveControlBuff();
}

void CPlayer::NotifySurroundingMonsterHpAdd( PlayerID addStarter, int addVal )
{
	CMuxMap* pMap = GetCurMap();
	if( NULL == pMap )
		return;

	pMap->NotifySurMonsterPlayerHpAdd( addStarter, GetFullID(), addVal, GetPosX(), GetPosY() );
}

//栏位6 栏位7为控制类DEBUFF的栏位


bool CPlayer::AbsorbCheck( unsigned int& damage )
{
	return m_buffManager.AbsorbCheck( damage );
}
	
void CPlayer::EndAbsorbBuff()
{
	m_buffManager.EndAbsorbBuff();
}


bool CPlayer::EndSpeedUpBuff()
{
	return m_buffManager.EndSpeedUpBuff();
}


void CPlayer::EndAllDebuff()
{
	m_buffManager.EndAllDebuff();
}


//删除控制buff
void CPlayer::EndControlBuff()
{
	m_buffManager.EndControlBuff();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//以下简略信息填充......
/*
struct GCSimplePlayerInfo
{
static const USHORT	wCmd = 0x115e;

PlayerID                 	otherPlayerID;                              	//该玩家的ID号；
TYPE_ID                  	uiID;                                       	//角色ID//
BYTE                     	nameSize;                                   	//角色名长度//
CHAR                     	szNickName[MAX_NAME_SIZE];                    	//角色名//
//可变部分 BYTE                     	ucLevel;                                    	//等级
BYTE                     	raceClass;                                  	//种族与职业,高4位种族，低4位职业//
//可变部分 TYPE_NUM                 	nCurHp;                                     	//当前Hp
//可变部分 TYPE_NUM                 	nMaxHp;                                     	//最大Hp
//可变部分 TYPE_NUM                 	nCurMagic;                                  	//当前魔法
//可变部分 TYPE_NUM                 	nMaxMagic;                                  	//最大魔法
BYTE                     	ucHair;                                     	//头发//
BYTE                     	ucFace;                                     	//脸型//
//可变部分 TYPE_POS                 	iPosX;                                      	//X坐标
//可变部分 TYPE_POS                 	iPosY;                                      	//Y坐标
//可变部分 FLOAT                    	fSpeed;                                     	//移动速度，如果小于等于0则玩家静止
//可变部分 FLOAT                    	fTargetX;                                   	//目标点X浮点坐标
//可变部分 FLOAT                    	fTargetY;                                   	//目标点Y浮点坐标
//可变部分 TYPE_SI16                	sGoodEvilPoint;                             	//善恶值
//可变部分 BYTE                     	ucAnamorphic;                               	//不等于0处于变形态
BYTE                     	bSex;                                       	//性别//
//可变部分 BYTE                     	ucFlag1;                                    	//按bit从高到低依次为:是否组队，是否队长，是否流氓，是否虚弱，是否时装态，是否新手保护，是否新手失效，是否族长
//可变部分 SimpleEquipInfo          	equipInfo;                                  	//5个可视位置装备简单信息
};
CLIPROTOCOL_T_MAXBYTES_CHECK(GCSimplePlayerInfo);
*/

///设置玩家简略信息中的不变部分
void CPlayer::SetUnchangeSimpleInfo()
{
	m_simplePlayerInfo.playerInfo.otherPlayerID = GetFullID();
	m_simplePlayerInfo.playerInfo.uiID          = GetDBID();
	SafeStrCpy( m_simplePlayerInfo.playerInfo.szNickName, GetNickName() );
	m_simplePlayerInfo.playerInfo.nameSize      = (BYTE)strlen(m_simplePlayerInfo.playerInfo.szNickName)+1;
	SetRaceClassPre(m_simplePlayerInfo.playerInfo);
	SetRaceToSimpleInfo(m_simplePlayerInfo.playerInfo, GetRace());
	SetClassToSimpleInfo(m_simplePlayerInfo.playerInfo, GetClass());
	m_simplePlayerInfo.playerInfo.ucHair = GetHair();
	m_simplePlayerInfo.playerInfo.ucFace = GetFace();
	m_simplePlayerInfo.playerInfo.ucPortrait = GetPortrait();
	m_simplePlayerInfo.playerInfo.shFlag  = GetSHFlag();

	return;
}

///设置玩家简略信息中的可变部分(以后可以细化为各个部分，例如：血量，装备等等)
void CPlayer::SetChangeSimpleInfo()
{
	m_isSimplePlayerInfoValid = true;//每次设过后，除非中间某些信息发生变化，否则一直有效；

	m_simplePlayerInfo.playerInfo.ucLevel = (unsigned char)GetLevel();
	m_simplePlayerInfo.playerInfo.nCurHp = GetCurrentHP();
	m_simplePlayerInfo.playerInfo.nMaxHp = GetMaxHP();
	m_simplePlayerInfo.playerInfo.nCurMagic = GetCurrentMP();
	m_simplePlayerInfo.playerInfo.nMaxMagic = GetMaxMP();
	m_simplePlayerInfo.playerInfo.iPosX = GetPosX();
	m_simplePlayerInfo.playerInfo.iPosY = GetPosY();
	m_simplePlayerInfo.playerInfo.fSpeed = GetSpeed();
	m_simplePlayerInfo.playerInfo.fTargetX = GetFloatTargetX();
	m_simplePlayerInfo.playerInfo.fTargetY = GetFloatTargetY();
	m_simplePlayerInfo.playerInfo.sGoodEvilPoint = GetGoodEvilPoint();
	m_simplePlayerInfo.playerInfo.ucAnamorphic = m_anamorphic;//细查；
	SetSimpleInfoFlag();//设置标记；
	SetSimpleInfoEquip();//设置装备；

	return;
}// void CPlayer::SetChangeSimpleInfo()

///设置玩家简略信息中的标记值
void CPlayer::SetSimpleInfoFlag()
{
	SetSimpleInfoFlag1Pre( m_simplePlayerInfo.playerInfo );
	SetSimpleInfoTeam( m_simplePlayerInfo.playerInfo, IsInTeam() );
	SetSimpleInfoHeader( m_simplePlayerInfo.playerInfo, IsTeamCaptain() );
	SetSimpleInfoRogue( m_simplePlayerInfo.playerInfo, IsRogueState() );
	SetSimpleInfoWeak( m_simplePlayerInfo.playerInfo, IsWeak() );
	SetSimpleInfoFashion( m_simplePlayerInfo.playerInfo, IsFashionState() );
	SetSimpleInfoRookie( m_simplePlayerInfo.playerInfo, IsRookieState() );
	SetSimpleInfoRBFault( m_simplePlayerInfo.playerInfo, IsRookieBreak() );
	SetSimpleInfoRMaster( m_simplePlayerInfo.playerInfo, IsRaceMaster() );

	return;
}// void CPlayer::SetSimpleInfoFlag()

///设置玩家简略信息中的装备
void CPlayer::SetSimpleInfoEquip()
{
    const PlayerInfo_2_Equips* playerFullEquip = GetEquip();
	if ( NULL == playerFullEquip )
	{
		D_ERROR( "CPlayer::SetSimpleInfoEquip, %s NULL == playerFullEquip\n", GetNickName() );
		return;
	}

	m_simplePlayerInfo.playerInfo.equipInfo.lightVal = GetEquipLightVal();//取流光值；
	m_simplePlayerInfo.playerInfo.equipInfo.equipArrSize = ARRAY_SIZE(m_simplePlayerInfo.playerInfo.equipInfo.equipArr);

	ItemInfo_i* pItemInfoI = NULL;
	bool isItemValid = GetPlayerGearEquip( E_BODY, pItemInfoI );
	if ( isItemValid )
	{
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].TypeID = (NULL != pItemInfoI)? pItemInfoI->usItemTypeID:0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].itemLevel = (NULL != pItemInfoI)? pItemInfoI->ucLevelUp:0;
	} else {
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].TypeID = 0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].itemLevel = 0;
	}

	isItemValid = GetPlayerGearEquip( E_HAND, pItemInfoI );
	if ( isItemValid )
	{
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_HAND].TypeID = (NULL != pItemInfoI)? pItemInfoI->usItemTypeID:0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_HAND].itemLevel = (NULL != pItemInfoI)? pItemInfoI->ucLevelUp:0;
	} else {
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].TypeID = 0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].itemLevel = 0;
	}

	isItemValid = GetPlayerGearEquip( E_FIRSTHAND, pItemInfoI );
	if ( isItemValid )
	{
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_WEAPON].TypeID = (NULL != pItemInfoI)? pItemInfoI->usItemTypeID:0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_WEAPON].itemLevel = (NULL != pItemInfoI)? pItemInfoI->ucLevelUp:0;
	} else {
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].TypeID = 0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].itemLevel = 0;
	}

	isItemValid = GetPlayerGearEquip( E_FOOT, pItemInfoI );
	if ( isItemValid )
	{
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_FOOT].TypeID = (NULL != pItemInfoI)? pItemInfoI->usItemTypeID:0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_FOOT].itemLevel = (NULL != pItemInfoI)? pItemInfoI->ucLevelUp:0;
	} else {
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].TypeID = 0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].itemLevel = 0;
	}

	isItemValid = GetPlayerGearEquip( E_SHOULDER, pItemInfoI );
	if ( isItemValid )
	{
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_SHOULDER].TypeID = (NULL != pItemInfoI)? pItemInfoI->usItemTypeID:0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_SHOULDER].itemLevel = (NULL != pItemInfoI)? pItemInfoI->ucLevelUp:0;
	} else {
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].TypeID = 0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].itemLevel = 0;
	}

	isItemValid = GetPlayerGearEquip( E_BACK, pItemInfoI );
	if ( isItemValid )
	{
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BACK].TypeID = (NULL != pItemInfoI)? pItemInfoI->usItemTypeID:0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BACK].itemLevel = (NULL != pItemInfoI)? pItemInfoI->ucLevelUp:0;
	} else {
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].TypeID = 0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_BODY].itemLevel = 0;
	}

	isItemValid = GetPlayerGearEquip( E_HEAD, pItemInfoI );
	if ( isItemValid )
	{
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_HEAD].TypeID = (NULL != pItemInfoI)? pItemInfoI->usItemTypeID:0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_HEAD].itemLevel = (NULL != pItemInfoI)? pItemInfoI->ucLevelUp:0;
	} else {
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_HEAD].TypeID = 0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_HEAD].itemLevel = 0;
	}

	isItemValid = GetPlayerGearEquip( E_SECONDHAND, pItemInfoI );
	if ( isItemValid )
	{
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_WEAPON2].TypeID = (NULL != pItemInfoI)? pItemInfoI->usItemTypeID:0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_WEAPON2].itemLevel = (NULL != pItemInfoI)? pItemInfoI->ucLevelUp:0;
	} else {
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_WEAPON2].TypeID = 0;
		m_simplePlayerInfo.playerInfo.equipInfo.equipArr[SE_WEAPON2].itemLevel = 0;
	}

	return;
}// void CPlayer::SetSimpleInfoEquip()

///取玩家身上流光值
unsigned char CPlayer::GetEquipLightVal()
{
	unsigned char initval = 200;

	const PlayerInfo_2_Equips* playerFullEquip = GetEquip();

	if ( NULL == playerFullEquip )
	{
		D_ERROR( "CPlayer::GetEquipLightVal, NULL == playerFullEquip\n" );
		return 0;
	}

	unsigned char tocompare = 0;
	ItemInfo_i* pItemInfoI = NULL;

	bool isItemValid = GetPlayerGearEquip( E_BODY, pItemInfoI );
	if ( isItemValid && (NULL != pItemInfoI) )
	{
		tocompare = pItemInfoI->ucLevelUp;
		initval = (initval <= tocompare) ? initval:tocompare;
	}

	isItemValid = GetPlayerGearEquip( E_HAND, pItemInfoI );
	if ( isItemValid && (NULL != pItemInfoI) )
	{
		tocompare = pItemInfoI->ucLevelUp;
		initval = (initval <= tocompare) ? initval:tocompare;
	}

	isItemValid = GetPlayerGearEquip( E_FOOT, pItemInfoI );
	if ( isItemValid && (NULL != pItemInfoI) )
	{
		tocompare = pItemInfoI->ucLevelUp;
		initval = (initval <= tocompare) ? initval:tocompare;
	}

	isItemValid = GetPlayerGearEquip( E_SHOULDER, pItemInfoI );
	if ( isItemValid && (NULL != pItemInfoI) )
	{
		tocompare = pItemInfoI->ucLevelUp;
		initval = (initval <= tocompare) ? initval:tocompare;
	}

	return initval;
}

///获取玩家简略(外观)信息
MGSimplePlayerInfo* CPlayer::GetSimplePlayerInfo()
{
    //以下项较难确定何时改变，因此每次取时都重读；
	m_simplePlayerInfo.playerInfo.nMaxHp = GetMaxHP(); 
	m_simplePlayerInfo.playerInfo.nMaxMagic = GetMaxMP();
	SetSimpleInfoRookie( m_simplePlayerInfo.playerInfo, IsRookieState() );

	if ( m_isSimplePlayerInfoValid )
	{
		return &m_simplePlayerInfo;
	}

	//以下重新填充玩家外观信息

	SetChangeSimpleInfo();

	return &m_simplePlayerInfo;
}

//......以上简略信息填充
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///获取自身基本信息
MGOtherPlayerInfo* CPlayer::GetDetailPlayerInfo()
{
	GetPlayerChangeFrequentlyInfo( m_selfPlayerInfo.playerInfo.playerInfo );//时装／装备切换时会用到
	m_selfPlayerInfo.playerInfo.nCurHp    = GetCurrentHP();
	m_selfPlayerInfo.playerInfo.nCurMagic = GetCurrentMP();
	m_selfPlayerInfo.playerInfo.fSpeed    = GetSpeed();
	m_selfPlayerInfo.playerInfo.isTeamCaptain = IsTeamCaptain();
	m_selfPlayerInfo.playerInfo.bRogue   = IsRogueState();
	m_selfPlayerInfo.playerInfo.isInTeam = IsInTeam();
	m_selfPlayerInfo.playerInfo.fTargetX = GetFloatTargetX();
	m_selfPlayerInfo.playerInfo.fTargetY = GetFloatTargetY();
	m_selfPlayerInfo.playerInfo.nMaxHp    = GetMaxHP();
	m_selfPlayerInfo.playerInfo.nMaxMagic = GetMaxMP();
	m_selfPlayerInfo.playerInfo.sGoodEvilPoint     = GetGoodEvilPoint();
	m_selfPlayerInfo.playerInfo.playerInfo.usLevel = GetLevel();
	m_selfPlayerInfo.playerInfo.bWeak = IsWeak();
	m_selfPlayerInfo.playerInfo.bFashionState = IsFashionState();
	m_selfPlayerInfo.playerInfo.Anamorphic = m_anamorphic;
	m_selfPlayerInfo.playerInfo.playerInfo.iPosX = GetPosX();
	m_selfPlayerInfo.playerInfo.playerInfo.iPosY = GetPosY();
	m_selfPlayerInfo.playerInfo.isRookieState = IsRookieState();
	m_selfPlayerInfo.playerInfo.isRookieBreak = IsRookieBreak();
	m_selfPlayerInfo.playerInfo.isRaceMaster  = IsRaceMaster();
	//m_otherPlayerInfo.playerInfo.nErrCode = MGOtherPlayerInfo::ISERR_OK;
	//if(IsFashionState())//时装态，用时装替换对应部位的装备
	//{
	//	m_otherPlayerInfo.playerInfo.playerInfo.equipInfo.equips[PlayerInfo_2_Equips::INDEX_BODY] = m_fashions.fashionData[0];
	//	m_otherPlayerInfo.playerInfo.playerInfo.equipInfo.equips[PlayerInfo_2_Equips::INDEX_HAND] = m_fashions.fashionData[1];
	//	m_otherPlayerInfo.playerInfo.playerInfo.equipInfo.equips[PlayerInfo_2_Equips::INDEX_FOOT] = m_fashions.fashionData[2];
	//}
	
	return &m_selfPlayerInfo;
}

//初始化玩家的OtherPlayerInfo信息
//void CPlayer::InitOtherPlayerConciseInfo()
//{
//	GCOtherPlayerInfo& playerInfo =  m_selfPlayerInfo.playerInfo;
//	playerInfo.otherPlayerID = GetFullID();
//	playerInfo.nErrCode      = MGOtherPlayerInfo::ISERR_OK;
//	GetPlayerLoginInfo( playerInfo.playerInfo );
//}

//void CPlayer::EquipItemAffectOtherPlayerInfo(const ItemInfo_i& itemInfo , unsigned int equipIndex )
//{
//	if ( equipIndex >= EQUIP_SIZE )
//		return;
//
//	GCOtherPlayerInfo& otherPlayer =  m_selfPlayerInfo.playerInfo;
//	otherPlayer.playerInfo.equipInfo.equips[equipIndex] = itemInfo;
//}

//void CPlayer::UnEquipItemAffectOtherPlayerInfo(unsigned int unequipIndex )
//{
//	if ( unequipIndex >= EQUIP_SIZE )
//		return;
//
//	GCOtherPlayerInfo& otherPlayer =  m_selfPlayerInfo.playerInfo;
//	ItemInfo_i& itemInfo = otherPlayer.playerInfo.equipInfo.equips[unequipIndex];
//	itemInfo.ucCount = itemInfo.ucLevelUp = itemInfo.uiID = itemInfo.usAddId = itemInfo.usItemTypeID =  itemInfo.usWearPoint = 0;
//}

void CPlayer::QueryRepairAllItemCost()
{
	MGRepairWearByNPC::eRepairResult result = MGRepairWearByNPC::TARGET_DO_NOT_REPAIR;

	//玩家的装备栏
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::QueryRepairAllItemCost, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_2_Equips& equips = playerInfo->equipInfo;

	//玩家的包裹栏
	PlayerInfo_3_Pkgs&  pkgs = playerInfo->pkgInfo;

	//玩家身上的金钱
	unsigned int leftMoney = 0;
	if ( IsUseGoldMoney() )
	{
		//使用金币
		leftMoney  = GetGoldMoney();
	} else {
		//使用银币
		leftMoney  = GetSilverMoney();
	}	

	unsigned int newWearPoint = 0;

	for(unsigned int i = 0; i< EQUIP_SIZE; i++)
	{
		ItemInfo_i& repairItem = equips.equips[i];
		if( repairItem.uiID > 0)
		{
			result = (MGRepairWearByNPC::eRepairResult)( CheckItemCanRepair(&(repairItem), leftMoney, newWearPoint));
			if((result == MGRepairWearByNPC::OTHER_ERROR) || (result == MGRepairWearByNPC::NO_ENOUGH_REPAIR_MONEY))
				break;
		}
	}

	if ( (result != MGRepairWearByNPC::OTHER_ERROR) && (result != MGRepairWearByNPC::NO_ENOUGH_REPAIR_MONEY) )
	{
		for( unsigned int i = 0 ; i <PACKAGE_SIZE; i++ )
		{
			ItemInfo_i& repairItem = pkgs.pkgs[i];
			if ( repairItem.uiID > 0 )
			{
				result = (MGRepairWearByNPC::eRepairResult)( CheckItemCanRepair(&(repairItem), leftMoney, newWearPoint));
				if((result == MGRepairWearByNPC::OTHER_ERROR) || (result == MGRepairWearByNPC::NO_ENOUGH_REPAIR_MONEY))
					break;
			}
		}
	}

	//说明金钱有变化了,玩家维修了道具
	MGRepairItemCostRst repairItemCost;
	if((result == MGRepairWearByNPC::OTHER_ERROR) || (result == MGRepairWearByNPC::NO_ENOUGH_REPAIR_MONEY))
	{
		if (MGRepairWearByNPC::OTHER_ERROR == result) 
		{
			//双币种判断，repairItemCost.costMoney = -2;//修复时发生错误，可能没有物品对应信息
			repairItemCost.costSilverMoney = 0;//已改为unsigned int,若原来-2作特殊解释，则改过来；-2;//修复时发生错误，可能没有物品对应信息
			repairItemCost.costGoldMoney = 0;//已改为unsigned int,若原来-2作特殊解释，则改过来；-2;//修复时发生错误，可能没有物品对应信息
		} else {
			repairItemCost.costSilverMoney = 0;//修复费用不够
			repairItemCost.costGoldMoney = 0;//修复费用不够
		}
	} else {
		if ( IsUseGoldMoney() )
		{
			//使用金币
			if ( GetGoldMoney() == leftMoney ) //双币种判断
			{
				//双币种判断，repairItemCost.costMoney = -1;//不可修复或者耐久度满
				repairItemCost.costSilverMoney = 0;//已改为unsigned int,若原来-2作特殊解释，则改过来；-1;//不可修复或者耐久度满
				repairItemCost.costGoldMoney = 0;
			} else {
				repairItemCost.costGoldMoney = ( GetGoldMoney() - leftMoney );
				repairItemCost.costSilverMoney = 0;
			}
		} else {
			//使用银币
			if ( GetSilverMoney() == leftMoney ) //双币种判断
			{
				//双币种判断，repairItemCost.costMoney = -1;//不可修复或者耐久度满
				repairItemCost.costSilverMoney = 0;//已改为unsigned int,若原来-2作特殊解释，则改过来；-1;//不可修复或者耐久度满
				repairItemCost.costGoldMoney = 0;
			} else {
				repairItemCost.costSilverMoney = ( GetSilverMoney() - leftMoney );
				repairItemCost.costGoldMoney = 0;
			}
		}
	}

	SendPkg<MGRepairItemCostRst>( &repairItemCost );
}

short CPlayer::GetGoodEvilPoint()
{
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::GetGoodEvilPoint, NULL == GetFullPlayerInfo()\n");
		return 0;
	}
	int evilPoint = playerInfo->baseInfo.sGoodEvilPoint;

	if ( evilPoint < 0 )
	{
		//恶魔时间
		unsigned int addTime = playerInfo->baseInfo.uiEvilTime/EVIL_UPDATE_TIME;

		evilPoint += addTime;
		if ( evilPoint > 0 )
			evilPoint = 0 ;
	}
	
	return evilPoint;
}

bool CPlayer::IsCanDeadDrop(unsigned int itemUID )
{
	//无论何种保护,都不能与玩家交易
	ProtectItem* pProtectItem = GetProtectItemManager().FindItemIsProtected( itemUID );
	if ( pProtectItem )
		return false;

	//如果非保护道具,检测是否被绑定
	CBaseItem* pItem = GetItemDelegate().GetItemByItemUID( itemUID );
	if ( !pItem )
		return false;

	ITEM_BIND_TYPE bindType = pItem->GetItemBindType();
	if ( bindType != E_UN_BIND )
	{
		if ( bindType == E_EQUIP_BIND )//是否为装备绑定
		{
			int itemIndex =  -1;
			bool isEquip  =  false;
			ItemInfo_i* pItemInfo = GetItemInfoByItemUID(  itemUID , itemIndex, isEquip  );
			if ( !pItemInfo )
				return false;

			if( ITEM_IS_EQUIPED( pItemInfo->ucLevelUp ) )//如果道具曾经装备过
				return false;
		}
		else if( bindType == E_PICK_BIND )
		{
			return false;
		}
	}
	return true;
}

bool CPlayer::IsCanDrop(unsigned int itemUID )
{
	//只要道具保护阶段,不能丢弃道具
	ProtectItem* pProtectItem = GetProtectItemManager().FindItemIsProtected( itemUID );
	if ( !pProtectItem )
	{
		return true;
	}

	return false;
}

bool CPlayer::IsCanTradeWithPlayer(unsigned int itemUID , unsigned int& errType )
{
	//无论何种保护,都不能与玩家交易
	ProtectItem* pProtectItem = GetProtectItemManager().FindItemIsProtected( itemUID );
	if ( pProtectItem )
	{
		errType = 2;
		return false;
	}

	CBaseItem* pItem = GetItemDelegate().GetItemByItemUID( itemUID );
	if ( !pItem )
		return false;

	ITEM_BIND_TYPE bindType = pItem->GetItemBindType();
	if ( bindType != E_UN_BIND )
	{
		errType = 1;
		if ( bindType == E_EQUIP_BIND )
		{
			int itemIndex =  -1;
			bool isEquip  =  false;
			ItemInfo_i* pItemInfo = GetItemInfoByItemUID(  itemUID , itemIndex, isEquip  );
			if ( !pItemInfo )
				return false;

			if( ITEM_IS_EQUIPED( pItemInfo->ucLevelUp ) )//如果道具曾经装备过
				return false;
		}
		else if( bindType == E_PICK_BIND )
		{
			return false;
		}
	}
	return true;
}

bool CPlayer::IsCanTradeWithNPC(unsigned int itemUID )
{
	//无论何种保护,都不能与玩家交易
	ProtectItem* pProtectItem = GetProtectItemManager().FindItemIsProtected( itemUID );
	if ( pProtectItem )
		return false;

	return true;
}

bool CPlayer::IsCanUpdateItem(unsigned int itemUID )
{
	//无论何种保护,都不能与玩家交易
	ProtectItem* pProtectItem = GetProtectItemManager().FindItemIsProtected( itemUID );
	if ( !pProtectItem )
		return true;

	if ( pProtectItem->protectType == E_SPECIAL_PROTECT )
		return false;

	return true;
}

void CPlayer::OnGMUpdateGoodEvilPoint(int point )
{
	if ( point < 0 )
		m_goodEvilMan.SubGoodEvilPoint( point*-1);
	else
		m_goodEvilMan.AddGoodEvilPoint( point );
}

void CPlayer::IssueSwitchMap( unsigned newMapID, unsigned int targetPosX, unsigned int targetPosY )
{
	//束缚
	if ( IsFaint() )
	{
		SendSystemChat("晕旋态无法跳地图");
		D_DEBUG("晕旋态无法跳地图\n");
		return;
	}

	//眩晕
	if ( IsBondage() )
	{
		SendSystemChat("束缚状态无法跳地图");
		D_DEBUG("束缚状态无法跳地图\n");
		return;
	}

	CRideState& ridestate = GetSelfStateManager().GetRideState();
	if ( ridestate.IsActive() )
	{
		if( ridestate.GetPlayerRideType() != CRideState::E_LEADER )//如果不是骑乘队长，不能控制跳地图
		{
			SendSystemChat("您不是骑乘队长,不能跳地图");
			D_DEBUG("您不是骑乘队长,不能跳地图\n");
			return ;
		}
	}
	
	MGSwichMapReq switchMsg;
	switchMsg.newMapID = newMapID;
	switchMsg.targetX = targetPosX;
	switchMsg.targetY = targetPosY;
	SendPkg<MGSwichMapReq>( &switchMsg );

	CNewMap* pMap = GetCurMap();
	if ( NULL == pMap )
	{
		D_ERROR( "IssueSwitchMap,找不到玩家所在地图\n" );
		return;
	}

	CLogManager::DoSwitchMap(this, pMap->GetMapNo(), newMapID);//记日至

	return;
}

int CPlayer::GetItemMaxSpeed()
{
	if ( IsRideState() )
	{
		int rideSpeed = GetRideMaxSpeed();
		int itemSpeed = GetEquipMaxSpeed();
		int speed = rideSpeed > itemSpeed ? rideSpeed:itemSpeed;
		return speed;
	}
	return GetEquipMaxSpeed();
}

void CPlayer::EquipItemChangeSpeed(int speed )
{
	//int oldspeed = maxItemSpeed;
	maxItemSpeed = speed > maxItemSpeed?speed:maxItemSpeed;
	//如果速度发生改变了
	//if ( maxItemSpeed != oldspeed )
	//{
		//NoticeSpeedChange();
	//}
}

void CPlayer::NoticeSpeedChange()
{
	//通知客户端速度修改了
	MGRoleattUpdate updateAttr;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = (int)( GetSpeed() * 100);
	updateAttr.lChangedPlayerID = GetFullID();
	updateAttr.nType = RT_SPEED;	//修改速度
	updateAttr.changeTime = 0;		//启程为永久 7.9 hyn
	updateAttr.bFloat = true;
	CMuxMap* pPlayerMap = GetCurMap();
	if ( pPlayerMap )
		pPlayerMap->NotifySurrounding<MGRoleattUpdate>( &updateAttr, GetPosX(),GetPosY() );
	
	//SendPkg<MGRoleattUpdate>( &updateAttr );
}

//因为耐久度恢复，将道具所关联的属性加到玩家的身上
void CPlayer::RepaireEquipItemAddProp( const ItemInfo_i& itemInfo ,unsigned int equipIndex )
{
	CBaseItem* pItem = GetItemDelegate().GetEquipItem( itemInfo.uiID );
	if ( pItem )
	{
		//修复道具影响玩家的OtherPlayerInfo
		//EquipItemAffectOtherPlayerInfo( itemInfo, equipIndex );

		float oldspeed = GetSpeed();

		ItemAttributeHelper::OnRepairWearPointEmtpyItem( this, itemInfo );
		//pItem->AddItemPropToPlayer( this, itemInfo );

		//如果是套装,在维修看查看是否需要将套装的属性加给玩家
		unsigned int suitID = pItem->GetSuitID();
		if (  suitID > 0 )
			GetSuitDelegate().AfterRepairWearEmptySuitItem(  suitID, equipIndex );

		//通知血,魔发生变化了
		NoticeHPMPChange();

		if ( oldspeed != GetSpeed() )
		{
			NoticeSpeedChange();
		}
	}
}

int CPlayer::GetEquipedItemMaxSpeed(unsigned int exceptItemUID )
{
	unsigned int maxSpeed = 0;
	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::GetEquipedItemMaxSpeed, NULL == GetFullPlayerInfo()\n");
		return 0;
	}
	PlayerInfo_2_Equips& Equips = playerInfo->equipInfo;

	//检查所有的装备道具
	for ( int  i = 0 ; i < EQUIP_SIZE; i++ )
	{

		//不检查骑乘的位置
		if ( IsEquipPosAccord( E_DRIVE, i ) )
		{
			continue;
		}

		const ItemInfo_i& item = Equips.equips[i];
		if ( (item.uiID == exceptItemUID)
			 || (item.uiID == PlayerInfo_2_Equips::INVALIDATE)
			 || (item.usWearPoint == 0) )
		{
			continue;
		}

		CBaseItem* pItem = GetItemDelegate().GetEquipItem( item.uiID );
		if ( pItem )
		{
			CEquipItemEx* pEquipItem =  dynamic_cast<CEquipItemEx* >( pItem );
			if ( !pEquipItem  || ! pEquipItem->GetEquipProperty() )
				continue;

			unsigned int  itemSpeed   =  pEquipItem->GetItemSpeed();//获取基础速度

			
			////去除追加，原位置用于随机集 //获取追加后的速度
			//ChangeProp*   pChangeProp = ItemChangeAttributeManager::GetChangeItemAttribute( item.randSet/*usAddId*/ );
			//if ( pChangeProp != NULL )
			//	itemSpeed += pChangeProp->AddSpeed;			

			//获取升级后的速度
			unsigned int levelUpID  = pEquipItem->GetEquipProperty()->brand  * 100u + GET_ITEM_LEVELUP( item.ucLevelUp );
			LevelUpProp* pLevelProp = ItemLevelUpAttributeManager::GetLevelUpAttribute(  levelUpID );
			if ( pLevelProp )
			{
				unsigned int Modulus = CItemLevelUpModulus::GetLevelUpModulus( pEquipItem->GetItemLevel() );
				itemSpeed += ( pLevelProp->speed * Modulus );
			}

			if ( itemSpeed > maxSpeed )
				maxSpeed = itemSpeed;
		}
	}
	return maxSpeed;
}


void CPlayer::OnUpdateEquipItemInfo( const ItemInfo_i& oldItemInfo,const ItemInfo_i& newItemInfo )
{
	CBaseItem* pEquipItem =  GetItemDelegate().GetEquipItem( oldItemInfo.uiID );
	if ( !pEquipItem )
	{
		D_ERROR("更新装备的信息，但在玩家%s的装备管理器中找不到该道具%d\n", GetNickName(), oldItemInfo.uiID  );
		return;
	}

	 float oldSpeed = GetSpeed();
	 
	//如果有耐久度，那么减去这个装备所加的属性
	//if ( oldItemInfo.usWearPoint > 0 )
	{
		//pEquipItem->SubItemPropToPlayer( this, oldItemInfo );
		ItemAttributeHelper::OnUnEquipItemAffectPlayer( this, oldItemInfo );
	}

	//如果有耐久度，那么加上这个装备所加的属性
	//if ( newItemInfo.usWearPoint > 0 )
	{
		//pEquipItem->AddItemPropToPlayer( this, newItemInfo );
		ItemAttributeHelper::OnEquipItemAffectPlayer( this, newItemInfo );
	}

	if( GetCurrentHP() > GetMaxHP() )
	{
		SetPlayerHp( GetMaxHP() );
		NotifyHpChange();
	}

	if ( GetCurrentMP() > GetMaxMP() )
	{
		SetPlayerMp( GetMaxMP() );
		NotifyMpChange();
	}

	//如果速度发生变化
	if( oldSpeed != GetSpeed() )
	{
		NotifySpeedChange();
	}
}

void CPlayer::RandUpdateOneEquipItem()
{
	TRY_BEGIN;

	ItemDelegate& itemDelgate = GetItemDelegate();
	//记录装备的道具数目
	unsigned int equipSize = itemDelgate.GetItemCountOnEquip();
	if ( equipSize == 0 )
		return;

	//随机次数
	bool isNeedUpdate = false;
	CBaseItem*   pEquipItem = NULL;

	int randCount = equipSize;
	while ( randCount > 0  && !isNeedUpdate )
	{
		//从这些装备的道具中随机出一件物品升级
		unsigned int randIndex  = RandNumber( 0 ,equipSize - 1 );
		pEquipItem = itemDelgate.GetEquipItemByIndex( randIndex );
		if ( !pEquipItem )
		{
			D_ERROR("%s准备随机升级装备，但在装备管理器中找不到对应的索引%d 道具个数%d\n", GetNickName(), randIndex,equipSize );
			return;
		}//只有白色以上的物品，才能被升级
		//else if( pEquipItem->GetItemMassLevel() > E_WHITE  )
		{
			isNeedUpdate = true;
		}

		randCount--;
	}

	if ( !isNeedUpdate  )
	{
		D_ERROR("天谴:%s 全身没有可升级的物品\n",GetNickName() );
		return;
	}
	
	if ( pEquipItem == NULL )
	{
		return;
	}

	FullPlayerInfo * playerInfo = GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CPlayer::RandUpdateOneEquipItem, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_2_Equips& Equips = playerInfo->equipInfo;
	for ( unsigned int i = 0 ; i< EQUIP_SIZE; i++ )
	{
		ItemInfo_i& tmpInfo = Equips.equips[i];
		if ( tmpInfo.uiID == pEquipItem->GetItemUID() )
		{
			ItemInfo_i oldItemInfo =  tmpInfo;
			//如果到了装备升级的最大等级 
			if ( GET_ITEM_LEVELUP( tmpInfo.ucLevelUp ) < pEquipItem->GetLevelUpLimit() )
			{
				//道具升一级
				tmpInfo.ucLevelUp++;

				//通知玩家道具的等级改变了
				NoticeEquipItemChange( i );

				//升级后,道具改变了玩家的属性
				OnUpdateEquipItemInfo( oldItemInfo, tmpInfo );

				//记录动态
				AddFriendTweet(MUX_PROTO::TWEET_TYPE_EQUIPMENT_UPGRADE, (const void *)&tmpInfo, sizeof(tmpInfo));

				break;
			}
			else
			{
				D_DEBUG("%s玩家天谴随机升级物品，但升级的物品已经为%d级了，不能升级\n", GetNickName(),GET_ITEM_LEVELUP( tmpInfo.ucLevelUp ) );
				return;
			}
		}
	}

	TRY_END;
}

float CPlayer::GetSkillAddFaintRate()   //获得增加的击晕几率
{
	return m_buffManager.GetSkillAddFaintRate();
}

int CPlayer::GetEquipAntiFaintLevel()	//获得抗晕几率
{
	return m_itemTotalProp.Antistun;
}

int CPlayer::GetEquipAddFaintLevel()
{
	return m_itemTotalProp.Stun;
}

float CPlayer::GetSkillAntiFaintRate()
{
	return m_buffManager.GetSkillAntiFaintRate();
}

int CPlayer::GetEquipAddBondageLevel()	//获得装备抗束缚等级
{
	return m_itemTotalProp.Tie;
}


int CPlayer::GetEquipAntiBondageLevel()	 //获得装备抗束缚几率
{
	return m_itemTotalProp.Antitie;
}


float CPlayer::GetSkillAddBondageRate()	//获得技能增加束缚几率
{
	return m_buffManager.GetSkillAddBondageRate();
}


float CPlayer::GetSkillAntiBondageRate()		//获得技能抗束缚几率
{
	return m_buffManager.GetSkillAntiBondageRate();
}

//设置银币数目
bool CPlayer::SetSilverMoney( unsigned int moneyCount )
{
	if ( MAX_MONEY < moneyCount ) //双币种判断
	{
		D_WARNING("玩家%s，银币数量%d超过限制\n", GetAccount(), moneyCount);
		SendSystemChat( "SetSilverMoney,银币数量超过限制" );
		return false;
	}

	unsigned int oldSilver = GetSilverMoney();

	m_fullPlayerInfo.baseInfo.dwSilverMoney = moneyCount;
	CLogManager::DoMoneyChange(this, LOG_SVC_NS::CT_PLUS, 0, LOG_SVC_NS::MT_MONEY, moneyCount);//记日至//双币种判断

	NotifySilverMoney();

	//RankEventManagerSingle::instance()->MonitorPlayerMoneyChange( this, GetSilverMoney() );
	RankEventManager::MonitorPlayerSilverMoney( this, oldSilver );

	return true;
}

//设置金币数目
bool CPlayer::SetGoldMoney( unsigned int moneyCount )
{
	if ( MAX_MONEY < moneyCount ) //双币种判断
	{
		D_WARNING("玩家%s，金币数量%d超过限制\n", GetAccount(), moneyCount);
		SendSystemChat( "SetGoldMoney,金币数量超过限制,玩家" );
		return false;
	}

	unsigned int oldGold = GetGoldMoney();

	m_fullPlayerInfo.baseInfo.dwGoldMoney = moneyCount;
	//金币数量暂时不记日志，by dzj, 10.07.15, CLogManager::DoMoneyChange(this, LOG_SVC_NS::CT_PLUS, 0, LOG_SVC_NS::MT_MONEY, moneyCount);//记日至//双币种判断

	NotifyGoldMoney();

	RankEventManager::MonitorPlayerGoldMoney( this, oldGold );

	return true;
}

bool CPlayer::AddSilverMoney(unsigned int uiMoney, int addType )
{
#ifdef ANTI_ADDICTION
	if( addType == 0 )
	{
		TriedStateProfitCheck( uiMoney, TYPE_MONEY );
	}
#endif /*ANTI_ADDICTION*/

	if ( MAX_MONEY < uiMoney+GetSilverMoney() ) //双币种判断
	{
		SendSystemChat("增加的银币已超过限制");
		return false;
	}
	unsigned int oldSilver = GetSilverMoney();
	m_fullPlayerInfo.baseInfo.dwSilverMoney += uiMoney;
	CLogManager::DoMoneyChange(this, LOG_SVC_NS::CT_PLUS, addType, LOG_SVC_NS::MT_MONEY, uiMoney);//记日至

	NotifySilverMoney();

	//RankEventManagerSingle::instance()->MonitorPlayerMoneyChange( this, GetSilverMoney() );
	RankEventManager::MonitorPlayerSilverMoney( this, oldSilver );

	return true;
}

bool CPlayer::AddGoldMoney( unsigned int uiMoney, int addType ) 
{
	//金币获得不受ANTI_ADDICTION影响, by dzj, 10.07.15？

	if ( MAX_MONEY < uiMoney+GetGoldMoney() ) //双币种判断
	{
		SendSystemChat("增加的金币已超过限制");
		return false;
	}
	m_fullPlayerInfo.baseInfo.dwGoldMoney += uiMoney;
	//金币暂不记日志，by dzj, 10.07.15, CLogManager::DoMoneyChange(this, LOG_SVC_NS::CT_PLUS, 0, LOG_SVC_NS::MT_MONEY, uiMoney);//记日至

	NotifyGoldMoney();

	//金币暂不影响排行榜，by dzj, 10.07.15, RankEventManagerSingle::instance()->MonitorPlayerMoneyChange( this, GetSilverMoney() );

	return true;
};

bool CPlayer::SubSilverMoney(unsigned int moneyNum )
{
	unsigned int oldMoney = GetSilverMoney();

	if ( m_fullPlayerInfo.baseInfo.dwSilverMoney > moneyNum )
	{
		m_fullPlayerInfo.baseInfo.dwSilverMoney -= moneyNum;
	} else {
		m_fullPlayerInfo.baseInfo.dwSilverMoney  = 0;
	}

	CLogManager::DoMoneyChange(this, LOG_SVC_NS::CT_MINUS, 0, LOG_SVC_NS::MT_MONEY, oldMoney - GetSilverMoney());//记日至

	NotifySilverMoney();
	//RankEventManagerSingle::instance()->MonitorPlayerMoneyChange( this, GetSilverMoney() );
	RankEventManager::MonitorPlayerSilverMoney( this, oldMoney );
	
	return true;
}

bool CPlayer::SubGoldMoney( unsigned int moneyNum ) 
{ 
	unsigned int oldMoney = GetGoldMoney();
	if ( m_fullPlayerInfo.baseInfo.dwGoldMoney > moneyNum )
	{
		m_fullPlayerInfo.baseInfo.dwGoldMoney -= moneyNum;
	} else {
		m_fullPlayerInfo.baseInfo.dwGoldMoney  = 0;
	}

	//金币暂时不记日志，by dzj, 10.07.15,CLogManager::DoMoneyChange(this, LOG_SVC_NS::CT_MINUS, 0, LOG_SVC_NS::MT_MONEY, oldMoney - GetSilverMoney());//记日至

	NotifyGoldMoney();
	//金币排行榜暂时不管，by dzj, 10.07.15,RankEventManagerSingle::instance()->MonitorPlayerMoneyChange( this, GetGoldMoney() );
	
	return true; 
};

//09.04.28,取消工会任务新实现,///发送工会任务信息；
//void CPlayer::SendUnionTaskInfo()
//{
//	CUnion* pUnion = Union();//工会信息
//	if ( NULL == pUnion )
//	{
//		return;
//	}
//	
//	pUnion->SendUnionTaskToPlayer( this );
//
//	return;
//}

///给玩家增加一个跟随怪物；
bool CPlayer::AddFollowingMonster( unsigned int monsterType, unsigned int taskID )
{
	CMuxMap* pMap = GetCurMap();
	if ( NULL == pMap )
	{
		return false;
	}

	TYPE_POS posX = 0, posY = 0;
	bool isposok = GetPos( posX, posY );
	if ( !isposok )
	{
		return false;
	}

	if ( NULL != GetFollowingMonster() )
	{
		//当前已有跟随怪物，不可以再新增；
		return false;
	}

	CMonster* pMonsterCreated = NULL;
	CCreatorIns::SinglePtRefreshNoCreator( monsterType, pMap, posX, posY, 0, pMonsterCreated, this );//以后改为在玩家周围找一个空格，而不要直接刷在玩家同一格;		
	if( pMonsterCreated )
	{
		pMonsterCreated->OnStartFollowing( taskID );
	}
	return true;
}

int CPlayer::IsValidSuit(SuitInfo_i *pSuit, CBaseItem *pBase[], bool &isFull)
{
	if((NULL == pSuit) || (NULL == pBase))
		return -1;

	//装备是否完整？
	isFull = true;
	int count = 0;//有效道具个数
	for(int i = 0; i < EQUIP_SIZE; i++)
	{
		if(pSuit->equips[i] != 0)
		{
			CBaseItem *pTemp = NULL;		
			pTemp = GetItemDelegate().GetPkgItem(pSuit->equips[i]);
			if(NULL == pTemp)
			{
				pTemp = GetItemDelegate().GetEquipItem(pSuit->equips[i]);
			}

			if(NULL == pTemp)
			{
				isFull = false;
				pSuit->equips[i] = 0;
				pBase[i] = NULL;
			}
			else
			{
				pBase[i] = pTemp;
				count++;
			}
		}
	}

	if(count > 0)
	{
		CBaseItem *pTemp = NULL;
		for( unsigned int i = 0; i < EQUIP_SIZE; i++)
		{	
			if(NULL != pBase[i])
			{
				pTemp = pBase[i];
				if(!pTemp->IsCanUse( this, GetRace(), GetClass(), GetLevel()  ) )
				{
					SendSystemChat("种族，职业，等级原因不能使用装备\n");
					return -1;
				}

				ITEM_GEARARM_TYPE gearArmType = pTemp->GetGearArmType();
				if( gearArmType == E_POSITION_MAX )
				{
					SendSystemChat("装配部位参数无效\n");
					return -1;
				}

				if ((unsigned int)gearArmType >= ARRAY_SIZE(EQUIPTYPE_POS))
				{
					D_ERROR("CPlayer::IsValidSuit, gearArmType(%d) >= ARRAY_SIZE(EQUIPTYPE_POS)\n", gearArmType);
					return -1;
				}
				//比较该装备是否能够装备在该位置上
				//unsigned int equipPosIndex = (unsigned int)EQUIPTYPE_POS[gearArmType];
				//if ( equipPosIndex != i )	
				if ( !IsEquipPosAccord( gearArmType, i ) )
				{
					//2010.5.13 gcq 因为没有了放在不同位置的道具,所以这个功能能够被去掉
					//if ( gearArmType == E_EAR || gearArmType == E_BRACELET || gearArmType == E_FINGER )
					{
						//by dzj, 10.05.18,同上，if( equipPosIndex + 1 != i )
						{	
							SendSystemChat("存在一部位不能装备\n");
							return -1;
						}
					}
				}
			}
		}
	}

	return count;
}

void CPlayer::EquipFashionSwitch(void)
{
	if(IsFashionState())
	{
		ACE_CLR_BITS(m_fullPlayerInfo.baseInfo.tag,  PlayerInfo_1_Base::IN_FASHION_STATE);
	}
	else
	{
		ACE_SET_BITS(m_fullPlayerInfo.baseInfo.tag,  PlayerInfo_1_Base::IN_FASHION_STATE);
	}

	MGSimplePlayerInfo *pPlayerInfo = GetSimplePlayerInfo();
	if(NULL != pPlayerInfo)
	{
		CMuxMap* pPlayerMap = GetCurMap();
		if ( NULL != pPlayerMap )
		{
			pPlayerMap->NotifySurrounding<MGSimplePlayerInfo>( pPlayerInfo, GetPosX(), GetPosY() );
		}
	}

	return;
}

void CPlayer::ChanageUnionActive(unsigned short gainMode, int changeValue)
{
	if(NULL != m_pUnion)
	{
		if(0 != gainMode )//gainMode == 0：gm指令修改或物品使用
			changeValue = m_pUnion->ComputeActive(gainMode, changeValue);

		MGChangeUnionActive changeActive;
		StructMemSet( changeActive, 0x00, sizeof(changeActive));

		changeActive.addFlag = changeValue > 0 ? true :false;
		changeActive.changeValue = changeValue > 0 ? changeValue : -1*changeValue;

		SendPkg<MGChangeUnionActive>(&changeActive);
	}

	return;
}

void CPlayer::ChanageUnionPrestige(unsigned short gainMode, int chanageValue)
{
	if(NULL != m_pUnion)
	{
		if(0 != gainMode )//gainMode == 0：gm指令修改或物品使用
			chanageValue = m_pUnion->ComputePrestige(gainMode, chanageValue);

		MGChangeUnionPrestige changePrestige;
		StructMemSet( changePrestige, 0x00, sizeof(changePrestige));

		changePrestige.addFlag = chanageValue > 0 ? true :false;
		changePrestige.changeValue = chanageValue > 0 ? chanageValue : -1*chanageValue;

		SendPkg<MGChangeUnionPrestige>(&changePrestige);
	}

	return;
}

void CPlayer::SetUnionActOrPresTimes(bool activeFlag, BYTE times, unsigned int expireTime)
{
	if(NULL != m_pUnion)
	{
		MGUnionMultiReq req;
		req.type = (activeFlag ? 0 : 1);
		req.multi = times;
		req.endTime = expireTime;
		
		SendPkg<MGUnionMultiReq>(&req);
	}

	return;
}

void CPlayer::WithdrawalUnionFashion(void)
{
	for(int i = 0; i < MAX_FASHION_COUNT; i++)
	{
		if(m_fashions.fashionData[i].uiID > 0)
		{
			this->OnDropItem( m_fashions.fashionData[i].uiID );
		}
	}

	for(int i = 0; i < PACKAGE_SIZE; i++)
	{
		if(m_fullPlayerInfo.pkgInfo.pkgs[i].uiID > 0)
		{
			FashionProperty* pFashionProp = CFashionPropertyManagerSingle::instance()->GetSpecialPropertyT( m_fullPlayerInfo.pkgInfo.pkgs[i].usItemTypeID );
			if((NULL != pFashionProp) && (pFashionProp->unionFlag))
			{
				this->OnDropItem( m_fullPlayerInfo.pkgInfo.pkgs[i].uiID );
			}
		}
	}

	return;
}

void CPlayer::SaveRebirthPt(unsigned short leftOfRanage, unsigned short topOfRanage,unsigned short rightOfRanage, unsigned short bottomOfRanage)
{
	if(NULL != m_pMap)
	{
		unsigned short randPosX, randPosY;
		bool bOk = m_pMap->GetRandPointByRanage(leftOfRanage, topOfRanage, rightOfRanage,bottomOfRanage, randPosX, randPosY);
		if(bOk)
		{
			m_fullPlayerInfo.baseInfo.rebirthMapID =(unsigned short) m_pMap->GetMapNo();
			m_fullPlayerInfo.baseInfo.rebirthPosX = randPosX;
			m_fullPlayerInfo.baseInfo.rebirthPosY = randPosY;

			MGSaveRebirthPosNtf notify;
			notify.saveRebirthPos.rebirthMapID = m_fullPlayerInfo.baseInfo.rebirthMapID; 
			notify.saveRebirthPos.rebirthPosX = m_fullPlayerInfo.baseInfo.rebirthPosX;
			notify.saveRebirthPos.rebirthPosY = m_fullPlayerInfo.baseInfo.rebirthPosY;

			SendPkg<MGSaveRebirthPosNtf>(&notify);
		}
	}

	return;
}

void CPlayer::NotifyDefaultRebirthPt(void)
{
	if(NULL != m_pMap)
	{
		MGSaveRebirthPosNtf notify;
		bool flag = false;

		if(m_pMap->GetMapNo() != (unsigned long)m_fullPlayerInfo.baseInfo.rebirthMapID)
		{
			unsigned long mapID;
			MuxPoint defaultRebirthPt;
			if(m_pMap->GetDefaultRebirthPt(mapID, defaultRebirthPt))
			{
				notify.saveRebirthPos.rebirthMapID = (unsigned short)mapID; 
				notify.saveRebirthPos.rebirthPosX = defaultRebirthPt.nPosX;
				notify.saveRebirthPos.rebirthPosY = defaultRebirthPt.nPosY;
				flag = true;
			}
		}
		else
		{
			notify.saveRebirthPos.rebirthMapID = m_fullPlayerInfo.baseInfo.rebirthMapID;
			notify.saveRebirthPos.rebirthPosX = m_fullPlayerInfo.baseInfo.rebirthPosX;
			notify.saveRebirthPos.rebirthPosY = m_fullPlayerInfo.baseInfo.rebirthPosY;
			flag = true;
		}

		if(flag)
			SendPkg<MGSaveRebirthPosNtf>(&notify);
	}
	return;
}


void CPlayer::CheckWeakState( void )
{
	m_fullPlayerInfo.stateInfo.CheckWeakBuff( this );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12

///玩家非存盘信息，但是需要跨地图保存者，通知Gate;
void CPlayer::PlayerNDSMRInfoSend()
{
	TRY_BEGIN;

	//以下依次通知gate各种需要在跳地图过程中保存但又不存盘的信息；
	MGNDSMRInfo nInfo;
	nInfo.playerID = GetFullID();	//BUG
	
	if ( 0 != m_ndmsFollowingMonsterID )
	{
		//1.跟随怪物的信息；
		nInfo.infoType = NI_FOLLOW_MONSTER;
		nInfo.followMonsterType = m_ndmsFollowingMonsterID;//跟随怪物类型；
		ForceGateSend<MGNDSMRInfo>( &nInfo );
		m_ndmsFollowingMonsterID = 0;
	}

	//2 是否是流氓状态
	if( IsHaveRogueState() )
	{
		nInfo.infoType = NI_ROGUE_TIME;
		nInfo.rogueTime = GetRogueStateTime();
		ForceGateSend<MGNDSMRInfo>( &nInfo );
	}

	//是否是疲劳状态
#ifdef ANTI_ADDICTION
	if( IsTriedState() )
	{
		nInfo.infoType = NI_GAME_STATE;
		nInfo.gameState = GetAddictionState();
		ForceGateSend<MGNDSMRInfo>( &nInfo );
	}
#endif /*ANTI_ADDICTION*/

	//战斗模式
	nInfo.infoType = NI_BATTLE_MODE;
	nInfo.battleMode = GetBattleMode();
	ForceGateSend<MGNDSMRInfo>( &nInfo );

	if ( IsActivePet() ) //宠物已激活，在跳地图之间保存宠物信息
	{
		nInfo.infoType = NI_PET_INFO;
		FillPlayerPetInfo( nInfo.petInfo );
		ForceGateSend<MGNDSMRInfo>( &nInfo );

		if( IsSummonPet() )
		{
			nInfo.infoType = NI_PET_TIMER;
			FillPetTimer( nInfo.petTimer );
			ForceGateSend<MGNDSMRInfo>( &nInfo );
		}
	}

#ifdef OPEN_PUNISHMENT
	//天谴任务的计数器
	if( IsHavePunishTask() )
	{
		nInfo.infoType = NI_KILL_COUNTER;
		nInfo.punishTaskInfo.killCounter = GetKillCounter();
		nInfo.punishTaskInfo.isHavePunishTask = true;
		ForceGateSend<MGNDSMRInfo>( &nInfo );
		D_INFO("玩家%s切换地图时杀人计数为%d\n", GetNickName(), nInfo.punishTaskInfo.killCounter );
	}
#endif //OPEN_PUNISHMENT

	if ( IsAnamorphicState() )//变形状态
	{
		nInfo.infoType = NI_ANAMORPHIC_STATE;
		nInfo.anamorphicState = m_anamorphic;
		ForceGateSend<MGNDSMRInfo>(&nInfo);
	}

	//仓库信息
	if( GetPlayerStorage().GetStorageSafeStrategy() ==  E_FIRSTLOGIN_CHECK
		&& !GetPlayerStorage().IsLockStorage() ) 
	{
		nInfo.infoType = NI_STORAGE_INFO;
		nInfo.storageLock = GetPlayerStorage().IsLockStorage();
		ForceGateSend<MGNDSMRInfo>( &nInfo );
	}

	//新手破保
	if( IsRookieBreak() )
	{
		nInfo.infoType = NI_ROOKIE_BREAK_TIME;
		nInfo.rookieBreakTime = (unsigned int)( ACE_OS::gettimeofday() - m_rookieBreakTime).sec() ;
		ForceGateSend<MGNDSMRInfo>( &nInfo );
	}

	if( IsHaveBuff() )
	{
		MGTempBuffData tfBuffData;
		GetSwitchMapBuffData( tfBuffData );
		nInfo.infoType = NI_BUFF_DATA;
		nInfo.tpBuffData = tfBuffData;
		ForceGateSend<MGNDSMRInfo>( &nInfo );
	}

	unsigned int killCount = GetKillCount() ;
	if( killCount )
	{
		nInfo.infoType = NI_KILL_INFO;
		nInfo.killInfo.killCount = killCount;
		nInfo.killInfo.roleUID = GetKillRoleUID();
		ForceGateSend<MGNDSMRInfo>( &nInfo );
	}
	
	return;
	TRY_END;
	return;
}

///gate发来的玩家NDSMR信息处理
void CPlayer::OnNDSMRInfo( const GMNDSMRInfo* pNdsmrInfo )
{
	TRY_BEGIN;

	if ( NULL == pNdsmrInfo )
	{
		return;
	}

	switch ( pNdsmrInfo->infoType )
	{
	case NI_FOLLOW_MONSTER: //跟随怪物信息；
		AddFollowingMonster( pNdsmrInfo->followMonsterType, 0 );		
		break;

	case NI_ROGUE_TIME:
		CreateRogueState( pNdsmrInfo->rogueTime );
		break;

#ifdef ANTI_ADDICTION
	case NI_GAME_STATE:
		SetAddictionState( pNdsmrInfo->gameState );
		D_DEBUG("收到防沉迷的状态%d\n", pNdsmrInfo->gameState );
		break;
#endif /*ANTI_ADDICTION*/

	case NI_PET_INFO://宠物信息；
		InitPlayerPetInfo( &(pNdsmrInfo->petInfo), true );
		NotifySelfPetInfo();//如果是跳地图，则会到此处，而之前的OnPlayerAppear中因为还没有设置宠物因此不会通知，如果是直接上线，则不会到此处，因为gate的跳地图信息为空(因此也不会有宠物信息)
		break;

	case NI_BATTLE_MODE:
		{
			ChangeBattleMode( pNdsmrInfo->battleMode );
			MGChangeBattleMode modemsg;
			modemsg.bSuccess = true;
			modemsg.mode = pNdsmrInfo->battleMode;
			SendPkg<MGChangeBattleMode>( &modemsg );
		}
		break;

	case NI_KILL_COUNTER:
		if( pNdsmrInfo->punishTaskInfo.isHavePunishTask )
		{
			GivePunishTask( pNdsmrInfo->punishTaskInfo.killCounter );	//切换地图后创建任务
		}
		break;

	case NI_PET_TIMER:
		InitPetTimer( &(pNdsmrInfo->petTimer) );
		if( IsSummonPet() )
		{
			CMuxMap* pMap = GetCurMap();
			if( pMap )
			{
				MGPetinfoStable* movePetInfo = GetSurPetNotiInfo();
				if( NULL != movePetInfo )
				{
					pMap->NotifySurrounding<MGPetinfoStable>( movePetInfo, GetPosX(), GetPosY() );
				}
			}
		}
		//NotifySelfPetInfo();//如果是跳地图，则会到此处，而之前的OnPlayerAppear中因为还没有设置宠物因此不会通知，如果是直接上线，则不会到此处，因为gate的跳地图信息为空(因此也不会有宠物信息)
		break;

	case NI_COPY_INFO:
		InitCopyInfoFromCommCopyInfo( pNdsmrInfo->copyInfo );		
		break;

	case NI_ANAMORPHIC_STATE:
		{
			SetAnamorphic(pNdsmrInfo->anamorphicState);
			MGSimplePlayerInfo* movPlayerInfo = GetSimplePlayerInfo();
			CMuxMap* pMap = GetCurMap();
			if(NULL != pMap)
			{	
				pMap->NotifySurrounding( movPlayerInfo, GetPosX(), GetPosY() );	
			}
		}
		break;

	case NI_STORAGE_INFO:
		{
			GetPlayerStorage().SetStorageLockState( pNdsmrInfo->storageLock );
		}
		break;

	case NI_ROOKIE_BREAK_TIME:
		{
			ACE_Time_Value startTime = ACE_OS::gettimeofday();
			startTime -= pNdsmrInfo->rookieBreakTime;
			CreateRookieBreak( startTime );
		}
		break;

	case NI_BUFF_DATA:
		{
			PlayerID createid;
			createid.dwPID = 0;
			createid.wGID = 0;
			for( unsigned int i =0; i< pNdsmrInfo->tpBuffData.buffArrSize && i< ARRAY_SIZE(pNdsmrInfo->tpBuffData.buffDataArr); ++i )
			{
				CreateMuxBuffByID( pNdsmrInfo->tpBuffData.buffDataArr[i].buffID,  pNdsmrInfo->tpBuffData.buffDataArr[i].buffTime, createid );
			}
		}
		break;

	case NI_KILL_INFO:
		{
			SetKillInfo( pNdsmrInfo->killInfo );
		}
		break;

	default:
		{
			D_ERROR( "OnNDSMRInfo，不可识别的NDSMR信息类型%d\n", pNdsmrInfo->followMonsterType );
		}
	}

	return;
	TRY_END;
	return;
}

///玩家信息存盘
void CPlayer::PlayerDbSave()
{
	TRY_BEGIN;

	//有可能是跳地图，为了保险，总是向gate通知非存盘但跳地图需保存的信息；
	PlayerNDSMRInfoSend();

	/*
	1.send DBSavePre
	2.send 各待存信息
	3.send DBSavePost
	*/

	//1.send DBSavePre
	MGDbSavePre savePre;
	savePre.playerID = GetFullID();
	savePre.playerUID = GetDBUID();
	SafeStrCpy( savePre.roleName, GetNickName() );
	SafeStrCpy( savePre.accountName, GetAccount() );
	ForceGateSend<MGDbSavePre>( &savePre );

	//2.send 各待存信息
	MGDbSaveInfo saveInfo;
	FillPlayerPetInfo( saveInfo );
	ForceGateSend<MGDbSaveInfo>( &saveInfo );

	FillPlayerSuitInfo( saveInfo );
	ForceGateSend<MGDbSaveInfo>( &saveInfo );
	
	FillPlayerFashionInfo( saveInfo );
	ForceGateSend<MGDbSaveInfo>( &saveInfo );

	FillPlayerCopyStageInfo( saveInfo );
	ForceGateSend<MGDbSaveInfo>( &saveInfo );
	
	GetProtectItemManager().UpdatePlayerProtectItemToDB() ;

	GetNewTaskRecordDelegate().UpdateTaskRdInfoToDB();

	//3.send DBSavePost
	MGDbSavePost savePost;
	ForceGateSend<MGDbSavePost>( &savePost );

	return;
	TRY_END;
	return;
};

bool CPlayer::InitPlayerProtectItemInfo( const ProtectItemInfo& protectItemInfo )
{
	GetProtectItemManager().RecvPlayerProtectItem( (ProtectItem *)( &protectItemInfo.mProtectItemArr[0] ), protectItemInfo.mCount, protectItemInfo.isEnd );
	return true;
}

bool CPlayer::InitPlayerNewTaskRecordInfo( const TaskRdInfo& newTaskRdInfo )
{
	GetNewTaskRecordDelegate().RecvPlayerTaskRecordArr( (unsigned int *)(&newTaskRdInfo.mTaskRdIArr[0] ), newTaskRdInfo.mCount );
	return true;
}

//新玩家收到的玩家存盘信息；
bool CPlayer::OnDbGetInfo( const GMDbGetInfo* pDbGetInfo )
{
	TRY_BEGIN;

	if ( NULL == pDbGetInfo )
	{
		D_ERROR( "CPlayer::OnDbGetInfo, 输入的DbGetInfo空\n" );
		return false;
	}

	switch ( pDbGetInfo->infoType )
	{
	case (DI_PET):
		{
			InitPlayerPetInfo( &(pDbGetInfo->petInfo), false );
			break;
		}
	case (DI_SUIT):
		{
			InitPlayerSuitInfo( &(pDbGetInfo->suitInfo) );
			break;
		}
	case (DI_FASHION):
		{
			InitPlayerFashionInfo( &(pDbGetInfo->fashionInfo) );
			break;
		}
	case (DI_PROTECTITEM):
		{
			InitPlayerProtectItemInfo( pDbGetInfo->protectItemInfo );
			break;
		}
	case (DI_TASKRD):
		{
			InitPlayerNewTaskRecordInfo( pDbGetInfo->taskRdInfo );
			break;
		}
	case (DI_COPYSTAGE):
		{
			InitPlayerCopyStageInfos( pDbGetInfo->copyStageInfos );
			break;
		}
	default:
		{
			D_WARNING( "OnDbGetInfo，不可识别的DB存盘信息类型%d", pDbGetInfo->infoType );
		}
	};

	return true;
	TRY_END;
	return false;
}

///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef LEVELUP_SCRIPT

////升级脚本中不能再设置相关属性，因为相关属性都由基本属性点与加成计算而来，暂时来说升级脚本没有了功能性作用, by dzj, 10.05.26, //设置最大MaxHp
//void CPlayer::SetBaseMaxHp( unsigned int maxHp)
//{
//	m_TmpProp.lMaxHP = maxHp;
//}
//
////设置最大MaxMp
//void CPlayer::SetBaseMaxMp( unsigned int maxMp )
//{
//	m_TmpProp.lMaxMP = maxMp;
//}
//
////设置基础力量
//void CPlayer::SetBaseStr( unsigned int baseStr)
//{
//	m_TmpProp.nStrength = baseStr;
//}
//
////设置基础敏捷
//void CPlayer::SetBaseAgi( unsigned int baseAgi)
//{
//	m_TmpProp.nAgility = baseAgi;
//}
//
////设置基础智力
//void CPlayer::SetBaseInt( unsigned int baseInt )
//{
//	m_TmpProp.nIntelligence = baseInt;
//}
//
////设置基础物攻
//void CPlayer::SetBasePhyAtk( unsigned int basePhyAtk )
//{
//	m_TmpProp.nPhysicsAttack = basePhyAtk;
//}
//
////设置基础物防
//void CPlayer::SetBasePhyDef( unsigned int basePhyDef )
//{
//	m_TmpProp.nPhysicsDefend = basePhyDef;
//}
//
////设置基础魔攻
//void CPlayer::SetBaseMagAtk( unsigned int baseMagAtk )
//{
//	m_TmpProp.nMagicAttack = baseMagAtk;
//}
//
////设置基础魔防
//void CPlayer::SetBaseMagDef( unsigned int baseMagDef )
//{
//	m_TmpProp.nMagicDefend = baseMagDef;
//}
//
////设置基础物理命中
//void CPlayer::SetBasePhyHit( unsigned int basePhyHit)
//{
//	m_TmpProp.nPhysicsHit = basePhyHit;
//}
//
////设置基础魔法命中
//void CPlayer::SetBaseMagHit( unsigned int baseMagHit )
//{
//	m_TmpProp.nMagicHit = baseMagHit;
//}
//
////设置下级经验值
//void CPlayer::SetNextLevelExp( unsigned int nextLevelExp )
//{
//	m_TmpProp.lLastLevelExp = 0;
//	m_TmpProp.lNextLevelExp = nextLevelExp;
//}
//
unsigned int CPlayer::AddSpBean( unsigned int beanNumber )
{
	return AddSpPower( beanNumber * CSpSkillManager::GetPowerPerBean() );
}

#endif //LEVELUP_SCRIPT


void CPlayer::NotifyActivityState()
{
	//看天谴活动时候开始
	if( CPunishmentManager::GetPunishState() )
	{
		MGNotifyActivityState srvmsg;
		srvmsg.gateMsg.activityState = GCNotifyActivityState::ACTIVITY_START;
		srvmsg.gateMsg.activityType = GCNotifyActivityState::PUNISHMENT_ACTIVITY;
		SendPkg<MGNotifyActivityState>( &srvmsg );
	}
}


bool CPlayer::IsRaceMaster()
{
	return AllRaceMasterManagerSingle::instance()->IsRaceMaster( this );
}

bool CPlayer::IsRookieState()
{
	CMuxMap* pMap = GetCurMap();
	if( NULL == pMap )
		return false;

	//攻城战下一直是攻城破防状态
	if( pMap->InWarState() )
		return false;

	if( GetGoodEvilPoint() < 0 )
		return false;

	return ( GetLevel() < 30 );
}

bool CPlayer::IsRookieBreak()
{
	return m_bRookieBreak;
}


//察看新手是否能被攻击 
bool CPlayer::IsRookieCanBeAttack()
{
	//新手破防都能攻击
	if( IsRookieBreak() )
		return true;

	//如果不是则察看是否是新手状态 
	return (!IsRookieState());
}


void CPlayer::CreateRookieBreak( const ACE_Time_Value& rookieBreakTime )
{
	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(新手打断)

	m_rookieBreakTime = rookieBreakTime;
	m_bRookieBreak = true;

	//向客户端通知
	MGRoleattUpdate roleMsg;
	roleMsg.bFloat = false;
	roleMsg.changeTime = 0;
	roleMsg.lChangedPlayerID = GetFullID();
	roleMsg.nNewValue = 1;
	roleMsg.nType = RT_ROOKIE_BREAK;
	SendPkg<MGRoleattUpdate>( &roleMsg );
	CMuxMap* pMap = GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGRoleattUpdate>( &roleMsg, GetPosX(), GetPosY() );
	}
}

void CPlayer::EndRookieBreak()
{
	OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(新手打断)

	m_bRookieBreak = false;
	
	//向客户端通知
	MGRoleattUpdate roleMsg;
	roleMsg.bFloat = false;
	roleMsg.changeTime = 0;
	roleMsg.lChangedPlayerID = GetFullID();
	roleMsg.nNewValue = 0;
	roleMsg.nType = RT_ROOKIE_BREAK;
	CMuxMap* pMap = GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGRoleattUpdate>( &roleMsg, GetPosX(), GetPosY() );
	}
}

void CPlayer::RookieBreakCheck( unsigned int rookieBreakTime )
{
	if( !IsRookieBreak() )
		return;

	ACE_Time_Value currentTime = ACE_OS::gettimeofday();
	if( (currentTime - m_rookieBreakTime).sec() >= rookieBreakTime )
	{
		EndRookieBreak();
	}
}

void CPlayer::OnRookieStateCheck( bool isOnline )	//上线和升级的时候调用
{
	UpdateRookieState();
}


void CPlayer::UpdateRookieState()
{
	MGNotifyRookieState rookie;
	rookie.rookieMsg.rookieState = (unsigned int)IsRookieState();
	rookie.rookieMsg.characterID = GetFullID();
	CMuxMap* pMap = GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGNotifyRookieState>( &rookie, GetPosX(), GetPosY() );
	}
}


bool CPlayer::InitMuxCommonSkill( const PlayerInfo_4_Skills& skillInfo )
{
	int arrSize = ARRAY_SIZE(skillInfo.skillData);
	for(int i=0; i<arrSize; ++i)
	{
		//如果无效，略过
		if(m_fullPlayerInfo.skillInfo.skillData[i].skillID == PlayerInfo_4_Skills::INVALIDATE)
			continue;

		CMuxPlayerSkill* pSkill = g_poolManager->RetriveSkill();
		if( NULL == pSkill )
			return false;

		if( !pSkill->InitPlayerSkill( m_fullPlayerInfo.skillInfo.skillData[i], i, this ) )
		{
			D_ERROR("初始化技能错误 技能ID:%d\n", m_fullPlayerInfo.skillInfo.skillData[i].skillID );
			g_poolManager->ReleaseSkill(pSkill);
			return false;
		}
		m_vecSkillList.push_back( pSkill );
	}
	return true;
}


CMuxPlayerSkill* CPlayer::FindMuxSkill( unsigned int skillID, bool isNormalSkill )
{
	if( isNormalSkill )
	{
		for(unsigned int i=0; i<m_vecSkillList.size(); ++i)
		{
			if( NULL == m_vecSkillList[i] )
				continue;

			if( skillID == m_vecSkillList[i]->GetSkillID() )
			{
				return (m_vecSkillList[i]);
			}
		}
		return NULL;
	}else
	{
		CUnion* pUnion = Union();
		if( NULL == pUnion )
		{
			return NULL;
		}
		
		//低阶段也不能让其使用
		if( pUnion->InLowActiveState() )
			return NULL;

		return pUnion->GetSkillArgsByID( skillID );
	}
}


void CPlayer::ReleaseMuxSkill()		//释放MuxSkill
{
	CMuxPlayerSkill* pSkill = NULL;
	for( size_t i = 0; i < m_vecSkillList.size(); ++i )
	{
		pSkill = m_vecSkillList[i];
		if( NULL == pSkill )
			continue;

		g_poolManager->ReleaseSkill( pSkill );
	}
	m_vecSkillList.clear();
}


void CPlayer::AddSpExp(unsigned int uiSpExp)
{
#ifdef ANTI_ADDICTION
	this->TriedStateProfitCheck( uiSpExp, SKILL_SPEXP );
#endif //ANTI_ADDICTION

	if( GetSpLevel() == CSpSkillManager::GetMaxSpLevel() )
		return;

	m_fullPlayerInfo.baseInfo.usSpExp += uiSpExp;
	if( m_fullPlayerInfo.baseInfo.usSpExp >= m_spInfo.nextLevelExp )
	{
		m_spInfo.usSpLevel ++;	//SP level升级
		m_spInfo.nextLevelExp = CSpSkillManager::GetSpLevelExp( m_spInfo.usSpLevel );
		//升级的话自动找该等级的SP技能
		vector<CMuxSkillProp*> vecSkillProp;
		MuxSkillPropSingleton::instance()->FindSpSkill( m_spInfo.usSpLevel, GetClass(), vecSkillProp );
		for( size_t i = 0; i < vecSkillProp.size();  ++i )
		{
			AddSkillByProperty( vecSkillProp[i] );
			//MGAddSpSkillResult addResult;
			//addResult.addSpSkillID = vecSkillProp[i]->GetObjID();
			//addResult.nSkillDataIndex = 0;	//	暂时不使用
			//SendPkg<MGAddSpSkillResult>( &addResult );
		}
	}
}

//添加技能
bool CPlayer::AddSkillByID( TYPE_ID skillID )
{
	CMuxSkillProp* pSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp( skillID );
	if( !pSkillProp )
	{
		D_ERROR( "CPlayer::AddSkillByID(%d) pSkillProp=NULL\n", skillID );
		return false;
	}

	int nSkillID = pSkillProp->m_skillID;
	CMuxPlayerSkill* pSkill = FindMuxSkill( nSkillID );
	if( pSkill != NULL )
	{
		D_WARNING("CPlayer::AddSkillByID 要添加的技能%d在玩家%s已存在\n", nSkillID, GetNickName() );
		return false;  
	}

	int nClass = GetClass();
	if( !pSkillProp->IsClassSkill( nClass ) )
	{
		D_WARNING("要添加的技能不是该职业的技能\n");
		return false;
	}


	if( pSkillProp->IsSpSkill() )
	{
		if( pSkillProp->m_upgradeLevel > GetLevel() )
		{
			D_WARNING("AddSkillByID 玩家还不到升级技能的等级\n");
			return false;
		}
	}

	bool isaddok = false;

	unsigned int arraySize = (unsigned int)ARRAY_SIZE( m_fullPlayerInfo.skillInfo.skillData );
	CMuxPlayerSkill* pNewSkill = g_poolManager->RetriveSkill();
	if( NULL == pNewSkill )
	{
		return false;
	}
	for ( unsigned int i=0; i<arraySize; ++i )
	{
		if ( m_fullPlayerInfo.skillInfo.skillData[i].skillID == 0 )
		{
			m_fullPlayerInfo.skillInfo.skillData[i].skillID = nSkillID;  //更新数据库的技能栏位
			m_fullPlayerInfo.skillInfo.skillData[i].effectiveSkillID/*skillExp = 0*/= nSkillID;
			if( !pNewSkill->InitPlayerSkill( m_fullPlayerInfo.skillInfo.skillData[i], i, this ) )
			{
				m_fullPlayerInfo.skillInfo.skillData[i].skillID = 0;
				break;
			}
			isaddok = true;
			m_vecSkillList.push_back( pNewSkill );
			break;
		}
	}

	if ( !isaddok )
	{
		g_poolManager->ReleaseSkill(pSkill);
		return false;
	}

	//ADD完技能以后通知客户端增加了这个技能
	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_SKILL;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = nSkillID;
	updateAttr.lChangedPlayerID = GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	SendPkg<MGRoleattUpdate>( &updateAttr );

#ifdef NEW_SKILL_UPDATE
	UpdateSkillInfoToDB();
#endif

	return true;
}


bool CPlayer::AddSkillByProperty(CMuxSkillProp* pSkillProp)
{
	if( !pSkillProp )
	{
		D_ERROR("CPlayer::AddSkill pSkillProp=NULL\n");
		return false;
	}


	int nSkillID = pSkillProp->m_skillID;
	CMuxPlayerSkill* pSkill = FindMuxSkill( nSkillID );
	if( NULL != pSkill )
	{
		D_WARNING("CPlayer::AddSkill 要添加的技能%d在玩家%s已存在\n", nSkillID, GetNickName() );
		return false;  
	}

	unsigned int job = GetClass();
	if( !pSkillProp->IsClassSkill( job ) )
	{
		D_WARNING("要添加的技能不是该职业的技能\n");
		return false;
	}

	if( !pSkillProp->IsSpSkill() )
	{
		if( pSkillProp->m_upgradeLevel > GetLevel() )
		{
			D_WARNING("AddSkillByProperty 玩家还不到升级技能的等级\n");
			return false;
		}
	}

	CMuxPlayerSkill* pNewSkill = g_poolManager->RetriveSkill();
	if( NULL == pNewSkill )
	{
		D_ERROR("g_poolManager 中RetriveSkill为NULL\n");
		return false;
	}

	bool isAddOK = false;
	unsigned int arrSize = ARRAY_SIZE( m_fullPlayerInfo.skillInfo.skillData );
	for( unsigned int i=0; i< arrSize; ++i )
	{
		if( m_fullPlayerInfo.skillInfo.skillData[i].skillID == 0 )
		{
			m_fullPlayerInfo.skillInfo.skillData[i].skillID = nSkillID;  //更新数据库的技能栏位
			m_fullPlayerInfo.skillInfo.skillData[i].effectiveSkillID/*skillExp = 0*/= nSkillID;
			pNewSkill->SetDbIndex( i );
			pNewSkill->SetMuxSkillProp( pSkillProp ); 
			m_vecSkillList.push_back( pNewSkill );
			isAddOK = true;
			break;
		}
	}

	if (!isAddOK)
	{
		g_poolManager->ReleaseSkill(pSkill);
		return false;
	}
	

	//ADD完技能以后通知客户端增加了这个技能
	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_SKILL;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = nSkillID;
	updateAttr.lChangedPlayerID = GetFullID();
	updateAttr.changeTime = 0;
	updateAttr.bFloat = false;
	SendPkg<MGRoleattUpdate>( &updateAttr );

#ifdef NEW_SKILL_UPDATE
	UpdateSkillInfoToDB();
#endif

	return true;
}



void CPlayer::SetSkillData( unsigned int nIndex, TYPE_ID skillID, unsigned int uiExp )
{
	if(  nIndex >= ARRAY_SIZE( m_fullPlayerInfo.skillInfo.skillData ) )
	{
		D_ERROR( "SetSkillData  索引越界 %d\n",nIndex );
		return;
	}
	m_fullPlayerInfo.skillInfo.skillData[nIndex].skillID = skillID;
	m_fullPlayerInfo.skillInfo.skillData[nIndex].effectiveSkillID/*skillExp*/ = uiExp; 
}

///玩家真正进入copy;
bool CPlayer::OnPlayerEnterCurCopy( CMuxCopyInfo* pCopy )
{
	if ( NULL == pCopy )
	{
		return false;
	}
	SetStageInfo( pCopy->GetCopyMapID(), 0/*should get copy's cur stage*/, 0, GetTickCount() );
	return true;
}

/////离开组队的玩家在本mapsrv上，检测其是否在副本中，若在副本中，则发起离开副本操作，并重置副本中该玩家的初始进入信息；
//bool CPlayer::OnLeaveTeamCopyProcCheck()
//{
//	if ( !IsPlayerCurInCopy() )
//	{
//		return true;//不在副本中，无需做任何清理工作；
//	}
//
//	CMuxMap* curMap = GetCurMap();
//	if ( NULL == curMap )
//	{
//		D_ERROR( "OnLeaveTeamCopyProcCheck, 玩家%s所在地图空\n", GetAccount() );
//		return false;
//	}
//
//	return curMap->OnPlayerLeaveTeamCopyProc( this );
//}

///请求传送玩家进副本;
bool CPlayer::TransPlayerToCopyQuest( unsigned short copymapid, unsigned short scrollType )
{
	ACE_UNUSED_ARG( scrollType );

	//CMuxMapProperty* pMapProperty = CManMapProperty::FindMapProperty( copymapid );
	//if ( NULL == pMapProperty )
	//{
	//	D_ERROR( "CManMuxCopy::TransPlayerToCopyQuest, can not find map property %d\n", copymapid );
	//	return false;
	//}

	//if ( !(pMapProperty->IsCopyMap()) )
	//{
	//	D_ERROR( "CManMuxCopy::TransPlayerToCopyQuest, map %d not copy map\n", copymapid );
	//	return false;
	//}

	D_DEBUG( "向centersrv发送进真副本请求，玩家%s，副本类型%d\n", GetAccount(), copymapid );

	MGPlayerTryCopyQuest playerTryCopy;
	StructMemSet( playerTryCopy, 0, sizeof(playerTryCopy ) );
	playerTryCopy.isPCopy = false;//真副本；
	playerTryCopy.enterCopyReq.playerID = GetFullID();
	SafeStrCpy( playerTryCopy.enterCopyReq.szAccount, GetAccount() );
	playerTryCopy.enterCopyReq.teamID = GetPlayerCopyFlag();//组队
	playerTryCopy.enterCopyReq.scrollType = CounterItemByID( COPY_SCROLL_TYPE/*副本券ID号*/ );//进入时身上拥有的副本券数量；
	playerTryCopy.enterCopyReq.copyMapID = copymapid;
	playerTryCopy.enterCopyReq.mapSrvID = (unsigned short)CLoadSrvConfig::GetSelfID();

	SendPkg<MGPlayerTryCopyQuest>( &playerTryCopy );

	return true;
}

///请求传送玩家进伪副本;
bool CPlayer::TransPlayerToPCopyQuest( unsigned short copymapid, unsigned short scrollType )
{
	ACE_UNUSED_ARG( scrollType );

	if ( CounterItemByID( COPY_SCROLL_TYPE/*副本券ID号*/ ) <= 0 )
	{
		D_DEBUG( "TransPlayerToPCopyQuest,玩家%s试图进伪副本，但副本券数量为0\n", GetAccount() );
		return false;
	}

	CNewMap* pFoundPCopyMap = CManNormalMap::FindMap( copymapid );
	if ( NULL == pFoundPCopyMap )
	{
		D_ERROR( "CManMuxCopy::TransPlayerToPCopyQuest, can not find pcopy %d\n", copymapid );
		return false;
	}

	D_DEBUG( "向gatesrv发送进伪副本请求，玩家%s，伪副本类型%d\n", GetAccount(), copymapid );

	MGPlayerTryCopyQuest playerTryPCopy;
	StructMemSet( playerTryPCopy, 0, sizeof(playerTryPCopy ) );
	playerTryPCopy.isPCopy = true;//伪副本；
	playerTryPCopy.enterCopyReq.playerID = GetFullID();
	SafeStrCpy( playerTryPCopy.enterCopyReq.szAccount, GetAccount() );
	playerTryPCopy.enterCopyReq.teamID = GetPlayerCopyFlag();//组队
	playerTryPCopy.enterCopyReq.scrollType = CounterItemByID( COPY_SCROLL_TYPE/*副本券ID号*/ );//进入时身上拥有的副本券数量
	playerTryPCopy.enterCopyReq.copyMapID = copymapid;
	playerTryPCopy.enterCopyReq.mapSrvID = (unsigned short)CLoadSrvConfig::GetSelfID();

	SendPkg<MGPlayerTryCopyQuest>( &playerTryPCopy );

	return true;
}

///发起玩家离开副本下半部
bool CPlayer::IssuePlayerLeaveCopyLF()
{
	/*
	///进入副本请求结构
	struct EnterCopyReq
	{
	PlayerID       playerID;
	char           szAccount[MAX_NAME_SIZE];		//帐号信息
	unsigned int   teamID;//请求者所在队伍；
	unsigned short copyMapID;//副本地图ID号；
	unsigned short mapSrvID;//请求者所在mapsrv；
	};

	///副本管理器中的元素结构
	struct CopyManagerEle
	{
	unsigned short mapsrvID;//副本所在mapsrv的ID号；
	unsigned int   copyID;//副本ID号，全局唯一；
	EnterCopyReq   enterReq;//创建者的进入请求；
	unsigned int   createTime;//创建时间；
	unsigned short curPlayerNum;//当前副本中人数；
	unsigned int   delIssueTime;
	bool           isNoMorePlayer;//是否不允许更多玩家进入；
	};    
	*/
	if ( !(m_CopyInfo.IsIssueLeaveHF()) )
	{
		D_ERROR( "CPlayer::IssuePlayerLeaveCopyLF, 玩家%s之前未进入HF\n", GetAccount() );
		return false;
	}

	if ( m_CopyInfo.IsIssueLeaveLF() )
	{
		//D_WARNING( "重复请求玩家%s离开副本，LF，可能上次请求还未来得及执行的情况下，又检测到此玩家在副本中\n", GetAccount() );
		return false;
	}
	m_CopyInfo.SetIssueLeaveLF();

	unsigned int copyID=0;
	unsigned short copyMapID=0;
	if ( !GetCurCopyMapInfo( copyID, copyMapID ) )
	{
		D_WARNING( "CPlayer::IssuePlayerLeaveCopyLF, 发起玩家%s离开当前副本，下半部，取玩家副本信息失败\n", GetAccount() );
		return false;
	}

	MGPlayerLeaveCopyQuest leaveQuest;//欲离开的副本信息(只有部分字段填充，包括：玩家ID号，副本ID号，副本地图号，玩家帐号)；
	StructMemSet( leaveQuest, 0, sizeof(leaveQuest) );
	leaveQuest.lNotiPlayerID = GetFullID();//player full ID;
	leaveQuest.leaveCopyInfo.enterReq.playerID = GetFullID();//player full ID;
	leaveQuest.leaveCopyInfo.copyID = copyID;//副本ID号，防止本消息到达centersrv时，玩家已进入了另一个副本；
	leaveQuest.leaveCopyInfo.enterReq.copyMapID = copyMapID;//副本地图号，校验用；
	SafeStrCpy( leaveQuest.leaveCopyInfo.enterReq.szAccount, GetAccount() );//玩家帐号，用于centersrv查找玩家playerGateID;
	SendPkg<MGPlayerLeaveCopyQuest>( &leaveQuest );

	D_DEBUG( "CPlayer::IssuePlayerLeaveCopyLF, 发起玩家%s离开当前副本，下半部，向gate发离开请求\n", GetAccount() );

	return true;
}

///发起玩家离开副本上半部
bool CPlayer::IssuePlayerLeaveCopyHF( bool isSelfSendMsg )
{
	if ( !IsPlayerCurInCopy() )
	{
		D_WARNING( "请求玩家%s离开副本时，该玩家不在副本中\n", GetAccount() );
		return false;
	}

	unsigned int copyID=0;
	unsigned short copyMapID=0;
	if ( !GetCurCopyMapInfo( copyID, copyMapID ) )
	{
		D_WARNING( "请求玩家%s离开副本，取玩家copyID与copyMapID失败\n", GetAccount() );
		return false;
	}

	if ( m_CopyInfo.IsIssueLeaveHF() )
	{
		//D_WARNING( "重复请求玩家%s离开副本，HF，可能上次请求还未来得及执行的情况下，又检测到此玩家在副本中\n", GetAccount() );
		return false;
	}
	m_CopyInfo.SetIssueLeaveHF();

	D_DEBUG( "CPlayer::IssuePlayerLeaveCopyHF，准备发起玩家%s离开当前副本，上半部\n", GetAccount() );	

	if ( isSelfSendMsg )
	{
		//就在这里通知玩家而不是通过稍后的广播
		MGTimeLimit timeLimit;
		timeLimit.limitTime.timeLimit = 60;//副本倒计时永远60秒；
		timeLimit.limitTime.limitReason = 1;//预备踢出副本；
		SendPkg<MGTimeLimit>( &timeLimit );//通知客户端倒计时；
	}

	return true;
}

//...copy
/////////////////////////////////////////////////////////


void CPlayer::BuffProcess()
{
	m_buffManager.BuffProcess();
}


void CPlayer::ClearBuffData()
{
	StructMemSet( m_fullPlayerInfo.bufferInfo, 0, sizeof( m_fullPlayerInfo.bufferInfo ) );
}

void CPlayer::UpdateBuffData( unsigned int saveType  ) // -1 离线时处理 -0 平时的处理
{
	ClearBuffData();
	m_buffManager.UpdateBuffData( m_fullPlayerInfo.bufferInfo, saveType );
	if ( 0 == saveType ) //根据原来的代码修改而来，原代码每次以参数0后调用后，必定SendInfoToMember。故作此修改。by dzj, 10.03.29;
	{
		SetNeedNotiTeamMember();
	}
}


bool CPlayer::ActiveEndBuff( unsigned int buffIndex )  //主动结束buff
{
	return m_buffManager.ActiveEndBuff( buffIndex );
}

bool CPlayer::ActiveEndRestBuff()
{
	return m_buffManager.ActiveEndRestBuff();
}


void CPlayer::AddRestSkillInfo( MuxBuffProp* pBuffProp )
{
	if( NULL == pBuffProp )
		return;

	if( pBuffProp->AssistType == BUFF_SP_COMPLEX )
	{
		for( size_t i =0; i< pBuffProp->vecBuffProp.size(); ++i)
		{
			AtomRestCheck( pBuffProp->vecBuffProp[i] );
		}
	}else
	{
		AtomRestCheck( pBuffProp );
	}
}


void CPlayer::RestSkillCheck( CMuxSkillProp* pSkillProp )
{
	if( NULL == pSkillProp )
		return;

	if( pSkillProp->m_skillType == 5 )
	{
		m_restSkillInfo.isHavePassiveSkill = true;
		AddRestSkillInfo( pSkillProp->m_pBuffProp );
	}
}


void CPlayer::AtomRestCheck( MuxBuffProp* pBuffProp )
{
	if( NULL == pBuffProp )
		return;

	switch(  pBuffProp->AssistType )
	{
	case MAG_HOT:
		m_restSkillInfo.mp = pBuffProp->Value;
		break;

	case MAG_PER_HOT:
		m_restSkillInfo.perMp = (pBuffProp->Value / 100.f);
		break;

	case BUFF_HP_HOT:
		m_restSkillInfo.hp = pBuffProp->Value;
		break;

	case BUFF_PERCENT_HOT:
		m_restSkillInfo.perHp = (pBuffProp->Value / 100.f);
		break;
	}
}


void CPlayer::RestSkillProcess()
{
	if( !m_restSkillInfo.isHavePassiveSkill || GetCurrentHP() == 0 )
	{
		return;
	}

	unsigned int maxHp = GetMaxHP();
	unsigned int maxMp = GetMaxMP();

	bool isNotiHp = ( maxHp != GetCurrentHP() );
	bool isNotiMp = ( maxMp != GetCurrentMP() );
		
	if( isNotiHp )
	{
		unsigned int addHp = (unsigned int)(maxHp*m_restSkillInfo.perHp) + m_restSkillInfo.hp;
		AddPlayerHp( addHp );
		NotifyHpChange();
	}

	if( isNotiMp )
	{
		unsigned int addMp = (unsigned int)(maxMp*m_restSkillInfo.perMp) + m_restSkillInfo.mp;
		AddPlayerMp( addMp );
		NotifyMpChange();
	}
}


unsigned int CPlayer::GetTeamID()
{
	CGroupTeam* pTeam = GetGroupTeam();
	if( NULL == pTeam )
		return 0;
	return pTeam->GetGroupTeamID();
}


void CPlayer::NotifyDamageRebound( unsigned int skillID, unsigned int reboundDamage, EBattleFlag batFlg )
{
	MGCharacterDamageRebound damReb;
	damReb.gcRebound.characterID = GetFullID();
	damReb.gcRebound.skillID = skillID;
	damReb.gcRebound.reboundDamage = reboundDamage;
	damReb.gcRebound.battleFlag = batFlg;
	if ( (reboundDamage <= 0) && (batFlg != MISS) )
	{
		D_WARNING( "CPlayer::NotifyDamageRebound，伤血为0但返回为非MISS, skillID%d\n", skillID );
	}
	CMuxMap* pMap = GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGCharacterDamageRebound>( &damReb, GetPosX(), GetPosY() );
	}
}

void CPlayer::AllocatePoint(unsigned short toStr, unsigned short toVit, unsigned short toAgi, unsigned short toSpi, unsigned short toInt)
{
	unsigned short totalToAllocate = toStr + toVit + toAgi + toSpi + toInt;
	if(0 == totalToAllocate)
	{
		SendSystemChat("分配方案中各值都为0");
		return;
	}
	else if((totalToAllocate < toStr) || (totalToAllocate < toVit) || (totalToAllocate < toAgi)  || (totalToAllocate < toSpi) || (totalToAllocate < toInt))
	{
		SendSystemChat("分配方案无效");
		return;
	}
	else if(m_fullPlayerInfo.baseInfo.unallocatePoint < totalToAllocate)
	{
		SendSystemChat("剩余属性点不足");
		return;
	}
	
	unsigned short oldValue = 0;
	MGRoleattUpdate updatemsg;
	if(toStr > 0)
	{
		oldValue = m_fullPlayerInfo.baseInfo.strength;
		m_fullPlayerInfo.baseInfo.strength += toStr;
		
		updatemsg.nType = RT_STR;
		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType = 0;
		updatemsg.nOldValue = oldValue ;
		updatemsg.nNewValue = m_fullPlayerInfo.baseInfo.strength;
		SendPkg<MGRoleattUpdate>(&updatemsg);
	}
		
	if(toVit > 0)
	{
		oldValue = m_fullPlayerInfo.baseInfo.vitality;
		m_fullPlayerInfo.baseInfo.vitality += toVit;

		updatemsg.nType = RT_VIT;
		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType = 0;
		updatemsg.nOldValue = oldValue ;
		updatemsg.nNewValue = m_fullPlayerInfo.baseInfo.vitality;
		SendPkg<MGRoleattUpdate>(&updatemsg);
	}
		
	if(toAgi > 0)
	{
		oldValue = m_fullPlayerInfo.baseInfo.agility;
		m_fullPlayerInfo.baseInfo.agility += toAgi;

		updatemsg.nType = RT_AGI;
		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType = 0;
		updatemsg.nOldValue = oldValue ;
		updatemsg.nNewValue = m_fullPlayerInfo.baseInfo.agility;
		SendPkg<MGRoleattUpdate>(&updatemsg);
	}
		
	if(toSpi > 0)
	{
		oldValue = m_fullPlayerInfo.baseInfo.spirit;
		m_fullPlayerInfo.baseInfo.spirit += toSpi;

		updatemsg.nType = RT_SPI;
		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType = 0;
		updatemsg.nOldValue = oldValue ;
		updatemsg.nNewValue = m_fullPlayerInfo.baseInfo.spirit;
		SendPkg<MGRoleattUpdate>(&updatemsg);
	}
		
	if(toInt > 0)
	{
		oldValue = m_fullPlayerInfo.baseInfo.intelligence;
		m_fullPlayerInfo.baseInfo.intelligence += toInt;

		updatemsg.nType = RT_INT;
		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType = 0;
		updatemsg.nOldValue = oldValue ;
		updatemsg.nNewValue = m_fullPlayerInfo.baseInfo.intelligence;
		SendPkg<MGRoleattUpdate>(&updatemsg);
	}
	
	//未分配属性点变化通知
	oldValue = m_fullPlayerInfo.baseInfo.unallocatePoint;
	m_fullPlayerInfo.baseInfo.unallocatePoint -= totalToAllocate;
	
	updatemsg.nType = RT_UNALLOCATEPOINT;
	updatemsg.bFloat = false;
	updatemsg.changeTime = 0;
	updatemsg.lChangedPlayerID = GetFullID();
	updatemsg.nAddType = 0;
	updatemsg.nOldValue = oldValue;
	updatemsg.nNewValue = m_fullPlayerInfo.baseInfo.unallocatePoint;
	SendPkg<MGRoleattUpdate>(&updatemsg);

	return;
}

void CPlayer::ManualUpgradeLevel(void)
{
	//当前经验是否可以升级
	if( GetCurrentHP() == 0 )//死亡
		return;

	if(GetLevel() < 30)//30级前不能手动升级
		return;

	if( NULL == m_pClassLevelProp )//如果已经是最高等级了无需再处理了
	{
		D_WARNING( "CPlayer::ManualUpgradeLevel, NULL == m_pClassLevelProp, player:%s\n", GetAccount() );
		return;
	}

	if( m_pClassLevelProp->GetMaxLevel() == GetLevel() )
	{
		//m_fullPlayerInfo.baseInfo.dwExp = 0;
		return;
	}

	//如果升级的话...
	if( m_fullPlayerInfo.baseInfo.dwExp  >= GetNextLevelExp() )
	{
		unsigned int oldExp = m_fullPlayerInfo.baseInfo.dwExp;
		unsigned short oldLevel = m_fullPlayerInfo.baseInfo.usLevel;
		unsigned short oldUnallocatePoint = m_fullPlayerInfo.baseInfo.unallocatePoint;
		do
		{
			++m_fullPlayerInfo.baseInfo.usLevel; //升级
			m_fullPlayerInfo.baseInfo.dwExp -= GetNextLevelExp();	//升级减去需要的经验值
			m_fullPlayerInfo.baseInfo.unallocatePoint += 500;//每升一级加5个属性点

			if ( !CalculateTmpProp() )//获取新等级的非存盘数据,也就是下一级的经验
			{
				D_DEBUG("获取非存盘数据时发生错误\n");
			}

#ifdef LEVELUP_SCRIPT	//脚本优先
			if( NULL != m_pLevelUpScript )
			{
				//m_pLevelUpScript->OnPlayerSetBaseProp( this );	//设置基础属性
				m_pLevelUpScript->OnPlayerLevelUp( this );		//设置升级时的特殊操作
			}
#endif //LEVELUP_SCRIPT

			if( GetLevel() == m_pClassLevelProp->GetMaxLevel() )
			{
				//m_fullPlayerInfo.baseInfo.dwExp = 0;
				break;
			}

		}while(false/*m_fullPlayerInfo.baseInfo.dwExp  >= GetNextLevelExp()*/);

		CLogManager::DoPlayerLevelUp(this, oldLevel);//记日至
		unsigned int level = GetLevel();
		AddFriendTweet(MUX_PROTO::TWEET_TYPE_UPGRADE, (const void *)&level, sizeof(unsigned int));//记录好友动态

		//属性变化通知
		MGRoleattUpdate updatemsg;
		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType = GCPlayerLevelUp::IN_BATTLE;
		updatemsg.nOldValue = oldExp;
		updatemsg.nNewValue = GetExp();
		updatemsg.nType = RT_EXP;
		SendPkg<MGRoleattUpdate>(&updatemsg);//经验属性变化

		updatemsg.bFloat = false;
		updatemsg.changeTime = 0;
		updatemsg.lChangedPlayerID = GetFullID();
		updatemsg.nAddType = GCPlayerLevelUp::IN_BATTLE;
		updatemsg.nOldValue = oldUnallocatePoint;
		updatemsg.nNewValue = GetUnallocatPoint();
		updatemsg.nType = RT_UNALLOCATEPOINT;
		SendPkg<MGRoleattUpdate>(&updatemsg);//未分配属性点变化

		////临时数据变化通知
		////该消息已废除，因为相关属性已另行计算或通知, by dzj, 10.05.26, MGTmpInfoUpdate tmpInfoUpdate;
		//tmpInfoUpdate.playerID = GetFullID();
		//tmpInfoUpdate.tmpInfo = m_TmpProp;
		//tmpInfoUpdate.tmpInfo.fSpeed = GetSpeed();
		//SendPkg<MGTmpInfoUpdate>(&tmpInfoUpdate);

		//升级要补满血和魔
		SetPlayerHp( GetMaxHP() );
		SetPlayerMp( GetMaxMP() );

		//通知下一等级需要经验
		NotifyNextLevelExp();

		if(m_pMap != NULL)
		{
			m_pMap->HandlePlayerLevelUp(this, GCPlayerLevelUp::IN_BATTLE );
		}
	}

	return;
}

void CPlayer::ModifyEffectiveSkill(unsigned int skillID, bool incFlag)
{
	//获取技能信息
	CMuxPlayerSkill* pSkill = FindMuxSkill( skillID );
	if( pSkill == NULL )
	{
		D_WARNING("CPlayer::ModifyEffectiveSkill 指定的技能%d在玩家%s上不存在\n", skillID, GetNickName() );
		return;  
	}

	if( pSkill->IsSpSkill() )
	{
		D_WARNING("要改变的战斗技能是SP技能 ID:%d\n",skillID );
		return;
	}

	unsigned int skillType = pSkill->GetSkillType();
	//非战斗技能也可以升级，by dzj, 10.06.18, if( !(skillType == TYPE_PHYSICS || skillType == TYPE_MAGIC) )
	//{
	//	D_WARNING("要改变的技能不能是辅助技能 ID:%d\n", skillID);
	//	return;
	//}

	//当前有效技能编号，有效技能属性
	unsigned int effectiveSkillID = pSkill->GetEffectiveSkillID();
	CMuxSkillProp* effectiveSkillProp = pSkill->GetSkillProp();
	if(NULL == effectiveSkillProp)
	{
		D_WARNING("CPlayer::ModifyEffectiveSkill 没有找到技能%d对应的属性\n", skillID );
		return;
	}

	bool successFlag = false;
	CMuxSkillProp* targetEffectiveSkillProp = NULL;
	if(incFlag)//升级
	{
		if(skillID != effectiveSkillID)
		{
			targetEffectiveSkillProp = MuxSkillPropSingleton::instance()->FindTSkillProp(effectiveSkillProp->GetNextSkillID());
			if(NULL != targetEffectiveSkillProp)
				successFlag = true;
		}
	}
	else//降级
	{
		std::map<unsigned int, CMuxSkillProp*>& propList = MuxSkillPropSingleton::instance()->SkillProps();
		for( map<unsigned int, CMuxSkillProp*>::iterator iter = propList.begin(); iter != propList.end(); iter++ )
		{
			if(NULL != iter->second)
			{
				if(iter->second->GetNextSkillID() == effectiveSkillID)//查找有效技能的前置技能
				{	
					targetEffectiveSkillProp = iter->second;
					successFlag = true;
					break;
				}
			}
		}
	}

	MGModifyEffectSkillIDRst rstMsg;
	rstMsg.rst.recode = successFlag;
	if(successFlag)
	{
		//设置属性
		pSkill->SetMuxSkillProp(targetEffectiveSkillProp, false);
		rstMsg.rst.newSkill.skillID = skillID;
		rstMsg.rst.newSkill.effectiveSkillID = targetEffectiveSkillProp->m_skillID;
	}

	SendPkg<MGModifyEffectSkillIDRst>(&rstMsg);

	return;
}

void CPlayer::UpdateExpireTimeItems(const ACE_Time_Value& onLineTV, const ACE_Time_Value& offLineTV)
{
	for(unsigned int i = 0; i < ARRAY_SIZE(m_fullPlayerInfo.equipInfo.equips); i++)
	{
		ItemInfo_i &itemInfo = m_fullPlayerInfo.equipInfo.equips[i];
		if(itemInfo.uiID != PlayerInfo_2_Equips::INVALIDATE)
		{	
			CItemPublicProp *pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(itemInfo.usItemTypeID);
			if((NULL != pPublicProp) && (ACE_BIT_ENABLED(pPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY)))
			{
				unsigned expireTime = onLineTV.sec() +  itemInfo.usWearPoint;
				if(expireTime <= offLineTV.sec())
					itemInfo.usWearPoint = 0;
				else
					itemInfo.usWearPoint = expireTime - offLineTV.sec();
			}
		}
	}

	for(unsigned int i = 0; i < ARRAY_SIZE(m_fullPlayerInfo.pkgInfo.pkgs); i++)
	{
		ItemInfo_i &itemInfo = m_fullPlayerInfo.pkgInfo.pkgs[i];
		if(itemInfo.uiID != 0)
		{	
			CItemPublicProp *pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(itemInfo.usItemTypeID);
			if((NULL != pPublicProp) && (ACE_BIT_ENABLED(pPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY)))
			{
				unsigned expireTime = onLineTV.sec() +  itemInfo.usWearPoint;
				if(expireTime <= offLineTV.sec())
					itemInfo.usWearPoint = 0;
				else
					itemInfo.usWearPoint = expireTime - offLineTV.sec();
			}
		}
	}

	for(unsigned int i = 0; i < ARRAY_SIZE(m_fashions.fashionData); i++)
	{
		ItemInfo_i &itemInfo = m_fashions.fashionData[i];
		if(itemInfo.uiID != 0)
		{	
			CItemPublicProp *pPublicProp = CItemPublicPropManagerExSingle::instance()->GetItemPublicProp(itemInfo.usItemTypeID);
			if((NULL != pPublicProp) && (ACE_BIT_ENABLED(pPublicProp->m_wearConsumeMode, CItemPublicProp::LAST_DAY)))
			{
				unsigned expireTime = onLineTV.sec() +  itemInfo.usWearPoint;
				if(expireTime <= offLineTV.sec())
					itemInfo.usWearPoint = 0;
				else
					itemInfo.usWearPoint = expireTime - offLineTV.sec();
			}
		}
	}

	return;
}

void CPlayer::AddFriendTweet(unsigned short tweetType, const void* tweetInfo, unsigned short tweetInfoLen)
{
	/*if(GetFriendNum() == 0)
		return;*/

	bool needRecord = true;
	MGAddTweetNtf addTweet;
	addTweet.tweet.createdTime = (UINT)ACE_OS::time(NULL);
	addTweet.tweet.type = tweetType;
	switch(tweetType)
	{
	case MUX_PROTO::TWEET_TYPE_KILL_BOSS://好友击杀BOSS
		{
			const unsigned int *pBossType = (unsigned int *)tweetInfo;
			StructMemCpy(addTweet.tweet.tweet, pBossType, sizeof(*pBossType));
			addTweet.tweet.tweetLen = sizeof(*pBossType);
		}
		break;

	case MUX_PROTO::TWEET_TYPE_GET_TITLE://好友获得称号
		{
			const unsigned int *pTitle = (unsigned int *)tweetInfo;
			StructMemCpy(addTweet.tweet.tweet, pTitle, sizeof(*pTitle));
			addTweet.tweet.tweetLen = sizeof(*pTitle);
		}
		break;

	case MUX_PROTO::TWEET_TYPE_GET_ACH://好友获得成就
		{

		}
		break;

	case MUX_PROTO::TWEET_TYPE_UPGRADE://好友升级
		{
			const unsigned int *pNewLevel = (unsigned int *)tweetInfo;
			StructMemCpy(addTweet.tweet.tweet, pNewLevel, sizeof(*pNewLevel));
			addTweet.tweet.tweetLen = sizeof(*pNewLevel);
		}
		break;

	case MUX_PROTO::TWEET_TYPE_DEAD://好友死亡
		{
			//null
		}
		break;

	case MUX_PROTO::TWEET_TYPE_KILL_PLAYER://好友击杀玩家
		{
			const char *beKilledPlayer = (const char *)tweetInfo;
			StructMemCpy(addTweet.tweet.tweet, beKilledPlayer, (UINT)strlen(beKilledPlayer));
			addTweet.tweet.tweetLen = strlen(beKilledPlayer);
		}
		break;

	case MUX_PROTO::TWEET_TYPE_EQUIPMENT_UPGRADE://好友装备升级
		{
			const ItemInfo_i *pItem = (ItemInfo_i *)tweetInfo;
			StructMemCpy(addTweet.tweet.tweet, pItem, sizeof(*pItem));
			addTweet.tweet.tweetLen = sizeof(*pItem);
		}
		break;

	case MUX_PROTO::TWEET_TYPE_EQUIPMENT_MOSAIC://好友装备镶嵌
		{

		}
		break;

	case MUX_PROTO::TWEET_TYPE_EQUIPMENT_TRANSFORM://好友装备改造
		{

		}
		break;

	case MUX_PROTO::TWEET_TYPE_UPLINE://好友上线
	case MUX_PROTO::TWEET_TYPE_OFFLINE://好友下线
	case MUX_PROTO::TWEET_TYPE_ADD_FRIEND://好友加新的好友
	case MUX_PROTO::TWEET_TYPE_JOIN_GUILD://好友加入战盟
	case MUX_PROTO::TWEET_TYPE_LEAVE_GUILD://好友离开战盟
	case MUX_PROTO::TWEET_TYPE_SHOPPING://好友购买商城物品
	default:
		{
			//其他服务器处理
			needRecord = false;
		}
		break;
	}

	if(needRecord)
		SendPkg<MGAddTweetNtf>(&addTweet);

	return;
}

void CPlayer::AddFriendDegree(unsigned int friendId, unsigned int friendDegree)
{
	/*if(GetFriendNum() == 0)
		return;*/

	MGAddFriendlyDegreeReq addDegree;
	addDegree.friendUiid = friendId;
	addDegree.degreeAdded = friendDegree;
	
	SendPkg<MGAddFriendlyDegreeReq>(&addDegree);

	return;
}

void CPlayer::AddExpAffectFriendGain(unsigned int exp, bool isBoss)
{
	/*if(GetFriendNum() == 0)
		return;*/

	MGReportFriendNonTeamGain nonTeamGain;
	nonTeamGain.exp = exp;
	nonTeamGain.isBoss = isBoss;

	SendPkg<MGReportFriendNonTeamGain>(&nonTeamGain);

	return;
}

void CPlayer::OnRecvAdditionalFriendGain(bool inTeam, unsigned int expOrNot)
{
	/*if(GetFriendNum() == 0)
		return;*/

	if(inTeam/* && IsInTeam()*/)
	{
		SetFriendGroupGainAddi(expOrNot);
#ifdef _DEBUG
		D_INFO("玩家%s的好友组队收益设置为%u\n", GetNickName(),expOrNot);
#endif
	}
	else
	{
		AddExp(expOrNot, GCPlayerLevelUp::IN_BATTLE, false);
	}

	return;
}
