﻿#include "FightPet.h"
#include "Player.h"

#include "../MuxMap/muxmapbase.h"
#include "../LogManager.h"

///重置战斗宠相关属性
void CFightPet::FightPetReset()
{
	StructMemSet( m_petInfoPrivate, 0, sizeof(m_petInfoPrivate) );//战斗宠易变信息(只宠物主人知晓)
	StructMemSet( m_petInfoPublic, 0, sizeof(m_petInfoPublic) );//战斗宠稳定信息(宠物主人及周边人知晓)
	m_pPetOwner = NULL;				//战斗宠主人
	m_resExtItemInfo = NULL;//相应道具的额外信息
}

///使用战斗宠道具初始化宠物信息
bool CFightPet::FightPetFromItem( FightPetItemExtInfo* petItemExtInfo )		
{
	return false;
}

//获得用于向周围人广播的战斗宠信息；
MGFightPetPublic* CFightPet::GetSurFightPetNotiInfo()
{
	return NULL;
}

//战斗宠状态变化时，通知周围人此变化信息；
void CFightPet::NotifySurFightPetInfo( bool isOnlySelf /*是否只通知自己*/ )
{
	if ( NULL == m_pPetOwner )
	{
		//宠物不属于任何玩家;
		D_ERROR( "CFightPet::NotifySurPetInfo，战斗宠所属玩家空" );
		return;
	}

	CMuxMap* pPlayerMap = m_pPetOwner->GetCurMap();
	if ( NULL == pPlayerMap )
	{
		return;
	}

	return;
}

//收回战斗宠(反激活)
bool CFightPet::CloseFightPet()						
{
	return true;
}

//通知战斗宠主人，战斗宠的完整信息；
bool CFightPet::NotifySelfFightPetInfo()
{
	//NotifyInstableInfo();
	//NotifyStableInfo();
	return true;
}

void CFightPet::UpdateResFightPetItem()
{
	//if ( m_pPetOwner )
	//{
	//	MGDbSaveInfo saveInfo;
	//	saveInfo.playerID = m_pPetOwner->GetFullID();
	//	m_pPetOwner->FillPlayerPetInfo( saveInfo );
	//	m_pPetOwner->ForceGateSend<MGDbSaveInfo>( &saveInfo );
	//}
}

//通知战斗宠主人，战斗宠不稳定信息改变
void CFightPet::NotifyFightPetInstableInfo()					
{
	if( NULL == m_pPetOwner )
	{
		D_ERROR("CFightPet::NotifyInstableInfo m_pPetOwner为空\n");
		return;
	}

	//m_pPetOwner->SendPkg<MGPetinfoInstable>( &m_petInfoPrivate );
}

//通知战斗宠主人，战斗宠稳定信息改变
void CFightPet::NotifyFightPetStableInfo()					
{
	if( NULL == m_pPetOwner )
	{
		D_ERROR("CFightPet::NotifyStableInfo m_pPetOwner为空\n");
		return;
	}

	////已召出，则通知周围人；
	//CMuxMap* pMap = m_pPetOwner->GetCurMap();
	//if( NULL == pMap )
	//{
	//	D_ERROR("CFightPet::NotifyStableInfo pMap为空\n");
	//	return;
	//}
	//m_petInfoPublic.petInfoStable.isNewSummoned = true;//内部通知永远为新召出，只有外部移动通知时才为旧宠物；
	//pMap->NotifySurrounding<MGPetinfoStable>( &m_petInfoPublic, m_pPetOwner->GetPosX(), m_pPetOwner->GetPosY() );
	//m_petInfoPublic.petInfoStable.isNewSummoned = false;//内部通知永远为新召出，只有外部移动通知时才为旧宠物；
}

/////填充DB存盘信息，以便存盘；
//bool CFightPet::FillFightPetItemInfo( MGDbSaveInfo& dbSaveInfo ) 
//{ 
//	//dbSaveInfo.infoType = DI_PET;
//	//StructMemCpy( (dbSaveInfo.petInfo.petInfoInstable), &(m_petInfoPrivate), sizeof(dbSaveInfo.petInfo.petInfoInstable) );
//	//StructMemCpy( (dbSaveInfo.petInfo.petInfoAbility), &(m_petAbility), sizeof(dbSaveInfo.petInfo.petInfoAbility) );
//	//StructMemCpy( (dbSaveInfo.petInfo.petInfoStable), &(m_petInfoPublic), sizeof(dbSaveInfo.petInfo.petInfoStable) );
//	return true; 
//};

