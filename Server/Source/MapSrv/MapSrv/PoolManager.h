﻿/* @file PoolManager.h 
@brief 此类主要集成内存工作
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：hyn
*	完成日期：
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef POOLMANAGER_H
#define POOLMANAGER_H

#include "tcache/tcache.h"
#include "MuxMap/MapCommonHead.h"
//#include "Player.h"
#include "ScampBuff.h"
#include "MuxPlayerSkill.h"

class CPlayer;
class CGateSrv;

class CPoolManager
{

private:
	CPoolManager( const CPoolManager& );  //屏蔽这两个操作；
	CPoolManager& operator = ( const CPoolManager& );//屏蔽这两个操作；

public:
	///构造
	CPoolManager()
	{
		m_pPlayerPool = NULL;
	}

	///析构
	~CPoolManager()
	{
		Clear();
	}

	void Init()
	{
		m_pPlayerPool = NULL;
		//m_pMonsterPool = NULL;
	}


	///开辟内存
	bool Intialize(int nMapCapacity = 300000, int nPlayerCapacity = 5000, int nMonsterCapacity = 1000);

	void Clear();
	
	///取一个玩家对象；
	CPlayer* RetrivePlayer();

	//取一个技能对象
	CMuxPlayerSkill* RetriveSkill();

	///取一个gatesrv对象；
	CGateSrv* RetriveGateSrv();

	CScampBuff* RetriveScampBuff()
	{
		return m_pScampBuffPool->RetrieveOrCreate();
	}
 

	void ReleaseGateSrv( CGateSrv* pGateSrv );

	///释放
	void Release( CPlayer* pPlayer );
	//释放技能
	void ReleaseSkill( CMuxPlayerSkill* pSkill );

	void ReleaseRogueBuff( CScampBuff* pBuff );


private:
	 TCache<CPlayer>* m_pPlayerPool;  ///玩家的对象池
	 TCache< CGateSrv>*     m_pGateSrvPool;//gatesrv对象池；
	 TCache< CScampBuff>* m_pScampBuffPool;  //流氓状态池
	 TCache< CMuxPlayerSkill>* m_pSkillPool;		//技能池
};

extern CPoolManager* g_poolManager;  ///全局对象池管理器

#endif /*POOLMANAGER_H*/
