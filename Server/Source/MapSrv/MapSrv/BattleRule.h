﻿

#ifndef BUFFPUSHRULE_H
#define BUFFPUSHRULE_H

#include "IBuff.h"

class CMuxPlayerSkill;

class CBattleRule
{
public:
	CBattleRule(void);
	~CBattleRule(void);

	//战斗规则　战斗模式人对怪物
	static bool BattleModeCheckPvc( CPlayer* pAttackPlayer, CMonster* pMonster, bool bGoodSkill );
	//战斗规则　战斗模式人对人
	static bool BattleModeCheckPvp( CPlayer* pAttackPlayer, CPlayer* pTargetPlayer, bool bGoodSkill );
	//战斗规则　新手保护规则
	static bool RookieProtect( CPlayer* pAttackPlayer,  CPlayer* pTargetPlayer, bool bGoodSkill, bool bInScopeAttack = false);
	//人打怪物的所有判定都在这
	static bool PvcTargetCheck( CPlayer* pAttackPlayer,  CMonster* pMonster, bool bGoodSkill, bool isScopeAttack );

	static bool PvpTargetCheck( CPlayer* pAttackPlayer,  CPlayer* pTargetPlayer, bool isGoodSkill, bool isScopeAttack );

	static bool HandlePlayerSkill( CPlayer* pAttackPlayer, GMUseSkill* pUseSkill );

	static bool PlayerStateCheck( CPlayer* pAttackPlayer );

	static bool DistanceCheck( CPlayer* pAttackPlayer, TYPE_POS targetPosX, TYPE_POS targetPosY, unsigned int minRange, unsigned int maxRange, unsigned int targetSize );

	static bool NpcCheckNpc( CMonster* pAtkMonster, CMonster* pTargetMonster );

	static bool NpcCheckPlayer( CMonster* pAtkMonster, CPlayer* pTargetPlayer );

	static bool NewSkillCheck( CPlayer* pUseSkillPlayer, CMuxPlayerSkill* pSkill );
};


#endif //BUFFPUSHRULE_H
