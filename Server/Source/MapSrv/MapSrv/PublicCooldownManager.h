﻿

#ifndef PUBLIC_COOLDOWN_MANAGER_H
#define PUBLIC_COOLDOWN_MANAGER_H

#include <map>
#include <vector>
using namespace std;


class CPublicCooldownManager
{
public:
	static void InsertPublicCooldownInfo( int cooldownID, unsigned int skillID);

	static vector<unsigned int>* GetCoolDownSkills( int cooldownID );

private:
	CPublicCooldownManager(void);
	~CPublicCooldownManager(void);

private:
	static map< int,vector<unsigned int> > m_mapTypeSkill;
};



#endif
