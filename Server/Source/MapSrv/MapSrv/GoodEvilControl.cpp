﻿#include "GoodEvilControl.h"
#include "MuxMap/muxmapbase.h"
#include "Rank/rankeventmanager.h"

#define MAXEVILPOINT -9999
#define MAXGOODPOINT  9999

void CGoodEviControl::SubGoodEvilPoint(unsigned int evilNum )
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::SubGoodEvilPoint, NULL == m_pAttachPlayer\n" );
		return;
	}

	if( !m_pAttachPlayer->SubRacePrestige( evilNum * 10 ) )  //每减少一点善邪值会减少10点种族声望,减少值过大直接设置为0
	{
		m_pAttachPlayer->SubRacePrestige( m_pAttachPlayer->GetRacePrestige() );
	}

	FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CGoodEviControl::SubGoodEvilPoint, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_1_Base& playerBase = playerInfo->baseInfo;
	int oldPoint = playerBase.sGoodEvilPoint;
	//如果>0,则只能从大于0变为0
	if ( playerBase.sGoodEvilPoint == 0 )
	{
		evilNum = evilNum > MAXGOODPOINT ? MAXGOODPOINT:evilNum;
		playerBase.sGoodEvilPoint -= evilNum;
		StartEvilTimeMode();
		m_pAttachPlayer->UpdateGateGoodEvilPoint();
	}
	else if( playerBase.sGoodEvilPoint < 0 )
	{
		if ( playerBase.sGoodEvilPoint - (int)evilNum < MAXEVILPOINT )
			playerBase.sGoodEvilPoint = MAXEVILPOINT;
		else
			playerBase.sGoodEvilPoint -= evilNum;
	}
	else if( playerBase.sGoodEvilPoint > 0 )
	{
		if ( (unsigned int)playerBase.sGoodEvilPoint > evilNum )
		{
			playerBase.sGoodEvilPoint -= evilNum;
		}
		else
			playerBase.sGoodEvilPoint = 0;
	}
	NoticeGoodEvilPointChange( oldPoint );
}

void CGoodEviControl::AddGoodEvilPoint(unsigned int evilNum )
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::AddGoodEvilPoint, NULL == m_pAttachPlayer\n" );
		return;
	}

//#ifdef ANTI_ADDICTION
//	m_pAttachPlayer->TriedStateProfitCheck( evilNum, TYPE_GOODEVILPOINT );		//防沉迷的验证
//#endif /*ANTI_ADDICTION*/

	FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CGoodEviControl::AddGoodEvilPoint, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_1_Base& playerBase = playerInfo->baseInfo;
	int oldPoint = playerBase.sGoodEvilPoint;
	if ( playerBase.sGoodEvilPoint >= 0  )
	{
		if( playerBase.sGoodEvilPoint + evilNum > MAXGOODPOINT )
			playerBase.sGoodEvilPoint = MAXGOODPOINT;
		else
			playerBase.sGoodEvilPoint += evilNum;
	}
	else
	{
		//只能从<0变为 = 0
		if ( ( playerBase.sGoodEvilPoint * -1 ) <= (int)evilNum )
		{
			playerBase.sGoodEvilPoint = 0;
			m_pAttachPlayer->UpdateGateGoodEvilPoint();		//魔头变为非魔头 通知GATE
			//更新新手状态
			m_pAttachPlayer->UpdateRookieState();
		}
		else
		{
			playerBase.sGoodEvilPoint += evilNum;
		}
	}
	NoticeGoodEvilPointChange( oldPoint );

}

void CGoodEviControl::NoticeGoodEvilPointChange( int oldPoint )
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::NoticeGoodEvilPointChange, NULL == pControl\n" );
		return;
	}

	m_pAttachPlayer->OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(善恶值)

	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_GOOEVILPOINT;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = m_pAttachPlayer->GetGoodEvilPoint();
	updateAttr.lChangedPlayerID = m_pAttachPlayer->GetFullID();
	updateAttr.changeTime = 0;//hyn检查; 检查完毕
	updateAttr.nAddType = MGRoleattUpdate::TYPE_SET;//hyn检查;检查完毕
	updateAttr.bFloat = false;//hyn检查; 检查完毕

//#ifdef _DEBUG
//	D_DEBUG("%s GoodEvilPoint:%d\n", m_pAttachPlayer->GetNickName(),m_pAttachPlayer->GetGoodEvilPoint() );
//#endif

	CMuxMap* pMap = m_pAttachPlayer->GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGRoleattUpdate>(&updateAttr, m_pAttachPlayer->GetPosX(), m_pAttachPlayer->GetPosY() );
	}

	m_pAttachPlayer->SetNeedNotiTeamMember();
	m_pAttachPlayer->OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(善恶值)

//#ifdef RANKCHART_OPEN
	//监视玩家善恶值的改变	
	//RankEventManagerSingle::instance()->MonitorPlayerGoodEvilPointChange( m_pAttachPlayer );
	RankEventManager::MonitorPlayerGoodEvilPointChange( m_pAttachPlayer, oldPoint );
//#endif //RANKCHART_OPEN
}

void CGoodEviControl::StartRogueState()
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::StartRogueState, NULL == pControl\n" );
		return;
	}

	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_ROGUE;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = (unsigned int)true;
	updateAttr.lChangedPlayerID = m_pAttachPlayer->GetFullID();
	updateAttr.changeTime = 0;//hyn检查; 检查完毕
	updateAttr.nAddType = MGRoleattUpdate::TYPE_SET;//hyn检查;检查完毕
	updateAttr.bFloat = false;//hyn检查; 检查完毕

#ifdef _DEBUG
	D_DEBUG("%s　开始流氓状态\n", m_pAttachPlayer->GetNickName() );
#endif

	CMuxMap* pMap = m_pAttachPlayer->GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGRoleattUpdate>(&updateAttr, m_pAttachPlayer->GetPosX(), m_pAttachPlayer->GetPosY() );
	}

}

void CGoodEviControl::EndRogueState()
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::EndRogueState, NULL == pControl\n" );
		return;
	}
	
	MGRoleattUpdate updateAttr;
	updateAttr.nType = RT_ROGUE;
	updateAttr.nOldValue = 0;
	updateAttr.nNewValue = (unsigned int)false;
	updateAttr.lChangedPlayerID = m_pAttachPlayer->GetFullID();
	updateAttr.changeTime = 0;//hyn检查; 检查完毕
	updateAttr.nAddType = MGRoleattUpdate::TYPE_SET;//hyn检查;检查完毕
	updateAttr.bFloat = false;//hyn检查; 检查完毕

#ifdef _DEBUG
	D_DEBUG("玩家%s 流氓状态结束 \n", m_pAttachPlayer->GetNickName()  );
#endif

	CMuxMap* pMap = m_pAttachPlayer->GetCurMap();
	if( pMap )
	{
		pMap->NotifySurrounding<MGRoleattUpdate>(&updateAttr, m_pAttachPlayer->GetPosX(), m_pAttachPlayer->GetPosY() );
	}
}


void CGoodEviControl::StartEvilTimeMode()
{	
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::StartEvilTimeMode, NULL == pControl\n" );
		return;
	}

	//如果处于流氓状态的玩家进入恶魔状态,则取消先前的流氓状态
	if( m_pAttachPlayer->IsRogueState() )
	{
		m_pAttachPlayer->EndRogueState();
	}

	m_pAttachPlayer->UpdateRookieState();
	
	//设置恶魔的时间
	ResetEvilTime();

	//将该计时插入
	InsertEvilTime();
}

bool CGoodEviControl::EndEvilTimeMode()
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::EndEvilTimeMode, NULL == m_pAttachPlayer\n" );
		return false;
	}

	m_pAttachPlayer->OnSimpleInfoChange();//简单信息改变，下次取简单信息时需要重新填充；(善恶值)

	//恶魔计时变为0
	FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CGoodEviControl::EndEvilTimeMode, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	playerInfo->baseInfo.sGoodEvilPoint = 0;
	playerInfo->baseInfo.uiEvilTime = 0;
	m_pAttachPlayer->UpdateGateGoodEvilPoint();	

	m_pAttachPlayer->UpdateRookieState();
	return true;
}

bool CGoodEviControl::UpdateEvilTime()
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::UpdateEvilTime, NULL == pControl\n" );
		return false;
	}

	time_t curTime;
	time(&curTime);

	FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CGoodEviControl::UpdateEvilTime, NULL == GetFullPlayerInfo()\n");
		return false;
	}
	PlayerInfo_1_Base& playerBase = playerInfo->baseInfo;

	//先前的恶魔在线时间
	unsigned int oldhour = playerBase.uiEvilTime /EVIL_UPDATE_TIME;

	//流失的时间
	unsigned losttime = (unsigned int)difftime(curTime, mEvilUpdateTime);
	mEvilUpdateTime = curTime;

	//现在的恶魔时间
	playerBase.uiEvilTime += losttime;

	unsigned newhour = playerBase.uiEvilTime /EVIL_UPDATE_TIME;

	int oldPoint = playerBase.sGoodEvilPoint;
	if( newhour != oldhour )
	{
		//通知玩家善恶值改变了
		NoticeGoodEvilPointChange(oldPoint);

		//判断更新时间后，玩家的恶魔状态是否已经改变
		if ( m_pAttachPlayer->GetGoodEvilMode() != EVIL_MODE )
		{
			//结束恶魔状态
			EndEvilTimeMode();
			return false;
		}
	}
	return true;
}

void CGoodEviControl::ResetEvilTime()
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::ResetEvilTime, NULL == pControl\n" );
		return;
	}

	//开始恶魔状态的计时
	FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
	if ( NULL == playerInfo )
	{
		D_ERROR("CGoodEviControl::ResetEvilTime, NULL == GetFullPlayerInfo()\n");
		return;
	}
	PlayerInfo_1_Base& playerBase =playerInfo->baseInfo;
	//开始3小时的计时
	playerBase.uiEvilTime =  0;
	
	time(&mEvilUpdateTime);
}

void CGoodEviControl::InsertEvilTime()
{
	CEvilTimeControlManagerSingle::instance()->PushEvilTimeControl( this );
}

void CGoodEviControl::CheckLoseEvilTime()
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::CheckLoseEvilTime, NULL == m_pAttachPlayer\n" );
		return;
	}

	mEvilUpdateTime = 0 ;

	GOODEVIL_MODE mode = m_pAttachPlayer->GetGoodEvilMode();
	//如果玩家是恶魔模式
	if ( mode == EVIL_MODE )
	{
		time(&mEvilUpdateTime);

		FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CGoodEviControl::CheckLoseEvilTime, NULL == GetFullPlayerInfo()\n");
			return;
		}
		PlayerInfo_1_Base& playerBase = playerInfo->baseInfo;
		if ( playerBase.sGoodEvilPoint < 0 )
		{
			InsertEvilTime();
		}
	}
}

unsigned int CGoodEviControl::GetEvilTime()
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::GetEvilTime, NULL == m_pAttachPlayer\n" );
		return 0;
	}

	if ( m_pAttachPlayer->GetGoodEvilPoint() < 0 )
	{
		FullPlayerInfo * playerInfo = m_pAttachPlayer->GetFullPlayerInfo();
		if ( NULL == playerInfo )
		{
			D_ERROR("CGoodEviControl::GetEvilTime, NULL == GetFullPlayerInfo()\n");
			return 0;
		}
		int evilPoint    = playerInfo->baseInfo.sGoodEvilPoint * -1;
		unsigned int evilLastTime = evilPoint * EVIL_UPDATE_TIME;
		PlayerInfo_1_Base& playerBase = playerInfo->baseInfo;
		unsigned int subtime = 0;
		if ( evilLastTime > playerBase.uiEvilTime )
			subtime = evilLastTime -  playerBase.uiEvilTime;

		return subtime;
	}
	else
	{
		return 0;
	}
	
}

void CGoodEviControl::OnKillerOtherPlayer(CPlayer* pBeKiller )
{
	if ( NULL == pBeKiller )
	{
		D_ERROR( "CGoodEviControl::OnKillerOtherPlayer, NULL == pBeKiller\n" );
		return;
	}

	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::OnKillerOtherPlayer, NULL == m_pAttachPlayer\n" );
		return;
	}

	//自己杀自己不改变自己的善恶值
	if ( pBeKiller == m_pAttachPlayer )
	{
		return;
	}

//#ifdef RANKCHART_OPEN
	//RankEventManagerSingle::instance()->MonitorPlayerKill( m_pAttachPlayer );
	RankEventManager::MonitorPlayerKill( m_pAttachPlayer );
//#endif //RANKCHART_OPEN

	//不同种族的互相PK不影响玩家的善恶值
	if ( pBeKiller->GetRace() != m_pAttachPlayer->GetRace() )
		return;
	
	//被杀死玩家的善恶模式
	GOODEVIL_MODE beKilerGoodEvilMode = pBeKiller->GetGoodEvilMode();
	//获取本玩家的善恶模式
	GOODEVIL_MODE goodEvilMode = m_pAttachPlayer->GetGoodEvilMode();
	//如果被杀死的玩家不是恶魔
	if ( beKilerGoodEvilMode != EVIL_MODE )
	{
		//那么杀人的玩家必须是流氓才行
		if ( pBeKiller->IsRogueState() )
			return;
	}
	else 
	{
		//恶魔杀死恶魔,不算善恶值
		if ( goodEvilMode == beKilerGoodEvilMode )
			return;
	}

	switch( goodEvilMode )
	{
	case NORMAL_MODE:
		{
			if (   beKilerGoodEvilMode == NORMAL_MODE 
				|| beKilerGoodEvilMode == HERO_MODE )
			{
				SubGoodEvilPoint( 1 );
			}
			else if ( beKilerGoodEvilMode == EVIL_MODE )
			{
				//对抗规则修正的新内容
				if( m_pAttachPlayer->CanGetKillProfit( pBeKiller->GetLevel() ) )
				{
					AddGoodEvilPoint( 1 );
				}
			}
		}
		break;
	case EVIL_MODE:
		{
			if (   beKilerGoodEvilMode == NORMAL_MODE 
				|| beKilerGoodEvilMode == HERO_MODE )
			{
				SubGoodEvilPoint( 1 );
			}
		}
		break;
	case HERO_MODE:
		{
			//杀死普通或英雄的,善恶值-5
			if ( beKilerGoodEvilMode == NORMAL_MODE ||
				beKilerGoodEvilMode == HERO_MODE )
			{
				SubGoodEvilPoint( 5 );
			}
			else if ( beKilerGoodEvilMode ==  EVIL_MODE )
			{
			    //对抗规则修正的新内容
				if( m_pAttachPlayer->CanGetKillProfit( pBeKiller->GetLevel() ) )
				{
					AddGoodEvilPoint( 1 );
				}
			}
		}
		break;
	default:
		break;
	}
	
	pBeKiller->BeKilled( m_pAttachPlayer );
}


void CGoodEviControl::OnKillNpc( CMonster* pDeadNpc )
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::OnKillNpc, NULL == m_pAttachPlayer\n" );
		return;
	}

	if( NULL == pDeadNpc )
	{
		D_ERROR( "CGoodEviControl::OnKillNpc, NULL == pDeadNpc\n" );
		return;
	}

	if( pDeadNpc->GetRaceAttribute() == m_pAttachPlayer->GetRace()  && pDeadNpc->GetNpcType() ==  15 )
	{
		SubGoodEvilPoint( 1 );
	}
}

unsigned int CGoodEviControl::GetGoodEvilControlID()
{
	if ( NULL == m_pAttachPlayer )
	{
		D_ERROR( "CGoodEviControl::GetGoodEvilControlID, NULL == m_pAttachPlayer\n" );
		return 0;
	}

	return m_pAttachPlayer->GetDBUID();
}


void CEvilTimeControlManager::RemoveGoodEvilControl(unsigned int playerUID )
{
	std::vector<CGoodEviControl *>::iterator lter = mEvilTimeVec.begin();
	for ( ;lter != mEvilTimeVec.end(); ++lter )
	{
		CGoodEviControl* pControl = *lter;
		if ( pControl  && pControl->GetGoodEvilControlID() == playerUID )
		{
			mEvilTimeVec.erase( lter );
			return;
		}
	}
}


void CEvilTimeControlManager::PushEvilTimeControl(CGoodEviControl* pControl )
{
	if ( NULL == pControl )
	{
		D_ERROR( "CGoodEviControl::PushEvilTimeControl, NULL == pControl\n" );
		return;
	}

	mEvilTimeVec.push_back(  pControl ) ;
	return;
}

void CEvilTimeControlManager::UpdateEvilTime()
{
	time_t curTime;
	time(&curTime);

	//每15秒更新一次
	unsigned int diftime = (unsigned int)difftime( curTime, updateTime );
	if ( diftime > 5 )
	{
		if ( mEvilTimeVec.empty()  )
			return;

		std::vector<CGoodEviControl *>::iterator lter = mEvilTimeVec.begin();
		for ( ;lter != mEvilTimeVec.end(); )
		{
			CGoodEviControl* pControl = *lter;
			if ( NULL != pControl )
			{
				//更新善恶值失败:可能完成更新， 可能因为其他原因失败
				if( !pControl->UpdateEvilTime() )
				{
					lter = mEvilTimeVec.erase( lter );
				}
				else
					++lter;
			}
		}
		time(&updateTime);
	}
}

CEvilTimeControlManager::CEvilTimeControlManager()
{
	time(&updateTime);
}
