﻿#include "RaceMaster.h"
#include "Player/Player.h"
#include "../../Base/Utility.h"
#include "MuxMap/muxmapbase.h"


bool CRaceMaster::SetRaceMasterPlayer(CPlayer* pPlayer )
{
	if ( !pPlayer )
	{
		D_ERROR("设置种族族长指针为NULL\n" );
		return false;
	}

	if ( pPlayer->GetDBUID() != m_masterUID )
	{
		D_ERROR("进入设置族长函数,但玩家唯一编号 %d 和 种族组长唯一编号 %d 不同,不能设置族长\n ", pPlayer->GetDBUID(), m_masterUID );
		return false;
	}

	m_pRaceMaster = pPlayer;
	if ( !m_pRaceMaster )
		return false;

	D_DEBUG("玩家 %s 成为 %d 族族长\n", m_pRaceMaster->GetNickName(), m_pRaceMaster->GetRace() );

	MGSimplePlayerInfo *pPlayerInfo = m_pRaceMaster->GetSimplePlayerInfo();
	if(NULL != pPlayerInfo)
	{
		CMuxMap* pPlayerMap = m_pRaceMaster->GetCurMap();
		if ( NULL != pPlayerMap )
		{
			pPlayerMap->NotifySurrounding<MGSimplePlayerInfo>( pPlayerInfo, m_pRaceMaster->GetPosX(), m_pRaceMaster->GetPosY() );
		}
	}

	//通知新的族长的周围的人
	NoticeRaceMasterStateChange( true );

	time_t curtime = time(NULL);
	struct tm cur_tm  = *ACE_OS::localtime( &curtime );
	time_t lastupdatetime = (time_t)m_lastupdatetime;
	struct tm last_tm = *ACE_OS::localtime( &lastupdatetime );

	if (  ( cur_tm.tm_year == last_tm.tm_year ) 
		&&( cur_tm.tm_mon  == last_tm.tm_mon  )
		&&( cur_tm.tm_mday == last_tm.tm_mday ) )//如果时间过期了
	{
		return false;
	}
	else
		return true;

	return false;
}

void CRaceMaster::NoticeRaceMasterStateChange(bool isMaster )
{
	CPlayer* pPlayer = m_pRaceMaster;
	if ( pPlayer )
	{
		MGRoleattUpdate updateAttr;
		updateAttr.nType = RT_RACEMASTER;
		updateAttr.nOldValue = 0;
		updateAttr.nNewValue = (isMaster == true )?1:0;
		updateAttr.lChangedPlayerID = pPlayer->GetFullID();
		updateAttr.changeTime = 0;//hyn检查; 检查完毕
		updateAttr.nAddType = MGRoleattUpdate::TYPE_SET;//hyn检查;检查完毕
		updateAttr.bFloat = false;//hyn检查; 检查完毕

		CMuxMap* pMap = pPlayer->GetCurMap();
		if( pMap )
		{
			pMap->NotifySurrounding<MGRoleattUpdate>(&updateAttr, pPlayer->GetPosX(), pPlayer->GetPosY() );
		}

	}
}

const char* CRaceMaster::GetRaceMasterName()
{
	if ( m_pRaceMaster )
		return m_pRaceMaster->GetNickName();

	return NULL;
}

int CRaceMaster::MasterGetSalary()
{
	if ( !m_pRaceMaster )
		return 0;

	int oldsalary = m_daysalary;
	m_pRaceMaster->AddSilverMoney( m_daysalary );//双币种判断
	m_daysalary = 0;

	return oldsalary;
}