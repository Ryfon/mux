﻿#include "CastManager.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "Player/Player.h"
#include "MuxMap/muxmapbase.h"

using namespace MUX_PROTO;

void CastManager::OnStart(int skillID, int time )
{
	if ( NULL == m_pPlayer )
	{
		return;
	}
	//如果当前处于读条状态，则先中断读条
	if (IsInCast())
	{
		OnStop();
	}
	MGStartCast msg;
	msg.req.castTime = time;
	msg.req.skillID = skillID;
	msg.req.playerID = m_pPlayer->GetFullID();

	m_skillID = skillID;
	CMuxPlayerSkill* pSkill = NULL;
	pSkill = m_pPlayer->FindMuxSkill( skillID );
	if( NULL == pSkill )
	{
		return;
	}

	CMuxSkillProp* pSkillProp = pSkill->GetSkillProp();
	if (NULL == pSkillProp)
	{
		return;
	}
	m_time = pSkillProp->m_castTime;
	m_startTime = GetTickCount();

	CMuxMap * pMap = m_pPlayer->GetCurMap();
	if ( NULL != pMap )
	{
		pMap->NotifySurrounding<MGStartCast>( &msg, m_pPlayer->GetPosX(), m_pPlayer->GetPosY() );
	}

}

void CastManager::OnStop()
{
	if ( NULL == m_pPlayer )
	{
		return;
	}
	if (!IsInCast())
	{
		D_WARNING("玩家%s不处于读条状态，但请求中断读条\n", m_pPlayer->GetNickName() );
		return;
	}

	m_skillID = 0;
	m_time = 0;
	m_startTime = 0;

	MGStopCast msg;
	CMuxMap * pMap = m_pPlayer->GetCurMap();
	if ( NULL != pMap )
	{
		pMap->NotifySurrounding<MGStopCast>( &msg, m_pPlayer->GetPosX(), m_pPlayer->GetPosY() );
	}
}
