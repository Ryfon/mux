﻿#ifndef CLI_PROTOCOL_H
#define CLI_PROTOCOL_H

#define  PKGPOINTCHECK( TYPE, P ){\
	P = (TYPE *)(pPkg);\
	}

#define  PKGSIZECHECK( TYPE ){\
	if ( wPkgLen != sizeof(TYPE) ){\
	D_DEBUG("SrvErr:消息包大小错误:%s\n", #TYPE );\
	return false;}\
	}

#define  PKG_CONVERT(TYPE)    const TYPE* pMsg = NULL;\
	PKGSIZECHECK(TYPE);\
	PKGPOINTCHECK(TYPE,pMsg);

namespace MUX_PROTO
{
	const int MAX_MSG_SIZE = 512;
	template<bool> struct OrgMsgDefineChecker;
	template<> struct OrgMsgDefineChecker<true> {};

	//用于确保不会定义大于预定大小的消息结构,消息结构最大为507；
#define MSG_SIZE_GUARD(msgtype) \
	namespace MSG_CHECK{ struct check_##msgtype{\
	OrgMsgDefineChecker<(sizeof(msgtype)<507)> struct_lager_than_507_##msgtype;\
	};}//namespace MSG_CHECK;
};

#include "CliProtocol_T.h"

using namespace MUX_PROTO;

inline bool operator < ( const PlayerID& first,const PlayerID& sec )
{
	return first.dwPID < sec.dwPID ? true : false;
}

inline bool operator == ( const PlayerID& first,const PlayerID &sec )
{
	return ( ( first.wGID == sec.wGID ) && ( first.dwPID == sec.dwPID ) );
}

inline bool operator != ( const PlayerID& first,const PlayerID &sec )
{
	return ( ( first.wGID != sec.wGID ) || ( first.dwPID != sec.dwPID ) );
}

inline void operator+=(EquipAdditonInfo& first, const EquipAdditonInfo &sec)
{
	first.AddHP += sec.AddHP;
  first.AddMP += sec.AddMP;
	first.AddStr += sec.AddStr;
	first.AddInt += sec.AddInt;
	first.AddAgi += sec.AddAgi;
	first.AddPhysicsHit += sec.AddPhysicsHit;
	first.AddMagicHit += sec.AddMagicHit;
	first.AddPhysicsHitOdds += sec.AddPhysicsHitOdds;
	first.AddMagicHitOdds += sec.AddMagicHitOdds;
	first.AddPhysicsNum += sec.AddPhysicsNum;
	first.AddMagicNum += sec.AddMagicNum;
	first.AddPhysicsAttack += sec.AddPhysicsAttack;
	first.AddMagicAttack += sec.AddMagicAttack;
	first.AddDecPhyNumHarm += sec.AddDecPhyNumHarm;
	first.AddDecMagNumHarm += sec.AddDecMagNumHarm;
	first.AddPhysicsNumAffix += sec.AddPhysicsNumAffix;
	first.AddMagicNumAffix += sec.AddMagicNumAffix;
	first.AddPhysicsDef += sec.AddPhysicsDef;
	first.AddMagicDef += sec.AddMagicDef;

	return;
}

//注意由于戒指,耳环,手镯有2个栏位,自动判断下一个栏位+1是否能装备, INDEX_TWO_HAND需要7和8都空才能装备
const int EQUIPTYPE_POS[] = 
{
	0,	//IDNEX_HEAD的栏位是0
	1,  //INDEX_BODY的栏位是1
	2,  //INDEX_HAND的栏位是2
	3,	//INDEX_FOOT的栏位是3
	4,	//INDEX_SHOULDER的栏位是4
	5,  //INDEX_BACK的栏位是5
	14,	//INDEX_EARRING的栏位是14
	9,  //INDEX_NECKLACE的栏位是9
	12,  //INDEX_BRACELET的栏位是12
	10,  //INDEX_RING的栏位是10
	6,  //INDEX_MAIN_HAND的栏位是6
	7,  //INDEX_ASSIS_HAND的栏位是7
	6,  //INDEX_TWO_HAND栏位是6和7,这个需要特别判断
	8  //INDEX_RIDE栏位是8
};

//};

#endif/*CLI_PROTOCOL_H*/