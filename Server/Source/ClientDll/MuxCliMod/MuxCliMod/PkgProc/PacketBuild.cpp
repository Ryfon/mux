﻿/**
* @file PacketBuild.cpp
* @brief 组包模块cpp文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: PacketBuild.cpp
* 摘    要: 根据通信协议（类型，命令字，协议结构）进行组包
* 作    者: dzj
* 完成日期: 2007.11.30
*
*/

#include "stdafx.h"
#include "packetbuild.h"

char CPacketBuild::m_InnerBuf[PKGBUILD_BUF_SIZE];
CliProtocol CPacketBuild::m_clipro;
