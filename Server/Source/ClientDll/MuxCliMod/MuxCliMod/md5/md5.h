﻿/********************************************************************
created:	2008/07/31
created:	31:7:2008   17:31
filename: 	d:\Projects\mux\Developer\PlatformTools\ServiceSpace\LoginService\md5.h
file path:	d:\Projects\mux\Developer\PlatformTools\ServiceSpace\LoginService
file base:	md5
file ext:	h
author:		

purpose:	
*********************************************************************/

#ifndef __SERVICE_SPACE_LOGIN_SERVICE_MD5_H__
#define __SERVICE_SPACE_LOGIN_SERVICE_MD5_H__

#define __USE_WIN32_MD5

#include <stdexcept>
#ifndef __USE_WIN32_MD5
#include <openssl/md5.h>
typedef MD5_CTX sslMD5_CTX;
#endif

struct MD5_CTX_t
{
#if defined (WIN32) && defined (__USE_WIN32_MD5)
	unsigned long i[2];
	unsigned long buf[4];
	unsigned char in[64];
#else
	sslMD5_CTX ssl_md5_ctx_;
#endif
	unsigned char digest[16];
};

#ifdef WIN32

typedef void (__stdcall * __MD5Init)(MD5_CTX_t * context);
typedef void (__stdcall * __MD5Update)(MD5_CTX_t * context, unsigned char * input, unsigned int inlen);
typedef void (__stdcall * __MD5Final)(MD5_CTX_t * context);

#else

typedef void (__attribute__((STDCALL))* __MD5Init)(MD5_CTX_t * context);
typedef void (__attribute__((STDCALL))* __MD5Update)(MD5_CTX_t * context, unsigned char * input, unsigned int inlen);
typedef void (__attribute__((STDCALL))* __MD5Final)(MD5_CTX_t * context);

#endif

#ifdef __USE_WIN32_MD5

#else

#	ifdef WIN32

void __stdcall md5init(MD5_CTX_t * context);
void __stdcall md5update(MD5_CTX_t * context, unsigned char * input, unsigned int inlen);
void __stdcall md5final(MD5_CTX_t * context);

#	else

void __attribute__((STDCALL)) md5init(MD5_CTX_t * context);
void __attribute__((STDCALL)) md5update(MD5_CTX_t * context, unsigned char * input, unsigned int inlen);
void __attribute__((STDCALL)) md5final(MD5_CTX_t * context);

#	endif

#endif

enum {MS_MD5 = 1};

template<int _T>
class __md5;

template<int _T>
class __md5
{
public:
	static __MD5Init MD5Init;
	static __MD5Update MD5Update;
	static __MD5Final MD5Final;

	static void init()
	{
#ifdef __USE_WIN32_MD5

		HMODULE hModule = ::LoadLibrary("Cryptdll.dll");
		if (!hModule)
			throw std::bad_exception("failed to load Cryptdll.dll");
		MD5Init = (__MD5Init)GetProcAddress(hModule, ("MD5Init"));
		MD5Update = (__MD5Update)GetProcAddress(hModule, ("MD5Update"));
		MD5Final = (__MD5Final)GetProcAddress(hModule, ("MD5Final"));

#else

		MD5Init = md5init;
		MD5Update = md5update;
		MD5Final = md5final;

#endif
	}

private:
	__md5()
	{
		__md5::init();
	}

	static __md5 __md5_inst;
};

template<int _T> __MD5Init __md5<_T>::MD5Init = 0;
template<int _T> __MD5Update __md5<_T>::MD5Update = 0;
template<int _T> __MD5Final __md5<_T>::MD5Final = 0;

template<int _T>
__md5<_T> __md5<_T>::__md5_inst;

typedef __md5<MS_MD5> md5;

void build_md5(const char * buf, int len, char * dst);
void build_md5(const std::string & id, const std::string & passwrd, const char * randomString, int randomStringlen, char * dst);

#endif