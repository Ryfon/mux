﻿#include "stdafx.h"
#include "md5.h"

#ifndef __USE_WIN32_MD5

#ifdef WIN32
void __stdcall md5init(MD5_CTX_t * context)
#else
void __attribute__((STDCALL)) md5init(MD5_CTX_t * context)
#endif
{
	MD5_Init(&context->ssl_md5_ctx_);
}


#ifdef WIN32
void __stdcall md5update(MD5_CTX_t * context, unsigned char * input, unsigned int inlen)
#else
void __attribute__((STDCALL)) md5update(MD5_CTX_t * context, unsigned char * input, unsigned int inlen)
#endif
{
	MD5_Update(&context->ssl_md5_ctx_, input, inlen);
}


#ifdef WIN32
void __stdcall md5final(MD5_CTX_t * context)
#else
void __attribute__((STDCALL)) md5final(MD5_CTX_t * context)
#endif
{
	MD5_Final(context->digest, &context->ssl_md5_ctx_);
}

#endif

void build_md5(const char * buf, int len, char * dst)
{
	MD5_CTX_t ctx;
	md5::MD5Init(&ctx);
	md5::MD5Update(&ctx, (unsigned char *)buf, (unsigned int)len);
	md5::MD5Final(&ctx);
	memcpy(dst, ctx.digest, sizeof(ctx.digest));
}

void build_md5(const std::string & id, const std::string & passwrd, const char * randomString, int randomStringlen, char * dst)
{
	char buf[256];
	sprintf(buf, "%s%s", id.c_str(), passwrd.c_str());
	memcpy(buf + strlen(buf), randomString, randomStringlen);
	build_md5(buf, (int)(id.size() + passwrd.size() + randomStringlen), dst);
}
