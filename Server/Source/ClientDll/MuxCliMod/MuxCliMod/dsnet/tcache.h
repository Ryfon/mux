﻿#ifndef T_CACHE
#define T_CACHE

#include "../utility/dslog.h"
#include "../utility/Utility.h"
#include <vector>

using namespace std;

//////////////////////////////////////////////////////////////////////////
//使用以下内存模板管理小对象, from Andrew Kirmse, added by dzj, 06.03.17;
//////////////////////////////////////////////////////////////////////////

/** Copyright (C) Andrew Kirmse, 2001. 
* All rights reserved worldwide.
*
* This software is provided "as is" without express or implied
* warranties. You may freely copy and compile this source into
* applications you distribute provided that the copyright text
* below is included in the resulting source code, for example:
* "Portions Copyright (C) Andrew Kirmse, 2001"
*/
#ifndef _CACHE_H
#define _CACHE_H

/***********************************************************************
**                                                                   **
** TCache                                                            **
**                                                                   **
** Objects can be released to the cache in arbitrary order.          **
**                                                                   **
***********************************************************************/

/**
///！！！注意：任何类，一旦其某些实例经过了TCache管理，则所有该类对象都必须从TCache管理的池中分配，
///不可自行new，不可定义局部变量，不可定义静态变量，不可memset;
///因为使用了下述的池管理策略，且所有的对象最后都要通过对象池回收，不遵守上述约定会造成内存错误；
*/
#define PoolFlagDefine	\
    void SetPoolFlag() { bIsPoolObj = true; }; \
    void ClearPoolFlag() { bIsPoolObj = false; }; \
    bool IsPoolObj() { return bIsPoolObj; }; \
    bool bIsPoolObj; \
    void PoolObjInit

template <class T>
class TCache
{
public:

	TCache(unsigned int MaxCacheSize)
		: mMaxCacheSize(MaxCacheSize)
	{		
		mCache.reserve(MaxCacheSize);

		//构造时预先生成好池中的内存结构, by dzj, 07.12.03；
		m_pArray = NEW T[MaxCacheSize];
		for ( unsigned int i=0; i<mMaxCacheSize; ++i )
		{
			m_pArray[i].PoolObjInit();
			m_pArray[i].SetPoolFlag();//初始池中对象；
			mCache.push_back( &m_pArray[i] );
		}
	}

	~TCache()
	{
 		Clear();
	}

	void SetCapcity( unsigned int MaxCacheSize ) //added by dzj, 06.03.16;
	{
		CDsLock<CDsMutex> tmpLock( mLock );
		mCache.reserve( MaxCacheSize );
	}

	// Returns NULL if no objects available in cache
	T* Retrieve()
	{
		CDsLock<CDsMutex> tmpLock( mLock );
		T* pItem = NULL;

		if (false == mCache.empty())
		{
			pItem = mCache.back();
			mCache.pop_back();
		}

		if ( NULL != pItem )
		{
			pItem->PoolObjInit();
		}

		return pItem;
	}

	// Creates a new object on the help if no objects
	// available in cache
	T* RetrieveOrCreate()
	{
		CDsLock<CDsMutex> tmpLock( mLock );
		T* pItem = NULL;

		if (false == mCache.empty())
		{
			pItem = mCache.back();
			mCache.pop_back();
		} else {
			pItem = NEW T;
			pItem->ClearPoolFlag();//不是初始池中对象；
		}

		if ( NULL != pItem )
		{
			pItem->PoolObjInit();
		}

		return pItem;
	}

	void Release(T* pItem)
	{
		CDsLock<CDsMutex> tmpLock( mLock );
		if ( pItem->IsPoolObj() )
		{
			pItem->PoolObjInit();
			mCache.push_back( pItem );
		} else {
			delete pItem; pItem = NULL;
		}
		//// If cache isn't full, return object to cache
		//if (mMaxCacheSize > mCache.size() &&
		//	NULL != pItem)
		//{
		//	mCache.push_back(pItem);
		//}
		//else
		//	delete pItem;
	}

	void Clear()
	{
		//while (false == mCache.empty())
		//{
		//	// Pop him off
		//	T* pItem = mCache.back();
		//	mCache.pop_back();

		//	// Release
		//	delete pItem;
		//}
		if(m_pArray != NULL){
			delete[] m_pArray;m_pArray=NULL;
		}
		mCache.clear();
	}

	unsigned int GetMaxCacheSize() const
	{
		return mMaxCacheSize;
	}

	unsigned int GetCurrentCacheSize() const
	{
		return mCache.size();
	}

private:

	// Private data
	unsigned int     mMaxCacheSize;
	std::vector<T*>  mCache;
	T* m_pArray;

	CDsMutex mLock;
};
#endif // #define _CACHE_H
//////////////////////////////////////////////////////////////////////////
//使用以上内存模板管理小对象；
//////////////////////////////////////////////////////////////////////////

#endif //T_CACHE
