﻿/**
* @file dsmuxsocket.h
* @brief 通信类封装头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称:dsmuxsocket.h
* 摘    要:本文件为socket通信封装类头文件；
* 作    者:dzj
* 创建日期:2007.11.09；
*/

#pragma once

#include "winsock2.h"
#include "..\utility\utility.h"
#include "msgtoput.h"

///DsSocket的当前状态；
enum DS_SOC_STAT //DsSocket的当前状态；
{
	DSS_INVALID=0, //无效；
	DSS_CREATED,   //已创建
	DSS_BOUND,     //已绑定；
	DSS_CONNECTING,//正在连接；
	DSS_CONNECTED,  //已连接(可以收发数据);
	DSS_DISCONNECTING //正在断开(还不可重用)；
};

///SOCKET消息回调者基类；
class IDsSocketSinkBase
{
public:
	IDsSocketSinkBase() {};
	virtual ~IDsSocketSinkBase(void);

public:
	///连接建立；
	virtual void OnConnected( BOOL isSuc ) = 0; //连接建立；
	///连接断开；
	virtual void OnDisconnected() = 0;//连接断开；

	///发送回调
	virtual void OnSent( BOOL isSuc ) = 0;
	///接收回调
	virtual void OnRecved( const char* pBuf, INT nLen ) = 0;

	//SOCKET出错，主线程执行网络处理时回调；
	virtual void OnDsSocketError( DWORD errNO ) = 0;
};

extern BOOL g_bIsStopSend;//是否停止往外发数据；

#define ERR_DISCONNECTED 2001
#define ERR_RCV_ERROR   2002
#define ERR_SEND_ERROR  2003

///SOCKET封装基类；
class CMuxSocket
{
public:
	///使用回调者构造；
	CMuxSocket( IDsSocketSinkBase* pSinker )
	{
		SetSocketStatus( DSS_INVALID );
        InitSocketEnv();
		CreateInnerSocket();
		SetSinker( pSinker );
	}

	///析构，释放关闭内部SOCKET;
	virtual ~CMuxSocket(void)
	{
		SetSinker( NULL );
        CloseInnerSocket();
		//Sleep(2000);
		//if ( DSS_CONNECTED == m_DssStatus )
		//{
		//	WaitForMultipleObjects( 2, m_harrNetThread, TRUE, INFINITE );//等待网络线程结束；
		//}
		SetSocketStatus( DSS_INVALID );
        EndSocketEnv();
	};

	///初始化socket环境；
	void InitSocketEnv()
	{
		WORD wVersionRequested;
		WSADATA wsaData;
		int err;

		wVersionRequested = MAKEWORD( 2, 2 );

		err = WSAStartup( wVersionRequested, &wsaData );
		if ( err != 0 ) {
			/* Tell the user that we could not find a usable */
			/* WinSock DLL.                                  */
			return;
		}

		if ( LOBYTE( wsaData.wVersion ) != 2 ||
			HIBYTE( wsaData.wVersion ) != 2 ) 
		{
			/* Tell the user that we could not find a usable */
			/* WinSock DLL.                                  */
			WSACleanup( );
			return; 
		}

		m_RcvBufQueue.Init();
	    m_SendBufQueue.Init();
	}

	///停socket环境；
	void EndSocketEnv()
	{
	    m_SendBufQueue.Release();
		m_RcvBufQueue.Release();

		WSACleanup();
	}

	void PreDesturct()
	{
		//析构之前调用，这部分工作如果在析构时才调用，则会使得关socket导致的消息无法找到相应的this加以处理；
		//因为同样的原因，调用本函数后，应该稍等一段时间再再析构。
		SetSinker( NULL );
		ShutdownInnerSocket();
	}

public:
	///连接远程机器发起;
	BOOL IssueConnectRemoteAddr( const char* strRemoteAddr, INT nRemotePort, BOOL isNeedClose/*连接失败时需要断开*/ );//连接远程机器;
	///连接建立；
	virtual void OnConnected( BOOL isSuc );
	///连接断开；
	virtual void OnDisConnected() 
	{
		CDsLock<CDsMutex> tmpLock( m_ErrNoLock );
		m_errNO = ERR_DISCONNECTED;
	} //连接断开；
	///接收错误；
	virtual void OnRcvError( INT nErrCode ) 
	{
		CDsLock<CDsMutex> tmpLock( m_ErrNoLock );
		m_errNO = nErrCode;
		//if ( NULL != m_pSinker )
		//{
		//	m_pSinker->OnDisconnected();
		//}		
		//CloseInnerSocket();
	};///接收错误；
	///发送错误；
	virtual void OnSendError( INT nErrCode ) 
	{
		CDsLock<CDsMutex> tmpLock( m_ErrNoLock );
		m_errNO = nErrCode;
		//if ( NULL != m_pSinker )
		//{
		//	m_pSinker->OnDisconnected();
		//}		
		//CloseInnerSocket();
	};///发送错误；



	//void ResetSendEvent()
	//{
	//	ResetEvent( m_SendBufQueue.GetSendEventRef() );
	//}

	//void WaitSendEvent()
	//{
	//	WaitForSingleObject( m_SendBufQueue.GetSendEventRef(), INFINITE );
	//}

	//用于写线程取待发数据，如果没有待发数据，则挂起；
    int GetDataToSend( MsgToPut*& pMsg )
	{
		//写线程调用；
		if ( m_errNO != 0 )
		{
			//已出错，不再外发数据；
			pMsg = NULL;
			return -1;
		}

		pMsg = m_SendBufQueue.GetOnePkgToSend();
		if ( NULL != pMsg )
		{
			return pMsg->GetValidSize();
		} else {
			return 0;
		}
	}

	///通知主线程，socket已出错，在主线程调用send函数或rcv函数时，通过检测错误代码回调;
	void NotifyMainThreadError( DWORD errNO )
	{
		if ( NULL != m_pSinker )
		{
			m_pSinker->OnDsSocketError( errNO );
		}

		//SetSinker( NULL );
		//ShutdownInnerSocket();
		//Sleep( 2000 );
  //      CloseInnerSocket();
		//Sleep(2000);
		//SetSocketStatus( DSS_INVALID );

		LogErr( LOGERR_LEV_ERR, "执行网络事件时检测到错误%d，停止操作", errNO );
		return;
	}

	///发送数据请求；
	BOOL SendReq( MsgToPut* pMsg, BOOL& isNeedClose ) 
	{
		isNeedClose = FALSE;
		if ( DSS_CONNECTED != m_DssStatus )
		{
			return FALSE;
		}

		if ( m_errNO != 0 )
		{
			//之前已出错，告知主线程；
			NotifyMainThreadError( m_errNO );
			isNeedClose = TRUE;
			return FALSE;
		}

		return m_SendBufQueue.AddPkgContent( pMsg );
	};

	///由应用程序主线程调用，取一个收包，取完后必须处理，否则在下次取包时，前次收包会失效；
	BOOL GetRcvedPkg( char*& pPkg, unsigned short& wSize, BOOL& isNeedClose )
	{
		isNeedClose = FALSE;
		if ( m_errNO != 0 )
		{
			//之前已出错，告知主线程；
			NotifyMainThreadError( m_errNO );
			isNeedClose = TRUE;
			return FALSE;
		}

		if ( DSS_CONNECTED != m_DssStatus )
		{
			return FALSE;
		}

		//尝试取一个已收包
		bool isParsePkgErr = false;
		pPkg = m_RcvBufQueue.GetOneRcvedPkg( wSize, isParsePkgErr );
		if ( isParsePkgErr )
		{
			NotifyMainThreadError( 9999 );//解包错误;
			return FALSE;
		}
		if ( NULL == pPkg )
		{
			wSize = 0;
			return FALSE;
		} else {
			return TRUE;
		}
	}

	///发包；
	virtual void OnSent( BOOL isSuc ) {};

	///读线程调用，收包；
	virtual int OnRecved( MsgToPut* pMsg ) 
	{
		//读线程调用；
		if ( m_errNO != 0 )
		{
			//已出错，不再外发数据；
			g_poolMsgToPut->Release( pMsg );			
			return -1;
		}
		m_RcvBufQueue.AddPkgContent( pMsg );
		return 1;//正常返回；
	};

	///取句柄；
	BOOL GetSocketHandle( SOCKET& outSocket )	
	{ 
		if ( DSS_INVALID != m_DssStatus )
		{
			outSocket = m_hSocket;
			return TRUE;
		} else {
			return FALSE;
		}
	}

	///是否已连接;
	BOOL IsDsSocketConnected()//是否已连接;
	{
		return ( DSS_CONNECTED == m_DssStatus );
	}

	BOOL IsDsSocketConnecting()
	{
		return ( DSS_CONNECTING == m_DssStatus );
	}	

	///设置SOCKET状态，外部除网络线程外不应调用；
	void SetSocketStatus( DS_SOC_STAT dssStatus )
	{
		m_DssStatus = dssStatus;
	}

	//取远端SOCKADDR_IN地址；
	SOCKADDR_IN GetRemoteSockAddrIn()
	{
		return m_RemoteSockAddr;
	}
	//取远端地址；
	const char* GetRemoteAddr()
	{
		return m_strRemoteIpAddr;
	}
	//取远端端口；
	WORD GetRemotePort()
	{
		return m_dwRemotePort;
	}

public:
	///实际连接；
	BOOL MuxCliConnect( BOOL& isNeedClose/*连接失败时置TRUE*/ );

private:
	///防止无参数构造；
	CMuxSocket(void);

	///置回调者；
	void SetSinker( IDsSocketSinkBase* pSinker )
	{
		m_pSinker = pSinker;
	}

	///绑定至本地端口
	BOOL BindToLocalAddr( INT nPort );//绑定至本地端口；

	///建内部SOCKET；
	BOOL CreateInnerSocket();

	///设为阻塞模式，以执行阻塞收发；
	BOOL SetBlock();

	//shutdown内部SOCKET;
	BOOL ShutdownInnerSocket()
	{
		if ( DSS_INVALID != m_DssStatus )
		{
			shutdown( m_hSocket, SD_SEND );
		}
		return TRUE;
	}

	//close内部SOCKET;
	BOOL CloseInnerSocket()
	{
		g_bIsStopSend = TRUE;
		closesocket( m_hSocket );
		if ( IsDsSocketConnected() )
		{
			WaitForMultipleObjects( 2, m_harrNetThread, TRUE, INFINITE );//等待网络线程结束；
		}
//		SetEvent( m_SendBufQueue.GetSendEventRef() );//激活可能还在等待的写线程；
		return TRUE;
	}

private:
	///回调者；
	IDsSocketSinkBase* m_pSinker;

	///系统SOCKET句柄；
	SOCKET  m_hSocket;
	///当前状态；
	DS_SOC_STAT m_DssStatus;//当前状态；

	///本地与远程SOCKADDR_IN;
	SOCKADDR_IN m_LocalSockAddr, m_RemoteSockAddr;
	///远端IP地址；
	char    m_strRemoteIpAddr[16];//远端IP地址；
	WORD    m_dwRemotePort;
	///本地IP地址；
	char    m_strSelfIpAddr[16];//本地IP地址；

	///0:网络读线程HANDLE，1:网络写线程HANDLE;
	HANDLE  m_harrNetThread[2];

private:
	CRcvBufferQueue m_RcvBufQueue;
	CSendBufferQueue m_SendBufQueue;

private:
	DWORD  m_errNO;//错误号;
	CDsMutex m_ErrNoLock;//错误号锁;
};





