﻿/**
* @file dsmuxsocket.cpp
* @brief 通信类封装实现文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称:dsmuxsocket.cpp
* 摘    要:本文件为socket通信封装类实现文件；
* 作    者:dzj
* 创建日期:2007.11.09；
*/
#include "StdAfx.h"
#include "dsmuxsocket.h"

#include "../utility/consoledbg.h"

#ifdef USE_CRYPT
extern char* g_rds;
#endif //USE_CRYPT
extern unsigned int g_TimeBias;//与服务器商定的基准时刻；

///析构，析构时，sinker与socket总有一个先关闭，先关闭者要将关联对方中的自己置为空，以防止内存访问错误;
IDsSocketSinkBase::~IDsSocketSinkBase(void)
{
};

///绑定至本地端口
BOOL CMuxSocket::BindToLocalAddr( INT nPort )//绑定至本地端口；
{
	if ( DSS_BOUND == m_DssStatus )//已经绑定过了，直接返回；
	{
		return TRUE;
	}

	if ( DSS_CREATED != m_DssStatus )//如果不是新创建的socket，则不允许绑定；
	{
		CreateInnerSocket();
		if ( DSS_CREATED != m_DssStatus )//再次检测；
		{
			return FALSE;
		}		
	}

	memcpy( m_strSelfIpAddr, "127.0.0.1", sizeof(m_strSelfIpAddr) );

	memset( &m_LocalSockAddr, 0, sizeof(m_LocalSockAddr) );//本地地址设置；
	m_LocalSockAddr.sin_family = AF_INET;
	m_LocalSockAddr.sin_port = htons( nPort );    
	m_LocalSockAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	if ( 0 == bind( m_hSocket, (SOCKADDR *) &m_LocalSockAddr, sizeof(m_LocalSockAddr) ) )
	{
		SetSocketStatus( DSS_BOUND );
		return TRUE;
	} else {
		LogErr( LOGERR_LEV_LOG, "绑定到端口%d失败", nPort );
	}

	return FALSE;
}

///建内部SOCKET；
BOOL CMuxSocket::CreateInnerSocket()
{
	g_bIsStopSend = FALSE;
	m_errNO = 0;//初始无错误；
	m_hSocket = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP );
	if ( INVALID_SOCKET != m_hSocket )
	{
		INT tmpul = 1;//初始设为非阻塞，以执行连接操作；
		INT nRet = ioctlsocket(m_hSocket, FIONBIO, (unsigned long *) &tmpul);//置为阻塞方式；
		if (nRet == SOCKET_ERROR)
		{
			LogErr( LOGERR_LEV_LOG, "CreateInnerSocket,置非阻塞模式失败" );
			INT jjj = 0;//置阻塞方式失败;
		}

		BOOL reusesock = TRUE;//重用socket;
		setsockopt( m_hSocket, SOL_SOCKET, SO_REUSEADDR, (const char*)(&reusesock), sizeof(reusesock) );

		BOOL isDNTLinger = TRUE;//不使用linger;
		setsockopt( m_hSocket, SOL_SOCKET, SO_DONTLINGER, (const char*)(&isDNTLinger), sizeof(isDNTLinger) );

		BOOL toset = TRUE;//使用Nagle算法；
		setsockopt( m_hSocket, IPPROTO_TCP, TCP_NODELAY, (const char*)(&toset), sizeof(toset) );
		SetSocketStatus( DSS_CREATED );
		return TRUE;
	} else {
		INT jjj = GetLastError();
	}
	return FALSE;
}

///设为阻塞模式，以执行阻塞收发；
BOOL CMuxSocket::SetBlock()
{
	if ( INVALID_SOCKET != m_hSocket )
	{
		INT tmpul = 0;//初始设为非阻塞，以执行连接操作；
		INT nRet = ioctlsocket(m_hSocket, FIONBIO, (unsigned long *) &tmpul);//置为阻塞方式；
		if (nRet == SOCKET_ERROR)
		{
			LogErr( LOGERR_LEV_LOG, "SetBlock,置阻塞模式失败" );
			INT jjj = 0;//置阻塞方式失败;
			return false;
		}
	}
	return true;
}

///实际连接，阻塞；
BOOL CMuxSocket::MuxCliConnect( BOOL& isNeedClose/*连接失败时置TRUE*/ )
{
	isNeedClose = FALSE;
	if ( IsDsSocketConnected() )
	{
		return TRUE;
	}

	SOCKET socketUsed = INVALID_SOCKET;
	int isok = GetSocketHandle( socketUsed );

	if ( !IsDsSocketConnecting() )
	{
		//第一次连接；
		SetSocketStatus( DSS_CONNECTING );//将状态设为正在连接；
		LogErr( LOGERR_LEV_LOG, "Connect, ip:%s,端口：%d", GetRemoteAddr(), GetRemotePort() );

		if ( !isok )
		{
			LogErr( LOGERR_LEV_LOG, "网络线程获取socket句柄失败" );
			return FALSE;
		}
		SOCKADDR_IN remoteAddrIn = GetRemoteSockAddrIn();
		WORD        wPort        = GetRemotePort();

		isok = connect( socketUsed, (sockaddr*)&remoteAddrIn, sizeof(remoteAddrIn) );

		if ( 0 == isok )
		{
			OnConnected( TRUE );
			return TRUE;
		} else {
			INT nerr = WSAGetLastError();
			if ( WSAEISCONN == nerr )
			{
				OnConnected( TRUE );
				return TRUE;//已连接成功；
			}  else if ( (WSAEWOULDBLOCK == nerr) || ( WSAEALREADY == nerr ) || ( WSAEINPROGRESS == nerr ) ) {
				//连接进行中。。。
			} else {
				//连接失败；
				LogErr( LOGERR_LEV_WARNING, "连接远端地址%s失败，错误%d", GetRemoteAddr(), nerr );
				OnDisConnected();
				OnConnected( FALSE );
				isNeedClose = TRUE;				
			}
		}
		return TRUE;//第一次连接；
	} 

	//连接进行中；
	fd_set efds;
	fd_set wfds;
	FD_ZERO(&efds);
	FD_ZERO(&wfds);
	FD_SET(socketUsed, &efds);
	FD_SET(socketUsed, &wfds);
	timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
	int srst = select( 2/*忽略*/, NULL, &wfds, &efds, &timeout );
	if ( 0 == srst )
	{
		///进行中
		return TRUE;
	}
	if ( SOCKET_ERROR == srst )
	{
		int nerr = WSAGetLastError();
		LogErr( LOGERR_LEV_WARNING, "连接远端地址%s失败，错误%d", GetRemoteAddr(), nerr );
		OnDisConnected();
		OnConnected( FALSE );
		isNeedClose = TRUE;
	}
	if ( srst > 0 )
	{
		int nerr = WSAGetLastError();
		if ( FD_ISSET( socketUsed, &wfds ) )
		{
			//连接成功；
			OnConnected( TRUE );
			return TRUE;//已连接成功；
		} else if ( FD_ISSET( socketUsed, &efds) ) {
			LogErr( LOGERR_LEV_WARNING, "select,连接远端地址%s失败", GetRemoteAddr() );
			OnDisConnected();
			OnConnected( FALSE );
			isNeedClose = TRUE;
		}
	}
	return FALSE;
};

///网络读线程；
DWORD WINAPI ReadThread( LPVOID pParam );
///网络写线程；
DWORD WINAPI WriteThread( LPVOID pParam );

///连接建立；
void CMuxSocket::OnConnected( BOOL isSuc ) 
{
	if ( isSuc )
	{
		SetBlock();//设为阻塞模式，准备阻塞接收；
		SetSocketStatus( DSS_CONNECTED );
		LogErr( LOGERR_LEV_LOG, "连接远端地址%s:%d成功", GetRemoteAddr(), GetRemotePort() );
		m_harrNetThread[0] = HXBCBEGINTHREADEX( NULL, 0, ReadThread, this, 0, NULL );
		m_harrNetThread[1] = HXBCBEGINTHREADEX( NULL, 0, WriteThread, this, 0, NULL );
	} else {
		SetSocketStatus( DSS_BOUND );
	}

	if ( NULL != m_pSinker )
	{
		m_pSinker->OnConnected( isSuc );
	}
} //连接建立；

///连接远程机器发起;
BOOL CMuxSocket::IssueConnectRemoteAddr( const char* strRemoteAddr, INT nRemotePort, BOOL isNeedClose/*连接失败时需要断开*/ )
//使用connectex连接远程机器;
{
	TRY_BEGIN
		;
	LogErr( LOGERR_LEV_LOG, "收到连接远端地址%s请求...", strRemoteAddr );

	if ( DSS_BOUND != m_DssStatus )//未绑定；
	{
		if ( ! BindToLocalAddr( 0 ) ) //assign a random port;
		{
			LogErr( LOGERR_LEV_LOG, "绑定到端口失败" );
			return FALSE;
		}
	}

	memcpy( m_strRemoteIpAddr, strRemoteAddr, sizeof(m_strRemoteIpAddr) );//远端地址字符串；
	m_dwRemotePort = nRemotePort;
	memset( &m_RemoteSockAddr, 0, sizeof(m_RemoteSockAddr) );//远端地址设置；
	m_RemoteSockAddr.sin_family = AF_INET;
	m_RemoteSockAddr.sin_port = htons( nRemotePort );    
	m_RemoteSockAddr.sin_addr.s_addr = inet_addr( m_strRemoteIpAddr );

	if ( !MuxCliConnect( isNeedClose ) )
	{
		return FALSE;
	}

	return TRUE;
	TRY_END
		;
	return FALSE;
}

///////BUG查找――模拟收包，内容随机；
//#define DEBUG_BUF_SIZE 10240
//#include "../PkgProc/PacketBuild.h"
//#include "../PkgProc/CliProtocol.h"
//using namespace MUX_PROTO;
//MsgToPut* GenRandomPkg()
//{
//	static char sendBuf[DEBUG_BUF_SIZE];//包缓存;
//	static int writePos = 0;//写内容指针；
//	static int readPos = 0;//取内容指针；
//	static int initBufferedNum = 0;	
//	if ( readPos >= writePos )
//	{
//		initBufferedNum = 0;
//		//读指针==写指针，说明存储的内容都已发完，重新用随机包填sendBuf;
//		/*
//		typedef struct StrGCRun
//		{
//		static const unsigned char  byPkgType = G_C;
//		static const unsigned short wCmd = G_C_RUN;
//		PlayerID lPlayerID;//玩家ID号；
//		unsigned long lCurOffset;//客户端时刻偏移值；
//		unsigned short nOrgX;//移动出发点X坐标；
//		unsigned short nOrgY;//移动出发点Y坐标；
//		float fOrgCroodX;//移动出发点X客户端坐标；
//		float fOrgCroodY;//移动出发点Y客户端坐标；
//		unsigned short nNewX;//移动目标点X坐标；
//		unsigned short nNewY;//移动目标点Y坐标；
//		float fNewCroodX;//移动目标点X客户端坐标；
//		float fNewCroodY;//移动目标点Y客户端坐标；
//		}GCRun; //服务器发给客户端的跑动消息
//		*/
//		readPos = writePos = 0;//回复初始位置；
//		while ( DEBUG_BUF_SIZE - writePos > 512 )//只要剩余空间足以放下一个最大包，则继续放临时包直至把空间放满，之后供模拟线程发包使用；
//		{
//			int trand = rand() % 2;
//			if ( trand<1 )
//			{
//				char* pPkg = NULL;
//				int nPkgLen = 0;
//				GCRun tmpSend;
//				tmpSend.lPlayerID.wGID = 0;
//				tmpSend.lPlayerID.dwPID = rand();
//				tmpSend.lCurOffset = 0x87654321;
//				tmpSend.nOrgX = 100;
//				tmpSend.nOrgY = 10;
//				tmpSend.fOrgCroodX = (float)100.12;
//				tmpSend.fOrgCroodY = (float)100.34;
//				tmpSend.nNewX = 101;
//				tmpSend.nNewY = 11;
//				tmpSend.fNewCroodX = (float)101.12;
//				tmpSend.fNewCroodY = (float)101.34;
//				CPacketBuild::CreatePkg<GCRun>( &tmpSend, &pPkg, nPkgLen );
//				if ( ( nPkgLen>0 ) //有组包；
//					&& ( DEBUG_BUF_SIZE - writePos > nPkgLen ) //还有足够的空间
//					)
//				{
//					memcpy( &(sendBuf[writePos]), pPkg, nPkgLen );
//					writePos += nPkgLen;
//					++initBufferedNum;
//				}
//			} else {
//				char* pPkg = NULL;
//				int nPkgLen = 0;
//				GCPlayerSelfInfo tmpSend;
//				tmpSend.lPlayerID.wGID = 0;
//				tmpSend.lPlayerID.dwPID = rand();
//				CPacketBuild::CreatePkg<GCPlayerSelfInfo>( &tmpSend, &pPkg, nPkgLen );
//				if ( ( nPkgLen>0 ) //有组包；
//					&& ( DEBUG_BUF_SIZE - writePos > nPkgLen ) //还有足够的空间
//					)
//				{
//					memcpy( &(sendBuf[writePos]), pPkg, nPkgLen );
//					writePos += nPkgLen;
//					++initBufferedNum;
//				}
//			}
//		}
//	}
//
//	if ( writePos <= readPos )
//	{
//		__asm int 3
//	}
//	int curBuffered = writePos - readPos;
//	int thistimesend = rand() % 511 + 1;//随机选部分内容发送出去;
//	if ( curBuffered < 10 )		
//	{
//		thistimesend = curBuffered;
//	}
//	if (thistimesend >= curBuffered)
//	{
//		thistimesend = curBuffered;
//	}
//	MsgToPut* pMsgToPut = g_poolMsgToPut->RetrieveOrCreate();
//	pMsgToPut->nMsgLen = thistimesend;
//	memcpy( pMsgToPut->pMsg, &(sendBuf[readPos]), thistimesend );
//	readPos += thistimesend;
//	if ( readPos == 9750 )
//	{
//		int jjj = 0;
//	}
//	return pMsgToPut;
//}
//
/////BUG查找――模拟收包，内容随机；
//DWORD WINAPI ReadThread( LPVOID pParam )
//{
//	CMuxSocket* pSocket = (CMuxSocket*) pParam;
//
//	SOCKET socketUsed = INVALID_SOCKET;
//	int isok = pSocket->GetSocketHandle( socketUsed );
//
//	MsgToPut* pMsg = NULL;
//	for ( int i=0; i<200; ++i )
//	{
//		pMsg = GenRandomPkg();
//		pSocket->OnRecved( pMsg );
//		Sleep(500);
//	}
//
//	return 0;
//}


///网络读线程；
//#define RCVBUF_SIZE 2048
DWORD WINAPI ReadThread( LPVOID pParam )
{
	CMuxSocket* pSocket = (CMuxSocket*) pParam;

	SOCKET socketUsed = INVALID_SOCKET;
	int isok = pSocket->GetSocketHandle( socketUsed );

	MsgToPut* pMsg = NULL;

	while ( TRUE )//thread will exit with closesocket;
	{
		pMsg = g_poolMsgToPut->RetrieveOrCreate();
		int rcved = recv( socketUsed, pMsg->pMsg, MsgToPut::MSG_MAX_SIZE, 0/*no flag*/ );
		if ( g_bIsStopSend )
		{
			return 0;
		}
		if ( 0 == rcved )
		{
			//socket closed;
			if ( NULL != g_poolMsgToPut )
			{
				g_poolMsgToPut->Release( pMsg );
			} else {
				//LogErr( LOGERR_LEV_ERR, "MsgToPut池空" );
				return 0;
				//__asm int 3 //delete pMsg; pMsg = NULL;
			}
			pSocket->OnDisConnected();
			return 0;
		}
		if ( SOCKET_ERROR == rcved )
		{
			g_poolMsgToPut->Release( pMsg );
			//error occured;
			INT nerr = GetLastError();
			if ( WSAEWOULDBLOCK == nerr )
			{
				//实际因为已将socket设为阻塞，不可能到此处；
				continue;
			}
			pSocket->OnRcvError( nerr );
			return 0;
		}
		//else recv ok;
		pMsg->nMsgLen = rcved;
		if ( pSocket->OnRecved( pMsg ) < 0 )
		{
			//收包处理错，退出收包线程；
			return 0;
		}
		pMsg = NULL;//准备下次接收；
	}

	return 0;
}

#include "../PkgProc/PacketBuild.h" //用于网络线程发送心跳包；
#ifdef USE_CRYPT
#include "../../../../Base/crypt/dscryptbf.h"
#endif //USE_CRYPT

//				int sendpos = 0;//发送位置；
//				while ( true )
//				{
//#ifdef USE_CRYPT
//					int sended = send( socketUsed, enced+sendpos, toSendLen, 0/*no flag*/ );
//#else  //USE_CRYPT
//					int sended = send( socketUsed, pToSend/*有效内容*/+sendpos, toSendLen, 0/*no flag*/ );
//#endif //USE_CRYPT
//					if ( SOCKET_ERROR == sended )
//					{
//						//error occured;
//						INT nerr = GetLastError();
//						if ( WSAEWOULDBLOCK == nerr )
//						{
//							//实际因为已将socket设为阻塞，不可能到此处；
//							continue;
//						}
//						pSocket->OnSendError( nerr );
//
//#ifdef USE_CRYPT
//						if ( NULL != toenc )
//						{
//							delete [] toenc;//暂存待加密消息
//							toenc = NULL;
//						}
//						if ( NULL != enced )
//						{
//							delete [] enced;
//							enced = NULL;
//						}
//#endif //USE_CRYPT
//						return 0;
//					}
//
//					if ( sended < 0 )
//					{
//						INT nerr = GetLastError();
//						pSocket->OnSendError( nerr );
//					}
//					//else send ok;
//					sendpos += sended;//发送位置前移；
//					toSendLen -= sended;
//					if ( toSendLen <= 0 )
//					{
//						break;//心跳消息已发送完毕；
//					}
//				} //while

		//		//发送待发包；		
//		while ( pMsgToSend->GetValidSize() > 0 )//没有发完，则一直发完为止；
//		{
//			int sended = send( socketUsed
//				, pMsgToSend->GetValidMsg()  //有效内容；
//				, pMsgToSend->GetValidSize() //待发送尺寸；
//				, 0/*no flag*/ );
//			if ( SOCKET_ERROR == sended )
//			{
//				//error occured;
//				INT nerr = GetLastError();
//				if ( WSAEWOULDBLOCK == nerr )
//				{
//					//实际因为已将socket设为阻塞，不可能到此处；
//					continue;
//				}
//				pSocket->OnSendError( nerr );
//
//#ifdef USE_CRYPT
//				if ( NULL != toenc )
//				{
//					delete [] toenc;//暂存待加密消息
//					toenc = NULL;
//				}
//				if ( NULL != enced )
//				{
//					delete [] enced;
//					enced = NULL;
//				}
//#endif //USE_CRYPT
//				return 0;
//			}
//			//else send ok;
//			int tmpint;
//			pMsgToSend->PopSomeMsg( sended, tmpint );
//			if ( tmpint != sended )
//			{
//				//LogErr( LOGERR_LEV_ERR, "写线程:发送消息MsgToPut中tmpint != sended" );
//#ifdef USE_CRYPT
//				if ( NULL != toenc )
//				{
//					delete [] toenc;//暂存待加密消息
//					toenc = NULL;
//				}
//				if ( NULL != enced )
//				{
//					delete [] enced;
//					enced = NULL;
//				}
//#endif //USE_CRYPT
//				return 0;
//				//__asm int 3
//			}
//		}

const unsigned int SEND_ENC_BUFSIZE = 1024;


//包发送，由唯一的写线程调用；
#ifdef USE_CRYPT
bool SocketBlockSend( const SOCKET& sendsocket, const char* toSend, const int inLen, DsCryptBF& sendCrypt, char* toencbuf, char* encedbuf, INT& nerr )
#else  //USE_CRYPT
bool SocketBlockSend( const SOCKET& sendsocket, const char* toSend, const int inLen, INT& nerr )
#endif //USE_CRYPT
{
	nerr = 0;
	if ( ( NULL == toSend ) 
		|| ( inLen <= 0 )
		)
	{
		return false;
	}

	int toSendLen = inLen;//待发送长度；

#ifdef USE_CRYPT
	if ( ( NULL == toencbuf ) || ( NULL == encedbuf ) )
	{
		return false;
	}
	memcpy( toencbuf+sizeof(unsigned int)+sizeof(unsigned int), toSend, inLen );//留出前面的8个字节存放时间信息以及包序号；
	unsigned int encsendlen = 0;
	unsigned int dumptimeinfo = ::GetTickCount() - g_TimeBias;;
	if ( 0 != g_TimeBias )
	{
		CONSOLE_DEBUG( "发送包中的时间信息%d", dumptimeinfo );
	}
	if ( ! sendCrypt.EncryptSendPkg( (const unsigned char*)toencbuf, inLen+sizeof(unsigned int)+sizeof(unsigned int)
		, (unsigned char*)encedbuf, SEND_ENC_BUFSIZE, encsendlen, dumptimeinfo, true ) )
	{
		return false;
	}

	//加密标志检测；
	bool isStSendCrypt = false;
	if ( sizeof(StEncDec) == inLen )
	{
		if ( !(sendCrypt.IsStSendCrypt()) )
		{
			//尚未开始发送加密，检测是否为开启发送加密指示；
			StEncDec* tmpSend = (StEncDec*)toSend;
			if ( tmpSend->IsValidStEncDec() )
			{
				//往后发包为加密包，先以明文发送本包，本包发送完毕后，将设置加密开始，以后的包将加密发送；
				CONSOLE_DEBUG( "发送加密开始标记，长度%d:%d", inLen, encsendlen );
				if ( NULL == g_rds )
				{
					CONSOLE_DEBUG( "设置发送加密，无可用密钥" );
					return false;
				}
				sendCrypt.SetSendKey( g_rds, 16 );//置加密密钥;
				delete [] g_rds; g_rds = NULL;//删去已使用过的key;

				sendCrypt.StSendCrypt();//置发送加密开始；
				isStSendCrypt = true;
			}
		}
	}

	if ( ! isStSendCrypt )
	{
		if ( sendCrypt.IsStSendCrypt() )
		{
			CONSOLE_DEBUG( "发送加密包，长度%d:%d\n", inLen, encsendlen );
		} else {
			CONSOLE_DEBUG( "发送明文包，长度%d:%d\n", inLen, encsendlen );
		}
	}
	toSendLen = encsendlen;
#endif //USE_CRYPT

	int sendpos = 0;//发送位置；
	while ( true )
	{
#ifdef USE_CRYPT
		int sended = send( sendsocket, encedbuf+sendpos, toSendLen, 0/*no flag*/ );
#else  //USE_CRYPT
		int sended = send( sendsocket, toSend+sendpos, toSendLen, 0/*no flag*/ );
#endif //USE_CRYPT
		if ( SOCKET_ERROR == sended )
		{
			//error occured;
			nerr = GetLastError();
			if ( WSAEWOULDBLOCK == nerr )
			{
				//实际因为已将socket设为阻塞，不可能到此处；
				continue;
			}
			return false;
		}

		if ( sended < 0 )
		{
			INT nerr = GetLastError();
			return false;
		}

		//else send ok;
		sendpos += sended;//发送位置前移；
		toSendLen -= sended;
		if ( toSendLen <= 0 )
		{
			break;//心跳消息已发送完毕；
		}
	} //while

	return true;
} 

BOOL g_bIsStopSend = FALSE;//是否停止往外发数据；
///网络写线程；
DWORD WINAPI WriteThread( LPVOID pParam )
{
	CMuxSocket* pSocket = (CMuxSocket*) pParam;

	SOCKET socketUsed = INVALID_SOCKET;
	int isok = pSocket->GetSocketHandle( socketUsed );

	MsgToPut* pMsgToSend = NULL;

#ifdef USE_CRYPT
	DsCryptBF sendCrypt;
	//unsigned char cipherkeysend[16];
	//for ( int i=0; i<16; ++i )
	//{
	//	cipherkeysend[i] = i;
	//}
	//sendCrypt.SetSendKey( (const char*)cipherkeysend, 16 );
	char* toenc = NEW char[SEND_ENC_BUFSIZE];//暂存待加密消息
	char* enced = NEW char[SEND_ENC_BUFSIZE];//存放加密后消息以便发送
#endif //USE_CRYPT

	////////////////////////////////////////////////////////////////
	//心跳检测消息发送；
	static CPacketBuildNetTrd netTrdPkgBuild;
	static unsigned int lastCheckID = 0;
	static unsigned int lastchecktime = GetTickCount();
	unsigned int curpassed = 0;
	CGTimeCheck timeCheck;
	//心跳检测消息发送；
	////////////////////////////////////////////////////////////////

	//得到待发包；
	//		pSocket->WaitSendEvent();//等待写网络事件发生；
	while ( !g_bIsStopSend )
	{
		////////////////////////////////////////////////////////////////
		//心跳检测消息发送；
		curpassed = GetTickCount() - lastchecktime;
		if ( curpassed > 1000 )
		{
			lastchecktime = GetTickCount();
			//send心跳；			
			++lastCheckID;
			timeCheck.checkID = lastCheckID;
			timeCheck.sequenceID = lastchecktime;
			char* pToSend = NULL;
			int toSendLen = 0;
			if ( netTrdPkgBuild.CreateNetTrdPkg<CGTimeCheck>( &timeCheck, &pToSend, toSendLen ) )
			{
				INT nerr = 0;
#ifdef USE_CRYPT
				bool isSendOK = SocketBlockSend( socketUsed, pToSend, toSendLen, sendCrypt, toenc, enced, nerr );
#else  //USE_CRYPT
				bool isSendOK = SocketBlockSend( socketUsed, pToSend, toSendLen, nerr );
#endif //USE_CRYPT
				if ( !isSendOK )
				{
					pSocket->OnSendError( nerr );
#ifdef USE_CRYPT
					if ( NULL != toenc )
					{
						delete [] toenc;
						toenc = NULL;
					}
					if ( NULL != enced )
					{
						delete [] enced;
						enced = NULL;
					}
#endif //USE_CRYPT
					return 0;
				}
			} else {
				//LogErr( LOGERR_LEV_ERR, "写线程:发送心跳消息时组包失败" );
				pSocket->OnSendError( 6543 );
#ifdef USE_CRYPT
				if ( NULL != toenc )
				{
					delete [] toenc;
					toenc = NULL;
				}
				if ( NULL != enced )
				{
					delete [] enced;
					enced = NULL;
				}
#endif //USE_CRYPT
				return 0;
			}
		}
		//心跳检测消息发送；
		////////////////////////////////////////////////////////////////

		int getrst = pSocket->GetDataToSend( pMsgToSend );
		if ( getrst < 0 )
		{
			//取数据错误，可能是因为之前收或发数据已失败，中断写线程；
#ifdef USE_CRYPT
			if ( NULL != toenc )
			{
				delete [] toenc;//暂存待加密消息
				toenc = NULL;
			}
			if ( NULL != enced )
			{
				delete [] enced;
				enced = NULL;
			}
#endif //USE_CRYPT
			return 0;
		}

		if ( g_bIsStopSend )
		{
#ifdef USE_CRYPT
			if ( NULL != toenc )
			{
				delete [] toenc;//暂存待加密消息
				toenc = NULL;
			}
			if ( NULL != enced )
			{
				delete [] enced;
				enced = NULL;
			}
#endif //USE_CRYPT
			return 0;
		}

		if ( ( NULL == pMsgToSend )
			|| ( pMsgToSend->GetValidSize()<=0 )
			)
		{
			Sleep(2);//让出部分CPU；
			continue;
		}

		INT nerr = 0;

#ifdef USE_CRYPT
		bool isSendOK = SocketBlockSend( socketUsed, pMsgToSend->GetValidMsg(), pMsgToSend->GetValidSize(), sendCrypt, toenc, enced, nerr );
#else  //USE_CRYPT
		bool isSendOK = SocketBlockSend( socketUsed, pMsgToSend->GetValidMsg(), pMsgToSend->GetValidSize(), nerr );
#endif //USE_CRYPT
		if ( !isSendOK )
		{
			if ( NULL != g_poolMsgToPut )
			{
				g_poolMsgToPut->Release( pMsgToSend );
			}
			
			pSocket->OnSendError( nerr );
#ifdef USE_CRYPT
			if ( NULL != toenc )
			{
				delete [] toenc;
				toenc = NULL;
			}
			if ( NULL != enced )
			{
				delete [] enced;
				enced = NULL;
			}
#endif //USE_CRYPT
			return 0;
		}

		//回收待发包；
		if ( NULL != g_poolMsgToPut )
		{
			g_poolMsgToPut->Release( pMsgToSend );
		} else {
			//LogErr( LOGERR_LEV_ERR, "写线程:回收MsgToPut时发现g_poolMsgToPut空" );
#ifdef USE_CRYPT
			if ( NULL != toenc )
			{
				delete [] toenc;//暂存待加密消息
				toenc = NULL;
			}
			if ( NULL != enced )
			{
				delete [] enced;
				enced = NULL;
			}
#endif //USE_CRYPT
			return 0;
			//__asm int 3 //delete pMsgToSend; pMsgToSend = NULL;
		}

		pMsgToSend = NULL;//准备下次发送；
	}

	//pSocket->ResetSendEvent();
#ifdef USE_CRYPT
	if ( NULL != toenc )
	{
		delete [] toenc;//暂存待加密消息
		toenc = NULL;
	}
	if ( NULL != enced )
	{
		delete [] enced;
		enced = NULL;
	}
#endif //USE_CRYPT
	return 0;
}

