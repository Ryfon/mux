﻿/**
* @file MsgToPut.cpp
* @brief 在网络与应用之间传递的消息结构定义
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MsgToPut.cpp
* 摘    要: 在网络与应用之间传递的消息结构定义
* 作    者: dzj
* 完成日期: 2007.11.30
*
*/
#include "StdAfx.h"
#include "MsgToPut.h"

MsgToPut* PopMsgToPutFromList( list<MsgToPut*>* pList )
{
	if ( pList->empty() )
	{
		return NULL;
	} else {
		MsgToPut* tmpReturn = pList->front();
		pList->pop_front();
		return tmpReturn;
	}
}

///取一个完整包，会确保在两次取之间返回的内容一直有效，但一旦再次取包，则之前的内容将立即失效；
///所有的pop操作只能在本函数中发生；
char* CRcvBufferQueue::GetOneRcvedPkg( unsigned short& pkgLen, bool& isParsePkgErr )
//char* CRcvBufferQueue::GetOneRcvedPkg( unsigned short& pkgLen )
{
	isParsePkgErr = false;
	//取一个收包；
	if ( m_NextPkgSize <=0 )
	{
		int getRst = GetPkgLen();
		if ( getRst<0 )
		{
			//取包长错误；
			isParsePkgErr = true;
			return NULL;
		} else if ( 0==GetPkgLen() ) {
			//取不到包长，或包长还未取完整；
			return NULL;
		} else {
			//包长已取出，继续进行；
		}
	}

	if ( m_NextPkgSize < 5 )
	{
		//包头长度为5，因此包长不可能小于5;
		LogErr( LOGERR_LEV_ERR, "包头长度小于5,位置CRcvBufferQueue::GetOneRcvedPkg" );
		isParsePkgErr = true;
		return NULL;
		//__asm int 3
	}

	int nToRcv = m_NextPkgSize - 2;//除去已收取的包头长度字段；

	//以下继续取包；
	while ( nToRcv-m_CurBuffed > 0 )
	{
		//还未收够一个完整包；
		if ( ( NULL == m_pCurMsg )
			|| ( m_pCurMsg->GetValidSize()<=0 )
			)
		{
			//当前处理包空，或当前处理包的有效内容已空;
			m_pCurMsg = m_pRcvQueue->PopMsg();
			if ( NULL == m_pCurMsg )
			{
				return NULL;//队列中已没有未处理包；
			}
		}

		int curRcved = 0;
		char* pRcved = m_pCurMsg->PopSomeMsg( nToRcv-m_CurBuffed, curRcved );
		if ( curRcved <= 0 )
		{
			//不应收到长度为0消息；
		    LogErr( LOGERR_LEV_ERR, "收到长度为0消息,位置CRcvBufferQueue::GetOneRcvedPkg" );
		    isParsePkgErr = true;
			return NULL;
			//__asm int 3
		}
		if ( m_CurBuffed+curRcved >= MsgToPut::MSG_MAX_SIZE )
		{
			//收包大小溢出;
		    LogErr( LOGERR_LEV_ERR, "收包大小溢出,位置CRcvBufferQueue::GetOneRcvedPkg" );
		    isParsePkgErr = true;
			return NULL;
			//__asm int 3
		}
		memcpy( &(m_MsgBuff[m_CurBuffed]), pRcved, curRcved );
		if ( m_pCurMsg->GetValidSize() <= 0 )
		{
			//如果包内容已被取空，则回收；
			if ( NULL != g_poolMsgToPut )
			{
				g_poolMsgToPut->Release( m_pCurMsg );
			} else {
				LogErr( LOGERR_LEV_ERR, "回收MsgToPut时发现g_poolMsgToPut空，位置GetOneRcvedPkg" );
				isParsePkgErr = true;
				return NULL;
				//__asm int 3 //delete m_pCurMsg; m_pCurMsg = NULL;
			}

			m_pCurMsg = NULL;
		}
		m_CurBuffed += curRcved;
	}//继续取包，while ( m_NextPkgSize-m_CurBuffed > 0 )

	if ( m_CurBuffed == nToRcv )
	{
		//收到了一个完整包；
		//此时立即清收包长度准备取下一个包，也就是说，本函数的调用者在下次调用之前必须处理完这次的返回包；
		pkgLen = nToRcv;
		m_CurBuffed = 0;
		m_byHalfPkgLen[0] = 0;
		m_byHalfPkgLen[1] = 0;
		m_NextPkgSize = 0;
		return ((char*)m_MsgBuff);
	} else {
		pkgLen = 0;
		return NULL;
	}
}

///返回值, 0:包头长度字段还未收完，1:包头长度已取出,-1：包头长度错
int CRcvBufferQueue::GetPkgLen()
{
	if ( m_NextPkgSize > 0 )//已经取过了包长，则直接返回；
	{
		return 1;
	}

	//没有取过包长，或之前取包长不完整，则取包长；
	for ( int i=0; i<2; ++i ) //不可能连续取了2个包都取不到足够的包头；
	{
		if ( ( NULL == m_pCurMsg )
			|| ( m_pCurMsg->GetValidSize()<=0 )
			)
		{
			m_pCurMsg = m_pRcvQueue->PopMsg();
			if ( NULL == m_pCurMsg )
			{
				return 0;//队列中已没有未处理包；
			}
		}

		if ( ( NULL == m_pCurMsg )
			|| ( m_pCurMsg->GetValidSize() <= 0 )
			)
		{
			LogErr( LOGERR_LEV_ERR, "MsgToPut有效长度0，位置CRcvBufferQueue::GetPkgLen" );
			return -1;
			//__asm int 3 //永远不应收到长度为0的消息包；
		}

		//包头大小2字节;
		int validSize = 0;//取到的数据大小；
		char* msgPtr = NULL;

		if ( m_bIsHalfPkgLen )//是否已经收了一半包长
		{
			msgPtr = m_pCurMsg->PopSomeMsg( 1, validSize );
		} else {
			msgPtr = m_pCurMsg->PopSomeMsg( 2, validSize );
		}

		if ( NULL == msgPtr )
		{
			LogErr( LOGERR_LEV_ERR, "msgPtr空，位置CRcvBufferQueue::GetPkgLen" );
			return -1;
			//__asm int 3  //不应到此处；
		}

		if ( m_bIsHalfPkgLen )
		{
			//原来已经收了一半包长字段；
			m_byHalfPkgLen[1] = *msgPtr;
			m_bIsHalfPkgLen = false;
		} else {
			//原来没收过包长字段；
			if ( validSize <= 0 )
			{
				LogErr( LOGERR_LEV_ERR, "收到长度为0的MsgToPut" );
				return -1;
				//__asm int 3 //永远不应收到长度为0的消息包；
			} else if ( validSize<=1 ){
				//只收到长度的一个字节
				m_byHalfPkgLen[0] = *msgPtr;
				m_bIsHalfPkgLen = true;
			} else if ( validSize<=2 ){
				m_byHalfPkgLen[0] = *msgPtr;
				m_byHalfPkgLen[1] = *(msgPtr+1);
				m_bIsHalfPkgLen = false;
			} else {
				LogErr( LOGERR_LEV_ERR, "取到的validSize>2，位置CRcvBufferQueue::GetPkgLen" );
			    return -1;
				//__asm int 3 
			}
		}//if ( m_bIsHalfPkgLen ) //之前是否收了一半包；

		if ( m_pCurMsg->GetValidSize() <= 0 )
		{
			//如果包内容已被取空，则回收；
			if ( NULL != g_poolMsgToPut )
			{
				g_poolMsgToPut->Release( m_pCurMsg );
			} else {
				LogErr( LOGERR_LEV_ERR, "全局g_poolMsgToPut空，位置CRcvBufferQueue::GetPkgLen, 二" );
			    return -1;
				//__asm int 3 //delete m_pCurMsg; m_pCurMsg = NULL;
			}
			m_pCurMsg = NULL;
		}

		if ( !m_bIsHalfPkgLen )//收到的包不只一半，因为如果没有收过包，则在之前会break，而只要收到了包，则最少会收一半；
		{
			////说明已经收到了完整的包长；
			//static int lastPkgSize = 0;//保留上一个包长;
			//static int rcvedPkgNum = 0;
			m_NextPkgSize = *( (unsigned short*) ( (char*)(&m_byHalfPkgLen) ) );
#ifdef CHECK_ENDIAN
			if ( g_isBigEndian )
			{
				m_NextPkgSize = ENDIAN_CHANGE_S(m_NextPkgSize);
			}
#endif //CHECK_ENDIAN

			if ( m_NextPkgSize >= MsgToPut::MSG_MAX_SIZE ) //包大小判定
			{
				LogErr( LOGERR_LEV_ERR, "消息包包头指明长度%d超过预期", m_NextPkgSize );
				return -1;
				//__asm int 3
			} else if ( m_NextPkgSize < 5 ){
				LogErr( LOGERR_LEV_ERR, "消息包包头指明长度%d太小", m_NextPkgSize );
				return -1;
			}
			//lastPkgSize = m_NextPkgSize;
			//++rcvedPkgNum;
			//if ( rcvedPkgNum >= 250 )
			//{
			//	int jjj = 0;
			//}

			break;
		}
	}//如果一次没有取够包头，则再取下一个包，for ( int i=0; i<2; ++i ) //不可能连续取了2个包都取不到足够的包头；

	return (m_NextPkgSize>0) ? 1:0;
}

