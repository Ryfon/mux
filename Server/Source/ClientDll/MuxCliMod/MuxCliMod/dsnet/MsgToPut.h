﻿/**
* @file MsgToPut.h
* @brief 在网络与应用之间传递的消息结构定义
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: MsgToPut.h
* 摘    要: 在网络与应用之间传递的消息结构定义
* 作    者: dzj
* 完成日期: 2007.11.30
*
*/

#pragma once

#include "tcache.h"

const unsigned short SRV_SID_MAX = 1000;
const unsigned short MAPSRV_SID_BASE = 700; //主动连接mapsrv时，mapsrv的sessionID base;
const unsigned short MAPSRV_SID_MAX = 800; //最大mapsrv sessionID号，最多100个mapsrv;

#ifdef CHECK_ENDIAN
extern bool g_isBigEndian;//intel/windows系统字节序，如果不是，则需要转成此字节序;
#endif //CHECK_ENDIAN

#define ENDIAN_CHANGE_S( toTran ) ((toTran<<8) | (toTran>>8)) 
#define ENDIAN_CHANGE_L( toTran ) ((toTran<<24) | (toTran>>8<<24>>8) | (toTran<<8>>24<<8) | (toTran>>24)) 

///在通信模块与缓冲队列之间传递的消息，中间包含了与通信模块相关的句柄值
///将此句柄值以及校验码传回通信模块，通信模块将可以唯一确定与该消息关联的客户端
///！！！注意：必须从全局池中分配，不可自行new，不可定义临时变量，不可定义静态变量，不可memset;
///因为所有的MsgToPut最后都要通过对象池回收，不遵守上述约定会造成内存错误；
struct MsgToPut
{
	static const unsigned int MSG_MAX_SIZE = 512;
	MsgToPut() : nMsgLen(0), nCurPos(0) {}
	~MsgToPut()
	{
		PoolObjInit();
	}

	PoolFlagDefine()
	{
		nMsgLen = 0;
		nCurPos = 0;
	}

	//取有效内容大小；
	int GetValidSize()
	{
		return (nMsgLen-nCurPos);
	}

	char* GetValidMsg()
	{
		if ( nMsgLen > nCurPos )
		{
		   return &(pMsg[nCurPos]);
		} else {
			return NULL;
		}
	}

	//弹出部分有效内容，返回有效内容的指针；
	char* PopSomeMsg( int popSize, int& popedSize )
	{
		int orgPos = nCurPos;
		popedSize = min( nMsgLen-nCurPos, popSize );		
		if ( popedSize > 0 )
		{
			nCurPos += popedSize;
			return &(pMsg[orgPos]);
		} else {
			return NULL;
		}
	}

	int nMsgLen;//可用消息大小；
	int nCurPos;//读消息当前位置；
	char pMsg[MSG_MAX_SIZE];/*消息设为固定大小*/
};

extern TCache< MsgToPut >* g_poolMsgToPut;

#include <list>
///缓冲队列实现类;
MsgToPut* PopMsgToPutFromList( list<MsgToPut*>* pList );
class CBufQueue
{
public:
	CBufQueue()
	{
		m_pBufQueue = NEW list<MsgToPut*>;
		m_pBufQueue->clear();
	}

	~CBufQueue()
	{
		CDsLock<CDsMutex> tmpLock( m_QueueLock );
		//把尚未处理的包全部删掉；
		if ( NULL != m_pBufQueue )
		{
			MsgToPut* pOutMsg = PopMsgToPutFromList( m_pBufQueue );
			while ( NULL != pOutMsg )
			{
				if ( NULL != g_poolMsgToPut )
				{
					g_poolMsgToPut->Release( pOutMsg );
				} else {
					delete pOutMsg; pOutMsg = NULL;
				}
				pOutMsg = PopMsgToPutFromList( m_pBufQueue );
			}

		}

		m_pBufQueue->clear();
		delete m_pBufQueue; m_pBufQueue = NULL;
	}

public:

	//////////////////////////////////////////////////////////////////////////
	///写信息使用；
	///将信息放入缓冲；
	virtual int PushMsg( MsgToPut* pInMsg ) 
	{ 
		//读写信息时获取m_MsgSwitchLock的读锁；
		if ( NULL == pInMsg )
		{
			//空信息也认为操作成功;
			return 0;
		}
		if ( NULL == m_pBufQueue )
		{
			//内部队列空，直接将收包删掉；
			if ( NULL != g_poolMsgToPut )
			{
				g_poolMsgToPut->Release( pInMsg );
			} else {
				LogErr( LOGERR_LEV_ERR, "CBufQueue::PushMsg时发现g_poolMsgToPut空" );
				delete pInMsg; pInMsg = NULL;
				//__asm int 3 
			}

			return -1;
		}
		CDsLock<CDsMutex> tmpLock( m_QueueLock );
		m_pBufQueue->push_back( pInMsg );
		return 0; 
	};
	///写信息使用；
	//////////////////////////////////////////////////////////////////////////

	///从缓冲中取信息；
	virtual MsgToPut* PopMsg() 
	{ 
		if ( NULL == m_pBufQueue )
		{
			return NULL;
		}

		CDsLock<CDsMutex> tmpLock( m_QueueLock );
		MsgToPut* tmpOut = PopMsgToPutFromList( m_pBufQueue );
		return tmpOut;
	};
	///读信息使用
	//////////////////////////////////////////////////////////////////////////

private:
	///用于输入的队列；
	list<MsgToPut*>* m_pBufQueue;
	///读写锁；
	CDsMutex m_QueueLock;
	///指示本缓存有信息可读的信号；
};

class CRcvBufferQueue
{
public:
	CRcvBufferQueue() : m_pRcvQueue(NULL), m_CurBuffed(0) {};

public:
	void Init()
	{
		m_pRcvQueue = NEW CBufQueue;
		m_CurBuffed = 0;
		m_NextPkgSize = 0;
		m_pCurMsg = NULL;
		m_bIsHalfPkgLen = false;
		m_byHalfPkgLen[0] = 0;
		m_byHalfPkgLen[1] = 0;
	}

	void Release()
	{
		if ( NULL != m_pRcvQueue )
		{
			delete m_pRcvQueue; m_pRcvQueue = NULL;
		}
	}

	bool AddPkgContent( MsgToPut* pMsg )
	{
		return ( 0 == m_pRcvQueue->PushMsg( pMsg ) );
	}

	///取一个完整包，会确保在两次取之间返回的内容一直有效，但一旦再次取包，则之前的内容将立即失效；
	///所有的pop操作只能在本函数中发生；
	char* GetOneRcvedPkg( unsigned short& pkgLen, bool& isParsePkgErr );

private:
	///取包长；
	int GetPkgLen();

private:
	CBufQueue* m_pRcvQueue;
	char m_MsgBuff[MsgToPut::MSG_MAX_SIZE];//临时组包缓冲；
	int m_CurBuffed;//当前临时组包缓冲中有效内容大小；

	char m_byHalfPkgLen[2];//包头长度；
	unsigned short m_NextPkgSize;//下一个欲收包的大小，每次处理完一个包后该值置0；	
	bool m_bIsHalfPkgLen;//包头长度的前一半是否有效；

	MsgToPut* m_pCurMsg;//当前正在处理的消息包；
	
};

class CSendBufferQueue
{
public:
	CSendBufferQueue() : m_pSendQueue(NULL) {};

public:
	void Init()
	{
		m_pSendQueue = NEW CBufQueue;
		//m_SendDataEvent = CreateEvent( NULL// no security attributes
		//	, FALSE         // auto-reset event，notice 每次wait成功后自动重清signaled？
		//	, FALSE        // initial state is not signaled
		//	, NULL         // object name
		//	); 
	}

 //   HANDLE& GetSendEventRef()
	//{
	//	return m_SendDataEvent;
	//}

	void Release()
	{
		if ( NULL != m_pSendQueue )
		{
			delete m_pSendQueue; m_pSendQueue = NULL;
		}
	}

	BOOL AddPkgContent( MsgToPut* pMsg )
	{
		bool isok = ( 0==m_pSendQueue->PushMsg( pMsg ) );
		if (!isok)
		{
			return FALSE;
		}

//		SetEvent( m_SendDataEvent );
		return TRUE;
	}

	MsgToPut* GetOnePkgToSend()
	{
		return m_pSendQueue->PopMsg();
	}

private:
	CBufQueue* m_pSendQueue;
//	HANDLE m_SendDataEvent;
};


