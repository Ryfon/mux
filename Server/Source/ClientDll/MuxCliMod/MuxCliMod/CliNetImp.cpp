﻿/**
* @file clinetimp.cpp
* @brief 客户端通信模块实现cpp文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称:clinetimp.cpp
* 摘    要:本cpp文件用于DLL，实现客户端通信模块接口
* 作    者:dzj
* 完成日期:2007.11.09
*/

#include "stdafx.h"

#include "..\..\LocalTest\CliNet.h"
#include "CliNetImp.h"
#include "PkgProc/PacketBuild.h"
#include "dsnet/msgtoput.h"

#include "winsock2.h"
#include "MSWSOCK.H"

#include "md5\md5.h"

//#include "utility\consoledbg.h"

extern HMODULE g_hModule;

char* g_rds = NULL;
unsigned int g_TimeBias = 0;//与服务器商定的基准时刻；

#ifdef CHECK_ENDIAN
bool  g_isBigEndian = false;//intel/windows系统字节序，如果不是，则需要转成此字节序;
#endif //CHECK_ENDIAN

using namespace MUX_CLIMOD;
using namespace MUX_PROTO;

#define SendPkg( PKG_TYPE, PKG_CONTENT ) \
{\
	char* pPkg = NULL;\
	int nPkgLen = 0;\
	if ( CPacketBuild::CreatePkg<PKG_TYPE>( &PKG_CONTENT, &pPkg, nPkgLen ) )/*//组包之后必须立即调用发包函数，在tmpRunMsg释放之前；*/\
    {\
	    int rst = SendPkgReq( 0/*包编号，目前暂时不用*/, pPkg/*发送缓冲区*/, nPkgLen/*发送数据长度*/ );\
	    if ( MUX_CLI_SUCESS != rst )\
        {\
		   int jjj = 0;\
        }\
    }\
}


#define DealPlayerPkgPre( OUTTYPE, outPtr, inputPkg, inputLen ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	memset( outPtr, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)m_clipro.DeCode( OUTTYPE::wCmd, &tmpStruct, outBufLen, inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
	{\
		return 0;\
	}

namespace MUX_CLIMOD
{
	///网络模块获得；
	MUXDLLFUN int GetMuxCliNetMod( IMuxCliNetMod** pNetMod )
	{
		*pNetMod = (IMuxCliNetMod*) NEW CMuxCliNetMod;
		if ( NULL != *pNetMod )
		{
			return MUX_CLI_SUCESS;
		} else {
			return MUX_CLI_NORMALL_ERR;
		}	
	}

	//取GetTickCount();
	DWORD DllGetTick()
	{
		return GetTickCount();
	}

	//取NiGetCurrentTimeInSec()
	MUXDLLFUN float DllNiGetCurSec()
	{
		static bool bFirst = true;
		static LARGE_INTEGER freq;
		static LARGE_INTEGER initial;

		if (bFirst)
		{
			QueryPerformanceFrequency(&freq);
			QueryPerformanceCounter(&initial);
			bFirst = false;
		}

		LARGE_INTEGER counter;
		QueryPerformanceCounter(&counter);
		return (float)((long double)
			(counter.QuadPart - initial.QuadPart) / 
			(long double) freq.QuadPart);
	}

	///初始化ds通信模块使用者；
	BOOL CMuxCliNetMod::InitDsSocketUser()
	{
		m_pSocket = NEW CMuxSocket( this );//将m_pPkgProcer与DsSocket关联；
		if ( NULL != m_pSocket )
		{
			return TRUE;
		}
		return FALSE;
	}

	///初始化客户端网络模块，输入参数为回调对象、服务器IP地址与端口；
	int CMuxCliNetMod::InitMuxCliNetMod( IMuxCliNetSinker* pMsgRcver, const char* assignSrvIp, WORD assignSrvPort )
	{
		//HANDLE d3dAddr = GetModuleHandle( "mssmp3.asi" );
		//d3dAddr = GetModuleHandle( "d3d9.dll" );		
		//if ( NULL == d3dAddr )
		//{
		//	CONSOLE_DEBUG( "未装载d3dx" );			
		//} else {
		//	CONSOLE_DEBUG( "已装载d3dx至%d", (DWORD)d3dAddr );
		//}

#ifdef CHECK_ENDIAN
		unsigned int testtest = 0x01;
		g_isBigEndian = ( *(char*)(&testtest) != 0x01 ) ? true:false;
#endif //CHECK_ENDIAN

		volatile static long isoverlap = 0;
		if ( m_bIsInited )
		{
			return MUX_CLI_REOPERA;
		}
		if ( InterlockedExchange( &isoverlap, 1 ) )
		{
			return MUX_CLI_INPROCESS;
		}
		if ( m_bIsInited )//双锁，防止重复进入；
		{
			return MUX_CLI_REOPERA;
		}
		m_bIsInited = TRUE;

#ifdef OPEN_CONSOLE
		RedirectIOToConsole();
#endif //OPEN_CONSOLE

		//CONSOLE_DEBUG( "初始化debug窗口" );
		//LogErr( LOGERR_LEV_DEBUG, "初始化debug窗口" );

#ifdef USE_CRYPT
		m_rcvCrypt.InitCryptBF();
		//unsigned char cipherkeyrcv[16];
		//for ( int i=0; i<16; ++i )
		//{
		//	cipherkeyrcv[i] = i;
		//}
		//m_rcvCrypt.SetRcvKey( (const char*)cipherkeyrcv, 16 );
		if ( NULL == m_todec )
		{
			m_todec = NEW char[DEC_ENC_BUFSIZE];//暂存待解密消息
		}
		if ( NULL == m_deced )
		{
			m_deced = NEW char[DEC_ENC_BUFSIZE];//存放解密后消息以便处理	
		}
#endif //USE_CRYPT

		md5::init();
		memset( m_strAccount, 0, sizeof(m_strAccount) );
		if ( NULL != m_tmpPassword )
		{
			memset( m_tmpPassword, 0, m_tmpPasswordLen );
			delete [] m_tmpPassword; m_tmpPassword = NULL;
		}
		//memset( m_strPassword, 0, sizeof(m_strPassword) );
		//////////////////////////////////////////////////////////////////////////////////////////
		///基准时刻协商...
		m_vecDetectPkgRcvTime.clear();
		m_dwNetBias = GetTickCount();//与服务器商定的基准时刻；
		g_TimeBias = 0;
		m_bIsNetBiasAquired = false;//是否已与服务器商定基准时刻；
		///...基准时刻协商；
		//////////////////////////////////////////////////////////////////////////////////////////

		m_nNetBusyStat = -1;//初始值；
		m_dwLastCheckID = 0;
		m_dwLastCheckTime = 0;

		if ( NULL == g_poolMsgToPut )
		{
		    g_poolMsgToPut = NEW TCache< MsgToPut >( 3 );//全局MsgToPut对象池；
		}

		if ( pMsgRcver == NULL )
		{
			//输入sinker空；
			return MUX_CLI_NORMALL_ERR;
		}

		//关联sinker与netmod;
		m_pMsgRcver = pMsgRcver;

		m_bIsConnected = FALSE;

		m_lSrvAddr = inet_addr( assignSrvIp );
		m_dwPort = assignSrvPort;
		if ( ( INADDR_NONE == m_lSrvAddr ) || ( 0 == m_lSrvAddr ) )
		{
			//输入地址不正确，无法转换；
			memset( m_strSrvAddr, 0, sizeof(m_strSrvAddr) );
			return MUX_CLI_NORMALL_ERR;
		} else {
			strcpy_s( m_strSrvAddr, sizeof(m_strSrvAddr), assignSrvIp ); 
		}

		m_bIsReset = FALSE;

		InterlockedExchange( &isoverlap, 0 );

		return 0;
	}

	///应用程序主线程调用，异步，向服务器发消息；
	int CMuxCliNetMod::SendPkgReq( const DWORD /*dwPkgID*/, const char* pPkg, const DWORD dwLen )
	{

#ifdef OPEN_CONSOLE
#pragma pack( push, 1 )
		typedef struct StrMsgHead
		{
			unsigned short msgLen;
			char           msgType;
			unsigned short msgCmd;			
		}MSG_HEAD;
#pragma pack( pop )
		//MSG_HEAD* pTmpHead = (MSG_HEAD*) pPkg;
		//if ( 0x102 == pTmpHead->msgCmd )
		//{
		//	CONSOLE_DEBUG( "向SRV发送聊天消息%x", pTmpHead->msgCmd );
		//} else if ( 0x10b == pTmpHead->msgCmd ) {
		//	//CONSOLE_DEBUG( "向SRV发送打怪消息%x", pTmpHead->msgCmd );
		//} else if ( CGTimeCheck::wCmd == pTmpHead->msgCmd ) {
		//	//CGTimeCheck* pTimeCheck = (CGTimeCheck*) (pPkg+5);
		//	//CONSOLE_DEBUG( "向SRV发送心跳消息" );
		//	//LogErr( LOGERR_LEV_DEBUG, "向SRV发送心跳消息" );
		//}
#endif //OPEN_CONSOLE

		if ( NULL == m_pSocket )
		{
			return MUX_CLI_NOINIT;
		}

		if ( !m_bIsConnected )
		{
			return MUX_CLI_NOTLOGIN;//尚未登录；
		}
		MsgToPut* pMsg = g_poolMsgToPut->RetrieveOrCreate();
		if ( sizeof(pMsg->pMsg) < dwLen )
		{
			//发包太大；
			LogErr( LOGERR_LEV_ERR, "发包太大，位置CMuxCliNetMod::SendPkgReq" );
			//__asm int 3
			g_poolMsgToPut->Release( pMsg );
			return MUX_CLI_NORMALL_ERR;
		}
		memcpy( pMsg->pMsg, pPkg, dwLen );
		pMsg->nMsgLen = dwLen;//初始有效信息长度；
		pMsg->nCurPos = 0;//读信息起始位置；
		BOOL isNeedClose = FALSE;
		BOOL isok = m_pSocket->SendReq( pMsg, isNeedClose );

		if ( isNeedClose )
		{
			ResetAll();
			return MUX_CLI_NORMALL_ERR;
		}

		if ( isok )
		{
			return MUX_CLI_SUCESS;
		} else {
			return MUX_CLI_NORMALL_ERR;
		}
	}

	/////进行心跳检测；
	//void CMuxCliNetMod::CheckTimeCheck() 
	//{ 
	//	if ( GetTickCount() - m_dwLastCheckTime < 1000 )
	//	{
	//		//每5秒发一次心跳检测包；
	//		return;
	//	}
	//	m_dwLastCheckTime = GetTickCount();
	//	CGTimeCheck timeCheck;
	//	++m_dwLastCheckID;
	//	timeCheck.checkID = m_dwLastCheckID;
	//	SendPkg( CGTimeCheck, timeCheck );
	//	//CONSOLE_DEBUG( "CheckTimeCheck,向SRV发送心跳消息%d", timeCheck.checkID );
	//	//LogErr( LOGERR_LEV_DEBUG, "CheckTimeCheck,向SRV发送心跳消息%d", timeCheck.checkID );
	//};
	/////收到心跳检测回复包；
	//void CMuxCliNetMod::OnTimeCheckRcved( unsigned long checkID ) 
	//{
	//	if ( checkID != m_dwLastCheckTime )
	//	{
	//		//对应包发出已经超过了5秒；
	//		m_nNetBusyStat = 100;
	//	}
	//	unsigned long dwPassedTime = GetTickCount() - m_dwLastCheckTime;
	//	if ( dwPassedTime > 600 )
	//	{
	//		m_nNetBusyStat = 100;
	//	} else {
	//		m_nNetBusyStat = (int) ( dwPassedTime / 6 );
	//	}
	//	return;
	//};


	//真正处理收包，由ProcessNetEvent内部回调；
	int CMuxCliNetMod::RealProcessRcvedPkg( unsigned short wCmd, const char* pPkg, const DWORD dwLen )
	{
		if ( NULL != m_pMsgRcver )
		{
			//首先处理网络模块可以处理的消息
			if ( wCmd == G_C_RANDOM_STRING )
			{
				TRY_BEGIN;

				if ( ( NULL == GetAccount() )
					|| ( NULL == m_tmpPassword )
					)
				{
					LogErr( LOGERR_LEV_ERR, "RealProcessRcvedPkg，处理random失败" );
					CONSOLE_DEBUG( "RealProcessRcvedPkg，处理random失败" );
					return 0;
				}

				//服务器发来md5校验字符串；
				DealPlayerPkgPre( GCRandomString, randomString, pPkg, dwLen );
				char md5val[16];
				memset( md5val, 0, sizeof(md5val) );

				if ( m_tmpPasswordLen < 1 )
				{
					LogErr( LOGERR_LEV_ERR, "RealProcessRcvedPkg，储存的密码错误" );
					CONSOLE_DEBUG( "RealProcessRcvedPkg，储存的密码错误" );
					return 0;
				}

				for ( int i=0; i<m_tmpPasswordLen; ++i )
				{
					m_tmpPassword[i] -= (67 + i*2);//临时解开真正的密码；
				}

				build_md5( GetAccount(), m_tmpPassword, randomString->randomString, randomString->rdStrSize, md5val );//计算md5值;
#ifdef USE_CRYPT
				if ( NULL != g_rds )
				{
					CONSOLE_DEBUG( "重复生成g_rds" );
					return 0;
				}
				g_rds = NEW char[16];
				build_md5( GetAccount(), m_tmpPassword, randomString->randomString2, randomString->rdStrSize2, g_rds );//计算md5值;

				//设置接收用密钥，发送密钥由发送线程设置；
				if ( NULL == g_rds )
				{
					CONSOLE_DEBUG( "设置接收加密，无可用密钥" );
					return false;
				}				
				//CONSOLE_DEBUG( "设置密钥--" );
				//for ( int i=0; i<16; ++i )
				//{
				//	CONSOLE_DEBUG( "key%d:%d ", i, g_rds[i] );
				//}
#endif //USE_CRYPT
				//登录密码使用完毕，删去；
				if ( NULL != m_tmpPassword )
				{
					memset( m_tmpPassword, 0, m_tmpPasswordLen );
					delete [] m_tmpPassword; m_tmpPassword = NULL;
				}

#ifdef USE_CRYPT
				m_rcvCrypt.SetRcvKey( g_rds, 16 );
#endif //USE_CRYPT

				CGUserMD5Value userMd5Value;//md5值返回服务器去验证；
				memset( &userMd5Value, 0, sizeof(userMd5Value) );
				userMd5Value.md5ValueSize = 16;
				if ( sizeof(userMd5Value.md5Value) < userMd5Value.md5ValueSize )
				{
					LogErr( LOGERR_LEV_ERR, "RealProcessRcvedPkg，md5值超出消息包可容纳大小" );
					return 0;
				}
				memcpy_s( userMd5Value.md5Value, sizeof(userMd5Value.md5Value), md5val, userMd5Value.md5ValueSize );
				SendPkg( CGUserMD5Value, userMd5Value );				
				TRY_END;
				return 0;
			} else if ( wCmd == G_C_PLAYER_LOGIN ){				
				//登录结果消息；
				DealPlayerPkgPre( GCPlayerLogin, playerLogin, pPkg, dwLen );
#ifdef USE_CRYPT
				if ( !CliStSendCrypt() )
				{
					LogErr( LOGERR_LEV_ERR, "RealProcessRcvedPkg，设置发送加密开始失败" );
					CONSOLE_DEBUG( "RealProcessRcvedPkg，设置发送加密开始失败" );
				}
#endif //USE_CRYPT
    			m_pMsgRcver->OnLoginRes( playerLogin->byIsLoginOK );
				return 0;
			} 
			else if ( wCmd == G_C_TIME_CHECK) {
				////心跳检测；
				DealPlayerPkgPre( GCTimeCheck, timeCheck, pPkg, dwLen );

				//OnTimeCheckRcved( timeCheck->checkID );
				unsigned long dwPassedTime = GetTickCount() - timeCheck->sequenceID;
				if ( dwPassedTime > 600 )
				{
					m_nNetBusyStat = 100;
				} else {
					m_nNetBusyStat = (int) ( dwPassedTime / 6 );
				}
				CONSOLE_DEBUG( "网络状况%d,ms值:%d"
					, m_nNetBusyStat, dwPassedTime );
				return 0;
			} 
			else if ( wCmd == G_C_NET_DETECT ){
				DealPlayerPkgPre( GCNetDetect, netDetect, pPkg, dwLen );
				
				if ( netDetect->pkgID != m_vecDetectPkgRcvTime.size() )
				{
					LogErr( LOGERR_LEV_ERR, "服务器端发来的GCNetDetect包编号%d顺序错误", netDetect->pkgID );
					return 0;
				}
				m_vecDetectPkgRcvTime.push_back( GetTickCount() );

				CGNetDetect resDetect;//回复服务器该查询包；
				resDetect.pkgID = netDetect->pkgID;
				SendPkg( CGNetDetect, resDetect );				
				return 0;
			} else if ( wCmd == G_C_NET_BIAS ){
				DealPlayerPkgPre( GCNetBias, netBias, pPkg, dwLen );
				
				if ( netBias->pkgID >= (int)(m_vecDetectPkgRcvTime.size()) )
				{
					LogErr( LOGERR_LEV_ERR, "服务器端发来的netBias包编号%d错误", netBias->pkgID );
					return 0;
				}
				m_dwNetBias = m_vecDetectPkgRcvTime[netBias->pkgID];//与服务器商定的基准时刻；
				m_dwNetTime = netBias->BiasTime;//与服务器商定的日期时间
				g_TimeBias = m_dwNetBias;//全局基准时刻，用于网络发送线程；
				m_bIsNetBiasAquired = true;//是否已与服务器商定基准时刻；
				CONSOLE_DEBUG( "与服务器商定基准时刻%d", g_TimeBias );
				return 0;
			}else if( wCmd == G_C_SRV_SYNCHTIME ) 
			{
				DealPlayerPkgPre(GCSrvSynchTime, netBias, pPkg, dwLen  );

				CGClinetSynchTime synchTime;
				synchTime.timeSynchID = netBias->timeSynchID;
				SendPkg(CGClinetSynchTime ,synchTime );
				return 0;
			}else if( wCmd == G_C_SYNCH_CLIENTTIME )
			{
				DealPlayerPkgPre(GCSynchClientTime, netBias, pPkg, dwLen  );
#ifdef _DEBUG
				LogErr( LOGERR_LEV_DEBUG, "接受到服务器传来的延迟时间 : Time:%s %s delay:%d \n", netBias->srvTime,netBias->srvTime+9,netBias->delayTime );
#endif
				//SetSynchTimeInfo( netBias->delayTime,netBias->srvTime );
				return 0;
			}else {
				//其它消息，转m_pMsgRcver处理；
				return m_pMsgRcver->OnPkgRcved( wCmd, pPkg, dwLen );
			}			
		} else {
			return 0;
		}
	}

	//客户端必须在网络模块初始化成功后循环调用本函数以处理网络事件;
	int CMuxCliNetMod::ProcessNetEvent()
	{
		if ( NULL == m_pSocket )
		{
			return 0;
		}

		if ( m_pSocket->IsDsSocketConnecting() )
		{
			BOOL isNeedClose = FALSE;
			m_pSocket->MuxCliConnect( isNeedClose );//连接中。。。，继续连接直至成功或失败；
			if ( isNeedClose )
			{
				ResetAll();
			}
			return 0;
		}
		if ( !(m_pSocket->IsDsSocketConnected()) )
		{
			//尚未连接；
			return 0;
		}
		//CONSOLE_DEBUG( "CLI循环：ProcessNetEvent被调用" );
		char* pPkg = NULL;
		unsigned short wPkgSize = 0;
		if ( NULL == m_pSocket )
		{
			return -1;
		}

		if ( m_bIsReset )
		{
			ResetAll();
			return -1;
		}

		//CheckTimeCheck();//心跳检测；

		if ( NULL == m_pSocket )
		{
			//在上面计算客户端当前时间过程中，可能会检测到socket错，从而删去m_pSocket，导致后续访问出错；
			return -1;
		}

		BOOL isSocketNeedClose = FALSE;		
		while ( m_pSocket->GetRcvedPkg( pPkg, wPkgSize, isSocketNeedClose ) )
		{
			if ( isSocketNeedClose )
			{
				ResetAll();
				return 0;
			}

			if ( wPkgSize < 3 )
			{
				LogErr( LOGERR_LEV_ERR, "包长小于3，不足一个包头，位置CMuxCliNetMod::ProcessNetEvent" );
			    return -1;
				//__asm int 3
			}

#ifdef USE_CRYPT
			if ( sizeof(StEncDec) == wPkgSize + sizeof(unsigned short) ) //因为GetRcvedPkg上递的包去除了包首的长度字段；
			{
				if ( !(m_rcvCrypt.IsStRcvCrypt()) )
				{
					//尚未开始收包加密，检测是否收包加密标志；
					StEncDec tmpRcved;
					memcpy( ((char*)&tmpRcved)+2, pPkg, sizeof(tmpRcved)-2 );
					if ( tmpRcved.IsValidStEncDec() )
					{
						//往后收包为加密包；
						CONSOLE_DEBUG( "收到加密开始标记,%d:%d", wPkgSize, wPkgSize+sizeof(unsigned short) );
						//1、设置加密开始
						m_rcvCrypt.StRcvCrypt();
						return 0;
					}
				}
			}

			unsigned int dumptimeinfo = 0;//无用的时间信息，因为srv过来的消息中不会包含此字段；
			unsigned int decedlen = 0;
			char* pDecedPkg = NULL;
			if ( ! m_rcvCrypt.DecryptRcvPkg( (const unsigned char*)pPkg, wPkgSize, (unsigned char*)m_deced, DEC_ENC_BUFSIZE, (unsigned char*&)pDecedPkg, decedlen, dumptimeinfo ) )
			{
				CONSOLE_DEBUG( "收包解密错误，当前收包解密状态%d", (m_rcvCrypt.IsStRcvCrypt() ? 1:0) );
				LogErr( LOGERR_LEV_DEBUG, "收包解密错误，当前收包解密状态%d", (m_rcvCrypt.IsStRcvCrypt() ? 1:0) );
				ResetAll();//断开退出；
				return 0;
			}
			unsigned short decedPkgLen = decedlen;

			if ( m_rcvCrypt.IsStRcvCrypt() )
			{
				CONSOLE_DEBUG( "收到加密包,%d:%d", wPkgSize, decedlen );
			} else {
				CONSOLE_DEBUG( "收到明文包,%d:%d", wPkgSize, decedlen );
			}

#else  //USE_CRYPT

			char* pDecedPkg = pPkg;
			unsigned short decedPkgLen = wPkgSize;

#endif //USE_CRYPT

			if ( ( NULL == pDecedPkg )
				|| ( decedPkgLen <=0 )
				)
			{
				LogErr( LOGERR_LEV_ERR, "解包错误2!" );
				return 0;
			}

			//处理所有的收包
#pragma pack(push, 1)
			struct StrRcvedPkg
			{
				unsigned char byPkgType;
		        unsigned short wCmd;
			};
#pragma pack(pop)
			struct StrRcvedPkg* pPkgHead = (StrRcvedPkg*) pDecedPkg;
#ifdef CHECK_ENDIAN
			if ( g_isBigEndian )
			{
				pPkgHead->wCmd = ENDIAN_CHANGE_S(pPkgHead->wCmd);
			}
#endif //CHECK_ENDIAN

#ifdef PKG_STAT
		if ( pkgStat.empty() )
		{
			startTime = GetTickCount();//起始统计时刻；
		}
		if ( pkgStat.end() == pkgStat.find( pPkgHead->wCmd ) )
		{
			struct PkgStatInfo* newPkgStat = new PkgStatInfo;
			newPkgStat->pkgCmd = pPkgHead->wCmd;
			newPkgStat->pkgSize = decedPkgLen;
			newPkgStat->pkgNum = 0;
			pkgStat.insert( pair<int, struct PkgStatInfo*>( pPkgHead->wCmd, newPkgStat ) );
		}
		((struct PkgStatInfo*)((pkgStat.find( pPkgHead->wCmd ))->second))->pkgNum ++;//收包数+1;
#endif //PKG_STAT

#ifdef OPEN_CONSOLE
			if ( 0x1002 == pPkgHead->wCmd )
			{
				CONSOLE_DEBUG( "CLI循环：向应用递送SRV聊天消息" );
#ifdef PKG_STAT
				LogErr( LOGERR_LEV_ERR, "//////////////////////////////////////////////////////////////" );
				unsigned int passedmsec = GetTickCount()-startTime;
				LogErr( LOGERR_LEV_ERR, "cli收包统计..., 区间起始时刻:%d(msec),当前时刻:%d(msec),区间用时:%d(msec)", startTime, GetTickCount(), passedmsec );
				LogErr( LOGERR_LEV_ERR, "命令字	|	包大小	|	包数量	|	总大小	|	bits/秒");
				PkgStatInfo* tmpPkgStat = NULL;
				unsigned int totalbits = 0;
				for ( map<unsigned int, struct PkgStatInfo*>::iterator iter=pkgStat.begin(); iter!=pkgStat.end(); ++iter )
				{
					tmpPkgStat = iter->second;
					totalbits = tmpPkgStat->pkgSize*tmpPkgStat->pkgNum*8;
					LogErr( LOGERR_LEV_ERR, "%x	|	%d	|	%d	|	%d	|	%f", tmpPkgStat->pkgCmd, tmpPkgStat->pkgSize, tmpPkgStat->pkgNum, totalbits, (float)totalbits/(passedmsec/1000) );
				}
				LogErr( LOGERR_LEV_ERR, "...cli收包统计结束", startTime, GetTickCount(), GetTickCount()-startTime   );
				LogErr( LOGERR_LEV_ERR, "//////////////////////////////////////////////////////////////" );
#endif //PKG_STAT
			} else if ( 0x1018 == pPkgHead->wCmd ) {
				//CONSOLE_DEBUG( "CLI循环：向应用递送SRV攻击响应消息" );
				//LogErr( LOGERR_LEV_DEBUG, "CLI循环：向应用递送SRV攻击响应消息" );
			} else if ( GCTimeCheck::wCmd == pPkgHead->wCmd ) {
				//CONSOLE_DEBUG( "CLI循环：向应用递送SRV心跳响应" );
				//LogErr( LOGERR_LEV_DEBUG, "CLI循环：向应用递送SRV心跳响应" );
			} else if ( GCSwitchMap::wCmd == pPkgHead->wCmd ) {
//				CONSOLE_DEBUG( "CLI循环：收到GCSwitchMap" );
			} else if ( 0x100e == pPkgHead->wCmd ) {
				CONSOLE_DEBUG( "CLI循环：收到角色信息" );
			}
#endif //OPEN_CONSOLE
			RealProcessRcvedPkg( pPkgHead->wCmd, pDecedPkg+3, decedPkgLen-3/*byPkgType+wCmd*/ );
			if ( NULL == m_pSocket )
			{
				//防止循环中检测到错误删去m_pSocket的情况；
				return -1;
			}
		}

		if ( isSocketNeedClose )
		{
			ResetAll();
			return 0;
		}

		return 0;
	}

	///重置网络模块；
	void CMuxCliNetMod::ResetAll()
	{
		if ( NULL != m_pSocket )
		{
			m_pSocket->PreDesturct();
			//Sleep( 1000 );
			delete m_pSocket; m_pSocket = NULL;
		}

		m_bIsInited = FALSE;		
		m_bIsConnected = FALSE;
		m_lSrvAddr = 0;
		m_dwPort = 0;
		m_pMsgRcver = NULL;//本地只是引用，因此不能删除；

#ifdef USE_CRYPT
		if ( NULL != g_rds )
		{
			delete [] g_rds; g_rds = NULL;
		}
		if ( NULL != m_todec )
		{
			delete [] m_todec; m_todec = NULL;
		}
		if ( NULL != m_deced )
		{
			delete [] m_deced; m_deced = NULL;
		}
#endif //USE_CRYPT

		////////////////////////uninit/////////////////////////////////////
		memset( m_strAccount, 0, sizeof(m_strAccount) );
		//memset( m_strPassword, 0, sizeof(m_strPassword) );
		if ( NULL != m_tmpPassword )
		{
			memset( m_tmpPassword, 0, m_tmpPasswordLen );
			delete [] m_tmpPassword; m_tmpPassword = NULL;
		}
		//////////////////////////////////////////////////////////////////////////////////////////
		///基准时刻协商...
		m_vecDetectPkgRcvTime.clear();
		m_dwNetBias = GetTickCount();//与服务器商定的基准时刻；
		g_TimeBias = 0;
		m_bIsNetBiasAquired = false;//是否已与服务器商定基准时刻；
		///...基准时刻协商；
		//////////////////////////////////////////////////////////////////////////////////////////

		m_nNetBusyStat = -1;//初始值；
		m_dwLastCheckID = 0;
		m_dwLastCheckTime = 0;

		m_bIsConnected = FALSE;

		m_bIsReset = FALSE;

		/*
		//CONSOLE_DEBUG( "初始化debug窗口" );
		//LogErr( LOGERR_LEV_DEBUG, "初始化debug窗口" );
		md5::init();

		if ( NULL == g_poolMsgToPut )
		{
		    g_poolMsgToPut = NEW TCache< MsgToPut >( 3 );//全局MsgToPut对象池；
		}

		volatile static long isoverlap = 0;
		if ( m_bIsInited )
		{
			return MUX_CLI_REOPERA;
		}
		if ( InterlockedExchange( &isoverlap, 1 ) )
		{
			return MUX_CLI_INPROCESS;
		}
		if ( m_bIsInited )//双锁，防止重复进入；
		{
			return MUX_CLI_REOPERA;
		}

		if ( pMsgRcver == NULL )
		{
			//输入sinker空；
			return MUX_CLI_NORMALL_ERR;
		}

		//关联sinker与netmod;
		m_pMsgRcver = pMsgRcver;

		m_bIsConnected = FALSE;

		m_lSrvAddr = inet_addr( assignSrvIp );
		m_dwPort = assignSrvPort;
		if ( ( INADDR_NONE == m_lSrvAddr ) || ( 0 == m_lSrvAddr ) )
		{
			//输入地址不正确，无法转换；
			memset( m_strSrvAddr, 0, sizeof(m_strSrvAddr) );
			return MUX_CLI_NORMALL_ERR;
		} else {
			strcpy_s( m_strSrvAddr, sizeof(m_strSrvAddr), assignSrvIp ); 
		}

		InterlockedExchange( &isoverlap, 0 );
		*/

		return;
	}

	///对应InitMuxCliNetMod的反初始化；
	void CMuxCliNetMod::UninitMuxCliNetMod()
	{
		ResetAll();
		return;
	}

	///释放客户端网络模块；
	void CMuxCliNetMod::ReleaseMuxCliNetMod()
	{
		ResetAll();
		OutPutAllLog();//强制输出所有日志；
		if ( NULL != g_poolMsgToPut )
		{
		    delete g_poolMsgToPut;//全局MsgToPut对象池；
			g_poolMsgToPut = NULL;
		}
		delete this;
	}

	///向服务器请求区服务器列表；
	int CMuxCliNetMod::GetSrvList( unsigned short wRegion )
	{
		//目前直接返回客户端；
		ACSrvOption srvOption0;
		ACSrvOption srvOption1;
		ACSrvOption srvOption2;
		srvOption0.byFlag = 1;
		srvOption0.wSrvNo = 0;
		srvOption0.wPort = m_dwPort;
		strcpy_s( srvOption0.srvName, sizeof(srvOption0.srvName), "网通:测试服务器0" );//0结尾服务器名（例如：网通一区：燕赵雄风）
		srvOption0.byOverload = 0;//负载标识（数字0-100,数字越小负载越轻)
		strcpy_s( srvOption0.strAddr , sizeof(srvOption0.strAddr), m_strSrvAddr);

		srvOption1.byFlag = 1;
		srvOption1.wSrvNo = 1;
		srvOption1.wPort = m_dwPort;
		strcpy_s( srvOption1.srvName, sizeof(srvOption1.srvName), "电信:测试服务器1" );//0结尾服务器名（例如：网通一区：燕赵雄风）
		srvOption1.byOverload = 100;//负载标识（数字0-100,数字越小负载越轻)
		strcpy_s( srvOption1.strAddr , sizeof(srvOption1.strAddr), m_strSrvAddr);

		memset( &srvOption2, 0, sizeof(srvOption2) );
		srvOption2.byFlag = 0;//列表结束消息；
		if ( NULL != m_pMsgRcver )
		{
			m_pMsgRcver->OnPkgRcved( A_C_SRV_OPTION, (char*)(&srvOption0), sizeof(srvOption0) );//直接返回两个列表给客户端；
			m_pMsgRcver->OnPkgRcved( A_C_SRV_OPTION, (char*)(&srvOption1), sizeof(srvOption1) );//直接返回两个列表给客户端；
			m_pMsgRcver->OnPkgRcved( A_C_SRV_OPTION, (char*)(&srvOption2), sizeof(srvOption2) );
		}
		return MUX_CLI_SUCESS;
	}
	///客户端选择服务器；
	int CMuxCliNetMod::SelectSrv( unsigned short wSrvNo )
	{
		return MUX_CLI_SUCESS;
	}
	///客户端选角色；
	int CMuxCliNetMod::SelectRole( unsigned int uiRoleNo )
	{
		CGSelectRole selRole;
		selRole.uiRoleNo = uiRoleNo;
		SendPkg( CGSelectRole, selRole );

		return MUX_CLI_SUCESS;
	}
    ///客户端发送进入世界消息（游戏开始，进入场景）；
	int CMuxCliNetMod::EnterWorld()
	{
		CGEnterWorld enterWorld;
		SendPkg( CGEnterWorld, enterWorld );
		return MUX_CLI_SUCESS;
	}

	///连接远端地址；
	int CMuxCliNetMod::ConnectIssue()
	{
		if ( InitDsSocketUser() )
		{
			BOOL isNeedClose = FALSE;
			m_pSocket->IssueConnectRemoteAddr( m_strSrvAddr, m_dwPort, isNeedClose );///连接远程机器发起;
			if ( isNeedClose )
			{
				ResetAll();
			}
		}
		return 0;
	}

	///连接成功开始登录流程；
	int CMuxCliNetMod::StLoginReq()
	{
#ifdef ENCRT_LOGIN //加密登录；
		CGRequestRandomString msgReqRdstr;
		memset( &msgReqRdstr, 0, sizeof(msgReqRdstr) );
		strcpy_s( msgReqRdstr.strUserAccount, sizeof(msgReqRdstr.strUserAccount), GetAccount() );
		msgReqRdstr.accountSize = (unsigned int) (strlen(msgReqRdstr.strUserAccount) + 1);
		SendPkg( CGRequestRandomString, msgReqRdstr );

#else //ENCRT_LOGIN
		CGLogin msgLogin;
		strcpy_s( msgLogin.strUserAccount, sizeof(msgLogin.strUserAccount), userId );
		strcpy_s( msgLogin.strUserPassword, sizeof(msgLogin.strUserPassword), userPassword );

		// 使用协议工具2008.7.15
		msgLogin.accountSize = (UINT)strlen(userId)+1;
		msgLogin.passwdSize = (UINT)strlen(userPassword)+1;

		SendPkg( CGLogin, msgLogin );
#endif //ENCRT_LOGIN
		return 0;
	}

	///异步，发登录请求，登录结果在OnLoginRes中返回；
	int CMuxCliNetMod::LoginReq( const char* userId, const char* userPassword )
	{
		//保存帐号与密码
		if ( !SetAccount( userId ) )
		{
			LogErr( LOGERR_LEV_ERR, "LoginReq,设置帐号%s失败", userId );
			return MUX_CLI_NORMALL_ERR;
		}
		if ( !SetPassword( userPassword ) )
		{
			LogErr( LOGERR_LEV_ERR, "LoginReq,设置密码%s失败", userPassword );
			return MUX_CLI_NORMALL_ERR;
		}

		ConnectIssue();

		return MUX_CLI_INPROGRESS;
	}

	///异步，查询帐号是否有人使用；
	int CMuxCliNetMod::TestAccountsReq( const char* accountsTest )
	{
		return MUX_CLI_SUCESS;
	}

	///异步，创建帐号请求；
	int CMuxCliNetMod::CreateAccountsReq( const char* userId, const char* userPassword )
	{
		return MUX_CLI_SUCESS;
	}

	time_t CMuxCliNetMod::Time()
	{
		if ( m_bIsNetBiasAquired )
		{
			return (time_t)( m_dwNetTime + GetConnPassedMsec()/1000 );
		}
		return time(NULL);
	}

	void CMuxCliNetMod::GetCurDateTime(unsigned int& year, unsigned int& month, unsigned int& day, unsigned int& hour, unsigned int& minute, unsigned int& second )
	{
		if ( m_bIsNetBiasAquired )
		{
			time_t curtime   =  Time();
			struct tm* local =  localtime( &curtime );
			year  =  1900 + local->tm_year;
			month =  local->tm_mon + 1;
			day   =  local->tm_mday;
			hour   =  local->tm_hour;
			minute =  local->tm_min;
			second =  local->tm_sec;
		}
		else
		{
			year = month = day = hour = minute = second = 0;
		}
	}

}//namespace MUX_CLIMOD