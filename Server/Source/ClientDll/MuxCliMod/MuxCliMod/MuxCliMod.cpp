﻿// MuxCliMod.cpp : 定义 DLL 应用程序的入口点。
//

#include "stdafx.h"
#include "dsnet/msgtoput.h"

#ifdef _MANAGED
#pragma managed(push, off)
#endif

HMODULE g_hModule;

TCache< MsgToPut >* g_poolMsgToPut = NULL;

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	g_hModule = hModule;
    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

