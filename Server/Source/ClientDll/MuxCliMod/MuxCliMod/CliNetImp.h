﻿/**
* @file clinetimp.h
* @brief 客户端通信模块实现头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称:clinetimp.h
* 摘    要:本头文件用于DLL，实现客户端通信模块接口
* 作    者:dzj
* 完成日期:2007.11.09
*/

#ifndef MUX_CLIMOD_IMPLEMENT
#define MUX_CLIMOD_IMPLEMENT

#include "stdafx.h"
#include "..\..\LocalTest\CliNet.h"
#include "dsnet\dsmuxsocket.h"
#include "PkgProc\CliProtocol.h"

#ifdef USE_CRYPT
#include "crypt/dscryptbf.h"
#endif //USE_CRYPT

#include "utility/consoledbg.h"

#define	MAX_NAME_SIZE    	 	20  	//最大名字长段

#ifdef USE_CRYPT
extern char* g_rds;//通信用cipher;
#endif //USE_CRYPT

namespace MUX_CLIMOD
{
	///网络模块实现；
	class CMuxCliNetMod : public IMuxCliNetMod, public IDsSocketSinkBase
	{
	public:
		CMuxCliNetMod() 
			: m_pSocket(NULL), m_bIsInited(FALSE), m_bIsConnected(FALSE), m_lSrvAddr(0), m_dwPort(0)
			, m_pMsgRcver(NULL), m_bIsReset(FALSE),m_tmpPassword(NULL), m_tmpPasswordLen(0)
#ifdef USE_CRYPT
			, m_todec(NULL), m_deced(NULL)
#endif //USE_CRYPT
		{
		}
		//////////////////////////////////////////////////////////////////////////
		///IMuxCliNetMod虚函数实现；
	public:
		///初始化客户端网络模块，输入参数为回调对象、服务器IP地址与端口；
		virtual int InitMuxCliNetMod( IMuxCliNetSinker* pMsgRcver, const char* assignSrvIp, WORD assignSrvPort );
		///对应InitMuxCliNetMod的反初始化；
		void UninitMuxCliNetMod();
		///向服务器请求区服务器列表；
		virtual int GetSrvList( unsigned short wRegion );
		///客户端选择服务器；
		virtual int SelectSrv( unsigned short wSrvNo );
		///异步，发登录请求，登录结果在OnLoginRes中返回；
		virtual int LoginReq( const char* userId, const char* userPassword );
		///客户端选角色；
		virtual int SelectRole( unsigned int uiRoleNo );
        ///客户端发送进入世界消息（游戏开始，进入场景）；
		virtual int EnterWorld();

		///查询网络状况接口，08.03.03新加;		
		virtual int GetNetStat() { return m_nNetBusyStat; };//取网络繁忙状况，返回值[0-100],0：最空闲,100:最繁忙；

		//客户端必须在网络模块初始化成功后循环调用本函数以处理网络事件;
		virtual int ProcessNetEvent();
		///释放客户端网络模块；
		virtual void ReleaseMuxCliNetMod();

		//获取当前的时间
		virtual time_t Time();
		//获取现在的时间，年月日分秒
		virtual void   GetCurDateTime( unsigned int& year, unsigned int& month, unsigned int& day, unsigned int& hour, unsigned int& minute, unsigned int& second );

	public://以下为发消息接口;
		///异步，查询帐号是否有人使用；
		virtual int TestAccountsReq( const char* accountsTest );
		///异步，创建帐号请求；
		virtual int CreateAccountsReq( const char* userId, const char* userPassword );
		virtual int CreateRole() {return MUX_CLI_SUCESS;};
		virtual int DeleteRole() {return MUX_CLI_SUCESS;};
		//SOCKET出错，主线程执行网络处理时回调；
		virtual void OnDsSocketError( DWORD errNO )
		{
			if ( NULL != m_pMsgRcver )
			{
				m_pMsgRcver->OnError( errNO, 0 );
			}
			return ;
		}
		///异步，向服务器发消息；
		virtual int SendPkgReq( const DWORD dwPkgID, const char* pPkg, const DWORD dwLen ); 

		///返回距离商定时刻过去了多久(单位毫秒)，如果当前还未商定，则返回0；
		virtual unsigned long GetConnPassedMsec()
		{
			if ( m_bIsNetBiasAquired )
			{
				return (::GetTickCount() - m_dwNetBias);
			} else {
				return 0;
			}
		}

	public:
		///连接远端地址；
		int ConnectIssue();
		///连接成功开始登录流程；
		int StLoginReq();

	public:
		///开启或关闭调试控制台窗口，第一次开启，第二次关闭；
		virtual void DbgConsole( const char* consoleLogFile=NULL/*是否同时将调试窗口信息输送至文件*/ ) {};		
		///发给调试控制台窗口的命令；
		/*
		   monsend msgID [-c]//将指定类型消息添加到发送监视列表,第一次执行开启监视，第二次执行关闭， -c打出消息各字段内容；
		   monrcv  msgID [-c]//将指定类型消息添加到接收监视列表,第一次执行开启监视，第二次执行关闭， -c打出消息各字段内容；
		   simsend msgID ... //模拟客户端向服务器发送指定类型消息,...为消息各字段内容。
		   simrcv  msgID ... //模拟服务器向客户端发送指定类型消息,...为消息各字段内容。
		*/
		virtual void DbgConsoleCmd( const char* ctrlCmd, ... ) {};
		///IMuxCliNetMod虚函数实现；
		//////////////////////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////////////////////
		///IDsSocketSinkBase虚函数实现；
	public:
		///连接建立；
		virtual void OnConnected( BOOL isSuc ) 
		{
			if ( isSuc )
			{
				m_bIsConnected = TRUE;
				StLoginReq();//开始登录流程；
				//m_pMsgRcver->OnConnected();
			} else {
				m_bIsConnected = FALSE;
				if ( NULL != m_pMsgRcver )
				{
					m_pMsgRcver->OnError( MUX_CLI_CONNECT_ERR, 0 );//通知sinker连接失败；
				}
				m_bIsReset = TRUE;
			}
		}; //连接建立；
		///连接断开；
		virtual void OnDisconnected() 
		{
			m_bIsConnected = FALSE;
			if ( NULL != m_pMsgRcver )
			{
				m_pMsgRcver->OnError( MUX_CLI_DISCONNECT, 0 );//通知sinker连接失败；
			}
			m_bIsReset = TRUE;
		};//连接断开；

		///发送完毕回调
		virtual void OnSent( BOOL isSuc ) {};
		///接收包回调
		virtual void OnRecved( const char* pBuf, INT nLen ) {};
		///IDsSocketSinkBase虚函数实现；
		//////////////////////////////////////////////////////////////////////////

		//真正处理收包，由ProcessNetEvent内部回调；
		int RealProcessRcvedPkg( unsigned short wCmd, const char* pPkg, const DWORD dwLen );
		//真正处理发包，由ProcessNetEvent内部回调；
		int RealProcessSendPkg( const DWORD dwPkgID, const char* pPkg, const DWORD dwLen ); 

	private:
		char          m_strSrvAddr[32];
		unsigned long m_lSrvAddr;//当前连接服务器地址;
		WORD          m_dwPort;//当前连接服务器端口;
		BOOL          m_bIsConnected;
		BOOL          m_bIsInited;

		vector<DWORD> m_vecDetectPkgRcvTime;//收到服务器端网络检测包的时刻；
		DWORD         m_dwNetBias;//与服务器商定的基准时刻；
		DWORD         m_dwNetTime;//与服务器商定的日期时间；
		bool          m_bIsNetBiasAquired;//是否已与服务器商定基准时刻；

	private:
		bool SetAccount( const char* inAccount )//置玩家帐号；
		{
			if ( NULL != inAccount )
			{
				if ( sizeof(m_strAccount) > strlen(inAccount) )
				{
					strcpy_s( m_strAccount, sizeof(m_strAccount), inAccount );
					return true;
				}
			}
			return false;
		}
		bool SetPassword( const char* inPassword )//置玩家密码；
		{
			if ( NULL != inPassword )
			{
				unsigned int needLen = (unsigned int)strlen(inPassword) + 1;
				if ( needLen > 32 )
				{
					return false;//密码不可能超过32位；
				}
				if ( NULL != m_tmpPassword )
				{
					memset( m_tmpPassword, 0, m_tmpPasswordLen );
					delete [] m_tmpPassword; m_tmpPassword = NULL;
				}
				m_tmpPassword = NEW char[needLen];				
				memset( m_tmpPassword, 0, needLen );

				m_tmpPasswordLen = (int)needLen;
				if ( m_tmpPasswordLen >= 1 )
				{
					for ( int i=0; i<m_tmpPasswordLen; ++i )
					{
						m_tmpPassword[i] += (67 + i*2 + inPassword[i]);
					}
				}
				return true;
			}
			return false;
		}

		char* GetAccount()//取玩家帐号；
		{
			return m_strAccount;
		}

		//char* GetPassword()//取玩家密码；
		//{
		//	//return m_strPassword;
		//	return m_tmpPassword;
		//}

	private:
		char          m_strAccount[MAX_NAME_SIZE];//玩家帐号；
		//char          m_strPassword[MAX_NAME_SIZE];//玩家密码;
		char*         m_tmpPassword;//为了降低被内存读取的可能性，每次设时临时分配，并在计算完密钥后立刻释放；
		int           m_tmpPasswordLen;//password长度；

	private:
		///建立网络模块执行者；
		int InitDsSocketUser();
		///重置网络模块；
		void ResetAll();
		/////进行心跳检测；
		//void CheckTimeCheck() ;
		/////收到心跳检测回复包；
		//void OnTimeCheckRcved( unsigned long checkID );

	private:
		unsigned long m_dwLastCheckID;
		unsigned long m_dwLastCheckTime;
		int m_nNetBusyStat; 
		///sinker;
		IMuxCliNetSinker* m_pMsgRcver;
		///通信socket；
		CMuxSocket* m_pSocket;

		BOOL m_bIsReset;

		// 使用协议工具2008.7.15
		CliProtocol m_clipro;

#ifdef USE_CRYPT
	private:
		///设置发送加密开始;
		bool CliStSendCrypt()
		{
			StEncDec tmpToSend;
#ifdef CHECK_ENDIAN
			if ( g_isBigEndian )
			{
				tmpToSend.pkgLen = ENDIAN_CHANGE_S(tmpToSend.pkgLen);
			}
#endif //CHECK_ENDIAN
			SendPkgReq( 0, (const char*)&tmpToSend, sizeof(tmpToSend) );
			return true;
		}
	private:
		DsCryptBF m_rcvCrypt;
#define DEC_ENC_BUFSIZE 1024
		char* m_todec;//暂存待解密消息
		char* m_deced;//存放解密后消息以便处理
#endif //USE_CRYPT

#ifdef PKG_STAT
		struct PkgStatInfo
		{
			unsigned int pkgCmd;//包命令字
			unsigned int pkgSize;//包大小
			unsigned int pkgNum;//包数量
		};
		map<unsigned int, struct PkgStatInfo*> pkgStat;
		unsigned int startTime;
#endif //PKG_STAT


	};
}

#endif //MUX_CLIMOD_IMPLEMENT