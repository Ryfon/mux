﻿
/*
  控制台调试窗口, from <<Windows Developer Journal.Adding Console I/O to a Win32 GUI App>>, 1997, Andrew Tucker,(ast@halcyon.com), by dzj, 09.01.15
*/

#ifndef CONSOLE_DBG_H
#define CONSOLE_DBG_H

#include <windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>

#ifndef _USE_OLD_IOSTREAMS
using namespace std;
#endif //_USE_OLD_IOSTREAMS

static const WORD MAX_CONSOLE_LINES = 200;

//#define OPEN_CONSOLE //开启控制台调试；

#ifdef OPEN_CONSOLE
#define CONSOLE_DEBUG( ... ) \
	printf( "%d", GetTickCount() );\
	printf( ">>>" __VA_ARGS__ ); \
	printf( "\n" );
#else  //OPEN_CONSOLE
#define CONSOLE_DEBUG( ... )
#endif //OPEN_CONSOLE

void RedirectIOToConsole();

#endif  //CONSOLE_DBG_H


