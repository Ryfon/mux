﻿
/*
  控制台调试窗口, from <<Windows Developer Journal.Adding Console I/O to a Win32 GUI App>>, 1997, Andrew Tucker,(ast@halcyon.com), by dzj, 09.01.15
*/

#include "stdafx.h"
#include "consoledbg.h"

void RedirectIOToConsole()
{
	int hConHandle = 0;
	long lStdHandle = 0;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	FILE* fp = NULL;

	FreeConsole();
	AllocConsole();

	GetConsoleScreenBufferInfo( GetStdHandle( STD_OUTPUT_HANDLE), &coninfo );
	coninfo.dwSize.Y = MAX_CONSOLE_LINES;
	SetConsoleScreenBufferSize( GetStdHandle( STD_OUTPUT_HANDLE), coninfo.dwSize );

	lStdHandle = (long) GetStdHandle( STD_OUTPUT_HANDLE );
	hConHandle = _open_osfhandle( lStdHandle, _O_TEXT );
	fp = _fdopen( hConHandle, "w" );
	*stdout = *fp;
	setvbuf( stdout, NULL, _IONBF, 0 );

	//stdin
	lStdHandle = (long) GetStdHandle( STD_INPUT_HANDLE );
	hConHandle = _open_osfhandle( lStdHandle, _O_TEXT );
	fp = _fdopen( hConHandle, "r" );
	*stdin = *fp;
	setvbuf( stdin, NULL, _IONBF, 0 );

	//stderror
	lStdHandle = (long) GetStdHandle( STD_ERROR_HANDLE );
	hConHandle = _open_osfhandle( lStdHandle, _O_TEXT );
	fp = _fdopen( hConHandle, "w" );
	*stderr = *fp;
	setvbuf( stderr, NULL, _IONBF, 0 );

	ios::sync_with_stdio();

}




