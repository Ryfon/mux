﻿/**
* @file utility.cpp
* @brief 杂项功能实现文件；
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称:utility.h
* 摘    要:杂项功能
* 作    者:dzj
* 修改日期:2007.11.09,部分注释方式修改；
*/
#include "stdafx.h"
#include "utility.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#ifdef DS_LOG
static CDsLog g_DsLog( ".", 1024*16 );//主缓存与后备缓存分别为16K与32K;
#endif //DS_LOG

static vector< char* > g_vecToLog;//所有待记录信息,单条信息最大长度1024,by dzj, 06.04.20;
static DWORD g_nToLog = 0;
static bool  g_bIsFirstLog = true;

//const INT g_nLogLevel = LOGERR_LEV_LOG; //LOG日志不打；
const INT g_nLogLevel = 0;//所有日志都打；

/**
* //由调用者确保内存有效不会越界,源字符串长度不要超过1024；
* //在pSrc中找到第一个sechar,以其为中心将pSrc分为两段，
* //前一段存放于pFirstSection中，后一段仍存回pSrc中，两段都不包括sechar，如果成功，返回TRUE，
* //by dzj, 06.07.04；
*/
BOOL SeperatePStr( char* pSrc, char* pFirstSection, const char& sechar )
//由调用者确保内存有效不会越界,源字符串长度不要超过1024；
//在pSrc中找到第一个sechar,以其为中心将pSrc分为两段，
//前一段存放于pFirstSection中，后一段仍存回pSrc中，两段都不包括sechar，如果成功，返回TRUE，
//by dzj, 06.07.04；
{
	if ( ( strlen(pSrc) <=0 )
		|| ( strlen(pSrc) >=1024 )
		)
	{
		return FALSE;
	}

	char* pStart = strchr( pSrc, sechar );
	if ( ( NULL == pStart )
		|| ( strlen(pStart+1)<=0 )//后半部分长度为0
		)
	{
		return FALSE;
	}

	strncpy( pFirstSection, pSrc, pStart-pSrc ); 
	pFirstSection[pStart-pSrc] = 0;
	char pSecSection[1024];
	strcpy( pSecSection, pStart+1 );//避免拷贝源与目标overlap;
	strcpy( pSrc, pSecSection );

	if ( strlen(pFirstSection)<=0 )//如果前一段长度为0;
	{
		return FALSE;
	}

	return TRUE;
}

#ifdef LOG_MEM_TEMP
void WriteVecLog()
//将g_vecToLog中的内容写至文件；
{
	TRY_BEGIN
		;

	SYSTEMTIME time;
	ZeroMemory(&time,sizeof(SYSTEMTIME));
	GetLocalTime(&time);

	//文件目录；
	char lpszCurDir[MAX_PATH] = {0};
	GetCurrentDirectory(MAX_PATH,lpszCurDir);
	strcat(lpszCurDir,"\\ErrorLog\\");

	if( !IsDirExist(lpszCurDir) )
	{
		if(CreateDirectory(lpszCurDir,NULL) == 0)	return;
	}

	sprintf(lpszCurDir,"%s%d-%.2d\\",lpszCurDir,time.wYear,time.wMonth);

	if( !IsDirExist(lpszCurDir) )
	{
		if(CreateDirectory(lpszCurDir,NULL) == 0)	return;
	}
	sprintf(lpszCurDir,"%s%.2d-%.2d-%.2d.log",lpszCurDir,time.wYear,time.wMonth,time.wDay);

	HANDLE hFileHandle;
	DWORD	dwPos,dwWriteNum;
	hFileHandle = CreateFile(lpszCurDir,GENERIC_WRITE,0,NULL,OPEN_ALWAYS,0,NULL);
	if(hFileHandle==INVALID_HANDLE_VALUE)
	{
		return;
	}
	dwPos = SetFilePointer(hFileHandle, 0, NULL, FILE_END); 
	LockFile(hFileHandle, dwPos, 0, dwPos + g_nToLog, 0); 

	TRY_BEGIN
		;

	char* lpszWriteBuf = NULL;
	vector< char* >::iterator infoiter;
	for ( infoiter=g_vecToLog.begin(); infoiter!=g_vecToLog.end(); ++infoiter )
	{
		lpszWriteBuf = *infoiter;
		WriteFile(hFileHandle, lpszWriteBuf, (DWORD) strlen(lpszWriteBuf), &dwWriteNum, NULL);
	}

	TRY_END
		;

	UnlockFile(hFileHandle, dwPos, 0, dwPos + g_nToLog, 0); 
	CloseHandle(hFileHandle);

	//清记录数组；
	char* toerase = NULL;
	while (false == g_vecToLog.empty())
	{
		toerase = g_vecToLog.back();
		g_vecToLog.pop_back();
		TRY_BEGIN
			;
		delete [] toerase;
		TRY_END
			;
	}

	g_vecToLog.clear();
	g_nToLog = 0;

	return;
	TRY_END
		;
	return;
}
#endif //LOG_MEM_TEMP

#ifdef DS_LOG
void OutPutAllLog()
{
	g_DsLog.ForceWriteLog();
	return;
}
#endif //DS_LOG

DWORD TestSEH()
{
	DWORD jjj = 0;

	__try{
		jjj = 1;
		//goto EARLY_RETURN;
		return jjj;
	} __finally {
		jjj = 2;
		//return jjj;
	}

	return jjj;

	jjj = 3;
	return jjj;
}


void LogErr( INT nLevel, const char *fmt, ...)
{
#ifdef CANCEL_LOG
	return;
#endif //CANCEL_LOG

	if ( ( nLevel <= g_nLogLevel )//普通日志不记
		&& ( LOGERR_LEV_DEBUG != nLevel ) //且不是调试日志；
		)
	{
		return;
	}

	TRY_BEGIN
		;

	//////////////////////////////////////////////////////////////////////////
	SYSTEMTIME time;
	ZeroMemory(&time,sizeof(SYSTEMTIME));
	GetLocalTime(&time);

	//////////////////////////////////////////////////////////////////////////
	char lpszBuf[10240] = {0};
	char lpszWriteBuf[10240] = {0};
	va_list	ap;
	va_start(ap, fmt);
	vsprintf(lpszBuf, fmt, ap);
	va_end(ap);

#ifdef DS_LOG
	sprintf(lpszWriteBuf,"[%.2d:%.2d:%.2d:%.3d]Error: %s\r\n",time.wHour,time.wMinute,time.wSecond,time.wMilliseconds,lpszBuf);
	g_DsLog.LogMsg( lpszWriteBuf );
	return;
#endif //DS_LOG

#ifdef LOG_MEM_TEMP

	if ( g_bIsFirstLog )
	{
		g_vecToLog.reserve( 1024 );//第一次记日志时预留空间；
		g_bIsFirstLog = false;
	}
	sprintf(lpszWriteBuf,"[%.2d:%.2d:%.2d:%.3d]Error: %s\r\n",time.wHour,time.wMinute,time.wSecond,time.wMilliseconds,lpszBuf);

	DWORD ncpylen = (DWORD)strlen(lpszWriteBuf);
	char* ptowrite = new char[ncpylen+1];
	//memcpy( ptowrite, lpszWriteBuf, ncpylen+1 );
	strcpy( ptowrite, lpszWriteBuf );
	g_vecToLog.push_back( ptowrite );
	g_nToLog += (INT) strlen( ptowrite );
	if ( g_vecToLog.size() < 1023 )//每1024条记录记一次；
	{
		goto THIS_RETURN;
	}

	WriteVecLog();

THIS_RETURN:
#endif //LOG_MEM_TEMP

	return;
	TRY_END
		;
	return;
}

bool IsDirExist(char *Dir)
{
	DWORD dwRet = GetFileAttributes(Dir);
	if(!((dwRet != 0xFFFFFFFF) && (dwRet & FILE_ATTRIBUTE_DIRECTORY)))
	{
		return false;
	}
	return true;
}

void Write2File(char *FileName,char *buf)
{	
	HANDLE hFileHandle;
	DWORD	dwPos,dwWriteNum;

 	hFileHandle = CreateFile(FileName,GENERIC_WRITE,0,NULL,OPEN_ALWAYS,0,NULL);
	if(hFileHandle==INVALID_HANDLE_VALUE)
	{
		return;
	}

	dwPos = SetFilePointer(hFileHandle, 0, NULL, FILE_END); 
	LockFile(hFileHandle, dwPos, 0, dwPos + strlen(buf), 0); 
	WriteFile(hFileHandle,buf,(DWORD) strlen(buf),&dwWriteNum,NULL);
	UnlockFile(hFileHandle, dwPos, 0, dwPos + strlen(buf), 0); 
	CloseHandle(hFileHandle);	 
}

void LogSelf(const char *lpszDir,const char *fmt, ...)
{
	char lpszCurDir[MAX_PATH] = {0};
	strcpy(lpszCurDir,lpszDir);

	if( !IsDirExist(lpszCurDir) )
	{
		if(CreateDirectory(lpszCurDir,NULL) == 0)	return;
	}

	SYSTEMTIME time;
	ZeroMemory(&time,sizeof(SYSTEMTIME));
	GetLocalTime(&time);

	sprintf(lpszCurDir,"%s\\%d-%.2d\\",lpszCurDir,time.wYear,time.wMonth);

	if( !IsDirExist(lpszCurDir) )
	{
		if(CreateDirectory(lpszCurDir,NULL) == 0)	return;
	}

	sprintf(lpszCurDir,"%s%.2d-%.2d-%.2d.log",lpszCurDir,time.wYear,time.wMonth,time.wDay);

	char lpszBuf[10240] = {0};
	va_list	ap;
	va_start(ap, fmt);
	vsprintf(lpszBuf, fmt, ap);
	va_end(ap);

	char lpszWriteBuf[10240] = {0};
	sprintf(lpszWriteBuf,"[%.2d:%.2d:%.2d]: %s\r\n",time.wHour,time.wMinute,time.wSecond,lpszBuf);
	Write2File(lpszCurDir,lpszWriteBuf);
}

const char * GetRootPath()
{
	static char szPath[MAX_PATH];
	static bool bFirstTime = true;

	if(bFirstTime)
	{
		bFirstTime = false;
		GetModuleFileName(NULL, szPath, sizeof(szPath));
		char *p = strrchr(szPath, '\\');
		*p = '\0';
	}

	return szPath;
}
