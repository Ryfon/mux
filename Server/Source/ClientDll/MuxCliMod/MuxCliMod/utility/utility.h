﻿/**
* @file utility.h
* @brief 杂项功能头文件；
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称:utility.h
* 摘    要:杂项功能
* 作    者:dzj
* 修改日期:2007.11.09,部分注释方式修改；
*/

#ifndef UTILITY_H
#define UTILITY_H
#include <windows.h>
#include <time.h>
#pragma warning(disable:4786)
#include <string>
#include <list>
#include <vector>

#include "dslog.h" //新日志方法；

using namespace std;

#define MAX_TIMELIMIT 100

#define ENCRT_LOGIN //加密登录；

#define DS_LOG

//#define D_TRY_USE

#ifdef D_TRY_USE

#define TRY_BEGIN	try {

#define TRY_END		}	\
					catch(exception e) \
					{\
						LogSelf("Exception","%s:%d %s",__FILE__,__LINE__,e.what()); \
					}\
					catch(...) \
					{ \
						LogSelf("Exception","%s:%d",__FILE__,__LINE__); \
					}

#else
	#define TRY_BEGIN
	#define TRY_END
#endif

//#define PKG_STAT //收包统计

#define USE_CRYPT    //使用加解密；

#ifdef USE_CRYPT
#pragma pack(push,1)
struct StEncDec{ //开启加解密的特殊信息；
	static const unsigned char CHECKBT1 = 7;
	static const unsigned char CHECKBT2 = 23;
	static const unsigned char CHECKBT3 = 97;
	static const unsigned char CHECKBT4 = 231;
public:
	StEncDec() : pkgLen(sizeof(StEncDec)), checkbt1(CHECKBT1), checkbt2(CHECKBT2), checkbt3(CHECKBT3), checkbt4(CHECKBT4) {};

public:
	inline bool IsValidStEncDec()
	{
		return ( (CHECKBT1 == checkbt1) && (CHECKBT2 == checkbt2) && (CHECKBT3 == checkbt3) && (CHECKBT4 == checkbt4) );
	}

public:
	unsigned short pkgLen;//包头(作为包发送时使用)；
	unsigned char checkbt1;
	unsigned char checkbt2;
	unsigned char checkbt3;
	unsigned char checkbt4;
};
#pragma pack(pop)
#endif //USE_CRYPT


///by dzj, 06.10.11;
#define DS_STR_CPYTO_ARR( dst, src ) \
	if ( (strlen(src)+1) < sizeof(dst) )\
	{\
		strcpy( dst, src );\
	}

#define RAND ((rand()<<15) + rand())

 ///根据概率分派任务, by dzj, 06.10.17；
#define DS_POSSI_DESPATCH_2( totalPossi, possi1, fun1, possi2, fun2 ) \
	{\
		if ( totalPossi > 0 ) \
		{\
			INT tmprst = RAND % totalPossi;\
			if ( tmprst < possi1 )\
			{\
				fun1();\
			} else if ( tmprst < possi1+possi2 ) {\
				fun2();\
			}\
		}\
	}

#define DS_POSSI_DESPATCH_3( totalPossi, possi1, fun1, possi2, fun2, possi3, fun3 ) \
	{\
		if ( totalPossi > 0 ) \
		{\
			INT tmprst = RAND % totalPossi;\
			if ( tmprst < possi1 )\
			{\
				fun1();\
			} else if ( tmprst < possi1+possi2 ) {\
				fun2();\
			} else if ( tmprst < possi1+possi2+possi3 ) {\
				fun3();\
			}\
		}\
	}

#define DS_POSSI_DESPATCH_4( totalPossi, possi1, fun1, possi2, fun2, possi3, fun3, possi4, fun4 ) \
	{\
		if ( totalPossi > 0 ) \
		{\
			INT tmprst = RAND % totalPossi;\
			if ( tmprst < possi1 )\
			{\
				fun1();\
			} else if ( tmprst < possi1+possi2 ) {\
				fun2();\
			} else if ( tmprst < possi1+possi2+possi3 ) {\
				fun3();\
			} else if ( tmprst < possi1+possi2+possi3+possi4 ) {\
				fun4();\
			}\
		}\
	}

const char * GetRootPath();


DWORD TestSEH();

///		判断指定目录lpszDir是否已存在
bool IsDirExist(char *Dir);

///		将字符串lpszBuf写到文件FileName中，如果文件不存在则创建文件,如果文件存在则写到文件末尾
void Write2File(char *FileName,char *lpszBuf);

///		在指定目录lpszDir下，按年月创建目录，在此目录中创建以年月日为文件名的日志，用于记录异常；
void LogSelf(const char *lpszDir,const char *fmt, ...);


#ifdef DS_LOG
///输出缓存的所有日志；
void OutPutAllLog();
#endif //DS_LOG

///错误；
const INT LOGERR_LEV_ERR = 30;//错误；
///警告；
const INT LOGERR_LEV_WARNING = 20;//警告；
///一般日志；
const INT LOGERR_LEV_NORMAL = 10;//一般日志；
///跟踪日志；
const INT LOGERR_LEV_LOG = 3;//跟踪日志；
///调试日志；
const INT LOGERR_LEV_DEBUG = 999;//调试日志；
///使用缓存日志类记日志
void LogErr( INT nLevel, const char *fmt, ...);

///将g_vecToLog中的内容写至文件, by dzj, 06.04.20；
void WriteVecLog();//将g_vecToLog中的内容写至文件, by dzj, 06.04.20；

///由调用者确保内存有效不会越界,源字符串长度不要超过1024；
///在pSrc中找到第一个sechar,以其为中心将pSrc分为两段，
///前一段存放于pFirstSection中，后一段仍存回pSrc中，两段都不包括sechar，如果成功，返回TRUE，
///by dzj, 06.07.04；
BOOL SeperatePStr( char* pSrc, char* pFirstSection, const char& sechar );//根据在pSrc中找到的第一个sechar,将pSrc分为两段，前一段存放于pResult中，后一段仍存回pSrc中，两段都不包括sechar，如果成功，返回TRUE，by dzj, 06.07.04；

#endif