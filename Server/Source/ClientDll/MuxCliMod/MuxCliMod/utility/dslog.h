﻿/**
* @file dslog.h
* @brief 日志类头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称:dslog.h
* 摘    要:本文件用于日志记录类；
* 作    者:dzj
* 修改日期:2007.11.09，部分注释方式修改；
* //////////////////////////////////////////////////////////////////////////
* //日志类，by dzj, 06.07.10;
* //初步想法：1、预先分配一整块内存，写日志过程中不重新分配释放内存;
* //			2、使用一个单独的写线程执行IO繁忙操作；
* //			3、使用内部写缓存，尽量减少写线程执行时，主线程等待日志缓存锁的时间（从等待IO操作完成改为等待到写缓存的内存拷贝操作完成）；
* //注：主缓存与后备缓存必须足够大以防止丢失在写另一部分缓存内容时到来的包；
* //	(实验表明：如BACKBUF_MULTI为2，则主缓存设为2k以上即可，这样后备缓存有4k，
* //		即使日志不间断到来，4k也足够储存将主缓存拷贝到写缓存这段时间内新到的日志)
* //////////////////////////////////////////////////////////////////////////
*/


#pragma once

class CDsLog;
class CDsStrWriteBuffer;

///互斥量封装；
class CDsMutex
{
public:
	CDsMutex() 
	{
		InitializeCriticalSection( &m_Mutex ); 
	};
	~CDsMutex() 
	{ 
		DeleteCriticalSection(&m_Mutex); 
	};
public:
	///加锁；
	void lock() { ::EnterCriticalSection( &m_Mutex ); };
	///解锁；
	void unlock() { ::LeaveCriticalSection( & m_Mutex);	};
public:
	///封装的内部互斥量；
	CRITICAL_SECTION m_Mutex;
};

///封装互斥量的加锁与解锁，构造时自动加锁，析构时自动解锁
template <typename T_Locker>
class CDsLock
{
public:
	CDsLock( T_Locker& inLocker ): m_Locker(inLocker) { m_Locker.lock(); };
	~CDsLock() { m_Locker.unlock(); };
private:
	CDsLock(){};

private:
	///模板参数传递，互斥量；
    T_Locker& m_Locker;
};

///日志写线程参数定义
typedef struct tagWriteThreadParam {
	CDsLog*		pDsLog;
	HANDLE      hWaitHandle;
	CDsStrWriteBuffer* pWriteBuffer;
} WriteThreadParam;

///日志写线程；
UINT WINAPI DsLogWriteThread( LPVOID lpParam );
///计算日志文件名；
void GetLogFileName( char* pFileName );
///指定目录是否存在；
bool IsDirExist(char *Dir);

#define BACKBUF_MULTI 2 //主缓存与后备缓存必须足够大以防止丢失在写另一部分缓存内容时到来的包；(实验表明：如BACKBUF_MULTI为2，则主缓存设为2k以上即可，这样后备缓存有4k，即使日志不间断到来，4k也足够储存将主缓存拷贝到写缓存这段时间内新到的日志)

///写缓存；
class CDsStrWriteBuffer
{
public:
	CDsStrWriteBuffer( DWORD dwSize ): m_dwSize(dwSize), m_dwCurPos(0), m_dwBackBufPos(0), m_pInnerBuffer(NEW char[dwSize]), m_pInnerBackBuffer(NEW char[dwSize*BACKBUF_MULTI]), m_pWriteBuffer(NEW char[dwSize*BACKBUF_MULTI]) {};
	~CDsStrWriteBuffer()
	{ 
		delete [] m_pInnerBuffer; 
		delete [] m_pInnerBackBuffer;
		delete [] m_pWriteBuffer;
	};

public:
	///向写缓存中添加内容；
    BOOL AddString( const char* pInString )
	{
		CDsLock<CDsMutex> tmpLock( m_bufLock );

		DWORD insize = (DWORD) ( strlen( pInString ) + 1 );
		if ( (m_dwSize > m_dwCurPos)
			&& ( m_dwSize - m_dwCurPos > insize )
			)
		{
			strcpy( (m_pInnerBuffer+m_dwCurPos), pInString );
			m_dwCurPos += insize;
			return TRUE;
		} else if ( ( m_dwSize*BACKBUF_MULTI > m_dwBackBufPos )
			&& ( m_dwSize*BACKBUF_MULTI - m_dwBackBufPos > insize ) )
		{ //可以存至后备缓存，后备缓存主要用于保存在写主缓存过程中到来的日志， 后备缓存要足够大，以防止写线程写的时间中来到过多的日志使得日志丢失；
			strcpy( (m_pInnerBackBuffer+m_dwBackBufPos), pInString );
			m_dwBackBufPos += insize;
			return FALSE;//返回FALSE告诉外部调用线程主缓存已满，此时外部调用线程应将主缓存输出；
		} else {
			return FALSE;
		}
	}

	///取内部缓存；
	char* GetBuffer() { return m_pInnerBuffer; }
	///取已用内存大小； 
	DWORD GetUsedSize() { return m_dwCurPos; }
	///取整个内存大小；
	DWORD GetTotalSize() { return m_dwSize;	}
	///取剩余大小；
	DWORD GetRemainSize() { return m_dwSize - m_dwCurPos; }
	///清已用内存；
	void  ClearUsed() { m_dwCurPos = 0; }
	///输出缓存至文件；
	void  WriteBufferTo( const char* inFileName )
	{
		if ( ( m_dwCurPos>0 )
			|| ( m_dwBackBufPos>0 )
			)
		{
			HANDLE hFileHandle;
			hFileHandle = CreateFile( inFileName, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, 0, NULL );
			if ( hFileHandle==INVALID_HANDLE_VALUE )
			{
				return;
			}

			m_dwWriteSize = 0;

			if ( TRUE )
			{
				CDsLock<CDsMutex> tmpLock( m_bufLock );
				if ( m_dwCurPos>0 )//主缓存内容拷至写缓存;
				{
					memcpy( m_pWriteBuffer, m_pInnerBuffer, m_dwCurPos );
					m_dwWriteSize = m_dwCurPos;
					m_dwCurPos = 0;//清主缓存内容；
				}
			}

			if ( m_dwWriteSize > 0 )
			{
				DWORD	dwPos,dwWriteNum;
				dwPos = SetFilePointer(hFileHandle, 0, NULL, FILE_END); 
				LockFile(hFileHandle, dwPos, 0, dwPos + m_dwWriteSize, 0); 
				WriteFile(hFileHandle, m_pWriteBuffer, m_dwWriteSize, &dwWriteNum, NULL);
				UnlockFile(hFileHandle, dwPos, 0, dwPos + m_dwWriteSize, 0); 
				m_dwWriteSize = 0;//清写缓存;
			}

			if ( TRUE )
			{
				CDsLock<CDsMutex> tmpLock( m_backBufLock );
				if ( m_dwBackBufPos>0 )//后备缓存内容拷至写缓存;
				{
					memcpy( m_pWriteBuffer, m_pInnerBackBuffer, m_dwBackBufPos );
					m_dwWriteSize = m_dwBackBufPos ;
					m_dwBackBufPos = 0;//清后备缓存;
				}
			}

			if ( m_dwWriteSize > 0 )
			{
				DWORD	dwPos,dwWriteNum;
				dwPos = SetFilePointer(hFileHandle, 0, NULL, FILE_END); 
				LockFile(hFileHandle, dwPos, 0, dwPos + m_dwWriteSize, 0); 
				WriteFile(hFileHandle, m_pWriteBuffer, m_dwWriteSize, &dwWriteNum, NULL);
				UnlockFile(hFileHandle, dwPos, 0, dwPos + m_dwWriteSize, 0); 
				m_dwWriteSize = 0;//清写缓存;
			}

			CloseHandle(hFileHandle);
		}else {
			INT jjj = 0;
		}
		
	}

private:
	///防止显式构造；
	CDsStrWriteBuffer(){};

	//////////////////////////////////////////////////////////////////////////
	//主缓存与后备缓存为并行操作（主线程与写线程）；
	//写缓存为单线程操作（写线程），因为写线程只有一个；
	//////////////////////////////////////////////////////////////////////////	
private:
	///缓存指针；
	char* m_pInnerBuffer;//缓存指针;
	///后备缓存；
	char* m_pInnerBackBuffer;//后备缓存;

	///写缓存，写到磁盘时，将欲写内容拷至此内存并从此内存开始写，防止直接输出缓存时，新到来的日志等待写操作完成；
	char* m_pWriteBuffer;//写缓存，写到磁盘时，将欲写内容拷至此内存并从此内存开始写，防止直接输出缓存时，新到来的日志等待写操作完成；
	///写缓存大小；
	DWORD m_dwWriteSize;//写缓存大小；

	///主缓存总大小；
	DWORD m_dwSize;//主缓存总大小；
	///可用内存起始位置(已记录信息的终止位置)；
	DWORD m_dwCurPos;//可用内存起始位置(已记录信息的终止位置)；
	///后备缓存指针位置；
	DWORD m_dwBackBufPos;//后备缓存指针位置；

	///写缓存锁1；
	CDsMutex m_bufLock;//写缓存锁1；
	///后备缓存锁；
	CDsMutex m_backBufLock;//后备缓存锁；
};

///日志记录类；
class CDsLog
{
public:
	///构造，设置缓存大小；
	CDsLog( const char* inFilePath, DWORD dwBufferSize )
		: m_FilePath( NEW char[strlen(inFilePath)+1] )
		, m_WriteBuffer1(dwBufferSize)
	{
        strcpy( m_FilePath, inFilePath );

		m_WriteSignal1 = CreateEvent( NULL// no security attributes
			, TRUE         // manual-reset event
			, FALSE         // initial state is not signaled
			, NULL  // object name
			); 
		m_bIsEndWriteThread = FALSE;
		m_bIsFirstLog = TRUE;
	}

	///析构，停写线程；
	~CDsLog() 
	{ 
		{
			m_bIsEndWriteThread = TRUE;//停止写线程；
			SetEvent( m_WriteSignal1 );
			Sleep( 3000 );
		}
		CloseHandle( m_WriteSignal1 );
		delete [] m_FilePath;

	};

public:
	///记日志，单线程；
	BOOL LogMsg( const char* inMsg ) //单线程日志;
	{
		if ( m_bIsFirstLog )//第一次写日志时启动线程；
		{
			m_bIsFirstLog = FALSE;
			//1个写线程让其在m_WriteSignal1上等待；
			m_Thread1Param.pDsLog = this;
			m_Thread1Param.hWaitHandle = m_WriteSignal1;
			m_Thread1Param.pWriteBuffer = &m_WriteBuffer1;
			DWORD nThreadID1;
			HXBCBEGINTHREADEX( NULL, 0, (LPTHREAD_START_ROUTINE) DsLogWriteThread, (LPVOID)&m_Thread1Param, 0, &nThreadID1 );//写线程只能启动一个；
		}

		BOOL isWriteOK = FALSE;
		isWriteOK = m_WriteBuffer1.AddString( inMsg );//记录到写缓存1；
		//如果返回非，则说明主缓存已满，将缓存的内容（包括后备缓存)输出；
		//写线程在写 是 m_WriteSignal1为signal状态 的充要条件;
		if ( !isWriteOK && ( WAIT_OBJECT_0!=WaitForSingleObject( m_WriteSignal1, 0 ) ) )//如果主缓存目前没在写，则通知写;
		{
			SetEvent( m_WriteSignal1 );//通知写线程将缓存1的内容输出；
		}

		return TRUE;
	}

	///强制输出目前为止缓存的日志；
	void ForceWriteLog()//强制输出目前为止缓存的日志；
	{
		if ( WAIT_OBJECT_0!=WaitForSingleObject( m_WriteSignal1, 0 ) )//如果主缓存目前没在写，则通知写;
		{
			SetEvent( m_WriteSignal1 );//通知写线程将缓存1的内容输出；
		}
		return;
	}

private:
	///防止显示构造；
	CDsLog();

public:
	///文件路径；
	char* m_FilePath;
	///内部缓存对象；
	CDsStrWriteBuffer m_WriteBuffer1;
	///是否结束写线程；
	BOOL m_bIsEndWriteThread;//是否结否写线程；

	///写线程通知信号；
	HANDLE m_WriteSignal1;

private:
	///是否第一次记日志；
	BOOL m_bIsFirstLog;    
	///写线程参数；
	WriteThreadParam m_Thread1Param;

};

