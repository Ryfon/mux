﻿/**
* @file dslog.cpp
* @brief 日志类头文件
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称:dslog.cpp
* 摘    要:本文件用于日志记录类；
* 作    者:dzj
* 修改日期:2007.11.09，部分注释方式修改；
* //////////////////////////////////////////////////////////////////////////
* //日志类，by dzj, 06.07.10;
* //初步想法：1、预先分配一整块内存，写日志过程中不重新分配释放内存;
* //			2、使用一个单独的写线程执行IO繁忙操作；
* //			3、使用内部写缓存，尽量减少写线程执行时，主线程等待日志缓存锁的时间（从等待IO操作完成改为等待到写缓存的内存拷贝操作完成）；
* //注：主缓存与后备缓存必须足够大以防止丢失在写另一部分缓存内容时到来的包；
* //	(实验表明：如BACKBUF_MULTI为2，则主缓存设为2k以上即可，这样后备缓存有4k，
* //		即使日志不间断到来，4k也足够储存将主缓存拷贝到写缓存这段时间内新到的日志)
* //////////////////////////////////////////////////////////////////////////
*/

#include "StdAfx.h"
#include "dslog.h"
#include <stdio.h>

///写日志线程；
UINT WINAPI DsLogWriteThread( LPVOID lpParam )
{
	WriteThreadParam* inParam = (WriteThreadParam*) lpParam;
	CDsLog* pDsLog = inParam->pDsLog;
	HANDLE hWaitHandle = inParam->hWaitHandle;
	CDsStrWriteBuffer* pWriteBuffer = inParam->pWriteBuffer;

	char fileName[1024];
//	strcpy( fileName, pDsLog->m_FilePath );

	while ( TRUE )
	{
		if ( pDsLog->m_bIsEndWriteThread )
		{
			break;
		}

		WaitForSingleObject( hWaitHandle, INFINITE );

		//pWriteBuffer->WriteBufferTo( pDsLog->m_FilePath );
		GetLogFileName( fileName );
		pWriteBuffer->WriteBufferTo( fileName );

		ResetEvent( hWaitHandle );
	}

	return 0;
}

///计算日志文件名；
void GetLogFileName( char* pFileName )
{
	SYSTEMTIME time;
	ZeroMemory(&time,sizeof(SYSTEMTIME));
	GetLocalTime(&time);

	//文件目录；
	GetCurrentDirectory( MAX_PATH, pFileName );
	strcat(pFileName,"\\ErrorLog\\");

	if( !IsDirExist(pFileName) )
	{
		if(CreateDirectory(pFileName,NULL) == 0)	return;
	}

	sprintf(pFileName, "%s%d-%.2d\\", pFileName, time.wYear, time.wMonth);

	if( !IsDirExist(pFileName) )
	{
		if(CreateDirectory(pFileName,NULL) == 0)	return;
	}

	sprintf( pFileName, "%s%.2d-%.2d-%.2d.log", pFileName, time.wYear, time.wMonth, time.wDay );

	return;
}
