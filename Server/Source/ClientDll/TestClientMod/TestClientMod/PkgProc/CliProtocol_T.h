﻿#ifndef			__CliProtocol__
#define			__CliProtocol__
#include "ParserTool.h"
#include "map"

namespace MUX_PROTO
{
	//Enums
	#pragma pack(push,1)
	typedef	enum
	{
		NTS_NOACCEPT                      	=	0,           	//未接受任务,对于任务实例而言不可能
		NTS_TGT_NOT_ACHIEVE               	=	1,           	//接受但还未达成目标
		NTS_TGT_ACHIEVE                   	=	2,           	//接受且已达成目标
		NTS_FINISH                        	=	3,           	//已完成任务
		NTS_INVALID                       	=	4,           	//无效任务
		NTS_FAIL                          	=	5,           	//失败的任务
	}ETaskStatType;

	typedef	enum
	{
		PLATE_UPGRADE                     	=	0,           	//升级
		PLATE_ADD                         	=	1,           	//追加
		PLATE_MODIFY                      	=	2,           	//改造
		INVALID_PLATE                     	=	3,           	//无效
	}EPlateType;

	typedef	enum
	{
		PLATE_POS_MAIN                    	=	0,           	//主料位置
		PLATE_POS_ASST                    	=	1,           	//辅料位置
	}EPlatePos;

	typedef	enum
	{
		MAP_PS_INVALID                    	=	0,           	//无效状态(例如：对象池中待分配)
		MAP_PS_DIE                        	=	1,           	//死亡倒计时阶段(本阶段结束立即跳复活点，变成PS_ALIVE)
		MAP_PS_ALIVE                      	=	2,           	//活着
		MAP_PS_WAITING_CLIENT             	=	3,           	//等待客户端响应（装载地图等）
	}EPlayerBaseState;

	typedef	enum
	{
		INFO_LOGIN_APPEAR                 	=	0,           	//玩家出现时的消息
		INFO_SWITCH_APPEAR                	=	1,           	//切换地图
		INFO_REBIRTH_APPEAR               	=	2,           	//重生出现
		INFO_NORMAL_UPDATE                	=	3,           	//常规更新
		INFO_OFFLINE_UPDATE               	=	4,           	//玩家离开MapSrv时的更新
		INFO_SWITCH_UPDATE                	=	5,           	//玩家跳地图时，原地图信息更新
		INFO_MAP_ENTERED                  	=	6,           	//不论何种原因，用于通知玩家进入地图成功
	}EAppearType;

	typedef	enum
	{
		CT_SCOPE_CHAT                     	=	0,           	//周边范围内群聊
		CT_PRIVATE_CHAT                   	=	1,           	//私聊
		CT_MAP_BRO_CHAT                   	=	2,           	//地图内喊话
		CT_SYS_GM_NOTI_MAP                	=	3,           	//GM发布的通知(通知地图内所有人)
		CT_SYS_EVENT_NOTI                 	=	4,           	//系统事件通知（例如操作结果）
		CT_SYS_WARNING                    	=	5,           	//系统警告
		CT_NPC_UIINFO                     	=	6,           	//NPC对话界面信息；
		CT_ITEM_UIINFO                    	=	7,           	//Item对话界面信息；
		CT_WORLD_CHAT                     	=	8,           	//全世界广播
		CT_TEAM_CHAT                      	=	9,           	//组队广播
		CT_AREA_UIINFO                    	=	10,          	//区域脚本界面信息
		CT_PRIVATE_IRC_CHAT               	=	11,          	//IRC私聊
		CT_WORLD_IRC_CHAT                 	=	12,          	//IRC世界聊天
		CT_UNION_CHAT                     	=	13,          	//工会聊天
	}CHAT_TYPE;

	typedef	enum
	{
		ST_ITEM_SCRIPT                    	=	0,           	//Item脚本
		ST_NPC_SCRIPT                     	=	1,           	//NPC脚本
		ST_AREA_SCRIPT                    	=	2,           	//Area脚本
	}SCRIPT_TYPE;

	typedef	enum
	{
		DEFAULT_MODE                      	=	0,           	//默认模式
		FULL_FIELD_MODE                   	=	1,           	//全域战斗模式
		GUILD_MODE                        	=	2,           	//公会模式,暂不开放
	}BATTLE_MODE;

	typedef	enum
	{
		NORMAL_MODE                       	=	0,           	//普通状态
		EVIL_MODE                         	=	1,           	//魔头状态
		HERO_MODE                         	=	2,           	//英雄状态
		GOODEVIL_MODE_MAX                 	=	3,           	//
	}GOODEVIL_MODE;

	typedef	enum
	{
		RT_HP                             	=	0,           	//血值
		RT_MP                             	=	1,           	//魔法值
		RT_EXP                            	=	2,           	//经验值
		RT_SPEED                          	=	3,           	//速度
		RT_PHYLEVEL                       	=	4,           	//物理技能等级
		RT_MAGLEVEL                       	=	5,           	//魔法技能等级
		RT_PHYPOINT                       	=	6,           	//物理技能点
		RT_MAGPOINT                       	=	7,           	//魔法技能点
		RT_PHYEXP                         	=	8,           	//物理技能熟练度
		RT_MAGEXP                         	=	9,           	//魔法技能熟练度
		RT_PHYLEVEL_MAX                   	=	10,          	//物理技能等级上限
		RT_MAGLEVEL_MAX                   	=	11,          	//魔法技能等级上限
		RT_MONEY                          	=	12,          	//金钱
		RT_PHYATTACK                      	=	13,          	//物理攻击
		RT_MAGATTACK                      	=	14,          	//魔法攻击
		RT_PHYDEFEND                      	=	15,          	//物理防御
		RT_MAGDEFEND                      	=	16,          	//魔法防御
		RT_PHYHIT                         	=	17,          	//物理命中
		RT_MAGHIT                         	=	18,          	//魔法命中
		RT_PHYCRI                         	=	19,          	//物理爆击
		RT_MAGCRI                         	=	20,          	//魔法爆击
		RT_HPMAX                          	=	21,          	//最大HP
		RT_MPMAX                          	=	22,          	//最大MP
		RT_STR                            	=	23,          	//力量
		RT_INT                            	=	24,          	//智力
		RT_AGI                            	=	25,          	//敏捷
		RT_SKILL                          	=	26,          	//技能
		RT_GOOEVILPOINT                   	=	27,          	//善恶值
		RT_ROGUE                          	=	28,          	//流氓状态
	}ROLEATT_TYPE;

	typedef	enum
	{
		BATTLE_SUCCESS                    	=	0,           	//战斗成功
		SKILL_IN_COOLDOWN_ERROR           	=	1,           	//技能还在COOLDOWN中
		NOT_IN_ATK_RANGE_ERROR            	=	2,           	//不在攻击范围之内
		NOT_ENOUGH_SP                     	=	3,           	//SP不足
		INVALID_SKILL_ID_ERROR            	=	4,           	//客户端传来非战斗类技能
		TARGET_INVALID_ERROR              	=	5,           	//对象消失或者已经无效
		PLAYER_STATE_ERROR                	=	6,           	//攻击玩家处在不可攻击的状态,例如死亡,晕迷状态
		PLAYER_POS_ERROR                  	=	7,           	//攻击玩家处在错误的位置,可能是障碍物
		SKILL_IN_CT_TIME_ERROR            	=	8,           	//玩家还在CT动作中,不能释放任何技能
		NOT_ENOUGH_MP                     	=	9,           	//MP不足
		OTHER_ERROR                       	=	10,          	//其他任何未定义的未知错误
		BATTLE_MODE_ERROR                 	=	11,          	//战斗模式不支持该攻击目标
		NOT_PK_AREA_ERROR                 	=	12,          	//战斗模式不支持该攻击目标
	}EBattleResult;

	typedef	enum
	{
		INVITE_SUCCESS                    	=	0,           	//组队或者申请组队邀请成功
		PLAYER_HAS_TEAM                   	=	1,           	//玩家已经在别人的组队中
		PLAYER_NOT_FIND                   	=	2,           	//玩家不在线上或者根本无此玩家
		PLAYER_BUSY                       	=	3,           	//各种特殊状态,均声明为在忙
		PLAYER_REFUSE                     	=	4,           	//玩家设置了防骚扰开关或者玩家拒绝了组队
		PLAYER_NOT_CAPTAIN                	=	5,           	//对有的请求,可能因为不是队长而不能成功
		PLAYER_NOT_IN_TEAM                	=	6,           	//有的玩家不在组队中却发送了组队的请求
		PLAYER_NOT_SAME_RACE              	=	7,           	//不同种族的玩家不能组队
		PLAYER_MAX_ERROR                  	=	8,           	//组员超过了最大人数
	}EInviteTeamResult;

	typedef	enum
	{
		SEX_MALE                          	=	1,           	//男性
		SEX_FEMALE                        	=	2,           	//女性
	}ESex;

	typedef	enum
	{
		CRITICAL                          	=	0,           	//正常通知,范围消息中的一个
		HIT                               	=	1,           	//命中
		MISS                              	=	2,           	//未命中
		ABSORB                            	=	3,           	//吸收
		IMMUNITY                          	=	4,           	//免疫
		PUNISH_MISS                       	=	5,           	//天谴未命中
	}EBattleFlag;

	typedef	enum
	{
		SINGLE_ATTACK                     	=	0,           	//正常通知,独立完整包
		SCOPE_ATTACK                      	=	1,           	//正常通知,范围消息中的一个
		SCOPE_ATTACK_END                  	=	2,           	//范围列表结束
	}EScopeFlag;

	typedef	enum
	{
		E_FREE_MODE                       	=	0,           	//自由模式
		E_TURN_MODE                       	=	1,           	//轮流模式
		E_TEAM_MODE                       	=	2,           	//队伍模式
		E_ITEMMODE_MAX                    	=	3,           	//最大枚举
	}ITEM_SHAREMODE;

	typedef	enum
	{
		E_SHARE_MODE                      	=	0,           	//分享模式
		E_EXCLUSIVE_MODE                  	=	1,           	//独占模式
		E_EXPERIENCE_MAX                  	=	2,           	//最大枚举
	}EXPERIENCE_SHAREMODE;

	typedef	enum
	{
		E_GREEN_LEVEL                     	=	0,           	//绿色等级
		E_BLUE_LEVEL                      	=	1,           	//蓝色等级
		E_PURPLE_LEVEL                    	=	2,           	//紫色等级
		E_ORANGE_LEVEL                    	=	3,           	//橙色等级
		E_ROLLITEM_MAX                    	=	4,           	//最大枚举
	}ROLLITEM_MODE;

	typedef	enum
	{
		E_PKG_SUCCESS                     	=	0,           	//拾取成功
		E_PKG_FULL                        	=	1,           	//玩家的包裹满了
		E_PKG_EMPTY                       	=	2,           	//拾取的包裹为空
		E_PKG_OWNER                       	=	3,           	//道具的所有权错误
		E_PKG_COMBIND                     	=	4,           	//道具合并
		E_PKG_ERROR                       	=	5,           	//发生未知错误
	}EPickItemResult;

	typedef	enum
	{
		RACE_HUMAN                        	=	1,           	//人类
		RACE_ARKA                         	=	2,           	//艾卡
		RACE_ELF                          	=	3,           	//精灵
	}ERace;

	typedef	enum
	{
		CLASS_WARRIOR                     	=	1,           	//剑士
		CLASS_ASSASSIN                    	=	2,           	//刺客
		CLASS_PALADIN                     	=	3,           	//圣骑士
		CLASS_BLADER                      	=	4,           	//魔剑士
		CLASS_HUNTER                      	=	5,           	//弓箭手
		CLASS_MAGE                        	=	6,           	//魔法师
	}EClass;

	typedef	enum
	{
		TYPE_PHYSICS                      	=	0,           	//物理
		TYPE_MAGIC                        	=	1,           	//魔法
		TYPE_ASSIST                       	=	2,           	//辅助
		TYPE_SPECIAL                      	=	3,           	//特殊
	}ESkillType;

	typedef	enum
	{
		SHAPE_SINGLE                      	=	0,           	//单体
		SHAPE_CIRCLE                      	=	1,           	//圆型，自身范围的正方型
		SHAPE_RECTANGLE                   	=	2,           	//长方形的直线
	}ESkillShape;

	typedef	enum
	{
		TARGET_PASSIVE                    	=	0,           	//被动技能
		TARGET_SELF                       	=	1,           	//自己
		TARGET_TEAM                       	=	2,           	//队伍
		TARGET_OBJECT                     	=	3,           	//以玩家或者怪物为对象
		TARGET_GROUND                     	=	4,           	//地面
	}ESkillTarget;

	typedef	enum
	{
		UPGRADE_SUCCESS                   	=	0,           	//升级成功
		NOT_ENOUGH_POINT_ERROR            	=	1,           	//不足的点数
		INVALID_ID_ERROR                  	=	2,           	//玩家身上找不到该技能
		MAX_LEVEL_SKILL_ERROR             	=	3,           	//这个技能已经是最高级,不能升级了
		SPSKILL_ERROR                     	=	4,           	//SP技能不由玩家主动升级
		NOT_ENOUGH_LEVEL                  	=	5,           	//玩家没到必须的等级
	}EUpgradeSkillResult;

	typedef	enum
	{
		E_GROUPTEAM                       	=	0,           	//在处理组队请求
		E_RIDETEAM                        	=	1,           	//在处理骑乘请求
		UNKNOWN_ERROR                     	=	2,           	//未知错误
	}EPlayerBusyingReason;

	typedef	enum
	{
		MATERIAL_ERROR                    	=	0,           	//材料错误
		UNKNOWN_UPDATEERROR               	=	1,           	//未知错误
	}EUpdateItemError;

	typedef	enum
	{
		GIVETASK_PKGFULL                  	=	0,           	//背饱满接任务失败 
		FINISHTASK_PKGFULL                	=	1,           	//背包满还任务失败 
		NORMAL_PKGFULL                    	=	2,           	//一般情况背包满物品给与失败
	}ETaskUIMsgType;

	typedef	enum
	{
		TASK_NONE                         	=	0,           	//无任何任务状态
		TASK_PROCESS                      	=	1,           	//任务在进行中
		TASK_FINISH                       	=	2,           	//任务完成,并已经提交给NPC
		TASK_FAIL                         	=	3,           	//任务失败
		TASK_ACHIEVE                      	=	4,           	//任务完成,还未提交
	}ETaskState;

	typedef	enum
	{
		BUFF_ATTRIBUTE                    	=	0,           	//更改属性，也属于附和BUFF
		BUFF_HP_HOT                       	=	1,           	//HP持续上升
		BUFF_PHY_ATTACK_UP                	=	2,           	//物理攻击的提升
		BUFF_MAG_ATTACK_UP                	=	3,           	//魔法攻击的提升
		BUFF_PHY_DEFEND_UP                	=	4,           	//物理防御提升
		BUFF_MAG_DEFNED_UP                	=	5,           	//魔法防御提升
		BUFF_PHY_HIT_UP                   	=	6,           	//物理命中提升
		BUFF_MAG_HIT_UP                   	=	7,           	//魔法命中提升
		BUFF_PHY_CRI_UP                   	=	8,           	//物理暴击等级上升
		BUFF_MAG_CRI_UP                   	=	9,           	//魔法暴击等级上升
		BUFF_ADD_FAINT_RATE               	=	10,          	//增加暴几率，怪物用
		BUFF_ADD_FAINT_TIME               	=	11,          	//增加束几率, 怪物用
		DEBUFF_SPEED_DOWN                 	=	12,          	//速度下降
		DEBUFF_FAINT                      	=	13,          	//控制类的DEBUFF,晕旋
		DEBUFF_BONDAGE                    	=	14,          	//控制类的DEBUFF,束缚
		BUFF_HP_DOT                       	=	15,          	//HP持续减少
		DEBUFF_PHY_ATTACK_DOWN            	=	16,          	//物理攻击下降
		DEBUFF_MAG_ATTACK_DOWN            	=	17,          	//魔法攻击下降
		DEBUFF_PHY_DEFEND_DOWN            	=	18,          	//物理防御下降
		DEBUFF_MAG_DEFEND_DOWN            	=	19,          	//魔法防御下降
		BUFF_STR                          	=	20,          	//增加STR
		BUFF_AGI                          	=	21,          	//增加AGI
		BUFF_INT                          	=	22,          	//增加INT
		BUFF_MAXHP_UP                     	=	23,          	//增加最大HP
		BUFF_ADD_PHYCRI_DAMAGE            	=	24,          	//提高物理暴击追加伤害
		BUFF_ADD_MAGCRI_DAMAGE            	=	25,          	//提高魔法暴击追加伤害
		BUFF_RATE_PHYDAMAGE_UP            	=	26,          	//提高物理伤害的百分比
		BUFF_RATE_MAGDAMAGE_UP            	=	27,          	//提高魔法伤害的百分比
		BUFF_RATE_RESIST_FAINT            	=	28,          	//提高抗击晕的百分比
		BUFF_RATE_RESIST_BONDAGE          	=	29,          	//提高抗束缚的百分比
		BUFF_RATE_ADD_FAINT               	=	30,          	//提高击晕的百分比
		BUFF_RATE_ADD_BONDAGE             	=	31,          	//提高击晕的百分比
		BUFF_ADD_AOE_RANGE                	=	32,          	//提高范围攻击的作用范围
		BUFF_ADD_ATK_DISTANCE             	=	33,          	//提高攻击技能的施放距离
		BUFF_ADD_DOT_DAMAGE               	=	34,          	//提高每次DOT技能的伤害值
		BUFF_DECREASE_CD                  	=	35,          	//减少每个技能的COOL DOWN时间
		DEBUFF_SPECIAL_DOT                	=	36,          	//DOT的每跳的伤害需要和人物的属性相,这里只是个基础值
		BUFF_RATE_PHYCRI_HIT_UP           	=	37,          	//提高物理暴击百分比
		BUFF_RATE_MAGCRI_HIT_UP           	=	38,          	//提高魔法暴击百分比
		BUFF_RATE_PHYHIT_UP               	=	39,          	//提高物理攻击时的命中百分比
		BUFF_RATE_MAGHIT_UP               	=	40,          	//提高魔法攻击时的命中百分比
		BUFF_RATE_PHYCRI_DAMAGE_UP        	=	41,          	//提高物理暴击攻击的伤害比率
		BUFF_RATE_MAGCRI_DAMAGE_UP        	=	42,          	//提高魔法暴击攻击的伤害比率
		BUFF_PHYJOUK_LEVEL                	=	43,          	//提高物理攻击闪避等级
		BUFF_MAGJOUK_LEVEL                	=	44,          	//提高魔法攻击闪避等级
		BUFF_PERCENT_HOT                  	=	45,          	//百分比hot
		DAMAGE_ABSORB                     	=	46,          	//伤害吸收
		DAMAGE_IMMUNITY                   	=	47,          	//免疫伤害
		MOVE_IMMUNITY                     	=	48,          	//免疫移动限制
		ALL_IMMUNITY                      	=	49,          	//免疫移动限制
		SP_SPEEDUP                        	=	50,          	//加速获得SP能量
		BUFF_SP_COMPLEX                   	=	99,          	//复合SP BUFF
	}BUFF_TYPE;

	typedef	enum
	{
		SUCCEED                           	=	50001,       	//
		FAILED                            	=	50002,       	//
		NOOP                              	=	50003,       	//
		NO_GROUP                          	=	50004,       	//
		NO_MEMBER                         	=	50005,       	//
		GROUP_FULL                        	=	50006,       	//
		GROUP_EMPTY                       	=	50007,       	//
		GATE_ID_DIRTY                     	=	50008,       	//
		NO_ATTRI                          	=	50009,       	//
		ATTRI_FULL                        	=	50010,       	//
		ATTRI_UPDATE                      	=	50011,       	//
		GROUP_REPEAT                      	=	50012,       	//
		CAPTAIN_NOTIN_TEAM                	=	50013,       	//
		MEMBER_IN_OTHER_TEAM              	=	50014,       	//
		SEND_GROUP_MSG_TIMES              	=	50015,       	//
		NOT_FIND_FRIEND                   	=	50016,       	//
		FRIEND_COUNT_SLOP_OVER            	=	50017,       	//
		BLACKLST_EXIST                    	=	50018,       	//
		FOE_EXIST                         	=	50019,       	//
		FRIENT_EXIST                      	=	50020,       	//
		NO_EXIST                          	=	50021,       	//
		GROUP_COUNT_SLOP_OVER             	=	50021,       	//
		SOURCE_GROUP_NOT_FIND             	=	50022,       	//
		DESC_GROUP_NOT_FIND               	=	50023,       	//
		BLACKLST_COUNT_SLOP_OVER          	=	50024,       	//
		GROUP_NO_EMPTY                    	=	50025,       	//
		FOE_COUNT_SLOP_OVER               	=	50026,       	//
		FOE_NOT_FIND                      	=	50027,       	//
		ADD_FRIEND_SELF                   	=	50028,       	//
		ADD_BLACKLST_SELF                 	=	50029,       	//
		FRIEND_NAME_NO_EXIST              	=	50030,       	//
		IS_NOT_OWNER                      	=	50031,       	//
	}EChatGroupResult;

	typedef	enum
	{
		RESULT_UNION_SUCCEED              	=	60001,       	//
		RESULT_UNION_NAME_REPEATED        	=	60002,       	//
		RESULT_UNION_NO_MEMBER            	=	60003,       	//
		RESULT_UNION_IS_FULL              	=	60004,       	//
		RESULT_IN_OTHER_UNION             	=	60005,       	//
		RESULT_SERVICE_ERROR              	=	60006,       	//
		RESULT_NO_PLAYER                  	=	60007,       	//
		RESULT_ADD_UNION_MEMBER_SUCCEED   	=	60008,       	//
		RESULT_NEED_HIGHER_LEVEL          	=	60009,       	//
		RESULT_PLAYER_NO_UNION            	=	60010,       	//
		RESULT_RIGHT_CHECK_FAILED         	=	60011,       	//
		RESULT_UNION_ADD_REJECT           	=	60012,       	//
		RESULT_UNION_ADD_AGREE            	=	60013,       	//
		RESULT_UNION_NO_POS               	=	60014,       	//
		RESULT_UNION_FORBID_SPEEK         	=	60015,       	//
		RESULT_UNION_STUDY_SKILL_FAILED   	=	60016,       	//
		RESULT_UNION_TASK_LIST_POST_REPEAT	=	60017,       	//
		RESULT_UNION_ACTIVE_POINT_LACK    	=	60018,       	//
		RESULT_UNION_UNEXPECTED_SKILL_ID  	=	60019,       	//
	}EUnionResult;

	typedef	enum
	{
		IS_ACTIVATE_MASK                  	=	1,           	//
		AUTOMATIC_ATTACK_MASK             	=	2,           	//
		AUTOMATIC_USE_DRUGS_MASK          	=	4,           	//
		AUTOMATIC_PICK_MASK               	=	8,           	//
		WEAPON_LEVELUP_MASK               	=	16,          	//
		OFFLINE_LEVELUP_MASK              	=	32,          	//
		BACK4                             	=	64,          	//
	}EPetSkillMask;

	typedef	enum
	{
		UNION_RIGHT_LEAVE                 	=	0x00000001,  	//
		UNION_RIGHT_VIEW_MEMBERS          	=	0x00000002,  	//
		UNION_RIGHT_VIEW_UNION_LOG        	=	0x00000004,  	//
		UNION_RIGHT_CLAIM_TASK            	=	0x00000008,  	//
		UNION_RIGHT_OPEN_STORAGE          	=	0x00000010,  	//
		UNION_RIGHT_VIEW_STORAGE_LOG      	=	0x00000020,  	//
		UNION_RIGHT_USE_UNION_CHANNEL     	=	0x00000040,  	//
		UNION_RIGHT_USER_UNION_SKILL      	=	0x00000080,  	//
		UNION_RIGHT_ADD_MEMBER            	=	0x00000100,  	//
		UNION_RIGHT_REMOVE_MEMBER         	=	0x00000200,  	//
		UNION_RIGHT_EDIT_TITLE            	=	0x00000400,  	//
		UNION_RIGHT_ADVANCE_POS           	=	0x00000800,  	//
		UNION_RIGHT_REDUCE_POS            	=	0x00001000,  	//
		UNION_RIGHT_POST_ANNOUNCE         	=	0x00002000,  	//
		UNION_RIGHT_ADVANCE_UNION         	=	0x00004000,  	//
		UNION_RIGHT_POST_TASK             	=	0x00008000,  	//
		UNION_RIGHT_STUDY_SKILL           	=	0x00010000,  	//
		UNION_RIGHT_REMOVE_SPEEK_RESTRICT 	=	0x00020000,  	//
		UNION_RIGHT_ADD_SPEEK_RESTRICT    	=	0x00040000,  	//
		UNION_RIGHT_TRANSFER_CHAIR        	=	0x00080000,  	//
		UNION_RIGHT_MANAGER_UNION         	=	0x00100000,  	//
		UNION_RIGHT_DESIGN_UNION_BADGE    	=	0x00200000,  	//
		UNION_RIGHT_EXTEND_STORAGE        	=	0x00400000,  	//
		UNION_RIGHT_MANAGER_STORAGE       	=	0x00800000,  	//
		UNION_RIGHT_GET_PAY               	=	0x01000000,  	//
	}EUnionRightMask;

	typedef	enum
	{
		UNION_LOG_CREATE_EVENT            	=	0,           	//
		UNION_LOG_CAPTION_CHANGED_EVENT   	=	1,           	//
		UNION_LOG_ADVANCE_LEVEL_EVENT     	=	2,           	//
		UNION_LOG_REDUCE_LEVEL_EVENT      	=	3,           	//
		UNION_LOG_CRAFT_EVENT             	=	4,           	//
		UNION_LOG_BADGE_EVENT             	=	5,           	//
		UNION_LOG_KILL_NPC_EVENT          	=	6,           	//
		UNION_LOG_FIRST_PLACE_EVENT       	=	7,           	//
	}EUnionLog;

	typedef	enum
	{
		PUR_GAMEMONEY                     	=	0,           	//星钻方式购买
		PUR_SCORE                         	=	1,           	//积分方式购买
	}PurchaseCode;

	typedef	enum
	{
		PUR_OK                            	=	0,           	//购买成功
		PUR_MONEYNOTENOUGH                	=	10050,       	//余额不足
		PUR_NOTSUPPORTTYPE                	=	10001,       	//不支持此种购买方式
		PUR_VIP_EXISTS                    	=	10002,       	//存在未用完的Vip方案
		FETCH_VIPITEM_DONE                	=	10003,       	//当天的Vip道具包已经领取过
		SHOP_SYSERROR                     	=	10999,       	//系统错误
	}ShopOpCode;

	typedef	enum
	{
		E_NORMAL_MAIL                     	=	0,           	//普通邮件
		E_SYSYTEM_MAIL                    	=	1,           	//系统邮件
		E_INVAILD_MAIL                    	=	2,           	//非法邮件
	}MAIL_TYPE;

	typedef	enum
	{
		E_PLAYER_NOEXIST                  	=	0,           	//玩家不存在
		E_PLAYER_MAIL_FULL                	=	1,           	//玩家背包满了
		E_INVALID_FAIL                    	=	2,           	//发送非法错误
	}SENDMAIL_FAIL_TYPE;


	//Defines
	#define	MAX_CHAT_SIZE              	 	256 	//最大聊天长段
	#define	MAX_NAME_SIZE              	 	20  	//最大名字长段
	#define	EQUIP_SIZE                 	 	16  	//最大装备拦位
	#define	PACKAGE_SIZE               	 	144 	//包裹大小
	#define	PKG_PAGE_SIZE              	 	18  	//传送给客户端的每页包裹的大小
	#define	SKILL_SIZE                 	 	32  	//技能大小
	#define	BUFF_SIZE                  	 	16  	//BUFF栏位,增加4个栏位用于SP技能
	#define	PKG_GROUP_NUM              	 	30  	//请求包裹信息时，每组返回的包裹物品数目
	#define	MAXPACKAGEITEM             	 	20  	//每个最大的道具包中最大的道具数目
	#define	BRO_SCOPE                  	 	10  	//范围广播的范围，玩家视野范围
	#define	MONSTER_GID                	 	1000	//怪物的GID固定为1000
	#define	DIE_DURATION               	 	6   	//死亡持续时间(提示复活跳转的时间，单位:分钟)
	#define	MAXMOUNTCOUNT              	 	2   	//骑乘的最大人数
	#define	MAX_TASKRD_SIZE            	 	50  	//最大的任务数目
	#define	MD5_RDSTR_LEN              	 	16  	//计算md5值用字符串长度
	#define	MD5_VALUE_CHARLEN          	 	16  	//char单位md5结果长度
	#define	MAX_CHATGROUP_PASSWORD_LEN 	 	6   	//群聊密码的最大长度
	#define	MAX_CHATGROUP_MEMBERS_COUNT	 	20  	//群聊成员最大数目
	#define	MAX_CHATGROUP_NAME_LEN     	 	20  	//群聊名字最大长度
	#define	MAX_CHATDATA_LEN           	 	120 	//群聊内容最大长度
	#define	ITEMNUM_PER_SHOPLABEL      	 	60  	//NPC商店中每标签的商品数量
	#define	LABELNUM_PER_SHOP          	 	12  	//NPC商店中每商店的标签数量
	#define	MAX_UNIONNAME_LEN          	 	20  	//工会名字最大长度
	#define	MAX_UNIONTITLE_LEN         	 	20  	//工会称号最大名字
	#define	MAX_PLAYERNAME_LEN         	 	20  	//工会成员最大数量
	#define	MAX_ISSUE_TASK_NUM         	 	20  	//工会可发布任务的最大数目
	#define	MAX_UNION_SKILL_NUM        	 	10  	//工会技能的最大数目
	#define	MAX_PET_IMAGE_LEN          	 	32  	//宠物最大能变身的种类
	#define	MAX_CHARTID_LEN            	 	5   	//排行榜ID的长度
	#define	MAX_CHARTPAGE_LEN          	 	5   	//排行榜返回给客户端的每页的大小
	#define	ITEMS_PER_PAGE             	 	16  	//每页道具数
	#define	MAX_NOTE_SIZE              	 	100 	//备注最大程度
	#define	MAX_MAIL_CONCISE_COUNT     	 	5   	//邮件简明信息的每页个数
	#define	MAX_MAIL_CONCISE_TITLE_LEN 	 	20  	//邮件简明信息的主题长度
	#define	MAX_MAIL_TITILE_LEN        	 	40  	//邮件的主题长度
	#define	MAX_MAIL_CONTENT_LEN       	 	400 	//邮件的正文长度
	#define	MAX_SYSMAIL_CONTENT_LEN    	 	300 	//系统邮件的主题长度
	#define	MAX_MAIL_ATTACHITEM_COUNT  	 	6   	//邮件所含有的附件的最大个数

	//Typedefs
	typedef	USHORT	 	TYPE_POS;  	//
	typedef	UINT  	 	TYPE_ID;   	//
	typedef	UINT  	 	TYPE_TIME; 	//
	typedef	INT   	 	TYPE_FLAG; 	//
	typedef	INT   	 	TYPE_INDEX;	//
	typedef	INT   	 	TYPE_NUM;  	//
	typedef	UINT  	 	TYPE_UI32; 	//
	typedef	INT   	 	TYPE_SI32; 	//
	typedef	USHORT	 	TYPE_UI16; 	//
	typedef	SHORT 	 	TYPE_SI16; 	//
	typedef	BYTE  	 	TYPE_UI8;  	//
	typedef	CHAR  	 	TYPE_SI8;  	//

	//Types
	template<bool> struct TypeMaxBytes;
	template<> struct TypeMaxBytes<true> {};
	#define	CLIPROTOCOL_T_MAXBYTES_CHECK(typename) \
	struct type##typename \
	{\
		TypeMaxBytes< sizeof(typename) < 507 > struct_larger_##typename;\
	}

	struct PlayerID
	{
		USHORT              	wGID;                                       	//
		TYPE_ID             	dwPID;                                      	//
	};

	struct PlayerIDEx
	{
		PlayerID            	playerID;                                   	//
		TYPE_ID             	playerUID;                                  	//
	};

	struct TeamMemberCliInfo
	{
		PlayerID            	playerID;                                   	//
		UINT                	nameSize;                                   	//
		CHAR                	playerName[MAX_NAME_SIZE];                    	//
		TYPE_UI16           	portraitID;                                 	//
		BYTE                	ucClass;                                    	//
		BYTE                	bSex;                                       	//性别
		BYTE                	ucRace;                                     	//种族
	};

	struct PlayerInfo_1_Base
	{
		TYPE_ID             	uiID;                                       	//角色ID
		UINT                	accountSize;                                	//账号
		CHAR                	szAccount[MAX_NAME_SIZE];                     	//
		UINT                	nameSize;                                   	//角色名字
		CHAR                	szNickName[MAX_NAME_SIZE];                    	//
		TYPE_TIME           	uiCreateTime;                               	//角色创建时间
		TYPE_TIME           	uiLastSaveTime;                             	//最后存档时间
		TYPE_TIME           	uiLogTime;                                  	//登陆时间
		TYPE_TIME           	uiEvilTime;                                 	//恶魔时间
		TYPE_UI16           	ucRace;                                     	//种族
		TYPE_UI32           	usPortrait;                                 	//头像ID
		TYPE_UI32           	usHair;                                     	//头发
		TYPE_UI16           	usHairColor;                                	//发色
		TYPE_UI32           	usFace;                                     	//脸部
		TYPE_UI16           	usLevel;                                    	//等级
		TYPE_UI32           	dwExp;                                      	//经验
		TYPE_UI16           	usMapID;                                    	//地图ID
		TYPE_POS            	iPosX;                                      	//X坐标
		TYPE_POS            	iPosY;                                      	//Y坐标
		TYPE_UI32           	uiHP;                                       	//HP
		TYPE_UI32           	uiMP;                                       	//MP
		TYPE_UI8            	usClass;                                    	//职业
		TYPE_UI8            	usPhysicsLevel;                             	//物理技能等级
		TYPE_UI8            	usMagicLevel;                               	//魔法技能等级
		TYPE_UI32           	dwPhysicsExp;                               	//物理技能熟练度
		TYPE_UI32           	dwMagicExp;                                 	//魔法技能熟练度
		TYPE_UI16           	usPhysicsAllotPoint;                        	//可分配物理技能点数
		TYPE_UI16           	usMagicAllotPoint;                          	//可分配魔法技能点数
		TYPE_UI16           	usPhyTotalGetPoint;                         	//总共获得的物理技能点数
		TYPE_UI16           	usMagTotalGetPoint;                         	//总共获得的魔法技能点数
		TYPE_UI8            	ucMaxPhyLevelLimit;                         	//最大物理技能等级限制
		TYPE_UI8            	ucMaxMagLevelLimit;                         	//最大魔法技能等级限制
		TYPE_UI8            	ucMaxSpLimit;                               	//最大的豆的限制
		TYPE_UI16           	usSpPower;                                  	//SP的能量
		TYPE_UI32           	usSpExp;                                    	//SP的熟练度
		TYPE_UI32           	dwMoney;                                    	//金钱
		TYPE_SI16           	sGoodEvilPoint;                             	//善恶值
		BYTE                	bSex;                                       	//性别
	};

	struct ItemInfo_i
	{
		TYPE_ID             	uiID;                                       	//唯一编号
		TYPE_ID             	usItemTypeID;                               	//类型编号
		TYPE_ID             	usWearPoint;                                	//耐久度
		TYPE_UI16           	usAddId;                                    	//道具的追加编号
		CHAR                	ucCount;                                    	//道具的个数
		CHAR                	ucLevelUp;                                  	//道具的升级等级
	};

	struct SkillInfo_i
	{
		TYPE_ID             	skillID;                                    	//skill的编号
		USHORT              	skillExp;                                   	//技能的经验值，只针对SP技能有用
	};

	struct BuffInfo
	{
		TYPE_ID             	uiBuffID;                                   	//产生该BUFFD的辅助技能编号
		TYPE_TIME           	uiLeftDurSeconds;                           	//该BUFF剩下的秒数
	};

	struct PunishPlayerCliInfo
	{
		UINT                	nameLen;                                    	//天谴玩家姓名长度
		CHAR                	punishPlayerName[MAX_NAME_SIZE];              	//天谴玩家姓名
		UINT                	mapID;                                      	//所在地图编号，0代表下线
		TYPE_POS            	posX;                                       	//所在坐标X
		TYPE_POS            	posY;                                       	//所在坐标Y
	};

	struct BuffID
	{
		TYPE_ID             	uiBuffTypeID;                               	//BUFF的ID
		TYPE_ID             	uiBuffID;                                   	//MapSrv上的BUFF的唯一ID号
		TYPE_ID             	uiMapSrvID;                                 	//产生该BUFF的mapsrv的ID
	};

	struct BuffDetailInfo
	{
		//Enums
		typedef	enum
		{
			UNKNOWN	=	0,      	//
		}ItemType;

		BuffID              	buffID;                                     	//
		TYPE_TIME           	nDurSeconds;                                	//该BUFF将持续的秒数,如果nDurSeconds为UNKNOWN,则说明可能是其他玩家的BUFF，客户端本身没有权限看到
	};

	struct PlayerInfo_2_Equips
	{
		//Enums
		typedef	enum
		{
			INVALIDATE      	=	0,         	//
		}ItemType;

		typedef	enum
		{
			IDNEX_HEAD      	=	0,         	//
			INDEX_BODY      	=	1,         	//
			INDEX_HAND      	=	2,         	//
			INDEX_FOOT      	=	3,         	//
			INDEX_SHOULDER  	=	4,         	//
			INDEX_BACK      	=	5,         	//
			INDEX_EARRING   	=	6,         	//
			INDEX_NECKLACE  	=	7,         	//
			INDEX_BRACELET  	=	8,         	//
			INDEX_RING      	=	9,         	//
			INDEX_MAIN_HAND 	=	10,        	//
			INDEX_ASSIS_HAND	=	11,        	//
			INDEX_TWO_HAND  	=	12,        	//
			INDEX_RIDE      	=	13,        	//
		}ItemType2;

		UINT                	equipSize;                                  	//
		ItemInfo_i          	equips[EQUIP_SIZE];                           	//
	};

	struct PlayerInfo_4_Skills
	{
		//Enums
		typedef	enum
		{
			INVALIDATE	=	0,         	//
		}ItemType;

		UINT                	skillSize;                                  	//
		SkillInfo_i         	skillData[SKILL_SIZE];                        	//
	};

	struct PlayerInfo_5_Buffers
	{
		//Enums
		typedef	enum
		{
			INVALIDATE	=	0,         	//
		}ItemType;

		UINT                	buffSize;                                   	//
		BuffInfo            	buffData[BUFF_SIZE];                          	//
	};

	struct PlayerInfoLogin
	{
		TYPE_ID             	uiID;                                       	//角色ID
		UINT                	nameSize;                                   	//角色名字
		CHAR                	szNickName[MAX_NAME_SIZE];                    	//
		TYPE_UI16           	usLevel;                                    	//等级
		TYPE_UI16           	ucRace;                                     	//种族
		TYPE_UI16           	usClass;                                    	//职业
		TYPE_UI32           	usHair;                                     	//头发
		TYPE_UI32           	usHairColor;                                	//发色
		TYPE_UI32           	usFace;                                     	//脸部
		TYPE_UI32           	usPortrait;                                 	//头像ID
		TYPE_UI16           	usMapID;                                    	//地图ID
		TYPE_POS            	iPosX;                                      	//X坐标
		TYPE_POS            	iPosY;                                      	//Y坐标
		BYTE                	bSex;                                       	//性别
		PlayerInfo_2_Equips 	equipInfo;                                  	//装备列表
	};

	struct PlayerTmpInfo
	{
		TYPE_UI32           	nMagicAttack;                               	//魔法攻击力
		TYPE_UI32           	nMagicDefend;                               	//魔法防御力
		TYPE_UI32           	nPhysicsAttack;                             	//物理攻击力
		TYPE_UI32           	nPhysicsDefend;                             	//物理防御力
		TYPE_UI32           	nPhysicsHit;                                	//物理命中
		TYPE_UI32           	nMagicHit;                                  	//魔法命中
		TYPE_UI32           	nNormalState;                               	//平时状态(位的概念)
		TYPE_UI32           	lMaxHP;                                     	//最大HP
		TYPE_UI32           	lMaxMP;                                     	//最大MP
		TYPE_UI32           	nStrength;                                  	//力量
		TYPE_UI32           	nAgility;                                   	//敏捷
		TYPE_UI32           	nIntelligence;                              	//智力
		TYPE_UI32           	lNextLevelExp;                              	//升到下一级需要的经验值
		TYPE_UI32           	lLastLevelExp;                              	//上一级升级的经验
		TYPE_UI32           	lNextPhyLevelExp;                           	//下一级物理等级需要的经验
		TYPE_UI32           	lLastPhyLevelExp;                           	//上一级物理等级需要的经验
		TYPE_UI32           	lNextMagLevelExp;                           	//下一级魔法等级需要的经验
		TYPE_UI32           	lLastMagLevelExp;                           	//上一级魔法等级需要的经验
		TYPE_UI32           	lNextPhyPointExp;                           	//下一级物理技能点需要的经验
		TYPE_UI32           	lLastPhyPointExp;                           	//上一级物理技能点需要的经验
		TYPE_UI32           	lNextMagPointExp;                           	//下一级魔法技能点需要的经验
		TYPE_UI32           	lLastMagPointExp;                           	//上一级魔法技能点需要的经验
		USHORT              	currentLevelMaxPhyPoint;                    	//当前等级可获得最大物理技能点
		USHORT              	currentLevelMaxMagPoint;                    	//当前等级可获得最大魔法技能点
		TYPE_UI32           	maxLimitPhyExp;                             	//最大可获得的物理技能经验
		TYPE_UI32           	maxLimitMagExp;                             	//最大可获得的魔法技能经验
		USHORT              	phySkillTitleSize;                          	//物理技能称号的长度
		CHAR                	phySkillTitle[16];                            	//物理技能等级称号
		USHORT              	magSkillTitleSize;                          	//魔法技能称号的长度
		CHAR                	magSkillTitle[16];                            	//魔法技能等级称号
		FLOAT               	fSpeed;                                     	//根据人物的速度档位得出的人物速度
	};

	struct PlayerBattleInfo
	{
		TYPE_UI32           	nMagicAttack;                               	//魔法攻击力
		TYPE_UI32           	nMagicDefend;                               	//魔法防御力
		TYPE_UI32           	nPhysicsAttack;                             	//物理攻击力
		TYPE_UI32           	nPhysicsDefend;                             	//物理防御力
		TYPE_UI32           	nPhysicsHit;                                	//物理命中率
		TYPE_UI32           	nMagicHit;                                  	//魔法命中率
		TYPE_UI32           	nNormalState;                               	//平时状态(位的概念)
		TYPE_UI32           	lMaxHP;                                     	//最大HP
		TYPE_UI32           	lMaxMP;                                     	//最大MP
		TYPE_UI32           	nStrength;                                  	//力量
		TYPE_UI32           	nAgility;                                   	//敏捷
		TYPE_UI32           	nIntelligence;                              	//智力
		TYPE_UI32           	lNextLevelExp;                              	//升到下一级需要的经验值
		TYPE_UI32           	lLastLevelExp;                              	//上一级升级的经验
		TYPE_UI32           	lNextPhyLevelExp;                           	//下一级物理等级需要的经验
		TYPE_UI32           	lNextMagLevelExp;                           	//下一级魔法等级需要的经验
		TYPE_UI32           	lNextPhyPointExp;                           	//下一级物理技能点需要的经验
		TYPE_UI32           	lNextMagPointExp;                           	//下一级魔法技能点需要的经验
		TYPE_UI32           	nPhyCriLevel;                               	//物理暴级等级
		TYPE_UI32           	nMagCriLevel;                               	//魔法暴击等级
		FLOAT               	fSpeed;                                     	//根据人物的速度档位得出的人物速度
	};

	struct EquipAdditonInfo
	{
		TYPE_SI32           	AddHP;                                      	//
		TYPE_SI32           	AddMP;                                      	//
		TYPE_SI32           	AddStr;                                     	//
		TYPE_SI32           	AddInt;                                     	//
		TYPE_SI32           	AddAgi;                                     	//
		TYPE_SI32           	AddPhysicsHit;                              	//
		TYPE_SI32           	AddMagicHit;                                	//
		TYPE_SI32           	AddPhysicsHitOdds;                          	//
		TYPE_SI32           	AddMagicHitOdds;                            	//
		TYPE_SI32           	AddPhysicsNum;                              	//
		TYPE_SI32           	AddMagicNum;                                	//
		TYPE_SI32           	AddPhysicsAttack;                           	//
		TYPE_SI32           	AddMagicAttack;                             	//
		TYPE_SI32           	AddDecPhyNumHarm;                           	//
		TYPE_SI32           	AddDecMagNumHarm;                           	//
		TYPE_SI32           	AddPhysicsNumAffix;                         	//
		TYPE_SI32           	AddMagicNumAffix;                           	//
		TYPE_SI32           	AddPhysicsDef;                              	//
		TYPE_SI32           	AddMagicDef;                                	//
	};

	struct WeaponPeculiarProp
	{
		TYPE_SI32           	mMasslevel;                                 	//一个道具等级
		TYPE_SI32           	mTrade;                                     	//交易方式
		TYPE_SI32           	mCannotChuck;                               	//是否可丢弃
		TYPE_SI32           	mDeathLost;                                 	//死忙是否掉落
		TYPE_SI32           	mDeopt;                                     	//是否可存仓
		TYPE_SI32           	AttackPhyMax;                               	//攻击的最大值
		TYPE_SI32           	AttackPhyMin;                               	//攻击的最小值
		TYPE_SI32           	AttackMagMax;                               	//魔法攻击最大值
		TYPE_SI32           	AttackMagMin;                               	//魔法攻击的最小值
	};

	struct EquipPeculiarProp
	{
		TYPE_SI32           	mMasslevel;                                 	//一个道具等级
		TYPE_SI32           	mTrade;                                     	//交易方式
		TYPE_SI32           	mCannotChuck;                               	//是否可丢弃
		TYPE_SI32           	mDeathLost;                                 	//死忙是否掉落
		TYPE_SI32           	mDeopt;                                     	//是否可存仓
		TYPE_SI32           	mPhysicsDefend;                             	//物理防御力
		TYPE_SI32           	mMagicDefend;                               	//魔法防御力
		TYPE_SI32           	mPhysicsAttack;                             	//物理攻击力
		TYPE_SI32           	mMagicAttack;                               	//魔法攻击力
	};

	struct WeaponAdditionProp
	{
		WeaponPeculiarProp  	peculiarProp;                               	//武器特有属性
		EquipAdditonInfo    	addInfo;                                    	//附加信息
	};

	struct EquipAdditionProp
	{
		EquipPeculiarProp   	peculiarProp;                               	//装备的特有属性
		EquipAdditonInfo    	addInfo;                                    	//装备附加信息
	};

	struct EquipItemInfo
	{
		TYPE_ID             	mItemUID;                                   	//唯一性Item编号
		TYPE_ID             	mItemTypeID;                                	//道具在XML中的编号
		TYPE_SI32           	mType;                                      	//武器 0  装备1
		TYPE_SI32           	mMasslevel;                                 	//一个道具等级
		TYPE_SI32           	mTrade;                                     	//交易方式
		TYPE_SI32           	mCannotChuck;                               	//是否可丢弃
		TYPE_SI32           	mDeathLost;                                 	//死忙是否掉落
		TYPE_SI32           	mDeopt;                                     	//是否可存仓
		TYPE_SI32           	mPhysicsDefend;                             	//type = 0 物理防御力 type = 1 攻击的最小值
		TYPE_SI32           	mPhysicsAttack;                             	//type = 0 物理攻击力 type = 1 攻击的最大值
		TYPE_SI32           	mMagicAttack;                               	//type = 0 魔法攻击力 type = 1  魔法攻击最大值
		TYPE_SI32           	mMagicDefend;                               	//type = 0 魔法防御力 type = 1 魔法攻击的最小值
		EquipAdditonInfo    	addInfo;                                    	//装备的附加属性
	};

	struct ItemAddtionProp
	{
		TYPE_SI32           	Hp;                                         	//
		TYPE_SI32           	Mp;                                         	//
		TYPE_SI32           	PhysicsAttack;                              	//
		TYPE_SI32           	MagicAttack;                                	//
		TYPE_SI32           	PhysicsDefend;                              	//
		TYPE_SI32           	MagicDefend;                                	//
		TYPE_SI32           	PhysicsHit;                                 	//
		TYPE_SI32           	MagicHit;                                   	//
		TYPE_SI32           	PhysicsCritical;                            	//
		TYPE_SI32           	MagicCritical;                              	//
		TYPE_SI32           	PhysicsAttackPercent;                       	//
		TYPE_SI32           	MagicAttackPercent;                         	//
		TYPE_SI32           	PhysicsCriticalDamage;                      	//
		TYPE_SI32           	MagicCriticalDamage;                        	//
		TYPE_SI32           	PhysicsDefendPercent;                       	//
		TYPE_SI32           	MagicDefendPercent;                         	//
		TYPE_SI32           	PhysicsCriticalDerate;                      	//
		TYPE_SI32           	MagicCriticalDerate;                        	//
		TYPE_SI32           	PhysicsCriticalDamageDerate;                	//
		TYPE_SI32           	MagicCriticalDamageDerate;                  	//
		TYPE_SI32           	PhysicsJouk;                                	//
		TYPE_SI32           	MagicJouk;                                  	//
		TYPE_SI32           	PhysicsDeratePercent;                       	//
		TYPE_SI32           	MagicDeratePercent;                         	//
		TYPE_SI32           	Stun;                                       	//
		TYPE_SI32           	Tie;                                        	//
		TYPE_SI32           	Antistun;                                   	//
		TYPE_SI32           	Antitie;                                    	//
		TYPE_SI32           	PhysicsAttackAdd;                           	//
		TYPE_SI32           	MagicAttackAdd;                             	//
		TYPE_SI32           	PhysicsRift;                                	//
		TYPE_SI32           	MagicRift;                                  	//
		TYPE_SI32           	AddSpeed;                                   	//
		TYPE_SI32           	AttackPhyMin;                               	//
		TYPE_SI32           	AttackPhyMax;                               	//
		TYPE_SI32           	AttackMagMin;                               	//
		TYPE_SI32           	AttackMagMax;                               	//
	};

	struct TaskRecordInfo
	{
		TYPE_SI32           	taskID;                                     	//任务编号
		TYPE_UI16           	completeCount;                              	//任务完成次数
		TYPE_UI16           	processData;                                	//任务完成的状态数据
		TYPE_UI32           	taskTime;                                   	//任务开始的时间
		ETaskState          	taskState;                                  	//任务完成的状态
	};

	struct FriendGroupInfo
	{
		TYPE_ID             	groupID;                                    	//组号,用于与C--S间通信
		UINT                	groupNameSize;                              	//组名长度
		CHAR                	groupName[MAX_NAME_SIZE];                     	//组名，用于客户端显示
	};

	struct FriendInfo
	{
		TYPE_ID             	friendID;                                   	//好友编号,用于与C--S间通信
		TYPE_ID             	joinID;                                     	//加入序号，按加入的先后顺序编号，用于客户端排序显示；
		TYPE_ID             	groupID;                                    	//该好友所属组的组号
		UINT                	nickNameSize;                               	//好友伲称长度
		CHAR                	nickName[MAX_NAME_SIZE];                      	//好友伲称
		CHAR                	isOnline;                                   	//是否在线
		bool                	isHateMe;                                   	//该好友是否将自己作为仇人，用于客户端显示
		BYTE                	playerSex;                                  	//性别 0x01男 0x02女
		USHORT              	mapID;                                      	//玩家所在的地图
		USHORT              	playerLevel;                                	//等级
		USHORT              	playerClass;                                	//职业
		USHORT              	playerRace;                                 	//种族
		USHORT              	playerIcon;                                 	//玩家的icon
		UINT                	GuildNameLen;                               	//工会名称长度
		CHAR                	GuildName[MAX_NAME_SIZE];                     	//公会伲称
		UINT                	FereNameLen;                                	//伴侣名称长度
		CHAR                	FereName[MAX_NAME_SIZE];                      	//伴侣伲称
	};

	struct FoeInfo
	{
		TYPE_ID             	foeID;                                      	//仇人编号,用于与C--S间通信
		TYPE_ID             	joinID;                                     	//加入仇人名单的历史序号，用于客户端排序显示
		UINT                	nickNameSize;                               	//仇人昵称长度
		CHAR                	nickName[MAX_NAME_SIZE];                      	//好友伲称
		PlayerID            	playerID;                                   	//在线时有效，仇人的PlayerID信息
		USHORT              	mapID;                                      	//玩家的所在的地图
		USHORT              	playerLevel;                                	//等级
		USHORT              	playerClass;                                	//职业
		USHORT              	playerRace;                                 	//种族
		USHORT              	playerIcon;                                 	//玩家的icon
		BYTE                	playerSex;                                  	//性别
		bool                	isLocked;                                   	//是否被锁定(不可被新仇人覆盖)
		bool                	isHateMe;                                   	//该好友是否将自己作为仇人，用于客户端显示
		CHAR                	isOnline;                                   	//是否在线
		UINT                	GuildNameLen;                               	//工会名称长度
		CHAR                	GuildName[MAX_NAME_SIZE];                     	//公会伲称
		UINT                	FereNameLen;                                	//伴侣名称长度
		CHAR                	FereName[MAX_NAME_SIZE];                      	//伴侣伲称
	};

	struct BlackListItemInfo
	{
		TYPE_ID             	itemID;                                     	//黑名单中的玩家编号,用于与C--S间通信
		TYPE_ID             	joinID;                                     	//加入黑名单的历史序号，用于客户端排序显示
		UINT                	nickNameSize;                               	//黑名单中的玩家昵称长度
		CHAR                	nickName[MAX_NAME_SIZE];                      	//好友伲称
	};

	struct FoeDetailInfo
	{
		TYPE_ID             	foeID;                                      	//黑名单中的玩家编号,用于与C--S间通信,其他信息待加
	};

	struct WearChangedItems
	{
		TYPE_ID             	itemID;                                     	//物品唯一编号
		TYPE_ID             	newWear;                                    	//物品的新的耐久度
	};

	struct ThreatCliInfo
	{
		PlayerID            	threatPlayerID;                             	//威胁的玩家
		TYPE_ID             	threatValue;                                	//威胁值
	};

	struct ThreatList
	{
		USHORT              	threatCount;                                	//威胁列表里的有效值
		ThreatCliInfo       	threatList[10];                               	//具体每个威胁的成员
	};

	struct GroupChatMemberInfo
	{
		USHORT              	memberNameSize;                             	//群聊成员名字长度
		CHAR                	memberName[MAX_NAME_SIZE];                    	//成员名字
	};

	struct UnionMember
	{
		UINT                	gateId;                                     	//
		UINT                	playerId;                                   	//
		UINT                	uiid;                                       	//
		USHORT              	nameLen;                                    	//
		CHAR                	name[MAX_PLAYERNAME_LEN];                     	//
		USHORT              	job;                                        	//
		USHORT              	posSeq;                                     	//
		USHORT              	titleLen;                                   	//
		CHAR                	title[MAX_PLAYERNAME_LEN];                    	//
		USHORT              	level;                                      	//
		UINT                	lastQuit;                                   	//
		USHORT              	forbid;                                     	//
		USHORT              	onLine;                                     	//
	};

	struct UnionPosRight
	{
		USHORT              	seq;                                        	//
		USHORT              	posLen;                                     	//
		CHAR                	pos[8];                                       	//
		UINT                	right;                                      	//
	};

	struct Union
	{
		UINT                	id;                                         	//
		USHORT              	nameLen;                                    	//
		CHAR                	name[MAX_UNIONNAME_LEN];                      	//
		USHORT              	level;                                      	//
		USHORT              	num;                                        	//
		UINT                	ownerUiid;                                  	//
		USHORT              	titleLen;                                   	//
		CHAR                	title[MAX_UNIONTITLE_LEN];                    	//
		UINT                	active;                                     	//
		USHORT              	posListLen;                                 	//
		UnionPosRight       	posList[10];                                  	//
	};

	struct UnionMembers
	{
		USHORT              	count;                                      	//
		UnionMember         	members[4];                                   	//
	};

	struct MailConciseInfo
	{
		UINT                	mailuid;                                    	//
		CHAR                	nameLen;                                    	//
		CHAR                	sendPlayerName[MAX_NAME_SIZE];                	//
		CHAR                	titleLen;                                   	//
		CHAR                	mailTitle[MAX_MAIL_CONCISE_TITLE_LEN];        	//
		UINT                	expireTime;                                 	//
		bool                	isRead;                                     	//
	};

	struct CGPlayerBeyondBlock
	{
		static const USHORT	wCmd = 0x0169;

		FLOAT               	fCurX;                                      	//玩家当前浮点坐标X
		FLOAT               	fCurY;                                      	//玩家当前浮点坐标Y
		FLOAT               	fTargetX;                                   	//玩家目标点浮点坐标X
		FLOAT               	fTargetY;                                   	//玩家目标点浮点坐标Y
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPlayerBeyondBlock);

	struct CGAddActivePtReq
	{
		static const USHORT	wCmd = 0x0164;

		UINT                	reqID;                                      	//请求号，服务器返回添加结果以及活点信息时使用此ID号，玩家短时间内连续添加多个活点时，以此区别不同添加目标
		UINT                	nickNameSize;                               	//活点目标名字长度
		CHAR                	nickName[MAX_NAME_SIZE];                      	//活点目标名字
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddActivePtReq);

	struct CGQueryEvilTime
	{
		static const USHORT	wCmd = 0x0165;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryEvilTime);

	struct CGUseDamageItem
	{
		static const USHORT	wCmd = 0x0166;

		TYPE_ID             	itemTypeID;                                 	//使用道具的ID
		TYPE_POS            	attackPosX;                                 	//使用道具的攻击坐标X
		TYPE_POS            	attackPosY;                                 	//使用道具的攻击坐标Y
		PlayerID            	targetID;                                   	//攻击对象的ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGUseDamageItem);

	struct CGAddSthToPlate
	{
		static const USHORT	wCmd = 0x013c;

		EPlateType          	plateType;                                  	//托盘类型
		TYPE_ID             	platePos;                                   	//放置的位置
		ItemInfo_i          	sthID;                                      	//所添加物品的ID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddSthToPlate);

	struct CGTakeSthFromPlate
	{
		static const USHORT	wCmd = 0x013d;

		TYPE_ID             	platePos;                                   	//取走位置
		ItemInfo_i          	sthID;                                      	//所取走物品的ID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGTakeSthFromPlate);

	struct CGPlateExec
	{
		static const USHORT	wCmd = 0x013e;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPlateExec);

	struct CGClosePlate
	{
		static const USHORT	wCmd = 0x013f;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGClosePlate);

	struct CGQueryPkg
	{
		static const USHORT	wCmd = 0x0100;

		TYPE_ID             	nGroupID;                                   	//请求组号，将返回第
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryPkg);

	struct CGNetDetect
	{
		static const USHORT	wCmd = 0x0101;

		TYPE_ID             	pkgID;                                      	//检测包编号；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGNetDetect);

	struct CGChat
	{
		static const USHORT	wCmd = 0x0102;

		CHAT_TYPE           	nType;                                      	//消息类型：如群聊，密聊，组队内通话等；
		PlayerID            	targetPlayerID;                             	//如果是密聊，则为密聊对象；
		UINT                	tgtNameSize;                                	//聊天对象名字长度
		CHAR                	tgtName[MAX_NAME_SIZE];                       	//聊天对象名字，发话者不知道对象ID时使用
		UINT                	chatSize;                                   	//聊天消息内容长度
		CHAR                	strChat[MAX_CHAT_SIZE];                       	//聊天消息内容
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChat);

	struct CGTimeCheck
	{
		static const USHORT	wCmd = 0x0103;

		TYPE_ID             	checkID;                                    	//
		TYPE_ID             	sequenceID;                                 	//网络序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGTimeCheck);

	struct CGReqRoleInfo
	{
		static const USHORT	wCmd = 0x0104;

		PlayerID            	lPlayerID;                                  	//玩家ID号，请求此玩家的当前角色信息；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGReqRoleInfo);

	struct CGReqMonsterInfo
	{
		static const USHORT	wCmd = 0x0105;

		TYPE_ID             	lMonsterID;                                 	//怪物ID号，请求此怪物的相关信息；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGReqMonsterInfo);

	struct CGRun
	{
		static const USHORT	wCmd = 0x0106;

		PlayerID            	lPlayerID;                                  	//玩家ID号；
		FLOAT               	fOrgWorldPosX;                              	//移动出发点X客户端坐标；
		FLOAT               	fOrgWorldPosY;                              	//移动出发点Y客户端坐标；
		FLOAT               	fNewWorldPosX;                              	//移动目标点X客户端坐标；
		FLOAT               	fNewWorldPosY;                              	//移动目标点Y客户端坐标；
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRun);

	struct CGRequestRandomString
	{
		static const USHORT	wCmd = 0x014e;

		UINT                	accountSize;                                	//
		CHAR                	strUserAccount[32];                           	//
		UINT                	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRequestRandomString);

	struct CGUserMD5Value
	{
		static const USHORT	wCmd = 0x014f;

		UINT                	md5ValueSize;                               	//
		CHAR                	md5Value[MD5_VALUE_CHARLEN];                  	//
		UINT                	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGUserMD5Value);

	struct CGLogin
	{
		static const USHORT	wCmd = 0x0107;

		UINT                	accountSize;                                	//
		CHAR                	strUserAccount[32];                           	//
		UINT                	passwdSize;                                 	//
		CHAR                	strUserPassword[32];                          	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGLogin);

	struct CGSelectRole
	{
		static const USHORT	wCmd = 0x0108;

		TYPE_ID             	uiRoleNo;                                   	//所选角色ID号；
		TYPE_ID             	sequenceID;                                 	//序列号ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSelectRole);

	struct CGEnterWorld
	{
		static const USHORT	wCmd = 0x0109;

		TYPE_ID             	sequenceID;                                 	//序列号ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGEnterWorld);

	struct CGIrcEnterWorld
	{
		static const USHORT	wCmd = 0x015e;

		TYPE_ID             	sequenceID;                                 	//序列号ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGIrcEnterWorld);

	struct GCIrcEnterWorldRst
	{
		static const USHORT	wCmd = 0x10b1;

		bool                	bSuccess;                                   	//IRC登陆成功与否
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCIrcEnterWorldRst);

	struct CGSwitchEnterNewMap
	{
		static const USHORT	wCmd = 0x010a;

		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSwitchEnterNewMap);

	struct CGAttackMonster
	{
		static const USHORT	wCmd = 0x010b;

		TYPE_ID             	lMonsterID;                                 	//怪物ID，地图内唯一；
		PlayerID            	attackerID;                                 	//攻击者自身ID号；
		TYPE_ID             	lSkillType;                                 	//攻击所使用的方式或技能ID
		TYPE_POS            	nAttackPosX;                                	//技能的目标点X(无目标技能用)
		TYPE_POS            	nAttackPosY;                                	//技能的目标点Y(无目标技能用)
		FLOAT               	nAttackerPosX;                              	//攻击者位置x
		FLOAT               	nAttackerPosY;                              	//攻击者位置y
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAttackMonster);

	struct CGAttackPlayer
	{
		static const USHORT	wCmd = 0x010c;

		PlayerID            	attackerID;                                 	//攻击者ID号；
		PlayerID            	targetID;                                   	//被攻击者ID号；
		TYPE_ID             	lSkillType;                                 	//攻击所使用的方式或技能ID
		FLOAT               	nAttackerPosX;                              	//攻击者位置x
		FLOAT               	nAttackerPosY;                              	//攻击者位置y
		TYPE_ID             	sequenceID;                                 	//网络序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAttackPlayer);

	struct CGSwitchMap
	{
		static const USHORT	wCmd = 0x010d;

		PlayerID            	playerID;                                   	//
		TYPE_ID             	usMapID;                                    	//地图ID
		TYPE_POS            	iPosX;                                      	//X坐标
		TYPE_POS            	iPosY;                                      	//Y坐标
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSwitchMap);

	struct CGCreateRole
	{
		static const USHORT	wCmd = 0x010e;

		UINT                	nameSize;                                   	//
		CHAR                	strNickName[32];                              	//
		TYPE_UI16           	wRace;                                      	//种族；
		TYPE_UI16           	wSex;                                       	//性别；
		TYPE_UI32           	nClass;                                     	//人物职业
		TYPE_UI32           	nFace;                                      	//脸型
		TYPE_UI32           	nHair;                                      	//发型
		TYPE_UI16           	usPortrait;                                 	//肖像
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGCreateRole);

	struct CGDeleteRole
	{
		static const USHORT	wCmd = 0x010f;

		TYPE_INDEX          	usRoleIndex;                                	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDeleteRole);

	struct CGScriptChat
	{
		static const USHORT	wCmd = 0x0110;

		SCRIPT_TYPE         	tgtType;                                    	//对象类型，item脚本或npc脚本
		TYPE_ID             	tgtID;                                      	//对象ID，npcID或itemID
		TYPE_SI32           	nOption;                                    	//玩家的选项，对于初始点击（开始对话），nOption填0；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGScriptChat);

	struct CGQueryShop
	{
		static const USHORT	wCmd = 0x015f;

		TYPE_ID             	shopID;                                     	//商店ID号
		TYPE_ID             	shopPageID;                                 	//商店页码
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryShop);

	struct CGShopBuyReq
	{
		static const USHORT	wCmd = 0x0160;

		TYPE_ID             	shopID;                                     	//商店ID号
		TYPE_ID             	shopPageID;                                 	//商店页码
		UINT                	shopPagePos;                                	//商品在页中位置
		TYPE_ID             	itemID;                                     	//商品类型ID号
		UINT                	itemNum;                                    	//一次购买的商品数量
		TYPE_ID             	moneyType;                                  	//货币类型ID号
		UINT                	moneyNum;                                   	//购买使用的货币数量
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGShopBuyReq);

	struct CGItemSoldReq
	{
		static const USHORT	wCmd = 0x0161;

		TYPE_ID             	itemID;                                     	//物品类型ID号
		UINT                	itemPos;                                    	//售出物品在包裹中的位置
		UINT                	itemNum;                                    	//售出数量
		UINT                	moneyNum;                                   	//本次售出希望获得的金钱数
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGItemSoldReq);

	struct CGQueryTaskRecordInfo
	{
		static const USHORT	wCmd = 0x0151;

		TYPE_UI32           	taskID;                                     	//查询的任务ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryTaskRecordInfo);

	struct CGDeleteTask
	{
		static const USHORT	wCmd = 0x0154;

		TYPE_UI32           	taskID;                                     	//删除任务的编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDeleteTask);

	struct CGShareTask
	{
		static const USHORT	wCmd = 0x0155;

		TYPE_UI32           	taskID;                                     	//共享任务的编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGShareTask);

	struct GCUpdateTaskRecordInfo
	{
		static const USHORT	wCmd = 0x109b;

		TaskRecordInfo      	taskRecord;                                 	//任务的存储信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUpdateTaskRecordInfo);

	struct GCTaskHistory
	{
		static const USHORT	wCmd = 0x109c;

		UINT                	arrSize;                                    	//
		UINT                	ArrTaskHistory[30];                           	//存储的关于做过的任务的记录
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTaskHistory);

	struct CGDefaultRebirth
	{
		static const USHORT	wCmd = 0x0111;

		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDefaultRebirth);

	struct CGRebirthReady
	{
		static const USHORT	wCmd = 0x0112;

		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRebirthReady);

	struct CGPlayerCombo
	{
		static const USHORT	wCmd = 0x0113;

		PlayerID            	playerID;                                   	//玩家自身ID号；
		TYPE_NUM            	comboNum;                                   	//连击数；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPlayerCombo);

	struct GCPlayerNewTask
	{
		static const USHORT	wCmd = 0x1085;

		TYPE_ID             	taskID;                                     	//任务标识号(服务器端分配，与玩家的后续通信中标识特定任务)
		TYPE_ID             	taskTypeID;                                 	//任务类型标识号(对应Missions.xml中的任务ID号)
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerNewTask);

	struct GCPlayerTaskStatus
	{
		static const USHORT	wCmd = 0x1086;

		TYPE_ID             	taskID;                                     	//任务标识号(服务器端分配，与玩家的通信中标识特定任务)
		ETaskStatType       	taskStat;                                   	//任务状态
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerTaskStatus);

	struct GCShowShop
	{
		static const USHORT	wCmd = 0x10ad;

		TYPE_ID             	shopID;                                     	//商店ID号
		TYPE_ID             	shopPageID;                                 	//商店页码
		UINT                	itemArrSize;                                	//物品数量列表中的元素个数
		UINT                	itemNumArr[ITEMNUM_PER_SHOPLABEL];            	//商店中该页中的各位置物品当前数目
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCShowShop);

	struct GCShopBuyRst
	{
		//Enums
		typedef	enum
		{
			BUY_SUCCESS      	=	0,          	//购买成功
			NOT_ENOUGH_MONEY 	=	1,          	//没有足够的货币
			NOT_ENOUGH_SPACE 	=	2,          	//购买者包裹中没有足够空间
			ERROR_ITEM       	=	3,          	//错误的物品(没有指定的商店，没有指定页，没有指定位置的物品，指定位置的物品与购买请求中的物品不符等，提示玩家配置错，下载最新的版本等)
			NOT_ENOUGH_ITEM  	=	4,          	//没有足够的可售数量，物品已卖完
			ERROR_PLAYER_COND	=	5,          	//错误的玩家条件(玩家等级、声望等)
			OTHER_ERROR      	=	6,          	//其它错误
		}BuyRstType;

		static const USHORT	wCmd = 0x10ae;

		TYPE_ID             	shopID;                                     	//商店ID号
		TYPE_ID             	shopPageID;                                 	//商店页码
		UINT                	shopPagePos;                                	//商品在页中位置
		TYPE_ID             	itemID;                                     	//商品类型ID号
		UINT                	itemNum;                                    	//一次购买的商品数量
		TYPE_ID             	moneyType;                                  	//货币类型ID号
		UINT                	moneyNum;                                   	//购买使用的货币数量
		UINT                	itemPos;                                    	//购买成功以后，新买物品在包裹中的位置
		BuyRstType          	buyRst;                                     	//购买结果
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCShopBuyRst);

	struct GCItemSoldRst
	{
		//Enums
		typedef	enum
		{
			SOLD_SUCCESS   	=	0,           	//售出成功
			NO_ITEM        	=	1,           	//没有足够的售出物品或玩家包裹指定位置没有待售出物品
			CAN_NOT_SOLD   	=	2,           	//物品不可售出
			SOLD_COND_ERROR	=	3,           	//售出条件有错(例如：卖不到玩家希望的金钱数等，客户端的配置与服务器不一致)
			OTHER_ERROR    	=	4,           	//其它错误
		}SoldRstType;

		static const USHORT	wCmd = 0x10af;

		TYPE_ID             	itemID;                                     	//物品类型ID号
		UINT                	itemPos;                                    	//售出物品在包裹中的位置
		UINT                	itemNum;                                    	//售出数量
		UINT                	moneyNum;                                   	//本次售出希望获得的金钱数
		UINT                	playerMoneyNum;                             	//本次售出后玩家的金钱总数
		SoldRstType         	soldRst;                                    	//本次售出结果
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCItemSoldRst);

	struct GCAddActivePtRst
	{
		//Enums
		typedef	enum
		{
			ADDAPT_SUCCESS   	=	0,             	//
			ADDAPT_NOPLAYER  	=	1,             	//
			ADDAPT_OTHERERROR	=	2,             	//
		}AddAPTRst;

		static const USHORT	wCmd = 0x10b6;

		AddAPTRst           	reqRst;                                     	//加活点结果
		UINT                	reqID;                                      	//请求顺序号
		PlayerID            	tgtPlayerID;                                	//活点对应玩家ID号
		TYPE_POS            	posX;                                       	//活点位置信息X
		TYPE_POS            	posY;                                       	//活点位置信息Y
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddActivePtRst);

	struct GCEvilTimeRst
	{
		static const USHORT	wCmd = 0x10b7;

		UINT                	evilTime;                                   	//恶魔在线的时间
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCEvilTimeRst);

	struct GCShowPlate
	{
		static const USHORT	wCmd = 0x1077;

		EPlateType          	plateType;                                  	//弹出托盘类型
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCShowPlate);

	struct GCAddSthToPlateRst
	{
		//Enums
		typedef	enum
		{
			ADD_STH_OK                   	=	0,         	//
			E_ITEM_INVALID               	=	1,         	//
			E_NOT_CHANGE_ITEM            	=	2,         	//
			E_NOT_UPDATE_ITEM            	=	3,         	//
			E_NOT_ADD_ITEM               	=	4,         	//
			E_NOT_CHANGE_CONDITION       	=	5,         	//
			E_NOT_UPDATE_CONDITION       	=	6,         	//
			E_NOT_ADD_CONDITION          	=	7,         	//
			E_TAKE_ITEM_BEFORE_ADD       	=	8,         	//
			E_UPDATE_ITEM_NOT_REPLATE    	=	9,         	//
			E_UPDATE_AXIOLOGY_NOT_REPLACE	=	10,        	//
		}AddRstType;

		static const USHORT	wCmd = 0x1078;

		TYPE_ID             	platePos;                                   	//放置的位置
		ItemInfo_i          	sthID;                                      	//所添加物品的ID号
		AddRstType          	addRst;                                     	//放置结果
		bool                	isConFulfil;                                	//放物品后条件是否满足
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddSthToPlateRst);

	struct GCTakeSthFromPlateRst
	{
		//Enums
		typedef	enum
		{
			TAKE_STH_OK  	=	0,          	//
			TAKE_STH_FAIL	=	1,          	//
		}TakeRstType;

		static const USHORT	wCmd = 0x1079;

		TYPE_ID             	platePos;                                   	//取走位置
		ItemInfo_i          	sthID;                                      	//所取物品的ID号
		TakeRstType         	takeRst;                                    	//取物品结果
		bool                	isConFulfil;                                	//取物品后条件是否满足
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTakeSthFromPlateRst);

	struct GCPlateExecRst
	{
		//Enums
		typedef	enum
		{
			PLATE_EXEC_OK  	=	0,            	//
			PLATE_EXEC_FAIL	=	1,            	//
		}PlateExecRstType;

		static const USHORT	wCmd = 0x107a;

		PlateExecRstType    	execRst;                                    	//取物品结果
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlateExecRst);

	struct GCClosePlate
	{
		static const USHORT	wCmd = 0x107b;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCClosePlate);

	struct GCNetDetect
	{
		static const USHORT	wCmd = 0x1000;

		TYPE_ID             	pkgID;                                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCNetDetect);

	struct GCNetBias
	{
		static const USHORT	wCmd = 0x1001;

		TYPE_ID             	pkgID;                                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCNetBias);

	struct GCChat
	{
		static const USHORT	wCmd = 0x1002;

		CHAT_TYPE           	nType;                                      	//消息类型：如群聊，密聊，组队内通话等；
		PlayerID            	sourcePlayerID;                             	//如果是密聊，则为发出密聊信息者；
		UINT                	srcNameSize;                                	//说话者名字长度
		CHAR                	srcName[MAX_NAME_SIZE];                       	//说话者名字，说话者不在同一gate时使用
		UINT                	chatSize;                                   	//聊天消息内容，包括末尾的\0
		CHAR                	strChat[MAX_CHAT_SIZE];                       	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChat);

	struct GCRoleattUpdate
	{
		static const USHORT	wCmd = 0x1003;

		PlayerID            	lChangedPlayerID;                           	//属性改变的玩家ID号；
		ROLEATT_TYPE        	nType;                                      	//发生改变的属性所属类型：如血量，魔法值等
		TYPE_NUM            	nOldValue;                                  	//
		TYPE_NUM            	nNewValue;                                  	//
		TYPE_TIME           	changeTime;                                 	//更改的时间 0就是永久
		INT                 	nAddType;                                   	//是直接设置新值，还是表现为 oldvalue+改变的值
		bool                	bFloat;                                     	//是否是浮点型,是的除以100
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRoleattUpdate);

	struct GCPlayerLevelUp
	{
		//Enums
		typedef	enum
		{
			IN_BATTLE	=	0,        	//
			IN_TASK  	=	1,        	//
		}ItemType;

		static const USHORT	wCmd = 0x1004;

		PlayerID            	playerID;                                   	//玩家ID号；
		TYPE_NUM            	nNewLevel;                                  	//新升的等级
		ItemType            	levelUpType;                                	//升级的状态(客户端用)
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerLevelUp);

	struct GCTmpInfoUpdate
	{
		static const USHORT	wCmd = 0x1005;

		PlayerID            	playerID;                                   	//玩家ID号；
		PlayerTmpInfo       	tmpInfo;                                    	//更新的非存盘数据
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTmpInfoUpdate);

	struct GCPlayerPkg
	{
		static const USHORT	wCmd = 0x1006;

		PlayerID            	playerID;                                   	//
		TYPE_INDEX          	ucIndex;                                    	//第几页
		UINT                	pkgSize;                                    	//
		CHAR                	playerPkg[PKG_GROUP_NUM];                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerPkg);

	struct GCTimeCheck
	{
		static const USHORT	wCmd = 0x1007;

		TYPE_ID             	checkID;                                    	//新升的等级
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTimeCheck);

	struct GCRun
	{
		//Enums
		typedef	enum
		{
			RUNRST_OK           	=	0,        	//
			RUNRST_TARGETINVALID	=	1,        	//
		}ItemType;

		static const USHORT	wCmd = 0x1008;

		PlayerID            	lPlayerID;                                  	//玩家ID号；
		ItemType            	nErrCode;                                   	//RUNRST_OK:跑动正常，RUNRST_TARGETINVALID:目标点无效
		FLOAT               	fOrgWorldPosX;                              	//移动出发点X客户端坐标；
		FLOAT               	fOrgWorldPosY;                              	//移动出发点Y客户端坐标；
		FLOAT               	fSpeed;                                     	//移动速度；
		FLOAT               	fNewWorldPosX;                              	//移动目标点X客户端坐标；
		FLOAT               	fNewWorldPosY;                              	//移动目标点Y客户端坐标；
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRun);

	struct GCPlayerPosInfo
	{
		static const USHORT	wCmd = 0x1009;

		PlayerID            	lPlayerID;                                  	//玩家ID号；
		TYPE_ID             	lMapCode;                                   	//所在地图代号；
		TYPE_POS            	nMapPosX;                                   	//所在点X坐标；
		TYPE_POS            	nMapPosY;                                   	//所在点Y坐标；
		FLOAT               	fWorldPosX;                                 	//所在点X客户端坐标；
		FLOAT               	fWorldPosY;                                 	//所在点Y客户端坐标；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerPosInfo);

	struct GCPlayerSelfInfo
	{
		static const USHORT	wCmd = 0x100a;

		PlayerID            	lPlayerID;                                  	//玩家自身ID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerSelfInfo);

	struct GCPlayerAppear
	{
		static const USHORT	wCmd = 0x100b;

		PlayerID            	lPlayerID;                                  	//玩家ID号；
		PlayerInfo_1_Base   	playerInfo;                                 	//玩家详细信息；
		PlayerInfo_4_Skills 	playerSkills;                               	//
		EAppearType         	appearType;                                 	//出现类型
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerAppear);

	struct GCPlayerLeave
	{
		static const USHORT	wCmd = 0x100c;

		PlayerID            	lPlayerID;                                  	//玩家ID号；
		TYPE_ID             	lMapCode;                                   	//离开地图名；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerLeave);

	struct GCRandomString
	{
		static const USHORT	wCmd = 0x1099;

		UINT                	rdStrSize;                                  	//
		CHAR                	randomString[MD5_RDSTR_LEN];                  	//
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRandomString);

	struct GCPlayerLogin
	{
		//Enums
		typedef	enum
		{
			ISLOGINOK_SUCCESS               	=	0,                	//
			ISLOGINOK_FAILED_ACCOUNTNOTEXIST	=	1,                	//
			ISLOGINOK_FAILED_PWDWRONG       	=	2,                	//
			ISLOGINOK_FAILED_ONLINE         	=	3,                	//
			ISLOGINOK_FAILED_LOCKED         	=	4,                	//
		}ItemType;

		static const USHORT	wCmd = 0x100d;

		TYPE_FLAG           	byIsLoginOK;                                	//1:登录正常，其它值表明登录失败原因（2:帐号不存在,3:密码错误,4:同帐号玩家在线，其它待定）
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerLogin);

	struct GCPlayerRole
	{
		//Enums
		typedef	enum
		{
			ISERR_OK           	=	0,       	//
			ISERR_FAILED_NOROLE	=	1,       	//
			FLAG_ITEM          	=	2,       	//
			FLAG_END           	=	3,       	//
		}ItemType;

		static const USHORT	wCmd = 0x100e;

		TYPE_FLAG           	nErrCode;                                   	//1:ISERR_OK:无错，ISERR_FAILED_NOROLE:无相应角色
		TYPE_FLAG           	byFlag;                                     	//1:FLAG_ITEM为正常通知，FLAG_END为通知列表结束；
		PlayerInfoLogin     	playerInfo;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerRole);

	struct GCOtherPlayerInfo
	{
		//Enums
		typedef	enum
		{
			ISERR_OK           	=	0,       	//
			ISERR_FAILED_NOROLE	=	1,       	//
		}ItemType;

		static const USHORT	wCmd = 0x100f;

		PlayerID            	otherPlayerID;                              	//该玩家的ID号；
		TYPE_FLAG           	nErrCode;                                   	//ISERR_OK:无错，ISERR_FAILED_NOROLE:无相应角色
		PlayerInfoLogin     	playerInfo;                                 	//玩家简单信息；
		TYPE_NUM            	nCurHp;                                     	//当前Hp
		TYPE_NUM            	nMaxHp;                                     	//最大Hp
		FLOAT               	fSpeed;                                     	//移动速度，如果小于等于0则玩家静止
		FLOAT               	fTargetX;                                   	//目标点X浮点坐标
		FLOAT               	fTargetY;                                   	//目标点Y浮点坐标
		TYPE_NUM            	nCurMagic;                                  	//当前魔法
		TYPE_NUM            	nMaxMagic;                                  	//最大魔法
		bool                	isInTeam;                                   	//是否在组队
		bool                	isTeamCaptain;                              	//是否在组队的队长
		TYPE_SI16           	sGoodEvilPoint;                             	//善恶值
		bool                	bRogue;                                     	//是否在流氓状态
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCOtherPlayerInfo);

	struct GCSelectRole
	{
		//Enums
		typedef	enum
		{
			ISSELECTOK_SUCCESS	=	0,                 	//
			ISSELECTOK_FAILED 	=	1,                 	//
		}ItemType;

		static const USHORT	wCmd = 0x1010;

		TYPE_FLAG           	byIsSelectOK;                               	//ISSELECTOK_SUCCESS为选角色成功，ISSELECTOK_FAILED失败；
		TYPE_INDEX          	uiRoleNo;                                   	//所选角色序号；
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSelectRole);

	struct GCCreateRole
	{
		//Enums
		typedef	enum
		{
			ISSELECTOK_SUCCESS             	=	0,                 	//
			ISSELECTOK_FAILED              	=	1,                 	//
			ISSELECTOK_EXIST_SAME_NAME_ROLE	=	2,                 	//
		}ItemType;

		static const USHORT	wCmd = 0x1011;

		TYPE_FLAG           	byIsOK;                                     	//ISSELECTOK_SUCCESS创建角色成功，ISSELECTOK_FAILED失败
		TYPE_INDEX          	wRoleNo;                                    	//创建角色序号；
		TYPE_ID             	sequenceID;                                 	//序列ID
		PlayerInfo_2_Equips 	equipInfo;                                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCCreateRole);

	struct GCDeleteRole
	{
		//Enums
		typedef	enum
		{
			OP_SUCCESS	=	0,         	//
			OP_FAILED 	=	1,         	//
		}ItemType;

		static const USHORT	wCmd = 0x1012;

		TYPE_FLAG           	byIsOK;                                     	//
		TYPE_INDEX          	wRoleNo;                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDeleteRole);

	struct GCPlayerDying
	{
		static const USHORT	wCmd = 0x1013;

		PlayerID            	dyingPlayerID;                              	//死亡玩家ID号；
		TYPE_TIME           	countDownSec;                               	//倒计时时间(单位：秒)
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerDying);

	struct GCRebirthInfo
	{
		static const USHORT	wCmd = 0x1014;

		PlayerID            	dyingPlayerID;                              	//死亡玩家ID号；
		TYPE_ID             	mapID;                                      	//复活点所属地图；
		TYPE_POS            	nPosX;                                      	//复活点X坐标；
		TYPE_POS            	nPosY;                                      	//复活点Y坐标；
		TYPE_ID             	sequenceID;                                 	//网络序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRebirthInfo);

	struct GCMonsterBorn
	{
		static const USHORT	wCmd = 0x1015;

		TYPE_ID             	lMonsterTypeID;                             	//怪物类别ID(指明为哪类怪物)
		TYPE_ID             	lMonsterID;                                 	//怪物ID，地图内唯一
		TYPE_POS            	nMapPosX;                                   	//出生点X；
		TYPE_POS            	nMapPosY;                                   	//出生点Y；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMonsterBorn);

	struct GCMonsterMove
	{
		static const USHORT	wCmd = 0x1016;

		TYPE_ID             	lMonsterID;                                 	//怪物ID，地图内唯一
		TYPE_POS            	nOrgMapPosX;                                	//出发点X；
		TYPE_POS            	nOrgMapPosY;                                	//出发点Y；
		TYPE_POS            	nNewMapPosX;                                	//目标点X；
		TYPE_POS            	nNewMapPosY;                                	//目标点Y；
		TYPE_POS            	nTargetMapPosX;                             	//怪物的远距离目标点,以此可以看上去流畅
		TYPE_POS            	nTargetMapPosY;                             	//怪物的远距离目标点
		FLOAT               	fSpeed;                                     	//移动速度
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMonsterMove);

	struct GCMonsterDisappear
	{
		static const USHORT	wCmd = 0x1017;

		TYPE_ID             	lMonsterID;                                 	//怪物类别ID(指明为哪类怪物)
		TYPE_FLAG           	lDebugFlag;                                 	//出生点Y；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMonsterDisappear);

	struct GCPlayerAttackTarget
	{
		static const USHORT	wCmd = 0x1018;

		EBattleResult       	nErrCode;                                   	//错误代码
		EScopeFlag          	scopeFlag;                                  	//范围通知标志
		PlayerID            	lPlayerID;                                  	//攻击者ID；
		EBattleFlag         	battleFlag;                                 	//战斗显示标志
		TYPE_ID             	skillID;                                    	//攻击所使用的方式或技能ID
		TYPE_TIME           	lCurseTime;                                 	//技能的动作时间
		TYPE_TIME           	lCooldownTime;                              	//技能的冷却时间
		TYPE_ID             	consumeItemID;                              	//如果是道具技能会填充,其他技能不填
		PlayerID            	targetID;                                   	//目标ID
		TYPE_UI32           	lHpSubed;                                   	//怪物减去的HP
		TYPE_UI32           	lHpLeft;                                    	//怪物剩余HP
		TYPE_UI32           	nAtkPlayerHP;                               	//这次攻击攻击者的HP
		TYPE_UI32           	nAtkPlayerMP;                               	//这次攻击攻击者的MP
		PlayerID            	targetRewardID;                             	//若target为怪物的话更新怪物的RewardID
		PlayerID            	aoeMainTargetID;                            	//目标AOE的主攻击对象
		TYPE_POS            	aoeAttackPosX;                              	//针对无目标AOE所使用的目标点X
		TYPE_POS            	aoeAttackPosY;                              	//针对无目标AOE所使用的目标点Y
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerAttackTarget);

	struct GCMonsterAttackPlayer
	{
		static const USHORT	wCmd = 0x1019;

		TYPE_FLAG           	nErrCode;                                   	//错误代码
		PlayerID            	lPlayerID;                                  	//被攻击的玩家
		EScopeFlag          	scopeFlag;                                  	//怪物范围攻击的标志
		EBattleFlag         	battleFlag;                                 	//怪物攻击标志
		TYPE_ID             	lMonsterID;                                 	//怪物ID，地图内唯一
		TYPE_ID             	lMonsterType;                               	//怪物类型；
		TYPE_POS            	nMapPosX;                                   	//怪物位置X
		TYPE_POS            	nMapPosY;                                   	//怪物位置Y
		TYPE_ID             	nSkillID;                                   	//怪物所使用的技能ID
		TYPE_TIME           	nCurseTime;                                 	//传给客户端，怪物攻击播放的时间
		TYPE_UI32           	nDamage;                                    	//造成的伤害
		TYPE_UI32           	nLeftHp;                                    	//玩家被攻击后剩下的HP
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMonsterAttackPlayer);

	struct GCSwitchMap
	{
		//Enums
		typedef	enum
		{
			OP_SUSSCESS	=	0,          	//
			OP_ERROR   	=	1,          	//
		}ItemType;

		static const USHORT	wCmd = 0x101a;

		TYPE_FLAG           	nErrCode;                                   	//错误代码
		PlayerID            	playerID;                                   	//玩家ID号；
		TYPE_ID             	usMapID;                                    	//地图ID
		TYPE_POS            	iPosX;                                      	//X坐标
		TYPE_POS            	iPosY;                                      	//Y坐标
		TYPE_ID             	sequenceID;                                 	//序列ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSwitchMap);

	struct GCPlayerAttackPlayer
	{
		static const USHORT	wCmd = 0x101b;

		EBattleResult       	nErrCode;                                   	//错误代码
		EBattleFlag         	scopeFlag;                                  	//范围攻击的标志
		EBattleFlag         	hitFlag;                                    	//命中标志
		EBattleFlag         	criticalFlag;                               	//暴击标志
		PlayerID            	attackerID;                                 	//攻击者ID号；
		PlayerID            	targetID;                                   	//被攻击者ID号；
		TYPE_ID             	nSkillID;                                   	//所使用的技能ID
		TYPE_POS            	attackerPosX;                               	//攻击者位置X
		TYPE_POS            	attackerPosY;                               	//攻击者位置Y
		TYPE_POS            	targetPosX;                                 	//被攻击者位置X
		TYPE_POS            	targetPosY;                                 	//被攻击者位置Y
		TYPE_UI32           	nDamage;                                    	//造成的伤害
		TYPE_UI32           	nLeftHp;                                    	//玩家被攻击后剩下的HP
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerAttackPlayer);

	struct GCMonsterInfo
	{
		static const USHORT	wCmd = 0x101c;

		TYPE_ID             	lMonsterID;                                 	//
		TYPE_ID             	lMonsterType;                               	//怪物ID，地图内唯一
		TYPE_POS            	nMapPosX;                                   	//怪物类型；
		TYPE_POS            	nMapPosY;                                   	//怪物位置X
		FLOAT               	fSpeed;                                     	//怪物移动速度,如果小于等于0则怪物静止
		FLOAT               	fTargetX;                                   	//移动目标点X
		FLOAT               	fTargetY;                                   	//移动目标点Y
		TYPE_UI32           	lMonsterHp;                                 	//怪物位置X
		PlayerID            	rewardID;                                   	//所属掉落的玩家ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMonsterInfo);

	struct GCPlayerCombo
	{
		static const USHORT	wCmd = 0x101d;

		PlayerID            	playerID;                                   	//玩家ID号；
		TYPE_NUM            	comboNum;                                   	//连击数；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerCombo);

	struct GCSrvError
	{
		static const USHORT	wCmd = 0x101e;

		TYPE_FLAG           	errNO;                                      	//错误号: 1001：没有相应的mapsrv，1002：在mapsrv出现失败;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSrvError);

	struct GCItemPackageAppear
	{
		static const USHORT	wCmd = 0x101f;

		INT                 	PackageType;                                	//道具包的类型,现在默认为0
		TYPE_ID             	ItemPackageID;                              	//道具包的编号
		TYPE_POS            	x;                                          	//道具包出现的X位置
		TYPE_POS            	y;                                          	//道具包出现的Y位置
		PlayerID            	dropOwnerID;                                	//道具包所属的怪物号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCItemPackageAppear);

	struct GCItemsPkgDisappear
	{
		static const USHORT	wCmd = 0x1020;

		TYPE_ID             	ItemPackageID;                              	//;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCItemsPkgDisappear);

	struct CGQueryItemPackageInfo
	{
		static const USHORT	wCmd = 0x0114;

		PlayerID            	playerID;                                   	//玩家自身ID号；
		TYPE_ID             	ItemPackageID;                              	//道具包的编号,在服务器的编号
		TYPE_ID             	queryType;                                  	//查询的类型
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryItemPackageInfo);

	struct GCItemPackageInfo
	{
		static const USHORT	wCmd = 0x1021;

		TYPE_INDEX          	pkgIndex;                                   	//道具包在服务的编号
		TYPE_NUM            	itemCount;                                  	//道具包的个数
		TYPE_NUM            	money;                                      	//道具包所含有的金钱数
		UINT                	itemSize;                                   	//每个道具的类型编号
		ItemInfo_i          	ItemArr[MAXPACKAGEITEM];                      	//
		TYPE_ID             	queryType;                                  	//查询的类型
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCItemPackageInfo);

	struct GCPlayerPickItem
	{
		static const USHORT	wCmd = 0x1022;

		TYPE_INDEX          	pkgIndex;                                   	//包的个数
		TYPE_ID             	itemTypeID;                                 	//包中的Item
		bool                	IsPickMoney;                                	//是否拾取的是包中的金钱
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerPickItem);

	struct GCWeaponPackageInfo
	{
		static const USHORT	wCmd = 0x1023;

		TYPE_ID             	ItemPackageID;                              	//武器道具所属的道具包
		TYPE_ID             	ItemTypeID;                                 	//武器道具的类型编号
		WeaponAdditionProp  	weaponInfo;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCWeaponPackageInfo);

	struct GCEquipPackageInfo
	{
		static const USHORT	wCmd = 0x1024;

		TYPE_ID             	ItemPackageID;                              	//武器道具所属的道具包
		TYPE_ID             	ItemTypeID;                                 	//武器道具的类型编号
		EquipAdditionProp   	equipInfo;                                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCEquipPackageInfo);

	struct CGPickItem
	{
		static const USHORT	wCmd = 0x0115;

		TYPE_ID             	ItemPackageID;                              	//道具包编号
		TYPE_ID             	ItemTypeID;                                 	//道具的类型
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPickItem);

	struct CGPickAllItems
	{
		static const USHORT	wCmd = 0x0116;

		TYPE_ID             	ItemPackageID;                              	//道具包编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPickAllItems);

	struct GCSetPkgItem
	{
		static const USHORT	wCmd = 0x108c;

		TYPE_ID             	ItemPkgIndex;                               	//道具包中的编号
		ItemInfo_i          	ItemInfo;                                   	//道具包中该位置的道具信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSetPkgItem);

	struct GCSetEquipItem
	{
		static const USHORT	wCmd = 0x10bd;

		TYPE_ID             	ItemEquipIndex;                             	//在装备栏中的编号
		ItemInfo_i          	ItemInfo;                                   	//变更的装备信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSetEquipItem);

	struct GCTargetGetDamage
	{
		static const USHORT	wCmd = 0x10be;

		PlayerID            	targetID;                                   	//受到伤害的目标ID,可填怪物ID和玩家ID
		PlayerID            	notiecID;                                   	//可看的ID
		TYPE_ID             	skillID;                                    	//作用于目标身上的技能ID
		UINT                	damage;                                     	//目标受到的伤害值
		EBattleFlag         	battleFlag;                                 	//用于给客户端识别到底是暴击,MISS,或者免疫的标志
		UINT                	targetLeftHp;                               	//目标剩余的血量
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTargetGetDamage);

	struct GCPickItemFail
	{
		static const USHORT	wCmd = 0x1025;

		TYPE_ID             	itemTypeID;                                 	//
		EPickItemResult     	result;                                     	//拾取的失败结果
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPickItemFail);

	struct CGUseItem
	{
		static const USHORT	wCmd = 0x0117;

		TYPE_ID             	itemUID;                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGUseItem);

	struct GCUseItem
	{
		static const USHORT	wCmd = 0x1026;

		PlayerID            	UserID;                                     	//
		ItemInfo_i          	useItem;                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUseItem);

	struct GCUseItemResult
	{
		static const USHORT	wCmd = 0x1027;

		TYPE_FLAG           	result;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUseItemResult);

	struct CGUnUseItem
	{
		static const USHORT	wCmd = 0x0118;

		TYPE_INDEX          	From;                                       	//
		TYPE_INDEX          	To;                                         	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGUnUseItem);

	struct GCUnUseItem
	{
		static const USHORT	wCmd = 0x1028;

		PlayerID            	playerID;                                   	//
		TYPE_INDEX          	equipIndex;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnUseItem);

	struct GCUnUseItemResult
	{
		static const USHORT	wCmd = 0x1029;

		TYPE_FLAG           	result;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnUseItemResult);

	struct CGSWAPITEM
	{
		static const USHORT	wCmd = 0x0119;

		PlayerID            	playID;                                     	//
		TYPE_INDEX          	pkgFrom;                                    	//
		TYPE_INDEX          	pkgTo;                                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSWAPITEM);

	struct CGSplitItem
	{
		static const USHORT	wCmd = 0x014a;

		TYPE_INDEX          	ItemPkgIndex;                               	//
		TYPE_NUM            	SplitNum;                                   	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSplitItem);

	struct GCSwapItemResult
	{
		static const USHORT	wCmd = 0x102a;

		TYPE_FLAG           	result;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSwapItemResult);

	struct GCItemDetialInfo
	{
		static const USHORT	wCmd = 0x102b;

		EquipItemInfo       	itemDetialInfo;                             	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCItemDetialInfo);

	struct GCEquipConciseInfo
	{
		static const USHORT	wCmd = 0x102c;

		PlayerInfo_2_Equips 	equipInfo;                                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCEquipConciseInfo);

	struct GCItemPkgConciseInfo
	{
		static const USHORT	wCmd = 0x102d;

		UINT                	pkgInfoSize;                                	//
		ItemInfo_i          	pkgInfo[PKG_PAGE_SIZE];                       	//
		TYPE_INDEX          	pageIndex;                                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCItemPkgConciseInfo);

	struct GCDropItemResult
	{
		static const USHORT	wCmd = 0x102e;

		TYPE_FLAG           	result;                                     	//
		TYPE_ID             	itemUID;                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDropItemResult);

	struct CGEquipItem
	{
		static const USHORT	wCmd = 0x0150;

		TYPE_ID             	equipItemUID;                               	//所要装备的道具
		TYPE_ID             	equipIndex;                                 	//所装备的位置
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGEquipItem);

	struct CGDropItem
	{
		static const USHORT	wCmd = 0x011a;

		TYPE_ID             	itemUID;                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDropItem);

	struct GCMoneyUpdate
	{
		static const USHORT	wCmd = 0x102f;

		TYPE_NUM            	money;                                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMoneyUpdate);

	struct CGActiveMount
	{
		static const USHORT	wCmd = 0x011b;

		ItemInfo_i          	itemUID;                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGActiveMount);

	struct CGInviteMount
	{
		static const USHORT	wCmd = 0x011c;

		PlayerID            	targetID;                                   	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGInviteMount);

	struct CGAgreeBeInvite
	{
		static const USHORT	wCmd = 0x011d;

		PlayerID            	launchID;                                   	//
		bool                	isAgree;                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAgreeBeInvite);

	struct CGLeaveMountTeam
	{
		static const USHORT	wCmd = 0x011e;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGLeaveMountTeam);

	struct GCActiveMountFail
	{
		static const USHORT	wCmd = 0x1030;

		TYPE_NUM            	reason;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCActiveMountFail);

	struct GCMountLeader
	{
		static const USHORT	wCmd = 0x1031;

		TYPE_NUM            	mountTeamID;                                	//
		TYPE_NUM            	result;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMountLeader);

	struct GCMountAssist
	{
		static const USHORT	wCmd = 0x1032;

		TYPE_NUM            	mountTeamID;                                	//
		UINT                	mountSize;                                  	//
		PlayerID            	MountTeam[MAXMOUNTCOUNT];                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMountAssist);

	struct GCMountTeamInfo
	{
		static const USHORT	wCmd = 0x1033;

		UINT                	mountSize;                                  	//
		PlayerID            	MountTeam[MAXMOUNTCOUNT];                     	//
		ItemInfo_i          	rideItem;                                   	//
		TYPE_NUM            	x;                                          	//
		TYPE_NUM            	y;                                          	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMountTeamInfo);

	struct GCMountTeamDisBand
	{
		static const USHORT	wCmd = 0x1034;

		PlayerID            	mountTeamID;                                	//骑乘队伍解散
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMountTeamDisBand);

	struct GCPlayerLeaveMountTeam
	{
		static const USHORT	wCmd = 0x1035;

		TYPE_NUM            	mountTeamID;                                	//
		PlayerID            	levelID;                                    	//离开骑乘队伍
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerLeaveMountTeam);

	struct GCJoinMountTeam
	{
		static const USHORT	wCmd = 0x1036;

		PlayerID            	JoinPlayerID;                               	//
		TYPE_NUM            	mountTeamID;                                	//加入组队
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCJoinMountTeam);

	struct GCBeInviteMount
	{
		static const USHORT	wCmd = 0x1037;

		PlayerID            	launchID;                                   	//发起者ID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCBeInviteMount);

	struct GCInviteResult
	{
		static const USHORT	wCmd = 0x1038;

		PlayerID            	targetID;                                   	//同意的玩家
		bool                	isAgree;                                    	//是否同意骑乘
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCInviteResult);

	struct GCMountTeamMove
	{
		static const USHORT	wCmd = 0x1039;

		TYPE_NUM            	mountTeamID;                                	//骑乘队伍ID
		GCRun               	moveInfo;                                   	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMountTeamMove);

	struct GCPlayerBuffStart
	{
		static const USHORT	wCmd = 0x103a;

		PlayerID            	changePlayerID;                             	//
		BuffDetailInfo      	buffInfo;                                   	//
		TYPE_INDEX          	nBuffIndex;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerBuffStart);

	struct GCPlayerBuffEnd
	{
		static const USHORT	wCmd = 0x103b;

		PlayerID            	changePlayerID;                             	//
		BuffID              	buffID;                                     	//具体哪个BUFF
		TYPE_INDEX          	nBuffIndex;                                 	//具体哪个BUFF的栏位
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerBuffEnd);

	struct GCPlayerBuffUpdate
	{
		static const USHORT	wCmd = 0x103c;

		INT                 	nChangeHP;                                  	//变化的HP，正的加血，负的减血
		INT                 	nPlayerCurrentHP;                           	//现在玩家的血
		PlayerID            	changePlayerID;                             	//改变血的玩家ID
		BuffID              	buffID;                                     	//具体的哪个BUFF的信息
		TYPE_INDEX          	nBuffIndex;                                 	//具体哪个BUFF的栏位
		PlayerID            	createBuffPlayerID;                         	//创建BUFF的PLAYERID,满足策划需要只在释放着和被释放者看见的需求
		EBattleFlag         	battleFlag;                                 	//每次BUFF显示的标志
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerBuffUpdate);

	struct GCMonsterBuffEnd
	{
		static const USHORT	wCmd = 0x108a;

		TYPE_ID             	monsterID;                                  	//
		BuffID              	buffID;                                     	//具体哪个BUFF
		TYPE_INDEX          	nBuffIndex;                                 	//具体哪个BUFF的栏位
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMonsterBuffEnd);

	struct GCMonsterBuffStart
	{
		static const USHORT	wCmd = 0x103d;

		TYPE_ID             	monsterID;                                  	//怪物的ID
		BuffDetailInfo      	buffInfo;                                   	//具体哪个BUFF和持续的时间
		TYPE_INDEX          	nBuffIndex;                                 	//12个栏位里具体哪个位置
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMonsterBuffStart);

	struct GCMonsterBuffUpdate
	{
		static const USHORT	wCmd = 0x103e;

		INT                 	nChangeHP;                                  	//变化的HP，正的加血，负的减血
		INT                 	nMonsterCurrentHP;                          	//现在怪物的血
		TYPE_ID             	monsterID;                                  	//改变血的怪物ID
		BuffID              	buffID;                                     	//具体的哪个BUFF的信息
		TYPE_INDEX          	nBuffIndex;                                 	//具体哪个BUFF的栏位
		EBattleFlag         	battleFlag;                                 	//具体限制的标志
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMonsterBuffUpdate);

	struct CGReqBuffEnd
	{
		static const USHORT	wCmd = 0x011f;

		BuffID              	buffID;                                     	//要解除的BUFFID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGReqBuffEnd);

	struct GCReqBuffEnd
	{
		//Enums
		typedef	enum
		{
			FAILED 	=	0,     	//
			SUCCESS	=	1,     	//
		}ItemType;

		static const USHORT	wCmd = 0x103f;

		INT                 	nErrorCode;                                 	//成功与否
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCReqBuffEnd);

	struct CGInvitePlayerJoinTeam
	{
		static const USHORT	wCmd = 0x0120;

		UINT                	nameSize;                                   	//
		CHAR                	playerName[MAX_NAME_SIZE];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGInvitePlayerJoinTeam);

	struct GCInvitePlayerJoinTeam
	{
		static const USHORT	wCmd = 0x1040;

		EInviteTeamResult   	nErrorCode;                                 	//成功与否
		UINT                	nameSize;                                   	//
		CHAR                	playerName[MAX_NAME_SIZE];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCInvitePlayerJoinTeam);

	struct GCRecvInviteJoinTeam
	{
		static const USHORT	wCmd = 0x1041;

		UINT                	nameSize;                                   	//
		CHAR                	invitePlayerName[MAX_NAME_SIZE];              	//
		PlayerID            	invitePlayerID;                             	//邀请你的玩家ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRecvInviteJoinTeam);

	struct CGRecvInviteJoinTeamFeedBack
	{
		//Enums
		typedef	enum
		{
			YES	=	0,  	//
			NO 	=	1,  	//
		}ItemType;

		static const USHORT	wCmd = 0x0121;

		PlayerID            	invitePlayerID;                             	//邀请玩家的ID
		BYTE                	ucAnswer;                                   	//到底是同意不同意
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRecvInviteJoinTeamFeedBack);

	struct GCRecvInviteJoinTeamFeedBackError
	{
		static const USHORT	wCmd = 0x1042;

		EInviteTeamResult   	nErrorCode;                                 	//具体错误的原因
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRecvInviteJoinTeamFeedBackError);

	struct CGReqQuitTeam
	{
		static const USHORT	wCmd = 0x0122;

		TYPE_ID             	teamID;                                     	//要退出组队的teamID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGReqQuitTeam);

	struct GCReqQuitTeam
	{
		static const USHORT	wCmd = 0x1043;

		EInviteTeamResult   	nErrorCode;                                 	//针对发送退出的人的错误
		PlayerID            	quitMemberID;                               	//退出人的ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCReqQuitTeam);

	struct CGChangeTeamExpMode
	{
		static const USHORT	wCmd = 0x0123;

		UINT                	teamID;                                     	//
		TYPE_UI8            	ucExpMode;                                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChangeTeamExpMode);

	struct CGChangeTeamItemMode
	{
		static const USHORT	wCmd = 0x012c;

		UINT                	teamID;                                     	//
		TYPE_UI8            	ucItemMode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChangeTeamItemMode);

	struct GCChangeTeamExpMode
	{
		static const USHORT	wCmd = 0x1045;

		EInviteTeamResult   	nErrorCode;                                 	//
		TYPE_UI8            	ucExpMode;                                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChangeTeamExpMode);

	struct GCChangeTeamItemMode
	{
		static const USHORT	wCmd = 0x1060;

		EInviteTeamResult   	nErrorCode;                                 	//
		TYPE_UI8            	ucItemMode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChangeTeamItemMode);

	struct GCTeamInfo
	{
		static const USHORT	wCmd = 0x1094;

		UINT                	ulTeamID;                                   	//队伍的ID
		PlayerID            	leaderPlayerID;                             	//组队队长的ID
		UINT                	teamSize;                                   	//
		TeamMemberCliInfo   	membersInfo[7];                               	//
		TYPE_UI8            	ucExpMode;                                  	//组队的经验分配模式
		TYPE_UI8            	ucItemMode;                                 	//组队的物品分配模式
		TYPE_UI8            	ucRollItemMode;                             	//组队的ROLL等级
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTeamInfo);

	struct CGChangeLeader
	{
		static const USHORT	wCmd = 0x0125;

		TYPE_ID             	teamID;                                     	//队伍的编号
		PlayerID            	newLeaderID;                                	//组队成员中的新队长
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChangeLeader);

	struct GCChangeLeader
	{
		static const USHORT	wCmd = 0x1046;

		EInviteTeamResult   	nErrorCode;                                 	//只针对发送者而言返回错误
		PlayerID            	leaderID;                                   	//一个新的组队头领
		UINT                	memberSize;                                 	//新的组员成员
		PlayerID            	membersID[6];                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChangeLeader);

	struct GCTeamMemberDetailInfoUpdate
	{
		static const USHORT	wCmd = 0x1047;

		PlayerID            	teamMemberID;                               	//组员的ID
		TYPE_UI32           	uiHP;                                       	//HP
		TYPE_UI32           	uiMP;                                       	//MP
		TYPE_UI32           	maxHP;                                      	//max HP
		TYPE_UI32           	maxMP;                                      	//max MP
		TYPE_POS            	nPosX;                                      	//该组员的位置信息
		TYPE_POS            	nPosY;                                      	//该组员的位置信息
		PlayerInfo_5_Buffers	buffData;                                   	//该人物的具体BUFF数据
		USHORT              	mapID;                                      	//地图ID
		USHORT              	usPlayerLevel;                              	//组员等级
		TYPE_SI32           	goodEvilPoint;                              	//组员的善恶值信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTeamMemberDetailInfoUpdate);

	struct CGReqJoinTeam
	{
		static const USHORT	wCmd = 0x0126;

		UINT                	nameSize;                                   	//队长的姓名
		CHAR                	teamLeadName[MAX_NAME_SIZE];                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGReqJoinTeam);

	struct GCReqJoinTeamError
	{
		static const USHORT	wCmd = 0x1048;

		EInviteTeamResult   	nErrorCode;                                 	//错误码
		UINT                	nameSize;                                   	//要加的队长的姓名
		CHAR                	teamLeadName[MAX_NAME_SIZE];                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCReqJoinTeamError);

	struct CGBanTeamMember
	{
		static const USHORT	wCmd = 0x0127;

		TYPE_ID             	teamID;                                     	//队伍ID
		PlayerID            	banPlayerID;                                	//要踢的组员的名单
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGBanTeamMember);

	struct GCBanTeamMember
	{
		static const USHORT	wCmd = 0x1049;

		EInviteTeamResult   	nErrorCode;                                 	//错误代码，只对队长而言
		TYPE_ID             	teamID;                                     	//队伍ID
		PlayerID            	banPlayerID;                                	//要踢的组员的名单
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCBanTeamMember);

	struct GCRecvReqJoinTeam
	{
		static const USHORT	wCmd = 0x1087;

		UINT                	nameSize;                                   	//希望加入组队的玩家姓名
		CHAR                	reqPlayerName[MAX_NAME_SIZE];                 	//
		PlayerID            	reqPlayerID;                                	//该邀请人的ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRecvReqJoinTeam);

	struct CGRecvReqJoinTeamFeedBack
	{
		//Enums
		typedef	enum
		{
			YES	=	0,  	//
			NO 	=	1,  	//
		}ItemType;

		static const USHORT	wCmd = 0x0128;

		PlayerID            	reqPlayerID;                                	//要申请加入的ID
		TYPE_UI8            	ucAnswer;                                   	//到底允许他加入
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRecvReqJoinTeamFeedBack);

	struct GCRecvReqJoinTeamFeedBackError
	{
		static const USHORT	wCmd = 0x105a;

		EInviteTeamResult   	nErrorCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRecvReqJoinTeamFeedBackError);

	struct CGDestoryTeam
	{
		static const USHORT	wCmd = 0x0129;

		TYPE_ID             	teamID;                                     	//要解散的队伍ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDestoryTeam);

	struct GCDestoryTeam
	{
		static const USHORT	wCmd = 0x105b;

		EInviteTeamResult   	nErrorCode;                                 	//针对本人而言
		TYPE_ID             	teamID;                                     	//要解散的队伍ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDestoryTeam);

	struct CGTeamSwitch
	{
		//Enums
		typedef	enum
		{
			OPEN 	=	0,   	//
			CLOSE	=	1,   	//
		}ItemType;

		static const USHORT	wCmd = 0x012a;

		TYPE_UI8            	ucTeamSwitch;                               	//到底是开还是关
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGTeamSwitch);

	struct GCTeamSwitch
	{
		static const USHORT	wCmd = 0x105c;

		EInviteTeamResult   	nErrorCode;                                 	//
		TYPE_UI8            	ucNewTeamSwitch;                            	//到底是开还是关
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTeamSwitch);

	struct GCPickItemSuccess
	{
		static const USHORT	wCmd = 0x105d;

		TYPE_ID             	ItemPkgIndex;                               	//添加的道具在道具包中的编号
		ItemInfo_i          	ItemInfo;                                   	//本次成功的道具
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPickItemSuccess);

	struct CGUseAssistSkill
	{
		static const USHORT	wCmd = 0x012b;

		PlayerID            	targetPlayer;                               	//本次成功的道具
		bool                	bPlayer;                                    	//本次成功的道具
		TYPE_ID             	skillID;                                    	//本次成功的道具
		bool                	isUnionSkill;                               	//该技能是否是工会技能
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGUseAssistSkill);

	struct GCUseAssistSkill
	{
		static const USHORT	wCmd = 0x105e;

		PlayerID            	useSkillPlayer;                             	//使用者的PLAYER
		PlayerID            	targetPlayer;                               	//有可能是怪,也有可能是人
		TYPE_ID             	skillID;                                    	//
		UINT                	skillCurseTime;                             	//辅助技能的动作时间
		UINT                	skillCooldownTime;                          	//辅助技能的CD时间
		EBattleResult       	nErrorCode;                                 	//
		TYPE_UI32           	nAtkPlayerSpExp;                            	//这次攻击攻击者的总的SP阶段熟练度
		TYPE_UI32           	nAtkPlayerSpSkillExp;                       	//这次攻击攻击者使用的SP技能的技能经验值
		TYPE_UI32           	nAtkPlayerSpPower;                          	//这次攻击攻击者的SPPower
		TYPE_UI32           	nAtkPlayerMp;                               	//这次攻击攻击者的Mp
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUseAssistSkill);

	struct CGAddFriendReq
	{
		static const USHORT	wCmd = 0x012d;

		UINT                	nickNameSize;                               	//好友昵称长度
		CHAR                	nickName[MAX_NAME_SIZE];                      	//好友昵称
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddFriendReq);

	struct GCFriendGroups
	{
		static const USHORT	wCmd = 0x1061;

		UINT                	friendGroupsSize;                           	//好友数组中有效信息数
		FriendGroupInfo     	friendGroups[10];                             	//好友组信息数组
		CHAR                	groupMsgCount;                              	//所能发送的好友组信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCFriendGroups);

	struct GCPlayerFriends
	{
		static const USHORT	wCmd = 0x1062;

		UINT                	pageID;                                     	//每页最多通知10个好友
		UINT                	playerFriendsSize;                          	//后续数组中有效信息数
		FriendInfo          	playerFriends[5];                             	//好友组信息数组
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerFriends);

	struct GCFriendInfoUpdate
	{
		static const USHORT	wCmd = 0x1063;

		FriendInfo          	friendInfo;                                 	//玩家好友信息更新(例如玩家升级，下线等)
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCFriendInfoUpdate);

	struct GCAddFriendReq
	{
		static const USHORT	wCmd = 0x1064;

		PlayerID            	playerID;                                   	//请求者ID号
		UINT                	reqPlayerNameSize;                          	//请求者姓名长度
		CHAR                	reqPlayerName[MAX_NAME_SIZE];                 	//请求者姓名长度
		USHORT              	playerLevel;                                	//请求者等级
		TYPE_UI8            	playerClass;                                	//请求者职业
		BYTE                	playerSex;                                  	//请求者性别
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddFriendReq);

	struct CGAddFriendResponse
	{
		static const USHORT	wCmd = 0x012e;

		PlayerID            	reqPlayerID;                                	//请求者ID号
		UINT                	invitePlayerNameSize;                       	//请求者姓名长度
		CHAR                	invitePlayerName[MAX_NAME_SIZE];              	//请求者姓名长度
		bool                	isAgree;                                    	//响应者是否同意加好友请求
		bool                	isAddReqer;                                 	//响应者是否同时加请求者为好友
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddFriendResponse);

	struct GCAddFriendResponse
	{
		//Enums
		typedef	enum
		{
			ADD_FRIEND_OK       	=	0,            	//
			ADD_FRIEND_LIST_FULL	=	1,            	//
			ADD_FRIEND_FOE      	=	2,            	//
			ADD_FRIEND_REFUSED  	=	3,            	//
			ADD_FRIEND_NOT_EXIST	=	4,            	//
			ADD_FRIEND_REPEATED 	=	5,            	//
		}ItemType;

		static const USHORT	wCmd = 0x1074;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		bool                	isAgree;                                    	//响应者是否同意加好友请求
		bool                	isAddReqer;                                 	//响应者是否同时加请求者为好友
		bool                	isSynAddFriend;                             	//本消息是否是别人加了自己好友，自己选择同时加别人为好友的结果
		FriendInfo          	friendInfo;                                 	//新加好友信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddFriendResponse);

	struct CGDelFriendReq
	{
		static const USHORT	wCmd = 0x012f;

		TYPE_ID             	friendID;                                   	//被删好友ID号，对应FriendInfo中的friendID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDelFriendReq);

	struct GCDelFriendResponse
	{
		//Enums
		typedef	enum
		{
			DEL_FRIEND_OK    	=	0,            	//
			DEL_FRIEND_FAILED	=	1,            	//
		}ItemType;

		static const USHORT	wCmd = 0x1065;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		TYPE_ID             	friendID;                                   	//被删好友ID号，对应FriendInfo中的friendID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDelFriendResponse);

	struct CGCreateFriendGroupReq
	{
		static const USHORT	wCmd = 0x0130;

		UINT                	groupNameSize;                              	//组的名字长度
		CHAR                	groupName[MAX_NAME_SIZE];                     	//被删好友ID号，对应FriendInfo中的friendID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGCreateFriendGroupReq);

	struct GCCreateFriendGroupResponse
	{
		//Enums
		typedef	enum
		{
			CREATE_FRIENDGROUP_OK    	=	0,                    	//
			CREATE_FRIENDGROUP_FAILED	=	1,                    	//
		}ItemType;

		static const USHORT	wCmd = 0x1066;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		FriendGroupInfo     	newFriendGroup;                             	//新建组信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCCreateFriendGroupResponse);

	struct CGChgFriendGroupNameReq
	{
		static const USHORT	wCmd = 0x0132;

		FriendGroupInfo     	toupdateFriendGroup;                        	//请求修改成此组信息（不填写之前的组名，直接写新组名)
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChgFriendGroupNameReq);

	struct GCChgFriendGroupNameResponse
	{
		//Enums
		typedef	enum
		{
			CHG_FRIENDGROUPNAME_OK    	=	0,                     	//
			CHG_FRIENDGROUPNAME_FAILED	=	1,                     	//
		}ItemType;

		static const USHORT	wCmd = 0x1067;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		FriendGroupInfo     	updatedFriendGroup;                         	//修改过后的组信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChgFriendGroupNameResponse);

	struct CGDelFriendGroupReq
	{
		static const USHORT	wCmd = 0x0133;

		TYPE_ID             	groupID;                                    	//请求删除组的组号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDelFriendGroupReq);

	struct GCDelFriendGroupResponse
	{
		//Enums
		typedef	enum
		{
			DEL_FRIENDGROUP_OK      	=	0,                 	//
			DEL_FRIENDGROUP_NOTEMPTY	=	1,                 	//
			DEL_FRIENDGROUP_FAILED  	=	2,                 	//
		}ItemType;

		static const USHORT	wCmd = 0x1068;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		TYPE_ID             	groupID;                                    	//删除组的组号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDelFriendGroupResponse);

	struct CGMoveFriendGroupReq
	{
		static const USHORT	wCmd = 0x0134;

		TYPE_ID             	orgGroupID;                                 	//旧组号
		TYPE_ID             	newGroupID;                                 	//新组号
		TYPE_ID             	friendID;                                   	//被移动的好友ID号，对应FriendInfo中的friendID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGMoveFriendGroupReq);

	struct GCMoveFriendGroupResponse
	{
		//Enums
		typedef	enum
		{
			MOVE_FRIENDGROUP_OK        	=	0,                  	//
			MOVE_FRIENDGROUPNAME_FAILED	=	1,                  	//
		}ItemType;

		static const USHORT	wCmd = 0x1069;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		TYPE_ID             	orgGroupID;                                 	//旧组号
		TYPE_ID             	newGroupID;                                 	//新组号
		TYPE_ID             	friendID;                                   	//被移动的好友ID号，对应FriendInfo中的friendID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCMoveFriendGroupResponse);

	struct CGFriendChat
	{
		//Enums
		typedef	enum
		{
			FRIEND_CHAT_ALL   	=	0,              	//
			FRIEND_CHAT_GROUP 	=	1,              	//
			FRIEND_CHAT_SINGLE	=	2,              	//
		}ItemType;

		static const USHORT	wCmd = 0x0135;

		INT                 	friendChatType;                             	//聊天类型
		TYPE_ID             	groupID;                                    	//聊天对象组号，FRIEND_CHAT_GROUP==friendChatType时有用
		TYPE_ID             	friendID;                                   	//聊天对象好友ID号，FRIEND_CHAT_GROUP==friendChatType时有用
		UINT                	chatContentSize;                            	//聊天内容长度
		CHAR                	chatContent[256];                             	//聊天内容长度
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGFriendChat);

	struct GCFriendChat
	{
		//Enums
		typedef	enum
		{
			FRIEND_CHAT_ALL   	=	0,              	//
			FRIEND_CHAT_GROUP 	=	1,              	//
			FRIEND_CHAT_SINGLE	=	2,              	//
		}ItemType;

		static const USHORT	wCmd = 0x106a;

		INT                 	friendChatType;                             	//聊天类型
		UINT                	chaterNameSize;                             	//说话者名字长度
		CHAR                	chaterName[MAX_NAME_SIZE];                    	//聊天对象好友ID号，FRIEND_CHAT_GROUP==friendChatType时有用
		UINT                	chatContentSize;                            	//聊天内容长度
		CHAR                	chatContent[256];                             	//聊天内容长度
		CHAR                	Remanent;                                   	//剩余的消息数量
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCFriendChat);

	struct GCPlayerBlacklist
	{
		static const USHORT	wCmd = 0x106b;

		INT                 	pageID;                                     	//每页最多通知25个黑名单
		UINT                	playerBlacklistSize;                        	//后续数组中有效信息数
		BlackListItemInfo   	playerBlacklist;                            	//黑名单信息数组
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerBlacklist);

	struct CGAddBlacklistReq
	{
		static const USHORT	wCmd = 0x0136;

		UINT                	nickNameSize;                               	//被加入黑名单者的昵称长度
		CHAR                	nickName[MAX_NAME_SIZE];                      	//被加入黑名单者的昵称
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddBlacklistReq);

	struct GCAddBlacklistResponse
	{
		//Enums
		typedef	enum
		{
			ADD_BLACKLIST_OK           	=	0,               	//
			ADD_BLACKLIST_LIST_FULL    	=	1,               	//
			ADD_BLACKLIST_FAILED       	=	2,               	//
			ADD_BLACKLIST_FRIRENDFAILED	=	3,               	//
		}ItemType;

		static const USHORT	wCmd = 0x106c;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		BlackListItemInfo   	blacklistItemInfo;                          	//被加入黑名单者的信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddBlacklistResponse);

	struct CGDelBlacklistReq
	{
		static const USHORT	wCmd = 0x0137;

		INT                 	addBlackNameLen;                            	//黑名单中的玩家姓名
		CHAR                	addBlackName[MAX_NAME_SIZE];                  	//黑名单中的玩家姓名
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDelBlacklistReq);

	struct GCDelBlacklistResponse
	{
		//Enums
		typedef	enum
		{
			DEL_BLACKLIST_OK    	=	0,               	//
			DEL_BLACKLIST_FAILED	=	1,               	//
		}ItemType;

		static const USHORT	wCmd = 0x106d;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		INT                 	delBlackNameLen;                            	//删除的黑名单的姓名长度
		CHAR                	delBlackName[MAX_NAME_SIZE];                  	//删除的黑名单的姓名
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDelBlacklistResponse);

	struct GCFoeList
	{
		static const USHORT	wCmd = 0x106e;

		UINT                	playerFoeListSize;                          	//后续数组中有效信息数
		FoeInfo             	playerFoeList[5];                             	//黑名单信息数组
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCFoeList);

	struct GCNewFoe
	{
		static const USHORT	wCmd = 0x106f;

		FoeInfo             	newFoe;                                     	//新仇人信息
		TYPE_ID             	orgFoeItemID;                               	//如果有替换之前的仇人，则此字段填写被替换仇人的itemID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCNewFoe);

	struct CGDelFoeReq
	{
		static const USHORT	wCmd = 0x0148;

		TYPE_ID             	foeItemID;                                  	//被删仇人的itemID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDelFoeReq);

	struct GCDelFoeResponse
	{
		//Enums
		typedef	enum
		{
			DEL_FOE_OK    	=	0,         	//
			DEL_FOE_FAILED	=	1,         	//
		}ItemType;

		static const USHORT	wCmd = 0x1070;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		TYPE_ID             	foeItemID;                                  	//被加入黑名单者的信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDelFoeResponse);

	struct CGLockFoeReq
	{
		static const USHORT	wCmd = 0x0138;

		TYPE_ID             	foeItemID;                                  	//被锁仇人的itemID号
		bool                	bLock;                                      	//true为lock,false为解锁
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGLockFoeReq);

	struct GCFoeInfoUpdate
	{
		static const USHORT	wCmd = 0x108d;

		FoeInfo             	updateFoe;                                  	//更新的仇人信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCFoeInfoUpdate);

	struct GCLockFoeResponse
	{
		//Enums
		typedef	enum
		{
			LOCK_FOE_OK    	=	0,          	//
			LOCK_FOE_FAILED	=	1,          	//
		}ItemType;

		static const USHORT	wCmd = 0x1071;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		bool                	bLock;                                      	//是否缩定
		TYPE_ID             	foeItemID;                                  	//被锁仇人的itemID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCLockFoeResponse);

	struct CGQueryFoeReq
	{
		static const USHORT	wCmd = 0x0139;

		TYPE_ID             	foeItemID;                                  	//被查询仇人的itemID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryFoeReq);

	struct GCQueryFoeResponse
	{
		//Enums
		typedef	enum
		{
			QUERY_FOE_OK    	=	0,           	//
			QUERY_FOE_FAILED	=	1,           	//
		}ItemType;

		static const USHORT	wCmd = 0x1072;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		TYPE_ID             	foeItemID;                                  	//被锁仇人的itemID号
		FoeDetailInfo       	foeDetailInfo;                              	//被查询仇人的详细信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCQueryFoeResponse);

	struct CGFriendToBlacklistReq
	{
		static const USHORT	wCmd = 0x013a;

		TYPE_ID             	friendID;                                   	//被移动好友的ID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGFriendToBlacklistReq);

	struct GCFriendToBlacklistResponse
	{
		//Enums
		typedef	enum
		{
			FRIEND2BLACKLIST_OK    	=	0,                  	//
			FRIEND2BLACKLIST_FAILED	=	1,                  	//
		}ItemType;

		static const USHORT	wCmd = 0x1073;

		INT                 	nErrcode;                                   	//错误号(添加出错的原因，待添加完善)
		TYPE_ID             	foeItemID;                                  	//被转移者之前的好友编号,对应FriendInfo中的friendID
		BlackListItemInfo   	blacklistItemInfo;                          	//被查询仇人的详细信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCFriendToBlacklistResponse);

	struct CGRollItemFeedBack
	{
		static const USHORT	wCmd = 0x013b;

		TYPE_ID             	rollID;                                     	//ROLL的序列号
		TYPE_ID             	rollItemID;                                 	//ROLL的物品编号
		bool                	agree;                                      	//被查询仇人的详细信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRollItemFeedBack);

	struct GCRollItemResult
	{
		static const USHORT	wCmd = 0x1076;

		TYPE_ID             	rollID;                                     	//ROLL的序列号
		TYPE_ID             	rollItemID;                                 	//ROLL的物品编号
		INT                 	NameSize;                                   	//长度
		CHAR                	rollPlayerName[MAX_NAME_SIZE];                	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRollItemResult);

	struct GCReqAgreeRollItem
	{
		static const USHORT	wCmd = 0x1075;

		TYPE_ID             	rollID;                                     	//ROLL的序列号
		TYPE_ID             	rollItemID;                                 	//ROLL的物品TYPE编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCReqAgreeRollItem);

	struct GCTeamMemberPickItem
	{
		static const USHORT	wCmd = 0x1089;

		PlayerID            	getPlayerID;                                	//获得的组员ID
		TYPE_ID             	rollItemTypeID;                             	//ROLL的物品TYPE编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTeamMemberPickItem);

	struct CGSendTradeReq
	{
		static const USHORT	wCmd = 0x0140;

		PlayerID            	targetPlayerID;                             	//目标id
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSendTradeReq);

	struct GCSendTradeReqError
	{
		//Enums
		typedef	enum
		{
			SENDER_HAS_RECV_TRADE_REQ	=	1,                        	//
			SENDER_IN_TRADING        	=	2,                        	//
			RECVER_NOT_EXIST         	=	3,                        	//
			RECVER_HAS_DIED          	=	4,                        	//
			RECVER_HAS_RECV_TRADE    	=	5,                        	//
			RECVER_IN_TRADING        	=	6,                        	//
			RECVER_IN_CUDDLING       	=	7,                        	//
			RECVER_IN_UNNORAML       	=	8,                        	//
		}eTradeError;

		static const USHORT	wCmd = 0x107c;

		eTradeError         	tResult;                                    	//出错原因
		PlayerID            	targetPlayerID;                             	//目标id
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSendTradeReqError);

	struct GCRecvTradeReq
	{
		static const USHORT	wCmd = 0x107d;

		PlayerID            	sendReqPlayerID;                            	//发起申请的id
		INT                 	nameSize;                                   	//长度
		CHAR                	sendPlayerName[MAX_NAME_SIZE];                	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRecvTradeReq);

	struct CGSendTradeReqResult
	{
		//Enums
		typedef	enum
		{
			REFUSE	=	0,     	//
			AGREE 	=	1,     	//
		}eTradeReqResult;

		static const USHORT	wCmd = 0x0141;

		eTradeReqResult     	tResult;                                    	//出错原因
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSendTradeReqResult);

	struct GCSendTradeReqResult
	{
		//Enums
		typedef	enum
		{
			RECVER_HAS_DIED     	=	1,              	//
			RECVER_IN_TRADING   	=	2,              	//
			RECVER_IN_CUDDLING  	=	3,              	//
			SENDER_NOT_EXIST    	=	4,              	//
			SENDER_HAS_DIED     	=	5,              	//
			SENDER_IN_TRADING   	=	6,              	//
			SENDER_IN_CUDDLING  	=	7,              	//
			BOTH_OUT_TRADE_RANGE	=	8,              	//
			OTHER_ERROR         	=	9,              	//
			TRADE_CREATE_OK     	=	10,             	//
			TRADE_REQ_REFUSE    	=	11,             	//
		}eTradeReqResult;

		static const USHORT	wCmd = 0x107e;

		eTradeReqResult     	tResult;                                    	//出错原因
		PlayerID            	targetPlayerId;                             	//请求的目标id
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSendTradeReqResult);

	struct CGAddOrDeleteItem
	{
		//Enums
		typedef	enum
		{
			ADD_ITEM   	=	0,       	//
			DELETE_ITEM	=	1,       	//
		}eOperateType;

		static const USHORT	wCmd = 0x0142;

		eOperateType        	tResult;                                    	//操作类型
		UINT                	uiID;                                       	//物品唯一id
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddOrDeleteItem);

	struct GCAddOrDeleteItem
	{
		//Enums
		typedef	enum
		{
			ADD_ITEM   	=	0,       	//
			DELETE_ITEM	=	1,       	//
		}eOperateType;

		typedef	enum
		{
			FAILURE    	=	0,       	//
			SUCCESS    	=	1,       	//
		}eOperateResult;

		static const USHORT	wCmd = 0x107f;

		PlayerID            	operatorID;                                 	//操作方
		eOperateType        	tOperateType;                               	//操作类型
		eOperateResult      	tOperateResult;                             	//操作结果
		UINT                	uiID;                                       	//物品唯一id
		UINT                	uiItemTypeID;                               	//物品类型id
		TYPE_ID             	usWearPoint;                                	//耐久度
		CHAR                	ucCount;                                    	//道具的个数
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddOrDeleteItem);

	struct CGSetTradeMoney
	{
		static const USHORT	wCmd = 0x0149;

		UINT                	uiMoney;                                    	//金钱数
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSetTradeMoney);

	struct GCSetTradeMoney
	{
		static const USHORT	wCmd = 0x108b;

		PlayerID            	operatorID;                                 	//操作方
		UINT                	uiMoney;                                    	//金钱数
		bool                	bOK;                                        	//是否成功
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSetTradeMoney);

	struct CGCancelTrade
	{
		static const USHORT	wCmd = 0x0143;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGCancelTrade);

	struct GCCancelTrade
	{
		static const USHORT	wCmd = 0x1080;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCCancelTrade);

	struct CGLockTrade
	{
		//Enums
		typedef	enum
		{
			LOCK  	=	0,   	//
			UNLOCK	=	1,   	//
		}eOperateType;

		static const USHORT	wCmd = 0x0144;

		eOperateType        	tType;                                      	//操作类型
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGLockTrade);

	struct GCLockTrade
	{
		//Enums
		typedef	enum
		{
			LOCK  	=	0,   	//
			UNLOCK	=	1,   	//
		}eOperateType;

		static const USHORT	wCmd = 0x1081;

		eOperateType        	tType;                                      	//操作类型
		PlayerID            	operatePlayerID;                            	//玩家ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCLockTrade);

	struct CGConfirmTrade
	{
		static const USHORT	wCmd = 0x0145;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGConfirmTrade);

	struct GCConfirmTrade
	{
		//Enums
		typedef	enum
		{
			SUCCESS                  	=	0,      	//
			SELF_PKG_SPACE_NOT_ENOUGH	=	1,      	//
			PEER_PKG_SPACE_NOT_ENOUGH	=	2,      	//
			PEER_CONFIRM             	=	3,      	//
			PEER_NOT_CONFIRM         	=	4,      	//
			OTHER_ERROR              	=	5,      	//
		}eTradeResult;

		static const USHORT	wCmd = 0x1082;

		eTradeResult        	tResult;                                    	//交易结果
		PlayerID            	operatePlayerID;                            	//玩家ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCConfirmTrade);

	struct CGChangeReqSwitch
	{
		static const USHORT	wCmd = 0x0146;

		bool                	bReq;                                       	//申请入队的更改,只针对队长
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChangeReqSwitch);

	struct GCChangeReqSwitch
	{
		//Enums
		typedef	enum
		{
			NOT_IN_TEAM     	=	0,          	//
			NOT_TEAM_CAPTAIN	=	1,          	//
			SUCCESS         	=	2,          	//
		}ItemType;

		static const USHORT	wCmd = 0x1083;

		bool                	bReq;                                       	//申请入队的更改,只针对队长
		INT                 	nErrorCode;                                 	//错误原因
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChangeReqSwitch);

	struct CGChangeTeamRollLevel
	{
		static const USHORT	wCmd = 0x0147;

		TYPE_UI8            	ucRollLevel;                                	//更改ROLL的等级
		TYPE_ID             	teamID;                                     	//更改ROLL的等级
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChangeTeamRollLevel);

	struct CGDelFoeInfo
	{
		TYPE_ID             	foeID;                                      	//仇人的UID
	};

	struct GCChangeTeamRollLevel
	{
		//Enums
		typedef	enum
		{
			NOT_IN_TEAM     	=	0,          	//
			NOT_TEAM_CAPTAIN	=	1,          	//
			SUCCESS         	=	2,          	//
		}eTradeResult;

		static const USHORT	wCmd = 0x1084;

		TYPE_UI8            	ucRollLevel;                                	//更改ROLL的等级
		INT                 	nErrorCode;                                 	//错误原因
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChangeTeamRollLevel);

	struct GCRollItemNum
	{
		static const USHORT	wCmd = 0x1088;

		INT                 	NameSize;                                   	//长度
		CHAR                	rollPlayerName[MAX_NAME_SIZE];                	//
		INT                 	rollNum;                                    	//roll的数
		INT                 	rollID;                                     	//roll的具体编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRollItemNum);

	struct GCChangeGroupMsgNum
	{
		static const USHORT	wCmd = 0x109a;

		INT                 	MsgNum;                                     	//组的信息条数
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChangeGroupMsgNum);

	struct GCLeaveView
	{
		static const USHORT	wCmd = 0x108e;

		PlayerID            	leavePlayerID;                              	//离开的玩家ID号，如果字段都为0，则无效
		TYPE_ID             	leaveMonsterID;                             	//离开的怪物ID号，如果为0，则无效
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCLeaveView);

	struct CGRepairWearByItem
	{
		static const USHORT	wCmd = 0x014b;

		TYPE_ID             	itemRepairman;                              	//具有修复功能的物品
		TYPE_ID             	repairTarget;                               	//待修复的物品
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRepairWearByItem);

	struct CGQueryRepairItemCost
	{
		static const USHORT	wCmd = 0x0167;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryRepairItemCost);

	struct GCQueryRepairItemCostRst
	{
		static const USHORT	wCmd = 0x10b8;

		TYPE_ID             	costMoney;                                  	//查询修复全部道具所花费的金钱
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCQueryRepairItemCostRst);

	struct CGIrcQueryPlayerInfo
	{
		static const USHORT	wCmd = 0x0168;

		INT                 	nameSize;                                   	//玩家姓名的长度
		CHAR                	playerName[MAX_NAME_SIZE];                    	//请求信息的玩家姓名
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGIrcQueryPlayerInfo);

	struct GCIrcQueryPlayerInfoRst
	{
		//Enums
		typedef	enum
		{
			QUERY_SUCCESS         	=	0,            	//
			NOT_IRC_PLAYER        	=	1,            	//
			NOT_FOUND_QUERY_PLAYER	=	2,            	//
		}ItemType;

		static const USHORT	wCmd = 0x10b9;

		INT                 	errorCode;                                  	//错误信息
		INT                 	nameSize;                                   	//玩家姓名的长度
		CHAR                	playerName[MAX_NAME_SIZE];                    	//请求信息的玩家姓名
		BYTE                	playerSex;                                  	//性别 0x01男 0x02女
		USHORT              	mapID;                                      	//玩家所在的地图
		USHORT              	playerLevel;                                	//等级
		USHORT              	playerClass;                                	//职业
		USHORT              	playerRace;                                 	//种族
		UINT                	GuildNameLen;                               	//工会名称长度
		CHAR                	GuildName[MAX_NAME_SIZE];                     	//公会伲称
		UINT                	FereNameLen;                                	//伴侣名称长度
		CHAR                	FereName[MAX_NAME_SIZE];                      	//伴侣伲称
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCIrcQueryPlayerInfoRst);

	struct GCTargetPlaySound
	{
		static const USHORT	wCmd = 0x10ba;

		INT                 	pathSize;                                   	//路径的长度
		CHAR                	soundPath[256];                               	//路径
		PlayerID            	targetID;                                   	//播放声音的target
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTargetPlaySound);

	struct GCTargetChangeSize
	{
		static const USHORT	wCmd = 0x10bb;

		FLOAT               	newSize;                                    	//改变尺寸的大小
		PlayerID            	targetID;                                   	//播放声音的target
		TYPE_ID             	arg;                                        	//后备用的参数,考虑用于改变尺寸可能会用到的特效这种需求,暂不使用
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTargetChangeSize);

	struct GCInfoUpdate
	{
		static const USHORT	wCmd = 0x10bc;

		TYPE_UI32           	useSkillID;                                 	//这次攻击使用的技能ID
		TYPE_UI32           	nAtkPlayerPhyExp;                           	//这次攻击攻击者的物理熟练度
		TYPE_UI32           	nAtkPlayerMagExp;                           	//这次攻击攻击者的魔法熟练度
		TYPE_UI32           	nAtkPlayerExp;                              	//这次攻击攻击者的经验值
		TYPE_UI32           	nAtkPlayerHP;                               	//这次攻击攻击者的HP
		TYPE_UI32           	nAtkPlayerMP;                               	//这次攻击攻击者的MP
		TYPE_UI32           	nAtkPlayerMoney;                            	//这次攻击攻击者的金钱
		TYPE_UI32           	nAtkPlayerSpExp;                            	//这次攻击攻击者的总的SP阶段熟练度
		TYPE_UI32           	nAtkPlayerSpSkillExp;                       	//这次攻击攻击者使用的SP技能的技能经验值
		TYPE_UI32           	nAtkPlayerSpPower;                          	//这次攻击攻击者的SPPower
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCInfoUpdate);

	struct GCRepairWearByItem
	{
		//Enums
		typedef	enum
		{
			REPAIRMAN_NO_REPAIR_FUNCTION	=	0,                           	//
			TARGET_DO_NOT_REPAIR        	=	1,                           	//
			NO_ENOUGH_REPAIR_MONEY      	=	2,                           	//
			OTHER_ERROR                 	=	3,                           	//
			OK                          	=	4,                           	//
		}eRepairResult;

		static const USHORT	wCmd = 0x108f;

		TYPE_ID             	itemRepairman;                              	//具有修复功能的物品
		TYPE_ID             	repairTarget;                               	//待修复的物品
		eRepairResult       	tResult;                                    	//修复结果
		TYPE_ID             	newWear;                                    	//待修复的最新耐久度
		TYPE_ID             	uiRepairMoney;                              	//修复费用
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRepairWearByItem);

	struct GCNotifyItemWearChange
	{
		static const USHORT	wCmd = 0x1090;

		UINT                	itemSize;                                   	//物品数目
		WearChangedItems    	items[32];                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCNotifyItemWearChange);

	struct CGTimedWearIsZero
	{
		static const USHORT	wCmd = 0x0156;

		UINT                	uiID;                                       	//物品唯一id
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGTimedWearIsZero);

	struct CGUpgradeSkill
	{
		static const USHORT	wCmd = 0x014c;

		TYPE_ID             	skillID;                                    	//要升级的技能ID号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGUpgradeSkill);

	struct CGAddSpSkillReq
	{
		static const USHORT	wCmd = 0x014d;

		TYPE_ID             	skillID;                                    	//要新增的SP技能ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddSpSkillReq);

	struct GCAddSpSkillResult
	{
		static const USHORT	wCmd = 0x1095;

		TYPE_ID             	skillID;                                    	//要新增的SP技能ID
		bool                	bSuccess;                                   	//是否成功,成功可以在客户端上显示一些提示
		INT                 	nSkillDataIndex;                            	//新的技能在哪个栏位上
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddSpSkillResult);

	struct GCThreatList
	{
		static const USHORT	wCmd = 0x1096;

		ThreatList          	threatList;                                 	//威胁列表
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCThreatList);

	struct GCUpgradeSkillResult
	{
		static const USHORT	wCmd = 0x1091;

		EUpgradeSkillResult 	errorCode;                                  	//对应枚举EUpgradeSkillResult
		TYPE_ID             	oldSkillID;                                 	//旧的要被替换的技能ID
		TYPE_ID             	newSkillID;                                 	//新的技能ID
		TYPE_UI16           	phySkillPoint;                              	//升级后的物理技能点
		TYPE_UI16           	magSkillPoint;                              	//升级后的魔法技能点
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUpgradeSkillResult);

	struct GCPlayerIsBusying
	{
		static const USHORT	wCmd = 0x1093;

		UINT                	nameSize;                                   	//忙碌的玩家姓名
		CHAR                	PlayerName[32];                               	//
		EPlayerBusyingReason	busyCode;                                   	//对应枚举EPlayerBusyingReason
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerIsBusying);

	struct GCSpSkillUpgrade
	{
		static const USHORT	wCmd = 0x1092;

		TYPE_ID             	oldSpSkillID;                               	//旧的要被替换的sp技能ID
		TYPE_ID             	newSpSkillID;                               	//新的SP技能ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSpSkillUpgrade);

	struct GCChangeItemCri
	{
		static const USHORT	wCmd = 0x1097;

		TYPE_ID             	changeItemCri;                              	//改造道具时的概率变化
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChangeItemCri);

	struct GCUpdateItemError
	{
		static const USHORT	wCmd = 0x1098;

		EUpdateItemError    	updateItemError;                            	//对道具进行变化时发生了错误
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUpdateItemError);

	struct CGGetClientData
	{
		static const USHORT	wCmd = 0x0152;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGGetClientData);

	struct GCGetClientData
	{
		static const USHORT	wCmd = 0x109d;

		bool                	isOK;                                       	//
		USHORT              	index;                                      	//
		UINT                	dataSize;                                   	//
		CHAR                	clientData[400];                              	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCGetClientData);

	struct CGSaveClientData
	{
		static const USHORT	wCmd = 0x0153;

		bool                	isOK;                                       	//
		USHORT              	index;                                      	//
		UINT                	dataSize;                                   	//
		CHAR                	clientData[400];                              	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSaveClientData);

	struct GCPlayerEquipItem
	{
		static const USHORT	wCmd = 0x109f;

		PlayerID            	equipPlayer;                                	//
		ItemInfo_i          	equipItem;                                  	//
		UINT                	equipIndex;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerEquipItem);

	struct GCShowTaskUIInfo
	{
		static const USHORT	wCmd = 0x10a0;

		ETaskUIMsgType      	taskUIInfo;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCShowTaskUIInfo);

	struct GCCleanSkillPoint
	{
		static const USHORT	wCmd = 0x10a1;

		USHORT              	assignablePhyPoint;                         	//洗点以后的物理可分配点数
		USHORT              	assignableMagPoint;                         	//洗店以后的魔法可分配点数
		UINT                	skillSize;                                  	//
		SkillInfo_i         	skillData[SKILL_SIZE];                        	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCCleanSkillPoint);

	struct CGChangeBattleMode
	{
		static const USHORT	wCmd = 0x0163;

		BATTLE_MODE         	changeMode;                                 	//更改的战斗模式
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChangeBattleMode);

	struct GCChangeBattleMode
	{
		static const USHORT	wCmd = 0x10b3;

		BATTLE_MODE         	changeMode;                                 	//更改的战斗模式
		bool                	bSuccess;                                   	//改变成功与否
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChangeBattleMode);

	struct GCBuffData
	{
		static const USHORT	wCmd = 0x10b4;

		PlayerInfo_5_Buffers	buffData;                                   	//玩家的BUFF数据
		PlayerID            	creatureID;                                 	//玩家的ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCBuffData);

	struct GCPlayerUseHealItem
	{
		static const USHORT	wCmd = 0x10b5;

		PlayerID            	playerID;                                   	//使用道具的玩家
		TYPE_ID             	useItem;                                    	//道具编号
		UINT                	playerHp;                                   	//玩家的血值
		UINT                	playerMp;                                   	//玩家的魔值
		UINT                	playerSp;                                   	//玩家的能量值
		TYPE_ID             	itemSkillID;                                	//玩家的道具技能ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerUseHealItem);

	struct CGSwapEquipItem
	{
		static const USHORT	wCmd = 0x016a;

		CHAR                	equipFrom;                                  	//玩家交换道具的位置
		CHAR                	equipTo;                                    	//玩家所要移动到的位置
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSwapEquipItem);

	struct CGQueryItemPkgPage
	{
		static const USHORT	wCmd = 0x016b;

		CHAR                	queryPage;                                  	//查询道具包的页数信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryItemPkgPage);

	struct GCSwapEquipResult
	{
		static const USHORT	wCmd = 0x10bf;

		PlayerID            	swapPlayerID;                               	//交换装备栏道具的玩家
		CHAR                	equipFrom;                                  	//玩家交换道具的位置
		CHAR                	equipTo;                                    	//玩家所要移动到的位置
		bool                	swapResult;                                 	//玩家移动装备是否成功
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSwapEquipResult);

	struct GCChangeTeamState
	{
		static const USHORT	wCmd = 0x10c0;

		PlayerID            	changePlayerID;                             	//更换组队状态的玩家
		CHAR                	teamState;                                  	//0x00 无组队 0x01 组员 0x02组队队长
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChangeTeamState);

	struct CAGetSrvList
	{
		static const USHORT	wCmd = 0x0201;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CAGetSrvList);

	struct ACSrvOption
	{
		//Enums
		typedef	enum
		{
			FLAG_ITEM	=	0,        	//
			FLAG_END 	=	1,        	//
		}ItemType;

		static const USHORT	wCmd = 0x2001;

		TYPE_FLAG           	byFlag;                                     	//FLAG_ITEM为正常通知，FLAG_END为通知列表结束，如果为FLAG_END，则后续字段将全为0
		TYPE_UI16           	wSrvNo;                                     	//本次消息所标识服务器的序号
		TYPE_UI16           	wPort;                                      	//服务端口
		UINT                	addrSize;                                   	//0结尾IP地址字符串；
		CHAR                	strAddr[32];                                  	//
		UINT                	nameSize;                                   	//结尾服务器名
		CHAR                	srvName[32];                                  	//
		TYPE_UI8            	byOverload;                                 	//负载标识（数字0-100,数字越小负载越轻)
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(ACSrvOption);

	struct GCSetPlayerPos
	{
		static const USHORT	wCmd = 0x105f;

		PlayerID            	playerID;                                   	//
		TYPE_POS            	posX;                                       	//
		TYPE_POS            	posY;                                       	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSetPlayerPos);

	struct GCChatError
	{
		//Enums
		typedef	enum
		{
			TARGET_NOT_EXIST         	=	0,               	//
			NO_WORLD_CHAT_PRIVILEGE  	=	1,               	//
			NO_PRIVATE_CHAT_PRIVILEGE	=	2,               	//
			NO_TEAM_CHAT_PRIVILEGE   	=	3,               	//
		}ItemType;

		static const USHORT	wCmd = 0x109e;

		CHAT_TYPE           	nType;                                      	//消息类型：如群聊，密聊，组队内通话等；
		TYPE_UI8            	nErrorType;                                 	//错误类型
		UINT                	tgtNameSize;                                	//聊天目标名字长度
		CHAR                	tgtName[MAX_NAME_SIZE];                       	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChatError);

	struct CGCreateChatGroup
	{
		static const USHORT	wCmd = 0x0157;

		UINT                	chatGroupNameSize;                          	//群聊名字长度
		CHAR                	chatGroupName[MAX_CHATGROUP_NAME_LEN];        	//
		UINT                	chatGroupPasswdSize;                        	//群聊密码长度
		CHAR                	charGroupPasswd[MAX_CHATGROUP_PASSWORD_LEN];  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGCreateChatGroup);

	struct GCCreateChatGroupResult
	{
		static const USHORT	wCmd = 0x10a2;

		UINT                	ret;                                        	//返回值
		UINT                	chatGroupID;                                	//群组ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCCreateChatGroupResult);

	struct CGDestroyChatGroup
	{
		static const USHORT	wCmd = 0x0158;

		UINT                	chatGroupID;                                	//群组ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDestroyChatGroup);

	struct GCDestroyChatGroupResult
	{
		static const USHORT	wCmd = 0x10a3;

		UINT                	ret;                                        	//返回值
		UINT                	chatGroupID;                                	//群组ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDestroyChatGroupResult);

	struct GCDestroyChatGroupNotify
	{
		static const USHORT	wCmd = 0x10a4;

		UINT                	chatGroupID;                                	//群组ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDestroyChatGroupNotify);

	struct CGAddChatGroupMemberByOwner
	{
		static const USHORT	wCmd = 0x0159;

		UINT                	chatGroupID;                                	//群组ID
		UINT                	memberNameSize;                             	//成员名字长度
		CHAR                	memberName[MAX_NAME_SIZE];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddChatGroupMemberByOwner);

	struct CGAddChatGroupMember
	{
		static const USHORT	wCmd = 0x015a;

		UINT                	chatGroupID;                                	//群组ID
		UINT                	chatGroupPasswdSize;                        	//群聊密码长度
		CHAR                	charGroupPasswd[MAX_CHATGROUP_PASSWORD_LEN];  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddChatGroupMember);

	struct GCAddChatGroupMemberResult
	{
		static const USHORT	wCmd = 0x10a5;

		UINT                	ret;                                        	//返回值
		UINT                	chatGroupID;                                	//群组ID
		UINT                	chatGroupMemberCount;                       	//返回值
		GroupChatMemberInfo 	chatGroupMembers[MAX_CHATGROUP_MEMBERS_COUNT];	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddChatGroupMemberResult);

	struct GCAddChatGroupMemberNotify
	{
		static const USHORT	wCmd = 0x10a6;

		UINT                	chatGroupID;                                	//群组ID
		UINT                	memberNameSize;                             	//成员名字长度
		CHAR                	memberName[MAX_NAME_SIZE];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddChatGroupMemberNotify);

	struct CGRemoveChatGroupMember
	{
		static const USHORT	wCmd = 0x015b;

		UINT                	chatGroupID;                                	//群组ID
		UINT                	memberNameSize;                             	//成员名字长度
		CHAR                	memberName[MAX_NAME_SIZE];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRemoveChatGroupMember);

	struct GCRemoveChatGroupMemberResult
	{
		static const USHORT	wCmd = 0x10a7;

		UINT                	ret;                                        	//返回值
		UINT                	chatGroupID;                                	//群组ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRemoveChatGroupMemberResult);

	struct GCRemoveChatGroupNotify
	{
		static const USHORT	wCmd = 0x10a8;

		UINT                	chatGroupID;                                	//群组ID
		UINT                	memberNameSize;                             	//成员名字长度
		CHAR                	memberName[MAX_NAME_SIZE];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRemoveChatGroupNotify);

	struct CGChatGroupSpeak
	{
		static const USHORT	wCmd = 0x015c;

		UINT                	chatGroupID;                                	//群组ID
		UINT                	chatDataSize;                               	//聊天内容长度
		CHAR                	chatData[MAX_CHATDATA_LEN];                   	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChatGroupSpeak);

	struct GCChatGroupSpeakResult
	{
		static const USHORT	wCmd = 0x10a9;

		UINT                	chatGroupID;                                	//群组ID
		UINT                	ret;                                        	//返回值
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChatGroupSpeakResult);

	struct GCChatGroupSpeakNotify
	{
		static const USHORT	wCmd = 0x10aa;

		UINT                	chatGroupID;                                	//群组ID
		UINT                	speakerNameSize;                            	//聊天者名字长度
		CHAR                	speakerName[MAX_NAME_SIZE];                   	//
		UINT                	chatDataSize;                               	//聊天内容长度
		CHAR                	chatData[MAX_CHATDATA_LEN];                   	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChatGroupSpeakNotify);

	struct CGClinetSynchTime
	{
		static const USHORT	wCmd = 0x015d;

		UINT                	timeSynchID;                                	//返回给服务器的同步时间时的消息编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGClinetSynchTime);

	struct GCSrvSynchTime
	{
		static const USHORT	wCmd = 0x10ab;

		UINT                	timeSynchID;                                	//传递给客户端的同步时间时的消息编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSrvSynchTime);

	struct GCSynchClientTime
	{
		static const USHORT	wCmd = 0x10ac;

		UINT                	delayTime;                                  	//延迟时间
		UINT                	srvTimeSize;                                	//时间的数组长度
		CHAR                	srvTime[MAX_NAME_SIZE];                       	//服务器的当前时间
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSynchClientTime);

	struct CGRepairWearByNPC
	{
		static const USHORT	wCmd = 0x0162;

		bool                	repairAll;                                  	//全部修复
		TYPE_ID             	repairTarget;                               	//待修复的物品
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRepairWearByNPC);

	struct GCRepairWearByNPC
	{
		//Enums
		typedef	enum
		{
			TARGET_DO_NOT_REPAIR  	=	0,                   	//
			NO_ENOUGH_REPAIR_MONEY	=	1,                   	//
			OTHER_ERROR           	=	2,                   	//
			OK                    	=	3,                   	//
		}eRepairResult;

		static const USHORT	wCmd = 0x10b0;

		eRepairResult       	tResult;                                    	//修复结果
		bool                	repairAll;                                  	//全部修复
		TYPE_ID             	repairTarget;                               	//待修复的物品
		TYPE_ID             	newWear;                                    	//待修复的最新耐久度
		TYPE_ID             	uiRepairMoney;                              	//修复费用
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRepairWearByNPC);

	struct GCPlayerAddHp
	{
		static const USHORT	wCmd = 0x10b2;

		PlayerID            	playerID;                                   	//加血的玩家
		UINT                	MaxHp;                                      	//最大Hp
		UINT                	currentHp;                                  	//现在Hp
		UINT                	healVal;                                    	//治疗量
		UINT                	skillID;                                    	//使用的技能
		UINT                	cdTime;                                     	//cd时间，按微秒算
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerAddHp);

	struct CGCreateUnionRequest
	{
		static const USHORT	wCmd = 0x016c;

		USHORT              	nameLen;                                    	//
		CHAR                	name[MAX_UNIONNAME_LEN];                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGCreateUnionRequest);

	struct GCCreateUnionResult
	{
		static const USHORT	wCmd = 0x10c1;

		INT                 	returnCode;                                 	//
		Union               	_union;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCCreateUnionResult);

	struct CGDestroyUnionRequest
	{
		static const USHORT	wCmd = 0x016d;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDestroyUnionRequest);

	struct GCDestroyUnionResult
	{
		static const USHORT	wCmd = 0x10c2;

		INT                 	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDestroyUnionResult);

	struct GCUnionDestroyedNotify
	{
		static const USHORT	wCmd = 0x10c3;

		USHORT              	nameLen;                                    	//
		CHAR                	name[MAX_PLAYERNAME_LEN];                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionDestroyedNotify);

	struct CGQueryUnionBasicRequest
	{
		static const USHORT	wCmd = 0x016e;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryUnionBasicRequest);

	struct GCQueryUnionBasicResult
	{
		static const USHORT	wCmd = 0x10c4;

		INT                 	returnCode;                                 	//
		Union               	_union;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCQueryUnionBasicResult);

	struct CGQueryUnionMembersRequest
	{
		static const USHORT	wCmd = 0x016f;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryUnionMembersRequest);

	struct GCQueryUnionMembersResult
	{
		static const USHORT	wCmd = 0x10c5;

		INT                 	returnCode;                                 	//
		UnionMembers        	members;                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCQueryUnionMembersResult);

	struct CGAddUnionMemberRequest
	{
		static const USHORT	wCmd = 0x0170;

		USHORT              	nameLen;                                    	//
		CHAR                	name[MAX_PLAYERNAME_LEN];                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddUnionMemberRequest);

	struct GCAddUnionMemberConfirm
	{
		static const USHORT	wCmd = 0x10c6;

		UINT                	id;                                         	//
		USHORT              	unionNameLen;                               	//
		CHAR                	unionName[MAX_UNIONNAME_LEN];                 	//
		USHORT              	nameLen;                                    	//
		CHAR                	name[MAX_PLAYERNAME_LEN];                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddUnionMemberConfirm);

	struct CGAddUnionMemberConfirmed
	{
		static const USHORT	wCmd = 0x0171;

		UINT                	id;                                         	//
		USHORT              	flag;                                       	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddUnionMemberConfirmed);

	struct GCAddUnionMemberResult
	{
		static const USHORT	wCmd = 0x10c7;

		INT                 	returnCode;                                 	//
		USHORT              	nameLen;                                    	//
		CHAR                	name[MAX_PLAYERNAME_LEN];                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddUnionMemberResult);

	struct GCAddUnionMemberNotify
	{
		static const USHORT	wCmd = 0x10c8;

		UnionMember         	memberAdded;                                	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddUnionMemberNotify);

	struct CGRemoveUnionMemberRequest
	{
		static const USHORT	wCmd = 0x0172;

		UINT                	memberRemoved;                              	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRemoveUnionMemberRequest);

	struct GCUnionPosRightsNtf
	{
		static const USHORT	wCmd = 0x10d7;

		UINT                	rights;                                     	//
		USHORT              	posLen;                                     	//
		CHAR                	pos[MAX_PLAYERNAME_LEN];                      	//
		USHORT              	flag;                                       	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionPosRightsNtf);

	struct CGModifyUnionPosRighstReq
	{
		static const USHORT	wCmd = 0x0177;

		USHORT              	posLen;                                     	//
		CHAR                	pos[MAX_PLAYERNAME_LEN];                      	//
		UINT                	rights;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGModifyUnionPosRighstReq);

	struct GCModifyUnionPosRighstRst
	{
		static const USHORT	wCmd = 0x10d8;

		INT                 	returnCode;                                 	//
		USHORT              	posLen;                                     	//
		CHAR                	pos[MAX_PLAYERNAME_LEN];                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCModifyUnionPosRighstRst);

	struct CGAddUnionPosReq
	{
		static const USHORT	wCmd = 0x0178;

		USHORT              	posLen;                                     	//
		CHAR                	pos[MAX_PLAYERNAME_LEN];                      	//
		UINT                	rights;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAddUnionPosReq);

	struct GCAddUnionPosRst
	{
		static const USHORT	wCmd = 0x10d9;

		INT                 	returnCode;                                 	//
		USHORT              	posLen;                                     	//
		CHAR                	pos[MAX_PLAYERNAME_LEN];                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAddUnionPosRst);

	struct CGRemoveUnionPosReq
	{
		static const USHORT	wCmd = 0x0179;

		USHORT              	posLen;                                     	//
		CHAR                	pos[MAX_PLAYERNAME_LEN];                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGRemoveUnionPosReq);

	struct GCRemoveUnionPosRst
	{
		static const USHORT	wCmd = 0x10da;

		UINT                	returnCode;                                 	//
		USHORT              	posLen;                                     	//
		CHAR                	pos[MAX_PLAYERNAME_LEN];                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRemoveUnionPosRst);

	struct CGTransformUnionCaptionReq
	{
		static const USHORT	wCmd = 0x017a;

		UINT                	newCaptionUiid;                             	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGTransformUnionCaptionReq);

	struct GCTransformUnionCaptionRst
	{
		static const USHORT	wCmd = 0x10db;

		UINT                	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTransformUnionCaptionRst);

	struct GCTransformUnionCaptionNtf
	{
		static const USHORT	wCmd = 0x10dc;

		USHORT              	newCaptionNameLen;                          	//
		CHAR                	newCaptionName[MAX_PLAYERNAME_LEN];           	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCTransformUnionCaptionNtf);

	struct CGModifyUnionMemberTitleReq
	{
		static const USHORT	wCmd = 0x017b;

		UINT                	modifiedUiid;                               	//
		USHORT              	titleLen;                                   	//
		CHAR                	title[MAX_PLAYERNAME_LEN];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGModifyUnionMemberTitleReq);

	struct GCModifyUnionMemberTitleRst
	{
		static const USHORT	wCmd = 0x10dd;

		UINT                	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCModifyUnionMemberTitleRst);

	struct GCUnionMemberTitleNtf
	{
		static const USHORT	wCmd = 0x10de;

		USHORT              	memberNameLen;                              	//
		CHAR                	memberName[MAX_PLAYERNAME_LEN];               	//
		USHORT              	titleLen;                                   	//
		CHAR                	title[MAX_PLAYERNAME_LEN];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionMemberTitleNtf);

	struct CGAdvanceUnionMemberPosReq
	{
		static const USHORT	wCmd = 0x017c;

		UINT                	advancedUiid;                               	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAdvanceUnionMemberPosReq);

	struct GCAdvanceUnionMemberPosRst
	{
		static const USHORT	wCmd = 0x10df;

		UINT                	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAdvanceUnionMemberPosRst);

	struct GCAdvanceUnionMemberPosNtf
	{
		static const USHORT	wCmd = 0x10e0;

		USHORT              	memberNameLen;                              	//
		CHAR                	memberName[MAX_PLAYERNAME_LEN];               	//
		USHORT              	posSeq;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAdvanceUnionMemberPosNtf);

	struct CGReduceUnionMemberPosReq
	{
		static const USHORT	wCmd = 0x017d;

		UINT                	reducedUiid;                                	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGReduceUnionMemberPosReq);

	struct GCReduceUnionMemberPosRst
	{
		static const USHORT	wCmd = 0x10e1;

		UINT                	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCReduceUnionMemberPosRst);

	struct GCReduceUnionMemberPosNtf
	{
		static const USHORT	wCmd = 0x10e2;

		USHORT              	memberNameLen;                              	//
		CHAR                	memberName[MAX_PLAYERNAME_LEN];               	//
		USHORT              	posSeq;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCReduceUnionMemberPosNtf);

	struct CGUnionChannelSpeekReq
	{
		static const USHORT	wCmd = 0x017e;

		USHORT              	contentLen;                                 	//
		CHAR                	content[MAX_CHATDATA_LEN];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGUnionChannelSpeekReq);

	struct GCUnionChannelSpeekRst
	{
		static const USHORT	wCmd = 0x10e3;

		UINT                	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionChannelSpeekRst);

	struct GCUnionChannelSpeekNtf
	{
		static const USHORT	wCmd = 0x10e4;

		USHORT              	speekerNameLen;                             	//
		CHAR                	speekerName[MAX_PLAYERNAME_LEN];              	//
		USHORT              	contentLen;                                 	//
		CHAR                	content[MAX_CHATDATA_LEN];                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionChannelSpeekNtf);

	struct CGUpdateUnionPosCfgReq
	{
		static const USHORT	wCmd = 0x017f;

		USHORT              	posListLen;                                 	//
		UnionPosRight       	posList[10];                                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGUpdateUnionPosCfgReq);

	struct GCUpdateUnionPosCfgRst
	{
		static const USHORT	wCmd = 0x10e5;

		UINT                	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUpdateUnionPosCfgRst);

	struct GCUpdateUnionPosCfgNtf
	{
		static const USHORT	wCmd = 0x10e6;

		USHORT              	posListLen;                                 	//
		UnionPosRight       	posList[10];                                  	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUpdateUnionPosCfgNtf);

	struct CGForbidUnionSpeekReq
	{
		static const USHORT	wCmd = 0x0181;

		UINT                	forbidUiid;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGForbidUnionSpeekReq);

	struct GCForbidUnionSpeekRst
	{
		static const USHORT	wCmd = 0x10e7;

		UINT                	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCForbidUnionSpeekRst);

	struct GCForbidUnionSpeekNtf
	{
		static const USHORT	wCmd = 0x10e8;

		USHORT              	nameLen;                                    	//
		CHAR                	name[MAX_PLAYERNAME_LEN];                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCForbidUnionSpeekNtf);

	struct GCUnionSkills
	{
		static const USHORT	wCmd = 0x10eb;

		UINT                	unionID;                                    	//
		USHORT              	skillLen;                                   	//
		UINT                	unionSkill[MAX_UNION_SKILL_NUM];              	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionSkills);

	struct CGIssueUnionTaskReq
	{
		static const USHORT	wCmd = 0x0184;

		USHORT              	taskValidInterval;                          	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGIssueUnionTaskReq);

	struct GCIssueUnionTaskRst
	{
		static const USHORT	wCmd = 0x10ec;

		UINT                	result;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCIssueUnionTaskRst);

	struct GCIssueUnionTaskNtf
	{
		static const USHORT	wCmd = 0x10ed;

		UINT                	costActive;                                 	//
		UINT                	taskEndTime;                                	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCIssueUnionTaskNtf);

	struct GCDisplayUnionTasks
	{
		static const USHORT	wCmd = 0x10ee;

		UINT                	result;                                     	//
		USHORT              	taskLen;                                    	//
		UINT                	tasks[MAX_ISSUE_TASK_NUM];                    	//
		UINT                	taskEndTime;                                	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDisplayUnionTasks);

	struct CGLearnUnionSkillReq
	{
		static const USHORT	wCmd = 0x0185;

		UINT                	skillID;                                    	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGLearnUnionSkillReq);

	struct CGAdvanceUnionLevelReq
	{
		static const USHORT	wCmd = 0x0188;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGAdvanceUnionLevelReq);

	struct GCAdvanceUnionLevelRst
	{
		static const USHORT	wCmd = 0x10f4;

		UINT                	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAdvanceUnionLevelRst);

	struct GCUnionLevelNtf
	{
		static const USHORT	wCmd = 0x10f5;

		UINT                	level;                                      	//
		UINT                	active;                                     	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionLevelNtf);

	struct CGPostUnionBulletinReq
	{
		static const USHORT	wCmd = 0x0189;

		USHORT              	seq;                                        	//
		USHORT              	contentLen;                                 	//
		CHAR                	content[500];                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPostUnionBulletinReq);

	struct GCPostUnionBulletinRst
	{
		static const USHORT	wCmd = 0x10f6;

		UINT                	returnCode;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPostUnionBulletinRst);

	struct GCUnionBulletinNtf
	{
		static const USHORT	wCmd = 0x10f7;

		USHORT              	seq;                                        	//
		USHORT              	contentLen;                                 	//
		CHAR                	content[300];                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionBulletinNtf);

	struct GCUnionMemberOnOffLineNtf
	{
		static const USHORT	wCmd = 0x10ff;

		bool                	flag;                                       	//true：online/false:offline
		UINT                	uiid;                                       	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionMemberOnOffLineNtf);

	struct CGLeaveWaitQueue
	{
		static const USHORT	wCmd = 0x0186;

		PlayerID            	leavePlayer;                                	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGLeaveWaitQueue);

	struct CGUsePartionSkill
	{
		static const USHORT	wCmd = 0x0187;

		TYPE_ID             	partionSkillID;                             	//动作分割ID
		PlayerID            	targetID;                                   	//目标ID
		UINT                	clientTime;                                 	//对时用时间
		TYPE_POS            	targetPosX;                                 	//目标点X
		TYPE_POS            	targetPosY;                                 	//目标点Y
		TYPE_ID             	sequenceID;                                 	//网络序列号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGUsePartionSkill);

	struct GCLearnUnionSkillRst
	{
		static const USHORT	wCmd = 0x10ef;

		UINT                	result;                                     	//
		UINT                	skillID;                                    	//
		UINT                	costActive;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCLearnUnionSkillRst);

	struct GCLearnUnionSkillNtf
	{
		static const USHORT	wCmd = 0x10f0;

		UINT                	skillID;                                    	//
		UINT                	costActive;                                 	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCLearnUnionSkillNtf);

	struct GCChanageUnionActive
	{
		static const USHORT	wCmd = 0x10f1;

		UINT                	val;                                        	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCChanageUnionActive);

	struct GCUnionUISelectCmd
	{
		//Enums
		typedef	enum
		{
			CREATE_UNION       	=	0,           	//
			ADVANCE_UNION_LEVEL	=	1,           	//
			ISSUE_UNION_TASK   	=	2,           	//
			LEARN_UNION_SKILL  	=	3,           	//
			DESIGN_UNION_BADGE 	=	4,           	//
		}eUnionUICmd;

		static const USHORT	wCmd = 0x104c;

		UINT                	cmdID;                                      	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUnionUISelectCmd);

	struct LogEntryParam
	{
		USHORT              	paramLen;                                   	//
		CHAR                	param[32];                                    	//
	};

	struct UnionLogEntry
	{
		USHORT              	eventId;                                    	//
		UINT                	datetime;                                   	//
		LogEntryParam       	param1;                                     	//
		LogEntryParam       	param2;                                     	//
	};

	struct CGQueryUnionLogReq
	{
		static const USHORT	wCmd = 0x0191;

		UINT                	bookmark;                                   	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryUnionLogReq);

	struct GCQueryUnionLogRst
	{
		static const USHORT	wCmd = 0x104a;

		UINT                	bookmark;                                   	//
		USHORT              	logListLen;                                 	//
		UnionLogEntry       	logList[6];                                   	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCQueryUnionLogRst);

	struct GCWaitQueueResult
	{
		static const USHORT	wCmd = 0x10f2;

		PlayerID            	waitPlayer;                                 	//等待的人
		UINT                	waitIndex;                                  	//等待的序列
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCWaitQueueResult);

	struct GCUsePartionSkillRst
	{
		static const USHORT	wCmd = 0x10f3;

		EBattleResult       	nErrCode;                                   	//错误代码
		EScopeFlag          	scopeFlag;                                  	//范围通知标志
		PlayerID            	lPlayerID;                                  	//攻击者ID；
		EBattleFlag         	battleFlag;                                 	//战斗显示标志
		TYPE_ID             	skillID;                                    	//攻击所使用的方式或技能ID
		TYPE_TIME           	lCooldownTime;                              	//技能的冷却时间
		TYPE_ID             	consumeItemID;                              	//如果是道具技能会填充,其他技能不填
		PlayerID            	targetID;                                   	//目标ID
		TYPE_UI32           	lHpSubed;                                   	//怪物减去的HP
		TYPE_UI32           	lHpLeft;                                    	//怪物剩余HP
		TYPE_UI32           	nAtkPlayerHP;                               	//这次攻击攻击者的HP
		TYPE_UI32           	nAtkPlayerMP;                               	//这次攻击攻击者的MP
		PlayerID            	targetRewardID;                             	//若target为怪物的话更新怪物的RewardID
		PlayerID            	aoeMainTargetID;                            	//目标AOE的主攻击对象
		UINT                	StartTime;                                  	//动作分割起始时间
		UINT                	MidwaySpeed;                                	//动作分割中途速度
		UINT                	EndTime;                                    	//动作分割结束速度
		UINT                	MinMidwayTime;                              	//过程中需要时间
		TYPE_POS            	FinalPositionPosX;                          	//动作分割结束位置X
		TYPE_POS            	FinalPositionPosY;                          	//动作分割结束位置Y
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCUsePartionSkillRst);

	struct CGChangePetName
	{
		static const USHORT	wCmd = 0x0173;

		UINT                	petNameLen;                                 	//更改的宠物姓名的长度
		CHAR                	petName[MAX_NAME_SIZE];                       	//更改的宠物姓名
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChangePetName);

	struct CGClosePet
	{
		static const USHORT	wCmd = 0x0174;

		PlayerID            	petOwner;                                   	//宠物所有者
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGClosePet);

	struct CGChangePetSkillState
	{
		static const USHORT	wCmd = 0x0175;

		UINT                	skillState;                                 	//更改当前的宠物技能状态
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChangePetSkillState);

	struct CGChangePetImage
	{
		static const USHORT	wCmd = 0x0176;

		UINT                	imageID;                                    	//宠物新的外形ID
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGChangePetImage);

	struct CGSummonPet
	{
		static const USHORT	wCmd = 0x0180;

		PlayerID            	petOwner;                                   	//宠物所有者
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSummonPet);

	struct GCPetinfoInstable
	{
		static const USHORT	wCmd = 0x10cc;

		UINT                	petLevel;                                   	//宠物等级
		UINT                	petExp;                                     	//宠物经验
		UINT                	petHunger;                                  	//宠物饥饿度
		UINT                	petSkillSet;                                	//宠物技能设置
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPetinfoInstable);

	struct GCPetAbility
	{
		static const USHORT	wCmd = 0x10cd;

		UINT                	skillValid;                                 	//宠物可用技能
		UINT                	lookValidLen;                               	//可用外观数组长度
		CHAR                	lookValid[MAX_PET_IMAGE_LEN];                 	//宠物可用外观数组
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPetAbility);

	struct GCPetinfoStable
	{
		static const USHORT	wCmd = 0x10ce;

		PlayerID            	petOwnerID;                                 	//宠物主人ID号
		UINT                	petNameLen;                                 	//宠物名字长度
		CHAR                	petName[MAX_NAME_SIZE];                       	//宠物名字数组
		UINT                	lookPos;                                    	//宠物外观(256个中的第几个)
		bool                	isSummoned;                                 	//是否已召唤
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPetinfoStable);

	struct RankChartItem
	{
		UINT                	itemUID;                                    	//排行元素的唯一编号
		CHAR                	itemNameLen;                                	//排行的Item名称长度
		CHAR                	itemName[MAX_NAME_SIZE];                      	//排行Item的名称
		INT                 	rankInfo;                                   	//排行元素的所排行信息
		CHAR                	itemClassLen;                               	//排行元素的类型长度
		CHAR                	itemClass[MAX_NAME_SIZE];                     	//排行Item的类别 如果是玩家就是职业，如果是工会就是会长名称
		INT                 	itemLevel;                                  	//排行Item的等级
	};

	struct CGQueryRankInfoByPageIndex
	{
		static const USHORT	wCmd = 0x0182;

		UINT                	rankChartLen;                               	//查询的排行榜ID的长度
		CHAR                	rankChartID[MAX_CHARTID_LEN];                 	//排行榜的ID
		CHAR                	ChartPageIndex;                             	//查询的页数
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryRankInfoByPageIndex);

	struct CGQueryRankInfoByPlayerName
	{
		static const USHORT	wCmd = 0x0183;

		UINT                	rankChartLen;                               	//查询的排行榜ID的长度
		CHAR                	rankChartID[MAX_CHARTID_LEN];                 	//排行榜的ID
		CHAR                	playerNameLen;                              	//查询玩家的姓名长度
		CHAR                	playerName[MAX_NAME_SIZE];                    	//排行玩家的姓名
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryRankInfoByPlayerName);

	struct GCRankInfoByPageRst
	{
		static const USHORT	wCmd = 0x10e9;

		CHAR                	rankPageIndex;                              	//所查询的排行榜的页数
		CHAR                	rankCount;                                  	//排行榜的该页的个数
		RankChartItem       	rankInfoArr[MAX_CHARTPAGE_LEN];               	//返回给客户端的排行榜信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRankInfoByPageRst);

	struct GCQueryRankPlayerByNameRst
	{
		static const USHORT	wCmd = 0x10ea;

		bool                	queryRst;                                   	//查询是否成功
		CHAR                	rankIndex;                                  	//玩家在排行榜的编号
		RankChartItem       	rankPlayer;                                 	//玩家的信息
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCQueryRankPlayerByNameRst);

	struct CGSendMailToPlayer
	{
		static const USHORT	wCmd = 0x0193;

		USHORT              	titleLen;                                   	//发送邮件主题的长度
		CHAR                	szMailTitle[MAX_MAIL_TITILE_LEN];             	//邮件主题的长度
		USHORT              	contentLen;                                 	//邮件内容长度
		CHAR                	szMailContent[MAX_MAIL_CONTENT_LEN];          	//邮件正文的长度
		USHORT              	nameLen;                                    	//邮件收件人的姓名长度
		CHAR                	szRecvMailPlayerName[MAX_NAME_SIZE];          	//收件人姓名
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGSendMailToPlayer);

	struct CGQueryMailListByPage
	{
		static const USHORT	wCmd = 0x0195;

		MAIL_TYPE           	mailType;                                   	//邮件的种类
		UINT                	page;                                       	//邮件的页数
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryMailListByPage);

	struct CGDeleteMail
	{
		static const USHORT	wCmd = 0x0194;

		MAIL_TYPE           	mailType;                                   	//邮件的种类
		UINT                	mailuid;                                    	//邮件的编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGDeleteMail);

	struct CGQueryMailDetailInfo
	{
		static const USHORT	wCmd = 0x0196;

		MAIL_TYPE           	mailType;                                   	//邮件的种类
		UINT                	mailuid;                                    	//邮件的唯一编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryMailDetailInfo);

	struct CGPickMailAttachItem
	{
		static const USHORT	wCmd = 0x0197;

		UINT                	mailuid;                                    	//邮件的唯一编号
		UINT                	pickIndex;                                  	//邮件的附件编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPickMailAttachItem);

	struct CGPickMailAttachMoney
	{
		static const USHORT	wCmd = 0x0198;

		UINT                	mailuid;                                    	//邮件的唯一编号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPickMailAttachMoney);

	struct GCQueryMailListByPageRst
	{
		static const USHORT	wCmd = 0x1100;

		USHORT              	arrCount;                                   	//邮件每页的个数
		MailConciseInfo     	mailList[MAX_MAIL_CONCISE_COUNT];             	//邮件的每页的简明信息
		CHAR                	pageIndex;                                  	//这是第几页的信息
		CHAR                	pageEnd;                                    	//0x1 代表消息为这一页的后半页 0x2 代表这是邮件的最后一页
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCQueryMailListByPageRst);

	struct GCQueryMailDetialInfoRst
	{
		static const USHORT	wCmd = 0x1101;

		UINT                	mailuid;                                    	//邮件的唯一编号
		USHORT              	nameLen;                                    	//发送邮件的玩家姓名长度
		CHAR                	szSendPlayerName[MAX_NAME_SIZE];              	//发送的玩家姓名
		USHORT              	titleLen;                                   	//邮件的主题长度
		CHAR                	szMailTitle[MAX_MAIL_TITILE_LEN];             	//邮件的主题长度
		USHORT              	contentLen;                                 	//邮件的正文长度
		CHAR                	szMailContent[MAX_MAIL_CONTENT_LEN];          	//邮件的正文长度
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCQueryMailDetialInfoRst);

	struct GCQueryMailAttachInfoRst
	{
		static const USHORT	wCmd = 0x1102;

		UINT                	mailuid;                                    	//邮件的唯一编号
		INT                 	money;                                      	//邮件的附件的金钱
		USHORT              	itemCount;                                  	//邮件的附件个数
		ItemInfo_i          	szItemInfo[MAX_MAIL_ATTACHITEM_COUNT];        	//邮件的附件道具的个数
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCQueryMailAttachInfoRst);

	struct GCSendMailError
	{
		static const USHORT	wCmd = 0x1103;

		SENDMAIL_FAIL_TYPE  	errNo;                                      	//发送错误的类型
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCSendMailError);

	struct GCDeleteMailRst
	{
		static const USHORT	wCmd = 0x1104;

		UINT                	mailuid;                                    	//邮件的唯一编号
		bool                	isSucc;                                     	//邮件删除是否成功
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCDeleteMailRst);

	struct GCPickMailAttachItemRst
	{
		static const USHORT	wCmd = 0x1105;

		UINT                	mailuid;                                    	//邮件的唯一编号
		UINT                	pickIndex;                                  	//拾取附件的位置
		bool                	isSucc;                                     	//拾取附件道具是否成功
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPickMailAttachItemRst);

	struct GCPickMailAttachMoneyRst
	{
		static const USHORT	wCmd = 0x1106;

		UINT                	mailuid;                                    	//邮件的唯一编号
		bool                	isSucc;                                     	//拾取邮件金钱是否成功
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPickMailAttachMoneyRst);

	struct GCPlayerHaveUnReadMail
	{
		static const USHORT	wCmd = 0x1107;

		UINT                	unReadMailCount;                            	//未读邮件的个数
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPlayerHaveUnReadMail);

	struct GCRemoveUnionMemberResult
	{
		static const USHORT	wCmd = 0x10c9;

		INT                 	returnCode;                                 	//
		UINT                	memberRemoved;                              	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRemoveUnionMemberResult);

	struct GCRemoveUnionMemberNotify
	{
		static const USHORT	wCmd = 0x10ca;

		UINT                	memberRemoved;                              	//
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCRemoveUnionMemberNotify);

	struct GCPunishPlayerInfo
	{
		static const USHORT	wCmd = 0x10cb;

		UINT                	memberSize;                                 	//天谴玩家的人数
		PunishPlayerCliInfo 	punishMember[3];                              	//天谴玩家
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPunishPlayerInfo);

	struct CGQueryAccountInfo
	{
		static const USHORT	wCmd = 0x018a;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryAccountInfo);

	struct GCAccountInfo
	{
		static const USHORT	wCmd = 0x10f8;

		UINT                	GameMoney;                                  	//奇迹星钻余额
		UINT                	Score;                                      	//积分余额
		BYTE                	MemberClass;                                	//会员等级
		BYTE                	MemberLevel;                                	//会员级别
		UINT                	MemberExp;                                  	//会员经验
		FLOAT               	PurchaseDisc;                               	//折扣率
		USHORT              	Result;                                     	//0 表示查询成功 其他表示错误码
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCAccountInfo);

	struct VipItem
	{
		UINT                	ItemTypeID;                                 	//道具类型ID
		BYTE                	ItemNum;                                    	//道具数目
	};

	struct CGPurchaseItem
	{
		static const USHORT	wCmd = 0x018b;

		BYTE                	PurchaseType;                               	//购买方式 0 星钻 1 积分
		UINT                	ItemID;                                     	//道具类型ID
		UINT                	ItemNum;                                    	//道具数目
		UINT                	CliCalc;                                    	//客户端计算的金额数目
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPurchaseItem);

	struct GCPurchaseItem
	{
		static const USHORT	wCmd = 0x10f9;

		UINT                	TransID;                                    	//交易流水号
		UINT                	ItemID;                                     	//道具类型ID
		UINT                	ItemNum;                                    	//道具数目
		UINT                	GameMoney;                                  	//奇迹星钻余额
		UINT                	Score;                                      	//积分余额
		BYTE                	MemberClass;                                	//会员等级
		BYTE                	MemberLevel;                                	//会员级别
		UINT                	MemberExp;                                  	//会员经验
		FLOAT               	PurchaseDisc;                               	//折扣率
		USHORT              	PurchaseResult;                             	//购买结果，0表示成功 其他表示错误码
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPurchaseItem);

	struct CGQueryVipInfo
	{
		static const USHORT	wCmd = 0x018c;
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGQueryVipInfo);

	struct GCVipInfo
	{
		static const USHORT	wCmd = 0x10fa;

		UINT                	VipID;                                      	//vip方案号
		USHORT              	LeftDays;                                   	//剩余天数
		BYTE                	Fetched;                                    	//当日道具是否已领取 0 未领取 1 已领取
		USHORT              	Result;                                     	//0 表示查询成功 其他表示错误码
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCVipInfo);

	struct CGPurchaseVip
	{
		static const USHORT	wCmd = 0x018d;

		UINT                	VipID;                                      	//vip方案号
		UINT                	CliCalc;                                    	//客户端计算的金额数目
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPurchaseVip);

	struct GCPurchaseVip
	{
		static const USHORT	wCmd = 0x10fb;

		UINT                	TransID;                                    	//交易流水号
		UINT                	VipID;                                      	//vip方案号
		UINT                	GameMoney;                                  	//奇迹星钻余额
		UINT                	Score;                                      	//积分余额
		BYTE                	MemberClass;                                	//会员等级
		BYTE                	MemberLevel;                                	//会员级别
		UINT                	MemberExp;                                  	//会员经验
		FLOAT               	PurchaseDisc;                               	//折扣率
		USHORT              	PurchaseResult;                             	//购买结果，0表示成功 其他表示错误码
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPurchaseVip);

	struct CGFetchVipItem
	{
		static const USHORT	wCmd = 0x018e;

		UINT                	VipID;                                      	//Vip方案号
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGFetchVipItem);

	struct GCFetchVipItem
	{
		static const USHORT	wCmd = 0x10fc;

		UINT                	VipID;                                      	//vip方案号
		BYTE                	VipItemLen;                                 	//道具包长度
		VipItem             	VipItemArr[ITEMS_PER_PAGE];                   	//道具包数组
		USHORT              	Result;                                     	//0 表示查询成功 其他表示错误码
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCFetchVipItem);

	struct CGPurchaseGift
	{
		static const USHORT	wCmd = 0x018f;

		BYTE                	PurchaseType;                               	//购买类型 0 星钻 1 积分
		UINT                	ItemID;                                     	//道具类型ID
		UINT                	ItemNum;                                    	//道具数目
		BYTE                	DestAccountLen;                             	//接受方玩家账号长度
		CHAR                	DestAccount[MAX_NAME_SIZE];                   	//接受方玩家账号
		UINT                	DestRoleID;                                 	//接受方玩家角色
		UINT                	CliCalc;                                    	//客户端计算的金额数目
		BYTE                	NoteLen;                                    	//备注长度
		CHAR                	Note[MAX_NOTE_SIZE];                          	//留言
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPurchaseGift);

	struct CGPetMove
	{
		static const USHORT	wCmd = 0x0190;

		PlayerID            	petOwner;                                   	//宠物的主人
		FLOAT               	fOrgWorldPosX;                              	//移动出发点X客户端坐标；
		FLOAT               	fOrgWorldPosY;                              	//移动出发点Y客户端坐标；
		FLOAT               	fNewWorldPosX;                              	//移动目标点X客户端坐标；
		FLOAT               	fNewWorldPosY;                              	//移动目标点Y客户端坐标；
		FLOAT               	fPetSpeed;                                  	//宠物的速度
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGPetMove);

	struct GCPurchaseGift
	{
		static const USHORT	wCmd = 0x10fd;

		UINT                	TransID;                                    	//交易流水号
		UINT                	ItemID;                                     	//道具类型ID
		UINT                	ItemNum;                                    	//道具数目
		BYTE                	DestAccountLen;                             	//接受方玩家账号长度
		CHAR                	DestAccount[MAX_NAME_SIZE];                   	//接受方玩家账号
		UINT                	DestRoleID;                                 	//接受方玩家角色
		UINT                	GameMoney;                                  	//奇迹星钻余额
		UINT                	Score;                                      	//积分余额
		BYTE                	MemberClass;                                	//会员等级
		BYTE                	MemberLevel;                                	//会员级别
		UINT                	MemberExp;                                  	//会员经验
		FLOAT               	PurchaseDisc;                               	//折扣率
		USHORT              	PurchaseResult;                             	//购买结果，0表示成功 其他表示错误码
		BYTE                	NoteLen;                                    	//备注长度
		CHAR                	Note[MAX_NOTE_SIZE];                          	//留言
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPurchaseGift);

	struct TransItem
	{
		UINT                	TransID;                                    	//交易流水号
		UINT                	ItemID;                                     	//道具ID
		UINT                	ItemNum;                                    	//道具数量
		UINT                	OpeDate;                                    	//操作时间
	};

	struct GCPetMove
	{
		static const USHORT	wCmd = 0x10fe;

		PlayerID            	petOwner;                                   	//宠物的主人
		FLOAT               	fOrgWorldPosX;                              	//移动出发点X客户端坐标；
		FLOAT               	fOrgWorldPosY;                              	//移动出发点Y客户端坐标；
		FLOAT               	fSpeed;                                     	//移动速度；
		FLOAT               	fNewWorldPosX;                              	//移动目标点X客户端坐标；
		FLOAT               	fNewWorldPosY;                              	//移动目标点Y客户端坐标；
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCPetMove);

	struct CGFetchNonVipItem
	{
		static const USHORT	wCmd = 0x0192;

		TransItem           	itemId;                                     	//道具
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(CGFetchNonVipItem);

	struct GCFetchNonVipItem
	{
		static const USHORT	wCmd = 0x104b;

		TransItem           	itemId;                                     	//道具
		USHORT              	Result;                                     	//领取结果
	};
	CLIPROTOCOL_T_MAXBYTES_CHECK(GCFetchNonVipItem);


	//Messages
	typedef enum Message_id_type
	{
		C_G_QUERY_PKG                           	=	0x0100,	//CGQueryPkg
		C_G_NET_DETECT                          	=	0x0101,	//CGNetDetect
		C_G_CHAT                                	=	0x0102,	//CGChat
		C_G_TIME_CHECK                          	=	0x0103,	//CGTimeCheck
		C_G_REQ_ROLEINFO                        	=	0x0104,	//CGReqRoleInfo
		C_G_REQ_MONSTERINFO                     	=	0x0105,	//CGReqMonsterInfo
		C_G_RUN                                 	=	0x0106,	//CGRun
		C_G_LOGIN                               	=	0x0107,	//CGLogin
		C_G_SELECT_ROLE                         	=	0x0108,	//CGSelectRole
		C_G_ENTER_WORLD                         	=	0x0109,	//CGEnterWorld
		C_G_SWITCH_ENTER_NEWMAP                 	=	0x010a,	//CGSwitchEnterNewMap
		C_G_ATTACK_MONSTER                      	=	0x010b,	//CGAttackMonster
		C_G_ATTACK_PLAYER                       	=	0x010c,	//CGAttackPlayer
		C_G_SWITCH_MAP_REQ                      	=	0x010d,	//CGSwitchMap
		C_G_CREATE_ROLE                         	=	0x010e,	//CGCreateRole
		C_G_DELETE_ROLE                         	=	0x010f,	//CGDeleteRole
		C_G_SCRIPT_CHAT                         	=	0x0110,	//CGScriptChat
		C_G_DEFAULT_REBIRTH                     	=	0x0111,	//CGDefaultRebirth
		C_G_REBIRTH_READY                       	=	0x0112,	//CGRebirthReady
		C_G_PLAYER_COMBO                        	=	0x0113,	//CGPlayerCombo
		C_G_QUERYITEMPACKAGE_INFO               	=	0x0114,	//CGQueryItemPackageInfo
		C_G_PICKITEM                            	=	0x0115,	//CGPickItem
		C_G_PICKALLITEM                         	=	0x0116,	//CGPickAllItems
		C_G_USEITEM                             	=	0x0117,	//CGUseItem
		C_G_UNUSEITEM                           	=	0x0118,	//CGUnUseItem
		C_G_SWAPITEM                            	=	0x0119,	//CGSWAPITEM
		C_G_DROPITEM                            	=	0x011a,	//CGDropItem
		C_G_ACTIVEMOUNT                         	=	0x011b,	//CGActiveMount
		C_G_INVITEMOUNT                         	=	0x011c,	//CGInviteMount
		C_G_AGREE_BEINVITE                      	=	0x011d,	//CGAgreeBeInvite
		C_G_LEAVE_MOUNTTEAM                     	=	0x011e,	//CGLeaveMountTeam
		C_G_REQ_BUFFEND                         	=	0x011f,	//CGReqBuffEnd
		C_G_INVITE_PLAYER_JOIN_TEAM             	=	0x0120,	//CGInvitePlayerJoinTeam
		C_G_RECV_INVITE_JOIN_TEAM_FEEDBACK      	=	0x0121,	//CGRecvInviteJoinTeamFeedBack
		C_G_REQ_QUIT_TEAM                       	=	0x0122,	//CGReqQuitTeam
		C_G_CHANGE_TEAM_EXP_MODE                	=	0x0123,	//CGChangeTeamExpMode
		C_G_CHANGE_TEAM_LEADER                  	=	0x0125,	//CGChangeLeader
		C_G_REQ_JOIN_TEAM                       	=	0x0126,	//CGReqJoinTeam
		C_G_BAN_TEAM_MEMBER                     	=	0x0127,	//CGBanTeamMember
		C_G_RECV_REQ_JOIN_TEAM_FEEDBACK         	=	0x0128,	//CGRecvReqJoinTeamFeedBack
		C_G_DESTORY_TEAM                        	=	0x0129,	//CGDestoryTeam
		C_G_TEAM_SWITCH                         	=	0x012a,	//CGTeamSwitch
		C_G_USE_ASSISTSKILL                     	=	0x012b,	//CGUseAssistSkill
		C_G_CHANGE_TEAM_ITEM_MODE               	=	0x012c,	//CGChangeTeamItemMode
		C_G_ADDFRIEND_REQ                       	=	0x012d,	//CGAddFriendReq
		C_G_ADDFRIEND_RESPONSE                  	=	0x012e,	//CGAddFriendResponse
		C_G_DELFRIEND_REQ                       	=	0x012f,	//CGDelFriendReq
		C_G_CREATE_FRIENDGROUP_REQ              	=	0x0130,	//CGCreateFriendGroupReq
		C_G_CHG_FRIENDGROUPNAME_REQ             	=	0x0132,	//CGChgFriendGroupNameReq
		C_G_DEL_FRIENDGROUP_REQ                 	=	0x0133,	//CGDelFriendGroupReq
		C_G_MOVE_FRIENDGROUP_REQ                	=	0x0134,	//CGMoveFriendGroupReq
		C_G_FRIEND_CHAT                         	=	0x0135,	//CGFriendChat
		C_G_ADD_BLACKLIST_REQ                   	=	0x0136,	//CGAddBlacklistReq
		C_G_DEL_BLACKLIST_REQ                   	=	0x0137,	//CGDelBlacklistReq
		C_G_LOCKFOE_REQ                         	=	0x0138,	//CGLockFoeReq
		C_G_QUERYFOE_REQ                        	=	0x0139,	//CGQueryFoeReq
		C_G_FRIEND_TO_BLACKLIST_REQ             	=	0x013a,	//CGFriendToBlacklistReq
		C_G_ROLL_ITEM_FEEDBACK                  	=	0x013b,	//CGRollItemFeedBack
		C_G_ADDSTH_TO_PLATE                     	=	0x013c,	//CGAddSthToPlate
		C_G_TAKESTH_FROM_PLATE                  	=	0x013d,	//CGTakeSthFromPlate
		C_G_PLATE_EXEC                          	=	0x013e,	//CGPlateExec
		C_G_CLOSE_PLATE                         	=	0x013f,	//CGClosePlate
		C_G_SEND_TRADE_REQ                      	=	0x0140,	//CGSendTradeReq
		C_G_SEND_TRADE_REQ_RESULT               	=	0x0141,	//CGSendTradeReqResult
		C_G_ADD_OR_DELETE_ITEM                  	=	0x0142,	//CGAddOrDeleteItem
		C_G_CANCEL_TRADE                        	=	0x0143,	//CGCancelTrade
		C_G_LOCK_TRADE                          	=	0x0144,	//CGLockTrade
		C_G_CONFIRM_TRADE                       	=	0x0145,	//CGConfirmTrade
		C_G_CHANGE_REQ_SWITCH                   	=	0x0146,	//CGChangeReqSwitch
		C_G_CHANGE_TEAM_ROLL_LEVEL              	=	0x0147,	//CGChangeTeamRollLevel
		C_G_DEL_FOE_REQ                         	=	0x0148,	//CGDelFoeReq
		C_G_SET_TRADE_MONEY                     	=	0x0149,	//CGSetTradeMoney
		C_G_SPLIT_ITEM                          	=	0x014a,	//CGSplitItem
		C_G_REPAIR_WEAR_BY_ITEM                 	=	0x014b,	//CGRepairWearByItem
		C_G_UPGRADE_SKILL                       	=	0x014c,	//CGUpgradeSkill
		C_G_ADD_SPSKILL_REQ                     	=	0x014d,	//CGAddSpSkillReq
		C_G_REQ_RDSTR                           	=	0x014e,	//CGRequestRandomString
		C_G_USER_MD5VALUE                       	=	0x014f,	//CGUserMD5Value
		C_G_EQUIPITEM                           	=	0x0150,	//CGEquipItem
		C_G_QUERY_TASKRDINFO                    	=	0x0151,	//CGQueryTaskRecordInfo
		C_G_GET_CLIENT_DATA                     	=	0x0152,	//CGGetClientData
		C_G_SAVE_CLIENT_DATA                    	=	0x0153,	//CGSaveClientData
		C_G_DELETE_TASK                         	=	0x0154,	//CGDeleteTask
		C_G_SHARE_TASK                          	=	0x0155,	//CGShareTask
		C_G_TIMED_WEAR_IS_ZERO                  	=	0x0156,	//CGTimedWearIsZero
		C_G_CREATE_CHAT_GROUP                   	=	0x0157,	//CGCreateChatGroup
		C_G_DESTROY_CHAT_GROUP                  	=	0x0158,	//CGDestroyChatGroup
		C_G_ADD_CHAT_GROUP_MEMBER_BY_OWNER      	=	0x0159,	//CGAddChatGroupMemberByOwner
		C_G_ADD_CHAT_GROUP_MEMBER               	=	0x015a,	//CGAddChatGroupMember
		C_G_REMOVE_CHAT_GROUP_MEMBER            	=	0x015b,	//CGRemoveChatGroupMember
		C_G_CHAT_GROUP_SPEAK                    	=	0x015c,	//CGChatGroupSpeak
		C_G_CLIENT_SYNCHTIME                    	=	0x015d,	//CGClinetSynchTime
		C_G_IRC_ENTER_WORLD                     	=	0x015e,	//CGIrcEnterWorld
		C_G_QUERY_SHOP                          	=	0x015f,	//CGQueryShop
		C_G_SHOP_BUY_REQ                        	=	0x0160,	//CGShopBuyReq
		C_G_ITEM_SOLD_REQ                       	=	0x0161,	//CGItemSoldReq
		C_G_REPAIR_WEAR_BY_NPC                  	=	0x0162,	//CGRepairWearByNPC
		C_G_CHANGE_BATTLEMODE                   	=	0x0163,	//CGChangeBattleMode
		C_G_ADD_APT_REQ                         	=	0x0164,	//CGAddActivePtReq
		C_G_QUERY_EVILTIME                      	=	0x0165,	//CGQueryEvilTime
		C_G_USE_DAMAGE_ITEM                     	=	0x0166,	//CGUseDamageItem
		C_G_QUERY_REPAIRITEM_COST               	=	0x0167,	//CGQueryRepairItemCost
		C_G_IRC_QUERY_PLAYERINFO                	=	0x0168,	//CGIrcQueryPlayerInfo
		C_G_PLAYER_BEYONDBLOCK                  	=	0x0169,	//CGPlayerBeyondBlock
		C_G_SWAP_EQUIPITEM                      	=	0x016a,	//CGSwapEquipItem
		C_G_QUERY_ITEMPKG_PAGE                  	=	0x016b,	//CGQueryItemPkgPage
		C_G_CREATE_UNION_REQ                    	=	0x016c,	//CGCreateUnionRequest
		C_G_DESTROY_UNION_REQ                   	=	0x016d,	//CGDestroyUnionRequest
		C_G_QUERY_UNINO_BASE_REQ                	=	0x016e,	//CGQueryUnionBasicRequest
		C_G_QUERY_UNION_MEMBER_REQ              	=	0x016f,	//CGQueryUnionMembersRequest
		C_G_ADD_UNION_MEMBER_REQ                	=	0x0170,	//CGAddUnionMemberRequest
		C_G_ADD_UNION_MEMBER_CONFIRMED          	=	0x0171,	//CGAddUnionMemberConfirmed
		C_G_REMOVE_UNION_MEMBER_REQ             	=	0x0172,	//CGRemoveUnionMemberRequest
		C_G_CHANGE_PET_NAME                     	=	0x0173,	//CGChangePetName
		C_G_CLOSE_PET                           	=	0x0174,	//CGClosePet
		C_G_CHANGE_PET_SKILL_STATE              	=	0x0175,	//CGChangePetSkillState
		C_G_CHANGE_PET_IMAGE                    	=	0x0176,	//CGChangePetImage
		C_G_MOD_UNION_POS_RIG_REQ               	=	0x0177,	//CGModifyUnionPosRighstReq
		C_G_ADD_UNION_POS_REQ                   	=	0x0178,	//CGAddUnionPosReq
		C_G_ROM_UNION_POS_REQ                   	=	0x0179,	//CGRemoveUnionPosReq
		C_G_TRA_UNION_CAP_REQ                   	=	0x017a,	//CGTransformUnionCaptionReq
		C_G_MOD_UNION_MBR_TTL_REQ               	=	0x017b,	//CGModifyUnionMemberTitleReq
		C_G_ADV_UNION_MBR_POS_REQ               	=	0x017c,	//CGAdvanceUnionMemberPosReq
		C_G_RED_UNION_MBR_POS_REQ               	=	0x017d,	//CGReduceUnionMemberPosReq
		C_G_SPE_UNION_CHA_REQ                   	=	0x017e,	//CGUnionChannelSpeekReq
		C_G_UPD_UNION_POS_CFG_REQ               	=	0x017f,	//CGUpdateUnionPosCfgReq
		C_G_SUMMON_PET                          	=	0x0180,	//CGSummonPet
		C_G_FOR_UNION_SPEEK_REQ                 	=	0x0181,	//CGForbidUnionSpeekReq
		C_G_QUERY_RANKINFO_BYPAGE               	=	0x0182,	//CGQueryRankInfoByPageIndex
		C_G_QUERY_RANKINFO_BYNAME               	=	0x0183,	//CGQueryRankInfoByPlayerName
		C_G_ISSUE_UNION_TASK_REQ                	=	0x0184,	//CGIssueUnionTaskReq
		C_G_LEARN_UNION_SKILL_REQ               	=	0x0185,	//CGLearnUnionSkillReq
		C_G_LEAVE_WAIT_QUEUE                    	=	0x0186,	//CGLeaveWaitQueue
		C_G_USE_PARTION_SKILL                   	=	0x0187,	//CGUsePartionSkill
		C_G_ADV_UNION_LEVEL_REQ                 	=	0x0188,	//CGAdvanceUnionLevelReq
		C_G_POS_UNION_BULLETIN_REQ              	=	0x0189,	//CGPostUnionBulletinReq
		C_G_QUERY_ACCOUNT_INFO                  	=	0x018a,	//CGQueryAccountInfo
		C_G_PURCHASE_ITEM                       	=	0x018b,	//CGPurchaseItem
		C_G_QUERY_VIP_INFO                      	=	0x018c,	//CGQueryVipInfo
		C_G_PURCHASE_VIP                        	=	0x018d,	//CGPurchaseVip
		C_G_FETCH_VIP_ITEM                      	=	0x018e,	//CGFetchVipItem
		C_G_PURCHASE_GIFT                       	=	0x018f,	//CGPurchaseGift
		C_G_PET_MOVE                            	=	0x0190,	//CGPetMove
		C_G_QUERY_UNION_LOG_REQ                 	=	0x0191,	//CGQueryUnionLogReq
		C_G_FETCH_NON_VIP_ITEM                  	=	0x0192,	//CGFetchNonVipItem
		C_G_SENDMAIL_TO_PLAYER                  	=	0x0193,	//CGSendMailToPlayer
		C_G_DELETE_MAIL                         	=	0x0194,	//CGDeleteMail
		C_G_QUERY_MAILLIST_BYPAGE               	=	0x0195,	//CGQueryMailListByPage
		C_G_QUERY_MAIL_DETIALINFO               	=	0x0196,	//CGQueryMailDetailInfo
		C_G_PICK_MAIL_ATTACHITEM                	=	0x0197,	//CGPickMailAttachItem
		C_G_PICK_MAIL_ATTACHMONEY               	=	0x0198,	//CGPickMailAttachMoney
		G_C_NET_DETECT                          	=	0x1000,	//GCNetDetect
		G_C_NET_BIAS                            	=	0x1001,	//GCNetBias
		G_C_CHAT                                	=	0x1002,	//GCChat
		G_C_ROLEATT_UPDATE                      	=	0x1003,	//GCRoleattUpdate
		G_C_PLAYER_LEVELUP                      	=	0x1004,	//GCPlayerLevelUp
		G_C_TMPINFO_UPDATE                      	=	0x1005,	//GCTmpInfoUpdate
		G_C_PLAYER_PKG                          	=	0x1006,	//GCPlayerPkg
		G_C_TIME_CHECK                          	=	0x1007,	//GCTimeCheck
		G_C_RUN                                 	=	0x1008,	//GCRun
		G_C_PLAYER_POS_INFO                     	=	0x1009,	//GCPlayerPosInfo
		G_C_PLAYER_SELF_INFO                    	=	0x100a,	//GCPlayerSelfInfo
		G_C_PLAYER_APPEAR                       	=	0x100b,	//GCPlayerAppear
		G_C_PLAYER_LEAVE                        	=	0x100c,	//GCPlayerLeave
		G_C_PLAYER_LOGIN                        	=	0x100d,	//GCPlayerLogin
		G_C_PLAYER_ROLE                         	=	0x100e,	//GCPlayerRole
		G_C_OTHER_PLAYER_INFO                   	=	0x100f,	//GCOtherPlayerInfo
		G_C_SELECT_ROLE                         	=	0x1010,	//GCSelectRole
		G_C_PLAYER_CREATE_ROLE                  	=	0x1011,	//GCCreateRole
		G_C_DELETE_ROLE                         	=	0x1012,	//GCDeleteRole
		G_C_PLAYER_DYING                        	=	0x1013,	//GCPlayerDying
		G_C_REBIRTH_INFO                        	=	0x1014,	//GCRebirthInfo
		G_C_MONSTER_BORN                        	=	0x1015,	//GCMonsterBorn
		G_C_MONSTER_MOVE                        	=	0x1016,	//GCMonsterMove
		G_C_MONSTER_DISAPPEAR                   	=	0x1017,	//GCMonsterDisappear
		G_C_PLAYER_ATTACK_TARGET                	=	0x1018,	//GCPlayerAttackTarget
		G_C_MONSTER_ATTACK_PLAYER               	=	0x1019,	//GCMonsterAttackPlayer
		G_C_SWITCH_MAP                          	=	0x101a,	//GCSwitchMap
		G_C_PLAYER_ATTACK_PLAYER                	=	0x101b,	//GCPlayerAttackPlayer
		G_C_MONSTER_INFO                        	=	0x101c,	//GCMonsterInfo
		G_C_PLAYER_COMBO                        	=	0x101d,	//GCPlayerCombo
		G_C_SRV_ERROR                           	=	0x101e,	//GCSrvError
		G_C_ITEMSPACKAGE_APPEAR                 	=	0x101f,	//GCItemPackageAppear
		G_C_ITEMSPACKAGE_DISAPPEAR              	=	0x1020,	//GCItemsPkgDisappear
		G_C_ITEMSPACKAGE_INFO                   	=	0x1021,	//GCItemPackageInfo
		G_C_ONEPLAYER_PICKITEM                  	=	0x1022,	//GCPlayerPickItem
		G_C_WEAPONITEM_INFO                     	=	0x1023,	//GCWeaponPackageInfo
		G_C_EQUIPITEM_INFO                      	=	0x1024,	//GCEquipPackageInfo
		G_C_PICKITEM_FAIL                       	=	0x1025,	//GCPickItemFail
		G_C_USEITEM                             	=	0x1026,	//GCUseItem
		G_C_USEITEM_RESULT                      	=	0x1027,	//GCUseItemResult
		G_C_UNUSEITEM                           	=	0x1028,	//GCUnUseItem
		G_C_UNUSEITEM_RESULT                    	=	0x1029,	//GCUnUseItemResult
		G_C_SWAPITEM_RESULT                     	=	0x102a,	//GCSwapItemResult
		G_C_ITEM_DETAILINFO                     	=	0x102b,	//GCItemDetialInfo
		G_C_EQUIP_CONCISEINFO                   	=	0x102c,	//GCEquipConciseInfo
		G_C_ITEMPKG_CONCISEINFO                 	=	0x102d,	//GCItemPkgConciseInfo
		G_C_DROPITEM_RESULT                     	=	0x102e,	//GCDropItemResult
		G_C_MONEY_UPDATE                        	=	0x102f,	//GCMoneyUpdate
		G_C_ACTIVEMOUNT_FAIL                    	=	0x1030,	//GCActiveMountFail
		G_C_MOUNTLEADER                         	=	0x1031,	//GCMountLeader
		G_C_MOUNTASSIST                         	=	0x1032,	//GCMountAssist
		G_C_MOUNTTEAM_APPEAR                    	=	0x1033,	//GCMountTeamInfo
		G_C_MOUNTTEAM_DISBAND                   	=	0x1034,	//GCMountTeamDisBand
		G_C_PLAYER_LEAVE_MOUNTTEAM              	=	0x1035,	//GCPlayerLeaveMountTeam
		G_C_JOIN_MOUNTTEAM                      	=	0x1036,	//GCJoinMountTeam
		G_C_BEINVITE_MOUNTTEAM                  	=	0x1037,	//GCBeInviteMount
		G_C_INVITE_RESULT                       	=	0x1038,	//GCInviteResult
		G_C_MOUNTTEAM_MOVE                      	=	0x1039,	//GCMountTeamMove
		G_C_PLAYER_BUFFSTART                    	=	0x103a,	//GCPlayerBuffStart
		G_C_PLAYER_BUFFEND                      	=	0x103b,	//GCPlayerBuffEnd
		G_C_PLAYER_BUFFUPDATE                   	=	0x103c,	//GCPlayerBuffUpdate
		G_C_MONSTER_BUFFSTART                   	=	0x103d,	//GCMonsterBuffStart
		G_C_MONSTER_BUFFUPDATE                  	=	0x103e,	//GCMonsterBuffUpdate
		G_C_REQ_BUFFEND                         	=	0x103f,	//GCReqBuffEnd
		G_C_INVITE_PLAYER_JOIN_TEAM             	=	0x1040,	//GCInvitePlayerJoinTeam
		G_C_RECV_INVITE_JOIN_TEAM               	=	0x1041,	//GCRecvInviteJoinTeam
		G_C_RECV_INVITE_JOIN_TEAM_FEEDBACK_ERROR	=	0x1042,	//GCRecvInviteJoinTeamFeedBackError
		G_C_REQ_QUIT_TEAM                       	=	0x1043,	//GCReqQuitTeam
		G_C_CHANGE_TEAM_EXP_MODE                	=	0x1045,	//GCChangeTeamExpMode
		G_C_CHANGE_TEAM_LEADER                  	=	0x1046,	//GCChangeLeader
		G_C_TEAM_MEMBER_DETAILINFO_UPDATE       	=	0x1047,	//GCTeamMemberDetailInfoUpdate
		G_C_REQ_JOIN_TEAM_ERROR                 	=	0x1048,	//GCReqJoinTeamError
		G_C_BAN_TEAM_MEMBER                     	=	0x1049,	//GCBanTeamMember
		G_C_RECV_REQ_JOIN_TEAM_FEEDBACK_ERROR   	=	0x105a,	//GCRecvReqJoinTeamFeedBackError
		G_C_DESTORY_TEAM                        	=	0x105b,	//GCDestoryTeam
		G_C_TEAM_SWITCH                         	=	0x105c,	//GCTeamSwitch
		G_C_PICKITEM_SUCCESS                    	=	0x105d,	//GCPickItemSuccess
		G_C_USE_ASSISTSKILL                     	=	0x105e,	//GCUseAssistSkill
		G_C_SET_PLAYER_POS                      	=	0x105f,	//GCSetPlayerPos
		G_C_CHANGE_TEAM_ITEM_MODE               	=	0x1060,	//GCChangeTeamItemMode
		G_C_FRIEND_GROUPS                       	=	0x1061,	//GCFriendGroups
		G_C_FRIENDS                             	=	0x1062,	//GCPlayerFriends
		G_C_FRIEND_INFO_UPDATE                  	=	0x1063,	//GCFriendInfoUpdate
		G_C_ADDFRIEND_REQ                       	=	0x1064,	//GCAddFriendReq
		G_C_DELFRIEND_RESPONSE                  	=	0x1065,	//GCDelFriendResponse
		G_C_CREATE_FRIENDGROUP_RESPONSE         	=	0x1066,	//GCCreateFriendGroupResponse
		G_C_CHG_FRIENDGROUPNAME_RESPONSE        	=	0x1067,	//GCChgFriendGroupNameResponse
		G_C_DEL_FRIENDGROUP_RESPONSE            	=	0x1068,	//GCDelFriendGroupResponse
		G_C_MOVE_FRIENDGROUP_RESPONSE           	=	0x1069,	//GCMoveFriendGroupResponse
		G_C_FRIEND_CHAT                         	=	0x106a,	//GCFriendChat
		G_C_BLACKLIST                           	=	0x106b,	//GCPlayerBlacklist
		G_C_ADD_BLACKLIST_RESPONSE              	=	0x106c,	//GCAddBlacklistResponse
		G_C_DEL_BLACKLIST_RESPONSE              	=	0x106d,	//GCDelBlacklistResponse
		G_C_FOE_LIST                            	=	0x106e,	//GCFoeList
		G_C_NEW_FOE                             	=	0x106f,	//GCNewFoe
		G_C_DELFOE_RESPONSE                     	=	0x1070,	//GCDelFoeResponse
		G_C_LOCKFOE_RESPONSE                    	=	0x1071,	//GCLockFoeResponse
		G_C_QUERYFOE_RESPONSE                   	=	0x1072,	//GCQueryFoeResponse
		G_C_FRIEND_TO_BLACKLIST_RESPONSE        	=	0x1073,	//GCFriendToBlacklistResponse
		G_C_ADDFRIEND_RESPONSE                  	=	0x1074,	//GCAddFriendResponse
		G_C_REQ_AGREE_ROLL_ITEM                 	=	0x1075,	//GCReqAgreeRollItem
		G_C_ROLL_ITEM_RESULT                    	=	0x1076,	//GCRollItemResult
		G_C_SHOW_PLATE                          	=	0x1077,	//GCShowPlate
		G_C_ADDSTH_TO_PLATE_RST                 	=	0x1078,	//GCAddSthToPlateRst
		G_C_TAKESTH_FROM_PLATE_RST              	=	0x1079,	//GCTakeSthFromPlateRst
		G_C_PLATE_EXEC_RST                      	=	0x107a,	//GCPlateExecRst
		G_C_CLOSE_PLATE                         	=	0x107b,	//GCClosePlate
		G_C_SEND_TRADE_REQ_ERROR                	=	0x107c,	//GCSendTradeReqError
		G_C_RECV_TRADE_REQ                      	=	0x107d,	//GCRecvTradeReq
		G_C_SEND_TRADE_REQ_RESULT               	=	0x107e,	//GCSendTradeReqResult
		G_C_ADD_OR_DELETE_ITEM                  	=	0x107f,	//GCAddOrDeleteItem
		G_C_CANCEL_TRADE                        	=	0x1080,	//GCCancelTrade
		G_C_LOCK_TRADE                          	=	0x1081,	//GCLockTrade
		G_C_CONFIRM_TRADE                       	=	0x1082,	//GCConfirmTrade
		G_C_CHANGE_REQ_SWITCH                   	=	0x1083,	//GCChangeReqSwitch
		G_C_CHANGE_TEAM_ROLL_LEVEL              	=	0x1084,	//GCChangeTeamRollLevel
		G_C_PLAYER_NEWTASK                      	=	0x1085,	//GCPlayerNewTask
		G_C_PLAYER_TASKSTATUS                   	=	0x1086,	//GCPlayerTaskStatus
		G_C_RECV_REQ_JOIN_TEAM                  	=	0x1087,	//GCRecvReqJoinTeam
		G_C_ROLL_ITEM_NUM                       	=	0x1088,	//GCRollItemNum
		G_C_TEAMMEMBER_PICK_ITEM                	=	0x1089,	//GCTeamMemberPickItem
		G_C_MONSTER_BUFF_END                    	=	0x108a,	//GCMonsterBuffEnd
		G_C_SET_TRADE_MONEY                     	=	0x108b,	//GCSetTradeMoney
		G_C_SET_PKGITEM                         	=	0x108c,	//GCSetPkgItem
		G_C_FOEINFO_UPDATE                      	=	0x108d,	//GCFoeInfoUpdate
		G_C_LEAVE_VIEW                          	=	0x108e,	//GCLeaveView
		G_C_REPAIR_WEAR_BY_ITEM                 	=	0x108f,	//GCRepairWearByItem
		G_C_NOTIFY_ITEM_WEAR_CHANGE             	=	0x1090,	//GCNotifyItemWearChange
		G_C_UPGRADE_SKILL_RESULT                	=	0x1091,	//GCUpgradeSkillResult
		G_C_SPSKILL_UPGRADE                     	=	0x1092,	//GCSpSkillUpgrade
		G_C_PLAYERBUSYING                       	=	0x1093,	//GCPlayerIsBusying
		G_C_TEAMINFO                            	=	0x1094,	//GCTeamInfo
		G_C_ADD_SPSKILL_RESULT                  	=	0x1095,	//GCAddSpSkillResult
		G_C_THREAT_LIST                         	=	0x1096,	//GCThreatList
		G_C_CHANGEITEM_CRI                      	=	0x1097,	//GCChangeItemCri
		G_C_UPDATEITEM_ERROR                    	=	0x1098,	//GCUpdateItemError
		G_C_RANDOM_STRING                       	=	0x1099,	//GCRandomString
		G_C_CHANGE_FRIENDGROUP_COUNT            	=	0x109a,	//GCChangeGroupMsgNum
		G_C_UPDATE_TASKINFO                     	=	0x109b,	//GCUpdateTaskRecordInfo
		G_C_TASK_HISTORY                        	=	0x109c,	//GCTaskHistory
		G_C_GET_CLIENT_DATA                     	=	0x109d,	//GCGetClientData
		G_C_CHAT_ERROR                          	=	0x109e,	//GCChatError
		G_C_EQUIP_PLAERITEM                     	=	0x109f,	//GCPlayerEquipItem
		G_C_TASKUI_INFO                         	=	0x10a0,	//GCShowTaskUIInfo
		G_C_CLEAN_SKILLPOINT                    	=	0x10a1,	//GCCleanSkillPoint
		G_C_CREATE_CHAT_GROUP_RESULT            	=	0x10a2,	//GCCreateChatGroupResult
		G_C_DESTROY_CHAT_GROUP_RESULT           	=	0x10a3,	//GCDestroyChatGroupResult
		G_C_DESTROY_CHAT_GROUP_NOTIFY           	=	0x10a4,	//GCDestroyChatGroupNotify
		G_C_ADD_CHAT_GROUP_MEMBER_RESULT        	=	0x10a5,	//GCAddChatGroupMemberResult
		G_C_ADD_CHAT_GROUP_MEMBER_NOTIFY        	=	0x10a6,	//GCAddChatGroupMemberNotify
		G_C_REMOVE_CHAT_GROUP_MEMBER_RESULT     	=	0x10a7,	//GCRemoveChatGroupMemberResult
		G_C_REMOVE_CHAT_GROUP_MEMBER_NOTIFY     	=	0x10a8,	//GCRemoveChatGroupNotify
		G_C_CHAT_GROUP_SPEAK_RESULT             	=	0x10a9,	//GCChatGroupSpeakResult
		G_C_CHAT_GROUP_SPERK_NOTIFY             	=	0x10aa,	//GCChatGroupSpeakNotify
		G_C_SRV_SYNCHTIME                       	=	0x10ab,	//GCSrvSynchTime
		G_C_SYNCH_CLIENTTIME                    	=	0x10ac,	//GCSynchClientTime
		G_C_SHOW_SHOP                           	=	0x10ad,	//GCShowShop
		G_C_SHOP_BUY_RST                        	=	0x10ae,	//GCShopBuyRst
		G_C_ITEM_SOLD_RST                       	=	0x10af,	//GCItemSoldRst
		G_C_REPAIR_WEAR_BY_NPC                  	=	0x10b0,	//GCRepairWearByNPC
		G_C_IRC_ENTER_WORLD_RST                 	=	0x10b1,	//GCIrcEnterWorldRst
		G_C_PLAYER_ADD_HP                       	=	0x10b2,	//GCPlayerAddHp
		G_C_CHANGE_BATTLEMODE                   	=	0x10b3,	//GCChangeBattleMode
		G_C_BUFF_DATA                           	=	0x10b4,	//GCBuffData
		G_C_PLAYER_USE_HEAL_ITEM                	=	0x10b5,	//GCPlayerUseHealItem
		G_C_ADD_APT_RST                         	=	0x10b6,	//GCAddActivePtRst
		G_C_EVIL_TIME_RST                       	=	0x10b7,	//GCEvilTimeRst
		G_C_REPAIRITEM_COST_RST                 	=	0x10b8,	//GCQueryRepairItemCostRst
		G_C_IRC_QUERY_PLAYERINFO_RST            	=	0x10b9,	//GCIrcQueryPlayerInfoRst
		G_C_TARGET_PLAY_SOUND                   	=	0x10ba,	//GCTargetPlaySound
		G_C_TARGET_CHANGE_SIZE                  	=	0x10bb,	//GCTargetChangeSize
		G_C_INFO_UPDATE                         	=	0x10bc,	//GCInfoUpdate
		G_C_SETEQUIP_ITEM                       	=	0x10bd,	//GCSetEquipItem
		G_C_TARGET_GET_DAMAGE                   	=	0x10be,	//GCTargetGetDamage
		G_C_SWAP_EQUIPITEM_RST                  	=	0x10bf,	//GCSwapEquipResult
		G_C_CHANGE_TEAM_STATE                   	=	0x10c0,	//GCChangeTeamState
		G_C_CREATE_UNION_RST                    	=	0x10c1,	//GCCreateUnionResult
		G_C_DESTROY_UNION_RST                   	=	0x10c2,	//GCDestroyUnionResult
		G_C_DESTROY_UNION_NTF                   	=	0x10c3,	//GCUnionDestroyedNotify
		G_C_QUERY_UNION_BASIC_RST               	=	0x10c4,	//GCQueryUnionBasicResult
		G_C_QUERY_UNION_MEM_RST                 	=	0x10c5,	//GCQueryUnionMembersResult
		G_C_ADD_UNION_MEM_CFM                   	=	0x10c6,	//GCAddUnionMemberConfirm
		G_C_ADD_UNION_MEM_RST                   	=	0x10c7,	//GCAddUnionMemberResult
		G_C_ADD_UNION_MEM_NTF                   	=	0x10c8,	//GCAddUnionMemberNotify
		G_C_REMOVE_UNION_MEM_RST                	=	0x10c9,	//GCRemoveUnionMemberResult
		G_C_REMOVE_UNION_MEM_NTF                	=	0x10ca,	//GCRemoveUnionMemberNotify
		G_C_PUNISH_PLAYER_INFO                  	=	0x10cb,	//GCPunishPlayerInfo
		G_C_PETINFO_INSTABLE                    	=	0x10cc,	//GCPetinfoInstable
		G_C_PET_ABILITY                         	=	0x10cd,	//GCPetAbility
		G_C_PETINFO_STABLE                      	=	0x10ce,	//GCPetinfoStable
		G_C_UNION_POS_RIG_NTF                   	=	0x10d7,	//GCUnionPosRightsNtf
		G_C_MOD_UNION_POS_RIG_RST               	=	0x10d8,	//GCModifyUnionPosRighstRst
		G_C_ADD_UNION_POS_RST                   	=	0x10d9,	//GCAddUnionPosRst
		G_C_REM_UNION_POS_RST                   	=	0x10da,	//GCRemoveUnionPosRst
		G_C_TRA_UNION_CAP_RST                   	=	0x10db,	//GCTransformUnionCaptionRst
		G_C_TRA_UNION_CAP_NTF                   	=	0x10dc,	//GCTransformUnionCaptionNtf
		G_C_MOD_UNION_MBR_TTL_RST               	=	0x10dd,	//GCModifyUnionMemberTitleRst
		G_C_MOD_UNION_MBR_TTL_NTF               	=	0x10de,	//GCUnionMemberTitleNtf
		G_C_ADV_UNION_MBR_POS_RST               	=	0x10df,	//GCAdvanceUnionMemberPosRst
		G_C_ADV_UNION_MBR_POS_NTF               	=	0x10e0,	//GCAdvanceUnionMemberPosNtf
		G_C_RED_UNION_MBR_POS_RST               	=	0x10e1,	//GCReduceUnionMemberPosRst
		G_C_RED_UNION_MBR_POS_NTF               	=	0x10e2,	//GCReduceUnionMemberPosNtf
		G_C_SPE_UNION_CHA_RST                   	=	0x10e3,	//GCUnionChannelSpeekRst
		G_C_SPE_UNION_CHA_NTF                   	=	0x10e4,	//GCUnionChannelSpeekNtf
		G_C_UPD_UNION_POS_CON_RST               	=	0x10e5,	//GCUpdateUnionPosCfgRst
		G_C_UPD_UNION_POS_CON_NTF               	=	0x10e6,	//GCUpdateUnionPosCfgNtf
		G_C_FOR_UNION_SPEEK_RST                 	=	0x10e7,	//GCForbidUnionSpeekRst
		G_C_FOR_UNION_SPEEK_NTF                 	=	0x10e8,	//GCForbidUnionSpeekNtf
		G_C_RANKINFO_PAGE_RST                   	=	0x10e9,	//GCRankInfoByPageRst
		G_C_RANKINFO_NAME_RST                   	=	0x10ea,	//GCQueryRankPlayerByNameRst
		G_C_UNION_SKILLS                        	=	0x10eb,	//GCUnionSkills
		G_C_ISSUE_UNION_TASK_RST                	=	0x10ec,	//GCIssueUnionTaskRst
		G_C_ISSUE_UNION_TASK_NTF                	=	0x10ed,	//GCIssueUnionTaskNtf
		G_C_DISPLAY_UNION_TASKS                 	=	0x10ee,	//GCDisplayUnionTasks
		G_C_LEARN_UNION_SKILL_RST               	=	0x10ef,	//GCLearnUnionSkillRst
		G_C_LEARN_UNION_SKILL_NTF               	=	0x10f0,	//GCLearnUnionSkillNtf
		G_C_CHANGE_UNION_ACTIVE                 	=	0x10f1,	//GCChanageUnionActive
		G_C_WAIT_QUEUE_RESULT                   	=	0x10f2,	//GCWaitQueueResult
		G_C_USE_PARTION_SKILL_RST               	=	0x10f3,	//GCUsePartionSkillRst
		G_C_ADV_UNION_LEVEL_RST                 	=	0x10f4,	//GCAdvanceUnionLevelRst
		G_C_UNION_LEVEL_NTF                     	=	0x10f5,	//GCUnionLevelNtf
		G_C_POS_UNION_BULLETIN_RST              	=	0x10f6,	//GCPostUnionBulletinRst
		G_C_UNION_BULLETIN_NTF                  	=	0x10f7,	//GCUnionBulletinNtf
		G_C_ACCOUNT_INFO                        	=	0x10f8,	//GCAccountInfo
		G_C_PURCHASE_ITEM                       	=	0x10f9,	//GCPurchaseItem
		G_C_VIP_INFO                            	=	0x10fa,	//GCVipInfo
		G_C_PURCHASE_VIP                        	=	0x10fb,	//GCPurchaseVip
		G_C_FETCH_VIP_ITEM                      	=	0x10fc,	//GCFetchVipItem
		G_C_PURCHASE_GIFT                       	=	0x10fd,	//GCPurchaseGift
		G_C_PET_MOVE                            	=	0x10fe,	//GCPetMove
		G_C_UNION_MEM_ONOFF_LINE_NTF            	=	0x10ff,	//GCUnionMemberOnOffLineNtf
		G_C_QUERY_UNION_LOG_RST                 	=	0x104a,	//GCQueryUnionLogRst
		G_C_FETCH_NON_VIP_ITEM                  	=	0x104b,	//GCFetchNonVipItem
		G_C_UNION_UI_SELECT_CMD                 	=	0x104c,	//GCUnionUISelectCmd
		G_C_QUERY_MAILLIST_RST                  	=	0x1100,	//GCQueryMailListByPageRst
		G_C_MAIL_DETIAL_RST                     	=	0x1101,	//GCQueryMailDetialInfoRst
		G_C_MAIL_ATTACH_RST                     	=	0x1102,	//GCQueryMailAttachInfoRst
		G_C_SENDMAIL_ERROR                      	=	0x1103,	//GCSendMailError
		G_C_DELETE_MAIL_RST                     	=	0x1104,	//GCDeleteMailRst
		G_C_PICK_MAILITEM_RST                   	=	0x1105,	//GCPickMailAttachItemRst
		G_C_PICK_MAILMONEY_RST                  	=	0x1106,	//GCPickMailAttachMoneyRst
		G_C_HAVE_UNREAD_MAIL                    	=	0x1107,	//GCPlayerHaveUnReadMail
		C_A_GET_SRVLIST                         	=	0x0201,	//CAGetSrvList
		A_C_SRV_OPTION                          	=	0x2001,	//ACSrvOption
	};

	#pragma pack(pop)

	//Class Data
	class CliProtocol;
	typedef size_t (CliProtocol::*EnCodeFunc)(void* pData);
	typedef size_t (CliProtocol::*DeCodeFunc)(void* pData);

	class 
	#ifdef WIN32
	//_declspec(dllexport) 
	#endif
	CliProtocol
	{
	public:
		CliProtocol();
		~CliProtocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__PlayerID(void* pData);
		size_t	DeCode__PlayerID(void* pData);

		size_t	EnCode__PlayerIDEx(void* pData);
		size_t	DeCode__PlayerIDEx(void* pData);

		size_t	EnCode__TeamMemberCliInfo(void* pData);
		size_t	DeCode__TeamMemberCliInfo(void* pData);

		size_t	EnCode__PlayerInfo_1_Base(void* pData);
		size_t	DeCode__PlayerInfo_1_Base(void* pData);

		size_t	EnCode__ItemInfo_i(void* pData);
		size_t	DeCode__ItemInfo_i(void* pData);

		size_t	EnCode__SkillInfo_i(void* pData);
		size_t	DeCode__SkillInfo_i(void* pData);

		size_t	EnCode__BuffInfo(void* pData);
		size_t	DeCode__BuffInfo(void* pData);

		size_t	EnCode__PunishPlayerCliInfo(void* pData);
		size_t	DeCode__PunishPlayerCliInfo(void* pData);

		size_t	EnCode__BuffID(void* pData);
		size_t	DeCode__BuffID(void* pData);

		size_t	EnCode__BuffDetailInfo(void* pData);
		size_t	DeCode__BuffDetailInfo(void* pData);

		size_t	EnCode__PlayerInfo_2_Equips(void* pData);
		size_t	DeCode__PlayerInfo_2_Equips(void* pData);

		size_t	EnCode__PlayerInfo_4_Skills(void* pData);
		size_t	DeCode__PlayerInfo_4_Skills(void* pData);

		size_t	EnCode__PlayerInfo_5_Buffers(void* pData);
		size_t	DeCode__PlayerInfo_5_Buffers(void* pData);

		size_t	EnCode__PlayerInfoLogin(void* pData);
		size_t	DeCode__PlayerInfoLogin(void* pData);

		size_t	EnCode__PlayerTmpInfo(void* pData);
		size_t	DeCode__PlayerTmpInfo(void* pData);

		size_t	EnCode__PlayerBattleInfo(void* pData);
		size_t	DeCode__PlayerBattleInfo(void* pData);

		size_t	EnCode__EquipAdditonInfo(void* pData);
		size_t	DeCode__EquipAdditonInfo(void* pData);

		size_t	EnCode__WeaponPeculiarProp(void* pData);
		size_t	DeCode__WeaponPeculiarProp(void* pData);

		size_t	EnCode__EquipPeculiarProp(void* pData);
		size_t	DeCode__EquipPeculiarProp(void* pData);

		size_t	EnCode__WeaponAdditionProp(void* pData);
		size_t	DeCode__WeaponAdditionProp(void* pData);

		size_t	EnCode__EquipAdditionProp(void* pData);
		size_t	DeCode__EquipAdditionProp(void* pData);

		size_t	EnCode__EquipItemInfo(void* pData);
		size_t	DeCode__EquipItemInfo(void* pData);

		size_t	EnCode__ItemAddtionProp(void* pData);
		size_t	DeCode__ItemAddtionProp(void* pData);

		size_t	EnCode__TaskRecordInfo(void* pData);
		size_t	DeCode__TaskRecordInfo(void* pData);

		size_t	EnCode__FriendGroupInfo(void* pData);
		size_t	DeCode__FriendGroupInfo(void* pData);

		size_t	EnCode__FriendInfo(void* pData);
		size_t	DeCode__FriendInfo(void* pData);

		size_t	EnCode__FoeInfo(void* pData);
		size_t	DeCode__FoeInfo(void* pData);

		size_t	EnCode__BlackListItemInfo(void* pData);
		size_t	DeCode__BlackListItemInfo(void* pData);

		size_t	EnCode__FoeDetailInfo(void* pData);
		size_t	DeCode__FoeDetailInfo(void* pData);

		size_t	EnCode__WearChangedItems(void* pData);
		size_t	DeCode__WearChangedItems(void* pData);

		size_t	EnCode__ThreatCliInfo(void* pData);
		size_t	DeCode__ThreatCliInfo(void* pData);

		size_t	EnCode__ThreatList(void* pData);
		size_t	DeCode__ThreatList(void* pData);

		size_t	EnCode__GroupChatMemberInfo(void* pData);
		size_t	DeCode__GroupChatMemberInfo(void* pData);

		size_t	EnCode__UnionMember(void* pData);
		size_t	DeCode__UnionMember(void* pData);

		size_t	EnCode__UnionPosRight(void* pData);
		size_t	DeCode__UnionPosRight(void* pData);

		size_t	EnCode__Union(void* pData);
		size_t	DeCode__Union(void* pData);

		size_t	EnCode__UnionMembers(void* pData);
		size_t	DeCode__UnionMembers(void* pData);

		size_t	EnCode__MailConciseInfo(void* pData);
		size_t	DeCode__MailConciseInfo(void* pData);

		size_t	EnCode__CGPlayerBeyondBlock(void* pData);
		size_t	DeCode__CGPlayerBeyondBlock(void* pData);

		size_t	EnCode__CGAddActivePtReq(void* pData);
		size_t	DeCode__CGAddActivePtReq(void* pData);

		size_t	EnCode__CGQueryEvilTime(void* pData);
		size_t	DeCode__CGQueryEvilTime(void* pData);

		size_t	EnCode__CGUseDamageItem(void* pData);
		size_t	DeCode__CGUseDamageItem(void* pData);

		size_t	EnCode__CGAddSthToPlate(void* pData);
		size_t	DeCode__CGAddSthToPlate(void* pData);

		size_t	EnCode__CGTakeSthFromPlate(void* pData);
		size_t	DeCode__CGTakeSthFromPlate(void* pData);

		size_t	EnCode__CGPlateExec(void* pData);
		size_t	DeCode__CGPlateExec(void* pData);

		size_t	EnCode__CGClosePlate(void* pData);
		size_t	DeCode__CGClosePlate(void* pData);

		size_t	EnCode__CGQueryPkg(void* pData);
		size_t	DeCode__CGQueryPkg(void* pData);

		size_t	EnCode__CGNetDetect(void* pData);
		size_t	DeCode__CGNetDetect(void* pData);

		size_t	EnCode__CGChat(void* pData);
		size_t	DeCode__CGChat(void* pData);

		size_t	EnCode__CGTimeCheck(void* pData);
		size_t	DeCode__CGTimeCheck(void* pData);

		size_t	EnCode__CGReqRoleInfo(void* pData);
		size_t	DeCode__CGReqRoleInfo(void* pData);

		size_t	EnCode__CGReqMonsterInfo(void* pData);
		size_t	DeCode__CGReqMonsterInfo(void* pData);

		size_t	EnCode__CGRun(void* pData);
		size_t	DeCode__CGRun(void* pData);

		size_t	EnCode__CGRequestRandomString(void* pData);
		size_t	DeCode__CGRequestRandomString(void* pData);

		size_t	EnCode__CGUserMD5Value(void* pData);
		size_t	DeCode__CGUserMD5Value(void* pData);

		size_t	EnCode__CGLogin(void* pData);
		size_t	DeCode__CGLogin(void* pData);

		size_t	EnCode__CGSelectRole(void* pData);
		size_t	DeCode__CGSelectRole(void* pData);

		size_t	EnCode__CGEnterWorld(void* pData);
		size_t	DeCode__CGEnterWorld(void* pData);

		size_t	EnCode__CGIrcEnterWorld(void* pData);
		size_t	DeCode__CGIrcEnterWorld(void* pData);

		size_t	EnCode__GCIrcEnterWorldRst(void* pData);
		size_t	DeCode__GCIrcEnterWorldRst(void* pData);

		size_t	EnCode__CGSwitchEnterNewMap(void* pData);
		size_t	DeCode__CGSwitchEnterNewMap(void* pData);

		size_t	EnCode__CGAttackMonster(void* pData);
		size_t	DeCode__CGAttackMonster(void* pData);

		size_t	EnCode__CGAttackPlayer(void* pData);
		size_t	DeCode__CGAttackPlayer(void* pData);

		size_t	EnCode__CGSwitchMap(void* pData);
		size_t	DeCode__CGSwitchMap(void* pData);

		size_t	EnCode__CGCreateRole(void* pData);
		size_t	DeCode__CGCreateRole(void* pData);

		size_t	EnCode__CGDeleteRole(void* pData);
		size_t	DeCode__CGDeleteRole(void* pData);

		size_t	EnCode__CGScriptChat(void* pData);
		size_t	DeCode__CGScriptChat(void* pData);

		size_t	EnCode__CGQueryShop(void* pData);
		size_t	DeCode__CGQueryShop(void* pData);

		size_t	EnCode__CGShopBuyReq(void* pData);
		size_t	DeCode__CGShopBuyReq(void* pData);

		size_t	EnCode__CGItemSoldReq(void* pData);
		size_t	DeCode__CGItemSoldReq(void* pData);

		size_t	EnCode__CGQueryTaskRecordInfo(void* pData);
		size_t	DeCode__CGQueryTaskRecordInfo(void* pData);

		size_t	EnCode__CGDeleteTask(void* pData);
		size_t	DeCode__CGDeleteTask(void* pData);

		size_t	EnCode__CGShareTask(void* pData);
		size_t	DeCode__CGShareTask(void* pData);

		size_t	EnCode__GCUpdateTaskRecordInfo(void* pData);
		size_t	DeCode__GCUpdateTaskRecordInfo(void* pData);

		size_t	EnCode__GCTaskHistory(void* pData);
		size_t	DeCode__GCTaskHistory(void* pData);

		size_t	EnCode__CGDefaultRebirth(void* pData);
		size_t	DeCode__CGDefaultRebirth(void* pData);

		size_t	EnCode__CGRebirthReady(void* pData);
		size_t	DeCode__CGRebirthReady(void* pData);

		size_t	EnCode__CGPlayerCombo(void* pData);
		size_t	DeCode__CGPlayerCombo(void* pData);

		size_t	EnCode__GCPlayerNewTask(void* pData);
		size_t	DeCode__GCPlayerNewTask(void* pData);

		size_t	EnCode__GCPlayerTaskStatus(void* pData);
		size_t	DeCode__GCPlayerTaskStatus(void* pData);

		size_t	EnCode__GCShowShop(void* pData);
		size_t	DeCode__GCShowShop(void* pData);

		size_t	EnCode__GCShopBuyRst(void* pData);
		size_t	DeCode__GCShopBuyRst(void* pData);

		size_t	EnCode__GCItemSoldRst(void* pData);
		size_t	DeCode__GCItemSoldRst(void* pData);

		size_t	EnCode__GCAddActivePtRst(void* pData);
		size_t	DeCode__GCAddActivePtRst(void* pData);

		size_t	EnCode__GCEvilTimeRst(void* pData);
		size_t	DeCode__GCEvilTimeRst(void* pData);

		size_t	EnCode__GCShowPlate(void* pData);
		size_t	DeCode__GCShowPlate(void* pData);

		size_t	EnCode__GCAddSthToPlateRst(void* pData);
		size_t	DeCode__GCAddSthToPlateRst(void* pData);

		size_t	EnCode__GCTakeSthFromPlateRst(void* pData);
		size_t	DeCode__GCTakeSthFromPlateRst(void* pData);

		size_t	EnCode__GCPlateExecRst(void* pData);
		size_t	DeCode__GCPlateExecRst(void* pData);

		size_t	EnCode__GCClosePlate(void* pData);
		size_t	DeCode__GCClosePlate(void* pData);

		size_t	EnCode__GCNetDetect(void* pData);
		size_t	DeCode__GCNetDetect(void* pData);

		size_t	EnCode__GCNetBias(void* pData);
		size_t	DeCode__GCNetBias(void* pData);

		size_t	EnCode__GCChat(void* pData);
		size_t	DeCode__GCChat(void* pData);

		size_t	EnCode__GCRoleattUpdate(void* pData);
		size_t	DeCode__GCRoleattUpdate(void* pData);

		size_t	EnCode__GCPlayerLevelUp(void* pData);
		size_t	DeCode__GCPlayerLevelUp(void* pData);

		size_t	EnCode__GCTmpInfoUpdate(void* pData);
		size_t	DeCode__GCTmpInfoUpdate(void* pData);

		size_t	EnCode__GCPlayerPkg(void* pData);
		size_t	DeCode__GCPlayerPkg(void* pData);

		size_t	EnCode__GCTimeCheck(void* pData);
		size_t	DeCode__GCTimeCheck(void* pData);

		size_t	EnCode__GCRun(void* pData);
		size_t	DeCode__GCRun(void* pData);

		size_t	EnCode__GCPlayerPosInfo(void* pData);
		size_t	DeCode__GCPlayerPosInfo(void* pData);

		size_t	EnCode__GCPlayerSelfInfo(void* pData);
		size_t	DeCode__GCPlayerSelfInfo(void* pData);

		size_t	EnCode__GCPlayerAppear(void* pData);
		size_t	DeCode__GCPlayerAppear(void* pData);

		size_t	EnCode__GCPlayerLeave(void* pData);
		size_t	DeCode__GCPlayerLeave(void* pData);

		size_t	EnCode__GCRandomString(void* pData);
		size_t	DeCode__GCRandomString(void* pData);

		size_t	EnCode__GCPlayerLogin(void* pData);
		size_t	DeCode__GCPlayerLogin(void* pData);

		size_t	EnCode__GCPlayerRole(void* pData);
		size_t	DeCode__GCPlayerRole(void* pData);

		size_t	EnCode__GCOtherPlayerInfo(void* pData);
		size_t	DeCode__GCOtherPlayerInfo(void* pData);

		size_t	EnCode__GCSelectRole(void* pData);
		size_t	DeCode__GCSelectRole(void* pData);

		size_t	EnCode__GCCreateRole(void* pData);
		size_t	DeCode__GCCreateRole(void* pData);

		size_t	EnCode__GCDeleteRole(void* pData);
		size_t	DeCode__GCDeleteRole(void* pData);

		size_t	EnCode__GCPlayerDying(void* pData);
		size_t	DeCode__GCPlayerDying(void* pData);

		size_t	EnCode__GCRebirthInfo(void* pData);
		size_t	DeCode__GCRebirthInfo(void* pData);

		size_t	EnCode__GCMonsterBorn(void* pData);
		size_t	DeCode__GCMonsterBorn(void* pData);

		size_t	EnCode__GCMonsterMove(void* pData);
		size_t	DeCode__GCMonsterMove(void* pData);

		size_t	EnCode__GCMonsterDisappear(void* pData);
		size_t	DeCode__GCMonsterDisappear(void* pData);

		size_t	EnCode__GCPlayerAttackTarget(void* pData);
		size_t	DeCode__GCPlayerAttackTarget(void* pData);

		size_t	EnCode__GCMonsterAttackPlayer(void* pData);
		size_t	DeCode__GCMonsterAttackPlayer(void* pData);

		size_t	EnCode__GCSwitchMap(void* pData);
		size_t	DeCode__GCSwitchMap(void* pData);

		size_t	EnCode__GCPlayerAttackPlayer(void* pData);
		size_t	DeCode__GCPlayerAttackPlayer(void* pData);

		size_t	EnCode__GCMonsterInfo(void* pData);
		size_t	DeCode__GCMonsterInfo(void* pData);

		size_t	EnCode__GCPlayerCombo(void* pData);
		size_t	DeCode__GCPlayerCombo(void* pData);

		size_t	EnCode__GCSrvError(void* pData);
		size_t	DeCode__GCSrvError(void* pData);

		size_t	EnCode__GCItemPackageAppear(void* pData);
		size_t	DeCode__GCItemPackageAppear(void* pData);

		size_t	EnCode__GCItemsPkgDisappear(void* pData);
		size_t	DeCode__GCItemsPkgDisappear(void* pData);

		size_t	EnCode__CGQueryItemPackageInfo(void* pData);
		size_t	DeCode__CGQueryItemPackageInfo(void* pData);

		size_t	EnCode__GCItemPackageInfo(void* pData);
		size_t	DeCode__GCItemPackageInfo(void* pData);

		size_t	EnCode__GCPlayerPickItem(void* pData);
		size_t	DeCode__GCPlayerPickItem(void* pData);

		size_t	EnCode__GCWeaponPackageInfo(void* pData);
		size_t	DeCode__GCWeaponPackageInfo(void* pData);

		size_t	EnCode__GCEquipPackageInfo(void* pData);
		size_t	DeCode__GCEquipPackageInfo(void* pData);

		size_t	EnCode__CGPickItem(void* pData);
		size_t	DeCode__CGPickItem(void* pData);

		size_t	EnCode__CGPickAllItems(void* pData);
		size_t	DeCode__CGPickAllItems(void* pData);

		size_t	EnCode__GCSetPkgItem(void* pData);
		size_t	DeCode__GCSetPkgItem(void* pData);

		size_t	EnCode__GCSetEquipItem(void* pData);
		size_t	DeCode__GCSetEquipItem(void* pData);

		size_t	EnCode__GCTargetGetDamage(void* pData);
		size_t	DeCode__GCTargetGetDamage(void* pData);

		size_t	EnCode__GCPickItemFail(void* pData);
		size_t	DeCode__GCPickItemFail(void* pData);

		size_t	EnCode__CGUseItem(void* pData);
		size_t	DeCode__CGUseItem(void* pData);

		size_t	EnCode__GCUseItem(void* pData);
		size_t	DeCode__GCUseItem(void* pData);

		size_t	EnCode__GCUseItemResult(void* pData);
		size_t	DeCode__GCUseItemResult(void* pData);

		size_t	EnCode__CGUnUseItem(void* pData);
		size_t	DeCode__CGUnUseItem(void* pData);

		size_t	EnCode__GCUnUseItem(void* pData);
		size_t	DeCode__GCUnUseItem(void* pData);

		size_t	EnCode__GCUnUseItemResult(void* pData);
		size_t	DeCode__GCUnUseItemResult(void* pData);

		size_t	EnCode__CGSWAPITEM(void* pData);
		size_t	DeCode__CGSWAPITEM(void* pData);

		size_t	EnCode__CGSplitItem(void* pData);
		size_t	DeCode__CGSplitItem(void* pData);

		size_t	EnCode__GCSwapItemResult(void* pData);
		size_t	DeCode__GCSwapItemResult(void* pData);

		size_t	EnCode__GCItemDetialInfo(void* pData);
		size_t	DeCode__GCItemDetialInfo(void* pData);

		size_t	EnCode__GCEquipConciseInfo(void* pData);
		size_t	DeCode__GCEquipConciseInfo(void* pData);

		size_t	EnCode__GCItemPkgConciseInfo(void* pData);
		size_t	DeCode__GCItemPkgConciseInfo(void* pData);

		size_t	EnCode__GCDropItemResult(void* pData);
		size_t	DeCode__GCDropItemResult(void* pData);

		size_t	EnCode__CGEquipItem(void* pData);
		size_t	DeCode__CGEquipItem(void* pData);

		size_t	EnCode__CGDropItem(void* pData);
		size_t	DeCode__CGDropItem(void* pData);

		size_t	EnCode__GCMoneyUpdate(void* pData);
		size_t	DeCode__GCMoneyUpdate(void* pData);

		size_t	EnCode__CGActiveMount(void* pData);
		size_t	DeCode__CGActiveMount(void* pData);

		size_t	EnCode__CGInviteMount(void* pData);
		size_t	DeCode__CGInviteMount(void* pData);

		size_t	EnCode__CGAgreeBeInvite(void* pData);
		size_t	DeCode__CGAgreeBeInvite(void* pData);

		size_t	EnCode__CGLeaveMountTeam(void* pData);
		size_t	DeCode__CGLeaveMountTeam(void* pData);

		size_t	EnCode__GCActiveMountFail(void* pData);
		size_t	DeCode__GCActiveMountFail(void* pData);

		size_t	EnCode__GCMountLeader(void* pData);
		size_t	DeCode__GCMountLeader(void* pData);

		size_t	EnCode__GCMountAssist(void* pData);
		size_t	DeCode__GCMountAssist(void* pData);

		size_t	EnCode__GCMountTeamInfo(void* pData);
		size_t	DeCode__GCMountTeamInfo(void* pData);

		size_t	EnCode__GCMountTeamDisBand(void* pData);
		size_t	DeCode__GCMountTeamDisBand(void* pData);

		size_t	EnCode__GCPlayerLeaveMountTeam(void* pData);
		size_t	DeCode__GCPlayerLeaveMountTeam(void* pData);

		size_t	EnCode__GCJoinMountTeam(void* pData);
		size_t	DeCode__GCJoinMountTeam(void* pData);

		size_t	EnCode__GCBeInviteMount(void* pData);
		size_t	DeCode__GCBeInviteMount(void* pData);

		size_t	EnCode__GCInviteResult(void* pData);
		size_t	DeCode__GCInviteResult(void* pData);

		size_t	EnCode__GCMountTeamMove(void* pData);
		size_t	DeCode__GCMountTeamMove(void* pData);

		size_t	EnCode__GCPlayerBuffStart(void* pData);
		size_t	DeCode__GCPlayerBuffStart(void* pData);

		size_t	EnCode__GCPlayerBuffEnd(void* pData);
		size_t	DeCode__GCPlayerBuffEnd(void* pData);

		size_t	EnCode__GCPlayerBuffUpdate(void* pData);
		size_t	DeCode__GCPlayerBuffUpdate(void* pData);

		size_t	EnCode__GCMonsterBuffEnd(void* pData);
		size_t	DeCode__GCMonsterBuffEnd(void* pData);

		size_t	EnCode__GCMonsterBuffStart(void* pData);
		size_t	DeCode__GCMonsterBuffStart(void* pData);

		size_t	EnCode__GCMonsterBuffUpdate(void* pData);
		size_t	DeCode__GCMonsterBuffUpdate(void* pData);

		size_t	EnCode__CGReqBuffEnd(void* pData);
		size_t	DeCode__CGReqBuffEnd(void* pData);

		size_t	EnCode__GCReqBuffEnd(void* pData);
		size_t	DeCode__GCReqBuffEnd(void* pData);

		size_t	EnCode__CGInvitePlayerJoinTeam(void* pData);
		size_t	DeCode__CGInvitePlayerJoinTeam(void* pData);

		size_t	EnCode__GCInvitePlayerJoinTeam(void* pData);
		size_t	DeCode__GCInvitePlayerJoinTeam(void* pData);

		size_t	EnCode__GCRecvInviteJoinTeam(void* pData);
		size_t	DeCode__GCRecvInviteJoinTeam(void* pData);

		size_t	EnCode__CGRecvInviteJoinTeamFeedBack(void* pData);
		size_t	DeCode__CGRecvInviteJoinTeamFeedBack(void* pData);

		size_t	EnCode__GCRecvInviteJoinTeamFeedBackError(void* pData);
		size_t	DeCode__GCRecvInviteJoinTeamFeedBackError(void* pData);

		size_t	EnCode__CGReqQuitTeam(void* pData);
		size_t	DeCode__CGReqQuitTeam(void* pData);

		size_t	EnCode__GCReqQuitTeam(void* pData);
		size_t	DeCode__GCReqQuitTeam(void* pData);

		size_t	EnCode__CGChangeTeamExpMode(void* pData);
		size_t	DeCode__CGChangeTeamExpMode(void* pData);

		size_t	EnCode__CGChangeTeamItemMode(void* pData);
		size_t	DeCode__CGChangeTeamItemMode(void* pData);

		size_t	EnCode__GCChangeTeamExpMode(void* pData);
		size_t	DeCode__GCChangeTeamExpMode(void* pData);

		size_t	EnCode__GCChangeTeamItemMode(void* pData);
		size_t	DeCode__GCChangeTeamItemMode(void* pData);

		size_t	EnCode__GCTeamInfo(void* pData);
		size_t	DeCode__GCTeamInfo(void* pData);

		size_t	EnCode__CGChangeLeader(void* pData);
		size_t	DeCode__CGChangeLeader(void* pData);

		size_t	EnCode__GCChangeLeader(void* pData);
		size_t	DeCode__GCChangeLeader(void* pData);

		size_t	EnCode__GCTeamMemberDetailInfoUpdate(void* pData);
		size_t	DeCode__GCTeamMemberDetailInfoUpdate(void* pData);

		size_t	EnCode__CGReqJoinTeam(void* pData);
		size_t	DeCode__CGReqJoinTeam(void* pData);

		size_t	EnCode__GCReqJoinTeamError(void* pData);
		size_t	DeCode__GCReqJoinTeamError(void* pData);

		size_t	EnCode__CGBanTeamMember(void* pData);
		size_t	DeCode__CGBanTeamMember(void* pData);

		size_t	EnCode__GCBanTeamMember(void* pData);
		size_t	DeCode__GCBanTeamMember(void* pData);

		size_t	EnCode__GCRecvReqJoinTeam(void* pData);
		size_t	DeCode__GCRecvReqJoinTeam(void* pData);

		size_t	EnCode__CGRecvReqJoinTeamFeedBack(void* pData);
		size_t	DeCode__CGRecvReqJoinTeamFeedBack(void* pData);

		size_t	EnCode__GCRecvReqJoinTeamFeedBackError(void* pData);
		size_t	DeCode__GCRecvReqJoinTeamFeedBackError(void* pData);

		size_t	EnCode__CGDestoryTeam(void* pData);
		size_t	DeCode__CGDestoryTeam(void* pData);

		size_t	EnCode__GCDestoryTeam(void* pData);
		size_t	DeCode__GCDestoryTeam(void* pData);

		size_t	EnCode__CGTeamSwitch(void* pData);
		size_t	DeCode__CGTeamSwitch(void* pData);

		size_t	EnCode__GCTeamSwitch(void* pData);
		size_t	DeCode__GCTeamSwitch(void* pData);

		size_t	EnCode__GCPickItemSuccess(void* pData);
		size_t	DeCode__GCPickItemSuccess(void* pData);

		size_t	EnCode__CGUseAssistSkill(void* pData);
		size_t	DeCode__CGUseAssistSkill(void* pData);

		size_t	EnCode__GCUseAssistSkill(void* pData);
		size_t	DeCode__GCUseAssistSkill(void* pData);

		size_t	EnCode__CGAddFriendReq(void* pData);
		size_t	DeCode__CGAddFriendReq(void* pData);

		size_t	EnCode__GCFriendGroups(void* pData);
		size_t	DeCode__GCFriendGroups(void* pData);

		size_t	EnCode__GCPlayerFriends(void* pData);
		size_t	DeCode__GCPlayerFriends(void* pData);

		size_t	EnCode__GCFriendInfoUpdate(void* pData);
		size_t	DeCode__GCFriendInfoUpdate(void* pData);

		size_t	EnCode__GCAddFriendReq(void* pData);
		size_t	DeCode__GCAddFriendReq(void* pData);

		size_t	EnCode__CGAddFriendResponse(void* pData);
		size_t	DeCode__CGAddFriendResponse(void* pData);

		size_t	EnCode__GCAddFriendResponse(void* pData);
		size_t	DeCode__GCAddFriendResponse(void* pData);

		size_t	EnCode__CGDelFriendReq(void* pData);
		size_t	DeCode__CGDelFriendReq(void* pData);

		size_t	EnCode__GCDelFriendResponse(void* pData);
		size_t	DeCode__GCDelFriendResponse(void* pData);

		size_t	EnCode__CGCreateFriendGroupReq(void* pData);
		size_t	DeCode__CGCreateFriendGroupReq(void* pData);

		size_t	EnCode__GCCreateFriendGroupResponse(void* pData);
		size_t	DeCode__GCCreateFriendGroupResponse(void* pData);

		size_t	EnCode__CGChgFriendGroupNameReq(void* pData);
		size_t	DeCode__CGChgFriendGroupNameReq(void* pData);

		size_t	EnCode__GCChgFriendGroupNameResponse(void* pData);
		size_t	DeCode__GCChgFriendGroupNameResponse(void* pData);

		size_t	EnCode__CGDelFriendGroupReq(void* pData);
		size_t	DeCode__CGDelFriendGroupReq(void* pData);

		size_t	EnCode__GCDelFriendGroupResponse(void* pData);
		size_t	DeCode__GCDelFriendGroupResponse(void* pData);

		size_t	EnCode__CGMoveFriendGroupReq(void* pData);
		size_t	DeCode__CGMoveFriendGroupReq(void* pData);

		size_t	EnCode__GCMoveFriendGroupResponse(void* pData);
		size_t	DeCode__GCMoveFriendGroupResponse(void* pData);

		size_t	EnCode__CGFriendChat(void* pData);
		size_t	DeCode__CGFriendChat(void* pData);

		size_t	EnCode__GCFriendChat(void* pData);
		size_t	DeCode__GCFriendChat(void* pData);

		size_t	EnCode__GCPlayerBlacklist(void* pData);
		size_t	DeCode__GCPlayerBlacklist(void* pData);

		size_t	EnCode__CGAddBlacklistReq(void* pData);
		size_t	DeCode__CGAddBlacklistReq(void* pData);

		size_t	EnCode__GCAddBlacklistResponse(void* pData);
		size_t	DeCode__GCAddBlacklistResponse(void* pData);

		size_t	EnCode__CGDelBlacklistReq(void* pData);
		size_t	DeCode__CGDelBlacklistReq(void* pData);

		size_t	EnCode__GCDelBlacklistResponse(void* pData);
		size_t	DeCode__GCDelBlacklistResponse(void* pData);

		size_t	EnCode__GCFoeList(void* pData);
		size_t	DeCode__GCFoeList(void* pData);

		size_t	EnCode__GCNewFoe(void* pData);
		size_t	DeCode__GCNewFoe(void* pData);

		size_t	EnCode__CGDelFoeReq(void* pData);
		size_t	DeCode__CGDelFoeReq(void* pData);

		size_t	EnCode__GCDelFoeResponse(void* pData);
		size_t	DeCode__GCDelFoeResponse(void* pData);

		size_t	EnCode__CGLockFoeReq(void* pData);
		size_t	DeCode__CGLockFoeReq(void* pData);

		size_t	EnCode__GCFoeInfoUpdate(void* pData);
		size_t	DeCode__GCFoeInfoUpdate(void* pData);

		size_t	EnCode__GCLockFoeResponse(void* pData);
		size_t	DeCode__GCLockFoeResponse(void* pData);

		size_t	EnCode__CGQueryFoeReq(void* pData);
		size_t	DeCode__CGQueryFoeReq(void* pData);

		size_t	EnCode__GCQueryFoeResponse(void* pData);
		size_t	DeCode__GCQueryFoeResponse(void* pData);

		size_t	EnCode__CGFriendToBlacklistReq(void* pData);
		size_t	DeCode__CGFriendToBlacklistReq(void* pData);

		size_t	EnCode__GCFriendToBlacklistResponse(void* pData);
		size_t	DeCode__GCFriendToBlacklistResponse(void* pData);

		size_t	EnCode__CGRollItemFeedBack(void* pData);
		size_t	DeCode__CGRollItemFeedBack(void* pData);

		size_t	EnCode__GCRollItemResult(void* pData);
		size_t	DeCode__GCRollItemResult(void* pData);

		size_t	EnCode__GCReqAgreeRollItem(void* pData);
		size_t	DeCode__GCReqAgreeRollItem(void* pData);

		size_t	EnCode__GCTeamMemberPickItem(void* pData);
		size_t	DeCode__GCTeamMemberPickItem(void* pData);

		size_t	EnCode__CGSendTradeReq(void* pData);
		size_t	DeCode__CGSendTradeReq(void* pData);

		size_t	EnCode__GCSendTradeReqError(void* pData);
		size_t	DeCode__GCSendTradeReqError(void* pData);

		size_t	EnCode__GCRecvTradeReq(void* pData);
		size_t	DeCode__GCRecvTradeReq(void* pData);

		size_t	EnCode__CGSendTradeReqResult(void* pData);
		size_t	DeCode__CGSendTradeReqResult(void* pData);

		size_t	EnCode__GCSendTradeReqResult(void* pData);
		size_t	DeCode__GCSendTradeReqResult(void* pData);

		size_t	EnCode__CGAddOrDeleteItem(void* pData);
		size_t	DeCode__CGAddOrDeleteItem(void* pData);

		size_t	EnCode__GCAddOrDeleteItem(void* pData);
		size_t	DeCode__GCAddOrDeleteItem(void* pData);

		size_t	EnCode__CGSetTradeMoney(void* pData);
		size_t	DeCode__CGSetTradeMoney(void* pData);

		size_t	EnCode__GCSetTradeMoney(void* pData);
		size_t	DeCode__GCSetTradeMoney(void* pData);

		size_t	EnCode__CGCancelTrade(void* pData);
		size_t	DeCode__CGCancelTrade(void* pData);

		size_t	EnCode__GCCancelTrade(void* pData);
		size_t	DeCode__GCCancelTrade(void* pData);

		size_t	EnCode__CGLockTrade(void* pData);
		size_t	DeCode__CGLockTrade(void* pData);

		size_t	EnCode__GCLockTrade(void* pData);
		size_t	DeCode__GCLockTrade(void* pData);

		size_t	EnCode__CGConfirmTrade(void* pData);
		size_t	DeCode__CGConfirmTrade(void* pData);

		size_t	EnCode__GCConfirmTrade(void* pData);
		size_t	DeCode__GCConfirmTrade(void* pData);

		size_t	EnCode__CGChangeReqSwitch(void* pData);
		size_t	DeCode__CGChangeReqSwitch(void* pData);

		size_t	EnCode__GCChangeReqSwitch(void* pData);
		size_t	DeCode__GCChangeReqSwitch(void* pData);

		size_t	EnCode__CGChangeTeamRollLevel(void* pData);
		size_t	DeCode__CGChangeTeamRollLevel(void* pData);

		size_t	EnCode__CGDelFoeInfo(void* pData);
		size_t	DeCode__CGDelFoeInfo(void* pData);

		size_t	EnCode__GCChangeTeamRollLevel(void* pData);
		size_t	DeCode__GCChangeTeamRollLevel(void* pData);

		size_t	EnCode__GCRollItemNum(void* pData);
		size_t	DeCode__GCRollItemNum(void* pData);

		size_t	EnCode__GCChangeGroupMsgNum(void* pData);
		size_t	DeCode__GCChangeGroupMsgNum(void* pData);

		size_t	EnCode__GCLeaveView(void* pData);
		size_t	DeCode__GCLeaveView(void* pData);

		size_t	EnCode__CGRepairWearByItem(void* pData);
		size_t	DeCode__CGRepairWearByItem(void* pData);

		size_t	EnCode__CGQueryRepairItemCost(void* pData);
		size_t	DeCode__CGQueryRepairItemCost(void* pData);

		size_t	EnCode__GCQueryRepairItemCostRst(void* pData);
		size_t	DeCode__GCQueryRepairItemCostRst(void* pData);

		size_t	EnCode__CGIrcQueryPlayerInfo(void* pData);
		size_t	DeCode__CGIrcQueryPlayerInfo(void* pData);

		size_t	EnCode__GCIrcQueryPlayerInfoRst(void* pData);
		size_t	DeCode__GCIrcQueryPlayerInfoRst(void* pData);

		size_t	EnCode__GCTargetPlaySound(void* pData);
		size_t	DeCode__GCTargetPlaySound(void* pData);

		size_t	EnCode__GCTargetChangeSize(void* pData);
		size_t	DeCode__GCTargetChangeSize(void* pData);

		size_t	EnCode__GCInfoUpdate(void* pData);
		size_t	DeCode__GCInfoUpdate(void* pData);

		size_t	EnCode__GCRepairWearByItem(void* pData);
		size_t	DeCode__GCRepairWearByItem(void* pData);

		size_t	EnCode__GCNotifyItemWearChange(void* pData);
		size_t	DeCode__GCNotifyItemWearChange(void* pData);

		size_t	EnCode__CGTimedWearIsZero(void* pData);
		size_t	DeCode__CGTimedWearIsZero(void* pData);

		size_t	EnCode__CGUpgradeSkill(void* pData);
		size_t	DeCode__CGUpgradeSkill(void* pData);

		size_t	EnCode__CGAddSpSkillReq(void* pData);
		size_t	DeCode__CGAddSpSkillReq(void* pData);

		size_t	EnCode__GCAddSpSkillResult(void* pData);
		size_t	DeCode__GCAddSpSkillResult(void* pData);

		size_t	EnCode__GCThreatList(void* pData);
		size_t	DeCode__GCThreatList(void* pData);

		size_t	EnCode__GCUpgradeSkillResult(void* pData);
		size_t	DeCode__GCUpgradeSkillResult(void* pData);

		size_t	EnCode__GCPlayerIsBusying(void* pData);
		size_t	DeCode__GCPlayerIsBusying(void* pData);

		size_t	EnCode__GCSpSkillUpgrade(void* pData);
		size_t	DeCode__GCSpSkillUpgrade(void* pData);

		size_t	EnCode__GCChangeItemCri(void* pData);
		size_t	DeCode__GCChangeItemCri(void* pData);

		size_t	EnCode__GCUpdateItemError(void* pData);
		size_t	DeCode__GCUpdateItemError(void* pData);

		size_t	EnCode__CGGetClientData(void* pData);
		size_t	DeCode__CGGetClientData(void* pData);

		size_t	EnCode__GCGetClientData(void* pData);
		size_t	DeCode__GCGetClientData(void* pData);

		size_t	EnCode__CGSaveClientData(void* pData);
		size_t	DeCode__CGSaveClientData(void* pData);

		size_t	EnCode__GCPlayerEquipItem(void* pData);
		size_t	DeCode__GCPlayerEquipItem(void* pData);

		size_t	EnCode__GCShowTaskUIInfo(void* pData);
		size_t	DeCode__GCShowTaskUIInfo(void* pData);

		size_t	EnCode__GCCleanSkillPoint(void* pData);
		size_t	DeCode__GCCleanSkillPoint(void* pData);

		size_t	EnCode__CGChangeBattleMode(void* pData);
		size_t	DeCode__CGChangeBattleMode(void* pData);

		size_t	EnCode__GCChangeBattleMode(void* pData);
		size_t	DeCode__GCChangeBattleMode(void* pData);

		size_t	EnCode__GCBuffData(void* pData);
		size_t	DeCode__GCBuffData(void* pData);

		size_t	EnCode__GCPlayerUseHealItem(void* pData);
		size_t	DeCode__GCPlayerUseHealItem(void* pData);

		size_t	EnCode__CGSwapEquipItem(void* pData);
		size_t	DeCode__CGSwapEquipItem(void* pData);

		size_t	EnCode__CGQueryItemPkgPage(void* pData);
		size_t	DeCode__CGQueryItemPkgPage(void* pData);

		size_t	EnCode__GCSwapEquipResult(void* pData);
		size_t	DeCode__GCSwapEquipResult(void* pData);

		size_t	EnCode__GCChangeTeamState(void* pData);
		size_t	DeCode__GCChangeTeamState(void* pData);

		size_t	EnCode__CAGetSrvList(void* pData);
		size_t	DeCode__CAGetSrvList(void* pData);

		size_t	EnCode__ACSrvOption(void* pData);
		size_t	DeCode__ACSrvOption(void* pData);

		size_t	EnCode__GCSetPlayerPos(void* pData);
		size_t	DeCode__GCSetPlayerPos(void* pData);

		size_t	EnCode__GCChatError(void* pData);
		size_t	DeCode__GCChatError(void* pData);

		size_t	EnCode__CGCreateChatGroup(void* pData);
		size_t	DeCode__CGCreateChatGroup(void* pData);

		size_t	EnCode__GCCreateChatGroupResult(void* pData);
		size_t	DeCode__GCCreateChatGroupResult(void* pData);

		size_t	EnCode__CGDestroyChatGroup(void* pData);
		size_t	DeCode__CGDestroyChatGroup(void* pData);

		size_t	EnCode__GCDestroyChatGroupResult(void* pData);
		size_t	DeCode__GCDestroyChatGroupResult(void* pData);

		size_t	EnCode__GCDestroyChatGroupNotify(void* pData);
		size_t	DeCode__GCDestroyChatGroupNotify(void* pData);

		size_t	EnCode__CGAddChatGroupMemberByOwner(void* pData);
		size_t	DeCode__CGAddChatGroupMemberByOwner(void* pData);

		size_t	EnCode__CGAddChatGroupMember(void* pData);
		size_t	DeCode__CGAddChatGroupMember(void* pData);

		size_t	EnCode__GCAddChatGroupMemberResult(void* pData);
		size_t	DeCode__GCAddChatGroupMemberResult(void* pData);

		size_t	EnCode__GCAddChatGroupMemberNotify(void* pData);
		size_t	DeCode__GCAddChatGroupMemberNotify(void* pData);

		size_t	EnCode__CGRemoveChatGroupMember(void* pData);
		size_t	DeCode__CGRemoveChatGroupMember(void* pData);

		size_t	EnCode__GCRemoveChatGroupMemberResult(void* pData);
		size_t	DeCode__GCRemoveChatGroupMemberResult(void* pData);

		size_t	EnCode__GCRemoveChatGroupNotify(void* pData);
		size_t	DeCode__GCRemoveChatGroupNotify(void* pData);

		size_t	EnCode__CGChatGroupSpeak(void* pData);
		size_t	DeCode__CGChatGroupSpeak(void* pData);

		size_t	EnCode__GCChatGroupSpeakResult(void* pData);
		size_t	DeCode__GCChatGroupSpeakResult(void* pData);

		size_t	EnCode__GCChatGroupSpeakNotify(void* pData);
		size_t	DeCode__GCChatGroupSpeakNotify(void* pData);

		size_t	EnCode__CGClinetSynchTime(void* pData);
		size_t	DeCode__CGClinetSynchTime(void* pData);

		size_t	EnCode__GCSrvSynchTime(void* pData);
		size_t	DeCode__GCSrvSynchTime(void* pData);

		size_t	EnCode__GCSynchClientTime(void* pData);
		size_t	DeCode__GCSynchClientTime(void* pData);

		size_t	EnCode__CGRepairWearByNPC(void* pData);
		size_t	DeCode__CGRepairWearByNPC(void* pData);

		size_t	EnCode__GCRepairWearByNPC(void* pData);
		size_t	DeCode__GCRepairWearByNPC(void* pData);

		size_t	EnCode__GCPlayerAddHp(void* pData);
		size_t	DeCode__GCPlayerAddHp(void* pData);

		size_t	EnCode__CGCreateUnionRequest(void* pData);
		size_t	DeCode__CGCreateUnionRequest(void* pData);

		size_t	EnCode__GCCreateUnionResult(void* pData);
		size_t	DeCode__GCCreateUnionResult(void* pData);

		size_t	EnCode__CGDestroyUnionRequest(void* pData);
		size_t	DeCode__CGDestroyUnionRequest(void* pData);

		size_t	EnCode__GCDestroyUnionResult(void* pData);
		size_t	DeCode__GCDestroyUnionResult(void* pData);

		size_t	EnCode__GCUnionDestroyedNotify(void* pData);
		size_t	DeCode__GCUnionDestroyedNotify(void* pData);

		size_t	EnCode__CGQueryUnionBasicRequest(void* pData);
		size_t	DeCode__CGQueryUnionBasicRequest(void* pData);

		size_t	EnCode__GCQueryUnionBasicResult(void* pData);
		size_t	DeCode__GCQueryUnionBasicResult(void* pData);

		size_t	EnCode__CGQueryUnionMembersRequest(void* pData);
		size_t	DeCode__CGQueryUnionMembersRequest(void* pData);

		size_t	EnCode__GCQueryUnionMembersResult(void* pData);
		size_t	DeCode__GCQueryUnionMembersResult(void* pData);

		size_t	EnCode__CGAddUnionMemberRequest(void* pData);
		size_t	DeCode__CGAddUnionMemberRequest(void* pData);

		size_t	EnCode__GCAddUnionMemberConfirm(void* pData);
		size_t	DeCode__GCAddUnionMemberConfirm(void* pData);

		size_t	EnCode__CGAddUnionMemberConfirmed(void* pData);
		size_t	DeCode__CGAddUnionMemberConfirmed(void* pData);

		size_t	EnCode__GCAddUnionMemberResult(void* pData);
		size_t	DeCode__GCAddUnionMemberResult(void* pData);

		size_t	EnCode__GCAddUnionMemberNotify(void* pData);
		size_t	DeCode__GCAddUnionMemberNotify(void* pData);

		size_t	EnCode__CGRemoveUnionMemberRequest(void* pData);
		size_t	DeCode__CGRemoveUnionMemberRequest(void* pData);

		size_t	EnCode__GCUnionPosRightsNtf(void* pData);
		size_t	DeCode__GCUnionPosRightsNtf(void* pData);

		size_t	EnCode__CGModifyUnionPosRighstReq(void* pData);
		size_t	DeCode__CGModifyUnionPosRighstReq(void* pData);

		size_t	EnCode__GCModifyUnionPosRighstRst(void* pData);
		size_t	DeCode__GCModifyUnionPosRighstRst(void* pData);

		size_t	EnCode__CGAddUnionPosReq(void* pData);
		size_t	DeCode__CGAddUnionPosReq(void* pData);

		size_t	EnCode__GCAddUnionPosRst(void* pData);
		size_t	DeCode__GCAddUnionPosRst(void* pData);

		size_t	EnCode__CGRemoveUnionPosReq(void* pData);
		size_t	DeCode__CGRemoveUnionPosReq(void* pData);

		size_t	EnCode__GCRemoveUnionPosRst(void* pData);
		size_t	DeCode__GCRemoveUnionPosRst(void* pData);

		size_t	EnCode__CGTransformUnionCaptionReq(void* pData);
		size_t	DeCode__CGTransformUnionCaptionReq(void* pData);

		size_t	EnCode__GCTransformUnionCaptionRst(void* pData);
		size_t	DeCode__GCTransformUnionCaptionRst(void* pData);

		size_t	EnCode__GCTransformUnionCaptionNtf(void* pData);
		size_t	DeCode__GCTransformUnionCaptionNtf(void* pData);

		size_t	EnCode__CGModifyUnionMemberTitleReq(void* pData);
		size_t	DeCode__CGModifyUnionMemberTitleReq(void* pData);

		size_t	EnCode__GCModifyUnionMemberTitleRst(void* pData);
		size_t	DeCode__GCModifyUnionMemberTitleRst(void* pData);

		size_t	EnCode__GCUnionMemberTitleNtf(void* pData);
		size_t	DeCode__GCUnionMemberTitleNtf(void* pData);

		size_t	EnCode__CGAdvanceUnionMemberPosReq(void* pData);
		size_t	DeCode__CGAdvanceUnionMemberPosReq(void* pData);

		size_t	EnCode__GCAdvanceUnionMemberPosRst(void* pData);
		size_t	DeCode__GCAdvanceUnionMemberPosRst(void* pData);

		size_t	EnCode__GCAdvanceUnionMemberPosNtf(void* pData);
		size_t	DeCode__GCAdvanceUnionMemberPosNtf(void* pData);

		size_t	EnCode__CGReduceUnionMemberPosReq(void* pData);
		size_t	DeCode__CGReduceUnionMemberPosReq(void* pData);

		size_t	EnCode__GCReduceUnionMemberPosRst(void* pData);
		size_t	DeCode__GCReduceUnionMemberPosRst(void* pData);

		size_t	EnCode__GCReduceUnionMemberPosNtf(void* pData);
		size_t	DeCode__GCReduceUnionMemberPosNtf(void* pData);

		size_t	EnCode__CGUnionChannelSpeekReq(void* pData);
		size_t	DeCode__CGUnionChannelSpeekReq(void* pData);

		size_t	EnCode__GCUnionChannelSpeekRst(void* pData);
		size_t	DeCode__GCUnionChannelSpeekRst(void* pData);

		size_t	EnCode__GCUnionChannelSpeekNtf(void* pData);
		size_t	DeCode__GCUnionChannelSpeekNtf(void* pData);

		size_t	EnCode__CGUpdateUnionPosCfgReq(void* pData);
		size_t	DeCode__CGUpdateUnionPosCfgReq(void* pData);

		size_t	EnCode__GCUpdateUnionPosCfgRst(void* pData);
		size_t	DeCode__GCUpdateUnionPosCfgRst(void* pData);

		size_t	EnCode__GCUpdateUnionPosCfgNtf(void* pData);
		size_t	DeCode__GCUpdateUnionPosCfgNtf(void* pData);

		size_t	EnCode__CGForbidUnionSpeekReq(void* pData);
		size_t	DeCode__CGForbidUnionSpeekReq(void* pData);

		size_t	EnCode__GCForbidUnionSpeekRst(void* pData);
		size_t	DeCode__GCForbidUnionSpeekRst(void* pData);

		size_t	EnCode__GCForbidUnionSpeekNtf(void* pData);
		size_t	DeCode__GCForbidUnionSpeekNtf(void* pData);

		size_t	EnCode__GCUnionSkills(void* pData);
		size_t	DeCode__GCUnionSkills(void* pData);

		size_t	EnCode__CGIssueUnionTaskReq(void* pData);
		size_t	DeCode__CGIssueUnionTaskReq(void* pData);

		size_t	EnCode__GCIssueUnionTaskRst(void* pData);
		size_t	DeCode__GCIssueUnionTaskRst(void* pData);

		size_t	EnCode__GCIssueUnionTaskNtf(void* pData);
		size_t	DeCode__GCIssueUnionTaskNtf(void* pData);

		size_t	EnCode__GCDisplayUnionTasks(void* pData);
		size_t	DeCode__GCDisplayUnionTasks(void* pData);

		size_t	EnCode__CGLearnUnionSkillReq(void* pData);
		size_t	DeCode__CGLearnUnionSkillReq(void* pData);

		size_t	EnCode__CGAdvanceUnionLevelReq(void* pData);
		size_t	DeCode__CGAdvanceUnionLevelReq(void* pData);

		size_t	EnCode__GCAdvanceUnionLevelRst(void* pData);
		size_t	DeCode__GCAdvanceUnionLevelRst(void* pData);

		size_t	EnCode__GCUnionLevelNtf(void* pData);
		size_t	DeCode__GCUnionLevelNtf(void* pData);

		size_t	EnCode__CGPostUnionBulletinReq(void* pData);
		size_t	DeCode__CGPostUnionBulletinReq(void* pData);

		size_t	EnCode__GCPostUnionBulletinRst(void* pData);
		size_t	DeCode__GCPostUnionBulletinRst(void* pData);

		size_t	EnCode__GCUnionBulletinNtf(void* pData);
		size_t	DeCode__GCUnionBulletinNtf(void* pData);

		size_t	EnCode__GCUnionMemberOnOffLineNtf(void* pData);
		size_t	DeCode__GCUnionMemberOnOffLineNtf(void* pData);

		size_t	EnCode__CGLeaveWaitQueue(void* pData);
		size_t	DeCode__CGLeaveWaitQueue(void* pData);

		size_t	EnCode__CGUsePartionSkill(void* pData);
		size_t	DeCode__CGUsePartionSkill(void* pData);

		size_t	EnCode__GCLearnUnionSkillRst(void* pData);
		size_t	DeCode__GCLearnUnionSkillRst(void* pData);

		size_t	EnCode__GCLearnUnionSkillNtf(void* pData);
		size_t	DeCode__GCLearnUnionSkillNtf(void* pData);

		size_t	EnCode__GCChanageUnionActive(void* pData);
		size_t	DeCode__GCChanageUnionActive(void* pData);

		size_t	EnCode__GCUnionUISelectCmd(void* pData);
		size_t	DeCode__GCUnionUISelectCmd(void* pData);

		size_t	EnCode__LogEntryParam(void* pData);
		size_t	DeCode__LogEntryParam(void* pData);

		size_t	EnCode__UnionLogEntry(void* pData);
		size_t	DeCode__UnionLogEntry(void* pData);

		size_t	EnCode__CGQueryUnionLogReq(void* pData);
		size_t	DeCode__CGQueryUnionLogReq(void* pData);

		size_t	EnCode__GCQueryUnionLogRst(void* pData);
		size_t	DeCode__GCQueryUnionLogRst(void* pData);

		size_t	EnCode__GCWaitQueueResult(void* pData);
		size_t	DeCode__GCWaitQueueResult(void* pData);

		size_t	EnCode__GCUsePartionSkillRst(void* pData);
		size_t	DeCode__GCUsePartionSkillRst(void* pData);

		size_t	EnCode__CGChangePetName(void* pData);
		size_t	DeCode__CGChangePetName(void* pData);

		size_t	EnCode__CGClosePet(void* pData);
		size_t	DeCode__CGClosePet(void* pData);

		size_t	EnCode__CGChangePetSkillState(void* pData);
		size_t	DeCode__CGChangePetSkillState(void* pData);

		size_t	EnCode__CGChangePetImage(void* pData);
		size_t	DeCode__CGChangePetImage(void* pData);

		size_t	EnCode__CGSummonPet(void* pData);
		size_t	DeCode__CGSummonPet(void* pData);

		size_t	EnCode__GCPetinfoInstable(void* pData);
		size_t	DeCode__GCPetinfoInstable(void* pData);

		size_t	EnCode__GCPetAbility(void* pData);
		size_t	DeCode__GCPetAbility(void* pData);

		size_t	EnCode__GCPetinfoStable(void* pData);
		size_t	DeCode__GCPetinfoStable(void* pData);

		size_t	EnCode__RankChartItem(void* pData);
		size_t	DeCode__RankChartItem(void* pData);

		size_t	EnCode__CGQueryRankInfoByPageIndex(void* pData);
		size_t	DeCode__CGQueryRankInfoByPageIndex(void* pData);

		size_t	EnCode__CGQueryRankInfoByPlayerName(void* pData);
		size_t	DeCode__CGQueryRankInfoByPlayerName(void* pData);

		size_t	EnCode__GCRankInfoByPageRst(void* pData);
		size_t	DeCode__GCRankInfoByPageRst(void* pData);

		size_t	EnCode__GCQueryRankPlayerByNameRst(void* pData);
		size_t	DeCode__GCQueryRankPlayerByNameRst(void* pData);

		size_t	EnCode__CGSendMailToPlayer(void* pData);
		size_t	DeCode__CGSendMailToPlayer(void* pData);

		size_t	EnCode__CGQueryMailListByPage(void* pData);
		size_t	DeCode__CGQueryMailListByPage(void* pData);

		size_t	EnCode__CGDeleteMail(void* pData);
		size_t	DeCode__CGDeleteMail(void* pData);

		size_t	EnCode__CGQueryMailDetailInfo(void* pData);
		size_t	DeCode__CGQueryMailDetailInfo(void* pData);

		size_t	EnCode__CGPickMailAttachItem(void* pData);
		size_t	DeCode__CGPickMailAttachItem(void* pData);

		size_t	EnCode__CGPickMailAttachMoney(void* pData);
		size_t	DeCode__CGPickMailAttachMoney(void* pData);

		size_t	EnCode__GCQueryMailListByPageRst(void* pData);
		size_t	DeCode__GCQueryMailListByPageRst(void* pData);

		size_t	EnCode__GCQueryMailDetialInfoRst(void* pData);
		size_t	DeCode__GCQueryMailDetialInfoRst(void* pData);

		size_t	EnCode__GCQueryMailAttachInfoRst(void* pData);
		size_t	DeCode__GCQueryMailAttachInfoRst(void* pData);

		size_t	EnCode__GCSendMailError(void* pData);
		size_t	DeCode__GCSendMailError(void* pData);

		size_t	EnCode__GCDeleteMailRst(void* pData);
		size_t	DeCode__GCDeleteMailRst(void* pData);

		size_t	EnCode__GCPickMailAttachItemRst(void* pData);
		size_t	DeCode__GCPickMailAttachItemRst(void* pData);

		size_t	EnCode__GCPickMailAttachMoneyRst(void* pData);
		size_t	DeCode__GCPickMailAttachMoneyRst(void* pData);

		size_t	EnCode__GCPlayerHaveUnReadMail(void* pData);
		size_t	DeCode__GCPlayerHaveUnReadMail(void* pData);

		size_t	EnCode__GCRemoveUnionMemberResult(void* pData);
		size_t	DeCode__GCRemoveUnionMemberResult(void* pData);

		size_t	EnCode__GCRemoveUnionMemberNotify(void* pData);
		size_t	DeCode__GCRemoveUnionMemberNotify(void* pData);

		size_t	EnCode__GCPunishPlayerInfo(void* pData);
		size_t	DeCode__GCPunishPlayerInfo(void* pData);

		size_t	EnCode__CGQueryAccountInfo(void* pData);
		size_t	DeCode__CGQueryAccountInfo(void* pData);

		size_t	EnCode__GCAccountInfo(void* pData);
		size_t	DeCode__GCAccountInfo(void* pData);

		size_t	EnCode__VipItem(void* pData);
		size_t	DeCode__VipItem(void* pData);

		size_t	EnCode__CGPurchaseItem(void* pData);
		size_t	DeCode__CGPurchaseItem(void* pData);

		size_t	EnCode__GCPurchaseItem(void* pData);
		size_t	DeCode__GCPurchaseItem(void* pData);

		size_t	EnCode__CGQueryVipInfo(void* pData);
		size_t	DeCode__CGQueryVipInfo(void* pData);

		size_t	EnCode__GCVipInfo(void* pData);
		size_t	DeCode__GCVipInfo(void* pData);

		size_t	EnCode__CGPurchaseVip(void* pData);
		size_t	DeCode__CGPurchaseVip(void* pData);

		size_t	EnCode__GCPurchaseVip(void* pData);
		size_t	DeCode__GCPurchaseVip(void* pData);

		size_t	EnCode__CGFetchVipItem(void* pData);
		size_t	DeCode__CGFetchVipItem(void* pData);

		size_t	EnCode__GCFetchVipItem(void* pData);
		size_t	DeCode__GCFetchVipItem(void* pData);

		size_t	EnCode__CGPurchaseGift(void* pData);
		size_t	DeCode__CGPurchaseGift(void* pData);

		size_t	EnCode__CGPetMove(void* pData);
		size_t	DeCode__CGPetMove(void* pData);

		size_t	EnCode__GCPurchaseGift(void* pData);
		size_t	DeCode__GCPurchaseGift(void* pData);

		size_t	EnCode__TransItem(void* pData);
		size_t	DeCode__TransItem(void* pData);

		size_t	EnCode__GCPetMove(void* pData);
		size_t	DeCode__GCPetMove(void* pData);

		size_t	EnCode__CGFetchNonVipItem(void* pData);
		size_t	DeCode__CGFetchNonVipItem(void* pData);

		size_t	EnCode__GCFetchNonVipItem(void* pData);
		size_t	DeCode__GCFetchNonVipItem(void* pData);

	protected:
		std::map<int, EnCodeFunc>     m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>     m_mapDeCodeFunc;
		ParserTool                    m_kPackage;
	};
}
#endif			//__CliProtocol__
