﻿/**
* @file CliProtocol.h
* @brief 定义客户端有关通信协议
* Copyright(c) 2007,上海第九城市游戏研发部
* All rights reserved
* 文件名称: CliProtocol.h
* 摘    要: 定义客户端有关通信协议（类型，命令字，协议结构）
* 作    者: dzj
* 完成日期: 2007.11.29
*
*/

#pragma once

#pragma pack(push)
#pragma pack(1)

/**
*   第一部分：
*   以下为参与通信各方的类别标识号，程序自身的标识号应在本文件之外的某处定义好
*   例如：客户端程序应该这样定义，const int SELF_PROTO_TYPE = 0x00;
*	参与者类别	类别编号	简写
*	Client			0		C
*	Gatesrv			1		G
*	Assignsrv		2		A
*/
#define C_A  	0x02	//客户端发往assignsrv的消息；
#define A_C  	0x20	//assign发往客户端的消息；
#define C_G  	0x01	//客户端发往gatesrv的消息；
#define G_C  	0x10	//gatesrv发往客户端的消息；

/**
*   第二部分：
*   以下为通信协议命令字
*   制订原则是按功能划分可用命令字段，以便能较容易地由命令字的值识别该命令的大致功能
*   实际编码时只使用对应命令字的宏而不使用16进制数值
*   这部分定义应按参与者类别的不同而分块，以便编码时能迅速查找到相应的命令字，
*   同时命令字宏字母全为大定，命令字宏的头两个字母指出该消息的参与者，
*   其余部分为该命令字意义的简写，参与者与消息意义之间以下划线分隔
*/
//////////////////////////////////////////////////////////////////////////
///客户端发往assignsrv的消息命令字,形式0x0XXX...
///...客户端发往assignsrv的消息命令字
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
///assignsrv发往客户端的消息命令字,形式0x1XXX...
///...assignsrv发往客户端的消息命令字
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
///客户端发往gatesrv的消息命令字,形式0x2XXX...
#define C_G_STEP 0x0001  //客户端发往服务器的走动请求消息
#define C_G_RUN  0x0002  //客户端发往服务器的跑动请求消息
#define C_G_LOGIN 0x0003 //客户端发往服务器登录消息;
///...客户端发往gatesrv的消息命令字
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
///gatesrv发往客户端的消息命令字,形式0x3XXX...
#define G_C_STEP 0x1001  //服务器发往客户端的走动消息
#define G_C_RUN  0x1002  //服务器发往客户端的跑动消息
#define G_C_PLAYER_POS_INFO 0x1007 //服务器发往客户端的玩家所在位置信息
#define G_C_PLAYER_SELF_INFO 0x1008 //服务器发往客户端通知玩家信息(目前只有玩家ID号)
#define G_C_PLAYER_APPEAR 0x1009 //服务器发往客户端：(除自身之外别的)玩家在地图上出现
///...gatesrv发往客户端的消息命令字
//////////////////////////////////////////////////////////////////////////

/**
*   第三部分：
*   以下为通信消息结构
*   需要在每个结构之前注明该结构对应消息的命令字，同时，对于结构的每个字段，都分别注明该字段的物理意义
*   同时，对于结构的每个字段，都分别注明该字段的物理意义
*/
//////////////////////////////////////////////////////////////////////////
///客户端发往assignsrv的消息结构...
///...客户端发往assignsrv的消息结构
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
///assignsrv发往客户端的消息结构...
///...assignsrv发往客户端的消息结构
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
///客户端发往gatesrv的消息结构...
//#define C_G_STEP 0x0001  //客户端发往服务器的走动请求消息
typedef struct StrCGStep
{
	static const unsigned char  byPkgType = C_G;
	static const unsigned short wCmd = C_G_STEP;
	unsigned long lPlayerID;//玩家ID号；
	unsigned long lCurOffset;//客户端时刻偏移值；
	unsigned short nOrgX;//移动出发点X坐标；
	unsigned short nOrgY;//移动出发点Y坐标；
	float fOrgCroodX;//移动出发点X客户端坐标；
	float fOrgCroodY;//移动出发点Y客户端坐标；
	unsigned short nNewX;//移动目标点X坐标；
	unsigned short nNewY;//移动目标点Y坐标；
	float fNewCroodX;//移动目标点X客户端坐标；
	float fNewCroodY;//移动目标点Y客户端坐标；
}CGStep; //客户端发往服务器的走动请求消息

//#define C_G_RUN  0x0002  //客户端发往服务器的跑动请求消息
typedef struct StrCGRun
{
	static const unsigned char  byPkgType = C_G;
	static const unsigned short wCmd = C_G_RUN;
	unsigned long lPlayerID;//玩家ID号；
	unsigned long lCurOffset;//客户端时刻偏移值；
	unsigned short nOrgX;//移动出发点X坐标；
	unsigned short nOrgY;//移动出发点Y坐标；
	float fOrgCroodX;//移动出发点X客户端坐标；
	float fOrgCroodY;//移动出发点Y客户端坐标；
	unsigned short nNewX;//移动目标点X坐标；
	unsigned short nNewY;//移动目标点Y坐标；
	float fNewCroodX;//移动目标点X客户端坐标；
	float fNewCroodY;//移动目标点Y客户端坐标；
}CGRun; //客户端发往服务器的跑动请求消息

//#define C_G_LOGIN 0x0003 //客户端发往服务器登录消息;
typedef struct StrCGLogin
{
	static const unsigned char  byPkgType = C_G;
	static const unsigned short wCmd = C_G_LOGIN;
	char strUserAccount[32];
	char strUserPassword[32];
}CGLogin; //客户端发往服务器的跑动请求消息
///...客户端发往gatesrv的消息结构
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
///gatesrv发往客户端的消息结构...
//#define G_C_STEP 0x1001  //服务器发往客户端的走动消息
typedef struct StrGCStep
{
    static const unsigned char  byPkgType = G_C;
	static const unsigned short wCmd = G_C_STEP;
	unsigned long lPlayerID;//玩家ID号；
	unsigned long lCurOffset;//客户端时刻偏移值；
	unsigned short nOrgX;//移动出发点X坐标；
	unsigned short nOrgY;//移动出发点Y坐标；
	float fOrgCroodX;//移动出发点X客户端坐标；
	float fOrgCroodY;//移动出发点Y客户端坐标；
	unsigned short nNewX;//移动目标点X坐标；
	unsigned short nNewY;//移动目标点Y坐标；
	float fNewCroodX;//移动目标点X客户端坐标；
	float fNewCroodY;//移动目标点Y客户端坐标；
}GCStep; //服务器发给客户端的走动消息

//#define G_C_RUN  0x1002  //服务器发往客户端的跑动消息
typedef struct StrGCRun
{
	static const unsigned char  byPkgType = G_C;
	static const unsigned short wCmd = G_C_RUN;
	unsigned long lPlayerID;//玩家ID号；
	unsigned long lCurOffset;//客户端时刻偏移值；
	unsigned short nOrgX;//移动出发点X坐标；
	unsigned short nOrgY;//移动出发点Y坐标；
	float fOrgCroodX;//移动出发点X客户端坐标；
	float fOrgCroodY;//移动出发点Y客户端坐标；
	unsigned short nNewX;//移动目标点X坐标；
	unsigned short nNewY;//移动目标点Y坐标；
	float fNewCroodX;//移动目标点X客户端坐标；
	float fNewCroodY;//移动目标点Y客户端坐标；
}GCRun; //服务器发给客户端的跑动消息

//#define G_C_PLAYER_POS_INFO 0x1007 //服务器发往客户端的玩家所在位置信息
typedef struct StrGCPlayerPosInfo
{
	static const unsigned char  byPkgType = G_C;
	static const unsigned short wCmd = G_C_PLAYER_POS_INFO;
	unsigned long lPlayerID;//玩家ID号；
	unsigned long lMapCode;//所在地图代号；
	unsigned short nPosX;//所在点X坐标；
	unsigned short nPosY;//所在点Y坐标；
	float fPosCroodX;//所在点X客户端坐标；
	float fPosCroodY;//所在点Y客户端坐标；
}GCPlayerPosInfo; //服务器发给客户端的跑动消息

//#define G_C_PLAYER_SELF_INFO 0x1008 //服务器发往客户端通知玩家自身信息(目前只有玩家ID号)
typedef struct StrGCPlayerSelfInfo
{
	static const unsigned char  byPkgType = G_C;
	static const unsigned short wCmd = G_C_PLAYER_SELF_INFO;
	unsigned long lPlayerID;//玩家自身ID号；
}GCPlayerSelfInfo; //服务器发往客户端通知玩家自身信息(目前只有玩家ID号)

//#define G_C_PLAYER_APPEAR 0x1009 //服务器发往客户端：(除自身之外别的)玩家在地图上出现
typedef struct StrGCPlayerAppear
{
	static const unsigned char  byPkgType = G_C;
	static const unsigned short wCmd = G_C_PLAYER_APPEAR;
	unsigned long lPlayerID;//玩家ID号；
	unsigned short nPosX;//所在点X坐标；
	unsigned short nPosY;//所在点Y坐标；
	float fPosCroodX;//所在点X客户端坐标；
	float fPosCroodY;//所在点Y客户端坐标；
}GCPlayerAppear; //服务器发往客户端：(除自身之外别的)玩家在地图上出现
///...gatesrv发往客户端的消息结构
//////////////////////////////////////////////////////////////////////////
#pragma pack(pop)
