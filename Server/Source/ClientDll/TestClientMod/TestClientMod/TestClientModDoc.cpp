﻿// TestClientModDoc.cpp : CTestClientModDoc 类的实现
//

#include "stdafx.h"
#include "TestClientMod.h"

#include "TestClientModDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestClientModDoc

IMPLEMENT_DYNCREATE(CTestClientModDoc, CDocument)

BEGIN_MESSAGE_MAP(CTestClientModDoc, CDocument)
END_MESSAGE_MAP()


// CTestClientModDoc 构造/析构

CTestClientModDoc::CTestClientModDoc()
{
	// TODO: 在此添加一次性构造代码

}

CTestClientModDoc::~CTestClientModDoc()
{
}

BOOL CTestClientModDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 在此添加重新初始化代码
	// (SDI 文档将重用该文档)

	return TRUE;
}




// CTestClientModDoc 序列化

void CTestClientModDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 在此添加存储代码
	}
	else
	{
		// TODO: 在此添加加载代码
	}
}


// CTestClientModDoc 诊断

#ifdef _DEBUG
void CTestClientModDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTestClientModDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CTestClientModDoc 命令
