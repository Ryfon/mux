﻿// TestClientModView.cpp : CTestClientModView 类的实现
//

#include "stdafx.h"
#include "TestClientMod.h"

#include "TestClientModDoc.h"
#include "TestClientModView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CTestClientModView

PlayerID g_SelfID;

IMPLEMENT_DYNCREATE(CTestClientModView, CView)

BEGIN_MESSAGE_MAP(CTestClientModView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_COMMAND(ID_NETMODTEST_CONNECT, &CTestClientModView::OnNetmodtestConnect)
	ON_COMMAND(ID_NETMODTEST_SENDDATA, &CTestClientModView::OnNetmodtestSenddata)
	ON_COMMAND(ID_NETMODTEST_CANCEL, &CTestClientModView::OnNetmodtestCancel)
END_MESSAGE_MAP()

#ifdef USING_MUXCLI_MOD
#define SendPkg( PKG_TYPE, PKG_CONTENT ) \
{\
	char* pPkg = NULL;\
	int nPkgLen = 0;\
	if ( CPacketBuild::CreatePkg<PKG_TYPE>( &PKG_CONTENT, &pPkg, nPkgLen ) )/*//组包之后必须立即调用发包函数，在tmpRunMsg释放之前；*/\
    {\
	  int rst = m_pNetMod->SendPkgReq( 0/*包编号，目前暂时不用*/, pPkg/*发送缓冲区*/, nPkgLen/*发送数据长度*/ );\
	  if ( MUX_CLI_SUCESS != rst )\
      {\
	    CString tmpstr;\
	    tmpstr.Format( TEXT("无法发包, in OnNetmodtestSenddata，错误%d"), rst );\
	    ::MessageBox( m_hWnd, tmpstr, TEXT("错误"), NULL );\
      }\
    }\
}
#endif //USING_MUXCLI_MOD

///在此函数中返回登录结果；
int CMuxCliNetSinker::OnLoginRes( int errNo ) 
{ 
	if ( GCPlayerLogin::ISLOGINOK_SUCCESS == errNo )
	{
		m_pView->OnLogin( true );
		::MessageBox( m_hWnd, TEXT("登录成功"), TEXT("error"), NULL );
	} else {
		m_pView->OnLogin( false );
		::MessageBox( m_hWnd, TEXT("登录失败"), TEXT("error"), NULL );
	}	

	return 0; 
};

///出错时回调；
int CMuxCliNetSinker::OnError( const int& errNo, const DWORD dwPkgID ) 
{
	if ( MUX_CLI_CONNECT_ERR == errNo )
	{
		::MessageBox( m_hWnd, TEXT("连接请求失败"), TEXT("error"), NULL );
	} else {
		m_pView->StopNetTimer();
		CString tmpstr;
		tmpstr.Format( TEXT("收到网络模块错误通知:%d"), errNo );
		::MessageBox( m_hWnd, tmpstr, TEXT("错误"), NULL );
	}	
	return 0; 
};

	///收到服务器发来的消息；
int CMuxCliNetSinker::OnPkgRcved( unsigned short wCmd, const char* pPkg, const DWORD dwLen ) 
{ 
	static DWORD dwPkgCount = 0;//收包计数；
	dwPkgCount++;
	//此处加收包处理；
	if ( wCmd == G_C_RUN )
	{
		if ( dwLen != sizeof(GCRun) )
		{
			//包大小不正确；
			return 0;
		}
		GCRun* gcRun = (GCRun*) pPkg;
		CString tmpstr;
		tmpstr.Format( TEXT("收到服务器第%d个消息，服务器跑动包:\n玩家:%x"), dwPkgCount, gcRun->lPlayerID.dwPID );
		m_pView->ShowMessage( tmpstr );
		//::MessageBox( m_hWnd, tmpstr, TEXT("收到包"), NULL );
	} else if ( wCmd == G_C_PLAYER_SELF_INFO ) {
		if ( dwLen != sizeof(GCPlayerSelfInfo) )
		{
			//包大小不正确；
			return 0;
		}
		GCPlayerSelfInfo* gcselfInfo = (GCPlayerSelfInfo*) pPkg;
		g_SelfID = gcselfInfo->lPlayerID;
		CString tmpstr;
		tmpstr.Format( TEXT("收到服务器第%d个消息，玩家自身消息:\n玩家:%x"), dwPkgCount, gcselfInfo->lPlayerID.dwPID );
		m_pView->ShowMessage( tmpstr );
	} else {
		CString tmpstr;
		tmpstr.Format( TEXT("收到服务器第%d个消息，命令字:%x"), dwPkgCount, wCmd );
		m_pView->ShowMessage( tmpstr );
		//::MessageBox( m_hWnd, tmpstr, TEXT("收到包"), NULL );
	}
	return 0; 
};

// CTestClientModView 构造/析构

CTestClientModView::CTestClientModView()
{
	// TODO: 在此处添加构造代码
	m_cstrMsg.Format( TEXT("") );
	m_CurTime.Format( TEXT("") );
}

CTestClientModView::~CTestClientModView()
{
}

#ifdef USING_MUXCLI_MOD
///取网络模块函数；
BOOL CTestClientModView::GetCreateCliNetModFun()
{
	if ( NULL != m_hComDll )
	{
		FreeLibrary( m_hComDll );
	}
	m_hComDll  = LoadLibrary( TEXT("MuxCliMod.dll") );
	if( m_hComDll == NULL)
	{
		return FALSE;
	}

	m_pfnGetMuxCliNetMod = (PFN_GetMuxCliNetMod)GetProcAddress( m_hComDll, "GetMuxCliNetMod");
	if ( NULL == m_pfnGetMuxCliNetMod )
	{
		return FALSE;
	}

	m_pfnGetTick = (PFN_DllGetTick)GetProcAddress( m_hComDll, "DllGetTick");
	if ( NULL == m_pfnGetTick )
	{
		return FALSE;
	}

	m_pfnNiGetCurSec = (PFN_DllNiGetCurSec)GetProcAddress( m_hComDll, "DllNiGetCurSec");
	if ( NULL == m_pfnNiGetCurSec )
	{
		return FALSE;
	}


	return TRUE;
}
#endif //USING_MUXCLI_MOD

DWORD testA()
{
	return GetTickCount();
}

BOOL CTestClientModView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式	
#ifdef USING_MUXCLI_MOD
	m_hComDll = NULL;
	m_pfnGetMuxCliNetMod = NULL;
	m_pNetMod = NULL;
	if ( !GetCreateCliNetModFun() )//得到取网络模块函数；
	{
		return FALSE;
	}

	if ( NULL != m_pfnGetMuxCliNetMod )
	{
		//取网络模块，并将网络事件回调者与其关联；
		int isok = (*m_pfnGetMuxCliNetMod)( &m_pNetMod );
		if ( MUX_CLI_SUCESS != isok )
		{
			m_pNetMod = NULL;
			return FALSE;
		}
		if ( NULL == m_pNetMod )
		{
			return FALSE;
		}
	} else {
		return FALSE;
	}
#endif //USING_MUXCLI_MOD

	return CView::PreCreateWindow(cs);
}

// CTestClientModView 绘制

void CTestClientModView::OnDraw(CDC* pDC)
{
	CTestClientModDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	RECT tmpRect;
	tmpRect.left = 10;
	tmpRect.top = 10;
	tmpRect.right = 300;
	tmpRect.bottom = 300;	
	RECT tmpRect2;
	tmpRect2.left = 10;
	tmpRect2.top = 50;
	tmpRect2.right = 500;
	tmpRect2.bottom = 360;	
	pDC->DrawText( m_cstrMsg, m_cstrMsg.GetLength(), &tmpRect, DT_CENTER );
	pDC->DrawText( m_CurTime, m_CurTime.GetLength(), &tmpRect2, DT_CENTER );

	// TODO: 在此处为本机数据添加绘制代码
}

///显示消息；
void CTestClientModView::ShowMessage( CString pMsg )
{
	m_cstrMsg.Format( pMsg );
	//RECT tmpRect;
	//tmpRect.left = 10;
	//tmpRect.top = 10;
	//tmpRect.right = 200;
	//tmpRect.bottom = 30;		
	//this->InvalidateRect( &tmpRect );
	this->Invalidate();
	return;
}



// CTestClientModView 打印

BOOL CTestClientModView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestClientModView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestClientModView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清除过程
}


// CTestClientModView 诊断

#ifdef _DEBUG
void CTestClientModView::AssertValid() const
{
	CView::AssertValid();
}

void CTestClientModView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestClientModDoc* CTestClientModView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestClientModDoc)));
	return (CTestClientModDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestClientModView 消息处理程序

void CTestClientModView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch ( nIDEvent )
	{
	case 1://网络事件处理时钟；
#ifdef USING_MUXCLI_MOD
		if ( NULL != m_pNetMod )
		{
			m_pNetMod->ProcessNetEvent();
			m_CurTime.Format( TEXT("app tick:%d   dll tick:%d  dll nisec:%f")
				, GetTickCount()/1000, m_pfnGetTick()/1000, m_pfnNiGetCurSec() /*m_pNetMod->DllGetTick()/1000*/ );
			RedrawWindow();
		}
#endif //USING_MUXCLI_MOD
		break;
	default:
		{
			;
		}

	}

	CView::OnTimer(nIDEvent);
}

void CTestClientModView::OnDestroy()
{
	CView::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
#ifdef USING_MUXCLI_MOD
	if ( NULL != m_pNetMod )
	{
		//不再使用时调用ReleaseMuxCliNetMod释放模块用到的资源；
		m_pNetMod->ReleaseMuxCliNetMod();
	}
	if ( NULL != m_hComDll )
	{
		BOOL isok = FreeLibrary( m_hComDll );
	}
#endif //USING_MUXCLI_MOD

}

void CTestClientModView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	// TODO: 在此添加专用代码和/或调用基类
}

void CTestClientModView::OnNetmodtestConnect()
{
#ifdef USING_MUXCLI_MOD
	//初始化网络模块代码，根据实际需要决定何时执行；
	m_CliNetSinker.SetWnd( this->GetSafeHwnd(), this );
	if ( NULL == m_pNetMod )
	{
		return;
	}
	int isok = m_pNetMod->InitMuxCliNetMod( &m_CliNetSinker, "172.18.8.30", 8800 );
	if ( MUX_CLI_SUCESS != isok )
	{
		if ( MUX_CLI_INPROCESS == isok )
		{
			MessageBox( TEXT("重复初始化网络模块失败, in OnNetmodtestConnect") );
		} else if ( MUX_CLI_REOPERA == isok ) {
			MessageBox( TEXT("重复操作, in OnNetmodtestConnect") );
		} else {
			MessageBox( TEXT("初始化网络模块失败, in OnNetmodtestConnect") );
		}
		return;
	}

	//置时钟，时钟内执行ProcessNetEvent()，建议每帧执行一次m_pNetMod->ProcessNetEvent()，以及时处理收发包；
	SetTimer( 1, 100, NULL );

	m_pNetMod->GetSrvList( 0 );
	m_pNetMod->SelectSrv( 0 );

	//发登录请求，登录结果或错误在m_CliNetSinker中回调；
	m_pNetMod->LoginReq( "dzj2", "1111" );
#endif //USING_MUXCLI_MOD

	return;
}

void CTestClientModView::OnLogin( bool isLoginOk )
{
	if ( isLoginOk )
	{
		m_pNetMod->SelectRole( 0 );
		m_pNetMod->EnterWorld();
	}
}

void CTestClientModView::OnNetmodtestSenddata()
{
#ifdef USING_MUXCLI_MOD
	//char testData[] = "test, just for test";
	if ( NULL == m_pNetMod )
	{
		return;
	}
	CGRun tmpRunMsg;

	//DWORD passedmsec = m_pNetMod->GetConnPassedMsec();

	//tmpRunMsg.lPlayerID.wGID = g_SelfID.wGID;
	//tmpRunMsg.lPlayerID.dwPID = g_SelfID.dwPID;
	//tmpRunMsg.nOrgMapPosX = 1;
	//tmpRunMsg.nOrgMapPosY = 10;
	//tmpRunMsg.fOrgWorldPosX = 0;
	//tmpRunMsg.fOrgWorldPosY = 0;
	//tmpRunMsg.nNewMapPosX = 0;
	//tmpRunMsg.nNewMapPosY = 9;
	//tmpRunMsg.fNewWorldPosX = 0;
	//tmpRunMsg.fNewWorldPosY = 0;
	SendPkg( CGRun, tmpRunMsg );
	//char* pPkg = NULL;
	//int nPkgLen = 0;
	//if ( CPacketBuild::CreatePkg<CGRun>( &tmpRunMsg, &pPkg, nPkgLen ) )//组包之后必须立即调用发包函数，在tmpRunMsg释放之前；
	//{
	//	int rst = m_pNetMod->SendPkgReq( 0/*包编号，目前暂时不用*/, pPkg/*发送缓冲区*/, nPkgLen/*发送数据长度*/ );
	//	if ( MUX_CLI_SUCESS != rst )
	//	{
	//		CString tmpstr;
	//		tmpstr.Format( TEXT("无法发包, in OnNetmodtestSenddata，错误%d"), rst );
	//		::MessageBox( m_hWnd, tmpstr, TEXT("错误"), NULL );
	//	}
	//}
#endif //USING_MUXCLI_MOD
	return;
}



void CTestClientModView::OnNetmodtestCancel()
{
#ifdef USING_MUXCLI_MOD
	//char testData[] = "test, just for test";
	if ( NULL == m_pNetMod )
	{
		return;
	}
	m_pNetMod->UninitMuxCliNetMod();
#endif //USING_MUXCLI_MOD
}
