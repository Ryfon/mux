﻿// TestClientModView.h : CTestClientModView 类的接口
//


#pragma once

#ifdef USING_MUXCLI_MOD

#include "..\..\LocalTest\clinet.h"
#include "PkgProc/PacketBuild.h"

//使用模块时，应指定此名字空间；
using namespace MUX_CLIMOD;
using namespace MUX_PROTO;

extern PlayerID g_SelfID;

class CTestClientModView;
///处理网络事件者，需要自IMuxCliNetSinker继承，并定义相关处理函数；
class CMuxCliNetSinker : public IMuxCliNetSinker
{
public:		
	///在此函数中返回登录结果；
	virtual int OnLoginRes( int errNo );

	///返回之前的测试帐号结果；
	virtual int OnTestAccountsRes( int errNo ) { return 0; };

	///返回之前创建帐号请求的结果；
	virtual int OnCreateAccountsRes( int errNo ) { return 0; };

	///收到服务器发来的消息；
	virtual int OnPkgRcved( unsigned short wCmd, const char* pPkg, const DWORD dwLen );

	///出错时回调；
	virtual int OnError( const int& errNo, const DWORD dwPkgID );

public:
	void SetWnd( HWND hWnd, CTestClientModView* pView )
	{
		m_pView = pView;
		m_hWnd = hWnd;
	}
private:
	HWND m_hWnd;
	CTestClientModView* m_pView;
};

#endif //USING_MUXCLI_MOD


class CTestClientModView : public CView
{
protected: // 仅从序列化创建
	CTestClientModView();
	DECLARE_DYNCREATE(CTestClientModView)

// 属性
public:
	CTestClientModDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestClientModView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

#ifdef USING_MUXCLI_MOD
	///取网络模块函数；
	BOOL GetCreateCliNetModFun();
#endif //USING_MUXCLI_MOD

	///显示消息；
	void ShowMessage( CString pMsg );

	///停止网络消息处理；
	void StopNetTimer()
	{
		KillTimer( 1 );
	}

protected:

	CString m_cstrMsg;
	CString m_CurTime;


#ifdef USING_MUXCLI_MOD
	///取模块函数指针；
    PFN_GetMuxCliNetMod m_pfnGetMuxCliNetMod;
	PFN_DllGetTick      m_pfnGetTick;
	PFN_DllNiGetCurSec  m_pfnNiGetCurSec;
	///网络模块；
	IMuxCliNetMod* m_pNetMod;
	///网络事件处理者；
	CMuxCliNetSinker m_CliNetSinker;
	///通信库句柄；
	HINSTANCE m_hComDll;
#endif //USING_MUXCLI_MOD

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
	afx_msg void OnDestroy();
public:
	virtual void OnInitialUpdate();
public:
	afx_msg void OnNetmodtestConnect();
public:
	afx_msg void OnNetmodtestSenddata();
public:
	void OnLogin( bool isLoginOk );
	afx_msg void OnNetmodtestCancel();
};

#ifndef _DEBUG  // TestClientModView.cpp 中的调试版本
inline CTestClientModDoc* CTestClientModView::GetDocument() const
   { return reinterpret_cast<CTestClientModDoc*>(m_pDocument); }
#endif

