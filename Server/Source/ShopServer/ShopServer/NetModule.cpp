﻿#include "Config.h"
#include "NetModule.h"

CNetModule::CNetModule(void)
{
	
}

CNetModule::~CNetModule(void)
{

}

int CNetModule::init(int argc, ACE_TCHAR *argv)
{
	ACE_UNUSED_ARG(argc);
	ACE_UNUSED_ARG(argv);
	//在给定的端口上进行侦听
	char szBuf[128];
	ACE_OS::sprintf(szBuf, "%s:%d", CConfigInstance::instance()->m_ServerIp.c_str(), CConfigInstance::instance()->m_ServerPort);
	ACE_INET_Addr ServerAddr(szBuf);
	if (m_Acceptor.open(ServerAddr, ACE_Reactor::instance()) == 1)
	{
		ACE_ERROR_RETURN((LM_ERROR, "%p\n", "cannot register acceptor"), -1);
	}
	ACE_DEBUG((LM_INFO, ACE_TEXT("start 侦听:")));
	ACE_DEBUG((LM_INFO, ACE_TEXT(szBuf)));
	ACE_DEBUG((LM_INFO, ACE_TEXT("\n")));

	return 0;
}

int CNetModule::fini()
{
	m_Acceptor.close();
	return 0;
}

int CNetModule::info(ACE_TCHAR **info_string, size_t length)
{
	ACE_UNUSED_ARG(info_string);
	ACE_UNUSED_ARG(length);
	return 0;
}
