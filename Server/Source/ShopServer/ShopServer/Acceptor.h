﻿/** @file Acceptor.h 
@brief tcp连接工厂，创建到来的连接请求
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-03-26
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef ACCEPTOR_H
#define ACCEPTOR_H
#include "Common.h"
#include "SessionHandler.h"

class CAcceptor: public ACE_Acceptor<CSessionHandler, ACE_SOCK_ACCEPTOR>
{
public:
	typedef ACE_Acceptor<CSessionHandler, ACE_SOCK_ACCEPTOR> supper;
	CAcceptor(void);
	virtual ~CAcceptor(void);
public:
	virtual int make_svc_handler(CSessionHandler *&sh);
	virtual int accept_svc_handler(CSessionHandler *svc_handler);
};

#endif
