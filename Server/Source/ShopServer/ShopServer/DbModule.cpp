﻿#include "DbModule.h"
#include "Config.h"
#include "XmlManager.h"

CDbModule::CDbModule(void)
: m_DbConn()
{
	ACE_OS::memset(&m_DbConnString, 0, sizeof(_stConnectionString));
	m_DbConn = new CConnection;
}

CDbModule::~CDbModule(void)
{
	if (m_DbConn)
	{
		delete m_DbConn;
		m_DbConn = 0;
	}
}

int CDbModule::info(ACE_TCHAR **info_string, size_t length)
{
	ACE_UNUSED_ARG(info_string);
	ACE_UNUSED_ARG(length);
	return 0;
}

int CDbModule::fini()
{
	m_DbConn->Close();
	return 0;
}

int CDbModule::init(int argc, ACE_TCHAR *argv)
{
	ACE_UNUSED_ARG(argc);
	ACE_UNUSED_ARG(argv);
	//从全局配置对象中获得DB配置信息
	ACE_OS::strcpy(m_DbConnString.szHost, CConfigInstance::instance()->m_DbServerIp.c_str());
	ACE_OS::strcpy(m_DbConnString.szDb, CConfigInstance::instance()->m_DbName.c_str());
	ACE_OS::strcpy(m_DbConnString.szUser, CConfigInstance::instance()->m_DbUser.c_str());
	ACE_OS::strcpy(m_DbConnString.szPassword, CConfigInstance::instance()->m_DbPwd.c_str());
	m_DbConnString.nPort = CConfigInstance::instance()->m_DbPort;
	if (m_DbConn->Open(&m_DbConnString) != 0)
	{
		ACE_ERROR_RETURN((LM_ERROR, "%p\n", "open connection to db failed."), -1);
	}
	//读取本地配置文件并且导入到数据库中
	LoadLocalData();
	return 0;
}

int CDbModule::open(void *args)
{
	ACE_UNUSED_ARG(args);
	m_TaskQue.queue()->high_water_mark(MAX_QUE_SIZE);
	m_TaskQue.queue()->low_water_mark(MAX_QUE_SIZE);

	//this->activate(THR_NEW_LWP | THR_JOINABLE | THR_INHERIT_SCHED, MAX_DB_THREAD_NUM);
	return 0;
}

int CDbModule::svc()
{
	//创建数据库连接
	while (true)
	{
		auto_ptr<ACE_Method_Object> Mo(this->m_TaskQue.dequeue());
		if (Mo->call() == -1)
		{
			fini();
			break;
		}
		//智能指针自动删除对象
	}

	ACE_DEBUG((LM_INFO, ACE_TEXT("Db Thread quit.\n")));
	return 0;
}

int CDbModule::PushTask(ACE_Method_Object* pMethodObject)
{
	this->m_TaskQue.enqueue(pMethodObject);
	auto_ptr<ACE_Method_Object> Mo(this->m_TaskQue.dequeue());
	Mo->call();

	return 0;
}

int CDbModule::LoadLocalData()
{
	const char* ITEMFILE = "ShopItem.xml";

	TiXmlDocument xmlDoc;
	if (!xmlDoc.LoadFile(ITEMFILE))
	{
		ACE_ERROR_RETURN((LM_ERROR, ACE_TEXT("(%t) %p\n"), ACE_TEXT("load ShopItem failed.")), -1);
	}
	TiXmlElement* pXmlRoot = xmlDoc.RootElement();
	//道具信息
	CQuery* pQuery = m_DbConn->CreateQuery();
	//删除道具配置表
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"%s", "DELETE FROM ItemConf");
	pQuery->ExecuteUpdate(szStatement, iStatementLen);
	//赠送费用
	TiXmlElement* pXmlElement = pXmlRoot->FirstChildElement();
	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "INSERT INTO ItemConf VALUES(0, 0, 0, 0, 0, %s, 0);", pXmlElement->Attribute("Fee"));
	pQuery->ExecuteUpdate(szStatement, iStatementLen);

	pXmlElement = pXmlElement->NextSiblingElement();
	for (const TiXmlElement* pItemInfoElement = pXmlElement->FirstChildElement(); pItemInfoElement; pItemInfoElement = pItemInfoElement->NextSiblingElement())
	{
		for (const TiXmlElement* pSubCatElment = pItemInfoElement->FirstChildElement(); pSubCatElment; pSubCatElment = pSubCatElment->NextSiblingElement())
		{
			for (const TiXmlElement* pItemElement = pSubCatElment->FirstChildElement(); pItemElement; pItemElement = pItemElement->NextSiblingElement())
			{
				iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "INSERT INTO ItemConf VALUES(%s, %s, %s, %s, %s, %s, %s);", 
					pItemElement->Attribute("ItemID"), pItemInfoElement->Attribute("ItemCatID"), pSubCatElment->Attribute("ItemSubCatID"), pItemElement->Attribute("PurchaseType"),
					pItemElement->Attribute("ScoreNum"), pItemElement->Attribute("PointNum"), pItemElement->Attribute("PriceType"));
				pQuery->ExecuteUpdate(szStatement, iStatementLen);
			}
		}
	}

	//Vip方案
	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "%s", "DELETE FROM VipConf;");
	pQuery->ExecuteUpdate(szStatement, iStatementLen);

	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "%s", "DELETE FROM VipItemInfo;");
	pQuery->ExecuteUpdate(szStatement, iStatementLen);

	pXmlElement = pXmlElement->NextSiblingElement();
	for (const TiXmlElement* pVipInfoElement = pXmlElement->FirstChildElement(); pVipInfoElement; pVipInfoElement = pVipInfoElement->NextSiblingElement())
	{
		iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "INSERT INTO VipConf VALUES(%s, %s, '%s', %s);", pVipInfoElement->Attribute("VipID"), pVipInfoElement->Attribute("ValidDateNum")
			, "", pVipInfoElement->Attribute("GameMoney"));
		pQuery->ExecuteUpdate(szStatement, iStatementLen);
		for (const TiXmlElement* pVipElement = pVipInfoElement->FirstChildElement(); pVipElement; pVipElement = pVipElement->NextSiblingElement())
		{
			iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "INSERT INTO VipItemInfo VALUES(%s, %s, %s);", pVipInfoElement->Attribute("VipID"), pVipElement->Attribute("ItemID")
				, pVipElement->Attribute("ItemNum"));
			pQuery->ExecuteUpdate(szStatement, iStatementLen);
		}
	}

	//会员等级
	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "%s", "DELETE FROM ShopCustomerConf;");
	pQuery->ExecuteUpdate(szStatement, iStatementLen);
	pXmlElement = pXmlElement->NextSiblingElement();
	for (const TiXmlElement* pXmlMember = pXmlElement->FirstChildElement(); pXmlMember; pXmlMember = pXmlMember->NextSiblingElement())
	{
		iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "INSERT INTO ShopCustomerConf VALUES(%s, %s, %s, %s, %s, %s);", pXmlMember->Attribute("Class")
			, pXmlMember->Attribute("Level"), pXmlMember->Attribute("MinExp"), pXmlMember->Attribute("MaxExp"), pXmlMember->Attribute("DiscountRate"), pXmlMember->Attribute("LevelExp"));
		pQuery->ExecuteUpdate(szStatement, iStatementLen);
	}
	
	m_DbConn->DestroyQuery(pQuery);
	return 0;
}

