﻿/** @file SessionHandler.h 
@brief 处理TCP连接的具体细节（连接建立、数据到来、数据发送、连接关闭）
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-03-26
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef SESSIONHANDLER_H 
#define SESSIONHANDLER_H
#include "Common.h"
enum {GRACE_CLOSE = 0, ABORTIVE_CLOSE = 1};

class CSessionHandler: public ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_SYNCH>
{
public:
	typedef ACE_Svc_Handler<ACE_SOCK_STREAM, ACE_SYNCH> parent;
	CSessionHandler(void);
	virtual ~CSessionHandler(void);
public:
	virtual int open(void * = 0);
	virtual int handle_input(ACE_HANDLE);
	virtual int handle_close(ACE_HANDLE, ACE_Reactor_Mask);
	virtual int handle_output(ACE_HANDLE fd /* = ACE_INVALID_HANDLE */);
public:
	virtual int notifysend();
private:
	bool m_Connected;
};

#endif
