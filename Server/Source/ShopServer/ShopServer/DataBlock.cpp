﻿#include "DataBlock.h"

CDataBlock::CDataBlock(size_t nSize)
: ACE_Message_Block(nSize), m_OrgSize(0), m_CurSize(0)
{

}

CDataBlock::~CDataBlock(void)
{

}

void CDataBlock::CurSize(size_t nCurSize)
{
	m_CurSize += nCurSize;
}

void CDataBlock::DataSize(size_t nDataSize)
{
	m_OrgSize = nDataSize;
}
