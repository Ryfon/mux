﻿/** @file DataProc.h 
@brief 管理协议处理函数
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-03-30
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef DATAPROC_H
#define DATAPROC_H
#include "Common.h"
#include "ShopSvrProtocol.h"

class CDataProc;
class CSessionHandler;

struct MSGHANDLER 
{
	typedef int (CDataProc::*HANDLER)(CSessionHandler* pSession, char* pData, unsigned short usDataLen);
	MSGHANDLER(): handler(0){}
	MSGHANDLER(HANDLER _handler): handler(_handler){}
	~MSGHANDLER(){}
	HANDLER handler;
};

#define DealPlayerPkgPre( OUTTYPE, outPtr, inputPkg, inputLen, ProtolTool ) \
	OUTTYPE  tmpStruct;\
	OUTTYPE* outPtr = &tmpStruct;\
	memset( outPtr, 0, sizeof(OUTTYPE) );\
	size_t outBufLen = sizeof(tmpStruct);\
	int nDecodeLen = (int)ProtolTool.DeCode(OUTTYPE::wCmd, &tmpStruct, outBufLen, (char*)inputPkg, inputLen );\
	if ( nDecodeLen < 0 )\
{\
	return false;\
}

//Shop内部用结构定义
#pragma pack(push, 1)

//保证协议按字节对齐

typedef ShopServer::MsgHeader MSGHEADER;
typedef ShopServer::MsgHeader* PMSGHEADER;

#define INN_CONN 0xFFFF
#define INN_CONNABORTCLOSE 0xFFFE
#define INN_CONNPASSIVECLOSE 0xFFFD
#define INN_SHOPEXIT 0xFFFC

//连接建立消息
struct CONNMSG 
{
	static const USHORT wCmd = INN_CONN;	
};


//主动关闭连接
struct CONNABORTCLOSE 
{
	static const USHORT wCmd = INN_CONNABORTCLOSE;	
};

//被动关闭连接
struct CONNPASSIVECLOSE 
{
	static const USHORT wCmd = INN_CONNPASSIVECLOSE;	
};

//程序退出
struct SHOPEXIT 
{
	static const USHORT wCmd = INN_SHOPEXIT;	
};
#pragma pack(pop)

typedef std::map<ACE_HANDLE, CSessionHandler*> SESSION_LIST;

struct PLAYERINFO 
{
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;	
	unsigned int m_RoleID;
	std::string m_RoleName;
	unsigned int m_RaceID;
	unsigned int m_ClassID;
	unsigned int m_Level;
	unsigned int m_GuildNo; //0表示没有加入战盟
};

typedef ACE_Hash_Map_Manager<unsigned int, PLAYERINFO, ACE_Null_Mutex> MAP_PLAYERINFO;


class CProcTask;
class CPurchaseGiftObject;
class CDataProc
{
	typedef std::map<unsigned short, MSGHANDLER> HANDLERMAP;
public:
	CDataProc(CProcTask& ProcTask);
	virtual ~CDataProc(void);
public:
	int ProcMsg(CSessionHandler* pSession, ACE_Message_Block* pMsg);
public:
	int OnConnected(CSessionHandler* pSession);
	int OnConnectionClose(CSessionHandler* pSession);
	int OnPacket(CSessionHandler* pSession, ACE_Message_Block* pMsg);
public:
	int ShopExit();
private:
	int RegisterHandler();
private:
	//连接到来消息
	int ProcConn(CSessionHandler*, char*, unsigned short);
	//被动关闭连接
	int ProcPassiveClose(CSessionHandler*, char*, unsigned short);
	//程序退出
	int ProcShopExit(CSessionHandler*, char*, unsigned short);
private:
	//处理玩家上线/下线通知
	int ProcPlayerNotify(CSessionHandler*, char*, unsigned short);
	//查询玩家账号信息
	int ProcQryAccountInfo(CSessionHandler*, char*, unsigned short);
	//处理买道具请求
    int ProcPurchaseItem(CSessionHandler*, char*, unsigned short);
	//处理查询玩家vip方案请求
	int ProcQryVipInfo(CSessionHandler*, char*, unsigned short);
	//处理买vip方案请求
	int ProcPurchaseVip(CSessionHandler*, char*, unsigned short);
	//处理领取vip方案请求
	int ProcFetchVipItem(CSessionHandler*, char*, unsigned short);
	//处理领取vip方案回应
	int ProcFetchVipItemResult(CSessionHandler*, char*, unsigned short);
	//处理购买赠送请求
	int ProcPurchaseGift(CSessionHandler*, char*, unsigned short);
	//处理gate发来的交易失败
	int ProcTranFailed(CSessionHandler*, char*, unsigned short);
	//处理gate发来的玩家查询玩家缓存的道具
	int ProcQryCachedItem(CSessionHandler*, char*, unsigned short);
	//处理gate发来的玩家上线/下线通知
	int ProcFetchCachedItem(CSessionHandler*, char*, unsigned short);
	//处理
	int ProcQryRoleInfo(CSessionHandler*, char*, unsigned short);
	//
	int ProcQryRankInfo(CSessionHandler*, char*, unsigned short);
	
	int ProcPlayerGuildInfo(CSessionHandler*, char*, unsigned short);
	
public:
	ShopServer::ShopSrvProtocol* GetProtocol()
	{
		return &m_ShopProtocol;
	}
	MAP_PLAYERINFO* GetPlayerInfoMap()
	{
		return &m_PlayerInfoMap;
	}
private:
	HANDLERMAP m_HandlerMap;
	typedef std::vector<CPurchaseGiftObject*> PURGIFTOBJVECT;
	std::map<std::string, PURGIFTOBJVECT> m_PurGiftObjMap;
	MAP_PLAYERINFO m_PlayerInfoMap;
    SESSION_LIST m_SessionList;
	CProcTask& m_ProcTask;
	ShopServer::ShopSrvProtocol m_ShopProtocol; //协议解析器
	bool m_Exit;
};

#endif
