﻿/** @file MysqlWrapper.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2007-10-26
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef MYSQL_WRAPPER_H
#define MYSQL_WRAPPER_H

#include "Common.h"
//msyql header
#include <mysql.h>

#pragma pack(push, 1)

typedef struct _stConnectionString /// 连接串信息
{
	char szHost[64];	/// 主机名
	char szUser[16];	/// 用户名
	char szPassword[16];/// 口令
	char szDb[16];		/// 数据库名
	unsigned short nPort; //数据库端口
}stConnectionString;

#pragma pack(pop)


/// 前置声明
class CConnection;
class CQuery;
class CResult;

/**
* @class CConnection
*
* @brief 到Mysql服务器的连接
* 
*/
class CConnection
{
public:
	/// 构造
	CConnection(void);
	/// 析构
	~CConnection(void);

public:
	/// 打开连接
	int Open(stConnectionString *pstConnStr);

	/// 关闭
	void Close(void);

	/// 检查连接状况
	int Ping(void);

	/// 获取内支句柄
	MYSQL *Mysql(void);

	/// 创建一个请求
	CQuery * CreateQuery(void);
	int EscapeString(char* szTo, const char* szFrom, int nLen);
	/// 关闭一个请求
	void DestroyQuery(CQuery* pQuery);

private:
	/// 是否初始化
	bool m_bIsInit;

	/// 内置连接句柄
	MYSQL *m_pMysql;

	/// 连接串信息
	stConnectionString m_stConnStr;
};

class CQuery
{
	friend class CConnection;

private:
	/// 构造
	explicit CQuery(CConnection *pConnection);

public:
	/// 析构
	~CQuery(void);

public:
	/// 执行查询(处理Select/Show等操作)
	int ExecuteSelect(const char *pszQuery, unsigned long ulLength, CResult*& pResult);

	/// 执行更新(处理Update/Insert/Delete等操作)
	int ExecuteUpdate(const char *pszQuery, unsigned long ulLength);

	/// 释放结果集
	void FreeResult(CResult *pResult);
	
	// 转义操作
	unsigned long RealEscape(char *pszDest, const char *pszSource, unsigned long ulSourceLen);

	// insertID
	unsigned int InsertID(void);
private:
	/// 对应的连接
	CConnection *m_pConnection;

	/// insertID(对insert语句有效)
	unsigned int m_uiInsertID;
};

class CResult
{	
	friend class CConnection;
	friend class CQuery;
	friend class ProcedureWrapper;
	friend class LoginCheckProc;

private:
	/// 构造
	CResult(CConnection *pConnection, MYSQL_RES *pRes);

public:
	/// 析构
	~CResult(void);

public:
	/// 获取内置结果集
	MYSQL_RES *Res(void);
	
	/// 获取行数
	unsigned int RowNum(void);

	/// 获取列数
	unsigned int FieldNum(void);

public:
	/// 获取下一行
	bool Next(void);

	/// 获取整形字段
	int GetInt(unsigned int uiFieldIndex);

	/// 获取字符串字段
	char *GetString(unsigned int uiFieldIndex);

	/// 获取双精度字段
	double GetDouble(unsigned int uiFieldIndex);

	/// 获取单精度字段
	float GetFloat(unsigned int uiFieldIndex);

	/// 获取日期字段
	unsigned int GetDate(unsigned int uiFieldIndex);

	/// .....

private:
	/// 对应的连接
	CConnection *m_pConnection;

	/// 内置的结果集
	MYSQL_RES *m_pResult;

	/// 循环子(中间变量)
	MYSQL_ROW m_row;

	/// 行数
	unsigned int m_rowNum;
	
	/// 字段数
	unsigned int m_fieldNum;
};

#endif/*MYSQL_WRAPPER_H*/

