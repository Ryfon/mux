﻿#include "MethodObject.h"
#include "MysqlWrapper.h"
#include "SessionHandler.h"
#include "DataBlock.h"
#include "DataProc.h"

CMethodObject::CMethodObject(CConnection *pConnetion, CDataProc& DataProc)
: m_DbConn(pConnetion), m_DataProc(DataProc)
{

}
CMethodObject::~CMethodObject()
{

}

int CMethodObject::GetAccountInfo(const char *szAccount, UINT &nGameMoney, UINT &nScore, BYTE &nMemberClass, BYTE &nMemberLevel, UINT &nMemberExp, FLOAT &fPurchaseDisc)
{
	int nRet = 0;
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_GetAccountInfo('%s', @oGameMoney, @oScore, @oMemberClass, @oMemberLevel, @oMemberExp, @oPurchaseDisc)", szAccount);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	if (nRet != 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		m_DbConn->DestroyQuery(pQuery);
		return nRet;
	}
	//获得输出参数
	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "SELECT @oGameMoney, @oScore, @oMemberClass, @oMemberLevel, @oMemberExp, @oPurchaseDisc");
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	else
	{
		if (pResult->Next())
		{
			nGameMoney = pResult->GetInt(0);
			nScore = pResult->GetInt(1);
			nMemberClass = pResult->GetInt(2);
			nMemberLevel = pResult->GetInt(3);
			nMemberExp = pResult->GetInt(4);
			fPurchaseDisc = pResult->GetFloat(5);
		}
	}

	if (pResult)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);

	return nRet;
}
int CMethodObject::PurchaseCheck(const char* szAccount, BYTE nServiceType, UINT nServiceID, UINT nServiceNum, BYTE nPurchaseType, BYTE nPurchaseDest, UINT& nServiceMoney, UINT& nAdditionMoney)
{
	int nRet = 0;
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_PurchaseCheck('%s', %u, %d, %u, %d, %d, @Result, @GameMoney, @AdditionMoney)", szAccount, nServiceType, nServiceID, nServiceNum, nPurchaseType, nPurchaseDest);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	if (nRet != 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		m_DbConn->DestroyQuery(pQuery);
		return nRet;
	}
	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "SELECT @Result, @GameMoney, @AdditionMoney");
	pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet == 0 && pResult != 0)
	{
		if (pResult->Next())
		{
			nRet = pResult->GetInt(0);
			nServiceMoney = pResult->GetInt(1);
			nAdditionMoney = pResult->GetInt(2);
		}
		else
		{
			nRet = ShopServer::SHOP_SYSERROR;
		}
	}
	else
		nRet = ShopServer::SHOP_SYSERROR;

	if (pResult)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);
	return nRet;
}


int CMethodObject::GetVipInfo(const char *szAccount, unsigned int RoleID, UINT &nVipID, USHORT &nLeftDays, BYTE &nFetched)
{
	int nRet = 0;
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_GetAccountVipInfo('%s', %u)", szAccount, RoleID);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	else
	{
		if (pResult->Next())
		{
			nVipID = pResult->GetInt(0);
			nLeftDays = pResult->GetInt(1);
			nFetched = pResult->GetInt(2);
		}
		else
		{

		}
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);
	return nRet;
}

int CMethodObject::UpdateConsumeRank(unsigned int nRoleID, int nCost)
{
	PLAYERINFO playerInfo;
	int nRet = m_DataProc.GetPlayerInfoMap()->find(nRoleID, playerInfo);
	if (nRet == -1)
	{
		return -1;
	}

	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_UpdateRank('%s', %u, '%s', %u, %u, %u, %d)", playerInfo.m_Account.c_str(), nRoleID, playerInfo.m_RoleName.c_str(), playerInfo.m_RaceID, playerInfo.m_Level, playerInfo.m_ClassID, nCost);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet != 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);

	return 0;
}

int CMethodObject::UpdateGuildConsume(unsigned int nRoleID, int nCost, unsigned int& nPrestigeAdd)
{
	nPrestigeAdd = 0;
	PLAYERINFO playerInfo;
	int nRet = m_DataProc.GetPlayerInfoMap()->find(nRoleID, playerInfo);
	if (nRet == -1)
	{
		return -1;
	}

	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_UpdateGuildConsume('%s', %u, %u, %u, @PrestigeAdd)", playerInfo.m_Account.c_str(), nRoleID, playerInfo.m_GuildNo, nCost);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);		
	}
	if (nRet != 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}

	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "SELECT @PrestigeAdd");
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet == 0 && pResult != 0)
	{
		if (pResult->Next())
		{
			nRet = 0;
			nPrestigeAdd = pResult->GetInt(0);
		}
		else
		{
			nRet = ShopServer::SHOP_SYSERROR;
		}
	}
	else
		nRet = ShopServer::SHOP_SYSERROR;

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);
	return 0;
}

int CMethodObject::SendPrestigeAddMsg(CSessionHandler* pSession, ShopServer::S_PlayerID PID, unsigned int nRoleID, unsigned int nPrestigeAdd)
{
	if (nPrestigeAdd == 0)
	{
		return 0;
	}
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGGuildPrestigeAdd::wCmd;
	pMsgHeader->PackType = 0;

	ShopServer::SGGuildPrestigeAdd sgGuildPrestigeAdd;
	ACE_OS::memset(&sgGuildPrestigeAdd, 0, sizeof(sgGuildPrestigeAdd));
	sgGuildPrestigeAdd.UserId = PID;
	sgGuildPrestigeAdd.PrestigeAdd = nPrestigeAdd;
	sgGuildPrestigeAdd.RoleID = nRoleID;
	//组包
	ShopServer::SGGuildPrestigeAdd* pSGGuildPrestigeAdd = reinterpret_cast<ShopServer::SGGuildPrestigeAdd*>(pData->base() + HEADERSIZE);
	size_t nEnCodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgGuildPrestigeAdd, (char*)pSGGuildPrestigeAdd, sizeof(ShopServer::SGGuildPrestigeAdd));
	if (nEnCodeLen == (size_t)-1)
	{
		//ACE_DEBUG
		pData->release();
		return -1;
	}

	pMsgHeader->PackLen = (USHORT)(nEnCodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);

	//发送数据
	pSession->putq(pData);
	pSession->notifysend();
	return 0;
}


CQryAccountObject::CQryAccountObject(char* szAccount, ShopServer::S_PlayerID PId, CConnection* pConnection, CSessionHandler* pSession, CDataProc& DataProc)
: CMethodObject(pConnection, DataProc), m_Account(szAccount), m_UserId(PId),  m_Session(pSession) 
{
	char szBuf[MAX_NAME_SIZE];
	m_DbConn->EscapeString(szBuf, szAccount, strlen(szAccount));
	m_Account = szBuf;
}

CQryAccountObject::~CQryAccountObject(void)
{

}

int CQryAccountObject::call()
{
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGAccountInfo::wCmd;
	pMsgHeader->PackType = 0;

	ShopServer::SGAccountInfo sgAccountInfo;
	ACE_OS::memset(&sgAccountInfo, 0, sizeof(sgAccountInfo));
	ACE_OS::strncpy(sgAccountInfo.Account, m_Account.c_str(), m_Account.length());
	sgAccountInfo.AccountLen = (BYTE)(m_Account.length() + 1);
	
	int nRet = GetAccountInfo(m_Account.c_str(), sgAccountInfo.GameMoney, sgAccountInfo.Score, sgAccountInfo.MemberClass, sgAccountInfo.MemberLevel, 
		sgAccountInfo.MemberExp, sgAccountInfo.PurchaseDisc);

	sgAccountInfo.Result = nRet;
	sgAccountInfo.UserId = m_UserId;

	//组包
	ShopServer::SGAccountInfo* pSGAccountInfo = reinterpret_cast<ShopServer::SGAccountInfo*>(pData->base() + HEADERSIZE);
	size_t nEnCodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgAccountInfo, (char*)pSGAccountInfo, sizeof(ShopServer::SGAccountInfo));
	if (nEnCodeLen == (size_t)-1)
	{
		//ACE_DEBUG
		pData->release();
		return -1;
	}

	pMsgHeader->PackLen = (USHORT)(nEnCodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);

	//发送数据
	m_Session->putq(pData);
	//m_Session->handle_output(m_Session->get_handle());
	m_Session->notifysend();

	return 0;
}


CQryVipInfoObject::CQryVipInfoObject(char *szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_Account(szAccount), m_UserId(PId), m_Session(pSession), m_RoleID(RoleID)
{
	char szBuf[MAX_NAME_SIZE];
	m_DbConn->EscapeString(szBuf, szAccount, strlen(szAccount));
	m_Account = szBuf;
}

CQryVipInfoObject::~CQryVipInfoObject()
{

}

int CQryVipInfoObject::call(void)
{
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGVipInfo::wCmd;
	pMsgHeader->PackType = 0;

	ShopServer::SGVipInfo sgVipInfo;
	ACE_OS::memset(&sgVipInfo, 0, sizeof(sgVipInfo));
	sgVipInfo.AccountLen = (BYTE)(m_Account.length() + 1);
	ACE_OS::strncpy(sgVipInfo.Account, m_Account.c_str(), m_Account.length());
	sgVipInfo.UserId = m_UserId;
	sgVipInfo.RoleID = m_RoleID;
	sgVipInfo.Result = GetVipInfo(m_Account.c_str(), m_RoleID, sgVipInfo.VipID, sgVipInfo.LeftDays, sgVipInfo.Fetched);
	
	ShopServer::SGVipInfo* pSGVipInfo = reinterpret_cast<ShopServer::SGVipInfo*>(pData->base() + HEADERSIZE);
	size_t nEncodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgVipInfo, (char*)pSGVipInfo, sizeof(sgVipInfo));
	if (nEncodeLen == (size_t)-1)
	{
		pData->release();
		return -1;
	}
	pMsgHeader->PackLen = (USHORT)(nEncodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);

	//发送数据
	m_Session->putq(pData);
	m_Session->notifysend();

	return 0;
}

CPurchaseItemObject::CPurchaseItemObject(char *szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned char ucPurchaseType, unsigned int uItemID, 
	int nItemNum, unsigned int uCliCalc, 
	CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
	: CMethodObject(pConnection, DataProc), m_Account(szAccount), m_UserId(PId), m_Session(pSession), m_RoleID(RoleID), m_PurchaseType(ucPurchaseType), m_ItemID(uItemID), 
	m_ItemNum(nItemNum), m_CliCalc(uCliCalc)
	
{
	char szBuf[MAX_NAME_SIZE];
	m_DbConn->EscapeString(szBuf, szAccount, strlen(szAccount));
	m_Account = szBuf;
}

CPurchaseItemObject::~CPurchaseItemObject()
{

}

int CPurchaseItemObject::call()
{
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGPurchaseItem::wCmd;
	pMsgHeader->PackType = 0;

	ShopServer::SGPurchaseItem sgPurchaseItem;
	ACE_OS::memset(&sgPurchaseItem, 0, sizeof(sgPurchaseItem));
	sgPurchaseItem.AccountLen = (BYTE)(m_Account.length() + 1);
	ACE_OS::strncpy(sgPurchaseItem.Account, m_Account.c_str(), m_Account.length());
	sgPurchaseItem.UserId = m_UserId;
	sgPurchaseItem.RoleID = m_RoleID;
	sgPurchaseItem.ItemID = m_ItemID;
	sgPurchaseItem.ItemNum = m_ItemNum;
	sgPurchaseItem.PurchaseType = m_PurchaseType;

	UINT nServiceMoney, nAdditionMoney;
	int nRet = PurchaseCheck(m_Account.c_str(), 0, m_ItemID, m_ItemNum, m_PurchaseType, 0, nServiceMoney, nAdditionMoney);
	if (nRet == 0)
	{
		if (m_CliCalc != (nServiceMoney + nAdditionMoney))
		{
			nRet = ShopServer::SHOP_SYSERROR;
		}
	}

	UINT nTransID;
	if (nRet == 0)
	{
		nRet = PurchaseItem(nTransID);
	}

	sgPurchaseItem.PurchaseResult = nRet;

	if (nRet == 0)
	{
		sgPurchaseItem.ComsumeNum = nServiceMoney + nAdditionMoney;
		sgPurchaseItem.TransID = nTransID;
		if (m_PurchaseType == 0)
		{
			UpdateConsumeRank(m_RoleID, sgPurchaseItem.ComsumeNum);
			UINT nPrestigeAdd;
			UpdateGuildConsume(m_RoleID, sgPurchaseItem.ComsumeNum, nPrestigeAdd);
			SendPrestigeAddMsg(m_Session, m_UserId, m_RoleID, nPrestigeAdd);
		}
		
		//获得用户余额信息
		GetAccountInfo(m_Account.c_str(), sgPurchaseItem.GameMoney, sgPurchaseItem.Score, sgPurchaseItem.MemberClass, sgPurchaseItem.MemberLevel, sgPurchaseItem.MemberExp, 
			sgPurchaseItem.PurchaseDisc);
	}

	ShopServer::SGPurchaseItem* pSGPurchaseItem = reinterpret_cast<ShopServer::SGPurchaseItem*>(pData->base() + HEADERSIZE);
	size_t nEncodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgPurchaseItem, (char*)pSGPurchaseItem, sizeof(sgPurchaseItem));
	if (nEncodeLen == (size_t)-1)
	{
		pData->release();
		return -1;
	}

	pMsgHeader->PackLen = (USHORT)(nEncodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);
	m_Session->putq(pData);
	m_Session->notifysend();

	return 0;
}

int CPurchaseItemObject::PurchaseItem(UINT& nTransID)
{
	int nRet = 0;

	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_PurchaseItem('%s', %u, %d, %u, %d, @Result, @TransID)", m_Account.c_str(), m_RoleID, m_PurchaseType, m_ItemID, m_ItemNum);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	if (nRet != 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		m_DbConn->DestroyQuery(pQuery);
		return nRet;
	}
	//获得输出参数
	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "SELECT @Result, @TransID");
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	else
	{
		pResult->Next();
		nRet = pResult->GetInt(0);
		nTransID = pResult->GetInt(1);
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);	
	return nRet;
}

CPurchaseVipObject::CPurchaseVipObject(char *szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int VipID, unsigned int uCliCalc, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_Account(szAccount), m_UserId(PId), m_Session(pSession), m_RoleID(RoleID), m_VipID(VipID), m_CliCalc(uCliCalc)
{
	char szBuf[MAX_NAME_SIZE];
	m_DbConn->EscapeString(szBuf, szAccount, strlen(szAccount));
	m_Account = szBuf;
}

CPurchaseVipObject::~CPurchaseVipObject()
{

}

int CPurchaseVipObject::PurchaseVip()
{
	int nRet = 0;

	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_PurchaseVip('%s', %u, %u, @Result)", m_Account.c_str(), m_RoleID, m_VipID);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
		pResult = 0;
	}
	if (nRet != 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		m_DbConn->DestroyQuery(pQuery);
		return nRet;
	}
	//获得输出参数
	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "SELECT @Result");
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	else
	{
		if (pResult->Next())
		{
			nRet = pResult->GetInt(0);
		}
		else
			nRet = ShopServer::SHOP_SYSERROR;
		
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);	
	return nRet;
}

int CPurchaseVipObject::call()
{
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGPurchaseVip::wCmd;
	pMsgHeader->PackType = 0;

	ShopServer::SGPurchaseVip sgPurchaseVip;
	ACE_OS::memset(&sgPurchaseVip, 0, sizeof(sgPurchaseVip));
	sgPurchaseVip.AccountLen = (BYTE)(m_Account.length() + 1);
	ACE_OS::strncpy(sgPurchaseVip.Account, m_Account.c_str(), m_Account.length());
	sgPurchaseVip.UserId = m_UserId;
	sgPurchaseVip.RoleID = m_RoleID;
	sgPurchaseVip.VipID = m_VipID;

	UINT nServiceMoney, nAdditionMoney;
	int nRet = PurchaseCheck(m_Account.c_str(), 1, m_VipID, 1, 0, 0, nServiceMoney, nAdditionMoney);
	if (nRet == 0)
	{
		if (m_CliCalc != (nServiceMoney + nAdditionMoney))
		{
			nRet = ShopServer::SHOP_SYSERROR;
		}
	}

	if (nRet == 0)
	{
		nRet = PurchaseVip();
	}

	sgPurchaseVip.PurchaseResult = nRet;

	if (nRet == 0)
	{
		sgPurchaseVip.ComsumeNum = nServiceMoney + nAdditionMoney;
		sgPurchaseVip.TransID = 0;
		UpdateConsumeRank(m_RoleID, sgPurchaseVip.ComsumeNum);
		UINT nPrestigeAdd;
		UpdateGuildConsume(m_RoleID, sgPurchaseVip.ComsumeNum, nPrestigeAdd);
		SendPrestigeAddMsg(m_Session, m_UserId, m_RoleID, nPrestigeAdd);

		//获得用户余额信息
		GetAccountInfo(m_Account.c_str(), sgPurchaseVip.GameMoney, sgPurchaseVip.Score, sgPurchaseVip.MemberClass, sgPurchaseVip.MemberLevel, sgPurchaseVip.MemberExp, 
			sgPurchaseVip.PurchaseDisc);
	}

	ShopServer::SGPurchaseVip* pSGPurchaseVip = reinterpret_cast<ShopServer::SGPurchaseVip*>(pData->base() + HEADERSIZE);
	size_t nEncodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgPurchaseVip, (char*)pSGPurchaseVip, sizeof(sgPurchaseVip));
	if (nEncodeLen == (size_t)-1)
	{
		pData->release();
		return -1;
	}

	pMsgHeader->PackLen = (USHORT)(nEncodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);
	m_Session->putq(pData);
	m_Session->notifysend();
	return 0;
}

CFetchVipItemObject::CFetchVipItemObject(char *szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int VipID, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_Account(szAccount), m_UserId(PId), m_Session(pSession), m_RoleID(RoleID), m_VipID(VipID)
{
	char szBuf[MAX_NAME_SIZE];
	m_DbConn->EscapeString(szBuf, szAccount, strlen(szAccount));
	m_Account = szBuf;
}

CFetchVipItemObject::~CFetchVipItemObject()
{

}

int CFetchVipItemObject::FetchVipItem(UINT &nTransID)
{
	int nRet = 0;

	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_FetchVipItem('%s', %u, %u, @Result, @TransID)", m_Account.c_str(), m_RoleID, m_VipID);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
		pResult = 0;
	}
	if (nRet != 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		m_DbConn->DestroyQuery(pQuery);
		return nRet;
	}
	//获得输出参数
	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "SELECT @Result, @TransID");
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	else
	{
		pResult->Next();
		nRet = pResult->GetInt(0);
		nTransID = pResult->GetInt(1);
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);		
	return nRet;
}

int CFetchVipItemObject::call()
{
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGFetchVipItem::wCmd;
	pMsgHeader->PackType = 0;
	
	ShopServer::SGFetchVipItem sgFetchVipItem;
	ACE_OS::memset(&sgFetchVipItem, 0, sizeof(sgFetchVipItem));
	sgFetchVipItem.AccountLen = (BYTE)(m_Account.length() + 1);
	ACE_OS::strncpy(sgFetchVipItem.Account, m_Account.c_str(), m_Account.length());
	sgFetchVipItem.UserId = m_UserId;
	sgFetchVipItem.RoleID = m_RoleID;
	sgFetchVipItem.VipID = m_VipID;

	//检查玩家vip信息
	UINT nVipID;
	USHORT nLeftDays = 0;
	BYTE nFetched = 0;
	sgFetchVipItem.Result = GetVipInfo(m_Account.c_str(), m_RoleID, nVipID, nLeftDays, nFetched);
	if (sgFetchVipItem.Result == 0)
	{
		if (nVipID != m_VipID)
		{
			sgFetchVipItem.Result = ShopServer::SHOP_SYSERROR;
		}
		else
		{
			if (nFetched == 1)
			{
				sgFetchVipItem.Result = ShopServer::FETCH_VIPITEM_DONE;
			}
		}
	}

	//更新Vip领取信息
	if (sgFetchVipItem.Result == 0)
	{
		UINT nTransID;
		sgFetchVipItem.Result = FetchVipItem(nTransID);
	}

	std::vector<ShopServer::VipItem> vectVipItem;
	//组织Vip道具
	if (sgFetchVipItem.Result == 0)
	{
		sgFetchVipItem.Result = CreateVipItem(vectVipItem);
	}

	if (sgFetchVipItem.Result == 0)
	{
		sgFetchVipItem.VipItemLen = (BYTE)vectVipItem.size();
		for (size_t nIndex = 0; nIndex < vectVipItem.size(); nIndex++)
		{
			sgFetchVipItem.VipItemArr[nIndex] = vectVipItem[nIndex];
		}
	}

	ShopServer::SGFetchVipItem* pSGFetchVipItem = reinterpret_cast<ShopServer::SGFetchVipItem*>(pData->base() + HEADERSIZE);
	size_t nEncodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgFetchVipItem, (char*)pSGFetchVipItem, sizeof(sgFetchVipItem));
	if (nEncodeLen == (size_t)-1)
	{
		pData->release();
		return -1;
	}
	pMsgHeader->PackLen = (USHORT)(nEncodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);
	
	m_Session->putq(pData);
	m_Session->notifysend();

	return 0;
}

int CFetchVipItemObject::CreateVipItem(std::vector<ShopServer::VipItem> &vectVipItem)
{
	int nRet = 0;
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_VipItemInfo(%u)", m_VipID);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);

	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	else
	{
		while (pResult->Next())
		{
			ShopServer::VipItem VipItem;
			VipItem.ItemTypeID = pResult->GetInt(0);
			VipItem.ItemNum = pResult->GetInt(1);
			vectVipItem.push_back(VipItem);
		}
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);		
	return nRet;
}

CFetchVipItemFailedObject::CFetchVipItemFailedObject(char *szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int VipID, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_Account(szAccount), m_UserId(PId), m_Session(pSession), m_RoleID(RoleID), m_VipID(VipID) 
{
	char szBuf[MAX_NAME_SIZE];
	m_DbConn->EscapeString(szBuf, szAccount, strlen(szAccount));
	m_Account = szBuf;
}

CFetchVipItemFailedObject::~CFetchVipItemFailedObject()
{

}

int CFetchVipItemFailedObject::call()
{
	FetchVipItemFail();
	return 0;
}

int CFetchVipItemFailedObject::FetchVipItemFail()
{
	int nRet = 0;
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_FetchVipItemFailed('%s', %u, %u)", m_Account.c_str(), m_RoleID, m_VipID);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);

	if (nRet != 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}

	m_DbConn->DestroyQuery(pQuery);		
	return nRet;
}

CPurchaseGiftObject::CPurchaseGiftObject(char *szSrcAccount, ShopServer::S_PlayerID SrcPId, unsigned int SrcRoleID, char* szRoleName, unsigned char ucPurchaseType, unsigned int uItemID, int nItemNum, char *szDestAccount, 
	unsigned int DestRoleID, char* szDestRoleName, unsigned int uCliCalc, char* szNote, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
	: CMethodObject(pConnection, DataProc), m_SrcAccount(szSrcAccount), m_SrcUserId(SrcPId), m_Session(pSession), m_SrcRoleID(SrcRoleID), m_RoleName(szRoleName), m_PurchaseType(ucPurchaseType), m_ItemID(uItemID), 
	m_ItemNum(nItemNum), m_CliCalc(uCliCalc), m_DestAccount(szDestAccount), m_DestRoleID(DestRoleID), m_Note(szNote), m_DestRoleName(szDestRoleName)
{
	m_RoleInfo = 0;
	char szBuf[MAX_NOTE_SIZE] = {0};
	m_DbConn->EscapeString(szBuf, szRoleName, strlen(szRoleName));
	m_RoleName = szBuf;
	m_DbConn->EscapeString(szBuf, szDestRoleName, strlen(szDestRoleName));
	m_DestRoleName = szBuf;
	m_DbConn->EscapeString(szBuf, szDestAccount, strlen(szDestAccount));
	m_DestAccount = szBuf;
	m_DbConn->EscapeString(szBuf, szNote, strlen(szNote));
	m_Note = szBuf;

}

CPurchaseGiftObject::~CPurchaseGiftObject()
{

}

int CPurchaseGiftObject::call()
{
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGPurchaseGift::wCmd;
	pMsgHeader->PackType = 0;

	ShopServer::SGPurchaseGift sgPurchaseGift;
	ACE_OS::memset(&sgPurchaseGift, 0, sizeof(sgPurchaseGift));
	sgPurchaseGift.SrcAccountLen = (BYTE)(m_SrcAccount.length() + 1);
	ACE_OS::strncpy(sgPurchaseGift.SrcAccount, m_SrcAccount.c_str(), m_SrcAccount.length());
	sgPurchaseGift.UserId = m_SrcUserId;
	sgPurchaseGift.SrcRoleID = m_SrcRoleID;
	//sgPurchaseGift.SrcRoleNameLen = (BYTE)(m_RoleName.length() + 1);
	//ACE_OS::strncpy(sgPurchaseGift.SrcRoleName, m_RoleName.c_str(), m_RoleName.length());
	sgPurchaseGift.ItemID = m_ItemID;
	sgPurchaseGift.ItemNum = m_ItemNum;
	sgPurchaseGift.DestAccountLen = (BYTE)(m_DestAccount.length() + 1);
	ACE_OS::strncpy(sgPurchaseGift.DestAccount, m_DestAccount.c_str(), m_DestAccount.length() + 1);
	sgPurchaseGift.DestRoleID = m_DestRoleID;
	//sgPurchaseGift.DestRoleNameLen = (BYTE)(m_DestRoleName.length() + 1);
	//ACE_OS::strncpy(sgPurchaseGift.DestRoleName, m_DestRoleName.c_str(), m_DestRoleName.length());
	sgPurchaseGift.NoteLen = (BYTE)(m_Note.length() + 1);
	ACE_OS::strncpy(sgPurchaseGift.Note, m_Note.c_str(), m_Note.length());
	sgPurchaseGift.PurchaseType = m_PurchaseType;
	UINT nServiceMoney, nAdditionMoney;
	int nRet = m_RoleInfo == 0 ? 0 : ShopServer::PUR_GIFT_ROLEINVALID;
	if (nRet == 0)
	{
		nRet = PurchaseCheck(m_SrcAccount.c_str(), 0, m_ItemID, m_ItemNum, m_PurchaseType, 1, nServiceMoney, nAdditionMoney);
	}
		
	if (nRet == 0)
	{
		if (m_CliCalc != (nServiceMoney + nAdditionMoney))
		{
			nRet = ShopServer::SHOP_SYSERROR;
		}
	}

	if (nRet == 0)
	{
		nRet = PurchaseGift(sgPurchaseGift.TransID);
	}

	sgPurchaseGift.PurchaseResult = nRet;

	if (nRet == 0)
	{
		if (m_PurchaseType == 0)
		{
			UpdateConsumeRank(m_SrcRoleID, nAdditionMoney + nServiceMoney);
			sgPurchaseGift.MoneyCost = nAdditionMoney + nServiceMoney;
			sgPurchaseGift.SocreCost = 0;
			UINT nPrestigeAdd;
			UpdateGuildConsume(m_SrcRoleID, sgPurchaseGift.MoneyCost, nPrestigeAdd);
			SendPrestigeAddMsg(m_Session, m_SrcUserId, m_SrcRoleID, nPrestigeAdd);

		}
		else
		{
			UpdateConsumeRank(m_SrcRoleID, nAdditionMoney);
			sgPurchaseGift.MoneyCost = nAdditionMoney;
			sgPurchaseGift.SocreCost = nServiceMoney;
		}
		//获得用户余额信息
		GetAccountInfo(m_SrcAccount.c_str(), sgPurchaseGift.GameMoney, sgPurchaseGift.Score, sgPurchaseGift.MemberClass, sgPurchaseGift.MemberLevel, sgPurchaseGift.MemberExp, 
			sgPurchaseGift.PurchaseDisc);
	}

	ShopServer::SGPurchaseGift* pSGPurchaseGift = reinterpret_cast<ShopServer::SGPurchaseGift*>(pData->base() + HEADERSIZE);
	size_t nEncodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgPurchaseGift, (char*)pSGPurchaseGift, sizeof(sgPurchaseGift));
	if (nEncodeLen == (size_t)-1)
	{
		pData->release();
		return -1;
	}

	pMsgHeader->PackLen = (USHORT)(nEncodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);
	m_Session->putq(pData);
	m_Session->notifysend();
	return 0;
}

int CPurchaseGiftObject::PurchaseGift(UINT &nTransID)
{
	int nRet = 0;

	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_PurchaseGift('%s', %u, %d, %u, %d, '%s', %u, '%s', @Result, @TransID)", m_SrcAccount.c_str(), m_SrcRoleID, m_PurchaseType, m_ItemID, m_ItemNum, m_DestAccount.c_str(), m_DestRoleID,
		m_Note.c_str());

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet != 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		m_DbConn->DestroyQuery(pQuery);
		return nRet;
	}
	//获得输出参数
	iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "SELECT @Result, @TransID");
	pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	else
	{
		pResult->Next();
		nRet = pResult->GetInt(0);
		nTransID = pResult->GetInt(1);
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);	
	return nRet;
}

void CPurchaseGiftObject::SetRoleStatus(unsigned char ucRoleStatus)
{
	m_RoleInfo = ucRoleStatus;
}

CStoreItemObject::CStoreItemObject(char* szSrcAccount, ShopServer::S_PlayerID SrcPId, unsigned int SrcRoleID, unsigned int TransID, unsigned int uItemID, int nItemNum, 
								   char* szDestAccount, unsigned int DestRoleID, char* szNote, 
								   CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc)
: CMethodObject(pConnection, DataProc), m_SrcAccount(szSrcAccount), m_SrcUserId(SrcPId), m_Session(pSession), m_SrcRoleID(SrcRoleID), m_ItemID(uItemID), m_ItemNum(nItemNum), m_DestAccount(szDestAccount),
 m_DestRoleID(DestRoleID), m_Note(szNote), m_TransID(TransID) 
{

}

CStoreItemObject::~CStoreItemObject()
{

}

int CStoreItemObject::call()
{
	int nRet = StoreItem();
	if (nRet != 0)
	{
		//写入Log
	}
	return 0;
}

int CStoreItemObject::StoreItem()
{
	int nRet = 0;
	
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_StoreItem('%s', %u, %u, %u, '%s', %u, '%s', %u)", m_SrcAccount.c_str(), m_SrcRoleID, m_ItemID, m_ItemNum, m_DestAccount.c_str(), m_DestRoleID, 
		m_Note.c_str(), m_TransID);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);

	if (nRet != 0 || pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);	

	return nRet;
}

CQueryCachedItem::CQueryCachedItem(char *szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_Account(szAccount), m_UserId(PId), m_Session(pSession), m_RoleID(RoleID)
{
	char szBuf[MAX_NAME_SIZE];
	m_DbConn->EscapeString(szBuf, szAccount, strlen(szAccount));
	m_Account = szBuf;

}

CQueryCachedItem::~CQueryCachedItem()
{

}

int CQueryCachedItem::call()
{
	int nRet = 0;
	std::vector<ShopServer::TransItem> vectItems;
	nRet = QueryCachedItems(vectItems);
	ACE_Message_Block* pData = 0;
	ShopServer::SGQueryCachedItem* pFetchItem = 0;
	for (size_t nIndex = 0; nIndex < vectItems.size(); nIndex++)
	{
		//如果是新的一页，需要创建一个消息消息块
		if (!(nIndex % ITEMS_PER_PAGE))
		{
			ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
			ShopServer::SGQueryCachedItem sgFetchItem;
			ACE_OS::memset(&sgFetchItem, 0, sizeof(sgFetchItem));
			pFetchItem = &sgFetchItem;
		}
		//如果已经到达最大尺寸或者每页尺寸，就发送消息块，否则，就把道具信息放进原来的数据块中
		if (pFetchItem->ItemArrLen < ITEMS_PER_PAGE)
		{
			pFetchItem->ItemArr[pFetchItem->ItemArrLen] = vectItems[nIndex];
			pFetchItem->ItemArrLen++;
		}
		//组包发送消息
		if (pFetchItem->ItemArrLen == vectItems.size() || pFetchItem->ItemArrLen == ITEMS_PER_PAGE)
		{
			ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
			pMsgHeader->PackCmd = ShopServer::SGQueryCachedItem::wCmd;
			pMsgHeader->PackType = 0;
			pFetchItem->AccountLen = (BYTE)(m_Account.length() + 1);
			ACE_OS::strncpy(pFetchItem->Account, m_Account.c_str(), m_Account.length());
			pFetchItem->RoleID = m_RoleID;
			pFetchItem->UserId = m_UserId;
			pFetchItem->PageIndex = 0;

			ShopServer::SGQueryCachedItem* pSGFetchFailedItem = reinterpret_cast<ShopServer::SGQueryCachedItem*>(pData->base() + HEADERSIZE);
			size_t nEncodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, pFetchItem, (char*)pSGFetchFailedItem, sizeof(ShopServer::SGQueryCachedItem));
			if (nEncodeLen == (size_t)-1)
			{
				pData->release();
				return -1;
			}
			pMsgHeader->PackLen = (USHORT)(HEADERSIZE + nEncodeLen);

			pData->wr_ptr(pMsgHeader->PackLen);
			static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);
			m_Session->putq(pData);
			m_Session->notifysend();
		}
	}
	return 0;
}

int CQueryCachedItem::QueryCachedItems(std::vector<ShopServer::TransItem>& vectItems)
{
	int nRet = 0;
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_QueryCachedItems('%s', %u)", m_Account.c_str(), m_RoleID);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	if (nRet == 0)
	{
		while (pResult->Next())
		{
			ShopServer::TransItem Item;
			Item.ItemID = pResult->GetInt(0);
			Item.ItemNum = pResult->GetInt(1);
			Item.TransID = pResult->GetInt(2);
			Item.OpeDate = pResult->GetDate(3);
			vectItems.push_back(Item);
		}
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);		
	return nRet;
}
CFetchCachedItem::CFetchCachedItem(char *szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int TranID, unsigned int ItemID, unsigned int ItemNum, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_Account(szAccount), m_UserId(PId), m_Session(pSession), m_RoleID(RoleID), m_TranID(TranID), m_ItemID(ItemID), m_ItemNum(ItemNum)
{
	char szBuf[MAX_NAME_SIZE];
	m_DbConn->EscapeString(szBuf, szAccount, strlen(szAccount));
	m_Account = szBuf;

}

CFetchCachedItem::~CFetchCachedItem()
{

}

int CFetchCachedItem::call()
{
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGFetchCachedItem::wCmd;
	pMsgHeader->PackType = 0;

	ShopServer::SGFetchCachedItem sgFetchCachedItem;
	ACE_OS::memset(&sgFetchCachedItem, 0, sizeof(sgFetchCachedItem));
	sgFetchCachedItem.AccountLen = (BYTE)(m_Account.length() + 1);
	ACE_OS::strncpy(sgFetchCachedItem.Account, m_Account.c_str(), m_Account.length());
	sgFetchCachedItem.UserId = m_UserId;
	sgFetchCachedItem.RoleID = m_RoleID;
	sgFetchCachedItem.CachedItem.TransID = m_TranID;
	sgFetchCachedItem.CachedItem.ItemID = m_ItemID;
	sgFetchCachedItem.CachedItem.ItemNum = m_ItemNum;

	int nRet = FetchCachedItem();

	sgFetchCachedItem.Result = nRet;

	ShopServer::SGFetchCachedItem* pSGFetchCachedItem = reinterpret_cast<ShopServer::SGFetchCachedItem*>(pData->base() + HEADERSIZE);
	size_t nEncodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgFetchCachedItem, (char*)pSGFetchCachedItem, sizeof(sgFetchCachedItem));
	if (nEncodeLen == (size_t)-1)
	{
		pData->release();
		return -1;
	}

	pMsgHeader->PackLen = (USHORT)(nEncodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);
	m_Session->putq(pData);
	m_Session->notifysend();	
	return 0;
}

int CFetchCachedItem::FetchCachedItem()
{
	int nRet = 0;
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_FetchCachedItem('%s', %u, %u)", m_Account.c_str(), m_RoleID, m_TranID);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);

	if (nRet != 0 || pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);	
	return nRet;
}

CPlayerNotifyObject::CPlayerNotifyObject(char *szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, char* szRoleName, unsigned int RaceID, unsigned int ClassID, 
										 unsigned int Level, unsigned char NofityType, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_Account(szAccount), m_UserId(PId), m_Session(pSession), m_RoleID(RoleID), m_RoleName(szRoleName), m_RaceID(RaceID) 
	, m_ClassID(ClassID), m_Level(Level),m_NotifyType(NofityType) 
{

}

CPlayerNotifyObject::~CPlayerNotifyObject()
{

}

int CPlayerNotifyObject::call()
{
	//上线
	if (m_NotifyType == 0)
	{
		PLAYERINFO playerInfo;
		playerInfo.m_UserId = m_UserId;
		playerInfo.m_Account = m_Account;
		playerInfo.m_RoleID = m_RoleID;
		playerInfo.m_ClassID = m_ClassID;
		playerInfo.m_RaceID = m_RaceID;
		playerInfo.m_Level = m_Level;
		playerInfo.m_RoleName = m_RoleName;
		playerInfo.m_GuildNo = 0;
		m_DataProc.GetPlayerInfoMap()->bind(playerInfo.m_RoleID, playerInfo);
	}
	//下线
	if (m_NotifyType == 1)
	{
		int nRet = m_DataProc.GetPlayerInfoMap()->find(m_RoleID);
		if (nRet != -1)
		{
			m_DataProc.GetPlayerInfoMap()->unbind(m_RoleID);
		}
	}
	return 0;
}

CQryRankObject::CQryRankObject(ShopServer::S_PlayerID PId, unsigned int nRaceID, unsigned char nRankType, unsigned char nPageIndex, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_UserId(PId), m_Session(pSession), m_RaceID(nRaceID), m_RankType(nRankType), m_PageIndex(nPageIndex)
{

}

CQryRankObject::~CQryRankObject()
{

}
int CQryRankObject::call()
{
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGShopRank::wCmd;
	pMsgHeader->PackType = 0;

	ShopServer::SGShopRank sgShopRank;
	ACE_OS::memset(&sgShopRank, 0, sizeof(sgShopRank));
	sgShopRank.PageIndex = 0;
	sgShopRank.RaceID = m_RaceID;
	sgShopRank.RankType = m_RankType;
	sgShopRank.UserId = m_UserId;

	std::vector<ShopServer::RankInfo> vectRankInfo;
	bool bTailPage = IsTailPage();
	FetchRankInfo(vectRankInfo);
	if (bTailPage)
	{
		sgShopRank.PageIndex &= 0x80;
	}
	sgShopRank.PageIndex &= m_PageIndex;
	sgShopRank.RanksLen = (BYTE)vectRankInfo.size();
	for (size_t nIndex = 0; nIndex < vectRankInfo.size(); nIndex++)
	{
		sgShopRank.RankList[nIndex] = vectRankInfo[nIndex];
	}

	ShopServer::SGShopRank* pSGShopRank = reinterpret_cast<ShopServer::SGShopRank*>(pData->base() + HEADERSIZE);
	size_t nEncodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgShopRank, (char*)pSGShopRank, sizeof(sgShopRank));
	if (nEncodeLen == (size_t)-1)
	{
		pData->release();
		return -1;
	}
	pMsgHeader->PackLen = (USHORT)(nEncodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);

	m_Session->putq(pData);
	m_Session->notifysend();
	return 0;
}

int CQryRankObject::FetchRankInfo(std::vector<ShopServer::RankInfo>& vectRankInfo)
{
	int nRet = 0;
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_GetRankInfo(%u, %u, %u)", m_RaceID, m_RankType, m_PageIndex);

	//调用存储过程
	CResult* pResult = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);

	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	else
	{
		int nRankIndex = 0;
		while (pResult->Next())
		{
			nRankIndex++;
			ShopServer::RankInfo rankInfo;
			rankInfo.RankIndex = nRankIndex;
			ACE_OS::strcpy(rankInfo.RoleName, pResult->GetString(0));
			rankInfo.RoleNameLen = ACE_OS::strlen(rankInfo.RoleName);
			rankInfo.Level = pResult->GetInt(2);
			rankInfo.RaceID = pResult->GetInt(1);
			rankInfo.ClassID = pResult->GetInt(3);
			rankInfo.Cost = pResult->GetInt(4);
			vectRankInfo.push_back(rankInfo);
		}
	}

	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);			
	return 0;
}

bool CQryRankObject::IsTailPage()
{
	int nRet = 0;
	CQuery* pQuery = m_DbConn->CreateQuery();
	if (pQuery == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
		return nRet;
	}

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"CALL sp_GetRankCount(%u, %u, @RankCount)", m_RaceID, m_RankType);

	//调用存储过程
	CResult* pResult = 0;
	int nRankCount = 0;
	nRet = pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);

	if (nRet != 0 || pResult == 0)
	{
		nRet = ShopServer::SHOP_SYSERROR;
	}
	else
	{
		//获得输出参数
		iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), "SELECT @RankCount");
		pQuery->ExecuteSelect(szStatement, iStatementLen, pResult);
		if (nRet != 0 || pResult == 0)
		{
			nRet = ShopServer::SHOP_SYSERROR;
		}
		else
		{
			pResult->Next();
			nRankCount = pResult->GetInt(0);
		}
	}
	if (pResult != 0)
	{
		pQuery->FreeResult(pResult);
	}
	m_DbConn->DestroyQuery(pQuery);	

	
	return !(nRankCount > (m_PageIndex + 1) * 10);
}

CSessionCloseObject::CSessionCloseObject(CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_Session(pSession)
{
	
}

CSessionCloseObject::~CSessionCloseObject()
{

}

int CSessionCloseObject::call()
{
	//在处理这个消息时，基于这个消息是最后一个消息，所以需要向连接队列发送一个消息，表示可以删除handler了
	ACE_Message_Block* pMsg = 0;
	ACE_NEW_RETURN(pMsg, ACE_Message_Block(MAX_PACK_SIZE / 2, ACE_Message_Block::MB_STOP), -1);
	m_Session->putq(pMsg);
	return 0;
}

int CShopExistObject::call()
{
	return -1;
}

CQryRoleInfoOject::CQryRoleInfoOject(CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc, std::string strRoleName)
: CMethodObject(pConnection, DataProc), m_Session(pSession), m_RoleName(strRoleName)
{

}

int CQryRoleInfoOject::call()
{
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, CDataBlock(MAX_PACK_SIZE / 2), -1);
	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pData->base());
	pMsgHeader->PackCmd = ShopServer::SGQryRoleInfo::wCmd;
	pMsgHeader->PackType = 0;

	ShopServer::SGQryRoleInfo sgQryRoleInf;
	ACE_OS::memset(&sgQryRoleInf, 0, sizeof(sgQryRoleInf));
	sgQryRoleInf.RoleNameLen = (BYTE)(m_RoleName.length() + 1);
	ACE_OS::strncpy(sgQryRoleInf.RoleName, m_RoleName.c_str(), m_RoleName.length());

	ShopServer::SGQryRoleInfo* pSGQryRoleInfo = reinterpret_cast<ShopServer::SGQryRoleInfo*>(pData->base() + HEADERSIZE);
	size_t nEncodeLen = m_DataProc.GetProtocol()->EnCode(pMsgHeader->PackCmd, &sgQryRoleInf, (char*)pSGQryRoleInfo, sizeof(sgQryRoleInf));
	if (nEncodeLen == (size_t)-1)
	{
		pData->release();
		return -1;
	}

	pMsgHeader->PackLen = (USHORT)(nEncodeLen + HEADERSIZE);
	pData->wr_ptr(pMsgHeader->PackLen);
	static_cast<CDataBlock*>(pData)->DataSize(pMsgHeader->PackLen);
	m_Session->putq(pData);
	m_Session->notifysend();	
	return 0;
}

CQryRoleInfoOject::~CQryRoleInfoOject()
{

}


CPlayerGuildInfo::CPlayerGuildInfo(ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int GuildNo, CConnection *pConnection, CSessionHandler *pSession, CDataProc &DataProc)
: CMethodObject(pConnection, DataProc), m_UserId(PId), m_Session(pSession), m_RoleID(RoleID), m_GuildNo(GuildNo)
{

}

CPlayerGuildInfo::~CPlayerGuildInfo()
{

}

int CPlayerGuildInfo::call()
{
	//PLAYERINFO playerInfo;
	MAP_PLAYERINFO::iterator Ite(*(m_DataProc.GetPlayerInfoMap()));
	m_DataProc.GetPlayerInfoMap()->find(m_RoleID, Ite);
	if (Ite != m_DataProc.GetPlayerInfoMap()->end())
	{
		Ite->item().m_GuildNo = m_GuildNo;
	}
	else
	{

	}

	return 0;
}




