﻿#include "SessionHandler.h"
#include "ProcTask.h"
#include "DataBlock.h"

//设置tcp接收和发送缓冲区长度
#define NETBUFLEN 81920
#define BUF_SIZE 8192

CSessionHandler::CSessionHandler(void)
: m_Connected(false)
{

}

CSessionHandler::~CSessionHandler(void)
{

}

int CSessionHandler::open(void *)
{
	this->msg_queue()->high_water_mark(MAX_QUE_SIZE);
	this->msg_queue()->low_water_mark(MAX_QUE_SIZE);

	//设为非阻赛模式
	m_Connected = true;
	this->peer().enable(ACE_NONBLOCK);
	int nBufLen = NETBUFLEN;
	//设置发送缓冲区
	this->peer().set_option(SOL_SOCKET, SO_SNDBUF, &nBufLen, sizeof(nBufLen));
	//设置接收缓冲区
	this->peer().set_option(SOL_SOCKET, SO_RCVBUF, &nBufLen, sizeof(nBufLen));
	//在处理网络数据前，向处理模块发送连接建立消息
	CProcTaskInstance::instance()->OnConnected(this);
	//reactor注册handler
	this->reactor()->register_handler(this, ACE_Event_Handler::READ_MASK | ACE_Event_Handler::WRITE_MASK);
	return 0;
}

//数据到来事件通知
int CSessionHandler::handle_input(ACE_HANDLE)
{
	//创建数据块
	ACE_Message_Block* pMsg = 0;
	ACE_NEW_RETURN(pMsg, ACE_Message_Block(BUF_SIZE), -1);
	//读取消息
	int nRecvLen = peer().recv(pMsg->wr_ptr(), BUF_SIZE);
	//对端发送FIN
	if (nRecvLen == 0)
	{
		//连接正常关闭
		pMsg->release();
		m_Connected = false;
		CProcTaskInstance::instance()->OnConnectionClose(this, GRACE_CLOSE);
		return -1;
	}
	if (nRecvLen == -1)
	{
		//异常（包括连接异常关闭等）
		if (errno != EWOULDBLOCK)
		{
			//ACE_DEBUG()
			ACE_OS::printf("%d\n", errno);
			pMsg->release();
			m_Connected = false;
			CProcTaskInstance::instance()->OnConnectionClose(this, ABORTIVE_CLOSE);
			return -1;
		}
	}
	//把数据传给应用层
	pMsg->wr_ptr(nRecvLen);
	CProcTaskInstance::instance()->OnDataRecv(this, pMsg);
	return 0;
}

int CSessionHandler::handle_close(ACE_HANDLE fd, ACE_Reactor_Mask mask)
{
	if (mask == ACE_Event_Handler::WRITE_MASK)
	{
		return 0;
	}
	
	ACE_Message_Block* pData = 0;
	ACE_Time_Value tv(ACE_OS::gettimeofday());
	while (true)
	{
		if (-1 != getq(pData, &tv))
		{
			if (pData->msg_type() == ACE_Message_Block::MB_STOP)
			{
				break;
			}
			else
			{
				
			}
		}
		//投递到错误队列进行处理
	}
	return parent::handle_close(fd, mask);
}

int CSessionHandler::handle_output(ACE_HANDLE /*fd*/)
{
	ACE_Time_Value tv(ACE_OS::gettimeofday());
	ACE_Message_Block* pData = 0;
	ACE_DEBUG((LM_INFO, ACE_TEXT("handle output...\n")));
	while (-1 != getq(pData, &tv))
	{
		CDataBlock* pDataBlock = static_cast<CDataBlock*>(pData);
		int nRet = this->peer().send(pData->base(), pDataBlock->length());
		if (nRet == -1)
		{
			ungetq(pData);
			return -1;
		}
		else
		{
			pDataBlock->CurSize(nRet);
			//数据发送完整才进行释放，否则，仍然放在队列中
			if (pDataBlock->IsComplete())
			{
				pDataBlock->release();
			}
			else
			{
				pDataBlock->rd_ptr(nRet);
				ungetq(pData);
				break;
			}
		}
	}

	if (this->msg_queue()->is_empty())
	{
		this->reactor()->cancel_wakeup(this, ACE_Event_Handler::WRITE_MASK);
	}

	return 0;
}

int CSessionHandler::notifysend()
{
	//发送队列中有消息，通知进行output事件的检测

	if (m_Connected)
	{
		this->reactor()->mask_ops(this, ACE_Event_Handler::WRITE_MASK, ACE_Reactor::ADD_MASK);
	}
	return 0;
}

