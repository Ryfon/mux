﻿/** @file DbModule.h 
@brief 数据库模块主控类
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-03-26
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef DBMODULE_H
#define DBMODULE_H
#include "Common.h"
#include "MysqlWrapper.h"

class CDbModule: public ACE_Task<ACE_MT_SYNCH>
{
	enum {MAX_DB_THREAD_NUM = 1};
public:
	CDbModule(void);
	virtual ~CDbModule(void);
public:
	virtual int init(int argc, ACE_TCHAR* argv);
	virtual int fini(void);
	virtual int info(ACE_TCHAR **info_string, size_t length /* = 0 */);
public:
	int PushTask(ACE_Method_Object*);
	CConnection* GetDbConn()
	{
		return m_DbConn;
	}
public:
	virtual int open(void *args  = 0 );
	virtual int svc();
private:
	int LoadLocalData();
private:
	stConnectionString m_DbConnString;
	ACE_Activation_Queue m_TaskQue;
	CConnection* m_DbConn;
};

typedef ACE_Singleton<CDbModule, ACE_Null_Mutex> CDbModuleInstance;

#endif
