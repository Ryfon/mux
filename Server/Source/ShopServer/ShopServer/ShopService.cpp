﻿#include "ShopService.h"
#include "NetModule.h"
#include "DbModule.h"
#include "ProcTask.h"

CShopService::CShopService(void)
: m_SelectReactor(0)
{

}

CShopService::~CShopService(void)
{

}

int CShopService::init(int argc, ACE_TCHAR *argv)
{
	ACE_DEBUG((LM_INFO, ACE_TEXT("ShopServer starting.....\n")));

	//设置Reactor的实现为ACE_Select_Reactor
	m_SelectReactor = new ACE_Reactor(&m_Reactor_Imp, 0);
	ACE_Reactor::instance(m_SelectReactor, 0);

	ACE_Sig_Set sig_set;
	sig_set.sig_add(SIGINT);
	sig_set.sig_add(SIGQUIT);	
	//注册信号处理
	if (ACE_Reactor::instance()->register_handler(sig_set, this) == -1)
	{
		ACE_ERROR_RETURN((LM_ERROR, ACE_TEXT("(%t) %p\n"), ACE_TEXT("register_handler")), -1);
	}

	ACE_DEBUG((LM_INFO, ACE_TEXT("reading config info......\n")));

	ACE_DEBUG((LM_INFO, ACE_TEXT("connecting to database......\n")));
	//启动数据库模块
	if (CDbModuleInstance::instance()->init(argc, argv) == -1)
	{
		ACE_ERROR_RETURN((LM_ERROR, ACE_TEXT("(%t) %p\n"), ACE_TEXT("CDbModuleInstance::instance()->init")), -1);
	}

	CDbModuleInstance::instance()->open();
	//给业务逻辑模块设置数据库模块
	
	//启动业务逻辑模块
	ACE_DEBUG((LM_INFO, ACE_TEXT("start logic module.....\n")));
	CProcTaskInstance::instance()->open();
	//启动网络模块
	ACE_DEBUG((LM_INFO, ACE_TEXT("start net module......\n")));
	if (CNetModuleInstance::instance()->init(argc, argv) == -1)
	{
		ACE_ERROR_RETURN((LM_ERROR, ACE_TEXT("(%t) %p\n"), ACE_TEXT("netmodule init")), -1);
	}
	
	ACE_DEBUG((LM_INFO, ACE_TEXT("Shop running...\n")));
	ACE_Reactor::instance()->run_event_loop();
	return 0;
}

int CShopService::fini()
{
	//通知网络模块不再接受新建立的连接
	CNetModuleInstance::instance()->fini();
	//向线程发送退出消息
	CProcTaskInstance::instance()->fini();
	//等待线程退出
	ACE_Thread_Manager::instance()->wait();	

	if (m_SelectReactor)
	{
		delete m_SelectReactor;
		m_SelectReactor = 0;
	}

	ACE_DEBUG((LM_INFO, ACE_TEXT("Shop quit...\n")));
	return 0;
}

int CShopService::info(ACE_TCHAR **info_string, size_t length)
{
	ACE_UNUSED_ARG(info_string);
	ACE_UNUSED_ARG(length);
	return 0;
}

int CShopService::handle_input(ACE_HANDLE fd )
{
	ACE_UNUSED_ARG(fd);
	return 0;
}

int CShopService::handle_signal(int signum, siginfo_t * , ucontext_t * )
{
	ACE_UNUSED_ARG(signum);
	ACE_Reactor::instance()->end_event_loop();
	return -1;
}
