﻿/** @file DbModule.h 
@brief 网络模块主控类
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-03-26
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef NETMODULE_H
#define NETMODULE_H
#include "Common.h"
#include "Acceptor.h"

class CNetModule: public ACE_Service_Object
{
	friend class CSessionHandler;
public:
	CNetModule(void);
	virtual ~CNetModule(void);
public:
	virtual int init(int argc, ACE_TCHAR* argv);
	virtual int fini(void);
	virtual int info(ACE_TCHAR **info_string, size_t length /* = 0 */);
private:
	CAcceptor m_Acceptor;
};

typedef ACE_Singleton<CNetModule, ACE_Null_Mutex> CNetModuleInstance;
#endif
