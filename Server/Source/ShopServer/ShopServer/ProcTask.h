﻿/** @file ProcTask.h 
@brief 处理消息线程池
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-03-27
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef PROCTASK_H
#define PROCTASK_H
#include "Common.h"
#include "DataProc.h"

class CSessionHandler;
typedef ACE_Hash_Map_Manager<ACE_HANDLE, ACE_Message_Block*, ACE_Null_Mutex> SESSION_PARTDATA;

class CProcTask: public ACE_Task<ACE_MT_SYNCH>
{
	enum {MAX_WORKER_THREAD_NUM = 1};
public:
	typedef ACE_Task<ACE_MT_SYNCH> parent;
	CProcTask(void);
	virtual ~CProcTask(void);
public:
	virtual int init(int argc, ACE_TCHAR* argv[]);
	virtual int open(void *args  = 0 );
	//给外部提供接口发出退出请求
	virtual int fini();
	virtual int wait();
	virtual int ProcMsg(ACE_Message_Block* msg);
	virtual int svc();
public:
	//连接到来
	virtual int OnConnected(CSessionHandler*);
	//连接数据到来
	virtual int OnDataRecv(CSessionHandler*, ACE_Message_Block*);
	//连接被关闭
	virtual int OnConnectionClose(CSessionHandler*, int nCloseType);
private:
	SESSION_PARTDATA m_SessionPartData; 
	ACE_Token m_Session_Lock;
	CDataProc* m_DataProc;
private:
	int ProcPartMsg(CSessionHandler*, ACE_Message_Block*);
	
};

typedef ACE_Singleton<CProcTask, ACE_Null_Mutex> CProcTaskInstance;
#endif
