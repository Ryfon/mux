﻿#ifndef			__ShopSrvProtocol__
#define			__ShopSrvProtocol__
#include "ParserTool.h"
#include "map"
#pragma pack(push, 1)
namespace ShopServer
{
	//Enums
	typedef	enum
	{
		PUR_GAMEMONEY       	=	0,            	//星钻方式购买
		PUR_SCORE           	=	1,            	//积分方式购买
	}PurchaseCode;

	typedef	enum
	{
		PLAYERNOTIFY_ONLINE 	=	0,            	//上线
		PLAYERNOTIFY_OFFLINE	=	1,            	//下线
	}PlayerNotifyType;

	typedef	enum
	{
		PUR_OK              	=	0,            	//购买成功
		PUR_MONEYNOTENOUGH  	=	10050,        	//余额不足
		PUR_NOTSUPPORTTYPE  	=	10001,        	//不支持此种购买方式
		PUR_VIP_EXISTS      	=	10002,        	//存在未用完的Vip方案
		FETCH_VIPITEM_DONE  	=	10003,        	//当天的Vip道具包已经领取过
		PUR_GIFT_ROLEINVALID	=	10004,        	//赠送的角色无效
		SHOP_SYSERROR       	=	10999,        	//系统错误
	}ShopOpCode;


	//Defines
	#define	MAX_NAME_SIZE 	 	20 	//最大账号长度
	#define	ITEMS_PER_PAGE	 	16 	//每页道具数
	#define	PER_PAGE_RANKS	 	10 	//每页排行数
	#define	MAX_NOTE_SIZE 	 	100	//备注最大程度

	//Typedefs

	//Types
	template<bool> struct TypeMaxBytes;
	template<> struct TypeMaxBytes<true> {};
	#define	SHOPSVRPROTOCOL_MAXBYTES_CHECK(typename) \
	struct type##typename \
	{\
		TypeMaxBytes< sizeof(typename) < 507 > struct_larger_##typename;\
	}

	struct S_PlayerID
	{
		USHORT    	wGID;                    	//
		UINT      	dwPID;                   	//
	};

	struct MsgHeader
	{
		USHORT    	PackLen;                 	//消息包长度（包含消息头在内）
		BYTE      	PackType;                	//包类型
		USHORT    	PackCmd;                 	//消息命令字
	};

	struct GSQueryAccountInfo
	{
		static const USHORT	wCmd = 0x1801;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSQueryAccountInfo);

	struct SGAccountInfo
	{
		static const USHORT	wCmd = 0x8101;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	GameMoney;               	//奇迹星钻余额
		UINT      	Score;                   	//积分余额
		BYTE      	MemberClass;             	//会员等级
		BYTE      	MemberLevel;             	//会员级别
		UINT      	MemberExp;               	//会员经验
		FLOAT     	PurchaseDisc;            	//折扣率
		USHORT    	Result;                  	//0 表示查询成功 其他表示错误码
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGAccountInfo);

	struct GSPurchaseItem
	{
		static const USHORT	wCmd = 0x1802;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RoleID;                  	//角色ID
		BYTE      	PurchaseType;            	//购买方式 0 星钻 1 积分
		UINT      	ItemID;                  	//道具类型ID
		UINT      	ItemNum;                 	//道具数目
		UINT      	CliCalc;                 	//客户端计算的金额数目
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSPurchaseItem);

	struct SGPurchaseItem
	{
		static const USHORT	wCmd = 0x8102;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	TransID;                 	//交易流水号
		UINT      	RoleID;                  	//角色ID
		UINT      	ItemID;                  	//道具类型ID
		UINT      	ItemNum;                 	//道具数目
		UINT      	GameMoney;               	//奇迹星钻余额
		UINT      	Score;                   	//积分余额
		BYTE      	PurchaseType;            	//购买方式 0 星钻 1 积分
		UINT      	ComsumeNum;              	//消费金额
		BYTE      	MemberClass;             	//会员等级
		BYTE      	MemberLevel;             	//会员级别
		UINT      	MemberExp;               	//会员经验
		FLOAT     	PurchaseDisc;            	//折扣率
		USHORT    	PurchaseResult;          	//购买结果，0表示成功 其他表示错误码
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGPurchaseItem);

	struct GSQueryVipInfo
	{
		static const USHORT	wCmd = 0x1803;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RoleID;                  	//角色
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSQueryVipInfo);

	struct SGVipInfo
	{
		static const USHORT	wCmd = 0x8103;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RoleID;                  	//角色
		UINT      	VipID;                   	//vip方案号
		USHORT    	LeftDays;                	//剩余天数
		BYTE      	Fetched;                 	//当日道具是否已领取 0 未领取 1 已领取
		USHORT    	Result;                  	//0 表示查询成功 其他表示错误码
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGVipInfo);

	struct GSPurchaseVip
	{
		static const USHORT	wCmd = 0x1804;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RoleID;                  	//角色ID
		UINT      	VipID;                   	//vip方案号
		UINT      	CliCalc;                 	//客户端计算的金额数目
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSPurchaseVip);

	struct SGPurchaseVip
	{
		static const USHORT	wCmd = 0x8104;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	TransID;                 	//交易流水号
		UINT      	RoleID;                  	//角色ID
		UINT      	VipID;                   	//vip方案号
		UINT      	GameMoney;               	//奇迹星钻余额
		UINT      	Score;                   	//积分余额
		UINT      	ComsumeNum;              	//消费金额
		BYTE      	MemberClass;             	//会员等级
		BYTE      	MemberLevel;             	//会员级别
		UINT      	MemberExp;               	//会员经验
		FLOAT     	PurchaseDisc;            	//折扣率
		USHORT    	PurchaseResult;          	//购买结果，0表示成功 其他表示错误码
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGPurchaseVip);

	struct GSFetchVipItem
	{
		static const USHORT	wCmd = 0x1805;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RoleID;                  	//角色ID
		UINT      	VipID;                   	//Vip方案号
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSFetchVipItem);

	struct VipItem
	{
		UINT      	ItemTypeID;              	//道具类型ID
		BYTE      	ItemNum;                 	//道具数目
	};

	struct SGFetchVipItem
	{
		static const USHORT	wCmd = 0x8105;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RoleID;                  	//角色ID
		UINT      	VipID;                   	//vip方案号
		BYTE      	VipItemLen;              	//道具包长度
		VipItem   	VipItemArr[ITEMS_PER_PAGE];	//道具包数组
		USHORT    	Result;                  	//0 表示查询成功 其他表示错误码
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGFetchVipItem);

	struct GSFetchVipItemResult
	{
		static const USHORT	wCmd = 0x1806;

		BYTE      	AccountLen;              	//帐号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RoleID;                  	//角色ID
		UINT      	VipID;                   	//vip方案号
		USHORT    	Result;                  	//0 表示成功 其他表示错误码
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSFetchVipItemResult);

	struct GSPurchaseGift
	{
		static const USHORT	wCmd = 0x1807;

		BYTE      	SrcAccountLen;           	//账号长度
		CHAR      	SrcAccount[MAX_NAME_SIZE]; 	//玩家账号
		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	SrcRoleID;               	//角色ID
		BYTE      	PurchaseType;            	//购买类型 0 星钻 1 积分
		UINT      	ItemID;                  	//道具类型ID
		UINT      	ItemNum;                 	//道具数目
		BYTE      	DestAccountLen;          	//接受方玩家账号长度
		CHAR      	DestAccount[MAX_NAME_SIZE];	//接受方玩家账号
		UINT      	DestRoleID;              	//接受方玩家角色
		UINT      	CliCalc;                 	//客户端计算的金额数目
		BYTE      	NoteLen;                 	//备注长度
		CHAR      	Note[MAX_NOTE_SIZE];       	//留言
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSPurchaseGift);

	struct SGPurchaseGift
	{
		static const USHORT	wCmd = 0x8107;

		UINT      	TransID;                 	//交易流水号
		BYTE      	SrcAccountLen;           	//账号长度
		CHAR      	SrcAccount[MAX_NAME_SIZE]; 	//玩家账号
		UINT      	SrcRoleID;               	//角色ID
		S_PlayerID	UserId;                  	//玩家ID
		UINT      	ItemID;                  	//道具类型ID
		UINT      	ItemNum;                 	//道具数目
		BYTE      	DestAccountLen;          	//接受方玩家账号长度
		CHAR      	DestAccount[MAX_NAME_SIZE];	//接受方玩家账号
		UINT      	DestRoleID;              	//接受方玩家角色
		UINT      	GameMoney;               	//奇迹星钻余额
		UINT      	Score;                   	//积分余额
		BYTE      	PurchaseType;            	//购买类型 0 星钻 1 积分
		UINT      	MoneyCost;               	//消费星钻
		UINT      	SocreCost;               	//消费积分
		BYTE      	MemberClass;             	//会员等级
		BYTE      	MemberLevel;             	//会员级别
		UINT      	MemberExp;               	//会员经验
		FLOAT     	PurchaseDisc;            	//折扣率
		USHORT    	PurchaseResult;          	//购买结果，0表示成功 其他表示错误码
		BYTE      	NoteLen;                 	//备注长度
		CHAR      	Note[MAX_NOTE_SIZE];       	//留言
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGPurchaseGift);

	struct SGQryRoleInfo
	{
		static const USHORT	wCmd = 0x8112;

		BYTE      	RoleNameLen;             	//角色名长度
		CHAR      	RoleName[MAX_NAME_SIZE];   	//角色名
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGQryRoleInfo);

	struct GSQryRoleInfo
	{
		static const USHORT	wCmd = 0x1812;

		BYTE      	RoleNameLen;             	//角色名长度
		CHAR      	RoleName[MAX_NAME_SIZE];   	//角色名
		BYTE      	RoleStatus;              	//角色状态 0:存在 1:不存在
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSQryRoleInfo);

	struct GSTranFailed
	{
		static const USHORT	wCmd = 0x1808;

		S_PlayerID	UserId;                  	//玩家ID
		UINT      	TransID;                 	//交易流水号
		BYTE      	AccountLen;              	//账号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//玩家账号
		UINT      	RoleID;                  	//角色ID
		UINT      	ItemID;                  	//道具类型ID
		UINT      	ItemNum;                 	//道具数目
		BYTE      	DestAccountLen;          	//接收道具玩家账号长度
		CHAR      	DestAccount[MAX_NAME_SIZE];	//接收道具玩家账号
		UINT      	DestRoleID;              	//接收道具玩家角色
		BYTE      	NoteLen;                 	//备注长度
		CHAR      	Note[MAX_NOTE_SIZE];       	//留言
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSTranFailed);

	struct GSQueryCachedItem
	{
		static const USHORT	wCmd = 0x1809;

		S_PlayerID	UseId;                   	//
		BYTE      	AccountLen;              	//账号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//玩家账号
		UINT      	RoleID;                  	//角色ID
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSQueryCachedItem);

	struct TransItem
	{
		UINT      	TransID;                 	//交易流水号
		UINT      	ItemID;                  	//道具ID
		UINT      	ItemNum;                 	//道具数量
		UINT      	OpeDate;                 	//操作时间
	};

	struct SGQueryCachedItem
	{
		static const USHORT	wCmd = 0x8109;

		S_PlayerID	UserId;                  	//
		BYTE      	AccountLen;              	//账号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//玩家账号
		UINT      	RoleID;                  	//角色ID
		BYTE      	PageIndex;               	//道具页索引
		BYTE      	ItemArrLen;              	//道具数组长度
		TransItem 	ItemArr[ITEMS_PER_PAGE];   	//道具数组列表
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGQueryCachedItem);

	struct GSFetchCachedItem
	{
		static const USHORT	wCmd = 0x1810;

		S_PlayerID	UserId;                  	//
		BYTE      	AccountLen;              	//账号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//玩家账号
		UINT      	RoleID;                  	//角色ID
		TransItem 	CachedItem;              	//道具
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSFetchCachedItem);

	struct SGFetchCachedItem
	{
		static const USHORT	wCmd = 0x8110;

		S_PlayerID	UserId;                  	//
		BYTE      	AccountLen;              	//账号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//玩家账号
		UINT      	RoleID;                  	//角色ID
		TransItem 	CachedItem;              	//道具
		USHORT    	Result;                  	//领取结果
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGFetchCachedItem);

	struct GSPlayerNotify
	{
		static const USHORT	wCmd = 0x1811;

		S_PlayerID	UserId;                  	//
		BYTE      	AccountLen;              	//账号长度
		CHAR      	Account[MAX_NAME_SIZE];    	//玩家账号
		UINT      	RoleID;                  	//角色ID
		BYTE      	RoleNameLen;             	//角色名长度
		CHAR      	RoleName[MAX_NAME_SIZE];   	//角色名
		UINT      	RaceID;                  	//种族ID
		UINT      	ClassID;                 	//职业ID
		UINT      	Level;                   	//等级
		BYTE      	NotifyType;              	//通知类型 0 上线通知 1 下线通知
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSPlayerNotify);

	struct GSShopRank
	{
		static const USHORT	wCmd = 0x1813;

		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RaceID;                  	//种族ID 4为全部
		BYTE      	RankType;                	//排行类型 0 按周 1 按月
		BYTE      	PageIndex;               	//查询第几页
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSShopRank);

	struct RankInfo
	{
		USHORT    	RankIndex;               	//名次
		BYTE      	RoleNameLen;             	//角色名长度
		CHAR      	RoleName[MAX_NAME_SIZE];   	//角色名
		UINT      	Level;                   	//等级
		UINT      	RaceID;                  	//种族ID
		UINT      	ClassID;                 	//职业ID
		UINT      	Cost;                    	//消费额
	};

	struct SGShopRank
	{
		static const USHORT	wCmd = 0x8113;

		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RaceID;                  	//种族ID 4为全部
		BYTE      	RankType;                	//排行类型 0 按周 1 按月
		BYTE      	PageIndex;               	//第一个包是页数，0表示最后一页
		BYTE      	RanksLen;                	//当页排行数目
		RankInfo  	RankList[PER_PAGE_RANKS];  	//当页排行列表
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGShopRank);

	struct GSPlayerGuildInfo
	{
		static const USHORT	wCmd = 0x1814;

		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RoleID;                  	//角色ID
		UINT      	GuildNo;                 	//战盟编号 0表示无战盟
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(GSPlayerGuildInfo);

	struct SGGuildPrestigeAdd
	{
		static const USHORT	wCmd = 0x8114;

		S_PlayerID	UserId;                  	//玩家PlayerID
		UINT      	RoleID;                  	//角色ID
		UINT      	PrestigeAdd;             	//增加的战盟威望
	};
	SHOPSVRPROTOCOL_MAXBYTES_CHECK(SGGuildPrestigeAdd);


	//Messages
	typedef enum Message_id_type
	{
		G_S_QUERYACCOUNTINFO  	=	0x1801,	//GSQueryAccountInfo
		S_G_ACCOUNTINFO       	=	0x8101,	//SGAccountInfo
		G_S_PURCHASEITEM      	=	0x1802,	//GSPurchaseItem
		S_G_PURCHASEITEM      	=	0x8102,	//SGPurchaseItem
		G_S_VIPNIFO           	=	0x1803,	//GSQueryVipInfo
		S_G_VIPNIFO           	=	0x8103,	//SGVipInfo
		G_S_PURCHASEVIP       	=	0x1804,	//GSPurchaseVip
		S_G_PURCHASEVIP       	=	0x8104,	//SGPurchaseVip
		G_S_FETCHVIPITEM      	=	0x1805,	//GSFetchVipItem
		S_G_FETCHVIPITEM      	=	0x8105,	//SGFetchVipItem
		G_S_FETCHVIPITEMRESULT	=	0x1806,	//GSFetchVipItemResult
		G_S_PURCHASEGIFT      	=	0x1807,	//GSPurchaseGift
		S_G_PURCHASEGIFT      	=	0x8107,	//SGPurchaseGift
		G_S_TRANFAILED        	=	0x1808,	//GSTranFailed
		G_S_QUERYCACHEDITEM   	=	0x1809,	//GSQueryCachedItem
		S_G_QUERYCACHEDITEM   	=	0x8109,	//SGQueryCachedItem
		G_S_FETCHCACHEDITEM   	=	0x1810,	//GSFetchCachedItem
		S_G_FETCHCACHEDITEM   	=	0x8110,	//SGFetchCachedItem
		G_S_PLAYERNOTIFY      	=	0x1811,	//GSPlayerNotify
		S_G_QUERYROLEINFO     	=	0x8112,	//SGQryRoleInfo
		G_S_QUERYROLEINFO     	=	0x1812,	//GSQryRoleInfo
		G_S_QUERYRANKINFO     	=	0x1813,	//GSShopRank
		S_G_RANKINFO          	=	0x8113,	//SGShopRank
		G_S_PLAYERGUILDINFO   	=	0x1814,	//GSPlayerGuildInfo
		S_G_GUILD_PRESTIGE_ADD	=	0x8114,	//SGGuildPrestigeAdd
	};

	//Class Data
	class ShopSrvProtocol;
	typedef size_t (ShopSrvProtocol::*EnCodeFunc)(void* pData);
	typedef size_t (ShopSrvProtocol::*DeCodeFunc)(void* pData);

	class 
	#ifdef WIN32
	_declspec(dllexport) 
	#endif
	ShopSrvProtocol
	{
	public:
		ShopSrvProtocol();
		~ShopSrvProtocol();

		size_t	EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize);
		size_t	DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize);

	protected:
		EnCodeFunc	FindEnCodeFunc(int nMessageId);
		DeCodeFunc	FindDeCodeFunc(int nMessageId);

	protected:
		size_t	EnCode__S_PlayerID(void* pData);
		size_t	DeCode__S_PlayerID(void* pData);

		size_t	EnCode__MsgHeader(void* pData);
		size_t	DeCode__MsgHeader(void* pData);

		size_t	EnCode__GSQueryAccountInfo(void* pData);
		size_t	DeCode__GSQueryAccountInfo(void* pData);

		size_t	EnCode__SGAccountInfo(void* pData);
		size_t	DeCode__SGAccountInfo(void* pData);

		size_t	EnCode__GSPurchaseItem(void* pData);
		size_t	DeCode__GSPurchaseItem(void* pData);

		size_t	EnCode__SGPurchaseItem(void* pData);
		size_t	DeCode__SGPurchaseItem(void* pData);

		size_t	EnCode__GSQueryVipInfo(void* pData);
		size_t	DeCode__GSQueryVipInfo(void* pData);

		size_t	EnCode__SGVipInfo(void* pData);
		size_t	DeCode__SGVipInfo(void* pData);

		size_t	EnCode__GSPurchaseVip(void* pData);
		size_t	DeCode__GSPurchaseVip(void* pData);

		size_t	EnCode__SGPurchaseVip(void* pData);
		size_t	DeCode__SGPurchaseVip(void* pData);

		size_t	EnCode__GSFetchVipItem(void* pData);
		size_t	DeCode__GSFetchVipItem(void* pData);

		size_t	EnCode__VipItem(void* pData);
		size_t	DeCode__VipItem(void* pData);

		size_t	EnCode__SGFetchVipItem(void* pData);
		size_t	DeCode__SGFetchVipItem(void* pData);

		size_t	EnCode__GSFetchVipItemResult(void* pData);
		size_t	DeCode__GSFetchVipItemResult(void* pData);

		size_t	EnCode__GSPurchaseGift(void* pData);
		size_t	DeCode__GSPurchaseGift(void* pData);

		size_t	EnCode__SGPurchaseGift(void* pData);
		size_t	DeCode__SGPurchaseGift(void* pData);

		size_t	EnCode__SGQryRoleInfo(void* pData);
		size_t	DeCode__SGQryRoleInfo(void* pData);

		size_t	EnCode__GSQryRoleInfo(void* pData);
		size_t	DeCode__GSQryRoleInfo(void* pData);

		size_t	EnCode__GSTranFailed(void* pData);
		size_t	DeCode__GSTranFailed(void* pData);

		size_t	EnCode__GSQueryCachedItem(void* pData);
		size_t	DeCode__GSQueryCachedItem(void* pData);

		size_t	EnCode__TransItem(void* pData);
		size_t	DeCode__TransItem(void* pData);

		size_t	EnCode__SGQueryCachedItem(void* pData);
		size_t	DeCode__SGQueryCachedItem(void* pData);

		size_t	EnCode__GSFetchCachedItem(void* pData);
		size_t	DeCode__GSFetchCachedItem(void* pData);

		size_t	EnCode__SGFetchCachedItem(void* pData);
		size_t	DeCode__SGFetchCachedItem(void* pData);

		size_t	EnCode__GSPlayerNotify(void* pData);
		size_t	DeCode__GSPlayerNotify(void* pData);

		size_t	EnCode__GSShopRank(void* pData);
		size_t	DeCode__GSShopRank(void* pData);

		size_t	EnCode__RankInfo(void* pData);
		size_t	DeCode__RankInfo(void* pData);

		size_t	EnCode__SGShopRank(void* pData);
		size_t	DeCode__SGShopRank(void* pData);

		size_t	EnCode__GSPlayerGuildInfo(void* pData);
		size_t	DeCode__GSPlayerGuildInfo(void* pData);

		size_t	EnCode__SGGuildPrestigeAdd(void* pData);
		size_t	DeCode__SGGuildPrestigeAdd(void* pData);

	protected:
		std::map<int, EnCodeFunc>     m_mapEnCodeFunc;
		std::map<int, DeCodeFunc>     m_mapDeCodeFunc;
		ParserTool                    m_kPackage;
	};
}

#pragma pack(pop)
#endif			//__ShopSrvProtocol__
