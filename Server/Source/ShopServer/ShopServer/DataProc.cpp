﻿#include "DataProc.h"
#include "SessionHandler.h"
#include "ProcTask.h"
#include "MethodObject.h"
#include "DbModule.h"

CDataProc::CDataProc(CProcTask& ProcTask)
: m_ProcTask(ProcTask), m_Exit(false)
{
	RegisterHandler();
}

CDataProc::~CDataProc(void)
{

}

int CDataProc::ProcMsg(CSessionHandler* pSession, ACE_Message_Block* pMsg)
{
	int nRet = 0;
	unsigned long ulDataLen = *(unsigned short*)(pMsg->base());
	//对数据进行长度校验
	if (ulDataLen + sizeof(pSession) != pMsg->length())
	{
		pMsg->release();
		ACE_ERROR_RETURN((LM_ERROR, ACE_TEXT("(%t) %p\n"), ACE_TEXT("data package length error.")), -1);
	}

	ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pMsg->base());
	ACE_DEBUG((LM_INFO, ACE_TEXT("proc %d msg.\n"), pMsgHeader->PackCmd));
	HANDLERMAP::iterator Ite = m_HandlerMap.find(pMsgHeader->PackCmd);
	if (Ite != m_HandlerMap.end())
	{
		nRet = (this->*Ite->second.handler)(pSession, pMsg->base() + HEADERSIZE, pMsgHeader->PackLen - HEADERSIZE);
	}
	else
	{
		//
	}
	
	//释放内存
	pMsg->release();
	return nRet;
}

int CDataProc::RegisterHandler()
{
	//注册内部消息
	m_HandlerMap[INN_CONN] = MSGHANDLER(&CDataProc::ProcConn);
	m_HandlerMap[INN_CONNPASSIVECLOSE] = MSGHANDLER(&CDataProc::ProcPassiveClose);
	m_HandlerMap[INN_SHOPEXIT] = MSGHANDLER(&CDataProc::ProcShopExit);

	//注册Shop协议
	m_HandlerMap[ShopServer::G_S_PLAYERNOTIFY] = MSGHANDLER(&CDataProc::ProcPlayerNotify);
	m_HandlerMap[ShopServer::G_S_QUERYACCOUNTINFO] = MSGHANDLER(&CDataProc::ProcQryAccountInfo);
	m_HandlerMap[ShopServer::G_S_QUERYCACHEDITEM] = MSGHANDLER(&CDataProc::ProcQryCachedItem);
	m_HandlerMap[ShopServer::G_S_FETCHVIPITEM] = MSGHANDLER(&CDataProc::ProcFetchVipItem);
	m_HandlerMap[ShopServer::G_S_FETCHVIPITEMRESULT] = MSGHANDLER(&CDataProc::ProcFetchVipItemResult);
	m_HandlerMap[ShopServer::G_S_PURCHASEGIFT] = MSGHANDLER(&CDataProc::ProcPurchaseGift);
	m_HandlerMap[ShopServer::G_S_PURCHASEITEM] = MSGHANDLER(&CDataProc::ProcPurchaseItem);
	m_HandlerMap[ShopServer::G_S_PURCHASEVIP] = MSGHANDLER(&CDataProc::ProcPurchaseVip);
	m_HandlerMap[ShopServer::G_S_VIPNIFO] = MSGHANDLER(&CDataProc::ProcQryVipInfo);
	m_HandlerMap[ShopServer::G_S_TRANFAILED] = MSGHANDLER(&CDataProc::ProcTranFailed);
	m_HandlerMap[ShopServer::G_S_FETCHCACHEDITEM] = MSGHANDLER(&CDataProc::ProcFetchCachedItem);
	m_HandlerMap[ShopServer::G_S_QUERYROLEINFO] = MSGHANDLER(&CDataProc::ProcQryRoleInfo);
	m_HandlerMap[ShopServer::G_S_QUERYRANKINFO] = MSGHANDLER(&CDataProc::ProcQryRankInfo);
	m_HandlerMap[ShopServer::G_S_PLAYERGUILDINFO] = MSGHANDLER(&CDataProc::ProcPlayerGuildInfo);
	return 0;
}

int CDataProc::ProcConn(CSessionHandler * pSession, char *, unsigned short)
{
	SESSION_LIST::iterator Ite = m_SessionList.find(pSession->get_handle());
	if (Ite == m_SessionList.end())
	{
		m_SessionList[pSession->get_handle()] = pSession;
	}
	return 0;
}

int CDataProc::ProcPassiveClose(CSessionHandler* pSession, char*, unsigned short)
{
	SESSION_LIST::iterator Ite = m_SessionList.find(pSession->get_handle());
	if (Ite != m_SessionList.end())
	{
		m_SessionList.erase(Ite);
	}
	CSessionCloseObject* pMo = new CSessionCloseObject(NULL, pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	if (m_Exit && m_SessionList.size() == 0)
	{
		CShopExistObject* pMo = new CShopExistObject;
		CDbModuleInstance::instance()->PushTask(pMo);
	}
	return m_Exit && (m_SessionList.size() == 0) ? -1 : 0;
}

int CDataProc::ProcShopExit(CSessionHandler*, char*, unsigned short)
{
	m_Exit = true;
	for (SESSION_LIST::iterator Ite = m_SessionList.begin(); Ite != m_SessionList.end(); Ite++)
	{
		CSessionHandler* pSession = 0;
		pSession = (CSessionHandler*)(Ite->second);
		pSession->peer().close_writer();
	}

	if (m_SessionList.size() == 0)
	{
		CShopExistObject* pMo = new CShopExistObject;
		CDbModuleInstance::instance()->PushTask(pMo);
		return -1;
	}
	return 0;
}

int CDataProc::OnConnected(CSessionHandler *pSession)
{
	//组织连接建立数据包
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, ACE_Message_Block(MAX_PACK_SIZE / 2), -1);
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pData->base());
	pMsgHeader->PackCmd = CONNMSG::wCmd;
	pMsgHeader->PackType = 0;
	pMsgHeader->PackLen = sizeof(MSGHEADER);
	pData->wr_ptr(pMsgHeader->PackLen);
	//放进处理队列
	OnPacket(pSession, pData);
	return 0;
}

int CDataProc::OnConnectionClose(CSessionHandler* pSession)
{
	//组织连接关闭数据包
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, ACE_Message_Block(MAX_PACK_SIZE / 2), -1);
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pData->base());
	pMsgHeader->PackCmd = CONNPASSIVECLOSE::wCmd;
	pMsgHeader->PackType = 0;
	pMsgHeader->PackLen = sizeof(MSGHEADER);
	pData->wr_ptr(pMsgHeader->PackLen);
	//放进处理队列
	OnPacket(pSession, pData);
	return 0;
}

int CDataProc::OnPacket(CSessionHandler *pSession, ACE_Message_Block *pMsg)
{
	//把SessionHandler指针也放入消息块中
	pMsg->copy((char*)&pSession, sizeof(pSession));
	//提交给队列
	ACE_Time_Value tv(0);
	int nRet = m_ProcTask.putq(pMsg, &tv);
	if (nRet == -1)
	{
		ACE_ERROR_RETURN((LM_ERROR, "%p\n", "OnPacket failed."), 0);
	}
	return 0;
}

int CDataProc::ProcQryAccountInfo(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSQueryAccountInfo, pQueryAccountInfo, pData, usDataLen, m_ShopProtocol);
	CQryAccountObject* pMo = new CQryAccountObject(pQueryAccountInfo->Account, pQueryAccountInfo->UserId, CDbModuleInstance::instance()->GetDbConn(), pSession, *this)	;
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}

int CDataProc::ProcPurchaseItem(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSPurchaseItem, pPurchaseItem, pData, usDataLen, m_ShopProtocol);
	CPurchaseItemObject* pMo = new CPurchaseItemObject(pPurchaseItem->Account, pPurchaseItem->UserId, pPurchaseItem->RoleID, pPurchaseItem->PurchaseType,
		pPurchaseItem->ItemID, pPurchaseItem->ItemNum, pPurchaseItem->CliCalc, CDbModuleInstance::instance()->GetDbConn(), pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}

int CDataProc::ProcQryVipInfo(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSQueryVipInfo, pQueryVipInfo, pData, usDataLen, m_ShopProtocol);
	CQryVipInfoObject* pMo = new CQryVipInfoObject(pQueryVipInfo->Account, pQueryVipInfo->UserId, pQueryVipInfo->RoleID, CDbModuleInstance::instance()->GetDbConn(), pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}

int CDataProc::ProcPurchaseVip(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSPurchaseVip, pPurchaseVip, pData, usDataLen, m_ShopProtocol);
	CPurchaseVipObject* pMo = new CPurchaseVipObject(pPurchaseVip->Account, pPurchaseVip->UserId, pPurchaseVip->RoleID, pPurchaseVip->VipID, pPurchaseVip->CliCalc
		, CDbModuleInstance::instance()->GetDbConn(), pSession, *this) ;
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}

int CDataProc::ProcFetchVipItemResult(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSFetchVipItemResult, pFetchVipItemResult, pData, usDataLen, m_ShopProtocol);
	if (pFetchVipItemResult->Result != 0)
	{
		CFetchVipItemFailedObject* pMo = new CFetchVipItemFailedObject(pFetchVipItemResult->Account, pFetchVipItemResult->UserId, pFetchVipItemResult->RoleID, pFetchVipItemResult->VipID
			, CDbModuleInstance::instance()->GetDbConn(), pSession, *this);
		CDbModuleInstance::instance()->PushTask(pMo);
	}
	return 0;
}

int CDataProc::ProcPurchaseGift(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSPurchaseGift, pPurchaseGift, pData, usDataLen, m_ShopProtocol);
	CPurchaseGiftObject* pMo = new CPurchaseGiftObject(pPurchaseGift->SrcAccount, pPurchaseGift->UserId, pPurchaseGift->SrcRoleID, /*pPurchaseGift->SrcRoleName*/"", pPurchaseGift->PurchaseType,
		pPurchaseGift->ItemID, pPurchaseGift->ItemNum, pPurchaseGift->DestAccount, pPurchaseGift->DestRoleID, /*pPurchaseGift->DestRoleName*/"", pPurchaseGift->CliCalc, pPurchaseGift->Note, CDbModuleInstance::instance()->GetDbConn()
		, pSession, *this);
	std::map<std::string, PURGIFTOBJVECT>::iterator Ite = m_PurGiftObjMap.find(pPurchaseGift->DestAccount);
	if (Ite == m_PurGiftObjMap.end())
	{
		//创建查询玩家角色信息方法对象
		CQryRoleInfoOject* pQryRoleInfoObj = new CQryRoleInfoOject(CDbModuleInstance::instance()->GetDbConn(), pSession, *this, pPurchaseGift->DestAccount);
		CDbModuleInstance::instance()->PushTask(pQryRoleInfoObj);
		//缓存对象
		PURGIFTOBJVECT vect;
		vect.push_back(pMo);
		m_PurGiftObjMap[pPurchaseGift->DestAccount] = vect;
	}
	else
	{
		(Ite->second).push_back(pMo);
	}
	return 0;
}

int CDataProc::ProcQryCachedItem(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSQueryCachedItem, pQueryItem, pData, usDataLen, m_ShopProtocol);
	CQueryCachedItem* pMo = new CQueryCachedItem(pQueryItem->Account, pQueryItem->UseId, pQueryItem->RoleID, CDbModuleInstance::instance()->GetDbConn(), pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}

int CDataProc::ProcFetchVipItem(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSFetchVipItem, pFetchVipItem, pData, usDataLen, m_ShopProtocol);
	CFetchVipItemObject* pMo = new CFetchVipItemObject(pFetchVipItem->Account, pFetchVipItem->UserId, pFetchVipItem->RoleID, pFetchVipItem->VipID
		,CDbModuleInstance::instance()->GetDbConn() , pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}

int CDataProc::ProcTranFailed(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSTranFailed, pTranFailed, pData, usDataLen, m_ShopProtocol);
	CStoreItemObject* pMo = new CStoreItemObject(pTranFailed->Account, pTranFailed->UserId, pTranFailed->RoleID, pTranFailed->TransID, pTranFailed->ItemID,
		pTranFailed->ItemNum, pTranFailed->DestAccount, pTranFailed->DestRoleID, pTranFailed->Note, CDbModuleInstance::instance()->GetDbConn(), pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}

int CDataProc::ProcFetchCachedItem(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSFetchCachedItem, pFetchCachedItem, pData, usDataLen, m_ShopProtocol);
	CFetchCachedItem* pMo = new CFetchCachedItem(pFetchCachedItem->Account, pFetchCachedItem->UserId, pFetchCachedItem->RoleID, pFetchCachedItem->CachedItem.TransID
		, pFetchCachedItem->CachedItem.ItemID, pFetchCachedItem->CachedItem.ItemNum, CDbModuleInstance::instance()->GetDbConn(), pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}
int CDataProc::ProcPlayerNotify(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	DealPlayerPkgPre(ShopServer::GSPlayerNotify, pPlayerNotify, pData, usDataLen, m_ShopProtocol);
	CPlayerNotifyObject* pMo = new CPlayerNotifyObject(pPlayerNotify->Account, pPlayerNotify->UserId, pPlayerNotify->RoleID, pPlayerNotify->RoleName, pPlayerNotify->RaceID,
		pPlayerNotify->ClassID, pPlayerNotify->Level, pPlayerNotify->NotifyType, CDbModuleInstance::instance()->GetDbConn(), pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}

int CDataProc::ShopExit()
{
	//组织进程退出数据包
	ACE_Message_Block* pData = 0;
	ACE_NEW_RETURN(pData, ACE_Message_Block(MAX_PACK_SIZE / 2), -1);
	PMSGHEADER pMsgHeader = reinterpret_cast<PMSGHEADER>(pData->base());
	pMsgHeader->PackCmd = SHOPEXIT::wCmd;
	pMsgHeader->PackType = 0;
	pMsgHeader->PackLen = sizeof(MSGHEADER);
	pData->wr_ptr(pMsgHeader->PackLen);
	//放进处理队列
	OnPacket(0, pData);
	return 0;
}

int CDataProc::ProcQryRoleInfo(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	ACE_UNUSED_ARG(pSession);
	DealPlayerPkgPre(ShopServer::GSQryRoleInfo, pQryRoleInfo, pData, usDataLen, m_ShopProtocol);
	std::map<std::string, PURGIFTOBJVECT>::iterator Ite = m_PurGiftObjMap.find(pQryRoleInfo->RoleName);
	if (Ite != m_PurGiftObjMap.end())
	{
		PURGIFTOBJVECT* pVect = &(Ite->second);
		for (int nIndex = 0; nIndex <(int)pVect->size(); nIndex++)
		{
			CPurchaseGiftObject* pMo = (*pVect)[nIndex];
			pMo->SetRoleStatus(pQryRoleInfo->RoleStatus);
			CDbModuleInstance::instance()->PushTask(pMo);
		}
		m_PurGiftObjMap.erase(Ite);
	}
	return 0;
}

int CDataProc::ProcQryRankInfo(CSessionHandler* pSession, char* pData, unsigned short usDataLen)
{
	ACE_UNUSED_ARG(pSession);
	DealPlayerPkgPre(ShopServer::GSShopRank, pShopRank, pData, usDataLen, m_ShopProtocol);
	CQryRankObject* pMo = new CQryRankObject(pShopRank->UserId, pShopRank->RaceID, pShopRank->RankType, pShopRank->PageIndex, CDbModuleInstance::instance()->GetDbConn(), pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}

int CDataProc::ProcPlayerGuildInfo(CSessionHandler * pSession, char *pData, unsigned short usDataLen)
{
	ACE_UNUSED_ARG(pSession);
	DealPlayerPkgPre(ShopServer::GSPlayerGuildInfo, pPlayerGuildInfo, pData, usDataLen, m_ShopProtocol);
	CPlayerGuildInfo* pMo = new CPlayerGuildInfo(pPlayerGuildInfo->UserId, pPlayerGuildInfo->RoleID, pPlayerGuildInfo->GuildNo, CDbModuleInstance::instance()->GetDbConn(), pSession, *this);
	CDbModuleInstance::instance()->PushTask(pMo);
	return 0;
}