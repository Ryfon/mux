﻿/** @file DataBlock.h 
@brief 保存发送的消息
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-04-14
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef DATABLOCK_H
#define DATABLOCK_H
#include "Common.h"

class CDataBlock: public ACE_Message_Block
{
public:
	CDataBlock(size_t nSize);
	virtual ~CDataBlock(void);
public:
	void CurSize(size_t nCurSize);
	void DataSize(size_t nDataSize);
	size_t DataSize() const
	{
		return m_OrgSize;
	}
	size_t CurSize() const 
	{
		return m_CurSize;
	}
	bool IsComplete() const
	{
		return m_CurSize == m_OrgSize;
	}
private:
	size_t m_OrgSize;
	size_t m_CurSize;
};

#endif
