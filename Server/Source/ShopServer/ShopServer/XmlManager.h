﻿/** @file XmlManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2007-11-2
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef XML_MANAGER_H
#define XML_MANAGER_H

#include <tinyxml.h>
#include "Common.h"



/**
* @class CElement
*
* @brief xml文件中元素封装
*
* 定义元素的相关操作
*/
class CElement
{
public:
	/// 构造
	CElement(unsigned int uiParentIndex, unsigned int uiIndex, const char *pszName, unsigned int uiLevel, unsigned int uiAttrNum);

	/// 析构
	~CElement(void);

public:
	/// 名字
	const char * Name(void);

	/// 属性数目
	unsigned int AttrNum(void);

	/// 属性数目
	void AttrNum(unsigned int uiNum);

	/// 所处层次
	unsigned int Level(void);

	/// 父索引
	unsigned int ParentIndex(void);

	/// 索引
	unsigned int Index(void);

	/// 增加属性
	void AddAttr(const char* pszAttrName, const char* pszAttrValue);

	/// 获取属性值
	const char* GetAttr(const char* pszAttrName);

	/// 获取/设置值
	/// 获取值
	const char* Value(void);
	
	/// 设置值
	void Value(const char *Value);
private:
	/// 父元素索引
	unsigned int m_uiParentIndex;

	/// 索引
	unsigned int m_uiIndex;

	/// 名字 
	char m_szName[32];
	
	/// 所处层次
	unsigned int m_uiLevel;

	/// 属性数目
	unsigned int m_uiAttrNum;

	/// 属性列表
	std::map<std::string, std::string> m_attrList;

	/// Value
	char m_szValue[32];

};


/**
* @class CXmlManager
*
* @brief xml文件映像的管理
*
* 定义xml文件的访问接口
*/
class CXmlManager
{
public:
	/// 构造
	CXmlManager(void);

	/// 析构
	~CXmlManager(void);

public:
	/// 加载
	bool Load(const char* pszXmlFile);

	/// 重新加载
	bool Reload(const char* pszXmlFile);

	/// 清理
	void Clear(void);

	/// 获取根元素
	CElement * Root(void);

	/// 获取子元素
	void FindChildElements(CElement *pParent, std::vector<CElement *> &childElements);

	/// 获取子元素
	void FindChildElements(unsigned int uiParentIndex, std::vector<CElement *> &childElements);

	/// 获取同层元素(效率差)
	void FindSameLevelElements(unsigned int uiLevel, std::vector<CElement *> &childElements);

#ifndef USE_ACE_SAX_PARSE

private:
	bool Parse(const TiXmlElement *pXmlElement, unsigned int uiParentIndex, unsigned int &uiSelfIndex, unsigned int uiLevelIndex);

#endif/*USE_ACE_SAX_PARSE*/

private:
	/// 元素集
	std::vector<CElement*> m_elementSet;

};

#endif/*XML_MANAGER_H*/
