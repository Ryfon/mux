﻿#include "Acceptor.h"

CAcceptor::CAcceptor(void)
{

}

CAcceptor::~CAcceptor(void)
{

}

int CAcceptor::make_svc_handler(CSessionHandler *&sh)
{
	return supper::make_svc_handler(sh);
}

int CAcceptor::accept_svc_handler(CSessionHandler *svc_handler)
{
	return supper::accept_svc_handler(svc_handler);
}
