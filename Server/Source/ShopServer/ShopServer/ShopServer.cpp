﻿/** @file ShopServer.cpp 
@brief ShopServer主控程序
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-03-24
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#include "Common.h"
#include "ShopService.h"

int ACE_TMAIN(int argc, ACE_TCHAR * argv[])
{
	CShopService ShopService;
	if (ShopService.init(argc, argv[1]) == -1)
	{
		return -1;
	}
	ShopService.fini();
	return 0;
}
