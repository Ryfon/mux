﻿#ifndef CONFIG_H
#define CONFIG_H 
#include "Common.h"
#include "XmlManager.h"

class CConfig
{
public:
	CConfig(void);
	virtual ~CConfig(void);
public:
	std::string m_ServerIp;
	unsigned short m_ServerPort;
	std::string m_DbServerIp;
	std::string m_DbName;
	std::string m_DbUser;
	std::string m_DbPwd;
	unsigned short m_DbPort;
private:
	CXmlManager m_ConfigImp;
};

typedef ACE_Singleton<CConfig, ACE_Null_Mutex> CConfigInstance; 
#endif
