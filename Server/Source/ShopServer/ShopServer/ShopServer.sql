#道具配置信息
DROP TABLE IF EXISTS `ItemConf`;
CREATE TABLE `ItemConf`
(
    `ItemTpID` INT UNSIGNED NOT NULL COMMENT '道具类型ID 0：表示赠送 赠送只能用星钻',
    `ItemCatID` INT NOT NULL COMMENT '道具一级分类',
    `ItemCatSubID` INT NOT NULL COMMENT '道具二级分类',
    `SaleType` TINYINT NOT NULL COMMENT '销售类型 0 星钻 1 积分 2 星钻或者积分',
    `ScoreNum` INT NOT NULL COMMENT '需要的积分',	
    `PointNum` INT NOT NULL COMMENT '需要的星钻',
    `PriceType` INT NOT NULL DEFAULT 0 COMMENT '0 正常价 1 特价'
) ENGINE=MYISAM;

#会员配置信息
DROP TABLE IF EXISTS `ShopCustomerConf`;
CREATE TABLE `ShopCustomerConf`
(
    `CustID` TINYINT NOT NULL COMMENT '会员大类',
    `CustLevel` TINYINT NOT NULL DEFAULT 0 COMMENT '会员大类下的会员级别',
    `ReqMinExp` INT UNSIGNED NOT NULL COMMENT '会员级别所对应的最低消费经验值',
    `ReqMaxExp` INT UNSIGNED NOT NULL COMMENT '会员级别所对应的最高消费经验值',
    `SaleDisc` FLOAT NOT NULL COMMENT '会员大类对应的折扣',
    `LevelExp` INT NOT NULL COMMENT '保级经验'
) ENGINE=MYISAM;

#Vip 方案配置信息
DROP TABLE IF EXISTS `VipConf`;
CREATE TABLE `VipConf`
(
    `VipID` INT NOT NULL COMMENT 'Vip方案号',
    `ValidDateNum` TINYINT UNSIGNED NOT NULL COMMENT 'Vip方案对应的有效天数',
    `ItemInfo` VARCHAR(1024) NOT NULL COMMENT 'Vip方案对应的道具信息 格式：1001:2;1002:2',
    `GameMoney` INT NOT NULL COMMENT '该方案价值的金钻数'
) ENGINE=MYISAM;

#Vip 道具包信息
DROP TABLE IF EXISTS `VipItemInfo`;
CREATE TABLE `VipItemInfo`
(
	`VipID` INT NOT NULL COMMENT 'Vip方案号',
	`ItemID` INT NOT NULL COMMENT '道具类型号',
	`ItemNum` INT NOT NULL COMMENT '道具数目'
)ENGINE=MYISAM;

#账号信息
DROP TABLE IF EXISTS `AccountInfo`;
CREATE TABLE `AccountInfo`
(
    `Account` VARCHAR(255) NOT NULL COMMENT '玩家账号',
    `Money` INT NOT NULL COMMENT '星钻数目',
    `Score` INT NOT NULL COMMENT '积分数目',
    `CustExp` INT NOT NULL COMMENT '消费经验',
    `PeriodExp` INT NOT NULL COMMENT '保级周期内的经验值',
    `LastPeriod` DATE NOT NULL COMMENT '上次执行检查的时间',
    PRIMARY KEY (`Account`)
)ENGINE=INNODB;

#玩家星钻消费排行榜
DROP TABLE IF EXISTS `ConsumeRank`;
CREATE TABLE `ConsumeRank`
(
	`Account` VARCHAR(255) NOT NULL COMMENT '玩家账号',
	`RoleID` INT NOT NULL COMMENT '角色ID',
	`RoleName` VARCHAR(255) NOT NULL COMMENT '角色名',
	`RaceID` INT NOT NULL COMMENT '种族',
	`Level` INT NOT NULL COMMENT '等级',
	`ClassID` INT NOT NULL COMMENT '职业',
	`WeekCost` INT NOT NULL COMMENT '消费金额',
	`MonthCost` INT NOT NULL COMMENT '消费金额',
    `LastPeriod` DATE NOT NULL COMMENT '上次执行检查的时间',
    KEY(`Account`, `RoleID`, `RaceID`)
)ENGINE=INNODB;

#战盟玩家星钻消费记录
DROP TABLE IF EXISTS `GuildPlayerConsumeInfo`;
CREATE TABLE `GuildPlayerConsumeInfo`
(
	`Account` VARCHAR(255) NOT NULL COMMENT '玩家账号',
	`RoleID` INT NOT NULL COMMENT '角色ID',
	`GuildNo` INT NOT NULL COMMENT '战盟编号',	
	`Cost` INT NOT NULL COMMENT '星钻消费数目',
    PRIMARY KEY (`RoleID`)	
)ENGINE=INNODB;
 
#玩家的Vip信息
DROP TABLE IF EXISTS `VipInfo`;
CREATE TABLE `VipInfo`
(
     `Account` VARCHAR(255) NOT NULL COMMENT '玩家账号',
     `RoleID` INT NOT NULL COMMENT '角色名',
     `BeginDate` DATETIME NOT NULL COMMENT '生效时间',
     `EndDate` DATETIME NOT NULL COMMENT '结束时间',
     `LastFetchDate` DATETIME COMMENT '最近领取Vip道具包的时间',
     `VipID` INT NOT NULL COMMENT 'Vip方案号'
) ENGINE=INNODB;

#赠送信息 在Gate返回失败消息时会把信息存入这个表中，等待Gate发送领取消息
DROP TABLE IF EXISTS `GiftsInfo`;
CREATE TABLE `GiftsInfo`
(
	`ID` INT NOT NULL AUTO_INCREMENT COMMENT '流水号',
	`SrcAccount` VARCHAR(255) NOT NULL COMMENT '源玩家帐号',
	`SrcRoleID` INT NOT NULL COMMENT '源玩家角色',
    `DestAccount` VARCHAR(255) NOT NULL COMMENT '接收方玩家账号',
    `DestRoleID` 	INT NOT NULL COMMENT '接收方玩家角色名',
    `ItemID` INT NOT NULL COMMENT '道具类型ID',
    `ItemNum` INT NOT NULL COMMENT '道具数目',
    `TransID` INT NOT NULL COMMENT '交易流水号',
    `IsFetch` INT NOT NULL DEFAULT 0 COMMENT '是否已领取',
    `Note` VARCHAR(1024) COMMENT '购买备注',
    `OpeDate` DATETIME NOT NULL,
    PRIMARY KEY(`ID`),
    KEY(`SrcAccount`, `SrcRoleID`)
)ENGINE=INNODB;

#玩家充值记录
DROP TABLE IF EXISTS `PayLogs`;
CREATE TABLE `PayLogs`
(
    `Account` VARCHAR(255) NOT NULL COMMENT '玩家账号',
    `CardNo`  VARCHAR(255) NOT NULL COMMENT '充值卡号',
    `PayDate` DATETIME NOT NULL COMMENT '充值日期'
) ENGINE=INNODB;

#消费记录
DROP TABLE IF EXISTS `ConsumeLog`;
CREATE TABLE `ConsumeLog`
(
    `ConsumeID` INT NOT NULL AUTO_INCREMENT COMMENT '消费订单号',
    `Account` VARCHAR(255) NOT NULL COMMENT '玩家账号',
    `RoleID` INT	NOT NULL COMMENT '角色名',
    `ItemID` INT NOT NULL COMMENT '道具类型ID',
    `ItemNum` INT NOT NULL COMMENT '道具数目',
    `BuyType` TINYINT NOT NULL COMMENT '销售类型 0 星钻 1 积分',
    `Money` INT NOT NULL COMMENT '购买花费的积分或者星钻数目',
    `BuyTime` DATETIME NOT NULL COMMENT '购买时间',
    `ConsumeType` TINYINT NOT NULL COMMENT '消费类型 0：正常 1：特价',
    PRIMARY KEY(`ConsumeID`),
    KEY(`ItemID`)
)ENGINE=INNODB;

#经验扣除log
DROP TABLE IF EXISTS `PeriodExp`;
CREATE TABLE `PeriodExp`
(
    `Account` VARCHAR(255) NOT NULL COMMENT '玩家账号',
    `RoleID` INT	NOT NULL COMMENT '角色名',
    `PeriodDate` DATETIME NOT NULL COMMENT '扣除时间'
)
