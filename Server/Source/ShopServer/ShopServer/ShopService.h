﻿/** @file ShopService.h 
@brief 商城服务类封装
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-03-24
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef SHOPSERVICE_H
#define SHOPSERVICE_H
#include "Common.h"

class CShopService: public ACE_Service_Object
{
public:
	CShopService(void);
	virtual ~CShopService(void);
public:
	virtual int init(int argc, ACE_TCHAR* argv);
	virtual int fini(void);
	virtual int info(ACE_TCHAR **info_string, size_t length /* = 0 */);
	virtual int handle_signal(int signum, siginfo_t * = 0 , ucontext_t *  = 0 );
	virtual int handle_input(ACE_HANDLE fd /* = ACE_INVALID_HANDLE */);
private:
	ACE_Select_Reactor m_Reactor_Imp;
	ACE_Reactor* m_SelectReactor;
};

#endif
