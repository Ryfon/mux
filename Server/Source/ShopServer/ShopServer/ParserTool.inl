inline char* ParserTool::GetBufData()
{
	return m_pcBuf;
}

inline size_t ParserTool::GetBufDataLen()
{
	return m_unOffset;
}

inline void	ParserTool::SetBufDataLen(size_t unOffset)
{
	m_unOffset = unOffset;
}

inline size_t ParserTool::GetBufSize()
{
	return m_unSize;
}

inline size_t ParserTool::GetOutSize()
{
	return m_unOutSize;
}

