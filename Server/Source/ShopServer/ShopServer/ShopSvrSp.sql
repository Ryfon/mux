delimiter //
#根据购买金额计算消费经验、积分
DROP PROCEDURE IF EXISTS `sp_CalcExp`//
CREATE PROCEDURE `sp_CalcExp`
(
    IN iMoneyNum INT,
    OUT oExp INT,
    OUT oScore INT
)
BEGIN
    SET oExp = iMoneyNum;
    SET oScore = ROUND(iMoneyNum / 2);
END//

#保级经验验算
DROP PROCEDURE IF EXISTS `sp_VerifyClassExp`//
CREATE PROCEDURE `sp_VerifyClassExp`
(
    IN iAccount VARCHAR(255)
)
LABEL_BODY:
BEGIN
	DECLARE tmpCustExp INT DEFAULT 0;
	DECLARE tmpAccountLevelExp INT DEFAULT 0;
	DECLARE tmpLastPeriodDate DATE;
	DECLARE tmpCurDate DATE DEFAULT CURDATE();
	DECLARE tmpMonthDiff INT DEFAULT 0;
	DECLARE tmpCustID INT;
	DECLARE tmpLevelExp INT;
	DECLARE tmpReqMinExp INT;

    SELECT CustExp, PeriodExp, LastPeriod INTO tmpCustExp, tmpAccountLevelExp, tmpLastPeriodDate FROM AccountInfo WHERE Account = iAccount;

    IF FOUND_ROWS() = 0 THEN
		LEAVE LABEL_BODY;
    END IF;

	SET tmpMonthDiff = MONTH(tmpCurDate) + (YEAR(tmpCurDate) - YEAR(tmpLastPeriodDate)) * 12 - MONTH(tmpLastPeriodDate);
	WHILE tmpMonthDiff > 0 DO
		SELECT CustID , ReqMinExp , LevelExp INTO tmpCustID, tmpReqMinExp, tmpLevelExp FROM ShopCustomerConf
		  WHERE tmpCustExp >= ReqMinExp AND tmpCustExp <= ReqMaxExp;
		IF (tmpAccountLevelExp < tmpLevelExp) AND (tmpCustID > 1) THEN
			SET tmpCustExp = tmpReqMinExp;
			SET tmpAccountLevelExp = 0;
			UPDATE AccountInfo SET CustExp = tmpCustExp, PeriodExp = tmpAccountLevelExp, LastPeriod = tmpCurDate WHERE Account = iAccount;
		END IF;
		SET tmpMonthDiff = tmpMonthDiff - 1;
	END WHILE;
END//

#星钻消费排行榜更新 在进入商城(因为目前购买必须先进商城)时调用
DROP PROCEDURE IF EXISTS `sp_UpdateRank` //
CREATE PROCEDURE `sp_UpdateRank`
(
	IN iAccount VARCHAR(255),
	IN iRoleID INT,
	IN iRoleName VARCHAR(255),
	IN iRaceID INT,
	IN iLevel INT,
	IN iClassID INT,
	IN iCostValue INT
)
LABEL_BODY:
BEGIN
	DECLARE tmpCurDate DATE DEFAULT CURDATE();
	UPDATE ConsumeRank SET WeekCost	= 0 WHERE Account = iAccount AND RoleID = iRoleID AND WEEKOFYEAR(LastPeriod) != WEEKOFYEAR(tmpCurDate);
	UPDATE ConsumeRank SET MonthCost	= 0 WHERE Account = iAccount AND RoleID = iRoleID AND MONTH(LastPeriod) != MONTH(tmpCurDate);

	UPDATE ConsumeRank SET WeekCost	= WeekCost + iCostValue, Level = iLevel, MonthCost = MonthCost + iCostValue  WHERE Account = iAccount AND RoleID = iRoleID;

	IF ROW_COUNT() = 0 THEN
		INSERT INTO ConsumeRank	VALUES(iAccount, iRoleID, iRoleName, iRaceID, iLevel, iClassID, iCostValue, iCostValue, tmpCurDate);
	END IF;
END//

#获得上榜行数
DROP PROCEDURE IF EXISTS `sp_GetRankCount` //
CREATE PROCEDURE `sp_GetRankCount`
(
	IN iRaceID INT,
	IN iCat INT,
	OUT oRankCount INT
)
LABEL_BODY:
BEGIN
	DECLARE tmpCurDate DATE DEFAULT CURDATE();
	IF iRaceID = 4 AND iCat = 0 THEN
		SELECT COUNT(1) INTO oRankCount FROM ConsumeRank WHERE WEEKOFYEAR(LastPeriod) = WEEKOFYEAR(ADDDATE(tmpCurDate, -7));
	END IF;
	IF iRaceID = 4 AND iCat = 1 THEN
		SELECT COUNT(1) INTO oRankCount FROM ConsumeRank WHERE MONTH(LastPeriod) = MONTH(ADDDATE(tmpCurDate, -7));	
	END IF;
	IF iRaceID < 4 AND iCat = 0 THEN
		SELECT COUNT(1) INTO oRankCount FROM ConsumeRank WHERE RaceID = iRaceID AND WEEKOFYEAR(LastPeriod) = WEEKOFYEAR(ADDDATE(tmpCurDate, -7));	
	END IF;
	IF iRaceID < 4 AND iCat = 1 THEN
		SELECT COUNT(1) INTO oRankCount FROM ConsumeRank WHERE RaceID = iRaceID AND MONTH(LastPeriod) = MONTH(ADDDATE(tmpCurDate, -7));	
	END IF;

END //

#获取排行榜信息 iCat 0 week 1 month
DROP PROCEDURE IF EXISTS `sp_GetRankInfo` //
CREATE PROCEDURE `sp_GetRankInfo`
(
	IN iRaceID INT,
	IN iCat INT,
	IN iPageIndex INT
)
LABEL_BODY:
BEGIN
	DECLARE tmpCurDate DATE DEFAULT CURDATE();
	SET @StartIndex = iPageIndex * 10;
	SET @strSql = '';
	IF iRaceID = 4 AND iCat = 0 THEN
		SET @strSql = 'SELECT RoleName, RaceID, Level, ClassID, WeekCost FROM ConsumeRank WHERE WEEKOFYEAR(LastPeriod) = WEEKOFYEAR(?)  ORDER BY WeekCost DESC LIMIT ?, 10';
		PREPARE STMT FROM @strSql;
		SET @CurDate = ADDDATE(tmpCurDate, -7);
		EXECUTE STMT using @CurDate, @StartIndex;
		DEALLOCATE PREPARE STMT;
	END IF;
	IF iRaceID = 4 AND iCat = 1 THEN
		SET @strSql = 'SELECT RoleName, RaceID, Level, ClassID, MonthCost FROM ConsumeRank WHERE MONTH(LastPeriod) = MONTH(?) ORDER BY MonthCost DESC LIMIT ?, 10';
		PREPARE STMT FROM @strSql;
		SET @CurDate = ADDDATE(tmpCurDate, -7);
		EXECUTE STMT using @CurDate, @StartIndex;
		DEALLOCATE PREPARE STMT;
	END IF;
	IF iRaceID < 4 AND iCat = 0 THEN
		SET @strSql = 'SELECT RoleName, RaceID, Level, ClassID, WeekCost FROM ConsumeRank WHERE RaceID = ? AND WEEKOFYEAR(LastPeriod) = WEEKOFYEAR(?)  ORDER BY WeekCost DESC LIMIT ?, 10';
		PREPARE STMT FROM @strSql;
		SET @CurDate = ADDDATE(tmpCurDate, -7);
		SET @RaceID = iRaceID;
		EXECUTE STMT using @RaceID, @CurDate, @StartIndex;
		DEALLOCATE PREPARE STMT;
	END IF;
	IF iRaceID < 4 AND iCat = 1 THEN
		SET @strSql = 'SELECT RoleName, RaceID, Level, ClassID, MonthCost FROM ConsumeRank WHERE RaceID = ? AND MONTH(LastPeriod) = MONTH(?)  ORDER BY MonthCost DESC LIMIT ?, 10';
		PREPARE STMT FROM @strSql;
		SET @CurDate = ADDDATE(tmpCurDate, -7);
		SET @RaceID = iRaceID;
		EXECUTE STMT using @RaceID, @CurDate, @StartIndex;
		DEALLOCATE PREPARE STMT;
	END IF;
END//

#更新战盟玩家消费信息，获取玩家战盟威望升级值
DROP PROCEDURE IF EXISTS `sp_UpdateGuildConsume` //
CREATE PROCEDURE `sp_UpdateGuildConsume`
(
	IN iAccount VARCHAR(255),
	IN iRoleID INT,
	IN iGuildNo INT,
	IN iCost INT,
	OUT oPrestigeAdd INT
)
BEGIN
	DECLARE tmpGuildNo INT;
	DECLARE tmpCost INT;
	
	SELECT GuildNo, Cost INTO tmpGuildNo, tmpCost FROM GuildPlayerConsumeInfo WHERE RoleID = iRoleID;
	IF FOUND_ROWS() = 0 THEN
		IF iGuildNo > 0 THEN
			SET oPrestigeAdd = iCost DIV 100;
			INSERT GuildPlayerConsumeInfo(Account, RoleID, GuildNo, Cost) VALUES(iAccount, iRoleID, iGuildNo, MOD(iCost, 100));
		ELSE
			SET oPrestigeAdd = 0;
		END IF;
	ELSE
		IF iGuildNo = 0 THEN
			SET oPrestigeAdd = 0;
			UPDATE GuildPlayerConsumeInfo SET GuildNo = iGuildNo, Cost = 0 WHERE RoleID = iRoleID;
		ELSE
			IF iGuildNo = tmpGuildNo THEN
				SET oPrestigeAdd = (tmpCost + iCost) DIV 100;
				UPDATE GuildPlayerConsumeInfo SET Cost = MOD(tmpCost + iCost, 100) WHERE RoleID = iRoleID;
			ELSE
				SET oPrestigeAdd = iCost DIV 100;
				UPDATE GuildPlayerConsumeInfo SET GuildNo = iGuildNo, Cost = MOD(iCost, 100) WHERE RoleID = iRoleID;				
			END IF;
		END IF;
	END IF;
END //

#获取玩家信息
DROP PROCEDURE IF EXISTS `sp_GetAccountInfo`//
CREATE PROCEDURE `sp_GetAccountInfo`
(
    IN iAccount VARCHAR(255),
    OUT oGameMoney INT,
    OUT oScore INT,
    OUT oMemberClass TINYINT,
    OUT oMemberLevel TINYINT,
    OUT oMemberExp INT,
    OUT oPurchaseDisc FLOAT
)
BEGIN
    SET oGameMoney = 0;
    SET oScore = 0;
    SET oMemberClass = 0;
    SET oMemberLevel = 0;
    SET oMemberExp = 0;
    SET oPurchaseDisc = 0;

    CALL sp_VerifyClassExp(iAccount);

    SELECT Money , Score, CustExp INTO oGameMoney, oScore, oMemberExp FROM AccountInfo WHERE Account = iAccount;
    SELECT CustID , CustLevel , SaleDisc INTO oMemberClass, oMemberLevel, oPurchaseDisc FROM ShopCustomerConf
      WHERE oMemberExp >= ReqMinExp AND oMemberExp <= ReqMaxExp;
END//

#获得货架信息
DROP PROCEDURE IF EXISTS `sp_GetCatInfo`//
CREATE PROCEDURE `sp_GetCatInfo`
(
    IN iCatID INT
)
BEGIN
    IF iCatID = 0 THEN
        SELECT DISTINCT ItemCatID FROM ItemConf WHERE ItemTpID <> 0;
    ELSE
        SELECT DISTINCT ItemCatSubID FROM ItemConf WHERE ItemCatID = iCatID AND ItemTpID <> 0;
    END IF;
END//

#获得给定货架的商品信息
DROP PROCEDURE IF EXISTS `sp_GetItemInfo`//
CREATE PROCEDURE `sp_GetItemInfo`
(
    IN iCatID INT,
    IN iSubCatID INT
)
BEGIN
    #如果是热销商品，需要从历史纪录中统计
    IF iCatID = 0 AND iSubCatID = 0 THEN
        SELECT ItemTpID, ItemCatID, ItemCatSubID, SaleType, ScoreNum, PointNum, PriceType FROM ItemConf
          WHERE ItemCatID = iCatID AND ItemCatSubID = iSubCatID;
    ELSE
        SELECT ItemTpID, ItemCatID, ItemCatSubID, SaleType, ScoreNum, PointNum, PriceType FROM ItemConf
          WHERE ItemCatID = iCatID AND ItemCatSubID = iSubCatID AND ItemTpID <> 0;
    END IF;
END//

#获得Vip方案信息
DROP PROCEDURE IF EXISTS `sp_GetVipConf`//
CREATE PROCEDURE `sp_GetVipConf`()
BEGIN
    SELECT VipID, ValidDateNum, GameMoney FROM VipConf;
END//

#获得Vip方案的道具信息
DROP PROCEDURE IF EXISTS `sp_VipItemInfo`//
CREATE PROCEDURE `sp_VipItemInfo`
(
	IN iVipID INT
)
LABEL_BODY:
BEGIN
	SELECT ItemID, ItemNum FROM VipItemInfo WHERE VipID = iVipID;
END//
#购买验证
DROP PROCEDURE IF EXISTS `sp_PurchaseCheck`//
CREATE PROCEDURE `sp_PurchaseCheck`
(
    IN iAccount VARCHAR(255),
    IN iServiceType TINYINT,
    IN iServiceID INT,
    IN iServiceNum INT,
    IN iPurchaseType TINYINT,
    IN iPurchaseDest TINYINT,
    OUT oResult INT,
    OUT oServiceMoney INT,
    OUT oAdditionMoney INT
)
LABEL_BODY:
BEGIN
    DECLARE tmpItemPrice INT DEFAULT 0;
    DECLARE tmpAdditionMoney INT DEFAULT 0;
    DECLARE tmpTotalConsume INT DEFAULT 0;
    DECLARE tmpGameMoney INT DEFAULT 0;
    DECLARE tmpScore INT DEFAULT 0;
    DECLARE tmpMemberClass TINYINT DEFAULT 0;
    DECLARE tmpMemberLevel TINYINT DEFAULT 0;
    DECLARE tmpMemberExp INT DEFAULT 0;
    DECLARE tmpPurchaseDisc FLOAT DEFAULT 0;

    SET oServiceMoney = 0;
	SET oAdditionMoney = 0;
    #计算道具理论费用
    #道具
    IF iServiceType = 0 THEN
        IF iPurchaseType = 0 THEN
            SELECT PointNum INTO tmpItemPrice FROM ItemConf WHERE ItemTpID = iServiceID AND (SaleType = 2 OR SaleType = iPurchaseType) LIMIT 1;
        ELSE
            SELECT ScoreNum INTO tmpItemPrice FROM ItemConf WHERE ItemTpID = iServiceID AND (SaleType = 2 OR SaleType = iPurchaseType) LIMIT 1;
        END IF;
    #vip
    ELSE
        SELECT GameMoney INTO tmpItemPrice FROM VipConf WHERE VipID = iServiceID LIMIT 1;
    END IF;

    #不支持此种购买方式
    IF tmpItemPrice = 0 THEN
        SET oResult = 10001;
        LEAVE LABEL_BODY;
    END IF;

    SET tmpTotalConsume = tmpItemPrice * iServiceNum;
    #赠送费用
    IF iPurchaseDest = 1 THEN
        SELECT PointNum INTO tmpAdditionMoney FROM ItemConf WHERE ItemTpID = 0;
    END IF;


    #获取玩家余额、折扣信息
    CALL sp_GetAccountInfo(iAccount, tmpGameMoney, tmpScore, tmpMemberClass, tmpMemberLevel, tmpMemberExp, tmpPurchaseDisc);

    #星钻购买
    IF iPurchaseType = 0 THEN
    	IF tmpGameMoney >= ROUND((tmpTotalConsume) * (1 - tmpPurchaseDisc) + tmpAdditionMoney) THEN
    	    SET oResult = 0;
    	ELSE
    	    SET oResult = 10050;
    	END IF;
        SET oServiceMoney = ROUND(tmpTotalConsume * (1 - tmpPurchaseDisc));
    	IF iPurchaseDest = 1 THEN
    	    SET oAdditionMoney = tmpAdditionMoney;
    	ELSE
    	    SET oAdditionMoney = 0;
    	END IF;
    #积分购买
    ELSE
        IF (tmpScore >= tmpTotalConsume) AND (tmpGameMoney >= tmpAdditionMoney) THEN
            SET oResult = 0;
        ELSE
            SET oResult = 10050;
        END IF;
        SET oServiceMoney = tmpTotalConsume;
    	IF iPurchaseDest = 1 THEN
    	    SET oAdditionMoney = tmpAdditionMoney;
    	ELSE
    	    SET oAdditionMoney = 0;
    	END IF;
    END IF;
END//

#购买道具
DROP PROCEDURE IF EXISTS `sp_PurchaseItem`//
CREATE PROCEDURE `sp_PurchaseItem`
(
    IN iAccount VARCHAR(255),
    IN iRoleID INT,
    IN iPurchaseType TINYINT,
    IN iItemID INT,
    IN iItemNum INT,
    OUT oResult INT,
    OUT oTranID INT
)
LABEL_BODY:
BEGIN
    DECLARE ItemPrice INT DEFAULT 0;
    DECLARE TotalConsume INT DEFAULT 0;
    DECLARE GameMoney INT DEFAULT 0;
    DECLARE Score INT DEFAULT 0;
    DECLARE MemberClass TINYINT DEFAULT 0;
    DECLARE MemberLevel TINYINT DEFAULT 0;
    DECLARE MemberExp INT DEFAULT 0;
    DECLARE PurchaseDisc FLOAT DEFAULT 0;
    DECLARE AddExp INT DEFAULT 0;
    DECLARE AddScore INT DEFAULT 0;
    #计算道具理论费用
    IF iPurchaseType = 0 THEN
        SELECT PointNum INTO ItemPrice FROM ItemConf WHERE ItemTpID = iItemID AND (SaleType = 2 OR SaleType = iPurchaseType) LIMIT 1;
    ELSE
        SELECT ScoreNum INTO ItemPrice FROM ItemConf WHERE ItemTpID = iItemID AND (SaleType = 2 OR SaleType = iPurchaseType) LIMIT 1;
    END IF;

    #不支持此种购买方式
    IF ItemPrice = 0 THEN
        SET oResult = 10001;
        SET oTranID = 0;
        LEAVE LABEL_BODY;
    END IF;

    SET TotalConsume = ItemPrice * iItemNum;

    #获取玩家余额、折扣信息
    CALL sp_GetAccountInfo(iAccount, GameMoney, Score, MemberClass, MemberLevel, MemberExp, PurchaseDisc);
    #星钻购买
    IF iPurchaseType = 0 THEN
        SET TotalConsume = ROUND((TotalConsume) * (1 - PurchaseDisc));
    	IF GameMoney >= TotalConsume THEN
    	    CALL sp_CalcExp(TotalConsume, AddExp, AddScore);
    	    UPDATE AccountInfo SET Money = GameMoney - TotalConsume, Score = Score + AddScore, CustExp = CustExp + AddExp
    	      WHERE Account = iAccount;
    	    INSERT INTO ConsumeLog VALUES(0, iAccount, iRoleID, iItemID, iItemNum, iPurchaseType, TotalConsume, NOW(), 0);
    	    SET oTranID = LAST_INSERT_ID();
    	    SET oResult = 0;

    	ELSE
    	    SET oResult = 10050;
    	    SET oTranID = 0;
    	END IF;
    #积分购买
    ELSE
        IF Score >= TotalConsume THEN
            UPDATE AccountInfo SET Score = Score - TotalConsume WHERE Account = iAccount;
    	    INSERT INTO ConsumeLog VALUES(0, iAccount, iRoleID, iItemID, iItemNum, iPurchaseType, TotalConsume, NOW(), 0);
    	    SET oTranID = LAST_INSERT_ID();
            SET oResult = 0;
        ELSE
            SET oResult = 10050;
            SET oTranID = 0;
        END IF;
    END IF;
END//

#获得玩家的vip信息
DROP PROCEDURE IF EXISTS `sp_GetAccountVipInfo`//
CREATE PROCEDURE `sp_GetAccountVipInfo`
(
    IN iAccount VARCHAR(255),
    IN iRoleID INT
)
LABEL_BODY:
BEGIN
    DECLARE dtCurDate DATETIME;
    SET dtCurDate = NOW();
    SELECT a.VipID, IF(DATEDIFF(a.EndDate, NOW()) > b.ValidDateNum, b.ValidDateNum, DATEDIFF(a.EndDate, NOW())) AS LeftDays,
      CASE
      WHEN a.BeginDate <= NOW() AND NOW() < a.EndDate AND (a.LastFetchDate IS NULL	OR DATEDIFF(a.LastFetchDate, NOW()) != 0)  THEN 0
      ELSE 1
      END
      AS Fetched
      FROM VipInfo a, VipConf b
      WHERE a.Account = iAccount AND RoleID = iRoleID AND
      dtCurDate < EndDate
      AND a.VipID = b.VipID;
END//

#购买Vip方案
DROP PROCEDURE IF EXISTS `sp_PurchaseVip`//
CREATE PROCEDURE `sp_PurchaseVip`
(
    IN iAccount VARCHAR(255),
    IN iRoleID INT,
    IN iVipID TINYINT,
    OUT oResult INT
)
LABEL_BODY:
BEGIN
    DECLARE VipPrice INT DEFAULT 0;
    DECLARE tmpGameMoney INT DEFAULT 0;
    DECLARE DateNum INT DEFAULT 0;
    DECLARE Score INT DEFAULT 0;
    DECLARE MemberClass TINYINT DEFAULT 0;
    DECLARE MemberLevel TINYINT DEFAULT 0;
    DECLARE MemberExp INT DEFAULT 0;
    DECLARE PurchaseDisc FLOAT DEFAULT 0;
    DECLARE AddExp INT DEFAULT 0;
    DECLARE AddScore INT DEFAULT 0;
    DECLARE dtCurDate DATETIME;

    SET dtCurDate = NOW();


    #计算vip理论费用
    SELECT ValidDateNum, GameMoney INTO DateNum, VipPrice FROM VipConf WHERE VipID = iVipID;

    #不支持此种购买方式
    IF VipPrice = 0 THEN
        SET oResult = 10001;
        LEAVE LABEL_BODY;
    END IF;

    IF EXISTS(SELECT VipID  FROM VipInfo
      WHERE Account = iAccount AND RoleID = iRoleID AND
      dtCurDate < EndDate
      AND VipID = VipID) THEN
        SET oResult = 10002;
        LEAVE LABEL_BODY;
    END IF;

    #获取玩家余额、折扣信息
    CALL sp_GetAccountInfo(iAccount, tmpGameMoney, Score, MemberClass, MemberLevel, MemberExp, PurchaseDisc);
    #星钻购买

    SET VipPrice = ROUND((VipPrice) * (1 - PurchaseDisc));
    IF tmpGameMoney >= VipPrice THEN
        CALL sp_CalcExp(VipPrice, AddExp, AddScore);
    	UPDATE AccountInfo SET Money = tmpGameMoney - VipPrice, Score = Score + AddScore, CustExp = CustExp + AddExp
    	      WHERE Account = iAccount;

    	INSERT INTO VipInfo VALUES(iAccount, iRoleID, DATE(dtCurDate), ADDDATE(DATE(dtCurDate), DateNum), NULL, iVipID);
    	SET oResult = 0;
    ELSE
    	SET oResult = 10050;
    END IF;
END//

#领取Vip道具
DROP PROCEDURE IF EXISTS `sp_FetchVipItem`//
CREATE PROCEDURE `sp_FetchVipItem`
(
    IN iAccount VARCHAR(255),
    IN iRoleID INT,
    IN iVipID TINYINT,
    OUT oResult INT,
    OUT oTranID INT
)
LABEL_BODY:
BEGIN
    DECLARE dtCurDate DATETIME;
    SET dtCurDate = NOW();

    UPDATE VipInfo SET LastFetchDate = dtCurDate WHERE Account = iAccount AND RoleID = iRoleID AND VipID = iVipID
      AND dtCurDate >= BeginDate AND dtCurDate < EndDate AND (LastFetchDate IS NULL OR ( LastFetchDate IS NOT NULL AND DATEDIFF(dtCurDate, LastFetchDate) != 0));

	IF ROW_COUNT() > 0 THEN
	    INSERT INTO ConsumeLog VALUES(0, iAccount, iRoleID, iVipID, 0, 0, 0, NOW(), 0);
	    SET oTranID = LAST_INSERT_ID();
	ELSE
		SET oTranID = 0;
	END IF;
    SET oResult = 0;
END//

#领取Vip道具包失败处理
DROP PROCEDURE IF EXISTS `sp_FetchVipItemFailed`//
CREATE PROCEDURE `sp_FetchVipItemFailed`
(
    IN iAccount VARCHAR(255),
    IN iRoleID INT,
    IN iVipID TINYINT
)
LABEL_BODY:
BEGIN
    #此处应处理在0点以前领取，但是失败在0点以后到来的情况
    UPDATE VipInfo SET LastFetchDate = IF(DATEDIFF(BeginDate, LastFetchDate) != 0, ADDDATE(LastFetchDate, -1), NULL) WHERE Account = iAccount AND RoleID = iRoleID AND VipID = iVipID
      AND LastFetchDate >= BeginDate AND LastFetchDate < EndDate;
END//

#赠送道具
DROP PROCEDURE IF EXISTS `sp_PurchaseGift`//
CREATE PROCEDURE `sp_PurchaseGift`
(
    IN iAccount VARCHAR(255),
    IN iRoleID INT,
    IN iPurchaseType TINYINT,
    IN iItemID INT,
    IN iItemNum INT,
    IN iDestAccount VARCHAR(255),
    IN iDestRoleID INT,
    IN iNote VARCHAR(255),
    OUT oResult INT,
    OUT oTranID INT
)
LABEL_BODY:
BEGIN
    DECLARE ItemPrice INT DEFAULT 0;
    DECLARE TotalConsume INT DEFAULT 0;
    DECLARE GameMoney INT DEFAULT 0;
    DECLARE Score INT DEFAULT 0;
    DECLARE MemberClass TINYINT DEFAULT 0;
    DECLARE MemberLevel TINYINT DEFAULT 0;
    DECLARE MemberExp INT DEFAULT 0;
    DECLARE PurchaseDisc FLOAT DEFAULT 0;
    DECLARE MailFee INT DEFAULT 0;
    DECLARE AddExp INT DEFAULT 0;
    DECLARE AddScore INT DEFAULT 0;
    #计算道具理论费用
    IF iPurchaseType = 0 THEN
        SELECT PointNum INTO ItemPrice FROM ItemConf WHERE ItemTpID = iItemID AND (SaleType = 2 OR SaleType = iPurchaseType) LIMIT 1;
    ELSE
        SELECT ScoreNum INTO ItemPrice FROM ItemConf WHERE ItemTpID = iItemID AND (SaleType = 2 OR SaleType = iPurchaseType) LIMIT 1;
    END IF;

    #不支持此种购买方式
    IF ItemPrice = 0 THEN
        SET oResult = 10001;
        SET oTranID = 0;
        LEAVE LABEL_BODY;
    END IF;

    SELECT PointNum INTO MailFee FROM ItemConf WHERE ItemTpID = 0;

    SET TotalConsume = ItemPrice * iItemNum;

    #获取玩家余额、折扣信息
    CALL sp_GetAccountInfo(iAccount, GameMoney, Score, MemberClass, MemberLevel, MemberExp, PurchaseDisc);
    #星钻购买
    IF iPurchaseType = 0 THEN
        SET TotalConsume = ROUND((TotalConsume) * (1 - PurchaseDisc)) + MailFee;
    	IF GameMoney >= TotalConsume THEN
    	    CALL sp_CalcExp(TotalConsume, AddExp, AddScore);
    	    UPDATE AccountInfo SET Money = GameMoney - TotalConsume, Score = Score + AddScore, CustExp = CustExp + AddExp
    	      WHERE Account = iAccount;

    	    INSERT INTO ConsumeLog VALUES(0, iAccount, iRoleID, iItemID, iItemNum, iPurchaseType, TotalConsume, NOW(), 0);
    	    SET oTranID = LAST_INSERT_ID();

    	    INSERT INTO GiftsInfo VALUES(0, iAccount, iRoleID, iDestAccount, iDestRoleID, iItemID, iItemNum, oTranID, 0, iNote, Now());
    	    SET oResult = 0;
    	ELSE
    	    SET oResult = 10050;
    	    SET oTranID = 0;
    	END IF;
    #积分购买
    ELSE
        IF (Score >= TotalConsume) AND (GameMoney >= MailFee) THEN
            CALL sp_CalcExp(MailFee, AddExp, AddScore);
            UPDATE AccountInfo SET Score = Score - TotalConsume + AddScore, CustExp = CustExp + AddExp, Money = GameMoney - MailFee
              WHERE Account = iAccount;

    	    INSERT INTO ConsumeLog VALUES(0, iAccount, iRoleID, iItemID, iItemNum, iPurchaseType, TotalConsume, NOW(), 0);
    	    SET oTranID = LAST_INSERT_ID();

    	    INSERT INTO GiftsInfo VALUES(0, iAccount, iRoleID, iDestAccount, iDestRoleID, iItemID, iItemNum, oTranID, 0, iNote, Now());

            SET oResult = 0;
        ELSE
            SET oResult = 10050;
            SET oTranID = 0;
        END IF;
    END IF;
END//

#Gate报告道具发送失败
DROP PROCEDURE IF EXISTS `sp_StoreItem`//
CREATE PROCEDURE `sp_StoreItem`
(
    IN iAccount VARCHAR(255),
    IN iRoleID INT,
    IN iItemID INT,
    IN iItemNum INT,
    IN iDestAccount VARCHAR(255),
    IN iDestRoleID INT,
    IN iNote VARCHAR(255),
	IN iTransID INT
)
LABEL_BODY:
BEGIN
	UPDATE GiftsInfo SET IsFetch = 0 WHERE DestAccount = iDestAccount AND DestRoleID = iDestRoleID AND TransID = iTransID AND IsFetch = 1;
	IF ROW_COUNT() = 0 THEN
		INSERT INTO GiftsInfo VALUES(0, iAccount, iRoleID, iDestAccount, iDestRoleID, iItemID, iItemNum, iTransID, 0, iNote, Now());
	END IF;
END//

#获得玩家道具信息
DROP PROCEDURE IF EXISTS `sp_QueryCachedItems`//
CREATE PROCEDURE `sp_QueryCachedItems`
(
	IN iAccount VARCHAR(255),
	IN iRoleID INT
)
LABEL_BODY:
BEGIN
	SELECT ItemID, ItemNum, TransID, UNIX_TIMESTAMP(OpeDate) FROM GiftsInfo WHERE DestAccount = iAccount AND DestRoleID = iRoleID AND IsFetch = 0;
END//

#领取缓存在ShopServer的道具
DROP PROCEDURE IF EXISTS `sp_FetchCachedItem`//
CREATE PROCEDURE `sp_FetchCachedItem`
(
	IN iAccount VARCHAR(255),
	IN iRoleID INT,
	IN iTransID INT
)
LABEL_BODY:
BEGIN
	UPDATE GiftsInfo SET IsFetch = 1 WHERE DestAccount = iAccount AND DestRoleID = iRoleID AND TransID = iTransID AND IsFetch = 0;
END//

#测试用，设置积分
DROP PROCEDURE IF EXISTS `sp_SetScore` //
CREATE PROCEDURE `sp_SetScore`
(
	IN iAccount VARCHAR(255),
	IN iScore INT
)
LABEL_BODY:
BEGIN
	UPDATE AccountInfo SET Score = iScore WHERE Account = iAccount;
	IF ROW_COUNT() = 0 THEN
		INSERT INTO AccountInfo (Account, Money, Score, CustExp, PeriodExp, LastPeriod)
		SELECT iAccount, 0, iScore, 0, 0, CURDATE()
		FROM dual
		WHERE NOT EXISTS(SELECT * FROM AccountInfo WHERE Account = iAccount);
	END IF;
END//

#测试用，设置星钻
DROP PROCEDURE IF EXISTS `sp_SetPoint` //
CREATE PROCEDURE `sp_SetPoint`
(
	IN iAccount VARCHAR(255),
	IN iPoint INT
)
LABEL_BODY:
BEGIN
	UPDATE AccountInfo SET Money = iPoint WHERE Account = iAccount;
	IF ROW_COUNT() = 0 THEN
		INSERT INTO AccountInfo (Account, Money, Score, CustExp, PeriodExp, LastPeriod)
		SELECT iAccount, iPoint, 0, 0, 0, CURDATE()
		FROM dual
		WHERE NOT EXISTS(SELECT * FROM AccountInfo WHERE Account = iAccount);
	END IF;
END//

#测试用，设置消费经验
DROP PROCEDURE IF EXISTS `sp_SetExp` //
CREATE PROCEDURE `sp_SetExp`
(
	IN iAccount VARCHAR(255),
	IN iExp	INT
)
LABEL_BODY:
BEGIN
	UPDATE AccountInfo SET CustExp = iExp WHERE Account = iAccount;
	IF ROW_COUNT() = 0 THEN
		INSERT INTO AccountInfo (Account, Money, Score, CustExp, PeriodExp, LastPeriod)
		SELECT iAccount, 0, 0, iExp, 0, CURDATE()
		FROM dual
		WHERE NOT EXISTS(SELECT * FROM AccountInfo WHERE Account = iAccount);
	END IF;
END//

delimiter ;