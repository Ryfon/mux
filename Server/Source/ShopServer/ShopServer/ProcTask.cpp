﻿#include "ProcTask.h"
#include "SessionHandler.h"

CProcTask::CProcTask(void)
: m_DataProc(0)
{
	m_DataProc = new CDataProc(*this);
}

CProcTask::~CProcTask(void)
{
	if (m_DataProc)
	{
		delete m_DataProc;
		m_DataProc = 0;
	}
}

int CProcTask::init(int argc, ACE_TCHAR** argv)
{
	ACE_UNUSED_ARG(argc);
	ACE_UNUSED_ARG(argv);
	return 0;
}
int CProcTask::open(void *args)
{
	ACE_UNUSED_ARG(args);
	this->msg_queue()->high_water_mark(MAX_QUE_SIZE);
	this->msg_queue()->low_water_mark(MAX_QUE_SIZE);
	//启动线程池
	this->activate(THR_NEW_LWP | THR_JOINABLE | THR_INHERIT_SCHED, MAX_WORKER_THREAD_NUM);
	return 0;
}

int CProcTask::ProcMsg(ACE_Message_Block *msg)
{
	int nResult = 0;
	unsigned long ulPtr = *((unsigned long*)(msg->base() + msg->length() - sizeof(CSessionHandler*)));
	CSessionHandler* pSessionHandler = (CSessionHandler*)(ulPtr);
	//调用CDataProc的接口处理业务
	nResult = m_DataProc->ProcMsg(pSessionHandler, msg);
	return nResult;
}

int CProcTask::svc()
{
	ACE_Message_Block* pMsg = 0;
	while (true)
	{
		if (-1 == getq(pMsg))
		{
			//ACE_DEBUG
			break;
		}
		
		if (-1 == ProcMsg(pMsg))
		{
			break;
		}
	}
	ACE_DEBUG((LM_INFO, ACE_TEXT("ProcTask Thread quit.\n")));
	return 0;
}

int CProcTask::OnConnected(CSessionHandler* pSessionHandler)
{
	ACE_INET_Addr PeerAddr;
	pSessionHandler->peer().get_remote_addr(PeerAddr);
	ACE_DEBUG((LM_INFO, ACE_TEXT("gate srv %s:%d connected.\n"), PeerAddr.get_host_addr(), PeerAddr.get_port_number()));
	m_DataProc->OnConnected(pSessionHandler);
	return 0;
}

int CProcTask::OnDataRecv(CSessionHandler * pSessionHandler, ACE_Message_Block *pMsg)
{
	//处理不完整的包 注意：这个处理会修改批Msg中的数据
	ProcPartMsg(pSessionHandler, pMsg);
	//处理剩下的数据
	ACE_Message_Block* pPartData = 0;
	while (pMsg->length())
	{
		if (pMsg->length() < HEADERSIZE)
		{
			ACE_NEW_RETURN(pPartData, ACE_Message_Block(MAX_PACK_SIZE / 2), -1);
			pPartData->copy(pMsg->rd_ptr(), pMsg->length());
			pMsg->rd_ptr(pMsg->length());
			m_SessionPartData.bind(pSessionHandler->get_handle(), pPartData);
			break;
		}

		ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pMsg->rd_ptr());
		if (pMsgHeader->PackLen > pMsg->length())
		{
			ACE_NEW_RETURN(pPartData, ACE_Message_Block(MAX_PACK_SIZE / 2), -1);
			pPartData->copy(pMsg->rd_ptr(), pMsg->length());
			pMsg->rd_ptr(pMsg->length());
			m_SessionPartData.bind(pSessionHandler->get_handle(), pPartData);
			break;
		}
		else
		{
			ACE_Message_Block* pData = 0;
			ACE_NEW_RETURN(pData, ACE_Message_Block(pMsgHeader->PackLen + sizeof(pSessionHandler)), -1);
			pData->copy(pMsg->rd_ptr(), pMsgHeader->PackLen);
			m_DataProc->OnPacket(pSessionHandler, pData);
			pMsg->rd_ptr(pMsgHeader->PackLen);
		}
	}
	pMsg->release();
	return 0;
}

int CProcTask::OnConnectionClose(CSessionHandler * pSessionHandler, int nCloseType)
{
	ACE_DEBUG((LM_INFO, ACE_TEXT("gate srv disconnected.\n")));
	ACE_UNUSED_ARG(nCloseType);
	//检查该连接上的不完整数据，如果存在，删除之
	ACE_Message_Block* pPartData = 0;
	int nRet = m_SessionPartData.find(pSessionHandler->get_handle(), pPartData);
	if (nRet != -1)
	{
		pPartData->release();
		m_SessionPartData.unbind(pSessionHandler->get_handle());
	}

	m_DataProc->OnConnectionClose(pSessionHandler);
	return 0;;
}

int CProcTask::ProcPartMsg(CSessionHandler* pSessionHandler, ACE_Message_Block* pMsg)
{
	//检查该连接是否存在上次不完整的消息，如果存在，则合并为一个完整的消息
	ACE_Message_Block* pPartData = 0;
	int nRet = m_SessionPartData.find(pSessionHandler->get_handle(), pPartData);
	if (nRet != -1)
	{
		//如果剩余的消息不足一个包头，则从新消息中凑足包头
		size_t nPartLen = pPartData->length();
		size_t nDataLen = pMsg->length();
		size_t nCopyLen = 0;
		if (nPartLen + nDataLen < HEADERSIZE)
		{
			nCopyLen = nDataLen;
		}
		if (nPartLen < HEADERSIZE)
		{
			nCopyLen = HEADERSIZE - nPartLen;
		}
		if (nCopyLen != 0)
		{
			pPartData->copy(pMsg->rd_ptr(), nCopyLen);
			pMsg->rd_ptr(nCopyLen);
		}

		//数据仍不足消息头 此时pMsg消息已空	
		if (pPartData->length() < HEADERSIZE)
		{
			return 0;
		}
		//组完整数据包
		ShopServer::MsgHeader* pMsgHeader = reinterpret_cast<ShopServer::MsgHeader*>(pPartData->rd_ptr());
		nPartLen = pPartData->length();
		nDataLen = pMsg->length();
		if (nPartLen + nDataLen < pMsgHeader->PackLen)
		{
			nCopyLen = nDataLen;
		}
		else
		{
			nCopyLen = pMsgHeader->PackLen - nPartLen;
		}
		pPartData->copy(pMsg->rd_ptr(), nCopyLen);
		pMsg->rd_ptr(nCopyLen);
		if (pPartData->length() == pMsgHeader->PackLen)
		{
			m_DataProc->OnPacket(pSessionHandler, pPartData);
			m_SessionPartData.unbind(pSessionHandler->get_handle());
		}
	}
	return 0;
}

int CProcTask::fini()
{
	m_DataProc->ShopExit();
	return 0;
}

int CProcTask::wait()
{
	parent::wait();
	return 0;
}

