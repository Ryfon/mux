﻿#include "XmlManager.h"

CElement::CElement(unsigned int uiParentIndex, unsigned int uiIndex, const char *pszName, unsigned int uiLevel, unsigned int uiAttrNum)
: m_uiParentIndex(uiParentIndex), m_uiIndex(uiIndex), m_uiLevel(uiLevel), m_uiAttrNum(uiAttrNum)
{
	ACE_OS::strncpy(m_szName, pszName, sizeof(m_szName) );
	memset(m_szValue, 0x00, sizeof(m_szValue));
}

CElement::~CElement(void)
{

}
	
const char * CElement::Name(void)
{
	return this->m_szName;
}

unsigned int CElement::AttrNum(void)
{
	return this->m_uiAttrNum;
}

void CElement::AttrNum(unsigned int uiNum)
{
	this->m_uiAttrNum = uiNum;
}

unsigned int CElement::Level(void)
{
	return this->m_uiLevel;
}

unsigned int CElement::ParentIndex(void)
{
	return this->m_uiParentIndex;
}

unsigned int CElement::Index(void)
{
	return this->m_uiIndex;
}	

void CElement::AddAttr(const char* pszAttrName, const char* pszAttrValue)
{
	if((NULL == pszAttrName) || (NULL == pszAttrValue))
		return;
	
	m_attrList.insert(make_pair(std::string(pszAttrName), std::string(pszAttrValue)));
	return;
}

const char* CElement::GetAttr(const char* pszAttrName)
{
	if(NULL == pszAttrName)
		return NULL;

	std::map<std::string, std::string>::iterator iter = m_attrList.find(std::string(pszAttrName));
	if(iter != m_attrList.end())
		return iter->second.c_str();
	else
		return NULL;
}

const char* CElement::Value(void)
{
	return this->m_szValue;
}

void CElement::Value(const char *Value)
{
	if(NULL == Value)
		return;

	memcpy(m_szValue, Value, sizeof(m_szValue));
	return;
}

CXmlManager::CXmlManager(void)
{
	
}

CXmlManager::~CXmlManager(void)
{
	Clear();
}

bool CXmlManager::Load(const char* pszXmlFile)
{
	if(pszXmlFile == NULL)
		return false;

	//清理
	Clear();

	TiXmlDocument xmlConfigFile;
	bool bResult = xmlConfigFile.LoadFile(pszXmlFile);
	if(!bResult)
	{
		ACE_ERROR_RETURN((LM_ERROR, "%p\n", ACE_TEXT("tinyxml读取文件失败")), false);
	}

	TiXmlElement* pRoot = xmlConfigFile.RootElement();
	if(NULL == pRoot)
	{
		ACE_ERROR_RETURN((LM_ERROR, "%p\n", ACE_TEXT("root为空")), false);
	}

	unsigned int uiSelfIndex = 0;
	Parse(pRoot, 0, uiSelfIndex, 0);
	
	return true;
}

bool CXmlManager::Reload(const char* pszXmlFile)
{
	return Load(pszXmlFile);
}

void CXmlManager::Clear(void)
{
	std::vector<CElement*>::iterator iter;
	for(iter = m_elementSet.begin(); iter != m_elementSet.end(); iter++)
	{
		delete *iter;
	}
	m_elementSet.clear();
}

CElement * CXmlManager::Root(void)
{
	if ( m_elementSet.empty() )
		return NULL;
	else if( m_elementSet[0]->Index() == 0)
		return m_elementSet[0];
	else
		return NULL;
}

void CXmlManager::FindChildElements(CElement *pParent, std::vector<CElement *> &childElements)
{
	unsigned int uiIndex,uiLevel;

	// 有效性
	if(pParent == NULL)
		return;

	uiIndex = pParent->Index();
	if(uiIndex >= m_elementSet.size())
		return;

	uiLevel = pParent->Level();
	for(unsigned int i = uiIndex + 1; i < m_elementSet.size(); i++ )
	{
		if(m_elementSet[i]->Level() <= uiLevel)
			break;

		childElements.push_back(m_elementSet[i]);
	}

}
void CXmlManager::FindChildElements(unsigned int uiParentIndex, std::vector<CElement *> &childElements)
{
	// 索引的有效性
	if(uiParentIndex >= m_elementSet.size())
		return;

	std::vector<CElement*>::iterator iter;
	for(iter = m_elementSet.begin(); iter != m_elementSet.end(); iter++)
	{

		if((*iter)->ParentIndex() == uiParentIndex)
			childElements.push_back(*iter);
	}

	return;
}

void CXmlManager::FindSameLevelElements(unsigned int uiLevel, std::vector<CElement *> &childElements)
{
	std::vector<CElement*>::iterator iter;
	for(iter = m_elementSet.begin(); iter != m_elementSet.end(); iter++)
	{
		if((*iter)->Level() == uiLevel)
			childElements.push_back(*iter);
	}

	return;
}

bool CXmlManager::Parse(const TiXmlElement *pXmlElement, unsigned int uiParentIndex, unsigned int &uiSelfIndex, unsigned int uiLevelIndex)
{
	if(NULL == pXmlElement)
	{
		ACE_ERROR_RETURN((LM_ERROR, "%p\n", ACE_TEXT("pXmlElement为空")), true);
	}

	// 初始化并插入自身
	CElement *pElement = new CElement(uiParentIndex, uiSelfIndex, pXmlElement->Value(), uiLevelIndex, 0);
	
	if(NULL != pElement)
	{		
		unsigned int uiAttrNum = 0;
		const TiXmlAttribute *pAttribute = pXmlElement->FirstAttribute();/// 属性列表
		while(NULL != pAttribute)
		{
			pElement->AddAttr(pAttribute->Name(), pAttribute->Value());

			pAttribute = pAttribute->Next();
			uiAttrNum++;
		}

		pElement->AttrNum(uiAttrNum);///属性数目
		pElement->Value(pXmlElement->GetText());/// 文本

		//插入
		m_elementSet.push_back(pElement);

		//唯一索引自加
		uiSelfIndex++;
	}

	//如果有子元素，遍历子元素
	const TiXmlElement* pXmlChild = pXmlElement->FirstChildElement();
	for(; NULL != pXmlChild; pXmlChild = pXmlChild->NextSiblingElement())
	{
		Parse(pXmlChild, pElement->Index(), uiSelfIndex, pElement->Level() + 1);
	}

	return true;
}

