﻿#include "ShopSvrProtocol.h"


#define	FAILEDRETCODE		0XFFFFFFFF

ShopServer::ShopSrvProtocol::ShopSrvProtocol()
{
	m_mapEnCodeFunc[0x1801]	=	&ShopSrvProtocol::EnCode__GSQueryAccountInfo;
	m_mapDeCodeFunc[0x1801]	=	&ShopSrvProtocol::DeCode__GSQueryAccountInfo;

	m_mapEnCodeFunc[0x8101]	=	&ShopSrvProtocol::EnCode__SGAccountInfo;
	m_mapDeCodeFunc[0x8101]	=	&ShopSrvProtocol::DeCode__SGAccountInfo;

	m_mapEnCodeFunc[0x1802]	=	&ShopSrvProtocol::EnCode__GSPurchaseItem;
	m_mapDeCodeFunc[0x1802]	=	&ShopSrvProtocol::DeCode__GSPurchaseItem;

	m_mapEnCodeFunc[0x8102]	=	&ShopSrvProtocol::EnCode__SGPurchaseItem;
	m_mapDeCodeFunc[0x8102]	=	&ShopSrvProtocol::DeCode__SGPurchaseItem;

	m_mapEnCodeFunc[0x1803]	=	&ShopSrvProtocol::EnCode__GSQueryVipInfo;
	m_mapDeCodeFunc[0x1803]	=	&ShopSrvProtocol::DeCode__GSQueryVipInfo;

	m_mapEnCodeFunc[0x8103]	=	&ShopSrvProtocol::EnCode__SGVipInfo;
	m_mapDeCodeFunc[0x8103]	=	&ShopSrvProtocol::DeCode__SGVipInfo;

	m_mapEnCodeFunc[0x1804]	=	&ShopSrvProtocol::EnCode__GSPurchaseVip;
	m_mapDeCodeFunc[0x1804]	=	&ShopSrvProtocol::DeCode__GSPurchaseVip;

	m_mapEnCodeFunc[0x8104]	=	&ShopSrvProtocol::EnCode__SGPurchaseVip;
	m_mapDeCodeFunc[0x8104]	=	&ShopSrvProtocol::DeCode__SGPurchaseVip;

	m_mapEnCodeFunc[0x1805]	=	&ShopSrvProtocol::EnCode__GSFetchVipItem;
	m_mapDeCodeFunc[0x1805]	=	&ShopSrvProtocol::DeCode__GSFetchVipItem;

	m_mapEnCodeFunc[0x8105]	=	&ShopSrvProtocol::EnCode__SGFetchVipItem;
	m_mapDeCodeFunc[0x8105]	=	&ShopSrvProtocol::DeCode__SGFetchVipItem;

	m_mapEnCodeFunc[0x1806]	=	&ShopSrvProtocol::EnCode__GSFetchVipItemResult;
	m_mapDeCodeFunc[0x1806]	=	&ShopSrvProtocol::DeCode__GSFetchVipItemResult;

	m_mapEnCodeFunc[0x1807]	=	&ShopSrvProtocol::EnCode__GSPurchaseGift;
	m_mapDeCodeFunc[0x1807]	=	&ShopSrvProtocol::DeCode__GSPurchaseGift;

	m_mapEnCodeFunc[0x8107]	=	&ShopSrvProtocol::EnCode__SGPurchaseGift;
	m_mapDeCodeFunc[0x8107]	=	&ShopSrvProtocol::DeCode__SGPurchaseGift;

	m_mapEnCodeFunc[0x1808]	=	&ShopSrvProtocol::EnCode__GSTranFailed;
	m_mapDeCodeFunc[0x1808]	=	&ShopSrvProtocol::DeCode__GSTranFailed;

	m_mapEnCodeFunc[0x1809]	=	&ShopSrvProtocol::EnCode__GSQueryCachedItem;
	m_mapDeCodeFunc[0x1809]	=	&ShopSrvProtocol::DeCode__GSQueryCachedItem;

	m_mapEnCodeFunc[0x8109]	=	&ShopSrvProtocol::EnCode__SGQueryCachedItem;
	m_mapDeCodeFunc[0x8109]	=	&ShopSrvProtocol::DeCode__SGQueryCachedItem;

	m_mapEnCodeFunc[0x1810]	=	&ShopSrvProtocol::EnCode__GSFetchCachedItem;
	m_mapDeCodeFunc[0x1810]	=	&ShopSrvProtocol::DeCode__GSFetchCachedItem;

	m_mapEnCodeFunc[0x8110]	=	&ShopSrvProtocol::EnCode__SGFetchCachedItem;
	m_mapDeCodeFunc[0x8110]	=	&ShopSrvProtocol::DeCode__SGFetchCachedItem;

	m_mapEnCodeFunc[0x1811]	=	&ShopSrvProtocol::EnCode__GSPlayerNotify;
	m_mapDeCodeFunc[0x1811]	=	&ShopSrvProtocol::DeCode__GSPlayerNotify;

	m_mapEnCodeFunc[0x8112]	=	&ShopSrvProtocol::EnCode__SGQryRoleInfo;
	m_mapDeCodeFunc[0x8112]	=	&ShopSrvProtocol::DeCode__SGQryRoleInfo;

	m_mapEnCodeFunc[0x1812]	=	&ShopSrvProtocol::EnCode__GSQryRoleInfo;
	m_mapDeCodeFunc[0x1812]	=	&ShopSrvProtocol::DeCode__GSQryRoleInfo;

	m_mapEnCodeFunc[0x1813]	=	&ShopSrvProtocol::EnCode__GSShopRank;
	m_mapDeCodeFunc[0x1813]	=	&ShopSrvProtocol::DeCode__GSShopRank;

	m_mapEnCodeFunc[0x8113]	=	&ShopSrvProtocol::EnCode__SGShopRank;
	m_mapDeCodeFunc[0x8113]	=	&ShopSrvProtocol::DeCode__SGShopRank;

	m_mapEnCodeFunc[0x1814]	=	&ShopSrvProtocol::EnCode__GSPlayerGuildInfo;
	m_mapDeCodeFunc[0x1814]	=	&ShopSrvProtocol::DeCode__GSPlayerGuildInfo;

	m_mapEnCodeFunc[0x8114]	=	&ShopSrvProtocol::EnCode__SGGuildPrestigeAdd;
	m_mapDeCodeFunc[0x8114]	=	&ShopSrvProtocol::DeCode__SGGuildPrestigeAdd;

}

ShopServer::ShopSrvProtocol::~ShopSrvProtocol()
{
}

size_t	ShopServer::ShopSrvProtocol::EnCode(int nMessageId, void* pInData, char* pBuf, size_t unSize)
{
	EnCodeFunc pEnCodeFunc = FindEnCodeFunc(nMessageId);
	if(pEnCodeFunc != NULL)
	{
		m_kPackage.Init(pBuf, unSize);
		return	(this->*pEnCodeFunc)(pInData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

size_t	ShopServer::ShopSrvProtocol::DeCode(int nMessageId, void* pOutData, size_t nOutSize, const char* pBuf, size_t unSize)
{
	DeCodeFunc pDeCodeFunc = FindDeCodeFunc(nMessageId);
	if(pDeCodeFunc != NULL)
	{
		m_kPackage.Init((char*)pBuf, unSize, nOutSize);
		return	(this->*pDeCodeFunc)(pOutData);
	}
	else
	{
		return FAILEDRETCODE;
	}
}

ShopServer::EnCodeFunc	ShopServer::ShopSrvProtocol::FindEnCodeFunc(int nMessageId)
{
	std::map<int, EnCodeFunc>::iterator itr = m_mapEnCodeFunc.find(nMessageId);
	if(itr != m_mapEnCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

ShopServer::DeCodeFunc	ShopServer::ShopSrvProtocol::FindDeCodeFunc(int nMessageId)
{
	std::map<int, DeCodeFunc>::iterator itr = m_mapDeCodeFunc.find(nMessageId);
	if(itr != m_mapDeCodeFunc.end())
	{
		return	itr->second;
	}
	else
	{
		return NULL;
	}
}

size_t	ShopServer::ShopSrvProtocol::EnCode__S_PlayerID(void* pData)
{
	S_PlayerID* pkS_PlayerID = (S_PlayerID*)(pData);

	//EnCode wGID
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkS_PlayerID->wGID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode dwPID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkS_PlayerID->dwPID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__S_PlayerID(void* pData)
{
	S_PlayerID* pkS_PlayerID = (S_PlayerID*)(pData);

	//DeCode wGID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkS_PlayerID->wGID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode dwPID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkS_PlayerID->dwPID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(S_PlayerID);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__MsgHeader(void* pData)
{
	MsgHeader* pkMsgHeader = (MsgHeader*)(pData);

	//EnCode PackLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMsgHeader->PackLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PackType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkMsgHeader->PackType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PackCmd
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkMsgHeader->PackCmd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__MsgHeader(void* pData)
{
	MsgHeader* pkMsgHeader = (MsgHeader*)(pData);

	//DeCode PackLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMsgHeader->PackLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PackType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkMsgHeader->PackType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PackCmd
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkMsgHeader->PackCmd), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(MsgHeader);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSQueryAccountInfo(void* pData)
{
	GSQueryAccountInfo* pkGSQueryAccountInfo = (GSQueryAccountInfo*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSQueryAccountInfo->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSQueryAccountInfo->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSQueryAccountInfo->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSQueryAccountInfo->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSQueryAccountInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSQueryAccountInfo(void* pData)
{
	GSQueryAccountInfo* pkGSQueryAccountInfo = (GSQueryAccountInfo*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSQueryAccountInfo->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSQueryAccountInfo->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSQueryAccountInfo->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSQueryAccountInfo->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSQueryAccountInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSQueryAccountInfo);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGAccountInfo(void* pData)
{
	SGAccountInfo* pkSGAccountInfo = (SGAccountInfo*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGAccountInfo->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGAccountInfo->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGAccountInfo->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGAccountInfo->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGAccountInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode GameMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGAccountInfo->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Score
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGAccountInfo->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGAccountInfo->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGAccountInfo->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGAccountInfo->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkSGAccountInfo->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSGAccountInfo->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGAccountInfo(void* pData)
{
	SGAccountInfo* pkSGAccountInfo = (SGAccountInfo*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGAccountInfo->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGAccountInfo->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGAccountInfo->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGAccountInfo->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGAccountInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode GameMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGAccountInfo->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Score
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGAccountInfo->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGAccountInfo->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGAccountInfo->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGAccountInfo->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkSGAccountInfo->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSGAccountInfo->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SGAccountInfo);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSPurchaseItem(void* pData)
{
	GSPurchaseItem* pkGSPurchaseItem = (GSPurchaseItem*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPurchaseItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSPurchaseItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseItem->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSPurchaseItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSPurchaseItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPurchaseItem->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CliCalc
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseItem->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSPurchaseItem(void* pData)
{
	GSPurchaseItem* pkGSPurchaseItem = (GSPurchaseItem*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPurchaseItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSPurchaseItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseItem->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSPurchaseItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSPurchaseItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPurchaseItem->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CliCalc
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseItem->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSPurchaseItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGPurchaseItem(void* pData)
{
	SGPurchaseItem* pkSGPurchaseItem = (SGPurchaseItem*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGPurchaseItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseItem->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGPurchaseItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGPurchaseItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode TransID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseItem->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GameMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseItem->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Score
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseItem->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseItem->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ComsumeNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseItem->ComsumeNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseItem->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseItem->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseItem->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkSGPurchaseItem->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSGPurchaseItem->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGPurchaseItem(void* pData)
{
	SGPurchaseItem* pkSGPurchaseItem = (SGPurchaseItem*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGPurchaseItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseItem->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGPurchaseItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGPurchaseItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode TransID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseItem->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GameMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseItem->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Score
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseItem->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseItem->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ComsumeNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseItem->ComsumeNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseItem->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseItem->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseItem->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkSGPurchaseItem->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSGPurchaseItem->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SGPurchaseItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSQueryVipInfo(void* pData)
{
	GSQueryVipInfo* pkGSQueryVipInfo = (GSQueryVipInfo*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSQueryVipInfo->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSQueryVipInfo->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSQueryVipInfo->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSQueryVipInfo->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSQueryVipInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSQueryVipInfo->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSQueryVipInfo(void* pData)
{
	GSQueryVipInfo* pkGSQueryVipInfo = (GSQueryVipInfo*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSQueryVipInfo->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSQueryVipInfo->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSQueryVipInfo->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSQueryVipInfo->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSQueryVipInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSQueryVipInfo->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSQueryVipInfo);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGVipInfo(void* pData)
{
	SGVipInfo* pkSGVipInfo = (SGVipInfo*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGVipInfo->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGVipInfo->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGVipInfo->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGVipInfo->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGVipInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGVipInfo->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGVipInfo->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode LeftDays
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSGVipInfo->LeftDays), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Fetched
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGVipInfo->Fetched), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSGVipInfo->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGVipInfo(void* pData)
{
	SGVipInfo* pkSGVipInfo = (SGVipInfo*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGVipInfo->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGVipInfo->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGVipInfo->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGVipInfo->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGVipInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGVipInfo->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGVipInfo->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode LeftDays
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSGVipInfo->LeftDays), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Fetched
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGVipInfo->Fetched), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSGVipInfo->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SGVipInfo);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSPurchaseVip(void* pData)
{
	GSPurchaseVip* pkGSPurchaseVip = (GSPurchaseVip*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPurchaseVip->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSPurchaseVip->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseVip->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSPurchaseVip->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSPurchaseVip->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseVip->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseVip->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CliCalc
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseVip->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSPurchaseVip(void* pData)
{
	GSPurchaseVip* pkGSPurchaseVip = (GSPurchaseVip*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPurchaseVip->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSPurchaseVip->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseVip->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSPurchaseVip->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSPurchaseVip->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseVip->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseVip->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CliCalc
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseVip->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSPurchaseVip);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGPurchaseVip(void* pData)
{
	SGPurchaseVip* pkSGPurchaseVip = (SGPurchaseVip*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseVip->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGPurchaseVip->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseVip->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGPurchaseVip->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGPurchaseVip->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode TransID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseVip->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseVip->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseVip->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GameMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseVip->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Score
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseVip->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ComsumeNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseVip->ComsumeNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseVip->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseVip->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseVip->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkSGPurchaseVip->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSGPurchaseVip->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGPurchaseVip(void* pData)
{
	SGPurchaseVip* pkSGPurchaseVip = (SGPurchaseVip*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseVip->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGPurchaseVip->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseVip->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGPurchaseVip->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGPurchaseVip->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode TransID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseVip->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseVip->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseVip->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GameMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseVip->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Score
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseVip->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ComsumeNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseVip->ComsumeNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseVip->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseVip->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseVip->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkSGPurchaseVip->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSGPurchaseVip->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SGPurchaseVip);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSFetchVipItem(void* pData)
{
	GSFetchVipItem* pkGSFetchVipItem = (GSFetchVipItem*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSFetchVipItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSFetchVipItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSFetchVipItem->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSFetchVipItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSFetchVipItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSFetchVipItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSFetchVipItem->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSFetchVipItem(void* pData)
{
	GSFetchVipItem* pkGSFetchVipItem = (GSFetchVipItem*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSFetchVipItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSFetchVipItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSFetchVipItem->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSFetchVipItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSFetchVipItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSFetchVipItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSFetchVipItem->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSFetchVipItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__VipItem(void* pData)
{
	VipItem* pkVipItem = (VipItem*)(pData);

	//EnCode ItemTypeID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkVipItem->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkVipItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__VipItem(void* pData)
{
	VipItem* pkVipItem = (VipItem*)(pData);

	//DeCode ItemTypeID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkVipItem->ItemTypeID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkVipItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(VipItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGFetchVipItem(void* pData)
{
	SGFetchVipItem* pkSGFetchVipItem = (SGFetchVipItem*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGFetchVipItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGFetchVipItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGFetchVipItem->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGFetchVipItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGFetchVipItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGFetchVipItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGFetchVipItem->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipItemLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGFetchVipItem->VipItemLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipItemArr
	if((int)ITEMS_PER_PAGE < (int)pkSGFetchVipItem->VipItemLen)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkSGFetchVipItem->VipItemLen; ++i)
	{
		if(EnCode__VipItem(&(pkSGFetchVipItem->VipItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//EnCode Result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSGFetchVipItem->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGFetchVipItem(void* pData)
{
	SGFetchVipItem* pkSGFetchVipItem = (SGFetchVipItem*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGFetchVipItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGFetchVipItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGFetchVipItem->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGFetchVipItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGFetchVipItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGFetchVipItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGFetchVipItem->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipItemLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGFetchVipItem->VipItemLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipItemArr
	if((int)ITEMS_PER_PAGE < (int)pkSGFetchVipItem->VipItemLen)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkSGFetchVipItem->VipItemLen; ++i)
	{
		if(DeCode__VipItem(&(pkSGFetchVipItem->VipItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	//DeCode Result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSGFetchVipItem->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SGFetchVipItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSFetchVipItemResult(void* pData)
{
	GSFetchVipItemResult* pkGSFetchVipItemResult = (GSFetchVipItemResult*)(pData);

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSFetchVipItemResult->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSFetchVipItemResult->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSFetchVipItemResult->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSFetchVipItemResult->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSFetchVipItemResult->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSFetchVipItemResult->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode VipID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSFetchVipItemResult->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkGSFetchVipItemResult->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSFetchVipItemResult(void* pData)
{
	GSFetchVipItemResult* pkGSFetchVipItemResult = (GSFetchVipItemResult*)(pData);

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSFetchVipItemResult->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSFetchVipItemResult->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSFetchVipItemResult->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSFetchVipItemResult->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSFetchVipItemResult->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSFetchVipItemResult->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode VipID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSFetchVipItemResult->VipID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkGSFetchVipItemResult->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSFetchVipItemResult);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSPurchaseGift(void* pData)
{
	GSPurchaseGift* pkGSPurchaseGift = (GSPurchaseGift*)(pData);

	//EnCode SrcAccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPurchaseGift->SrcAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SrcAccount
	if((int)MAX_NAME_SIZE < (int)pkGSPurchaseGift->SrcAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseGift->SrcAccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSPurchaseGift->SrcAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSPurchaseGift->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode SrcRoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseGift->SrcRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPurchaseGift->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseGift->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseGift->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPurchaseGift->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccount
	if((int)MAX_NAME_SIZE < (int)pkGSPurchaseGift->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseGift->DestAccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSPurchaseGift->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestRoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseGift->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CliCalc
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPurchaseGift->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NoteLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPurchaseGift->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Note
	if((int)MAX_NOTE_SIZE < (int)pkGSPurchaseGift->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseGift->NoteLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSPurchaseGift->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSPurchaseGift(void* pData)
{
	GSPurchaseGift* pkGSPurchaseGift = (GSPurchaseGift*)(pData);

	//DeCode SrcAccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPurchaseGift->SrcAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SrcAccount
	if((int)MAX_NAME_SIZE < (int)pkGSPurchaseGift->SrcAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseGift->SrcAccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSPurchaseGift->SrcAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSPurchaseGift->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode SrcRoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseGift->SrcRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPurchaseGift->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseGift->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseGift->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPurchaseGift->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccount
	if((int)MAX_NAME_SIZE < (int)pkGSPurchaseGift->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseGift->DestAccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSPurchaseGift->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestRoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseGift->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CliCalc
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPurchaseGift->CliCalc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NoteLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPurchaseGift->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Note
	if((int)MAX_NOTE_SIZE < (int)pkGSPurchaseGift->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPurchaseGift->NoteLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSPurchaseGift->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSPurchaseGift);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGPurchaseGift(void* pData)
{
	SGPurchaseGift* pkSGPurchaseGift = (SGPurchaseGift*)(pData);

	//EnCode TransID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SrcAccountLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseGift->SrcAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SrcAccount
	if((int)MAX_NAME_SIZE < (int)pkSGPurchaseGift->SrcAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseGift->SrcAccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGPurchaseGift->SrcAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SrcRoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->SrcRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGPurchaseGift->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseGift->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccount
	if((int)MAX_NAME_SIZE < (int)pkSGPurchaseGift->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseGift->DestAccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGPurchaseGift->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestRoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GameMoney
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Score
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseGift->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MoneyCost
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->MoneyCost), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode SocreCost
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->SocreCost), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberClass
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseGift->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberLevel
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseGift->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode MemberExp
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGPurchaseGift->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.Pack("FLOAT", &(pkSGPurchaseGift->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSGPurchaseGift->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NoteLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGPurchaseGift->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Note
	if((int)MAX_NOTE_SIZE < (int)pkSGPurchaseGift->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseGift->NoteLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGPurchaseGift->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGPurchaseGift(void* pData)
{
	SGPurchaseGift* pkSGPurchaseGift = (SGPurchaseGift*)(pData);

	//DeCode TransID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SrcAccountLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseGift->SrcAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SrcAccount
	if((int)MAX_NAME_SIZE < (int)pkSGPurchaseGift->SrcAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseGift->SrcAccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGPurchaseGift->SrcAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SrcRoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->SrcRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGPurchaseGift->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseGift->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccount
	if((int)MAX_NAME_SIZE < (int)pkSGPurchaseGift->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseGift->DestAccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGPurchaseGift->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestRoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GameMoney
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->GameMoney), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Score
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->Score), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseGift->PurchaseType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MoneyCost
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->MoneyCost), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode SocreCost
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->SocreCost), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberClass
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseGift->MemberClass), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberLevel
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseGift->MemberLevel), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode MemberExp
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGPurchaseGift->MemberExp), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseDisc
	unCount = 1;
	if(!m_kPackage.UnPack("FLOAT", &(pkSGPurchaseGift->PurchaseDisc), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PurchaseResult
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSGPurchaseGift->PurchaseResult), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NoteLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGPurchaseGift->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Note
	if((int)MAX_NOTE_SIZE < (int)pkSGPurchaseGift->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGPurchaseGift->NoteLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGPurchaseGift->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SGPurchaseGift);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGQryRoleInfo(void* pData)
{
	SGQryRoleInfo* pkSGQryRoleInfo = (SGQryRoleInfo*)(pData);

	//EnCode RoleNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGQryRoleInfo->RoleNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleName
	if((int)MAX_NAME_SIZE < (int)pkSGQryRoleInfo->RoleNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGQryRoleInfo->RoleNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGQryRoleInfo->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGQryRoleInfo(void* pData)
{
	SGQryRoleInfo* pkSGQryRoleInfo = (SGQryRoleInfo*)(pData);

	//DeCode RoleNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGQryRoleInfo->RoleNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleName
	if((int)MAX_NAME_SIZE < (int)pkSGQryRoleInfo->RoleNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGQryRoleInfo->RoleNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGQryRoleInfo->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SGQryRoleInfo);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSQryRoleInfo(void* pData)
{
	GSQryRoleInfo* pkGSQryRoleInfo = (GSQryRoleInfo*)(pData);

	//EnCode RoleNameLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSQryRoleInfo->RoleNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleName
	if((int)MAX_NAME_SIZE < (int)pkGSQryRoleInfo->RoleNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSQryRoleInfo->RoleNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSQryRoleInfo->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleStatus
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSQryRoleInfo->RoleStatus), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSQryRoleInfo(void* pData)
{
	GSQryRoleInfo* pkGSQryRoleInfo = (GSQryRoleInfo*)(pData);

	//DeCode RoleNameLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSQryRoleInfo->RoleNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleName
	if((int)MAX_NAME_SIZE < (int)pkGSQryRoleInfo->RoleNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSQryRoleInfo->RoleNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSQryRoleInfo->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleStatus
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSQryRoleInfo->RoleStatus), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSQryRoleInfo);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSTranFailed(void* pData)
{
	GSTranFailed* pkGSTranFailed = (GSTranFailed*)(pData);

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSTranFailed->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode TransID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSTranFailed->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSTranFailed->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSTranFailed->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSTranFailed->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSTranFailed->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSTranFailed->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSTranFailed->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSTranFailed->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSTranFailed->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestAccount
	if((int)MAX_NAME_SIZE < (int)pkGSTranFailed->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSTranFailed->DestAccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSTranFailed->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode DestRoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSTranFailed->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NoteLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSTranFailed->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Note
	if((int)MAX_NOTE_SIZE < (int)pkGSTranFailed->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSTranFailed->NoteLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSTranFailed->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSTranFailed(void* pData)
{
	GSTranFailed* pkGSTranFailed = (GSTranFailed*)(pData);

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSTranFailed->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode TransID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSTranFailed->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSTranFailed->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSTranFailed->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSTranFailed->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSTranFailed->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSTranFailed->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSTranFailed->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSTranFailed->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccountLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSTranFailed->DestAccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestAccount
	if((int)MAX_NAME_SIZE < (int)pkGSTranFailed->DestAccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSTranFailed->DestAccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSTranFailed->DestAccount), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode DestRoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSTranFailed->DestRoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NoteLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSTranFailed->NoteLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Note
	if((int)MAX_NOTE_SIZE < (int)pkGSTranFailed->NoteLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSTranFailed->NoteLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSTranFailed->Note), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSTranFailed);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSQueryCachedItem(void* pData)
{
	GSQueryCachedItem* pkGSQueryCachedItem = (GSQueryCachedItem*)(pData);

	//EnCode UseId
	if(EnCode__S_PlayerID(&(pkGSQueryCachedItem->UseId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSQueryCachedItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSQueryCachedItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSQueryCachedItem->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSQueryCachedItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSQueryCachedItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSQueryCachedItem(void* pData)
{
	GSQueryCachedItem* pkGSQueryCachedItem = (GSQueryCachedItem*)(pData);

	//DeCode UseId
	if(DeCode__S_PlayerID(&(pkGSQueryCachedItem->UseId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSQueryCachedItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSQueryCachedItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSQueryCachedItem->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSQueryCachedItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSQueryCachedItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSQueryCachedItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__TransItem(void* pData)
{
	TransItem* pkTransItem = (TransItem*)(pData);

	//EnCode TransID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransItem->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemNum
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode OpeDate
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkTransItem->OpeDate), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__TransItem(void* pData)
{
	TransItem* pkTransItem = (TransItem*)(pData);

	//DeCode TransID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransItem->TransID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransItem->ItemID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemNum
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransItem->ItemNum), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode OpeDate
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkTransItem->OpeDate), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(TransItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGQueryCachedItem(void* pData)
{
	SGQueryCachedItem* pkSGQueryCachedItem = (SGQueryCachedItem*)(pData);

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGQueryCachedItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGQueryCachedItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGQueryCachedItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGQueryCachedItem->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGQueryCachedItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGQueryCachedItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PageIndex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGQueryCachedItem->PageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemArrLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGQueryCachedItem->ItemArrLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ItemArr
	if((int)ITEMS_PER_PAGE < (int)pkSGQueryCachedItem->ItemArrLen)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkSGQueryCachedItem->ItemArrLen; ++i)
	{
		if(EnCode__TransItem(&(pkSGQueryCachedItem->ItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGQueryCachedItem(void* pData)
{
	SGQueryCachedItem* pkSGQueryCachedItem = (SGQueryCachedItem*)(pData);

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGQueryCachedItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGQueryCachedItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGQueryCachedItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGQueryCachedItem->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGQueryCachedItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGQueryCachedItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PageIndex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGQueryCachedItem->PageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemArrLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGQueryCachedItem->ItemArrLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ItemArr
	if((int)ITEMS_PER_PAGE < (int)pkSGQueryCachedItem->ItemArrLen)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkSGQueryCachedItem->ItemArrLen; ++i)
	{
		if(DeCode__TransItem(&(pkSGQueryCachedItem->ItemArr[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(SGQueryCachedItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSFetchCachedItem(void* pData)
{
	GSFetchCachedItem* pkGSFetchCachedItem = (GSFetchCachedItem*)(pData);

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSFetchCachedItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSFetchCachedItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSFetchCachedItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSFetchCachedItem->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSFetchCachedItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSFetchCachedItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CachedItem
	if(EnCode__TransItem(&(pkGSFetchCachedItem->CachedItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSFetchCachedItem(void* pData)
{
	GSFetchCachedItem* pkGSFetchCachedItem = (GSFetchCachedItem*)(pData);

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSFetchCachedItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSFetchCachedItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSFetchCachedItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSFetchCachedItem->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSFetchCachedItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSFetchCachedItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CachedItem
	if(DeCode__TransItem(&(pkGSFetchCachedItem->CachedItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSFetchCachedItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGFetchCachedItem(void* pData)
{
	SGFetchCachedItem* pkSGFetchCachedItem = (SGFetchCachedItem*)(pData);

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGFetchCachedItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGFetchCachedItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGFetchCachedItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGFetchCachedItem->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkSGFetchCachedItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGFetchCachedItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode CachedItem
	if(EnCode__TransItem(&(pkSGFetchCachedItem->CachedItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode Result
	unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkSGFetchCachedItem->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGFetchCachedItem(void* pData)
{
	SGFetchCachedItem* pkSGFetchCachedItem = (SGFetchCachedItem*)(pData);

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGFetchCachedItem->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGFetchCachedItem->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkSGFetchCachedItem->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkSGFetchCachedItem->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkSGFetchCachedItem->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGFetchCachedItem->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode CachedItem
	if(DeCode__TransItem(&(pkSGFetchCachedItem->CachedItem)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode Result
	unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkSGFetchCachedItem->Result), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SGFetchCachedItem);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSPlayerNotify(void* pData)
{
	GSPlayerNotify* pkGSPlayerNotify = (GSPlayerNotify*)(pData);

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSPlayerNotify->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPlayerNotify->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSPlayerNotify->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPlayerNotify->AccountLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSPlayerNotify->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPlayerNotify->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleNameLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPlayerNotify->RoleNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleName
	if((int)MAX_NAME_SIZE < (int)pkGSPlayerNotify->RoleNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPlayerNotify->RoleNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkGSPlayerNotify->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RaceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPlayerNotify->RaceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ClassID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPlayerNotify->ClassID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Level
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPlayerNotify->Level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode NotifyType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSPlayerNotify->NotifyType), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSPlayerNotify(void* pData)
{
	GSPlayerNotify* pkGSPlayerNotify = (GSPlayerNotify*)(pData);

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSPlayerNotify->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode AccountLen
	size_t unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPlayerNotify->AccountLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Account
	if((int)MAX_NAME_SIZE < (int)pkGSPlayerNotify->AccountLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPlayerNotify->AccountLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSPlayerNotify->Account), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPlayerNotify->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPlayerNotify->RoleNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleName
	if((int)MAX_NAME_SIZE < (int)pkGSPlayerNotify->RoleNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkGSPlayerNotify->RoleNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkGSPlayerNotify->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RaceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPlayerNotify->RaceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ClassID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPlayerNotify->ClassID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Level
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPlayerNotify->Level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode NotifyType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSPlayerNotify->NotifyType), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSPlayerNotify);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSShopRank(void* pData)
{
	GSShopRank* pkGSShopRank = (GSShopRank*)(pData);

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSShopRank->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RaceID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSShopRank->RaceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RankType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSShopRank->RankType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PageIndex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkGSShopRank->PageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSShopRank(void* pData)
{
	GSShopRank* pkGSShopRank = (GSShopRank*)(pData);

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSShopRank->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RaceID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSShopRank->RaceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RankType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSShopRank->RankType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PageIndex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkGSShopRank->PageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSShopRank);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__RankInfo(void* pData)
{
	RankInfo* pkRankInfo = (RankInfo*)(pData);

	//EnCode RankIndex
	size_t unCount = 1;
	if(!m_kPackage.Pack("USHORT", &(pkRankInfo->RankIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleNameLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkRankInfo->RoleNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleName
	if((int)MAX_NAME_SIZE < (int)pkRankInfo->RoleNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRankInfo->RoleNameLen;
	if(!m_kPackage.Pack("CHAR", &(pkRankInfo->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Level
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRankInfo->Level), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RaceID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRankInfo->RaceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode ClassID
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRankInfo->ClassID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode Cost
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkRankInfo->Cost), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__RankInfo(void* pData)
{
	RankInfo* pkRankInfo = (RankInfo*)(pData);

	//DeCode RankIndex
	size_t unCount = 1;
	if(!m_kPackage.UnPack("USHORT", &(pkRankInfo->RankIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleNameLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkRankInfo->RoleNameLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleName
	if((int)MAX_NAME_SIZE < (int)pkRankInfo->RoleNameLen)
	{
		return FAILEDRETCODE;
	}

	unCount = pkRankInfo->RoleNameLen;
	if(!m_kPackage.UnPack("CHAR", &(pkRankInfo->RoleName), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Level
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRankInfo->Level), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RaceID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRankInfo->RaceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode ClassID
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRankInfo->ClassID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode Cost
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkRankInfo->Cost), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(RankInfo);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGShopRank(void* pData)
{
	SGShopRank* pkSGShopRank = (SGShopRank*)(pData);

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGShopRank->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RaceID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGShopRank->RaceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RankType
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGShopRank->RankType), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PageIndex
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGShopRank->PageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RanksLen
	unCount = 1;
	if(!m_kPackage.Pack("BYTE", &(pkSGShopRank->RanksLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode RankList
	if((int)PER_PAGE_RANKS < (int)pkSGShopRank->RanksLen)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkSGShopRank->RanksLen; ++i)
	{
		if(EnCode__RankInfo(&(pkSGShopRank->RankList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGShopRank(void* pData)
{
	SGShopRank* pkSGShopRank = (SGShopRank*)(pData);

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGShopRank->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RaceID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGShopRank->RaceID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RankType
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGShopRank->RankType), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PageIndex
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGShopRank->PageIndex), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RanksLen
	unCount = 1;
	if(!m_kPackage.UnPack("BYTE", &(pkSGShopRank->RanksLen), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode RankList
	if((int)PER_PAGE_RANKS < (int)pkSGShopRank->RanksLen)
	{
		return FAILEDRETCODE;
	}

	for(BYTE i = 0; i < pkSGShopRank->RanksLen; ++i)
	{
		if(DeCode__RankInfo(&(pkSGShopRank->RankList[i])) == FAILEDRETCODE)
		{
			return FAILEDRETCODE;
		}
	}

	return sizeof(SGShopRank);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__GSPlayerGuildInfo(void* pData)
{
	GSPlayerGuildInfo* pkGSPlayerGuildInfo = (GSPlayerGuildInfo*)(pData);

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkGSPlayerGuildInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPlayerGuildInfo->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode GuildNo
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkGSPlayerGuildInfo->GuildNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__GSPlayerGuildInfo(void* pData)
{
	GSPlayerGuildInfo* pkGSPlayerGuildInfo = (GSPlayerGuildInfo*)(pData);

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkGSPlayerGuildInfo->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPlayerGuildInfo->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode GuildNo
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkGSPlayerGuildInfo->GuildNo), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(GSPlayerGuildInfo);
}

size_t	ShopServer::ShopSrvProtocol::EnCode__SGGuildPrestigeAdd(void* pData)
{
	SGGuildPrestigeAdd* pkSGGuildPrestigeAdd = (SGGuildPrestigeAdd*)(pData);

	//EnCode UserId
	if(EnCode__S_PlayerID(&(pkSGGuildPrestigeAdd->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//EnCode RoleID
	size_t unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGGuildPrestigeAdd->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//EnCode PrestigeAdd
	unCount = 1;
	if(!m_kPackage.Pack("UINT", &(pkSGGuildPrestigeAdd->PrestigeAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	return m_kPackage.GetBufDataLen();
}

size_t	ShopServer::ShopSrvProtocol::DeCode__SGGuildPrestigeAdd(void* pData)
{
	SGGuildPrestigeAdd* pkSGGuildPrestigeAdd = (SGGuildPrestigeAdd*)(pData);

	//DeCode UserId
	if(DeCode__S_PlayerID(&(pkSGGuildPrestigeAdd->UserId)) == FAILEDRETCODE)
	{
		return FAILEDRETCODE;
	}

	//DeCode RoleID
	size_t unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGGuildPrestigeAdd->RoleID), unCount))
	{
		return FAILEDRETCODE;
	}

	//DeCode PrestigeAdd
	unCount = 1;
	if(!m_kPackage.UnPack("UINT", &(pkSGGuildPrestigeAdd->PrestigeAdd), unCount))
	{
		return FAILEDRETCODE;
	}

	return sizeof(SGGuildPrestigeAdd);
}

