﻿/** @file MethodObject.h 
@brief 方法对象
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-04-16
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef METHODOBJECT_H
#define METHODOBJECT_H
#include "Common.h"
#include "ShopSvrProtocol.h"

class CConnection;
class CQuery;
class CSessionHandler;
class CDataProc;

class CMethodObject: public ACE_Method_Object
{
public:
	CMethodObject(CConnection* pConnetion, CDataProc& DataProc);		
	virtual ~CMethodObject(void);
protected:
	int GetAccountInfo(const char* szAccount, UINT& nGameMoney, UINT& nScore, BYTE& nMemberClass, BYTE& nMemberLevel, UINT& nMemberExp, FLOAT& fPurchaseDisc);
	int PurchaseCheck(const char* szAccount, BYTE nServiceType, UINT nServiceID, UINT nServiceNum, BYTE nPurchaseType, BYTE nPurchaseDest, UINT& nServiceMoney, UINT& nAdditionMoney);
	int GetVipInfo(const char* szAccount, unsigned int RoleID, UINT& nVipID, USHORT& nLeftDays, BYTE& nFetched);
	int UpdateConsumeRank(unsigned int nRoleID, int nCost);
	int UpdateGuildConsume(unsigned int nRoleID, int nCost, unsigned int& nPrestigeAdd);
	int SendPrestigeAddMsg(CSessionHandler* pSession, ShopServer::S_PlayerID PID, unsigned int nRoleID, unsigned int nPrestigeAdd);
protected:
	CConnection* m_DbConn;
	CDataProc& m_DataProc;
};
//查询帐号余额方法对象
class CQryAccountObject: public CMethodObject
{
public:
	CQryAccountObject(char* szAccount, ShopServer::S_PlayerID PId, CConnection* pConnection, CSessionHandler* pSession, CDataProc& DataProc);
	virtual ~CQryAccountObject(void);
public:
	virtual int call(void);
private:
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
};

//查询Vip信息方法对象
class CQryVipInfoObject: public CMethodObject
{
public:
	CQryVipInfoObject(char* szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, CConnection* pConnection, CSessionHandler* pSession, CDataProc& DataProc);
	virtual ~CQryVipInfoObject();
public:
	virtual int call(void);
private:
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RoleID;
};

//购买道具方法对象
class CPurchaseItemObject: public CMethodObject
{
public:
	CPurchaseItemObject(char* szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned char ucPurchaseType,
		unsigned int uItemID, int nItemNum, unsigned int uCliCalc,
		CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CPurchaseItemObject(void);
public:
	virtual int call(void);
private:
	int PurchaseItem(UINT& nTransID);
private:
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RoleID;
	unsigned char m_PurchaseType;
	unsigned int m_ItemID;
	int m_ItemNum;
	unsigned int m_CliCalc;
};
//购买vip
class CPurchaseVipObject: public CMethodObject
{

public:
	CPurchaseVipObject(char* szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int VipID, unsigned int uCliCalc,
		CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CPurchaseVipObject();
public:
	int call();
public:
	int PurchaseVip();
private:
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RoleID;
	unsigned int m_VipID;
	unsigned int m_CliCalc;
};

//领取Vip道具包
class CFetchVipItemObject: public CMethodObject
{
public:
	CFetchVipItemObject(char* szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int VipID, CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CFetchVipItemObject();
public:
	virtual int call();
private:
	int FetchVipItem(UINT& nTransID);
	int CreateVipItem(std::vector<ShopServer::VipItem>& vectVipItem);
private:
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RoleID;
	unsigned int m_VipID;
};
//领取Vip道具包失败方法对象
class CFetchVipItemFailedObject: public CMethodObject
{
public:
	CFetchVipItemFailedObject(char* szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int VipID, CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CFetchVipItemFailedObject();
public:
	virtual int call();
protected:
	int FetchVipItemFail();
private:
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RoleID;
	unsigned int m_VipID;
};

//赠送道具Vip方法对象
class CPurchaseGiftObject: public CMethodObject
{
public:
	CPurchaseGiftObject(char* szSrcAccount, ShopServer::S_PlayerID SrcPId, unsigned int SrcRoleID, char* szRoleName, unsigned char ucPurchaseType,
		unsigned int uItemID, int nItemNum, char* szDestAccount, unsigned int DestRoleID, char* szDestRoleName, unsigned int uCliCalc, char* szNote, 
		CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CPurchaseGiftObject();
public:
	virtual int call();
	void SetRoleStatus(unsigned char ucRoleStatus);
private:
	int PurchaseGift(UINT& nTransID);

private:
	std::string m_SrcAccount;
	ShopServer::S_PlayerID m_SrcUserId;
	CSessionHandler* m_Session;
	unsigned int m_SrcRoleID;
	std::string m_RoleName;
	unsigned char m_PurchaseType;
	unsigned int m_ItemID;
	int m_ItemNum;
	unsigned int m_CliCalc;
	std::string m_DestAccount;
	unsigned int m_DestRoleID;
	std::string m_Note;
	std::string m_DestRoleName;
	unsigned char m_RoleInfo;
};

//保存未领成功的道具
class CStoreItemObject: public CMethodObject
{
public:
	CStoreItemObject(char* szSrcAccount, ShopServer::S_PlayerID SrcPId, unsigned int SrcRoleID, unsigned int TransID, unsigned int uItemID, int nItemNum, 
		char* szDestAccount, unsigned int DestRoleID, char* szNote, 
		CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CStoreItemObject();
public:
	virtual int call();
private:
	int StoreItem();
private:
	std::string m_SrcAccount;
	ShopServer::S_PlayerID m_SrcUserId;
	CSessionHandler* m_Session;
	unsigned int m_SrcRoleID;
	unsigned int m_ItemID;
	int m_ItemNum;
	std::string m_DestAccount;
	unsigned int m_DestRoleID;
	std::string m_Note;
	unsigned int m_TransID;
};

//玩家上线领取道具
class CQueryCachedItem: public CMethodObject
{
public:
	CQueryCachedItem(char* szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	~CQueryCachedItem();
public:
	virtual int call();
private:
	int QueryCachedItems(std::vector<ShopServer::TransItem>& vectItems);
private:
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RoleID;
};

class CFetchCachedItem: public CMethodObject
{
public:
	CFetchCachedItem(char* szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int TranID, unsigned int ItemID, unsigned int ItemNum, 
		CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CFetchCachedItem();
public:
	virtual int call();
protected:
	int FetchCachedItem();
private:
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RoleID;
	unsigned int m_TranID;
	unsigned int m_ItemID;
	unsigned int m_ItemNum;
};

class CPlayerNotifyObject: public CMethodObject
{

public:
	CPlayerNotifyObject(char* szAccount, ShopServer::S_PlayerID PId, unsigned int RoleID, char* szRoleName, unsigned int RaceID, unsigned int ClassID, 
		unsigned int Level, unsigned char NotifyType, CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CPlayerNotifyObject();
public:
	virtual int call();
private:
	std::string m_Account;
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RoleID;
	std::string m_RoleName;
	unsigned int m_RaceID;
	unsigned int m_ClassID;
	unsigned int m_Level;
	unsigned char m_NotifyType;
};

class CQryRankObject: public CMethodObject
{
public:
	CQryRankObject(ShopServer::S_PlayerID PId,  unsigned int nRaceID, unsigned char nRankType, unsigned char nPageIndex, CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CQryRankObject();
public:
	virtual int call();
private:
	int FetchRankInfo(std::vector<ShopServer::RankInfo>& vectRankInfo);
	bool IsTailPage();
private:
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RaceID;
	unsigned char m_RankType;
	unsigned char m_PageIndex;
};

class CSessionCloseObject: public CMethodObject
{
public:
	CSessionCloseObject(CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CSessionCloseObject();
public:
	virtual int call();
private:
	CSessionHandler* m_Session;
};

class CShopExistObject: public ACE_Method_Object
{
public:
	CShopExistObject(){};
	virtual ~CShopExistObject(){};
	virtual int call();
		 
};

class CQryRoleInfoOject: public CMethodObject
{
public:
	CQryRoleInfoOject(CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc, std::string strRoleName);
	virtual ~CQryRoleInfoOject();
public:
	virtual int call();
private:
	CSessionHandler* m_Session;
	std::string m_RoleName;
};

class CPlayerGuildInfo: public CMethodObject
{
public:
	CPlayerGuildInfo(ShopServer::S_PlayerID PId, unsigned int RoleID, unsigned int GuildNo, CConnection* pConnection, CSessionHandler* pSession,CDataProc& DataProc);
	virtual ~CPlayerGuildInfo();
public:
	virtual int call();
private:
	ShopServer::S_PlayerID m_UserId;
	CSessionHandler* m_Session;
	unsigned int m_RoleID;
	unsigned int m_GuildNo;
};


#endif
