﻿#include "Config.h"

const std::string CONFIGFILE_NAME = "ShopServer.xml";

CConfig::CConfig(void)
{
	m_ConfigImp.Load(CONFIGFILE_NAME.c_str());
	CElement* pRoot = m_ConfigImp.Root();
	std::vector<CElement*> vectElement;
	m_ConfigImp.FindChildElements(pRoot, vectElement);
	for (size_t nIndex = 0; nIndex < vectElement.size(); nIndex++)
	{
		if (!ACE_OS::strcmp(vectElement[nIndex]->Name(), "Net"))
		{
			m_ServerIp = vectElement[nIndex]->GetAttr("IpAddr");
			m_ServerPort = atoi(vectElement[nIndex]->GetAttr("Port"));
		}
		if (!ACE_OS::strcmp(vectElement[nIndex]->Name(), "Database"))
		{
			m_DbServerIp = vectElement[nIndex]->GetAttr("IpAddr");
			m_DbName = vectElement[nIndex]->GetAttr("DbName");
			m_DbUser = vectElement[nIndex]->GetAttr("UserName");
			m_DbPwd = vectElement[nIndex]->GetAttr("PassWd");
			m_DbPort = atoi(vectElement[nIndex]->GetAttr("Port"));
		}
	}
}

CConfig::~CConfig(void)
{

}
