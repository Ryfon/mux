﻿/** @file Common.h 
@brief 封装通用的功能
<pre>
*	Copyright (c) 2009，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：张少东
*	完成日期：2009-03-24
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/
#ifndef COMMON_H
#define COMMON_H
//ace header
//#include <ace/global_macros.h>
#include <ace/OS_main.h>
#include <ace/Event_Handler.h>
#include <ace/Service_Object.h>
#include <ace/Task_T.h>
#include <ace/Reactor.h>
#include <ace/Message_Queue_T.h>
#include <ace/Message_Block.h>
#include <ace/Acceptor.h>
#include <ace/SOCK_Acceptor.h>
#include <ace/SOCK_Stream.h>
#include <ace/Svc_Handler.h>
#include <ace/Thread_Mutex.h>
#include <ace/Synch.h>
#include <ace/Hash_Map_Manager.h>
#include <ace/Signal.h>
#include <ace/Token.h>
#include <ace/Select_Reactor.h>
#include <ace/Activation_Queue.h>
#include <ace/Method_Object.h>

//std header
#include <map>
#include <string>
#include <vector>
#include <list>

#define MAX_PACK_SIZE 1024
#define MAX_QUE_SIZE 16 * 1024 * 1024

#define HEADERSIZE sizeof(ShopServer::MsgHeader)

class CUtil
{
private:
	CUtil();
	~CUtil();
	CUtil(const CUtil&);
};
#endif
