﻿#include "MysqlWrapper.h"

CConnection::CConnection(void)
{
	m_bIsInit = false;
	m_pMysql = NULL;

	memset(&m_stConnStr, 0x00, sizeof(m_stConnStr));
}

CConnection::~CConnection(void)
{
	// 关闭连接
	Close();
}

int CConnection::Open(stConnectionString *pstConnStr)
{
	if(NULL == pstConnStr)
		return -1;

	if(!m_bIsInit)
	{
		// 判断连接串
		if(    (strncmp(pstConnStr->szHost,     "", strlen(pstConnStr->szHost)) == 0)
			|| (strncmp(pstConnStr->szUser,     "", strlen(pstConnStr->szUser)) == 0)
			|| (strncmp(pstConnStr->szPassword, "", strlen(pstConnStr->szPassword)) == 0)
			|| (strncmp(pstConnStr->szDb,       "", strlen(pstConnStr->szDb)) == 0)   )
		{
			ACE_ERROR_RETURN((LM_ERROR, "%p\n", ACE_TEXT("mysql连接串错误")), -1);
		}
		// 初始化mysql对象
		m_pMysql = mysql_init(NULL);
		if(NULL == m_pMysql)
			ACE_ERROR_RETURN((LM_ERROR, "%p\n", ACE_TEXT("mysql init failed.")), -1);
			

		// 连接
		if(mysql_real_connect(m_pMysql, 
							  pstConnStr->szHost, pstConnStr->szUser, pstConnStr->szPassword, pstConnStr->szDb,
							  pstConnStr->nPort, NULL, CLIENT_MULTI_STATEMENTS) == NULL) //CLIENT_NULTI_STATEMENTS支持存储过程
		{
			
			ACE_ERROR_RETURN((LM_ERROR, "%p\n", mysql_error(m_pMysql)), -1);
		}

		m_bIsInit = true;
		ACE_OS::memcpy(&m_stConnStr, pstConnStr, sizeof(m_stConnStr));
	}

	return 0;
}

int CConnection::EscapeString(char* szTo, const char* szFrom, int nLen)
{
	return mysql_real_escape_string(m_pMysql, szTo, szFrom, nLen);
}

void CConnection::Close(void)
{
	mysql_close(m_pMysql);

	m_bIsInit = false;
}

int CConnection::Ping(void)
{
	if(mysql_ping(m_pMysql) != 0)
	{
		// 关闭连接
		Close();

		// 尝试重连
		return Open(&m_stConnStr);
	}
	else
		return 0;
}

MYSQL *CConnection::Mysql(void)
{
	return m_pMysql;
}

CQuery * CConnection::CreateQuery(void)
{
	CQuery * pQuery = new CQuery(this);
	if(pQuery == NULL)
		return NULL;
	else
		return pQuery;
}

void CConnection::DestroyQuery(CQuery* pQuery)
{
	delete pQuery;
}

CQuery::CQuery(CConnection *pConnection)
{
	m_pConnection = pConnection;
	m_uiInsertID = 0;
}

CQuery::~CQuery(void)
{

}

int CQuery::ExecuteSelect(const char *pszQuery, unsigned long ulLength, CResult*& pResult)
{
	if(NULL == pszQuery)
		return -1;

	// 判断连接是否有效?
	if(m_pConnection->Ping() != 0)
		return -1;

	// 执行查询
	if(mysql_real_query(m_pConnection->Mysql(), pszQuery, ulLength) !=0)
	{
		ACE_ERROR_RETURN((LM_ERROR, "%p\n", mysql_error(m_pConnection->Mysql())), -1);
	}

	// 保存结果
	MYSQL_RES *res = mysql_store_result(m_pConnection->Mysql());
	if(NULL == res)
		return 0;

	pResult = new CResult(m_pConnection, res);
	if(pResult == NULL)
	{
		// 释放结果
		mysql_free_result(res);
		return -1;
	}

	return 0;
}

int CQuery::ExecuteUpdate(const char *pszQuery, unsigned long ulLength)
{
	if(NULL == pszQuery)
		return -1;

	// 判断连接是否有效?
	if(m_pConnection->Ping() != 0)
		return -1;

	// 执行查询
	if(mysql_real_query(m_pConnection->Mysql(), pszQuery, ulLength) !=0)
	{
		ACE_ERROR_RETURN((LM_ERROR, "%p\n", mysql_error(m_pConnection->Mysql())), -1);
	}
	
	m_uiInsertID = (unsigned int) mysql_insert_id(m_pConnection->Mysql());
	return (int)mysql_affected_rows(m_pConnection->Mysql());	
}

void CQuery::FreeResult(CResult *pResult)
{
	if(NULL == pResult)
		return;

	// 清理
	mysql_free_result(pResult->Res());
	
	delete pResult;
	
	return;
}

unsigned long CQuery::RealEscape(char *pszDest, const char *pszSource, unsigned long ulSourceLen)
{	
	return mysql_real_escape_string(m_pConnection->Mysql(), pszDest, pszSource, ulSourceLen);
}

unsigned int CQuery::InsertID(void)
{
	return m_uiInsertID;
}

CResult::CResult(CConnection *pConnection, MYSQL_RES *pRes)
{
	m_pConnection = pConnection;
	m_pResult = pRes;

	m_rowNum = 0;
	m_fieldNum = 0;

	if(m_pResult != NULL)
	{
		m_rowNum = (unsigned int) mysql_num_rows(m_pResult);
		m_fieldNum = mysql_num_fields(m_pResult);
	}
}

CResult::~CResult(void)
{

}

MYSQL_RES * CResult::Res(void)
{
	return this->m_pResult;
}

unsigned int CResult::RowNum(void)
{
	return (unsigned int) (mysql_num_rows(m_pResult));
}

unsigned int CResult::FieldNum(void)
{
	return mysql_num_fields(m_pResult);
}

bool CResult::Next(void)
{
	m_row = mysql_fetch_row(m_pResult);
	if(NULL == m_row)
		return false;
	else
		return true;
}

int CResult::GetInt(unsigned int uiFieldIndex)
{	
	if(uiFieldIndex < m_fieldNum)
		return atoi(m_row[uiFieldIndex]);
	else
		return -1;
}

char * CResult::GetString(unsigned int uiFieldIndex)
{
	if(uiFieldIndex < m_fieldNum)
		return m_row[uiFieldIndex];
	else 
		return NULL;
}

double CResult::GetDouble(unsigned int uiFieldIndex)
{
	if(uiFieldIndex < m_fieldNum)
		return atof(m_row[uiFieldIndex]);
	else
		return -1;
}

float CResult::GetFloat(unsigned int uiFieldIndex)
{
	if(uiFieldIndex < m_fieldNum)
		return (float) (atof(m_row[uiFieldIndex]));
	else
		return -1;
}

unsigned int CResult::GetDate(unsigned int uiFieldIndex)
{
	if(uiFieldIndex < m_fieldNum)
		return (unsigned int) (atoi(m_row[uiFieldIndex]));
	else
		return 0;
}

