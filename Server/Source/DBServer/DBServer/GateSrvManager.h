﻿/********************************************************************
	created:	2009/07/23
	created:	23:7:2009   10:31
	file:		GateSrvManager.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef GATE_SRV_MANAGER_H
#define GATE_SRV_MANAGER_H


#include <vector>
#include "../../Base/aceall.h"
#include "MsgToPutContent.h"

class  CGateServer;



class CGateSrvManger
{
	friend class ACE_Singleton<CGateSrvManger, ACE_Null_Mutex>;

public:
	void PushGateSrv( CGateServer* pServer );

	CGateServer* GetValidGateSrv();

	CGateServer* GetGateSrvBySessionID( unsigned int wID );

	void RemoveGateSrv( CGateServer* pGateSrv );

	unsigned int GetGateSrvCount() { return (unsigned)mGateSrvVec.size(); }

	void TrySendMsg();

	void SendMsgToPut( MsgToPutContent* pContent );

	void PushMsgContentToVecotr(  MsgToPutContent* pContent );

private:
	std::vector<CGateServer *> mGateSrvVec;
	std::vector<MsgToPutContent *> mMsgContentVector;
	ACE_Thread_Mutex mutex_;

};

typedef ACE_Singleton<CGateSrvManger, ACE_Null_Mutex> GateSrvMangerSingle;

#endif
