﻿/** @file RecordHandler.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2007-12-28
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef RECORD_HANDLER_H
#define RECORD_HANDLER_H


#include "../../Base/MysqlWrapper.h"
#include "../../Base/PkgProc/SrvProtocol.h"

/**
* @class CRecordHandler
*
* @brief db中相关表数据的处理器
* 
*/
class CRecordHandler
{
private:
	CRecordHandler();
	~CRecordHandler();

public:
	/// 检索玩家角色列表（简单信息）
	static int SelectRoleList( CConnection* pConnect, const char *pszAccount, char * pszData, unsigned long ulDataLength);

	/// 检索玩家信息
	static int SelectPlayerInfo( CConnection* pConnect,const char *pszAccount, const unsigned int uiID, char * pszData, unsigned long ulDataLength);

	/// 插入玩家信息
	static int InsertPlayerInfo(const char * pszData, unsigned long ulDataLength, unsigned int &uiInsertID);	

	/// 更新玩家信息
	static int UpdatePlayerInfo(const char * pszData, unsigned long ulDataLength);

	/// 删除玩家信息
	static int DeletePlayerInfo(unsigned int uiPlayerID);

	/// 获取唯一编号
	static int GetBatchID(unsigned int &uiStartID, unsigned int &uiEndID);

	/// 是否已经存在同名的玩家
	static int ExistSamePlayerName(const char *pszPlayerName);

	static int UpdatePlayerSkillInfo( CConnection* pConnect, const PlayerInfo_4_Skills& SkillInfo , unsigned int updateplayerid );

private:
	/// 检索当前最大编号
	static int SelectMaxID(unsigned int &uiStartID, unsigned int &uiEndID);

	/// 记录本次产生编号
	static int InsertIDInfo(unsigned int uiStart, unsigned int uiEndID);

};


#endif/*RECORD_HANDLER_H*/
