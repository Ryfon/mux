﻿#include "RecordHandler.h"
#include "PlayerInfoManager.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/Utility.h"
#include <string>
#include <sstream>
#include "ConfigInfoManager.h"

using namespace MUX_PROTO;

int CRecordHandler::SelectRoleList(CConnection* pConnect, const char *pszAccount, char * pszData, unsigned long ulDataLength)
{
	if ( !pConnect )
		return -1;

	if((pszData == NULL) || (ulDataLength < sizeof(PlayerInfoLogin) * 2))
		return -1;

	TRY_BEGIN;

	// 返回值
	int iRet = 0;
	PlayerInfoLogin *pPlayerInfo = (PlayerInfoLogin *)pszData;

	//// 创建查询
	//CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	//if(pConnection == NULL)
	//	return -1;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
		"SELECT ID, BaseData, EquipData FROM playerinfo WHERE Account = '%s'", 
		pszAccount);
	if(iStatementLen <= 0 )
		iRet = -1;
	else
	{
		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", szStatement);
			iRet = -1;
		}
		else
		{
			int iCount = 0;
			PlayerInfo_1_Base baseInfo;
			PlayerInfo_2_Equips equipInfo;

			// 处理结果集
			while(pResult->Next())
			{
				unsigned int uiID = pResult->GetInt(0);
				StructMemCpy(baseInfo, pResult->GetString(1), sizeof(baseInfo));
				StructMemCpy(equipInfo, pResult->GetString(2), sizeof(equipInfo));
				
				pPlayerInfo->uiID = uiID;
				pPlayerInfo->nameSize = ACE_OS::snprintf(pPlayerInfo->szNickName,MAX_NAME_SIZE,"%s", baseInfo.szNickName );
				//StructMemCpy(pPlayerInfo->szNickName, baseInfo.szNickName, sizeof(pPlayerInfo->szNickName));
				pPlayerInfo->usLevel = baseInfo.usLevel;
				pPlayerInfo->ucRace = baseInfo.ucRace;
				pPlayerInfo->usClass = baseInfo.usClass;
				pPlayerInfo->shFlag = baseInfo.shFlag;
				pPlayerInfo->usHair = baseInfo.usHair;
				pPlayerInfo->usHairColor = baseInfo.usHairColor;
				pPlayerInfo->usFace = baseInfo.usFace;
				pPlayerInfo->usPortrait = baseInfo.usPortrait;
				StructMemCpy(pPlayerInfo->equipInfo, &equipInfo, sizeof(pPlayerInfo->equipInfo));
				pPlayerInfo->usMapID = baseInfo.usMapID;
				pPlayerInfo->iPosX = baseInfo.iPosX;
				pPlayerInfo->iPosY = baseInfo.iPosY;

				pPlayerInfo++;
				iCount++;
				if(iCount >= 2)
					break;
			}

			// 销毁查询
			pQuery->FreeResult(pResult);
		}
	}

	// 销毁查询
	pConnect->DestroyQuery(pQuery);

	return iRet;

	TRY_END;

	return 0;
}

int CRecordHandler::SelectPlayerInfo(CConnection* pConnect,const char *pszAccount, const unsigned int uiID, char * pszData, unsigned long ulDataLength)
{
	if((pszData == NULL) || (ulDataLength < sizeof(FullPlayerInfo)))
		return -1;

	int iRet = 0;

	TRY_BEGIN;

	// 创建查询
//	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
//	if(pConnection == NULL)
//		return -1;


	if( pConnect == NULL )
		return -1;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"SELECT BaseData, EquipData, PkgData, SkillData, BufferData, StateData FROM playerinfo WHERE account = '%s' and ID = %d", 
		pszAccount, uiID);
	if(iStatementLen <= 0 )
		iRet = -1;
	else
	{
		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", szStatement);
			iRet = -1;
		}
		else
		{
			// 处理结果集
			if(pResult->Next())
			{
				FullPlayerInfo *pFullPlayerInfo = (FullPlayerInfo *)pszData;

				if ( pResult->GetString(0) )
					StructMemCpy(pFullPlayerInfo->baseInfo, pResult->GetString(0), sizeof(PlayerInfo_1_Base));

				if ( pResult->GetString(1) )
					StructMemCpy(pFullPlayerInfo->equipInfo, pResult->GetString(1), sizeof(PlayerInfo_2_Equips));
			
				if ( pResult->GetString(2) )
					StructMemCpy(pFullPlayerInfo->pkgInfo, pResult->GetString(2), sizeof(PlayerInfo_3_Pkgs));

				if ( pResult->GetString(3))
					StructMemCpy(pFullPlayerInfo->skillInfo, pResult->GetString(3), sizeof(PlayerInfo_4_Skills));

				if ( pResult->GetString(4))
					StructMemCpy(pFullPlayerInfo->bufferInfo,pResult->GetString(4), sizeof(PlayerInfo_5_Buffers));

				if ( pResult->GetString(5) )
					StructMemCpy(pFullPlayerInfo->stateInfo, pResult->GetString(5), sizeof(PlayerStateInfo));

				/*if ( pResult->GetString(6) )
					StructMemCpy(pFullPlayerInfo->itemInfo, pResult->GetString(6), sizeof(EquipItemInfo)*(EQUIP_SIZE + PACKAGE_SIZE));*/

				
				//StructMemCpy(pFullPlayerInfo->equipInfo, pResult->GetString(1), sizeof(PlayerInfo_2_Equips));
				//StructMemCpy(pFullPlayerInfo->pkgInfo, pResult->GetString(2), sizeof(PlayerInfo_3_Pkgs));
				//StructMemCpy(pFullPlayerInfo->skillInfo, pResult->GetString(3), sizeof(PlayerInfo_4_Skills));
				//StructMemCpy(pFullPlayerInfo->bufferInfo, pResult->GetString(4), sizeof(PlayerInfo_5_Buffers));
				//StructMemCpy(pFullPlayerInfo->stateInfo, pResult->GetString(5), sizeof(PlayerStateInfo));
				////2008/05/15 modify by gcq 2008/05/15拷贝短了，导致数据丢失
				//StructMemCpy(&pFullPlayerInfo->itemInfo, pResult->GetString(6), sizeof(EquipItemInfo)*(EQUIP_SIZE + PACKAGE_SIZE));
			}

			// 销毁查询
			pQuery->FreeResult(pResult);
		}
	}

	// 销毁查询
	pConnect->DestroyQuery(pQuery);

	return iRet;

	TRY_END;

	return 0;
}

int CRecordHandler::InsertPlayerInfo(const char * pszData, unsigned long ulDataLength, unsigned int &uiInsertID)
{
	if((pszData == NULL) || (ulDataLength < sizeof(FullPlayerInfo)))
		return -1;

	TRY_BEGIN;

	// 返回值
	int iRet = 0;
	FullPlayerInfo *pFullPlayerInfo = (FullPlayerInfo *)pszData;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return -1;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char escapeBaseData[2 * sizeof(PlayerInfo_1_Base) + 1] = {0};
	pQuery->RealEscape(escapeBaseData, (const char *)&pFullPlayerInfo->baseInfo, (unsigned long) sizeof(PlayerInfo_1_Base)); // 转义

	char escapteEquipData[2 * sizeof(PlayerInfo_2_Equips) + 1] = {0};
	pQuery->RealEscape(escapteEquipData, (const char *)&pFullPlayerInfo->equipInfo, (unsigned long) sizeof(PlayerInfo_2_Equips)); // 转义

	char escaptePkgData[2 * sizeof(PlayerInfo_3_Pkgs) + 1] = {0};
	pQuery->RealEscape(escaptePkgData, (const char *)&pFullPlayerInfo->pkgInfo, (unsigned long) sizeof(PlayerInfo_3_Pkgs)); // 转义

	char escapteSkillData[2 * sizeof(PlayerInfo_4_Skills) + 1] = {0};
	pQuery->RealEscape(escapteSkillData, (const char *)&pFullPlayerInfo->skillInfo, (unsigned long) sizeof(PlayerInfo_4_Skills)); // 转义

	char escapteBufferData[2 * sizeof(PlayerInfo_5_Buffers) + 1] = {0};
	pQuery->RealEscape(escapteBufferData, (const char *)&pFullPlayerInfo->bufferInfo, (unsigned long) sizeof(PlayerInfo_5_Buffers)); // 转义

	char escapteStateData[2 * sizeof(PlayerStateInfo) + 1] = {0};
	pQuery->RealEscape(escapteStateData, (const char *)&pFullPlayerInfo->stateInfo, (unsigned long) sizeof(PlayerStateInfo)); // 转义

	//char escapteItemData[2 * sizeof(EquipItemInfo)*(EQUIP_SIZE + PACKAGE_SIZE) + 1] = {0};
	//pQuery->RealEscape(escapteItemData, (const char *)&pFullPlayerInfo->itemInfo, (unsigned long) sizeof(EquipItemInfo)*(EQUIP_SIZE + PACKAGE_SIZE)); // 转义

	std::stringstream ss;
	ss << "INSERT INTO playerinfo (Account, Nickname, BaseData, EquipData, PkgData, SkillData, BufferData, StateData) VALUES("
	   << "'" << pFullPlayerInfo->baseInfo.szAccount << "',"
	   << "'" << pFullPlayerInfo->baseInfo.szNickName << "',"
	   << "'" << escapeBaseData << "',"
	   << "'" << escapteEquipData << "',"
	   << "'" << escaptePkgData << "',"
	   << "'" << escapteSkillData << "',"
	   << "'" << escapteBufferData << "',"
	   << "'" << escapteStateData << "')";
	   
	std::string statement = ss.str();
	   
    // 执行查询
    iRet = pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) );
    uiInsertID = pQuery->InsertID();

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;

	TRY_END;

	return 0;
}

int CRecordHandler::UpdatePlayerInfo(const char * pszData, unsigned long ulDataLength)
{
	if((pszData == NULL) || (ulDataLength < sizeof(FullPlayerInfo)))
		return -1;

	TRY_BEGIN;

	// 返回值
	int iRet = 0;
	FullPlayerInfo *pFullPlayerInfo = (FullPlayerInfo *)pszData;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return -1;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char escapeBaseData[2 * sizeof(PlayerInfo_1_Base) + 1] = {0};
	pQuery->RealEscape(escapeBaseData, (const char *)&pFullPlayerInfo->baseInfo, (unsigned long) sizeof(PlayerInfo_1_Base)); // 转义

	char escapteEquipData[2 * sizeof(PlayerInfo_2_Equips) + 1] = {0};
	pQuery->RealEscape(escapteEquipData, (const char *)&pFullPlayerInfo->equipInfo, (unsigned long) sizeof(PlayerInfo_2_Equips)); // 转义

	char escaptePkgData[2 * sizeof(PlayerInfo_3_Pkgs) + 1] = {0};
	pQuery->RealEscape(escaptePkgData, (const char *)&pFullPlayerInfo->pkgInfo, (unsigned long) sizeof(PlayerInfo_3_Pkgs)); // 转义

	char escapteSkillData[2 * sizeof(PlayerInfo_4_Skills) + 1] = {0};
	pQuery->RealEscape(escapteSkillData, (const char *)&pFullPlayerInfo->skillInfo, (unsigned long) sizeof(PlayerInfo_4_Skills)); // 转义

	char escapteBufferData[2 * sizeof(PlayerInfo_5_Buffers) + 1] = {0};
	pQuery->RealEscape(escapteBufferData, (const char *)&pFullPlayerInfo->bufferInfo, (unsigned long) sizeof(PlayerInfo_5_Buffers)); // 转义

	char escapteStateData[2 * sizeof(PlayerStateInfo) + 1] = {0};
	pQuery->RealEscape(escapteStateData, (const char *)&pFullPlayerInfo->stateInfo, (unsigned long) sizeof(PlayerStateInfo)); // 转义

	//char escapteItemData[2 * sizeof(EquipItemInfo)*(EQUIP_SIZE + PACKAGE_SIZE) + 1] = {0};
	//pQuery->RealEscape(escapteItemData, (const char *)&pFullPlayerInfo->itemInfo, (unsigned long) sizeof(EquipItemInfo)*(EQUIP_SIZE + PACKAGE_SIZE)); // 转义

	std::stringstream ss;
	ss << "UPDATE playerinfo SET "
		<< "BaseData='" << escapeBaseData << "',"
		<< "EquipData='" << escapteEquipData << "',"
		<< "PkgData='" << escaptePkgData << "',"
		<< "SkillData='" << escapteSkillData << "',"
		<< "BufferData='" << escapteBufferData << "',"
		<< "StateData='" << escapteStateData << "'"
		<< " WHERE " 
		<< "Account='" << pFullPlayerInfo->baseInfo.szAccount << "' AND "
		<< "ID=" << pFullPlayerInfo->baseInfo.uiID;

	std::string statement = ss.str();

	// 执行查询
	if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
		iRet = -1;

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;

	TRY_END;
	return 0;
}

int CRecordHandler::DeletePlayerInfo(unsigned int uiPlayerID)
{
	// 返回值
	int iRet = 0;

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return -1;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[256];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"DELETE FROM playerinfo WHERE ID = %d", 
		uiPlayerID);
	if(iStatementLen <= 0)
		iRet = -1;
	else
	{
		// 执行查询
		iRet = pQuery->ExecuteUpdate((const char*)szStatement, iStatementLen);
	}

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;
	TRY_END;
	return 0;
}

int CRecordHandler::GetBatchID(unsigned int &uiStartID, unsigned int &uiEndID)
{
	unsigned int uiCurrentStartID = 0;
	unsigned int uiCurrentEndID = 0;

	TRY_BEGIN;

	// 获取当前最大编号(起始编号，终止编号)
	if(SelectMaxID(uiCurrentStartID, uiCurrentEndID) == -1)	
		return -1;

	uiCurrentStartID = uiCurrentEndID + 1;
	uiCurrentEndID = uiCurrentEndID + 1000;

	if(InsertIDInfo(uiCurrentStartID, uiCurrentEndID) == -1)
		return -1;
	
	uiStartID = uiCurrentStartID;
	uiEndID = uiCurrentEndID;

	return 0;
	TRY_END;

	return 0;
}

int CRecordHandler::SelectMaxID(unsigned int &uiStartID, unsigned int &uiEndID)
{
	int iRet = 0;

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return -1;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
										 sizeof(szStatement), 
										 "SELECT StartID, EndID FROM idgenerator WHERE EndID =(SELECT MAX(EndID) FROM idgenerator)" );
	if(iStatementLen <= 0 )
		iRet = -1;
	else
	{
		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", szStatement);
			iRet = -1;
		}
		else
		{
			// 处理结果集
			if(pResult->Next())
			{
				uiStartID = pResult->GetInt(0);
				uiEndID = pResult->GetInt(1);
			}

			// 销毁查询
			pQuery->FreeResult(pResult);
		}
	}

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;

	TRY_END;
	return 0;
}

int CRecordHandler::InsertIDInfo(unsigned int uiStart, unsigned int uiEndID)
{
	// 返回值
	int iRet = 0;

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return -1;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	std::stringstream ss;
	ss << "INSERT INTO idgenerator (StartID, EndID) VALUES("
	   << uiStart << ","
	   << uiEndID  << ")";

	std::string statement = ss.str();

	// 执行查询
	iRet = pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) );

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;

	TRY_END;
	return 0;
}

int CRecordHandler::UpdatePlayerSkillInfo(CConnection* pConnection, const PlayerInfo_4_Skills& SkillInfo , unsigned int updateplayerid )
{
	// 返回值
	int iRet = 0;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	char escapteSkillData[2 * sizeof(PlayerInfo_4_Skills) + 1] = {0};
	pQuery->RealEscape(escapteSkillData, (const char *)&SkillInfo, (unsigned long) sizeof(PlayerInfo_4_Skills)); // 转义

	std::stringstream ss;
	ss <<  " UPDATE playerinfo SET "
		<< " SkillData='" << escapteSkillData <<"'"
		<< " WHERE "
		<< " ID= " << updateplayerid;

	std::string statement = ss.str();

	// 执行查询
	iRet = pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) );

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;
}

int CRecordHandler::ExistSamePlayerName(const char *pszPlayerName)
{
	// 返回值
	int iRet = 0;

	TRY_BEGIN;

	// 创建查询
	CConnection *pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return -1;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
		"SELECT 1 FROM playerinfo WHERE Nickname = '%s'", 
		pszPlayerName);
	if(iStatementLen <= 0 )
		iRet = -1;
	else
	{
		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", szStatement);
			iRet = -1;
		}
		else
		{
			iRet = pResult->RowNum();

			// 销毁查询
			pQuery->FreeResult(pResult);
		}
	}

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;

	TRY_END;

	return 0;
}
