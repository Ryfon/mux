﻿#include "OtherServer.h"
#include "ServerConnection.h"
#include "DealPkg.h"


CSrvBase::CSrvBase(CServerConnection* pConn)
{
	m_pConn = pConn;
}

CSrvBase::~CSrvBase(void)
{

}


#ifdef USE_DSIOCP
UniqueObj< CDsSocket>* CSrvBase::GetUniqueDsSocket()
{
	if ( NULL != m_pConn )
	{
		return m_pConn->GetUniqueDsSocket();//返回连接对应属性；
	} else {
		return NULL;
	}
}
#endif //USE_DSIOCP

int CSrvBase::GetHandleID(void)
{
	if(NULL != m_pConn)
		return m_pConn->GetHandleID();

	return -1;
}

int CSrvBase::GetSessionID(void)
{	
	if(NULL != m_pConn)
		return m_pConn->GetSessionID();

	return -1;
}

void CSrvBase::SendPkgToSrv( MsgToPut* pPkg )
{
	m_pConn->SendPkgToSrv(pPkg);
	return;
}

void CSrvBase::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )
{
	ACE_UNUSED_ARG(wCmd);
	ACE_UNUSED_ARG(pBuf);
	ACE_UNUSED_ARG(wPkgLen);
}

void CSrvBase::SetLastHeartbeatConfirmTime(void)
{
	if(NULL != m_pConn)
		m_pConn->SetLastHeartbeatConfirmTime();

	return;
}

bool CSrvBase::HeartbeatCheckOk(void)
{
	if(NULL != m_pConn)
		return m_pConn->HeartbeatCheckOk();

	return false;
}

unsigned int CSrvBase::IncSequenceID(void)
{
	if(NULL != m_pConn)
		return m_pConn->IncSequenceID();

	return 0;
}

CGateServer::CGateServer(CServerConnection* pConn)
	: CSrvBase(pConn)
{
	CGateServerManager::AddSrv(this);
}

CGateServer::~CGateServer(void)
{
	CGateServerManager::RemoveSrv(GetSessionID());
}

void CGateServer::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )
{
	CDealGateSrvPkg::DealPkg(this, wCmd, pBuf, wPkgLen);
	
	return;
}


std::map<unsigned short, CGateServer*> CGateServerManager::m_gateServers;

int CGateServerManager::AddSrv(CGateServer* pServer)
{
	if(NULL != pServer)
	{
		std::map<unsigned short, CGateServer*>::iterator i = m_gateServers.find(pServer->GetSessionID());
		if(m_gateServers.end() == i)
		{
			m_gateServers[pServer->GetSessionID()] = pServer;
			return 0;
		}
	}

	return -1;
}

void CGateServerManager::RemoveSrv(unsigned short usServerID)
{
	m_gateServers.erase(usServerID);

	return;
}

CGateServer* CGateServerManager::FindSvr(unsigned short usServerID)
{
	std::map<unsigned short, CGateServer*>::iterator i = m_gateServers.find(usServerID);
	if(i != m_gateServers.end())
		return i->second;

	return NULL;
}

void CGateServerManager::HeartbeartCheckTimer(void)
{
	ACE_Time_Value now = ACE_OS::gettimeofday();
	static ACE_Time_Value lastCheckTime = now;

	if((now - lastCheckTime).sec() > 60)//1分钟
	{
		lastCheckTime = now;

		if(m_gateServers.size() > 0)
		{
			for(std::map<unsigned short, CGateServer*>::iterator i = m_gateServers.begin(); i != m_gateServers.end(); i++)
			{
				if(i->second != NULL)
				{
					if(i->second->HeartbeatCheckOk())
					{	
						DGHeartbeatCheckReq heartbeatCheck;
						heartbeatCheck.sequenceID = i->second->IncSequenceID();

						MsgToPut *pMsgToPut = CreateSrvPkg(DGHeartbeatCheckReq, i->second, heartbeatCheck );
						i->second->SendPkgToSrv(pMsgToPut);
					}
				}
			}
		}
	}

	return;
}
