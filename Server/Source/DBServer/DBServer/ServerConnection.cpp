﻿#include "ServerConnection.h"
#include "OtherServer.h"
#include "GateSrvManager.h"

extern IBufQueue *g_pSrvSender;

CServerConnection::CServerConnection(void)
	:m_nHandleID(0), m_nSessionID(0), m_bySrvType(0), m_pSrvBase(NULL)
{
#ifdef USE_DSIOCP
		m_pUniqueSocket = NULL;
#endif //USE_DSIOCP
}

CServerConnection::~CServerConnection(void)
{
}

int CServerConnection::SetHandleInfo(/*T_PkgSender* pPkgSender,*/ int nHandleID, int nSessionID )
{
	// 正确的sessionID(0<nSessionID<=1000)
	/*TTTTTT if((nSessionID <= 0) || (nSessionID > 1000))
	{
		D_ERROR( "连接Server不成功, session = %d\n", nSessionID );
		return -1;
	}TTTTTTT*/

	m_bIsDesSelf = false;

	m_nHandleID = nHandleID;
	m_nSessionID = nSessionID;

	// 对端服务器信息处理
	//TTTTT0if((nSessionID >= 100) && (nSessionID < 200)) // 对端类型是GateServer
	//{
	//	m_bySrvType = 0x05;
	//	unsigned short wID = nSessionID - 100;
	//
	//	//m_pSrvBase = new CGateSrv( wID, this );
	//	D_DEBUG( "Server为GateServer(ID = %d)\n\n", wID );
	//}
	//else if((nSessionID >= 700) && (nSessionID < 800)) // 对端是MapServer
	//{
	//	m_bySrvType = 0x05;
	//	unsigned short wID = nSessionID - 700;
	//	
	//	//m_pSrvBase = new CMapSrv( wID, this );
	//	D_DEBUG( "Server为MapServer(ID = %d)\n\n", wID );
	//}TTTTTTT

	m_pSrvBase = NEW CGateServer(this);
	if(NULL == m_pSrvBase)
		return -1;

	GateSrvMangerSingle::instance()->PushGateSrv( (CGateServer *)m_pSrvBase );

	return 0;
}

void CServerConnection::OnCreated(void)
{
	ResetInfo();
	m_lastHeartbeatConfirmTime = ACE_OS::gettimeofday();

	return;
}

void CServerConnection::OnDestoryed(void)
{

#ifdef USE_DSIOCP
	if ( NULL != m_pUniqueSocket )
	{
		if ( NULL != g_poolUniDssocket )
		{
			if ( NULL != g_poolUniDssocket )
			{
				g_poolUniDssocket->Release( m_pUniqueSocket );
				m_pUniqueSocket = NULL;
			}
		}
	}
#endif //USE_DSIOCP

	if ( m_nHandleID != 0 )
	{
		//确实为断开，区别于新建连接时的回调；
		if ( NULL != m_pSrvBase)
		{
			D_INFO( "连接%d断开\n", m_nHandleID );
			GateSrvMangerSingle::instance()->RemoveGateSrv( (CGateServer *)m_pSrvBase );
			delete m_pSrvBase; 
			m_pSrvBase = NULL;
		}
	}

	ResetInfo();

	return;
}

void CServerConnection::ReqDestorySelf(void)
{
	if ( m_bIsDesSelf )
	{
		return;//已经发过断开请求，不再请求断开；
	}

	MsgToPut* disconnectMsg = g_poolMsgToPut->RetrieveOrCreate();

#ifdef USE_DSIOCP
	disconnectMsg->pUniqueSocket = GetUniqueDsSocket();//唯一需要主动设置unisocket者，其余都通过createsrvpkg的方式来生成；
#endif //USE_DSIOCP

	disconnectMsg->nHandleID = m_nHandleID;//向断开执行者传递自身标识；
	disconnectMsg->nSessionID = m_nSessionID;//向断开执行者传递自身标识；
	disconnectMsg->nMsgLen = 0;//表明断开连接请求;
	
	//发送断开自身；
	SendPkgToSrv( disconnectMsg );

	m_bIsDesSelf = true;
	
	D_DEBUG( "主动请求断开SRV(其sessionID:%d)\n", m_nSessionID );
	
	return;
}

void CServerConnection::OnPkgError(void)
{
	//解析包错误时断开连接；
	ReqDestorySelf();

	return;
}

void CServerConnection::OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen )
{
	//以后要处理第一个连接服务标识号，目前因为只连mapsrv，因此直接转发；
	if ( NULL != m_pSrvBase )
	{
		m_pSrvBase->OnPkgRcved( wCmd, pBuf, wPkgLen );
	}
	return; 
}

int CServerConnection::GetHandleID(void)
{
	return m_nHandleID;
}

int CServerConnection::GetSessionID(void)
{
	return m_nSessionID;
}

void CServerConnection::SendPkgToSrv( MsgToPut* pPkg )
{
	if ( NULL != g_pSrvSender )
	{
		//添加与2008.7.7号,为了发送消息的同步机制
		ACE_Guard<ACE_Thread_Mutex> guard( mutex_ );

		g_pSrvSender->PushMsg( pPkg );
		//g_pSrvSender->SetReadEvent();//通知网络模块立即发送出去；
	}

	return;
}

void CServerConnection::ResetInfo(void)
{
	m_nHandleID = 0;
	m_nSessionID = 0;
	m_lastHeartbeatConfirmTime = ACE_Time_Value::zero;
	m_sequenceID = 0;

	return;
}

void CServerConnection::SetLastHeartbeatConfirmTime(void)
{
	m_lastHeartbeatConfirmTime = ACE_OS::gettimeofday();
	return;
}

bool CServerConnection::HeartbeatCheckOk(void)
{
	if((ACE_OS::gettimeofday() - m_lastHeartbeatConfirmTime).sec() > 10 * 60)	
	{
		D_ERROR("10分钟内没有收到心跳回复，连接断开\n");
		ReqDestorySelf();
		return false;
	}
	
	return true;
}

unsigned int CServerConnection::IncSequenceID(void)
{
	m_sequenceID++;
	return m_sequenceID;
}
