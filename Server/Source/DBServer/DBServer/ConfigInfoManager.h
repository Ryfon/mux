﻿/** @file ConfigInfoManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-3-4
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef CONFIG_INFO_MANAGER_H
#define CONFIG_INFO_MANAGER_H

#include "../../Base/aceall.h"
#include <vector>
#include <map>
#include "../../Base/PkgProc/SrvProtocol.h"

#pragma pack(push, 1)

/// 角色初始化配置信息
typedef struct StrInitRoleInfo  
{
	unsigned short usRace;											// 种族
	unsigned short usClass;											// 职业
	char bSex; 														// 性别  by hyn 08.12.17
	unsigned short usPhysicsLevelMax;								// 最大物理等级
	unsigned short usMagicLevelMax;									// 最大魔法等级
	unsigned short usPhysicsPoint;									// 最大物理点数	
	unsigned short usMagicPoint;									// 最大魔法点数
	unsigned short usPortrait;										// 头像ID
	unsigned short usHair;											// 头发
	unsigned short usHairColor;										// 发色
	unsigned short usFace;											// 脸部		
	unsigned short usMapID;											// 地图ID
	int iPosX;														// X坐标
	int iPosY;														// Y坐标
	unsigned char ucLevel;											// 等级
	unsigned int uiHP;												// HP
	unsigned int uiMP;												// MP
	unsigned int uiMoney;											// 金钱
	unsigned short str;												// 力量
	unsigned short vit;												// 体质 
	unsigned short agi;												// 敏捷
	unsigned short spi;												// 精神
	unsigned short inte;											// 智力
	unsigned int uiSkill[6];										// 技能
}InitRoleInfo;

/// 技能信息
typedef struct StrSkillInfo
{
	unsigned int uiSkillID;											//技能ID号   
	unsigned short usRace;											//所限种族
	unsigned short usClass;											//所限职业
}SkillInfo;



#pragma pack(pop)

/**
* @class CRoleInitManager
*
* @brief 管理角色创建时的相关缺省属性
* 
*/
class CRoleInitManager
{
	friend class ACE_Singleton<CRoleInitManager, ACE_Null_Mutex>;

private:
	/// 构造
	CRoleInitManager(void);

	/// 析构
	~CRoleInitManager(void);

public:
	int Init(const char* pszFileName);

	InitRoleInfo * Find(unsigned short usRace, unsigned short usClass, char bSex);

	int Clear(void);

private:
	/// 初始化信息列表
	std::vector<InitRoleInfo*> m_playerInitInfos;
	
};

/*
* @class CSkillInfoManager
*
* @brief 管理技能属性
* 
*/
class CSkillInfoManager
{
	friend class ACE_Singleton<CSkillInfoManager, ACE_Null_Mutex>;

private:
	CSkillInfoManager(void);

	~CSkillInfoManager(void);

public:
	/// 初始化技能信息
	int Init(const char *pszConfigFile);

	/// 清除
	void Clear(void);

	/// 插入
	bool Insert(unsigned int uiSkillID, SkillInfo *pSkillInfo);

	/// 查找
	SkillInfo* Find(unsigned int uiID);

	/// 查找技能列表是否匹配对应职业
	bool Find(unsigned int uiIDs[], unsigned short usSize, unsigned short usClass);

private:
	std::map<unsigned int, SkillInfo*> m_skillInfos;

};


class CCityInfoManager
{
	friend class ACE_Singleton<CCityInfoManager, ACE_Null_Mutex>;

private:
	CCityInfoManager(void);

	~CCityInfoManager(void);

public:
	///初始化城市信息
	bool InitCityInfo(const char* szPath);

	///清除
	void Clear(void);

	CityInfo* GetCityInfo( unsigned int uiIndex );
	unsigned int GetSize();

private:
	std::vector<CityInfo*> m_vecCityInfo;	
};


/************************************************************************/
/*       初始装备金钱道具                                      */
/************************************************************************/

struct InitItem
{
	unsigned itemTypeID;//道具ID
	unsigned count;//个数
	bool     bstack;//是否是可堆叠的
};

struct InitRoleItemInfo
{
	unsigned short race;
	unsigned short CLASS;
	char sex;
	unsigned int money;
	unsigned int InitEquipItem[16];//出生时，身上装备的道具
	std::vector<InitItem> InitPkgItem;//出生时，包裹里的道具
};


class CInitRoleItemManager
{
	friend class ACE_Singleton<CInitRoleItemManager, ACE_Thread_Mutex>;

private:
	CInitRoleItemManager();

	~CInitRoleItemManager();

public:
	int Init( const char *pszConfigFile );

	void Clear();

	InitRoleItemInfo* Find( unsigned short race, unsigned short nclass, char sex );

#ifndef MULTI_DB_HASH
	void SetInsertItemStart( unsigned int itemStart,unsigned int itemEnd ){ insertRoleItemStart = itemStart; insertRoleItemEnd =itemEnd; }

	unsigned int GetInsertItemStart();
#endif

private:
	std::vector<InitRoleItemInfo *> m_InitRoleItemInfoVec;
#ifndef MULTI_DB_HASH
	unsigned int insertRoleItemStart;
	unsigned int insertRoleItemEnd;
	ACE_Thread_Mutex thread_mutex_;
#endif
};


typedef ACE_Singleton<CRoleInitManager, ACE_Null_Mutex> CRoleInitManagerSingle;
typedef ACE_Singleton<CSkillInfoManager, ACE_Null_Mutex> CSkillInfoManagerSingle;
typedef ACE_Singleton<CInitRoleItemManager, ACE_Thread_Mutex> CInitRoleItemManagerSingle;
typedef ACE_Singleton<CCityInfoManager, ACE_Null_Mutex >  CCityInfoManagerSingle;


#endif/*CONFIG_INFO_MANAGER_H*/

