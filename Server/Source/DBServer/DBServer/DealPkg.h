﻿/** @file DealPkg.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-1-3
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef DEAL_PKG_H
#define DEAL_PKG_H

#include "../../Base/PkgProc/DealPkgBase.h"
#include <map>

class CGateServer;


/// Gatesrv消息处理类;
class CDealGateSrvPkg : public IDealPkg<CGateServer>
{
public:
	/// 构造
	CDealGateSrvPkg(void);

	/// 析构
	virtual ~CDealGateSrvPkg(void);

public:
	/// 初始化(注册各命令字对应的处理函数)
	static void Init(void);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
public:
	////DB保存开始;Register(G_D_DBSAVE_PRE, OnDbSavePre );
	static bool OnDbSavePre(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen) { ACE_UNUSED_ARG(pOwner); ACE_UNUSED_ARG(pPkg); ACE_UNUSED_ARG(wPkgLen); return true; };	
	////DB保存结束(与M_G_DBSAVE_POST一一对应);Register(G_D_DBSAVE_POST, OnDbSavePost );
	static bool OnDbSavePost(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen) { ACE_UNUSED_ARG(pOwner); ACE_UNUSED_ARG(pPkg); ACE_UNUSED_ARG(wPkgLen); return true; };
	////DB保存信息;egister(G_D_DBSAVE_INFO, OnDbSaveInfo );
	static bool OnDbSaveInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen);
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public:
	/// 获取玩家角色列表
	static bool OnSelectRoleList(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen);

	/// 获取玩家信息
	static bool OnSelectPlayerInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen);

	/// 新增玩家角色
	static bool OnInsertPlayerInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen);

	/// 更新玩家信息
	static bool OnUpdatePlayerInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen);

	/// 删除玩家信息
	static bool OnDeletePlayerInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen);

	/// 获取物品批量流水号
	static bool OnGetBatchItemSeq(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen);

	/// 获取客户数据
	static bool OnGetClientData(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen);

	/// 保存客户数据
	static bool OnSaveClientData(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen);

	//查询玩家是否存在
	static bool OnQueryPlayerExist( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//请求关卡地图信息
	static bool OnReqCityInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	//更新关卡地图信息
	static bool OnUpdateCityInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdatePlayerItemInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnUpdatePlayerTaskRdInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnUpdatePlayerChangeBaseInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen );

	static bool OnHeartbeatCheck( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );

	static bool OnQueryOfflinePlayerInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  );
};

#endif/*DEAL_PKG_H*/
