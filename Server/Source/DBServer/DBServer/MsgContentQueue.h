﻿
#ifndef MSGCONTENT_QUEUQ_H
#define MSGCONTENT_QUEUQ_H


#include "../../Test/testthreadque_wait/cirqueue.h"

#define MAX_MSGQUEUE_COUNT 10

struct MsgToPutContent;
typedef CirQueue< MsgToPutContent, 1024, 10/*发送通知阈值*/ >  LSMSGCONTENT_QUEUE;

extern LSMSGCONTENT_QUEUE* msgToPutContentQueue[MAX_MSGQUEUE_COUNT];

LSMSGCONTENT_QUEUE* GET_MSGCONTENT_QUEUE( int );


#define PUSH_MSG_TO_QUEUE( MSG ){\
	LSMSGCONTENT_QUEUE* pMsgQueue = Get_MsgQueue();\
	if ( pMsgQueue ) pMsgQueue->PushEle(  &MSG , 1 );\
}\

#endif


