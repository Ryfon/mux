﻿#include "GateSrvManager.h"
#include "OtherServer.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/PkgProc/PacketBuild.h"
#include "../../Base/PkgProc/DealPkgBase.h"
#include "MsgContentQueue.h"

void CGateSrvManger::PushGateSrv(CGateServer* pServer )
{
	if ( !pServer )
		return;

	mGateSrvVec.push_back( pServer );
}

CGateServer* CGateSrvManger::GetValidGateSrv()
{
	if ( mGateSrvVec.empty() )
		return NULL;

	return mGateSrvVec[0];
}

void CGateSrvManger::RemoveGateSrv(CGateServer* pGateSrv )
{
	std::vector<CGateServer *>::iterator lter = mGateSrvVec.begin();
	for ( ;lter != mGateSrvVec.end(); ++lter )
	{
		if ( pGateSrv == *lter )
		{
			mGateSrvVec.erase( lter );
			return;
		}
	}
}

CGateServer* CGateSrvManger::GetGateSrvBySessionID(unsigned int wID )
{
	if ( mGateSrvVec.empty() )
		return NULL;

	std::vector<CGateServer *>::iterator lter = mGateSrvVec.begin();
	for ( ;lter != mGateSrvVec.end(); ++lter )
	{
		CGateServer* pSrv = *lter;
		if ( pSrv  && pSrv->GetSessionID() == (int)wID )
			return pSrv;
	}

	return NULL;
}

void CGateSrvManger::TrySendMsg()
{

#ifdef MULTI_DB_HASH


#define  READ_MSGCONTENT_SIZE 10

	MsgToPutContent* pMsgContentArr[READ_MSGCONTENT_SIZE] = {NULL};
	int validNum = 0;
	for ( unsigned int i = 0 ; i < MAX_MSGQUEUE_COUNT; i++ )
	{
		LSMSGCONTENT_QUEUE* pMsgQueue = GET_MSGCONTENT_QUEUE( i );
		if ( pMsgQueue )
		{
			do 
			{
				pMsgQueue->PopEle( pMsgContentArr, READ_MSGCONTENT_SIZE , validNum );

				if(validNum > sizeof(pMsgContentArr)/sizeof(pMsgContentArr[0]))
				{
					D_ERROR("CGateSrvManger::TrySendMsg下标越界,validNum = %d\n", validNum);
					continue;
				}

				for ( int j = 0 ; j < validNum ; j++ )
				{
					SendMsgToPut( pMsgContentArr[j] );
					delete pMsgContentArr[j];
					pMsgContentArr[j] = NULL;
				}

			} while( validNum == READ_MSGCONTENT_SIZE );
		}
	}

#endif

	ACE_Guard<ACE_Thread_Mutex> guard( mutex_ );

	if ( !mMsgContentVector.empty() )
	{
		for( std::vector<MsgToPutContent *>::iterator lter = mMsgContentVector.begin(); 
			lter != mMsgContentVector.end(); 
			++lter )
		{
			SendMsgToPut( *lter );
		}

		for ( std::vector<MsgToPutContent *>::iterator lter = mMsgContentVector.begin(); 
			lter != mMsgContentVector.end(); 
			++lter )
		{
			delete *lter;
		}

		mMsgContentVector.clear();
	}
}

void CGateSrvManger::PushMsgContentToVecotr( MsgToPutContent* pContent )
{
	ACE_Guard<ACE_Thread_Mutex> guard( mutex_ );

	mMsgContentVector.push_back( pContent );
}

void CGateSrvManger::SendMsgToPut( MsgToPutContent* pContent )
{
	if ( !pContent )
		return;

	CGateServer* pGateSrv = GetGateSrvBySessionID( pContent->sessionID );
	if ( !pGateSrv )
		return;

	MsgToPut *pMsgToPut = NULL;
	switch( pContent->msgType )
	{
	case DGDeletePlayerInfoReturn::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGDeletePlayerInfoReturn, pGateSrv,  *(( DGDeletePlayerInfoReturn *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGNewPlayerInfoReturn::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGNewPlayerInfoReturn, pGateSrv, *( (DGNewPlayerInfoReturn *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGQueryPlayerExistRst::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGQueryPlayerExistRst, pGateSrv, *( (DGQueryPlayerExistRst *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGCityInfoResult::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGCityInfoResult, pGateSrv, *( (DGCityInfoResult *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGPlayerInfo::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGPlayerInfo, pGateSrv, *( (DGPlayerInfo *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGPlayerInfoLogin::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGPlayerInfoLogin, pGateSrv, *((DGPlayerInfoLogin *)&pContent->szContent[0] )  );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGDbGetPre::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGDbGetPre, pGateSrv, *((DGDbGetPre *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGDbGetInfo::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGDbGetInfo, pGateSrv, *((DGDbGetInfo *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGDbGetPost::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGDbGetPost, pGateSrv, *((DGDbGetPost *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGExceptionNotify::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGExceptionNotify, pGateSrv, *((DGExceptionNotify *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	case DGBatchItemSequence::wCmd:
		{
			pMsgToPut = CreateSrvPkg( DGBatchItemSequence , pGateSrv , *((DGBatchItemSequence *)&pContent->szContent[0] ) );
			pGateSrv->SendPkgToSrv( pMsgToPut );
		}
		break;
	default:
		break;
	}

}


