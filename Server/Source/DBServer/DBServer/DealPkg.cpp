﻿#include "DealPkg.h"

#include "RecordHandler.h"
#include "PlayerInfoManager.h"
#include "OtherServer.h"
#include "ConfigInfoManager.h"
#include "../../Base/PkgProc/MsgToPut.h"
#include "../../Base/PkgProc/PacketBuild.h"
#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/Utility.h"
#include <vector>

#include "Task/TaskPool.h"
#include "Task/SelectRoleTask.h"
#include "Task/SelectPlayerInfoTask.h"
#include "Task/InsertPlayerInfoTask.h"
#include "Task/UpdatePlayerInfoTask.h"
#include "Task/DeletePlayerTask.h"
#include "Task/QueryPlayerExistTask.h"
#include "Task/SelectCityTask.h"
#include "Task/UpdateProtectItemTask.h"
#include "Task/UpdateTaskRdInfo.h"
#include "Task/UpdateCityInfoTask.h"
#include "Task/UpdatePlayerItemTask.h"
#include "Task/UpdatePlayerTaskInfoTask.h"
#include "Task/UpdatePlayerBaseInfoTask.h"
#include "Task/GetItemSequenceTask.h"
#include "Task/QueryOffLinePlayerInfo.h"

#include "Task/DbPlayerSaveTask.h"

#ifdef MULTI_DB_HASH
#include "TaskPoolManager.h"
#endif

using namespace MUX_PROTO;

CDealGateSrvPkg::CDealGateSrvPkg(void)
{
}

CDealGateSrvPkg::~CDealGateSrvPkg(void)
{
}

void CDealGateSrvPkg::Init(void)
{
	Register(G_D_PLAYER_INFO_LOGIN, OnSelectRoleList);
	Register(G_D_PLAYER_INFO, OnSelectPlayerInfo);
	Register(G_D_NEW_PLAYER_INFO, OnInsertPlayerInfo);
	Register(G_D_UPDATE_PLAYER_INFO, OnUpdatePlayerInfo);
	Register(G_D_DELETE_PLAYER_INFO, OnDeletePlayerInfo);
	Register(G_D_BATCH_ITEM_SEQUENCE, OnGetBatchItemSeq);
	Register(G_D_GET_CLIENT_DATA, OnGetClientData);
	Register(G_D_SAVE_CLIENT_DATA, OnSaveClientData);
	Register(G_D_QUERY_PLAYER_EXIST, OnQueryPlayerExist );
	Register(G_D_REQ_CITY_INFO,  OnReqCityInfo );
	Register(G_D_UPDATE_CITY_INFO,  OnUpdateCityInfo );
	Register(G_D_UPDATE_ITEM_INFO, OnUpdatePlayerItemInfo );
	Register(G_D_UPDATE_TASK_INFO, OnUpdatePlayerTaskRdInfo );
	Register(G_D_HEARTBEAT_CHECK_RST, OnHeartbeatCheck);
	Register(G_D_UPDATE_PLAYER_BASE_INFO, OnUpdatePlayerChangeBaseInfo );
	Register(G_D_QUERY_OFFLINE_PLAYER_INFO_REQ, OnQueryOfflinePlayerInfo);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//#define G_D_DBSAVE_PRE                   0x17c5     //DB保存开始;
	Register(G_D_DBSAVE_PRE, OnDbSavePre );
	//#define G_D_DBSAVE_POST                  0x17c6     //DB保存结束(与M_G_DBSAVE_POST一一对应);
	Register(G_D_DBSAVE_POST, OnDbSavePost );
	//#define G_D_DBSAVE_INFO                  0x17c8     //DB保存信息;
	Register(G_D_DBSAVE_INFO, OnDbSaveInfo );
	///新信息获取/DB存盘相关协议以及玩家离开地图协议--理清相关逻辑与操作, by dzj, 09.05.12
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return;
}

////DB保存信息;Register(G_D_DBSAVE_INFO, OnDbSaveInfo );
bool CDealGateSrvPkg::OnDbSaveInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
	{
		return false;
	}

	TRY_BEGIN;

	GDDbSaveInfo* pDbSaveInfo = (GDDbSaveInfo*) pPkg;
	if ( pDbSaveInfo->infoType == DI_PROTECTITEM )
	{
		FullPlayerInfoEx *pFullPlayerInfoEx = CPlayerInfoManagerSingle::instance()->FindPlayerInfo( pDbSaveInfo->playerUID );
		if(NULL == pFullPlayerInfoEx)
			return false;

		pFullPlayerInfoEx->RecvUpdateProtectItemInfo( pDbSaveInfo->protectInfo.mProtectItemArr, pDbSaveInfo->protectInfo.mCount );
		if ( pDbSaveInfo->protectInfo.isEnd  )
		{
			UpdateProtectItemTask* pProtectSaveTask = NEW UpdateProtectItemTask( pOwner, pFullPlayerInfoEx->GetProtectItemArr(), pDbSaveInfo->playerUID );
#ifndef MULTI_DB_HASH
			TaskPoolSingle::instance()->PushTask( (ITask *)pProtectSaveTask );
#else
			CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szAccount,
				                                                           pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szNickName,
																		   (ITask *)pProtectSaveTask);
#endif
		}
	}
	else if( pDbSaveInfo->infoType == DI_TASKRD )
	{
		FullPlayerInfoEx *pFullPlayerInfoEx = CPlayerInfoManagerSingle::instance()->FindPlayerInfo( pDbSaveInfo->playerUID );
		if(NULL == pFullPlayerInfoEx)
			return false;

		pFullPlayerInfoEx->RecvUpdateTaskRdInfo( pDbSaveInfo->taskRdInfo.mTaskRdIArr, pDbSaveInfo->taskRdInfo.mCount );
		if( pDbSaveInfo->taskRdInfo.isEnd )
		{
			UpdateTaskRdInfoTask* pUpdateTaskRdInfo = NEW UpdateTaskRdInfoTask( pOwner, pFullPlayerInfoEx->GetTaskRdInfoArr(),pDbSaveInfo->playerUID );
#ifndef MULTI_DB_HASH
			TaskPoolSingle::instance()->PushTask( (ITask *)pUpdateTaskRdInfo );
#else
			CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szAccount,
																		   pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szNickName,
																		   (ITask *)pUpdateTaskRdInfo);
#endif

		}
	}
	else
	{
		DbPlayerSaveTask* pDbPlayerSaveTask = NEW DbPlayerSaveTask( pOwner, pDbSaveInfo );
#ifndef MULTI_DB_HASH
		TaskPoolSingle::instance()->PushTask( (ITask *)pDbPlayerSaveTask );
#else
		CTaskPoolExManagerSingle::instance()->PushTaskByRoleID(pDbSaveInfo->playerUID, (ITask *)pDbPlayerSaveTask);
#endif
	}


	return true;
	TRY_END;
	return true;

}


bool CDealGateSrvPkg::OnSelectRoleList(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	//// 角色列表请求
	GDPlayerInfoLogin *pPlayerInfo = (GDPlayerInfoLogin *)pPkg;

	SelectRoleTask* pSelectRoleTask = NEW SelectRoleTask(pOwner,pPlayerInfo);
#ifndef MULTI_DB_HASH	
	TaskPoolSingle::instance()->PushTask( (ITask *)pSelectRoleTask );
#else
	CTaskPoolExManagerSingle::instance()->PushTaskByAccount(pPlayerInfo->szAccount, (ITask *)pSelectRoleTask);
#endif

	return true;
	TRY_END;
	return true;
}

bool CDealGateSrvPkg::OnSelectPlayerInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	// 角色信息请求
	GDPlayerInfo *pPlayerInfo = (GDPlayerInfo *)pPkg;

	SelectPlayerInfoTask* pSelectplayerTask = NEW SelectPlayerInfoTask( pOwner, pPlayerInfo );
#ifndef MULTI_DB_HASH	
	TaskPoolSingle::instance()->PushTask( (ITask *)pSelectplayerTask );
#else
	CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(pPlayerInfo->szAccount, pPlayerInfo->strNickName, (ITask *)pSelectplayerTask);
#endif

	return true;
	TRY_END;
	
	return true;
}

bool CDealGateSrvPkg::OnInsertPlayerInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	// 新增请求
	GDNewPlayerInfo *pNewPlayerInfo = (GDNewPlayerInfo *)pPkg;
	InsertPlayerInfoTask* pInsertplayerTask = NEW InsertPlayerInfoTask( pOwner,pNewPlayerInfo );
#ifndef MULTI_DB_HASH
	TaskPoolSingle::instance()->PushTask( (ITask *)pInsertplayerTask );
#else
	CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(pNewPlayerInfo->szAccount, pNewPlayerInfo->strNickName, (ITask *)pInsertplayerTask );
#endif
	return true;
	TRY_END;

	return true;
}

bool CDealGateSrvPkg::OnUpdatePlayerInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	// 更新请求
	GDUpdatePlayerInfo *pUpdatePlayerInfo = (GDUpdatePlayerInfo *)pPkg;
	
	// 获取玩家
	FullPlayerInfoEx *pFullPlayerInfoEx = CPlayerInfoManagerSingle::instance()->FindPlayerInfo(pUpdatePlayerInfo->uiRoleID);
	if(NULL == pFullPlayerInfoEx)
	{
		pFullPlayerInfoEx = NEW FullPlayerInfoEx;
		if(NULL==pFullPlayerInfoEx)
			return false;

		StructMemSet(*pFullPlayerInfoEx, 0x00, sizeof(*pFullPlayerInfoEx));
		CPlayerInfoManagerSingle::instance()->AddPlayerInfo(pUpdatePlayerInfo->uiRoleID, pFullPlayerInfoEx);
	}

	// 追加分片数据
	ReceiveFullStr<GDUpdatePlayerInfo, FullPlayerInfo>(pUpdatePlayerInfo, &(pFullPlayerInfoEx->fullPlayerInfo));
	
	//是否结束？
	if(pUpdatePlayerInfo->bEnd)
	{
		UpdatePlayerInfoTask* pUpdatePlayerInfoTask = NEW UpdatePlayerInfoTask( pOwner, pFullPlayerInfoEx ,pUpdatePlayerInfo->isOffline );
#ifndef MULTI_DB_HASH
		TaskPoolSingle::instance()->PushTask( (ITask *)pUpdatePlayerInfoTask );
#else
		CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szAccount, 
			                                                           pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szNickName,
																	   (ITask *)pUpdatePlayerInfoTask );
#endif
	}

	return true;

	TRY_END;

	return true;
}

bool CDealGateSrvPkg::OnDeletePlayerInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	// 删除请求
	GDDeletePlayerInfo *pDeletePlayerInfo = (GDDeletePlayerInfo *)pPkg;

	DeletePlayerTask* pDeletePlayerTask = NEW DeletePlayerTask( pOwner,pDeletePlayerInfo );
#ifndef MULTI_DB_HASH	
	TaskPoolSingle::instance()->PushTask( (ITask *)pDeletePlayerTask );
#else
	CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(pDeletePlayerInfo->szAccount, pDeletePlayerInfo->strNickName,  (ITask *)pDeletePlayerTask );
#endif
	return true;
	TRY_END;
	return true;
}

bool CDealGateSrvPkg::OnGetBatchItemSeq(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	// 物品流水号请求
	GDBatchItemSequence *pItemSeqReq = (GDBatchItemSequence *)pPkg;
#ifndef MULTI_DB_HASH
	// 分配流水号
	DGBatchItemSequence itemSeqResp;
	StructMemSet(itemSeqResp, 0x00, sizeof(itemSeqResp));
	itemSeqResp.uiServerID = pItemSeqReq->uiServerID;
	
	unsigned int uiStartSeq = 0u;
	unsigned int uiEndSeq = 0u;
	if(CRecordHandler::GetBatchID(uiStartSeq, uiEndSeq) == -1)
		itemSeqResp.bSuccess = false;
	else
	{
		itemSeqResp.bSuccess = true;
		itemSeqResp.uiStartSequence = uiStartSeq;
		itemSeqResp.uiEndSequence = uiEndSeq;
	}

	//发送结果
	//MsgToPut *pMsgToPut = CPacketBuild::CreatePkg(pOwner->GetHandleID(), pOwner->GetSessionID(), &itemSeqResp);
	MsgToPut *pMsgToPut = CreateSrvPkg( DGBatchItemSequence, pOwner, itemSeqResp );
	pOwner->SendPkgToSrv(pMsgToPut);
#else
	CGetItemSequenceTask* pGetItemSequenceTask = NEW CGetItemSequenceTask( pOwner,pItemSeqReq );
	CTaskPoolExManagerSingle::instance()->PushTask((ITask*)pGetItemSequenceTask);
#endif/*MULTI_DB_HASH*/
	return true;

	TRY_END;

	return true;
}

bool CDealGateSrvPkg::OnGetClientData(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;
		
	GDGetClientData *pReq = (GDGetClientData *)pPkg;
	FullPlayerInfoEx *pFullPlayerInfo = CPlayerInfoManagerSingle::instance()->FindPlayerInfo(pReq->uiRoleID);
	if( pFullPlayerInfo != NULL)
	{		
		unsigned short usIndex = 0;
		unsigned int uiLeft = sizeof(pFullPlayerInfo->clientData);
		while(uiLeft > 0)
		{
			DGGetClientData resp;
			StructMemSet(resp, 0x00, sizeof(resp));
			resp.playerID = pReq->playerID;

			if(uiLeft >= sizeof(resp.clientData))
			{	
				StructMemCpy(resp.clientData, pFullPlayerInfo->clientData + usIndex * sizeof(resp.clientData), sizeof(resp.clientData));
				resp.dataSize = sizeof(resp.clientData);
				resp.isOK = (uiLeft > sizeof(resp.clientData)? false : true);
				resp.index = usIndex;

				uiLeft -= sizeof(resp.clientData);
				usIndex++;
			}
			else
			{
				StructMemCpy(resp.clientData, pFullPlayerInfo->clientData + usIndex * sizeof(resp.clientData), uiLeft);
				resp.dataSize = uiLeft;
				resp.isOK = true;
				resp.index = usIndex;

				uiLeft = 0;
			}

			MsgToPut *pMsgToPut = CreateSrvPkg( DGGetClientData, pOwner, resp );
			pOwner->SendPkgToSrv(pMsgToPut);
		}
	}

	return false;
	TRY_END;

	return true;
}

bool CDealGateSrvPkg::OnSaveClientData(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen)
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;
	
	GDSaveClientData *pReq = (GDSaveClientData *)pPkg;
	FullPlayerInfoEx *pFullPlayerInfoEx = CPlayerInfoManagerSingle::instance()->FindPlayerInfo(pReq->uiRoleID);
	if( pFullPlayerInfoEx != NULL)
	{
		if((pReq->index * sizeof(pReq->clientData) + pReq->dataSize) <= sizeof(pFullPlayerInfoEx->clientData))
			memcpy(pFullPlayerInfoEx->clientData + pReq->index * sizeof(pReq->clientData), pReq->clientData, pReq->dataSize);
	}

	return false;
	TRY_END;

	return true;
}


bool CDealGateSrvPkg::OnQueryPlayerExist(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	GDQueryPlayerExist* pReq = ( GDQueryPlayerExist *)pPkg;

	QueryPlayerExistTask* pTask = NEW QueryPlayerExistTask( pOwner, pReq->recvPlayerName, pReq->queryUID, pReq->queryType );
#ifndef MULTI_DB_HASH
	TaskPoolSingle::instance()->PushTask( (ITask *)pTask );
#else
	CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(NULL, pReq->recvPlayerName, (ITask *)pTask);
#endif

	return false;
	TRY_END;

	return true;
}


bool CDealGateSrvPkg::OnReqCityInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	GDReqCityInfo* pReq = ( GDReqCityInfo *)pPkg;

	SelectCityTask* pTask = NEW SelectCityTask( pOwner, *pReq );
#ifndef MULTI_DB_HASH
	TaskPoolSingle::instance()->PushTask( (ITask *)pTask );
#else
	CTaskPoolExManagerSingle::instance()->PushTask((ITask *)pTask);
#endif

	return false;
	TRY_END;

	return true;
}



bool CDealGateSrvPkg::OnUpdateCityInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	GDUpdateCityInfo* pReq = ( GDUpdateCityInfo *)pPkg;

	UpdateCityInfoTask* pTask = NEW UpdateCityInfoTask( pOwner, pReq->updateCityInfo );
#ifndef MULTI_DB_HASH	
	TaskPoolSingle::instance()->PushTask( (ITask *)pTask );
#else
	CTaskPoolExManagerSingle::instance()->PushTask((ITask *)pTask);
#endif

	return false;
	TRY_END;

	return true;
}

bool CDealGateSrvPkg::OnUpdatePlayerItemInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	GDUpdateItemInfo* pReq = ( GDUpdateItemInfo *)pPkg;

	FullPlayerInfoEx *pFullPlayerInfoEx = CPlayerInfoManagerSingle::instance()->FindPlayerInfo(pReq->playerUID);
	if ( pFullPlayerInfoEx )
	{
		if( pReq->isEquip )
		{
			if ( pReq->itemIndex >= EQUIP_SIZE )
			{
				D_ERROR("更新玩家装备出错,位置错误 %d\n", pReq->itemIndex );
				return false;
			}

			pFullPlayerInfoEx->fullPlayerInfo.equipInfo.equips[pReq->itemIndex] = pReq->itemInfo; 
		}
		else
		{
			if ( pReq->itemIndex >= PACKAGE_SIZE )
			{
				D_ERROR("更新玩家道具包出错,位置错误 %d\n", pReq->itemIndex );
				return false;
			}

			pFullPlayerInfoEx->fullPlayerInfo.pkgInfo.pkgs[pReq->itemIndex] = pReq->itemInfo;
		}


		UpdatePlayerItemTask* pTask = NEW UpdatePlayerItemTask( pOwner, pFullPlayerInfoEx->GetEquipItemArr() , pFullPlayerInfoEx->GetPkgItemArr() , pReq->playerUID );
#ifndef MULTI_DB_HASH
		TaskPoolSingle::instance()->PushTask( (ITask *)pTask );
#else
		CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szAccount,
														               pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szNickName,
																	   (ITask *)pTask);
#endif
	}

	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnUpdatePlayerTaskRdInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	GDUpdateTaskRdInfo* pReq = ( GDUpdateTaskRdInfo *)pPkg;

	FullPlayerInfoEx *pFullPlayerInfoEx = CPlayerInfoManagerSingle::instance()->FindPlayerInfo(pReq->playerUID);
	if ( pFullPlayerInfoEx )
	{
		if ( pReq->taskIndex >= MAX_TASKRD_SIZE)
			return false;

		pFullPlayerInfoEx->fullPlayerInfo.taskInfo[ pReq->taskIndex ] = pReq->taskInfo;

		UpdatePlayerTaskInfoTask* pTask = NEW UpdatePlayerTaskInfoTask( pOwner, pFullPlayerInfoEx->GetTaskRdArr(), pReq->playerUID );
#ifndef MULTI_DB_HASH	
		TaskPoolSingle::instance()->PushTask( (ITask *)pTask );
#else
		CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szAccount,
																	   pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szNickName,
																	   (ITask *)pTask);
#endif
	}

	return true;

	TRY_END;

	return false;
}

bool CDealGateSrvPkg::OnUpdatePlayerChangeBaseInfo(CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen )
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	GDUpdatePlayerChangeBaseInfo* pBaseChangeInfo = ( GDUpdatePlayerChangeBaseInfo *)pPkg;

	FullPlayerInfoEx* pFullPlayerInfo = CPlayerInfoManagerSingle::instance()->FindPlayerInfo(pBaseChangeInfo->playerUID);
	if ( pFullPlayerInfo )
	{
		pFullPlayerInfo->fullPlayerInfo.baseInfo.usMapID  = pBaseChangeInfo->usMapID;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.iPosX = pBaseChangeInfo->iPosX;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.iPosY = pBaseChangeInfo->iPosY;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.uiHP  = pBaseChangeInfo->uiHP;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.uiMP  = pBaseChangeInfo->uiMP;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.sGoodEvilPoint = pBaseChangeInfo->sGoodEvilPoint;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.uiEvilTime = pBaseChangeInfo->uiEvilTime;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.usLevel = pBaseChangeInfo->usLevel;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.dwExp = pBaseChangeInfo->dwExp;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.usPhysicsLevel = pBaseChangeInfo->usPhysicsLevel;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.usMagicLevel = pBaseChangeInfo->usMagicLevel;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.dwPhysicsExp = pBaseChangeInfo->dwPhysicsExp;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.dwMagicExp   = pBaseChangeInfo->dwMagicExp;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.usPhysicsAllotPoint = pBaseChangeInfo->usPhysicsAllotPoint;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.usMagicAllotPoint = pBaseChangeInfo->usMagicAllotPoint;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.usPhyTotalGetPoint = pBaseChangeInfo->usPhyTotalGetPoint;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.usMagTotalGetPoint = pBaseChangeInfo->usMagTotalGetPoint;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.dwSilverMoney = pBaseChangeInfo->dwSilverMoney;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.dwGoldMoney = pBaseChangeInfo->dwGoldMoney;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.racePrestige = pBaseChangeInfo->racePrestige;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.strength =  pBaseChangeInfo->strength;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.vitality =  pBaseChangeInfo->vitality;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.agility =  pBaseChangeInfo->agility;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.spirit =  pBaseChangeInfo->spirit;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.intelligence =  pBaseChangeInfo->intelligence;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.unallocatePoint =  pBaseChangeInfo->unallocatePoint;
		pFullPlayerInfo->fullPlayerInfo.baseInfo.totalExp = pBaseChangeInfo->totalExp;
		

		UpdatePlayerBaseInfoTask* pTask = NEW UpdatePlayerBaseInfoTask( pOwner, pFullPlayerInfo->fullPlayerInfo.baseInfo, pBaseChangeInfo->playerUID);
#ifndef MULTI_DB_HASH
		TaskPoolSingle::instance()->PushTask( (ITask *)pTask );
#else
		CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(pFullPlayerInfo->fullPlayerInfo.baseInfo.szAccount,
			pFullPlayerInfo->fullPlayerInfo.baseInfo.szNickName,
			(ITask *)pTask);
#endif

		return true;
	}

	return false;

	TRY_END;

	return false;
}

bool CDealGateSrvPkg::OnHeartbeatCheck( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	GDHeartbeatCheckRst* pReq = ( GDHeartbeatCheckRst *)pPkg;
	pOwner->SetLastHeartbeatConfirmTime();
	//D_DEBUG("收到session=%d的gateserver的心跳应答:sequenceid=%d\n", pOwner->GetSessionID(), pReq->sequenceID);
	return true;

	TRY_END;
	return false;
}

bool CDealGateSrvPkg::OnQueryOfflinePlayerInfo( CGateServer* pOwner, const char* pPkg, const unsigned short wPkgLen  )
{
	if((pOwner == NULL) || (pPkg == NULL) || (wPkgLen == 0))
		return false;

	TRY_BEGIN;

	GDQueryOffLinePlayerInfoReq* pReq = ( GDQueryOffLinePlayerInfoReq *)pPkg;

	CQueryOffLinePlayerTask* pTask = NEW CQueryOffLinePlayerTask( pOwner, pReq->req.targetName, pReq->lNotiPlayerID );
	if(NULL == pTask)
	{	
		D_ERROR("CDealGateSrvPkg::OnQueryOfflinePlayerInfo, new 失败\n");
		return false;
	}

#ifndef MULTI_DB_HASH
	TaskPoolSingle::instance()->PushTask( (ITask *)pTask );
#else
	CTaskPoolExManagerSingle::instance()->PushTaskByAccountAndName(NULL, pReq->req.targetName, (ITask *)pTask);
#endif

	return true;
	TRY_END;

	return false;
}

