﻿#include "MsgContentQueue.h"
#include "../../Base/Utility.h"


LSMSGCONTENT_QUEUE* msgToPutContentQueue[MAX_MSGQUEUE_COUNT] = { NULL };

LSMSGCONTENT_QUEUE* GET_MSGCONTENT_QUEUE( int msgQueueID ) 
{
	if( msgQueueID == -1 )
		return  NULL;

	if ( msgQueueID >= sizeof(msgToPutContentQueue)/sizeof(msgToPutContentQueue[0]) )
	{
		D_ERROR("msgQueueID >= sizeof(msgToPutContentQueue)/sizeof(msgToPutContentQueue[0])下标越界，msgQueueID=%d\n", msgQueueID);
		return NULL;
	}

	return msgToPutContentQueue[msgQueueID];
}