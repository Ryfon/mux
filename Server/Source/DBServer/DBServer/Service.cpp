﻿#include "Service.h"
#include "DealPkg.h"
#include "PlayerInfoManager.h"
#include "ConfigInfoManager.h"
#include "../../Base/MysqlWrapper.h"
#include "../../Base/DBConfigManager.h"
#include "../../Base/LoadConfig/LoadSrvConfig.h"
#include "GateSrvManager.h"

#include "Task/TaskPool.h"
#include "OtherServer.h"
#include <string>

#include "../../Test/testthreadque_wait/sigexception/sigexception.h"
#ifdef MULTI_DB_HASH
#include "TaskPoolManager.h"
#endif


void FastTimer()
{
}

void SlowTimer()
{
	CGateServerManager::HeartbeartCheckTimer();

	return;
}


#ifdef USE_SIG_TRY
bool ExceptionCheck()
{
	if( g_isException )
	{
		DGExceptionNotify expMsg;
		expMsg.exceptionReason = 0;
		expMsg.srvId = CLoadSrvConfig::GetSelfID();
		CGateServer* pGateSrv = GateSrvMangerSingle::instance()->GetValidGateSrv();
		if( NULL != pGateSrv )
		{
			MsgToPutContent* pContent = NEW MsgToPutContent;
			pContent->msgType = DGExceptionNotify::wCmd;
			pContent->sessionID = pGateSrv->GetSessionID();
			StructMemCpy( pContent->szContent, &expMsg, sizeof(expMsg) );
			GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );
		}
	}
	return true;
}
#endif //USE_SIG_TRY

///时钟检测函数，循环执行以确定是否需要执行快时钟或慢时钟；
void TimerCheck()
{
	static ACE_Time_Value fastLastTime = ACE_OS::gettimeofday();
	static ACE_Time_Value slowLastTime = ACE_OS::gettimeofday();
	ACE_Time_Value curTime = ACE_OS::gettimeofday();
	ACE_Time_Value fastPastTime = curTime - fastLastTime;
	ACE_Time_Value slowPastTime = curTime - slowLastTime;
	if ( fastPastTime.msec() > 361 ) //361毫秒；
	{
		//激活快时钟；
		fastLastTime = curTime;
		FastTimer();
	} 
	if ( slowPastTime.msec() > 517 ) //517毫秒；
	{
		//激活慢时钟；
		slowLastTime = curTime;
		SlowTimer();
	}

#ifdef USE_SIG_TRY
	static ACE_Time_Value exceptionCheckLastTime = ACE_OS::gettimeofday();
	ACE_Time_Value exceptionPastTime = curTime - exceptionCheckLastTime;
	if( exceptionPastTime.sec() >= 10 )   //每10秒检测一次
	{
		exceptionCheckLastTime = curTime;
		ExceptionCheck();
	}
#endif //USE_SIG_TRY
	return;
}


extern IBufQueue *g_pSrvSender;


CService::CService(void)
: m_pOutFileStream(NULL), m_pReadNetMsgQueue(NULL), m_pSendNetMsgQueue(NULL), 
  m_pManCommandler(NULL)
#ifndef USE_DSIOCP
  , m_pNetTask(NULL)
#endif //USE_DSIOCP
{

}

CService::~CService(void)
{

}

/// 检查表(不处理内存泄漏，因为检查失败，服务必须停)；
bool CService::CheckTable( const char* cmd ,const char* account )
{
    stConnectionString* pConnStr = CDBConfigManagerSingle::instance()->Find();

	if ( NULL == pConnStr )
	{
		D_ERROR( "CService::CheckTable, NULL == pConnStr\n" );
		return false;
	}

	// 判断连接串
	if(    (strncmp(pConnStr->szHost,     "", strlen(pConnStr->szHost)) == 0)
		|| (strncmp(pConnStr->szUser,     "", strlen(pConnStr->szUser)) == 0)
		|| (strncmp(pConnStr->szPassword, "", strlen(pConnStr->szPassword)) == 0)
		|| (strncmp(pConnStr->szDb,       "", strlen(pConnStr->szDb)) == 0)   )
	{
		D_ERROR( "连接信息不充分\n" );
		return false;
	}

	// 初始化mysql对象
	MYSQL* pMysql = mysql_init(NULL);
	if(NULL == pMysql)
	{
		D_ERROR( "mysql初始化失败" );
		return false;
	}

	// 连接
	if( mysql_real_connect( pMysql, pConnStr->szHost, pConnStr->szUser, pConnStr->szPassword, pConnStr->szDb
		  , pConnStr->port, NULL, CLIENT_MULTI_STATEMENTS) == NULL)
	{
		D_ERROR( "CheckTable时, 连DB出错,错误原因:%s.\n", mysql_error(pMysql) );
		return false;
	}

	/*const char* usetb = "use test";
	if ( mysql_real_query( pMysql, usetb, (unsigned long)strlen(usetb) ) != 0 )
	{
		D_ERROR( "CheckTable时, use test错,原因:%s.\n", mysql_error(pMysql) );
		return false;
	}*/


	//关于关卡地图表的CHECK,只有0的DBSRV上才有关卡地图数据表 
	if( CLoadSrvConfig::GetSelfID() == 0 )
	{
		if( !CheckCityTable( pMysql ) )
		{
			D_ERROR("检查关卡数据表中出错,停止服务器\n");
			return false;
		}
	}

	const char* desctb = "desc playerinfo";//playerinfo";	
	if( mysql_real_query( pMysql, desctb, (unsigned long)strlen(desctb) ) != 0 )
	{
		D_ERROR( "CheckTable时, desc playerinfo错,原因:%s.\n", mysql_error(pMysql) );
		D_WARNING("尚无建立playerinfo的数据表,现在开始建立\n");
		//建表语句添加,
		const char* createplayerinfo = "CREATE TABLE playerinfo(\
									   ID int(11) unsigned NOT NULL AUTO_INCREMENT,\
									   Account varchar(32) NOT NULL DEFAULT '',\
									   Nickname varchar(32) DEFAULT NULL,\
									   BaseData blob,\
									   EquipData blob,\
									   PkgData blob,\
									   SkillData blob,\
									   BufferData blob,\
									   StateData blob,\
									   TaskData blob,\
									   petinfo blob,\
									   SuitData blob,\
									   FashionData blob,\
									   ProtectItemData blob,\
									   TaskRecord blob,\
									   CopyStageInfo blob,\
									   ClientData varchar(1024) DEFAULT NULL,\
									   CreateTime datetime DEFAULT NULL,\
									   PRIMARY KEY (ID,Account),\
									   KEY nickname_idx (Nickname),\
									   KEY account_idx  (Account)\
									   ) ENGINE=MyISAM AUTO_INCREMENT=3116104 DEFAULT CHARSET=latin1;";

		if( mysql_real_query(pMysql, createplayerinfo, (unsigned long)strlen(createplayerinfo)) != 0 )
		{
			D_ERROR("建表失败, create playerinfo错,原因%s.\n", mysql_error(pMysql) );
			return false;
		}

		D_INFO("建PlayerInfo表成功\n");
		mysql_close( pMysql );
		return true;
	}

	// 保存结果
	MYSQL_RES* res = mysql_store_result( pMysql );
	if ( NULL == res )
	{
		D_ERROR( "CheckTable时, store result错,原因:%s.\n", mysql_error(pMysql) );
		return false;
	}

	//字段总数;
	int fieldnum = (int)mysql_num_rows( res );
	D_INFO( "playerinfo表字段总数%d\n", fieldnum );

	int fieldPerRow = (int)mysql_num_fields(res);//用于row[0],row[1]判断
	if(fieldPerRow < 2)
	{
		D_ERROR( "CheckTable时, 字段描述信息个数小于2.\n" );
		return false;
	}

	MYSQL_ROW row;
	string fieldname;
	string fieldtype;
	for ( int i=0; i<fieldnum; ++i )
	{
		row = mysql_fetch_row(res);
		if ( 0==row )
		{
			break;
		}
		printf( "	%s:%s\n", row[0], row[1] );
		fieldname = row[0];
		fieldtype = row[1];
		switch(i)
		{
		case 10://petinfo字段blob
			if ( ( fieldname != "petinfo" )
				|| ( fieldtype != "blob" )
				)
			{
				const char* addpetinfo = "alter table playerinfo add petinfo blob after TaskData";
				D_ERROR( "playerinfo表petinfo字段属性错，请新建此字段(%s)\n", addpetinfo );
				return false;
			} else {
				D_INFO( "petinfo字段正确\n" );
			}
			break;
		case 11://SuitData字段blob
			if ( ( fieldname != "SuitData" ) || ( fieldtype != "blob" ) )
			{
				const char* addpetinfo = "alter table playerinfo add SuitData blob after petinfo";
				D_ERROR( "playerinfo表SuitData字段属性错，请新建此字段(%s)\n", addpetinfo );
				mysql_free_result(res);
				mysql_close( pMysql );
				return false;
			} else {
				D_INFO( "SuitData字段正确\n" );
			}
			break;
		case 12://FashionData字段blob
			if ( ( fieldname != "FashionData" ) || ( fieldtype != "blob" ) )
			{
				const char* addFashioninfo = "alter table playerinfo add FashionData blob after SuitData";
				D_ERROR( "playerinfo表FashionData字段属性错，请新建此字段(%s)\n", addFashioninfo );
				mysql_free_result(res);
				mysql_close( pMysql );
				return false;
			} else {
				D_INFO( "FashionData字段正确\n" );
			}
			break;
		case 13:
			{
				if ( ( fieldname != "ProtectItemData" ) || ( fieldtype != "blob" ) )
				{
					const char* addProtectItemInfo = "alter table playerinfo add ProtectItemData blob after FashionData";
					D_ERROR( "playerinfo表ProtectItemData字段属性错，请新建此字段(%s)\n", addProtectItemInfo );
					mysql_free_result(res);
					mysql_close( pMysql );
					return false;
				} else {
					D_INFO( "ProtectItemData字段正确\n" );
				}
			}
			break;
		case 14:
			{
				if ( ( fieldname != "TaskRecord" ) || ( fieldtype != "blob" ) )
				{
					const char* addTaskRecordInfo = "alter table playerinfo add TaskRecord blob after ProtectItemData";
					D_ERROR( "playerinfo表TaskRecord字段属性错，请新建此字段(%s)\n", addTaskRecordInfo );
					mysql_free_result(res);
					mysql_close( pMysql );
					return false;
				} else {
					D_INFO( "TaskRecord字段正确\n" );
				}
			}
			break;
		case 15:
			{
				if ( ( fieldname != "CopyStageInfo" ) || ( fieldtype != "blob" ) )
				{
					const char* addCopyStageInfo = "alter table playerinfo add CopyStageInfo blob after TaskRecord";
					D_ERROR( "playerinfo表CopyStageInfo字段属性错，请新建此字段(%s)\n", addCopyStageInfo );
					mysql_free_result(res);
					mysql_close( pMysql );
					return false;
				} else {
					D_INFO( "CopyStageInfo字段正确\n" );
				}
			}
			break;
		default:
			{
				;
			}
		}
	}

	mysql_free_result(res);
	mysql_close( pMysql );


	if ( cmd && strcmp("-upskill", cmd ) == 0)
	{
		D_DEBUG("运行更新SkillInfo程序....\n");
		CConnection* pConnect = NEW CConnection();
		if( pConnect->Open( pConnStr ) == -1 )
			return false;

		CQuery *pQuery = pConnect->CreateQuery();
		if(pQuery == NULL)
			return false;

		stringstream ss;
		if ( account )
		{
			ss<< "select SkillData,ID,Nickname from playerinfo where Account like '"<< account <<"'";
		}
		else
		{
			ss<< "select SkillData,ID,Nickname from playerinfo";
		}

		std::string statement = ss.str();

		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect( statement.c_str(), (unsigned long) (statement.size()));
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", statement.c_str() );
			return false;
		}

#pragma pack(push, 1)
		struct TmpSkillInfo_i
		{
			TYPE_ID                  	skillID;                                    	//skill的编号
			TYPE_ID                  	effectiveSkillID;                           	//逻辑有效的skill编号
			//USHORT                    skillExp;                                   	//技能的经验值，只针对SP技能有用
		};

		struct TmpPlayerInfo_4_Skills
		{
			//Enums
			typedef	enum
			{
				INVALIDATE	=	0,         	//
			}ItemType;

			UINT                     	    skillSize;                                  	//
			TmpSkillInfo_i              	skillData[SKILL_SIZE];                        	//
		};

		while( pResult->Next())
		{
			unsigned int updateplayerid = pResult->GetInt(1);
			D_DEBUG("开始更新角色:%s\n",  pResult->GetString(2));

			TmpPlayerInfo_4_Skills tmpskillinfo;
			StructMemSet( tmpskillinfo, 0x0, sizeof(tmpskillinfo) );
			StructMemCpy( tmpskillinfo, pResult->GetString(0), sizeof(TmpPlayerInfo_4_Skills));

			PlayerInfo_4_Skills newskillinfo;
			StructMemSet( newskillinfo,0x0, sizeof(PlayerInfo_4_Skills) );
			newskillinfo.skillSize = tmpskillinfo.skillSize;
			for ( unsigned int i = 0; i< sizeof(newskillinfo.skillData)/sizeof(newskillinfo.skillData[0]); i++ )
			{
				newskillinfo.skillData[i].effectiveSkillID/*skillExp*/ = tmpskillinfo.skillData[i].effectiveSkillID/*skillExp*/;
				newskillinfo.skillData[i].skillID  = tmpskillinfo.skillData[i].skillID;
				if ( newskillinfo.skillData[i].skillID > 0 )
				{
					D_DEBUG("玩家的新技能 skillID:%d , effectiveSkillID/*skillExp*/:%d\n", newskillinfo.skillData[i].skillID, newskillinfo.skillData[i].effectiveSkillID/*skillExp*/ );
				}
			}

			CRecordHandler::UpdatePlayerSkillInfo( pConnect, newskillinfo, updateplayerid );
			D_DEBUG("更新成功\n");
		}

		pConnect->DestroyQuery(pQuery);
	}

#pragma pack(pop)

   
	
	return true;
}


bool CService::CheckCityTable(MYSQL* pMysql )
{
	if( NULL == pMysql)
		return false;

	//关于关卡地图表的CHECK
	const char* desccitytb ="desc cityinfo";//cityinfo';
	if( mysql_real_query( pMysql, desccitytb, (unsigned long)strlen(desccitytb) ) != 0 )
	{
		D_ERROR( "CheckCityTable时, desc cityinfo错,原因:%s.\n", mysql_error(pMysql) );

		D_WARNING("尚无关卡地图的数据表,现在开始建立\n");
		//建表语句添加,读取初始建表的必要数据
		const char* createcitytb = "CREATE TABLE cityinfo(\
			ID int(11) NOT NULL AUTO_INCREMENT primary key,\
			CityLevel int(11) NOT NULL,\
			MapID int(11) NOT NULL,\
			CityExp int(11) NOT NULL,\
			Race varchar(1) NOT NULL,\
			Type varchar(1) NOT NULL\
			)ENGINE = MyISAM AUTO_INCREMENT=1;";

		if( mysql_real_query(pMysql, createcitytb, (unsigned long)strlen(createcitytb)) != 0 )
		{
			D_ERROR("建表失败, create cityinfo错,原因%s.\n", mysql_error(pMysql) );
			return false;
		}
		
		D_INFO("建表成功,现在开始读取配置文件向表中添加信息\n");
		if( !CCityInfoManagerSingle::instance()->InitCityInfo("config/cityinfo.xml") )
		{
			D_ERROR("读取城市配置文件失败，向表添加信息失败\n");
			return false;
		}

		D_INFO("读取配置成功,现在开始根据配置文件向表中插入配置的初始数据\n");
		char insertBuff[256];
		StructMemSet( insertBuff, 0, sizeof(insertBuff) );
		CityInfo* pCityInfo = NULL; 
		for( unsigned int i =0; i< CCityInfoManagerSingle::instance()->GetSize(); ++i )
		{
			pCityInfo = CCityInfoManagerSingle::instance()->GetCityInfo( i );
			if( NULL == pCityInfo )
				continue;

			/*	insertstream	<<"insert into cityinfo values('"  << pCityInfo->uiCityID
								<<"','" << pCityInfo->cityLevel	
								<<"','" << pCityInfo->uiMapID
								<<"','" << pCityInfo->cityExp
								<<"','" << pCityInfo->ucRace
								<<"','" << pCityInfo->ucType  <<"');";*/

			sprintf( insertBuff, "insert into cityinfo values('0','%d','%d','%d','%d','%d');", pCityInfo->cityLevel, pCityInfo->uiMapID, pCityInfo->cityExp, pCityInfo->ucRace, pCityInfo->ucType);
			

			if( mysql_real_query(pMysql, insertBuff, (unsigned long)strlen(insertBuff)) != 0 )
			{
				D_ERROR("插入数据失败, insert cityInfo错,原因%s.\n", mysql_error(pMysql) );
				break;
			}

			D_INFO("插入一条记录,ID:%d CityLevel:%d MapID:%d CityExp:%d Race:%d Type:%d\n", pCityInfo->uiCityID, pCityInfo->cityLevel, pCityInfo->uiMapID
				, pCityInfo->cityExp, pCityInfo->ucRace, pCityInfo->ucType );

			//insertstream.clear();		//为记录下一次的
		}
	}else{		
		//清除记录为以后的查询作准备
		MYSQL_RES* res = mysql_store_result( pMysql );
		mysql_free_result( res );
	}
	return true;
}

int CService::Open(void *pArgs)
{
	ACE_UNUSED_ARG(pArgs);

	// 返回值
	int iRet = -1;

	// 设置日志属性
	//if(this->setLog() == -1)
	//	return -1;

	// 装载配置文件
	if(this->LoadConfigFile() == -1)
		return -1;
	
	// 设置其他
	if(this->SetOther() == -1)
		return -1;

	// 创建中间队列
	m_pReadNetMsgQueue = NEW CBufQueue;
	m_pSendNetMsgQueue = NEW CBufQueue;
	if((m_pReadNetMsgQueue != NULL) && (m_pSendNetMsgQueue != NULL))
	{
		g_pSrvSender = m_pSendNetMsgQueue;

#ifdef USE_DSIOCP
		m_srvIocp = NEW CDsIocp< IBufQueue, IBufQueue, CDsSocket >;
		m_srvIocp->Init( 128, m_pReadNetMsgQueue, m_pSendNetMsgQueue );
		m_srvIocp->AddListen( CLoadSrvConfig::GetListenPort() );
#endif //USE_DSIOCP

		// 创建主逻辑处理模块
		m_pManCommandler = NEW CManCommHandler< CRcvPkgHandler<CServerConnection> >;  ///句柄管理类
		if(m_pManCommandler != NULL)
		{
			m_pManCommandler->SetNetQueue(m_pReadNetMsgQueue);//收包句柄管理，置收包来源(收包队列)
			m_pManCommandler->SetHandlePoolSize( 256 );//收包句柄管理，设置收包句柄对象池

#ifdef USE_DSIOCP
			return 0;
#else //USE_DSIOCP
#ifdef USE_NET_TASK
			// 创建任务
			m_pNetTask = NEW CNetTask<CHandlerT>(m_pReadNetMsgQueue, m_pSendNetMsgQueue);
			ACE_INET_Addr addr(CLoadSrvConfig::GetListenPort());
			if((m_pNetTask != NULL) && (m_pNetTask->open(&addr) == 0))
			{
				iRet = 0;
			}
#else
			// 创建任务
			m_pNetTask = NEW CNetTaskEx<CHandlerEx, ACE_Thread_Mutex>(m_pReadNetMsgQueue, m_pSendNetMsgQueue);
			ACE_INET_Addr addr(CLoadSrvConfig::GetListenPort());
			if((m_pNetTask != NULL) && (m_pNetTask->Open(&addr) == 0))
			{
				iRet = 0;
			}
#endif/*USE_NET_TASK*/
#endif //USE_DSIOCP
		}
	}

	if(iRet == -1)
	{
		Close();
	}

	return iRet;
}

int CService::Run(void)
{
	//主逻辑线程
	ACE_Time_Value startRunTime = ACE_OS::gettimeofday();
	ACE_Time_Value nowTime;
	ACE_Time_Value interval;

	D_INFO( "dbsrv:运行中...\n" );

	const unsigned long runTime = CLoadSrvConfig::GetRunTime() * 1000;
	ACE_UNUSED_ARG( runTime );
	while (!g_isGlobeStop)
	{
		nowTime = ACE_OS::gettimeofday();
		interval = nowTime - startRunTime;
		if ( ( runTime>0 )  //runtime == 0 不中断运行；
			&& ( interval.msec()>runTime )
			)
		{
#ifndef USE_DSIOCP
			m_pNetTask->Quit();
			g_pSrvSender->SetReadEvent( true );//通知网络模块发送
#endif //USE_DSIOCP
			D_DEBUG( "runTime时刻到，退出\n" );
			break;
		}

		ST_SIG_CATCH {

			TimerCheck();

			// 处理接受到所有消息
			m_pManCommandler->CommTimerProc();

			GateSrvMangerSingle::instance()->TrySendMsg();

			g_pSrvSender->SetReadEvent( false );//通知网络模块发送

			//ACE_Time_Value tmpTime;//等待一会；
			//tmpTime.sec(0);
			//tmpTime.usec( 1000 );
			//ACE_OS::sleep( tmpTime );

		} END_SIG_CATCH;
	}

#ifndef USE_DSIOCP
	// 等待网络线程
#ifdef USE_NET_TASK
	m_pNetTask->wait();
#else
	m_pNetTask->Wait();
#endif/*USE_NET_TASK*/
#endif //USE_DSIOCP

	return 0;
}

int CService::Close(void)
{
	//TaskPoolSingle::instance()->CloseThread(0);

#ifndef USE_DSIOCP
	// 删除网络模块
	if(m_pNetTask != NULL)
	{
		delete m_pNetTask;
		m_pNetTask = NULL;
	}
#endif //USE_DSIOCP

	// 删除主逻辑模块
	if(m_pManCommandler != NULL)
	{
		delete m_pManCommandler;
		m_pManCommandler = NULL;
	}

#ifdef USE_DSIOCP
	delete m_srvIocp; m_srvIocp = NULL;
#endif //USE_DSIOCP

	// 删除发送队列对象
	if(m_pSendNetMsgQueue != NULL)
	{
		delete m_pSendNetMsgQueue;
		m_pSendNetMsgQueue = NULL;
	}

	// 删除读队列对象
	if(m_pReadNetMsgQueue != NULL)
	{
		delete m_pReadNetMsgQueue;
		m_pReadNetMsgQueue = NULL;
	}

	return 0;
}

int CService::SetLog(void)
{
	// 设置日志输出到文件
	//ACE_LOG_MSG->clr_flags (ACE_Log_Msg::STDERR);
	ACE_LOG_MSG->set_flags (ACE_Log_Msg::OSTREAM);

	const char *filename = "dbserver.log";
	m_pOutFileStream = NEW ofstream(filename, ios::out | ios::trunc);
	if(NULL == m_pOutFileStream)
		return -1;

	ACE_LOG_MSG->msg_ostream(m_pOutFileStream, 1);
	return 0;
}

int CService::LoadConfigFile(void)
{
	int iRet = 0;

	// 装入服务器配置文件
	if(!CLoadSrvConfig::LoadConfig())
	{
		D_DEBUG("装入服务器配置文件出错\n");
		return -1;
	}

	D_INFO( "服务类型DBSrv，序号:%d\n", CLoadSrvConfig::GetSelfID() );

	// 装入角色初始化配置文件
	iRet = CRoleInitManagerSingle::instance()->Init("config/initroleinfo.xml");
	if(-1 == iRet)
	{
		D_DEBUG("装入角色初始化配置文件出错\n");
		return -1;
	}

	// 装入技能配置文件
	iRet = CSkillInfoManagerSingle::instance()->Init("config/skill.xml");
	if(-1 == iRet)
	{
		D_ERROR("装入技能配置文件出错\n");
		return -1;
	}

	// 装入数据库配置文件
	iRet = CDBConfigManagerSingle::instance()->Init("config/dbconfig.xml", "DBServer");
	if(-1 == iRet)
	{
		D_DEBUG("装入数据库配置文件\n");
		return -1;
	}

	// 连接数据库
#ifndef MULTI_DB_HASH
	CConnection * pConnection = CConnectionPoolSingle::instance()->GetConnection();
	if(pConnection == NULL)
		return -1;
	else 
	{	
		stConnectionString *pConnStr =CDBConfigManagerSingle::instance()->Find();
		if(NULL == pConnStr)
			return -1;

		//附属连接字符串 2008/06/30
		TaskPoolSingle::instance()->AttachDBConnecting( pConnStr);
		//开启线程
		TaskPoolSingle::instance()->open();

		if( pConnection->Open(pConnStr) == -1 )
		{
			D_ERROR("pConnection->Open(pConnStr) 出错\n");
#else
		if(CTaskPoolExManagerSingle::instance()->Open() == -1)
		{
			D_ERROR("CTaskPoolExManagerSingle::instance()->Open() 出错\n");
#endif/*MULTI_DB_HASH*/
			return -1;
		}
		else
		{
			iRet = CInitRoleItemManagerSingle::instance()->Init("config/initem.xml");
			if ( iRet == - 1 )
			{
				D_DEBUG("装载玩家初始装备错误\n");
				return -1;
			}
		}
#ifndef MULTI_DB_HASH
	}
#endif
	return 0;
}

int CService::SetOther(void)
{
	CDealGateSrvPkg::Init();
	return 0;
}
