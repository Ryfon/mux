﻿/** @file PlayerInfoManager.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-1-8
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef PLAYER_INFO_MANAGER_H
#define PLAYER_INFO_MANAGER_H

#include "../../Base/PkgProc/SrvProtocol.h"
#include "../../Base/Utility.h"
#include <map>

using namespace MUX_PROTO;

#pragma pack(push, 1)
typedef struct StrFullPlayerInfoEx
{
	FullPlayerInfo fullPlayerInfo;
	char clientData[1024];
	ProtectItem mProtectItemArr[PACKAGE_SIZE];//道具的保护信息
	unsigned int mTaskRdArr[100];//所有的任务存盘信息


public:
	StrFullPlayerInfoEx()
	{
		StructMemSet( fullPlayerInfo, 0x0, sizeof(fullPlayerInfo) );
		StructMemSet( mProtectItemArr, 0x0,sizeof(mProtectItemArr) );
		StructMemSet( mTaskRdArr, 0x0, sizeof(mTaskRdArr) );
	}

	void RecvUpdateProtectItemInfo( ProtectItem* protectItemArr, unsigned int pageIndex )
	{
		if ( pageIndex>=8 )
		{
			D_ERROR("RecvUpdateProtectItemInfo pageIndex = %d >= 8\n", pageIndex );
			return;
		}

		if ( pageIndex == 0 )
			StructMemSet( mProtectItemArr , 0x0, sizeof(mProtectItemArr));

		ACE_OS::memcpy( mProtectItemArr + pageIndex * PKG_PAGE_SIZE,protectItemArr, sizeof(ProtectItem)*PKG_PAGE_SIZE );
	}

	ProtectItem* GetProtectItemArr() { return mProtectItemArr; }

	unsigned int GetProtectItemLen() { return sizeof(mProtectItemArr); }

	void RecvUpdateTaskRdInfo( unsigned* taskRdArr, unsigned int pageIndex )
	{
		if ( pageIndex >= 1 )
		{
			D_ERROR("RecvUpdateTaskRdInfo pageIndex = %d >= 1\n",pageIndex );
			return;
		}

		if ( pageIndex == 0 )
			StructMemSet( mTaskRdArr, 0x0, sizeof(mTaskRdArr) );

		ACE_OS::memcpy( mTaskRdArr + pageIndex*TASK_RD_PAGE_SIZE, taskRdArr, sizeof(unsigned)*TASK_RD_PAGE_SIZE );
	}

	//玩家新的任务存盘
	unsigned* GetTaskRdInfoArr() { return mTaskRdArr; }

	//玩家新的任务存盘的长度
	unsigned int GetTaskRdInfoLen() { return  sizeof(mTaskRdArr); }


	//获取玩家的装备信息
	const ItemInfo_i* GetEquipItemArr() { return fullPlayerInfo.equipInfo.equips; }

	//获取玩家的包裹信息 
	const ItemInfo_i* GetPkgItemArr() { return fullPlayerInfo.pkgInfo.pkgs; }

	//获取玩家的任务信息
	const TaskRecordInfo* GetTaskRdArr() { return fullPlayerInfo.taskInfo; }
}
FullPlayerInfoEx;

#pragma pack(pop)

/**
* @class CPlayerInfoManager
*
* @brief 对玩家信息进行缓存
* 
*/
/************************************************************************/
/* 管传淇修改，将CPlayerInfoManager改成线程安全型,因为DB该用了线程池    */
/************************************************************************/


class CPlayerInfoManager
{
	//friend class ACE_Singleton<CPlayerInfoManager, ACE_Null_Mutex>;

	friend class ACE_Singleton<CPlayerInfoManager, ACE_Thread_Mutex>;

private:
	/// 构造
	CPlayerInfoManager(void);

public:
	/// 析构
	~CPlayerInfoManager(void);

public:
	/// 增加
	int AddPlayerInfo(unsigned int uiPlayerID, FullPlayerInfoEx *pFullPlayerInfo);

	/// 删除
	void DeletePlayerInfo(unsigned int uiPlayerID);

	/// 查找
	FullPlayerInfoEx *FindPlayerInfo(unsigned int uiPlayerID);

	/*/// 数目
	unsigned int size(void);*/

private:
	/// 玩家列表
	std::map<unsigned int, FullPlayerInfoEx*> m_playerInfos;
	
#ifdef ACE_WIN32
	DsCS             thread_mutex_;
#else  //ACE_WIN32
	ACE_Thread_Mutex thread_mutex_;
#endif //ACE_WIN32
};


class ThreadMutex_Guard
{
public:
	explicit ThreadMutex_Guard( ACE_Thread_Mutex& thread_mutex ):ace_thread_mutex(thread_mutex)
	{
		ace_thread_mutex.acquire();
	}

	~ThreadMutex_Guard()
	{
		ace_thread_mutex.release();
	}


private:
	ACE_Thread_Mutex& ace_thread_mutex;
};


//typedef ACE_Singleton<CPlayerInfoManager, ACE_Null_Mutex> CPlayerInfoManagerSingle;

typedef ACE_Singleton<CPlayerInfoManager, ACE_Thread_Mutex> CPlayerInfoManagerSingle;

#endif/*PLAYER_INFO_MANAGER_H*/
