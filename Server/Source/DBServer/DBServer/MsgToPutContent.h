﻿#ifndef MSGTOPUT_CONTENT_H  
#define MSGTOPUT_CONTENT_H

#include "../../Base/Utility.h"

struct MsgToPutContent
{
	unsigned int sessionID;
	unsigned int msgType;
	char szContent[512];

	MsgToPutContent():sessionID(0),msgType(0)
	{
	   StructMemSet( szContent, 0x0, sizeof(szContent) );
	}

};

#endif
