﻿/** @file Service.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-1-2
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef SERVICE_H
#define SERVICE_H

//#include "../../Base/NetTask.h"
#include "../../Base/NetTaskEx.h"
#include "../../Base/BufQueue/BufQueue.h"
#include "../../Base/tcache/tcache.h"
#include "../../Base/PkgProc/RcvPkgHandler.h"
#include "../../Base/PkgProc/CManCommHandler.h"
#include "RecordHandler.h"
#include "ServerConnection.h"
#include <fstream>


/**
* @class CService
*
* @brief Dbserver提供的所有功能的封装
* 
*/
class CService
{
public:
	/// 构造
	CService(void);

	/// 析构
	virtual ~CService(void);

private:
	///
	CService(const CService &);

	///
	CService & operator=(const CService &);

public:
	/// 初始化
	virtual int Open(void *pArgs = NULL);

	/// 运行
	virtual int Run(void);

	/// 关闭
	virtual int Close(void);

private:
	/// 设置日志属性
    int SetLog(void);

	/// 载入配置文件
	int LoadConfigFile(void);

	/// 设置其他
	int SetOther(void);

public:
	/// 检查表(不处理内存泄漏，因为检查失败，服务必须停)；
	bool CheckTable( const char* cmd ,const char* account );

	//检查关卡数据表,没表就新建表并从配置文件填入基本数据
	bool CheckCityTable( MYSQL* pMySql );

private:
	/// 文件流
	ofstream *m_pOutFileStream;

	/// 中间队列
	IBufQueue *m_pReadNetMsgQueue;
	IBufQueue *m_pSendNetMsgQueue;

	/// 主逻辑模块
	CManCommHandler< CRcvPkgHandler<CServerConnection> > *m_pManCommandler;
#ifdef USE_DSIOCP
	CDsIocp< IBufQueue, IBufQueue, CDsSocket >* m_srvIocp;
#else //USE_DSIOCP
	/// 网络模块
#ifdef USE_NET_TASK
	CNetTask<CHandlerT> *m_pNetTask;
#else
	CNetTaskEx<CHandlerEx, ACE_Thread_Mutex> *m_pNetTask;
#endif/*USE_NET_TASK*/
#endif //USE_DSIOCP
};

#endif/*SERVICE_H*/
