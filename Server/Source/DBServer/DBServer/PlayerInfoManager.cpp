﻿#include "PlayerInfoManager.h"


CPlayerInfoManager::CPlayerInfoManager(void):thread_mutex_()
{
}

CPlayerInfoManager::~CPlayerInfoManager(void)
{
	if ( !m_playerInfos.empty() )
	{
		std::map<unsigned int, FullPlayerInfoEx*>::iterator lter = m_playerInfos.begin();
		for ( ; lter != m_playerInfos.end(); lter++ )
		{
			FullPlayerInfoEx* pFullPlayer = (FullPlayerInfoEx* )lter->second;
			if ( pFullPlayer )
			{
				delete pFullPlayer;
				pFullPlayer = NULL;

			}
		}
		m_playerInfos.clear();
	}
	
}

int CPlayerInfoManager::AddPlayerInfo(unsigned int uiPlayerID, FullPlayerInfoEx *pPlayerInfo)
{
	//ThreadMutex_Guard thread_guard( thread_mutex_ );
	int result = -1;

#ifdef ACE_WIN32
	DsMutex guard(thread_mutex_);
#else  //ACE_WIN32
	ACE_GUARD_RETURN( ACE_Thread_Mutex, guard, thread_mutex_, -1 );
#endif //ACE_WIN32

	std::map<unsigned int, FullPlayerInfoEx*>::iterator iter = m_playerInfos.find(uiPlayerID);
	if(m_playerInfos.end() != iter)
	{
		D_ERROR("CPlayerInfoManager::AddPlayerInfo,插入玩家信息时发现玩家信息已存在,uiplayerid=%d\n", uiPlayerID);
	}
	else
	{	
		m_playerInfos[uiPlayerID] = pPlayerInfo;
		result = 0;
	}

	return result;
}

void CPlayerInfoManager::DeletePlayerInfo(unsigned int uiPlayerID)
{
#ifdef ACE_WIN32
	DsMutex guard(thread_mutex_);
#else  //ACE_WIN32
	ACE_GUARD( ACE_Thread_Mutex, guard, thread_mutex_ );
#endif //ACE_WIN32
	
	std::map<unsigned int, FullPlayerInfoEx*>::iterator lter = m_playerInfos.find( uiPlayerID );
	if ( lter != m_playerInfos.end() )
	{
		FullPlayerInfoEx* pFullPlayer = (FullPlayerInfoEx* )lter->second;
		if ( pFullPlayer )
		{
			delete pFullPlayer;
			pFullPlayer = NULL;
		}
		m_playerInfos.erase( lter );
	}

	return;
}

FullPlayerInfoEx *CPlayerInfoManager::FindPlayerInfo(unsigned int uiPlayerID)
{
#ifdef ACE_WIN32
	DsMutex guard(thread_mutex_);
#else  //ACE_WIN32
	ACE_GUARD_RETURN( ACE_Thread_Mutex, guard, thread_mutex_, NULL );
#endif //ACE_WIN32

	std::map<unsigned int, FullPlayerInfoEx*>::iterator iter = m_playerInfos.find(uiPlayerID);
	if(iter != m_playerInfos.end())
		return iter->second;
	else
		return NULL;
}

//unsigned int CPlayerInfoManager::size(void)
//{
//	ThreadMutex_Guard thread_guard( thread_mutex_ );
//
//	return (unsigned int)(m_playerInfos.size());
//}
