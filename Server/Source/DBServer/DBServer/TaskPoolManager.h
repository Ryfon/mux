﻿#ifndef TASK_POOL_MANAGER_H
#define TASK_POOL_MANAGER_H

#include "../../Base/MysqlWrapper.h"
#include "../../Base/aceall.h"
#include "Task/ITask.h"


#ifdef MULTI_DB_HASH

class CSequenceGenerator
{
public:
	CSequenceGenerator(void);
	~CSequenceGenerator(void);

public:
	unsigned int GetNextSequence(CConnection *pConn);

	int GetSequenceRanage(CConnection *pConn, unsigned int &tempStart, unsigned int &tempEnd);

private:

	int SelectMaxSequence(CConnection *pConn, unsigned int &tempStart, unsigned int &tempEnd);

	int InsertSequenceRecord(CConnection *pConn, unsigned int tempStart, unsigned int tempEnd);

private:
	unsigned int maxSequence;//可用序列号最大
	unsigned int nextSequence;//可用序列好下一个值
};

class CTaskPoolEx : public ACE_Task<ACE_MT_SYNCH>
{
public:
	enum
	{
		MAX_CONNECTION_PER_TASK = 1//连接数目
	};

public:
	CTaskPoolEx(void);

	virtual ~CTaskPoolEx(void);

public:
	virtual int open(void *args  = 0 );

	virtual int svc(void);

	//设置id
	void SetId(unsigned short id);

	//插入任务块
	virtual int PushTask( ITask* pTask );

	//获取后续的序列号
	unsigned int GetNextSequenceID(void);

private:
	//连接到db
	virtual int ConnectDB(void);

	//处理任务块
	virtual int PrcocessTask( ITask* pTask );

private:
	unsigned short id;//taskpool自身编号

	CConnection connectionPool[MAX_CONNECTION_PER_TASK];//连接池

	CSequenceGenerator seqGenerator;
};


class CTaskPoolExManager
{
	friend class ACE_Singleton<CTaskPoolExManager, ACE_Null_Mutex>;

	enum
	{
		MAX_TASK_POOL_NUM = 5
	};

private:
	CTaskPoolExManager(void);
	~CTaskPoolExManager(void);

public:
	//初始化
	int Open(void);

	//插入到taskPoolEx中(和角色无关的时候)
	int PushTask( ITask* pTask );

	//根据账号插入到taskPoolEx中(只知道账号的时候使用)
	int PushTaskByAccount(const char *account, ITask* pTask);

	//根据账号和角色名插入到taskPoolEx中(角色信息没有缓存的情况下)
	int PushTaskByAccountAndName(const char *account, const char *name, ITask* pTask);

	//根据角色ID插入到taskPoolEx中(角色信息已经缓存的情况下)
	int PushTaskByRoleID(const unsigned int roleID, ITask* pTask);
private:
	//hash函数(根据账号获取指定的taskPool)
	unsigned short HashAccount(const char *account);

	//hash函数(根据名字获取指定的connection)
	unsigned short HashRoleName(const char *nickname);
	
private:
	CTaskPoolEx taskPoolGroup[MAX_TASK_POOL_NUM];//任务池组

};

typedef ACE_Singleton<CTaskPoolExManager, ACE_Null_Mutex> CTaskPoolExManagerSingle;

#endif/*MULTI_DB_HASH*/

#endif/*TASK_POOL_MANAGER_H*/
