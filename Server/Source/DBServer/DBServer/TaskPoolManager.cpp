﻿#include "TaskPoolManager.h"
#include "../../Base/DBConfigManager.h"
#include "PlayerInfoManager.h"
#include "MsgContentQueue.h"


#ifdef MULTI_DB_HASH

CSequenceGenerator::CSequenceGenerator(void)
	:maxSequence(0), nextSequence(1)
{

}

CSequenceGenerator::~CSequenceGenerator(void)
{

}

unsigned int CSequenceGenerator::GetNextSequence(CConnection *pConn)
{
	if(NULL ==  pConn)
		return 0;

	if(nextSequence > maxSequence)
	{
		unsigned int tempStart, tempEnd;
		tempStart = tempEnd = 0;
		GetSequenceRanage(pConn, tempStart, tempEnd);
		if((tempStart > maxSequence) && (tempStart < tempEnd))
		{
			nextSequence = tempStart;
			maxSequence = tempEnd;
		}
		else
		{
			return 0;
		}
	}

	return nextSequence++;
}

int CSequenceGenerator::GetSequenceRanage(CConnection *pConn, unsigned int &tempStart, unsigned int &tempEnd)
{
	unsigned int uiCurrentStartID = 0;
	unsigned int uiCurrentEndID = 0;

	TRY_BEGIN;

	// 获取当前最大编号(起始编号，终止编号)
	if(SelectMaxSequence(pConn, uiCurrentStartID, uiCurrentEndID) == -1)	
		return -1;

	uiCurrentStartID = uiCurrentEndID + 1;
	uiCurrentEndID = uiCurrentEndID + 100000;

	if(InsertSequenceRecord(pConn, uiCurrentStartID, uiCurrentEndID) == -1)
		return -1;

	tempStart = uiCurrentStartID;
	tempEnd = uiCurrentEndID;

	return 0;
	TRY_END;

	return 0;
}

int CSequenceGenerator::SelectMaxSequence(CConnection *pConn, unsigned int &tempStart, unsigned int &tempEnd)
{
	int iRet = 0;

	TRY_BEGIN;

	// 创建查询
	if(pConn == NULL)
		return -1;

	CQuery *pQuery = pConn->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"SELECT StartID, EndID FROM idgenerator WHERE EndID =(SELECT MAX(EndID) FROM idgenerator)" );
	if(iStatementLen <= 0 )
		iRet = -1;
	else
	{
		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", szStatement);
			iRet = -1;
		}
		else
		{
			// 处理结果集
			if(pResult->Next())
			{
				tempStart = pResult->GetInt(0);
				tempEnd = pResult->GetInt(1);
			}

			// 销毁查询
			pQuery->FreeResult(pResult);
		}
	}

	// 销毁查询
	pConn->DestroyQuery(pQuery);

	return iRet;

	TRY_END;
	return 0;
}

int CSequenceGenerator::InsertSequenceRecord(CConnection *pConn, unsigned int tempStart, unsigned int tempEnd)
{
	// 返回值
	int iRet = 0;

	TRY_BEGIN;

	// 创建查询
	if(pConn == NULL)
		return -1;

	CQuery *pQuery = pConn->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	std::stringstream ss;
	ss << "INSERT INTO idgenerator (StartID, EndID) VALUES("
		<< tempStart << ","
		<< tempEnd  << ")";

	std::string statement = ss.str();

	// 执行查询
	iRet = pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) );

	// 销毁查询
	pConn->DestroyQuery(pQuery);

	return iRet;

	TRY_END;
	return 0;
}

CTaskPoolEx::CTaskPoolEx(void)
{

}

CTaskPoolEx::~CTaskPoolEx(void)
{

}

int CTaskPoolEx::open(void *args)
{
	ACE_UNUSED_ARG(args);

	//连接到db
	return ConnectDB();
}

int CTaskPoolEx::svc(void)
{
	D_DEBUG("编号=%d的任务池内部执行线程开始\n", id);
	static unsigned int totalThreadIndex = 0;

	unsigned int msgQueueID = totalThreadIndex ++;
	if(msgQueueID >= sizeof(msgToPutContentQueue)/sizeof(msgToPutContentQueue[0]))
	{
		D_ERROR("CTaskPoolEx::svc() msgQueueID >= sizeof()/sizeof(),其中msgQueueID=%d,线程id=%d退出\n", msgQueueID, id);
		return -1;
	}

	msgToPutContentQueue[ msgQueueID] = NEW LSMSGCONTENT_QUEUE(NULL);
	
	ACE_Message_Block* mb = NULL;
	while ( this->getq(mb) != -1 )
	{
		if( mb->msg_type() == ACE_Message_Block::MB_STOP )
			break;

		//获取执行任务
		char* c_data = mb->base();
		if ( c_data )
		{
			ITask* pTask = (ITask *)c_data;
			if ( pTask )
			{
				//设置该任务的消息推送队列
				pTask->SetTaskMsgQueue( msgToPutContentQueue[ msgQueueID] );

				PrcocessTask(pTask);

				delete pTask;
				pTask = NULL;

			}
		}
		mb->release();
	}

	delete msgToPutContentQueue[msgQueueID];

	D_DEBUG("编号=%d的任务池内部执行线程结束\n", id);
	return 0;
}

void CTaskPoolEx::SetId(unsigned short Id)
{
	id = Id;
	return;
}

int CTaskPoolEx::PushTask( ITask* pTask )
{
	if ( !pTask )
		return -1;

	char* c_data = (char *)pTask;

	ACE_Message_Block* mb = NULL;

	ACE_NEW_NORETURN(mb, ACE_Message_Block(c_data));
	if(NULL == mb)
	{
		delete pTask;
		return -1;
	}

	//插入数据
	if ( this->putq(mb) == -1 )
	{
		delete pTask;
		mb->release(); 
		return -1;
	}

	return 0;
}

unsigned int CTaskPoolEx::GetNextSequenceID(void)
{
	return seqGenerator.GetNextSequence(&connectionPool[0]);
}

int CTaskPoolEx::ConnectDB(void)
{
	std::map<unsigned short, stConnectionString *> &connectionInfos = CDBConfigManagerSingle::instance()->ConnectionInfos();
	if(connectionInfos.size() != MAX_CONNECTION_PER_TASK)
	{
		D_ERROR("配置文件中连接数目不等于缺省数目%d\n", MAX_CONNECTION_PER_TASK);
		return -1;
	}

	unsigned short i = 0;
	for(std::map<unsigned short, stConnectionString *>::iterator iter = connectionInfos.begin(); connectionInfos.end() != iter; iter++)
	{
		CConnection *pConn = &connectionPool[i];
		if(-1 == pConn->Open(iter->second))
		{
			D_ERROR("连接到%s/%s@%s出错\n", iter->second->szDb, iter->second->szUser, iter->second->szHost);
			return -1;
		}

		i++;
	}

	return 0;
}

int CTaskPoolEx::PrcocessTask( ITask* pTask )
{
	if(NULL == pTask)
		return -1;

	//所有连接上的操作
	pTask->OnFullConnection( connectionPool, MAX_CONNECTION_PER_TASK);

	//指定连接上的操作
	unsigned short connetionID = pTask->GetConnectionID();
	if(connetionID < MAX_CONNECTION_PER_TASK)
	{
		CConnection *pConn = &connectionPool[connetionID];
		pTask->OnThread(pConn);
	}
	else
	{
		D_ERROR("CTaskPoolEx::PrcocessTask,下标越界connetionID=%d\n", connetionID);
		return -1;
	}

	//结果处理
	pTask->OnResult();
	
	return 0;
}


CTaskPoolExManager::CTaskPoolExManager(void)
{
	for(int i = 0; i < MAX_TASK_POOL_NUM; i++)
	{
		taskPoolGroup[i].SetId(i);
	}
}

CTaskPoolExManager::~CTaskPoolExManager(void)
{

}

int CTaskPoolExManager::Open(void)
{
	for(int i = 0; i < MAX_TASK_POOL_NUM; i++)
	{
		if(taskPoolGroup[i].open() == -1)
			return -1;
	}

	for(int i = 0; i < MAX_TASK_POOL_NUM; i++)
	{
		if(taskPoolGroup[i].activate() == -1)
			return -1;
	}

	return 0;
}

int CTaskPoolExManager::PushTask( ITask* pTask )
{
	if(NULL == pTask)
		return -1;

	//设置连接id
	pTask->SetConnectionID(0, &taskPoolGroup[0]);
	taskPoolGroup[0].PushTask(pTask);

	return 0;
}

int CTaskPoolExManager::PushTaskByAccount(const char *account, ITask* pTask)
{
	if((NULL == account) || (NULL == pTask))
		return -1;

	unsigned short taskID = HashAccount(account);
	if(taskID >= MAX_TASK_POOL_NUM)
		return -1;

	//设置连接ID(按账号操作时尚没有确定角色名，connectionId=0)
	pTask->SetConnectionID(0, &taskPoolGroup[taskID]);
	taskPoolGroup[taskID].PushTask(pTask);

	return 0;
}

int CTaskPoolExManager::PushTaskByAccountAndName(const char *account, const char *name, ITask* pTask)
{
	if(NULL == pTask)
		return -1;

	unsigned short taskID = 0;
	if(NULL != account)
	{
		taskID = HashAccount(account);
		if(taskID >= MAX_TASK_POOL_NUM)
			return -1;
	}

	unsigned short connectionID = 0;
	if(NULL != name)
	{
		connectionID = HashRoleName(name);
		if(connectionID >= CTaskPoolEx::MAX_CONNECTION_PER_TASK)
		{
			D_ERROR("CTaskPoolExManager::PushTaskByAccountAndName,下标越界connectionid=%d\n", connectionID);
			return -1;
		}
	}

	//设置连接ID
	pTask->SetConnectionID(connectionID, &taskPoolGroup[taskID]);
	taskPoolGroup[taskID].PushTask(pTask);

	return 0;
}

int CTaskPoolExManager::PushTaskByRoleID(const unsigned int roleID, ITask* pTask)
{
	if(NULL == pTask)
		return -1;

	FullPlayerInfoEx *pFullPlayerInfo = CPlayerInfoManagerSingle::instance()->FindPlayerInfo(roleID);
	if(NULL == pFullPlayerInfo)
		return -1;

	return PushTaskByAccountAndName(pFullPlayerInfo->fullPlayerInfo.baseInfo.szAccount, pFullPlayerInfo->fullPlayerInfo.baseInfo.szNickName, pTask);
}

unsigned short CTaskPoolExManager::HashAccount(const char *account)
{
	if(NULL == account)
		return 0;

	if(MAX_TASK_POOL_NUM <= 1)
		return 0;
	
	unsigned int sum = 0;
	const char *pTemp = account;
	while( '\0' != *pTemp)
	{
		sum = sum+ *pTemp;
		pTemp++;
	}

	return sum % MAX_TASK_POOL_NUM;
}

unsigned short CTaskPoolExManager::HashRoleName(const char *roleName)
{
	if(NULL == roleName)
		return 0;
	
	if(CTaskPoolEx::MAX_CONNECTION_PER_TASK <= 1)
		return 0;

	unsigned int sum = 0;
	const char *pTemp = roleName;
	while( '\0' != *pTemp)
	{
		sum = sum+ *pTemp;
		pTemp++;
	}

	return sum % CTaskPoolEx::MAX_CONNECTION_PER_TASK;
}

#endif/*MULTI_DB_HASH*/
