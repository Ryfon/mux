﻿#include "ConfigInfoManager.h"
#include "../../Base/XmlManager.h"
#include "../../Base/Utility.h"
#include "RecordHandler.h"


CRoleInitManager::CRoleInitManager(void)
{
}

CRoleInitManager::~CRoleInitManager(void)
{
	Clear();
}

int CRoleInitManager::Init(const char* pszFileName)
{
	if(NULL == pszFileName)
		return -1;

	//清理
	Clear();

	CXmlManager xmlManager;
	if(!xmlManager.Load(pszFileName))
	{
		D_ERROR("玩家初始化文件加载出错");
		return -1;
	} 

	CElement *pRoot = xmlManager.Root();
	if(NULL == pRoot)
	{
		D_ERROR("配置文件可能为空");
		return -1;
	}

	std::vector<CElement *> childElements;
	xmlManager.FindChildElements(pRoot, childElements);

	unsigned short usCount = 0;
	for(std::vector<CElement *>::iterator i = childElements.begin(); i != childElements.end(); i++)
	{
		if((*i)->Level() == 1)
		{
			InitRoleInfo *pInitRoleInfo = NEW InitRoleInfo;
			if(pInitRoleInfo == NULL)
			{
				D_ERROR("初始化InitRoleInfo时出错");
				return -1;
			}

			pInitRoleInfo->usRace = /*STRING_TO_NUM((*i)->GetAttr("Race"))*/0;
			pInitRoleInfo->usClass = STRING_TO_NUM((*i)->GetAttr("Class"));
			pInitRoleInfo->bSex = STRING_TO_NUM((*i)->GetAttr("Sex"));
			pInitRoleInfo->usPhysicsLevelMax = /*STRING_TO_NUM((*i)->GetAttr("PhysicsLevelMax"))*/0;
			pInitRoleInfo->usMagicLevelMax = /*STRING_TO_NUM((*i)->GetAttr("MagicLevelMax"))*/0;
			pInitRoleInfo->usPhysicsPoint = /*STRING_TO_NUM((*i)->GetAttr("PhysicsPoint"))*/0;
			pInitRoleInfo->usMagicPoint = /*STRING_TO_NUM((*i)->GetAttr("MagicPoint"))*/0;
			pInitRoleInfo->usPortrait = STRING_TO_NUM((*i)->GetAttr("Portrait"));
			pInitRoleInfo->usHair = STRING_TO_NUM((*i)->GetAttr("Hair"));
			pInitRoleInfo->usHairColor = STRING_TO_NUM((*i)->GetAttr("HairColor"));
			pInitRoleInfo->usFace = STRING_TO_NUM((*i)->GetAttr("Face"));
			pInitRoleInfo->usMapID = STRING_TO_NUM((*i)->GetAttr("MapID"));
			pInitRoleInfo->iPosX = STRING_TO_NUM((*i)->GetAttr("PosX"));
			pInitRoleInfo->iPosY = STRING_TO_NUM((*i)->GetAttr("PosY"));
			pInitRoleInfo->ucLevel = STRING_TO_NUM((*i)->GetAttr("Level"));
			pInitRoleInfo->uiHP = STRING_TO_NUM((*i)->GetAttr("HP"));
			pInitRoleInfo->uiMP = STRING_TO_NUM((*i)->GetAttr("MP"));
			pInitRoleInfo->uiMoney = /*STRING_TO_NUM((*i)->GetAttr("Money"))*/0;
			pInitRoleInfo->str = STRING_TO_NUM((*i)->GetAttr("Str"));
			pInitRoleInfo->vit = STRING_TO_NUM((*i)->GetAttr("Vit"));
			pInitRoleInfo->agi = STRING_TO_NUM((*i)->GetAttr("Agi"));
			pInitRoleInfo->spi = STRING_TO_NUM((*i)->GetAttr("Spi"));
			pInitRoleInfo->inte = STRING_TO_NUM((*i)->GetAttr("Int"));

			
			pInitRoleInfo->uiSkill[0] = STRING_TO_NUM((*i)->GetAttr("Skill0ID"));
			pInitRoleInfo->uiSkill[1] = STRING_TO_NUM((*i)->GetAttr("Skill1ID"));
			pInitRoleInfo->uiSkill[2] = STRING_TO_NUM((*i)->GetAttr("Skill2ID"));
			pInitRoleInfo->uiSkill[3] = STRING_TO_NUM((*i)->GetAttr("Skill3ID"));
			pInitRoleInfo->uiSkill[4] = STRING_TO_NUM((*i)->GetAttr("Skill4ID"));
			pInitRoleInfo->uiSkill[5] = STRING_TO_NUM((*i)->GetAttr("Skill5ID"));
	
			// 插入
			m_playerInitInfos.push_back(pInitRoleInfo);
			usCount++;
		}		
	}

	if(usCount > 0)
		D_DEBUG("玩家初始化配置文件共有%d条记录\n", usCount);

	return 0;
}

InitRoleInfo * CRoleInitManager::Find(unsigned short usRace, unsigned short usClass, char bSex)
{
	std::vector<InitRoleInfo *>::iterator i;
	for(i = m_playerInitInfos.begin(); i != m_playerInitInfos.end(); i++)
	{
		if( /*((*i)->usRace == usRace) &&*/ ((*i)->usClass == usClass) && ((*i)->bSex == bSex))
			return *i;
	}
	
	return NULL;
}

int CRoleInitManager::Clear(void)
{
	for(std::vector<InitRoleInfo *>::iterator i = m_playerInitInfos.begin(); i != m_playerInitInfos.end(); i++)
	{
		delete *i;
		*i = NULL;
	}

	m_playerInitInfos.clear();
	
	return 0;
}


CSkillInfoManager::CSkillInfoManager(void)
{
	m_skillInfos.clear();
}

CSkillInfoManager::~CSkillInfoManager(void)
{
	Clear();
}

int CSkillInfoManager::Init(const char *pszConfigFile)
{
	//清理
	Clear();
	
	if(NULL == pszConfigFile)
	{
		D_ERROR( "加载技能配置文件出错:pszConfigFile = NULL\n" );
		return -1;
	}

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(pszConfigFile))
	{
		D_ERROR( "加载技能配置文件出错:请检查配置文件%s\n", pszConfigFile);
		return -1;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		D_ERROR( "读取技能配置文件根元素出错:请检查配置文件%s\n", pszConfigFile);
		return -1;
	}

	std::vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if( elementSet.empty())
	{
		D_ERROR( "技能配置文件为空:请检查配置文件%s\n", pszConfigFile);
		return -1;
	}

	CElement* tmpEle = NULL;
	SkillInfo* pSkillInfo = NULL;

	for(std::vector<CElement*>::iterator tmpiter = elementSet.begin(); tmpiter != elementSet.end(); ++tmpiter)
	{
		tmpEle = *tmpiter;

		if( strcmp(tmpEle->Name(), "skill" ) == 0)
		{
			pSkillInfo = NEW SkillInfo;
			if(NULL == pSkillInfo)
			{
				D_ERROR("LoadSkillConfig()分配SkillInfo对象出错\n");
				return -1;
			}

			pSkillInfo->uiSkillID = STRING_TO_NUM(tmpEle->GetAttr("nID"));
			pSkillInfo->usRace = 0;
			pSkillInfo->usClass = STRING_TO_NUM(tmpEle->GetAttr("nClass"));

			// 插入技能信息
			if(pSkillInfo->uiSkillID > 0)
			{
				if(!Insert(pSkillInfo->uiSkillID, pSkillInfo))
				{
					D_ERROR("插入技能纪录出错，技能编号=%u", pSkillInfo->uiSkillID);
					delete pSkillInfo;
					return -1;
				}
			}
		}
	}

	return 0;
}

void CSkillInfoManager::Clear(void)
{
	std::map<unsigned int, SkillInfo*>::iterator iter;
	for(iter = m_skillInfos.begin(); m_skillInfos.end() != iter; iter++)
	{
		delete iter->second;
		iter->second = NULL;
	}

	m_skillInfos.clear();
}

bool CSkillInfoManager::Insert(unsigned int uiSkillID, SkillInfo *pSkillInfo)
{
	std::map<unsigned int, SkillInfo*>::iterator iter = m_skillInfos.find(uiSkillID);
	if(m_skillInfos.end() == iter)
	{	
		m_skillInfos.insert(std::make_pair(uiSkillID, pSkillInfo));
		return true;
	}

	return false;
}

SkillInfo* CSkillInfoManager::Find(unsigned int uiID)
{
	std::map<unsigned int, SkillInfo*>::iterator iter = m_skillInfos.find(uiID);
	if(m_skillInfos.end() != iter) 
		return iter->second;
	else
		return NULL;
}

bool CSkillInfoManager::Find(unsigned int uiIDs[], unsigned short usSize, unsigned short usClass)
{
	std::map<unsigned int, SkillInfo*>::iterator iter;

	for(unsigned short i = 0; i < usSize; i++)
	{
		if(uiIDs[i] > 0)
		{
			iter = m_skillInfos.find(uiIDs[i]);
			if(m_skillInfos.end() == iter)
				return false;
			else
			{
				if(ACE_BIT_DISABLED(iter->second->usClass, 1 << usClass))
					return false;
			}
		}
	}

	return true;
}

CCityInfoManager::CCityInfoManager(void)
{

}


CCityInfoManager::~CCityInfoManager(void)
{
	Clear();
}


bool CCityInfoManager::InitCityInfo( const char* szPath )
{
	if(NULL == szPath)
	{
		D_ERROR( "InitCityInfo :szPath = NULL\n" );
		return false;
	}

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(szPath))
	{
		D_ERROR( "InitCityInfo 出错:请检查配置文件%s\n", szPath);
		return false;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		D_ERROR( "InitCityInfo 读取城市配置文件根元素出错:请检查配置文件%s\n", szPath);
		return false;
	}

	std::vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if( elementSet.empty())
	{
		D_ERROR( "城市配置文件为空:请检查配置文件%s\n", szPath );
		return false;
	}

	CElement* tmpEle = NULL;
	CityInfo* pCityInfo = NULL;

	for(std::vector<CElement*>::iterator tmpiter = elementSet.begin(); tmpiter != elementSet.end(); ++tmpiter)
	{
		tmpEle = *tmpiter;

		if( strcmp(tmpEle->Name(), "cityinfo" ) == 0)
		{
			pCityInfo = NEW CityInfo;
			if(NULL == pCityInfo)
			{
				D_ERROR("LoadSkillConfig()分配CityInfo 对象出错\n");
				return false;
			}

			pCityInfo->uiCityID = STRING_TO_NUM( tmpEle->GetAttr("ID"));
			pCityInfo->cityLevel = STRING_TO_NUM( tmpEle->GetAttr("CityLevel"));
			pCityInfo->uiMapID = STRING_TO_NUM( tmpEle->GetAttr("MapID"));
			pCityInfo->cityExp = STRING_TO_NUM( tmpEle->GetAttr("CityExp"));
			pCityInfo->ucRace = STRING_TO_NUM( tmpEle->GetAttr("Race"));
			pCityInfo->ucType = STRING_TO_NUM( tmpEle->GetAttr("Type") );

			m_vecCityInfo.push_back( pCityInfo );		
		}
	}

	return true;
}


void CCityInfoManager::Clear(void)
{
	for( std::vector<CityInfo*>::iterator iter = m_vecCityInfo.begin(); iter != m_vecCityInfo.end(); )
	{
		iter = m_vecCityInfo.erase( iter );
	}
}


unsigned int CCityInfoManager::GetSize()
{
	return (unsigned int)m_vecCityInfo.size();
}


CityInfo* CCityInfoManager::GetCityInfo( unsigned int uiIndex )
{
	if( uiIndex < m_vecCityInfo.size() )
		return m_vecCityInfo[uiIndex];
	else
	{
		D_ERROR("CCityInfoManager::GetCityInfo数组下标越界(uiIndex=%d, m_vecCityInfo.size=%d))\n", uiIndex, m_vecCityInfo.size());
		return NULL;
	}

}



CInitRoleItemManager::CInitRoleItemManager()
{
	Clear();
}

CInitRoleItemManager::~CInitRoleItemManager()
{
	Clear();
}

#ifndef MULTI_DB_HASH
unsigned int CInitRoleItemManager::GetInsertItemStart()
{
	ACE_GUARD_RETURN( ACE_Thread_Mutex, guard, thread_mutex_, -1 );

	//unsigned int itemUID = insertRoleItemStart;
	insertRoleItemStart++;
	if ( insertRoleItemEnd - insertRoleItemStart < 10  )
	{
		unsigned int start,end;
		CRecordHandler::GetBatchID( start, end );
		SetInsertItemStart( start ,end );
	}
	return insertRoleItemStart;
}
#endif

int CInitRoleItemManager::Init(const char *pszConfigFile )
{
	if(NULL == pszConfigFile)
	{
		D_ERROR( "加载技能配置文件出错:pszConfigFile = NULL\n" );
		return -1;
	}

	CXmlManager xmlLoader;
	if(!xmlLoader.Load(pszConfigFile))
	{
		D_ERROR( "加载技能配置文件出错:请检查配置文件%s\n", pszConfigFile);
		return -1;
	}

	CElement *pRoot = xmlLoader.Root();
	if(pRoot == NULL)
	{
		D_ERROR( "读取技能配置文件根元素出错:请检查配置文件%s\n", pszConfigFile);
		return -1;
	}

	std::vector<CElement*> elementSet;
	xmlLoader.FindChildElements(pRoot, elementSet);
	if( elementSet.empty())
	{
		D_ERROR( "技能配置文件为空:请检查配置文件%s\n", pszConfigFile);
		return -1;
	}

	CElement* tmpEle = NULL;

	for(std::vector<CElement*>::iterator tmpiter = elementSet.begin(); tmpiter != elementSet.end(); ++tmpiter)
	{
		tmpEle = *tmpiter;

		if( strcmp(tmpEle->Name(), "item" ) == 0)
		{
			InitRoleItemInfo* pRoleItemInfo = NEW InitRoleItemInfo;
			if(NULL == pRoleItemInfo)
			{
				D_ERROR("分配pRoleItemInfo对象出错\n");
				return -1;
			}

			pRoleItemInfo->race  = STRING_TO_NUM( tmpEle->GetAttr("race") );
			pRoleItemInfo->CLASS = STRING_TO_NUM( tmpEle->GetAttr("class") );
			pRoleItemInfo->sex   = STRING_TO_NUM( tmpEle->GetAttr("sex") );
			pRoleItemInfo->money = STRING_TO_NUM( tmpEle->GetAttr("money") );

			char ItemFormat[] = {"item%d"};
			for (unsigned int i = 0 ; i < sizeof(pRoleItemInfo->InitEquipItem)/sizeof(pRoleItemInfo->InitEquipItem[0]); i++ )
			{
				char ItemArr[10]  = {0};
#ifdef ACE_WIN32
				sprintf_s( ItemArr, sizeof(ItemArr), ItemFormat, i );
#else
				sprintf( ItemArr, ItemFormat, i ); 	 
#endif 
				pRoleItemInfo->InitEquipItem[i] = STRING_TO_NUM( tmpEle->GetAttr( ItemArr ) );
			}

			char* pUnStackItem = const_cast<char *>( tmpEle->GetAttr("item16") );
			if ( pUnStackItem )
			{
				char* p = strtok( pUnStackItem,";");
				while( p != NULL  )
				{
					InitItem initItem;
					initItem.bstack = false;

					char itemSegArr[32] ={0};
					strncpy( itemSegArr, p, sizeof(itemSegArr) );
					char* q = strchr( itemSegArr, ',' );
					if ( q )
					{
						initItem.itemTypeID = STRING_TO_NUM( itemSegArr );
						initItem.count      = STRING_TO_NUM( (q + 1) );
						pRoleItemInfo->InitPkgItem.push_back( initItem );
					}

					p = strtok(NULL,";");
				}
			}

			char* pStackItem = const_cast<char *>( tmpEle->GetAttr("item17") );
			if (  pStackItem )
			{
				char* p = strtok( pStackItem,";");
				while( p != NULL  )
				{
					InitItem initItem;
					initItem.bstack = true;

					char itemSegArr[32] ={0};
					strncpy( itemSegArr, p, sizeof(itemSegArr) );
					char* q = strchr( itemSegArr, ',' );
					if ( q )
					{
						initItem.itemTypeID = STRING_TO_NUM( itemSegArr );
						initItem.count      = STRING_TO_NUM( (q + 1) );
						pRoleItemInfo->InitPkgItem.push_back( initItem );
					}

					p = strtok(NULL,";");
				}
			}

			m_InitRoleItemInfoVec.push_back( pRoleItemInfo );
		}
	}

#ifndef MULTI_DB_HASH
	unsigned int start,end;
	CRecordHandler::GetBatchID( start, end );
	SetInsertItemStart( start ,end );
#endif

	return 0;
}

void CInitRoleItemManager::Clear()
{
	std::vector<InitRoleItemInfo *>::iterator lter = m_InitRoleItemInfoVec.begin();
	for (;lter!= m_InitRoleItemInfoVec.end(); ++lter )
	{
		delete *lter;
	}

	m_InitRoleItemInfoVec.clear();
}

InitRoleItemInfo* CInitRoleItemManager::Find(unsigned short race, unsigned short nclass, char sex )
{
	std::vector<InitRoleItemInfo *>::iterator lter = m_InitRoleItemInfoVec.begin();
	for (;lter!= m_InitRoleItemInfoVec.end(); ++lter )
	{
		InitRoleItemInfo* pRoleItem = *lter;
		if ( pRoleItem )
		{
			if ( /*pRoleItem->race == race &&*/
				pRoleItem->CLASS == nclass &&
				pRoleItem->sex   == sex )
			{
				return pRoleItem;
			}
		}
	}

	return NULL;
}
