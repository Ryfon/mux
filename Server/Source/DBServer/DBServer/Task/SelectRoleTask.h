﻿/********************************************************************
	created:	2008/06/30
	created:	30:6:2008   11:15
	file:		SelectRoleTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef _SELECTROLETASK_H_
#define _SELECTROLETASK_H_

#include "ITask.h"

class SelectRoleTask:public ITask
{
public:
	SelectRoleTask(CGateServer* pGateSrv,GDPlayerInfoLogin* pLogin ):ITask(pGateSrv),m_IsOK(false)
	{
		StructMemCpy( pLoginInfo,pLogin,sizeof(GDPlayerInfoLogin));
#ifdef MULTI_DB_HASH
		totalRoleCount = 0;
#endif
	}

	virtual ~SelectRoleTask()
	{
	}

public:

#ifdef MULTI_DB_HASH
	void OnFullConnection( CConnection *pConnect, unsigned short num);
#endif
	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

protected:
	bool m_IsOK;
	GDPlayerInfoLogin pLoginInfo;
	DGPlayerInfoLogin  playerInfoReturn;

#ifdef MULTI_DB_HASH

	DGPlayerInfoLogin  playerInfoList[4];
	int totalRoleCount;
#endif/*MULTI_DB_HASH*/

};
#endif
