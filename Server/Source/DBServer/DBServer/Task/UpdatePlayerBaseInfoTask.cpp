﻿#include "UpdatePlayerBaseInfoTask.h"


void UpdatePlayerBaseInfoTask::OnThread(CConnection* pConnect )
{
	if ( !pConnect )
		return;

	TRY_BEGIN;

	int iRet = 0;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
	{
		iRet = -1;
		return;
	}

	if ( iRet != -1 )
	{
		// 构造SQL
		char escapeBaseInfoData[2 * sizeof(mPlayerBaseInfo) + 1] = {0};
		pQuery->RealEscape(escapeBaseInfoData, (const char *)&mPlayerBaseInfo, (unsigned long) sizeof(mPlayerBaseInfo)); // 转义

		std::stringstream ss;
		ss <<  " UPDATE playerinfo SET "
			<< " BaseData='" << escapeBaseInfoData <<"' "
			<< " WHERE "
			<< " ID= " << updatePlayerUID;

		std::string statement = ss.str();

		// 执行查询
		if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
			iRet = -1;

		// 销毁查询
		pConnect->DestroyQuery(pQuery);

	}

	TRY_END;
}

void UpdatePlayerBaseInfoTask::OnResult()
{

}
