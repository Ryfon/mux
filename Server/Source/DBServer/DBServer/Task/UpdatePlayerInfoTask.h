﻿/********************************************************************
	created:	2008/09/23
	created:	23:9:2008   10:46
	file:		UpdatePlayerInfoTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef UPDATEPLAYERINFOTASK_H
#define UPDATEPLAYERINFOTASK_H

#include "ITask.h"
#include "../PlayerInfoManager.h"

class UpdatePlayerInfoTask:public ITask
{
public:
	UpdatePlayerInfoTask(CGateServer* pGateSrv,const FullPlayerInfoEx* playerInfo ,bool isOffLine ):ITask(pGateSrv),mIsOffLine(isOffLine)
	{
		StructMemCpy(mpFullPlayerInfoEx,playerInfo,sizeof(FullPlayerInfoEx) );
	}

	virtual ~UpdatePlayerInfoTask()
	{
	}

public:
	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

	int     UpdatePlayerInfo( CConnection* pConnect,const char * pszData, unsigned long ulDataLength );

private:
	FullPlayerInfoEx mpFullPlayerInfoEx;
	bool mIsOffLine;
};

#endif
