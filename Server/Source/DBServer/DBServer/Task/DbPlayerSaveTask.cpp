﻿#include "DbPlayerSaveTask.h"
#include "../../DBServer/ConfigInfoManager.h"
#include "ace/OS.h"

void DbPlayerSaveTask::OnThread(CConnection* pConnect )
{
	if ( NULL == pConnect )
	{
		return;
	}

	TRY_BEGIN;

	switch ( m_PlayerSaveInfo.infoType )
	{
	case (DI_PET):
		{
			CQuery *pQuery = pConnect->CreateQuery();
			if(pQuery == NULL)
			{
				D_WARNING( "DbPlayerSaveTask::OnThread, 建CQuery失败!\n" );
				return;
			}

			// 构造SQL
			char escapePetInfoData[2 * sizeof(m_PlayerSaveInfo.petInfo) + 1] = {0};
			pQuery->RealEscape( escapePetInfoData, (const char *)&m_PlayerSaveInfo.petInfo, (unsigned long) sizeof(m_PlayerSaveInfo.petInfo) ); // 转义

			std::stringstream ss;
			ss << "update playerinfo set petinfo = '"
				<< escapePetInfoData  << "' where Nickname = '" << m_PlayerSaveInfo.roleName << "'";

			// 执行查询
			pQuery->ExecuteUpdate( ss.str().c_str(), (unsigned long)ss.str().size() );

			// 销毁查询
			pConnect->DestroyQuery( pQuery );

			return;
		}
	case (DI_SUIT):
		{
			CQuery *pQuery = pConnect->CreateQuery();
			if(pQuery == NULL)
			{
				D_WARNING( "DbPlayerSaveTask::OnThread, 建CQuery失败!\n" );
				return;
			}

			// 构造SQL
			char escapeSuitInfoData[2 * sizeof(m_PlayerSaveInfo.suitInfo) + 1] = {0};
			pQuery->RealEscape( escapeSuitInfoData, (const char *)&m_PlayerSaveInfo.suitInfo, (unsigned long) sizeof(m_PlayerSaveInfo.suitInfo) ); // 转义

			std::stringstream ss;
			ss << "update playerinfo set suitdata = '"
				<< escapeSuitInfoData  << "' where Nickname = '" << m_PlayerSaveInfo.roleName << "'";

			// 执行查询
			pQuery->ExecuteUpdate( ss.str().c_str(), (unsigned long)ss.str().size() );

			// 销毁查询
			pConnect->DestroyQuery( pQuery );

			return;
		}
	case (DI_FASHION):
		{
			CQuery *pQuery = pConnect->CreateQuery();
			if(pQuery == NULL)
			{
				D_WARNING( "DbPlayerSaveTask::OnThread, 建CQuery失败!\n" );
				return;
			}

			// 构造SQL
			char escapeFashionInfoData[2 * sizeof(m_PlayerSaveInfo.fashionInfo) + 1] = {0};
			pQuery->RealEscape( escapeFashionInfoData, (const char *)&m_PlayerSaveInfo.fashionInfo, (unsigned long) sizeof(m_PlayerSaveInfo.fashionInfo) ); // 转义

			std::stringstream ss;
			ss << "update playerinfo set fashiondata = '"
				<< escapeFashionInfoData  << "' where Nickname = '" << m_PlayerSaveInfo.roleName << "'";

			// 执行查询
			pQuery->ExecuteUpdate( ss.str().c_str(), (unsigned long)ss.str().size() );

			// 销毁查询
			pConnect->DestroyQuery( pQuery );

			return;
		}
	case (DI_SKILL):
		{
			CQuery *pQuery = pConnect->CreateQuery();
			if(pQuery == NULL)
			{
				D_WARNING( "DbPlayerSaveTask::OnThread, 建CQuery失败!\n" );
				return;
			}

			// 构造SQL
			char escapesSkillInfoData[2 * sizeof(m_PlayerSaveInfo.skillInfo) + 1] = {0};
			pQuery->RealEscape( escapesSkillInfoData, (const char *)&m_PlayerSaveInfo.skillInfo, (unsigned long) sizeof(m_PlayerSaveInfo.skillInfo) ); // 转义

			std::stringstream ss;
			ss << "update playerinfo set SkillData = '"
				<< escapesSkillInfoData  << "' where Nickname = '" << m_PlayerSaveInfo.roleName << "'";

			// 执行查询
			pQuery->ExecuteUpdate( ss.str().c_str(), (unsigned long)ss.str().size() );

			// 销毁查询
			pConnect->DestroyQuery( pQuery );

			return;
		}
	case (DI_COPYSTAGE):
		{
			CQuery *pQuery = pConnect->CreateQuery();
			if(pQuery == NULL)
			{
				D_WARNING( "DbPlayerSaveTask::OnThread, 建CQuery失败!\n" );
				return;
			}

			// 构造SQL
			char escapeStageInfoData[2 * sizeof(m_PlayerSaveInfo.copyStageInfos) + 1] = {0};
			pQuery->RealEscape( escapeStageInfoData, (const char *)&m_PlayerSaveInfo.copyStageInfos, (unsigned long) sizeof(m_PlayerSaveInfo.copyStageInfos) ); // 转义

			std::stringstream ss;
			ss << "update playerinfo set CopyStageInfo = '"
				<< escapeStageInfoData  << "' where Nickname = '" << m_PlayerSaveInfo.roleName << "'";

			// 执行查询
			pQuery->ExecuteUpdate( ss.str().c_str(), (unsigned long)ss.str().size() );

			// 销毁查询
			pConnect->DestroyQuery( pQuery );

			return;
		}
	default:
		{
			;
		}
	}

	return;
	TRY_END;
	return;
}

//int InsertPlayerInfoTask::ExistSamePlayerName(CConnection* pConnection, const char* pszPlayerName )
//{
//	int iRet = 0;
//
//	if ( !pConnection )
//		return -1;
//
//	TRY_BEGIN;
//
//	CQuery *pQuery = pConnection->CreateQuery();
//	if(pQuery == NULL)
//		return -1;
//
//	// 构造SQL
//	char szStatement[1024];
//	int iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
//		"SELECT 1 FROM playerinfo WHERE Nickname = '%s'", 
//		pszPlayerName);
//	if(iStatementLen <= 0 )
//		iRet = -1;
//	else
//	{
//		// 执行查询
//		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
//		if(pResult == NULL)
//		{
//			D_ERROR("执行sql语句:%s错误\n", szStatement);
//			iRet = -1;
//		}
//		else
//		{
//			iRet = pResult->RowNum();
//
//			// 销毁查询
//			pQuery->FreeResult(pResult);
//		}
//	}
//
//	// 销毁查询
//	pConnection->DestroyQuery(pQuery);
//
//	return iRet;
//
//	TRY_END;
//
//	return 0;
//
//}

//void InsertPlayerInfoTask::OnThread(CConnection *pConnect)
//{
//	if ( !pConnect )
//		return;
//
//	//by dzj, 09.23 unsigned int t1 = GetTickCount();
//	unsigned int uiInsertID = 0;
//	unsigned int sequenceID = mNewPlayerInfo.sequenceID;
//	StructMemSet(newPlayerInfoReturn, 0x00, sizeof(newPlayerInfoReturn));
//	newPlayerInfoReturn.playerID = mNewPlayerInfo.playerID;
//
//	newPlayerInfoReturn.iRet = 	ExistSamePlayerName( pConnect,mNewPlayerInfo.strNickName);
//	if(newPlayerInfoReturn.iRet == -1)
//		newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
//	else if(newPlayerInfoReturn.iRet == 1)
//		newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_EXIST_SAME_NAME_ROLE;
//	else
//	{
//		char bSex = mNewPlayerInfo.wSex == SEX_MALE ? 0x01 : 0x02;  // hyn 08.12.17 
//
//		InitRoleInfo* pIntiRoleInfo = CRoleInitManagerSingle::instance()->Find(mNewPlayerInfo.wRace, mNewPlayerInfo.nClass, bSex );
//		if(pIntiRoleInfo == NULL)
//		{
//			D_DEBUG("账号%s的玩家创建角色出错:未找到对应种族=%d, 职业=%d, 性别=%d的初始化纪录\n", mNewPlayerInfo.szAccount, mNewPlayerInfo.wRace, mNewPlayerInfo.nClass, mNewPlayerInfo.wSex);
//			newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
//		}
//		else
//		{
//			if(false/*!CSkillInfoManagerSingle::instance()->Find(pIntiRoleInfo->uiSkill, sizeof(pIntiRoleInfo->uiSkill)/sizeof(pIntiRoleInfo->uiSkill[0]), pIntiRoleInfo->usClass)*/)
//			{
//				D_DEBUG("账号%s的玩家创建角色出错:未找到对应职业=%d的技能信息\n", mNewPlayerInfo.szAccount, mNewPlayerInfo.nClass);
//				newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
//			}
//			else
//			{
//				// 玩家角色信息初始化
//				FullPlayerInfo fullPlayerInfo;
//				StructMemSet(fullPlayerInfo, 0x00, sizeof(FullPlayerInfo));
//
//				// 客户端传递属性
//				StructMemCpy(fullPlayerInfo.baseInfo.szAccount,  mNewPlayerInfo.szAccount, sizeof(fullPlayerInfo.baseInfo.szAccount));
//				StructMemCpy(fullPlayerInfo.baseInfo.szNickName, mNewPlayerInfo.strNickName, sizeof(fullPlayerInfo.baseInfo.szNickName));
//				fullPlayerInfo.baseInfo.accountSize = ARRAY_SIZE(fullPlayerInfo.baseInfo.szAccount);
//				fullPlayerInfo.baseInfo.nameSize = ARRAY_SIZE(fullPlayerInfo.baseInfo.szNickName);
//				fullPlayerInfo.baseInfo.ucRace = (unsigned char)mNewPlayerInfo.wRace;
//				fullPlayerInfo.baseInfo.bSex    = bSex;
//				fullPlayerInfo.baseInfo.usClass = mNewPlayerInfo.nClass;
//
//				// 对应的缺省初始化属性
//				fullPlayerInfo.baseInfo.ucMaxPhyLevelLimit = (unsigned char)pIntiRoleInfo->usPhysicsLevelMax;
//				fullPlayerInfo.baseInfo.ucMaxMagLevelLimit = (unsigned char)pIntiRoleInfo->usMagicLevelMax;
//				fullPlayerInfo.baseInfo.usPhysicsAllotPoint = pIntiRoleInfo->usPhysicsPoint;
//				fullPlayerInfo.baseInfo.usMagicAllotPoint = pIntiRoleInfo->usMagicPoint;
//				fullPlayerInfo.baseInfo.usPortrait = mNewPlayerInfo.usPortrait; 
//				fullPlayerInfo.baseInfo.usHair =  mNewPlayerInfo.nHair;
//				fullPlayerInfo.baseInfo.usHairColor = pIntiRoleInfo->usHairColor;
//				fullPlayerInfo.baseInfo.usFace =  mNewPlayerInfo.nFace;
//				fullPlayerInfo.baseInfo.usMapID = pIntiRoleInfo->usMapID;
//				fullPlayerInfo.baseInfo.iPosX = pIntiRoleInfo->iPosX;
//				fullPlayerInfo.baseInfo.iPosY = pIntiRoleInfo->iPosY;
//				fullPlayerInfo.baseInfo.usLevel = pIntiRoleInfo->ucLevel;
//				fullPlayerInfo.baseInfo.uiHP = pIntiRoleInfo->uiHP;
//				fullPlayerInfo.baseInfo.uiMP = pIntiRoleInfo->uiMP;
//				fullPlayerInfo.baseInfo.ucMaxSpLimit = 5;	//最大SP级数8.21，写死
//				//9.11 changed by hyn 魔法物理技能等级要初始为
//				fullPlayerInfo.baseInfo.usPhysicsLevel = 1;
//				fullPlayerInfo.baseInfo.usMagicLevel = 1;
//				fullPlayerInfo.baseInfo.ucMaxPhyLevelLimit = 1;   
//				fullPlayerInfo.baseInfo.ucMaxMagLevelLimit = 1;
//
//				fullPlayerInfo.skillInfo.skillData[0].skillID = pIntiRoleInfo->uiSkill[0];
//				fullPlayerInfo.skillInfo.skillData[1].skillID = pIntiRoleInfo->uiSkill[1];
//				fullPlayerInfo.skillInfo.skillData[2].skillID = pIntiRoleInfo->uiSkill[2];
//				fullPlayerInfo.skillInfo.skillData[3].skillID = pIntiRoleInfo->uiSkill[3];
//				fullPlayerInfo.skillInfo.skillData[4].skillID = pIntiRoleInfo->uiSkill[4];
//				fullPlayerInfo.skillInfo.skillData[5].skillID = pIntiRoleInfo->uiSkill[5];
//
//				fullPlayerInfo.equipInfo.equipSize = EQUIP_SIZE;
//				fullPlayerInfo.skillInfo.skillSize = SKILL_SIZE;
//				fullPlayerInfo.bufferInfo.buffSize = BUFF_SIZE;
//				fullPlayerInfo.stateInfo.SetAlive();
//
//				InitRoleItemInfo* pRoleItemInfo = CInitRoleItemManagerSingle::instance()->Find( mNewPlayerInfo.wRace, mNewPlayerInfo.nClass, bSex  );
//				if ( pRoleItemInfo )
//				{
//					//初始化人物的金钱
//					fullPlayerInfo.baseInfo.dwMoney = pRoleItemInfo->money;
//
//					//初始化身上的装备
//					for ( unsigned int i = 0 ; i< EQUIP_SIZE ; i++ )
//					{
//						ItemInfo_i& equipItem = fullPlayerInfo.equipInfo.equips[i];
//						if ( pRoleItemInfo->InitEquipItem[i] != 0 )
//						{
//							equipItem.uiID = CInitRoleItemManagerSingle::instance()->GetInsertItemStart();
//							equipItem.usItemTypeID = pRoleItemInfo->InitEquipItem[i];
//							equipItem.usWearPoint  = 10000;
//							equipItem.ucCount = 1;
//						}
//					}
//
//					//初始化包裹中的物品
//					if( !pRoleItemInfo->InitPkgItem.empty() )
//					{
//						PlayerInfo_3_Pkgs& pkgInfo = fullPlayerInfo.pkgInfo;
//						std::vector<InitItem>::iterator lter =pRoleItemInfo->InitPkgItem.begin(),
//							                         lterEnd =pRoleItemInfo->InitPkgItem.end();
//						unsigned int pkgItemCount  = 0;
//						for(  ;lter != lterEnd; ++lter )
//						{
//							const InitItem& initItem = *lter;
//							if ( initItem.bstack )//如果可堆叠物品
//							{
//								pkgInfo.pkgs[pkgItemCount].uiID         = CInitRoleItemManagerSingle::instance()->GetInsertItemStart();
//								pkgInfo.pkgs[pkgItemCount].usItemTypeID = initItem.itemTypeID;
//								pkgInfo.pkgs[pkgItemCount].ucCount      = initItem.count;
//								pkgInfo.pkgs[pkgItemCount].usWearPoint  = 10000;
//								pkgItemCount++;
//							}
//							else//如果为不可堆叠物品
//							{
//								for ( int i = 0; i<  initItem.count; i++ )
//								{
//									pkgInfo.pkgs[pkgItemCount].uiID         = CInitRoleItemManagerSingle::instance()->GetInsertItemStart();
//									pkgInfo.pkgs[pkgItemCount].usItemTypeID = initItem.itemTypeID;
//									pkgInfo.pkgs[pkgItemCount].ucCount      = 1;
//									pkgInfo.pkgs[pkgItemCount].usWearPoint  = 10000;
//									pkgItemCount++;
//								}
//							}
//						}
//					}
//				}
//
//				// 插入表
//				if( InsertPlayerInfo( pConnect, (const char *)&fullPlayerInfo, sizeof(FullPlayerInfo), uiInsertID) == -1)
//				{
//					newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
//
//					D_DEBUG("新建账号为%s的角色信息出错\n", mNewPlayerInfo.szAccount);
//				}
//				else
//				{
//					newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_SUCCESS;
//					newPlayerInfoReturn.playerInfoLogin.uiID = uiInsertID;
//					StructMemCpy(newPlayerInfoReturn.playerInfoLogin.szNickName, mNewPlayerInfo.strNickName, sizeof(newPlayerInfoReturn.playerInfoLogin.szNickName));
//					newPlayerInfoReturn.playerInfoLogin.usLevel = pIntiRoleInfo->ucLevel;
//					newPlayerInfoReturn.playerInfoLogin.ucRace = mNewPlayerInfo.wRace;
//					newPlayerInfoReturn.playerInfoLogin.usClass = mNewPlayerInfo.nClass;
//					newPlayerInfoReturn.playerInfoLogin.usHair  = mNewPlayerInfo.nHair;
//					newPlayerInfoReturn.playerInfoLogin.usHairColor = pIntiRoleInfo->usHairColor;
//					newPlayerInfoReturn.playerInfoLogin.usFace = mNewPlayerInfo.nFace;
//					newPlayerInfoReturn.playerInfoLogin.usPortrait  = mNewPlayerInfo.usPortrait;
//					StructMemCpy((newPlayerInfoReturn.playerInfoLogin.equipInfo), &(fullPlayerInfo.equipInfo),sizeof(newPlayerInfoReturn.playerInfoLogin.equipInfo.equips));
//					newPlayerInfoReturn.playerInfoLogin.usMapID = pIntiRoleInfo->usMapID;
//					newPlayerInfoReturn.playerInfoLogin.iPosX = pIntiRoleInfo->iPosX;
//					newPlayerInfoReturn.playerInfoLogin.iPosY = pIntiRoleInfo->iPosY;
//				}
//			}
//		}
//	}
//
//	newPlayerInfoReturn.sequenceID = sequenceID;
//	MsgToPut *pMsgToPut = CreateSrvPkg( DGNewPlayerInfoReturn, mp_gateSrv, newPlayerInfoReturn );
//	mp_gateSrv->SendPkgToSrv(pMsgToPut);
//
//}
//
//void InsertPlayerInfoTask::OnResult()
//{
//}

//int InsertPlayerInfoTask::InsertPlayerInfo( CConnection*  pConnection, const char * pszData, unsigned long ulDataLength, unsigned int &uiInsertID)
//{
//	if((pszData == NULL) || (ulDataLength < sizeof(FullPlayerInfo)))
//		return -1;
//
//	TRY_BEGIN;
//
//	// 返回值
//	int iRet = 0;
//	FullPlayerInfo *pFullPlayerInfo = (FullPlayerInfo *)pszData;
//
//	CQuery *pQuery = pConnection->CreateQuery();
//	if(pQuery == NULL)
//		return -1;
//
//	// 构造SQL
//	char escapeBaseData[2 * sizeof(PlayerInfo_1_Base) + 1] = {0};
//	pQuery->RealEscape(escapeBaseData, (const char *)&pFullPlayerInfo->baseInfo, (unsigned long) sizeof(PlayerInfo_1_Base)); // 转义
//
//	char escapteEquipData[2 * sizeof(PlayerInfo_2_Equips) + 1] = {0};
//	pQuery->RealEscape(escapteEquipData, (const char *)&pFullPlayerInfo->equipInfo, (unsigned long) sizeof(PlayerInfo_2_Equips)); // 转义
//
//	char escaptePkgData[2 * sizeof(PlayerInfo_3_Pkgs) + 1] = {0};
//	pQuery->RealEscape(escaptePkgData, (const char *)&pFullPlayerInfo->pkgInfo, (unsigned long) sizeof(PlayerInfo_3_Pkgs)); // 转义
//
//	char escapteSkillData[2 * sizeof(PlayerInfo_4_Skills) + 1] = {0};
//	pQuery->RealEscape(escapteSkillData, (const char *)&pFullPlayerInfo->skillInfo, (unsigned long) sizeof(PlayerInfo_4_Skills)); // 转义
//
//	char escapteBufferData[2 * sizeof(PlayerInfo_5_Buffers) + 1] = {0};
//	pQuery->RealEscape(escapteBufferData, (const char *)&pFullPlayerInfo->bufferInfo, (unsigned long) sizeof(PlayerInfo_5_Buffers)); // 转义
//
//	char escapteStateData[2 * sizeof(PlayerStateInfo) + 1] = {0};
//	pQuery->RealEscape(escapteStateData, (const char *)&pFullPlayerInfo->stateInfo, (unsigned long) sizeof(PlayerStateInfo)); // 转义
//
//	char escapteTaskData[2 * sizeof(TaskRecordInfo)*(MAX_TASKRD_SIZE) + 1] = {0};
//	pQuery->RealEscape(escapteTaskData, (const char *)&pFullPlayerInfo->taskInfo, (unsigned long) sizeof(TaskRecordInfo)*(MAX_TASKRD_SIZE)); // 转义
//
//	std::stringstream ss;
//	ss << "INSERT INTO playerinfo (Account, Nickname, BaseData, EquipData, PkgData, SkillData, BufferData, StateData,TaskData, CreateTime) VALUES("
//		<< "'" << pFullPlayerInfo->baseInfo.szAccount  << "',"
//		<< "'" << pFullPlayerInfo->baseInfo.szNickName << "',"
//		<< "'" << escapeBaseData    << "',"
//		<< "'" << escapteEquipData  << "',"
//		<< "'" << escaptePkgData    << "',"
//		<< "'" << escapteSkillData  << "',"
//		<< "'" << escapteBufferData << "',"
//		<< "'" << escapteStateData  << "',"
//		<< "'" << escapteTaskData   << "',"
//		<< "NOW())";
//
//	std::string statement = ss.str();
//
//	// 执行查询
//	iRet = pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) );
//	uiInsertID = pQuery->InsertID();
//
//	// 销毁查询
//	pConnection->DestroyQuery(pQuery);
//
//	return iRet;
//
//	TRY_END;
//
//	return 0;
//}

