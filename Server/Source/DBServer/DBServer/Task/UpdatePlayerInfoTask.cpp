﻿#include "UpdatePlayerInfoTask.h"
#include "../../DBServer/PlayerInfoManager.h"


int UpdatePlayerInfoTask::UpdatePlayerInfo(CConnection* pConnection,const char * pszData, unsigned long ulDataLength )
{
	if ( !pConnection )
		return -1;

	if ( !pszData )
		return -1;

	if ( ulDataLength > sizeof(FullPlayerInfoEx) )
		return -1;

	int iRet = 0;
	TRY_BEGIN;

	// 返回值
	
	FullPlayerInfoEx *pFullPlayerInfoEx = (FullPlayerInfoEx *)pszData;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char escapeBaseData[2 * sizeof(PlayerInfo_1_Base) + 1] = {0};
	pQuery->RealEscape(escapeBaseData, (const char *)&pFullPlayerInfoEx->fullPlayerInfo.baseInfo, (unsigned long) sizeof(PlayerInfo_1_Base)); // 转义

	char escapteEquipData[2 * sizeof(PlayerInfo_2_Equips) + 1] = {0};
	pQuery->RealEscape(escapteEquipData, (const char *)&pFullPlayerInfoEx->fullPlayerInfo.equipInfo, (unsigned long) sizeof(PlayerInfo_2_Equips)); // 转义

	char escaptePkgData[2 * sizeof(PlayerInfo_3_Pkgs) + 1] = {0};
	pQuery->RealEscape(escaptePkgData, (const char *)&pFullPlayerInfoEx->fullPlayerInfo.pkgInfo, (unsigned long) sizeof(PlayerInfo_3_Pkgs)); // 转义

	char escapteSkillData[2 * sizeof(PlayerInfo_4_Skills) + 1] = {0};
	pQuery->RealEscape(escapteSkillData, (const char *)&pFullPlayerInfoEx->fullPlayerInfo.skillInfo, (unsigned long) sizeof(PlayerInfo_4_Skills)); // 转义

	char escapteBufferData[2 * sizeof(PlayerInfo_5_Buffers) + 1] = {0};
	pQuery->RealEscape(escapteBufferData, (const char *)&pFullPlayerInfoEx->fullPlayerInfo.bufferInfo, (unsigned long) sizeof(PlayerInfo_5_Buffers)); // 转义

	char escapteStateData[2 * sizeof(PlayerStateInfo) + 1] = {0};
	pQuery->RealEscape(escapteStateData, (const char *)&pFullPlayerInfoEx->fullPlayerInfo.stateInfo, (unsigned long) sizeof(PlayerStateInfo)); // 转义

	char escapteTaskData[2 * sizeof(TaskRecordInfo)*(MAX_TASKRD_SIZE) + 1] = {0};
	pQuery->RealEscape(escapteTaskData, (const char *)&pFullPlayerInfoEx->fullPlayerInfo.taskInfo, (unsigned long) sizeof(TaskRecordInfo)*(MAX_TASKRD_SIZE)); // 转义

	char escapteClientdata[2 * sizeof(pFullPlayerInfoEx->clientData) + 1] = {0};
	pQuery->RealEscape(escapteClientdata, (const char *)&pFullPlayerInfoEx->clientData, (unsigned long) sizeof(pFullPlayerInfoEx->clientData)); // 转义

	std::stringstream ss;
	ss << "UPDATE playerinfo SET "
		<< "BaseData='" << escapeBaseData << "',"
		<< "EquipData='" << escapteEquipData << "',"
		<< "PkgData='" << escaptePkgData << "',"
		<< "SkillData='" << escapteSkillData << "',"
		<< "BufferData='" << escapteBufferData << "',"
		<< "StateData='" << escapteStateData << "',"
		<< "TaskData='" << escapteTaskData <<"', "
		<< "ClientData='" << escapteClientdata <<"' "
		<< " WHERE " 
		<< "Account='" << pFullPlayerInfoEx->fullPlayerInfo.baseInfo.szAccount << "' AND "
		<< "ID=" << pFullPlayerInfoEx->fullPlayerInfo.baseInfo.uiID;

	std::string statement = ss.str();

	// 执行查询
	if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
		iRet = -1;

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	TRY_END;

	return iRet;

}

void UpdatePlayerInfoTask::OnThread( CConnection* pConnect )
{
	if ( !pConnect )
		return;

	int dbret = UpdatePlayerInfo( pConnect, (const char *)&mpFullPlayerInfoEx, sizeof(FullPlayerInfoEx));

	if(dbret == -1)
	{
		D_DEBUG("DB操作记录:更新角色信息:出错,账号=%s\n", mpFullPlayerInfoEx.fullPlayerInfo.baseInfo.szAccount);
	}
	else
	{
		D_DEBUG("DB操作记录:更新角色信息:成功,账号=%s\n", mpFullPlayerInfoEx.fullPlayerInfo.baseInfo.szAccount);
	}

	ACE_UNUSED_ARG(dbret);
	//// 发送更新结果
	//DGUpdatePlayerInfoReturn updatePlayerInfoReturn;
	//StructMemSet(updatePlayerInfoReturn, 0x00, sizeof(updatePlayerInfoReturn));

	//updatePlayerInfoReturn.playerID = pUpdatePlayerInfo->playerID;
	//updatePlayerInfoReturn.iRet = dbret;
	//
	////MsgToPut *pMsgToPut = CPacketBuild::CreatePkg(pOwner->GetHandleID(), pOwner->GetSessionID(), &updatePlayerInfoReturn);
	//MsgToPut *pMsgToPut = CreateSrvPkg( DGUpdatePlayerInfoReturn, pOwner, updatePlayerInfoReturn );
	//pOwner->SendPkgToSrv(pMsgToPut);

	// 删除缓存角色
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// by dzj, 08.04.27, 现在并不一定只更新一次，因此不能一更新就删掉，否则后续的更新将无法进行，CPlayerInfoManagerSingle::instance()->DeletePlayerInfo(pFullPlayerInfo->baseInfo.uiID);
	if ( mIsOffLine )
	{
		//只有下线时才从缓存中删掉；
		CPlayerInfoManagerSingle::instance()->DeletePlayerInfo(mpFullPlayerInfoEx.fullPlayerInfo.baseInfo.uiID);			
	}
}

void UpdatePlayerInfoTask::OnResult()
{

}

