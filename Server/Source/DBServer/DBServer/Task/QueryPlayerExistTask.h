﻿/********************************************************************
	created:	2009/05/05
	created:	5:5:2009   14:16
	file:		QueryPlayerExistTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef QUERY_PLAYER_EXISTS_TASK_H
#define QUERY_PLAYER_EXISTS_TASK_H

#include "ITask.h"

class QueryPlayerExistTask:public ITask
{
public:
	QueryPlayerExistTask( CGateServer* pGateSrv,  const char* pszName ,unsigned int queryID ,QUERY_PLAYER_EXIST_TYPE  queryType ):ITask(pGateSrv),
		mQueryID(queryID),mPlayerUID(0),
		mQueryExistType( queryType ),bInit(false),isExist(false)
	{
		if ( pszName )
			ACE_OS::strncpy( szQueryName, pszName, MAX_NAME_SIZE );
	
		bInit = true;
	}

	~QueryPlayerExistTask()
	{
	}

public:

	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:
	char szQueryName[MAX_NAME_SIZE];
	unsigned int mQueryID;
	unsigned int mPlayerUID;
	QUERY_PLAYER_EXIST_TYPE mQueryExistType;
	bool bInit;
	bool isExist;

};

#endif

