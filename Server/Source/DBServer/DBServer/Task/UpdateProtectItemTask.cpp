﻿#include "UpdateProtectItemTask.h"

void UpdateProtectItemTask::OnThread(CConnection* pConnect )
{
	if ( !pConnect )
		return;

	TRY_BEGIN;

	int iRet = 0;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
	{
		iRet = -1;
		return;
	}

	if ( iRet != -1 )
	{
		// 构造SQL
		char escapeProtectItemData[2 * sizeof(m_protectItemArr) + 1] = {0};
		pQuery->RealEscape(escapeProtectItemData, (const char *)&m_protectItemArr, (unsigned long) sizeof(m_protectItemArr)); // 转义


		std::stringstream ss;
		ss << " UPDATE playerinfo SET "
		   << " ProtectItemData='" << escapeProtectItemData <<"' "
		   << " WHERE "
		   << " ID= " << updatePlayerUID;


		std::string statement = ss.str();

		// 执行查询
		if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
			iRet = -1;

		// 销毁查询
		pConnect->DestroyQuery(pQuery);
		
	}

	TRY_END;
}

void UpdateProtectItemTask::OnResult()
{

}

