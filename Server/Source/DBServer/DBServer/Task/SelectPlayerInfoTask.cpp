﻿#include "SelectPlayerInfoTask.h"
#include "../../../Base/aceall.h"


int SelectPlayerInfoTask::SelectPlayerInfo(CConnection* pConnect,const char *pszAccount, const unsigned int uiID, char * pszData, unsigned long ulDataLength )
{
	if((pszData == NULL) || (ulDataLength < sizeof(FullPlayerInfo)))
		return -1;

	int iRet = 0;

	TRY_BEGIN;

	if( pConnect == NULL )
		return -1;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"SELECT BaseData, EquipData, PkgData, SkillData, BufferData, StateData, TaskData, petinfo, ClientData, SuitData, FashionData, ProtectItemData, TaskRecord, CopyStageInfo FROM playerinfo WHERE account = '%s' and ID = %d ", 
		pszAccount, uiID);
	if(iStatementLen <= 0 )
		iRet = -1;
	else
	{
		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", szStatement);
			iRet = -1;
		}
		else
		{
			// 处理结果集
			if(pResult->Next())
			{
				m_pFullPlayerInfo = (FullPlayerInfoEx *)pszData;

				if ( pResult->GetString(0) )
				{
					StructMemCpy(m_pFullPlayerInfo->fullPlayerInfo.baseInfo, pResult->GetString(0), sizeof(PlayerInfo_1_Base));
				}

				if ( pResult->GetString(1) )
				{
					StructMemCpy(m_pFullPlayerInfo->fullPlayerInfo.equipInfo, pResult->GetString(1), sizeof(PlayerInfo_2_Equips));
				}

				if ( pResult->GetString(2) )
				{
					StructMemCpy(m_pFullPlayerInfo->fullPlayerInfo.pkgInfo, pResult->GetString(2), sizeof(PlayerInfo_3_Pkgs));
				}

				if ( pResult->GetString(3))
				{
					StructMemCpy(m_pFullPlayerInfo->fullPlayerInfo.skillInfo, pResult->GetString(3), sizeof(PlayerInfo_4_Skills));
				}

				if ( pResult->GetString(4))
				{
					StructMemCpy(m_pFullPlayerInfo->fullPlayerInfo.bufferInfo,pResult->GetString(4), sizeof(PlayerInfo_5_Buffers));
				}

				if ( pResult->GetString(5) )
				{
					StructMemCpy(m_pFullPlayerInfo->fullPlayerInfo.stateInfo, pResult->GetString(5), sizeof(PlayerStateInfo));
				}

				if ( pResult->GetString(6) )
				{
					StructMemCpy(m_pFullPlayerInfo->fullPlayerInfo.taskInfo, pResult->GetString(6), sizeof(TaskRecordInfo)*(MAX_TASKRD_SIZE));
				}

				if ( pResult->GetString(7) )
				{
					StructMemCpy( petInfo, pResult->GetString(7), sizeof(petInfo) );
				} else {
					StructMemSet( petInfo, 0, sizeof(petInfo) );//初始宠物信息全清空；
				}

				if ( pResult->GetString(8) )
				{
					StructMemCpy(m_pFullPlayerInfo->clientData, pResult->GetString(8), sizeof(m_pFullPlayerInfo->clientData));
				}

				//套装
				if( pResult->GetString(9))
				{
					StructMemCpy( suitInfo, pResult->GetString(9), sizeof(suitInfo) );
				} else {
					StructMemSet( suitInfo, 0x00, sizeof(suitInfo));
					suitInfo.suitSize = MAX_SUIT_COUNT;
				}

				//时装
				if( pResult->GetString(10))
				{
					StructMemCpy( fashionInfo, pResult->GetString(10), sizeof(fashionInfo) );
				} else {
					StructMemSet( fashionInfo, 0x00, sizeof(fashionInfo));
					fashionInfo.fashionSize = MAX_FASHION_COUNT;
				}

				//保护道具
				if ( pResult->GetString(11) )
				{
					ACE_OS::memcpy( m_pFullPlayerInfo->GetProtectItemArr(), pResult->GetString(11), m_pFullPlayerInfo->GetProtectItemLen() );
				} else {
					ACE_OS::memset( m_pFullPlayerInfo->GetProtectItemArr(), 0x0, m_pFullPlayerInfo->GetProtectItemLen() );
				}

				if ( pResult->GetString(12) )
				{
					ACE_OS::memcpy( m_pFullPlayerInfo->GetTaskRdInfoArr(), pResult->GetString(12), m_pFullPlayerInfo->GetTaskRdInfoLen() );
				} else {
					ACE_OS::memset( m_pFullPlayerInfo->GetTaskRdInfoArr(), 0x0, m_pFullPlayerInfo->GetTaskRdInfoLen() );
				}

				if ( pResult->GetString(13) )
				{
					StructMemCpy( copyStageInfos, pResult->GetString(13), sizeof(copyStageInfos) );
				} else {
					StructMemSet( copyStageInfos, 0, sizeof(copyStageInfos) );//初始副本阶段信息全清空；
				}
			}
			// 销毁查询
			pQuery->FreeResult(pResult);
		}
	}

	// 销毁查询
	pConnect->DestroyQuery(pQuery);

	return iRet;

	TRY_END;

	return 0;
}

void SelectPlayerInfoTask::OnThread(CConnection *pConnect)
{
	if ( !pConnect )
		return;

	TRY_BEGIN;

	// 玩家完整信息
	FullPlayerInfoEx *pFullPlayerInfoEx = NEW FullPlayerInfoEx;
	if(pFullPlayerInfoEx == NULL)
		return;

	StructMemSet(*pFullPlayerInfoEx, 0x00, sizeof(FullPlayerInfoEx));	

	// 角色信息回复
	if( SelectPlayerInfo(pConnect,m_pPlayerInfo.szAccount, m_pPlayerInfo.uiRoleID, (char *)pFullPlayerInfoEx, sizeof(FullPlayerInfoEx)) == -1)
	{	
		StructMemSet(playerInfoReturn, 0x00, sizeof(playerInfoReturn));
		playerInfoReturn.playerID = m_pPlayerInfo.playerID;
		playerInfoReturn.byFlag = DGPlayerInfo::FLAG_INVALID;
		playerInfoReturn.sequenceID = m_pPlayerInfo.sequenceID;
		eResult = E_INVALID;

		delete  pFullPlayerInfoEx;
		pFullPlayerInfoEx = NULL;

		D_DEBUG("DB操作记录:检索角色信息:出错,account=%s,id=%d\n", m_pPlayerInfo.szAccount, m_pPlayerInfo.uiRoleID);
	}
	else
	{
		D_DEBUG("DB操作记录:检索角色信息:成功,account=%s,id=%d\n", m_pPlayerInfo.szAccount, m_pPlayerInfo.uiRoleID);

		// 更新角色ID(角色创建时内部ID为0)
		if(0 == pFullPlayerInfoEx->fullPlayerInfo.baseInfo.uiID)
			pFullPlayerInfoEx->fullPlayerInfo.baseInfo.uiID = m_pPlayerInfo.uiRoleID;

		// 插入全局缓存
		CPlayerInfoManagerSingle::instance()->AddPlayerInfo(pFullPlayerInfoEx->fullPlayerInfo.baseInfo.uiID, pFullPlayerInfoEx);
		SendFullStr<FullPlayerInfo, DGPlayerInfo>(&(pFullPlayerInfoEx->fullPlayerInfo), playerInfoVec);

		//填充序列号
		unsigned int count = (unsigned int)playerInfoVec.size();
		for( unsigned int i=0; i< count; ++i )
		{
			playerInfoVec[i].sequenceID = m_pPlayerInfo.sequenceID;
		}

		eResult = E_SUCCESS;
	}

	TRY_END;
}

void SelectPlayerInfoTask::OnResult()
{
	TRY_BEGIN;

	switch( eResult )
	{
	case E_INVALID:
		{
			MsgToPutContent* pContent = NEW MsgToPutContent;
			pContent->msgType = DGPlayerInfo::wCmd;
			pContent->sessionID = m_gateID;
			StructMemCpy( pContent->szContent, &playerInfoReturn, sizeof(playerInfoReturn) );
			
#ifdef MULTI_DB_HASH

			PUSH_MSG_TO_QUEUE( pContent );

#else
			GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif

		}
		break;
	case E_SUCCESS:
		{
			PlayerDbInfoSend();
			for(std::vector<DGPlayerInfo>::iterator i = playerInfoVec.begin(); playerInfoVec.end() != i; i++)
			{
				i->playerID = m_pPlayerInfo.playerID;
				i->byFlag   = DGPlayerInfo::FLAG_OK;

				MsgToPutContent* pContent = NEW MsgToPutContent;
				pContent->msgType = DGPlayerInfo::wCmd;
				pContent->sessionID = m_gateID;
				StructMemCpy( pContent->szContent, &(*i), sizeof(*i) );

#ifdef MULTI_DB_HASH

				PUSH_MSG_TO_QUEUE( pContent );

#else
				GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif;
			}
		}
		break;
	case E_OTHER:
		break;
	default:
		break;
	}

	TRY_END;
}

///向gate发送玩家存盘信息；
void SelectPlayerInfoTask::PlayerDbInfoSend()
{
	/*
	1.send DGDbGetPre
	2.send 各待取信息
	3.send DGDbGetPost
	*/
	//mp_gateSrv-->m_gateID;if ( NULL == mp_gateSrv )
	//{
	//	return;
	//}

	//1.send DGDbGetPre
	DGDbGetPre getPre;
	getPre.playerID = m_pPlayerInfo.playerID;
	
	MsgToPutContent* pContent1 = NEW MsgToPutContent;
	pContent1->msgType = DGDbGetPre::wCmd;
	pContent1->sessionID = m_gateID;
	StructMemCpy( pContent1->szContent, &getPre, sizeof(getPre) );

#ifdef MULTI_DB_HASH

	PUSH_MSG_TO_QUEUE( pContent1 );

#else
	GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent1 );

#endif;
	
	//发送宠物信息
	SendPlayerPetInfo();

	//发送套装信息
	SendPlayerSuitInfo();

	//发送时装信息
	SendPlayerFashionInfo();

	//道具保护信息
	SendPlayerProtectItemInfo();

	//玩家存盘的任务信息
	SendPlayerTaskRdInfo();

	//发送玩家副本阶段信息
	SendPlayerCopyStageInfo();

	//3.send DGDbGetPost
	DGDbGetPost getPost;
	getPost.playerID = m_pPlayerInfo.playerID;

	MsgToPutContent* pContent5 = NEW MsgToPutContent;
	pContent5->msgType   = DGDbGetPost::wCmd;
	pContent5->sessionID = m_gateID;
	StructMemCpy( pContent5->szContent, &getPost, sizeof(getPost) );

#ifdef MULTI_DB_HASH

	PUSH_MSG_TO_QUEUE( pContent5 );

#else
	GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent5 );

#endif;

	return;
}

/////填充待发的petinfo;
//void SelectPlayerInfoTask::FillPlayerPetInfo( DGDbGetInfo& getInfo )
//{
//	getInfo.infoType = DI_PET;
//	getInfo.playerID = m_pPlayerInfo.playerID;
//	StructMemCpy( (getInfo.petInfo), &petInfo, sizeof(getInfo.petInfo) );
//	return;
//}
//
/////填充待发的套装信息;
//void SelectPlayerInfoTask::FillPlayerSuitInfo( DGDbGetInfo& getInfo )
//{
//	getInfo.infoType = DI_SUIT;
//	getInfo.playerID = m_pPlayerInfo.playerID;
//	StructMemCpy( getInfo.suitInfo, &suitInfo, sizeof(getInfo.suitInfo));
//}
//
/////填充待发的时装服饰信息;
//void SelectPlayerInfoTask::FillPlayerFashionInfo( DGDbGetInfo& getInfo )
//{
//	getInfo.infoType = DI_FASHION;
//	getInfo.playerID = m_pPlayerInfo.playerID;
//	StructMemCpy( getInfo.fashionInfo, &fashionInfo, sizeof(getInfo.fashionInfo));
//}


void SelectPlayerInfoTask::SendPlayerFashionInfo()
{
	DGDbGetInfo srvmsg;
	srvmsg.infoType = DI_FASHION;
	srvmsg.playerID = m_pPlayerInfo.playerID;
	StructMemCpy( srvmsg.fashionInfo, &fashionInfo, sizeof(srvmsg.fashionInfo));

	MsgToPutContent* pContent = NEW MsgToPutContent;
	pContent->msgType   = DGDbGetInfo::wCmd;
	pContent->sessionID = m_gateID;
	StructMemCpy( pContent->szContent, &srvmsg, sizeof(srvmsg) );

#ifdef MULTI_DB_HASH

	PUSH_MSG_TO_QUEUE( pContent );

#else
	GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif;
}


void SelectPlayerInfoTask::SendPlayerSuitInfo()
{
	DGDbGetInfo srvmsg;
	srvmsg.playerID =  m_pPlayerInfo.playerID;
	srvmsg.infoType =  DI_SUIT;
	StructMemCpy( srvmsg.suitInfo, &suitInfo, sizeof(srvmsg.suitInfo));

	MsgToPutContent* pContent = NEW MsgToPutContent;
	pContent->msgType   = DGDbGetInfo::wCmd;
	pContent->sessionID = m_gateID;
	StructMemCpy( pContent->szContent, &srvmsg, sizeof(srvmsg) );


#ifdef MULTI_DB_HASH

	PUSH_MSG_TO_QUEUE( pContent );

#else
	GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif;
}

void SelectPlayerInfoTask::SendPlayerPetInfo()
{
	DGDbGetInfo srvmsg;
	srvmsg.playerID =  m_pPlayerInfo.playerID;
	srvmsg.infoType = DI_PET;
	StructMemCpy( (srvmsg.petInfo), &petInfo, sizeof(srvmsg.petInfo) );

	MsgToPutContent* pContent = NEW MsgToPutContent;
	pContent->msgType   = DGDbGetInfo::wCmd;
	pContent->sessionID = m_gateID;
	StructMemCpy( pContent->szContent, &srvmsg, sizeof(srvmsg) );
	
#ifdef MULTI_DB_HASH

	PUSH_MSG_TO_QUEUE( pContent );

#else
	GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif;
}

///发送玩家副本阶段信息；
void SelectPlayerInfoTask::SendPlayerCopyStageInfo()
{
	DGDbGetInfo srvmsg;
	srvmsg.playerID =  m_pPlayerInfo.playerID;
	srvmsg.infoType = DI_COPYSTAGE;
	StructMemCpy( (srvmsg.copyStageInfos), &copyStageInfos, sizeof(srvmsg.copyStageInfos) );

	MsgToPutContent* pContent = NEW MsgToPutContent;
	pContent->msgType   = DGDbGetInfo::wCmd;
	pContent->sessionID = m_gateID;
	StructMemCpy( pContent->szContent, &srvmsg, sizeof(srvmsg) );
	
#ifdef MULTI_DB_HASH

	PUSH_MSG_TO_QUEUE( pContent );

#else
	GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif;
}

void SelectPlayerInfoTask::SendPlayerProtectItemInfo()
{
	if ( m_pFullPlayerInfo )
	{
		DGDbGetInfo srvmsg;
		srvmsg.playerID =  m_pPlayerInfo.playerID;
		srvmsg.infoType =  DI_PROTECTITEM;
		StructMemSet( srvmsg.protectInfo, 0x0, sizeof(srvmsg.protectInfo) );

		unsigned int page = 0;
		unsigned int protectCount = 0;
		const ProtectItem * pProtectItemArr = m_pFullPlayerInfo->GetProtectItemArr();
		for ( unsigned i = 0 ; i< PACKAGE_SIZE; i++ )
		{
			const ProtectItem& protectItem = pProtectItemArr[i];
			if ( protectItem.protectItemUID != 0 )
			{
				if ( protectCount == PKG_PAGE_SIZE )
				{
					MsgToPutContent* pContent = NEW MsgToPutContent;
					pContent->msgType = DGDbGetInfo::wCmd;
					pContent->sessionID = m_gateID;
					srvmsg.protectInfo.isEnd = false;
					srvmsg.protectInfo.mCount = page;
					StructMemCpy( pContent->szContent, &srvmsg, sizeof(srvmsg) );
					
#ifdef MULTI_DB_HASH

					PUSH_MSG_TO_QUEUE( pContent );

#else
					GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif;
					page++;

					protectCount  = 0;
					StructMemSet( srvmsg.protectInfo, 0x0, sizeof(srvmsg.protectInfo) );
					srvmsg.protectInfo.mProtectItemArr[protectCount] = protectItem;
					protectCount++;
				}
				else
				{
					srvmsg.protectInfo.mProtectItemArr[protectCount] = protectItem;
					protectCount++;
				}
			}
			else
				break;
		}

		if ( protectCount > 0 )
		{
			MsgToPutContent* pContent = NEW MsgToPutContent;
			pContent->msgType   = DGDbGetInfo::wCmd;
			pContent->sessionID = m_gateID;
			srvmsg.protectInfo.isEnd  = true;
			srvmsg.protectInfo.mCount = page;
			StructMemCpy( pContent->szContent, &srvmsg, sizeof(srvmsg) );

#ifdef MULTI_DB_HASH

			PUSH_MSG_TO_QUEUE( pContent );

#else
			GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif;
		}
	}
}

void SelectPlayerInfoTask::SendPlayerTaskRdInfo()
{
	if ( m_pFullPlayerInfo )
	{
		DGDbGetInfo srvmsg;
		StructMemSet( srvmsg, 0x0, sizeof(srvmsg) );
		srvmsg.playerID = m_pPlayerInfo.playerID;
		srvmsg.infoType = DI_TASKRD;
		const unsigned* pTaskRdInfoArr = m_pFullPlayerInfo->GetTaskRdInfoArr();
		for ( unsigned i = 0 ; i < 1; i++ )
		{
			srvmsg.taskRdInfo.mCount = i;
			srvmsg.taskRdInfo.isEnd  = ( i == 0 );
			StructMemCpy( srvmsg.taskRdInfo.mTaskRdIArr, pTaskRdInfoArr + i* 100, sizeof(unsigned int)*100 );

			MsgToPutContent* pContent = NEW MsgToPutContent;
			pContent->msgType = DGDbGetInfo::wCmd;
			pContent->sessionID = m_gateID;
			StructMemCpy( pContent->szContent, &srvmsg, sizeof(srvmsg) );
			
#ifdef MULTI_DB_HASH

			PUSH_MSG_TO_QUEUE( pContent );

#else
			GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif;;
		}
	}
}

