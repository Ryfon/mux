﻿#pragma once
#include "ITask.h"

class SelectCityTask :
	public ITask
{
public:
	SelectCityTask( CGateServer* pGateSrv, const GDReqCityInfo& reqInfo):ITask( pGateSrv ),m_resCode(0)
	{
		m_reqInfo = reqInfo;
		m_cityInfoResult.reqMapsrvID = reqInfo.reqMapsrvID;
	}

	~SelectCityTask(void);

	//处理选择
	virtual void OnThread( CConnection* pConnect );

	//线程处理善后的过程
	virtual void OnResult();

	int SelectCityInfo( CConnection* pConnect );

private:
	DGCityInfoResult m_cityInfoResult;
	GDReqCityInfo m_reqInfo;
	int m_resCode;
};
