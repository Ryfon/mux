﻿/********************************************************************
	created:	2008/09/23
	created:	23:9:2008   11:04
	file:		DeletePlayerTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef DELETEPLAYERTASK_H
#define DELETEPLAYERTASK_H

#include "ITask.h"

class DeletePlayerTask:public ITask
{
public:
	DeletePlayerTask(CGateServer* pGateSrv,GDDeletePlayerInfo* deletePlayerInfo ):ITask(pGateSrv)
	{
		StructMemCpy( mDeletePlayerInfo, deletePlayerInfo,sizeof(GDDeletePlayerInfo) );
	}

	virtual ~DeletePlayerTask()
	{
	}

public:
	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

	int DeletePlayerInfo( CConnection* pConnection, unsigned int uiPlayerID );

protected:
	GDDeletePlayerInfo mDeletePlayerInfo;
	DGDeletePlayerInfoReturn delPlayerReturn;
};

#endif
