﻿#include "GetItemSequenceTask.h"
#include "../TaskPoolManager.h"

ACE_Mutex CGetItemSequenceTask::m_itemIDLock;

void CGetItemSequenceTask::OnThread(CConnection* pConnect )
{
	if(NULL == pConnect)
		return;

#ifdef MULTI_DB_HASH

	ACE_GUARD( ACE_Mutex, guard, m_itemIDLock );//sessionID写锁；
	CSequenceGenerator sequencGenerator;
	unsigned int startID, endID;
	startID =  endID = 0;
	
	resp.uiServerID = itemSequenceReq.uiServerID;
	resp.uiStartSequence = 0;
	resp.uiEndSequence = 0;
	resp.bSuccess = false;
	
	if(sequencGenerator.GetSequenceRanage(pConnect, startID, endID) != -1)
	{
		resp.bSuccess  = true;
		resp.uiStartSequence = startID;
		resp.uiEndSequence = endID;
	}

	MsgToPutContent* pContent = NEW MsgToPutContent;
	pContent->msgType = DGBatchItemSequence::wCmd;
	pContent->sessionID = m_gateID;
	StructMemCpy( pContent->szContent, &resp, sizeof(resp) );

	PUSH_MSG_TO_QUEUE( pContent );

#endif/*MULTI_DB_HASH*/

	return;
}

void CGetItemSequenceTask::OnResult()
{

}
