﻿

#ifndef UPDATECITYINFO_H
#define UPDATECITYINFO_H

#include "ITask.h"

class UpdateCityInfoTask : public ITask
{
public:
	UpdateCityInfoTask(CGateServer* pGateSrv,const CityInfo& updateCityinfo):ITask( pGateSrv),m_updateCityInfo( updateCityinfo )
	{
	
	}

	~UpdateCityInfoTask();

	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

	int UpdateCityInfo( CConnection* pConnect );


private:
	CityInfo m_updateCityInfo;
};


#endif /*UPDATECITYINFO_H*/

