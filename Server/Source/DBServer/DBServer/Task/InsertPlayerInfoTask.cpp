﻿#include "InsertPlayerInfoTask.h"
#include "../../DBServer/ConfigInfoManager.h"
#include "ace/OS.h"
#include "../TaskPoolManager.h"

int InsertPlayerInfoTask::TotalCountSameAccount( CConnection* pConnection, const char* szAccount )
{
	int iRet = 0;

	if ( !pConnection )
		return -1;

	TRY_BEGIN;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
		"SELECT count(*) FROM playerinfo WHERE Account = '%s'", 
		szAccount );
	if(iStatementLen <= 0 )
		iRet = -1;
	else
	{
		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", szStatement);
			iRet = -1;
		}
		else
		{
			if(pResult->Next())
				iRet = pResult->GetInt(0);

			// 销毁查询
			pQuery->FreeResult(pResult);
		}
	}

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;

	TRY_END;

	return 0;
}

int InsertPlayerInfoTask::ExistSamePlayerName(CConnection* pConnection, const char* pszPlayerName )
{
	int iRet = 0;

	if ( !pConnection )
		return -1;

	TRY_BEGIN;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
		"SELECT 1 FROM playerinfo WHERE Nickname = binary '%s'", 
		pszPlayerName );
	if(iStatementLen <= 0 )
		iRet = -1;
	else
	{
		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", szStatement);
			iRet = -1;
		}
		else
		{
			iRet = pResult->RowNum();

			// 销毁查询
			pQuery->FreeResult(pResult);
		}
	}

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;

	TRY_END;

	return 0;

}

#ifdef MULTI_DB_HASH
void InsertPlayerInfoTask::OnFullConnection( CConnection *pConnect, unsigned short num)
{
	for(unsigned short i = 0; i < num; i++)
	{
		int count = TotalCountSameAccount(&pConnect[i], mNewPlayerInfo.szAccount);
		if(count == -1)
		{	
			totalRoleCount = -1;
			break;
		}
		else if(count > 0)
		{
			totalRoleCount = totalRoleCount+ count;
		}
	}

	return;
}
#endif

void InsertPlayerInfoTask::OnThread(CConnection *pConnect)
{
	if ( !pConnect )
		return;

	//by dzj, 09.23 unsigned int t1 = GetTickCount();
	unsigned int uiInsertID = 0;
	unsigned int sequenceID = mNewPlayerInfo.sequenceID;
	StructMemSet(newPlayerInfoReturn, 0x00, sizeof(newPlayerInfoReturn));
	newPlayerInfoReturn.playerID = mNewPlayerInfo.playerID;

	do 
	{
#ifdef MULTI_DB_HASH
		newPlayerInfoReturn.iRet = TotalCountSameAccount(pConnect, mNewPlayerInfo.szAccount);
		if((newPlayerInfoReturn.iRet == -1) || (newPlayerInfoReturn.iRet >= 4) )//出错或当前角色数大于四个
		{
			newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
			break;
		}

		newPlayerInfoReturn.iRet = 	ExistSamePlayerName( pConnect,mNewPlayerInfo.strNickName);//出错
		if(newPlayerInfoReturn.iRet == -1)
		{
			newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
			break;
		}

		if(newPlayerInfoReturn.iRet == 1)//存在同名角色
		{
			newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_EXIST_SAME_NAME_ROLE;
			break;
		}

		if((totalRoleCount == -1) || (totalRoleCount >= 4) )//出错或当前角色数大于四个
		{
			newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
			break;
		}
#endif/*MULTI_DB_HASH*/

		char bSex = mNewPlayerInfo.wSex;  // hyn 08.12.17
		InitRoleInfo* pIntiRoleInfo = CRoleInitManagerSingle::instance()->Find(mNewPlayerInfo.wRace, mNewPlayerInfo.nClass,  bSex );
		if(pIntiRoleInfo == NULL)
		{
			D_DEBUG("账号%s的玩家创建角色出错:未找到对应种族=%d, 职业=%d, 性别=%d的初始化纪录\n", mNewPlayerInfo.szAccount, mNewPlayerInfo.wRace, mNewPlayerInfo.nClass, mNewPlayerInfo.wSex);
			newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
			break;
		}
	
		if(false/*!CSkillInfoManagerSingle::instance()->Find(pIntiRoleInfo->uiSkill, sizeof(pIntiRoleInfo->uiSkill)/sizeof(pIntiRoleInfo->uiSkill[0]), pIntiRoleInfo->usClass)*/)
		{
			D_DEBUG("账号%s的玩家创建角色出错:未找到对应职业=%d的技能信息\n", mNewPlayerInfo.szAccount, mNewPlayerInfo.nClass);
			newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
			break;
		}
			
		// 玩家角色信息初始化
		FullPlayerInfo fullPlayerInfo;
		StructMemSet(fullPlayerInfo, 0x00, sizeof(FullPlayerInfo));

		// 客户端传递属性
		SafeStrCpy(fullPlayerInfo.baseInfo.szAccount,  mNewPlayerInfo.szAccount);
		SafeStrCpy(fullPlayerInfo.baseInfo.szNickName, mNewPlayerInfo.strNickName);
		fullPlayerInfo.baseInfo.accountSize = ARRAY_SIZE(fullPlayerInfo.baseInfo.szAccount);
		fullPlayerInfo.baseInfo.nameSize = ARRAY_SIZE(fullPlayerInfo.baseInfo.szNickName);
		fullPlayerInfo.baseInfo.ucRace = (unsigned char)mNewPlayerInfo.wRace;
		SetSexToSHFlag(mNewPlayerInfo.wSex, fullPlayerInfo.baseInfo.shFlag);
		fullPlayerInfo.baseInfo.usClass = mNewPlayerInfo.nClass;

		// 对应的缺省初始化属性
		fullPlayerInfo.baseInfo.ucMaxPhyLevelLimit = (unsigned char)pIntiRoleInfo->usPhysicsLevelMax;
		fullPlayerInfo.baseInfo.ucMaxMagLevelLimit = (unsigned char)pIntiRoleInfo->usMagicLevelMax;
		fullPlayerInfo.baseInfo.usPhysicsAllotPoint = pIntiRoleInfo->usPhysicsPoint;
		fullPlayerInfo.baseInfo.usMagicAllotPoint = pIntiRoleInfo->usMagicPoint;
		fullPlayerInfo.baseInfo.usPortrait = mNewPlayerInfo.usPortrait; 
		fullPlayerInfo.baseInfo.usHair =  mNewPlayerInfo.nHair;
		fullPlayerInfo.baseInfo.usHairColor = pIntiRoleInfo->usHairColor;
		fullPlayerInfo.baseInfo.usFace =  mNewPlayerInfo.nFace;
		fullPlayerInfo.baseInfo.usMapID = pIntiRoleInfo->usMapID;
		fullPlayerInfo.baseInfo.iPosX = pIntiRoleInfo->iPosX;
		fullPlayerInfo.baseInfo.iPosY = pIntiRoleInfo->iPosY;
		fullPlayerInfo.baseInfo.usLevel = pIntiRoleInfo->ucLevel;
		fullPlayerInfo.baseInfo.uiHP = pIntiRoleInfo->uiHP;
		fullPlayerInfo.baseInfo.uiMP = pIntiRoleInfo->uiMP;
		fullPlayerInfo.baseInfo.ucMaxSpLimit = 3;	//最大SP级数8.21，写死
		//9.11 changed by hyn 魔法物理技能等级要初始为
		fullPlayerInfo.baseInfo.usPhysicsLevel = 1;
		fullPlayerInfo.baseInfo.usMagicLevel = 1;
		fullPlayerInfo.baseInfo.ucMaxPhyLevelLimit = 1;   
		fullPlayerInfo.baseInfo.ucMaxMagLevelLimit = 1;
		fullPlayerInfo.baseInfo.strength = pIntiRoleInfo->str;
		fullPlayerInfo.baseInfo.vitality = pIntiRoleInfo->vit;
		fullPlayerInfo.baseInfo.agility = pIntiRoleInfo->agi;
		fullPlayerInfo.baseInfo.spirit = pIntiRoleInfo->spi;
		fullPlayerInfo.baseInfo.intelligence = pIntiRoleInfo->inte;

		fullPlayerInfo.skillInfo.skillData[0].skillID = pIntiRoleInfo->uiSkill[0];
		fullPlayerInfo.skillInfo.skillData[0].effectiveSkillID = pIntiRoleInfo->uiSkill[0];
		fullPlayerInfo.skillInfo.skillData[1].skillID = pIntiRoleInfo->uiSkill[1];
		fullPlayerInfo.skillInfo.skillData[1].effectiveSkillID = pIntiRoleInfo->uiSkill[1];
		fullPlayerInfo.skillInfo.skillData[2].skillID = pIntiRoleInfo->uiSkill[2];
		fullPlayerInfo.skillInfo.skillData[2].effectiveSkillID = pIntiRoleInfo->uiSkill[2];
		fullPlayerInfo.skillInfo.skillData[3].skillID = pIntiRoleInfo->uiSkill[3];
		fullPlayerInfo.skillInfo.skillData[3].effectiveSkillID = pIntiRoleInfo->uiSkill[3];
		fullPlayerInfo.skillInfo.skillData[4].skillID = pIntiRoleInfo->uiSkill[4];
		fullPlayerInfo.skillInfo.skillData[4].effectiveSkillID = pIntiRoleInfo->uiSkill[4];
		fullPlayerInfo.skillInfo.skillData[5].skillID = pIntiRoleInfo->uiSkill[5];
		fullPlayerInfo.skillInfo.skillData[5].effectiveSkillID = pIntiRoleInfo->uiSkill[5];

		fullPlayerInfo.equipInfo.equipSize = EQUIP_SIZE;
		fullPlayerInfo.skillInfo.skillSize = SKILL_SIZE;
		fullPlayerInfo.bufferInfo.buffSize = BUFF_SIZE;
		fullPlayerInfo.stateInfo.SetAlive();

		InitRoleItemInfo* pRoleItemInfo = CInitRoleItemManagerSingle::instance()->Find( mNewPlayerInfo.wRace, mNewPlayerInfo.nClass, bSex  );
		if (NULL == pRoleItemInfo )
		{
			D_DEBUG("账号%s的玩家创建角色出错:未找到对应种族=%d, 职业=%d, 性别=%d的初始化玩家的装备失败\n", mNewPlayerInfo.szAccount, mNewPlayerInfo.wRace, mNewPlayerInfo.nClass, mNewPlayerInfo.wSex);
			newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
			break;
		}

		//初始化人物的金钱
		fullPlayerInfo.baseInfo.dwSilverMoney = pRoleItemInfo->money;
		fullPlayerInfo.baseInfo.dwGoldMoney = pRoleItemInfo->money;

		//初始化身上的装备
		for ( unsigned int i = 0 ; i< sizeof(fullPlayerInfo.equipInfo.equips)/sizeof(fullPlayerInfo.equipInfo.equips[0]) ; i++ )
		{
			ItemInfo_i& equipItem = fullPlayerInfo.equipInfo.equips[i];
			if ( pRoleItemInfo->InitEquipItem[i] != 0 )
			{
#ifndef MULTI_DB_HASH
				equipItem.uiID = CInitRoleItemManagerSingle::instance()->GetInsertItemStart();
#else
				equipItem.uiID = GetTaskPool()->GetNextSequenceID();
#endif
				equipItem.usItemTypeID = pRoleItemInfo->InitEquipItem[i];
				equipItem.usWearPoint  = 10000;
				equipItem.ucCount = 1;
				equipItem.ucLevelUp = (CHAR)0x80;
			}
		}

		//初始化包裹中的物品
		if( !pRoleItemInfo->InitPkgItem.empty() )
		{
			PlayerInfo_3_Pkgs& pkgInfo = fullPlayerInfo.pkgInfo;
			std::vector<InitItem>::iterator lter =pRoleItemInfo->InitPkgItem.begin(),
				                         lterEnd =pRoleItemInfo->InitPkgItem.end();
			unsigned int pkgItemCount  = 0;
			for(  ;lter != lterEnd; ++lter )
			{
				const InitItem& initItem = *lter;
				if ( initItem.bstack )//如果可堆叠物品
				{
					if(pkgItemCount >= sizeof(pkgInfo.pkgs)/sizeof(pkgInfo.pkgs[0]))
					{
						D_ERROR("pkgItemCount >= sizeof(pkgInfo.pkgs)/sizeof(pkgInfo.pkgs[0]),下标越界0\n");
						break;
					}
#ifndef MULTI_DB_HASH
					pkgInfo.pkgs[pkgItemCount].uiID         = CInitRoleItemManagerSingle::instance()->GetInsertItemStart();
#else
					pkgInfo.pkgs[pkgItemCount].uiID         = GetTaskPool()->GetNextSequenceID();
#endif
					pkgInfo.pkgs[pkgItemCount].usItemTypeID = initItem.itemTypeID;
					pkgInfo.pkgs[pkgItemCount].ucCount      = initItem.count;
					pkgInfo.pkgs[pkgItemCount].usWearPoint  = 10000;
					pkgItemCount++;
				}
				else//如果为不可堆叠物品
				{
					for ( unsigned int i = 0; i<  initItem.count; i++ )
					{
						if(pkgItemCount >= sizeof(pkgInfo.pkgs)/sizeof(pkgInfo.pkgs[0]))
						{
							D_ERROR("pkgItemCount >= sizeof(pkgInfo.pkgs)/sizeof(pkgInfo.pkgs[0]),下标越界1\n");
							break;
						}
#ifndef MULTI_DB_HASH
						pkgInfo.pkgs[pkgItemCount].uiID         = CInitRoleItemManagerSingle::instance()->GetInsertItemStart();
#else
						pkgInfo.pkgs[pkgItemCount].uiID         = GetTaskPool()->GetNextSequenceID();
#endif
						pkgInfo.pkgs[pkgItemCount].usItemTypeID = initItem.itemTypeID;
						pkgInfo.pkgs[pkgItemCount].ucCount      = 1;
						pkgInfo.pkgs[pkgItemCount].usWearPoint  = 10000;
						pkgItemCount++;
					}
				}
			}
		}

		// 插入表
		if( InsertPlayerInfo( pConnect, (const char *)&fullPlayerInfo, sizeof(FullPlayerInfo), uiInsertID) == -1)
		{
			newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_FAILED;
			D_DEBUG("DB操作记录:创建角色信息:出错,账号=%s\n", mNewPlayerInfo.szAccount);
			break;
		}
			
		D_DEBUG("DB操作记录:创建角色信息:成功,账号=%s\n", mNewPlayerInfo.szAccount);

		newPlayerInfoReturn.iRet = GCCreateRole::ISSELECTOK_SUCCESS;
		newPlayerInfoReturn.playerInfoLogin.uiID = uiInsertID;
		SafeStrCpy(newPlayerInfoReturn.playerInfoLogin.szNickName, mNewPlayerInfo.strNickName);
		newPlayerInfoReturn.playerInfoLogin.usLevel = pIntiRoleInfo->ucLevel;
		newPlayerInfoReturn.playerInfoLogin.ucRace = mNewPlayerInfo.wRace;
		newPlayerInfoReturn.playerInfoLogin.usClass = mNewPlayerInfo.nClass;
		newPlayerInfoReturn.playerInfoLogin.usHair  = mNewPlayerInfo.nHair;
		newPlayerInfoReturn.playerInfoLogin.usHairColor = pIntiRoleInfo->usHairColor;
		newPlayerInfoReturn.playerInfoLogin.usFace = mNewPlayerInfo.nFace;
		newPlayerInfoReturn.playerInfoLogin.usPortrait  = mNewPlayerInfo.usPortrait;
		StructMemCpy((newPlayerInfoReturn.playerInfoLogin.equipInfo), &(fullPlayerInfo.equipInfo),sizeof(newPlayerInfoReturn.playerInfoLogin.equipInfo.equips));
		newPlayerInfoReturn.playerInfoLogin.usMapID = pIntiRoleInfo->usMapID;
		newPlayerInfoReturn.playerInfoLogin.iPosX = pIntiRoleInfo->iPosX;
		newPlayerInfoReturn.playerInfoLogin.iPosY = pIntiRoleInfo->iPosY;
	} while(false);

	newPlayerInfoReturn.sequenceID = sequenceID;

	MsgToPutContent* pContent = NEW MsgToPutContent;
	pContent->msgType = DGNewPlayerInfoReturn::wCmd;
	pContent->sessionID = m_gateID;
	StructMemCpy( pContent->szContent, &newPlayerInfoReturn, sizeof(newPlayerInfoReturn) );

#ifdef MULTI_DB_HASH

	PUSH_MSG_TO_QUEUE( pContent );

#else

	GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif
	

	

	return;
}

void InsertPlayerInfoTask::OnResult()
{
}

int InsertPlayerInfoTask::InsertPlayerInfo( CConnection*  pConnection, const char * pszData, unsigned long ulDataLength, unsigned int &uiInsertID)
{
	if((pszData == NULL) || (ulDataLength < sizeof(FullPlayerInfo)))
		return -1;

	TRY_BEGIN;

	// 返回值
	int iRet = 0;
	FullPlayerInfo *pFullPlayerInfo = (FullPlayerInfo *)pszData;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char escapeBaseData[2 * sizeof(PlayerInfo_1_Base) + 1] = {0};
	pQuery->RealEscape(escapeBaseData, (const char *)&pFullPlayerInfo->baseInfo, (unsigned long) sizeof(PlayerInfo_1_Base)); // 转义

	char escapteEquipData[2 * sizeof(PlayerInfo_2_Equips) + 1] = {0};
	pQuery->RealEscape(escapteEquipData, (const char *)&pFullPlayerInfo->equipInfo, (unsigned long) sizeof(PlayerInfo_2_Equips)); // 转义

	char escaptePkgData[2 * sizeof(PlayerInfo_3_Pkgs) + 1] = {0};
	pQuery->RealEscape(escaptePkgData, (const char *)&pFullPlayerInfo->pkgInfo, (unsigned long) sizeof(PlayerInfo_3_Pkgs)); // 转义

	char escapteSkillData[2 * sizeof(PlayerInfo_4_Skills) + 1] = {0};
	pQuery->RealEscape(escapteSkillData, (const char *)&pFullPlayerInfo->skillInfo, (unsigned long) sizeof(PlayerInfo_4_Skills)); // 转义

	char escapteBufferData[2 * sizeof(PlayerInfo_5_Buffers) + 1] = {0};
	pQuery->RealEscape(escapteBufferData, (const char *)&pFullPlayerInfo->bufferInfo, (unsigned long) sizeof(PlayerInfo_5_Buffers)); // 转义

	char escapteStateData[2 * sizeof(PlayerStateInfo) + 1] = {0};
	pQuery->RealEscape(escapteStateData, (const char *)&pFullPlayerInfo->stateInfo, (unsigned long) sizeof(PlayerStateInfo)); // 转义

	char escapteTaskData[2 * sizeof(TaskRecordInfo)*(MAX_TASKRD_SIZE) + 1] = {0};
	pQuery->RealEscape(escapteTaskData, (const char *)&pFullPlayerInfo->taskInfo, (unsigned long) sizeof(TaskRecordInfo)*(MAX_TASKRD_SIZE)); // 转义

	std::stringstream ss;

	ss << "INSERT INTO playerinfo (Account, Nickname, BaseData, EquipData, PkgData, SkillData, BufferData, StateData,TaskData, CreateTime) VALUES("
		<< "'" << pFullPlayerInfo->baseInfo.szAccount  << "',"
		<< "'" << pFullPlayerInfo->baseInfo.szNickName << "',"
		<< "'" << escapeBaseData    << "',"
		<< "'" << escapteEquipData  << "',"
		<< "'" << escaptePkgData    << "',"
		<< "'" << escapteSkillData  << "',"
		<< "'" << escapteBufferData << "',"
		<< "'" << escapteStateData  << "',"
		<< "'" << escapteTaskData   << "',"
		<< "NOW())";

	std::string statement = ss.str();

	// 执行查询
	iRet = pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) );
	uiInsertID = pQuery->InsertID();

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;

	TRY_END;

	return 0;
}

