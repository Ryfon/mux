﻿#include "UpdatePlayerItemTask.h"

void UpdatePlayerItemTask::OnResult()
{

}

void UpdatePlayerItemTask::OnThread(CConnection *pConnect)
{
	if ( !pConnect )
		return;

	TRY_BEGIN;

	int iRet = 0;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
	{
		iRet = -1;
		return;
	}

	if ( iRet != -1 )
	{
		// 构造SQL
		char escapePkgItemData[2 * sizeof(mpkgItemArr) + 1] = {0};
		pQuery->RealEscape(escapePkgItemData, (const char *)&mpkgItemArr, (unsigned long) sizeof(mpkgItemArr)); // 转义

		char escapeEquipItemData[2*sizeof(mequipInfo) + 1] = {0};
		pQuery->RealEscape(escapeEquipItemData, (const char *)&mequipInfo, (unsigned long)sizeof(mequipInfo) );


		std::stringstream ss;
		ss <<  " UPDATE playerinfo SET "
			<< " EquipData='" << escapeEquipItemData <<"', "
			<< " PkgData='" << escapePkgItemData <<"' "
			<< " WHERE "
			<< " ID= " << updatePlayerUID;

		std::string statement = ss.str();

		// 执行查询
		if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
			iRet = -1;

		// 销毁查询
		pConnect->DestroyQuery(pQuery);

	}

	TRY_END;
}
