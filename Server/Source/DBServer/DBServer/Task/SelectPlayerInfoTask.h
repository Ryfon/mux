﻿/********************************************************************
	created:	2008/06/30
	created:	30:6:2008   16:24
	file:		SelectPlayerInfoTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef  SELECT_PLAYERINFO_TASK_H_
#define  SELECT_PLAYERINFO_TASK_H_

#include "ITask.h"
#include <vector>
#include "../PlayerInfoManager.h"


class SelectPlayerInfoTask:public ITask
{
public:
	enum ESELECTRESULT
	{
		E_OTHER,
		E_INVALID,
		E_SUCCESS,
	};

public:
	SelectPlayerInfoTask(CGateServer* pGateSrv, GDPlayerInfo* pPlayerInfo ):ITask(pGateSrv),m_pFullPlayerInfo(NULL),eResult(E_OTHER)
	{
		StructMemCpy( m_pPlayerInfo,pPlayerInfo,sizeof(GDPlayerInfo) );
	};

	virtual ~SelectPlayerInfoTask(){}

public:
	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

	int SelectPlayerInfo( CConnection* pConnect,const char *pszAccount, const unsigned int uiID, char * pszData, unsigned long ulDataLength );

private:
	///向gate发送玩家存盘信息；
	void PlayerDbInfoSend();

	/////填充待发的petinfo;
	//void FillPlayerPetInfo( DGDbGetInfo& getInfo );

	/////填充待发的套装信息
	//void FillPlayerSuitInfo( DGDbGetInfo& getInfo );

	/////填充待发的时装服饰信息
	//void FillPlayerFashionInfo( DGDbGetInfo& getInfo );

private:
	///发送玩家副本阶段信息；
	void SendPlayerCopyStageInfo();
	//发送宠物的信息
	void SendPlayerPetInfo();
	//发送套装的信息
	void SendPlayerSuitInfo();
	//发送时装的信息
	void SendPlayerFashionInfo();
	//发送保护道具信息
	void SendPlayerProtectItemInfo();
	//发送任务存盘信息
	void SendPlayerTaskRdInfo();
	//发送技能信息
	void SendPlayerSkillInfo();
	//发送Buff信息
	void SendPlayerBuffInfo();
	//发送状态信息
	//void SendPlayerStateInfo();
protected:
	GDPlayerInfo  m_pPlayerInfo;
	DGPlayerInfo  playerInfoReturn;
	DbPetInfo     petInfo;//玩家宠物信息；
	PlayerStageInfos copyStageInfos;//副本阶段信息；
	PlayerInfo_Suits suitInfo;//套装信息
	PlayerInfo_Fashions fashionInfo;//时装服饰信息
	FullPlayerInfoEx*   m_pFullPlayerInfo;
	std::vector<DGPlayerInfo> playerInfoVec;
	ESELECTRESULT   eResult;
	
};

#endif
