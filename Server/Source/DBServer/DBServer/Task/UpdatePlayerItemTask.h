﻿/********************************************************************
	created:	2009/06/24
	created:	24:6:2009   15:43
	file:		UpdatePlayerItemTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#include "ITask.h"

#ifndef _UPDATE_PLAYER_ITEM_TASK
#define _UPDATE_PLAYER_ITEM_TASK

class UpdatePlayerItemTask : public ITask
{
public:
	UpdatePlayerItemTask( CGateServer* pGateSrv, const ItemInfo_i* equipItemArr, const ItemInfo_i* pkgItemArr ,unsigned int playerUID ) : ITask(pGateSrv),updatePlayerUID( playerUID )
	{
		StructMemCpy( mpkgItemArr, pkgItemArr,sizeof(mpkgItemArr) );
		StructMemCpy( mequipInfo.equips, equipItemArr, sizeof(mequipInfo.equips) );
		mequipInfo.equipSize = 16;
	}

	virtual ~UpdatePlayerItemTask()
	{
	}

public:
	void OnThread( CConnection* pConnect );

	void OnResult();

private:
	unsigned int updatePlayerUID;
	ItemInfo_i   mpkgItemArr[PACKAGE_SIZE];
	PlayerInfo_2_Equips mequipInfo;
};

#endif //_UPDATE_PLAYER_ITEM_TASK

