﻿/********************************************************************
	created:	2009/06/09
	created:	9:6:2009   15:32
	file:		UpdateProtectItemTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef _UPDATE_PROTECT_ITEM_TASK_H
#define _UPDATE_PROTECT_ITEM_TASK_H

#include "ITask.h"

class UpdateProtectItemTask : public ITask
{
public:
	UpdateProtectItemTask( CGateServer* pGateSrv, const ProtectItem* protectItemArr ,unsigned int playerUID ) : ITask(pGateSrv),updatePlayerUID(playerUID)
	{
		if ( protectItemArr )
		{
			StructMemCpy( m_protectItemArr, protectItemArr,sizeof(m_protectItemArr) );
		}	
	}

	virtual ~UpdateProtectItemTask()
	{
	}

public:
	void OnThread( CConnection* pConnect );

	void OnResult();

private:
	ProtectItem m_protectItemArr[PACKAGE_SIZE];
	unsigned int updatePlayerUID;
};

#endif //_DBPLAYERSAVE_TASK_H
