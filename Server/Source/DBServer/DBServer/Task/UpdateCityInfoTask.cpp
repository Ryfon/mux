﻿#include "UpdateCityInfoTask.h"

UpdateCityInfoTask::~UpdateCityInfoTask()
{
}


void UpdateCityInfoTask::OnThread(CConnection* pConnect )
{
	int dbret = UpdateCityInfo( pConnect );

	if(dbret == -1)
	{
		D_DEBUG("DB操作记录:更新关卡地图数据:出错,关卡城市ID:%d\n", m_updateCityInfo.uiCityID );
	}
	else
	{
		D_DEBUG("DB操作记录:更新关卡地图数据:成功,关卡城市ID:%d\n", m_updateCityInfo.uiCityID );
	}

}


void UpdateCityInfoTask::OnResult()
{

}


int UpdateCityInfoTask::UpdateCityInfo( CConnection* pConnection )
{
	if ( !pConnection )
		return -1;

	int iRet = 0;
	TRY_BEGIN;
	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;
	
	

	std::stringstream ss;
	ss << "UPDATE cityinfo SET "
		<< "CityLevel='" << m_updateCityInfo.cityLevel << "',"
		<< "CityExp='" << m_updateCityInfo.cityExp << "',"
		<< "MapID='" << m_updateCityInfo.uiMapID << "',"
		<< "Race='" << (int)(m_updateCityInfo.ucRace) << "',"
		<< "Type='" << (int)(m_updateCityInfo.ucType) << "'"
		<< " WHERE " 
		<< "ID=" << m_updateCityInfo.uiCityID;


	std::string statement = ss.str();

	// 执行查询
	if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
		iRet = -1;

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	TRY_END;
	return iRet;
}



