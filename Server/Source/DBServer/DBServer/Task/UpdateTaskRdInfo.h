﻿/********************************************************************
	created:	2009/06/10
	created:	10:6:2009   19:18
	file:		UpdateTaskRdInfo.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#include "ITask.h"

#ifndef _UPDATE_TASKRDINFO_TASK
#define _UPDATE_TASKRDINFO_TASK

class UpdateTaskRdInfoTask : public ITask
{
public:
	UpdateTaskRdInfoTask( CGateServer* pGateSrv, const unsigned * taskRdArr ,unsigned int playerUID ) : ITask(pGateSrv),updatePlayerUID(playerUID)
	{
		if ( taskRdArr != NULL )
		{
			StructMemCpy( m_TaskRdInfoArr, taskRdArr,sizeof(m_TaskRdInfoArr) );
		}	
	}

	virtual ~UpdateTaskRdInfoTask()
	{
	}

public:
	void OnThread( CConnection* pConnect );

	void OnResult();

private:
	unsigned int m_TaskRdInfoArr[100];
	unsigned int updatePlayerUID;
};

#endif //_DBPLAYERSAVE_TASK_H
