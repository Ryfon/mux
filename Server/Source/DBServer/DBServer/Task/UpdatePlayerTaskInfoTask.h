﻿/********************************************************************
	created:	2009/06/24
	created:	24:6:2009   16:06
	file:		UpdatePlayerTaskInfoTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef UPDATE_PLAYER_TASKINFO_TASK
#define UPDATE_PLAYER_TASKINFO_TASK

#include "ITask.h"

class UpdatePlayerTaskInfoTask : public ITask
{
public:
	UpdatePlayerTaskInfoTask( CGateServer* pGateSrv, const TaskRecordInfo* taskRdInfo, unsigned int playerUID ) : ITask(pGateSrv),updatePlayerUID( playerUID )
	{
		StructMemCpy( mtaskInfo, taskRdInfo, sizeof(mtaskInfo) );
	}

	virtual ~UpdatePlayerTaskInfoTask()
	{
	}

public:
	void OnThread( CConnection* pConnect );

	void OnResult();

private:
	unsigned int    updatePlayerUID;
	TaskRecordInfo  mtaskInfo[MAX_TASKRD_SIZE];
};

#endif //UPDATE_PLAYER_TASKINFO_TASK
