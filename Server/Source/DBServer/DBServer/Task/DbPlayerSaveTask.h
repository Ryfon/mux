﻿/********************************************************************
	created:	2009/05/13
	file:		DbPlayerSaveTask.h
	author:		邓子建
	
	purpose:	
*********************************************************************/

#ifndef _DBPLAYERSAVE_TASK_H
#define _DBPLAYERSAVE_TASK_H

#include "ITask.h"

class DbPlayerSaveTask : public ITask
{
public:
	DbPlayerSaveTask( CGateServer* pGateSrv, GDDbSaveInfo* pPlayerSaveInfo ) : ITask(pGateSrv)
	{
		StructMemCpy( m_PlayerSaveInfo, pPlayerSaveInfo,sizeof(m_PlayerSaveInfo) );
	}

	virtual ~DbPlayerSaveTask()
	{
	}

public:
	void OnThread(  CConnection* pConnect );

	void OnResult() {};

private:
	GDDbSaveInfo       m_PlayerSaveInfo;
};

#endif //_DBPLAYERSAVE_TASK_H
