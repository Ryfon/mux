﻿#include "SelectCityTask.h"



SelectCityTask::~SelectCityTask(void)
{
}


void SelectCityTask::OnThread( CConnection* pConnect )
{
	if ( !pConnect )
		return;

	TRY_BEGIN;

	if( SelectCityInfo( pConnect ) == -1 )
	{
		D_ERROR("选择城市信息发生错误\n");
		m_resCode = -1;
		return;
	}

	TRY_END;
}


int SelectCityTask::SelectCityInfo( CConnection* pConnect )
{
	if( pConnect == NULL )
		return -1;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	int iRet = 0;
	m_cityInfoResult.cityNum = 0;
	unsigned int& cityNum = m_cityInfoResult.cityNum;
	char szStatement[1024];
	if ( m_reqInfo.nMapNum < 0 )
	{
		D_ERROR( "SelectCityTask::SelectCityInfo， m_reqInfo.nMapNum<0\n" );
		return -1;
	}
	else if(m_reqInfo.nMapNum >= sizeof(m_reqInfo.lMapCode)/sizeof(m_reqInfo.lMapCode[0]))
	{
		D_ERROR( "SelectCityTask::SelectCityInfo， 请求数目大于数组长度, m_reqInfo.nMapNum=%d\n", m_reqInfo.nMapNum);
		return -1;
	}
	
	unsigned int arraySize = ARRAY_SIZE( m_cityInfoResult.cityArr );
	for( unsigned int i=0; i< (unsigned int)m_reqInfo.nMapNum; ++i )
	{
		if( cityNum >= arraySize )
		{
			D_ERROR("地图数量已经超过能发送的最大城市信息数\n");
			iRet = -1;
			break;
		}

		int iStatementLen = ACE_OS::snprintf(szStatement, 
			sizeof(szStatement), 
			"SELECT ID, CityLevel, MapID, CityExp, Race, Type FROM cityinfo WHERE MapID ='%d'", m_reqInfo.lMapCode[i] );
		
		if(iStatementLen <= 0 )
		{
			iRet = -1;
			break;
		}
		else
		{
			// 执行查询
			CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
			if(pResult == NULL)
			{
				continue;	//有可能该地图就是没关卡数据
			}
			else
			{
				// 处理结果集
				if(pResult->Next())
				{
					m_cityInfoResult.cityArr[cityNum].uiCityID = pResult->GetInt( 0 );

					m_cityInfoResult.cityArr[cityNum].cityLevel = pResult->GetInt( 1 );

					m_cityInfoResult.cityArr[cityNum].uiMapID = pResult->GetInt( 2 );

					m_cityInfoResult.cityArr[cityNum].cityExp = pResult->GetInt( 3 );
					
					m_cityInfoResult.cityArr[cityNum].ucRace = pResult->GetInt( 4 );

					m_cityInfoResult.cityArr[cityNum].ucType = pResult->GetInt( 5 );

					cityNum++;
				}
				// 销毁查询
				pQuery->FreeResult(pResult);
			}
		}
	}

	return iRet;
}


void SelectCityTask::OnResult()
{
	if( m_resCode == 0 )
	{
		MsgToPutContent* pContent = NEW MsgToPutContent;
		pContent->msgType = DGCityInfoResult::wCmd;
		pContent->sessionID = m_gateID;//mp_gateSrv->GetSessionID();
		StructMemCpy( pContent->szContent, &m_cityInfoResult, sizeof(m_cityInfoResult) );

#ifdef MULTI_DB_HASH
		
		PUSH_MSG_TO_QUEUE( pContent );

#else
		GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif
	}else
	{
		D_ERROR("OnThread已经错误，不发送信息\n");	
	}
}
