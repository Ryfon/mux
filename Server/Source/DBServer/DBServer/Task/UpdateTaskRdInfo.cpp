﻿#include "UpdateTaskRdInfo.h"

void UpdateTaskRdInfoTask::OnThread(CConnection* pConnect )
{
	if ( !pConnect )
		return;

	TRY_BEGIN;

	int iRet = 0;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
	{
		iRet = -1;
		return;
	}

	if ( iRet != -1 )
	{
		// 构造SQL
		char escapeTaskRdInfoData[2 * sizeof(m_TaskRdInfoArr) + 1] = {0};
		pQuery->RealEscape(escapeTaskRdInfoData, (const char *)&m_TaskRdInfoArr, (unsigned long) sizeof(m_TaskRdInfoArr)); // 转义


		std::stringstream ss;
		ss <<  " UPDATE playerinfo SET "
			<< " TaskRecord='" << escapeTaskRdInfoData <<"' "
			<< " WHERE "
			<< " ID= " << updatePlayerUID;


		std::string statement = ss.str();

		// 执行查询
		if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
			iRet = -1;

		// 销毁查询
		pConnect->DestroyQuery(pQuery);

	}

	TRY_END;

}

void UpdateTaskRdInfoTask::OnResult()
{

}

