﻿#ifndef GET_ITEM_SEQUENCE_TASK_H
#define GET_ITEM_SEQUENCE_TASK_H

#include "ITask.h"
#include "ace/Mutex.h"

class CGetItemSequenceTask:public ITask
{
public:
	CGetItemSequenceTask(CGateServer* pGateSrv,GDBatchItemSequence *pItemSeqReq ):ITask(pGateSrv)
	{
		StructMemCpy(itemSequenceReq, pItemSeqReq, sizeof(itemSequenceReq));
	}

	virtual ~CGetItemSequenceTask()
	{
	}

public:
	virtual void OnThread(CConnection* pConnect );

	virtual void OnResult();

private:
	GDBatchItemSequence itemSequenceReq;

	DGBatchItemSequence resp;


	static ACE_Mutex m_itemIDLock;//全局sessionID写锁；
};



#endif/*GET_ITEM_SEQUENCE_TASK_H*/
