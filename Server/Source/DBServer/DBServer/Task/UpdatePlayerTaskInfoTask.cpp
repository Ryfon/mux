﻿#include "UpdatePlayerTaskInfoTask.h"

void UpdatePlayerTaskInfoTask::OnResult()
{

}


void UpdatePlayerTaskInfoTask::OnThread(CConnection* pConnect )
{
	if ( !pConnect )
		return;

	TRY_BEGIN;

	int iRet = 0;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
	{
		iRet = -1;
		return;
	}

	if ( iRet != -1 )
	{
		// 构造SQL
		char escapeTaskInfoData[2 * sizeof(mtaskInfo) + 1] = {0};
		pQuery->RealEscape(escapeTaskInfoData, (const char *)&mtaskInfo, (unsigned long) sizeof(mtaskInfo)); // 转义

		std::stringstream ss;
		ss <<  " UPDATE playerinfo SET "
			<< " TaskData='" << escapeTaskInfoData <<"' "
			<< " WHERE "
			<< " ID= " << updatePlayerUID;

		std::string statement = ss.str();

		// 执行查询
		if(pQuery->ExecuteUpdate(statement.c_str(), (unsigned long) (statement.size()) ) < 0)
		{
			D_DEBUG("更新任务状态失败\n");
			iRet = -1;
		}	

		// 销毁查询
		pConnect->DestroyQuery(pQuery);

	}

	TRY_END;
}

