﻿#include "QueryOffLinePlayerInfo.h"

void CQueryOffLinePlayerTask::OnThread(CConnection* pConnect )
{
	if ( !pConnect )
		return;

	TRY_BEGIN;

	int iRet = 0;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
	{
		iRet = -1;
		return;
	}

	if ( iRet != -1 )
	{
		// 构造SQL
		std::stringstream ss;
		ss << "select basedata, equipdata from playerinfo where binary Nickname = '" << m_szQueryName << "'";

		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect( ss.str().c_str(), (unsigned long)ss.str().size() );
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
			iRet = -1;
		}
		else
		{
			//清空
			StructMemSet(m_offLinePlayerInfo, 0x00, sizeof(m_offLinePlayerInfo));

			m_offLinePlayerInfo.lNotiPlayerID = m_notifyPlayerID;
			StructMemCpy(m_offLinePlayerInfo.rst.targetName, m_szQueryName, sizeof(m_offLinePlayerInfo.rst.targetName));
			m_offLinePlayerInfo.rst.nameSize = (UINT)strlen(m_szQueryName);
			m_offLinePlayerInfo.rst.existFlag = (pResult->RowNum() > 0) ? true : false;
			if( pResult->Next() )
			{
				D_DEBUG("DB操作记录:查找离线角色信息:成功,角色名=%s\n", m_szQueryName);

				PlayerInfo_1_Base baseInfo;
				PlayerInfo_2_Equips equipInfo;

				StructMemCpy(baseInfo, pResult->GetString(1), sizeof(baseInfo));
				StructMemCpy(equipInfo, pResult->GetString(2), sizeof(equipInfo));

				PlayerInfoLogin &playerInfo = m_offLinePlayerInfo.rst.playerInfo;
				playerInfo.uiID = baseInfo.uiID;
				playerInfo.nameSize = ACE_OS::snprintf(playerInfo.szNickName,MAX_NAME_SIZE,"%s", baseInfo.szNickName );
				playerInfo.usLevel = baseInfo.usLevel;
				playerInfo.ucRace = baseInfo.ucRace;
				playerInfo.usClass = baseInfo.usClass;
				playerInfo.shFlag = baseInfo.shFlag;
				playerInfo.usHair = baseInfo.usHair;
				playerInfo.usHairColor = baseInfo.usHairColor;
				playerInfo.usFace = baseInfo.usFace;
				playerInfo.usPortrait = baseInfo.usPortrait;
				StructMemCpy(playerInfo.equipInfo, &equipInfo, sizeof(playerInfo.equipInfo));
				playerInfo.usMapID = baseInfo.usMapID;
				playerInfo.iPosX = baseInfo.iPosX;
				playerInfo.iPosY = baseInfo.iPosY;
			}
			else
			{
				D_DEBUG("DB操作记录:查找离线角色信息:出错,角色名=%s\n", m_szQueryName);
			}

			MsgToPutContent* pContent = NEW MsgToPutContent;
			pContent->msgType   = DGQueryOffLinePlayerInfoRst::wCmd;
			pContent->sessionID = m_gateID;
			StructMemCpy( pContent->szContent, &m_offLinePlayerInfo, sizeof(m_offLinePlayerInfo) );

#ifdef MULTI_DB_HASH
			PUSH_MSG_TO_QUEUE( pContent );
#else
			GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );
#endif;
			
			pQuery->FreeResult( pResult );
		}
	}

	pConnect->DestroyQuery( pQuery );

	TRY_END;
}

void CQueryOffLinePlayerTask::OnResult()
{
	return;
}
