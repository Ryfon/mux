﻿/********************************************************************
	created:	2009/06/24
	created:	24:6:2009   15:55
	file:		UpdatePlayerBaseInfoTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef UPDATE_PLAYER_BASE_INFO_TASK
#define UPDATE_PLAYER_BASE_INFO_TASK


#include "ITask.h"

class UpdatePlayerBaseInfoTask : public ITask
{
public:
	UpdatePlayerBaseInfoTask( CGateServer* pGateSrv, const PlayerInfo_1_Base& baseInfo, unsigned int playerUID ) : ITask(pGateSrv),updatePlayerUID( playerUID )
	{
		StructMemCpy( mPlayerBaseInfo, &baseInfo, sizeof(mPlayerBaseInfo) );
	}

	virtual ~UpdatePlayerBaseInfoTask()
	{
	}

public:
	void OnThread( CConnection* pConnect );

	void OnResult();

private:
	unsigned int updatePlayerUID;
	PlayerInfo_1_Base mPlayerBaseInfo;
};

#endif //UPDATE_PLAYER_BASE_INFO_TASK
