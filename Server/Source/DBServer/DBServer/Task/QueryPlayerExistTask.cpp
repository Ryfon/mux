﻿#include "QueryPlayerExistTask.h"

void QueryPlayerExistTask::OnResult()
{
	{
		DGQueryPlayerExistRst queryRst;
		queryRst.playerExist = isExist;
		queryRst.playeruid   = mPlayerUID;
		queryRst.queryType   = mQueryExistType;
		queryRst.queryUID    = mQueryID;
		StructMemCpy(queryRst.recvPlayerName, szQueryName, sizeof(queryRst.recvPlayerName));

		MsgToPutContent* pContent = NEW MsgToPutContent;
		pContent->msgType = DGQueryPlayerExistRst::wCmd;
		pContent->sessionID = m_gateID;
		StructMemCpy( pContent->szContent, &queryRst, sizeof(queryRst) );

#ifdef MULTI_DB_HASH

		PUSH_MSG_TO_QUEUE( pContent );

#else
		GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif
	}
}


void QueryPlayerExistTask::OnThread(CConnection* pConnect )
{
	if ( !pConnect )
		return;

	if ( !bInit)
		return;

	TRY_BEGIN;

	int iRet = 0;

	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
	{
		iRet = -1;
		return;
	}

	if ( iRet != -1 )
	{
		// 构造SQL
		//char escapeName[2 * MAX_NAME_SIZE + 1] = {0};
		//pQuery->RealEscape( escapeName, (const char *)szQueryName, (unsigned long) sizeof(szQueryName) ); // 转义

		std::stringstream ss;
		ss << "select * from playerinfo where binary Nickname like '" << szQueryName << "'";

		// 执行查询
		CResult * pResult = pQuery->ExecuteSelect( ss.str().c_str(), (unsigned long)ss.str().size() );
		if(pResult == NULL)
		{
			D_ERROR("执行sql语句:%s错误\n", ss.str().c_str());
			iRet = -1;
		}
		else
		{
			//本次影响的行数
			int affectNum = pResult->RowNum();
			if ( affectNum > 0 && pResult->Next() )
			{
				mPlayerUID = pResult->GetInt(0);//ID 
				isExist = true;
				D_DEBUG("DB操作记录:查找指定角色是否存在:成功,角色名=%s\n", szQueryName);
			}
			else
			{
				D_DEBUG("DB操作记录:查找指定角色是否存在:出错,角色名=%s\n", szQueryName);
			}

			pQuery->FreeResult( pResult );
		}
	}

	pConnect->DestroyQuery( pQuery );

	TRY_END;
}

