﻿/********************************************************************
	created:	2008/06/27
	created:	27:6:2008   15:12
	file:		ITask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/


#ifndef ITask_H_
#define ITask_H_

#include "../OtherServer.h"
#include "../../../Base/MysqlWrapper.h"
#include "../../../Base/PkgProc/SrvProtocol.h"
#include "../../../Base/PkgProc/MsgToPut.h"
#include "../../../Base/PkgProc/PacketBuild.h"
#include "../../../Base/PkgProc/DealPkgBase.h"
#include "../RecordHandler.h"
#include "../GateSrvManager.h"
#include "../MsgContentQueue.h"
using namespace MUX_PROTO;

#ifdef MULTI_DB_HASH
class CTaskPoolEx;
#endif

//任务函数
class ITask
{
public:
	ITask( CGateServer* pGateSrv )
	{
		if ( pGateSrv )
			m_gateID = pGateSrv->GetSessionID();
		else
			m_gateID = 0;

		m_MsgQueue = NULL;
#ifdef MULTI_DB_HASH
		m_connectionID = 0;
		pTaskPool = NULL;
#endif
	};

	virtual ~ITask(){}

public:
	//线程处理过程
	virtual void OnThread( CConnection* pConnect ) = 0;

	//线程处理善后的过程
	virtual void OnResult() = 0;

public:
	void SetTaskMsgQueue( LSMSGCONTENT_QUEUE* pMsg_Queue ) { m_MsgQueue = pMsg_Queue ; } 

#ifdef MULTI_DB_HASH
	virtual void OnFullConnection( CConnection *pConnect, unsigned short num ){ACE_UNUSED_ARG(pConnect); ACE_UNUSED_ARG(num);}

	unsigned short GetConnectionID(void){return m_connectionID;}

	void SetConnectionID(unsigned short id, CTaskPoolEx *pPool){m_connectionID = id; pTaskPool = pPool;}

	CTaskPoolEx *GetTaskPool(void){return pTaskPool;}
#endif

protected:
	LSMSGCONTENT_QUEUE* Get_MsgQueue() { return m_MsgQueue ; }

protected:
	unsigned int m_gateID;

	LSMSGCONTENT_QUEUE* m_MsgQueue;

#ifdef MULTI_DB_HASH
	unsigned short m_connectionID;
	CTaskPoolEx *pTaskPool;
#endif
};


#endif

