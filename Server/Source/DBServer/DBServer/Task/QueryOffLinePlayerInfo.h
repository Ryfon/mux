﻿#ifndef QUERY_OFF_LINE_PLAYER_INFO_H
#define QUERY_OFF_LINE_PLAYER_INFO_H

#include "ITask.h"

class CQueryOffLinePlayerTask : public ITask
{
public:
	CQueryOffLinePlayerTask( CGateServer* pGateSrv,  const char* pszName ,PlayerID queryID)
		:ITask(pGateSrv), m_notifyPlayerID(queryID)
	{
		if( NULL != pszName )
		{
			SafeStrCpy( m_szQueryName, pszName);
		}
		else
		{
			StructMemSet( m_szQueryName, 0x00, MAX_NAME_SIZE);
		}
	}

	virtual ~CQueryOffLinePlayerTask()
	{
	}

public:

	virtual void OnThread(CConnection* pConnect );
	virtual void OnResult();

private:
	char m_szQueryName[MAX_NAME_SIZE];//目标玩家
	PlayerID m_notifyPlayerID;//查询玩家
	DGQueryOffLinePlayerInfoRst m_offLinePlayerInfo; //目标玩家的信息
};


#endif/*QUERY_OFF_LINE_PLAYER_INFO_H*/
