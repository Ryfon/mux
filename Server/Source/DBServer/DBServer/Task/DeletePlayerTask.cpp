﻿#include "DeletePlayerTask.h"


void DeletePlayerTask::OnResult()
{

}


int DeletePlayerTask::DeletePlayerInfo(CConnection* pConnection, unsigned int uiPlayerID )
{
	if ( !pConnection )
		return -1;

	// 返回值
	int iRet = 0;

	TRY_BEGIN;

	CQuery *pQuery = pConnection->CreateQuery();
	if(pQuery == NULL)
		return -1;

	// 构造SQL
	char szStatement[256];
	int iStatementLen = ACE_OS::snprintf(szStatement, 
		sizeof(szStatement), 
		"DELETE FROM playerinfo WHERE ID = %d", 
		uiPlayerID);
	if(iStatementLen <= 0)
		iRet = -1;
	else
	{
		// 执行查询
		iRet = pQuery->ExecuteUpdate((const char*)szStatement, iStatementLen);
	}

	// 销毁查询
	pConnection->DestroyQuery(pQuery);

	return iRet;
	TRY_END;

	return 0;
}

void DeletePlayerTask::OnThread(CConnection* pConnect )
{
	if ( !pConnect )
		return;

	DGDeletePlayerInfoReturn deletePlayerInfoReturn;
	StructMemSet(deletePlayerInfoReturn, 0x00, sizeof(deletePlayerInfoReturn));

	deletePlayerInfoReturn.playerID = mDeletePlayerInfo.playerID;
	deletePlayerInfoReturn.uiRoleID = mDeletePlayerInfo.uiPlayerInfoID;
	if( DeletePlayerInfo( pConnect, mDeletePlayerInfo.uiPlayerInfoID) == 1)
	{
		deletePlayerInfoReturn.iRet = GCDeleteRole::OP_SUCCESS;
		D_DEBUG("DB操作记录:删除角色信息:成功, roleid = %d\n", mDeletePlayerInfo.uiPlayerInfoID);
	}
	else
	{
		deletePlayerInfoReturn.iRet = GCDeleteRole::OP_FAILED;
		D_DEBUG("DB操作记录:删除角色信息:出错, roleid = %d\n", mDeletePlayerInfo.uiPlayerInfoID);
	}

	MsgToPutContent* pContent = NEW MsgToPutContent;
	pContent->msgType = DGDeletePlayerInfoReturn::wCmd;
	pContent->sessionID = m_gateID;
	StructMemCpy( pContent->szContent, &deletePlayerInfoReturn, sizeof(deletePlayerInfoReturn) );


#ifdef MULTI_DB_HASH
	
	PUSH_MSG_TO_QUEUE( pContent );

#else

	GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

#endif

	return;
}

