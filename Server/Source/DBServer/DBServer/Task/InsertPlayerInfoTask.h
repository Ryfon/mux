﻿/********************************************************************
	created:	2008/07/01
	created:	1:7:2008   13:26
	file:		InsertPlayerInfoTask.h
	author:		管传淇
	
	purpose:	
*********************************************************************/

#ifndef _INSERTPLAYERINFO_TASK_H
#define _INSERTPLAYERINFO_TASK_H

#include "ITask.h"

class InsertPlayerInfoTask:public ITask
{
public:
	InsertPlayerInfoTask( CGateServer* pGateSrv,GDNewPlayerInfo* NewPlayerInfo ):ITask(pGateSrv)
	{
		StructMemCpy( mNewPlayerInfo,NewPlayerInfo,sizeof(GDNewPlayerInfo) );
#ifdef MULTI_DB_HASH
		totalRoleCount = 0;
#endif
	}

	virtual ~InsertPlayerInfoTask()
	{
	}

public:

#ifdef MULTI_DB_HASH
	void OnFullConnection( CConnection *pConnect, unsigned short num);
#endif

	void OnThread( CConnection* pConnect );

	void OnResult();

	int TotalCountSameAccount( CConnection* pConnection, const char* szAccount );

	int ExistSamePlayerName( CConnection* pConnection, const char* szName );

	int  InsertPlayerInfo( CConnection*  pConnection, const char * pszData, unsigned long ulDataLength, unsigned int &uiInsertID);

private:
	GDNewPlayerInfo       mNewPlayerInfo;
	DGNewPlayerInfoReturn newPlayerInfoReturn;

#ifdef MULTI_DB_HASH
	int totalRoleCount;
#endif
};

#endif
