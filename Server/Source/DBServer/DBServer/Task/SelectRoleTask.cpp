﻿#include "SelectRoleTask.h"
#include "../../../Base/aceall.h"


#ifdef MULTI_DB_HASH
void SelectRoleTask::OnFullConnection( CConnection* pConnect, unsigned short num)
{
	TRY_BEGIN;
	
	char szStatement[1024];
	int iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
		"SELECT ID, BaseData, EquipData, FashionData FROM playerinfo WHERE Account = '%s'", 
		pLoginInfo.szAccount
		);

	if(iStatementLen <= 0 )
	{
		totalRoleCount = -1;
		return;
	}

	for(unsigned short i = 0; i < num; i++ )
	{
		if(totalRoleCount < 0)//此前连接上的操作可能出错
			return;

		CConnection *pTempConn = &pConnect[i];
		CQuery *pQuery = pTempConn->CreateQuery();
		if(pQuery == NULL)
		{
			D_ERROR("CreateQuery 出错\n");
			totalRoleCount = -1;

			return;
		}
		else
		{	
			// 执行查询
			CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
			if(pResult == NULL)
			{
				D_ERROR("执行sql语句:%s错误\n", szStatement);
				totalRoleCount = -1;

				return;
			}
			else
			{
				//本次影响的行数
				int affectNum = pResult->RowNum();
				ACE_UNUSED_ARG( affectNum );
				PlayerInfo_1_Base baseInfo;
				PlayerInfo_2_Equips equipInfo;

				// 处理结果集
				while( pResult->Next() )
				{
					if(totalRoleCount >= (int)(sizeof(playerInfoList)/sizeof(playerInfoList[0])))//到达最大角色个数
					{	
						D_DEBUG("账号为%s的角色数目超过%d个\n", pLoginInfo.szAccount, sizeof(playerInfoList)/sizeof(playerInfoList[0]));
						break;
					}

					StructMemSet(playerInfoList[totalRoleCount], 0x00, sizeof(DGPlayerInfoLogin));
					playerInfoList[totalRoleCount].playerID = pLoginInfo.playerID;
					playerInfoList[totalRoleCount].byFlag = DGPlayerInfoLogin::FLAG_OK;

					unsigned int uiID = pResult->GetInt(0);
					StructMemCpy(baseInfo, pResult->GetString(1), sizeof(baseInfo));
					StructMemCpy(equipInfo, pResult->GetString(2), sizeof(equipInfo));
					if( pResult->GetString(3))
					{
						StructMemCpy( (playerInfoList[totalRoleCount].fashion), pResult->GetString(3), sizeof(playerInfoList[totalRoleCount].fashion) );
					}
					else
					{
						StructMemSet( (playerInfoList[totalRoleCount].fashion), 0x00, sizeof(playerInfoList[totalRoleCount].fashion));
						playerInfoList[totalRoleCount].fashion.fashionSize = MAX_FASHION_COUNT;
					}

					PlayerInfoLogin *pPlayerInfo = &(playerInfoList[totalRoleCount].playerInfo);
					pPlayerInfo->uiID = uiID;
					pPlayerInfo->nameSize = ACE_OS::snprintf(pPlayerInfo->szNickName,MAX_NAME_SIZE,"%s", baseInfo.szNickName );
					pPlayerInfo->usLevel = baseInfo.usLevel;
					pPlayerInfo->ucRace = baseInfo.ucRace;
					pPlayerInfo->usClass = baseInfo.usClass;
					pPlayerInfo->shFlag = baseInfo.shFlag;
					pPlayerInfo->usHair = baseInfo.usHair;
					pPlayerInfo->usHairColor = baseInfo.usHairColor;
					pPlayerInfo->usFace = baseInfo.usFace;
					pPlayerInfo->usPortrait = baseInfo.usPortrait;
					StructMemCpy(pPlayerInfo->equipInfo, &equipInfo, sizeof(pPlayerInfo->equipInfo));
					pPlayerInfo->usMapID = baseInfo.usMapID;
					pPlayerInfo->iPosX = baseInfo.iPosX;
					pPlayerInfo->iPosY = baseInfo.iPosY;
					playerInfoList[totalRoleCount].tag = baseInfo.tag;

					//计数
					totalRoleCount++;
				}

				// 销毁查询
				pQuery->FreeResult(pResult);
			}

			// 销毁查询
			pTempConn->DestroyQuery(pQuery);
		}
	}

	TRY_END;
}
#endif/*MULTI_DB_HASH*/

void SelectRoleTask::OnThread(CConnection *pConnect)
{
	if ( !pConnect )
		return;

#ifndef MULTI_DB_HASH
	TRY_BEGIN;


	GDPlayerInfoLogin *pPlayerInfoLogin = (GDPlayerInfoLogin *)&pLoginInfo;

	const char *pszAccount =  pPlayerInfoLogin->szAccount;//搜索的帐号信息

	StructMemSet(playerInfoReturn, 0x00, sizeof(playerInfoReturn));
	playerInfoReturn.playerID = pPlayerInfoLogin->playerID;

	int iRet = 0;
	int iCount = 0;
	CQuery *pQuery = pConnect->CreateQuery();
	if(pQuery == NULL)
		iRet = -1;

	if ( iRet != -1 )
	{
		// 构造SQL
		char szStatement[1024];
		int iStatementLen = ACE_OS::snprintf(szStatement, sizeof(szStatement), 
			"SELECT ID, BaseData, EquipData, FashionData FROM playerinfo WHERE Account = '%s'", 
			pszAccount
			);

		if(iStatementLen <= 0 )
			iRet = -1;
		else
		{
			// 执行查询
			CResult * pResult = pQuery->ExecuteSelect((const char*)szStatement, iStatementLen);
			if(pResult == NULL)
			{
				D_ERROR("执行sql语句:%s错误\n", szStatement);
				iRet = -1;
			}
			else
			{
				//本次影响的行数
				int affectNum = pResult->RowNum();
				ACE_UNUSED_ARG( affectNum );
				PlayerInfo_1_Base baseInfo;
				PlayerInfo_2_Equips equipInfo;

				// 处理结果集
				while( pResult->Next() )
				{
					unsigned int uiID = pResult->GetInt(0);
					StructMemCpy(baseInfo, pResult->GetString(1), sizeof(baseInfo));
					StructMemCpy(equipInfo, pResult->GetString(2), sizeof(equipInfo));
					if( pResult->GetString(3))
						StructMemCpy( (playerInfoReturn.fashion), pResult->GetString(3), sizeof(playerInfoReturn.fashion) );
					else
					{
						StructMemSet( (playerInfoReturn.fashion), 0x00, sizeof(playerInfoReturn.fashion));
						playerInfoReturn.fashion.fashionSize = MAX_FASHION_COUNT;
					}

					PlayerInfoLogin *pPlayerInfo = &(playerInfoReturn.playerInfo);
					pPlayerInfo->uiID = uiID;
					pPlayerInfo->nameSize = ACE_OS::snprintf(pPlayerInfo->szNickName,MAX_NAME_SIZE,"%s", baseInfo.szNickName );
					pPlayerInfo->usLevel = baseInfo.usLevel;
					pPlayerInfo->ucRace = baseInfo.ucRace;
					pPlayerInfo->usClass = baseInfo.usClass;
					pPlayerInfo->bSex = baseInfo.bSex;
					pPlayerInfo->usHair = baseInfo.usHair;
					pPlayerInfo->usHairColor = baseInfo.usHairColor;
					pPlayerInfo->usFace = baseInfo.usFace;
					pPlayerInfo->usPortrait = baseInfo.usPortrait;
					StructMemCpy(pPlayerInfo->equipInfo, &equipInfo, sizeof(pPlayerInfo->equipInfo));
					pPlayerInfo->usMapID = baseInfo.usMapID;
					pPlayerInfo->iPosX = baseInfo.iPosX;
					pPlayerInfo->iPosY = baseInfo.iPosY;
					playerInfoReturn.tag = baseInfo.tag;

					//所读取的个数+1
					iCount++;

					playerInfoReturn.byFlag = DGPlayerInfoLogin::FLAG_OK;
					//MsgToPut *pMsgToPut = CreateSrvPkg(DGPlayerInfoLogin,mp_gateSrv,playerInfoReturn );
					//mp_gateSrv->SendPkgToSrv(pMsgToPut);
					MsgToPutContent* pContent = NEW MsgToPutContent;
					pContent->msgType = DGPlayerInfoLogin::wCmd;
					pContent->sessionID = m_gateID;//mp_gateSrv->GetSessionID();
					StructMemCpy( pContent->szContent, &playerInfoReturn, sizeof(playerInfoReturn) );
					GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );
				}
				
				// 销毁查询
				pQuery->FreeResult(pResult);
			}
		}

		// 销毁查询
		pConnect->DestroyQuery(pQuery);
	}

	//如果查询失败
	if ( iRet == -1 )
	{
		D_DEBUG("DB操作记录:检索角色列表:出错,账号=%s,数目=%d\n", pszAccount, iCount);
		playerInfoReturn.byFlag = DGPlayerInfoLogin::FLAG_INVALID;

		//MsgToPut *pMsgToPut = CreateSrvPkg(DGPlayerInfoLogin,mp_gateSrv,playerInfoReturn );
		//mp_gateSrv->SendPkgToSrv(pMsgToPut);

		MsgToPutContent* pContent = NEW MsgToPutContent;
		pContent->msgType = DGPlayerInfoLogin::wCmd;
		pContent->sessionID = m_gateID;//mp_gateSrv->GetSessionID();
		StructMemCpy( pContent->szContent, &playerInfoReturn, sizeof(playerInfoReturn) );
		GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );
	}
	else
	{
		D_DEBUG("DB操作记录:检索角色列表:成功,账号=%s,数目=%d\n", pszAccount, iCount);
		playerInfoReturn.byFlag = DGPlayerInfoLogin::FLAG_MSGEND;

		MsgToPutContent* pContent = NEW MsgToPutContent;
		pContent->msgType = DGPlayerInfoLogin::wCmd;
		pContent->sessionID = m_gateID;//mp_gateSrv->GetSessionID();
		StructMemCpy( pContent->szContent, &playerInfoReturn, sizeof(playerInfoReturn) );
		GateSrvMangerSingle::instance()->PushMsgContentToVecotr( pContent );

		//MsgToPut *pMsgToPut = CreateSrvPkg(DGPlayerInfoLogin,mp_gateSrv,playerInfoReturn );
		//mp_gateSrv->SendPkgToSrv(pMsgToPut);
	}

	TRY_END;
#endif/*MULTI_DB_HASH*/
}

void SelectRoleTask::OnResult()
{
#ifdef MULTI_DB_HASH
	if(totalRoleCount > 0)//存在角色
	{
		for(int i = 0; i < totalRoleCount; i++)
		{
			MsgToPutContent* pContent = NEW MsgToPutContent;
			pContent->msgType = DGPlayerInfoLogin::wCmd;
			pContent->sessionID = m_gateID;
			StructMemCpy( pContent->szContent, &playerInfoList[i], sizeof(playerInfoList[i]) );//确保sizeof(pContent->szContent)>=sizeof(playerInfoList[i])
			
			
			PUSH_MSG_TO_QUEUE( pContent );
		}
	}

	//总是发送如下消息
	StructMemSet(playerInfoReturn, 0x00, sizeof(playerInfoReturn));
	playerInfoReturn.playerID = pLoginInfo.playerID;
	playerInfoReturn.byFlag = (totalRoleCount >= 0) ? DGPlayerInfoLogin::FLAG_MSGEND :DGPlayerInfoLogin::FLAG_INVALID;

	MsgToPutContent* pContent = NEW MsgToPutContent;
	pContent->msgType = DGPlayerInfoLogin::wCmd;
	pContent->sessionID = m_gateID;
	StructMemCpy( pContent->szContent, &playerInfoReturn, sizeof(playerInfoReturn) );
	
	PUSH_MSG_TO_QUEUE( pContent );
#endif/*MULTI_DB_HASH*/
}
