﻿/** @file OtherServer.h 
@brief 
<pre>
*	Copyright (c) 2007，第九城市游戏研发中心
*	All rights reserved.
*
*	当前版本：
*	作    者：zhangsongwei
*	完成日期：2008-1-3
*
*	取代版本：
*	作    者：
*	完成日期：
</pre>*/

#ifndef OTHER_SERVER_H
#define OTHER_SERVER_H

#include "../../Base/BufQueue/BufQueue.h"
#include <map>
#include <vector>


class CServerConnection;

/**
* @class CSrvBase
*
* @brief 各服务端srv的基类
* 
*/
class CSrvBase
{
public:
	/// 构造
	explicit CSrvBase(CServerConnection* pConn);

	/// 析构
	virtual ~CSrvBase(void);

public:
#ifdef USE_DSIOCP
	UniqueObj< CDsSocket>* CSrvBase::GetUniqueDsSocket();
#endif //USE_DSIOCP
	///取handle;
	int GetHandleID(void);

	///取sessionID;
	int GetSessionID(void);

	///向外发包；
	void SendPkgToSrv(MsgToPut* pPkg);

	void SetLastHeartbeatConfirmTime(void);

	bool HeartbeatCheckOk(void);

	unsigned int IncSequenceID(void);

public:
	/// 收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen ) = 0;

private:
	/// 该服务对应的连接；
	CServerConnection* m_pConn;

};

class CGateServer : public CSrvBase
{
public:
	/// 构造
	explicit CGateServer(CServerConnection* pConn);

	/// 析构
	virtual ~CGateServer(void);

public:
	/// 收包处理；
	virtual void OnPkgRcved( unsigned short wCmd, const char* pBuf, unsigned short wPkgLen );

};


/**
* @class CGateServerManager
*
* @brief 管理gateserver
* 
*/
class CGateServerManager
{
private:
	CGateServerManager();
	~CGateServerManager();

public:
	/// 添加新gateserver;
	static int AddSrv(CGateServer* pServer);

	/// 删除gateserver;
	static void RemoveSrv(unsigned short usServerID);
	
	/// 得到特定gateserver;
	static CGateServer* FindSvr(unsigned short usServerID);

	/// 心跳检查定时器
	static void HeartbeartCheckTimer(void); 

private:
	/// 管理所有server
	static std::map<unsigned short, CGateServer*> m_gateServers;

};

#endif/*OTHER_SERVER_H*/
