﻿# Install script for directory: E:/Tech/mysql-connector-c++-1.0.5/cppconn

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "C:/Program Files/MySQL/ConnectorCPP")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/cppconn" TYPE FILE FILES
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/build_config.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/config.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/connection.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/datatype.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/driver.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/exception.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/metadata.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/parameter_metadata.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/prepared_statement.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/resultset.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/resultset_metadata.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/statement.h"
    "E:/Tech/mysql-connector-c++-1.0.5/cppconn/warning.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

